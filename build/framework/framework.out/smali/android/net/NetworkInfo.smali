.class public Landroid/net/NetworkInfo;
.super Ljava/lang/Object;
.source "NetworkInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/NetworkInfo$DetailedState;,
        Landroid/net/NetworkInfo$State;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/NetworkInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final stateMap:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Landroid/net/NetworkInfo$DetailedState;",
            "Landroid/net/NetworkInfo$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDetailedState:Landroid/net/NetworkInfo$DetailedState;

.field private mExtraInfo:Ljava/lang/String;

.field private mIsAvailable:Z

.field private mIsFailover:Z

.field private mIsRoaming:Z

.field private mNetworkType:I

.field private mReason:Ljava/lang/String;

.field private mSmCause:I

.field private mState:Landroid/net/NetworkInfo$State;

.field private mSubtype:I

.field private mSubtypeName:Ljava/lang/String;

.field private mTypeName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 92
    new-instance v0, Ljava/util/EnumMap;

    #@2
    const-class v1, Landroid/net/NetworkInfo$DetailedState;

    #@4
    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    #@7
    sput-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@9
    .line 96
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@b
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@d
    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@f
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    .line 97
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@14
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    #@16
    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@18
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 98
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@1d
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@1f
    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@21
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 99
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@26
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    #@28
    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@2a
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 100
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@2f
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    #@31
    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@33
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 101
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@38
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    #@3a
    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 102
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@41
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    #@43
    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@45
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 103
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@4a
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@4c
    sget-object v2, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@4e
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    .line 104
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@53
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->SUSPENDED:Landroid/net/NetworkInfo$DetailedState;

    #@55
    sget-object v2, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@57
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    .line 105
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@5c
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@5e
    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    #@60
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    .line 106
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@65
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@67
    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@69
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@6c
    .line 107
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@6e
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@70
    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@72
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@75
    .line 108
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@77
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    #@79
    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@7b
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    #@7e
    .line 506
    new-instance v0, Landroid/net/NetworkInfo$1;

    #@80
    invoke-direct {v0}, Landroid/net/NetworkInfo$1;-><init>()V

    #@83
    sput-object v0, Landroid/net/NetworkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@85
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 136
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "type"
    .parameter "subtype"
    .parameter "typeName"
    .parameter "subtypeName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 141
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 142
    invoke-static {p1}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_24

    #@b
    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "Invalid network type: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 145
    :cond_24
    iput p1, p0, Landroid/net/NetworkInfo;->mNetworkType:I

    #@26
    .line 146
    iput p2, p0, Landroid/net/NetworkInfo;->mSubtype:I

    #@28
    .line 147
    iput-object p3, p0, Landroid/net/NetworkInfo;->mTypeName:Ljava/lang/String;

    #@2a
    .line 148
    iput-object p4, p0, Landroid/net/NetworkInfo;->mSubtypeName:Ljava/lang/String;

    #@2c
    .line 149
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@2e
    invoke-virtual {p0, v0, v2, v2}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 150
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@33
    iput-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@35
    .line 151
    iput-boolean v1, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@37
    .line 152
    iput-boolean v1, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@39
    .line 154
    iput v1, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@3b
    .line 156
    return-void
.end method

.method public constructor <init>(Landroid/net/NetworkInfo;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 160
    if-eqz p1, :cond_35

    #@5
    .line 161
    iget v0, p1, Landroid/net/NetworkInfo;->mNetworkType:I

    #@7
    iput v0, p0, Landroid/net/NetworkInfo;->mNetworkType:I

    #@9
    .line 162
    iget v0, p1, Landroid/net/NetworkInfo;->mSubtype:I

    #@b
    iput v0, p0, Landroid/net/NetworkInfo;->mSubtype:I

    #@d
    .line 163
    iget-object v0, p1, Landroid/net/NetworkInfo;->mTypeName:Ljava/lang/String;

    #@f
    iput-object v0, p0, Landroid/net/NetworkInfo;->mTypeName:Ljava/lang/String;

    #@11
    .line 164
    iget-object v0, p1, Landroid/net/NetworkInfo;->mSubtypeName:Ljava/lang/String;

    #@13
    iput-object v0, p0, Landroid/net/NetworkInfo;->mSubtypeName:Ljava/lang/String;

    #@15
    .line 165
    iget-object v0, p1, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@17
    iput-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@19
    .line 166
    iget-object v0, p1, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@1b
    iput-object v0, p0, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@1d
    .line 167
    iget-object v0, p1, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@1f
    iput-object v0, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@21
    .line 168
    iget-object v0, p1, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@23
    iput-object v0, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@25
    .line 169
    iget-boolean v0, p1, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@27
    iput-boolean v0, p0, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@29
    .line 170
    iget-boolean v0, p1, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@2b
    iput-boolean v0, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@2d
    .line 171
    iget-boolean v0, p1, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@2f
    iput-boolean v0, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@31
    .line 173
    iget v0, p1, Landroid/net/NetworkInfo;->mSmCause:I

    #@33
    iput v0, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@35
    .line 176
    :cond_35
    return-void
.end method

.method static synthetic access$002(Landroid/net/NetworkInfo;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-object p1, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Landroid/net/NetworkInfo;Landroid/net/NetworkInfo$DetailedState;)Landroid/net/NetworkInfo$DetailedState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-object p1, p0, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@2
    return-object p1
.end method

.method static synthetic access$202(Landroid/net/NetworkInfo;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-boolean p1, p0, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@2
    return p1
.end method

.method static synthetic access$302(Landroid/net/NetworkInfo;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-boolean p1, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@2
    return p1
.end method

.method static synthetic access$402(Landroid/net/NetworkInfo;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-boolean p1, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@2
    return p1
.end method

.method static synthetic access$502(Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-object p1, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$602(Landroid/net/NetworkInfo;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput-object p1, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$702(Landroid/net/NetworkInfo;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    iput p1, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@2
    return p1
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 476
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDetailedState()Landroid/net/NetworkInfo$DetailedState;
    .registers 2

    #@0
    .prologue
    .line 370
    monitor-enter p0

    #@1
    .line 371
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 372
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getExtraInfo()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 423
    monitor-enter p0

    #@1
    .line 424
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 425
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getReason()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 411
    monitor-enter p0

    #@1
    .line 412
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 413
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getSmCause()I
    .registers 2

    #@0
    .prologue
    .line 446
    monitor-enter p0

    #@1
    .line 447
    :try_start_1
    iget v0, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 448
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getState()Landroid/net/NetworkInfo$State;
    .registers 2

    #@0
    .prologue
    .line 360
    monitor-enter p0

    #@1
    .line 361
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 362
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getSubtype()I
    .registers 2

    #@0
    .prologue
    .line 198
    monitor-enter p0

    #@1
    .line 199
    :try_start_1
    iget v0, p0, Landroid/net/NetworkInfo;->mSubtype:I

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 200
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getSubtypeName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 226
    monitor-enter p0

    #@1
    .line 227
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mSubtypeName:Ljava/lang/String;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 228
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 187
    monitor-enter p0

    #@1
    .line 188
    :try_start_1
    iget v0, p0, Landroid/net/NetworkInfo;->mNetworkType:I

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 189
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getTypeName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 216
    monitor-enter p0

    #@1
    .line 217
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mTypeName:Ljava/lang/String;

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    .line 218
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 295
    monitor-enter p0

    #@1
    .line 296
    :try_start_1
    iget-boolean v0, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 297
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public isConnected()Z
    .registers 3

    #@0
    .prologue
    .line 253
    monitor-enter p0

    #@1
    .line 254
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@3
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@5
    if-ne v0, v1, :cond_a

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    monitor-exit p0

    #@9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_8

    #@c
    .line 255
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public isConnectedOrConnecting()Z
    .registers 3

    #@0
    .prologue
    .line 241
    monitor-enter p0

    #@1
    .line 242
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@3
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@5
    if-eq v0, v1, :cond_d

    #@7
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@9
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    #@b
    if-ne v0, v1, :cond_10

    #@d
    :cond_d
    const/4 v0, 0x1

    #@e
    :goto_e
    monitor-exit p0

    #@f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_e

    #@12
    .line 243
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public isConnectedOrSuspended()Z
    .registers 3

    #@0
    .prologue
    .line 264
    monitor-enter p0

    #@1
    .line 265
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@3
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    #@5
    if-eq v0, v1, :cond_d

    #@7
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@9
    sget-object v1, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@b
    if-ne v0, v1, :cond_10

    #@d
    :cond_d
    const/4 v0, 0x1

    #@e
    :goto_e
    monitor-exit p0

    #@f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_e

    #@12
    .line 266
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public isFailover()Z
    .registers 2

    #@0
    .prologue
    .line 320
    monitor-enter p0

    #@1
    .line 321
    :try_start_1
    iget-boolean v0, p0, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 322
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public isRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 344
    monitor-enter p0

    #@1
    .line 345
    :try_start_1
    iget-boolean v0, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 346
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public isSuspended()Z
    .registers 3

    #@0
    .prologue
    .line 274
    monitor-enter p0

    #@1
    .line 275
    :try_start_1
    iget-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@3
    sget-object v1, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    #@5
    if-ne v0, v1, :cond_a

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    monitor-exit p0

    #@9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_8

    #@c
    .line 276
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "detailedState"
    .parameter "reason"
    .parameter "extraInfo"

    #@0
    .prologue
    .line 385
    monitor-enter p0

    #@1
    .line 386
    :try_start_1
    iput-object p1, p0, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@3
    .line 387
    sget-object v0, Landroid/net/NetworkInfo;->stateMap:Ljava/util/EnumMap;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/net/NetworkInfo$State;

    #@b
    iput-object v0, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@d
    .line 388
    iput-object p2, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@f
    .line 389
    iput-object p3, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@11
    .line 390
    monitor-exit p0

    #@12
    .line 391
    return-void

    #@13
    .line 390
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public setExtraInfo(Ljava/lang/String;)V
    .registers 3
    .parameter "extraInfo"

    #@0
    .prologue
    .line 400
    monitor-enter p0

    #@1
    .line 401
    :try_start_1
    iput-object p1, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@3
    .line 402
    monitor-exit p0

    #@4
    .line 403
    return-void

    #@5
    .line 402
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public setFailover(Z)V
    .registers 3
    .parameter "isFailover"

    #@0
    .prologue
    .line 332
    monitor-enter p0

    #@1
    .line 333
    :try_start_1
    iput-boolean p1, p0, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@3
    .line 334
    monitor-exit p0

    #@4
    .line 335
    return-void

    #@5
    .line 334
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public setIsAvailable(Z)V
    .registers 3
    .parameter "isAvailable"

    #@0
    .prologue
    .line 307
    monitor-enter p0

    #@1
    .line 308
    :try_start_1
    iput-boolean p1, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@3
    .line 309
    monitor-exit p0

    #@4
    .line 310
    return-void

    #@5
    .line 309
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method setRoaming(Z)V
    .registers 3
    .parameter "isRoaming"

    #@0
    .prologue
    .line 350
    monitor-enter p0

    #@1
    .line 351
    :try_start_1
    iput-boolean p1, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@3
    .line 352
    monitor-exit p0

    #@4
    .line 353
    return-void

    #@5
    .line 352
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public setSmCause(I)V
    .registers 3
    .parameter "smCause"

    #@0
    .prologue
    .line 435
    monitor-enter p0

    #@1
    .line 436
    :try_start_1
    iput p1, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@3
    .line 437
    monitor-exit p0

    #@4
    .line 438
    return-void

    #@5
    .line 437
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method setSubtype(ILjava/lang/String;)V
    .registers 4
    .parameter "subtype"
    .parameter "subtypeName"

    #@0
    .prologue
    .line 204
    monitor-enter p0

    #@1
    .line 205
    :try_start_1
    iput p1, p0, Landroid/net/NetworkInfo;->mSubtype:I

    #@3
    .line 206
    iput-object p2, p0, Landroid/net/NetworkInfo;->mSubtypeName:Ljava/lang/String;

    #@5
    .line 207
    monitor-exit p0

    #@6
    .line 208
    return-void

    #@7
    .line 207
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 454
    monitor-enter p0

    #@1
    .line 455
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    const-string v1, "NetworkInfo: "

    #@5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8
    .line 456
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "type: "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "["

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, "], state: "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget-object v2, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, "/"

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-object v2, p0, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, ", reason: "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    iget-object v1, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@45
    if-nez v1, :cond_92

    #@47
    const-string v1, "(unspecified)"

    #@49
    :goto_49
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, ", extra: "

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    iget-object v1, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@55
    if-nez v1, :cond_95

    #@57
    const-string v1, "(none)"

    #@59
    :goto_59
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    const-string v2, ", roaming: "

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    iget-boolean v2, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    const-string v2, ", failover: "

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    iget-boolean v2, p0, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    const-string v2, ", isAvailable: "

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    iget-boolean v2, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@7d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    const-string v2, ", smCause: "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    iget v2, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    .line 467
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    monitor-exit p0

    #@91
    return-object v1

    #@92
    .line 456
    :cond_92
    iget-object v1, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@94
    goto :goto_49

    #@95
    :cond_95
    iget-object v1, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@97
    goto :goto_59

    #@98
    .line 468
    .end local v0           #builder:Ljava/lang/StringBuilder;
    :catchall_98
    move-exception v1

    #@99
    monitor-exit p0
    :try_end_9a
    .catchall {:try_start_1 .. :try_end_9a} :catchall_98

    #@9a
    throw v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 484
    monitor-enter p0

    #@3
    .line 485
    :try_start_3
    iget v2, p0, Landroid/net/NetworkInfo;->mNetworkType:I

    #@5
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 486
    iget v2, p0, Landroid/net/NetworkInfo;->mSubtype:I

    #@a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 487
    iget-object v2, p0, Landroid/net/NetworkInfo;->mTypeName:Ljava/lang/String;

    #@f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 488
    iget-object v2, p0, Landroid/net/NetworkInfo;->mSubtypeName:Ljava/lang/String;

    #@14
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 489
    iget-object v2, p0, Landroid/net/NetworkInfo;->mState:Landroid/net/NetworkInfo$State;

    #@19
    invoke-virtual {v2}, Landroid/net/NetworkInfo$State;->name()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 490
    iget-object v2, p0, Landroid/net/NetworkInfo;->mDetailedState:Landroid/net/NetworkInfo$DetailedState;

    #@22
    invoke-virtual {v2}, Landroid/net/NetworkInfo$DetailedState;->name()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@29
    .line 491
    iget-boolean v2, p0, Landroid/net/NetworkInfo;->mIsFailover:Z

    #@2b
    if-eqz v2, :cond_51

    #@2d
    move v2, v0

    #@2e
    :goto_2e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 492
    iget-boolean v2, p0, Landroid/net/NetworkInfo;->mIsAvailable:Z

    #@33
    if-eqz v2, :cond_53

    #@35
    move v2, v0

    #@36
    :goto_36
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 493
    iget-boolean v2, p0, Landroid/net/NetworkInfo;->mIsRoaming:Z

    #@3b
    if-eqz v2, :cond_55

    #@3d
    :goto_3d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 494
    iget-object v0, p0, Landroid/net/NetworkInfo;->mReason:Ljava/lang/String;

    #@42
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@45
    .line 495
    iget-object v0, p0, Landroid/net/NetworkInfo;->mExtraInfo:Ljava/lang/String;

    #@47
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4a
    .line 497
    iget v0, p0, Landroid/net/NetworkInfo;->mSmCause:I

    #@4c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    .line 499
    monitor-exit p0

    #@50
    .line 500
    return-void

    #@51
    :cond_51
    move v2, v1

    #@52
    .line 491
    goto :goto_2e

    #@53
    :cond_53
    move v2, v1

    #@54
    .line 492
    goto :goto_36

    #@55
    :cond_55
    move v0, v1

    #@56
    .line 493
    goto :goto_3d

    #@57
    .line 499
    :catchall_57
    move-exception v0

    #@58
    monitor-exit p0
    :try_end_59
    .catchall {:try_start_3 .. :try_end_59} :catchall_57

    #@59
    throw v0
.end method
