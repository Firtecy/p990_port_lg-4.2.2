.class public Landroid/net/VpnService$Builder;
.super Ljava/lang/Object;
.source "VpnService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/VpnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Builder"
.end annotation


# instance fields
.field private final mAddresses:Ljava/lang/StringBuilder;

.field private final mConfig:Lcom/android/internal/net/VpnConfig;

.field private final mRoutes:Ljava/lang/StringBuilder;

.field final synthetic this$0:Landroid/net/VpnService;


# direct methods
.method public constructor <init>(Landroid/net/VpnService;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 259
    iput-object p1, p0, Landroid/net/VpnService$Builder;->this$0:Landroid/net/VpnService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 255
    new-instance v0, Lcom/android/internal/net/VpnConfig;

    #@7
    invoke-direct {v0}, Lcom/android/internal/net/VpnConfig;-><init>()V

    #@a
    iput-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@c
    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    iput-object v0, p0, Landroid/net/VpnService$Builder;->mAddresses:Ljava/lang/StringBuilder;

    #@13
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    iput-object v0, p0, Landroid/net/VpnService$Builder;->mRoutes:Ljava/lang/StringBuilder;

    #@1a
    .line 260
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@1c
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@26
    .line 261
    return-void
.end method

.method private check(Ljava/net/InetAddress;I)V
    .registers 5
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 302
    invoke-virtual {p1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Bad address"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 305
    :cond_e
    instance-of v0, p1, Ljava/net/Inet4Address;

    #@10
    if-eqz v0, :cond_20

    #@12
    .line 306
    if-ltz p2, :cond_18

    #@14
    const/16 v0, 0x20

    #@16
    if-le p2, v0, :cond_3a

    #@18
    .line 307
    :cond_18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v1, "Bad prefixLength"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 309
    :cond_20
    instance-of v0, p1, Ljava/net/Inet6Address;

    #@22
    if-eqz v0, :cond_32

    #@24
    .line 310
    if-ltz p2, :cond_2a

    #@26
    const/16 v0, 0x80

    #@28
    if-le p2, v0, :cond_3a

    #@2a
    .line 311
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2c
    const-string v1, "Bad prefixLength"

    #@2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 314
    :cond_32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@34
    const-string v1, "Unsupported family"

    #@36
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@39
    throw v0

    #@3a
    .line 316
    :cond_3a
    return-void
.end method


# virtual methods
.method public addAddress(Ljava/lang/String;I)Landroid/net/VpnService$Builder;
    .registers 4
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 345
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p2}, Landroid/net/VpnService$Builder;->addAddress(Ljava/net/InetAddress;I)Landroid/net/VpnService$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public addAddress(Ljava/net/InetAddress;I)Landroid/net/VpnService$Builder;
    .registers 6
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 326
    invoke-direct {p0, p1, p2}, Landroid/net/VpnService$Builder;->check(Ljava/net/InetAddress;I)V

    #@3
    .line 328
    invoke-virtual {p1}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v1, "Bad address"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 332
    :cond_11
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mAddresses:Ljava/lang/StringBuilder;

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const/16 v2, 0x20

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const/16 v2, 0x2f

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 333
    return-object p0
.end method

.method public addDnsServer(Ljava/lang/String;)Landroid/net/VpnService$Builder;
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 410
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/net/VpnService$Builder;->addDnsServer(Ljava/net/InetAddress;)Landroid/net/VpnService$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public addDnsServer(Ljava/net/InetAddress;)Landroid/net/VpnService$Builder;
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 391
    invoke-virtual {p1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p1}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 392
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v1, "Bad address"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 394
    :cond_14
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@16
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@18
    if-nez v0, :cond_23

    #@1a
    .line 395
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@1c
    new-instance v1, Ljava/util/ArrayList;

    #@1e
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@21
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@23
    .line 397
    :cond_23
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@25
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@27
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2e
    .line 398
    return-object p0
.end method

.method public addRoute(Ljava/lang/String;I)Landroid/net/VpnService$Builder;
    .registers 4
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 380
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p2}, Landroid/net/VpnService$Builder;->addRoute(Ljava/net/InetAddress;I)Landroid/net/VpnService$Builder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public addRoute(Ljava/net/InetAddress;I)Landroid/net/VpnService$Builder;
    .registers 10
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 355
    invoke-direct {p0, p1, p2}, Landroid/net/VpnService$Builder;->check(Ljava/net/InetAddress;I)V

    #@3
    .line 357
    div-int/lit8 v1, p2, 0x8

    #@5
    .line 358
    .local v1, offset:I
    invoke-virtual {p1}, Ljava/net/InetAddress;->getAddress()[B

    #@8
    move-result-object v0

    #@9
    .line 359
    .local v0, bytes:[B
    array-length v2, v0

    #@a
    if-ge v1, v2, :cond_26

    #@c
    .line 360
    aget-byte v2, v0, v1

    #@e
    rem-int/lit8 v3, p2, 0x8

    #@10
    shl-int/2addr v2, v3

    #@11
    int-to-byte v2, v2

    #@12
    aput-byte v2, v0, v1

    #@14
    :goto_14
    array-length v2, v0

    #@15
    if-ge v1, v2, :cond_26

    #@17
    .line 361
    aget-byte v2, v0, v1

    #@19
    if-eqz v2, :cond_23

    #@1b
    .line 362
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v3, "Bad address"

    #@1f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v2

    #@23
    .line 360
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_14

    #@26
    .line 367
    :cond_26
    iget-object v2, p0, Landroid/net/VpnService$Builder;->mRoutes:Ljava/lang/StringBuilder;

    #@28
    const-string v3, " %s/%d"

    #@2a
    const/4 v4, 0x2

    #@2b
    new-array v4, v4, [Ljava/lang/Object;

    #@2d
    const/4 v5, 0x0

    #@2e
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    aput-object v6, v4, v5

    #@34
    const/4 v5, 0x1

    #@35
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v6

    #@39
    aput-object v6, v4, v5

    #@3b
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 368
    return-object p0
.end method

.method public addSearchDomain(Ljava/lang/String;)Landroid/net/VpnService$Builder;
    .registers 4
    .parameter "domain"

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@4
    if-nez v0, :cond_f

    #@6
    .line 418
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@8
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@f
    .line 420
    :cond_f
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@11
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@13
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@16
    .line 421
    return-object p0
.end method

.method public establish()Landroid/os/ParcelFileDescriptor;
    .registers 4

    #@0
    .prologue
    .line 468
    iget-object v1, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2
    iget-object v2, p0, Landroid/net/VpnService$Builder;->mAddresses:Ljava/lang/StringBuilder;

    #@4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@a
    .line 469
    iget-object v1, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@c
    iget-object v2, p0, Landroid/net/VpnService$Builder;->mRoutes:Ljava/lang/StringBuilder;

    #@e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@14
    .line 472
    :try_start_14
    invoke-static {}, Landroid/net/VpnService;->access$100()Landroid/net/IConnectivityManager;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@1a
    invoke-interface {v1, v2}, Landroid/net/IConnectivityManager;->establishVpn(Lcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_1d} :catch_1f

    #@1d
    move-result-object v1

    #@1e
    return-object v1

    #@1f
    .line 473
    :catch_1f
    move-exception v0

    #@20
    .line 474
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/IllegalStateException;

    #@22
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    #@25
    throw v1
.end method

.method public setConfigureIntent(Landroid/app/PendingIntent;)Landroid/net/VpnService$Builder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2
    iput-object p1, v0, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    #@4
    .line 280
    return-object p0
.end method

.method public setMtu(I)Landroid/net/VpnService$Builder;
    .registers 4
    .parameter "mtu"

    #@0
    .prologue
    .line 291
    if-gtz p1, :cond_a

    #@2
    .line 292
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Bad mtu"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 294
    :cond_a
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@c
    iput p1, v0, Lcom/android/internal/net/VpnConfig;->mtu:I

    #@e
    .line 295
    return-object p0
.end method

.method public setSession(Ljava/lang/String;)Landroid/net/VpnService$Builder;
    .registers 3
    .parameter "session"

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Landroid/net/VpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    #@2
    iput-object p1, v0, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    #@4
    .line 270
    return-object p0
.end method
