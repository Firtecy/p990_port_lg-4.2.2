.class public Landroid/net/LinkAddress;
.super Ljava/lang/Object;
.source "LinkAddress.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/LinkAddress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final address:Ljava/net/InetAddress;

.field private final prefixLength:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 129
    new-instance v0, Landroid/net/LinkAddress$1;

    #@2
    invoke-direct {v0}, Landroid/net/LinkAddress$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/LinkAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/net/InetAddress;I)V
    .registers 6
    .parameter "address"
    .parameter "prefixLength"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    if-eqz p1, :cond_13

    #@5
    if-ltz p2, :cond_13

    #@7
    instance-of v0, p1, Ljava/net/Inet4Address;

    #@9
    if-eqz v0, :cond_f

    #@b
    const/16 v0, 0x20

    #@d
    if-gt p2, v0, :cond_13

    #@f
    :cond_f
    const/16 v0, 0x80

    #@11
    if-le p2, v0, :cond_30

    #@13
    .line 46
    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Bad LinkAddress params "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v0

    #@30
    .line 49
    :cond_30
    iput-object p1, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@32
    .line 50
    iput p2, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@34
    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/net/InterfaceAddress;)V
    .registers 3
    .parameter "interfaceAddress"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    invoke-virtual {p1}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@9
    .line 55
    invoke-virtual {p1}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@f
    .line 56
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 108
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 73
    instance-of v2, p1, Landroid/net/LinkAddress;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 77
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 76
    check-cast v0, Landroid/net/LinkAddress;

    #@9
    .line 77
    .local v0, linkAddress:Landroid/net/LinkAddress;
    iget-object v2, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@b
    iget-object v3, v0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@d
    invoke-virtual {v2, v3}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_5

    #@13
    iget v2, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@15
    iget v3, v0, Landroid/net/LinkAddress;->prefixLength:I

    #@17
    if-ne v2, v3, :cond_5

    #@19
    const/4 v1, 0x1

    #@1a
    goto :goto_5
.end method

.method public getAddress()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@2
    return-object v0
.end method

.method public getNetworkPrefixLength()I
    .registers 2

    #@0
    .prologue
    .line 100
    iget v0, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@2
    if-nez v0, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    iget v1, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@7
    add-int/2addr v0, v1

    #@8
    return v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@b
    invoke-virtual {v0}, Ljava/net/InetAddress;->hashCode()I

    #@e
    move-result v0

    #@f
    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@2
    if-nez v0, :cond_7

    #@4
    const-string v0, ""

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    iget-object v1, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@e
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    const-string v1, "/"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    iget v1, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    goto :goto_6
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 117
    const/4 v0, 0x1

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@8
    .line 118
    iget-object v0, p0, Landroid/net/LinkAddress;->address:Ljava/net/InetAddress;

    #@a
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@d
    move-result-object v0

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@11
    .line 119
    iget v0, p0, Landroid/net/LinkAddress;->prefixLength:I

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 123
    :goto_16
    return-void

    #@17
    .line 121
    :cond_17
    const/4 v0, 0x0

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    #@1b
    goto :goto_16
.end method
