.class public final Landroid/net/Proxy;
.super Ljava/lang/Object;
.source "Proxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/Proxy$AndroidProxySelectorRoutePlanner;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final EXCLLIST_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final EXCLLIST_REGEXP:Ljava/lang/String; = "$|^(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*)+(,(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*))*$"

.field public static final EXTRA_PROXY_INFO:Ljava/lang/String; = "proxy"

.field private static final HOSTNAME_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final HOSTNAME_REGEXP:Ljava/lang/String; = "^$|^[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*$"

.field private static final NAME_IP_REGEX:Ljava/lang/String; = "[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*"

.field public static final PROXY_CHANGE_ACTION:Ljava/lang/String; = "android.intent.action.PROXY_CHANGE"

.field private static final TAG:Ljava/lang/String; = "Proxy"

.field private static sConnectivityManager:Landroid/net/ConnectivityManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/net/Proxy;->sConnectivityManager:Landroid/net/ConnectivityManager;

    #@3
    .line 97
    const-string v0, "^$|^[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*$"

    #@5
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Landroid/net/Proxy;->HOSTNAME_PATTERN:Ljava/util/regex/Pattern;

    #@b
    .line 98
    const-string v0, "$|^(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*)+(,(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*))*$"

    #@d
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@10
    move-result-object v0

    #@11
    sput-object v0, Landroid/net/Proxy;->EXCLLIST_PATTERN:Ljava/util/regex/Pattern;

    #@13
    .line 99
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 283
    return-void
.end method

.method public static final getAndroidProxySelectorRoutePlanner(Landroid/content/Context;)Lorg/apache/http/conn/routing/HttpRoutePlanner;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 320
    new-instance v0, Landroid/net/Proxy$AndroidProxySelectorRoutePlanner;

    #@2
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    #@4
    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    #@7
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    #@a
    move-result-object v2

    #@b
    invoke-direct {v0, v1, v2, p0}, Landroid/net/Proxy$AndroidProxySelectorRoutePlanner;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;Ljava/net/ProxySelector;Landroid/content/Context;)V

    #@e
    .line 322
    .local v0, ret:Landroid/net/Proxy$AndroidProxySelectorRoutePlanner;
    return-object v0
.end method

.method public static final getDefaultHost()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 183
    const-string v1, "http.proxyHost"

    #@2
    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 184
    .local v0, host:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_d

    #@c
    const/4 v0, 0x0

    #@d
    .line 185
    .end local v0           #host:Ljava/lang/String;
    :cond_d
    return-object v0
.end method

.method public static final getDefaultPort()I
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 197
    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 201
    .local v0, e:Ljava/lang/NumberFormatException;
    :goto_7
    return v1

    #@8
    .line 199
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :cond_8
    :try_start_8
    const-string v2, "http.proxyPort"

    #@a
    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_11
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_11} :catch_13

    #@11
    move-result v1

    #@12
    goto :goto_7

    #@13
    .line 200
    :catch_13
    move-exception v0

    #@14
    .line 201
    .restart local v0       #e:Ljava/lang/NumberFormatException;
    goto :goto_7
.end method

.method public static final getHost(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "ctx"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 148
    invoke-static {p0, v3}, Landroid/net/Proxy;->getProxy(Landroid/content/Context;Ljava/lang/String;)Ljava/net/Proxy;

    #@4
    move-result-object v1

    #@5
    .line 149
    .local v1, proxy:Ljava/net/Proxy;
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@7
    if-ne v1, v2, :cond_b

    #@9
    move-object v2, v3

    #@a
    .line 153
    :goto_a
    return-object v2

    #@b
    .line 151
    :cond_b
    :try_start_b
    invoke-virtual {v1}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljava/net/InetSocketAddress;

    #@11
    check-cast v2, Ljava/net/InetSocketAddress;

    #@13
    invoke-virtual {v2}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_16} :catch_18

    #@16
    move-result-object v2

    #@17
    goto :goto_a

    #@18
    .line 152
    :catch_18
    move-exception v0

    #@19
    .local v0, e:Ljava/lang/Exception;
    move-object v2, v3

    #@1a
    .line 153
    goto :goto_a
.end method

.method public static final getPort(Landroid/content/Context;)I
    .registers 5
    .parameter "ctx"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 165
    const/4 v2, 0x0

    #@2
    invoke-static {p0, v2}, Landroid/net/Proxy;->getProxy(Landroid/content/Context;Ljava/lang/String;)Ljava/net/Proxy;

    #@5
    move-result-object v1

    #@6
    .line 166
    .local v1, proxy:Ljava/net/Proxy;
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@8
    if-ne v1, v2, :cond_c

    #@a
    move v2, v3

    #@b
    .line 170
    :goto_b
    return v2

    #@c
    .line 168
    :cond_c
    :try_start_c
    invoke-virtual {v1}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Ljava/net/InetSocketAddress;

    #@12
    check-cast v2, Ljava/net/InetSocketAddress;

    #@14
    invoke-virtual {v2}, Ljava/net/InetSocketAddress;->getPort()I
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_17} :catch_19

    #@17
    move-result v2

    #@18
    goto :goto_b

    #@19
    .line 169
    :catch_19
    move-exception v0

    #@1a
    .local v0, e:Ljava/lang/Exception;
    move v2, v3

    #@1b
    .line 170
    goto :goto_b
.end method

.method public static final getPreferredHttpHost(Landroid/content/Context;Ljava/lang/String;)Lorg/apache/http/HttpHost;
    .registers 8
    .parameter "context"
    .parameter "url"

    #@0
    .prologue
    .line 220
    invoke-static {p0, p1}, Landroid/net/Proxy;->getProxy(Landroid/content/Context;Ljava/lang/String;)Ljava/net/Proxy;

    #@3
    move-result-object v0

    #@4
    .line 221
    .local v0, prefProxy:Ljava/net/Proxy;
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@6
    invoke-virtual {v0, v2}, Ljava/net/Proxy;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_e

    #@c
    .line 222
    const/4 v2, 0x0

    #@d
    .line 225
    :goto_d
    return-object v2

    #@e
    .line 224
    :cond_e
    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Ljava/net/InetSocketAddress;

    #@14
    .line 225
    .local v1, sa:Ljava/net/InetSocketAddress;
    new-instance v2, Lorg/apache/http/HttpHost;

    #@16
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I

    #@1d
    move-result v4

    #@1e
    const-string v5, "http"

    #@20
    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@23
    goto :goto_d
.end method

.method public static final getProxy(Landroid/content/Context;Ljava/lang/String;)Ljava/net/Proxy;
    .registers 6
    .parameter "ctx"
    .parameter "url"

    #@0
    .prologue
    .line 111
    const-string v0, ""

    #@2
    .line 112
    .local v0, host:Ljava/lang/String;
    if-eqz p1, :cond_c

    #@4
    .line 113
    invoke-static {p1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    #@7
    move-result-object v2

    #@8
    .line 114
    .local v2, uri:Ljava/net/URI;
    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 117
    .end local v2           #uri:Ljava/net/URI;
    :cond_c
    invoke-static {v0}, Landroid/net/Proxy;->isLocalHost(Ljava/lang/String;)Z

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_3a

    #@12
    .line 118
    sget-object v3, Landroid/net/Proxy;->sConnectivityManager:Landroid/net/ConnectivityManager;

    #@14
    if-nez v3, :cond_20

    #@16
    .line 119
    const-string v3, "connectivity"

    #@18
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    check-cast v3, Landroid/net/ConnectivityManager;

    #@1e
    sput-object v3, Landroid/net/Proxy;->sConnectivityManager:Landroid/net/ConnectivityManager;

    #@20
    .line 122
    :cond_20
    sget-object v3, Landroid/net/Proxy;->sConnectivityManager:Landroid/net/ConnectivityManager;

    #@22
    if-nez v3, :cond_27

    #@24
    sget-object v3, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@26
    .line 134
    :goto_26
    return-object v3

    #@27
    .line 124
    :cond_27
    sget-object v3, Landroid/net/Proxy;->sConnectivityManager:Landroid/net/ConnectivityManager;

    #@29
    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getProxy()Landroid/net/ProxyProperties;

    #@2c
    move-result-object v1

    #@2d
    .line 126
    .local v1, proxyProperties:Landroid/net/ProxyProperties;
    if-eqz v1, :cond_3a

    #@2f
    .line 128
    invoke-virtual {v1, p1}, Landroid/net/ProxyProperties;->isExcluded(Ljava/lang/String;)Z

    #@32
    move-result v3

    #@33
    if-nez v3, :cond_3a

    #@35
    .line 129
    invoke-virtual {v1}, Landroid/net/ProxyProperties;->makeProxy()Ljava/net/Proxy;

    #@38
    move-result-object v3

    #@39
    goto :goto_26

    #@3a
    .line 134
    .end local v1           #proxyProperties:Landroid/net/ProxyProperties;
    :cond_3a
    sget-object v3, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@3c
    goto :goto_26
.end method

.method private static final isLocalHost(Ljava/lang/String;)Z
    .registers 4
    .parameter "host"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 230
    if-nez p0, :cond_5

    #@4
    .line 244
    :cond_4
    :goto_4
    return v0

    #@5
    .line 234
    :cond_5
    if-eqz p0, :cond_4

    #@7
    .line 235
    :try_start_7
    const-string/jumbo v2, "localhost"

    #@a
    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_12

    #@10
    move v0, v1

    #@11
    .line 236
    goto :goto_4

    #@12
    .line 238
    :cond_12
    invoke-static {p0}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/net/InetAddress;->isLoopbackAddress()Z
    :try_end_19
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_19} :catch_1e

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_4

    #@1c
    move v0, v1

    #@1d
    .line 239
    goto :goto_4

    #@1e
    .line 242
    :catch_1e
    move-exception v1

    #@1f
    goto :goto_4
.end method

.method public static final setHttpProxySystemProperty(Landroid/net/ProxyProperties;)V
    .registers 5
    .parameter "p"

    #@0
    .prologue
    .line 327
    const/4 v1, 0x0

    #@1
    .line 328
    .local v1, host:Ljava/lang/String;
    const/4 v2, 0x0

    #@2
    .line 329
    .local v2, port:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .line 330
    .local v0, exclList:Ljava/lang/String;
    if-eqz p0, :cond_15

    #@5
    .line 331
    invoke-virtual {p0}, Landroid/net/ProxyProperties;->getHost()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 332
    invoke-virtual {p0}, Landroid/net/ProxyProperties;->getPort()I

    #@c
    move-result v3

    #@d
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    .line 333
    invoke-virtual {p0}, Landroid/net/ProxyProperties;->getExclusionList()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 335
    :cond_15
    invoke-static {v1, v2, v0}, Landroid/net/Proxy;->setHttpProxySystemProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 336
    return-void
.end method

.method public static final setHttpProxySystemProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "host"
    .parameter "port"
    .parameter "exclList"

    #@0
    .prologue
    .line 340
    if-eqz p2, :cond_b

    #@2
    const-string v0, ","

    #@4
    const-string/jumbo v1, "|"

    #@7
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@a
    move-result-object p2

    #@b
    .line 342
    :cond_b
    if-eqz p0, :cond_30

    #@d
    .line 343
    const-string v0, "http.proxyHost"

    #@f
    invoke-static {v0, p0}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12
    .line 344
    const-string v0, "https.proxyHost"

    #@14
    invoke-static {v0, p0}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    .line 349
    :goto_17
    if-eqz p1, :cond_3b

    #@19
    .line 350
    const-string v0, "http.proxyPort"

    #@1b
    invoke-static {v0, p1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    .line 351
    const-string v0, "https.proxyPort"

    #@20
    invoke-static {v0, p1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@23
    .line 356
    :goto_23
    if-eqz p2, :cond_46

    #@25
    .line 357
    const-string v0, "http.nonProxyHosts"

    #@27
    invoke-static {v0, p2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    .line 358
    const-string v0, "https.nonProxyHosts"

    #@2c
    invoke-static {v0, p2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2f
    .line 363
    :goto_2f
    return-void

    #@30
    .line 346
    :cond_30
    const-string v0, "http.proxyHost"

    #@32
    invoke-static {v0}, Ljava/lang/System;->clearProperty(Ljava/lang/String;)Ljava/lang/String;

    #@35
    .line 347
    const-string v0, "https.proxyHost"

    #@37
    invoke-static {v0}, Ljava/lang/System;->clearProperty(Ljava/lang/String;)Ljava/lang/String;

    #@3a
    goto :goto_17

    #@3b
    .line 353
    :cond_3b
    const-string v0, "http.proxyPort"

    #@3d
    invoke-static {v0}, Ljava/lang/System;->clearProperty(Ljava/lang/String;)Ljava/lang/String;

    #@40
    .line 354
    const-string v0, "https.proxyPort"

    #@42
    invoke-static {v0}, Ljava/lang/System;->clearProperty(Ljava/lang/String;)Ljava/lang/String;

    #@45
    goto :goto_23

    #@46
    .line 360
    :cond_46
    const-string v0, "http.nonProxyHosts"

    #@48
    invoke-static {v0}, Ljava/lang/System;->clearProperty(Ljava/lang/String;)Ljava/lang/String;

    #@4b
    .line 361
    const-string v0, "https.nonProxyHosts"

    #@4d
    invoke-static {v0}, Ljava/lang/System;->clearProperty(Ljava/lang/String;)Ljava/lang/String;

    #@50
    goto :goto_2f
.end method

.method public static validate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "hostname"
    .parameter "port"
    .parameter "exclList"

    #@0
    .prologue
    .line 252
    sget-object v4, Landroid/net/Proxy;->HOSTNAME_PATTERN:Ljava/util/regex/Pattern;

    #@2
    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@5
    move-result-object v2

    #@6
    .line 253
    .local v2, match:Ljava/util/regex/Matcher;
    sget-object v4, Landroid/net/Proxy;->EXCLLIST_PATTERN:Ljava/util/regex/Pattern;

    #@8
    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@b
    move-result-object v1

    #@c
    .line 255
    .local v1, listMatch:Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_18

    #@12
    .line 256
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@14
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@17
    throw v4

    #@18
    .line 259
    :cond_18
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    #@1b
    move-result v4

    #@1c
    if-nez v4, :cond_24

    #@1e
    .line 260
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@20
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@23
    throw v4

    #@24
    .line 263
    :cond_24
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@27
    move-result v4

    #@28
    if-lez v4, :cond_36

    #@2a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_36

    #@30
    .line 264
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@32
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@35
    throw v4

    #@36
    .line 267
    :cond_36
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@39
    move-result v4

    #@3a
    if-lez v4, :cond_61

    #@3c
    .line 268
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3f
    move-result v4

    #@40
    if-nez v4, :cond_48

    #@42
    .line 269
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@44
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@47
    throw v4

    #@48
    .line 271
    :cond_48
    const/4 v3, -0x1

    #@49
    .line 273
    .local v3, portVal:I
    :try_start_49
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4c
    .catch Ljava/lang/NumberFormatException; {:try_start_49 .. :try_end_4c} :catch_5a

    #@4c
    move-result v3

    #@4d
    .line 277
    if-lez v3, :cond_54

    #@4f
    const v4, 0xffff

    #@52
    if-le v3, v4, :cond_61

    #@54
    .line 278
    :cond_54
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@56
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@59
    throw v4

    #@5a
    .line 274
    :catch_5a
    move-exception v0

    #@5b
    .line 275
    .local v0, ex:Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@5d
    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@60
    throw v4

    #@61
    .line 281
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    .end local v3           #portVal:I
    :cond_61
    return-void
.end method
