.class public Landroid/net/RouteInfo;
.super Ljava/lang/Object;
.source "RouteInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDestination:Landroid/net/LinkAddress;

.field private final mGateway:Ljava/net/InetAddress;

.field private final mIsDefault:Z

.field private final mIsHost:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 181
    new-instance v0, Landroid/net/RouteInfo$1;

    #@2
    invoke-direct {v0}, Landroid/net/RouteInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/net/RouteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V
    .registers 6
    .parameter "destination"
    .parameter "gateway"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 49
    if-nez p1, :cond_13

    #@6
    .line 50
    if-eqz p2, :cond_4d

    #@8
    .line 51
    instance-of v0, p2, Ljava/net/Inet4Address;

    #@a
    if-eqz v0, :cond_45

    #@c
    .line 52
    new-instance p1, Landroid/net/LinkAddress;

    #@e
    .end local p1
    sget-object v0, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@10
    invoke-direct {p1, v0, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@13
    .line 61
    .restart local p1
    :cond_13
    :goto_13
    if-nez p2, :cond_1f

    #@15
    .line 62
    invoke-virtual {p1}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@18
    move-result-object v0

    #@19
    instance-of v0, v0, Ljava/net/Inet4Address;

    #@1b
    if-eqz v0, :cond_55

    #@1d
    .line 63
    sget-object p2, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@1f
    .line 68
    :cond_1f
    :goto_1f
    new-instance v0, Landroid/net/LinkAddress;

    #@21
    invoke-virtual {p1}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {p1}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@28
    move-result v2

    #@29
    invoke-static {v1, v2}, Landroid/net/NetworkUtils;->getNetworkPart(Ljava/net/InetAddress;I)Ljava/net/InetAddress;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {p1}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@30
    move-result v2

    #@31
    invoke-direct {v0, v1, v2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@34
    iput-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@36
    .line 70
    iput-object p2, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@38
    .line 71
    invoke-direct {p0}, Landroid/net/RouteInfo;->isDefault()Z

    #@3b
    move-result v0

    #@3c
    iput-boolean v0, p0, Landroid/net/RouteInfo;->mIsDefault:Z

    #@3e
    .line 72
    invoke-direct {p0}, Landroid/net/RouteInfo;->isHost()Z

    #@41
    move-result v0

    #@42
    iput-boolean v0, p0, Landroid/net/RouteInfo;->mIsHost:Z

    #@44
    .line 73
    return-void

    #@45
    .line 54
    :cond_45
    new-instance p1, Landroid/net/LinkAddress;

    #@47
    .end local p1
    sget-object v0, Ljava/net/Inet6Address;->ANY:Ljava/net/InetAddress;

    #@49
    invoke-direct {p1, v0, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@4c
    .restart local p1
    goto :goto_13

    #@4d
    .line 58
    :cond_4d
    new-instance v0, Ljava/lang/RuntimeException;

    #@4f
    const-string v1, "Invalid arguments passed in."

    #@51
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@54
    throw v0

    #@55
    .line 65
    :cond_55
    sget-object p2, Ljava/net/Inet6Address;->ANY:Ljava/net/InetAddress;

    #@57
    goto :goto_1f
.end method

.method public constructor <init>(Ljava/net/InetAddress;)V
    .registers 3
    .parameter "gateway"

    #@0
    .prologue
    .line 76
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@4
    .line 77
    return-void
.end method

.method private isDefault()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 98
    const/4 v0, 0x0

    #@3
    .line 99
    .local v0, val:Z
    iget-object v3, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@5
    if-eqz v3, :cond_1a

    #@7
    .line 100
    iget-object v3, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@9
    instance-of v3, v3, Ljava/net/Inet4Address;

    #@b
    if-eqz v3, :cond_1d

    #@d
    .line 101
    iget-object v3, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@f
    if-eqz v3, :cond_19

    #@11
    iget-object v3, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@13
    invoke-virtual {v3}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_1b

    #@19
    :cond_19
    move v0, v2

    #@1a
    .line 106
    :cond_1a
    :goto_1a
    return v0

    #@1b
    :cond_1b
    move v0, v1

    #@1c
    .line 101
    goto :goto_1a

    #@1d
    .line 103
    :cond_1d
    iget-object v3, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@1f
    if-eqz v3, :cond_29

    #@21
    iget-object v3, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@23
    invoke-virtual {v3}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@26
    move-result v3

    #@27
    if-nez v3, :cond_2b

    #@29
    :cond_29
    move v0, v2

    #@2a
    :goto_2a
    goto :goto_1a

    #@2b
    :cond_2b
    move v0, v1

    #@2c
    goto :goto_2a
.end method

.method private isHost()Z
    .registers 3

    #@0
    .prologue
    .line 94
    iget-object v0, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@2
    sget-object v1, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    #@4
    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_14

    #@a
    iget-object v0, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@c
    sget-object v1, Ljava/net/Inet6Address;->ANY:Ljava/net/InetAddress;

    #@e
    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    :cond_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public static makeHostRoute(Ljava/net/InetAddress;)Landroid/net/RouteInfo;
    .registers 2
    .parameter "host"

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/net/RouteInfo;->makeHostRoute(Ljava/net/InetAddress;Ljava/net/InetAddress;)Landroid/net/RouteInfo;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static makeHostRoute(Ljava/net/InetAddress;Ljava/net/InetAddress;)Landroid/net/RouteInfo;
    .registers 5
    .parameter "host"
    .parameter "gateway"

    #@0
    .prologue
    .line 84
    if-nez p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 89
    :goto_3
    return-object v0

    #@4
    .line 86
    :cond_4
    instance-of v0, p0, Ljava/net/Inet4Address;

    #@6
    if-eqz v0, :cond_15

    #@8
    .line 87
    new-instance v0, Landroid/net/RouteInfo;

    #@a
    new-instance v1, Landroid/net/LinkAddress;

    #@c
    const/16 v2, 0x20

    #@e
    invoke-direct {v1, p0, v2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@11
    invoke-direct {v0, v1, p1}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@14
    goto :goto_3

    #@15
    .line 89
    :cond_15
    new-instance v0, Landroid/net/RouteInfo;

    #@17
    new-instance v1, Landroid/net/LinkAddress;

    #@19
    const/16 v2, 0x80

    #@1b
    invoke-direct {v1, p0, v2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@1e
    invoke-direct {v0, v1, p1}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    #@21
    goto :goto_3
.end method

.method private matches(Ljava/net/InetAddress;)Z
    .registers 4
    .parameter "destination"

    #@0
    .prologue
    .line 220
    if-nez p1, :cond_4

    #@2
    const/4 v1, 0x0

    #@3
    .line 230
    :goto_3
    return v1

    #@4
    .line 224
    :cond_4
    invoke-direct {p0}, Landroid/net/RouteInfo;->isDefault()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_c

    #@a
    const/4 v1, 0x1

    #@b
    goto :goto_3

    #@c
    .line 227
    :cond_c
    iget-object v1, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@e
    invoke-virtual {v1}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@11
    move-result v1

    #@12
    invoke-static {p1, v1}, Landroid/net/NetworkUtils;->getNetworkPart(Ljava/net/InetAddress;I)Ljava/net/InetAddress;

    #@15
    move-result-object v0

    #@16
    .line 230
    .local v0, dstNet:Ljava/net/InetAddress;
    iget-object v1, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@18
    invoke-virtual {v1}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    goto :goto_3
.end method

.method public static selectBestRoute(Ljava/util/Collection;Ljava/net/InetAddress;)Landroid/net/RouteInfo;
    .registers 7
    .parameter
    .parameter "dest"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/RouteInfo;",
            ">;",
            "Ljava/net/InetAddress;",
            ")",
            "Landroid/net/RouteInfo;"
        }
    .end annotation

    #@0
    .prologue
    .line 241
    .local p0, routes:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/net/RouteInfo;>;"
    if-eqz p0, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    .line 255
    :cond_5
    return-object v0

    #@6
    .line 243
    :cond_6
    const/4 v0, 0x0

    #@7
    .line 245
    .local v0, bestRoute:Landroid/net/RouteInfo;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_5

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/net/RouteInfo;

    #@17
    .line 246
    .local v2, route:Landroid/net/RouteInfo;
    iget-object v3, v2, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@19
    invoke-virtual {v3}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v3, p1}, Landroid/net/NetworkUtils;->addressTypeMatches(Ljava/net/InetAddress;Ljava/net/InetAddress;)Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_b

    #@23
    .line 247
    if-eqz v0, :cond_33

    #@25
    iget-object v3, v0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@27
    invoke-virtual {v3}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@2a
    move-result v3

    #@2b
    iget-object v4, v2, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@2d
    invoke-virtual {v4}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@30
    move-result v4

    #@31
    if-ge v3, v4, :cond_b

    #@33
    .line 252
    :cond_33
    invoke-direct {v2, p1}, Landroid/net/RouteInfo;->matches(Ljava/net/InetAddress;)Z

    #@36
    move-result v3

    #@37
    if-eqz v3, :cond_b

    #@39
    move-object v0, v2

    #@3a
    goto :goto_b
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 134
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "obj"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 156
    if-ne p0, p1, :cond_5

    #@4
    .line 170
    :cond_4
    :goto_4
    return v3

    #@5
    .line 158
    :cond_5
    instance-of v5, p1, Landroid/net/RouteInfo;

    #@7
    if-nez v5, :cond_b

    #@9
    move v3, v4

    #@a
    goto :goto_4

    #@b
    :cond_b
    move-object v2, p1

    #@c
    .line 160
    check-cast v2, Landroid/net/RouteInfo;

    #@e
    .line 162
    .local v2, target:Landroid/net/RouteInfo;
    iget-object v5, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@10
    if-nez v5, :cond_32

    #@12
    invoke-virtual {v2}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@15
    move-result-object v5

    #@16
    if-nez v5, :cond_30

    #@18
    move v1, v3

    #@19
    .line 166
    .local v1, sameDestination:Z
    :goto_19
    iget-object v5, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@1b
    if-nez v5, :cond_3f

    #@1d
    invoke-virtual {v2}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@20
    move-result-object v5

    #@21
    if-nez v5, :cond_3d

    #@23
    move v0, v3

    #@24
    .line 170
    .local v0, sameAddress:Z
    :goto_24
    if-eqz v1, :cond_2e

    #@26
    if-eqz v0, :cond_2e

    #@28
    iget-boolean v5, p0, Landroid/net/RouteInfo;->mIsDefault:Z

    #@2a
    iget-boolean v6, v2, Landroid/net/RouteInfo;->mIsDefault:Z

    #@2c
    if-eq v5, v6, :cond_4

    #@2e
    :cond_2e
    move v3, v4

    #@2f
    goto :goto_4

    #@30
    .end local v0           #sameAddress:Z
    .end local v1           #sameDestination:Z
    :cond_30
    move v1, v4

    #@31
    .line 162
    goto :goto_19

    #@32
    :cond_32
    iget-object v5, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@34
    invoke-virtual {v2}, Landroid/net/RouteInfo;->getDestination()Landroid/net/LinkAddress;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v5, v6}, Landroid/net/LinkAddress;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    goto :goto_19

    #@3d
    .restart local v1       #sameDestination:Z
    :cond_3d
    move v0, v4

    #@3e
    .line 166
    goto :goto_24

    #@3f
    :cond_3f
    iget-object v5, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@41
    invoke-virtual {v2}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v5, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v0

    #@49
    goto :goto_24
.end method

.method public getDestination()Landroid/net/LinkAddress;
    .registers 2

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@2
    return-object v0
.end method

.method public getGateway()Ljava/net/InetAddress;
    .registers 2

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 176
    iget-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@3
    if-nez v0, :cond_12

    #@5
    move v0, v1

    #@6
    :goto_6
    iget-object v2, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@8
    if-nez v2, :cond_19

    #@a
    :goto_a
    add-int/2addr v1, v0

    #@b
    iget-boolean v0, p0, Landroid/net/RouteInfo;->mIsDefault:Z

    #@d
    if-eqz v0, :cond_20

    #@f
    const/4 v0, 0x3

    #@10
    :goto_10
    add-int/2addr v0, v1

    #@11
    return v0

    #@12
    :cond_12
    iget-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@14
    invoke-virtual {v0}, Landroid/net/LinkAddress;->hashCode()I

    #@17
    move-result v0

    #@18
    goto :goto_6

    #@19
    :cond_19
    iget-object v1, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@1b
    invoke-virtual {v1}, Ljava/net/InetAddress;->hashCode()I

    #@1e
    move-result v1

    #@1f
    goto :goto_a

    #@20
    :cond_20
    const/4 v0, 0x7

    #@21
    goto :goto_10
.end method

.method public isDefaultRoute()Z
    .registers 2

    #@0
    .prologue
    .line 119
    iget-boolean v0, p0, Landroid/net/RouteInfo;->mIsDefault:Z

    #@2
    return v0
.end method

.method public isHostRoute()Z
    .registers 2

    #@0
    .prologue
    .line 123
    iget-boolean v0, p0, Landroid/net/RouteInfo;->mIsHost:Z

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 127
    const-string v0, ""

    #@2
    .line 128
    .local v0, val:Ljava/lang/String;
    iget-object v1, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@4
    if-eqz v1, :cond_c

    #@6
    iget-object v1, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@8
    invoke-virtual {v1}, Landroid/net/LinkAddress;->toString()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 129
    :cond_c
    iget-object v1, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@e
    if-eqz v1, :cond_2d

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " -> "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@21
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 130
    :cond_2d
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 138
    iget-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@4
    if-nez v0, :cond_11

    #@6
    .line 139
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    #@9
    .line 146
    :goto_9
    iget-object v0, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@b
    if-nez v0, :cond_2b

    #@d
    .line 147
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    #@10
    .line 152
    :goto_10
    return-void

    #@11
    .line 141
    :cond_11
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@14
    .line 142
    iget-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@16
    invoke-virtual {v0}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@21
    .line 143
    iget-object v0, p0, Landroid/net/RouteInfo;->mDestination:Landroid/net/LinkAddress;

    #@23
    invoke-virtual {v0}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    #@26
    move-result v0

    #@27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    goto :goto_9

    #@2b
    .line 149
    :cond_2b
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@2e
    .line 150
    iget-object v0, p0, Landroid/net/RouteInfo;->mGateway:Ljava/net/InetAddress;

    #@30
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    #@33
    move-result-object v0

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@37
    goto :goto_10
.end method
