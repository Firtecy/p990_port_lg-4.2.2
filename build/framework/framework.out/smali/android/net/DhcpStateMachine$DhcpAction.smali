.class final enum Landroid/net/DhcpStateMachine$DhcpAction;
.super Ljava/lang/Enum;
.source "DhcpStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/DhcpStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DhcpAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/DhcpStateMachine$DhcpAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/DhcpStateMachine$DhcpAction;

.field public static final enum RENEW:Landroid/net/DhcpStateMachine$DhcpAction;

.field public static final enum START:Landroid/net/DhcpStateMachine$DhcpAction;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 121
    new-instance v0, Landroid/net/DhcpStateMachine$DhcpAction;

    #@4
    const-string v1, "START"

    #@6
    invoke-direct {v0, v1, v2}, Landroid/net/DhcpStateMachine$DhcpAction;-><init>(Ljava/lang/String;I)V

    #@9
    sput-object v0, Landroid/net/DhcpStateMachine$DhcpAction;->START:Landroid/net/DhcpStateMachine$DhcpAction;

    #@b
    .line 122
    new-instance v0, Landroid/net/DhcpStateMachine$DhcpAction;

    #@d
    const-string v1, "RENEW"

    #@f
    invoke-direct {v0, v1, v3}, Landroid/net/DhcpStateMachine$DhcpAction;-><init>(Ljava/lang/String;I)V

    #@12
    sput-object v0, Landroid/net/DhcpStateMachine$DhcpAction;->RENEW:Landroid/net/DhcpStateMachine$DhcpAction;

    #@14
    .line 120
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Landroid/net/DhcpStateMachine$DhcpAction;

    #@17
    sget-object v1, Landroid/net/DhcpStateMachine$DhcpAction;->START:Landroid/net/DhcpStateMachine$DhcpAction;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Landroid/net/DhcpStateMachine$DhcpAction;->RENEW:Landroid/net/DhcpStateMachine$DhcpAction;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Landroid/net/DhcpStateMachine$DhcpAction;->$VALUES:[Landroid/net/DhcpStateMachine$DhcpAction;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/DhcpStateMachine$DhcpAction;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 120
    const-class v0, Landroid/net/DhcpStateMachine$DhcpAction;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/DhcpStateMachine$DhcpAction;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/net/DhcpStateMachine$DhcpAction;
    .registers 1

    #@0
    .prologue
    .line 120
    sget-object v0, Landroid/net/DhcpStateMachine$DhcpAction;->$VALUES:[Landroid/net/DhcpStateMachine$DhcpAction;

    #@2
    invoke-virtual {v0}, [Landroid/net/DhcpStateMachine$DhcpAction;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/net/DhcpStateMachine$DhcpAction;

    #@8
    return-object v0
.end method
