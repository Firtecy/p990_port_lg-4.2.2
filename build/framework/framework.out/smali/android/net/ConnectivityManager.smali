.class public Landroid/net/ConnectivityManager;
.super Ljava/lang/Object;
.source "ConnectivityManager.java"


# static fields
.field public static final ACTION_BACKGROUND_DATA_SETTING_CHANGED:Ljava/lang/String; = "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_DATA_ACTIVITY_CHANGE:Ljava/lang/String; = "android.net.conn.DATA_ACTIVITY_CHANGE"

.field public static final ACTION_DATA_BLOCK_IN_MMS:Ljava/lang/String; = "android.net.conn.DATA_DATA_BLOCK_IN_MMS"

.field public static final ACTION_DATA_CALLING_SETMOBILE:Ljava/lang/String; = "com.lge.callingsetmobile"

.field public static final ACTION_DATA_CONNECTED_DONE:Ljava/lang/String; = "android.net.conn.DATA_CONNECTED_DONE"

.field public static final ACTION_DATA_CONNECTED_STATUS:Ljava/lang/String; = "android.net.conn.DATA_CONNECTED_STATUS"

.field public static final ACTION_DATA_DISCONNECTED_DONE:Ljava/lang/String; = "android.net.conn.DATA_DISCONNECTED_DONE"

.field public static final ACTION_DATA_PDP_REJECT_CAUSE:Ljava/lang/String; = "android.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

.field public static final ACTION_DATA_PDP_REJECT_CAUSE_LGE:Ljava/lang/String; = "com.lge.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

.field public static final ACTION_DATA_PDP_REJECT_CAUSE_LGE_MMS:Ljava/lang/String; = "com.lge.mms.net.conn.ACTION_DATA_PDP_REJECT_CAUSE"

.field public static final ACTION_FAST_DORMANCY_DISABLED:Ljava/lang/String; = "android.net.conn.ACTION_FAST_DORMANCY_DISABLED"

.field public static final ACTION_STARTING_IN_DATA_SETTING_DISABLE:Ljava/lang/String; = "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE"

.field public static final ACTION_STARTING_IN_DATA_SETTING_DISABLE_3GONLY:Ljava/lang/String; = "android.net.conn.STARTING_IN_DATA_SETTING_DISABLE_3GONLY"

.field public static final ACTION_STARTING_IN_ROAM_SETTING_DISABLE:Ljava/lang/String; = "android.net.conn.STARTING_IN_ROAM_SETTING_DISABLE"

.field public static final ACTION_TETHER_STATE_CHANGED:Ljava/lang/String; = "android.net.conn.TETHER_STATE_CHANGED"

.field public static final ACTION__FAST_DORMANCY_ENABLED:Ljava/lang/String; = "android.net.conn.ACTION_FAST_DORMANCY_ENABLED"

.field public static final CONNECTIVITY_ACTION:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE"

.field public static final CONNECTIVITY_ACTION_IMMEDIATE:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

.field public static final CONNECTIVITY_CHANGE_DELAY_DEFAULT:I = 0xbb8

.field public static final DEFAULT_NETWORK_PREFERENCE:I = 0x1

.field public static final EXTRA_ACTIVE_TETHER:Ljava/lang/String; = "activeArray"

.field public static final EXTRA_AVAILABLE_TETHER:Ljava/lang/String; = "availableArray"

.field public static final EXTRA_DEVICE_TYPE:Ljava/lang/String; = "deviceType"

.field public static final EXTRA_ERRORED_TETHER:Ljava/lang/String; = "erroredArray"

.field public static final EXTRA_EXTRA_INFO:Ljava/lang/String; = "extraInfo"

.field public static final EXTRA_INET_CONDITION:Ljava/lang/String; = "inetCondition"

.field public static final EXTRA_IS_ACTIVE:Ljava/lang/String; = "isActive"

.field public static final EXTRA_IS_FAILOVER:Ljava/lang/String; = "isFailover"

.field public static final EXTRA_NETWORK_INFO:Ljava/lang/String; = "networkInfo"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXTRA_NETWORK_TYPE:Ljava/lang/String; = "networkType"

.field public static final EXTRA_NO_CONNECTIVITY:Ljava/lang/String; = "noConnectivity"

.field public static final EXTRA_OTHER_NETWORK_INFO:Ljava/lang/String; = "otherNetwork"

.field public static final EXTRA_REASON:Ljava/lang/String; = "reason"

.field public static final EXTRA_SM_CAUSE:Ljava/lang/String; = "smCause"

.field public static final INET_CONDITION_ACTION:Ljava/lang/String; = "android.net.conn.INET_CONDITION_ACTION"

.field public static final MAX_NETWORK_TYPE:I = 0x17

.field public static final MAX_RADIO_TYPE:I = 0x17

.field private static final TAG:Ljava/lang/String; = "ConnectivityManager"

.field public static final TETHER_ERROR_DISABLE_NAT_ERROR:I = 0x9

.field public static final TETHER_ERROR_ENABLE_NAT_ERROR:I = 0x8

.field public static final TETHER_ERROR_IFACE_CFG_ERROR:I = 0xa

.field public static final TETHER_ERROR_MASTER_ERROR:I = 0x5

.field public static final TETHER_ERROR_NO_ERROR:I = 0x0

.field public static final TETHER_ERROR_SERVICE_UNAVAIL:I = 0x2

.field public static final TETHER_ERROR_TETHER_IFACE_ERROR:I = 0x6

.field public static final TETHER_ERROR_UNAVAIL_IFACE:I = 0x4

.field public static final TETHER_ERROR_UNKNOWN_IFACE:I = 0x1

.field public static final TETHER_ERROR_UNSUPPORTED:I = 0x3

.field public static final TETHER_ERROR_UNTETHER_IFACE_ERROR:I = 0x7

.field public static final TYPE_BLUETOOTH:I = 0x7

.field public static final TYPE_DUMMY:I = 0x8

.field public static final TYPE_ETHERNET:I = 0x9

.field public static final TYPE_MOBILE:I = 0x0

.field public static final TYPE_MOBILE_ADMIN:I = 0xe

.field public static final TYPE_MOBILE_BIP:I = 0x16

.field public static final TYPE_MOBILE_CBS:I = 0xc

.field public static final TYPE_MOBILE_DUN:I = 0x4

.field public static final TYPE_MOBILE_EMERGENCY:I = 0x17

.field public static final TYPE_MOBILE_ENTITLEMENT:I = 0x10

.field public static final TYPE_MOBILE_FOTA:I = 0xa

.field public static final TYPE_MOBILE_HIPRI:I = 0x5

.field public static final TYPE_MOBILE_IMS:I = 0xb

.field public static final TYPE_MOBILE_KTMULTIRAB1:I = 0x13

.field public static final TYPE_MOBILE_KTMULTIRAB2:I = 0x14

.field public static final TYPE_MOBILE_MMS:I = 0x2

.field public static final TYPE_MOBILE_RCS:I = 0x15

.field public static final TYPE_MOBILE_SUPL:I = 0x3

.field public static final TYPE_MOBILE_TETHERING:I = 0x12

.field public static final TYPE_MOBILE_VZW800:I = 0x11

.field public static final TYPE_MOBILE_VZWAPP:I = 0xf

.field public static final TYPE_NONE:I = -0x1

.field public static final TYPE_WIFI:I = 0x1

.field public static final TYPE_WIFI_P2P:I = 0xd

.field public static final TYPE_WIMAX:I = 0x6


# instance fields
.field private final mService:Landroid/net/IConnectivityManager;


# direct methods
.method public constructor <init>(Landroid/net/IConnectivityManager;)V
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 893
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 894
    const-string/jumbo v0, "missing IConnectivityManager"

    #@6
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/net/IConnectivityManager;

    #@c
    iput-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@e
    .line 895
    return-void
.end method

.method public static from(Landroid/content/Context;)Landroid/net/ConnectivityManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 899
    const-string v0, "connectivity"

    #@2
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/ConnectivityManager;

    #@8
    return-object v0
.end method

.method public static getNetworkTypeName(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 521
    packed-switch p0, :pswitch_data_4e

    #@3
    .line 581
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 523
    :pswitch_8
    const-string v0, "MOBILE"

    #@a
    goto :goto_7

    #@b
    .line 525
    :pswitch_b
    const-string v0, "WIFI"

    #@d
    goto :goto_7

    #@e
    .line 527
    :pswitch_e
    const-string v0, "MOBILE_MMS"

    #@10
    goto :goto_7

    #@11
    .line 529
    :pswitch_11
    const-string v0, "MOBILE_SUPL"

    #@13
    goto :goto_7

    #@14
    .line 531
    :pswitch_14
    const-string v0, "MOBILE_DUN"

    #@16
    goto :goto_7

    #@17
    .line 533
    :pswitch_17
    const-string v0, "MOBILE_HIPRI"

    #@19
    goto :goto_7

    #@1a
    .line 535
    :pswitch_1a
    const-string v0, "WIMAX"

    #@1c
    goto :goto_7

    #@1d
    .line 537
    :pswitch_1d
    const-string v0, "BLUETOOTH"

    #@1f
    goto :goto_7

    #@20
    .line 539
    :pswitch_20
    const-string v0, "DUMMY"

    #@22
    goto :goto_7

    #@23
    .line 541
    :pswitch_23
    const-string v0, "ETHERNET"

    #@25
    goto :goto_7

    #@26
    .line 543
    :pswitch_26
    const-string v0, "MOBILE_FOTA"

    #@28
    goto :goto_7

    #@29
    .line 545
    :pswitch_29
    const-string v0, "MOBILE_IMS"

    #@2b
    goto :goto_7

    #@2c
    .line 547
    :pswitch_2c
    const-string v0, "MOBILE_CBS"

    #@2e
    goto :goto_7

    #@2f
    .line 549
    :pswitch_2f
    const-string v0, "WIFI_P2P"

    #@31
    goto :goto_7

    #@32
    .line 552
    :pswitch_32
    const-string v0, "MOBILE_ADMIN"

    #@34
    goto :goto_7

    #@35
    .line 554
    :pswitch_35
    const-string v0, "MOBILE_VZWAPP"

    #@37
    goto :goto_7

    #@38
    .line 556
    :pswitch_38
    const-string v0, "MOBILE_VZW800"

    #@3a
    goto :goto_7

    #@3b
    .line 560
    :pswitch_3b
    const-string v0, "MOBILE_TETHERING"

    #@3d
    goto :goto_7

    #@3e
    .line 564
    :pswitch_3e
    const-string v0, "MOBILE_KTMULTIRAB1"

    #@40
    goto :goto_7

    #@41
    .line 566
    :pswitch_41
    const-string v0, "MOBILE_KTMULTIRAB2"

    #@43
    goto :goto_7

    #@44
    .line 570
    :pswitch_44
    const-string v0, "MOBILE_ENTITLEMENT"

    #@46
    goto :goto_7

    #@47
    .line 574
    :pswitch_47
    const-string v0, "MOBILE_BIP"

    #@49
    goto :goto_7

    #@4a
    .line 578
    :pswitch_4a
    const-string v0, "MOBILE_RCS"

    #@4c
    goto :goto_7

    #@4d
    .line 521
    nop

    #@4e
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
        :pswitch_26
        :pswitch_29
        :pswitch_2c
        :pswitch_2f
        :pswitch_32
        :pswitch_35
        :pswitch_44
        :pswitch_38
        :pswitch_3b
        :pswitch_3e
        :pswitch_41
        :pswitch_4a
        :pswitch_47
    .end packed-switch
.end method

.method public static isNetworkTypeMobile(I)Z
    .registers 2
    .parameter "networkType"

    #@0
    .prologue
    .line 587
    packed-switch p0, :pswitch_data_8

    #@3
    .line 617
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 615
    :pswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 587
    nop

    #@8
    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static isNetworkTypeValid(I)Z
    .registers 2
    .parameter "networkType"

    #@0
    .prologue
    .line 516
    if-ltz p0, :cond_8

    #@2
    const/16 v0, 0x17

    #@4
    if-gt p0, v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method


# virtual methods
.method public captivePortalCheckComplete(Landroid/net/NetworkInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 1247
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->captivePortalCheckComplete(Landroid/net/NetworkInfo;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1250
    :goto_5
    return-void

    #@6
    .line 1248
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public clearTetheringBlockNotification()V
    .registers 2

    #@0
    .prologue
    .line 1183
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0}, Landroid/net/IConnectivityManager;->clearTetheringBlockNotification()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1187
    :goto_5
    return-void

    #@6
    .line 1184
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public getActiveLinkProperties()Landroid/net/LinkProperties;
    .registers 3

    #@0
    .prologue
    .line 680
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getActiveLinkProperties()Landroid/net/LinkProperties;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 682
    :goto_6
    return-object v1

    #@7
    .line 681
    :catch_7
    move-exception v0

    #@8
    .line 682
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getActiveNetworkInfo()Landroid/net/NetworkInfo;
    .registers 3

    #@0
    .prologue
    .line 646
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 648
    :goto_6
    return-object v1

    #@7
    .line 647
    :catch_7
    move-exception v0

    #@8
    .line 648
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getActiveNetworkInfoForUid(I)Landroid/net/NetworkInfo;
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 655
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getActiveNetworkInfoForUid(I)Landroid/net/NetworkInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 657
    :goto_6
    return-object v1

    #@7
    .line 656
    :catch_7
    move-exception v0

    #@8
    .line 657
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getActiveNetworkQuotaInfo()Landroid/net/NetworkQuotaInfo;
    .registers 3

    #@0
    .prologue
    .line 843
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getActiveNetworkQuotaInfo()Landroid/net/NetworkQuotaInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 845
    :goto_6
    return-object v1

    #@7
    .line 844
    :catch_7
    move-exception v0

    #@8
    .line 845
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getAllNetworkInfo()[Landroid/net/NetworkInfo;
    .registers 3

    #@0
    .prologue
    .line 671
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 673
    :goto_6
    return-object v1

    #@7
    .line 672
    :catch_7
    move-exception v0

    #@8
    .line 673
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getBackgroundDataSetting()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 816
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getGlobalProxy()Landroid/net/ProxyProperties;
    .registers 3

    #@0
    .prologue
    .line 1095
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getGlobalProxy()Landroid/net/ProxyProperties;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 1097
    :goto_6
    return-object v1

    #@7
    .line 1096
    :catch_7
    move-exception v0

    #@8
    .line 1097
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getLastTetherError(Ljava/lang/String;)I
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 1045
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getLastTetherError(Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1047
    :goto_6
    return v1

    #@7
    .line 1046
    :catch_7
    move-exception v0

    #@8
    .line 1047
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x2

    #@9
    goto :goto_6
.end method

.method public getLinkProperties(I)Landroid/net/LinkProperties;
    .registers 4
    .parameter "networkType"

    #@0
    .prologue
    .line 689
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 691
    :goto_6
    return-object v1

    #@7
    .line 690
    :catch_7
    move-exception v0

    #@8
    .line 691
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getMobileDataEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 857
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getMobileDataEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 859
    :goto_6
    return v1

    #@7
    .line 858
    :catch_7
    move-exception v0

    #@8
    .line 859
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x1

    #@9
    goto :goto_6
.end method

.method public getMobileDataEnabledByUser()Z
    .registers 3

    #@0
    .prologue
    .line 883
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getMobileDataEnabledByUser()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 885
    :goto_6
    return v1

    #@7
    .line 884
    :catch_7
    move-exception v0

    #@8
    .line 885
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getNetworkInfo(I)Landroid/net/NetworkInfo;
    .registers 4
    .parameter "networkType"

    #@0
    .prologue
    .line 663
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 665
    :goto_6
    return-object v1

    #@7
    .line 664
    :catch_7
    move-exception v0

    #@8
    .line 665
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getNetworkPreference()I
    .registers 3

    #@0
    .prologue
    .line 630
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getNetworkPreference()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 632
    :goto_6
    return v1

    #@7
    .line 631
    :catch_7
    move-exception v0

    #@8
    .line 632
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@9
    goto :goto_6
.end method

.method public getProxy()Landroid/net/ProxyProperties;
    .registers 3

    #@0
    .prologue
    .line 1107
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getProxy()Landroid/net/ProxyProperties;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 1109
    :goto_6
    return-object v1

    #@7
    .line 1108
    :catch_7
    move-exception v0

    #@8
    .line 1109
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public getTetherableBluetoothRegexs()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 997
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 999
    :goto_6
    return-object v1

    #@7
    .line 998
    :catch_7
    move-exception v0

    #@8
    .line 999
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    goto :goto_6
.end method

.method public getTetherableIfaces()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 907
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getTetherableIfaces()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 909
    :goto_6
    return-object v1

    #@7
    .line 908
    :catch_7
    move-exception v0

    #@8
    .line 909
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    goto :goto_6
.end method

.method public getTetherableUsbRegexs()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 975
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getTetherableUsbRegexs()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 977
    :goto_6
    return-object v1

    #@7
    .line 976
    :catch_7
    move-exception v0

    #@8
    .line 977
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    goto :goto_6
.end method

.method public getTetherableWifiRegexs()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 986
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 988
    :goto_6
    return-object v1

    #@7
    .line 987
    :catch_7
    move-exception v0

    #@8
    .line 988
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    goto :goto_6
.end method

.method public getTetheredIfaces()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 918
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 920
    :goto_6
    return-object v1

    #@7
    .line 919
    :catch_7
    move-exception v0

    #@8
    .line 920
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    goto :goto_6
.end method

.method public getTetheringErroredIfaces()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 929
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->getTetheringErroredIfaces()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 931
    :goto_6
    return-object v1

    #@7
    .line 930
    :catch_7
    move-exception v0

    #@8
    .line 931
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    new-array v1, v1, [Ljava/lang/String;

    #@b
    goto :goto_6
.end method

.method public handleConnectMobile()V
    .registers 2

    #@0
    .prologue
    .line 1201
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0}, Landroid/net/IConnectivityManager;->handleConnectMobile()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1204
    :goto_5
    return-void

    #@6
    .line 1202
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public handleDisconnectMobile()V
    .registers 2

    #@0
    .prologue
    .line 1194
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0}, Landroid/net/IConnectivityManager;->handleDisconnectMobile()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1197
    :goto_5
    return-void

    #@6
    .line 1195
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public isActiveNetworkMetered()Z
    .registers 3

    #@0
    .prologue
    .line 1152
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->isActiveNetworkMetered()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1154
    :goto_6
    return v1

    #@7
    .line 1153
    :catch_7
    move-exception v0

    #@8
    .line 1154
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isNetworkSupported(I)Z
    .registers 3
    .parameter "networkType"

    #@0
    .prologue
    .line 1138
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->isNetworkSupported(I)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v0

    #@6
    .line 1140
    :goto_6
    return v0

    #@7
    .line 1139
    :catch_7
    move-exception v0

    #@8
    .line 1140
    const/4 v0, 0x0

    #@9
    goto :goto_6
.end method

.method public isTetheringSupported()Z
    .registers 3

    #@0
    .prologue
    .line 964
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->isTetheringSupported()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 966
    :goto_6
    return v1

    #@7
    .line 965
    :catch_7
    move-exception v0

    #@8
    .line 966
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public mobileDataPdpReset()V
    .registers 2

    #@0
    .prologue
    .line 1236
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0}, Landroid/net/IConnectivityManager;->mobileDataPdpReset()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1239
    :goto_5
    return-void

    #@6
    .line 1237
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public reportInetCondition(II)V
    .registers 4
    .parameter "networkType"
    .parameter "percentage"

    #@0
    .prologue
    .line 1073
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/net/IConnectivityManager;->reportInetCondition(II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1076
    :goto_5
    return-void

    #@6
    .line 1074
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public requestNetworkTransitionWakelock(Ljava/lang/String;)Z
    .registers 4
    .parameter "forWhom"

    #@0
    .prologue
    .line 1059
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->requestNetworkTransitionWakelock(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    .line 1060
    const/4 v1, 0x1

    #@6
    .line 1062
    :goto_6
    return v1

    #@7
    .line 1061
    :catch_7
    move-exception v0

    #@8
    .line 1062
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public requestRemRouteToHostAddress(ILjava/net/InetAddress;)Z
    .registers 6
    .parameter "networkType"
    .parameter "hostAddress"

    #@0
    .prologue
    .line 1221
    invoke-virtual {p2}, Ljava/net/InetAddress;->getAddress()[B

    #@3
    move-result-object v0

    #@4
    .line 1223
    .local v0, address:[B
    :try_start_4
    iget-object v2, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@6
    invoke-interface {v2, p1, v0}, Landroid/net/IConnectivityManager;->requestRemRouteToHostAddress(I[B)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v2

    #@a
    .line 1225
    :goto_a
    return v2

    #@b
    .line 1224
    :catch_b
    move-exception v1

    #@c
    .line 1225
    .local v1, e:Landroid/os/RemoteException;
    const/4 v2, 0x0

    #@d
    goto :goto_a
.end method

.method public requestRouteToHost(II)Z
    .registers 5
    .parameter "networkType"
    .parameter "hostAddress"

    #@0
    .prologue
    .line 768
    invoke-static {p2}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    .line 770
    .local v0, inetAddress:Ljava/net/InetAddress;
    if-nez v0, :cond_8

    #@6
    .line 771
    const/4 v1, 0x0

    #@7
    .line 774
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {p0, p1, v0}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public requestRouteToHostAddress(ILjava/net/InetAddress;)Z
    .registers 6
    .parameter "networkType"
    .parameter "hostAddress"

    #@0
    .prologue
    .line 788
    invoke-virtual {p2}, Ljava/net/InetAddress;->getAddress()[B

    #@3
    move-result-object v0

    #@4
    .line 790
    .local v0, address:[B
    :try_start_4
    iget-object v2, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@6
    invoke-interface {v2, p1, v0}, Landroid/net/IConnectivityManager;->requestRouteToHostAddress(I[B)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v2

    #@a
    .line 792
    :goto_a
    return v2

    #@b
    .line 791
    :catch_b
    move-exception v1

    #@c
    .line 792
    .local v1, e:Landroid/os/RemoteException;
    const/4 v2, 0x0

    #@d
    goto :goto_a
.end method

.method public setBackgroundDataSetting(Z)V
    .registers 2
    .parameter "allowBackgroundData"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 832
    return-void
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 3
    .parameter "msger"

    #@0
    .prologue
    .line 1211
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->setDataConnectionMessanger(Landroid/os/Messenger;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1214
    :goto_5
    return-void

    #@6
    .line 1212
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setDataDependency(IZ)V
    .registers 4
    .parameter "networkType"
    .parameter "met"

    #@0
    .prologue
    .line 1120
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/net/IConnectivityManager;->setDataDependency(IZ)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1123
    :goto_5
    return-void

    #@6
    .line 1121
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setGlobalProxy(Landroid/net/ProxyProperties;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 1084
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->setGlobalProxy(Landroid/net/ProxyProperties;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1087
    :goto_5
    return-void

    #@6
    .line 1085
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setMobileDataEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 872
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->setMobileDataEnabled(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 875
    :goto_5
    return-void

    #@6
    .line 873
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setNetworkPreference(I)V
    .registers 3
    .parameter "preference"

    #@0
    .prologue
    .line 623
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->setNetworkPreference(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 626
    :goto_5
    return-void

    #@6
    .line 624
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setRadio(IZ)Z
    .registers 5
    .parameter "networkType"
    .parameter "turnOn"

    #@0
    .prologue
    .line 707
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/net/IConnectivityManager;->setRadio(IZ)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 709
    :goto_6
    return v1

    #@7
    .line 708
    :catch_7
    move-exception v0

    #@8
    .line 709
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setRadios(Z)Z
    .registers 4
    .parameter "turnOn"

    #@0
    .prologue
    .line 698
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->setRadios(Z)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 700
    :goto_6
    return v1

    #@7
    .line 699
    :catch_7
    move-exception v0

    #@8
    .line 700
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public setUsbTethering(Z)I
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1008
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->setUsbTethering(Z)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1010
    :goto_6
    return v1

    #@7
    .line 1009
    :catch_7
    move-exception v0

    #@8
    .line 1010
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x2

    #@9
    goto :goto_6
.end method

.method public showTetheringBlockNotification(I)V
    .registers 3
    .parameter "icon"

    #@0
    .prologue
    .line 1173
    :try_start_0
    iget-object v0, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v0, p1}, Landroid/net/IConnectivityManager;->showTetheringBlockNotification(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1177
    :goto_5
    return-void

    #@6
    .line 1174
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public startUsingNetworkFeature(ILjava/lang/String;)I
    .registers 6
    .parameter "networkType"
    .parameter "feature"

    #@0
    .prologue
    .line 728
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    new-instance v2, Landroid/os/Binder;

    #@4
    invoke-direct {v2}, Landroid/os/Binder;-><init>()V

    #@7
    invoke-interface {v1, p1, p2, v2}, Landroid/net/IConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;Landroid/os/IBinder;)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 731
    :goto_b
    return v1

    #@c
    .line 730
    :catch_c
    move-exception v0

    #@d
    .line 731
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@e
    goto :goto_b
.end method

.method public stopUsingNetworkFeature(ILjava/lang/String;)I
    .registers 5
    .parameter "networkType"
    .parameter "feature"

    #@0
    .prologue
    .line 750
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/net/IConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 752
    :goto_6
    return v1

    #@7
    .line 751
    :catch_7
    move-exception v0

    #@8
    .line 752
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@9
    goto :goto_6
.end method

.method public tether(Ljava/lang/String;)I
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 941
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->tether(Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 943
    :goto_6
    return v1

    #@7
    .line 942
    :catch_7
    move-exception v0

    #@8
    .line 943
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x2

    #@9
    goto :goto_6
.end method

.method public untether(Ljava/lang/String;)I
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 953
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/net/IConnectivityManager;->untether(Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 955
    :goto_6
    return v1

    #@7
    .line 954
    :catch_7
    move-exception v0

    #@8
    .line 955
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x2

    #@9
    goto :goto_6
.end method

.method public updateLockdownVpn()Z
    .registers 3

    #@0
    .prologue
    .line 1161
    :try_start_0
    iget-object v1, p0, Landroid/net/ConnectivityManager;->mService:Landroid/net/IConnectivityManager;

    #@2
    invoke-interface {v1}, Landroid/net/IConnectivityManager;->updateLockdownVpn()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1163
    :goto_6
    return v1

    #@7
    .line 1162
    :catch_7
    move-exception v0

    #@8
    .line 1163
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method
