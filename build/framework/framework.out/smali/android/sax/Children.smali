.class Landroid/sax/Children;
.super Ljava/lang/Object;
.source "Children.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/sax/Children$Child;
    }
.end annotation


# instance fields
.field children:[Landroid/sax/Children$Child;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 25
    const/16 v0, 0x10

    #@5
    new-array v0, v0, [Landroid/sax/Children$Child;

    #@7
    iput-object v0, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    #@9
    .line 86
    return-void
.end method


# virtual methods
.method get(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;
    .registers 9
    .parameter "uri"
    .parameter "localName"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 66
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    #@4
    move-result v4

    #@5
    mul-int/lit8 v4, v4, 0x1f

    #@7
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    #@a
    move-result v5

    #@b
    add-int v1, v4, v5

    #@d
    .line 67
    .local v1, hash:I
    and-int/lit8 v2, v1, 0xf

    #@f
    .line 69
    .local v2, index:I
    iget-object v4, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    #@11
    aget-object v0, v4, v2

    #@13
    .line 70
    .local v0, current:Landroid/sax/Children$Child;
    if-nez v0, :cond_16

    #@15
    .line 82
    :goto_15
    return-object v3

    #@16
    .line 74
    :cond_16
    iget v4, v0, Landroid/sax/Children$Child;->hash:I

    #@18
    if-ne v4, v1, :cond_2c

    #@1a
    iget-object v4, v0, Landroid/sax/Element;->uri:Ljava/lang/String;

    #@1c
    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_2c

    #@22
    iget-object v4, v0, Landroid/sax/Element;->localName:Ljava/lang/String;

    #@24
    invoke-virtual {v4, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_2c

    #@2a
    move-object v3, v0

    #@2b
    .line 77
    goto :goto_15

    #@2c
    .line 79
    :cond_2c
    iget-object v0, v0, Landroid/sax/Children$Child;->next:Landroid/sax/Children$Child;

    #@2e
    .line 80
    if-nez v0, :cond_16

    #@30
    goto :goto_15
.end method

.method getOrCreate(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;
    .registers 13
    .parameter "parent"
    .parameter "uri"
    .parameter "localName"

    #@0
    .prologue
    .line 31
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    #@3
    move-result v1

    #@4
    mul-int/lit8 v1, v1, 0x1f

    #@6
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    #@9
    move-result v2

    #@a
    add-int v5, v1, v2

    #@c
    .line 32
    .local v5, hash:I
    and-int/lit8 v7, v5, 0xf

    #@e
    .line 34
    .local v7, index:I
    iget-object v1, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    #@10
    aget-object v0, v1, v7

    #@12
    .line 35
    .local v0, current:Landroid/sax/Children$Child;
    if-nez v0, :cond_26

    #@14
    .line 37
    new-instance v0, Landroid/sax/Children$Child;

    #@16
    .end local v0           #current:Landroid/sax/Children$Child;
    iget v1, p1, Landroid/sax/Element;->depth:I

    #@18
    add-int/lit8 v4, v1, 0x1

    #@1a
    move-object v1, p1

    #@1b
    move-object v2, p2

    #@1c
    move-object v3, p3

    #@1d
    invoke-direct/range {v0 .. v5}, Landroid/sax/Children$Child;-><init>(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;II)V

    #@20
    .line 38
    .restart local v0       #current:Landroid/sax/Children$Child;
    iget-object v1, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    #@22
    aput-object v0, v1, v7

    #@24
    move-object v6, v0

    #@25
    .line 58
    .end local v0           #current:Landroid/sax/Children$Child;
    .local v6, current:Ljava/lang/Object;
    :goto_25
    return-object v6

    #@26
    .line 44
    .end local v6           #current:Ljava/lang/Object;
    .restart local v0       #current:Landroid/sax/Children$Child;
    :cond_26
    iget v1, v0, Landroid/sax/Children$Child;->hash:I

    #@28
    if-ne v1, v5, :cond_3c

    #@2a
    iget-object v1, v0, Landroid/sax/Element;->uri:Ljava/lang/String;

    #@2c
    invoke-virtual {v1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2f
    move-result v1

    #@30
    if-nez v1, :cond_3c

    #@32
    iget-object v1, v0, Landroid/sax/Element;->localName:Ljava/lang/String;

    #@34
    invoke-virtual {v1, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@37
    move-result v1

    #@38
    if-nez v1, :cond_3c

    #@3a
    move-object v6, v0

    #@3b
    .line 48
    .restart local v6       #current:Ljava/lang/Object;
    goto :goto_25

    #@3c
    .line 51
    .end local v6           #current:Ljava/lang/Object;
    :cond_3c
    move-object v8, v0

    #@3d
    .line 52
    .local v8, previous:Landroid/sax/Children$Child;
    iget-object v0, v0, Landroid/sax/Children$Child;->next:Landroid/sax/Children$Child;

    #@3f
    .line 53
    if-nez v0, :cond_26

    #@41
    .line 56
    new-instance v0, Landroid/sax/Children$Child;

    #@43
    .end local v0           #current:Landroid/sax/Children$Child;
    iget v1, p1, Landroid/sax/Element;->depth:I

    #@45
    add-int/lit8 v4, v1, 0x1

    #@47
    move-object v1, p1

    #@48
    move-object v2, p2

    #@49
    move-object v3, p3

    #@4a
    invoke-direct/range {v0 .. v5}, Landroid/sax/Children$Child;-><init>(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;II)V

    #@4d
    .line 57
    .restart local v0       #current:Landroid/sax/Children$Child;
    iput-object v0, v8, Landroid/sax/Children$Child;->next:Landroid/sax/Children$Child;

    #@4f
    move-object v6, v0

    #@50
    .line 58
    .restart local v6       #current:Ljava/lang/Object;
    goto :goto_25
.end method
