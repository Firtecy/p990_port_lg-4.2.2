.class public Landroid/sax/Element;
.super Ljava/lang/Object;
.source "Element.java"


# instance fields
.field children:Landroid/sax/Children;

.field final depth:I

.field endElementListener:Landroid/sax/EndElementListener;

.field endTextElementListener:Landroid/sax/EndTextElementListener;

.field final localName:Ljava/lang/String;

.field final parent:Landroid/sax/Element;

.field requiredChilden:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/sax/Element;",
            ">;"
        }
    .end annotation
.end field

.field startElementListener:Landroid/sax/StartElementListener;

.field final uri:Ljava/lang/String;

.field visited:Z


# direct methods
.method constructor <init>(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "parent"
    .parameter "uri"
    .parameter "localName"
    .parameter "depth"

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    iput-object p1, p0, Landroid/sax/Element;->parent:Landroid/sax/Element;

    #@5
    .line 50
    iput-object p2, p0, Landroid/sax/Element;->uri:Ljava/lang/String;

    #@7
    .line 51
    iput-object p3, p0, Landroid/sax/Element;->localName:Ljava/lang/String;

    #@9
    .line 52
    iput p4, p0, Landroid/sax/Element;->depth:I

    #@b
    .line 53
    return-void
.end method

.method static toString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "uri"
    .parameter "localName"

    #@0
    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "\'"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    const-string v1, ""

    #@d
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_22

    #@13
    .end local p1
    :goto_13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, "\'"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    return-object v0

    #@22
    .restart local p1
    :cond_22
    new-instance v1, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, ":"

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object p1

    #@39
    goto :goto_13
.end method


# virtual methods
.method checkRequiredChildren(Lorg/xml/sax/Locator;)V
    .registers 8
    .parameter "locator"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    #@0
    .prologue
    .line 191
    iget-object v2, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@2
    .line 192
    .local v2, requiredChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/sax/Element;>;"
    if-eqz v2, :cond_48

    #@4
    .line 193
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    add-int/lit8 v1, v3, -0x1

    #@a
    .local v1, i:I
    :goto_a
    if-ltz v1, :cond_48

    #@c
    .line 194
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/sax/Element;

    #@12
    .line 195
    .local v0, child:Landroid/sax/Element;
    iget-boolean v3, v0, Landroid/sax/Element;->visited:Z

    #@14
    if-nez v3, :cond_45

    #@16
    .line 196
    new-instance v3, Landroid/sax/BadXmlException;

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Element named "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " is missing required"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, " child element named "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, "."

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-direct {v3, v4, p1}, Landroid/sax/BadXmlException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;)V

    #@44
    throw v3

    #@45
    .line 193
    :cond_45
    add-int/lit8 v1, v1, -0x1

    #@47
    goto :goto_a

    #@48
    .line 203
    .end local v0           #child:Landroid/sax/Element;
    .end local v1           #i:I
    :cond_48
    return-void
.end method

.method public getChild(Ljava/lang/String;)Landroid/sax/Element;
    .registers 3
    .parameter "localName"

    #@0
    .prologue
    .line 60
    const-string v0, ""

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/sax/Element;->getChild(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getChild(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;
    .registers 5
    .parameter "uri"
    .parameter "localName"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Landroid/sax/Element;->endTextElementListener:Landroid/sax/EndTextElementListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "This element already has an end text element listener. It cannot have children."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 72
    :cond_c
    iget-object v0, p0, Landroid/sax/Element;->children:Landroid/sax/Children;

    #@e
    if-nez v0, :cond_17

    #@10
    .line 73
    new-instance v0, Landroid/sax/Children;

    #@12
    invoke-direct {v0}, Landroid/sax/Children;-><init>()V

    #@15
    iput-object v0, p0, Landroid/sax/Element;->children:Landroid/sax/Children;

    #@17
    .line 76
    :cond_17
    iget-object v0, p0, Landroid/sax/Element;->children:Landroid/sax/Children;

    #@19
    invoke-virtual {v0, p0, p1, p2}, Landroid/sax/Children;->getOrCreate(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public requireChild(Ljava/lang/String;)Landroid/sax/Element;
    .registers 3
    .parameter "localName"

    #@0
    .prologue
    .line 86
    const-string v0, ""

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/sax/Element;->requireChild(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public requireChild(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;
    .registers 5
    .parameter "uri"
    .parameter "localName"

    #@0
    .prologue
    .line 95
    invoke-virtual {p0, p1, p2}, Landroid/sax/Element;->getChild(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;

    #@3
    move-result-object v0

    #@4
    .line 97
    .local v0, child:Landroid/sax/Element;
    iget-object v1, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@6
    if-nez v1, :cond_15

    #@8
    .line 98
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v1, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@f
    .line 99
    iget-object v1, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 106
    :cond_14
    :goto_14
    return-object v0

    #@15
    .line 101
    :cond_15
    iget-object v1, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_14

    #@1d
    .line 102
    iget-object v1, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22
    goto :goto_14
.end method

.method resetRequiredChildren()V
    .registers 5

    #@0
    .prologue
    .line 179
    iget-object v1, p0, Landroid/sax/Element;->requiredChilden:Ljava/util/ArrayList;

    #@2
    .line 180
    .local v1, requiredChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/sax/Element;>;"
    if-eqz v1, :cond_18

    #@4
    .line 181
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    add-int/lit8 v0, v2, -0x1

    #@a
    .local v0, i:I
    :goto_a
    if-ltz v0, :cond_18

    #@c
    .line 182
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/sax/Element;

    #@12
    const/4 v3, 0x0

    #@13
    iput-boolean v3, v2, Landroid/sax/Element;->visited:Z

    #@15
    .line 181
    add-int/lit8 v0, v0, -0x1

    #@17
    goto :goto_a

    #@18
    .line 185
    .end local v0           #i:I
    :cond_18
    return-void
.end method

.method public setElementListener(Landroid/sax/ElementListener;)V
    .registers 2
    .parameter "elementListener"

    #@0
    .prologue
    .line 113
    invoke-virtual {p0, p1}, Landroid/sax/Element;->setStartElementListener(Landroid/sax/StartElementListener;)V

    #@3
    .line 114
    invoke-virtual {p0, p1}, Landroid/sax/Element;->setEndElementListener(Landroid/sax/EndElementListener;)V

    #@6
    .line 115
    return-void
.end method

.method public setEndElementListener(Landroid/sax/EndElementListener;)V
    .registers 4
    .parameter "endElementListener"

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/sax/Element;->endElementListener:Landroid/sax/EndElementListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "End element listener has already been set."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 145
    :cond_c
    iput-object p1, p0, Landroid/sax/Element;->endElementListener:Landroid/sax/EndElementListener;

    #@e
    .line 146
    return-void
.end method

.method public setEndTextElementListener(Landroid/sax/EndTextElementListener;)V
    .registers 4
    .parameter "endTextElementListener"

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Landroid/sax/Element;->endTextElementListener:Landroid/sax/EndTextElementListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 154
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "End text element listener has already been set."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 158
    :cond_c
    iget-object v0, p0, Landroid/sax/Element;->children:Landroid/sax/Children;

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    #@12
    const-string v1, "This element already has children. It cannot have an end text element listener."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 163
    :cond_18
    iput-object p1, p0, Landroid/sax/Element;->endTextElementListener:Landroid/sax/EndTextElementListener;

    #@1a
    .line 164
    return-void
.end method

.method public setStartElementListener(Landroid/sax/StartElementListener;)V
    .registers 4
    .parameter "startElementListener"

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Landroid/sax/Element;->startElementListener:Landroid/sax/StartElementListener;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Start element listener has already been set."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 134
    :cond_c
    iput-object p1, p0, Landroid/sax/Element;->startElementListener:Landroid/sax/StartElementListener;

    #@e
    .line 135
    return-void
.end method

.method public setTextElementListener(Landroid/sax/TextElementListener;)V
    .registers 2
    .parameter "elementListener"

    #@0
    .prologue
    .line 121
    invoke-virtual {p0, p1}, Landroid/sax/Element;->setStartElementListener(Landroid/sax/StartElementListener;)V

    #@3
    .line 122
    invoke-virtual {p0, p1}, Landroid/sax/Element;->setEndTextElementListener(Landroid/sax/EndTextElementListener;)V

    #@6
    .line 123
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Landroid/sax/Element;->uri:Ljava/lang/String;

    #@2
    iget-object v1, p0, Landroid/sax/Element;->localName:Ljava/lang/String;

    #@4
    invoke-static {v0, v1}, Landroid/sax/Element;->toString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
