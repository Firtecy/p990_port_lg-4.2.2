.class public abstract Landroid/accounts/IAccountAuthenticatorResponse$Stub;
.super Landroid/os/Binder;
.source "IAccountAuthenticatorResponse.java"

# interfaces
.implements Landroid/accounts/IAccountAuthenticatorResponse;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/IAccountAuthenticatorResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/IAccountAuthenticatorResponse$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.accounts.IAccountAuthenticatorResponse"

.field static final TRANSACTION_onError:I = 0x3

.field static final TRANSACTION_onRequestContinued:I = 0x2

.field static final TRANSACTION_onResult:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.accounts.IAccountAuthenticatorResponse"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.accounts.IAccountAuthenticatorResponse"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/accounts/IAccountAuthenticatorResponse;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/accounts/IAccountAuthenticatorResponse;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/accounts/IAccountAuthenticatorResponse$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 42
    sparse-switch p1, :sswitch_data_42

    #@4
    .line 79
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 46
    :sswitch_9
    const-string v3, "android.accounts.IAccountAuthenticatorResponse"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 51
    :sswitch_f
    const-string v3, "android.accounts.IAccountAuthenticatorResponse"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_26

    #@1a
    .line 54
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/os/Bundle;

    #@22
    .line 59
    .local v0, _arg0:Landroid/os/Bundle;
    :goto_22
    invoke-virtual {p0, v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->onResult(Landroid/os/Bundle;)V

    #@25
    goto :goto_8

    #@26
    .line 57
    .end local v0           #_arg0:Landroid/os/Bundle;
    :cond_26
    const/4 v0, 0x0

    #@27
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_22

    #@28
    .line 64
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_28
    const-string v3, "android.accounts.IAccountAuthenticatorResponse"

    #@2a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 65
    invoke-virtual {p0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->onRequestContinued()V

    #@30
    goto :goto_8

    #@31
    .line 70
    :sswitch_31
    const-string v3, "android.accounts.IAccountAuthenticatorResponse"

    #@33
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v0

    #@3a
    .line 74
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    .line 75
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->onError(ILjava/lang/String;)V

    #@41
    goto :goto_8

    #@42
    .line 42
    :sswitch_data_42
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_31
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
