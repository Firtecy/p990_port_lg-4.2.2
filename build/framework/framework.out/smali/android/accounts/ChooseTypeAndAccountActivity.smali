.class public Landroid/accounts/ChooseTypeAndAccountActivity;
.super Landroid/app/Activity;
.source "ChooseTypeAndAccountActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# static fields
.field public static final EXTRA_ADD_ACCOUNT_AUTH_TOKEN_TYPE_STRING:Ljava/lang/String; = "authTokenType"

.field public static final EXTRA_ADD_ACCOUNT_OPTIONS_BUNDLE:Ljava/lang/String; = "addAccountOptions"

.field public static final EXTRA_ADD_ACCOUNT_REQUIRED_FEATURES_STRING_ARRAY:Ljava/lang/String; = "addAccountRequiredFeatures"

.field public static final EXTRA_ALLOWABLE_ACCOUNTS_ARRAYLIST:Ljava/lang/String; = "allowableAccounts"

.field public static final EXTRA_ALLOWABLE_ACCOUNT_TYPES_STRING_ARRAY:Ljava/lang/String; = "allowableAccountTypes"

.field public static final EXTRA_ALWAYS_PROMPT_FOR_ACCOUNT:Ljava/lang/String; = "alwaysPromptForAccount"

.field public static final EXTRA_DESCRIPTION_TEXT_OVERRIDE:Ljava/lang/String; = "descriptionTextOverride"

.field public static final EXTRA_SELECTED_ACCOUNT:Ljava/lang/String; = "selectedAccount"

.field private static final KEY_INSTANCE_STATE_EXISTING_ACCOUNTS:Ljava/lang/String; = "existingAccounts"

.field private static final KEY_INSTANCE_STATE_PENDING_REQUEST:Ljava/lang/String; = "pendingRequest"

.field private static final KEY_INSTANCE_STATE_SELECTED_ACCOUNT_NAME:Ljava/lang/String; = "selectedAccountName"

.field private static final KEY_INSTANCE_STATE_SELECTED_ADD_ACCOUNT:Ljava/lang/String; = "selectedAddAccount"

.field public static final REQUEST_ADD_ACCOUNT:I = 0x2

.field public static final REQUEST_CHOOSE_TYPE:I = 0x1

.field public static final REQUEST_NULL:I = 0x0

.field private static final SELECTED_ITEM_NONE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "AccountChooser"


# instance fields
.field private mAccounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation
.end field

.field private mAlwaysPromptForAccount:Z

.field private mDescriptionOverride:Ljava/lang/String;

.field private mExistingAccounts:[Landroid/os/Parcelable;

.field private mOkButton:Landroid/widget/Button;

.field private mPendingRequest:I

.field private mSelectedAccountName:Ljava/lang/String;

.field private mSelectedAddNewAccount:Z

.field private mSelectedItemIndex:I

.field private mSetOfAllowableAccounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation
.end field

.field private mSetOfRelevantAccountTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@5
    .line 112
    iput-object v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAccountName:Ljava/lang/String;

    #@7
    .line 113
    iput-boolean v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAddNewAccount:Z

    #@9
    .line 114
    iput-boolean v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAlwaysPromptForAccount:Z

    #@b
    .line 118
    iput v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@d
    .line 119
    iput-object v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mExistingAccounts:[Landroid/os/Parcelable;

    #@f
    return-void
.end method

.method static synthetic access$002(Landroid/accounts/ChooseTypeAndAccountActivity;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 44
    iput p1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/accounts/ChooseTypeAndAccountActivity;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 44
    iget-object v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mOkButton:Landroid/widget/Button;

    #@2
    return-object v0
.end method

.method private getAcceptableAccountChoices(Landroid/accounts/AccountManager;)Ljava/util/ArrayList;
    .registers 10
    .parameter "accountManager"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 442
    invoke-virtual {p1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    #@3
    move-result-object v1

    #@4
    .line 443
    .local v1, accounts:[Landroid/accounts/Account;
    new-instance v2, Ljava/util/ArrayList;

    #@6
    array-length v6, v1

    #@7
    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    #@a
    .line 444
    .local v2, accountsToPopulate:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    move-object v3, v1

    #@b
    .local v3, arr$:[Landroid/accounts/Account;
    array-length v5, v3

    #@c
    .local v5, len$:I
    const/4 v4, 0x0

    #@d
    .local v4, i$:I
    :goto_d
    if-ge v4, v5, :cond_32

    #@f
    aget-object v0, v3, v4

    #@11
    .line 445
    .local v0, account:Landroid/accounts/Account;
    iget-object v6, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfAllowableAccounts:Ljava/util/Set;

    #@13
    if-eqz v6, :cond_20

    #@15
    iget-object v6, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfAllowableAccounts:Ljava/util/Set;

    #@17
    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@1a
    move-result v6

    #@1b
    if-nez v6, :cond_20

    #@1d
    .line 444
    :cond_1d
    :goto_1d
    add-int/lit8 v4, v4, 0x1

    #@1f
    goto :goto_d

    #@20
    .line 449
    :cond_20
    iget-object v6, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfRelevantAccountTypes:Ljava/util/Set;

    #@22
    if-eqz v6, :cond_2e

    #@24
    iget-object v6, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfRelevantAccountTypes:Ljava/util/Set;

    #@26
    iget-object v7, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@28
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@2b
    move-result v6

    #@2c
    if-eqz v6, :cond_1d

    #@2e
    .line 453
    :cond_2e
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    goto :goto_1d

    #@32
    .line 455
    .end local v0           #account:Landroid/accounts/Account;
    :cond_32
    return-object v2
.end method

.method private getAllowableAccountSet(Landroid/content/Intent;)Ljava/util/Set;
    .registers 7
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/Set",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 485
    const/4 v2, 0x0

    #@1
    .line 486
    .local v2, setOfAllowableAccounts:Ljava/util/Set;,"Ljava/util/Set<Landroid/accounts/Account;>;"
    const-string v4, "allowableAccounts"

    #@3
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@6
    move-result-object v3

    #@7
    .line 488
    .local v3, validAccounts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    if-eqz v3, :cond_28

    #@9
    .line 489
    new-instance v2, Ljava/util/HashSet;

    #@b
    .end local v2           #setOfAllowableAccounts:Ljava/util/Set;,"Ljava/util/Set<Landroid/accounts/Account;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v4

    #@f
    invoke-direct {v2, v4}, Ljava/util/HashSet;-><init>(I)V

    #@12
    .line 490
    .restart local v2       #setOfAllowableAccounts:Ljava/util/Set;,"Ljava/util/Set<Landroid/accounts/Account;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v0

    #@16
    .local v0, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_28

    #@1c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/os/Parcelable;

    #@22
    .line 491
    .local v1, parcelable:Landroid/os/Parcelable;
    check-cast v1, Landroid/accounts/Account;

    #@24
    .end local v1           #parcelable:Landroid/os/Parcelable;
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@27
    goto :goto_16

    #@28
    .line 494
    .end local v0           #i$:Ljava/util/Iterator;
    :cond_28
    return-object v2
.end method

.method private getItemIndexToSelect(Ljava/util/ArrayList;Ljava/lang/String;Z)I
    .registers 6
    .parameter
    .parameter "selectedAccountName"
    .parameter "selectedAddNewAccount"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/accounts/Account;",
            ">;",
            "Ljava/lang/String;",
            "Z)I"
        }
    .end annotation

    #@0
    .prologue
    .line 411
    .local p1, accounts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    if-eqz p3, :cond_7

    #@2
    .line 412
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 421
    :cond_6
    :goto_6
    return v0

    #@7
    .line 415
    :cond_7
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v1

    #@c
    if-ge v0, v1, :cond_1f

    #@e
    .line 416
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/accounts/Account;

    #@14
    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_6

    #@1c
    .line 415
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_8

    #@1f
    .line 421
    :cond_1f
    const/4 v0, -0x1

    #@20
    goto :goto_6
.end method

.method private getListOfDisplayableOptions(Ljava/util/ArrayList;)[Ljava/lang/String;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/accounts/Account;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 427
    .local p1, accounts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    add-int/lit8 v2, v2, 0x1

    #@6
    new-array v1, v2, [Ljava/lang/String;

    #@8
    .line 428
    .local v1, listItems:[Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    .local v0, i:I
    :goto_9
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v2

    #@d
    if-ge v0, v2, :cond_1c

    #@f
    .line 429
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/accounts/Account;

    #@15
    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@17
    aput-object v2, v1, v0

    #@19
    .line 428
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_9

    #@1c
    .line 431
    :cond_1c
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v2

    #@20
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getResources()Landroid/content/res/Resources;

    #@23
    move-result-object v3

    #@24
    const v4, 0x10404e2

    #@27
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    aput-object v3, v1, v2

    #@2d
    .line 433
    return-object v1
.end method

.method private getReleventAccountTypes(Landroid/content/Intent;)Ljava/util/Set;
    .registers 11
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 465
    const/4 v6, 0x0

    #@1
    .line 466
    .local v6, setOfRelevantAccountTypes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v8, "allowableAccountTypes"

    #@3
    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 468
    .local v0, allowedAccountTypes:[Ljava/lang/String;
    if-eqz v0, :cond_2d

    #@9
    .line 469
    invoke-static {v0}, Lcom/google/android/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    #@c
    move-result-object v6

    #@d
    .line 470
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    #@14
    move-result-object v3

    #@15
    .line 471
    .local v3, descs:[Landroid/accounts/AuthenticatorDescription;
    new-instance v7, Ljava/util/HashSet;

    #@17
    array-length v8, v3

    #@18
    invoke-direct {v7, v8}, Ljava/util/HashSet;-><init>(I)V

    #@1b
    .line 472
    .local v7, supportedAccountTypes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    move-object v1, v3

    #@1c
    .local v1, arr$:[Landroid/accounts/AuthenticatorDescription;
    array-length v5, v1

    #@1d
    .local v5, len$:I
    const/4 v4, 0x0

    #@1e
    .local v4, i$:I
    :goto_1e
    if-ge v4, v5, :cond_2a

    #@20
    aget-object v2, v1, v4

    #@22
    .line 473
    .local v2, desc:Landroid/accounts/AuthenticatorDescription;
    iget-object v8, v2, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@24
    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@27
    .line 472
    add-int/lit8 v4, v4, 0x1

    #@29
    goto :goto_1e

    #@2a
    .line 475
    .end local v2           #desc:Landroid/accounts/AuthenticatorDescription;
    :cond_2a
    invoke-interface {v6, v7}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    #@2d
    .line 477
    .end local v1           #arr$:[Landroid/accounts/AuthenticatorDescription;
    .end local v3           #descs:[Landroid/accounts/AuthenticatorDescription;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v7           #supportedAccountTypes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2d
    return-object v6
.end method

.method private onAccountSelected(Landroid/accounts/Account;)V
    .registers 5
    .parameter "account"

    #@0
    .prologue
    .line 369
    const-string v0, "AccountChooser"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "selected account "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 370
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@1b
    iget-object v1, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@1d
    invoke-direct {p0, v0, v1}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResultAndFinish(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 371
    return-void
.end method

.method private overrideDescriptionIfSupplied(Ljava/lang/String;)V
    .registers 4
    .parameter "descriptionOverride"

    #@0
    .prologue
    .line 502
    const v1, 0x1020286

    #@3
    invoke-virtual {p0, v1}, Landroid/accounts/ChooseTypeAndAccountActivity;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/TextView;

    #@9
    .line 503
    .local v0, descriptionView:Landroid/widget/TextView;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_13

    #@f
    .line 504
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@12
    .line 508
    :goto_12
    return-void

    #@13
    .line 506
    :cond_13
    const/16 v1, 0x8

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@18
    goto :goto_12
.end method

.method private final populateUIAccountList([Ljava/lang/String;)V
    .registers 6
    .parameter "listItems"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 515
    const v1, 0x102000a

    #@4
    invoke-virtual {p0, v1}, Landroid/accounts/ChooseTypeAndAccountActivity;->findViewById(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/widget/ListView;

    #@a
    .line 516
    .local v0, list:Landroid/widget/ListView;
    new-instance v1, Landroid/widget/ArrayAdapter;

    #@c
    const v2, 0x109000f

    #@f
    invoke-direct {v1, p0, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    #@12
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@15
    .line 518
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@18
    .line 519
    const/4 v1, 0x0

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    #@1c
    .line 520
    new-instance v1, Landroid/accounts/ChooseTypeAndAccountActivity$1;

    #@1e
    invoke-direct {v1, p0}, Landroid/accounts/ChooseTypeAndAccountActivity$1;-><init>(Landroid/accounts/ChooseTypeAndAccountActivity;)V

    #@21
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@24
    .line 528
    iget v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@26
    const/4 v2, -0x1

    #@27
    if-eq v1, v2, :cond_57

    #@29
    .line 529
    iget v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@2b
    invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@2e
    .line 530
    const-string v1, "AccountChooser"

    #@30
    const/4 v2, 0x2

    #@31
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_57

    #@37
    .line 531
    const-string v1, "AccountChooser"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "List item "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    iget v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, " should be selected"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 534
    :cond_57
    return-void
.end method

.method private setResultAndFinish(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "accountName"
    .parameter "accountType"

    #@0
    .prologue
    .line 374
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 375
    .local v0, bundle:Landroid/os/Bundle;
    const-string v1, "authAccount"

    #@7
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 376
    const-string v1, "accountType"

    #@c
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 377
    const/4 v1, -0x1

    #@10
    new-instance v2, Landroid/content/Intent;

    #@12
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@15
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {p0, v1, v2}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResult(ILandroid/content/Intent;)V

    #@1c
    .line 378
    const-string v1, "AccountChooser"

    #@1e
    const/4 v2, 0x2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_47

    #@25
    .line 379
    const-string v1, "AccountChooser"

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v3, "ChooseTypeAndAccountActivity.setResultAndFinish: selected account "

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    const-string v3, ", "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 382
    :cond_47
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->finish()V

    #@4a
    .line 383
    return-void
.end method

.method private startChooseAccountTypeActivity()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 386
    const-string v1, "AccountChooser"

    #@3
    const/4 v2, 0x2

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 387
    const-string v1, "AccountChooser"

    #@c
    const-string v2, "ChooseAccountTypeActivity.startChooseAccountTypeActivity()"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 389
    :cond_11
    new-instance v0, Landroid/content/Intent;

    #@13
    const-class v1, Landroid/accounts/ChooseAccountTypeActivity;

    #@15
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@18
    .line 390
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x8

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@1d
    .line 391
    const-string v1, "allowableAccountTypes"

    #@1f
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@22
    move-result-object v2

    #@23
    const-string v3, "allowableAccountTypes"

    #@25
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@2c
    .line 393
    const-string v1, "addAccountOptions"

    #@2e
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@31
    move-result-object v2

    #@32
    const-string v3, "addAccountOptions"

    #@34
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@3b
    .line 395
    const-string v1, "addAccountRequiredFeatures"

    #@3d
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@40
    move-result-object v2

    #@41
    const-string v3, "addAccountRequiredFeatures"

    #@43
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@4a
    .line 397
    const-string v1, "authTokenType"

    #@4c
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@4f
    move-result-object v2

    #@50
    const-string v3, "authTokenType"

    #@52
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@59
    .line 399
    invoke-virtual {p0, v0, v4}, Landroid/accounts/ChooseTypeAndAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V

    #@5c
    .line 400
    iput v4, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@5e
    .line 401
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 18
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 258
    const-string v11, "AccountChooser"

    #@2
    const/4 v12, 0x2

    #@3
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v11

    #@7
    if-eqz v11, :cond_52

    #@9
    .line 259
    if-eqz p3, :cond_18

    #@b
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@e
    move-result-object v11

    #@f
    if-eqz v11, :cond_18

    #@11
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@14
    move-result-object v11

    #@15
    invoke-virtual {v11}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@18
    .line 260
    :cond_18
    if-eqz p3, :cond_6b

    #@1a
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@1d
    move-result-object v7

    #@1e
    .line 261
    .local v7, extras:Landroid/os/Bundle;
    :goto_1e
    const-string v11, "AccountChooser"

    #@20
    new-instance v12, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v13, "ChooseTypeAndAccountActivity.onActivityResult(reqCode="

    #@27
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v12

    #@2b
    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v12

    #@2f
    const-string v13, ", resCode="

    #@31
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v12

    #@35
    move/from16 v0, p2

    #@37
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v12

    #@3b
    const-string v13, ", extras="

    #@3d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v12

    #@41
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v12

    #@45
    const-string v13, ")"

    #@47
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v12

    #@4b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v12

    #@4f
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 266
    .end local v7           #extras:Landroid/os/Bundle;
    :cond_52
    const/4 v11, 0x0

    #@53
    iput v11, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@55
    .line 268
    if-nez p2, :cond_6d

    #@57
    .line 271
    iget-object v11, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@59
    if-eqz v11, :cond_6a

    #@5b
    iget-object v11, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@5d
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    #@60
    move-result v11

    #@61
    if-eqz v11, :cond_6a

    #@63
    .line 272
    const/4 v11, 0x0

    #@64
    invoke-virtual {p0, v11}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResult(I)V

    #@67
    .line 273
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->finish()V

    #@6a
    .line 326
    :cond_6a
    :goto_6a
    return-void

    #@6b
    .line 260
    :cond_6b
    const/4 v7, 0x0

    #@6c
    goto :goto_1e

    #@6d
    .line 278
    :cond_6d
    const/4 v11, -0x1

    #@6e
    move/from16 v0, p2

    #@70
    if-ne v0, v11, :cond_93

    #@72
    .line 279
    const/4 v11, 0x1

    #@73
    if-ne p1, v11, :cond_ab

    #@75
    .line 280
    if-eqz p3, :cond_85

    #@77
    .line 281
    const-string v11, "accountType"

    #@79
    move-object/from16 v0, p3

    #@7b
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@7e
    move-result-object v4

    #@7f
    .line 282
    .local v4, accountType:Ljava/lang/String;
    if-eqz v4, :cond_85

    #@81
    .line 283
    invoke-virtual {p0, v4}, Landroid/accounts/ChooseTypeAndAccountActivity;->runAddAccountForAuthenticator(Ljava/lang/String;)V

    #@84
    goto :goto_6a

    #@85
    .line 287
    .end local v4           #accountType:Ljava/lang/String;
    :cond_85
    const-string v11, "AccountChooser"

    #@87
    const-string v12, "ChooseTypeAndAccountActivity.onActivityResult: unable to find account type, pretending the request was canceled"

    #@89
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 318
    :cond_8c
    const-string v11, "AccountChooser"

    #@8e
    const-string v12, "ChooseTypeAndAccountActivity.onActivityResult: unable to find added account, pretending the request was canceled"

    #@90
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 321
    :cond_93
    const-string v11, "AccountChooser"

    #@95
    const/4 v12, 0x2

    #@96
    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@99
    move-result v11

    #@9a
    if-eqz v11, :cond_a3

    #@9c
    .line 322
    const-string v11, "AccountChooser"

    #@9e
    const-string v12, "ChooseTypeAndAccountActivity.onActivityResult: canceled"

    #@a0
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 324
    :cond_a3
    const/4 v11, 0x0

    #@a4
    invoke-virtual {p0, v11}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResult(I)V

    #@a7
    .line 325
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->finish()V

    #@aa
    goto :goto_6a

    #@ab
    .line 289
    :cond_ab
    const/4 v11, 0x2

    #@ac
    if-ne p1, v11, :cond_8c

    #@ae
    .line 290
    const/4 v2, 0x0

    #@af
    .line 291
    .local v2, accountName:Ljava/lang/String;
    const/4 v4, 0x0

    #@b0
    .line 293
    .restart local v4       #accountType:Ljava/lang/String;
    if-eqz p3, :cond_c2

    #@b2
    .line 294
    const-string v11, "authAccount"

    #@b4
    move-object/from16 v0, p3

    #@b6
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@b9
    move-result-object v2

    #@ba
    .line 295
    const-string v11, "accountType"

    #@bc
    move-object/from16 v0, p3

    #@be
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@c1
    move-result-object v4

    #@c2
    .line 298
    :cond_c2
    if-eqz v2, :cond_c6

    #@c4
    if-nez v4, :cond_f4

    #@c6
    .line 299
    :cond_c6
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@c9
    move-result-object v11

    #@ca
    invoke-virtual {v11}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    #@cd
    move-result-object v6

    #@ce
    .line 300
    .local v6, currentAccounts:[Landroid/accounts/Account;
    new-instance v10, Ljava/util/HashSet;

    #@d0
    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    #@d3
    .line 301
    .local v10, preExistingAccounts:Ljava/util/Set;,"Ljava/util/Set<Landroid/accounts/Account;>;"
    iget-object v5, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mExistingAccounts:[Landroid/os/Parcelable;

    #@d5
    .local v5, arr$:[Landroid/os/Parcelable;
    array-length v9, v5

    #@d6
    .local v9, len$:I
    const/4 v8, 0x0

    #@d7
    .local v8, i$:I
    :goto_d7
    if-ge v8, v9, :cond_e3

    #@d9
    aget-object v3, v5, v8

    #@db
    .line 302
    .local v3, accountParcel:Landroid/os/Parcelable;
    check-cast v3, Landroid/accounts/Account;

    #@dd
    .end local v3           #accountParcel:Landroid/os/Parcelable;
    invoke-interface {v10, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@e0
    .line 301
    add-int/lit8 v8, v8, 0x1

    #@e2
    goto :goto_d7

    #@e3
    .line 304
    :cond_e3
    move-object v5, v6

    #@e4
    .local v5, arr$:[Landroid/accounts/Account;
    array-length v9, v5

    #@e5
    const/4 v8, 0x0

    #@e6
    :goto_e6
    if-ge v8, v9, :cond_f4

    #@e8
    aget-object v1, v5, v8

    #@ea
    .line 305
    .local v1, account:Landroid/accounts/Account;
    invoke-interface {v10, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@ed
    move-result v11

    #@ee
    if-nez v11, :cond_fd

    #@f0
    .line 306
    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@f2
    .line 307
    iget-object v4, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@f4
    .line 313
    .end local v1           #account:Landroid/accounts/Account;
    .end local v5           #arr$:[Landroid/accounts/Account;
    .end local v6           #currentAccounts:[Landroid/accounts/Account;
    .end local v8           #i$:I
    .end local v9           #len$:I
    .end local v10           #preExistingAccounts:Ljava/util/Set;,"Ljava/util/Set<Landroid/accounts/Account;>;"
    :cond_f4
    if-nez v2, :cond_f8

    #@f6
    if-eqz v4, :cond_8c

    #@f8
    .line 314
    :cond_f8
    invoke-direct {p0, v2, v4}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResultAndFinish(Ljava/lang/String;Ljava/lang/String;)V

    #@fb
    goto/16 :goto_6a

    #@fd
    .line 304
    .restart local v1       #account:Landroid/accounts/Account;
    .restart local v5       #arr$:[Landroid/accounts/Account;
    .restart local v6       #currentAccounts:[Landroid/accounts/Account;
    .restart local v8       #i$:I
    .restart local v9       #len$:I
    .restart local v10       #preExistingAccounts:Ljava/util/Set;,"Ljava/util/Set<Landroid/accounts/Account;>;"
    :cond_fd
    add-int/lit8 v8, v8, 0x1

    #@ff
    goto :goto_e6
.end method

.method public onCancelButtonClicked(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 240
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->onBackPressed()V

    #@3
    .line 241
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x0

    #@2
    .line 125
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@5
    .line 126
    const-string v2, "AccountChooser"

    #@7
    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_2b

    #@d
    .line 127
    const-string v2, "AccountChooser"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "ChooseTypeAndAccountActivity.onCreate(savedInstanceState="

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, ")"

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 132
    :cond_2b
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@2e
    move-result-object v0

    #@2f
    .line 134
    .local v0, intent:Landroid/content/Intent;
    if-eqz p1, :cond_94

    #@31
    .line 135
    const-string/jumbo v2, "pendingRequest"

    #@34
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@37
    move-result v2

    #@38
    iput v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@3a
    .line 136
    const-string v2, "existingAccounts"

    #@3c
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    #@3f
    move-result-object v2

    #@40
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mExistingAccounts:[Landroid/os/Parcelable;

    #@42
    .line 140
    const-string/jumbo v2, "selectedAccountName"

    #@45
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAccountName:Ljava/lang/String;

    #@4b
    .line 143
    const-string/jumbo v2, "selectedAddAccount"

    #@4e
    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@51
    move-result v2

    #@52
    iput-boolean v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAddNewAccount:Z

    #@54
    .line 156
    :cond_54
    :goto_54
    const-string v2, "AccountChooser"

    #@56
    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_77

    #@5c
    .line 157
    const-string v2, "AccountChooser"

    #@5e
    new-instance v3, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string/jumbo v4, "selected account name is "

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    iget-object v4, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAccountName:Ljava/lang/String;

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 161
    :cond_77
    invoke-direct {p0, v0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getAllowableAccountSet(Landroid/content/Intent;)Ljava/util/Set;

    #@7a
    move-result-object v2

    #@7b
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfAllowableAccounts:Ljava/util/Set;

    #@7d
    .line 162
    invoke-direct {p0, v0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getReleventAccountTypes(Landroid/content/Intent;)Ljava/util/Set;

    #@80
    move-result-object v2

    #@81
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfRelevantAccountTypes:Ljava/util/Set;

    #@83
    .line 163
    const-string v2, "alwaysPromptForAccount"

    #@85
    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@88
    move-result v2

    #@89
    iput-boolean v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAlwaysPromptForAccount:Z

    #@8b
    .line 164
    const-string v2, "descriptionTextOverride"

    #@8d
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mDescriptionOverride:Ljava/lang/String;

    #@93
    .line 165
    return-void

    #@94
    .line 146
    :cond_94
    iput v5, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@96
    .line 147
    const/4 v2, 0x0

    #@97
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mExistingAccounts:[Landroid/os/Parcelable;

    #@99
    .line 150
    const-string/jumbo v2, "selectedAccount"

    #@9c
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@9f
    move-result-object v1

    #@a0
    check-cast v1, Landroid/accounts/Account;

    #@a2
    .line 151
    .local v1, selectedAccount:Landroid/accounts/Account;
    if-eqz v1, :cond_54

    #@a4
    .line 152
    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@a6
    iput-object v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAccountName:Ljava/lang/String;

    #@a8
    goto :goto_54
.end method

.method protected onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 215
    const-string v0, "AccountChooser"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 216
    const-string v0, "AccountChooser"

    #@b
    const-string v1, "ChooseTypeAndAccountActivity.onDestroy()"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 218
    :cond_10
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    #@13
    .line 219
    return-void
.end method

.method public onOkButtonClicked(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 244
    iget v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@2
    iget-object v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_e

    #@a
    .line 246
    invoke-direct {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->startChooseAccountTypeActivity()V

    #@d
    .line 250
    :cond_d
    :goto_d
    return-void

    #@e
    .line 247
    :cond_e
    iget v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@10
    const/4 v1, -0x1

    #@11
    if-eq v0, v1, :cond_d

    #@13
    .line 248
    iget-object v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@15
    iget v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/accounts/Account;

    #@1d
    invoke-direct {p0, v0}, Landroid/accounts/ChooseTypeAndAccountActivity;->onAccountSelected(Landroid/accounts/Account;)V

    #@20
    goto :goto_d
.end method

.method protected onResume()V
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 169
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    #@5
    .line 170
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@8
    move-result-object v1

    #@9
    .line 172
    .local v1, accountManager:Landroid/accounts/AccountManager;
    invoke-direct {p0, v1}, Landroid/accounts/ChooseTypeAndAccountActivity;->getAcceptableAccountChoices(Landroid/accounts/AccountManager;)Ljava/util/ArrayList;

    #@c
    move-result-object v3

    #@d
    iput-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@f
    .line 178
    iget v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@11
    if-nez v3, :cond_53

    #@13
    .line 181
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_37

    #@1b
    .line 182
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfRelevantAccountTypes:Ljava/util/Set;

    #@1d
    invoke-interface {v3}, Ljava/util/Set;->size()I

    #@20
    move-result v3

    #@21
    if-ne v3, v4, :cond_33

    #@23
    .line 183
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSetOfRelevantAccountTypes:Ljava/util/Set;

    #@25
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v3

    #@29
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Ljava/lang/String;

    #@2f
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseTypeAndAccountActivity;->runAddAccountForAuthenticator(Ljava/lang/String;)V

    #@32
    .line 211
    :goto_32
    return-void

    #@33
    .line 185
    :cond_33
    invoke-direct {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->startChooseAccountTypeActivity()V

    #@36
    goto :goto_32

    #@37
    .line 191
    :cond_37
    iget-boolean v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAlwaysPromptForAccount:Z

    #@39
    if-nez v3, :cond_53

    #@3b
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@3d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@40
    move-result v3

    #@41
    if-ne v3, v4, :cond_53

    #@43
    .line 192
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@45
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    check-cast v0, Landroid/accounts/Account;

    #@4b
    .line 193
    .local v0, account:Landroid/accounts/Account;
    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@4d
    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@4f
    invoke-direct {p0, v3, v4}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResultAndFinish(Ljava/lang/String;Ljava/lang/String;)V

    #@52
    goto :goto_32

    #@53
    .line 198
    .end local v0           #account:Landroid/accounts/Account;
    :cond_53
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@55
    invoke-direct {p0, v3}, Landroid/accounts/ChooseTypeAndAccountActivity;->getListOfDisplayableOptions(Ljava/util/ArrayList;)[Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    .line 199
    .local v2, listItems:[Ljava/lang/String;
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@5b
    iget-object v6, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAccountName:Ljava/lang/String;

    #@5d
    iget-boolean v7, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedAddNewAccount:Z

    #@5f
    invoke-direct {p0, v3, v6, v7}, Landroid/accounts/ChooseTypeAndAccountActivity;->getItemIndexToSelect(Ljava/util/ArrayList;Ljava/lang/String;Z)I

    #@62
    move-result v3

    #@63
    iput v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@65
    .line 204
    const v3, 0x1090036

    #@68
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseTypeAndAccountActivity;->setContentView(I)V

    #@6b
    .line 205
    iget-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mDescriptionOverride:Ljava/lang/String;

    #@6d
    invoke-direct {p0, v3}, Landroid/accounts/ChooseTypeAndAccountActivity;->overrideDescriptionIfSupplied(Ljava/lang/String;)V

    #@70
    .line 206
    invoke-direct {p0, v2}, Landroid/accounts/ChooseTypeAndAccountActivity;->populateUIAccountList([Ljava/lang/String;)V

    #@73
    .line 209
    const v3, 0x102001a

    #@76
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseTypeAndAccountActivity;->findViewById(I)Landroid/view/View;

    #@79
    move-result-object v3

    #@7a
    check-cast v3, Landroid/widget/Button;

    #@7c
    iput-object v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mOkButton:Landroid/widget/Button;

    #@7e
    .line 210
    iget-object v6, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mOkButton:Landroid/widget/Button;

    #@80
    iget v3, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@82
    const/4 v7, -0x1

    #@83
    if-eq v3, v7, :cond_8a

    #@85
    move v3, v4

    #@86
    :goto_86
    invoke-virtual {v6, v3}, Landroid/widget/Button;->setEnabled(Z)V

    #@89
    goto :goto_32

    #@8a
    :cond_8a
    move v3, v5

    #@8b
    goto :goto_86
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    #@0
    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 224
    const-string/jumbo v0, "pendingRequest"

    #@6
    iget v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@8
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@b
    .line 225
    iget v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@d
    const/4 v1, 0x2

    #@e
    if-ne v0, v1, :cond_17

    #@10
    .line 226
    const-string v0, "existingAccounts"

    #@12
    iget-object v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mExistingAccounts:[Landroid/os/Parcelable;

    #@14
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    #@17
    .line 228
    :cond_17
    iget v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@19
    const/4 v1, -0x1

    #@1a
    if-eq v0, v1, :cond_2d

    #@1c
    .line 229
    iget v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@1e
    iget-object v1, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v1

    #@24
    if-ne v0, v1, :cond_2e

    #@26
    .line 230
    const-string/jumbo v0, "selectedAddAccount"

    #@29
    const/4 v1, 0x1

    #@2a
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@2d
    .line 237
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 232
    :cond_2e
    const-string/jumbo v0, "selectedAddAccount"

    #@31
    const/4 v1, 0x0

    #@32
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@35
    .line 233
    const-string/jumbo v1, "selectedAccountName"

    #@38
    iget-object v0, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mAccounts:Ljava/util/ArrayList;

    #@3a
    iget v2, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mSelectedItemIndex:I

    #@3c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Landroid/accounts/Account;

    #@42
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@44
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    goto :goto_2d
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 345
    .local p1, accountManagerFuture:Landroid/accounts/AccountManagerFuture;,"Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/os/Bundle;

    #@6
    .line 346
    .local v0, accountManagerResult:Landroid/os/Bundle;
    const-string v4, "intent"

    #@8
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@b
    move-result-object v3

    #@c
    check-cast v3, Landroid/content/Intent;

    #@e
    .line 348
    .local v3, intent:Landroid/content/Intent;
    if-eqz v3, :cond_37

    #@10
    .line 349
    const/4 v4, 0x2

    #@11
    iput v4, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mPendingRequest:I

    #@13
    .line 350
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    #@1a
    move-result-object v4

    #@1b
    iput-object v4, p0, Landroid/accounts/ChooseTypeAndAccountActivity;->mExistingAccounts:[Landroid/os/Parcelable;

    #@1d
    .line 351
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    #@20
    move-result v4

    #@21
    const v5, -0x10000001

    #@24
    and-int/2addr v4, v5

    #@25
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@28
    .line 352
    const/4 v4, 0x2

    #@29
    invoke-virtual {p0, v3, v4}, Landroid/accounts/ChooseTypeAndAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2c
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_2c} :catch_2d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_2c} :catch_54
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_2c} :catch_36

    #@2c
    .line 366
    .end local v0           #accountManagerResult:Landroid/os/Bundle;
    .end local v3           #intent:Landroid/content/Intent;
    :goto_2c
    return-void

    #@2d
    .line 355
    :catch_2d
    move-exception v2

    #@2e
    .line 356
    .local v2, e:Landroid/accounts/OperationCanceledException;
    const/4 v4, 0x0

    #@2f
    invoke-virtual {p0, v4}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResult(I)V

    #@32
    .line 357
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->finish()V

    #@35
    goto :goto_2c

    #@36
    .line 360
    .end local v2           #e:Landroid/accounts/OperationCanceledException;
    :catch_36
    move-exception v4

    #@37
    .line 362
    :cond_37
    :goto_37
    new-instance v1, Landroid/os/Bundle;

    #@39
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@3c
    .line 363
    .local v1, bundle:Landroid/os/Bundle;
    const-string v4, "errorMessage"

    #@3e
    const-string v5, "error communicating with server"

    #@40
    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 364
    const/4 v4, -0x1

    #@44
    new-instance v5, Landroid/content/Intent;

    #@46
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@49
    invoke-virtual {v5, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {p0, v4, v5}, Landroid/accounts/ChooseTypeAndAccountActivity;->setResult(ILandroid/content/Intent;)V

    #@50
    .line 365
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->finish()V

    #@53
    goto :goto_2c

    #@54
    .line 359
    .end local v1           #bundle:Landroid/os/Bundle;
    :catch_54
    move-exception v4

    #@55
    goto :goto_37
.end method

.method protected runAddAccountForAuthenticator(Ljava/lang/String;)V
    .registers 10
    .parameter "type"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 329
    const-string v0, "AccountChooser"

    #@3
    const/4 v1, 0x2

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_23

    #@a
    .line 330
    const-string v0, "AccountChooser"

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string/jumbo v6, "runAddAccountForAuthenticator: "

    #@14
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 332
    :cond_23
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@26
    move-result-object v0

    #@27
    const-string v1, "addAccountOptions"

    #@29
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    #@2c
    move-result-object v4

    #@2d
    .line 334
    .local v4, options:Landroid/os/Bundle;
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@30
    move-result-object v0

    #@31
    const-string v1, "addAccountRequiredFeatures"

    #@33
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    .line 336
    .local v3, requiredFeatures:[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/accounts/ChooseTypeAndAccountActivity;->getIntent()Landroid/content/Intent;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, "authTokenType"

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    .line 338
    .local v2, authTokenType:Ljava/lang/String;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@44
    move-result-object v0

    #@45
    move-object v1, p1

    #@46
    move-object v6, p0

    #@47
    move-object v7, v5

    #@48
    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    #@4b
    .line 340
    return-void
.end method
