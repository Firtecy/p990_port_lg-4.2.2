.class public abstract Landroid/accounts/IAccountManager$Stub;
.super Landroid/os/Binder;
.source "IAccountManager.java"

# interfaces
.implements Landroid/accounts/IAccountManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/IAccountManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/IAccountManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.accounts.IAccountManager"

.field static final TRANSACTION_addAccount:I = 0x8

.field static final TRANSACTION_addAcount:I = 0x12

.field static final TRANSACTION_clearPassword:I = 0xe

.field static final TRANSACTION_confirmCredentialsAsUser:I = 0x15

.field static final TRANSACTION_editProperties:I = 0x14

.field static final TRANSACTION_getAccounts:I = 0x4

.field static final TRANSACTION_getAccountsAsUser:I = 0x5

.field static final TRANSACTION_getAccountsByFeatures:I = 0x7

.field static final TRANSACTION_getAuthToken:I = 0x11

.field static final TRANSACTION_getAuthTokenLabel:I = 0x16

.field static final TRANSACTION_getAuthenticatorTypes:I = 0x3

.field static final TRANSACTION_getPassword:I = 0x1

.field static final TRANSACTION_getUserData:I = 0x2

.field static final TRANSACTION_hasFeatures:I = 0x6

.field static final TRANSACTION_invalidateAuthToken:I = 0xa

.field static final TRANSACTION_peekAuthToken:I = 0xb

.field static final TRANSACTION_removeAccount:I = 0x9

.field static final TRANSACTION_setAuthToken:I = 0xc

.field static final TRANSACTION_setPassword:I = 0xd

.field static final TRANSACTION_setUserData:I = 0xf

.field static final TRANSACTION_updateAppPermission:I = 0x10

.field static final TRANSACTION_updateCredentials:I = 0x13


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.accounts.IAccountManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/accounts/IAccountManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.accounts.IAccountManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/accounts/IAccountManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/accounts/IAccountManager;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/accounts/IAccountManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/accounts/IAccountManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_370

    #@5
    .line 430
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v8

    #@9
    :goto_9
    return v8

    #@a
    .line 46
    :sswitch_a
    const-string v0, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v0, "android.accounts.IAccountManager"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_2e

    #@1b
    .line 54
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/accounts/Account;

    #@23
    .line 59
    .local v1, _arg0:Landroid/accounts/Account;
    :goto_23
    invoke-virtual {p0, v1}, Landroid/accounts/IAccountManager$Stub;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    .line 60
    .local v7, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    .line 61
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    goto :goto_9

    #@2e
    .line 57
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v7           #_result:Ljava/lang/String;
    :cond_2e
    const/4 v1, 0x0

    #@2f
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_23

    #@30
    .line 66
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_30
    const-string v0, "android.accounts.IAccountManager"

    #@32
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_52

    #@3b
    .line 69
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3d
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@40
    move-result-object v1

    #@41
    check-cast v1, Landroid/accounts/Account;

    #@43
    .line 75
    .restart local v1       #_arg0:Landroid/accounts/Account;
    :goto_43
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    .line 76
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountManager$Stub;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    .line 77
    .restart local v7       #_result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    .line 78
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@51
    goto :goto_9

    #@52
    .line 72
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v7           #_result:Ljava/lang/String;
    :cond_52
    const/4 v1, 0x0

    #@53
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_43

    #@54
    .line 83
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_54
    const-string v0, "android.accounts.IAccountManager"

    #@56
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59
    .line 84
    invoke-virtual {p0}, Landroid/accounts/IAccountManager$Stub;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    #@5c
    move-result-object v7

    #@5d
    .line 85
    .local v7, _result:[Landroid/accounts/AuthenticatorDescription;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@60
    .line 86
    invoke-virtual {p3, v7, v8}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@63
    goto :goto_9

    #@64
    .line 91
    .end local v7           #_result:[Landroid/accounts/AuthenticatorDescription;
    :sswitch_64
    const-string v0, "android.accounts.IAccountManager"

    #@66
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@69
    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    .line 94
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/accounts/IAccountManager$Stub;->getAccounts(Ljava/lang/String;)[Landroid/accounts/Account;

    #@70
    move-result-object v7

    #@71
    .line 95
    .local v7, _result:[Landroid/accounts/Account;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@74
    .line 96
    invoke-virtual {p3, v7, v8}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@77
    goto :goto_9

    #@78
    .line 101
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v7           #_result:[Landroid/accounts/Account;
    :sswitch_78
    const-string v0, "android.accounts.IAccountManager"

    #@7a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7d
    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@80
    move-result-object v1

    #@81
    .line 105
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@84
    move-result v2

    #@85
    .line 106
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountManager$Stub;->getAccountsAsUser(Ljava/lang/String;I)[Landroid/accounts/Account;

    #@88
    move-result-object v7

    #@89
    .line 107
    .restart local v7       #_result:[Landroid/accounts/Account;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    .line 108
    invoke-virtual {p3, v7, v8}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@8f
    goto/16 :goto_9

    #@91
    .line 113
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v7           #_result:[Landroid/accounts/Account;
    :sswitch_91
    const-string v0, "android.accounts.IAccountManager"

    #@93
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@96
    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@99
    move-result-object v0

    #@9a
    invoke-static {v0}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@9d
    move-result-object v1

    #@9e
    .line 117
    .local v1, _arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a1
    move-result v0

    #@a2
    if-eqz v0, :cond_b8

    #@a4
    .line 118
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a6
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a9
    move-result-object v2

    #@aa
    check-cast v2, Landroid/accounts/Account;

    #@ac
    .line 124
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_ac
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@af
    move-result-object v3

    #@b0
    .line 125
    .local v3, _arg2:[Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->hasFeatures(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;[Ljava/lang/String;)V

    #@b3
    .line 126
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    goto/16 :goto_9

    #@b8
    .line 121
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:[Ljava/lang/String;
    :cond_b8
    const/4 v2, 0x0

    #@b9
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_ac

    #@ba
    .line 131
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    :sswitch_ba
    const-string v0, "android.accounts.IAccountManager"

    #@bc
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c2
    move-result-object v0

    #@c3
    invoke-static {v0}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@c6
    move-result-object v1

    #@c7
    .line 135
    .restart local v1       #_arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ca
    move-result-object v2

    #@cb
    .line 137
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@ce
    move-result-object v3

    #@cf
    .line 138
    .restart local v3       #_arg2:[Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->getAccountsByFeatures(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;[Ljava/lang/String;)V

    #@d2
    .line 139
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d5
    goto/16 :goto_9

    #@d7
    .line 144
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:[Ljava/lang/String;
    :sswitch_d7
    const-string v9, "android.accounts.IAccountManager"

    #@d9
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 146
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@df
    move-result v9

    #@e0
    if-eqz v9, :cond_10b

    #@e2
    .line 147
    sget-object v9, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e4
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e7
    move-result-object v1

    #@e8
    check-cast v1, Landroid/accounts/Account;

    #@ea
    .line 153
    .local v1, _arg0:Landroid/accounts/Account;
    :goto_ea
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ed
    move-result-object v2

    #@ee
    .line 155
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v9

    #@f2
    if-eqz v9, :cond_10d

    #@f4
    .line 156
    sget-object v9, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f6
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f9
    move-result-object v3

    #@fa
    check-cast v3, Landroid/os/Bundle;

    #@fc
    .line 161
    .local v3, _arg2:Landroid/os/Bundle;
    :goto_fc
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->addAccount(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    #@ff
    move-result v7

    #@100
    .line 162
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@103
    .line 163
    if-eqz v7, :cond_106

    #@105
    move v0, v8

    #@106
    :cond_106
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@109
    goto/16 :goto_9

    #@10b
    .line 150
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Landroid/os/Bundle;
    .end local v7           #_result:Z
    :cond_10b
    const/4 v1, 0x0

    #@10c
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_ea

    #@10d
    .line 159
    .restart local v2       #_arg1:Ljava/lang/String;
    :cond_10d
    const/4 v3, 0x0

    #@10e
    .restart local v3       #_arg2:Landroid/os/Bundle;
    goto :goto_fc

    #@10f
    .line 168
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Landroid/os/Bundle;
    :sswitch_10f
    const-string v0, "android.accounts.IAccountManager"

    #@111
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@114
    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@117
    move-result-object v0

    #@118
    invoke-static {v0}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@11b
    move-result-object v1

    #@11c
    .line 172
    .local v1, _arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11f
    move-result v0

    #@120
    if-eqz v0, :cond_132

    #@122
    .line 173
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@124
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@127
    move-result-object v2

    #@128
    check-cast v2, Landroid/accounts/Account;

    #@12a
    .line 178
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_12a
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountManager$Stub;->removeAccount(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;)V

    #@12d
    .line 179
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@130
    goto/16 :goto_9

    #@132
    .line 176
    .end local v2           #_arg1:Landroid/accounts/Account;
    :cond_132
    const/4 v2, 0x0

    #@133
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_12a

    #@134
    .line 184
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    :sswitch_134
    const-string v0, "android.accounts.IAccountManager"

    #@136
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@139
    .line 186
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13c
    move-result-object v1

    #@13d
    .line 188
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@140
    move-result-object v2

    #@141
    .line 189
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountManager$Stub;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    #@144
    .line 190
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@147
    goto/16 :goto_9

    #@149
    .line 195
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_149
    const-string v0, "android.accounts.IAccountManager"

    #@14b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14e
    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@151
    move-result v0

    #@152
    if-eqz v0, :cond_16c

    #@154
    .line 198
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@156
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@159
    move-result-object v1

    #@15a
    check-cast v1, Landroid/accounts/Account;

    #@15c
    .line 204
    .local v1, _arg0:Landroid/accounts/Account;
    :goto_15c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15f
    move-result-object v2

    #@160
    .line 205
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountManager$Stub;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    #@163
    move-result-object v7

    #@164
    .line 206
    .local v7, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@167
    .line 207
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16a
    goto/16 :goto_9

    #@16c
    .line 201
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v7           #_result:Ljava/lang/String;
    :cond_16c
    const/4 v1, 0x0

    #@16d
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_15c

    #@16e
    .line 212
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_16e
    const-string v0, "android.accounts.IAccountManager"

    #@170
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173
    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@176
    move-result v0

    #@177
    if-eqz v0, :cond_191

    #@179
    .line 215
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@17b
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17e
    move-result-object v1

    #@17f
    check-cast v1, Landroid/accounts/Account;

    #@181
    .line 221
    .restart local v1       #_arg0:Landroid/accounts/Account;
    :goto_181
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@184
    move-result-object v2

    #@185
    .line 223
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@188
    move-result-object v3

    #@189
    .line 224
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    #@18c
    .line 225
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18f
    goto/16 :goto_9

    #@191
    .line 218
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    :cond_191
    const/4 v1, 0x0

    #@192
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_181

    #@193
    .line 230
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_193
    const-string v0, "android.accounts.IAccountManager"

    #@195
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@198
    .line 232
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19b
    move-result v0

    #@19c
    if-eqz v0, :cond_1b2

    #@19e
    .line 233
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a0
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a3
    move-result-object v1

    #@1a4
    check-cast v1, Landroid/accounts/Account;

    #@1a6
    .line 239
    .restart local v1       #_arg0:Landroid/accounts/Account;
    :goto_1a6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1a9
    move-result-object v2

    #@1aa
    .line 240
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountManager$Stub;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    #@1ad
    .line 241
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b0
    goto/16 :goto_9

    #@1b2
    .line 236
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    :cond_1b2
    const/4 v1, 0x0

    #@1b3
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_1a6

    #@1b4
    .line 246
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_1b4
    const-string v0, "android.accounts.IAccountManager"

    #@1b6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b9
    .line 248
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1bc
    move-result v0

    #@1bd
    if-eqz v0, :cond_1cf

    #@1bf
    .line 249
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c1
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c4
    move-result-object v1

    #@1c5
    check-cast v1, Landroid/accounts/Account;

    #@1c7
    .line 254
    .restart local v1       #_arg0:Landroid/accounts/Account;
    :goto_1c7
    invoke-virtual {p0, v1}, Landroid/accounts/IAccountManager$Stub;->clearPassword(Landroid/accounts/Account;)V

    #@1ca
    .line 255
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1cd
    goto/16 :goto_9

    #@1cf
    .line 252
    .end local v1           #_arg0:Landroid/accounts/Account;
    :cond_1cf
    const/4 v1, 0x0

    #@1d0
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_1c7

    #@1d1
    .line 260
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_1d1
    const-string v0, "android.accounts.IAccountManager"

    #@1d3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d6
    .line 262
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d9
    move-result v0

    #@1da
    if-eqz v0, :cond_1f4

    #@1dc
    .line 263
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1de
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1e1
    move-result-object v1

    #@1e2
    check-cast v1, Landroid/accounts/Account;

    #@1e4
    .line 269
    .restart local v1       #_arg0:Landroid/accounts/Account;
    :goto_1e4
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e7
    move-result-object v2

    #@1e8
    .line 271
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1eb
    move-result-object v3

    #@1ec
    .line 272
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    #@1ef
    .line 273
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f2
    goto/16 :goto_9

    #@1f4
    .line 266
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    :cond_1f4
    const/4 v1, 0x0

    #@1f5
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_1e4

    #@1f6
    .line 278
    .end local v1           #_arg0:Landroid/accounts/Account;
    :sswitch_1f6
    const-string v9, "android.accounts.IAccountManager"

    #@1f8
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1fb
    .line 280
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1fe
    move-result v9

    #@1ff
    if-eqz v9, :cond_220

    #@201
    .line 281
    sget-object v9, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@203
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@206
    move-result-object v1

    #@207
    check-cast v1, Landroid/accounts/Account;

    #@209
    .line 287
    .restart local v1       #_arg0:Landroid/accounts/Account;
    :goto_209
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20c
    move-result-object v2

    #@20d
    .line 289
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@210
    move-result v3

    #@211
    .line 291
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@214
    move-result v9

    #@215
    if-eqz v9, :cond_222

    #@217
    move v4, v8

    #@218
    .line 292
    .local v4, _arg3:Z
    :goto_218
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/accounts/IAccountManager$Stub;->updateAppPermission(Landroid/accounts/Account;Ljava/lang/String;IZ)V

    #@21b
    .line 293
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21e
    goto/16 :goto_9

    #@220
    .line 284
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:Z
    :cond_220
    const/4 v1, 0x0

    #@221
    .restart local v1       #_arg0:Landroid/accounts/Account;
    goto :goto_209

    #@222
    .restart local v2       #_arg1:Ljava/lang/String;
    .restart local v3       #_arg2:I
    :cond_222
    move v4, v0

    #@223
    .line 291
    goto :goto_218

    #@224
    .line 298
    .end local v1           #_arg0:Landroid/accounts/Account;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    :sswitch_224
    const-string v9, "android.accounts.IAccountManager"

    #@226
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@229
    .line 300
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@22c
    move-result-object v9

    #@22d
    invoke-static {v9}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@230
    move-result-object v1

    #@231
    .line 302
    .local v1, _arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@234
    move-result v9

    #@235
    if-eqz v9, :cond_268

    #@237
    .line 303
    sget-object v9, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@239
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23c
    move-result-object v2

    #@23d
    check-cast v2, Landroid/accounts/Account;

    #@23f
    .line 309
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_23f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@242
    move-result-object v3

    #@243
    .line 311
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@246
    move-result v9

    #@247
    if-eqz v9, :cond_26a

    #@249
    move v4, v8

    #@24a
    .line 313
    .restart local v4       #_arg3:Z
    :goto_24a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@24d
    move-result v9

    #@24e
    if-eqz v9, :cond_26c

    #@250
    move v5, v8

    #@251
    .line 315
    .local v5, _arg4:Z
    :goto_251
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@254
    move-result v0

    #@255
    if-eqz v0, :cond_26e

    #@257
    .line 316
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@259
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25c
    move-result-object v6

    #@25d
    check-cast v6, Landroid/os/Bundle;

    #@25f
    .local v6, _arg5:Landroid/os/Bundle;
    :goto_25f
    move-object v0, p0

    #@260
    .line 321
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/IAccountManager$Stub;->getAuthToken(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZZLandroid/os/Bundle;)V

    #@263
    .line 322
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@266
    goto/16 :goto_9

    #@268
    .line 306
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:Landroid/os/Bundle;
    :cond_268
    const/4 v2, 0x0

    #@269
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_23f

    #@26a
    .restart local v3       #_arg2:Ljava/lang/String;
    :cond_26a
    move v4, v0

    #@26b
    .line 311
    goto :goto_24a

    #@26c
    .restart local v4       #_arg3:Z
    :cond_26c
    move v5, v0

    #@26d
    .line 313
    goto :goto_251

    #@26e
    .line 319
    .restart local v5       #_arg4:Z
    :cond_26e
    const/4 v6, 0x0

    #@26f
    .restart local v6       #_arg5:Landroid/os/Bundle;
    goto :goto_25f

    #@270
    .line 327
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:Landroid/os/Bundle;
    :sswitch_270
    const-string v9, "android.accounts.IAccountManager"

    #@272
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@275
    .line 329
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@278
    move-result-object v9

    #@279
    invoke-static {v9}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@27c
    move-result-object v1

    #@27d
    .line 331
    .restart local v1       #_arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@280
    move-result-object v2

    #@281
    .line 333
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@284
    move-result-object v3

    #@285
    .line 335
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@288
    move-result-object v4

    #@289
    .line 337
    .local v4, _arg3:[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28c
    move-result v9

    #@28d
    if-eqz v9, :cond_2a7

    #@28f
    move v5, v8

    #@290
    .line 339
    .restart local v5       #_arg4:Z
    :goto_290
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@293
    move-result v0

    #@294
    if-eqz v0, :cond_2a9

    #@296
    .line 340
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@298
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@29b
    move-result-object v6

    #@29c
    check-cast v6, Landroid/os/Bundle;

    #@29e
    .restart local v6       #_arg5:Landroid/os/Bundle;
    :goto_29e
    move-object v0, p0

    #@29f
    .line 345
    invoke-virtual/range {v0 .. v6}, Landroid/accounts/IAccountManager$Stub;->addAcount(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLandroid/os/Bundle;)V

    #@2a2
    .line 346
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a5
    goto/16 :goto_9

    #@2a7
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:Landroid/os/Bundle;
    :cond_2a7
    move v5, v0

    #@2a8
    .line 337
    goto :goto_290

    #@2a9
    .line 343
    .restart local v5       #_arg4:Z
    :cond_2a9
    const/4 v6, 0x0

    #@2aa
    .restart local v6       #_arg5:Landroid/os/Bundle;
    goto :goto_29e

    #@2ab
    .line 351
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:[Ljava/lang/String;
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:Landroid/os/Bundle;
    :sswitch_2ab
    const-string v9, "android.accounts.IAccountManager"

    #@2ad
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b0
    .line 353
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2b3
    move-result-object v9

    #@2b4
    invoke-static {v9}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@2b7
    move-result-object v1

    #@2b8
    .line 355
    .restart local v1       #_arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2bb
    move-result v9

    #@2bc
    if-eqz v9, :cond_2e8

    #@2be
    .line 356
    sget-object v9, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c0
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c3
    move-result-object v2

    #@2c4
    check-cast v2, Landroid/accounts/Account;

    #@2c6
    .line 362
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_2c6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c9
    move-result-object v3

    #@2ca
    .line 364
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cd
    move-result v9

    #@2ce
    if-eqz v9, :cond_2ea

    #@2d0
    move v4, v8

    #@2d1
    .line 366
    .local v4, _arg3:Z
    :goto_2d1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d4
    move-result v0

    #@2d5
    if-eqz v0, :cond_2ec

    #@2d7
    .line 367
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d9
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2dc
    move-result-object v5

    #@2dd
    check-cast v5, Landroid/os/Bundle;

    #@2df
    .local v5, _arg4:Landroid/os/Bundle;
    :goto_2df
    move-object v0, p0

    #@2e0
    .line 372
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/IAccountManager$Stub;->updateCredentials(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZLandroid/os/Bundle;)V

    #@2e3
    .line 373
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e6
    goto/16 :goto_9

    #@2e8
    .line 359
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Landroid/os/Bundle;
    :cond_2e8
    const/4 v2, 0x0

    #@2e9
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_2c6

    #@2ea
    .restart local v3       #_arg2:Ljava/lang/String;
    :cond_2ea
    move v4, v0

    #@2eb
    .line 364
    goto :goto_2d1

    #@2ec
    .line 370
    .restart local v4       #_arg3:Z
    :cond_2ec
    const/4 v5, 0x0

    #@2ed
    .restart local v5       #_arg4:Landroid/os/Bundle;
    goto :goto_2df

    #@2ee
    .line 378
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Landroid/os/Bundle;
    :sswitch_2ee
    const-string v9, "android.accounts.IAccountManager"

    #@2f0
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f3
    .line 380
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2f6
    move-result-object v9

    #@2f7
    invoke-static {v9}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@2fa
    move-result-object v1

    #@2fb
    .line 382
    .restart local v1       #_arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2fe
    move-result-object v2

    #@2ff
    .line 384
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@302
    move-result v9

    #@303
    if-eqz v9, :cond_30e

    #@305
    move v3, v8

    #@306
    .line 385
    .local v3, _arg2:Z
    :goto_306
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->editProperties(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Z)V

    #@309
    .line 386
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@30c
    goto/16 :goto_9

    #@30e
    .end local v3           #_arg2:Z
    :cond_30e
    move v3, v0

    #@30f
    .line 384
    goto :goto_306

    #@310
    .line 391
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_310
    const-string v9, "android.accounts.IAccountManager"

    #@312
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@315
    .line 393
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@318
    move-result-object v9

    #@319
    invoke-static {v9}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@31c
    move-result-object v1

    #@31d
    .line 395
    .restart local v1       #_arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@320
    move-result v9

    #@321
    if-eqz v9, :cond_34d

    #@323
    .line 396
    sget-object v9, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@325
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@328
    move-result-object v2

    #@329
    check-cast v2, Landroid/accounts/Account;

    #@32b
    .line 402
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_32b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@32e
    move-result v9

    #@32f
    if-eqz v9, :cond_34f

    #@331
    .line 403
    sget-object v9, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@333
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@336
    move-result-object v3

    #@337
    check-cast v3, Landroid/os/Bundle;

    #@339
    .line 409
    .local v3, _arg2:Landroid/os/Bundle;
    :goto_339
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33c
    move-result v9

    #@33d
    if-eqz v9, :cond_351

    #@33f
    move v4, v8

    #@340
    .line 411
    .restart local v4       #_arg3:Z
    :goto_340
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@343
    move-result v5

    #@344
    .local v5, _arg4:I
    move-object v0, p0

    #@345
    .line 412
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/IAccountManager$Stub;->confirmCredentialsAsUser(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Landroid/os/Bundle;ZI)V

    #@348
    .line 413
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34b
    goto/16 :goto_9

    #@34d
    .line 399
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Landroid/os/Bundle;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:I
    :cond_34d
    const/4 v2, 0x0

    #@34e
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_32b

    #@34f
    .line 406
    :cond_34f
    const/4 v3, 0x0

    #@350
    .restart local v3       #_arg2:Landroid/os/Bundle;
    goto :goto_339

    #@351
    :cond_351
    move v4, v0

    #@352
    .line 409
    goto :goto_340

    #@353
    .line 418
    .end local v1           #_arg0:Landroid/accounts/IAccountManagerResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Landroid/os/Bundle;
    :sswitch_353
    const-string v0, "android.accounts.IAccountManager"

    #@355
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@358
    .line 420
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@35b
    move-result-object v0

    #@35c
    invoke-static {v0}, Landroid/accounts/IAccountManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountManagerResponse;

    #@35f
    move-result-object v1

    #@360
    .line 422
    .restart local v1       #_arg0:Landroid/accounts/IAccountManagerResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@363
    move-result-object v2

    #@364
    .line 424
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@367
    move-result-object v3

    #@368
    .line 425
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountManager$Stub;->getAuthTokenLabel(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;)V

    #@36b
    .line 426
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@36e
    goto/16 :goto_9

    #@370
    .line 42
    :sswitch_data_370
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_30
        0x3 -> :sswitch_54
        0x4 -> :sswitch_64
        0x5 -> :sswitch_78
        0x6 -> :sswitch_91
        0x7 -> :sswitch_ba
        0x8 -> :sswitch_d7
        0x9 -> :sswitch_10f
        0xa -> :sswitch_134
        0xb -> :sswitch_149
        0xc -> :sswitch_16e
        0xd -> :sswitch_193
        0xe -> :sswitch_1b4
        0xf -> :sswitch_1d1
        0x10 -> :sswitch_1f6
        0x11 -> :sswitch_224
        0x12 -> :sswitch_270
        0x13 -> :sswitch_2ab
        0x14 -> :sswitch_2ee
        0x15 -> :sswitch_310
        0x16 -> :sswitch_353
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
