.class Landroid/accounts/AccountManagerService$4;
.super Landroid/accounts/AccountManagerService$Session;
.source "AccountManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/accounts/AccountManagerService;->getAuthToken(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZZLandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/accounts/AccountManagerService;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accounts:Landroid/accounts/AccountManagerService$UserAccounts;

.field final synthetic val$authTokenType:Ljava/lang/String;

.field final synthetic val$callerUid:I

.field final synthetic val$customTokens:Z

.field final synthetic val$loginOptions:Landroid/os/Bundle;

.field final synthetic val$notifyOnAuthFailure:Z

.field final synthetic val$permissionGranted:Z


# direct methods
.method constructor <init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLandroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;ZZIZLandroid/accounts/AccountManagerService$UserAccounts;)V
    .registers 15
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1133
    iput-object p1, p0, Landroid/accounts/AccountManagerService$4;->this$0:Landroid/accounts/AccountManagerService;

    #@2
    iput-object p7, p0, Landroid/accounts/AccountManagerService$4;->val$loginOptions:Landroid/os/Bundle;

    #@4
    iput-object p8, p0, Landroid/accounts/AccountManagerService$4;->val$account:Landroid/accounts/Account;

    #@6
    iput-object p9, p0, Landroid/accounts/AccountManagerService$4;->val$authTokenType:Ljava/lang/String;

    #@8
    iput-boolean p10, p0, Landroid/accounts/AccountManagerService$4;->val$notifyOnAuthFailure:Z

    #@a
    iput-boolean p11, p0, Landroid/accounts/AccountManagerService$4;->val$permissionGranted:Z

    #@c
    iput p12, p0, Landroid/accounts/AccountManagerService$4;->val$callerUid:I

    #@e
    iput-boolean p13, p0, Landroid/accounts/AccountManagerService$4;->val$customTokens:Z

    #@10
    iput-object p14, p0, Landroid/accounts/AccountManagerService$4;->val$accounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@12
    invoke-direct/range {p0 .. p6}, Landroid/accounts/AccountManagerService$Session;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZ)V

    #@15
    return-void
.end method


# virtual methods
.method public onResult(Landroid/os/Bundle;)V
    .registers 12
    .parameter "result"

    #@0
    .prologue
    .line 1154
    if-eqz p1, :cond_90

    #@2
    .line 1155
    const-string v0, "authTokenLabelKey"

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_2f

    #@a
    .line 1156
    iget-object v0, p0, Landroid/accounts/AccountManagerService$4;->this$0:Landroid/accounts/AccountManagerService;

    #@c
    iget-object v1, p0, Landroid/accounts/AccountManagerService$4;->val$account:Landroid/accounts/Account;

    #@e
    iget v2, p0, Landroid/accounts/AccountManagerService$4;->val$callerUid:I

    #@10
    new-instance v3, Landroid/accounts/AccountAuthenticatorResponse;

    #@12
    invoke-direct {v3, p0}, Landroid/accounts/AccountAuthenticatorResponse;-><init>(Landroid/accounts/IAccountAuthenticatorResponse;)V

    #@15
    iget-object v4, p0, Landroid/accounts/AccountManagerService$4;->val$authTokenType:Ljava/lang/String;

    #@17
    const-string v5, "authTokenLabelKey"

    #@19
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-static/range {v0 .. v5}, Landroid/accounts/AccountManagerService;->access$1100(Landroid/accounts/AccountManagerService;Landroid/accounts/Account;ILandroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@20
    move-result-object v4

    #@21
    .line 1160
    .local v4, intent:Landroid/content/Intent;
    new-instance v7, Landroid/os/Bundle;

    #@23
    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    #@26
    .line 1161
    .local v7, bundle:Landroid/os/Bundle;
    const-string v0, "intent"

    #@28
    invoke-virtual {v7, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@2b
    .line 1162
    invoke-virtual {p0, v7}, Landroid/accounts/AccountManagerService$4;->onResult(Landroid/os/Bundle;)V

    #@2e
    .line 1188
    .end local v4           #intent:Landroid/content/Intent;
    .end local v7           #bundle:Landroid/os/Bundle;
    :goto_2e
    return-void

    #@2f
    .line 1165
    :cond_2f
    const-string v0, "authtoken"

    #@31
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    .line 1166
    .local v6, authToken:Ljava/lang/String;
    if-eqz v6, :cond_69

    #@37
    .line 1167
    const-string v0, "authAccount"

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v8

    #@3d
    .line 1168
    .local v8, name:Ljava/lang/String;
    const-string v0, "accountType"

    #@3f
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v9

    #@43
    .line 1169
    .local v9, type:Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@46
    move-result v0

    #@47
    if-nez v0, :cond_4f

    #@49
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4c
    move-result v0

    #@4d
    if-eqz v0, :cond_57

    #@4f
    .line 1170
    :cond_4f
    const/4 v0, 0x5

    #@50
    const-string/jumbo v1, "the type and name should not be empty"

    #@53
    invoke-virtual {p0, v0, v1}, Landroid/accounts/AccountManagerService$4;->onError(ILjava/lang/String;)V

    #@56
    goto :goto_2e

    #@57
    .line 1174
    :cond_57
    iget-boolean v0, p0, Landroid/accounts/AccountManagerService$4;->val$customTokens:Z

    #@59
    if-nez v0, :cond_69

    #@5b
    .line 1175
    iget-object v0, p0, Landroid/accounts/AccountManagerService$4;->this$0:Landroid/accounts/AccountManagerService;

    #@5d
    iget-object v1, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@5f
    new-instance v2, Landroid/accounts/Account;

    #@61
    invoke-direct {v2, v8, v9}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    iget-object v3, p0, Landroid/accounts/AccountManagerService$4;->val$authTokenType:Ljava/lang/String;

    #@66
    invoke-static {v0, v1, v2, v3, v6}, Landroid/accounts/AccountManagerService;->access$1200(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    #@69
    .line 1180
    .end local v8           #name:Ljava/lang/String;
    .end local v9           #type:Ljava/lang/String;
    :cond_69
    const-string v0, "intent"

    #@6b
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@6e
    move-result-object v4

    #@6f
    check-cast v4, Landroid/content/Intent;

    #@71
    .line 1181
    .restart local v4       #intent:Landroid/content/Intent;
    if-eqz v4, :cond_90

    #@73
    iget-boolean v0, p0, Landroid/accounts/AccountManagerService$4;->val$notifyOnAuthFailure:Z

    #@75
    if-eqz v0, :cond_90

    #@77
    iget-boolean v0, p0, Landroid/accounts/AccountManagerService$4;->val$customTokens:Z

    #@79
    if-nez v0, :cond_90

    #@7b
    .line 1182
    iget-object v0, p0, Landroid/accounts/AccountManagerService$4;->this$0:Landroid/accounts/AccountManagerService;

    #@7d
    iget-object v1, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@7f
    iget-object v2, p0, Landroid/accounts/AccountManagerService$4;->val$account:Landroid/accounts/Account;

    #@81
    const-string v3, "authFailedMessage"

    #@83
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@86
    move-result-object v3

    #@87
    iget-object v5, p0, Landroid/accounts/AccountManagerService$4;->val$accounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@89
    invoke-static {v5}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@8c
    move-result v5

    #@8d
    invoke-static/range {v0 .. v5}, Landroid/accounts/AccountManagerService;->access$1300(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/CharSequence;Landroid/content/Intent;I)V

    #@90
    .line 1187
    .end local v4           #intent:Landroid/content/Intent;
    .end local v6           #authToken:Ljava/lang/String;
    :cond_90
    invoke-super {p0, p1}, Landroid/accounts/AccountManagerService$Session;->onResult(Landroid/os/Bundle;)V

    #@93
    goto :goto_2e
.end method

.method public run()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1146
    iget-boolean v0, p0, Landroid/accounts/AccountManagerService$4;->val$permissionGranted:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 1147
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@6
    iget-object v1, p0, Landroid/accounts/AccountManagerService$4;->val$authTokenType:Ljava/lang/String;

    #@8
    invoke-interface {v0, p0, v1}, Landroid/accounts/IAccountAuthenticator;->getAuthTokenLabel(Landroid/accounts/IAccountAuthenticatorResponse;Ljava/lang/String;)V

    #@b
    .line 1151
    :goto_b
    return-void

    #@c
    .line 1149
    :cond_c
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@e
    iget-object v1, p0, Landroid/accounts/AccountManagerService$4;->val$account:Landroid/accounts/Account;

    #@10
    iget-object v2, p0, Landroid/accounts/AccountManagerService$4;->val$authTokenType:Ljava/lang/String;

    #@12
    iget-object v3, p0, Landroid/accounts/AccountManagerService$4;->val$loginOptions:Landroid/os/Bundle;

    #@14
    invoke-interface {v0, p0, v1, v2, v3}, Landroid/accounts/IAccountAuthenticator;->getAuthToken(Landroid/accounts/IAccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@17
    goto :goto_b
.end method

.method protected toDebugString(J)Ljava/lang/String;
    .registers 5
    .parameter "now"

    #@0
    .prologue
    .line 1135
    iget-object v0, p0, Landroid/accounts/AccountManagerService$4;->val$loginOptions:Landroid/os/Bundle;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/accounts/AccountManagerService$4;->val$loginOptions:Landroid/os/Bundle;

    #@6
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@9
    .line 1136
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-super {p0, p1, p2}, Landroid/accounts/AccountManagerService$Session;->toDebugString(J)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    const-string v1, ", getAuthToken"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, ", "

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    iget-object v1, p0, Landroid/accounts/AccountManagerService$4;->val$account:Landroid/accounts/Account;

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, ", authTokenType "

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    iget-object v1, p0, Landroid/accounts/AccountManagerService$4;->val$authTokenType:Ljava/lang/String;

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    const-string v1, ", loginOptions "

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    iget-object v1, p0, Landroid/accounts/AccountManagerService$4;->val$loginOptions:Landroid/os/Bundle;

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    const-string v1, ", notifyOnAuthFailure "

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-boolean v1, p0, Landroid/accounts/AccountManagerService$4;->val$notifyOnAuthFailure:Z

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    return-object v0
.end method
