.class public Landroid/accounts/GrantCredentialsPermissionActivity;
.super Landroid/app/Activity;
.source "GrantCredentialsPermissionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final EXTRAS_ACCOUNT:Ljava/lang/String; = "account"

.field public static final EXTRAS_ACCOUNT_TYPE_LABEL:Ljava/lang/String; = "accountTypeLabel"

.field public static final EXTRAS_AUTH_TOKEN_LABEL:Ljava/lang/String; = "authTokenLabel"

.field public static final EXTRAS_AUTH_TOKEN_TYPE:Ljava/lang/String; = "authTokenType"

.field public static final EXTRAS_PACKAGES:Ljava/lang/String; = "application"

.field public static final EXTRAS_REQUESTING_UID:Ljava/lang/String; = "uid"

.field public static final EXTRAS_RESPONSE:Ljava/lang/String; = "response"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthTokenType:Ljava/lang/String;

.field protected mInflater:Landroid/view/LayoutInflater;

.field private mResultBundle:Landroid/os/Bundle;

.field private mUid:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 49
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mResultBundle:Landroid/os/Bundle;

    #@6
    return-void
.end method

.method private getAccountLabel(Landroid/accounts/Account;)Ljava/lang/String;
    .registers 9
    .parameter "account"

    #@0
    .prologue
    .line 139
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@3
    move-result-object v5

    #@4
    invoke-virtual {v5}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    #@7
    move-result-object v1

    #@8
    .line 141
    .local v1, authenticatorTypes:[Landroid/accounts/AuthenticatorDescription;
    const/4 v4, 0x0

    #@9
    .local v4, i:I
    array-length v0, v1

    #@a
    .local v0, N:I
    :goto_a
    if-ge v4, v0, :cond_31

    #@c
    .line 142
    aget-object v2, v1, v4

    #@e
    .line 143
    .local v2, desc:Landroid/accounts/AuthenticatorDescription;
    iget-object v5, v2, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@10
    iget-object v6, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_2e

    #@18
    .line 145
    :try_start_18
    iget-object v5, v2, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    #@1a
    const/4 v6, 0x0

    #@1b
    invoke-virtual {p0, v5, v6}, Landroid/accounts/GrantCredentialsPermissionActivity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@1e
    move-result-object v5

    #@1f
    iget v6, v2, Landroid/accounts/AuthenticatorDescription;->labelId:I

    #@21
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_24
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_18 .. :try_end_24} :catch_26
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_18 .. :try_end_24} :catch_2a

    #@24
    move-result-object v5

    #@25
    .line 153
    .end local v2           #desc:Landroid/accounts/AuthenticatorDescription;
    :goto_25
    return-object v5

    #@26
    .line 146
    .restart local v2       #desc:Landroid/accounts/AuthenticatorDescription;
    :catch_26
    move-exception v3

    #@27
    .line 147
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@29
    goto :goto_25

    #@2a
    .line 148
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_2a
    move-exception v3

    #@2b
    .line 149
    .local v3, e:Landroid/content/res/Resources$NotFoundException;
    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@2d
    goto :goto_25

    #@2e
    .line 141
    .end local v3           #e:Landroid/content/res/Resources$NotFoundException;
    :cond_2e
    add-int/lit8 v4, v4, 0x1

    #@30
    goto :goto_a

    #@31
    .line 153
    .end local v2           #desc:Landroid/accounts/AuthenticatorDescription;
    :cond_31
    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@33
    goto :goto_25
.end method

.method private newPackageView(Ljava/lang/String;)Landroid/view/View;
    .registers 6
    .parameter "packageLabel"

    #@0
    .prologue
    .line 157
    iget-object v1, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mInflater:Landroid/view/LayoutInflater;

    #@2
    const v2, 0x109009d

    #@5
    const/4 v3, 0x0

    #@6
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 158
    .local v0, view:Landroid/view/View;
    const v1, 0x102034e

    #@d
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/widget/TextView;

    #@13
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@16
    .line 159
    return-object v0
.end method


# virtual methods
.method public finish()V
    .registers 5

    #@0
    .prologue
    .line 189
    invoke-virtual {p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->getIntent()Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 190
    .local v0, intent:Landroid/content/Intent;
    const-string/jumbo v2, "response"

    #@7
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@a
    move-result-object v1

    #@b
    check-cast v1, Landroid/accounts/AccountAuthenticatorResponse;

    #@d
    .line 191
    .local v1, response:Landroid/accounts/AccountAuthenticatorResponse;
    if-eqz v1, :cond_18

    #@f
    .line 193
    iget-object v2, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mResultBundle:Landroid/os/Bundle;

    #@11
    if-eqz v2, :cond_1c

    #@13
    .line 194
    iget-object v2, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mResultBundle:Landroid/os/Bundle;

    #@15
    invoke-virtual {v1, v2}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    #@18
    .line 199
    :cond_18
    :goto_18
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    #@1b
    .line 200
    return-void

    #@1c
    .line 196
    :cond_1c
    const/4 v2, 0x4

    #@1d
    const-string v3, "canceled"

    #@1f
    invoke-virtual {v1, v2, v3}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    #@22
    goto :goto_18
.end method

.method public onClick(Landroid/view/View;)V
    .registers 9
    .parameter "v"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@5
    move-result v1

    #@6
    packed-switch v1, :pswitch_data_42

    #@9
    .line 177
    :goto_9
    invoke-virtual {p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->finish()V

    #@c
    .line 178
    return-void

    #@d
    .line 165
    :pswitch_d
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@13
    iget-object v3, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAuthTokenType:Ljava/lang/String;

    #@15
    iget v4, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mUid:I

    #@17
    invoke-virtual {v1, v2, v3, v4, v6}, Landroid/accounts/AccountManager;->updateAppPermission(Landroid/accounts/Account;Ljava/lang/String;IZ)V

    #@1a
    .line 166
    new-instance v0, Landroid/content/Intent;

    #@1c
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@1f
    .line 167
    .local v0, result:Landroid/content/Intent;
    const-string/jumbo v1, "retry"

    #@22
    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@25
    .line 168
    const/4 v1, -0x1

    #@26
    invoke-virtual {p0, v1, v0}, Landroid/accounts/GrantCredentialsPermissionActivity;->setResult(ILandroid/content/Intent;)V

    #@29
    .line 169
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {p0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    #@30
    goto :goto_9

    #@31
    .line 173
    .end local v0           #result:Landroid/content/Intent;
    :pswitch_31
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@34
    move-result-object v1

    #@35
    iget-object v2, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@37
    iget-object v3, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAuthTokenType:Ljava/lang/String;

    #@39
    iget v4, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mUid:I

    #@3b
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/accounts/AccountManager;->updateAppPermission(Landroid/accounts/Account;Ljava/lang/String;IZ)V

    #@3e
    .line 174
    invoke-virtual {p0, v5}, Landroid/accounts/GrantCredentialsPermissionActivity;->setResult(I)V

    #@41
    goto :goto_9

    #@42
    .line 163
    :pswitch_data_42
    .packed-switch 0x102029d
        :pswitch_31
        :pswitch_d
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 23
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 53
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 54
    sget-boolean v17, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@5
    if-eqz v17, :cond_11

    #@7
    .line 55
    const v17, 0x20a01c9

    #@a
    move-object/from16 v0, p0

    #@c
    move/from16 v1, v17

    #@e
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setTheme(I)V

    #@11
    .line 57
    :cond_11
    const v17, 0x1090046

    #@14
    move-object/from16 v0, p0

    #@16
    move/from16 v1, v17

    #@18
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setContentView(I)V

    #@1b
    .line 58
    const v17, 0x10404a4

    #@1e
    move-object/from16 v0, p0

    #@20
    move/from16 v1, v17

    #@22
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setTitle(I)V

    #@25
    .line 60
    const-string/jumbo v17, "layout_inflater"

    #@28
    move-object/from16 v0, p0

    #@2a
    move-object/from16 v1, v17

    #@2c
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v17

    #@30
    check-cast v17, Landroid/view/LayoutInflater;

    #@32
    move-object/from16 v0, v17

    #@34
    move-object/from16 v1, p0

    #@36
    iput-object v0, v1, Landroid/accounts/GrantCredentialsPermissionActivity;->mInflater:Landroid/view/LayoutInflater;

    #@38
    .line 62
    invoke-virtual/range {p0 .. p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->getIntent()Landroid/content/Intent;

    #@3b
    move-result-object v17

    #@3c
    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@3f
    move-result-object v9

    #@40
    .line 63
    .local v9, extras:Landroid/os/Bundle;
    if-nez v9, :cond_4f

    #@42
    .line 65
    const/16 v17, 0x0

    #@44
    move-object/from16 v0, p0

    #@46
    move/from16 v1, v17

    #@48
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setResult(I)V

    #@4b
    .line 66
    invoke-virtual/range {p0 .. p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->finish()V

    #@4e
    .line 136
    :goto_4e
    return-void

    #@4f
    .line 71
    :cond_4f
    const-string v17, "account"

    #@51
    move-object/from16 v0, v17

    #@53
    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@56
    move-result-object v17

    #@57
    check-cast v17, Landroid/accounts/Account;

    #@59
    move-object/from16 v0, v17

    #@5b
    move-object/from16 v1, p0

    #@5d
    iput-object v0, v1, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@5f
    .line 72
    const-string v17, "authTokenType"

    #@61
    move-object/from16 v0, v17

    #@63
    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@66
    move-result-object v17

    #@67
    move-object/from16 v0, v17

    #@69
    move-object/from16 v1, p0

    #@6b
    iput-object v0, v1, Landroid/accounts/GrantCredentialsPermissionActivity;->mAuthTokenType:Ljava/lang/String;

    #@6d
    .line 73
    const-string/jumbo v17, "uid"

    #@70
    move-object/from16 v0, v17

    #@72
    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@75
    move-result v17

    #@76
    move/from16 v0, v17

    #@78
    move-object/from16 v1, p0

    #@7a
    iput v0, v1, Landroid/accounts/GrantCredentialsPermissionActivity;->mUid:I

    #@7c
    .line 74
    invoke-virtual/range {p0 .. p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7f
    move-result-object v16

    #@80
    .line 75
    .local v16, pm:Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    #@82
    iget v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mUid:I

    #@84
    move/from16 v17, v0

    #@86
    invoke-virtual/range {v16 .. v17}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@89
    move-result-object v13

    #@8a
    .line 77
    .local v13, packages:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@8c
    iget-object v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@8e
    move-object/from16 v17, v0

    #@90
    if-eqz v17, :cond_9c

    #@92
    move-object/from16 v0, p0

    #@94
    iget-object v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAuthTokenType:Ljava/lang/String;

    #@96
    move-object/from16 v17, v0

    #@98
    if-eqz v17, :cond_9c

    #@9a
    if-nez v13, :cond_a9

    #@9c
    .line 79
    :cond_9c
    const/16 v17, 0x0

    #@9e
    move-object/from16 v0, p0

    #@a0
    move/from16 v1, v17

    #@a2
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setResult(I)V

    #@a5
    .line 80
    invoke-virtual/range {p0 .. p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->finish()V

    #@a8
    goto :goto_4e

    #@a9
    .line 86
    :cond_a9
    :try_start_a9
    move-object/from16 v0, p0

    #@ab
    iget-object v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@ad
    move-object/from16 v17, v0

    #@af
    move-object/from16 v0, p0

    #@b1
    move-object/from16 v1, v17

    #@b3
    invoke-direct {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->getAccountLabel(Landroid/accounts/Account;)Ljava/lang/String;
    :try_end_b6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a9 .. :try_end_b6} :catch_14d

    #@b6
    move-result-object v4

    #@b7
    .line 94
    .local v4, accountTypeLabel:Ljava/lang/String;
    const v17, 0x102029a

    #@ba
    move-object/from16 v0, p0

    #@bc
    move/from16 v1, v17

    #@be
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    #@c1
    move-result-object v6

    #@c2
    check-cast v6, Landroid/widget/TextView;

    #@c4
    .line 95
    .local v6, authTokenTypeView:Landroid/widget/TextView;
    const/16 v17, 0x8

    #@c6
    move/from16 v0, v17

    #@c8
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    #@cb
    .line 97
    new-instance v7, Landroid/accounts/GrantCredentialsPermissionActivity$1;

    #@cd
    move-object/from16 v0, p0

    #@cf
    invoke-direct {v7, v0, v6}, Landroid/accounts/GrantCredentialsPermissionActivity$1;-><init>(Landroid/accounts/GrantCredentialsPermissionActivity;Landroid/widget/TextView;)V

    #@d2
    .line 117
    .local v7, callback:Landroid/accounts/AccountManagerCallback;,"Landroid/accounts/AccountManagerCallback<Ljava/lang/String;>;"
    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@d5
    move-result-object v17

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget-object v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@da
    move-object/from16 v18, v0

    #@dc
    move-object/from16 v0, v18

    #@de
    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@e0
    move-object/from16 v18, v0

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget-object v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAuthTokenType:Ljava/lang/String;

    #@e6
    move-object/from16 v19, v0

    #@e8
    const/16 v20, 0x0

    #@ea
    move-object/from16 v0, v17

    #@ec
    move-object/from16 v1, v18

    #@ee
    move-object/from16 v2, v19

    #@f0
    move-object/from16 v3, v20

    #@f2
    invoke-virtual {v0, v1, v2, v7, v3}, Landroid/accounts/AccountManager;->getAuthTokenLabel(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    #@f5
    .line 119
    const v17, 0x102029e

    #@f8
    move-object/from16 v0, p0

    #@fa
    move/from16 v1, v17

    #@fc
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    #@ff
    move-result-object v17

    #@100
    move-object/from16 v0, v17

    #@102
    move-object/from16 v1, p0

    #@104
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@107
    .line 120
    const v17, 0x102029d

    #@10a
    move-object/from16 v0, p0

    #@10c
    move/from16 v1, v17

    #@10e
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    #@111
    move-result-object v17

    #@112
    move-object/from16 v0, v17

    #@114
    move-object/from16 v1, p0

    #@116
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@119
    .line 122
    const v17, 0x1020296

    #@11c
    move-object/from16 v0, p0

    #@11e
    move/from16 v1, v17

    #@120
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    #@123
    move-result-object v14

    #@124
    check-cast v14, Landroid/widget/LinearLayout;

    #@126
    .line 124
    .local v14, packagesListView:Landroid/widget/LinearLayout;
    move-object v5, v13

    #@127
    .local v5, arr$:[Ljava/lang/String;
    array-length v11, v5

    #@128
    .local v11, len$:I
    const/4 v10, 0x0

    #@129
    .local v10, i$:I
    :goto_129
    if-ge v10, v11, :cond_15f

    #@12b
    aget-object v15, v5, v10

    #@12d
    .line 127
    .local v15, pkg:Ljava/lang/String;
    const/16 v17, 0x0

    #@12f
    :try_start_12f
    move-object/from16 v0, v16

    #@131
    move/from16 v1, v17

    #@133
    invoke-virtual {v0, v15, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@136
    move-result-object v17

    #@137
    invoke-virtual/range {v16 .. v17}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@13a
    move-result-object v17

    #@13b
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_13e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12f .. :try_end_13e} :catch_15c

    #@13e
    move-result-object v12

    #@13f
    .line 131
    .local v12, packageLabel:Ljava/lang/String;
    :goto_13f
    move-object/from16 v0, p0

    #@141
    invoke-direct {v0, v12}, Landroid/accounts/GrantCredentialsPermissionActivity;->newPackageView(Ljava/lang/String;)Landroid/view/View;

    #@144
    move-result-object v17

    #@145
    move-object/from16 v0, v17

    #@147
    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@14a
    .line 124
    add-int/lit8 v10, v10, 0x1

    #@14c
    goto :goto_129

    #@14d
    .line 87
    .end local v4           #accountTypeLabel:Ljava/lang/String;
    .end local v5           #arr$:[Ljava/lang/String;
    .end local v6           #authTokenTypeView:Landroid/widget/TextView;
    .end local v7           #callback:Landroid/accounts/AccountManagerCallback;,"Landroid/accounts/AccountManagerCallback<Ljava/lang/String;>;"
    .end local v10           #i$:I
    .end local v11           #len$:I
    .end local v12           #packageLabel:Ljava/lang/String;
    .end local v14           #packagesListView:Landroid/widget/LinearLayout;
    .end local v15           #pkg:Ljava/lang/String;
    :catch_14d
    move-exception v8

    #@14e
    .line 89
    .local v8, e:Ljava/lang/IllegalArgumentException;
    const/16 v17, 0x0

    #@150
    move-object/from16 v0, p0

    #@152
    move/from16 v1, v17

    #@154
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->setResult(I)V

    #@157
    .line 90
    invoke-virtual/range {p0 .. p0}, Landroid/accounts/GrantCredentialsPermissionActivity;->finish()V

    #@15a
    goto/16 :goto_4e

    #@15c
    .line 128
    .end local v8           #e:Ljava/lang/IllegalArgumentException;
    .restart local v4       #accountTypeLabel:Ljava/lang/String;
    .restart local v5       #arr$:[Ljava/lang/String;
    .restart local v6       #authTokenTypeView:Landroid/widget/TextView;
    .restart local v7       #callback:Landroid/accounts/AccountManagerCallback;,"Landroid/accounts/AccountManagerCallback<Ljava/lang/String;>;"
    .restart local v10       #i$:I
    .restart local v11       #len$:I
    .restart local v14       #packagesListView:Landroid/widget/LinearLayout;
    .restart local v15       #pkg:Ljava/lang/String;
    :catch_15c
    move-exception v8

    #@15d
    .line 129
    .local v8, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    move-object v12, v15

    #@15e
    .restart local v12       #packageLabel:Ljava/lang/String;
    goto :goto_13f

    #@15f
    .line 134
    .end local v8           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v12           #packageLabel:Ljava/lang/String;
    .end local v15           #pkg:Ljava/lang/String;
    :cond_15f
    const v17, 0x1020299

    #@162
    move-object/from16 v0, p0

    #@164
    move/from16 v1, v17

    #@166
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    #@169
    move-result-object v17

    #@16a
    check-cast v17, Landroid/widget/TextView;

    #@16c
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Landroid/accounts/GrantCredentialsPermissionActivity;->mAccount:Landroid/accounts/Account;

    #@170
    move-object/from16 v18, v0

    #@172
    move-object/from16 v0, v18

    #@174
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@176
    move-object/from16 v18, v0

    #@178
    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@17b
    .line 135
    const v17, 0x1020298

    #@17e
    move-object/from16 v0, p0

    #@180
    move/from16 v1, v17

    #@182
    invoke-virtual {v0, v1}, Landroid/accounts/GrantCredentialsPermissionActivity;->findViewById(I)Landroid/view/View;

    #@185
    move-result-object v17

    #@186
    check-cast v17, Landroid/widget/TextView;

    #@188
    move-object/from16 v0, v17

    #@18a
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@18d
    goto/16 :goto_4e
.end method

.method public final setAccountAuthenticatorResult(Landroid/os/Bundle;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 181
    iput-object p1, p0, Landroid/accounts/GrantCredentialsPermissionActivity;->mResultBundle:Landroid/os/Bundle;

    #@2
    .line 182
    return-void
.end method
