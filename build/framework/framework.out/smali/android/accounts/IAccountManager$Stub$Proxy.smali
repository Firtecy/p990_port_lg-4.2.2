.class Landroid/accounts/IAccountManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IAccountManager.java"

# interfaces
.implements Landroid/accounts/IAccountManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/IAccountManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 436
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 437
    iput-object p1, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 438
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 11
    .parameter "account"
    .parameter "password"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 592
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 593
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 596
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 597
    if-eqz p1, :cond_3e

    #@11
    .line 598
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 599
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 604
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 605
    if-eqz p3, :cond_4b

    #@1e
    .line 606
    const/4 v4, 0x1

    #@1f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 607
    const/4 v4, 0x0

    #@23
    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@26
    .line 612
    :goto_26
    iget-object v4, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/16 v5, 0x8

    #@2a
    const/4 v6, 0x0

    #@2b
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2e
    .line 613
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@31
    .line 614
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_34
    .catchall {:try_start_a .. :try_end_34} :catchall_43

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_50

    #@37
    .line 617
    .local v2, _result:Z
    :goto_37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 618
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 620
    return v2

    #@3e
    .line 602
    .end local v2           #_result:Z
    :cond_3e
    const/4 v4, 0x0

    #@3f
    :try_start_3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_43

    #@42
    goto :goto_19

    #@43
    .line 617
    :catchall_43
    move-exception v3

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 618
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v3

    #@4b
    .line 610
    :cond_4b
    const/4 v4, 0x0

    #@4c
    :try_start_4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_26

    #@50
    :cond_50
    move v2, v3

    #@51
    .line 614
    goto :goto_37
.end method

.method public addAcount(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLandroid/os/Bundle;)V
    .registers 12
    .parameter "response"
    .parameter "accountType"
    .parameter "authTokenType"
    .parameter "requiredFeatures"
    .parameter "expectActivityLaunch"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 832
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 833
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 835
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 836
    if-eqz p1, :cond_42

    #@11
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 837
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 838
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 839
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@21
    .line 840
    if-eqz p5, :cond_44

    #@23
    :goto_23
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 841
    if-eqz p6, :cond_46

    #@28
    .line 842
    const/4 v2, 0x1

    #@29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 843
    const/4 v2, 0x0

    #@2d
    invoke-virtual {p6, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@30
    .line 848
    :goto_30
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@32
    const/16 v3, 0x12

    #@34
    const/4 v4, 0x0

    #@35
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@38
    .line 849
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_3b
    .catchall {:try_start_a .. :try_end_3b} :catchall_4b

    #@3b
    .line 852
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 853
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 855
    return-void

    #@42
    .line 836
    :cond_42
    const/4 v4, 0x0

    #@43
    goto :goto_15

    #@44
    :cond_44
    move v2, v3

    #@45
    .line 840
    goto :goto_23

    #@46
    .line 846
    :cond_46
    const/4 v2, 0x0

    #@47
    :try_start_47
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_4b

    #@4a
    goto :goto_30

    #@4b
    .line 852
    :catchall_4b
    move-exception v2

    #@4c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4f
    .line 853
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@52
    throw v2
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 441
    iget-object v0, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public clearPassword(Landroid/accounts/Account;)V
    .registers 7
    .parameter "account"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 732
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 733
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 735
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 736
    if-eqz p1, :cond_29

    #@f
    .line 737
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 738
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 743
    :goto_17
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0xe

    #@1b
    const/4 v4, 0x0

    #@1c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 744
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2e

    #@22
    .line 747
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 748
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 750
    return-void

    #@29
    .line 741
    :cond_29
    const/4 v2, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_17

    #@2e
    .line 747
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 748
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public confirmCredentialsAsUser(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Landroid/os/Bundle;ZI)V
    .registers 11
    .parameter "response"
    .parameter "account"
    .parameter "options"
    .parameter "expectActivityLaunch"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 906
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 907
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 909
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 910
    if-eqz p1, :cond_46

    #@11
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 911
    if-eqz p2, :cond_48

    #@1a
    .line 912
    const/4 v4, 0x1

    #@1b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 913
    const/4 v4, 0x0

    #@1f
    invoke-virtual {p2, v0, v4}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@22
    .line 918
    :goto_22
    if-eqz p3, :cond_55

    #@24
    .line 919
    const/4 v4, 0x1

    #@25
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 920
    const/4 v4, 0x0

    #@29
    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c
    .line 925
    :goto_2c
    if-eqz p4, :cond_5a

    #@2e
    :goto_2e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 926
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 927
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@36
    const/16 v3, 0x15

    #@38
    const/4 v4, 0x0

    #@39
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@3c
    .line 928
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_3f
    .catchall {:try_start_a .. :try_end_3f} :catchall_4d

    #@3f
    .line 931
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 932
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 934
    return-void

    #@46
    .line 910
    :cond_46
    const/4 v4, 0x0

    #@47
    goto :goto_15

    #@48
    .line 916
    :cond_48
    const/4 v4, 0x0

    #@49
    :try_start_49
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4c
    .catchall {:try_start_49 .. :try_end_4c} :catchall_4d

    #@4c
    goto :goto_22

    #@4d
    .line 931
    :catchall_4d
    move-exception v2

    #@4e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@51
    .line 932
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@54
    throw v2

    #@55
    .line 923
    :cond_55
    const/4 v4, 0x0

    #@56
    :try_start_56
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_59
    .catchall {:try_start_56 .. :try_end_59} :catchall_4d

    #@59
    goto :goto_2c

    #@5a
    :cond_5a
    move v2, v3

    #@5b
    .line 925
    goto :goto_2e
.end method

.method public editProperties(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Z)V
    .registers 9
    .parameter "response"
    .parameter "accountType"
    .parameter "expectActivityLaunch"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 889
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 890
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 892
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.accounts.IAccountManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 893
    if-eqz p1, :cond_32

    #@10
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 894
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 895
    if-eqz p3, :cond_1d

    #@1c
    const/4 v2, 0x1

    #@1d
    :cond_1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 896
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v3, 0x14

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 897
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_34

    #@2b
    .line 900
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 901
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 903
    return-void

    #@32
    .line 893
    :cond_32
    const/4 v3, 0x0

    #@33
    goto :goto_14

    #@34
    .line 900
    :catchall_34
    move-exception v2

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 901
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v2
.end method

.method public getAccounts(Ljava/lang/String;)[Landroid/accounts/Account;
    .registers 8
    .parameter "accountType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 515
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 516
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 519
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 520
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 521
    iget-object v3, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x4

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 522
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 523
    sget-object v3, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, [Landroid/accounts/Account;
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_29

    #@22
    .line 526
    .local v2, _result:[Landroid/accounts/Account;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 527
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 529
    return-object v2

    #@29
    .line 526
    .end local v2           #_result:[Landroid/accounts/Account;
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 527
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public getAccountsAsUser(Ljava/lang/String;I)[Landroid/accounts/Account;
    .registers 9
    .parameter "accountType"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 533
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 534
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 537
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 538
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 539
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 540
    iget-object v3, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v4, 0x5

    #@16
    const/4 v5, 0x0

    #@17
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 541
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 542
    sget-object v3, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, [Landroid/accounts/Account;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2c

    #@25
    .line 545
    .local v2, _result:[Landroid/accounts/Account;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 546
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 548
    return-object v2

    #@2c
    .line 545
    .end local v2           #_result:[Landroid/accounts/Account;
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 546
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    throw v3
.end method

.method public getAccountsByFeatures(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 9
    .parameter "response"
    .parameter "accountType"
    .parameter "features"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 575
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 576
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 578
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 579
    if-eqz p1, :cond_2d

    #@f
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 580
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 581
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@1c
    .line 582
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v3, 0x7

    #@1f
    const/4 v4, 0x0

    #@20
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 583
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 586
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 587
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 589
    return-void

    #@2d
    .line 579
    :cond_2d
    const/4 v2, 0x0

    #@2e
    goto :goto_13

    #@2f
    .line 586
    :catchall_2f
    move-exception v2

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 587
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v2
.end method

.method public getAuthToken(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZZLandroid/os/Bundle;)V
    .registers 12
    .parameter "response"
    .parameter "account"
    .parameter "authTokenType"
    .parameter "notifyOnAuthFailure"
    .parameter "expectActivityLaunch"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 800
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 801
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 803
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 804
    if-eqz p1, :cond_4c

    #@11
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 805
    if-eqz p2, :cond_4e

    #@1a
    .line 806
    const/4 v4, 0x1

    #@1b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 807
    const/4 v4, 0x0

    #@1f
    invoke-virtual {p2, v0, v4}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@22
    .line 812
    :goto_22
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 813
    if-eqz p4, :cond_5b

    #@27
    move v4, v2

    #@28
    :goto_28
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 814
    if-eqz p5, :cond_5d

    #@2d
    :goto_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 815
    if-eqz p6, :cond_5f

    #@32
    .line 816
    const/4 v2, 0x1

    #@33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@36
    .line 817
    const/4 v2, 0x0

    #@37
    invoke-virtual {p6, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@3a
    .line 822
    :goto_3a
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@3c
    const/16 v3, 0x11

    #@3e
    const/4 v4, 0x0

    #@3f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@42
    .line 823
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_45
    .catchall {:try_start_a .. :try_end_45} :catchall_53

    #@45
    .line 826
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 827
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4b
    .line 829
    return-void

    #@4c
    .line 804
    :cond_4c
    const/4 v4, 0x0

    #@4d
    goto :goto_15

    #@4e
    .line 810
    :cond_4e
    const/4 v4, 0x0

    #@4f
    :try_start_4f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_53

    #@52
    goto :goto_22

    #@53
    .line 826
    :catchall_53
    move-exception v2

    #@54
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@57
    .line 827
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@5a
    throw v2

    #@5b
    :cond_5b
    move v4, v3

    #@5c
    .line 813
    goto :goto_28

    #@5d
    :cond_5d
    move v2, v3

    #@5e
    .line 814
    goto :goto_2d

    #@5f
    .line 820
    :cond_5f
    const/4 v2, 0x0

    #@60
    :try_start_60
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_63
    .catchall {:try_start_60 .. :try_end_63} :catchall_53

    #@63
    goto :goto_3a
.end method

.method public getAuthTokenLabel(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "response"
    .parameter "accountType"
    .parameter "authTokenType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 937
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 938
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 940
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 941
    if-eqz p1, :cond_2e

    #@f
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 942
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 943
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 944
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v3, 0x16

    #@20
    const/4 v4, 0x0

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 945
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_27
    .catchall {:try_start_8 .. :try_end_27} :catchall_30

    #@27
    .line 948
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 949
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 951
    return-void

    #@2e
    .line 941
    :cond_2e
    const/4 v2, 0x0

    #@2f
    goto :goto_13

    #@30
    .line 948
    :catchall_30
    move-exception v2

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 949
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    throw v2
.end method

.method public getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 498
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 499
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 502
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 503
    iget-object v3, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x3

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 504
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 505
    sget-object v3, Landroid/accounts/AuthenticatorDescription;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, [Landroid/accounts/AuthenticatorDescription;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_26

    #@1f
    .line 508
    .local v2, _result:[Landroid/accounts/AuthenticatorDescription;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 509
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 511
    return-object v2

    #@26
    .line 508
    .end local v2           #_result:[Landroid/accounts/AuthenticatorDescription;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 509
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 445
    const-string v0, "android.accounts.IAccountManager"

    #@2
    return-object v0
.end method

.method public getPassword(Landroid/accounts/Account;)Ljava/lang/String;
    .registers 8
    .parameter "account"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 449
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 450
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 453
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 454
    if-eqz p1, :cond_2c

    #@f
    .line 455
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 456
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 461
    :goto_17
    iget-object v3, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x1

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 462
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 463
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result-object v2

    #@25
    .line 466
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 467
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 469
    return-object v2

    #@2c
    .line 459
    .end local v2           #_result:Ljava/lang/String;
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 466
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 467
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method

.method public getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "account"
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 473
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 474
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 477
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 478
    if-eqz p1, :cond_2f

    #@f
    .line 479
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 480
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 485
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 486
    iget-object v3, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x2

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 487
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 488
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_27
    .catchall {:try_start_8 .. :try_end_27} :catchall_34

    #@27
    move-result-object v2

    #@28
    .line 491
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 492
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 494
    return-object v2

    #@2f
    .line 483
    .end local v2           #_result:Ljava/lang/String;
    :cond_2f
    const/4 v3, 0x0

    #@30
    :try_start_30
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_34

    #@33
    goto :goto_17

    #@34
    .line 491
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 492
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public hasFeatures(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;[Ljava/lang/String;)V
    .registers 9
    .parameter "response"
    .parameter "account"
    .parameter "features"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 552
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 553
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 555
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 556
    if-eqz p1, :cond_34

    #@f
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 557
    if-eqz p2, :cond_36

    #@18
    .line 558
    const/4 v2, 0x1

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 559
    const/4 v2, 0x0

    #@1d
    invoke-virtual {p2, v0, v2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 564
    :goto_20
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@23
    .line 565
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@25
    const/4 v3, 0x6

    #@26
    const/4 v4, 0x0

    #@27
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 566
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2d
    .catchall {:try_start_8 .. :try_end_2d} :catchall_3b

    #@2d
    .line 569
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 570
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 572
    return-void

    #@34
    .line 556
    :cond_34
    const/4 v2, 0x0

    #@35
    goto :goto_13

    #@36
    .line 562
    :cond_36
    const/4 v2, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_20

    #@3b
    .line 569
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 570
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2
.end method

.method public invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "accountType"
    .parameter "authToken"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 646
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 647
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 649
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 650
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 651
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 652
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0xa

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 653
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 656
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 657
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 659
    return-void

    #@25
    .line 656
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 657
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "account"
    .parameter "authTokenType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 662
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 663
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 666
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 667
    if-eqz p1, :cond_30

    #@f
    .line 668
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 669
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 674
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 675
    iget-object v3, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v4, 0xb

    #@1e
    const/4 v5, 0x0

    #@1f
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 676
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@25
    .line 677
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_35

    #@28
    move-result-object v2

    #@29
    .line 680
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 683
    return-object v2

    #@30
    .line 672
    .end local v2           #_result:Ljava/lang/String;
    :cond_30
    const/4 v3, 0x0

    #@31
    :try_start_31
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_35

    #@34
    goto :goto_17

    #@35
    .line 680
    :catchall_35
    move-exception v3

    #@36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v3
.end method

.method public removeAccount(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;)V
    .registers 8
    .parameter "response"
    .parameter "account"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 624
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 625
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 627
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 628
    if-eqz p1, :cond_32

    #@f
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 629
    if-eqz p2, :cond_34

    #@18
    .line 630
    const/4 v2, 0x1

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 631
    const/4 v2, 0x0

    #@1d
    invoke-virtual {p2, v0, v2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 636
    :goto_20
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v3, 0x9

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 637
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_39

    #@2b
    .line 640
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 641
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 643
    return-void

    #@32
    .line 628
    :cond_32
    const/4 v2, 0x0

    #@33
    goto :goto_13

    #@34
    .line 634
    :cond_34
    const/4 v2, 0x0

    #@35
    :try_start_35
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_20

    #@39
    .line 640
    :catchall_39
    move-exception v2

    #@3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 641
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    throw v2
.end method

.method public setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "account"
    .parameter "authTokenType"
    .parameter "authToken"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 687
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 688
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 690
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 691
    if-eqz p1, :cond_2f

    #@f
    .line 692
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 693
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 698
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 699
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 700
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0xc

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 701
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_34

    #@28
    .line 704
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 705
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 707
    return-void

    #@2f
    .line 696
    :cond_2f
    const/4 v2, 0x0

    #@30
    :try_start_30
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_34

    #@33
    goto :goto_17

    #@34
    .line 704
    :catchall_34
    move-exception v2

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 705
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v2
.end method

.method public setPassword(Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 8
    .parameter "account"
    .parameter "password"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 710
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 711
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 713
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 714
    if-eqz p1, :cond_2c

    #@f
    .line 715
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 716
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 721
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 722
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v3, 0xd

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 723
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_31

    #@25
    .line 726
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 727
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 729
    return-void

    #@2c
    .line 719
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 726
    :catchall_31
    move-exception v2

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 727
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v2
.end method

.method public setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "account"
    .parameter "key"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 753
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 754
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 756
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accounts.IAccountManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 757
    if-eqz p1, :cond_2f

    #@f
    .line 758
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 759
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 764
    :goto_17
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 765
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 766
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0xf

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 767
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_34

    #@28
    .line 770
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 771
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 773
    return-void

    #@2f
    .line 762
    :cond_2f
    const/4 v2, 0x0

    #@30
    :try_start_30
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_34

    #@33
    goto :goto_17

    #@34
    .line 770
    :catchall_34
    move-exception v2

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 771
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v2
.end method

.method public updateAppPermission(Landroid/accounts/Account;Ljava/lang/String;IZ)V
    .registers 10
    .parameter "account"
    .parameter "authTokenType"
    .parameter "uid"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 776
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 777
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 779
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 780
    if-eqz p1, :cond_36

    #@11
    .line 781
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 782
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 787
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 788
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 789
    if-eqz p4, :cond_43

    #@21
    :goto_21
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 790
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@26
    const/16 v3, 0x10

    #@28
    const/4 v4, 0x0

    #@29
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 791
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2f
    .catchall {:try_start_a .. :try_end_2f} :catchall_3b

    #@2f
    .line 794
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 795
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 797
    return-void

    #@36
    .line 785
    :cond_36
    const/4 v4, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_19

    #@3b
    .line 794
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 795
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2

    #@43
    :cond_43
    move v2, v3

    #@44
    .line 789
    goto :goto_21
.end method

.method public updateCredentials(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .registers 11
    .parameter "response"
    .parameter "account"
    .parameter "authTokenType"
    .parameter "expectActivityLaunch"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 858
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 859
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 861
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.accounts.IAccountManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 862
    if-eqz p1, :cond_46

    #@11
    invoke-interface {p1}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 863
    if-eqz p2, :cond_48

    #@1a
    .line 864
    const/4 v4, 0x1

    #@1b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 865
    const/4 v4, 0x0

    #@1f
    invoke-virtual {p2, v0, v4}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    #@22
    .line 870
    :goto_22
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    .line 871
    if-eqz p4, :cond_55

    #@27
    :goto_27
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 872
    if-eqz p5, :cond_57

    #@2c
    .line 873
    const/4 v2, 0x1

    #@2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 874
    const/4 v2, 0x0

    #@31
    invoke-virtual {p5, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@34
    .line 879
    :goto_34
    iget-object v2, p0, Landroid/accounts/IAccountManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@36
    const/16 v3, 0x13

    #@38
    const/4 v4, 0x0

    #@39
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@3c
    .line 880
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_3f
    .catchall {:try_start_a .. :try_end_3f} :catchall_4d

    #@3f
    .line 883
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 884
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 886
    return-void

    #@46
    .line 862
    :cond_46
    const/4 v4, 0x0

    #@47
    goto :goto_15

    #@48
    .line 868
    :cond_48
    const/4 v4, 0x0

    #@49
    :try_start_49
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4c
    .catchall {:try_start_49 .. :try_end_4c} :catchall_4d

    #@4c
    goto :goto_22

    #@4d
    .line 883
    :catchall_4d
    move-exception v2

    #@4e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@51
    .line 884
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@54
    throw v2

    #@55
    :cond_55
    move v2, v3

    #@56
    .line 871
    goto :goto_27

    #@57
    .line 877
    :cond_57
    const/4 v2, 0x0

    #@58
    :try_start_58
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_5b
    .catchall {:try_start_58 .. :try_end_5b} :catchall_4d

    #@5b
    goto :goto_34
.end method
