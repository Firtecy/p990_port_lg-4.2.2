.class abstract Landroid/accounts/AccountManager$Future2Task;
.super Landroid/accounts/AccountManager$BaseFutureTask;
.source "AccountManager.java"

# interfaces
.implements Landroid/accounts/AccountManagerFuture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/AccountManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "Future2Task"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/accounts/AccountManager$BaseFutureTask",
        "<TT;>;",
        "Landroid/accounts/AccountManagerFuture",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final mCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<TT;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/accounts/AccountManager;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;Landroid/os/Handler;Landroid/accounts/AccountManagerCallback;)V
    .registers 4
    .parameter
    .parameter "handler"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Landroid/accounts/AccountManagerCallback",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1547
    .local p0, this:Landroid/accounts/AccountManager$Future2Task;,"Landroid/accounts/AccountManager$Future2Task<TT;>;"
    .local p3, callback:Landroid/accounts/AccountManagerCallback;,"Landroid/accounts/AccountManagerCallback<TT;>;"
    iput-object p1, p0, Landroid/accounts/AccountManager$Future2Task;->this$0:Landroid/accounts/AccountManager;

    #@2
    .line 1548
    invoke-direct {p0, p1, p2}, Landroid/accounts/AccountManager$BaseFutureTask;-><init>(Landroid/accounts/AccountManager;Landroid/os/Handler;)V

    #@5
    .line 1549
    iput-object p3, p0, Landroid/accounts/AccountManager$Future2Task;->mCallback:Landroid/accounts/AccountManagerCallback;

    #@7
    .line 1550
    return-void
.end method

.method private internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 8
    .parameter "timeout"
    .parameter "unit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/accounts/AccountManager$Future2Task;,"Landroid/accounts/AccountManager$Future2Task<TT;>;"
    const/4 v4, 0x1

    #@1
    .line 1569
    invoke-virtual {p0}, Landroid/accounts/AccountManager$Future2Task;->isDone()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_c

    #@7
    .line 1570
    iget-object v2, p0, Landroid/accounts/AccountManager$Future2Task;->this$0:Landroid/accounts/AccountManager;

    #@9
    invoke-static {v2}, Landroid/accounts/AccountManager;->access$200(Landroid/accounts/AccountManager;)V

    #@c
    .line 1573
    :cond_c
    if-nez p1, :cond_16

    #@e
    .line 1574
    :try_start_e
    invoke-virtual {p0}, Landroid/accounts/AccountManager$Future2Task;->get()Ljava/lang/Object;
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_2b
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_11} :catch_69
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_e .. :try_end_11} :catch_63
    .catch Ljava/util/concurrent/CancellationException; {:try_start_e .. :try_end_11} :catch_55
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_e .. :try_end_11} :catch_1f

    #@11
    move-result-object v2

    #@12
    .line 1600
    :goto_12
    invoke-virtual {p0, v4}, Landroid/accounts/AccountManager$Future2Task;->cancel(Z)Z

    #@15
    .line 1576
    return-object v2

    #@16
    :cond_16
    :try_start_16
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_2b
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_19} :catch_69
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_16 .. :try_end_19} :catch_65
    .catch Ljava/util/concurrent/CancellationException; {:try_start_16 .. :try_end_19} :catch_5f
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_16 .. :try_end_19} :catch_1f

    #@19
    move-result-wide v2

    #@1a
    :try_start_1a
    invoke-virtual {p0, v2, v3, p2}, Landroid/accounts/AccountManager$Future2Task;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_1d
    .catchall {:try_start_1a .. :try_end_1d} :catchall_2b
    .catch Ljava/lang/InterruptedException; {:try_start_1a .. :try_end_1d} :catch_69
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1a .. :try_end_1d} :catch_67
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1a .. :try_end_1d} :catch_61
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1a .. :try_end_1d} :catch_1f

    #@1d
    move-result-object v2

    #@1e
    goto :goto_12

    #@1f
    .line 1584
    :catch_1f
    move-exception v1

    #@20
    .line 1585
    .local v1, e:Ljava/util/concurrent/ExecutionException;
    :try_start_20
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    #@23
    move-result-object v0

    #@24
    .line 1586
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v2, v0, Ljava/io/IOException;

    #@26
    if-eqz v2, :cond_30

    #@28
    .line 1587
    check-cast v0, Ljava/io/IOException;

    #@2a
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0
    :try_end_2b
    .catchall {:try_start_20 .. :try_end_2b} :catchall_2b

    #@2b
    .line 1600
    .end local v1           #e:Ljava/util/concurrent/ExecutionException;
    :catchall_2b
    move-exception v2

    #@2c
    invoke-virtual {p0, v4}, Landroid/accounts/AccountManager$Future2Task;->cancel(Z)Z

    #@2f
    throw v2

    #@30
    .line 1588
    .restart local v0       #cause:Ljava/lang/Throwable;
    .restart local v1       #e:Ljava/util/concurrent/ExecutionException;
    :cond_30
    :try_start_30
    instance-of v2, v0, Ljava/lang/UnsupportedOperationException;

    #@32
    if-eqz v2, :cond_3a

    #@34
    .line 1589
    new-instance v2, Landroid/accounts/AuthenticatorException;

    #@36
    invoke-direct {v2, v0}, Landroid/accounts/AuthenticatorException;-><init>(Ljava/lang/Throwable;)V

    #@39
    throw v2

    #@3a
    .line 1590
    :cond_3a
    instance-of v2, v0, Landroid/accounts/AuthenticatorException;

    #@3c
    if-eqz v2, :cond_41

    #@3e
    .line 1591
    check-cast v0, Landroid/accounts/AuthenticatorException;

    #@40
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@41
    .line 1592
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_41
    instance-of v2, v0, Ljava/lang/RuntimeException;

    #@43
    if-eqz v2, :cond_48

    #@45
    .line 1593
    check-cast v0, Ljava/lang/RuntimeException;

    #@47
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@48
    .line 1594
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_48
    instance-of v2, v0, Ljava/lang/Error;

    #@4a
    if-eqz v2, :cond_4f

    #@4c
    .line 1595
    check-cast v0, Ljava/lang/Error;

    #@4e
    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    #@4f
    .line 1597
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_4f
    new-instance v2, Ljava/lang/IllegalStateException;

    #@51
    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    #@54
    throw v2
    :try_end_55
    .catchall {:try_start_30 .. :try_end_55} :catchall_2b

    #@55
    .line 1582
    .end local v0           #cause:Ljava/lang/Throwable;
    .end local v1           #e:Ljava/util/concurrent/ExecutionException;
    :catch_55
    move-exception v2

    #@56
    .line 1600
    :goto_56
    invoke-virtual {p0, v4}, Landroid/accounts/AccountManager$Future2Task;->cancel(Z)Z

    #@59
    .line 1602
    new-instance v2, Landroid/accounts/OperationCanceledException;

    #@5b
    invoke-direct {v2}, Landroid/accounts/OperationCanceledException;-><init>()V

    #@5e
    throw v2

    #@5f
    .line 1582
    :catch_5f
    move-exception v2

    #@60
    goto :goto_56

    #@61
    :catch_61
    move-exception v2

    #@62
    goto :goto_56

    #@63
    .line 1580
    :catch_63
    move-exception v2

    #@64
    goto :goto_56

    #@65
    :catch_65
    move-exception v2

    #@66
    goto :goto_56

    #@67
    :catch_67
    move-exception v2

    #@68
    goto :goto_56

    #@69
    .line 1578
    :catch_69
    move-exception v2

    #@6a
    goto :goto_56
.end method


# virtual methods
.method protected done()V
    .registers 2

    #@0
    .prologue
    .line 1553
    .local p0, this:Landroid/accounts/AccountManager$Future2Task;,"Landroid/accounts/AccountManager$Future2Task<TT;>;"
    iget-object v0, p0, Landroid/accounts/AccountManager$Future2Task;->mCallback:Landroid/accounts/AccountManagerCallback;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1554
    new-instance v0, Landroid/accounts/AccountManager$Future2Task$1;

    #@6
    invoke-direct {v0, p0}, Landroid/accounts/AccountManager$Future2Task$1;-><init>(Landroid/accounts/AccountManager$Future2Task;)V

    #@9
    invoke-virtual {p0, v0}, Landroid/accounts/AccountManager$Future2Task;->postRunnableToHandler(Ljava/lang/Runnable;)V

    #@c
    .line 1560
    :cond_c
    return-void
.end method

.method public getResult()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/accounts/AccountManager$Future2Task;,"Landroid/accounts/AccountManager$Future2Task<TT;>;"
    const/4 v0, 0x0

    #@1
    .line 1607
    invoke-direct {p0, v0, v0}, Landroid/accounts/AccountManager$Future2Task;->internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5
    .parameter "timeout"
    .parameter "unit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;
        }
    .end annotation

    #@0
    .prologue
    .line 1612
    .local p0, this:Landroid/accounts/AccountManager$Future2Task;,"Landroid/accounts/AccountManager$Future2Task<TT;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p3}, Landroid/accounts/AccountManager$Future2Task;->internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public start()Landroid/accounts/AccountManager$Future2Task;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/accounts/AccountManager$Future2Task",
            "<TT;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 1563
    .local p0, this:Landroid/accounts/AccountManager$Future2Task;,"Landroid/accounts/AccountManager$Future2Task<TT;>;"
    invoke-virtual {p0}, Landroid/accounts/AccountManager$Future2Task;->startTask()V

    #@3
    .line 1564
    return-object p0
.end method
