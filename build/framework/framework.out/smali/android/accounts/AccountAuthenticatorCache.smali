.class Landroid/accounts/AccountAuthenticatorCache;
.super Landroid/content/pm/RegisteredServicesCache;
.source "AccountAuthenticatorCache.java"

# interfaces
.implements Landroid/accounts/IAccountAuthenticatorCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/AccountAuthenticatorCache$1;,
        Landroid/accounts/AccountAuthenticatorCache$MySerializer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/pm/RegisteredServicesCache",
        "<",
        "Landroid/accounts/AuthenticatorDescription;",
        ">;",
        "Landroid/accounts/IAccountAuthenticatorCache;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Account"

.field private static final sSerializer:Landroid/accounts/AccountAuthenticatorCache$MySerializer;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 45
    new-instance v0, Landroid/accounts/AccountAuthenticatorCache$MySerializer;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/accounts/AccountAuthenticatorCache$MySerializer;-><init>(Landroid/accounts/AccountAuthenticatorCache$1;)V

    #@6
    sput-object v0, Landroid/accounts/AccountAuthenticatorCache;->sSerializer:Landroid/accounts/AccountAuthenticatorCache$MySerializer;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 48
    const-string v2, "android.accounts.AccountAuthenticator"

    #@2
    const-string v3, "android.accounts.AccountAuthenticator"

    #@4
    const-string v4, "account-authenticator"

    #@6
    sget-object v5, Landroid/accounts/AccountAuthenticatorCache;->sSerializer:Landroid/accounts/AccountAuthenticatorCache$MySerializer;

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    invoke-direct/range {v0 .. v5}, Landroid/content/pm/RegisteredServicesCache;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/XmlSerializerAndParser;)V

    #@d
    .line 51
    return-void
.end method


# virtual methods
.method public bridge synthetic getServiceInfo(Landroid/accounts/AuthenticatorDescription;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Landroid/content/pm/RegisteredServicesCache;->getServiceInfo(Ljava/lang/Object;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/accounts/AuthenticatorDescription;
    .registers 13
    .parameter "res"
    .parameter "packageName"
    .parameter "attrs"

    #@0
    .prologue
    .line 55
    sget-object v0, Lcom/android/internal/R$styleable;->AccountAuthenticator:[I

    #@2
    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v8

    #@6
    .line 58
    .local v8, sa:Landroid/content/res/TypedArray;
    const/4 v0, 0x2

    #@7
    :try_start_7
    invoke-virtual {v8, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 60
    .local v1, accountType:Ljava/lang/String;
    const/4 v0, 0x0

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v8, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@10
    move-result v3

    #@11
    .line 62
    .local v3, labelId:I
    const/4 v0, 0x1

    #@12
    const/4 v2, 0x0

    #@13
    invoke-virtual {v8, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@16
    move-result v4

    #@17
    .line 64
    .local v4, iconId:I
    const/4 v0, 0x3

    #@18
    const/4 v2, 0x0

    #@19
    invoke-virtual {v8, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1c
    move-result v5

    #@1d
    .line 66
    .local v5, smallIconId:I
    const/4 v0, 0x4

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-virtual {v8, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@22
    move-result v6

    #@23
    .line 68
    .local v6, prefId:I
    const/4 v0, 0x5

    #@24
    const/4 v2, 0x0

    #@25
    invoke-virtual {v8, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@28
    move-result v7

    #@29
    .line 70
    .local v7, customTokens:Z
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2c
    .catchall {:try_start_7 .. :try_end_2c} :catchall_3b

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_34

    #@2f
    .line 71
    const/4 v0, 0x0

    #@30
    .line 76
    :goto_30
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@33
    .line 73
    return-object v0

    #@34
    :cond_34
    :try_start_34
    new-instance v0, Landroid/accounts/AuthenticatorDescription;

    #@36
    move-object v2, p2

    #@37
    invoke-direct/range {v0 .. v7}, Landroid/accounts/AuthenticatorDescription;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIZ)V
    :try_end_3a
    .catchall {:try_start_34 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_30

    #@3b
    .line 76
    .end local v1           #accountType:Ljava/lang/String;
    .end local v3           #labelId:I
    .end local v4           #iconId:I
    .end local v5           #smallIconId:I
    .end local v6           #prefId:I
    .end local v7           #customTokens:Z
    :catchall_3b
    move-exception v0

    #@3c
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    #@3f
    throw v0
.end method

.method public bridge synthetic parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 41
    invoke-virtual {p0, p1, p2, p3}, Landroid/accounts/AccountAuthenticatorCache;->parseServiceAttributes(Landroid/content/res/Resources;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/accounts/AuthenticatorDescription;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
