.class public Landroid/accounts/ChooseAccountActivity;
.super Landroid/app/Activity;
.source "ChooseAccountActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/ChooseAccountActivity$AccountArrayAdapter;,
        Landroid/accounts/ChooseAccountActivity$ViewHolder;,
        Landroid/accounts/ChooseAccountActivity$AccountInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountManager"


# instance fields
.field private mAccountManagerResponse:Landroid/accounts/AccountManagerResponse;

.field private mAccounts:[Landroid/os/Parcelable;

.field private mResult:Landroid/os/Bundle;

.field private mTypeToAuthDescription:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/accounts/AuthenticatorDescription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@4
    .line 45
    iput-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@6
    .line 46
    iput-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mAccountManagerResponse:Landroid/accounts/AccountManagerResponse;

    #@8
    .line 49
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    iput-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mTypeToAuthDescription:Ljava/util/HashMap;

    #@f
    .line 158
    return-void
.end method

.method private getAuthDescriptions()V
    .registers 7

    #@0
    .prologue
    .line 95
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@3
    move-result-object v4

    #@4
    invoke-virtual {v4}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    #@7
    move-result-object v0

    #@8
    .local v0, arr$:[Landroid/accounts/AuthenticatorDescription;
    array-length v3, v0

    #@9
    .local v3, len$:I
    const/4 v2, 0x0

    #@a
    .local v2, i$:I
    :goto_a
    if-ge v2, v3, :cond_18

    #@c
    aget-object v1, v0, v2

    #@e
    .line 96
    .local v1, desc:Landroid/accounts/AuthenticatorDescription;
    iget-object v4, p0, Landroid/accounts/ChooseAccountActivity;->mTypeToAuthDescription:Ljava/util/HashMap;

    #@10
    iget-object v5, v1, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@12
    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    .line 95
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_a

    #@18
    .line 98
    .end local v1           #desc:Landroid/accounts/AuthenticatorDescription;
    :cond_18
    return-void
.end method

.method private getDrawableForType(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 9
    .parameter "accountType"

    #@0
    .prologue
    const/4 v6, 0x5

    #@1
    .line 101
    const/4 v3, 0x0

    #@2
    .line 102
    .local v3, icon:Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Landroid/accounts/ChooseAccountActivity;->mTypeToAuthDescription:Ljava/util/HashMap;

    #@4
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_23

    #@a
    .line 104
    :try_start_a
    iget-object v4, p0, Landroid/accounts/ChooseAccountActivity;->mTypeToAuthDescription:Ljava/util/HashMap;

    #@c
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/accounts/AuthenticatorDescription;

    #@12
    .line 105
    .local v1, desc:Landroid/accounts/AuthenticatorDescription;
    iget-object v4, v1, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    #@14
    const/4 v5, 0x0

    #@15
    invoke-virtual {p0, v4, v5}, Landroid/accounts/ChooseAccountActivity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@18
    move-result-object v0

    #@19
    .line 106
    .local v0, authContext:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1c
    move-result-object v4

    #@1d
    iget v5, v1, Landroid/accounts/AuthenticatorDescription;->iconId:I

    #@1f
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_22
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_22} :catch_24
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_a .. :try_end_22} :catch_46

    #@22
    move-result-object v3

    #@23
    .line 119
    .end local v0           #authContext:Landroid/content/Context;
    .end local v1           #desc:Landroid/accounts/AuthenticatorDescription;
    :cond_23
    :goto_23
    return-object v3

    #@24
    .line 107
    :catch_24
    move-exception v2

    #@25
    .line 109
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "AccountManager"

    #@27
    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_23

    #@2d
    .line 110
    const-string v4, "AccountManager"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "No icon name for account type "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_23

    #@46
    .line 112
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_46
    move-exception v2

    #@47
    .line 114
    .local v2, e:Landroid/content/res/Resources$NotFoundException;
    const-string v4, "AccountManager"

    #@49
    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@4c
    move-result v4

    #@4d
    if-eqz v4, :cond_23

    #@4f
    .line 115
    const-string v4, "AccountManager"

    #@51
    new-instance v5, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v6, "No icon resource for account type "

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_23
.end method


# virtual methods
.method public finish()V
    .registers 4

    #@0
    .prologue
    .line 133
    iget-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mAccountManagerResponse:Landroid/accounts/AccountManagerResponse;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 134
    iget-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mResult:Landroid/os/Bundle;

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 135
    iget-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mAccountManagerResponse:Landroid/accounts/AccountManagerResponse;

    #@a
    iget-object v1, p0, Landroid/accounts/ChooseAccountActivity;->mResult:Landroid/os/Bundle;

    #@c
    invoke-virtual {v0, v1}, Landroid/accounts/AccountManagerResponse;->onResult(Landroid/os/Bundle;)V

    #@f
    .line 140
    :cond_f
    :goto_f
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    #@12
    .line 141
    return-void

    #@13
    .line 137
    :cond_13
    iget-object v0, p0, Landroid/accounts/ChooseAccountActivity;->mAccountManagerResponse:Landroid/accounts/AccountManagerResponse;

    #@15
    const/4 v1, 0x4

    #@16
    const-string v2, "canceled"

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManagerResponse;->onError(ILjava/lang/String;)V

    #@1b
    goto :goto_f
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 55
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@6
    if-eqz v3, :cond_e

    #@8
    .line 56
    const v3, 0x20a01c5

    #@b
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseAccountActivity;->setTheme(I)V

    #@e
    .line 59
    :cond_e
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountActivity;->getIntent()Landroid/content/Intent;

    #@11
    move-result-object v3

    #@12
    const-string v4, "accounts"

    #@14
    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    #@17
    move-result-object v3

    #@18
    iput-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@1a
    .line 60
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountActivity;->getIntent()Landroid/content/Intent;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, "accountManagerResponse"

    #@20
    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@23
    move-result-object v3

    #@24
    check-cast v3, Landroid/accounts/AccountManagerResponse;

    #@26
    iput-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccountManagerResponse:Landroid/accounts/AccountManagerResponse;

    #@28
    .line 64
    iget-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@2a
    if-nez v3, :cond_34

    #@2c
    .line 65
    const/4 v3, 0x0

    #@2d
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseAccountActivity;->setResult(I)V

    #@30
    .line 66
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountActivity;->finish()V

    #@33
    .line 92
    :goto_33
    return-void

    #@34
    .line 70
    :cond_34
    invoke-direct {p0}, Landroid/accounts/ChooseAccountActivity;->getAuthDescriptions()V

    #@37
    .line 72
    iget-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@39
    array-length v3, v3

    #@3a
    new-array v2, v3, [Landroid/accounts/ChooseAccountActivity$AccountInfo;

    #@3c
    .line 73
    .local v2, mAccountInfos:[Landroid/accounts/ChooseAccountActivity$AccountInfo;
    const/4 v0, 0x0

    #@3d
    .local v0, i:I
    :goto_3d
    iget-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@3f
    array-length v3, v3

    #@40
    if-ge v0, v3, :cond_60

    #@42
    .line 74
    new-instance v4, Landroid/accounts/ChooseAccountActivity$AccountInfo;

    #@44
    iget-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@46
    aget-object v3, v3, v0

    #@48
    check-cast v3, Landroid/accounts/Account;

    #@4a
    iget-object v5, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@4c
    iget-object v3, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@4e
    aget-object v3, v3, v0

    #@50
    check-cast v3, Landroid/accounts/Account;

    #@52
    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@54
    invoke-direct {p0, v3}, Landroid/accounts/ChooseAccountActivity;->getDrawableForType(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@57
    move-result-object v3

    #@58
    invoke-direct {v4, v5, v3}, Landroid/accounts/ChooseAccountActivity$AccountInfo;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    #@5b
    aput-object v4, v2, v0

    #@5d
    .line 73
    add-int/lit8 v0, v0, 0x1

    #@5f
    goto :goto_3d

    #@60
    .line 78
    :cond_60
    const v3, 0x1090033

    #@63
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseAccountActivity;->setContentView(I)V

    #@66
    .line 81
    const v3, 0x102000a

    #@69
    invoke-virtual {p0, v3}, Landroid/accounts/ChooseAccountActivity;->findViewById(I)Landroid/view/View;

    #@6c
    move-result-object v1

    #@6d
    check-cast v1, Landroid/widget/ListView;

    #@6f
    .line 83
    .local v1, list:Landroid/widget/ListView;
    new-instance v3, Landroid/accounts/ChooseAccountActivity$AccountArrayAdapter;

    #@71
    const v4, 0x1090003

    #@74
    invoke-direct {v3, p0, v4, v2}, Landroid/accounts/ChooseAccountActivity$AccountArrayAdapter;-><init>(Landroid/content/Context;I[Landroid/accounts/ChooseAccountActivity$AccountInfo;)V

    #@77
    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@7a
    .line 85
    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@7d
    .line 86
    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    #@80
    .line 87
    new-instance v3, Landroid/accounts/ChooseAccountActivity$1;

    #@82
    invoke-direct {v3, p0}, Landroid/accounts/ChooseAccountActivity$1;-><init>(Landroid/accounts/ChooseAccountActivity;)V

    #@85
    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@88
    goto :goto_33
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 11
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 123
    iget-object v2, p0, Landroid/accounts/ChooseAccountActivity;->mAccounts:[Landroid/os/Parcelable;

    #@2
    aget-object v0, v2, p3

    #@4
    check-cast v0, Landroid/accounts/Account;

    #@6
    .line 124
    .local v0, account:Landroid/accounts/Account;
    const-string v2, "AccountManager"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v4, "selected account "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 125
    new-instance v1, Landroid/os/Bundle;

    #@21
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@24
    .line 126
    .local v1, bundle:Landroid/os/Bundle;
    const-string v2, "authAccount"

    #@26
    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@28
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 127
    const-string v2, "accountType"

    #@2d
    iget-object v3, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@2f
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 128
    iput-object v1, p0, Landroid/accounts/ChooseAccountActivity;->mResult:Landroid/os/Bundle;

    #@34
    .line 129
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountActivity;->finish()V

    #@37
    .line 130
    return-void
.end method
