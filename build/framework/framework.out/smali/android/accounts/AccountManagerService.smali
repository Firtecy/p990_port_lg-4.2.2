.class public Landroid/accounts/AccountManagerService;
.super Landroid/accounts/IAccountManager$Stub;
.source "AccountManagerService.java"

# interfaces
.implements Landroid/content/pm/RegisteredServicesCacheListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/AccountManagerService$DatabaseHelper;,
        Landroid/accounts/AccountManagerService$MessageHandler;,
        Landroid/accounts/AccountManagerService$Session;,
        Landroid/accounts/AccountManagerService$GetAccountsByTypeAndFeatureSession;,
        Landroid/accounts/AccountManagerService$RemoveAccountSession;,
        Landroid/accounts/AccountManagerService$TestFeaturesSession;,
        Landroid/accounts/AccountManagerService$UserAccounts;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/accounts/IAccountManager$Stub;",
        "Landroid/content/pm/RegisteredServicesCacheListener",
        "<",
        "Landroid/accounts/AuthenticatorDescription;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACCOUNTS_CHANGED_INTENT:Landroid/content/Intent; = null

.field private static final ACCOUNTS_ID:Ljava/lang/String; = "_id"

.field private static final ACCOUNTS_NAME:Ljava/lang/String; = "name"

.field private static final ACCOUNTS_PASSWORD:Ljava/lang/String; = "password"

.field private static final ACCOUNTS_TYPE:Ljava/lang/String; = "type"

.field private static final ACCOUNTS_TYPE_COUNT:Ljava/lang/String; = "count(type)"

.field private static final ACCOUNT_TYPE_COUNT_PROJECTION:[Ljava/lang/String; = null

.field private static final AUTHTOKENS_ACCOUNTS_ID:Ljava/lang/String; = "accounts_id"

.field private static final AUTHTOKENS_AUTHTOKEN:Ljava/lang/String; = "authtoken"

.field private static final AUTHTOKENS_ID:Ljava/lang/String; = "_id"

.field private static final AUTHTOKENS_TYPE:Ljava/lang/String; = "type"

.field private static final COLUMNS_AUTHTOKENS_TYPE_AND_AUTHTOKEN:[Ljava/lang/String; = null

.field private static final COLUMNS_EXTRAS_KEY_AND_VALUE:[Ljava/lang/String; = null

.field private static final COUNT_OF_MATCHING_GRANTS:Ljava/lang/String; = "SELECT COUNT(*) FROM grants, accounts WHERE accounts_id=_id AND uid=? AND auth_token_type=? AND name=? AND type=?"

.field private static final DATABASE_NAME:Ljava/lang/String; = "accounts.db"

.field private static final DATABASE_VERSION:I = 0x4

.field private static final EMPTY_ACCOUNT_ARRAY:[Landroid/accounts/Account; = null

.field private static final EXTRAS_ACCOUNTS_ID:Ljava/lang/String; = "accounts_id"

.field private static final EXTRAS_ID:Ljava/lang/String; = "_id"

.field private static final EXTRAS_KEY:Ljava/lang/String; = "key"

.field private static final EXTRAS_VALUE:Ljava/lang/String; = "value"

.field private static final GRANTS_ACCOUNTS_ID:Ljava/lang/String; = "accounts_id"

.field private static final GRANTS_AUTH_TOKEN_TYPE:Ljava/lang/String; = "auth_token_type"

.field private static final GRANTS_GRANTEE_UID:Ljava/lang/String; = "uid"

.field private static final MESSAGE_TIMED_OUT:I = 0x3

.field private static final META_KEY:Ljava/lang/String; = "key"

.field private static final META_VALUE:Ljava/lang/String; = "value"

.field private static final SELECTION_AUTHTOKENS_BY_ACCOUNT:Ljava/lang/String; = "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)"

.field private static final SELECTION_USERDATA_BY_ACCOUNT:Ljava/lang/String; = "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)"

.field private static final TABLE_ACCOUNTS:Ljava/lang/String; = "accounts"

.field private static final TABLE_AUTHTOKENS:Ljava/lang/String; = "authtokens"

.field private static final TABLE_EXTRAS:Ljava/lang/String; = "extras"

.field private static final TABLE_GRANTS:Ljava/lang/String; = "grants"

.field private static final TABLE_META:Ljava/lang/String; = "meta"

.field private static final TAG:Ljava/lang/String; = "AccountManagerService"

.field private static final TIMEOUT_DELAY_MS:I = 0xea60

.field private static sThis:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Landroid/accounts/AccountManagerService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

.field private final mContext:Landroid/content/Context;

.field private final mMessageHandler:Landroid/accounts/AccountManagerService$MessageHandler;

.field private mMessageThread:Landroid/os/HandlerThread;

.field private final mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mSessions:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/accounts/AccountManagerService$Session;",
            ">;"
        }
    .end annotation
.end field

.field private mUserManager:Landroid/os/UserManager;

.field private final mUsers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/accounts/AccountManagerService$UserAccounts;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 138
    new-array v0, v4, [Ljava/lang/String;

    #@5
    const-string/jumbo v1, "type"

    #@8
    aput-object v1, v0, v2

    #@a
    const-string v1, "count(type)"

    #@c
    aput-object v1, v0, v3

    #@e
    sput-object v0, Landroid/accounts/AccountManagerService;->ACCOUNT_TYPE_COUNT_PROJECTION:[Ljava/lang/String;

    #@10
    .line 152
    new-array v0, v4, [Ljava/lang/String;

    #@12
    const-string/jumbo v1, "type"

    #@15
    aput-object v1, v0, v2

    #@17
    const-string v1, "authtoken"

    #@19
    aput-object v1, v0, v3

    #@1b
    sput-object v0, Landroid/accounts/AccountManagerService;->COLUMNS_AUTHTOKENS_TYPE_AND_AUTHTOKEN:[Ljava/lang/String;

    #@1d
    .line 157
    new-array v0, v4, [Ljava/lang/String;

    #@1f
    const-string/jumbo v1, "key"

    #@22
    aput-object v1, v0, v2

    #@24
    const-string/jumbo v1, "value"

    #@27
    aput-object v1, v0, v3

    #@29
    sput-object v0, Landroid/accounts/AccountManagerService;->COLUMNS_EXTRAS_KEY_AND_VALUE:[Ljava/lang/String;

    #@2b
    .line 191
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    #@2d
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    #@30
    sput-object v0, Landroid/accounts/AccountManagerService;->sThis:Ljava/util/concurrent/atomic/AtomicReference;

    #@32
    .line 193
    new-array v0, v2, [Landroid/accounts/Account;

    #@34
    sput-object v0, Landroid/accounts/AccountManagerService;->EMPTY_ACCOUNT_ARRAY:[Landroid/accounts/Account;

    #@36
    .line 196
    new-instance v0, Landroid/content/Intent;

    #@38
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    #@3a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3d
    sput-object v0, Landroid/accounts/AccountManagerService;->ACCOUNTS_CHANGED_INTENT:Landroid/content/Intent;

    #@3f
    .line 197
    sget-object v0, Landroid/accounts/AccountManagerService;->ACCOUNTS_CHANGED_INTENT:Landroid/content/Intent;

    #@41
    const/high16 v1, 0x800

    #@43
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@46
    .line 198
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 212
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Landroid/accounts/AccountAuthenticatorCache;

    #@6
    invoke-direct {v1, p1}, Landroid/accounts/AccountAuthenticatorCache;-><init>(Landroid/content/Context;)V

    #@9
    invoke-direct {p0, p1, v0, v1}, Landroid/accounts/AccountManagerService;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/accounts/IAccountAuthenticatorCache;)V

    #@c
    .line 213
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/accounts/IAccountAuthenticatorCache;)V
    .registers 8
    .parameter "context"
    .parameter "packageManager"
    .parameter "authenticatorCache"

    #@0
    .prologue
    .line 216
    invoke-direct {p0}, Landroid/accounts/IAccountManager$Stub;-><init>()V

    #@3
    .line 159
    new-instance v2, Ljava/util/LinkedHashMap;

    #@5
    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    #@8
    iput-object v2, p0, Landroid/accounts/AccountManagerService;->mSessions:Ljava/util/LinkedHashMap;

    #@a
    .line 160
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    #@c
    const/4 v3, 0x1

    #@d
    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@10
    iput-object v2, p0, Landroid/accounts/AccountManagerService;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

    #@12
    .line 189
    new-instance v2, Landroid/util/SparseArray;

    #@14
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@17
    iput-object v2, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@19
    .line 217
    iput-object p1, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@1b
    .line 218
    iput-object p2, p0, Landroid/accounts/AccountManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@1d
    .line 220
    new-instance v2, Landroid/os/HandlerThread;

    #@1f
    const-string v3, "AccountManagerService"

    #@21
    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@24
    iput-object v2, p0, Landroid/accounts/AccountManagerService;->mMessageThread:Landroid/os/HandlerThread;

    #@26
    .line 221
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mMessageThread:Landroid/os/HandlerThread;

    #@28
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    #@2b
    .line 222
    new-instance v2, Landroid/accounts/AccountManagerService$MessageHandler;

    #@2d
    iget-object v3, p0, Landroid/accounts/AccountManagerService;->mMessageThread:Landroid/os/HandlerThread;

    #@2f
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@32
    move-result-object v3

    #@33
    invoke-direct {v2, p0, v3}, Landroid/accounts/AccountManagerService$MessageHandler;-><init>(Landroid/accounts/AccountManagerService;Landroid/os/Looper;)V

    #@36
    iput-object v2, p0, Landroid/accounts/AccountManagerService;->mMessageHandler:Landroid/accounts/AccountManagerService$MessageHandler;

    #@38
    .line 224
    iput-object p3, p0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@3a
    .line 225
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@3c
    const/4 v3, 0x0

    #@3d
    invoke-interface {v2, p0, v3}, Landroid/accounts/IAccountAuthenticatorCache;->setListener(Landroid/content/pm/RegisteredServicesCacheListener;Landroid/os/Handler;)V

    #@40
    .line 227
    sget-object v2, Landroid/accounts/AccountManagerService;->sThis:Ljava/util/concurrent/atomic/AtomicReference;

    #@42
    invoke-virtual {v2, p0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@45
    .line 229
    new-instance v0, Landroid/content/IntentFilter;

    #@47
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@4a
    .line 230
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    #@4c
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4f
    .line 231
    const-string/jumbo v2, "package"

    #@52
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@55
    .line 232
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@57
    new-instance v3, Landroid/accounts/AccountManagerService$1;

    #@59
    invoke-direct {v3, p0}, Landroid/accounts/AccountManagerService$1;-><init>(Landroid/accounts/AccountManagerService;)V

    #@5c
    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@5f
    .line 239
    new-instance v1, Landroid/content/IntentFilter;

    #@61
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@64
    .line 240
    .local v1, userFilter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.USER_REMOVED"

    #@66
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@69
    .line 241
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@6b
    new-instance v3, Landroid/accounts/AccountManagerService$2;

    #@6d
    invoke-direct {v3, p0}, Landroid/accounts/AccountManagerService$2;-><init>(Landroid/accounts/AccountManagerService;)V

    #@70
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@73
    .line 247
    return-void
.end method

.method static synthetic access$000(Landroid/accounts/AccountManagerService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->purgeOldGrantsAll()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/accounts/AccountManagerService;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->onUserRemoved(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Landroid/accounts/AccountManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/accounts/AccountManagerService;Landroid/accounts/Account;ILandroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 88
    invoke-direct/range {p0 .. p5}, Landroid/accounts/AccountManagerService;->newGrantCredentialsPermissionIntent(Landroid/accounts/Account;ILandroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1200(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/accounts/AccountManagerService;->saveAuthTokenToDatabase(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1300(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/CharSequence;Landroid/content/Intent;I)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 88
    invoke-direct/range {p0 .. p5}, Landroid/accounts/AccountManagerService;->doNotification(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/CharSequence;Landroid/content/Intent;I)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/accounts/AccountManagerService;)Ljava/util/LinkedHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mSessions:Ljava/util/LinkedHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/accounts/AccountManagerService;)Landroid/accounts/AccountManagerService$MessageHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mMessageHandler:Landroid/accounts/AccountManagerService$MessageHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Landroid/accounts/AccountManagerService;->getSigninRequiredNotificationId(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1800(Landroid/accounts/AccountManagerService;)Landroid/accounts/IAccountAuthenticatorCache;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(I)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-static {p0}, Landroid/accounts/AccountManagerService;->getDatabaseName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$900(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Landroid/accounts/AccountManagerService;->removeAccountInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V

    #@3
    return-void
.end method

.method private addAccountInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 21
    .parameter "accounts"
    .parameter "account"
    .parameter "password"
    .parameter "extras"

    #@0
    .prologue
    .line 532
    if-nez p2, :cond_4

    #@2
    .line 533
    const/4 v1, 0x0

    #@3
    .line 574
    :goto_3
    return v1

    #@4
    .line 535
    :cond_4
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@7
    move-result-object v11

    #@8
    monitor-enter v11

    #@9
    .line 536
    :try_start_9
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@10
    move-result-object v2

    #@11
    .line 537
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_14
    .catchall {:try_start_9 .. :try_end_14} :catchall_58

    #@14
    .line 539
    :try_start_14
    const-string/jumbo v1, "select count(*) from accounts WHERE name=? AND type=?"

    #@17
    const/4 v12, 0x2

    #@18
    new-array v12, v12, [Ljava/lang/String;

    #@1a
    const/4 v13, 0x0

    #@1b
    move-object/from16 v0, p2

    #@1d
    iget-object v14, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@1f
    aput-object v14, v12, v13

    #@21
    const/4 v13, 0x1

    #@22
    move-object/from16 v0, p2

    #@24
    iget-object v14, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@26
    aput-object v14, v12, v13

    #@28
    invoke-static {v2, v1, v12}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    #@2b
    move-result-wide v8

    #@2c
    .line 543
    .local v8, numMatches:J
    const-wide/16 v12, 0x0

    #@2e
    cmp-long v1, v8, v12

    #@30
    if-lez v1, :cond_5b

    #@32
    .line 544
    const-string v1, "AccountManagerService"

    #@34
    new-instance v12, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v13, "insertAccountIntoDatabase: "

    #@3b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v12

    #@3f
    move-object/from16 v0, p2

    #@41
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v12

    #@45
    const-string v13, ", skipping since the account already exists"

    #@47
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v12

    #@4b
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v12

    #@4f
    invoke-static {v1, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_52
    .catchall {:try_start_14 .. :try_end_52} :catchall_11b

    #@52
    .line 546
    const/4 v1, 0x0

    #@53
    .line 571
    :try_start_53
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@56
    .line 546
    monitor-exit v11

    #@57
    goto :goto_3

    #@58
    .line 575
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v8           #numMatches:J
    :catchall_58
    move-exception v1

    #@59
    monitor-exit v11
    :try_end_5a
    .catchall {:try_start_53 .. :try_end_5a} :catchall_58

    #@5a
    throw v1

    #@5b
    .line 548
    .restart local v2       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8       #numMatches:J
    :cond_5b
    :try_start_5b
    new-instance v10, Landroid/content/ContentValues;

    #@5d
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    #@60
    .line 549
    .local v10, values:Landroid/content/ContentValues;
    const-string/jumbo v1, "name"

    #@63
    move-object/from16 v0, p2

    #@65
    iget-object v12, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@67
    invoke-virtual {v10, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 550
    const-string/jumbo v1, "type"

    #@6d
    move-object/from16 v0, p2

    #@6f
    iget-object v12, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@71
    invoke-virtual {v10, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@74
    .line 551
    const-string/jumbo v1, "password"

    #@77
    move-object/from16 v0, p3

    #@79
    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 552
    const-string v1, "accounts"

    #@7e
    const-string/jumbo v12, "name"

    #@81
    invoke-virtual {v2, v1, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@84
    move-result-wide v3

    #@85
    .line 553
    .local v3, accountId:J
    const-wide/16 v12, 0x0

    #@87
    cmp-long v1, v3, v12

    #@89
    if-gez v1, :cond_b2

    #@8b
    .line 554
    const-string v1, "AccountManagerService"

    #@8d
    new-instance v12, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v13, "insertAccountIntoDatabase: "

    #@94
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v12

    #@98
    move-object/from16 v0, p2

    #@9a
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v12

    #@9e
    const-string v13, ", skipping the DB insert failed"

    #@a0
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v12

    #@a4
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v12

    #@a8
    invoke-static {v1, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ab
    .catchall {:try_start_5b .. :try_end_ab} :catchall_11b

    #@ab
    .line 556
    const/4 v1, 0x0

    #@ac
    .line 571
    :try_start_ac
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@af
    .line 556
    monitor-exit v11
    :try_end_b0
    .catchall {:try_start_ac .. :try_end_b0} :catchall_58

    #@b0
    goto/16 :goto_3

    #@b2
    .line 558
    :cond_b2
    if-eqz p4, :cond_105

    #@b4
    .line 559
    :try_start_b4
    invoke-virtual/range {p4 .. p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@b7
    move-result-object v1

    #@b8
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@bb
    move-result-object v7

    #@bc
    .local v7, i$:Ljava/util/Iterator;
    :cond_bc
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@bf
    move-result v1

    #@c0
    if-eqz v1, :cond_105

    #@c2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@c5
    move-result-object v5

    #@c6
    check-cast v5, Ljava/lang/String;

    #@c8
    .line 560
    .local v5, key:Ljava/lang/String;
    move-object/from16 v0, p4

    #@ca
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@cd
    move-result-object v6

    #@ce
    .local v6, value:Ljava/lang/String;
    move-object/from16 v1, p0

    #@d0
    .line 561
    invoke-direct/range {v1 .. v6}, Landroid/accounts/AccountManagerService;->insertExtraLocked(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;)J

    #@d3
    move-result-wide v12

    #@d4
    const-wide/16 v14, 0x0

    #@d6
    cmp-long v1, v12, v14

    #@d8
    if-gez v1, :cond_bc

    #@da
    .line 562
    const-string v1, "AccountManagerService"

    #@dc
    new-instance v12, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v13, "insertAccountIntoDatabase: "

    #@e3
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v12

    #@e7
    move-object/from16 v0, p2

    #@e9
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v12

    #@ed
    const-string v13, ", skipping since insertExtra failed for key "

    #@ef
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v12

    #@f3
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v12

    #@f7
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v12

    #@fb
    invoke-static {v1, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_fe
    .catchall {:try_start_b4 .. :try_end_fe} :catchall_11b

    #@fe
    .line 564
    const/4 v1, 0x0

    #@ff
    .line 571
    :try_start_ff
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@102
    .line 564
    monitor-exit v11
    :try_end_103
    .catchall {:try_start_ff .. :try_end_103} :catchall_58

    #@103
    goto/16 :goto_3

    #@105
    .line 568
    .end local v5           #key:Ljava/lang/String;
    .end local v6           #value:Ljava/lang/String;
    .end local v7           #i$:Ljava/util/Iterator;
    :cond_105
    :try_start_105
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    #@108
    .line 569
    invoke-direct/range {p0 .. p2}, Landroid/accounts/AccountManagerService;->insertAccountIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V
    :try_end_10b
    .catchall {:try_start_105 .. :try_end_10b} :catchall_11b

    #@10b
    .line 571
    :try_start_10b
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@10e
    .line 573
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@111
    move-result v1

    #@112
    move-object/from16 v0, p0

    #@114
    invoke-direct {v0, v1}, Landroid/accounts/AccountManagerService;->sendAccountsChangedBroadcast(I)V

    #@117
    .line 574
    const/4 v1, 0x1

    #@118
    monitor-exit v11

    #@119
    goto/16 :goto_3

    #@11b
    .line 571
    .end local v3           #accountId:J
    .end local v8           #numMatches:J
    .end local v10           #values:Landroid/content/ContentValues;
    :catchall_11b
    move-exception v1

    #@11c
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@11f
    throw v1
    :try_end_120
    .catchall {:try_start_10b .. :try_end_120} :catchall_58
.end method

.method private checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V
    .registers 5
    .parameter "account"

    #@0
    .prologue
    .line 2305
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "android.permission.AUTHENTICATE_ACCOUNTS"

    #@6
    aput-object v2, v0, v1

    #@8
    invoke-direct {p0, v0}, Landroid/accounts/AccountManagerService;->checkBinderPermission([Ljava/lang/String;)V

    #@b
    .line 2306
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkCallingUidAgainstAuthenticator(Landroid/accounts/Account;)V

    #@e
    .line 2307
    return-void
.end method

.method private varargs checkBinderPermission([Ljava/lang/String;)V
    .registers 11
    .parameter "permissions"

    #@0
    .prologue
    .line 2198
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v5

    #@4
    .line 2200
    .local v5, uid:I
    move-object v0, p1

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_42

    #@9
    aget-object v4, v0, v1

    #@b
    .line 2201
    .local v4, perm:Ljava/lang/String;
    iget-object v6, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v6, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@10
    move-result v6

    #@11
    if-nez v6, :cond_3f

    #@13
    .line 2202
    const-string v6, "AccountManagerService"

    #@15
    const/4 v7, 0x2

    #@16
    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@19
    move-result v6

    #@1a
    if-eqz v6, :cond_3e

    #@1c
    .line 2203
    const-string v6, "AccountManagerService"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "  caller uid "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, " has "

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v7

    #@3b
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 2205
    :cond_3e
    return-void

    #@3f
    .line 2200
    :cond_3f
    add-int/lit8 v1, v1, 0x1

    #@41
    goto :goto_7

    #@42
    .line 2209
    .end local v4           #perm:Ljava/lang/String;
    :cond_42
    new-instance v6, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v7, "caller uid "

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    const-string v7, " lacks any of "

    #@53
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    const-string v7, ","

    #@59
    invoke-static {v7, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    .line 2210
    .local v3, msg:Ljava/lang/String;
    const-string v6, "AccountManagerService"

    #@67
    new-instance v7, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v8, "  "

    #@6e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v7

    #@72
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 2211
    new-instance v6, Ljava/lang/SecurityException;

    #@7f
    invoke-direct {v6, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@82
    throw v6
.end method

.method private checkCallingUidAgainstAuthenticator(Landroid/accounts/Account;)V
    .registers 7
    .parameter "account"

    #@0
    .prologue
    .line 2293
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v1

    #@4
    .line 2294
    .local v1, uid:I
    if-eqz p1, :cond_e

    #@6
    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@8
    invoke-direct {p0, v2, v1}, Landroid/accounts/AccountManagerService;->hasAuthenticatorUid(Ljava/lang/String;I)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_32

    #@e
    .line 2295
    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "caller uid "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " is different than the authenticator\'s uid"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 2296
    .local v0, msg:Ljava/lang/String;
    const-string v2, "AccountManagerService"

    #@29
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 2297
    new-instance v2, Ljava/lang/SecurityException;

    #@2e
    invoke-direct {v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@31
    throw v2

    #@32
    .line 2299
    .end local v0           #msg:Ljava/lang/String;
    :cond_32
    const-string v2, "AccountManagerService"

    #@34
    const/4 v3, 0x2

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_59

    #@3b
    .line 2300
    const-string v2, "AccountManagerService"

    #@3d
    new-instance v3, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v4, "caller uid "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    const-string v4, " is the same as the authenticator\'s uid"

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2302
    :cond_59
    return-void
.end method

.method private checkManageAccountsOrUseCredentialsPermissions()V
    .registers 4

    #@0
    .prologue
    .line 2318
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "android.permission.MANAGE_ACCOUNTS"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "android.permission.USE_CREDENTIALS"

    #@b
    aput-object v2, v0, v1

    #@d
    invoke-direct {p0, v0}, Landroid/accounts/AccountManagerService;->checkBinderPermission([Ljava/lang/String;)V

    #@10
    .line 2320
    return-void
.end method

.method private checkManageAccountsPermission()V
    .registers 4

    #@0
    .prologue
    .line 2314
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "android.permission.MANAGE_ACCOUNTS"

    #@6
    aput-object v2, v0, v1

    #@8
    invoke-direct {p0, v0}, Landroid/accounts/AccountManagerService;->checkBinderPermission([Ljava/lang/String;)V

    #@b
    .line 2315
    return-void
.end method

.method private checkReadAccountsPermission()V
    .registers 4

    #@0
    .prologue
    .line 2310
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "android.permission.GET_ACCOUNTS"

    #@6
    aput-object v2, v0, v1

    #@8
    invoke-direct {p0, v0}, Landroid/accounts/AccountManagerService;->checkBinderPermission([Ljava/lang/String;)V

    #@b
    .line 2311
    return-void
.end method

.method private createNoCredentialsPermissionNotification(Landroid/accounts/Account;Landroid/content/Intent;I)V
    .registers 21
    .parameter "account"
    .parameter "intent"
    .parameter "userId"

    #@0
    .prologue
    .line 1197
    const-string/jumbo v2, "uid"

    #@3
    const/4 v3, -0x1

    #@4
    move-object/from16 v0, p2

    #@6
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@9
    move-result v15

    #@a
    .line 1199
    .local v15, uid:I
    const-string v2, "authTokenType"

    #@c
    move-object/from16 v0, p2

    #@e
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v9

    #@12
    .line 1201
    .local v9, authTokenType:Ljava/lang/String;
    const-string v2, "authTokenLabel"

    #@14
    move-object/from16 v0, p2

    #@16
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v8

    #@1a
    .line 1204
    .local v8, authTokenLabel:Ljava/lang/String;
    new-instance v11, Landroid/app/Notification;

    #@1c
    const v2, 0x108008a

    #@1f
    const/4 v3, 0x0

    #@20
    const-wide/16 v4, 0x0

    #@22
    invoke-direct {v11, v2, v3, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@25
    .line 1206
    .local v11, n:Landroid/app/Notification;
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@29
    const v3, 0x10404a8

    #@2c
    const/4 v4, 0x1

    #@2d
    new-array v4, v4, [Ljava/lang/Object;

    #@2f
    const/4 v5, 0x0

    #@30
    move-object/from16 v0, p1

    #@32
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@34
    aput-object v6, v4, v5

    #@36
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@39
    move-result-object v14

    #@3a
    .line 1209
    .local v14, titleAndSubtitle:Ljava/lang/String;
    const/16 v2, 0xa

    #@3c
    invoke-virtual {v14, v2}, Ljava/lang/String;->indexOf(I)I

    #@3f
    move-result v10

    #@40
    .line 1210
    .local v10, index:I
    move-object v13, v14

    #@41
    .line 1211
    .local v13, title:Ljava/lang/String;
    const-string v12, ""

    #@43
    .line 1212
    .local v12, subtitle:Ljava/lang/String;
    if-lez v10, :cond_50

    #@45
    .line 1213
    const/4 v2, 0x0

    #@46
    invoke-virtual {v14, v2, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@49
    move-result-object v13

    #@4a
    .line 1214
    add-int/lit8 v2, v10, 0x1

    #@4c
    invoke-virtual {v14, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4f
    move-result-object v12

    #@50
    .line 1216
    :cond_50
    new-instance v7, Landroid/os/UserHandle;

    #@52
    move/from16 v0, p3

    #@54
    invoke-direct {v7, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@57
    .line 1217
    .local v7, user:Landroid/os/UserHandle;
    move-object/from16 v0, p0

    #@59
    iget-object v0, v0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@5b
    move-object/from16 v16, v0

    #@5d
    move-object/from16 v0, p0

    #@5f
    iget-object v2, v0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@61
    const/4 v3, 0x0

    #@62
    const/high16 v5, 0x1000

    #@64
    const/4 v6, 0x0

    #@65
    move-object/from16 v4, p2

    #@67
    invoke-static/range {v2 .. v7}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@6a
    move-result-object v2

    #@6b
    move-object/from16 v0, v16

    #@6d
    invoke-virtual {v11, v0, v13, v12, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@70
    .line 1220
    move-object/from16 v0, p0

    #@72
    move-object/from16 v1, p1

    #@74
    invoke-direct {v0, v1, v9, v15}, Landroid/accounts/AccountManagerService;->getCredentialPermissionNotificationId(Landroid/accounts/Account;Ljava/lang/String;I)Ljava/lang/Integer;

    #@77
    move-result-object v2

    #@78
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@7b
    move-result v2

    #@7c
    move-object/from16 v0, p0

    #@7e
    invoke-virtual {v0, v2, v11, v7}, Landroid/accounts/AccountManagerService;->installNotification(ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@81
    .line 1222
    return-void
.end method

.method private doNotification(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/CharSequence;Landroid/content/Intent;I)V
    .registers 23
    .parameter "accounts"
    .parameter "account"
    .parameter "message"
    .parameter "intent"
    .parameter "userId"

    #@0
    .prologue
    .line 2150
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@3
    move-result-wide v10

    #@4
    .line 2152
    .local v10, identityToken:J
    :try_start_4
    const-string v4, "AccountManagerService"

    #@6
    const/4 v5, 0x2

    #@7
    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_33

    #@d
    .line 2153
    const-string v4, "AccountManagerService"

    #@f
    new-instance v5, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v6, "doNotification: "

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    move-object/from16 v0, p3

    #@1c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    const-string v6, " intent:"

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    move-object/from16 v0, p4

    #@28
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 2156
    :cond_33
    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@36
    move-result-object v4

    #@37
    if-eqz v4, :cond_5c

    #@39
    const-class v4, Landroid/accounts/GrantCredentialsPermissionActivity;

    #@3b
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v4

    #@4b
    if-eqz v4, :cond_5c

    #@4d
    .line 2159
    move-object/from16 v0, p0

    #@4f
    move-object/from16 v1, p2

    #@51
    move-object/from16 v2, p4

    #@53
    move/from16 v3, p5

    #@55
    invoke-direct {v0, v1, v2, v3}, Landroid/accounts/AccountManagerService;->createNoCredentialsPermissionNotification(Landroid/accounts/Account;Landroid/content/Intent;I)V
    :try_end_58
    .catchall {:try_start_4 .. :try_end_58} :catchall_bb

    #@58
    .line 2176
    :goto_58
    invoke-static {v10, v11}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@5b
    .line 2178
    return-void

    #@5c
    .line 2161
    :cond_5c
    :try_start_5c
    invoke-direct/range {p0 .. p2}, Landroid/accounts/AccountManagerService;->getSigninRequiredNotificationId(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;

    #@5f
    move-result-object v13

    #@60
    .line 2162
    .local v13, notificationId:Ljava/lang/Integer;
    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    move-object/from16 v0, p4

    #@66
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@69
    .line 2163
    new-instance v12, Landroid/app/Notification;

    #@6b
    const v4, 0x108008a

    #@6e
    const/4 v5, 0x0

    #@6f
    const-wide/16 v6, 0x0

    #@71
    invoke-direct {v12, v4, v5, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@74
    .line 2165
    .local v12, n:Landroid/app/Notification;
    new-instance v9, Landroid/os/UserHandle;

    #@76
    move/from16 v0, p5

    #@78
    invoke-direct {v9, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@7b
    .line 2166
    .local v9, user:Landroid/os/UserHandle;
    move-object/from16 v0, p0

    #@7d
    iget-object v4, v0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@7f
    const v5, 0x10400ec

    #@82
    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@89
    move-result-object v14

    #@8a
    .line 2168
    .local v14, notificationTitleFormat:Ljava/lang/String;
    move-object/from16 v0, p0

    #@8c
    iget-object v15, v0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@8e
    const/4 v4, 0x1

    #@8f
    new-array v4, v4, [Ljava/lang/Object;

    #@91
    const/4 v5, 0x0

    #@92
    move-object/from16 v0, p2

    #@94
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@96
    aput-object v6, v4, v5

    #@98
    invoke-static {v14, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@9b
    move-result-object v16

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v4, v0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@a0
    const/4 v5, 0x0

    #@a1
    const/high16 v7, 0x1000

    #@a3
    const/4 v8, 0x0

    #@a4
    move-object/from16 v6, p4

    #@a6
    invoke-static/range {v4 .. v9}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@a9
    move-result-object v4

    #@aa
    move-object/from16 v0, v16

    #@ac
    move-object/from16 v1, p3

    #@ae
    invoke-virtual {v12, v15, v0, v1, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@b1
    .line 2173
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    #@b4
    move-result v4

    #@b5
    move-object/from16 v0, p0

    #@b7
    invoke-virtual {v0, v4, v12, v9}, Landroid/accounts/AccountManagerService;->installNotification(ILandroid/app/Notification;Landroid/os/UserHandle;)V
    :try_end_ba
    .catchall {:try_start_5c .. :try_end_ba} :catchall_bb

    #@ba
    goto :goto_58

    #@bb
    .line 2176
    .end local v9           #user:Landroid/os/UserHandle;
    .end local v12           #n:Landroid/app/Notification;
    .end local v13           #notificationId:Ljava/lang/Integer;
    .end local v14           #notificationTitleFormat:Ljava/lang/String;
    :catchall_bb
    move-exception v4

    #@bc
    invoke-static {v10, v11}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@bf
    throw v4
.end method

.method private dumpUser(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Z)V
    .registers 27
    .parameter "userAccounts"
    .parameter "fd"
    .parameter "fout"
    .parameter "args"
    .parameter "isCheckinRequest"

    #@0
    .prologue
    .line 2109
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@3
    move-result-object v20

    #@4
    monitor-enter v20

    #@5
    .line 2110
    :try_start_5
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@8
    move-result-object v4

    #@9
    invoke-virtual {v4}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@c
    move-result-object v3

    #@d
    .line 2112
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    if-eqz p5, :cond_55

    #@f
    .line 2114
    const-string v4, "accounts"

    #@11
    sget-object v5, Landroid/accounts/AccountManagerService;->ACCOUNT_TYPE_COUNT_PROJECTION:[Ljava/lang/String;

    #@13
    const/4 v6, 0x0

    #@14
    const/4 v7, 0x0

    #@15
    const-string/jumbo v8, "type"

    #@18
    const/4 v9, 0x0

    #@19
    const/4 v10, 0x0

    #@1a
    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_52

    #@1d
    move-result-object v14

    #@1e
    .line 2117
    .local v14, cursor:Landroid/database/Cursor;
    :goto_1e
    :try_start_1e
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@21
    move-result v4

    #@22
    if-eqz v4, :cond_11c

    #@24
    .line 2119
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const/4 v5, 0x0

    #@2a
    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    const-string v5, ","

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    const/4 v5, 0x1

    #@39
    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    move-object/from16 v0, p3

    #@47
    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_4a
    .catchall {:try_start_1e .. :try_end_4a} :catchall_4b

    #@4a
    goto :goto_1e

    #@4b
    .line 2122
    :catchall_4b
    move-exception v4

    #@4c
    if-eqz v14, :cond_51

    #@4e
    .line 2123
    :try_start_4e
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@51
    .line 2122
    :cond_51
    throw v4

    #@52
    .line 2145
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v14           #cursor:Landroid/database/Cursor;
    :catchall_52
    move-exception v4

    #@53
    monitor-exit v20
    :try_end_54
    .catchall {:try_start_4e .. :try_end_54} :catchall_52

    #@54
    throw v4

    #@55
    .line 2127
    .restart local v3       #db:Landroid/database/sqlite/SQLiteDatabase;
    :cond_55
    const/4 v4, 0x0

    #@56
    :try_start_56
    move-object/from16 v0, p0

    #@58
    move-object/from16 v1, p1

    #@5a
    invoke-virtual {v0, v1, v4}, Landroid/accounts/AccountManagerService;->getAccountsFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;

    #@5d
    move-result-object v12

    #@5e
    .line 2128
    .local v12, accounts:[Landroid/accounts/Account;
    new-instance v4, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v5, "Accounts: "

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    array-length v5, v12

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    move-object/from16 v0, p3

    #@74
    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@77
    .line 2129
    move-object v13, v12

    #@78
    .local v13, arr$:[Landroid/accounts/Account;
    array-length v0, v13

    #@79
    move/from16 v16, v0

    #@7b
    .local v16, len$:I
    const/4 v15, 0x0

    #@7c
    .local v15, i$:I
    :goto_7c
    move/from16 v0, v16

    #@7e
    if-ge v15, v0, :cond_9d

    #@80
    aget-object v11, v13, v15

    #@82
    .line 2130
    .local v11, account:Landroid/accounts/Account;
    new-instance v4, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v5, "  "

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    move-object/from16 v0, p3

    #@97
    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9a
    .line 2129
    add-int/lit8 v15, v15, 0x1

    #@9c
    goto :goto_7c

    #@9d
    .line 2133
    .end local v11           #account:Landroid/accounts/Account;
    :cond_9d
    invoke-virtual/range {p3 .. p3}, Ljava/io/PrintWriter;->println()V

    #@a0
    .line 2134
    move-object/from16 v0, p0

    #@a2
    iget-object v5, v0, Landroid/accounts/AccountManagerService;->mSessions:Ljava/util/LinkedHashMap;

    #@a4
    monitor-enter v5
    :try_end_a5
    .catchall {:try_start_56 .. :try_end_a5} :catchall_52

    #@a5
    .line 2135
    :try_start_a5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a8
    move-result-wide v17

    #@a9
    .line 2136
    .local v17, now:J
    new-instance v4, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v6, "Active Sessions: "

    #@b0
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v4

    #@b4
    move-object/from16 v0, p0

    #@b6
    iget-object v6, v0, Landroid/accounts/AccountManagerService;->mSessions:Ljava/util/LinkedHashMap;

    #@b8
    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->size()I

    #@bb
    move-result v6

    #@bc
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v4

    #@c0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v4

    #@c4
    move-object/from16 v0, p3

    #@c6
    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c9
    .line 2137
    move-object/from16 v0, p0

    #@cb
    iget-object v4, v0, Landroid/accounts/AccountManagerService;->mSessions:Ljava/util/LinkedHashMap;

    #@cd
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    #@d0
    move-result-object v4

    #@d1
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d4
    move-result-object v15

    #@d5
    .local v15, i$:Ljava/util/Iterator;
    :goto_d5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    #@d8
    move-result v4

    #@d9
    if-eqz v4, :cond_105

    #@db
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@de
    move-result-object v19

    #@df
    check-cast v19, Landroid/accounts/AccountManagerService$Session;

    #@e1
    .line 2138
    .local v19, session:Landroid/accounts/AccountManagerService$Session;
    new-instance v4, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v6, "  "

    #@e8
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v4

    #@ec
    move-object/from16 v0, v19

    #@ee
    move-wide/from16 v1, v17

    #@f0
    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManagerService$Session;->toDebugString(J)Ljava/lang/String;

    #@f3
    move-result-object v6

    #@f4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v4

    #@f8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v4

    #@fc
    move-object/from16 v0, p3

    #@fe
    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@101
    goto :goto_d5

    #@102
    .line 2140
    .end local v15           #i$:Ljava/util/Iterator;
    .end local v17           #now:J
    .end local v19           #session:Landroid/accounts/AccountManagerService$Session;
    :catchall_102
    move-exception v4

    #@103
    monitor-exit v5
    :try_end_104
    .catchall {:try_start_a5 .. :try_end_104} :catchall_102

    #@104
    :try_start_104
    throw v4
    :try_end_105
    .catchall {:try_start_104 .. :try_end_105} :catchall_52

    #@105
    .restart local v15       #i$:Ljava/util/Iterator;
    .restart local v17       #now:J
    :cond_105
    :try_start_105
    monitor-exit v5
    :try_end_106
    .catchall {:try_start_105 .. :try_end_106} :catchall_102

    #@106
    .line 2142
    :try_start_106
    invoke-virtual/range {p3 .. p3}, Ljava/io/PrintWriter;->println()V

    #@109
    .line 2143
    move-object/from16 v0, p0

    #@10b
    iget-object v4, v0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@10d
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@110
    move-result v5

    #@111
    move-object/from16 v0, p2

    #@113
    move-object/from16 v1, p3

    #@115
    move-object/from16 v2, p4

    #@117
    invoke-interface {v4, v0, v1, v2, v5}, Landroid/accounts/IAccountAuthenticatorCache;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    #@11a
    .line 2145
    .end local v12           #accounts:[Landroid/accounts/Account;
    .end local v13           #arr$:[Landroid/accounts/Account;
    .end local v15           #i$:Ljava/util/Iterator;
    .end local v16           #len$:I
    .end local v17           #now:J
    :cond_11a
    :goto_11a
    monitor-exit v20

    #@11b
    .line 2146
    return-void

    #@11c
    .line 2122
    .restart local v14       #cursor:Landroid/database/Cursor;
    :cond_11c
    if-eqz v14, :cond_11a

    #@11e
    .line 2123
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_121
    .catchall {:try_start_106 .. :try_end_121} :catchall_52

    #@121
    goto :goto_11a
.end method

.method private getAccountIdLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)J
    .registers 12
    .parameter "db"
    .parameter "account"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 1641
    const-string v1, "accounts"

    #@5
    new-array v2, v7, [Ljava/lang/String;

    #@7
    const-string v0, "_id"

    #@9
    aput-object v0, v2, v6

    #@b
    const-string/jumbo v3, "name=? AND type=?"

    #@e
    const/4 v0, 0x2

    #@f
    new-array v4, v0, [Ljava/lang/String;

    #@11
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@13
    aput-object v0, v4, v6

    #@15
    iget-object v0, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@17
    aput-object v0, v4, v7

    #@19
    move-object v0, p1

    #@1a
    move-object v6, v5

    #@1b
    move-object v7, v5

    #@1c
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1f
    move-result-object v8

    #@20
    .line 1644
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_20
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2f

    #@26
    .line 1645
    const/4 v0, 0x0

    #@27
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2a
    .catchall {:try_start_20 .. :try_end_2a} :catchall_32

    #@2a
    move-result-wide v0

    #@2b
    .line 1649
    :goto_2b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@2e
    .line 1647
    return-wide v0

    #@2f
    :cond_2f
    const-wide/16 v0, -0x1

    #@31
    goto :goto_2b

    #@32
    .line 1649
    :catchall_32
    move-exception v0

    #@33
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@36
    throw v0
.end method

.method private getAccounts([I)[Landroid/accounts/AccountAndUser;
    .registers 15
    .parameter "userIds"

    #@0
    .prologue
    .line 1560
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v6

    #@4
    .line 1561
    .local v6, runningAccounts:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/AccountAndUser;>;"
    iget-object v10, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@6
    monitor-enter v10

    #@7
    .line 1562
    move-object v3, p1

    #@8
    .local v3, arr$:[I
    :try_start_8
    array-length v5, v3

    #@9
    .local v5, len$:I
    const/4 v4, 0x0

    #@a
    .local v4, i$:I
    :goto_a
    if-ge v4, v5, :cond_3a

    #@c
    aget v8, v3, v4

    #@e
    .line 1563
    .local v8, userId:I
    invoke-virtual {p0, v8}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@11
    move-result-object v7

    #@12
    .line 1564
    .local v7, userAccounts:Landroid/accounts/AccountManagerService$UserAccounts;
    if-nez v7, :cond_17

    #@14
    .line 1562
    :goto_14
    add-int/lit8 v4, v4, 0x1

    #@16
    goto :goto_a

    #@17
    .line 1565
    :cond_17
    invoke-static {v7}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@1a
    move-result-object v11

    #@1b
    monitor-enter v11
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_37

    #@1c
    .line 1566
    const/4 v9, 0x0

    #@1d
    :try_start_1d
    invoke-virtual {p0, v7, v9}, Landroid/accounts/AccountManagerService;->getAccountsFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;

    #@20
    move-result-object v1

    #@21
    .line 1567
    .local v1, accounts:[Landroid/accounts/Account;
    const/4 v0, 0x0

    #@22
    .local v0, a:I
    :goto_22
    array-length v9, v1

    #@23
    if-ge v0, v9, :cond_32

    #@25
    .line 1568
    new-instance v9, Landroid/accounts/AccountAndUser;

    #@27
    aget-object v12, v1, v0

    #@29
    invoke-direct {v9, v12, v8}, Landroid/accounts/AccountAndUser;-><init>(Landroid/accounts/Account;I)V

    #@2c
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    .line 1567
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_22

    #@32
    .line 1570
    :cond_32
    monitor-exit v11

    #@33
    goto :goto_14

    #@34
    .end local v0           #a:I
    .end local v1           #accounts:[Landroid/accounts/Account;
    :catchall_34
    move-exception v9

    #@35
    monitor-exit v11
    :try_end_36
    .catchall {:try_start_1d .. :try_end_36} :catchall_34

    #@36
    :try_start_36
    throw v9

    #@37
    .line 1572
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v7           #userAccounts:Landroid/accounts/AccountManagerService$UserAccounts;
    .end local v8           #userId:I
    :catchall_37
    move-exception v9

    #@38
    monitor-exit v10
    :try_end_39
    .catchall {:try_start_36 .. :try_end_39} :catchall_37

    #@39
    throw v9

    #@3a
    .restart local v4       #i$:I
    .restart local v5       #len$:I
    :cond_3a
    :try_start_3a
    monitor-exit v10
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_37

    #@3b
    .line 1574
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@3e
    move-result v9

    #@3f
    new-array v2, v9, [Landroid/accounts/AccountAndUser;

    #@41
    .line 1575
    .local v2, accountsArray:[Landroid/accounts/AccountAndUser;
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@44
    move-result-object v9

    #@45
    check-cast v9, [Landroid/accounts/AccountAndUser;

    #@47
    return-object v9
.end method

.method private getCredentialPermissionNotificationId(Landroid/accounts/Account;Ljava/lang/String;I)Ljava/lang/Integer;
    .registers 10
    .parameter "account"
    .parameter "authTokenType"
    .parameter "uid"

    #@0
    .prologue
    .line 1246
    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    #@3
    move-result v3

    #@4
    invoke-virtual {p0, v3}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@7
    move-result-object v0

    #@8
    .line 1247
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$800(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@b
    move-result-object v4

    #@c
    monitor-enter v4

    #@d
    .line 1248
    :try_start_d
    new-instance v2, Landroid/util/Pair;

    #@f
    new-instance v3, Landroid/util/Pair;

    #@11
    invoke-direct {v3, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@14
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v5

    #@18
    invoke-direct {v2, v3, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@1b
    .line 1251
    .local v2, key:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;Ljava/lang/Integer;>;"
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$800(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Ljava/lang/Integer;

    #@25
    .line 1252
    .local v1, id:Ljava/lang/Integer;
    if-nez v1, :cond_38

    #@27
    .line 1253
    iget-object v3, p0, Landroid/accounts/AccountManagerService;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

    #@29
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@2c
    move-result v3

    #@2d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v1

    #@31
    .line 1254
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$800(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    .line 1256
    :cond_38
    monitor-exit v4

    #@39
    .line 1257
    return-object v1

    #@3a
    .line 1256
    .end local v1           #id:Ljava/lang/Integer;
    .end local v2           #key:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;Ljava/lang/Integer;>;"
    :catchall_3a
    move-exception v3

    #@3b
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_d .. :try_end_3c} :catchall_3a

    #@3c
    throw v3
.end method

.method private static getDatabaseName(I)Ljava/lang/String;
    .registers 8
    .parameter "userId"

    #@0
    .prologue
    .line 1945
    invoke-static {}, Landroid/os/Environment;->getSystemSecureDirectory()Ljava/io/File;

    #@3
    move-result-object v2

    #@4
    .line 1946
    .local v2, systemDir:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    #@6
    invoke-static {p0}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@9
    move-result-object v4

    #@a
    const-string v5, "accounts.db"

    #@c
    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@f
    .line 1947
    .local v0, databaseFile:Ljava/io/File;
    if-nez p0, :cond_6c

    #@11
    .line 1952
    new-instance v1, Ljava/io/File;

    #@13
    const-string v4, "accounts.db"

    #@15
    invoke-direct {v1, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@18
    .line 1953
    .local v1, oldFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_6c

    #@1e
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@21
    move-result v4

    #@22
    if-nez v4, :cond_6c

    #@24
    .line 1955
    invoke-static {p0}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@27
    move-result-object v3

    #@28
    .line 1956
    .local v3, userDir:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_4d

    #@2e
    .line 1957
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    #@31
    move-result v4

    #@32
    if-nez v4, :cond_4d

    #@34
    .line 1958
    new-instance v4, Ljava/lang/IllegalStateException;

    #@36
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v6, "User dir cannot be created: "

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v4

    #@4d
    .line 1961
    :cond_4d
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@50
    move-result v4

    #@51
    if-nez v4, :cond_6c

    #@53
    .line 1962
    new-instance v4, Ljava/lang/IllegalStateException;

    #@55
    new-instance v5, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v6, "User dir cannot be migrated: "

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v4

    #@6c
    .line 1966
    .end local v1           #oldFile:Ljava/io/File;
    .end local v3           #userDir:Ljava/io/File;
    :cond_6c
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    return-object v4
.end method

.method private getExtrasIdLocked(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)J
    .registers 14
    .parameter "db"
    .parameter "accountId"
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 1654
    const-string v1, "extras"

    #@5
    new-array v2, v4, [Ljava/lang/String;

    #@7
    const-string v0, "_id"

    #@9
    aput-object v0, v2, v6

    #@b
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "accounts_id="

    #@12
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v3, " AND "

    #@1c
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    const-string/jumbo v3, "key"

    #@23
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v3, "=?"

    #@29
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    new-array v4, v4, [Ljava/lang/String;

    #@33
    aput-object p4, v4, v6

    #@35
    move-object v0, p1

    #@36
    move-object v6, v5

    #@37
    move-object v7, v5

    #@38
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3b
    move-result-object v8

    #@3c
    .line 1658
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_3c
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_4b

    #@42
    .line 1659
    const/4 v0, 0x0

    #@43
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_46
    .catchall {:try_start_3c .. :try_end_46} :catchall_4e

    #@46
    move-result-wide v0

    #@47
    .line 1663
    :goto_47
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@4a
    .line 1661
    return-wide v0

    #@4b
    :cond_4b
    const-wide/16 v0, -0x1

    #@4d
    goto :goto_47

    #@4e
    .line 1663
    :catchall_4e
    move-exception v0

    #@4f
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@52
    throw v0
.end method

.method private getSigninRequiredNotificationId(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;
    .registers 6
    .parameter "accounts"
    .parameter "account"

    #@0
    .prologue
    .line 1262
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$1400(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@3
    move-result-object v2

    #@4
    monitor-enter v2

    #@5
    .line 1263
    :try_start_5
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$1400(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Ljava/lang/Integer;

    #@f
    .line 1264
    .local v0, id:Ljava/lang/Integer;
    if-nez v0, :cond_22

    #@11
    .line 1265
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mNotificationIds:Ljava/util/concurrent/atomic/AtomicInteger;

    #@13
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v0

    #@1b
    .line 1266
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$1400(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 1268
    :cond_22
    monitor-exit v2

    #@23
    .line 1269
    return-object v0

    #@24
    .line 1268
    .end local v0           #id:Ljava/lang/Integer;
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public static getSingleton()Landroid/accounts/AccountManagerService;
    .registers 1

    #@0
    .prologue
    .line 208
    sget-object v0, Landroid/accounts/AccountManagerService;->sThis:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/accounts/AccountManagerService;

    #@8
    return-object v0
.end method

.method private getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;
    .registers 2

    #@0
    .prologue
    .line 386
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private getUserManager()Landroid/os/UserManager;
    .registers 3

    #@0
    .prologue
    .line 253
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mUserManager:Landroid/os/UserManager;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 254
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string/jumbo v1, "user"

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/os/UserManager;

    #@f
    iput-object v0, p0, Landroid/accounts/AccountManagerService;->mUserManager:Landroid/os/UserManager;

    #@11
    .line 256
    :cond_11
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mUserManager:Landroid/os/UserManager;

    #@13
    return-object v0
.end method

.method private grantAppPermission(Landroid/accounts/Account;Ljava/lang/String;I)V
    .registers 13
    .parameter "account"
    .parameter "authTokenType"
    .parameter "uid"

    #@0
    .prologue
    .line 2345
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_11

    #@4
    .line 2346
    :cond_4
    const-string v5, "AccountManagerService"

    #@6
    const-string v6, "grantAppPermission: called with invalid arguments"

    #@8
    new-instance v7, Ljava/lang/Exception;

    #@a
    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    #@d
    invoke-static {v5, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    .line 2369
    :goto_10
    return-void

    #@11
    .line 2349
    :cond_11
    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    #@14
    move-result v5

    #@15
    invoke-virtual {p0, v5}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@18
    move-result-object v2

    #@19
    .line 2350
    .local v2, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@1c
    move-result-object v6

    #@1d
    monitor-enter v6

    #@1e
    .line 2351
    :try_start_1e
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@25
    move-result-object v3

    #@26
    .line 2352
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_29
    .catchall {:try_start_1e .. :try_end_29} :catchall_73

    #@29
    .line 2354
    :try_start_29
    invoke-direct {p0, v3, p1}, Landroid/accounts/AccountManagerService;->getAccountIdLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)J

    #@2c
    move-result-wide v0

    #@2d
    .line 2355
    .local v0, accountId:J
    const-wide/16 v7, 0x0

    #@2f
    cmp-long v5, v0, v7

    #@31
    if-ltz v5, :cond_5a

    #@33
    .line 2356
    new-instance v4, Landroid/content/ContentValues;

    #@35
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@38
    .line 2357
    .local v4, values:Landroid/content/ContentValues;
    const-string v5, "accounts_id"

    #@3a
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@41
    .line 2358
    const-string v5, "auth_token_type"

    #@43
    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 2359
    const-string/jumbo v5, "uid"

    #@49
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@50
    .line 2360
    const-string v5, "grants"

    #@52
    const-string v7, "accounts_id"

    #@54
    invoke-virtual {v3, v5, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@57
    .line 2361
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5a
    .catchall {:try_start_29 .. :try_end_5a} :catchall_76

    #@5a
    .line 2364
    .end local v4           #values:Landroid/content/ContentValues;
    :cond_5a
    :try_start_5a
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@5d
    .line 2366
    invoke-direct {p0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->getCredentialPermissionNotificationId(Landroid/accounts/Account;Ljava/lang/String;I)Ljava/lang/Integer;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@64
    move-result v5

    #@65
    new-instance v7, Landroid/os/UserHandle;

    #@67
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@6a
    move-result v8

    #@6b
    invoke-direct {v7, v8}, Landroid/os/UserHandle;-><init>(I)V

    #@6e
    invoke-virtual {p0, v5, v7}, Landroid/accounts/AccountManagerService;->cancelNotification(ILandroid/os/UserHandle;)V

    #@71
    .line 2368
    monitor-exit v6

    #@72
    goto :goto_10

    #@73
    .end local v0           #accountId:J
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_73
    move-exception v5

    #@74
    monitor-exit v6
    :try_end_75
    .catchall {:try_start_5a .. :try_end_75} :catchall_73

    #@75
    throw v5

    #@76
    .line 2364
    .restart local v3       #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_76
    move-exception v5

    #@77
    :try_start_77
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@7a
    throw v5
    :try_end_7b
    .catchall {:try_start_77 .. :try_end_7b} :catchall_73
.end method

.method private hasAuthenticatorUid(Ljava/lang/String;I)Z
    .registers 9
    .parameter "accountType"
    .parameter "callingUid"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2256
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    #@4
    move-result v0

    #@5
    .line 2258
    .local v0, callingUserId:I
    iget-object v3, p0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@7
    invoke-interface {v3, v0}, Landroid/accounts/IAccountAuthenticatorCache;->getAllServices(I)Ljava/util/Collection;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v1

    #@f
    .local v1, i$:Ljava/util/Iterator;
    :cond_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_39

    #@15
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@1b
    .line 2259
    .local v2, serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    iget-object v3, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@1d
    check-cast v3, Landroid/accounts/AuthenticatorDescription;

    #@1f
    iget-object v3, v3, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@21
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_f

    #@27
    .line 2260
    iget v3, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@29
    if-eq v3, p2, :cond_35

    #@2b
    iget-object v3, p0, Landroid/accounts/AccountManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2d
    iget v5, v2, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->uid:I

    #@2f
    invoke-virtual {v3, v5, p2}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    #@32
    move-result v3

    #@33
    if-nez v3, :cond_37

    #@35
    :cond_35
    const/4 v3, 0x1

    #@36
    .line 2265
    .end local v2           #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    :goto_36
    return v3

    #@37
    .restart local v2       #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    :cond_37
    move v3, v4

    #@38
    .line 2260
    goto :goto_36

    #@39
    .end local v2           #serviceInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    :cond_39
    move v3, v4

    #@3a
    .line 2265
    goto :goto_36
.end method

.method private hasExplicitlyGrantedPermission(Landroid/accounts/Account;Ljava/lang/String;I)Z
    .registers 14
    .parameter "account"
    .parameter "authTokenType"
    .parameter "callerUid"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 2270
    const/16 v5, 0x3e8

    #@4
    if-ne p3, v5, :cond_8

    #@6
    move v3, v4

    #@7
    .line 2288
    :goto_7
    return v3

    #@8
    .line 2273
    :cond_8
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@b
    move-result-object v0

    #@c
    .line 2274
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@f
    move-result-object v5

    #@10
    monitor-enter v5

    #@11
    .line 2275
    :try_start_11
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@18
    move-result-object v2

    #@19
    .line 2276
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v6, 0x4

    #@1a
    new-array v1, v6, [Ljava/lang/String;

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@20
    move-result-object v7

    #@21
    aput-object v7, v1, v6

    #@23
    const/4 v6, 0x1

    #@24
    aput-object p2, v1, v6

    #@26
    const/4 v6, 0x2

    #@27
    iget-object v7, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@29
    aput-object v7, v1, v6

    #@2b
    const/4 v6, 0x3

    #@2c
    iget-object v7, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@2e
    aput-object v7, v1, v6

    #@30
    .line 2278
    .local v1, args:[Ljava/lang/String;
    const-string v6, "SELECT COUNT(*) FROM grants, accounts WHERE accounts_id=_id AND uid=? AND auth_token_type=? AND name=? AND type=?"

    #@32
    invoke-static {v2, v6, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    #@35
    move-result-wide v6

    #@36
    const-wide/16 v8, 0x0

    #@38
    cmp-long v6, v6, v8

    #@3a
    if-eqz v6, :cond_3d

    #@3c
    move v3, v4

    #@3d
    .line 2280
    .local v3, permissionGranted:Z
    :cond_3d
    if-nez v3, :cond_7b

    #@3f
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    #@42
    move-result v6

    #@43
    if-eqz v6, :cond_7b

    #@45
    .line 2283
    const-string v6, "AccountManagerService"

    #@47
    new-instance v7, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string/jumbo v8, "no credentials permission for usage of "

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v7

    #@57
    const-string v8, ", "

    #@59
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v7

    #@61
    const-string v8, " by uid "

    #@63
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v7

    #@67
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v7

    #@6b
    const-string v8, " but ignoring since device is in test harness."

    #@6d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v7

    #@71
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v7

    #@75
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 2286
    monitor-exit v5

    #@79
    move v3, v4

    #@7a
    goto :goto_7

    #@7b
    .line 2288
    :cond_7b
    monitor-exit v5

    #@7c
    goto :goto_7

    #@7d
    .line 2289
    .end local v1           #args:[Ljava/lang/String;
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v3           #permissionGranted:Z
    :catchall_7d
    move-exception v4

    #@7e
    monitor-exit v5
    :try_end_7f
    .catchall {:try_start_11 .. :try_end_7f} :catchall_7d

    #@7f
    throw v4
.end method

.method private inSystemImage(I)Z
    .registers 16
    .parameter "callingUid"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2215
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    #@4
    move-result v1

    #@5
    .line 2219
    .local v1, callingUserId:I
    :try_start_5
    iget-object v10, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@7
    const-string v11, "android"

    #@9
    const/4 v12, 0x0

    #@a
    new-instance v13, Landroid/os/UserHandle;

    #@c
    invoke-direct {v13, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@f
    invoke-virtual {v10, v11, v12, v13}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    #@12
    move-result-object v10

    #@13
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_16
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_16} :catch_33

    #@16
    move-result-object v8

    #@17
    .line 2225
    .local v8, userPackageManager:Landroid/content/pm/PackageManager;
    invoke-virtual {v8, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@1a
    move-result-object v7

    #@1b
    .line 2226
    .local v7, packages:[Ljava/lang/String;
    move-object v0, v7

    #@1c
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@1d
    .local v4, len$:I
    const/4 v3, 0x0

    #@1e
    .local v3, i$:I
    :goto_1e
    if-ge v3, v4, :cond_32

    #@20
    aget-object v5, v0, v3

    #@22
    .line 2228
    .local v5, name:Ljava/lang/String;
    const/4 v10, 0x0

    #@23
    :try_start_23
    invoke-virtual {v8, v5, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@26
    move-result-object v6

    #@27
    .line 2229
    .local v6, packageInfo:Landroid/content/pm/PackageInfo;
    if-eqz v6, :cond_37

    #@29
    iget-object v10, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2b
    iget v10, v10, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_2d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_23 .. :try_end_2d} :catch_35

    #@2d
    and-int/lit8 v10, v10, 0x1

    #@2f
    if-eqz v10, :cond_37

    #@31
    .line 2231
    const/4 v9, 0x1

    #@32
    .line 2237
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #name:Ljava/lang/String;
    .end local v6           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v7           #packages:[Ljava/lang/String;
    .end local v8           #userPackageManager:Landroid/content/pm/PackageManager;
    :cond_32
    :goto_32
    return v9

    #@33
    .line 2221
    :catch_33
    move-exception v2

    #@34
    .line 2222
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_32

    #@35
    .line 2233
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    .restart local v5       #name:Ljava/lang/String;
    .restart local v7       #packages:[Ljava/lang/String;
    .restart local v8       #userPackageManager:Landroid/content/pm/PackageManager;
    :catch_35
    move-exception v2

    #@36
    .line 2234
    .restart local v2       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_32

    #@37
    .line 2226
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6       #packageInfo:Landroid/content/pm/PackageInfo;
    :cond_37
    add-int/lit8 v3, v3, 0x1

    #@39
    goto :goto_1e
.end method

.method private initUser(I)Landroid/accounts/AccountManagerService$UserAccounts;
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 260
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 261
    :try_start_3
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/accounts/AccountManagerService$UserAccounts;

    #@b
    .line 262
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    if-nez v0, :cond_20

    #@d
    .line 263
    new-instance v0, Landroid/accounts/AccountManagerService$UserAccounts;

    #@f
    .end local v0           #accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@11
    invoke-direct {v0, v1, p1}, Landroid/accounts/AccountManagerService$UserAccounts;-><init>(Landroid/content/Context;I)V

    #@14
    .line 264
    .restart local v0       #accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@19
    .line 265
    invoke-direct {p0, v0}, Landroid/accounts/AccountManagerService;->purgeOldGrants(Landroid/accounts/AccountManagerService$UserAccounts;)V

    #@1c
    .line 266
    const/4 v1, 0x1

    #@1d
    invoke-direct {p0, v0, v1}, Landroid/accounts/AccountManagerService;->validateAccountsInternal(Landroid/accounts/AccountManagerService$UserAccounts;Z)V

    #@20
    .line 268
    :cond_20
    monitor-exit v2

    #@21
    return-object v0

    #@22
    .line 269
    .end local v0           #accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method private insertAccountIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V
    .registers 9
    .parameter "accounts"
    .parameter "account"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2435
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@4
    move-result-object v4

    #@5
    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@7
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, [Landroid/accounts/Account;

    #@d
    .line 2436
    .local v0, accountsForType:[Landroid/accounts/Account;
    if-eqz v0, :cond_25

    #@f
    array-length v2, v0

    #@10
    .line 2437
    .local v2, oldLength:I
    :goto_10
    add-int/lit8 v4, v2, 0x1

    #@12
    new-array v1, v4, [Landroid/accounts/Account;

    #@14
    .line 2438
    .local v1, newAccountsForType:[Landroid/accounts/Account;
    if-eqz v0, :cond_19

    #@16
    .line 2439
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 2441
    :cond_19
    aput-object p2, v1, v2

    #@1b
    .line 2442
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@1e
    move-result-object v3

    #@1f
    iget-object v4, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@21
    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 2443
    return-void

    #@25
    .end local v1           #newAccountsForType:[Landroid/accounts/Account;
    .end local v2           #oldLength:I
    :cond_25
    move v2, v3

    #@26
    .line 2436
    goto :goto_10
.end method

.method private insertExtraLocked(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;)J
    .registers 9
    .parameter "db"
    .parameter "accountId"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 579
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 580
    .local v0, values:Landroid/content/ContentValues;
    const-string/jumbo v1, "key"

    #@8
    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 581
    const-string v1, "accounts_id"

    #@d
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@14
    .line 582
    const-string/jumbo v1, "value"

    #@17
    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 583
    const-string v1, "extras"

    #@1c
    const-string/jumbo v2, "key"

    #@1f
    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@22
    move-result-wide v1

    #@23
    return-wide v1
.end method

.method private invalidateAuthTokenLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "accounts"
    .parameter "db"
    .parameter "accountType"
    .parameter "authToken"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 798
    if-eqz p4, :cond_7

    #@5
    if-nez p3, :cond_8

    #@7
    .line 824
    :cond_7
    :goto_7
    return-void

    #@8
    .line 801
    :cond_8
    const-string v0, "SELECT authtokens._id, accounts.name, authtokens.type FROM accounts JOIN authtokens ON accounts._id = accounts_id WHERE authtoken = ? AND accounts.type = ?"

    #@a
    new-array v1, v1, [Ljava/lang/String;

    #@c
    aput-object p4, v1, v2

    #@e
    aput-object p3, v1, v3

    #@10
    invoke-virtual {p2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v9

    #@14
    .line 813
    .local v9, cursor:Landroid/database/Cursor;
    :goto_14
    :try_start_14
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_54

    #@1a
    .line 814
    const/4 v0, 0x0

    #@1b
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    #@1e
    move-result-wide v7

    #@1f
    .line 815
    .local v7, authTokenId:J
    const/4 v0, 0x1

    #@20
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v6

    #@24
    .line 816
    .local v6, accountName:Ljava/lang/String;
    const/4 v0, 0x2

    #@25
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    .line 817
    .local v4, authTokenType:Ljava/lang/String;
    const-string v0, "authtokens"

    #@2b
    new-instance v1, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v2, "_id="

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    const/4 v2, 0x0

    #@3f
    invoke-virtual {p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@42
    .line 818
    new-instance v3, Landroid/accounts/Account;

    #@44
    invoke-direct {v3, v6, p3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    const/4 v5, 0x0

    #@48
    move-object v0, p0

    #@49
    move-object v1, p1

    #@4a
    move-object v2, p2

    #@4b
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManagerService;->writeAuthTokenIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4e
    .catchall {:try_start_14 .. :try_end_4e} :catchall_4f

    #@4e
    goto :goto_14

    #@4f
    .line 822
    .end local v4           #authTokenType:Ljava/lang/String;
    .end local v6           #accountName:Ljava/lang/String;
    .end local v7           #authTokenId:J
    :catchall_4f
    move-exception v0

    #@50
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@53
    throw v0

    #@54
    :cond_54
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@57
    goto :goto_7
.end method

.method private newGrantCredentialsPermissionIntent(Landroid/accounts/Account;ILandroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 9
    .parameter "account"
    .parameter "uid"
    .parameter "response"
    .parameter "authTokenType"
    .parameter "authTokenLabel"

    #@0
    .prologue
    .line 1227
    new-instance v0, Landroid/content/Intent;

    #@2
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@4
    const-class v2, Landroid/accounts/GrantCredentialsPermissionActivity;

    #@6
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@9
    .line 1231
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@e
    .line 1232
    invoke-direct {p0, p1, p4, p2}, Landroid/accounts/AccountManagerService;->getCredentialPermissionNotificationId(Landroid/accounts/Account;Ljava/lang/String;I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@19
    .line 1235
    const-string v1, "account"

    #@1b
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1e
    .line 1236
    const-string v1, "authTokenType"

    #@20
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@23
    .line 1237
    const-string/jumbo v1, "response"

    #@26
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@29
    .line 1238
    const-string/jumbo v1, "uid"

    #@2c
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2f
    .line 1240
    return-object v0
.end method

.method private onResult(Landroid/accounts/IAccountManagerResponse;Landroid/os/Bundle;)V
    .registers 8
    .parameter "response"
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 1018
    if-nez p2, :cond_10

    #@3
    .line 1019
    const-string v1, "AccountManagerService"

    #@5
    const-string/jumbo v2, "the result is unexpectedly null"

    #@8
    new-instance v3, Ljava/lang/Exception;

    #@a
    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    #@d
    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    .line 1021
    :cond_10
    const-string v1, "AccountManagerService"

    #@12
    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_3c

    #@18
    .line 1022
    const-string v1, "AccountManagerService"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, " calling onResult() on response "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1026
    :cond_3c
    :try_start_3c
    invoke-interface {p1, p2}, Landroid/accounts/IAccountManagerResponse;->onResult(Landroid/os/Bundle;)V
    :try_end_3f
    .catch Landroid/os/RemoteException; {:try_start_3c .. :try_end_3f} :catch_40

    #@3f
    .line 1034
    :cond_3f
    :goto_3f
    return-void

    #@40
    .line 1027
    :catch_40
    move-exception v0

    #@41
    .line 1030
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "AccountManagerService"

    #@43
    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_3f

    #@49
    .line 1031
    const-string v1, "AccountManagerService"

    #@4b
    const-string v2, "failure while notifying response"

    #@4d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@50
    goto :goto_3f
.end method

.method private onUserRemoved(Landroid/content/Intent;)V
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    .line 401
    const-string v3, "android.intent.extra.user_handle"

    #@2
    const/4 v4, -0x1

    #@3
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v2

    #@7
    .line 402
    .local v2, userId:I
    const/4 v3, 0x1

    #@8
    if-ge v2, v3, :cond_b

    #@a
    .line 420
    :goto_a
    return-void

    #@b
    .line 405
    :cond_b
    iget-object v4, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@d
    monitor-enter v4

    #@e
    .line 406
    :try_start_e
    iget-object v3, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@10
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/accounts/AccountManagerService$UserAccounts;

    #@16
    .line 407
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    iget-object v3, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@18
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->remove(I)V

    #@1b
    .line 408
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_e .. :try_end_1c} :catchall_2b

    #@1c
    .line 409
    if-nez v0, :cond_2e

    #@1e
    .line 410
    new-instance v1, Ljava/io/File;

    #@20
    invoke-static {v2}, Landroid/accounts/AccountManagerService;->getDatabaseName(I)Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@27
    .line 411
    .local v1, dbFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@2a
    goto :goto_a

    #@2b
    .line 408
    .end local v0           #accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    .end local v1           #dbFile:Ljava/io/File;
    :catchall_2b
    move-exception v3

    #@2c
    :try_start_2c
    monitor-exit v4
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v3

    #@2e
    .line 415
    .restart local v0       #accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    :cond_2e
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@31
    move-result-object v4

    #@32
    monitor-enter v4

    #@33
    .line 416
    :try_start_33
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Landroid/accounts/AccountManagerService$DatabaseHelper;->close()V

    #@3a
    .line 417
    new-instance v1, Ljava/io/File;

    #@3c
    invoke-static {v2}, Landroid/accounts/AccountManagerService;->getDatabaseName(I)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@43
    .line 418
    .restart local v1       #dbFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@46
    .line 419
    monitor-exit v4

    #@47
    goto :goto_a

    #@48
    .end local v1           #dbFile:Ljava/io/File;
    :catchall_48
    move-exception v3

    #@49
    monitor-exit v4
    :try_end_4a
    .catchall {:try_start_33 .. :try_end_4a} :catchall_48

    #@4a
    throw v3
.end method

.method private permissionIsGranted(Landroid/accounts/Account;Ljava/lang/String;I)Z
    .registers 12
    .parameter "account"
    .parameter "authTokenType"
    .parameter "callerUid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2241
    invoke-direct {p0, p3}, Landroid/accounts/AccountManagerService;->inSystemImage(I)Z

    #@5
    move-result v2

    #@6
    .line 2242
    .local v2, inSystemImage:Z
    if-eqz p1, :cond_61

    #@8
    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@a
    invoke-direct {p0, v5, p3}, Landroid/accounts/AccountManagerService;->hasAuthenticatorUid(Ljava/lang/String;I)Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_61

    #@10
    move v0, v3

    #@11
    .line 2244
    .local v0, fromAuthenticator:Z
    :goto_11
    if-eqz p1, :cond_63

    #@13
    invoke-direct {p0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->hasExplicitlyGrantedPermission(Landroid/accounts/Account;Ljava/lang/String;I)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_63

    #@19
    move v1, v3

    #@1a
    .line 2246
    .local v1, hasExplicitGrants:Z
    :goto_1a
    const-string v5, "AccountManagerService"

    #@1c
    const/4 v6, 0x2

    #@1d
    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_59

    #@23
    .line 2247
    const-string v5, "AccountManagerService"

    #@25
    new-instance v6, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v7, "checkGrantsOrCallingUidAgainstAuthenticator: caller uid "

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    const-string v7, ", "

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    const-string v7, ": is authenticator? "

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    const-string v7, ", has explicit permission? "

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v6

    #@56
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2252
    :cond_59
    if-nez v0, :cond_5f

    #@5b
    if-nez v1, :cond_5f

    #@5d
    if-eqz v2, :cond_60

    #@5f
    :cond_5f
    move v4, v3

    #@60
    :cond_60
    return v4

    #@61
    .end local v0           #fromAuthenticator:Z
    .end local v1           #hasExplicitGrants:Z
    :cond_61
    move v0, v4

    #@62
    .line 2242
    goto :goto_11

    #@63
    .restart local v0       #fromAuthenticator:Z
    :cond_63
    move v1, v4

    #@64
    .line 2244
    goto :goto_1a
.end method

.method private purgeOldGrants(Landroid/accounts/AccountManagerService$UserAccounts;)V
    .registers 16
    .parameter "accounts"

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v12, 0x0

    #@2
    .line 281
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@5
    move-result-object v13

    #@6
    monitor-enter v13

    #@7
    .line 282
    :try_start_7
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@e
    move-result-object v0

    #@f
    .line 283
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "grants"

    #@11
    const/4 v2, 0x1

    #@12
    new-array v2, v2, [Ljava/lang/String;

    #@14
    const/4 v3, 0x0

    #@15
    const-string/jumbo v4, "uid"

    #@18
    aput-object v4, v2, v3

    #@1a
    const/4 v3, 0x0

    #@1b
    const/4 v4, 0x0

    #@1c
    const-string/jumbo v5, "uid"

    #@1f
    const/4 v6, 0x0

    #@20
    const/4 v7, 0x0

    #@21
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_24
    .catchall {:try_start_7 .. :try_end_24} :catchall_71

    #@24
    move-result-object v8

    #@25
    .line 287
    .local v8, cursor:Landroid/database/Cursor;
    :cond_25
    :goto_25
    :try_start_25
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_76

    #@2b
    .line 288
    const/4 v1, 0x0

    #@2c
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    #@2f
    move-result v10

    #@30
    .line 289
    .local v10, uid:I
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    #@32
    invoke-virtual {v1, v10}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    if-eqz v1, :cond_74

    #@38
    move v9, v11

    #@39
    .line 290
    .local v9, packageExists:Z
    :goto_39
    if-nez v9, :cond_25

    #@3b
    .line 293
    const-string v1, "AccountManagerService"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "deleting grants for UID "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    const-string v3, " because its package is no longer installed"

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 295
    const-string v1, "grants"

    #@5b
    const-string/jumbo v2, "uid=?"

    #@5e
    const/4 v3, 0x1

    #@5f
    new-array v3, v3, [Ljava/lang/String;

    #@61
    const/4 v4, 0x0

    #@62
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    aput-object v5, v3, v4

    #@68
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6b
    .catchall {:try_start_25 .. :try_end_6b} :catchall_6c

    #@6b
    goto :goto_25

    #@6c
    .line 299
    .end local v9           #packageExists:Z
    .end local v10           #uid:I
    :catchall_6c
    move-exception v1

    #@6d
    :try_start_6d
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@70
    throw v1

    #@71
    .line 301
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v8           #cursor:Landroid/database/Cursor;
    :catchall_71
    move-exception v1

    #@72
    monitor-exit v13
    :try_end_73
    .catchall {:try_start_6d .. :try_end_73} :catchall_71

    #@73
    throw v1

    #@74
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8       #cursor:Landroid/database/Cursor;
    .restart local v10       #uid:I
    :cond_74
    move v9, v12

    #@75
    .line 289
    goto :goto_39

    #@76
    .line 299
    .end local v10           #uid:I
    :cond_76
    :try_start_76
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@79
    .line 301
    monitor-exit v13
    :try_end_7a
    .catchall {:try_start_76 .. :try_end_7a} :catchall_71

    #@7a
    .line 302
    return-void
.end method

.method private purgeOldGrantsAll()V
    .registers 4

    #@0
    .prologue
    .line 273
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 274
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    :try_start_4
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@9
    move-result v1

    #@a
    if-ge v0, v1, :cond_1a

    #@c
    .line 275
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/accounts/AccountManagerService$UserAccounts;

    #@14
    invoke-direct {p0, v1}, Landroid/accounts/AccountManagerService;->purgeOldGrants(Landroid/accounts/AccountManagerService$UserAccounts;)V

    #@17
    .line 274
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_4

    #@1a
    .line 277
    :cond_1a
    monitor-exit v2

    #@1b
    .line 278
    return-void

    #@1c
    .line 277
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method private readPasswordInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/String;
    .registers 14
    .parameter "accounts"
    .parameter "account"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 447
    if-nez p2, :cond_5

    #@3
    move-object v1, v9

    #@4
    .line 460
    :goto_4
    return-object v1

    #@5
    .line 451
    :cond_5
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@8
    move-result-object v10

    #@9
    monitor-enter v10

    #@a
    .line 452
    :try_start_a
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@11
    move-result-object v0

    #@12
    .line 453
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "accounts"

    #@14
    const/4 v2, 0x1

    #@15
    new-array v2, v2, [Ljava/lang/String;

    #@17
    const/4 v3, 0x0

    #@18
    const-string/jumbo v4, "password"

    #@1b
    aput-object v4, v2, v3

    #@1d
    const-string/jumbo v3, "name=? AND type=?"

    #@20
    const/4 v4, 0x2

    #@21
    new-array v4, v4, [Ljava/lang/String;

    #@23
    const/4 v5, 0x0

    #@24
    iget-object v6, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@26
    aput-object v6, v4, v5

    #@28
    const/4 v5, 0x1

    #@29
    iget-object v6, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@2b
    aput-object v6, v4, v5

    #@2d
    const/4 v5, 0x0

    #@2e
    const/4 v6, 0x0

    #@2f
    const/4 v7, 0x0

    #@30
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_33
    .catchall {:try_start_a .. :try_end_33} :catchall_44

    #@33
    move-result-object v8

    #@34
    .line 457
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_34
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_47

    #@3a
    .line 458
    const/4 v1, 0x0

    #@3b
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3e
    .catchall {:try_start_34 .. :try_end_3e} :catchall_4d

    #@3e
    move-result-object v1

    #@3f
    .line 462
    :try_start_3f
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@42
    .line 458
    monitor-exit v10

    #@43
    goto :goto_4

    #@44
    .line 464
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v8           #cursor:Landroid/database/Cursor;
    :catchall_44
    move-exception v1

    #@45
    monitor-exit v10
    :try_end_46
    .catchall {:try_start_3f .. :try_end_46} :catchall_44

    #@46
    throw v1

    #@47
    .line 462
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8       #cursor:Landroid/database/Cursor;
    :cond_47
    :try_start_47
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@4a
    .line 460
    monitor-exit v10

    #@4b
    move-object v1, v9

    #@4c
    goto :goto_4

    #@4d
    .line 462
    :catchall_4d
    move-exception v1

    #@4e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@51
    throw v1
    :try_end_52
    .catchall {:try_start_47 .. :try_end_52} :catchall_44
.end method

.method private removeAccountFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V
    .registers 12
    .parameter "accounts"
    .parameter "account"

    #@0
    .prologue
    .line 2411
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@3
    move-result-object v7

    #@4
    iget-object v8, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@6
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v6

    #@a
    check-cast v6, [Landroid/accounts/Account;

    #@c
    .line 2412
    .local v6, oldAccountsForType:[Landroid/accounts/Account;
    if-eqz v6, :cond_35

    #@e
    .line 2413
    new-instance v5, Ljava/util/ArrayList;

    #@10
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@13
    .line 2414
    .local v5, newAccountsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    move-object v0, v6

    #@14
    .local v0, arr$:[Landroid/accounts/Account;
    array-length v3, v0

    #@15
    .local v3, len$:I
    const/4 v2, 0x0

    #@16
    .local v2, i$:I
    :goto_16
    if-ge v2, v3, :cond_26

    #@18
    aget-object v1, v0, v2

    #@1a
    .line 2415
    .local v1, curAccount:Landroid/accounts/Account;
    invoke-virtual {v1, p2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v7

    #@1e
    if-nez v7, :cond_23

    #@20
    .line 2416
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 2414
    :cond_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_16

    #@26
    .line 2419
    .end local v1           #curAccount:Landroid/accounts/Account;
    :cond_26
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@29
    move-result v7

    #@2a
    if-eqz v7, :cond_44

    #@2c
    .line 2420
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@2f
    move-result-object v7

    #@30
    iget-object v8, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@32
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    .line 2427
    .end local v0           #arr$:[Landroid/accounts/Account;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v5           #newAccountsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_35
    :goto_35
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$600(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    .line 2428
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    .line 2429
    return-void

    #@44
    .line 2422
    .restart local v0       #arr$:[Landroid/accounts/Account;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v5       #newAccountsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_44
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@47
    move-result v7

    #@48
    new-array v4, v7, [Landroid/accounts/Account;

    #@4a
    .line 2423
    .local v4, newAccountsForType:[Landroid/accounts/Account;
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@4d
    move-result-object v4

    #@4e
    .end local v4           #newAccountsForType:[Landroid/accounts/Account;
    check-cast v4, [Landroid/accounts/Account;

    #@50
    .line 2424
    .restart local v4       #newAccountsForType:[Landroid/accounts/Account;
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@53
    move-result-object v7

    #@54
    iget-object v8, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@56
    invoke-virtual {v7, v8, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@59
    goto :goto_35
.end method

.method private removeAccountInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V
    .registers 10
    .parameter "accounts"
    .parameter "account"

    #@0
    .prologue
    .line 760
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@3
    move-result-object v2

    #@4
    monitor-enter v2

    #@5
    .line 761
    :try_start_5
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@c
    move-result-object v0

    #@d
    .line 762
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "accounts"

    #@f
    const-string/jumbo v3, "name=? AND type=?"

    #@12
    const/4 v4, 0x2

    #@13
    new-array v4, v4, [Ljava/lang/String;

    #@15
    const/4 v5, 0x0

    #@16
    iget-object v6, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@18
    aput-object v6, v4, v5

    #@1a
    const/4 v5, 0x1

    #@1b
    iget-object v6, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@1d
    aput-object v6, v4, v5

    #@1f
    invoke-virtual {v0, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@22
    .line 764
    invoke-direct {p0, p1, p2}, Landroid/accounts/AccountManagerService;->removeAccountFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V

    #@25
    .line 765
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@28
    move-result v1

    #@29
    invoke-direct {p0, v1}, Landroid/accounts/AccountManagerService;->sendAccountsChangedBroadcast(I)V

    #@2c
    .line 766
    monitor-exit v2

    #@2d
    .line 767
    return-void

    #@2e
    .line 766
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2e
    move-exception v1

    #@2f
    monitor-exit v2
    :try_end_30
    .catchall {:try_start_5 .. :try_end_30} :catchall_2e

    #@30
    throw v1
.end method

.method private revokeAppPermission(Landroid/accounts/Account;Ljava/lang/String;I)V
    .registers 14
    .parameter "account"
    .parameter "authTokenType"
    .parameter "uid"

    #@0
    .prologue
    .line 2380
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_12

    #@4
    .line 2381
    :cond_4
    const-string v4, "AccountManagerService"

    #@6
    const-string/jumbo v5, "revokeAppPermission: called with invalid arguments"

    #@9
    new-instance v6, Ljava/lang/Exception;

    #@b
    invoke-direct {v6}, Ljava/lang/Exception;-><init>()V

    #@e
    invoke-static {v4, v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 2404
    :goto_11
    return-void

    #@12
    .line 2384
    :cond_12
    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    #@15
    move-result v4

    #@16
    invoke-virtual {p0, v4}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@19
    move-result-object v2

    #@1a
    .line 2385
    .local v2, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@1d
    move-result-object v5

    #@1e
    monitor-enter v5

    #@1f
    .line 2386
    :try_start_1f
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@26
    move-result-object v3

    #@27
    .line 2387
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_2a
    .catchall {:try_start_1f .. :try_end_2a} :catchall_6b

    #@2a
    .line 2389
    :try_start_2a
    invoke-direct {p0, v3, p1}, Landroid/accounts/AccountManagerService;->getAccountIdLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)J

    #@2d
    move-result-wide v0

    #@2e
    .line 2390
    .local v0, accountId:J
    const-wide/16 v6, 0x0

    #@30
    cmp-long v4, v0, v6

    #@32
    if-ltz v4, :cond_52

    #@34
    .line 2391
    const-string v4, "grants"

    #@36
    const-string v6, "accounts_id=? AND auth_token_type=? AND uid=?"

    #@38
    const/4 v7, 0x3

    #@39
    new-array v7, v7, [Ljava/lang/String;

    #@3b
    const/4 v8, 0x0

    #@3c
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@3f
    move-result-object v9

    #@40
    aput-object v9, v7, v8

    #@42
    const/4 v8, 0x1

    #@43
    aput-object p2, v7, v8

    #@45
    const/4 v8, 0x2

    #@46
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@49
    move-result-object v9

    #@4a
    aput-object v9, v7, v8

    #@4c
    invoke-virtual {v3, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@4f
    .line 2396
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_52
    .catchall {:try_start_2a .. :try_end_52} :catchall_6e

    #@52
    .line 2399
    :cond_52
    :try_start_52
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@55
    .line 2401
    invoke-direct {p0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->getCredentialPermissionNotificationId(Landroid/accounts/Account;Ljava/lang/String;I)Ljava/lang/Integer;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@5c
    move-result v4

    #@5d
    new-instance v6, Landroid/os/UserHandle;

    #@5f
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@62
    move-result v7

    #@63
    invoke-direct {v6, v7}, Landroid/os/UserHandle;-><init>(I)V

    #@66
    invoke-virtual {p0, v4, v6}, Landroid/accounts/AccountManagerService;->cancelNotification(ILandroid/os/UserHandle;)V

    #@69
    .line 2403
    monitor-exit v5

    #@6a
    goto :goto_11

    #@6b
    .end local v0           #accountId:J
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6b
    move-exception v4

    #@6c
    monitor-exit v5
    :try_end_6d
    .catchall {:try_start_52 .. :try_end_6d} :catchall_6b

    #@6d
    throw v4

    #@6e
    .line 2399
    .restart local v3       #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6e
    move-exception v4

    #@6f
    :try_start_6f
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@72
    throw v4
    :try_end_73
    .catchall {:try_start_6f .. :try_end_73} :catchall_6b
.end method

.method private saveAuthTokenToDatabase(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 15
    .parameter "accounts"
    .parameter "account"
    .parameter "type"
    .parameter "authToken"

    #@0
    .prologue
    .line 828
    if-eqz p2, :cond_4

    #@2
    if-nez p3, :cond_6

    #@4
    .line 829
    :cond_4
    const/4 v0, 0x0

    #@5
    .line 853
    :goto_5
    return v0

    #@6
    .line 831
    :cond_6
    invoke-direct {p0, p1, p2}, Landroid/accounts/AccountManagerService;->getSigninRequiredNotificationId(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@d
    move-result v0

    #@e
    new-instance v1, Landroid/os/UserHandle;

    #@10
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@13
    move-result v3

    #@14
    invoke-direct {v1, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@17
    invoke-virtual {p0, v0, v1}, Landroid/accounts/AccountManagerService;->cancelNotification(ILandroid/os/UserHandle;)V

    #@1a
    .line 833
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@1d
    move-result-object v9

    #@1e
    monitor-enter v9

    #@1f
    .line 834
    :try_start_1f
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@26
    move-result-object v2

    #@27
    .line 835
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_2a
    .catchall {:try_start_1f .. :try_end_2a} :catchall_3a

    #@2a
    .line 837
    :try_start_2a
    invoke-direct {p0, v2, p2}, Landroid/accounts/AccountManagerService;->getAccountIdLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)J
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_ae

    #@2d
    move-result-wide v6

    #@2e
    .line 838
    .local v6, accountId:J
    const-wide/16 v0, 0x0

    #@30
    cmp-long v0, v6, v0

    #@32
    if-gez v0, :cond_3d

    #@34
    .line 839
    const/4 v0, 0x0

    #@35
    .line 855
    :try_start_35
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@38
    .line 839
    monitor-exit v9

    #@39
    goto :goto_5

    #@3a
    .line 857
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v6           #accountId:J
    :catchall_3a
    move-exception v0

    #@3b
    monitor-exit v9
    :try_end_3c
    .catchall {:try_start_35 .. :try_end_3c} :catchall_3a

    #@3c
    throw v0

    #@3d
    .line 841
    .restart local v2       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v6       #accountId:J
    :cond_3d
    :try_start_3d
    const-string v0, "authtokens"

    #@3f
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v3, "accounts_id="

    #@46
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    const-string v3, " AND "

    #@50
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    const-string/jumbo v3, "type"

    #@57
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    const-string v3, "=?"

    #@5d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    const/4 v3, 0x1

    #@66
    new-array v3, v3, [Ljava/lang/String;

    #@68
    const/4 v4, 0x0

    #@69
    aput-object p3, v3, v4

    #@6b
    invoke-virtual {v2, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@6e
    .line 844
    new-instance v8, Landroid/content/ContentValues;

    #@70
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@73
    .line 845
    .local v8, values:Landroid/content/ContentValues;
    const-string v0, "accounts_id"

    #@75
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@78
    move-result-object v1

    #@79
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@7c
    .line 846
    const-string/jumbo v0, "type"

    #@7f
    invoke-virtual {v8, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 847
    const-string v0, "authtoken"

    #@84
    invoke-virtual {v8, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@87
    .line 848
    const-string v0, "authtokens"

    #@89
    const-string v1, "authtoken"

    #@8b
    invoke-virtual {v2, v0, v1, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@8e
    move-result-wide v0

    #@8f
    const-wide/16 v3, 0x0

    #@91
    cmp-long v0, v0, v3

    #@93
    if-ltz v0, :cond_a7

    #@95
    .line 849
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    #@98
    move-object v0, p0

    #@99
    move-object v1, p1

    #@9a
    move-object v3, p2

    #@9b
    move-object v4, p3

    #@9c
    move-object v5, p4

    #@9d
    .line 850
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManagerService;->writeAuthTokenIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a0
    .catchall {:try_start_3d .. :try_end_a0} :catchall_ae

    #@a0
    .line 851
    const/4 v0, 0x1

    #@a1
    .line 855
    :try_start_a1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@a4
    .line 851
    monitor-exit v9

    #@a5
    goto/16 :goto_5

    #@a7
    .line 853
    :cond_a7
    const/4 v0, 0x0

    #@a8
    .line 855
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@ab
    .line 853
    monitor-exit v9

    #@ac
    goto/16 :goto_5

    #@ae
    .line 855
    .end local v6           #accountId:J
    .end local v8           #values:Landroid/content/ContentValues;
    :catchall_ae
    move-exception v0

    #@af
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@b2
    throw v0
    :try_end_b3
    .catchall {:try_start_a1 .. :try_end_b3} :catchall_3a
.end method

.method private static scanArgs([Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "args"
    .parameter "value"

    #@0
    .prologue
    .line 2075
    if-eqz p0, :cond_14

    #@2
    .line 2076
    move-object v1, p0

    #@3
    .local v1, arr$:[Ljava/lang/String;
    array-length v3, v1

    #@4
    .local v3, len$:I
    const/4 v2, 0x0

    #@5
    .local v2, i$:I
    :goto_5
    if-ge v2, v3, :cond_14

    #@7
    aget-object v0, v1, v2

    #@9
    .line 2077
    .local v0, arg:Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_11

    #@f
    .line 2078
    const/4 v4, 0x1

    #@10
    .line 2082
    .end local v0           #arg:Ljava/lang/String;
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :goto_10
    return v4

    #@11
    .line 2076
    .restart local v0       #arg:Ljava/lang/String;
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_11
    add-int/lit8 v2, v2, 0x1

    #@13
    goto :goto_5

    #@14
    .line 2082
    .end local v0           #arg:Ljava/lang/String;
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_14
    const/4 v4, 0x0

    #@15
    goto :goto_10
.end method

.method private sendAccountsChangedBroadcast(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 941
    const-string v0, "AccountManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "the accounts changed, sending broadcast of "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    sget-object v2, Landroid/accounts/AccountManagerService;->ACCOUNTS_CHANGED_INTENT:Landroid/content/Intent;

    #@10
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 943
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@21
    sget-object v1, Landroid/accounts/AccountManagerService;->ACCOUNTS_CHANGED_INTENT:Landroid/content/Intent;

    #@23
    new-instance v2, Landroid/os/UserHandle;

    #@25
    invoke-direct {v2, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@2b
    .line 944
    return-void
.end method

.method private setPasswordInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 13
    .parameter "accounts"
    .parameter "account"
    .parameter "password"

    #@0
    .prologue
    .line 916
    if-nez p2, :cond_3

    #@2
    .line 938
    :goto_2
    return-void

    #@3
    .line 919
    :cond_3
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@6
    move-result-object v6

    #@7
    monitor-enter v6

    #@8
    .line 920
    :try_start_8
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@b
    move-result-object v5

    #@c
    invoke-virtual {v5}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@f
    move-result-object v3

    #@10
    .line 921
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_13
    .catchall {:try_start_8 .. :try_end_13} :catchall_56

    #@13
    .line 923
    :try_start_13
    new-instance v4, Landroid/content/ContentValues;

    #@15
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@18
    .line 924
    .local v4, values:Landroid/content/ContentValues;
    const-string/jumbo v5, "password"

    #@1b
    invoke-virtual {v4, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 925
    invoke-direct {p0, v3, p2}, Landroid/accounts/AccountManagerService;->getAccountIdLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)J

    #@21
    move-result-wide v0

    #@22
    .line 926
    .local v0, accountId:J
    const-wide/16 v7, 0x0

    #@24
    cmp-long v5, v0, v7

    #@26
    if-ltz v5, :cond_4a

    #@28
    .line 927
    const/4 v5, 0x1

    #@29
    new-array v2, v5, [Ljava/lang/String;

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@2f
    move-result-object v7

    #@30
    aput-object v7, v2, v5

    #@32
    .line 928
    .local v2, argsAccountId:[Ljava/lang/String;
    const-string v5, "accounts"

    #@34
    const-string v7, "_id=?"

    #@36
    invoke-virtual {v3, v5, v4, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@39
    .line 929
    const-string v5, "authtokens"

    #@3b
    const-string v7, "accounts_id=?"

    #@3d
    invoke-virtual {v3, v5, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@40
    .line 930
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    .line 931
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4a
    .catchall {:try_start_13 .. :try_end_4a} :catchall_59

    #@4a
    .line 934
    .end local v2           #argsAccountId:[Ljava/lang/String;
    :cond_4a
    :try_start_4a
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@4d
    .line 936
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@50
    move-result v5

    #@51
    invoke-direct {p0, v5}, Landroid/accounts/AccountManagerService;->sendAccountsChangedBroadcast(I)V

    #@54
    .line 937
    monitor-exit v6

    #@55
    goto :goto_2

    #@56
    .end local v0           #accountId:J
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v4           #values:Landroid/content/ContentValues;
    :catchall_56
    move-exception v5

    #@57
    monitor-exit v6
    :try_end_58
    .catchall {:try_start_4a .. :try_end_58} :catchall_56

    #@58
    throw v5

    #@59
    .line 934
    .restart local v3       #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_59
    move-exception v5

    #@5a
    :try_start_5a
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@5d
    throw v5
    :try_end_5e
    .catchall {:try_start_5a .. :try_end_5e} :catchall_56
.end method

.method private setUserdataInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 20
    .parameter "accounts"
    .parameter "account"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 984
    if-eqz p2, :cond_4

    #@2
    if-nez p3, :cond_5

    #@4
    .line 1015
    :cond_4
    :goto_4
    return-void

    #@5
    .line 987
    :cond_5
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@8
    move-result-object v14

    #@9
    monitor-enter v14

    #@a
    .line 988
    :try_start_a
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@11
    move-result-object v2

    #@12
    .line 989
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_15
    .catchall {:try_start_a .. :try_end_15} :catchall_26

    #@15
    .line 991
    :try_start_15
    move-object/from16 v0, p2

    #@17
    invoke-direct {p0, v2, v0}, Landroid/accounts/AccountManagerService;->getAccountIdLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)J
    :try_end_1a
    .catchall {:try_start_15 .. :try_end_1a} :catchall_8e

    #@1a
    move-result-wide v3

    #@1b
    .line 992
    .local v3, accountId:J
    const-wide/16 v5, 0x0

    #@1d
    cmp-long v1, v3, v5

    #@1f
    if-gez v1, :cond_29

    #@21
    .line 1012
    :try_start_21
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@24
    .line 993
    monitor-exit v14

    #@25
    goto :goto_4

    #@26
    .line 1014
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v3           #accountId:J
    :catchall_26
    move-exception v1

    #@27
    monitor-exit v14
    :try_end_28
    .catchall {:try_start_21 .. :try_end_28} :catchall_26

    #@28
    throw v1

    #@29
    .line 995
    .restart local v2       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3       #accountId:J
    :cond_29
    :try_start_29
    move-object/from16 v0, p3

    #@2b
    invoke-direct {p0, v2, v3, v4, v0}, Landroid/accounts/AccountManagerService;->getExtrasIdLocked(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)J

    #@2e
    move-result-wide v11

    #@2f
    .line 996
    .local v11, extrasId:J
    const-wide/16 v5, 0x0

    #@31
    cmp-long v1, v11, v5

    #@33
    if-gez v1, :cond_49

    #@35
    move-object v1, p0

    #@36
    move-object/from16 v5, p3

    #@38
    move-object/from16 v6, p4

    #@3a
    .line 997
    invoke-direct/range {v1 .. v6}, Landroid/accounts/AccountManagerService;->insertExtraLocked(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;Ljava/lang/String;)J
    :try_end_3d
    .catchall {:try_start_29 .. :try_end_3d} :catchall_8e

    #@3d
    move-result-wide v11

    #@3e
    .line 998
    const-wide/16 v5, 0x0

    #@40
    cmp-long v1, v11, v5

    #@42
    if-gez v1, :cond_78

    #@44
    .line 1012
    :try_start_44
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@47
    .line 999
    monitor-exit v14
    :try_end_48
    .catchall {:try_start_44 .. :try_end_48} :catchall_26

    #@48
    goto :goto_4

    #@49
    .line 1002
    :cond_49
    :try_start_49
    new-instance v13, Landroid/content/ContentValues;

    #@4b
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    #@4e
    .line 1003
    .local v13, values:Landroid/content/ContentValues;
    const-string/jumbo v1, "value"

    #@51
    move-object/from16 v0, p4

    #@53
    invoke-virtual {v13, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 1004
    const/4 v1, 0x1

    #@57
    const-string v5, "extras"

    #@59
    new-instance v6, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v7, "_id="

    #@60
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v6

    #@64
    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v6

    #@6c
    const/4 v7, 0x0

    #@6d
    invoke-virtual {v2, v5, v13, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_70
    .catchall {:try_start_49 .. :try_end_70} :catchall_8e

    #@70
    move-result v5

    #@71
    if-eq v1, v5, :cond_78

    #@73
    .line 1012
    :try_start_73
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@76
    .line 1005
    monitor-exit v14
    :try_end_77
    .catchall {:try_start_73 .. :try_end_77} :catchall_26

    #@77
    goto :goto_4

    #@78
    .end local v13           #values:Landroid/content/ContentValues;
    :cond_78
    move-object v5, p0

    #@79
    move-object/from16 v6, p1

    #@7b
    move-object v7, v2

    #@7c
    move-object/from16 v8, p2

    #@7e
    move-object/from16 v9, p3

    #@80
    move-object/from16 v10, p4

    #@82
    .line 1009
    :try_start_82
    invoke-virtual/range {v5 .. v10}, Landroid/accounts/AccountManagerService;->writeUserDataIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 1010
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_88
    .catchall {:try_start_82 .. :try_end_88} :catchall_8e

    #@88
    .line 1012
    :try_start_88
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@8b
    .line 1014
    monitor-exit v14

    #@8c
    goto/16 :goto_4

    #@8e
    .line 1012
    .end local v3           #accountId:J
    .end local v11           #extrasId:J
    :catchall_8e
    move-exception v1

    #@8f
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@92
    throw v1
    :try_end_93
    .catchall {:try_start_88 .. :try_end_93} :catchall_26
.end method

.method private static final stringArrayToString([Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 2407
    if-eqz p0, :cond_22

    #@2
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "["

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, ","

    #@f
    invoke-static {v1, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, "]"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    :goto_21
    return-object v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method private validateAccountsInternal(Landroid/accounts/AccountManagerService$UserAccounts;Z)V
    .registers 30
    .parameter "accounts"
    .parameter "invalidateAuthenticatorCache"

    #@0
    .prologue
    .line 322
    if-eqz p2, :cond_d

    #@2
    .line 323
    move-object/from16 v0, p0

    #@4
    iget-object v3, v0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@6
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@9
    move-result v4

    #@a
    invoke-interface {v3, v4}, Landroid/accounts/IAccountAuthenticatorCache;->invalidateCache(I)V

    #@d
    .line 326
    :cond_d
    invoke-static {}, Lcom/google/android/collect/Sets;->newHashSet()Ljava/util/HashSet;

    #@10
    move-result-object v24

    #@11
    .line 328
    .local v24, knownAuth:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/accounts/AuthenticatorDescription;>;"
    move-object/from16 v0, p0

    #@13
    iget-object v3, v0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@15
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@18
    move-result v4

    #@19
    invoke-interface {v3, v4}, Landroid/accounts/IAccountAuthenticatorCache;->getAllServices(I)Ljava/util/Collection;

    #@1c
    move-result-object v3

    #@1d
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@20
    move-result-object v22

    #@21
    .local v22, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_37

    #@27
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2a
    move-result-object v25

    #@2b
    check-cast v25, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@2d
    .line 329
    .local v25, service:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    move-object/from16 v0, v25

    #@2f
    iget-object v3, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@31
    move-object/from16 v0, v24

    #@33
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_21

    #@37
    .line 332
    .end local v25           #service:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    :cond_37
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@3a
    move-result-object v26

    #@3b
    monitor-enter v26

    #@3c
    .line 333
    :try_start_3c
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@43
    move-result-object v2

    #@44
    .line 334
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v11, 0x0

    #@45
    .line 335
    .local v11, accountDeleted:Z
    const-string v3, "accounts"

    #@47
    const/4 v4, 0x3

    #@48
    new-array v4, v4, [Ljava/lang/String;

    #@4a
    const/4 v5, 0x0

    #@4b
    const-string v6, "_id"

    #@4d
    aput-object v6, v4, v5

    #@4f
    const/4 v5, 0x1

    #@50
    const-string/jumbo v6, "type"

    #@53
    aput-object v6, v4, v5

    #@55
    const/4 v5, 0x2

    #@56
    const-string/jumbo v6, "name"

    #@59
    aput-object v6, v4, v5

    #@5b
    const/4 v5, 0x0

    #@5c
    const/4 v6, 0x0

    #@5d
    const/4 v7, 0x0

    #@5e
    const/4 v8, 0x0

    #@5f
    const/4 v9, 0x0

    #@60
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_63
    .catchall {:try_start_3c .. :try_end_63} :catchall_101

    #@63
    move-result-object v20

    #@64
    .line 339
    .local v20, cursor:Landroid/database/Cursor;
    :try_start_64
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@6b
    .line 340
    new-instance v16, Ljava/util/LinkedHashMap;

    #@6d
    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedHashMap;-><init>()V

    #@70
    .line 342
    .local v16, accountNamesByType:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :goto_70
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    #@73
    move-result v3

    #@74
    if-eqz v3, :cond_11d

    #@76
    .line 343
    const/4 v3, 0x0

    #@77
    move-object/from16 v0, v20

    #@79
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    #@7c
    move-result-wide v12

    #@7d
    .line 344
    .local v12, accountId:J
    const/4 v3, 0x1

    #@7e
    move-object/from16 v0, v20

    #@80
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@83
    move-result-object v17

    #@84
    .line 345
    .local v17, accountType:Ljava/lang/String;
    const/4 v3, 0x2

    #@85
    move-object/from16 v0, v20

    #@87
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v14

    #@8b
    .line 347
    .local v14, accountName:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/accounts/AuthenticatorDescription;->newKey(Ljava/lang/String;)Landroid/accounts/AuthenticatorDescription;

    #@8e
    move-result-object v3

    #@8f
    move-object/from16 v0, v24

    #@91
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@94
    move-result v3

    #@95
    if-nez v3, :cond_104

    #@97
    .line 348
    const-string v3, "AccountManagerService"

    #@99
    new-instance v4, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v5, "deleting account "

    #@a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v4

    #@a8
    const-string v5, " because type "

    #@aa
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v4

    #@ae
    move-object/from16 v0, v17

    #@b0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v4

    #@b4
    const-string v5, " no longer has a registered authenticator"

    #@b6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v4

    #@ba
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v4

    #@be
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    .line 350
    const-string v3, "accounts"

    #@c3
    new-instance v4, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v5, "_id="

    #@ca
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v4

    #@ce
    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v4

    #@d2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v4

    #@d6
    const/4 v5, 0x0

    #@d7
    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@da
    .line 351
    const/4 v11, 0x1

    #@db
    .line 352
    new-instance v10, Landroid/accounts/Account;

    #@dd
    move-object/from16 v0, v17

    #@df
    invoke-direct {v10, v14, v0}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e2
    .line 353
    .local v10, account:Landroid/accounts/Account;
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$600(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@e5
    move-result-object v3

    #@e6
    invoke-virtual {v3, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@e9
    .line 354
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@ec
    move-result-object v3

    #@ed
    invoke-virtual {v3, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f0
    .catchall {:try_start_64 .. :try_end_f0} :catchall_f1

    #@f0
    goto :goto_70

    #@f1
    .line 377
    .end local v10           #account:Landroid/accounts/Account;
    .end local v12           #accountId:J
    .end local v14           #accountName:Ljava/lang/String;
    .end local v16           #accountNamesByType:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    .end local v17           #accountType:Ljava/lang/String;
    .end local v22           #i$:Ljava/util/Iterator;
    :catchall_f1
    move-exception v3

    #@f2
    :try_start_f2
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    #@f5
    .line 378
    if-eqz v11, :cond_100

    #@f7
    .line 379
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@fa
    move-result v4

    #@fb
    move-object/from16 v0, p0

    #@fd
    invoke-direct {v0, v4}, Landroid/accounts/AccountManagerService;->sendAccountsChangedBroadcast(I)V

    #@100
    .line 377
    :cond_100
    throw v3

    #@101
    .line 382
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v11           #accountDeleted:Z
    .end local v20           #cursor:Landroid/database/Cursor;
    :catchall_101
    move-exception v3

    #@102
    monitor-exit v26
    :try_end_103
    .catchall {:try_start_f2 .. :try_end_103} :catchall_101

    #@103
    throw v3

    #@104
    .line 356
    .restart local v2       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11       #accountDeleted:Z
    .restart local v12       #accountId:J
    .restart local v14       #accountName:Ljava/lang/String;
    .restart local v16       #accountNamesByType:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    .restart local v17       #accountType:Ljava/lang/String;
    .restart local v20       #cursor:Landroid/database/Cursor;
    .restart local v22       #i$:Ljava/util/Iterator;
    :cond_104
    :try_start_104
    invoke-virtual/range {v16 .. v17}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@107
    move-result-object v15

    #@108
    check-cast v15, Ljava/util/ArrayList;

    #@10a
    .line 357
    .local v15, accountNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v15, :cond_118

    #@10c
    .line 358
    new-instance v15, Ljava/util/ArrayList;

    #@10e
    .end local v15           #accountNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    #@111
    .line 359
    .restart local v15       #accountNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, v16

    #@113
    move-object/from16 v1, v17

    #@115
    invoke-virtual {v0, v1, v15}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@118
    .line 361
    :cond_118
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11b
    goto/16 :goto_70

    #@11d
    .line 365
    .end local v12           #accountId:J
    .end local v14           #accountName:Ljava/lang/String;
    .end local v15           #accountNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17           #accountType:Ljava/lang/String;
    :cond_11d
    invoke-virtual/range {v16 .. v16}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    #@120
    move-result-object v3

    #@121
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@124
    move-result-object v22

    #@125
    .end local v22           #i$:Ljava/util/Iterator;
    :goto_125
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    #@128
    move-result v3

    #@129
    if-eqz v3, :cond_16f

    #@12b
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12e
    move-result-object v19

    #@12f
    check-cast v19, Ljava/util/Map$Entry;

    #@131
    .line 366
    .local v19, cur:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@134
    move-result-object v17

    #@135
    check-cast v17, Ljava/lang/String;

    #@137
    .line 367
    .restart local v17       #accountType:Ljava/lang/String;
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@13a
    move-result-object v15

    #@13b
    check-cast v15, Ljava/util/ArrayList;

    #@13d
    .line 368
    .restart local v15       #accountNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@140
    move-result v3

    #@141
    new-array v0, v3, [Landroid/accounts/Account;

    #@143
    move-object/from16 v18, v0

    #@145
    .line 369
    .local v18, accountsForType:[Landroid/accounts/Account;
    const/16 v21, 0x0

    #@147
    .line 370
    .local v21, i:I
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@14a
    move-result-object v23

    #@14b
    .local v23, i$:Ljava/util/Iterator;
    :goto_14b
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    #@14e
    move-result v3

    #@14f
    if-eqz v3, :cond_163

    #@151
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@154
    move-result-object v14

    #@155
    check-cast v14, Ljava/lang/String;

    #@157
    .line 371
    .restart local v14       #accountName:Ljava/lang/String;
    new-instance v3, Landroid/accounts/Account;

    #@159
    move-object/from16 v0, v17

    #@15b
    invoke-direct {v3, v14, v0}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@15e
    aput-object v3, v18, v21

    #@160
    .line 372
    add-int/lit8 v21, v21, 0x1

    #@162
    goto :goto_14b

    #@163
    .line 374
    .end local v14           #accountName:Ljava/lang/String;
    :cond_163
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@166
    move-result-object v3

    #@167
    move-object/from16 v0, v17

    #@169
    move-object/from16 v1, v18

    #@16b
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_16e
    .catchall {:try_start_104 .. :try_end_16e} :catchall_f1

    #@16e
    goto :goto_125

    #@16f
    .line 377
    .end local v15           #accountNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17           #accountType:Ljava/lang/String;
    .end local v18           #accountsForType:[Landroid/accounts/Account;
    .end local v19           #cur:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    .end local v21           #i:I
    .end local v23           #i$:Ljava/util/Iterator;
    :cond_16f
    :try_start_16f
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    #@172
    .line 378
    if-eqz v11, :cond_17d

    #@174
    .line 379
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@177
    move-result v3

    #@178
    move-object/from16 v0, p0

    #@17a
    invoke-direct {v0, v3}, Landroid/accounts/AccountManagerService;->sendAccountsChangedBroadcast(I)V

    #@17d
    .line 382
    :cond_17d
    monitor-exit v26
    :try_end_17e
    .catchall {:try_start_16f .. :try_end_17e} :catchall_101

    #@17e
    .line 383
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 10
    .parameter "account"
    .parameter "password"
    .parameter "extras"

    #@0
    .prologue
    .line 512
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_3d

    #@9
    .line 513
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "addAccount: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v5, ", caller\'s uid "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@23
    move-result v5

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v5, ", pid "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@31
    move-result v5

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 517
    :cond_3d
    if-nez p1, :cond_47

    #@3f
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@41
    const-string v4, "account is null"

    #@43
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v3

    #@47
    .line 518
    :cond_47
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@4a
    .line 520
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@4d
    move-result-object v0

    #@4e
    .line 522
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@51
    move-result-wide v1

    #@52
    .line 524
    .local v1, identityToken:J
    :try_start_52
    invoke-direct {p0, v0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->addAccountInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    :try_end_55
    .catchall {:try_start_52 .. :try_end_55} :catchall_5a

    #@55
    move-result v3

    #@56
    .line 526
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@59
    .line 524
    return v3

    #@5a
    .line 526
    :catchall_5a
    move-exception v3

    #@5b
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@5e
    throw v3
.end method

.method public addAcount(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZLandroid/os/Bundle;)V
    .registers 23
    .parameter "response"
    .parameter "accountType"
    .parameter "authTokenType"
    .parameter "requiredFeatures"
    .parameter "expectActivityLaunch"
    .parameter "optionsIn"

    #@0
    .prologue
    .line 1275
    const-string v1, "AccountManagerService"

    #@2
    const/4 v2, 0x2

    #@3
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_71

    #@9
    .line 1276
    const-string v1, "AccountManagerService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "addAccount: accountType "

    #@12
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    move-object/from16 v0, p2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v4, ", response "

    #@1e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    move-object/from16 v0, p1

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v4, ", authTokenType "

    #@2a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    move-object/from16 v0, p3

    #@30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v4, ", requiredFeatures "

    #@36
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-static/range {p4 .. p4}, Landroid/accounts/AccountManagerService;->stringArrayToString([Ljava/lang/String;)Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v4, ", expectActivityLaunch "

    #@44
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    move/from16 v0, p5

    #@4a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    const-string v4, ", caller\'s uid "

    #@50
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@57
    move-result v4

    #@58
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    const-string v4, ", pid "

    #@5e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@65
    move-result v4

    #@66
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 1284
    :cond_71
    if-nez p1, :cond_7c

    #@73
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@75
    const-string/jumbo v2, "response is null"

    #@78
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7b
    throw v1

    #@7c
    .line 1285
    :cond_7c
    if-nez p2, :cond_86

    #@7e
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@80
    const-string v2, "accountType is null"

    #@82
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@85
    throw v1

    #@86
    .line 1286
    :cond_86
    invoke-direct/range {p0 .. p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsPermission()V

    #@89
    .line 1288
    invoke-direct/range {p0 .. p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@8c
    move-result-object v3

    #@8d
    .line 1289
    .local v3, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@90
    move-result v14

    #@91
    .line 1290
    .local v14, pid:I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@94
    move-result v15

    #@95
    .line 1291
    .local v15, uid:I
    if-nez p6, :cond_c5

    #@97
    new-instance v10, Landroid/os/Bundle;

    #@99
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@9c
    .line 1292
    .local v10, options:Landroid/os/Bundle;
    :goto_9c
    const-string v1, "callerUid"

    #@9e
    invoke-virtual {v10, v1, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@a1
    .line 1293
    const-string v1, "callerPid"

    #@a3
    invoke-virtual {v10, v1, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@a6
    .line 1295
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@a9
    move-result-wide v12

    #@aa
    .line 1297
    .local v12, identityToken:J
    :try_start_aa
    new-instance v1, Landroid/accounts/AccountManagerService$5;

    #@ac
    const/4 v7, 0x1

    #@ad
    move-object/from16 v2, p0

    #@af
    move-object/from16 v4, p1

    #@b1
    move-object/from16 v5, p2

    #@b3
    move/from16 v6, p5

    #@b5
    move-object/from16 v8, p3

    #@b7
    move-object/from16 v9, p4

    #@b9
    move-object/from16 v11, p2

    #@bb
    invoke-direct/range {v1 .. v11}, Landroid/accounts/AccountManagerService$5;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLjava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    #@be
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$5;->bind()V
    :try_end_c1
    .catchall {:try_start_aa .. :try_end_c1} :catchall_c8

    #@c1
    .line 1314
    invoke-static {v12, v13}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@c4
    .line 1316
    return-void

    #@c5
    .end local v10           #options:Landroid/os/Bundle;
    .end local v12           #identityToken:J
    :cond_c5
    move-object/from16 v10, p6

    #@c7
    .line 1291
    goto :goto_9c

    #@c8
    .line 1314
    .restart local v10       #options:Landroid/os/Bundle;
    .restart local v12       #identityToken:J
    :catchall_c8
    move-exception v1

    #@c9
    invoke-static {v12, v13}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@cc
    throw v1
.end method

.method protected cancelNotification(ILandroid/os/UserHandle;)V
    .registers 7
    .parameter "id"
    .parameter "user"

    #@0
    .prologue
    .line 2187
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 2189
    .local v0, identityToken:J
    :try_start_4
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string/jumbo v3, "notification"

    #@9
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    check-cast v2, Landroid/app/NotificationManager;

    #@f
    const/4 v3, 0x0

    #@10
    invoke-virtual {v2, v3, p1, p2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_17

    #@13
    .line 2192
    invoke-static {v0, v1}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@16
    .line 2194
    return-void

    #@17
    .line 2192
    :catchall_17
    move-exception v2

    #@18
    invoke-static {v0, v1}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@1b
    throw v2
.end method

.method public clearPassword(Landroid/accounts/Account;)V
    .registers 8
    .parameter "account"

    #@0
    .prologue
    .line 947
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_3d

    #@9
    .line 948
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "clearPassword: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v5, ", caller\'s uid "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@23
    move-result v5

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v5, ", pid "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@31
    move-result v5

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 952
    :cond_3d
    if-nez p1, :cond_47

    #@3f
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@41
    const-string v4, "account is null"

    #@43
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v3

    #@47
    .line 953
    :cond_47
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsPermission()V

    #@4a
    .line 954
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@4d
    move-result-object v0

    #@4e
    .line 955
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@51
    move-result-wide v1

    #@52
    .line 957
    .local v1, identityToken:J
    const/4 v3, 0x0

    #@53
    :try_start_53
    invoke-direct {p0, v0, p1, v3}, Landroid/accounts/AccountManagerService;->setPasswordInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)V
    :try_end_56
    .catchall {:try_start_53 .. :try_end_56} :catchall_5a

    #@56
    .line 959
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@59
    .line 961
    return-void

    #@5a
    .line 959
    :catchall_5a
    move-exception v3

    #@5b
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@5e
    throw v3
.end method

.method public confirmCredentialsAsUser(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Landroid/os/Bundle;ZI)V
    .registers 18
    .parameter "response"
    .parameter "account"
    .parameter "options"
    .parameter "expectActivityLaunch"
    .parameter "userId"

    #@0
    .prologue
    .line 1323
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v1

    #@4
    move/from16 v0, p5

    #@6
    if-eq v0, v1, :cond_3b

    #@8
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@b
    move-result v1

    #@c
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@f
    move-result v2

    #@10
    if-eq v1, v2, :cond_3b

    #@12
    .line 1325
    new-instance v1, Ljava/lang/SecurityException;

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "User "

    #@1b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@22
    move-result v4

    #@23
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v4, " trying to confirm account credentials for "

    #@29
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    move/from16 v0, p5

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v1

    #@3b
    .line 1329
    :cond_3b
    const-string v1, "AccountManagerService"

    #@3d
    const/4 v2, 0x2

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_8e

    #@44
    .line 1330
    const-string v1, "AccountManagerService"

    #@46
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v4, "confirmCredentials: "

    #@4d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    const-string v4, ", response "

    #@57
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    const-string v4, ", expectActivityLaunch "

    #@61
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    move/from16 v0, p4

    #@67
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    const-string v4, ", caller\'s uid "

    #@6d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@74
    move-result v4

    #@75
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    const-string v4, ", pid "

    #@7b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@82
    move-result v4

    #@83
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v2

    #@8b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 1336
    :cond_8e
    if-nez p1, :cond_99

    #@90
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@92
    const-string/jumbo v2, "response is null"

    #@95
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@98
    throw v1

    #@99
    .line 1337
    :cond_99
    if-nez p2, :cond_a3

    #@9b
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9d
    const-string v2, "account is null"

    #@9f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a2
    throw v1

    #@a3
    .line 1338
    :cond_a3
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsPermission()V

    #@a6
    .line 1339
    move/from16 v0, p5

    #@a8
    invoke-virtual {p0, v0}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@ab
    move-result-object v3

    #@ac
    .line 1340
    .local v3, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@af
    move-result-wide v10

    #@b0
    .line 1342
    .local v10, identityToken:J
    :try_start_b0
    new-instance v1, Landroid/accounts/AccountManagerService$6;

    #@b2
    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@b4
    const/4 v7, 0x1

    #@b5
    move-object v2, p0

    #@b6
    move-object v4, p1

    #@b7
    move/from16 v6, p4

    #@b9
    move-object v8, p2

    #@ba
    move-object v9, p3

    #@bb
    invoke-direct/range {v1 .. v9}, Landroid/accounts/AccountManagerService$6;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLandroid/accounts/Account;Landroid/os/Bundle;)V

    #@be
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$6;->bind()V
    :try_end_c1
    .catchall {:try_start_b0 .. :try_end_c1} :catchall_c5

    #@c1
    .line 1353
    invoke-static {v10, v11}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@c4
    .line 1355
    return-void

    #@c5
    .line 1353
    :catchall_c5
    move-exception v1

    #@c6
    invoke-static {v10, v11}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@c9
    throw v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "fd"
    .parameter "fout"
    .parameter "args"

    #@0
    .prologue
    .line 2087
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_3f

    #@a
    .line 2089
    new-instance v0, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v1, "Permission Denial: can\'t dump AccountsManager from from pid="

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v1

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", uid="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, " without permission "

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    const-string v1, "android.permission.DUMP"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3e
    .line 2105
    :cond_3e
    return-void

    #@3f
    .line 2094
    :cond_3f
    const-string v0, "--checkin"

    #@41
    invoke-static {p3, v0}, Landroid/accounts/AccountManagerService;->scanArgs([Ljava/lang/String;Ljava/lang/String;)Z

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_4f

    #@47
    const-string v0, "-c"

    #@49
    invoke-static {p3, v0}, Landroid/accounts/AccountManagerService;->scanArgs([Ljava/lang/String;Ljava/lang/String;)Z

    #@4c
    move-result v0

    #@4d
    if-eqz v0, :cond_a1

    #@4f
    :cond_4f
    const/4 v5, 0x1

    #@50
    .line 2095
    .local v5, isCheckinRequest:Z
    :goto_50
    new-instance v3, Lcom/android/internal/util/IndentingPrintWriter;

    #@52
    const-string v0, "  "

    #@54
    invoke-direct {v3, p2, v0}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@57
    .line 2097
    .local v3, ipw:Lcom/android/internal/util/IndentingPrintWriter;
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserManager()Landroid/os/UserManager;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@5e
    move-result-object v8

    #@5f
    .line 2098
    .local v8, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@62
    move-result-object v6

    #@63
    .local v6, i$:Ljava/util/Iterator;
    :goto_63
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_3e

    #@69
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6c
    move-result-object v7

    #@6d
    check-cast v7, Landroid/content/pm/UserInfo;

    #@6f
    .line 2099
    .local v7, user:Landroid/content/pm/UserInfo;
    new-instance v0, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v1, "User "

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v0

    #@7a
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    const-string v1, ":"

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v0

    #@88
    invoke-virtual {v3, v0}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    #@8b
    .line 2100
    invoke-virtual {v3}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@8e
    .line 2101
    iget v0, v7, Landroid/content/pm/UserInfo;->id:I

    #@90
    invoke-virtual {p0, v0}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@93
    move-result-object v1

    #@94
    move-object v0, p0

    #@95
    move-object v2, p1

    #@96
    move-object v4, p3

    #@97
    invoke-direct/range {v0 .. v5}, Landroid/accounts/AccountManagerService;->dumpUser(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Z)V

    #@9a
    .line 2102
    invoke-virtual {v3}, Lcom/android/internal/util/IndentingPrintWriter;->println()V

    #@9d
    .line 2103
    invoke-virtual {v3}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@a0
    goto :goto_63

    #@a1
    .line 2094
    .end local v3           #ipw:Lcom/android/internal/util/IndentingPrintWriter;
    .end local v5           #isCheckinRequest:Z
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #user:Landroid/content/pm/UserInfo;
    .end local v8           #users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_a1
    const/4 v5, 0x0

    #@a2
    goto :goto_50
.end method

.method public editProperties(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Z)V
    .registers 14
    .parameter "response"
    .parameter "accountType"
    .parameter "expectActivityLaunch"

    #@0
    .prologue
    .line 1395
    const-string v0, "AccountManagerService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_51

    #@9
    .line 1396
    const-string v0, "AccountManagerService"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "editProperties: accountType "

    #@12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v3, ", response "

    #@1c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v3, ", expectActivityLaunch "

    #@26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v3, ", caller\'s uid "

    #@30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@37
    move-result v3

    #@38
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    const-string v3, ", pid "

    #@3e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@45
    move-result v3

    #@46
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 1402
    :cond_51
    if-nez p1, :cond_5c

    #@53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@55
    const-string/jumbo v1, "response is null"

    #@58
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v0

    #@5c
    .line 1403
    :cond_5c
    if-nez p2, :cond_66

    #@5e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@60
    const-string v1, "accountType is null"

    #@62
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@65
    throw v0

    #@66
    .line 1404
    :cond_66
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsPermission()V

    #@69
    .line 1405
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@6c
    move-result-object v2

    #@6d
    .line 1406
    .local v2, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@70
    move-result-wide v8

    #@71
    .line 1408
    .local v8, identityToken:J
    :try_start_71
    new-instance v0, Landroid/accounts/AccountManagerService$8;

    #@73
    const/4 v6, 0x1

    #@74
    move-object v1, p0

    #@75
    move-object v3, p1

    #@76
    move-object v4, p2

    #@77
    move v5, p3

    #@78
    move-object v7, p2

    #@79
    invoke-direct/range {v0 .. v7}, Landroid/accounts/AccountManagerService$8;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLjava/lang/String;)V

    #@7c
    invoke-virtual {v0}, Landroid/accounts/AccountManagerService$8;->bind()V
    :try_end_7f
    .catchall {:try_start_71 .. :try_end_7f} :catchall_83

    #@7f
    .line 1419
    invoke-static {v8, v9}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@82
    .line 1421
    return-void

    #@83
    .line 1419
    :catchall_83
    move-exception v0

    #@84
    invoke-static {v8, v9}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@87
    throw v0
.end method

.method public getAccounts(I)[Landroid/accounts/Account;
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 1521
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkReadAccountsPermission()V

    #@3
    .line 1522
    invoke-virtual {p0, p1}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@6
    move-result-object v0

    #@7
    .line 1523
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@a
    move-result-wide v1

    #@b
    .line 1525
    .local v1, identityToken:J
    :try_start_b
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@e
    move-result-object v4

    #@f
    monitor-enter v4
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_1d

    #@10
    .line 1526
    const/4 v3, 0x0

    #@11
    :try_start_11
    invoke-virtual {p0, v0, v3}, Landroid/accounts/AccountManagerService;->getAccountsFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;

    #@14
    move-result-object v3

    #@15
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_11 .. :try_end_16} :catchall_1a

    #@16
    .line 1529
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@19
    .line 1526
    return-object v3

    #@1a
    .line 1527
    :catchall_1a
    move-exception v3

    #@1b
    :try_start_1b
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    #@1c
    :try_start_1c
    throw v3
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1d

    #@1d
    .line 1529
    :catchall_1d
    move-exception v3

    #@1e
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@21
    throw v3
.end method

.method public getAccounts(Ljava/lang/String;)[Landroid/accounts/Account;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1606
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/accounts/AccountManagerService;->getAccountsAsUser(Ljava/lang/String;I)[Landroid/accounts/Account;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getAccountsAsUser(Ljava/lang/String;I)[Landroid/accounts/Account;
    .registers 9
    .parameter "type"
    .parameter "userId"

    #@0
    .prologue
    .line 1581
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v3

    #@4
    if-eq p2, v3, :cond_37

    #@6
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@9
    move-result v3

    #@a
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@d
    move-result v4

    #@e
    if-eq v3, v4, :cond_37

    #@10
    .line 1583
    new-instance v3, Ljava/lang/SecurityException;

    #@12
    new-instance v4, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "User "

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@20
    move-result v5

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, " trying to get account for "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@36
    throw v3

    #@37
    .line 1587
    :cond_37
    const-string v3, "AccountManagerService"

    #@39
    const/4 v4, 0x2

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_74

    #@40
    .line 1588
    const-string v3, "AccountManagerService"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "getAccounts: accountType "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    const-string v5, ", caller\'s uid "

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5a
    move-result v5

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    const-string v5, ", pid "

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@68
    move-result v5

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v4

    #@71
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 1592
    :cond_74
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkReadAccountsPermission()V

    #@77
    .line 1593
    invoke-virtual {p0, p2}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@7a
    move-result-object v0

    #@7b
    .line 1594
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@7e
    move-result-wide v1

    #@7f
    .line 1596
    .local v1, identityToken:J
    :try_start_7f
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@82
    move-result-object v4

    #@83
    monitor-enter v4
    :try_end_84
    .catchall {:try_start_7f .. :try_end_84} :catchall_90

    #@84
    .line 1597
    :try_start_84
    invoke-virtual {p0, v0, p1}, Landroid/accounts/AccountManagerService;->getAccountsFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;

    #@87
    move-result-object v3

    #@88
    monitor-exit v4
    :try_end_89
    .catchall {:try_start_84 .. :try_end_89} :catchall_8d

    #@89
    .line 1600
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@8c
    .line 1597
    return-object v3

    #@8d
    .line 1598
    :catchall_8d
    move-exception v3

    #@8e
    :try_start_8e
    monitor-exit v4
    :try_end_8f
    .catchall {:try_start_8e .. :try_end_8f} :catchall_8d

    #@8f
    :try_start_8f
    throw v3
    :try_end_90
    .catchall {:try_start_8f .. :try_end_90} :catchall_90

    #@90
    .line 1600
    :catchall_90
    move-exception v3

    #@91
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@94
    throw v3
.end method

.method public getAccountsByFeatures(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 14
    .parameter "response"
    .parameter "type"
    .parameter "features"

    #@0
    .prologue
    .line 1611
    const-string v0, "AccountManagerService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_55

    #@9
    .line 1612
    const-string v0, "AccountManagerService"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "getAccounts: accountType "

    #@12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v3, ", response "

    #@1c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v3, ", features "

    #@26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-static {p3}, Landroid/accounts/AccountManagerService;->stringArrayToString([Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v3, ", caller\'s uid "

    #@34
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3b
    move-result v3

    #@3c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    const-string v3, ", pid "

    #@42
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@49
    move-result v3

    #@4a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 1618
    :cond_55
    if-nez p1, :cond_60

    #@57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@59
    const-string/jumbo v1, "response is null"

    #@5c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v0

    #@60
    .line 1619
    :cond_60
    if-nez p2, :cond_6a

    #@62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@64
    const-string v1, "accountType is null"

    #@66
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@69
    throw v0

    #@6a
    .line 1620
    :cond_6a
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkReadAccountsPermission()V

    #@6d
    .line 1621
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@70
    move-result-object v2

    #@71
    .line 1622
    .local v2, userAccounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@74
    move-result-wide v7

    #@75
    .line 1624
    .local v7, identityToken:J
    if-eqz p3, :cond_7a

    #@77
    :try_start_77
    array-length v0, p3

    #@78
    if-nez v0, :cond_9d

    #@7a
    .line 1626
    :cond_7a
    invoke-static {v2}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@7d
    move-result-object v1

    #@7e
    monitor-enter v1
    :try_end_7f
    .catchall {:try_start_77 .. :try_end_7f} :catchall_98

    #@7f
    .line 1627
    :try_start_7f
    invoke-virtual {p0, v2, p2}, Landroid/accounts/AccountManagerService;->getAccountsFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;

    #@82
    move-result-object v6

    #@83
    .line 1628
    .local v6, accounts:[Landroid/accounts/Account;
    monitor-exit v1
    :try_end_84
    .catchall {:try_start_7f .. :try_end_84} :catchall_95

    #@84
    .line 1629
    :try_start_84
    new-instance v9, Landroid/os/Bundle;

    #@86
    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    #@89
    .line 1630
    .local v9, result:Landroid/os/Bundle;
    const-string v0, "accounts"

    #@8b
    invoke-virtual {v9, v0, v6}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    #@8e
    .line 1631
    invoke-direct {p0, p1, v9}, Landroid/accounts/AccountManagerService;->onResult(Landroid/accounts/IAccountManagerResponse;Landroid/os/Bundle;)V
    :try_end_91
    .catchall {:try_start_84 .. :try_end_91} :catchall_98

    #@91
    .line 1636
    .end local v6           #accounts:[Landroid/accounts/Account;
    .end local v9           #result:Landroid/os/Bundle;
    :goto_91
    invoke-static {v7, v8}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@94
    .line 1638
    return-void

    #@95
    .line 1628
    :catchall_95
    move-exception v0

    #@96
    :try_start_96
    monitor-exit v1
    :try_end_97
    .catchall {:try_start_96 .. :try_end_97} :catchall_95

    #@97
    :try_start_97
    throw v0
    :try_end_98
    .catchall {:try_start_97 .. :try_end_98} :catchall_98

    #@98
    .line 1636
    :catchall_98
    move-exception v0

    #@99
    invoke-static {v7, v8}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@9c
    throw v0

    #@9d
    .line 1634
    :cond_9d
    :try_start_9d
    new-instance v0, Landroid/accounts/AccountManagerService$GetAccountsByTypeAndFeatureSession;

    #@9f
    move-object v1, p0

    #@a0
    move-object v3, p1

    #@a1
    move-object v4, p2

    #@a2
    move-object v5, p3

    #@a3
    invoke-direct/range {v0 .. v5}, Landroid/accounts/AccountManagerService$GetAccountsByTypeAndFeatureSession;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;[Ljava/lang/String;)V

    #@a6
    invoke-virtual {v0}, Landroid/accounts/AccountManagerService$GetAccountsByTypeAndFeatureSession;->bind()V
    :try_end_a9
    .catchall {:try_start_9d .. :try_end_a9} :catchall_98

    #@a9
    goto :goto_91
.end method

.method protected getAccountsFromCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Ljava/lang/String;)[Landroid/accounts/Account;
    .registers 9
    .parameter "userAccounts"
    .parameter "accountType"

    #@0
    .prologue
    .line 2446
    if-eqz p2, :cond_19

    #@2
    .line 2447
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, [Landroid/accounts/Account;

    #@c
    .line 2448
    .local v0, accounts:[Landroid/accounts/Account;
    if-nez v0, :cond_11

    #@e
    .line 2449
    sget-object v4, Landroid/accounts/AccountManagerService;->EMPTY_ACCOUNT_ARRAY:[Landroid/accounts/Account;

    #@10
    .line 2468
    .end local v0           #accounts:[Landroid/accounts/Account;
    :goto_10
    return-object v4

    #@11
    .line 2451
    .restart local v0       #accounts:[Landroid/accounts/Account;
    :cond_11
    array-length v4, v0

    #@12
    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@15
    move-result-object v4

    #@16
    check-cast v4, [Landroid/accounts/Account;

    #@18
    goto :goto_10

    #@19
    .line 2454
    .end local v0           #accounts:[Landroid/accounts/Account;
    :cond_19
    const/4 v3, 0x0

    #@1a
    .line 2455
    .local v3, totalLength:I
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@21
    move-result-object v4

    #@22
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@25
    move-result-object v2

    #@26
    .local v2, i$:Ljava/util/Iterator;
    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v4

    #@2a
    if-eqz v4, :cond_35

    #@2c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v0

    #@30
    check-cast v0, [Landroid/accounts/Account;

    #@32
    .line 2456
    .restart local v0       #accounts:[Landroid/accounts/Account;
    array-length v4, v0

    #@33
    add-int/2addr v3, v4

    #@34
    goto :goto_26

    #@35
    .line 2458
    .end local v0           #accounts:[Landroid/accounts/Account;
    :cond_35
    if-nez v3, :cond_3a

    #@37
    .line 2459
    sget-object v4, Landroid/accounts/AccountManagerService;->EMPTY_ACCOUNT_ARRAY:[Landroid/accounts/Account;

    #@39
    goto :goto_10

    #@3a
    .line 2461
    :cond_3a
    new-array v0, v3, [Landroid/accounts/Account;

    #@3c
    .line 2462
    .restart local v0       #accounts:[Landroid/accounts/Account;
    const/4 v3, 0x0

    #@3d
    .line 2463
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$500(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@44
    move-result-object v4

    #@45
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v2

    #@49
    :goto_49
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v4

    #@4d
    if-eqz v4, :cond_5d

    #@4f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v1

    #@53
    check-cast v1, [Landroid/accounts/Account;

    #@55
    .line 2464
    .local v1, accountsOfType:[Landroid/accounts/Account;
    const/4 v4, 0x0

    #@56
    array-length v5, v1

    #@57
    invoke-static {v1, v4, v0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5a
    .line 2466
    array-length v4, v1

    #@5b
    add-int/2addr v3, v4

    #@5c
    goto :goto_49

    #@5d
    .end local v1           #accountsOfType:[Landroid/accounts/Account;
    :cond_5d
    move-object v4, v0

    #@5e
    .line 2468
    goto :goto_10
.end method

.method public getAllAccounts()[Landroid/accounts/AccountAndUser;
    .registers 5

    #@0
    .prologue
    .line 1551
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserManager()Landroid/os/UserManager;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@7
    move-result-object v2

    #@8
    .line 1552
    .local v2, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@b
    move-result v3

    #@c
    new-array v1, v3, [I

    #@e
    .line 1553
    .local v1, userIds:[I
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    array-length v3, v1

    #@10
    if-ge v0, v3, :cond_1f

    #@12
    .line 1554
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    check-cast v3, Landroid/content/pm/UserInfo;

    #@18
    iget v3, v3, Landroid/content/pm/UserInfo;->id:I

    #@1a
    aput v3, v1, v0

    #@1c
    .line 1553
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_f

    #@1f
    .line 1556
    :cond_1f
    invoke-direct {p0, v1}, Landroid/accounts/AccountManagerService;->getAccounts([I)[Landroid/accounts/AccountAndUser;

    #@22
    move-result-object v3

    #@23
    return-object v3
.end method

.method public getAuthToken(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZZLandroid/os/Bundle;)V
    .registers 30
    .parameter "response"
    .parameter "account"
    .parameter "authTokenType"
    .parameter "notifyOnAuthFailure"
    .parameter "expectActivityLaunch"
    .parameter "loginOptionsIn"

    #@0
    .prologue
    .line 1082
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_6f

    #@9
    .line 1083
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v6, "getAuthToken: "

    #@12
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    move-object/from16 v0, p2

    #@18
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v6, ", response "

    #@1e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    move-object/from16 v0, p1

    #@24
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v6, ", authTokenType "

    #@2a
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    move-object/from16 v0, p3

    #@30
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v6, ", notifyOnAuthFailure "

    #@36
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    move/from16 v0, p4

    #@3c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    const-string v6, ", expectActivityLaunch "

    #@42
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    move/from16 v0, p5

    #@48
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    const-string v6, ", caller\'s uid "

    #@4e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@55
    move-result v6

    #@56
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    const-string v6, ", pid "

    #@5c
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@63
    move-result v6

    #@64
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 1091
    :cond_6f
    if-nez p1, :cond_7a

    #@71
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@73
    const-string/jumbo v4, "response is null"

    #@76
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@79
    throw v3

    #@7a
    .line 1092
    :cond_7a
    if-nez p2, :cond_84

    #@7c
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@7e
    const-string v4, "account is null"

    #@80
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@83
    throw v3

    #@84
    .line 1093
    :cond_84
    if-nez p3, :cond_8e

    #@86
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@88
    const-string v4, "authTokenType is null"

    #@8a
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8d
    throw v3

    #@8e
    .line 1094
    :cond_8e
    const/4 v3, 0x1

    #@8f
    new-array v3, v3, [Ljava/lang/String;

    #@91
    const/4 v4, 0x0

    #@92
    const-string v6, "android.permission.USE_CREDENTIALS"

    #@94
    aput-object v6, v3, v4

    #@96
    move-object/from16 v0, p0

    #@98
    invoke-direct {v0, v3}, Landroid/accounts/AccountManagerService;->checkBinderPermission([Ljava/lang/String;)V

    #@9b
    .line 1095
    invoke-direct/range {p0 .. p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@9e
    move-result-object v5

    #@9f
    .line 1097
    .local v5, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    move-object/from16 v0, p0

    #@a1
    iget-object v3, v0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@a3
    move-object/from16 v0, p2

    #@a5
    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@a7
    invoke-static {v4}, Landroid/accounts/AuthenticatorDescription;->newKey(Ljava/lang/String;)Landroid/accounts/AuthenticatorDescription;

    #@aa
    move-result-object v4

    #@ab
    invoke-static {v5}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@ae
    move-result v6

    #@af
    invoke-interface {v3, v4, v6}, Landroid/accounts/IAccountAuthenticatorCache;->getServiceInfo(Landroid/accounts/AuthenticatorDescription;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@b2
    move-result-object v19

    #@b3
    .line 1099
    .local v19, authenticatorInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    if-eqz v19, :cond_137

    #@b5
    move-object/from16 v0, v19

    #@b7
    iget-object v3, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@b9
    check-cast v3, Landroid/accounts/AuthenticatorDescription;

    #@bb
    iget-boolean v3, v3, Landroid/accounts/AuthenticatorDescription;->customTokens:Z

    #@bd
    if-eqz v3, :cond_137

    #@bf
    const/16 v16, 0x1

    #@c1
    .line 1103
    .local v16, customTokens:Z
    :goto_c1
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c4
    move-result v15

    #@c5
    .line 1104
    .local v15, callerUid:I
    if-nez v16, :cond_d3

    #@c7
    move-object/from16 v0, p0

    #@c9
    move-object/from16 v1, p2

    #@cb
    move-object/from16 v2, p3

    #@cd
    invoke-direct {v0, v1, v2, v15}, Landroid/accounts/AccountManagerService;->permissionIsGranted(Landroid/accounts/Account;Ljava/lang/String;I)Z

    #@d0
    move-result v3

    #@d1
    if-eqz v3, :cond_13a

    #@d3
    :cond_d3
    const/4 v14, 0x1

    #@d4
    .line 1107
    .local v14, permissionGranted:Z
    :goto_d4
    if-nez p6, :cond_13c

    #@d6
    new-instance v10, Landroid/os/Bundle;

    #@d8
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@db
    .line 1110
    .local v10, loginOptions:Landroid/os/Bundle;
    :goto_db
    const-string v3, "callerUid"

    #@dd
    invoke-virtual {v10, v3, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@e0
    .line 1111
    const-string v3, "callerPid"

    #@e2
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@e5
    move-result v4

    #@e6
    invoke-virtual {v10, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@e9
    .line 1112
    if-eqz p4, :cond_f2

    #@eb
    .line 1113
    const-string/jumbo v3, "notifyOnAuthFailure"

    #@ee
    const/4 v4, 0x1

    #@ef
    invoke-virtual {v10, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@f2
    .line 1116
    :cond_f2
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@f5
    move-result-wide v20

    #@f6
    .line 1120
    .local v20, identityToken:J
    if-nez v16, :cond_13f

    #@f8
    if-eqz v14, :cond_13f

    #@fa
    .line 1121
    :try_start_fa
    move-object/from16 v0, p0

    #@fc
    move-object/from16 v1, p2

    #@fe
    move-object/from16 v2, p3

    #@100
    invoke-virtual {v0, v5, v1, v2}, Landroid/accounts/AccountManagerService;->readAuthTokenInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    #@103
    move-result-object v18

    #@104
    .line 1122
    .local v18, authToken:Ljava/lang/String;
    if-eqz v18, :cond_13f

    #@106
    .line 1123
    new-instance v22, Landroid/os/Bundle;

    #@108
    invoke-direct/range {v22 .. v22}, Landroid/os/Bundle;-><init>()V

    #@10b
    .line 1124
    .local v22, result:Landroid/os/Bundle;
    const-string v3, "authtoken"

    #@10d
    move-object/from16 v0, v22

    #@10f
    move-object/from16 v1, v18

    #@111
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@114
    .line 1125
    const-string v3, "authAccount"

    #@116
    move-object/from16 v0, p2

    #@118
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@11a
    move-object/from16 v0, v22

    #@11c
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@11f
    .line 1126
    const-string v3, "accountType"

    #@121
    move-object/from16 v0, p2

    #@123
    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@125
    move-object/from16 v0, v22

    #@127
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@12a
    .line 1127
    move-object/from16 v0, p0

    #@12c
    move-object/from16 v1, p1

    #@12e
    move-object/from16 v2, v22

    #@130
    invoke-direct {v0, v1, v2}, Landroid/accounts/AccountManagerService;->onResult(Landroid/accounts/IAccountManagerResponse;Landroid/os/Bundle;)V
    :try_end_133
    .catchall {:try_start_fa .. :try_end_133} :catchall_15b

    #@133
    .line 1191
    .end local v18           #authToken:Ljava/lang/String;
    .end local v22           #result:Landroid/os/Bundle;
    :goto_133
    invoke-static/range {v20 .. v21}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@136
    .line 1193
    return-void

    #@137
    .line 1099
    .end local v10           #loginOptions:Landroid/os/Bundle;
    .end local v14           #permissionGranted:Z
    .end local v15           #callerUid:I
    .end local v16           #customTokens:Z
    .end local v20           #identityToken:J
    :cond_137
    const/16 v16, 0x0

    #@139
    goto :goto_c1

    #@13a
    .line 1104
    .restart local v15       #callerUid:I
    .restart local v16       #customTokens:Z
    :cond_13a
    const/4 v14, 0x0

    #@13b
    goto :goto_d4

    #@13c
    .restart local v14       #permissionGranted:Z
    :cond_13c
    move-object/from16 v10, p6

    #@13e
    .line 1107
    goto :goto_db

    #@13f
    .line 1132
    .restart local v10       #loginOptions:Landroid/os/Bundle;
    .restart local v20       #identityToken:J
    :cond_13f
    :try_start_13f
    new-instance v3, Landroid/accounts/AccountManagerService$4;

    #@141
    move-object/from16 v0, p2

    #@143
    iget-object v7, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@145
    const/4 v9, 0x0

    #@146
    move-object/from16 v4, p0

    #@148
    move-object/from16 v6, p1

    #@14a
    move/from16 v8, p5

    #@14c
    move-object/from16 v11, p2

    #@14e
    move-object/from16 v12, p3

    #@150
    move/from16 v13, p4

    #@152
    move-object/from16 v17, v5

    #@154
    invoke-direct/range {v3 .. v17}, Landroid/accounts/AccountManagerService$4;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLandroid/os/Bundle;Landroid/accounts/Account;Ljava/lang/String;ZZIZLandroid/accounts/AccountManagerService$UserAccounts;)V

    #@157
    invoke-virtual {v3}, Landroid/accounts/AccountManagerService$4;->bind()V
    :try_end_15a
    .catchall {:try_start_13f .. :try_end_15a} :catchall_15b

    #@15a
    goto :goto_133

    #@15b
    .line 1191
    :catchall_15b
    move-exception v3

    #@15c
    invoke-static/range {v20 .. v21}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@15f
    throw v3
.end method

.method public getAuthTokenLabel(Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter "response"
    .parameter "accountType"
    .parameter "authTokenType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1039
    if-nez p2, :cond_a

    #@2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "accountType is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1040
    :cond_a
    if-nez p3, :cond_14

    #@c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v1, "authTokenType is null"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 1042
    :cond_14
    invoke-static {}, Landroid/accounts/AccountManagerService;->getCallingUid()I

    #@17
    move-result v9

    #@18
    .line 1043
    .local v9, callingUid:I
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@1b
    .line 1044
    const/16 v0, 0x3e8

    #@1d
    if-eq v9, v0, :cond_27

    #@1f
    .line 1045
    new-instance v0, Ljava/lang/SecurityException;

    #@21
    const-string v1, "can only call from system"

    #@23
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 1047
    :cond_27
    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    #@2a
    move-result v0

    #@2b
    invoke-virtual {p0, v0}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@2e
    move-result-object v2

    #@2f
    .line 1048
    .local v2, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@32
    move-result-wide v10

    #@33
    .line 1050
    .local v10, identityToken:J
    :try_start_33
    new-instance v0, Landroid/accounts/AccountManagerService$3;

    #@35
    const/4 v5, 0x0

    #@36
    const/4 v6, 0x0

    #@37
    move-object v1, p0

    #@38
    move-object v3, p1

    #@39
    move-object v4, p2

    #@3a
    move-object v7, p2

    #@3b
    move-object v8, p3

    #@3c
    invoke-direct/range {v0 .. v8}, Landroid/accounts/AccountManagerService$3;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    #@3f
    invoke-virtual {v0}, Landroid/accounts/AccountManagerService$3;->bind()V
    :try_end_42
    .catchall {:try_start_33 .. :try_end_42} :catchall_46

    #@42
    .line 1075
    invoke-static {v10, v11}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@45
    .line 1077
    return-void

    #@46
    .line 1075
    :catchall_46
    move-exception v0

    #@47
    invoke-static {v10, v11}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@4a
    throw v0
.end method

.method public getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;
    .registers 12

    #@0
    .prologue
    .line 487
    const-string v8, "AccountManagerService"

    #@2
    const/4 v9, 0x2

    #@3
    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v8

    #@7
    if-eqz v8, :cond_33

    #@9
    .line 488
    const-string v8, "AccountManagerService"

    #@b
    new-instance v9, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v10, "getAuthenticatorTypes: caller\'s uid "

    #@12
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@19
    move-result v10

    #@1a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v9

    #@1e
    const-string v10, ", pid "

    #@20
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v9

    #@24
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@27
    move-result v10

    #@28
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v9

    #@2c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v9

    #@30
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 492
    :cond_33
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@36
    move-result v7

    #@37
    .line 493
    .local v7, userId:I
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@3a
    move-result-wide v4

    #@3b
    .line 496
    .local v4, identityToken:J
    :try_start_3b
    iget-object v8, p0, Landroid/accounts/AccountManagerService;->mAuthenticatorCache:Landroid/accounts/IAccountAuthenticatorCache;

    #@3d
    invoke-interface {v8, v7}, Landroid/accounts/IAccountAuthenticatorCache;->getAllServices(I)Ljava/util/Collection;

    #@40
    move-result-object v1

    #@41
    .line 497
    .local v1, authenticatorCollection:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    #@44
    move-result v8

    #@45
    new-array v6, v8, [Landroid/accounts/AuthenticatorDescription;

    #@47
    .line 499
    .local v6, types:[Landroid/accounts/AuthenticatorDescription;
    const/4 v2, 0x0

    #@48
    .line 501
    .local v2, i:I
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4b
    move-result-object v3

    #@4c
    .local v3, i$:Ljava/util/Iterator;
    :goto_4c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4f
    move-result v8

    #@50
    if-eqz v8, :cond_66

    #@52
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@55
    move-result-object v0

    #@56
    check-cast v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@58
    .line 502
    .local v0, authenticator:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    iget-object v8, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->type:Ljava/lang/Object;

    #@5a
    check-cast v8, Landroid/accounts/AuthenticatorDescription;

    #@5c
    aput-object v8, v6, v2
    :try_end_5e
    .catchall {:try_start_3b .. :try_end_5e} :catchall_61

    #@5e
    .line 503
    add-int/lit8 v2, v2, 0x1

    #@60
    goto :goto_4c

    #@61
    .line 507
    .end local v0           #authenticator:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    .end local v1           #authenticatorCollection:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;>;"
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v6           #types:[Landroid/accounts/AuthenticatorDescription;
    :catchall_61
    move-exception v8

    #@62
    invoke-static {v4, v5}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@65
    throw v8

    #@66
    .restart local v1       #authenticatorCollection:Ljava/util/Collection;,"Ljava/util/Collection<Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;>;"
    .restart local v2       #i:I
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v6       #types:[Landroid/accounts/AuthenticatorDescription;
    :cond_66
    invoke-static {v4, v5}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@69
    .line 505
    return-object v6
.end method

.method public getPassword(Landroid/accounts/Account;)Ljava/lang/String;
    .registers 8
    .parameter "account"

    #@0
    .prologue
    .line 429
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_3d

    #@9
    .line 430
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "getPassword: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v5, ", caller\'s uid "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@23
    move-result v5

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v5, ", pid "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@31
    move-result v5

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 434
    :cond_3d
    if-nez p1, :cond_47

    #@3f
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@41
    const-string v4, "account is null"

    #@43
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v3

    #@47
    .line 435
    :cond_47
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@4a
    .line 437
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@4d
    move-result-object v0

    #@4e
    .line 438
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@51
    move-result-wide v1

    #@52
    .line 440
    .local v1, identityToken:J
    :try_start_52
    invoke-direct {p0, v0, p1}, Landroid/accounts/AccountManagerService;->readPasswordInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/String;
    :try_end_55
    .catchall {:try_start_52 .. :try_end_55} :catchall_5a

    #@55
    move-result-object v3

    #@56
    .line 442
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@59
    .line 440
    return-object v3

    #@5a
    .line 442
    :catchall_5a
    move-exception v3

    #@5b
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@5e
    throw v3
.end method

.method public getRunningAccounts()[Landroid/accounts/AccountAndUser;
    .registers 4

    #@0
    .prologue
    .line 1541
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v2

    #@4
    invoke-interface {v2}, Landroid/app/IActivityManager;->getRunningUserIds()[I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_d

    #@7
    move-result-object v1

    #@8
    .line 1546
    .local v1, runningUserIds:[I
    invoke-direct {p0, v1}, Landroid/accounts/AccountManagerService;->getAccounts([I)[Landroid/accounts/AccountAndUser;

    #@b
    move-result-object v2

    #@c
    return-object v2

    #@d
    .line 1542
    .end local v1           #runningUserIds:[I
    :catch_d
    move-exception v0

    #@e
    .line 1544
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@10
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@13
    throw v2
.end method

.method protected getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 390
    iget-object v2, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 391
    :try_start_3
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/accounts/AccountManagerService$UserAccounts;

    #@b
    .line 392
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    if-nez v0, :cond_16

    #@d
    .line 393
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->initUser(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@10
    move-result-object v0

    #@11
    .line 394
    iget-object v1, p0, Landroid/accounts/AccountManagerService;->mUsers:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@16
    .line 396
    :cond_16
    monitor-exit v2

    #@17
    return-object v0

    #@18
    .line 397
    .end local v0           #accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "account"
    .parameter "key"

    #@0
    .prologue
    .line 468
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_47

    #@9
    .line 469
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "getUserData: "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v5, ", key "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, ", caller\'s uid "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2d
    move-result v5

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    const-string v5, ", pid "

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3b
    move-result v5

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 474
    :cond_47
    if-nez p1, :cond_51

    #@49
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4b
    const-string v4, "account is null"

    #@4d
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@50
    throw v3

    #@51
    .line 475
    :cond_51
    if-nez p2, :cond_5c

    #@53
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@55
    const-string/jumbo v4, "key is null"

    #@58
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v3

    #@5c
    .line 476
    :cond_5c
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@5f
    .line 477
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@62
    move-result-object v0

    #@63
    .line 478
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@66
    move-result-wide v1

    #@67
    .line 480
    .local v1, identityToken:J
    :try_start_67
    invoke-virtual {p0, v0, p1, p2}, Landroid/accounts/AccountManagerService;->readUserDataInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6a
    .catchall {:try_start_67 .. :try_end_6a} :catchall_6f

    #@6a
    move-result-object v3

    #@6b
    .line 482
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@6e
    .line 480
    return-object v3

    #@6f
    .line 482
    :catchall_6f
    move-exception v3

    #@70
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@73
    throw v3
.end method

.method public hasFeatures(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;[Ljava/lang/String;)V
    .registers 12
    .parameter "response"
    .parameter "account"
    .parameter "features"

    #@0
    .prologue
    .line 588
    const-string v0, "AccountManagerService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_55

    #@9
    .line 589
    const-string v0, "AccountManagerService"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "hasFeatures: "

    #@12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v3, ", response "

    #@1c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v3, ", features "

    #@26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-static {p3}, Landroid/accounts/AccountManagerService;->stringArrayToString([Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v3, ", caller\'s uid "

    #@34
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3b
    move-result v3

    #@3c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    const-string v3, ", pid "

    #@42
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@49
    move-result v3

    #@4a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 595
    :cond_55
    if-nez p1, :cond_60

    #@57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@59
    const-string/jumbo v1, "response is null"

    #@5c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v0

    #@60
    .line 596
    :cond_60
    if-nez p2, :cond_6a

    #@62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@64
    const-string v1, "account is null"

    #@66
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@69
    throw v0

    #@6a
    .line 597
    :cond_6a
    if-nez p3, :cond_74

    #@6c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6e
    const-string v1, "features is null"

    #@70
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@73
    throw v0

    #@74
    .line 598
    :cond_74
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkReadAccountsPermission()V

    #@77
    .line 599
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@7a
    move-result-object v2

    #@7b
    .line 600
    .local v2, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@7e
    move-result-wide v6

    #@7f
    .line 602
    .local v6, identityToken:J
    :try_start_7f
    new-instance v0, Landroid/accounts/AccountManagerService$TestFeaturesSession;

    #@81
    move-object v1, p0

    #@82
    move-object v3, p1

    #@83
    move-object v4, p2

    #@84
    move-object v5, p3

    #@85
    invoke-direct/range {v0 .. v5}, Landroid/accounts/AccountManagerService$TestFeaturesSession;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;[Ljava/lang/String;)V

    #@88
    invoke-virtual {v0}, Landroid/accounts/AccountManagerService$TestFeaturesSession;->bind()V
    :try_end_8b
    .catchall {:try_start_7f .. :try_end_8b} :catchall_8f

    #@8b
    .line 604
    invoke-static {v6, v7}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@8e
    .line 606
    return-void

    #@8f
    .line 604
    :catchall_8f
    move-exception v0

    #@90
    invoke-static {v6, v7}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@93
    throw v0
.end method

.method protected installNotification(ILandroid/app/Notification;Landroid/os/UserHandle;)V
    .registers 6
    .parameter "notificationId"
    .parameter "n"
    .parameter "user"

    #@0
    .prologue
    .line 2182
    iget-object v0, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v1, "notification"

    #@5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/app/NotificationManager;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@f
    .line 2184
    return-void
.end method

.method public invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "accountType"
    .parameter "authToken"

    #@0
    .prologue
    .line 770
    const-string v4, "AccountManagerService"

    #@2
    const/4 v5, 0x2

    #@3
    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v4

    #@7
    if-eqz v4, :cond_3d

    #@9
    .line 771
    const-string v4, "AccountManagerService"

    #@b
    new-instance v5, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v6, "invalidateAuthToken: accountType "

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    const-string v6, ", caller\'s uid "

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@23
    move-result v6

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    const-string v6, ", pid "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@31
    move-result v6

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 775
    :cond_3d
    if-nez p1, :cond_47

    #@3f
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@41
    const-string v5, "accountType is null"

    #@43
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v4

    #@47
    .line 776
    :cond_47
    if-nez p2, :cond_51

    #@49
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@4b
    const-string v5, "authToken is null"

    #@4d
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@50
    throw v4

    #@51
    .line 777
    :cond_51
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsOrUseCredentialsPermissions()V

    #@54
    .line 778
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@57
    move-result-object v0

    #@58
    .line 779
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@5b
    move-result-wide v2

    #@5c
    .line 781
    .local v2, identityToken:J
    :try_start_5c
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@5f
    move-result-object v5

    #@60
    monitor-enter v5
    :try_end_61
    .catchall {:try_start_5c .. :try_end_61} :catchall_82

    #@61
    .line 782
    :try_start_61
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@68
    move-result-object v1

    #@69
    .line 783
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_6c
    .catchall {:try_start_61 .. :try_end_6c} :catchall_7f

    #@6c
    .line 785
    :try_start_6c
    invoke-direct {p0, v0, v1, p1, p2}, Landroid/accounts/AccountManagerService;->invalidateAuthTokenLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    .line 786
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_72
    .catchall {:try_start_6c .. :try_end_72} :catchall_7a

    #@72
    .line 788
    :try_start_72
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@75
    .line 790
    monitor-exit v5
    :try_end_76
    .catchall {:try_start_72 .. :try_end_76} :catchall_7f

    #@76
    .line 792
    invoke-static {v2, v3}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@79
    .line 794
    return-void

    #@7a
    .line 788
    :catchall_7a
    move-exception v4

    #@7b
    :try_start_7b
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@7e
    throw v4

    #@7f
    .line 790
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7f
    move-exception v4

    #@80
    monitor-exit v5
    :try_end_81
    .catchall {:try_start_7b .. :try_end_81} :catchall_7f

    #@81
    :try_start_81
    throw v4
    :try_end_82
    .catchall {:try_start_81 .. :try_end_82} :catchall_82

    #@82
    .line 792
    :catchall_82
    move-exception v4

    #@83
    invoke-static {v2, v3}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@86
    throw v4
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 2065
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService;->asBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public onServiceChanged(Landroid/accounts/AuthenticatorDescription;IZ)V
    .registers 7
    .parameter "desc"
    .parameter "userId"
    .parameter "removed"

    #@0
    .prologue
    .line 424
    const-string v0, "AccountManagerService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "onServiceChanged() for userId "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 425
    invoke-virtual {p0, p2}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@1c
    move-result-object v0

    #@1d
    const/4 v1, 0x0

    #@1e
    invoke-direct {p0, v0, v1}, Landroid/accounts/AccountManagerService;->validateAccountsInternal(Landroid/accounts/AccountManagerService$UserAccounts;Z)V

    #@21
    .line 426
    return-void
.end method

.method public bridge synthetic onServiceChanged(Ljava/lang/Object;IZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 88
    check-cast p1, Landroid/accounts/AuthenticatorDescription;

    #@2
    .end local p1
    invoke-virtual {p0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->onServiceChanged(Landroid/accounts/AuthenticatorDescription;IZ)V

    #@5
    return-void
.end method

.method public peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "account"
    .parameter "authTokenType"

    #@0
    .prologue
    .line 861
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_48

    #@9
    .line 862
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v5, "peekAuthToken: "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, ", authTokenType "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, ", caller\'s uid "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, ", pid "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3c
    move-result v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 867
    :cond_48
    if-nez p1, :cond_52

    #@4a
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4c
    const-string v4, "account is null"

    #@4e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@51
    throw v3

    #@52
    .line 868
    :cond_52
    if-nez p2, :cond_5c

    #@54
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@56
    const-string v4, "authTokenType is null"

    #@58
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v3

    #@5c
    .line 869
    :cond_5c
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@5f
    .line 870
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@62
    move-result-object v0

    #@63
    .line 871
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@66
    move-result-wide v1

    #@67
    .line 873
    .local v1, identityToken:J
    :try_start_67
    invoke-virtual {p0, v0, p1, p2}, Landroid/accounts/AccountManagerService;->readAuthTokenInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6a
    .catchall {:try_start_67 .. :try_end_6a} :catchall_6f

    #@6a
    move-result-object v3

    #@6b
    .line 875
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@6e
    .line 873
    return-object v3

    #@6f
    .line 875
    :catchall_6f
    move-exception v3

    #@70
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@73
    throw v3
.end method

.method protected readAuthTokenInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "accounts"
    .parameter "account"
    .parameter "authTokenType"

    #@0
    .prologue
    .line 2502
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@3
    move-result-object v3

    #@4
    monitor-enter v3

    #@5
    .line 2503
    :try_start_5
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Ljava/util/HashMap;

    #@f
    .line 2504
    .local v0, authTokensForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_24

    #@11
    .line 2506
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@18
    move-result-object v1

    #@19
    .line 2507
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0, v1, p2}, Landroid/accounts/AccountManagerService;->readAuthTokensForAccountFromDatabaseLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Ljava/util/HashMap;

    #@1c
    move-result-object v0

    #@1d
    .line 2508
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 2510
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    :cond_24
    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Ljava/lang/String;

    #@2a
    monitor-exit v3

    #@2b
    return-object v2

    #@2c
    .line 2511
    .end local v0           #authTokensForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_2c
    move-exception v2

    #@2d
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_5 .. :try_end_2e} :catchall_2c

    #@2e
    throw v2
.end method

.method protected readAuthTokensForAccountFromDatabaseLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Ljava/util/HashMap;
    .registers 15
    .parameter "db"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    .line 2549
    new-instance v9, Ljava/util/HashMap;

    #@5
    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    #@8
    .line 2550
    .local v9, authTokensForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "authtokens"

    #@a
    sget-object v2, Landroid/accounts/AccountManagerService;->COLUMNS_AUTHTOKENS_TYPE_AND_AUTHTOKEN:[Ljava/lang/String;

    #@c
    const-string v3, "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)"

    #@e
    const/4 v0, 0x2

    #@f
    new-array v4, v0, [Ljava/lang/String;

    #@11
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@13
    aput-object v0, v4, v6

    #@15
    iget-object v0, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@17
    aput-object v0, v4, v7

    #@19
    move-object v0, p1

    #@1a
    move-object v6, v5

    #@1b
    move-object v7, v5

    #@1c
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1f
    move-result-object v10

    #@20
    .line 2556
    .local v10, cursor:Landroid/database/Cursor;
    :goto_20
    :try_start_20
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_39

    #@26
    .line 2557
    const/4 v0, 0x0

    #@27
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v11

    #@2b
    .line 2558
    .local v11, type:Ljava/lang/String;
    const/4 v0, 0x1

    #@2c
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2f
    move-result-object v8

    #@30
    .line 2559
    .local v8, authToken:Ljava/lang/String;
    invoke-virtual {v9, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_33
    .catchall {:try_start_20 .. :try_end_33} :catchall_34

    #@33
    goto :goto_20

    #@34
    .line 2562
    .end local v8           #authToken:Ljava/lang/String;
    .end local v11           #type:Ljava/lang/String;
    :catchall_34
    move-exception v0

    #@35
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@38
    throw v0

    #@39
    :cond_39
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@3c
    .line 2564
    return-object v9
.end method

.method protected readUserDataForAccountFromDatabaseLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Ljava/util/HashMap;
    .registers 15
    .parameter "db"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    .line 2529
    new-instance v10, Ljava/util/HashMap;

    #@5
    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    #@8
    .line 2530
    .local v10, userDataForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "extras"

    #@a
    sget-object v2, Landroid/accounts/AccountManagerService;->COLUMNS_EXTRAS_KEY_AND_VALUE:[Ljava/lang/String;

    #@c
    const-string v3, "accounts_id=(select _id FROM accounts WHERE name=? AND type=?)"

    #@e
    const/4 v0, 0x2

    #@f
    new-array v4, v0, [Ljava/lang/String;

    #@11
    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@13
    aput-object v0, v4, v6

    #@15
    iget-object v0, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@17
    aput-object v0, v4, v7

    #@19
    move-object v0, p1

    #@1a
    move-object v6, v5

    #@1b
    move-object v7, v5

    #@1c
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1f
    move-result-object v8

    #@20
    .line 2536
    .local v8, cursor:Landroid/database/Cursor;
    :goto_20
    :try_start_20
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_39

    #@26
    .line 2537
    const/4 v0, 0x0

    #@27
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v9

    #@2b
    .line 2538
    .local v9, tmpkey:Ljava/lang/String;
    const/4 v0, 0x1

    #@2c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2f
    move-result-object v11

    #@30
    .line 2539
    .local v11, value:Ljava/lang/String;
    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_33
    .catchall {:try_start_20 .. :try_end_33} :catchall_34

    #@33
    goto :goto_20

    #@34
    .line 2542
    .end local v9           #tmpkey:Ljava/lang/String;
    .end local v11           #value:Ljava/lang/String;
    :catchall_34
    move-exception v0

    #@35
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@38
    throw v0

    #@39
    :cond_39
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@3c
    .line 2544
    return-object v10
.end method

.method protected readUserDataInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "accounts"
    .parameter "account"
    .parameter "key"

    #@0
    .prologue
    .line 2515
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$200(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/lang/Object;

    #@3
    move-result-object v3

    #@4
    monitor-enter v3

    #@5
    .line 2516
    :try_start_5
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$600(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Ljava/util/HashMap;

    #@f
    .line 2517
    .local v1, userDataForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v1, :cond_24

    #@11
    .line 2519
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$300(Landroid/accounts/AccountManagerService$UserAccounts;)Landroid/accounts/AccountManagerService$DatabaseHelper;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Landroid/accounts/AccountManagerService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@18
    move-result-object v0

    #@19
    .line 2520
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0, v0, p2}, Landroid/accounts/AccountManagerService;->readUserDataForAccountFromDatabaseLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Ljava/util/HashMap;

    #@1c
    move-result-object v1

    #@1d
    .line 2521
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$600(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 2523
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    :cond_24
    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Ljava/lang/String;

    #@2a
    monitor-exit v3

    #@2b
    return-object v2

    #@2c
    .line 2524
    .end local v1           #userDataForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_2c
    move-exception v2

    #@2d
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_5 .. :try_end_2e} :catchall_2c

    #@2e
    throw v2
.end method

.method public removeAccount(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;)V
    .registers 13
    .parameter "response"
    .parameter "account"

    #@0
    .prologue
    .line 661
    const-string v7, "AccountManagerService"

    #@2
    const/4 v8, 0x2

    #@3
    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v7

    #@7
    if-eqz v7, :cond_48

    #@9
    .line 662
    const-string v7, "AccountManagerService"

    #@b
    new-instance v8, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v9, "removeAccount: "

    #@13
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v8

    #@17
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    const-string v9, ", response "

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    const-string v9, ", caller\'s uid "

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v9

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    const-string v9, ", pid "

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3c
    move-result v9

    #@3d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v8

    #@45
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 667
    :cond_48
    if-nez p1, :cond_53

    #@4a
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@4c
    const-string/jumbo v8, "response is null"

    #@4f
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@52
    throw v7

    #@53
    .line 668
    :cond_53
    if-nez p2, :cond_5d

    #@55
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@57
    const-string v8, "account is null"

    #@59
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v7

    #@5d
    .line 671
    :cond_5d
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@60
    move-result-object v7

    #@61
    if-eqz v7, :cond_74

    #@63
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@66
    move-result-object v7

    #@67
    iget-object v8, p0, Landroid/accounts/AccountManagerService;->mContext:Landroid/content/Context;

    #@69
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@6c
    move-result v9

    #@6d
    invoke-interface {v7, v8, p2, v9}, Lcom/lge/cappuccino/IMdm;->checkDisabledAccountManagerService(Landroid/content/Context;Landroid/accounts/Account;I)Z

    #@70
    move-result v7

    #@71
    if-eqz v7, :cond_74

    #@73
    .line 699
    :goto_73
    return-void

    #@74
    .line 678
    :cond_74
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsPermission()V

    #@77
    .line 679
    invoke-static {}, Landroid/os/Binder;->getCallingUserHandle()Landroid/os/UserHandle;

    #@7a
    move-result-object v6

    #@7b
    .line 680
    .local v6, user:Landroid/os/UserHandle;
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@7e
    move-result-object v0

    #@7f
    .line 681
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@82
    move-result-wide v3

    #@83
    .line 683
    .local v3, identityToken:J
    invoke-direct {p0, v0, p2}, Landroid/accounts/AccountManagerService;->getSigninRequiredNotificationId(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;

    #@86
    move-result-object v7

    #@87
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@8a
    move-result v7

    #@8b
    invoke-virtual {p0, v7, v6}, Landroid/accounts/AccountManagerService;->cancelNotification(ILandroid/os/UserHandle;)V

    #@8e
    .line 684
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$800(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@91
    move-result-object v8

    #@92
    monitor-enter v8

    #@93
    .line 686
    :try_start_93
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$800(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@96
    move-result-object v7

    #@97
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@9a
    move-result-object v7

    #@9b
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9e
    move-result-object v1

    #@9f
    .local v1, i$:Ljava/util/Iterator;
    :cond_9f
    :goto_9f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a2
    move-result v7

    #@a3
    if-eqz v7, :cond_cc

    #@a5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a8
    move-result-object v5

    #@a9
    check-cast v5, Landroid/util/Pair;

    #@ab
    .line 687
    .local v5, pair:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;Ljava/lang/Integer;>;"
    iget-object v7, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@ad
    check-cast v7, Landroid/util/Pair;

    #@af
    iget-object v7, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@b1
    invoke-virtual {p2, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@b4
    move-result v7

    #@b5
    if-eqz v7, :cond_9f

    #@b7
    .line 688
    invoke-static {v0}, Landroid/accounts/AccountManagerService$UserAccounts;->access$800(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@ba
    move-result-object v7

    #@bb
    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@be
    move-result-object v7

    #@bf
    check-cast v7, Ljava/lang/Integer;

    #@c1
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@c4
    move-result v2

    #@c5
    .line 689
    .local v2, id:I
    invoke-virtual {p0, v2, v6}, Landroid/accounts/AccountManagerService;->cancelNotification(ILandroid/os/UserHandle;)V

    #@c8
    goto :goto_9f

    #@c9
    .line 692
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #id:I
    .end local v5           #pair:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Landroid/accounts/Account;Ljava/lang/String;>;Ljava/lang/Integer;>;"
    :catchall_c9
    move-exception v7

    #@ca
    monitor-exit v8
    :try_end_cb
    .catchall {:try_start_93 .. :try_end_cb} :catchall_c9

    #@cb
    throw v7

    #@cc
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_cc
    :try_start_cc
    monitor-exit v8
    :try_end_cd
    .catchall {:try_start_cc .. :try_end_cd} :catchall_c9

    #@cd
    .line 695
    :try_start_cd
    new-instance v7, Landroid/accounts/AccountManagerService$RemoveAccountSession;

    #@cf
    invoke-direct {v7, p0, v0, p1, p2}, Landroid/accounts/AccountManagerService$RemoveAccountSession;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;)V

    #@d2
    invoke-virtual {v7}, Landroid/accounts/AccountManagerService$RemoveAccountSession;->bind()V
    :try_end_d5
    .catchall {:try_start_cd .. :try_end_d5} :catchall_d9

    #@d5
    .line 697
    invoke-static {v3, v4}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@d8
    goto :goto_73

    #@d9
    :catchall_d9
    move-exception v7

    #@da
    invoke-static {v3, v4}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@dd
    throw v7
.end method

.method protected removeAccountInternal(Landroid/accounts/Account;)V
    .registers 3
    .parameter "account"

    #@0
    .prologue
    .line 756
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p1}, Landroid/accounts/AccountManagerService;->removeAccountInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)V

    #@7
    .line 757
    return-void
.end method

.method public setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "account"
    .parameter "authTokenType"
    .parameter "authToken"

    #@0
    .prologue
    .line 880
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_48

    #@9
    .line 881
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v5, "setAuthToken: "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, ", authTokenType "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, ", caller\'s uid "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, ", pid "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3c
    move-result v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 886
    :cond_48
    if-nez p1, :cond_52

    #@4a
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4c
    const-string v4, "account is null"

    #@4e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@51
    throw v3

    #@52
    .line 887
    :cond_52
    if-nez p2, :cond_5c

    #@54
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@56
    const-string v4, "authTokenType is null"

    #@58
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v3

    #@5c
    .line 888
    :cond_5c
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@5f
    .line 889
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@62
    move-result-object v0

    #@63
    .line 890
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@66
    move-result-wide v1

    #@67
    .line 892
    .local v1, identityToken:J
    :try_start_67
    invoke-direct {p0, v0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->saveAuthTokenToDatabase(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_6a
    .catchall {:try_start_67 .. :try_end_6a} :catchall_6e

    #@6a
    .line 894
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@6d
    .line 896
    return-void

    #@6e
    .line 894
    :catchall_6e
    move-exception v3

    #@6f
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@72
    throw v3
.end method

.method public setPassword(Landroid/accounts/Account;Ljava/lang/String;)V
    .registers 9
    .parameter "account"
    .parameter "password"

    #@0
    .prologue
    .line 899
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_3e

    #@9
    .line 900
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v5, "setAuthToken: "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, ", caller\'s uid "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@24
    move-result v5

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, ", pid "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@32
    move-result v5

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 904
    :cond_3e
    if-nez p1, :cond_48

    #@40
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@42
    const-string v4, "account is null"

    #@44
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@47
    throw v3

    #@48
    .line 905
    :cond_48
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@4b
    .line 906
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@4e
    move-result-object v0

    #@4f
    .line 907
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@52
    move-result-wide v1

    #@53
    .line 909
    .local v1, identityToken:J
    :try_start_53
    invoke-direct {p0, v0, p1, p2}, Landroid/accounts/AccountManagerService;->setPasswordInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;)V
    :try_end_56
    .catchall {:try_start_53 .. :try_end_56} :catchall_5a

    #@56
    .line 911
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@59
    .line 913
    return-void

    #@5a
    .line 911
    :catchall_5a
    move-exception v3

    #@5b
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@5e
    throw v3
.end method

.method public setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "account"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 964
    const-string v3, "AccountManagerService"

    #@2
    const/4 v4, 0x2

    #@3
    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_48

    #@9
    .line 965
    const-string v3, "AccountManagerService"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v5, "setUserData: "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, ", key "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    const-string v5, ", caller\'s uid "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, ", pid "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3c
    move-result v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 970
    :cond_48
    if-nez p2, :cond_53

    #@4a
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4c
    const-string/jumbo v4, "key is null"

    #@4f
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@52
    throw v3

    #@53
    .line 971
    :cond_53
    if-nez p1, :cond_5d

    #@55
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@57
    const-string v4, "account is null"

    #@59
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v3

    #@5d
    .line 972
    :cond_5d
    invoke-direct {p0, p1}, Landroid/accounts/AccountManagerService;->checkAuthenticateAccountsPermission(Landroid/accounts/Account;)V

    #@60
    .line 973
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@63
    move-result-object v0

    #@64
    .line 974
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@67
    move-result-wide v1

    #@68
    .line 976
    .local v1, identityToken:J
    :try_start_68
    invoke-direct {p0, v0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->setUserdataInternal(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_6f

    #@6b
    .line 978
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@6e
    .line 980
    return-void

    #@6f
    .line 978
    :catchall_6f
    move-exception v3

    #@70
    invoke-static {v1, v2}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@73
    throw v3
.end method

.method public systemReady()V
    .registers 1

    #@0
    .prologue
    .line 250
    return-void
.end method

.method public updateAppPermission(Landroid/accounts/Account;Ljava/lang/String;IZ)V
    .registers 7
    .parameter "account"
    .parameter "authTokenType"
    .parameter "uid"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2324
    invoke-static {}, Landroid/accounts/AccountManagerService;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 2326
    .local v0, callingUid:I
    const/16 v1, 0x3e8

    #@6
    if-eq v0, v1, :cond_e

    #@8
    .line 2327
    new-instance v1, Ljava/lang/SecurityException;

    #@a
    invoke-direct {v1}, Ljava/lang/SecurityException;-><init>()V

    #@d
    throw v1

    #@e
    .line 2330
    :cond_e
    if-eqz p4, :cond_14

    #@10
    .line 2331
    invoke-direct {p0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->grantAppPermission(Landroid/accounts/Account;Ljava/lang/String;I)V

    #@13
    .line 2335
    :goto_13
    return-void

    #@14
    .line 2333
    :cond_14
    invoke-direct {p0, p1, p2, p3}, Landroid/accounts/AccountManagerService;->revokeAppPermission(Landroid/accounts/Account;Ljava/lang/String;I)V

    #@17
    goto :goto_13
.end method

.method public updateCredentials(Landroid/accounts/IAccountManagerResponse;Landroid/accounts/Account;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .registers 19
    .parameter "response"
    .parameter "account"
    .parameter "authTokenType"
    .parameter "expectActivityLaunch"
    .parameter "loginOptions"

    #@0
    .prologue
    .line 1360
    const-string v1, "AccountManagerService"

    #@2
    const/4 v2, 0x2

    #@3
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_60

    #@9
    .line 1361
    const-string v1, "AccountManagerService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v4, "updateCredentials: "

    #@13
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v4, ", response "

    #@1d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v4, ", authTokenType "

    #@27
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    move-object/from16 v0, p3

    #@2d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v4, ", expectActivityLaunch "

    #@33
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    move/from16 v0, p4

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    const-string v4, ", caller\'s uid "

    #@3f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@46
    move-result v4

    #@47
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v4, ", pid "

    #@4d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@54
    move-result v4

    #@55
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 1368
    :cond_60
    if-nez p1, :cond_6b

    #@62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@64
    const-string/jumbo v2, "response is null"

    #@67
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@6a
    throw v1

    #@6b
    .line 1369
    :cond_6b
    if-nez p2, :cond_75

    #@6d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@6f
    const-string v2, "account is null"

    #@71
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@74
    throw v1

    #@75
    .line 1370
    :cond_75
    if-nez p3, :cond_7f

    #@77
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@79
    const-string v2, "authTokenType is null"

    #@7b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7e
    throw v1

    #@7f
    .line 1371
    :cond_7f
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->checkManageAccountsPermission()V

    #@82
    .line 1372
    invoke-direct {p0}, Landroid/accounts/AccountManagerService;->getUserAccountsForCaller()Landroid/accounts/AccountManagerService$UserAccounts;

    #@85
    move-result-object v3

    #@86
    .line 1373
    .local v3, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    invoke-static {}, Landroid/accounts/AccountManagerService;->clearCallingIdentity()J

    #@89
    move-result-wide v11

    #@8a
    .line 1375
    .local v11, identityToken:J
    :try_start_8a
    new-instance v1, Landroid/accounts/AccountManagerService$7;

    #@8c
    iget-object v5, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@8e
    const/4 v7, 0x1

    #@8f
    move-object v2, p0

    #@90
    move-object v4, p1

    #@91
    move/from16 v6, p4

    #@93
    move-object v8, p2

    #@94
    move-object/from16 v9, p3

    #@96
    move-object/from16 v10, p5

    #@98
    invoke-direct/range {v1 .. v10}, Landroid/accounts/AccountManagerService$7;-><init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZLandroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@9b
    invoke-virtual {v1}, Landroid/accounts/AccountManagerService$7;->bind()V
    :try_end_9e
    .catchall {:try_start_8a .. :try_end_9e} :catchall_a2

    #@9e
    .line 1389
    invoke-static {v11, v12}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@a1
    .line 1391
    return-void

    #@a2
    .line 1389
    :catchall_a2
    move-exception v1

    #@a3
    invoke-static {v11, v12}, Landroid/accounts/AccountManagerService;->restoreCallingIdentity(J)V

    #@a6
    throw v1
.end method

.method public validateAccounts(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 309
    invoke-virtual {p0, p1}, Landroid/accounts/AccountManagerService;->getUserAccounts(I)Landroid/accounts/AccountManagerService$UserAccounts;

    #@3
    move-result-object v0

    #@4
    .line 313
    .local v0, accounts:Landroid/accounts/AccountManagerService$UserAccounts;
    const/4 v1, 0x1

    #@5
    invoke-direct {p0, v0, v1}, Landroid/accounts/AccountManagerService;->validateAccountsInternal(Landroid/accounts/AccountManagerService$UserAccounts;Z)V

    #@8
    .line 314
    return-void
.end method

.method protected writeAuthTokenIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "accounts"
    .parameter "db"
    .parameter "account"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 2488
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/util/HashMap;

    #@a
    .line 2489
    .local v0, authTokensForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_17

    #@c
    .line 2490
    invoke-virtual {p0, p2, p3}, Landroid/accounts/AccountManagerService;->readAuthTokensForAccountFromDatabaseLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Ljava/util/HashMap;

    #@f
    move-result-object v0

    #@10
    .line 2491
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$700(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 2493
    :cond_17
    if-nez p5, :cond_1d

    #@19
    .line 2494
    invoke-virtual {v0, p4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 2498
    :goto_1c
    return-void

    #@1d
    .line 2496
    :cond_1d
    invoke-virtual {v0, p4, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    goto :goto_1c
.end method

.method protected writeUserDataIntoCacheLocked(Landroid/accounts/AccountManagerService$UserAccounts;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "accounts"
    .parameter "db"
    .parameter "account"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 2474
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$600(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/util/HashMap;

    #@a
    .line 2475
    .local v0, userDataForAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_17

    #@c
    .line 2476
    invoke-virtual {p0, p2, p3}, Landroid/accounts/AccountManagerService;->readUserDataForAccountFromDatabaseLocked(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)Ljava/util/HashMap;

    #@f
    move-result-object v0

    #@10
    .line 2477
    invoke-static {p1}, Landroid/accounts/AccountManagerService$UserAccounts;->access$600(Landroid/accounts/AccountManagerService$UserAccounts;)Ljava/util/HashMap;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 2479
    :cond_17
    if-nez p5, :cond_1d

    #@19
    .line 2480
    invoke-virtual {v0, p4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 2484
    :goto_1c
    return-void

    #@1d
    .line 2482
    :cond_1d
    invoke-virtual {v0, p4, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    goto :goto_1c
.end method
