.class public Landroid/accounts/ChooseAccountTypeActivity;
.super Landroid/app/Activity;
.source "ChooseAccountTypeActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/ChooseAccountTypeActivity$AccountArrayAdapter;,
        Landroid/accounts/ChooseAccountTypeActivity$ViewHolder;,
        Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AccountChooser"


# instance fields
.field private mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTypeToAuthenticatorInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 48
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/accounts/ChooseAccountTypeActivity;->mTypeToAuthenticatorInfo:Ljava/util/HashMap;

    #@a
    .line 171
    return-void
.end method

.method static synthetic access$000(Landroid/accounts/ChooseAccountTypeActivity;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/accounts/ChooseAccountTypeActivity;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/accounts/ChooseAccountTypeActivity;->setResultAndFinish(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private buildTypeToAuthDescriptionMap()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x5

    #@1
    .line 127
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@4
    move-result-object v10

    #@5
    invoke-virtual {v10}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    #@8
    move-result-object v0

    #@9
    .local v0, arr$:[Landroid/accounts/AuthenticatorDescription;
    array-length v7, v0

    #@a
    .local v7, len$:I
    const/4 v5, 0x0

    #@b
    .local v5, i$:I
    :goto_b
    if-ge v5, v7, :cond_8d

    #@d
    aget-object v3, v0, v5

    #@f
    .line 128
    .local v3, desc:Landroid/accounts/AuthenticatorDescription;
    const/4 v8, 0x0

    #@10
    .line 129
    .local v8, name:Ljava/lang/String;
    const/4 v6, 0x0

    #@11
    .line 131
    .local v6, icon:Landroid/graphics/drawable/Drawable;
    :try_start_11
    iget-object v10, v3, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    #@13
    const/4 v11, 0x0

    #@14
    invoke-virtual {p0, v10, v11}, Landroid/accounts/ChooseAccountTypeActivity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@17
    move-result-object v1

    #@18
    .line 132
    .local v1, authContext:Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1b
    move-result-object v10

    #@1c
    iget v11, v3, Landroid/accounts/AuthenticatorDescription;->iconId:I

    #@1e
    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@21
    move-result-object v6

    #@22
    .line 133
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@25
    move-result-object v10

    #@26
    iget v11, v3, Landroid/accounts/AuthenticatorDescription;->labelId:I

    #@28
    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@2b
    move-result-object v9

    #@2c
    .line 134
    .local v9, sequence:Ljava/lang/CharSequence;
    if-eqz v9, :cond_32

    #@2e
    .line 135
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@31
    move-result-object v8

    #@32
    .line 137
    :cond_32
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_35
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11 .. :try_end_35} :catch_45
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_11 .. :try_end_35} :catch_69

    #@35
    move-result-object v8

    #@36
    .line 149
    .end local v1           #authContext:Landroid/content/Context;
    .end local v9           #sequence:Ljava/lang/CharSequence;
    :cond_36
    :goto_36
    new-instance v2, Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;

    #@38
    invoke-direct {v2, v3, v8, v6}, Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;-><init>(Landroid/accounts/AuthenticatorDescription;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    #@3b
    .line 150
    .local v2, authInfo:Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;
    iget-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mTypeToAuthenticatorInfo:Ljava/util/HashMap;

    #@3d
    iget-object v11, v3, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@3f
    invoke-virtual {v10, v11, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    .line 127
    add-int/lit8 v5, v5, 0x1

    #@44
    goto :goto_b

    #@45
    .line 138
    .end local v2           #authInfo:Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;
    :catch_45
    move-exception v4

    #@46
    .line 140
    .local v4, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v10, "AccountChooser"

    #@48
    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@4b
    move-result v10

    #@4c
    if-eqz v10, :cond_36

    #@4e
    .line 141
    const-string v10, "AccountChooser"

    #@50
    new-instance v11, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v12, "No icon name for account type "

    #@57
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v11

    #@5b
    iget-object v12, v3, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@5d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v11

    #@61
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v11

    #@65
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_36

    #@69
    .line 143
    .end local v4           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_69
    move-exception v4

    #@6a
    .line 145
    .local v4, e:Landroid/content/res/Resources$NotFoundException;
    const-string v10, "AccountChooser"

    #@6c
    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6f
    move-result v10

    #@70
    if-eqz v10, :cond_36

    #@72
    .line 146
    const-string v10, "AccountChooser"

    #@74
    new-instance v11, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v12, "No icon resource for account type "

    #@7b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v11

    #@7f
    iget-object v12, v3, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@81
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v11

    #@85
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v11

    #@89
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    goto :goto_36

    #@8d
    .line 152
    .end local v3           #desc:Landroid/accounts/AuthenticatorDescription;
    .end local v4           #e:Landroid/content/res/Resources$NotFoundException;
    .end local v6           #icon:Landroid/graphics/drawable/Drawable;
    .end local v8           #name:Ljava/lang/String;
    :cond_8d
    return-void
.end method

.method private setResultAndFinish(Ljava/lang/String;)V
    .registers 6
    .parameter "type"

    #@0
    .prologue
    .line 116
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 117
    .local v0, bundle:Landroid/os/Bundle;
    const-string v1, "accountType"

    #@7
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 118
    const/4 v1, -0x1

    #@b
    new-instance v2, Landroid/content/Intent;

    #@d
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@10
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {p0, v1, v2}, Landroid/accounts/ChooseAccountTypeActivity;->setResult(ILandroid/content/Intent;)V

    #@17
    .line 119
    const-string v1, "AccountChooser"

    #@19
    const/4 v2, 0x2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_38

    #@20
    .line 120
    const-string v1, "AccountChooser"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "ChooseAccountTypeActivity.setResultAndFinish: selected account type "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 123
    :cond_38
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountTypeActivity;->finish()V

    #@3b
    .line 124
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 16
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 55
    const-string v10, "AccountChooser"

    #@6
    const/4 v11, 0x2

    #@7
    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@a
    move-result v10

    #@b
    if-eqz v10, :cond_2b

    #@d
    .line 56
    const-string v10, "AccountChooser"

    #@f
    new-instance v11, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v12, "ChooseAccountTypeActivity.onCreate(savedInstanceState="

    #@16
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v11

    #@1a
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v11

    #@1e
    const-string v12, ")"

    #@20
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v11

    #@24
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v11

    #@28
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 61
    :cond_2b
    const/4 v7, 0x0

    #@2c
    .line 62
    .local v7, setOfAllowableAccountTypes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountTypeActivity;->getIntent()Landroid/content/Intent;

    #@2f
    move-result-object v10

    #@30
    const-string v11, "allowableAccountTypes"

    #@32
    invoke-virtual {v10, v11}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@35
    move-result-object v9

    #@36
    .line 64
    .local v9, validAccountTypes:[Ljava/lang/String;
    if-eqz v9, :cond_4b

    #@38
    .line 65
    new-instance v7, Ljava/util/HashSet;

    #@3a
    .end local v7           #setOfAllowableAccountTypes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    array-length v10, v9

    #@3b
    invoke-direct {v7, v10}, Ljava/util/HashSet;-><init>(I)V

    #@3e
    .line 66
    .restart local v7       #setOfAllowableAccountTypes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    move-object v0, v9

    #@3f
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@40
    .local v5, len$:I
    const/4 v3, 0x0

    #@41
    .local v3, i$:I
    :goto_41
    if-ge v3, v5, :cond_4b

    #@43
    aget-object v8, v0, v3

    #@45
    .line 67
    .local v8, type:Ljava/lang/String;
    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@48
    .line 66
    add-int/lit8 v3, v3, 0x1

    #@4a
    goto :goto_41

    #@4b
    .line 72
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    .end local v8           #type:Ljava/lang/String;
    :cond_4b
    invoke-direct {p0}, Landroid/accounts/ChooseAccountTypeActivity;->buildTypeToAuthDescriptionMap()V

    #@4e
    .line 76
    new-instance v10, Ljava/util/ArrayList;

    #@50
    iget-object v11, p0, Landroid/accounts/ChooseAccountTypeActivity;->mTypeToAuthenticatorInfo:Ljava/util/HashMap;

    #@52
    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    #@55
    move-result v11

    #@56
    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    #@59
    iput-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@5b
    .line 77
    iget-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mTypeToAuthenticatorInfo:Ljava/util/HashMap;

    #@5d
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@60
    move-result-object v10

    #@61
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@64
    move-result-object v3

    #@65
    .local v3, i$:Ljava/util/Iterator;
    :cond_65
    :goto_65
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@68
    move-result v10

    #@69
    if-eqz v10, :cond_8b

    #@6b
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6e
    move-result-object v2

    #@6f
    check-cast v2, Ljava/util/Map$Entry;

    #@71
    .line 78
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@74
    move-result-object v8

    #@75
    check-cast v8, Ljava/lang/String;

    #@77
    .line 79
    .restart local v8       #type:Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@7a
    move-result-object v4

    #@7b
    check-cast v4, Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;

    #@7d
    .line 80
    .local v4, info:Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;
    if-eqz v7, :cond_85

    #@7f
    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@82
    move-result v10

    #@83
    if-eqz v10, :cond_65

    #@85
    .line 84
    :cond_85
    iget-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@87
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8a
    goto :goto_65

    #@8b
    .line 87
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;>;"
    .end local v4           #info:Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;
    .end local v8           #type:Ljava/lang/String;
    :cond_8b
    iget-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@8d
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    #@90
    move-result v10

    #@91
    if-eqz v10, :cond_b1

    #@93
    .line 88
    new-instance v1, Landroid/os/Bundle;

    #@95
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@98
    .line 89
    .local v1, bundle:Landroid/os/Bundle;
    const-string v10, "errorMessage"

    #@9a
    const-string/jumbo v11, "no allowable account types"

    #@9d
    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 90
    const/4 v10, -0x1

    #@a1
    new-instance v11, Landroid/content/Intent;

    #@a3
    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    #@a6
    invoke-virtual {v11, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@a9
    move-result-object v11

    #@aa
    invoke-virtual {p0, v10, v11}, Landroid/accounts/ChooseAccountTypeActivity;->setResult(ILandroid/content/Intent;)V

    #@ad
    .line 91
    invoke-virtual {p0}, Landroid/accounts/ChooseAccountTypeActivity;->finish()V

    #@b0
    .line 113
    .end local v1           #bundle:Landroid/os/Bundle;
    :goto_b0
    return-void

    #@b1
    .line 95
    :cond_b1
    iget-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@b3
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@b6
    move-result v10

    #@b7
    const/4 v11, 0x1

    #@b8
    if-ne v10, v11, :cond_ca

    #@ba
    .line 96
    iget-object v10, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@bc
    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@bf
    move-result-object v10

    #@c0
    check-cast v10, Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;

    #@c2
    iget-object v10, v10, Landroid/accounts/ChooseAccountTypeActivity$AuthInfo;->desc:Landroid/accounts/AuthenticatorDescription;

    #@c4
    iget-object v10, v10, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    #@c6
    invoke-direct {p0, v10}, Landroid/accounts/ChooseAccountTypeActivity;->setResultAndFinish(Ljava/lang/String;)V

    #@c9
    goto :goto_b0

    #@ca
    .line 100
    :cond_ca
    const v10, 0x1090035

    #@cd
    invoke-virtual {p0, v10}, Landroid/accounts/ChooseAccountTypeActivity;->setContentView(I)V

    #@d0
    .line 102
    const v10, 0x102000a

    #@d3
    invoke-virtual {p0, v10}, Landroid/accounts/ChooseAccountTypeActivity;->findViewById(I)Landroid/view/View;

    #@d6
    move-result-object v6

    #@d7
    check-cast v6, Landroid/widget/ListView;

    #@d9
    .line 104
    .local v6, list:Landroid/widget/ListView;
    new-instance v10, Landroid/accounts/ChooseAccountTypeActivity$AccountArrayAdapter;

    #@db
    const v11, 0x1090003

    #@de
    iget-object v12, p0, Landroid/accounts/ChooseAccountTypeActivity;->mAuthenticatorInfosToDisplay:Ljava/util/ArrayList;

    #@e0
    invoke-direct {v10, p0, v11, v12}, Landroid/accounts/ChooseAccountTypeActivity$AccountArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    #@e3
    invoke-virtual {v6, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@e6
    .line 106
    invoke-virtual {v6, v13}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@e9
    .line 107
    invoke-virtual {v6, v13}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    #@ec
    .line 108
    new-instance v10, Landroid/accounts/ChooseAccountTypeActivity$1;

    #@ee
    invoke-direct {v10, p0}, Landroid/accounts/ChooseAccountTypeActivity$1;-><init>(Landroid/accounts/ChooseAccountTypeActivity;)V

    #@f1
    invoke-virtual {v6, v10}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@f4
    goto :goto_b0
.end method
