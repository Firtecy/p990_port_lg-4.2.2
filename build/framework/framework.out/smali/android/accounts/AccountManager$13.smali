.class Landroid/accounts/AccountManager$13;
.super Landroid/content/BroadcastReceiver;
.source "AccountManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/AccountManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/accounts/AccountManager;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1914
    iput-object p1, p0, Landroid/accounts/AccountManager$13;->this$0:Landroid/accounts/AccountManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1916
    iget-object v3, p0, Landroid/accounts/AccountManager$13;->this$0:Landroid/accounts/AccountManager;

    #@2
    invoke-virtual {v3}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    #@5
    move-result-object v0

    #@6
    .line 1918
    .local v0, accounts:[Landroid/accounts/Account;
    iget-object v3, p0, Landroid/accounts/AccountManager$13;->this$0:Landroid/accounts/AccountManager;

    #@8
    invoke-static {v3}, Landroid/accounts/AccountManager;->access$1300(Landroid/accounts/AccountManager;)Ljava/util/HashMap;

    #@b
    move-result-object v5

    #@c
    monitor-enter v5

    #@d
    .line 1920
    :try_start_d
    iget-object v3, p0, Landroid/accounts/AccountManager$13;->this$0:Landroid/accounts/AccountManager;

    #@f
    invoke-static {v3}, Landroid/accounts/AccountManager;->access$1300(Landroid/accounts/AccountManager;)Ljava/util/HashMap;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@16
    move-result-object v3

    #@17
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1a
    move-result-object v2

    #@1b
    .local v2, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_3c

    #@21
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    check-cast v1, Ljava/util/Map$Entry;

    #@27
    .line 1921
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;>;"
    iget-object v6, p0, Landroid/accounts/AccountManager$13;->this$0:Landroid/accounts/AccountManager;

    #@29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/os/Handler;

    #@2f
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@32
    move-result-object v4

    #@33
    check-cast v4, Landroid/accounts/OnAccountsUpdateListener;

    #@35
    invoke-static {v6, v3, v4, v0}, Landroid/accounts/AccountManager;->access$1400(Landroid/accounts/AccountManager;Landroid/os/Handler;Landroid/accounts/OnAccountsUpdateListener;[Landroid/accounts/Account;)V

    #@38
    goto :goto_1b

    #@39
    .line 1923
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_39
    move-exception v3

    #@3a
    monitor-exit v5
    :try_end_3b
    .catchall {:try_start_d .. :try_end_3b} :catchall_39

    #@3b
    throw v3

    #@3c
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_3c
    :try_start_3c
    monitor-exit v5
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_39

    #@3d
    .line 1924
    return-void
.end method
