.class abstract Landroid/accounts/AccountManagerService$Session;
.super Landroid/accounts/IAccountAuthenticatorResponse$Stub;
.source "AccountManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/AccountManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "Session"
.end annotation


# instance fields
.field final mAccountType:Ljava/lang/String;

.field protected final mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

.field mAuthenticator:Landroid/accounts/IAccountAuthenticator;

.field final mCreationTime:J

.field final mExpectActivityLaunch:Z

.field private mNumErrors:I

.field private mNumRequestContinued:I

.field public mNumResults:I

.field mResponse:Landroid/accounts/IAccountManagerResponse;

.field private final mStripAuthTokenFromResult:Z

.field final synthetic this$0:Landroid/accounts/AccountManagerService;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/IAccountManagerResponse;Ljava/lang/String;ZZ)V
    .registers 12
    .parameter
    .parameter "accounts"
    .parameter "response"
    .parameter "accountType"
    .parameter "expectActivityLaunch"
    .parameter "stripAuthTokenFromResult"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1685
    iput-object p1, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@4
    .line 1686
    invoke-direct {p0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;-><init>()V

    #@7
    .line 1674
    iput v1, p0, Landroid/accounts/AccountManagerService$Session;->mNumResults:I

    #@9
    .line 1675
    iput v1, p0, Landroid/accounts/AccountManagerService$Session;->mNumRequestContinued:I

    #@b
    .line 1676
    iput v1, p0, Landroid/accounts/AccountManagerService$Session;->mNumErrors:I

    #@d
    .line 1679
    iput-object v4, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@f
    .line 1687
    if-nez p3, :cond_1a

    #@11
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string/jumbo v2, "response is null"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 1688
    :cond_1a
    if-nez p4, :cond_24

    #@1c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1e
    const-string v2, "accountType is null"

    #@20
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 1689
    :cond_24
    iput-object p2, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@26
    .line 1690
    iput-boolean p6, p0, Landroid/accounts/AccountManagerService$Session;->mStripAuthTokenFromResult:Z

    #@28
    .line 1691
    iput-object p3, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@2a
    .line 1692
    iput-object p4, p0, Landroid/accounts/AccountManagerService$Session;->mAccountType:Ljava/lang/String;

    #@2c
    .line 1693
    iput-boolean p5, p0, Landroid/accounts/AccountManagerService$Session;->mExpectActivityLaunch:Z

    #@2e
    .line 1694
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@31
    move-result-wide v1

    #@32
    iput-wide v1, p0, Landroid/accounts/AccountManagerService$Session;->mCreationTime:J

    #@34
    .line 1695
    invoke-static {p1}, Landroid/accounts/AccountManagerService;->access$1500(Landroid/accounts/AccountManagerService;)Ljava/util/LinkedHashMap;

    #@37
    move-result-object v2

    #@38
    monitor-enter v2

    #@39
    .line 1696
    :try_start_39
    invoke-static {p1}, Landroid/accounts/AccountManagerService;->access$1500(Landroid/accounts/AccountManagerService;)Ljava/util/LinkedHashMap;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v1, v3, p0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    .line 1697
    monitor-exit v2
    :try_end_45
    .catchall {:try_start_39 .. :try_end_45} :catchall_4e

    #@45
    .line 1699
    :try_start_45
    invoke-interface {p3}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@48
    move-result-object v1

    #@49
    const/4 v2, 0x0

    #@4a
    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_4d
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_4d} :catch_51

    #@4d
    .line 1704
    :goto_4d
    return-void

    #@4e
    .line 1697
    :catchall_4e
    move-exception v1

    #@4f
    :try_start_4f
    monitor-exit v2
    :try_end_50
    .catchall {:try_start_4f .. :try_end_50} :catchall_4e

    #@50
    throw v1

    #@51
    .line 1700
    :catch_51
    move-exception v0

    #@52
    .line 1701
    .local v0, e:Landroid/os/RemoteException;
    iput-object v4, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@54
    .line 1702
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->binderDied()V

    #@57
    goto :goto_4d
.end method

.method private bindToAuthenticator(Ljava/lang/String;)Z
    .registers 10
    .parameter "authenticatorType"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v7, 0x2

    #@3
    .line 1898
    iget-object v4, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@5
    invoke-static {v4}, Landroid/accounts/AccountManagerService;->access$1800(Landroid/accounts/AccountManagerService;)Landroid/accounts/IAccountAuthenticatorCache;

    #@8
    move-result-object v4

    #@9
    invoke-static {p1}, Landroid/accounts/AuthenticatorDescription;->newKey(Ljava/lang/String;)Landroid/accounts/AuthenticatorDescription;

    #@c
    move-result-object v5

    #@d
    iget-object v6, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@f
    invoke-static {v6}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@12
    move-result v6

    #@13
    invoke-interface {v4, v5, v6}, Landroid/accounts/IAccountAuthenticatorCache;->getServiceInfo(Landroid/accounts/AuthenticatorDescription;I)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;

    #@16
    move-result-object v0

    #@17
    .line 1900
    .local v0, authenticatorInfo:Landroid/content/pm/RegisteredServicesCache$ServiceInfo;,"Landroid/content/pm/RegisteredServicesCache$ServiceInfo<Landroid/accounts/AuthenticatorDescription;>;"
    if-nez v0, :cond_41

    #@19
    .line 1901
    const-string v3, "AccountManagerService"

    #@1b
    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_40

    #@21
    .line 1902
    const-string v3, "AccountManagerService"

    #@23
    new-instance v4, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string/jumbo v5, "there is no authenticator for "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, ", bailing out"

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1922
    :cond_40
    :goto_40
    return v2

    #@41
    .line 1908
    :cond_41
    new-instance v1, Landroid/content/Intent;

    #@43
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@46
    .line 1909
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "android.accounts.AccountAuthenticator"

    #@48
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@4b
    .line 1910
    iget-object v4, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->componentName:Landroid/content/ComponentName;

    #@4d
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@50
    .line 1911
    const-string v4, "AccountManagerService"

    #@52
    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@55
    move-result v4

    #@56
    if-eqz v4, :cond_73

    #@58
    .line 1912
    const-string v4, "AccountManagerService"

    #@5a
    new-instance v5, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string/jumbo v6, "performing bindService to "

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    iget-object v6, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->componentName:Landroid/content/ComponentName;

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v5

    #@70
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 1914
    :cond_73
    iget-object v4, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@75
    invoke-static {v4}, Landroid/accounts/AccountManagerService;->access$1000(Landroid/accounts/AccountManagerService;)Landroid/content/Context;

    #@78
    move-result-object v4

    #@79
    iget-object v5, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@7b
    invoke-static {v5}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@7e
    move-result v5

    #@7f
    invoke-virtual {v4, v1, p0, v3, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@82
    move-result v4

    #@83
    if-nez v4, :cond_ae

    #@85
    .line 1915
    const-string v3, "AccountManagerService"

    #@87
    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@8a
    move-result v3

    #@8b
    if-eqz v3, :cond_40

    #@8d
    .line 1916
    const-string v3, "AccountManagerService"

    #@8f
    new-instance v4, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v5, "bindService to "

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    iget-object v5, v0, Landroid/content/pm/RegisteredServicesCache$ServiceInfo;->componentName:Landroid/content/ComponentName;

    #@9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    const-string v5, " failed"

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto :goto_40

    #@ae
    :cond_ae
    move v2, v3

    #@af
    .line 1922
    goto :goto_40
.end method

.method private close()V
    .registers 4

    #@0
    .prologue
    .line 1717
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@2
    invoke-static {v0}, Landroid/accounts/AccountManagerService;->access$1500(Landroid/accounts/AccountManagerService;)Ljava/util/LinkedHashMap;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 1718
    :try_start_7
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@9
    invoke-static {v0}, Landroid/accounts/AccountManagerService;->access$1500(Landroid/accounts/AccountManagerService;)Ljava/util/LinkedHashMap;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    if-nez v0, :cond_19

    #@17
    .line 1720
    monitor-exit v1

    #@18
    .line 1732
    :goto_18
    return-void

    #@19
    .line 1722
    :cond_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_7 .. :try_end_1a} :catchall_32

    #@1a
    .line 1723
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@1c
    if-eqz v0, :cond_2b

    #@1e
    .line 1725
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@20
    invoke-interface {v0}, Landroid/accounts/IAccountManagerResponse;->asBinder()Landroid/os/IBinder;

    #@23
    move-result-object v0

    #@24
    const/4 v1, 0x0

    #@25
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@28
    .line 1728
    const/4 v0, 0x0

    #@29
    iput-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@2b
    .line 1730
    :cond_2b
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->cancelTimeout()V

    #@2e
    .line 1731
    invoke-direct {p0}, Landroid/accounts/AccountManagerService$Session;->unbind()V

    #@31
    goto :goto_18

    #@32
    .line 1722
    :catchall_32
    move-exception v0

    #@33
    :try_start_33
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method private unbind()V
    .registers 2

    #@0
    .prologue
    .line 1762
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 1763
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@7
    .line 1764
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@9
    invoke-static {v0}, Landroid/accounts/AccountManagerService;->access$1000(Landroid/accounts/AccountManagerService;)Landroid/content/Context;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@10
    .line 1766
    :cond_10
    return-void
.end method


# virtual methods
.method bind()V
    .registers 4

    #@0
    .prologue
    .line 1752
    const-string v0, "AccountManagerService"

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_23

    #@9
    .line 1753
    const-string v0, "AccountManagerService"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "initiating bind to authenticator type "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Landroid/accounts/AccountManagerService$Session;->mAccountType:Ljava/lang/String;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1755
    :cond_23
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mAccountType:Ljava/lang/String;

    #@25
    invoke-direct {p0, v0}, Landroid/accounts/AccountManagerService$Session;->bindToAuthenticator(Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_4d

    #@2b
    .line 1756
    const-string v0, "AccountManagerService"

    #@2d
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "bind attempt failed for "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->toDebugString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1757
    const/4 v0, 0x1

    #@48
    const-string v1, "bind failure"

    #@4a
    invoke-virtual {p0, v0, v1}, Landroid/accounts/AccountManagerService$Session;->onError(ILjava/lang/String;)V

    #@4d
    .line 1759
    :cond_4d
    return-void
.end method

.method public binderDied()V
    .registers 2

    #@0
    .prologue
    .line 1735
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@3
    .line 1736
    invoke-direct {p0}, Landroid/accounts/AccountManagerService$Session;->close()V

    #@6
    .line 1737
    return-void
.end method

.method public cancelTimeout()V
    .registers 3

    #@0
    .prologue
    .line 1774
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@2
    invoke-static {v0}, Landroid/accounts/AccountManagerService;->access$1600(Landroid/accounts/AccountManagerService;)Landroid/accounts/AccountManagerService$MessageHandler;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x3

    #@7
    invoke-virtual {v0, v1, p0}, Landroid/accounts/AccountManagerService$MessageHandler;->removeMessages(ILjava/lang/Object;)V

    #@a
    .line 1775
    return-void
.end method

.method getResponseAndClose()Landroid/accounts/IAccountManagerResponse;
    .registers 3

    #@0
    .prologue
    .line 1707
    iget-object v1, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@2
    if-nez v1, :cond_6

    #@4
    .line 1709
    const/4 v0, 0x0

    #@5
    .line 1713
    :goto_5
    return-object v0

    #@6
    .line 1711
    :cond_6
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@8
    .line 1712
    .local v0, response:Landroid/accounts/IAccountManagerResponse;
    invoke-direct {p0}, Landroid/accounts/AccountManagerService$Session;->close()V

    #@b
    goto :goto_5
.end method

.method public onError(ILjava/lang/String;)V
    .registers 9
    .parameter "errorCode"
    .parameter "errorMessage"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 1871
    iget v2, p0, Landroid/accounts/AccountManagerService$Session;->mNumErrors:I

    #@3
    add-int/lit8 v2, v2, 0x1

    #@5
    iput v2, p0, Landroid/accounts/AccountManagerService$Session;->mNumErrors:I

    #@7
    .line 1872
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->getResponseAndClose()Landroid/accounts/IAccountManagerResponse;

    #@a
    move-result-object v1

    #@b
    .line 1873
    .local v1, response:Landroid/accounts/IAccountManagerResponse;
    if-eqz v1, :cond_4e

    #@d
    .line 1874
    const-string v2, "AccountManagerService"

    #@f
    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_39

    #@15
    .line 1875
    const-string v2, "AccountManagerService"

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, " calling onError() on response "

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1879
    :cond_39
    :try_start_39
    invoke-interface {v1, p1, p2}, Landroid/accounts/IAccountManagerResponse;->onError(ILjava/lang/String;)V
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_3c} :catch_3d

    #@3c
    .line 1890
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 1880
    :catch_3d
    move-exception v0

    #@3e
    .line 1881
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "AccountManagerService"

    #@40
    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@43
    move-result v2

    #@44
    if-eqz v2, :cond_3c

    #@46
    .line 1882
    const-string v2, "AccountManagerService"

    #@48
    const-string v3, "Session.onError: caught RemoteException while responding"

    #@4a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    goto :goto_3c

    #@4e
    .line 1886
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_4e
    const-string v2, "AccountManagerService"

    #@50
    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@53
    move-result v2

    #@54
    if-eqz v2, :cond_3c

    #@56
    .line 1887
    const-string v2, "AccountManagerService"

    #@58
    const-string v3, "Session.onError: already closed"

    #@5a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_3c
.end method

.method public onRequestContinued()V
    .registers 2

    #@0
    .prologue
    .line 1867
    iget v0, p0, Landroid/accounts/AccountManagerService$Session;->mNumRequestContinued:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/accounts/AccountManagerService$Session;->mNumRequestContinued:I

    #@6
    .line 1868
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .registers 12
    .parameter "result"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    .line 1821
    iget v5, p0, Landroid/accounts/AccountManagerService$Session;->mNumResults:I

    #@3
    add-int/lit8 v5, v5, 0x1

    #@5
    iput v5, p0, Landroid/accounts/AccountManagerService$Session;->mNumResults:I

    #@7
    .line 1822
    if-eqz p1, :cond_4e

    #@9
    const-string v5, "authtoken"

    #@b
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v5

    #@f
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@12
    move-result v5

    #@13
    if-nez v5, :cond_4e

    #@15
    .line 1823
    const-string v5, "authAccount"

    #@17
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 1824
    .local v1, accountName:Ljava/lang/String;
    const-string v5, "accountType"

    #@1d
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 1825
    .local v2, accountType:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_4e

    #@27
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2a
    move-result v5

    #@2b
    if-nez v5, :cond_4e

    #@2d
    .line 1826
    new-instance v0, Landroid/accounts/Account;

    #@2f
    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 1827
    .local v0, account:Landroid/accounts/Account;
    iget-object v5, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@34
    iget-object v6, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@36
    iget-object v7, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@38
    invoke-static {v6, v7, v0}, Landroid/accounts/AccountManagerService;->access$1700(Landroid/accounts/AccountManagerService;Landroid/accounts/AccountManagerService$UserAccounts;Landroid/accounts/Account;)Ljava/lang/Integer;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@3f
    move-result v6

    #@40
    new-instance v7, Landroid/os/UserHandle;

    #@42
    iget-object v8, p0, Landroid/accounts/AccountManagerService$Session;->mAccounts:Landroid/accounts/AccountManagerService$UserAccounts;

    #@44
    invoke-static {v8}, Landroid/accounts/AccountManagerService$UserAccounts;->access$400(Landroid/accounts/AccountManagerService$UserAccounts;)I

    #@47
    move-result v8

    #@48
    invoke-direct {v7, v8}, Landroid/os/UserHandle;-><init>(I)V

    #@4b
    invoke-virtual {v5, v6, v7}, Landroid/accounts/AccountManagerService;->cancelNotification(ILandroid/os/UserHandle;)V

    #@4e
    .line 1832
    .end local v0           #account:Landroid/accounts/Account;
    .end local v1           #accountName:Ljava/lang/String;
    .end local v2           #accountType:Ljava/lang/String;
    :cond_4e
    iget-boolean v5, p0, Landroid/accounts/AccountManagerService$Session;->mExpectActivityLaunch:Z

    #@50
    if-eqz v5, :cond_97

    #@52
    if-eqz p1, :cond_97

    #@54
    const-string v5, "intent"

    #@56
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@59
    move-result v5

    #@5a
    if-eqz v5, :cond_97

    #@5c
    .line 1834
    iget-object v4, p0, Landroid/accounts/AccountManagerService$Session;->mResponse:Landroid/accounts/IAccountManagerResponse;

    #@5e
    .line 1838
    .local v4, response:Landroid/accounts/IAccountManagerResponse;
    :goto_5e
    if-eqz v4, :cond_96

    #@60
    .line 1840
    if-nez p1, :cond_9c

    #@62
    .line 1841
    :try_start_62
    const-string v5, "AccountManagerService"

    #@64
    const/4 v6, 0x2

    #@65
    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@68
    move-result v5

    #@69
    if-eqz v5, :cond_8f

    #@6b
    .line 1842
    const-string v5, "AccountManagerService"

    #@6d
    new-instance v6, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    const-string v7, " calling onError() on response "

    #@80
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v6

    #@8c
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 1845
    :cond_8f
    const/4 v5, 0x5

    #@90
    const-string/jumbo v6, "null bundle returned"

    #@93
    invoke-interface {v4, v5, v6}, Landroid/accounts/IAccountManagerResponse;->onError(ILjava/lang/String;)V
    :try_end_96
    .catch Landroid/os/RemoteException; {:try_start_62 .. :try_end_96} :catch_d6

    #@96
    .line 1864
    :cond_96
    :goto_96
    return-void

    #@97
    .line 1836
    .end local v4           #response:Landroid/accounts/IAccountManagerResponse;
    :cond_97
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->getResponseAndClose()Landroid/accounts/IAccountManagerResponse;

    #@9a
    move-result-object v4

    #@9b
    .restart local v4       #response:Landroid/accounts/IAccountManagerResponse;
    goto :goto_5e

    #@9c
    .line 1848
    :cond_9c
    :try_start_9c
    iget-boolean v5, p0, Landroid/accounts/AccountManagerService$Session;->mStripAuthTokenFromResult:Z

    #@9e
    if-eqz v5, :cond_a5

    #@a0
    .line 1849
    const-string v5, "authtoken"

    #@a2
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    #@a5
    .line 1851
    :cond_a5
    const-string v5, "AccountManagerService"

    #@a7
    const/4 v6, 0x2

    #@a8
    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@ab
    move-result v5

    #@ac
    if-eqz v5, :cond_d2

    #@ae
    .line 1852
    const-string v5, "AccountManagerService"

    #@b0
    new-instance v6, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b8
    move-result-object v7

    #@b9
    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@bc
    move-result-object v7

    #@bd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v6

    #@c1
    const-string v7, " calling onResult() on response "

    #@c3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v6

    #@c7
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v6

    #@cb
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v6

    #@cf
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 1855
    :cond_d2
    invoke-interface {v4, p1}, Landroid/accounts/IAccountManagerResponse;->onResult(Landroid/os/Bundle;)V
    :try_end_d5
    .catch Landroid/os/RemoteException; {:try_start_9c .. :try_end_d5} :catch_d6

    #@d5
    goto :goto_96

    #@d6
    .line 1857
    :catch_d6
    move-exception v3

    #@d7
    .line 1859
    .local v3, e:Landroid/os/RemoteException;
    const-string v5, "AccountManagerService"

    #@d9
    invoke-static {v5, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@dc
    move-result v5

    #@dd
    if-eqz v5, :cond_96

    #@df
    .line 1860
    const-string v5, "AccountManagerService"

    #@e1
    const-string v6, "failure while notifying response"

    #@e3
    invoke-static {v5, v6, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e6
    goto :goto_96
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 6
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 1778
    invoke-static {p2}, Landroid/accounts/IAccountAuthenticator$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticator;

    #@3
    move-result-object v1

    #@4
    iput-object v1, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@6
    .line 1780
    :try_start_6
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->run()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_9} :catch_a

    #@9
    .line 1785
    :goto_9
    return-void

    #@a
    .line 1781
    :catch_a
    move-exception v0

    #@b
    .line 1782
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x1

    #@c
    const-string/jumbo v2, "remote exception"

    #@f
    invoke-virtual {p0, v1, v2}, Landroid/accounts/AccountManagerService$Session;->onError(ILjava/lang/String;)V

    #@12
    goto :goto_9
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "name"

    #@0
    .prologue
    .line 1788
    const/4 v2, 0x0

    #@1
    iput-object v2, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@3
    .line 1789
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->getResponseAndClose()Landroid/accounts/IAccountManagerResponse;

    #@6
    move-result-object v1

    #@7
    .line 1790
    .local v1, response:Landroid/accounts/IAccountManagerResponse;
    if-eqz v1, :cond_f

    #@9
    .line 1792
    const/4 v2, 0x1

    #@a
    :try_start_a
    const-string v3, "disconnected"

    #@c
    invoke-interface {v1, v2, v3}, Landroid/accounts/IAccountManagerResponse;->onError(ILjava/lang/String;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    .line 1801
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1794
    :catch_10
    move-exception v0

    #@11
    .line 1795
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "AccountManagerService"

    #@13
    const/4 v3, 0x2

    #@14
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_f

    #@1a
    .line 1796
    const-string v2, "AccountManagerService"

    #@1c
    const-string v3, "Session.onServiceDisconnected: caught RemoteException while responding"

    #@1e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_f
.end method

.method public onTimedOut()V
    .registers 5

    #@0
    .prologue
    .line 1806
    invoke-virtual {p0}, Landroid/accounts/AccountManagerService$Session;->getResponseAndClose()Landroid/accounts/IAccountManagerResponse;

    #@3
    move-result-object v1

    #@4
    .line 1807
    .local v1, response:Landroid/accounts/IAccountManagerResponse;
    if-eqz v1, :cond_d

    #@6
    .line 1809
    const/4 v2, 0x1

    #@7
    :try_start_7
    const-string/jumbo v3, "timeout"

    #@a
    invoke-interface {v1, v2, v3}, Landroid/accounts/IAccountManagerResponse;->onError(ILjava/lang/String;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_d} :catch_e

    #@d
    .line 1818
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1811
    :catch_e
    move-exception v0

    #@f
    .line 1812
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "AccountManagerService"

    #@11
    const/4 v3, 0x2

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_d

    #@18
    .line 1813
    const-string v2, "AccountManagerService"

    #@1a
    const-string v3, "Session.onTimedOut: caught RemoteException while responding"

    #@1c
    invoke-static {v2, v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_d
.end method

.method public abstract run()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public scheduleTimeout()V
    .registers 5

    #@0
    .prologue
    .line 1769
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@2
    invoke-static {v0}, Landroid/accounts/AccountManagerService;->access$1600(Landroid/accounts/AccountManagerService;)Landroid/accounts/AccountManagerService$MessageHandler;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Landroid/accounts/AccountManagerService$Session;->this$0:Landroid/accounts/AccountManagerService;

    #@8
    invoke-static {v1}, Landroid/accounts/AccountManagerService;->access$1600(Landroid/accounts/AccountManagerService;)Landroid/accounts/AccountManagerService$MessageHandler;

    #@b
    move-result-object v1

    #@c
    const/4 v2, 0x3

    #@d
    invoke-virtual {v1, v2, p0}, Landroid/accounts/AccountManagerService$MessageHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v1

    #@11
    const-wide/32 v2, 0xea60

    #@14
    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManagerService$MessageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@17
    .line 1771
    return-void
.end method

.method protected toDebugString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1740
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/accounts/AccountManagerService$Session;->toDebugString(J)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method protected toDebugString(J)Ljava/lang/String;
    .registers 8
    .parameter "now"

    #@0
    .prologue
    .line 1744
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Session: expectLaunch "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Landroid/accounts/AccountManagerService$Session;->mExpectActivityLaunch:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", connected "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget-object v0, p0, Landroid/accounts/AccountManagerService$Session;->mAuthenticator:Landroid/accounts/IAccountAuthenticator;

    #@19
    if-eqz v0, :cond_64

    #@1b
    const/4 v0, 0x1

    #@1c
    :goto_1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    const-string v1, ", stats ("

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    iget v1, p0, Landroid/accounts/AccountManagerService$Session;->mNumResults:I

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v0

    #@2c
    const-string v1, "/"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    iget v1, p0, Landroid/accounts/AccountManagerService$Session;->mNumRequestContinued:I

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    const-string v1, "/"

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    iget v1, p0, Landroid/accounts/AccountManagerService$Session;->mNumErrors:I

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    const-string v1, ")"

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    const-string v1, ", lifetime "

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    iget-wide v1, p0, Landroid/accounts/AccountManagerService$Session;->mCreationTime:J

    #@52
    sub-long v1, p1, v1

    #@54
    long-to-double v1, v1

    #@55
    const-wide v3, 0x408f400000000000L

    #@5a
    div-double/2addr v1, v3

    #@5b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    return-object v0

    #@64
    :cond_64
    const/4 v0, 0x0

    #@65
    goto :goto_1c
.end method
