.class public Landroid/accounts/AccountManager$BaseFutureTask$Response;
.super Landroid/accounts/IAccountManagerResponse$Stub;
.source "AccountManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/AccountManager$BaseFutureTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Response"
.end annotation


# instance fields
.field final synthetic this$1:Landroid/accounts/AccountManager$BaseFutureTask;


# direct methods
.method protected constructor <init>(Landroid/accounts/AccountManager$BaseFutureTask;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1517
    .local p0, this:Landroid/accounts/AccountManager$BaseFutureTask$Response;,"Landroid/accounts/AccountManager$BaseFutureTask<TT;>.Response;"
    iput-object p1, p0, Landroid/accounts/AccountManager$BaseFutureTask$Response;->this$1:Landroid/accounts/AccountManager$BaseFutureTask;

    #@2
    invoke-direct {p0}, Landroid/accounts/IAccountManagerResponse$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .registers 5
    .parameter "code"
    .parameter "message"

    #@0
    .prologue
    .line 1535
    .local p0, this:Landroid/accounts/AccountManager$BaseFutureTask$Response;,"Landroid/accounts/AccountManager$BaseFutureTask<TT;>.Response;"
    const/4 v0, 0x4

    #@1
    if-ne p1, v0, :cond_a

    #@3
    .line 1536
    iget-object v0, p0, Landroid/accounts/AccountManager$BaseFutureTask$Response;->this$1:Landroid/accounts/AccountManager$BaseFutureTask;

    #@5
    const/4 v1, 0x1

    #@6
    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager$BaseFutureTask;->cancel(Z)Z

    #@9
    .line 1540
    :goto_9
    return-void

    #@a
    .line 1539
    :cond_a
    iget-object v0, p0, Landroid/accounts/AccountManager$BaseFutureTask$Response;->this$1:Landroid/accounts/AccountManager$BaseFutureTask;

    #@c
    iget-object v1, p0, Landroid/accounts/AccountManager$BaseFutureTask$Response;->this$1:Landroid/accounts/AccountManager$BaseFutureTask;

    #@e
    iget-object v1, v1, Landroid/accounts/AccountManager$BaseFutureTask;->this$0:Landroid/accounts/AccountManager;

    #@10
    invoke-static {v1, p1, p2}, Landroid/accounts/AccountManager;->access$400(Landroid/accounts/AccountManager;ILjava/lang/String;)Ljava/lang/Exception;

    #@13
    move-result-object v1

    #@14
    invoke-static {v0, v1}, Landroid/accounts/AccountManager$BaseFutureTask;->access$800(Landroid/accounts/AccountManager$BaseFutureTask;Ljava/lang/Throwable;)V

    #@17
    goto :goto_9
.end method

.method public onResult(Landroid/os/Bundle;)V
    .registers 5
    .parameter "bundle"

    #@0
    .prologue
    .line 1520
    .local p0, this:Landroid/accounts/AccountManager$BaseFutureTask$Response;,"Landroid/accounts/AccountManager$BaseFutureTask<TT;>.Response;"
    :try_start_0
    iget-object v1, p0, Landroid/accounts/AccountManager$BaseFutureTask$Response;->this$1:Landroid/accounts/AccountManager$BaseFutureTask;

    #@2
    invoke-virtual {v1, p1}, Landroid/accounts/AccountManager$BaseFutureTask;->bundleToResult(Landroid/os/Bundle;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 1521
    .local v0, result:Ljava/lang/Object;,"TT;"
    if-nez v0, :cond_9

    #@8
    .line 1532
    .end local v0           #result:Ljava/lang/Object;,"TT;"
    :goto_8
    return-void

    #@9
    .line 1524
    .restart local v0       #result:Ljava/lang/Object;,"TT;"
    :cond_9
    iget-object v1, p0, Landroid/accounts/AccountManager$BaseFutureTask$Response;->this$1:Landroid/accounts/AccountManager$BaseFutureTask;

    #@b
    invoke-static {v1, v0}, Landroid/accounts/AccountManager$BaseFutureTask;->access$700(Landroid/accounts/AccountManager$BaseFutureTask;Ljava/lang/Object;)V
    :try_end_e
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_e} :catch_f
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_e} :catch_18

    #@e
    goto :goto_8

    #@f
    .line 1526
    .end local v0           #result:Ljava/lang/Object;,"TT;"
    :catch_f
    move-exception v1

    #@10
    .line 1531
    :goto_10
    const/4 v1, 0x5

    #@11
    const-string/jumbo v2, "no result in response"

    #@14
    invoke-virtual {p0, v1, v2}, Landroid/accounts/AccountManager$BaseFutureTask$Response;->onError(ILjava/lang/String;)V

    #@17
    goto :goto_8

    #@18
    .line 1528
    :catch_18
    move-exception v1

    #@19
    goto :goto_10
.end method
