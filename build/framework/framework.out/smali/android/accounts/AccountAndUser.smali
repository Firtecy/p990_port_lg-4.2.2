.class public Landroid/accounts/AccountAndUser;
.super Ljava/lang/Object;
.source "AccountAndUser.java"


# instance fields
.field public account:Landroid/accounts/Account;

.field public userId:I


# direct methods
.method public constructor <init>(Landroid/accounts/Account;I)V
    .registers 3
    .parameter "account"
    .parameter "userId"

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    iput-object p1, p0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@5
    .line 30
    iput p2, p0, Landroid/accounts/AccountAndUser;->userId:I

    #@7
    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 34
    if-ne p0, p1, :cond_5

    #@4
    .line 37
    :cond_4
    :goto_4
    return v1

    #@5
    .line 35
    :cond_5
    instance-of v3, p1, Landroid/accounts/AccountAndUser;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 36
    check-cast v0, Landroid/accounts/AccountAndUser;

    #@e
    .line 37
    .local v0, other:Landroid/accounts/AccountAndUser;
    iget-object v3, p0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@10
    iget-object v4, v0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@12
    invoke-virtual {v3, v4}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_1e

    #@18
    iget v3, p0, Landroid/accounts/AccountAndUser;->userId:I

    #@1a
    iget v4, v0, Landroid/accounts/AccountAndUser;->userId:I

    #@1c
    if-eq v3, v4, :cond_4

    #@1e
    :cond_1e
    move v1, v2

    #@1f
    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@2
    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/accounts/AccountAndUser;->userId:I

    #@8
    add-int/2addr v0, v1

    #@9
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    iget-object v1, p0, Landroid/accounts/AccountAndUser;->account:Landroid/accounts/Account;

    #@7
    invoke-virtual {v1}, Landroid/accounts/Account;->toString()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " u"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget v1, p0, Landroid/accounts/AccountAndUser;->userId:I

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    return-object v0
.end method
