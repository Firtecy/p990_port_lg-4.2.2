.class public abstract Landroid/accounts/IAccountAuthenticator$Stub;
.super Landroid/os/Binder;
.source "IAccountAuthenticator.java"

# interfaces
.implements Landroid/accounts/IAccountAuthenticator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accounts/IAccountAuthenticator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accounts/IAccountAuthenticator$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.accounts.IAccountAuthenticator"

.field static final TRANSACTION_addAccount:I = 0x1

.field static final TRANSACTION_confirmCredentials:I = 0x2

.field static final TRANSACTION_editProperties:I = 0x6

.field static final TRANSACTION_getAccountRemovalAllowed:I = 0x8

.field static final TRANSACTION_getAuthToken:I = 0x3

.field static final TRANSACTION_getAuthTokenLabel:I = 0x4

.field static final TRANSACTION_hasFeatures:I = 0x7

.field static final TRANSACTION_updateCredentials:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/accounts/IAccountAuthenticator$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticator;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.accounts.IAccountAuthenticator"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/accounts/IAccountAuthenticator;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/accounts/IAccountAuthenticator;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/accounts/IAccountAuthenticator$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/accounts/IAccountAuthenticator$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 42
    sparse-switch p1, :sswitch_data_158

    #@4
    .line 193
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 46
    :sswitch_9
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v0, v6

    #@f
    .line 47
    goto :goto_8

    #@10
    .line 51
    :sswitch_10
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@1c
    move-result-object v1

    #@1d
    .line 55
    .local v1, _arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 57
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    .line 59
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    .line 61
    .local v4, _arg3:[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_3d

    #@2f
    .line 62
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@31
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@34
    move-result-object v5

    #@35
    check-cast v5, Landroid/os/Bundle;

    #@37
    .local v5, _arg4:Landroid/os/Bundle;
    :goto_37
    move-object v0, p0

    #@38
    .line 67
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/IAccountAuthenticator$Stub;->addAccount(Landroid/accounts/IAccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)V

    #@3b
    move v0, v6

    #@3c
    .line 68
    goto :goto_8

    #@3d
    .line 65
    .end local v5           #_arg4:Landroid/os/Bundle;
    :cond_3d
    const/4 v5, 0x0

    #@3e
    .restart local v5       #_arg4:Landroid/os/Bundle;
    goto :goto_37

    #@3f
    .line 72
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:[Ljava/lang/String;
    .end local v5           #_arg4:Landroid/os/Bundle;
    :sswitch_3f
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@41
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@47
    move-result-object v0

    #@48
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@4b
    move-result-object v1

    #@4c
    .line 76
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v0

    #@50
    if-eqz v0, :cond_6d

    #@52
    .line 77
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@54
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@57
    move-result-object v2

    #@58
    check-cast v2, Landroid/accounts/Account;

    #@5a
    .line 83
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_5a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v0

    #@5e
    if-eqz v0, :cond_6f

    #@60
    .line 84
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@62
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@65
    move-result-object v3

    #@66
    check-cast v3, Landroid/os/Bundle;

    #@68
    .line 89
    .local v3, _arg2:Landroid/os/Bundle;
    :goto_68
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountAuthenticator$Stub;->confirmCredentials(Landroid/accounts/IAccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)V

    #@6b
    move v0, v6

    #@6c
    .line 90
    goto :goto_8

    #@6d
    .line 80
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Landroid/os/Bundle;
    :cond_6d
    const/4 v2, 0x0

    #@6e
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_5a

    #@6f
    .line 87
    :cond_6f
    const/4 v3, 0x0

    #@70
    .restart local v3       #_arg2:Landroid/os/Bundle;
    goto :goto_68

    #@71
    .line 94
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Landroid/os/Bundle;
    :sswitch_71
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@73
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@76
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@79
    move-result-object v0

    #@7a
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@7d
    move-result-object v1

    #@7e
    .line 98
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v0

    #@82
    if-eqz v0, :cond_a4

    #@84
    .line 99
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@86
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@89
    move-result-object v2

    #@8a
    check-cast v2, Landroid/accounts/Account;

    #@8c
    .line 105
    .restart local v2       #_arg1:Landroid/accounts/Account;
    :goto_8c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8f
    move-result-object v3

    #@90
    .line 107
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@93
    move-result v0

    #@94
    if-eqz v0, :cond_a6

    #@96
    .line 108
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@98
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@9b
    move-result-object v4

    #@9c
    check-cast v4, Landroid/os/Bundle;

    #@9e
    .line 113
    .local v4, _arg3:Landroid/os/Bundle;
    :goto_9e
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/accounts/IAccountAuthenticator$Stub;->getAuthToken(Landroid/accounts/IAccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@a1
    move v0, v6

    #@a2
    .line 114
    goto/16 :goto_8

    #@a4
    .line 102
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Landroid/os/Bundle;
    :cond_a4
    const/4 v2, 0x0

    #@a5
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_8c

    #@a6
    .line 111
    .restart local v3       #_arg2:Ljava/lang/String;
    :cond_a6
    const/4 v4, 0x0

    #@a7
    .restart local v4       #_arg3:Landroid/os/Bundle;
    goto :goto_9e

    #@a8
    .line 118
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Landroid/os/Bundle;
    :sswitch_a8
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@aa
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ad
    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b0
    move-result-object v0

    #@b1
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@b4
    move-result-object v1

    #@b5
    .line 122
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b8
    move-result-object v2

    #@b9
    .line 123
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountAuthenticator$Stub;->getAuthTokenLabel(Landroid/accounts/IAccountAuthenticatorResponse;Ljava/lang/String;)V

    #@bc
    move v0, v6

    #@bd
    .line 124
    goto/16 :goto_8

    #@bf
    .line 128
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_bf
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@c1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c4
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c7
    move-result-object v0

    #@c8
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@cb
    move-result-object v1

    #@cc
    .line 132
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cf
    move-result v0

    #@d0
    if-eqz v0, :cond_f2

    #@d2
    .line 133
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d4
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d7
    move-result-object v2

    #@d8
    check-cast v2, Landroid/accounts/Account;

    #@da
    .line 139
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_da
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@dd
    move-result-object v3

    #@de
    .line 141
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e1
    move-result v0

    #@e2
    if-eqz v0, :cond_f4

    #@e4
    .line 142
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e6
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e9
    move-result-object v4

    #@ea
    check-cast v4, Landroid/os/Bundle;

    #@ec
    .line 147
    .restart local v4       #_arg3:Landroid/os/Bundle;
    :goto_ec
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/accounts/IAccountAuthenticator$Stub;->updateCredentials(Landroid/accounts/IAccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    #@ef
    move v0, v6

    #@f0
    .line 148
    goto/16 :goto_8

    #@f2
    .line 136
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Landroid/os/Bundle;
    :cond_f2
    const/4 v2, 0x0

    #@f3
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_da

    #@f4
    .line 145
    .restart local v3       #_arg2:Ljava/lang/String;
    :cond_f4
    const/4 v4, 0x0

    #@f5
    .restart local v4       #_arg3:Landroid/os/Bundle;
    goto :goto_ec

    #@f6
    .line 152
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Landroid/os/Bundle;
    :sswitch_f6
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@f8
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fb
    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@fe
    move-result-object v0

    #@ff
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@102
    move-result-object v1

    #@103
    .line 156
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@106
    move-result-object v2

    #@107
    .line 157
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountAuthenticator$Stub;->editProperties(Landroid/accounts/IAccountAuthenticatorResponse;Ljava/lang/String;)V

    #@10a
    move v0, v6

    #@10b
    .line 158
    goto/16 :goto_8

    #@10d
    .line 162
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_10d
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@10f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@112
    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@115
    move-result-object v0

    #@116
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@119
    move-result-object v1

    #@11a
    .line 166
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11d
    move-result v0

    #@11e
    if-eqz v0, :cond_132

    #@120
    .line 167
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@122
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@125
    move-result-object v2

    #@126
    check-cast v2, Landroid/accounts/Account;

    #@128
    .line 173
    .local v2, _arg1:Landroid/accounts/Account;
    :goto_128
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@12b
    move-result-object v3

    #@12c
    .line 174
    .local v3, _arg2:[Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/accounts/IAccountAuthenticator$Stub;->hasFeatures(Landroid/accounts/IAccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)V

    #@12f
    move v0, v6

    #@130
    .line 175
    goto/16 :goto_8

    #@132
    .line 170
    .end local v2           #_arg1:Landroid/accounts/Account;
    .end local v3           #_arg2:[Ljava/lang/String;
    :cond_132
    const/4 v2, 0x0

    #@133
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_128

    #@134
    .line 179
    .end local v1           #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    .end local v2           #_arg1:Landroid/accounts/Account;
    :sswitch_134
    const-string v0, "android.accounts.IAccountAuthenticator"

    #@136
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@139
    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@13c
    move-result-object v0

    #@13d
    invoke-static {v0}, Landroid/accounts/IAccountAuthenticatorResponse$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accounts/IAccountAuthenticatorResponse;

    #@140
    move-result-object v1

    #@141
    .line 183
    .restart local v1       #_arg0:Landroid/accounts/IAccountAuthenticatorResponse;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@144
    move-result v0

    #@145
    if-eqz v0, :cond_155

    #@147
    .line 184
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    #@149
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14c
    move-result-object v2

    #@14d
    check-cast v2, Landroid/accounts/Account;

    #@14f
    .line 189
    .restart local v2       #_arg1:Landroid/accounts/Account;
    :goto_14f
    invoke-virtual {p0, v1, v2}, Landroid/accounts/IAccountAuthenticator$Stub;->getAccountRemovalAllowed(Landroid/accounts/IAccountAuthenticatorResponse;Landroid/accounts/Account;)V

    #@152
    move v0, v6

    #@153
    .line 190
    goto/16 :goto_8

    #@155
    .line 187
    .end local v2           #_arg1:Landroid/accounts/Account;
    :cond_155
    const/4 v2, 0x0

    #@156
    .restart local v2       #_arg1:Landroid/accounts/Account;
    goto :goto_14f

    #@157
    .line 42
    nop

    #@158
    :sswitch_data_158
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_3f
        0x3 -> :sswitch_71
        0x4 -> :sswitch_a8
        0x5 -> :sswitch_bf
        0x6 -> :sswitch_f6
        0x7 -> :sswitch_10d
        0x8 -> :sswitch_134
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
