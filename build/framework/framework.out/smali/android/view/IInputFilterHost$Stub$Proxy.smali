.class Landroid/view/IInputFilterHost$Stub$Proxy;
.super Ljava/lang/Object;
.source "IInputFilterHost.java"

# interfaces
.implements Landroid/view/IInputFilterHost;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IInputFilterHost$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    iput-object p1, p0, Landroid/view/IInputFilterHost$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 74
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/view/IInputFilterHost$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 81
    const-string v0, "android.view.IInputFilterHost"

    #@2
    return-object v0
.end method

.method public sendInputEvent(Landroid/view/InputEvent;I)V
    .registers 8
    .parameter "event"
    .parameter "policyFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 85
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 87
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IInputFilterHost"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 88
    if-eqz p1, :cond_22

    #@b
    .line 89
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 90
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/view/InputEvent;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 95
    :goto_13
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 96
    iget-object v1, p0, Landroid/view/IInputFilterHost$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v2, 0x1

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_27

    #@1e
    .line 99
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 101
    return-void

    #@22
    .line 93
    :cond_22
    const/4 v1, 0x0

    #@23
    :try_start_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_13

    #@27
    .line 99
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v1
.end method
