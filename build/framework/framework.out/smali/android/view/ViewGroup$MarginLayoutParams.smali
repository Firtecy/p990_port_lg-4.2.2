.class public Landroid/view/ViewGroup$MarginLayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "ViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MarginLayoutParams"
.end annotation


# static fields
.field public static final DEFAULT_RELATIVE:I = -0x80000000

.field private static LAYOUT_DIRECTION_UNDEFINED:I


# instance fields
.field public bottomMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private endMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private initialLeftMargin:I

.field private initialRightMargin:I

.field private layoutDirection:I

.field public leftMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field public rightMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private startMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field public topMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 5779
    const/4 v0, -0x1

    #@1
    sput v0, Landroid/view/ViewGroup$MarginLayoutParams;->LAYOUT_DIRECTION_UNDEFINED:I

    #@3
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/high16 v0, -0x8000

    #@2
    .line 5840
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@5
    .line 5759
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@7
    .line 5767
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@9
    .line 5782
    sget v0, Landroid/view/ViewGroup$MarginLayoutParams;->LAYOUT_DIRECTION_UNDEFINED:I

    #@b
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@d
    .line 5841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    const/high16 v5, -0x8000

    #@2
    const/4 v4, 0x0

    #@3
    .line 5793
    invoke-direct {p0}, Landroid/view/ViewGroup$LayoutParams;-><init>()V

    #@6
    .line 5759
    iput v5, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@8
    .line 5767
    iput v5, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@a
    .line 5782
    sget v2, Landroid/view/ViewGroup$MarginLayoutParams;->LAYOUT_DIRECTION_UNDEFINED:I

    #@c
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@e
    .line 5795
    sget-object v2, Lcom/android/internal/R$styleable;->ViewGroup_MarginLayout:[I

    #@10
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@13
    move-result-object v0

    #@14
    .line 5796
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x1

    #@15
    invoke-virtual {p0, v0, v4, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setBaseAttributes(Landroid/content/res/TypedArray;II)V

    #@18
    .line 5800
    const/4 v2, 0x2

    #@19
    const/4 v3, -0x1

    #@1a
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@1d
    move-result v1

    #@1e
    .line 5802
    .local v1, margin:I
    if-ltz v1, :cond_34

    #@20
    .line 5803
    iput v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@22
    .line 5804
    iput v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@24
    .line 5805
    iput v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@26
    .line 5806
    iput v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@28
    .line 5830
    :cond_28
    :goto_28
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@2a
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialLeftMargin:I

    #@2c
    .line 5831
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@2e
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialRightMargin:I

    #@30
    .line 5833
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@33
    .line 5834
    return-void

    #@34
    .line 5808
    :cond_34
    const/4 v2, 0x3

    #@35
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@38
    move-result v2

    #@39
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@3b
    .line 5810
    const/4 v2, 0x4

    #@3c
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@3f
    move-result v2

    #@40
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@42
    .line 5812
    const/4 v2, 0x5

    #@43
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@46
    move-result v2

    #@47
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@49
    .line 5814
    const/4 v2, 0x6

    #@4a
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@4d
    move-result v2

    #@4e
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@50
    .line 5816
    const/4 v2, 0x7

    #@51
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@54
    move-result v2

    #@55
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@57
    .line 5818
    const/16 v2, 0x8

    #@59
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@5c
    move-result v2

    #@5d
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@5f
    .line 5820
    sget-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@61
    if-eqz v2, :cond_6d

    #@63
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@65
    if-eqz v2, :cond_6d

    #@67
    .line 5821
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@69
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@6b
    .line 5822
    iput v4, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@6d
    .line 5824
    :cond_6d
    sget-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@6f
    if-eqz v2, :cond_28

    #@71
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@73
    if-eqz v2, :cond_28

    #@75
    .line 5825
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@77
    iput v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@79
    .line 5826
    iput v4, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@7b
    goto :goto_28
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    const/high16 v0, -0x8000

    #@2
    .line 5869
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    .line 5759
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@7
    .line 5767
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@9
    .line 5782
    sget v0, Landroid/view/ViewGroup$MarginLayoutParams;->LAYOUT_DIRECTION_UNDEFINED:I

    #@b
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@d
    .line 5870
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    const/high16 v0, -0x8000

    #@2
    .line 5848
    invoke-direct {p0}, Landroid/view/ViewGroup$LayoutParams;-><init>()V

    #@5
    .line 5759
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@7
    .line 5767
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@9
    .line 5782
    sget v0, Landroid/view/ViewGroup$MarginLayoutParams;->LAYOUT_DIRECTION_UNDEFINED:I

    #@b
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@d
    .line 5849
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@f
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@11
    .line 5850
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@13
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@15
    .line 5852
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@17
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@19
    .line 5853
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1b
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1d
    .line 5854
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@1f
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@21
    .line 5855
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@23
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@25
    .line 5856
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@27
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@29
    .line 5857
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@2b
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@2d
    .line 5859
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@2f
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialLeftMargin:I

    #@31
    .line 5860
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@33
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialRightMargin:I

    #@35
    .line 5862
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@37
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setLayoutDirection(I)V

    #@3a
    .line 5863
    return-void
.end method


# virtual methods
.method public getLayoutDirection()I
    .registers 2

    #@0
    .prologue
    .line 6012
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@2
    return v0
.end method

.method public getMarginEnd()I
    .registers 3

    #@0
    .prologue
    .line 5971
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@2
    const/high16 v1, -0x8000

    #@4
    if-eq v0, v1, :cond_9

    #@6
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@8
    .line 5977
    :goto_8
    return v0

    #@9
    .line 5972
    :cond_9
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@b
    packed-switch v0, :pswitch_data_14

    #@e
    .line 5977
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@10
    goto :goto_8

    #@11
    .line 5974
    :pswitch_11
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@13
    goto :goto_8

    #@14
    .line 5972
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_11
    .end packed-switch
.end method

.method public getMarginStart()I
    .registers 3

    #@0
    .prologue
    .line 5942
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@2
    const/high16 v1, -0x8000

    #@4
    if-eq v0, v1, :cond_9

    #@6
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@8
    .line 5948
    :goto_8
    return v0

    #@9
    .line 5943
    :cond_9
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@b
    packed-switch v0, :pswitch_data_14

    #@e
    .line 5948
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@10
    goto :goto_8

    #@11
    .line 5945
    :pswitch_11
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@13
    goto :goto_8

    #@14
    .line 5943
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_11
    .end packed-switch
.end method

.method public isLayoutRtl()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 6042
    iget v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@3
    if-ne v1, v0, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isMarginRelative()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, -0x8000

    #@2
    .line 5990
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@4
    if-ne v0, v1, :cond_a

    #@6
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@8
    if-eq v0, v1, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public onDebugDraw(Landroid/view/View;Landroid/graphics/Canvas;)V
    .registers 9
    .parameter "view"
    .parameter "canvas"

    #@0
    .prologue
    .line 6050
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@6
    sub-int v1, v0, v1

    #@8
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    #@b
    move-result v0

    #@c
    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@e
    sub-int v2, v0, v2

    #@10
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    #@13
    move-result v0

    #@14
    iget v3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@16
    add-int/2addr v3, v0

    #@17
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    #@1a
    move-result v0

    #@1b
    iget v4, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@1d
    add-int/2addr v4, v0

    #@1e
    const v5, -0xff01

    #@21
    move-object v0, p2

    #@22
    invoke-static/range {v0 .. v5}, Landroid/view/ViewGroup;->access$500(Landroid/graphics/Canvas;IIIII)V

    #@25
    .line 6055
    return-void
.end method

.method public resolveLayoutDirection(I)V
    .registers 4
    .parameter "layoutDirection"

    #@0
    .prologue
    const/high16 v1, -0x8000

    #@2
    .line 6021
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->setLayoutDirection(I)V

    #@5
    .line 6023
    invoke-virtual {p0}, Landroid/view/ViewGroup$MarginLayoutParams;->isMarginRelative()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_c

    #@b
    .line 6036
    :goto_b
    return-void

    #@c
    .line 6025
    :cond_c
    packed-switch p1, :pswitch_data_3e

    #@f
    .line 6032
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@11
    if-le v0, v1, :cond_37

    #@13
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@15
    :goto_15
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@17
    .line 6033
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@19
    if-le v0, v1, :cond_3a

    #@1b
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@1d
    :goto_1d
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@1f
    goto :goto_b

    #@20
    .line 6027
    :pswitch_20
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@22
    if-le v0, v1, :cond_31

    #@24
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@26
    :goto_26
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@28
    .line 6028
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@2a
    if-le v0, v1, :cond_34

    #@2c
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@2e
    :goto_2e
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@30
    goto :goto_b

    #@31
    .line 6027
    :cond_31
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialLeftMargin:I

    #@33
    goto :goto_26

    #@34
    .line 6028
    :cond_34
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialRightMargin:I

    #@36
    goto :goto_2e

    #@37
    .line 6032
    :cond_37
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialLeftMargin:I

    #@39
    goto :goto_15

    #@3a
    .line 6033
    :cond_3a
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialRightMargin:I

    #@3c
    goto :goto_1d

    #@3d
    .line 6025
    nop

    #@3e
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_20
    .end packed-switch
.end method

.method public setLayoutDirection(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 6000
    if-eqz p1, :cond_6

    #@2
    const/4 v0, 0x1

    #@3
    if-eq p1, v0, :cond_6

    #@5
    .line 6003
    :goto_5
    return-void

    #@6
    .line 6002
    :cond_6
    iput p1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->layoutDirection:I

    #@8
    goto :goto_5
.end method

.method public setMarginEnd(I)V
    .registers 2
    .parameter "end"

    #@0
    .prologue
    .line 5960
    iput p1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@2
    .line 5961
    return-void
.end method

.method public setMarginStart(I)V
    .registers 2
    .parameter "start"

    #@0
    .prologue
    .line 5931
    iput p1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@2
    .line 5932
    return-void
.end method

.method public setMargins(IIII)V
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 5888
    iput p1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@2
    .line 5889
    iput p2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@4
    .line 5890
    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@6
    .line 5891
    iput p4, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@8
    .line 5892
    iput p1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialLeftMargin:I

    #@a
    .line 5893
    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialRightMargin:I

    #@c
    .line 5894
    return-void
.end method

.method public setMarginsRelative(IIII)V
    .registers 6
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5915
    iput p1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->startMargin:I

    #@3
    .line 5916
    iput p2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@5
    .line 5917
    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->endMargin:I

    #@7
    .line 5918
    iput p4, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@9
    .line 5919
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialLeftMargin:I

    #@b
    .line 5920
    iput v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->initialRightMargin:I

    #@d
    .line 5921
    return-void
.end method
