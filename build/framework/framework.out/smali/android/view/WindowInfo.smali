.class public Landroid/view/WindowInfo;
.super Ljava/lang/Object;
.source "WindowInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/WindowInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_POOL_SIZE:I = 0x14

.field private static UNDEFINED:I

.field private static sPool:Landroid/view/WindowInfo;

.field private static sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field public compatibilityScale:F

.field public displayId:I

.field public final frame:Landroid/graphics/Rect;

.field public layer:I

.field private mInPool:Z

.field private mNext:Landroid/view/WindowInfo;

.field public token:Landroid/os/IBinder;

.field public final touchableRegion:Landroid/graphics/Rect;

.field public type:I

.field public visible:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    const/4 v0, -0x1

    #@1
    sput v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@3
    .line 35
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    sput-object v0, Landroid/view/WindowInfo;->sPoolLock:Ljava/lang/Object;

    #@a
    .line 163
    new-instance v0, Landroid/view/WindowInfo$1;

    #@c
    invoke-direct {v0}, Landroid/view/WindowInfo$1;-><init>()V

    #@f
    sput-object v0, Landroid/view/WindowInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@11
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@a
    .line 46
    new-instance v0, Landroid/graphics/Rect;

    #@c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v0, p0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@11
    .line 48
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@13
    iput v0, p0, Landroid/view/WindowInfo;->type:I

    #@15
    .line 50
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@17
    int-to-float v0, v0

    #@18
    iput v0, p0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@1a
    .line 54
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@1c
    iput v0, p0, Landroid/view/WindowInfo;->displayId:I

    #@1e
    .line 56
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@20
    iput v0, p0, Landroid/view/WindowInfo;->layer:I

    #@22
    .line 60
    return-void
.end method

.method static synthetic access$000(Landroid/view/WindowInfo;Landroid/os/Parcel;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/view/WindowInfo;->initFromParcel(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private clear()V
    .registers 2

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@3
    .line 136
    iget-object v0, p0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@5
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    #@8
    .line 137
    iget-object v0, p0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@a
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    #@d
    .line 138
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@f
    iput v0, p0, Landroid/view/WindowInfo;->type:I

    #@11
    .line 139
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@13
    int-to-float v0, v0

    #@14
    iput v0, p0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@16
    .line 140
    const/4 v0, 0x0

    #@17
    iput-boolean v0, p0, Landroid/view/WindowInfo;->visible:Z

    #@19
    .line 141
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@1b
    iput v0, p0, Landroid/view/WindowInfo;->displayId:I

    #@1d
    .line 142
    sget v0, Landroid/view/WindowInfo;->UNDEFINED:I

    #@1f
    iput v0, p0, Landroid/view/WindowInfo;->layer:I

    #@21
    .line 143
    return-void
.end method

.method private initFromParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "parcel"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@8
    .line 82
    iget-object v2, p0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/graphics/Rect;

    #@10
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@13
    .line 83
    iget-object v2, p0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@15
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/graphics/Rect;

    #@1b
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@1e
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/view/WindowInfo;->type:I

    #@24
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@2a
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v0

    #@2e
    if-ne v0, v1, :cond_40

    #@30
    move v0, v1

    #@31
    :goto_31
    iput-boolean v0, p0, Landroid/view/WindowInfo;->visible:Z

    #@33
    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v0

    #@37
    iput v0, p0, Landroid/view/WindowInfo;->displayId:I

    #@39
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    iput v0, p0, Landroid/view/WindowInfo;->layer:I

    #@3f
    .line 89
    return-void

    #@40
    .line 86
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_31
.end method

.method public static obtain()Landroid/view/WindowInfo;
    .registers 3

    #@0
    .prologue
    .line 105
    sget-object v2, Landroid/view/WindowInfo;->sPoolLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 106
    :try_start_3
    sget v1, Landroid/view/WindowInfo;->sPoolSize:I

    #@5
    if-lez v1, :cond_1b

    #@7
    .line 107
    sget-object v0, Landroid/view/WindowInfo;->sPool:Landroid/view/WindowInfo;

    #@9
    .line 108
    .local v0, info:Landroid/view/WindowInfo;
    iget-object v1, v0, Landroid/view/WindowInfo;->mNext:Landroid/view/WindowInfo;

    #@b
    sput-object v1, Landroid/view/WindowInfo;->sPool:Landroid/view/WindowInfo;

    #@d
    .line 109
    const/4 v1, 0x0

    #@e
    iput-object v1, v0, Landroid/view/WindowInfo;->mNext:Landroid/view/WindowInfo;

    #@10
    .line 110
    const/4 v1, 0x0

    #@11
    iput-boolean v1, v0, Landroid/view/WindowInfo;->mInPool:Z

    #@13
    .line 111
    sget v1, Landroid/view/WindowInfo;->sPoolSize:I

    #@15
    add-int/lit8 v1, v1, -0x1

    #@17
    sput v1, Landroid/view/WindowInfo;->sPoolSize:I

    #@19
    .line 112
    monitor-exit v2

    #@1a
    .line 114
    .end local v0           #info:Landroid/view/WindowInfo;
    :goto_1a
    return-object v0

    #@1b
    :cond_1b
    new-instance v0, Landroid/view/WindowInfo;

    #@1d
    invoke-direct {v0}, Landroid/view/WindowInfo;-><init>()V

    #@20
    monitor-exit v2

    #@21
    goto :goto_1a

    #@22
    .line 116
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public static obtain(Landroid/view/WindowInfo;)Landroid/view/WindowInfo;
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 92
    invoke-static {}, Landroid/view/WindowInfo;->obtain()Landroid/view/WindowInfo;

    #@3
    move-result-object v0

    #@4
    .line 93
    .local v0, info:Landroid/view/WindowInfo;
    iget-object v1, p0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@6
    iput-object v1, v0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@8
    .line 94
    iget-object v1, v0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@a
    iget-object v2, p0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@c
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@f
    .line 95
    iget-object v1, v0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@11
    iget-object v2, p0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@13
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@16
    .line 96
    iget v1, p0, Landroid/view/WindowInfo;->type:I

    #@18
    iput v1, v0, Landroid/view/WindowInfo;->type:I

    #@1a
    .line 97
    iget v1, p0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@1c
    iput v1, v0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@1e
    .line 98
    iget-boolean v1, p0, Landroid/view/WindowInfo;->visible:Z

    #@20
    iput-boolean v1, v0, Landroid/view/WindowInfo;->visible:Z

    #@22
    .line 99
    iget v1, p0, Landroid/view/WindowInfo;->displayId:I

    #@24
    iput v1, v0, Landroid/view/WindowInfo;->displayId:I

    #@26
    .line 100
    iget v1, p0, Landroid/view/WindowInfo;->layer:I

    #@28
    iput v1, v0, Landroid/view/WindowInfo;->layer:I

    #@2a
    .line 101
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 120
    iget-boolean v0, p0, Landroid/view/WindowInfo;->mInPool:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Already recycled."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 123
    :cond_c
    invoke-direct {p0}, Landroid/view/WindowInfo;->clear()V

    #@f
    .line 124
    sget-object v1, Landroid/view/WindowInfo;->sPoolLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 125
    :try_start_12
    sget v0, Landroid/view/WindowInfo;->sPoolSize:I

    #@14
    const/16 v2, 0x14

    #@16
    if-ge v0, v2, :cond_27

    #@18
    .line 126
    sget-object v0, Landroid/view/WindowInfo;->sPool:Landroid/view/WindowInfo;

    #@1a
    iput-object v0, p0, Landroid/view/WindowInfo;->mNext:Landroid/view/WindowInfo;

    #@1c
    .line 127
    sput-object p0, Landroid/view/WindowInfo;->sPool:Landroid/view/WindowInfo;

    #@1e
    .line 128
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Landroid/view/WindowInfo;->mInPool:Z

    #@21
    .line 129
    sget v0, Landroid/view/WindowInfo;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Landroid/view/WindowInfo;->sPoolSize:I

    #@27
    .line 131
    :cond_27
    monitor-exit v1

    #@28
    .line 132
    return-void

    #@29
    .line 131
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 148
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "Window [token:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    iget-object v1, p0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@d
    if-eqz v1, :cond_73

    #@f
    iget-object v1, p0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@11
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@14
    move-result v1

    #@15
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v1

    #@19
    :goto_19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    .line 149
    const-string v1, ", displayId:"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget v2, p0, Landroid/view/WindowInfo;->displayId:I

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    .line 150
    const-string v1, ", type:"

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    iget v2, p0, Landroid/view/WindowInfo;->type:I

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    .line 151
    const-string v1, ", visible:"

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    iget-boolean v2, p0, Landroid/view/WindowInfo;->visible:Z

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d
    .line 152
    const-string v1, ", layer:"

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    iget v2, p0, Landroid/view/WindowInfo;->layer:I

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    .line 153
    const-string v1, ", compatibilityScale:"

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    iget v2, p0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@53
    .line 154
    const-string v1, ", frame:"

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    iget-object v2, p0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    .line 155
    const-string v1, ", touchableRegion:"

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v1

    #@64
    iget-object v2, p0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    .line 156
    const-string v1, "]"

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 157
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    return-object v1

    #@73
    .line 148
    :cond_73
    const/4 v1, 0x0

    #@74
    goto :goto_19
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 69
    iget-object v1, p0, Landroid/view/WindowInfo;->token:Landroid/os/IBinder;

    #@3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@6
    .line 70
    iget-object v1, p0, Landroid/view/WindowInfo;->frame:Landroid/graphics/Rect;

    #@8
    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@b
    .line 71
    iget-object v1, p0, Landroid/view/WindowInfo;->touchableRegion:Landroid/graphics/Rect;

    #@d
    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@10
    .line 72
    iget v1, p0, Landroid/view/WindowInfo;->type:I

    #@12
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 73
    iget v1, p0, Landroid/view/WindowInfo;->compatibilityScale:F

    #@17
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeFloat(F)V

    #@1a
    .line 74
    iget-boolean v1, p0, Landroid/view/WindowInfo;->visible:Z

    #@1c
    if-eqz v1, :cond_1f

    #@1e
    const/4 v0, 0x1

    #@1f
    :cond_1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 75
    iget v0, p0, Landroid/view/WindowInfo;->displayId:I

    #@24
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 76
    iget v0, p0, Landroid/view/WindowInfo;->layer:I

    #@29
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 77
    invoke-virtual {p0}, Landroid/view/WindowInfo;->recycle()V

    #@2f
    .line 78
    return-void
.end method
