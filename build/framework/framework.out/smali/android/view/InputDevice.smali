.class public final Landroid/view/InputDevice;
.super Ljava/lang/Object;
.source "InputDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/InputDevice$MotionRange;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/InputDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEYBOARD_TYPE_ALPHABETIC:I = 0x2

.field public static final KEYBOARD_TYPE_NONE:I = 0x0

.field public static final KEYBOARD_TYPE_NON_ALPHABETIC:I = 0x1

.field public static final MOTION_RANGE_ORIENTATION:I = 0x8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_PRESSURE:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_SIZE:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_TOOL_MAJOR:I = 0x6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_TOOL_MINOR:I = 0x7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_TOUCH_MAJOR:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_TOUCH_MINOR:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_X:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MOTION_RANGE_Y:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SOURCE_ANY:I = -0x100

.field public static final SOURCE_CLASS_BUTTON:I = 0x1

.field public static final SOURCE_CLASS_JOYSTICK:I = 0x10

.field public static final SOURCE_CLASS_MASK:I = 0xff

.field public static final SOURCE_CLASS_POINTER:I = 0x2

.field public static final SOURCE_CLASS_POSITION:I = 0x8

.field public static final SOURCE_CLASS_TRACKBALL:I = 0x4

.field public static final SOURCE_DPAD:I = 0x201

.field public static final SOURCE_GAMEPAD:I = 0x401

.field public static final SOURCE_JOYSTICK:I = 0x1000010

.field public static final SOURCE_KEYBOARD:I = 0x101

.field public static final SOURCE_MOUSE:I = 0x2002

.field public static final SOURCE_STYLUS:I = 0x4002

.field public static final SOURCE_TOUCHPAD:I = 0x100008

.field public static final SOURCE_TOUCHSCREEN:I = 0x1002

.field public static final SOURCE_TRACKBALL:I = 0x10004

.field public static final SOURCE_UNKNOWN:I


# instance fields
.field private final mDescriptor:Ljava/lang/String;

.field private final mGeneration:I

.field private final mHasVibrator:Z

.field private final mId:I

.field private final mIsExternal:Z

.field private final mKeyCharacterMap:Landroid/view/KeyCharacterMap;

.field private final mKeyboardType:I

.field private final mMotionRanges:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/InputDevice$MotionRange;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private final mSources:I

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 315
    new-instance v0, Landroid/view/InputDevice$1;

    #@2
    invoke-direct {v0}, Landroid/view/InputDevice$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/InputDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(IILjava/lang/String;Ljava/lang/String;ZIILandroid/view/KeyCharacterMap;Z)V
    .registers 11
    .parameter "id"
    .parameter "generation"
    .parameter "name"
    .parameter "descriptor"
    .parameter "isExternal"
    .parameter "sources"
    .parameter "keyboardType"
    .parameter "keyCharacterMap"
    .parameter "hasVibrator"

    #@0
    .prologue
    .line 328
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@a
    .line 329
    iput p1, p0, Landroid/view/InputDevice;->mId:I

    #@c
    .line 330
    iput p2, p0, Landroid/view/InputDevice;->mGeneration:I

    #@e
    .line 331
    iput-object p3, p0, Landroid/view/InputDevice;->mName:Ljava/lang/String;

    #@10
    .line 332
    iput-object p4, p0, Landroid/view/InputDevice;->mDescriptor:Ljava/lang/String;

    #@12
    .line 333
    iput-boolean p5, p0, Landroid/view/InputDevice;->mIsExternal:Z

    #@14
    .line 334
    iput p6, p0, Landroid/view/InputDevice;->mSources:I

    #@16
    .line 335
    iput p7, p0, Landroid/view/InputDevice;->mKeyboardType:I

    #@18
    .line 336
    iput-object p8, p0, Landroid/view/InputDevice;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@1a
    .line 337
    iput-boolean p9, p0, Landroid/view/InputDevice;->mHasVibrator:Z

    #@1c
    .line 338
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 9
    .parameter "in"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 340
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@c
    .line 341
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/view/InputDevice;->mId:I

    #@12
    .line 342
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/view/InputDevice;->mGeneration:I

    #@18
    .line 343
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/view/InputDevice;->mName:Ljava/lang/String;

    #@1e
    .line 344
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Landroid/view/InputDevice;->mDescriptor:Ljava/lang/String;

    #@24
    .line 345
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_52

    #@2a
    move v0, v2

    #@2b
    :goto_2b
    iput-boolean v0, p0, Landroid/view/InputDevice;->mIsExternal:Z

    #@2d
    .line 346
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    iput v0, p0, Landroid/view/InputDevice;->mSources:I

    #@33
    .line 347
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v0

    #@37
    iput v0, p0, Landroid/view/InputDevice;->mKeyboardType:I

    #@39
    .line 348
    sget-object v0, Landroid/view/KeyCharacterMap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3b
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Landroid/view/KeyCharacterMap;

    #@41
    iput-object v0, p0, Landroid/view/InputDevice;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@43
    .line 349
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_54

    #@49
    :goto_49
    iput-boolean v2, p0, Landroid/view/InputDevice;->mHasVibrator:Z

    #@4b
    .line 352
    :goto_4b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v1

    #@4f
    .line 353
    .local v1, axis:I
    if-gez v1, :cond_56

    #@51
    .line 359
    return-void

    #@52
    .end local v1           #axis:I
    :cond_52
    move v0, v3

    #@53
    .line 345
    goto :goto_2b

    #@54
    :cond_54
    move v2, v3

    #@55
    .line 349
    goto :goto_49

    #@56
    .line 356
    .restart local v1       #axis:I
    :cond_56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@59
    move-result v2

    #@5a
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@5d
    move-result v3

    #@5e
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@61
    move-result v4

    #@62
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@65
    move-result v5

    #@66
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@69
    move-result v6

    #@6a
    move-object v0, p0

    #@6b
    invoke-direct/range {v0 .. v6}, Landroid/view/InputDevice;->addMotionRange(IIFFFF)V

    #@6e
    goto :goto_4b
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/view/InputDevice$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/view/InputDevice;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private addMotionRange(IIFFFF)V
    .registers 16
    .parameter "axis"
    .parameter "source"
    .parameter "min"
    .parameter "max"
    .parameter "flat"
    .parameter "fuzz"

    #@0
    .prologue
    .line 570
    iget-object v8, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@2
    new-instance v0, Landroid/view/InputDevice$MotionRange;

    #@4
    const/4 v7, 0x0

    #@5
    move v1, p1

    #@6
    move v2, p2

    #@7
    move v3, p3

    #@8
    move v4, p4

    #@9
    move v5, p5

    #@a
    move v6, p6

    #@b
    invoke-direct/range {v0 .. v7}, Landroid/view/InputDevice$MotionRange;-><init>(IIFFFFLandroid/view/InputDevice$1;)V

    #@e
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11
    .line 571
    return-void
.end method

.method private appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V
    .registers 5
    .parameter "description"
    .parameter "source"
    .parameter "sourceName"

    #@0
    .prologue
    .line 767
    iget v0, p0, Landroid/view/InputDevice;->mSources:I

    #@2
    and-int/2addr v0, p2

    #@3
    if-ne v0, p2, :cond_d

    #@5
    .line 768
    const-string v0, " "

    #@7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 769
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 771
    :cond_d
    return-void
.end method

.method public static getDevice(I)Landroid/view/InputDevice;
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 367
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getDeviceIds()[I
    .registers 1

    #@0
    .prologue
    .line 375
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 712
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 430
    iget-object v0, p0, Landroid/view/InputDevice;->mDescriptor:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getGeneration()I
    .registers 2

    #@0
    .prologue
    .line 404
    iget v0, p0, Landroid/view/InputDevice;->mGeneration:I

    #@2
    return v0
.end method

.method public getId()I
    .registers 2

    #@0
    .prologue
    .line 391
    iget v0, p0, Landroid/view/InputDevice;->mId:I

    #@2
    return v0
.end method

.method public getKeyCharacterMap()Landroid/view/KeyCharacterMap;
    .registers 2

    #@0
    .prologue
    .line 502
    iget-object v0, p0, Landroid/view/InputDevice;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@2
    return-object v0
.end method

.method public getKeyboardType()I
    .registers 2

    #@0
    .prologue
    .line 494
    iget v0, p0, Landroid/view/InputDevice;->mKeyboardType:I

    #@2
    return v0
.end method

.method public getMotionRange(I)Landroid/view/InputDevice$MotionRange;
    .registers 6
    .parameter "axis"

    #@0
    .prologue
    .line 521
    iget-object v3, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 522
    .local v1, numRanges:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_1b

    #@9
    .line 523
    iget-object v3, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/view/InputDevice$MotionRange;

    #@11
    .line 524
    .local v2, range:Landroid/view/InputDevice$MotionRange;
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$100(Landroid/view/InputDevice$MotionRange;)I

    #@14
    move-result v3

    #@15
    if-ne v3, p1, :cond_18

    #@17
    .line 528
    .end local v2           #range:Landroid/view/InputDevice$MotionRange;
    :goto_17
    return-object v2

    #@18
    .line 522
    .restart local v2       #range:Landroid/view/InputDevice$MotionRange;
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_7

    #@1b
    .line 528
    .end local v2           #range:Landroid/view/InputDevice$MotionRange;
    :cond_1b
    const/4 v2, 0x0

    #@1c
    goto :goto_17
.end method

.method public getMotionRange(II)Landroid/view/InputDevice$MotionRange;
    .registers 7
    .parameter "axis"
    .parameter "source"

    #@0
    .prologue
    .line 547
    iget-object v3, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 548
    .local v1, numRanges:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_21

    #@9
    .line 549
    iget-object v3, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/view/InputDevice$MotionRange;

    #@11
    .line 550
    .local v2, range:Landroid/view/InputDevice$MotionRange;
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$100(Landroid/view/InputDevice$MotionRange;)I

    #@14
    move-result v3

    #@15
    if-ne v3, p1, :cond_1e

    #@17
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$200(Landroid/view/InputDevice$MotionRange;)I

    #@1a
    move-result v3

    #@1b
    if-ne v3, p2, :cond_1e

    #@1d
    .line 554
    .end local v2           #range:Landroid/view/InputDevice$MotionRange;
    :goto_1d
    return-object v2

    #@1e
    .line 548
    .restart local v2       #range:Landroid/view/InputDevice$MotionRange;
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_7

    #@21
    .line 554
    .end local v2           #range:Landroid/view/InputDevice$MotionRange;
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_1d
.end method

.method public getMotionRanges()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/InputDevice$MotionRange;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 478
    iget-object v0, p0, Landroid/view/InputDevice;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSources()I
    .registers 2

    #@0
    .prologue
    .line 486
    iget v0, p0, Landroid/view/InputDevice;->mSources:I

    #@2
    return v0
.end method

.method public getVibrator()Landroid/os/Vibrator;
    .registers 4

    #@0
    .prologue
    .line 586
    iget-object v1, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 587
    :try_start_3
    iget-object v0, p0, Landroid/view/InputDevice;->mVibrator:Landroid/os/Vibrator;

    #@5
    if-nez v0, :cond_17

    #@7
    .line 588
    iget-boolean v0, p0, Landroid/view/InputDevice;->mHasVibrator:Z

    #@9
    if-eqz v0, :cond_1b

    #@b
    .line 589
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@e
    move-result-object v0

    #@f
    iget v2, p0, Landroid/view/InputDevice;->mId:I

    #@11
    invoke-virtual {v0, v2}, Landroid/hardware/input/InputManager;->getInputDeviceVibrator(I)Landroid/os/Vibrator;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/view/InputDevice;->mVibrator:Landroid/os/Vibrator;

    #@17
    .line 594
    :cond_17
    :goto_17
    iget-object v0, p0, Landroid/view/InputDevice;->mVibrator:Landroid/os/Vibrator;

    #@19
    monitor-exit v1

    #@1a
    return-object v0

    #@1b
    .line 591
    :cond_1b
    invoke-static {}, Landroid/os/NullVibrator;->getInstance()Landroid/os/NullVibrator;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/view/InputDevice;->mVibrator:Landroid/os/Vibrator;

    #@21
    goto :goto_17

    #@22
    .line 595
    :catchall_22
    move-exception v0

    #@23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v0
.end method

.method public isExternal()Z
    .registers 2

    #@0
    .prologue
    .line 458
    iget-boolean v0, p0, Landroid/view/InputDevice;->mIsExternal:Z

    #@2
    return v0
.end method

.method public isFullKeyboard()Z
    .registers 3

    #@0
    .prologue
    .line 469
    iget v0, p0, Landroid/view/InputDevice;->mSources:I

    #@2
    and-int/lit16 v0, v0, 0x101

    #@4
    const/16 v1, 0x101

    #@6
    if-ne v0, v1, :cond_f

    #@8
    iget v0, p0, Landroid/view/InputDevice;->mKeyboardType:I

    #@a
    const/4 v1, 0x2

    #@b
    if-ne v0, v1, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public isVirtual()Z
    .registers 2

    #@0
    .prologue
    .line 446
    iget v0, p0, Landroid/view/InputDevice;->mId:I

    #@2
    if-gez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 717
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 718
    .local v0, description:Ljava/lang/StringBuilder;
    const-string v4, "Input Device "

    #@7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v4

    #@b
    iget v5, p0, Landroid/view/InputDevice;->mId:I

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, ": "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    iget-object v5, p0, Landroid/view/InputDevice;->mName:Ljava/lang/String;

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    const-string v5, "\n"

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 719
    const-string v4, "  Descriptor: "

    #@24
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    iget-object v5, p0, Landroid/view/InputDevice;->mDescriptor:Ljava/lang/String;

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, "\n"

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 720
    const-string v4, "  Generation: "

    #@35
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    iget v5, p0, Landroid/view/InputDevice;->mGeneration:I

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, "\n"

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 721
    const-string v4, "  Location: "

    #@46
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    iget-boolean v4, p0, Landroid/view/InputDevice;->mIsExternal:Z

    #@4c
    if-eqz v4, :cond_14b

    #@4e
    const-string v4, "external"

    #@50
    :goto_50
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, "\n"

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    .line 723
    const-string v4, "  Keyboard Type: "

    #@5b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 724
    iget v4, p0, Landroid/view/InputDevice;->mKeyboardType:I

    #@60
    packed-switch v4, :pswitch_data_16c

    #@63
    .line 735
    :goto_63
    const-string v4, "\n"

    #@65
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    .line 737
    const-string v4, "  Has Vibrator: "

    #@6a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    iget-boolean v5, p0, Landroid/view/InputDevice;->mHasVibrator:Z

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    const-string v5, "\n"

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    .line 739
    const-string v4, "  Sources: 0x"

    #@7b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    iget v5, p0, Landroid/view/InputDevice;->mSources:I

    #@81
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    const-string v5, " ("

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    .line 740
    const/16 v4, 0x101

    #@90
    const-string/jumbo v5, "keyboard"

    #@93
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@96
    .line 741
    const/16 v4, 0x201

    #@98
    const-string v5, "dpad"

    #@9a
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@9d
    .line 742
    const/16 v4, 0x1002

    #@9f
    const-string/jumbo v5, "touchscreen"

    #@a2
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@a5
    .line 743
    const/16 v4, 0x2002

    #@a7
    const-string/jumbo v5, "mouse"

    #@aa
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@ad
    .line 744
    const/16 v4, 0x4002

    #@af
    const-string/jumbo v5, "stylus"

    #@b2
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@b5
    .line 745
    const v4, 0x10004

    #@b8
    const-string/jumbo v5, "trackball"

    #@bb
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@be
    .line 746
    const v4, 0x100008

    #@c1
    const-string/jumbo v5, "touchpad"

    #@c4
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@c7
    .line 747
    const v4, 0x1000010

    #@ca
    const-string/jumbo v5, "joystick"

    #@cd
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@d0
    .line 748
    const/16 v4, 0x401

    #@d2
    const-string v5, "gamepad"

    #@d4
    invoke-direct {p0, v0, v4, v5}, Landroid/view/InputDevice;->appendSourceDescriptionIfApplicable(Ljava/lang/StringBuilder;ILjava/lang/String;)V

    #@d7
    .line 749
    const-string v4, " )\n"

    #@d9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    .line 751
    iget-object v4, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@de
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@e1
    move-result v2

    #@e2
    .line 752
    .local v2, numAxes:I
    const/4 v1, 0x0

    #@e3
    .local v1, i:I
    :goto_e3
    if-ge v1, v2, :cond_166

    #@e5
    .line 753
    iget-object v4, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@e7
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ea
    move-result-object v3

    #@eb
    check-cast v3, Landroid/view/InputDevice$MotionRange;

    #@ed
    .line 754
    .local v3, range:Landroid/view/InputDevice$MotionRange;
    const-string v4, "    "

    #@ef
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v4

    #@f3
    invoke-static {v3}, Landroid/view/InputDevice$MotionRange;->access$100(Landroid/view/InputDevice$MotionRange;)I

    #@f6
    move-result v5

    #@f7
    invoke-static {v5}, Landroid/view/MotionEvent;->axisToString(I)Ljava/lang/String;

    #@fa
    move-result-object v5

    #@fb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    .line 755
    const-string v4, ": source=0x"

    #@100
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v4

    #@104
    invoke-static {v3}, Landroid/view/InputDevice$MotionRange;->access$200(Landroid/view/InputDevice$MotionRange;)I

    #@107
    move-result v5

    #@108
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10b
    move-result-object v5

    #@10c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    .line 756
    const-string v4, " min="

    #@111
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v4

    #@115
    invoke-static {v3}, Landroid/view/InputDevice$MotionRange;->access$400(Landroid/view/InputDevice$MotionRange;)F

    #@118
    move-result v5

    #@119
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@11c
    .line 757
    const-string v4, " max="

    #@11e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v4

    #@122
    invoke-static {v3}, Landroid/view/InputDevice$MotionRange;->access$500(Landroid/view/InputDevice$MotionRange;)F

    #@125
    move-result v5

    #@126
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@129
    .line 758
    const-string v4, " flat="

    #@12b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v4

    #@12f
    invoke-static {v3}, Landroid/view/InputDevice$MotionRange;->access$600(Landroid/view/InputDevice$MotionRange;)F

    #@132
    move-result v5

    #@133
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@136
    .line 759
    const-string v4, " fuzz="

    #@138
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v4

    #@13c
    invoke-static {v3}, Landroid/view/InputDevice$MotionRange;->access$700(Landroid/view/InputDevice$MotionRange;)F

    #@13f
    move-result v5

    #@140
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@143
    .line 760
    const-string v4, "\n"

    #@145
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    .line 752
    add-int/lit8 v1, v1, 0x1

    #@14a
    goto :goto_e3

    #@14b
    .line 721
    .end local v1           #i:I
    .end local v2           #numAxes:I
    .end local v3           #range:Landroid/view/InputDevice$MotionRange;
    :cond_14b
    const-string v4, "built-in"

    #@14d
    goto/16 :goto_50

    #@14f
    .line 726
    :pswitch_14f
    const-string/jumbo v4, "none"

    #@152
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    goto/16 :goto_63

    #@157
    .line 729
    :pswitch_157
    const-string/jumbo v4, "non-alphabetic"

    #@15a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    goto/16 :goto_63

    #@15f
    .line 732
    :pswitch_15f
    const-string v4, "alphabetic"

    #@161
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    goto/16 :goto_63

    #@166
    .line 762
    .restart local v1       #i:I
    .restart local v2       #numAxes:I
    :cond_166
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v4

    #@16a
    return-object v4

    #@16b
    .line 724
    nop

    #@16c
    :pswitch_data_16c
    .packed-switch 0x0
        :pswitch_14f
        :pswitch_157
        :pswitch_15f
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 687
    iget v3, p0, Landroid/view/InputDevice;->mId:I

    #@4
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 688
    iget v3, p0, Landroid/view/InputDevice;->mGeneration:I

    #@9
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 689
    iget-object v3, p0, Landroid/view/InputDevice;->mName:Ljava/lang/String;

    #@e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 690
    iget-object v3, p0, Landroid/view/InputDevice;->mDescriptor:Ljava/lang/String;

    #@13
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 691
    iget-boolean v3, p0, Landroid/view/InputDevice;->mIsExternal:Z

    #@18
    if-eqz v3, :cond_72

    #@1a
    move v3, v4

    #@1b
    :goto_1b
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 692
    iget v3, p0, Landroid/view/InputDevice;->mSources:I

    #@20
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 693
    iget v3, p0, Landroid/view/InputDevice;->mKeyboardType:I

    #@25
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 694
    iget-object v3, p0, Landroid/view/InputDevice;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@2a
    invoke-virtual {v3, p1, p2}, Landroid/view/KeyCharacterMap;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 695
    iget-boolean v3, p0, Landroid/view/InputDevice;->mHasVibrator:Z

    #@2f
    if-eqz v3, :cond_74

    #@31
    :goto_31
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 697
    iget-object v3, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@39
    move-result v1

    #@3a
    .line 698
    .local v1, numRanges:I
    const/4 v0, 0x0

    #@3b
    .local v0, i:I
    :goto_3b
    if-ge v0, v1, :cond_76

    #@3d
    .line 699
    iget-object v3, p0, Landroid/view/InputDevice;->mMotionRanges:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v2

    #@43
    check-cast v2, Landroid/view/InputDevice$MotionRange;

    #@45
    .line 700
    .local v2, range:Landroid/view/InputDevice$MotionRange;
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$100(Landroid/view/InputDevice$MotionRange;)I

    #@48
    move-result v3

    #@49
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    .line 701
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$200(Landroid/view/InputDevice$MotionRange;)I

    #@4f
    move-result v3

    #@50
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    .line 702
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$400(Landroid/view/InputDevice$MotionRange;)F

    #@56
    move-result v3

    #@57
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeFloat(F)V

    #@5a
    .line 703
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$500(Landroid/view/InputDevice$MotionRange;)F

    #@5d
    move-result v3

    #@5e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeFloat(F)V

    #@61
    .line 704
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$600(Landroid/view/InputDevice$MotionRange;)F

    #@64
    move-result v3

    #@65
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeFloat(F)V

    #@68
    .line 705
    invoke-static {v2}, Landroid/view/InputDevice$MotionRange;->access$700(Landroid/view/InputDevice$MotionRange;)F

    #@6b
    move-result v3

    #@6c
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeFloat(F)V

    #@6f
    .line 698
    add-int/lit8 v0, v0, 0x1

    #@71
    goto :goto_3b

    #@72
    .end local v0           #i:I
    .end local v1           #numRanges:I
    .end local v2           #range:Landroid/view/InputDevice$MotionRange;
    :cond_72
    move v3, v5

    #@73
    .line 691
    goto :goto_1b

    #@74
    :cond_74
    move v4, v5

    #@75
    .line 695
    goto :goto_31

    #@76
    .line 707
    .restart local v0       #i:I
    .restart local v1       #numRanges:I
    :cond_76
    const/4 v3, -0x1

    #@77
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    .line 708
    return-void
.end method
