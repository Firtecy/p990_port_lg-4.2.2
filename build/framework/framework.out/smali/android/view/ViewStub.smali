.class public final Landroid/view/ViewStub;
.super Landroid/view/View;
.source "ViewStub.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ViewStub$OnInflateListener;
    }
.end annotation


# instance fields
.field private mInflateListener:Landroid/view/ViewStub$OnInflateListener;

.field private mInflatedId:I

.field private mInflatedViewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayoutResource:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Landroid/view/View;-><init>()V

    #@3
    .line 72
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@6
    .line 81
    invoke-direct {p0, p1}, Landroid/view/ViewStub;->initialize(Landroid/content/Context;)V

    #@9
    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "layoutResource"

    #@0
    .prologue
    .line 90
    invoke-direct {p0}, Landroid/view/View;-><init>()V

    #@3
    .line 72
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@6
    .line 91
    iput p2, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@8
    .line 92
    invoke-direct {p0, p1}, Landroid/view/ViewStub;->initialize(Landroid/content/Context;)V

    #@b
    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 96
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 100
    invoke-direct {p0}, Landroid/view/View;-><init>()V

    #@5
    .line 72
    iput v2, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@7
    .line 101
    sget-object v1, Lcom/android/internal/R$styleable;->ViewStub:[I

    #@9
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    .line 104
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    #@e
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@11
    move-result v1

    #@12
    iput v1, p0, Landroid/view/ViewStub;->mInflatedId:I

    #@14
    .line 105
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@17
    move-result v1

    #@18
    iput v1, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@1a
    .line 107
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1d
    .line 109
    sget-object v1, Lcom/android/internal/R$styleable;->View:[I

    #@1f
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@22
    move-result-object v0

    #@23
    .line 110
    const/16 v1, 0x8

    #@25
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@28
    move-result v1

    #@29
    iput v1, p0, Landroid/view/View;->mID:I

    #@2b
    .line 111
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2e
    .line 113
    invoke-direct {p0, p1}, Landroid/view/ViewStub;->initialize(Landroid/content/Context;)V

    #@31
    .line 114
    return-void
.end method

.method private initialize(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 117
    iput-object p1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    .line 118
    const/16 v0, 0x8

    #@4
    invoke-virtual {p0, v0}, Landroid/view/ViewStub;->setVisibility(I)V

    #@7
    .line 119
    const/4 v0, 0x1

    #@8
    invoke-virtual {p0, v0}, Landroid/view/ViewStub;->setWillNotDraw(Z)V

    #@b
    .line 120
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 210
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 206
    return-void
.end method

.method public getInflatedId()I
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Landroid/view/ViewStub;->mInflatedId:I

    #@2
    return v0
.end method

.method public getLayoutInflater()Landroid/view/LayoutInflater;
    .registers 2

    #@0
    .prologue
    .line 196
    iget-object v0, p0, Landroid/view/ViewStub;->mInflater:Landroid/view/LayoutInflater;

    #@2
    return-object v0
.end method

.method public getLayoutResource()I
    .registers 2

    #@0
    .prologue
    .line 164
    iget v0, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@2
    return v0
.end method

.method public inflate()Landroid/view/View;
    .registers 9

    #@0
    .prologue
    .line 247
    invoke-virtual {p0}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    #@3
    move-result-object v5

    #@4
    .line 249
    .local v5, viewParent:Landroid/view/ViewParent;
    if-eqz v5, :cond_5c

    #@6
    instance-of v6, v5, Landroid/view/ViewGroup;

    #@8
    if-eqz v6, :cond_5c

    #@a
    .line 250
    iget v6, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@c
    if-eqz v6, :cond_54

    #@e
    move-object v3, v5

    #@f
    .line 251
    check-cast v3, Landroid/view/ViewGroup;

    #@11
    .line 253
    .local v3, parent:Landroid/view/ViewGroup;
    iget-object v6, p0, Landroid/view/ViewStub;->mInflater:Landroid/view/LayoutInflater;

    #@13
    if-eqz v6, :cond_49

    #@15
    .line 254
    iget-object v0, p0, Landroid/view/ViewStub;->mInflater:Landroid/view/LayoutInflater;

    #@17
    .line 258
    .local v0, factory:Landroid/view/LayoutInflater;
    :goto_17
    iget v6, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@19
    const/4 v7, 0x0

    #@1a
    invoke-virtual {v0, v6, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@1d
    move-result-object v4

    #@1e
    .line 261
    .local v4, view:Landroid/view/View;
    iget v6, p0, Landroid/view/ViewStub;->mInflatedId:I

    #@20
    const/4 v7, -0x1

    #@21
    if-eq v6, v7, :cond_28

    #@23
    .line 262
    iget v6, p0, Landroid/view/ViewStub;->mInflatedId:I

    #@25
    invoke-virtual {v4, v6}, Landroid/view/View;->setId(I)V

    #@28
    .line 265
    :cond_28
    invoke-virtual {v3, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    #@2b
    move-result v1

    #@2c
    .line 266
    .local v1, index:I
    invoke-virtual {v3, p0}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    #@2f
    .line 268
    invoke-virtual {p0}, Landroid/view/ViewStub;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@32
    move-result-object v2

    #@33
    .line 269
    .local v2, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    if-eqz v2, :cond_50

    #@35
    .line 270
    invoke-virtual {v3, v4, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@38
    .line 275
    :goto_38
    new-instance v6, Ljava/lang/ref/WeakReference;

    #@3a
    invoke-direct {v6, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@3d
    iput-object v6, p0, Landroid/view/ViewStub;->mInflatedViewRef:Ljava/lang/ref/WeakReference;

    #@3f
    .line 277
    iget-object v6, p0, Landroid/view/ViewStub;->mInflateListener:Landroid/view/ViewStub$OnInflateListener;

    #@41
    if-eqz v6, :cond_48

    #@43
    .line 278
    iget-object v6, p0, Landroid/view/ViewStub;->mInflateListener:Landroid/view/ViewStub$OnInflateListener;

    #@45
    invoke-interface {v6, p0, v4}, Landroid/view/ViewStub$OnInflateListener;->onInflate(Landroid/view/ViewStub;Landroid/view/View;)V

    #@48
    .line 281
    :cond_48
    return-object v4

    #@49
    .line 256
    .end local v0           #factory:Landroid/view/LayoutInflater;
    .end local v1           #index:I
    .end local v2           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v4           #view:Landroid/view/View;
    :cond_49
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4b
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@4e
    move-result-object v0

    #@4f
    .restart local v0       #factory:Landroid/view/LayoutInflater;
    goto :goto_17

    #@50
    .line 272
    .restart local v1       #index:I
    .restart local v2       #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .restart local v4       #view:Landroid/view/View;
    :cond_50
    invoke-virtual {v3, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    #@53
    goto :goto_38

    #@54
    .line 283
    .end local v0           #factory:Landroid/view/LayoutInflater;
    .end local v1           #index:I
    .end local v2           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v3           #parent:Landroid/view/ViewGroup;
    .end local v4           #view:Landroid/view/View;
    :cond_54
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@56
    const-string v7, "ViewStub must have a valid layoutResource"

    #@58
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v6

    #@5c
    .line 286
    :cond_5c
    new-instance v6, Ljava/lang/IllegalStateException;

    #@5e
    const-string v7, "ViewStub must have a non-null ViewGroup viewParent"

    #@60
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@63
    throw v6
.end method

.method protected onMeasure(II)V
    .registers 4
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 201
    invoke-virtual {p0, v0, v0}, Landroid/view/ViewStub;->setMeasuredDimension(II)V

    #@4
    .line 202
    return-void
.end method

.method public setInflatedId(I)V
    .registers 2
    .parameter "inflatedId"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 148
    iput p1, p0, Landroid/view/ViewStub;->mInflatedId:I

    #@2
    .line 149
    return-void
.end method

.method public setLayoutInflater(Landroid/view/LayoutInflater;)V
    .registers 2
    .parameter "inflater"

    #@0
    .prologue
    .line 189
    iput-object p1, p0, Landroid/view/ViewStub;->mInflater:Landroid/view/LayoutInflater;

    #@2
    .line 190
    return-void
.end method

.method public setLayoutResource(I)V
    .registers 2
    .parameter "layoutResource"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 181
    iput p1, p0, Landroid/view/ViewStub;->mLayoutResource:I

    #@2
    .line 182
    return-void
.end method

.method public setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V
    .registers 2
    .parameter "inflateListener"

    #@0
    .prologue
    .line 299
    iput-object p1, p0, Landroid/view/ViewStub;->mInflateListener:Landroid/view/ViewStub$OnInflateListener;

    #@2
    .line 300
    return-void
.end method

.method public setVisibility(I)V
    .registers 5
    .parameter "visibility"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 224
    iget-object v1, p0, Landroid/view/ViewStub;->mInflatedViewRef:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v1, :cond_1b

    #@4
    .line 225
    iget-object v1, p0, Landroid/view/ViewStub;->mInflatedViewRef:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/view/View;

    #@c
    .line 226
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_12

    #@e
    .line 227
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    #@11
    .line 237
    .end local v0           #view:Landroid/view/View;
    :cond_11
    :goto_11
    return-void

    #@12
    .line 229
    .restart local v0       #view:Landroid/view/View;
    :cond_12
    new-instance v1, Ljava/lang/IllegalStateException;

    #@14
    const-string/jumbo v2, "setVisibility called on un-referenced view"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 232
    .end local v0           #view:Landroid/view/View;
    :cond_1b
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    #@1e
    .line 233
    if-eqz p1, :cond_23

    #@20
    const/4 v1, 0x4

    #@21
    if-ne p1, v1, :cond_11

    #@23
    .line 234
    :cond_23
    invoke-virtual {p0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    #@26
    goto :goto_11
.end method
