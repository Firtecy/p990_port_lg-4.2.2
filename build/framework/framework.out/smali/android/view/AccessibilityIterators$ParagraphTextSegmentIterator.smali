.class Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;
.super Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;
.source "AccessibilityIterators.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/AccessibilityIterators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ParagraphTextSegmentIterator"
.end annotation


# static fields
.field private static sInstance:Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 257
    invoke-direct {p0}, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;
    .registers 1

    #@0
    .prologue
    .line 261
    sget-object v0, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 262
    new-instance v0, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;

    #@6
    invoke-direct {v0}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;-><init>()V

    #@9
    sput-object v0, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;

    #@b
    .line 264
    :cond_b
    sget-object v0, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;

    #@d
    return-object v0
.end method

.method private isEndBoundary(I)Z
    .registers 5
    .parameter "index"

    #@0
    .prologue
    const/16 v2, 0xa

    #@2
    .line 326
    if-lez p1, :cond_20

    #@4
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@6
    add-int/lit8 v1, p1, -0x1

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@b
    move-result v0

    #@c
    if-eq v0, v2, :cond_20

    #@e
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@10
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@13
    move-result v0

    #@14
    if-eq p1, v0, :cond_1e

    #@16
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@18
    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    #@1b
    move-result v0

    #@1c
    if-ne v0, v2, :cond_20

    #@1e
    :cond_1e
    const/4 v0, 0x1

    #@1f
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method

.method private isStartBoundary(I)Z
    .registers 5
    .parameter "index"

    #@0
    .prologue
    const/16 v2, 0xa

    #@2
    .line 321
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    #@7
    move-result v0

    #@8
    if-eq v0, v2, :cond_18

    #@a
    if-eqz p1, :cond_16

    #@c
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@e
    add-int/lit8 v1, p1, -0x1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    if-ne v0, v2, :cond_18

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method


# virtual methods
.method public following(I)[I
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 269
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@6
    move-result v2

    #@7
    .line 270
    .local v2, textLength:I
    if-gtz v2, :cond_a

    #@9
    .line 291
    :cond_9
    :goto_9
    return-object v3

    #@a
    .line 273
    :cond_a
    if-ge p1, v2, :cond_9

    #@c
    .line 276
    move v1, p1

    #@d
    .line 277
    .local v1, start:I
    if-gez v1, :cond_10

    #@f
    .line 278
    const/4 v1, 0x0

    #@10
    .line 281
    :cond_10
    :goto_10
    if-ge v1, v2, :cond_25

    #@12
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@14
    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v4

    #@18
    const/16 v5, 0xa

    #@1a
    if-ne v4, v5, :cond_25

    #@1c
    invoke-direct {p0, v1}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->isStartBoundary(I)Z

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_25

    #@22
    .line 282
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_10

    #@25
    .line 284
    :cond_25
    if-ge v1, v2, :cond_9

    #@27
    .line 287
    add-int/lit8 v0, v1, 0x1

    #@29
    .line 288
    .local v0, end:I
    :goto_29
    if-ge v0, v2, :cond_34

    #@2b
    invoke-direct {p0, v0}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->isEndBoundary(I)Z

    #@2e
    move-result v3

    #@2f
    if-nez v3, :cond_34

    #@31
    .line 289
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_29

    #@34
    .line 291
    :cond_34
    invoke-virtual {p0, v1, v0}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->getRange(II)[I

    #@37
    move-result-object v3

    #@38
    goto :goto_9
.end method

.method public preceding(I)[I
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 296
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@6
    move-result v2

    #@7
    .line 297
    .local v2, textLength:I
    if-gtz v2, :cond_a

    #@9
    .line 317
    :cond_9
    :goto_9
    return-object v3

    #@a
    .line 300
    :cond_a
    if-lez p1, :cond_9

    #@c
    .line 303
    move v0, p1

    #@d
    .line 304
    .local v0, end:I
    if-le v0, v2, :cond_10

    #@f
    .line 305
    move v0, v2

    #@10
    .line 307
    :cond_10
    :goto_10
    if-lez v0, :cond_27

    #@12
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@14
    add-int/lit8 v5, v0, -0x1

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    #@19
    move-result v4

    #@1a
    const/16 v5, 0xa

    #@1c
    if-ne v4, v5, :cond_27

    #@1e
    invoke-direct {p0, v0}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->isEndBoundary(I)Z

    #@21
    move-result v4

    #@22
    if-nez v4, :cond_27

    #@24
    .line 308
    add-int/lit8 v0, v0, -0x1

    #@26
    goto :goto_10

    #@27
    .line 310
    :cond_27
    if-lez v0, :cond_9

    #@29
    .line 313
    add-int/lit8 v1, v0, -0x1

    #@2b
    .line 314
    .local v1, start:I
    :goto_2b
    if-lez v1, :cond_36

    #@2d
    invoke-direct {p0, v1}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->isStartBoundary(I)Z

    #@30
    move-result v3

    #@31
    if-nez v3, :cond_36

    #@33
    .line 315
    add-int/lit8 v1, v1, -0x1

    #@35
    goto :goto_2b

    #@36
    .line 317
    :cond_36
    invoke-virtual {p0, v1, v0}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->getRange(II)[I

    #@39
    move-result-object v3

    #@3a
    goto :goto_9
.end method
