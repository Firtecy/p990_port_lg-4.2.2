.class Landroid/view/ViewGroup$ChildListForAccessibility;
.super Ljava/lang/Object;
.source "ViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ChildListForAccessibility"
.end annotation


# static fields
.field private static final MAX_POOL_SIZE:I = 0x20

.field private static sPool:Landroid/view/ViewGroup$ChildListForAccessibility;

.field private static final sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field private final mChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mHolders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/ViewGroup$ViewLocationHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mIsPooled:Z

.field private mNext:Landroid/view/ViewGroup$ChildListForAccessibility;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 6170
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 6166
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 6180
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mChildren:Ljava/util/ArrayList;

    #@a
    .line 6182
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mHolders:Ljava/util/ArrayList;

    #@11
    return-void
.end method

.method private clear()V
    .registers 2

    #@0
    .prologue
    .line 6253
    iget-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mChildren:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 6254
    return-void
.end method

.method private init(Landroid/view/ViewGroup;Z)V
    .registers 10
    .parameter "parent"
    .parameter "sort"

    #@0
    .prologue
    .line 6229
    iget-object v2, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mChildren:Ljava/util/ArrayList;

    #@2
    .line 6230
    .local v2, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    #@5
    move-result v1

    #@6
    .line 6231
    .local v1, childCount:I
    const/4 v5, 0x0

    #@7
    .local v5, i:I
    :goto_7
    if-ge v5, v1, :cond_13

    #@9
    .line 6232
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    .line 6233
    .local v0, child:Landroid/view/View;
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 6231
    add-int/lit8 v5, v5, 0x1

    #@12
    goto :goto_7

    #@13
    .line 6235
    .end local v0           #child:Landroid/view/View;
    :cond_13
    if-eqz p2, :cond_44

    #@15
    .line 6236
    iget-object v4, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mHolders:Ljava/util/ArrayList;

    #@17
    .line 6237
    .local v4, holders:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewGroup$ViewLocationHolder;>;"
    const/4 v5, 0x0

    #@18
    :goto_18
    if-ge v5, v1, :cond_2a

    #@1a
    .line 6238
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/view/View;

    #@20
    .line 6239
    .restart local v0       #child:Landroid/view/View;
    invoke-static {p1, v0}, Landroid/view/ViewGroup$ViewLocationHolder;->obtain(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/view/ViewGroup$ViewLocationHolder;

    #@23
    move-result-object v3

    #@24
    .line 6240
    .local v3, holder:Landroid/view/ViewGroup$ViewLocationHolder;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 6237
    add-int/lit8 v5, v5, 0x1

    #@29
    goto :goto_18

    #@2a
    .line 6242
    .end local v0           #child:Landroid/view/View;
    .end local v3           #holder:Landroid/view/ViewGroup$ViewLocationHolder;
    :cond_2a
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@2d
    .line 6243
    const/4 v5, 0x0

    #@2e
    :goto_2e
    if-ge v5, v1, :cond_41

    #@30
    .line 6244
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v3

    #@34
    check-cast v3, Landroid/view/ViewGroup$ViewLocationHolder;

    #@36
    .line 6245
    .restart local v3       #holder:Landroid/view/ViewGroup$ViewLocationHolder;
    iget-object v6, v3, Landroid/view/ViewGroup$ViewLocationHolder;->mView:Landroid/view/View;

    #@38
    invoke-virtual {v2, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 6246
    invoke-virtual {v3}, Landroid/view/ViewGroup$ViewLocationHolder;->recycle()V

    #@3e
    .line 6243
    add-int/lit8 v5, v5, 0x1

    #@40
    goto :goto_2e

    #@41
    .line 6248
    .end local v3           #holder:Landroid/view/ViewGroup$ViewLocationHolder;
    :cond_41
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@44
    .line 6250
    .end local v4           #holders:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewGroup$ViewLocationHolder;>;"
    :cond_44
    return-void
.end method

.method public static obtain(Landroid/view/ViewGroup;Z)Landroid/view/ViewGroup$ChildListForAccessibility;
    .registers 6
    .parameter "parent"
    .parameter "sort"

    #@0
    .prologue
    .line 6185
    const/4 v0, 0x0

    #@1
    .line 6186
    .local v0, list:Landroid/view/ViewGroup$ChildListForAccessibility;
    sget-object v3, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 6187
    :try_start_4
    sget-object v2, Landroid/view/ViewGroup$ChildListForAccessibility;->sPool:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@6
    if-eqz v2, :cond_1f

    #@8
    .line 6188
    sget-object v0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPool:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@a
    .line 6189
    iget-object v2, v0, Landroid/view/ViewGroup$ChildListForAccessibility;->mNext:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@c
    sput-object v2, Landroid/view/ViewGroup$ChildListForAccessibility;->sPool:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@e
    .line 6190
    const/4 v2, 0x0

    #@f
    iput-object v2, v0, Landroid/view/ViewGroup$ChildListForAccessibility;->mNext:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@11
    .line 6191
    const/4 v2, 0x0

    #@12
    iput-boolean v2, v0, Landroid/view/ViewGroup$ChildListForAccessibility;->mIsPooled:Z

    #@14
    .line 6192
    sget v2, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolSize:I

    #@16
    add-int/lit8 v2, v2, -0x1

    #@18
    sput v2, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolSize:I

    #@1a
    .line 6196
    :goto_1a
    invoke-direct {v0, p0, p1}, Landroid/view/ViewGroup$ChildListForAccessibility;->init(Landroid/view/ViewGroup;Z)V

    #@1d
    .line 6197
    monitor-exit v3

    #@1e
    return-object v0

    #@1f
    .line 6194
    :cond_1f
    new-instance v1, Landroid/view/ViewGroup$ChildListForAccessibility;

    #@21
    invoke-direct {v1}, Landroid/view/ViewGroup$ChildListForAccessibility;-><init>()V

    #@24
    .end local v0           #list:Landroid/view/ViewGroup$ChildListForAccessibility;
    .local v1, list:Landroid/view/ViewGroup$ChildListForAccessibility;
    move-object v0, v1

    #@25
    .end local v1           #list:Landroid/view/ViewGroup$ChildListForAccessibility;
    .restart local v0       #list:Landroid/view/ViewGroup$ChildListForAccessibility;
    goto :goto_1a

    #@26
    .line 6198
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_4 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method


# virtual methods
.method public getChildAt(I)Landroid/view/View;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 6221
    iget-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mChildren:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/View;

    #@8
    return-object v0
.end method

.method public getChildCount()I
    .registers 2

    #@0
    .prologue
    .line 6217
    iget-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mChildren:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getChildIndex(Landroid/view/View;)I
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 6225
    iget-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mChildren:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 6202
    iget-boolean v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mIsPooled:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 6203
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Instance already recycled."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 6205
    :cond_c
    invoke-direct {p0}, Landroid/view/ViewGroup$ChildListForAccessibility;->clear()V

    #@f
    .line 6206
    sget-object v1, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 6207
    :try_start_12
    sget v0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolSize:I

    #@14
    const/16 v2, 0x20

    #@16
    if-ge v0, v2, :cond_27

    #@18
    .line 6208
    sget-object v0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPool:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@1a
    iput-object v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mNext:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@1c
    .line 6209
    const/4 v0, 0x1

    #@1d
    iput-boolean v0, p0, Landroid/view/ViewGroup$ChildListForAccessibility;->mIsPooled:Z

    #@1f
    .line 6210
    sput-object p0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPool:Landroid/view/ViewGroup$ChildListForAccessibility;

    #@21
    .line 6211
    sget v0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Landroid/view/ViewGroup$ChildListForAccessibility;->sPoolSize:I

    #@27
    .line 6213
    :cond_27
    monitor-exit v1

    #@28
    .line 6214
    return-void

    #@29
    .line 6213
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method
