.class Landroid/view/IWindowSession$Stub$Proxy;
.super Ljava/lang/Object;
.source "IWindowSession.java"

# interfaces
.implements Landroid/view/IWindowSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IWindowSession$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 621
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 622
    iput-object p1, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 623
    return-void
.end method


# virtual methods
.method public add(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;Landroid/view/InputChannel;)I
    .registers 13
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "outContentInsets"
    .parameter "outInputChannel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 634
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 635
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 638
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 639
    if-eqz p1, :cond_4d

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 640
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 641
    if-eqz p3, :cond_4f

    #@1b
    .line 642
    const/4 v3, 0x1

    #@1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 643
    const/4 v3, 0x0

    #@20
    invoke-virtual {p3, v0, v3}, Landroid/view/WindowManager$LayoutParams;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 648
    :goto_23
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 649
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/4 v4, 0x1

    #@29
    const/4 v5, 0x0

    #@2a
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 650
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@30
    .line 651
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v2

    #@34
    .line 652
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v3

    #@38
    if-eqz v3, :cond_3d

    #@3a
    .line 653
    invoke-virtual {p5, v1}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V

    #@3d
    .line 655
    :cond_3d
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v3

    #@41
    if-eqz v3, :cond_46

    #@43
    .line 656
    invoke-virtual {p6, v1}, Landroid/view/InputChannel;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_46
    .catchall {:try_start_8 .. :try_end_46} :catchall_54

    #@46
    .line 660
    :cond_46
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 661
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 663
    return v2

    #@4d
    .line 639
    .end local v2           #_result:I
    :cond_4d
    const/4 v3, 0x0

    #@4e
    goto :goto_13

    #@4f
    .line 646
    :cond_4f
    const/4 v3, 0x0

    #@50
    :try_start_50
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_53
    .catchall {:try_start_50 .. :try_end_53} :catchall_54

    #@53
    goto :goto_23

    #@54
    .line 660
    :catchall_54
    move-exception v3

    #@55
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@58
    .line 661
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@5b
    throw v3
.end method

.method public addToDisplay(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I
    .registers 14
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "layerStackId"
    .parameter "outContentInsets"
    .parameter "outInputChannel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 667
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 668
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 671
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 672
    if-eqz p1, :cond_50

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 673
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 674
    if-eqz p3, :cond_52

    #@1b
    .line 675
    const/4 v3, 0x1

    #@1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 676
    const/4 v3, 0x0

    #@20
    invoke-virtual {p3, v0, v3}, Landroid/view/WindowManager$LayoutParams;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 681
    :goto_23
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 682
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 683
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2b
    const/4 v4, 0x2

    #@2c
    const/4 v5, 0x0

    #@2d
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@30
    .line 684
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@33
    .line 685
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v2

    #@37
    .line 686
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_40

    #@3d
    .line 687
    invoke-virtual {p6, v1}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V

    #@40
    .line 689
    :cond_40
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_49

    #@46
    .line 690
    invoke-virtual {p7, v1}, Landroid/view/InputChannel;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_49
    .catchall {:try_start_8 .. :try_end_49} :catchall_57

    #@49
    .line 694
    :cond_49
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 695
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4f
    .line 697
    return v2

    #@50
    .line 672
    .end local v2           #_result:I
    :cond_50
    const/4 v3, 0x0

    #@51
    goto :goto_13

    #@52
    .line 679
    :cond_52
    const/4 v3, 0x0

    #@53
    :try_start_53
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_56
    .catchall {:try_start_53 .. :try_end_56} :catchall_57

    #@56
    goto :goto_23

    #@57
    .line 694
    :catchall_57
    move-exception v3

    #@58
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@5b
    .line 695
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@5e
    throw v3
.end method

.method public addToDisplayWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;)I
    .registers 13
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "layerStackId"
    .parameter "outContentInsets"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 731
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 732
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 735
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 736
    if-eqz p1, :cond_47

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 737
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 738
    if-eqz p3, :cond_49

    #@1b
    .line 739
    const/4 v3, 0x1

    #@1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 740
    const/4 v3, 0x0

    #@20
    invoke-virtual {p3, v0, v3}, Landroid/view/WindowManager$LayoutParams;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 745
    :goto_23
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 746
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 747
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2b
    const/4 v4, 0x4

    #@2c
    const/4 v5, 0x0

    #@2d
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@30
    .line 748
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@33
    .line 749
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v2

    #@37
    .line 750
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_40

    #@3d
    .line 751
    invoke-virtual {p6, v1}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_40
    .catchall {:try_start_8 .. :try_end_40} :catchall_4e

    #@40
    .line 755
    :cond_40
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@43
    .line 756
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 758
    return v2

    #@47
    .line 736
    .end local v2           #_result:I
    :cond_47
    const/4 v3, 0x0

    #@48
    goto :goto_13

    #@49
    .line 743
    :cond_49
    const/4 v3, 0x0

    #@4a
    :try_start_4a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_4e

    #@4d
    goto :goto_23

    #@4e
    .line 755
    :catchall_4e
    move-exception v3

    #@4f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@52
    .line 756
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@55
    throw v3
.end method

.method public addWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;)I
    .registers 12
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "viewVisibility"
    .parameter "outContentInsets"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 701
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 702
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 705
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 706
    if-eqz p1, :cond_44

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 707
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 708
    if-eqz p3, :cond_46

    #@1b
    .line 709
    const/4 v3, 0x1

    #@1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 710
    const/4 v3, 0x0

    #@20
    invoke-virtual {p3, v0, v3}, Landroid/view/WindowManager$LayoutParams;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 715
    :goto_23
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 716
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/4 v4, 0x3

    #@29
    const/4 v5, 0x0

    #@2a
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 717
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@30
    .line 718
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v2

    #@34
    .line 719
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v3

    #@38
    if-eqz v3, :cond_3d

    #@3a
    .line 720
    invoke-virtual {p5, v1}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_3d
    .catchall {:try_start_8 .. :try_end_3d} :catchall_4b

    #@3d
    .line 724
    :cond_3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 725
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    .line 727
    return v2

    #@44
    .line 706
    .end local v2           #_result:I
    :cond_44
    const/4 v3, 0x0

    #@45
    goto :goto_13

    #@46
    .line 713
    :cond_46
    const/4 v3, 0x0

    #@47
    :try_start_47
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_4b

    #@4a
    goto :goto_23

    #@4b
    .line 724
    :catchall_4b
    move-exception v3

    #@4c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4f
    .line 725
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@52
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 626
    iget-object v0, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public dragRecipientEntered(Landroid/view/IWindow;)V
    .registers 7
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1147
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1148
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1150
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1151
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 1152
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x13

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 1153
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 1156
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 1157
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1159
    return-void

    #@28
    .line 1151
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 1156
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 1157
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public dragRecipientExited(Landroid/view/IWindow;)V
    .registers 7
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1165
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1166
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1168
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1169
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 1170
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x14

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 1171
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 1174
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 1175
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1177
    return-void

    #@28
    .line 1169
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 1174
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 1175
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public finishDrawing(Landroid/view/IWindow;)V
    .registers 7
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 994
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 995
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 997
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 998
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 999
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0xc

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 1000
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 1003
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 1004
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 1006
    return-void

    #@28
    .line 998
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 1003
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 1004
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public getDisplayFrame(Landroid/view/IWindow;Landroid/graphics/Rect;)V
    .registers 8
    .parameter "window"
    .parameter "outDisplayFrame"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 976
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 977
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 979
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 980
    if-eqz p1, :cond_31

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 981
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0xb

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 982
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 983
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_2a

    #@27
    .line 984
    invoke-virtual {p2, v1}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_2a
    .catchall {:try_start_8 .. :try_end_2a} :catchall_33

    #@2a
    .line 988
    :cond_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 989
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 991
    return-void

    #@31
    .line 980
    :cond_31
    const/4 v2, 0x0

    #@32
    goto :goto_13

    #@33
    .line 988
    :catchall_33
    move-exception v2

    #@34
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 989
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    throw v2
.end method

.method public getInTouchMode()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1024
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1025
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1028
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.view.IWindowSession"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1029
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0xe

    #@12
    const/4 v5, 0x0

    #@13
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 1030
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 1031
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_9 .. :try_end_1c} :catchall_27

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_20

    #@1f
    const/4 v2, 0x1

    #@20
    .line 1034
    .local v2, _result:Z
    :cond_20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 1035
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1037
    return v2

    #@27
    .line 1034
    .end local v2           #_result:Z
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1035
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 630
    const-string v0, "android.view.IWindowSession"

    #@2
    return-object v0
.end method

.method public isWindowSplit(Landroid/view/IWindow;Landroid/graphics/Rect;)Z
    .registers 9
    .parameter "window"
    .parameter "outSplitWindowFrame"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1327
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1328
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1331
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.view.IWindowSession"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1332
    if-eqz p1, :cond_39

    #@10
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 1333
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x1b

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 1334
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 1335
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_29

    #@28
    const/4 v2, 0x1

    #@29
    .line 1336
    .local v2, _result:Z
    :cond_29
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_32

    #@2f
    .line 1337
    invoke-virtual {p2, v1}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_32
    .catchall {:try_start_9 .. :try_end_32} :catchall_3b

    #@32
    .line 1341
    :cond_32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 1342
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 1344
    return v2

    #@39
    .line 1332
    .end local v2           #_result:Z
    :cond_39
    const/4 v3, 0x0

    #@3a
    goto :goto_14

    #@3b
    .line 1341
    :catchall_3b
    move-exception v3

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 1342
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v3
.end method

.method public onRectangleOnScreenRequested(Landroid/os/IBinder;Landroid/graphics/Rect;Z)V
    .registers 9
    .parameter "token"
    .parameter "rectangle"
    .parameter "immediate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1302
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 1303
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 1305
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.view.IWindowSession"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 1306
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 1307
    if-eqz p2, :cond_33

    #@14
    .line 1308
    const/4 v4, 0x1

    #@15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 1309
    const/4 v4, 0x0

    #@19
    invoke-virtual {p2, v0, v4}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 1314
    :goto_1c
    if-eqz p3, :cond_40

    #@1e
    :goto_1e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 1315
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v3, 0x1a

    #@25
    const/4 v4, 0x0

    #@26
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 1316
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2c
    .catchall {:try_start_a .. :try_end_2c} :catchall_38

    #@2c
    .line 1319
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 1320
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1322
    return-void

    #@33
    .line 1312
    :cond_33
    const/4 v4, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_1c

    #@38
    .line 1319
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 1320
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v2

    #@40
    :cond_40
    move v2, v3

    #@41
    .line 1314
    goto :goto_1e
.end method

.method public outOfMemory(Landroid/view/IWindow;)Z
    .registers 8
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 880
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 881
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 884
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.view.IWindowSession"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 885
    if-eqz p1, :cond_30

    #@10
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 886
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x8

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 887
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 888
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_9 .. :try_end_25} :catchall_32

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_29

    #@28
    const/4 v2, 0x1

    #@29
    .line 891
    .local v2, _result:Z
    :cond_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 892
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 894
    return v2

    #@30
    .line 885
    .end local v2           #_result:Z
    :cond_30
    const/4 v3, 0x0

    #@31
    goto :goto_14

    #@32
    .line 891
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 892
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    throw v3
.end method

.method public performDeferredDestroy(Landroid/view/IWindow;)V
    .registers 7
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 862
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 863
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 865
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 866
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 867
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x7

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 868
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 871
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 872
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 874
    return-void

    #@27
    .line 866
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 871
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 872
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public performDrag(Landroid/view/IWindow;Landroid/os/IBinder;FFFFLandroid/content/ClipData;)Z
    .registers 15
    .parameter "window"
    .parameter "dragToken"
    .parameter "touchX"
    .parameter "touchY"
    .parameter "thumbCenterX"
    .parameter "thumbCenterY"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1093
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 1094
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 1097
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.view.IWindowSession"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 1098
    if-eqz p1, :cond_49

    #@11
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 1099
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1b
    .line 1100
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeFloat(F)V

    #@1e
    .line 1101
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeFloat(F)V

    #@21
    .line 1102
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeFloat(F)V

    #@24
    .line 1103
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeFloat(F)V

    #@27
    .line 1104
    if-eqz p7, :cond_4b

    #@29
    .line 1105
    const/4 v4, 0x1

    #@2a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 1106
    const/4 v4, 0x0

    #@2e
    invoke-virtual {p7, v0, v4}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@31
    .line 1111
    :goto_31
    iget-object v4, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@33
    const/16 v5, 0x11

    #@35
    const/4 v6, 0x0

    #@36
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@39
    .line 1112
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@3c
    .line 1113
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_3f
    .catchall {:try_start_a .. :try_end_3f} :catchall_50

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_58

    #@42
    .line 1116
    .local v2, _result:Z
    :goto_42
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 1117
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 1119
    return v2

    #@49
    .line 1098
    .end local v2           #_result:Z
    :cond_49
    const/4 v4, 0x0

    #@4a
    goto :goto_15

    #@4b
    .line 1109
    :cond_4b
    const/4 v4, 0x0

    #@4c
    :try_start_4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_50

    #@4f
    goto :goto_31

    #@50
    .line 1116
    :catchall_50
    move-exception v3

    #@51
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@54
    .line 1117
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@57
    throw v3

    #@58
    :cond_58
    move v2, v3

    #@59
    .line 1113
    goto :goto_42
.end method

.method public performHapticFeedback(Landroid/view/IWindow;IZ)Z
    .registers 11
    .parameter "window"
    .parameter "effectId"
    .parameter "always"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1041
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 1042
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 1045
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "android.view.IWindowSession"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 1046
    if-eqz p1, :cond_39

    #@11
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 1047
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 1048
    if-eqz p3, :cond_3b

    #@1d
    move v4, v2

    #@1e
    :goto_1e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 1049
    iget-object v4, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v5, 0xf

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 1050
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2c
    .line 1051
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2f
    .catchall {:try_start_a .. :try_end_2f} :catchall_3f

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_3d

    #@32
    .line 1054
    .local v2, _result:Z
    :goto_32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 1055
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 1057
    return v2

    #@39
    .line 1046
    .end local v2           #_result:Z
    :cond_39
    const/4 v4, 0x0

    #@3a
    goto :goto_15

    #@3b
    :cond_3b
    move v4, v3

    #@3c
    .line 1048
    goto :goto_1e

    #@3d
    :cond_3d
    move v2, v3

    #@3e
    .line 1051
    goto :goto_32

    #@3f
    .line 1054
    :catchall_3f
    move-exception v3

    #@40
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@43
    .line 1055
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@46
    throw v3
.end method

.method public prepareDrag(Landroid/view/IWindow;IIILandroid/view/Surface;)Landroid/os/IBinder;
    .registers 12
    .parameter "window"
    .parameter "flags"
    .parameter "thumbnailWidth"
    .parameter "thumbnailHeight"
    .parameter "outSurface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1066
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1067
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1070
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1071
    if-eqz p1, :cond_3e

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v3

    #@13
    :goto_13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 1072
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 1073
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1074
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 1075
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/16 v4, 0x10

    #@23
    const/4 v5, 0x0

    #@24
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 1076
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 1077
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2d
    move-result-object v2

    #@2e
    .line 1078
    .local v2, _result:Landroid/os/IBinder;
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_37

    #@34
    .line 1079
    invoke-virtual {p5, v1}, Landroid/view/Surface;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_40

    #@37
    .line 1083
    :cond_37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 1084
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 1086
    return-object v2

    #@3e
    .line 1071
    .end local v2           #_result:Landroid/os/IBinder;
    :cond_3e
    const/4 v3, 0x0

    #@3f
    goto :goto_13

    #@40
    .line 1083
    :catchall_40
    move-exception v3

    #@41
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@44
    .line 1084
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@47
    throw v3
.end method

.method public relayout(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    .registers 20
    .parameter "window"
    .parameter "seq"
    .parameter "attrs"
    .parameter "requestedWidth"
    .parameter "requestedHeight"
    .parameter "viewVisibility"
    .parameter "flags"
    .parameter "outFrame"
    .parameter "outContentInsets"
    .parameter "outVisibleInsets"
    .parameter "outConfig"
    .parameter "outSurface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 813
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 814
    .local v1, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 817
    .local v2, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.view.IWindowSession"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 818
    if-eqz p1, :cond_79

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 819
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 820
    if-eqz p3, :cond_7b

    #@1b
    .line 821
    const/4 v4, 0x1

    #@1c
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 822
    const/4 v4, 0x0

    #@20
    invoke-virtual {p3, v1, v4}, Landroid/view/WindowManager$LayoutParams;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 827
    :goto_23
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 828
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 829
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 830
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 831
    iget-object v4, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@31
    const/4 v5, 0x6

    #@32
    const/4 v6, 0x0

    #@33
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@36
    .line 832
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@39
    .line 833
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v3

    #@3d
    .line 834
    .local v3, _result:I
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v4

    #@41
    if-eqz v4, :cond_46

    #@43
    .line 835
    invoke-virtual {p8, v2}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V

    #@46
    .line 837
    :cond_46
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v4

    #@4a
    if-eqz v4, :cond_51

    #@4c
    .line 838
    move-object/from16 v0, p9

    #@4e
    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V

    #@51
    .line 840
    :cond_51
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v4

    #@55
    if-eqz v4, :cond_5c

    #@57
    .line 841
    move-object/from16 v0, p10

    #@59
    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->readFromParcel(Landroid/os/Parcel;)V

    #@5c
    .line 843
    :cond_5c
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_67

    #@62
    .line 844
    move-object/from16 v0, p11

    #@64
    invoke-virtual {v0, v2}, Landroid/content/res/Configuration;->readFromParcel(Landroid/os/Parcel;)V

    #@67
    .line 846
    :cond_67
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@6a
    move-result v4

    #@6b
    if-eqz v4, :cond_72

    #@6d
    .line 847
    move-object/from16 v0, p12

    #@6f
    invoke-virtual {v0, v2}, Landroid/view/Surface;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_72
    .catchall {:try_start_8 .. :try_end_72} :catchall_80

    #@72
    .line 851
    :cond_72
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@75
    .line 852
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@78
    .line 854
    return v3

    #@79
    .line 818
    .end local v3           #_result:I
    :cond_79
    const/4 v4, 0x0

    #@7a
    goto :goto_13

    #@7b
    .line 825
    :cond_7b
    const/4 v4, 0x0

    #@7c
    :try_start_7c
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_7f
    .catchall {:try_start_7c .. :try_end_7f} :catchall_80

    #@7f
    goto :goto_23

    #@80
    .line 851
    :catchall_80
    move-exception v4

    #@81
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@84
    .line 852
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@87
    throw v4
.end method

.method public remove(Landroid/view/IWindow;)V
    .registers 7
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 762
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 763
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 765
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 766
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 767
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x5

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 768
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 771
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 772
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 774
    return-void

    #@27
    .line 766
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 771
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 772
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public reportDropResult(Landroid/view/IWindow;Z)V
    .registers 8
    .parameter "window"
    .parameter "consumed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1128
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1129
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1131
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.view.IWindowSession"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1132
    if-eqz p1, :cond_2f

    #@10
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 1133
    if-eqz p2, :cond_1a

    #@19
    const/4 v2, 0x1

    #@1a
    :cond_1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 1134
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x12

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 1135
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_28
    .catchall {:try_start_9 .. :try_end_28} :catchall_31

    #@28
    .line 1138
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1139
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 1141
    return-void

    #@2f
    .line 1132
    :cond_2f
    const/4 v3, 0x0

    #@30
    goto :goto_14

    #@31
    .line 1138
    :catchall_31
    move-exception v2

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 1139
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v2
.end method

.method public sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .registers 14
    .parameter "window"
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"
    .parameter "sync"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1220
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 1221
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 1224
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v5, "android.view.IWindowSession"

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 1225
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 1226
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 1227
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 1228
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 1229
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 1230
    if-eqz p6, :cond_4d

    #@20
    .line 1231
    const/4 v5, 0x1

    #@21
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 1232
    const/4 v5, 0x0

    #@25
    invoke-virtual {p6, v0, v5}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    .line 1237
    :goto_28
    if-eqz p7, :cond_5a

    #@2a
    :goto_2a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 1238
    iget-object v3, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2f
    const/16 v4, 0x17

    #@31
    const/4 v5, 0x0

    #@32
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@35
    .line 1239
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@38
    .line 1240
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_5c

    #@3e
    .line 1241
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43
    move-result-object v2

    #@44
    check-cast v2, Landroid/os/Bundle;
    :try_end_46
    .catchall {:try_start_a .. :try_end_46} :catchall_52

    #@46
    .line 1248
    .local v2, _result:Landroid/os/Bundle;
    :goto_46
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 1249
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 1251
    return-object v2

    #@4d
    .line 1235
    .end local v2           #_result:Landroid/os/Bundle;
    :cond_4d
    const/4 v5, 0x0

    #@4e
    :try_start_4e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_51
    .catchall {:try_start_4e .. :try_end_51} :catchall_52

    #@51
    goto :goto_28

    #@52
    .line 1248
    :catchall_52
    move-exception v3

    #@53
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@56
    .line 1249
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@59
    throw v3

    #@5a
    :cond_5a
    move v3, v4

    #@5b
    .line 1237
    goto :goto_2a

    #@5c
    .line 1244
    :cond_5c
    const/4 v2, 0x0

    #@5d
    .restart local v2       #_result:Landroid/os/Bundle;
    goto :goto_46
.end method

.method public setInTouchMode(Z)V
    .registers 7
    .parameter "showFocus"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1009
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 1010
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 1012
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.view.IWindowSession"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 1013
    if-eqz p1, :cond_11

    #@10
    const/4 v2, 0x1

    #@11
    :cond_11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1014
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v3, 0xd

    #@18
    const/4 v4, 0x0

    #@19
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 1015
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_26

    #@1f
    .line 1018
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1019
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1021
    return-void

    #@26
    .line 1018
    :catchall_26
    move-exception v2

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1019
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v2
.end method

.method public setInsets(Landroid/view/IWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V
    .registers 11
    .parameter "window"
    .parameter "touchableInsets"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "touchableRegion"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 935
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 936
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 938
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 939
    if-eqz p1, :cond_49

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 940
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 941
    if-eqz p3, :cond_4b

    #@1b
    .line 942
    const/4 v2, 0x1

    #@1c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 943
    const/4 v2, 0x0

    #@20
    invoke-virtual {p3, v0, v2}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 948
    :goto_23
    if-eqz p4, :cond_58

    #@25
    .line 949
    const/4 v2, 0x1

    #@26
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 950
    const/4 v2, 0x0

    #@2a
    invoke-virtual {p4, v0, v2}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 955
    :goto_2d
    if-eqz p5, :cond_5d

    #@2f
    .line 956
    const/4 v2, 0x1

    #@30
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 957
    const/4 v2, 0x0

    #@34
    invoke-virtual {p5, v0, v2}, Landroid/graphics/Region;->writeToParcel(Landroid/os/Parcel;I)V

    #@37
    .line 962
    :goto_37
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@39
    const/16 v3, 0xa

    #@3b
    const/4 v4, 0x0

    #@3c
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@3f
    .line 963
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_42
    .catchall {:try_start_8 .. :try_end_42} :catchall_50

    #@42
    .line 966
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@45
    .line 967
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 969
    return-void

    #@49
    .line 939
    :cond_49
    const/4 v2, 0x0

    #@4a
    goto :goto_13

    #@4b
    .line 946
    :cond_4b
    const/4 v2, 0x0

    #@4c
    :try_start_4c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_50

    #@4f
    goto :goto_23

    #@50
    .line 966
    :catchall_50
    move-exception v2

    #@51
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@54
    .line 967
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@57
    throw v2

    #@58
    .line 953
    :cond_58
    const/4 v2, 0x0

    #@59
    :try_start_59
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    goto :goto_2d

    #@5d
    .line 960
    :cond_5d
    const/4 v2, 0x0

    #@5e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_61
    .catchall {:try_start_59 .. :try_end_61} :catchall_50

    #@61
    goto :goto_37
.end method

.method public setTransparentRegion(Landroid/view/IWindow;Landroid/graphics/Region;)V
    .registers 8
    .parameter "window"
    .parameter "region"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 903
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 904
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 906
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 907
    if-eqz p1, :cond_32

    #@f
    invoke-interface {p1}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 908
    if-eqz p2, :cond_34

    #@18
    .line 909
    const/4 v2, 0x1

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 910
    const/4 v2, 0x0

    #@1d
    invoke-virtual {p2, v0, v2}, Landroid/graphics/Region;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 915
    :goto_20
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v3, 0x9

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 916
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_39

    #@2b
    .line 919
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 920
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 922
    return-void

    #@32
    .line 907
    :cond_32
    const/4 v2, 0x0

    #@33
    goto :goto_13

    #@34
    .line 913
    :cond_34
    const/4 v2, 0x0

    #@35
    :try_start_35
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_39

    #@38
    goto :goto_20

    #@39
    .line 919
    :catchall_39
    move-exception v2

    #@3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 920
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    throw v2
.end method

.method public setUniverseTransform(Landroid/os/IBinder;FFFFFFF)V
    .registers 14
    .parameter "window"
    .parameter "alpha"
    .parameter "offx"
    .parameter "offy"
    .parameter "dsdx"
    .parameter "dtdx"
    .parameter "dsdy"
    .parameter "dtdy"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1277
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1278
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1280
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1281
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 1282
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeFloat(F)V

    #@13
    .line 1283
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeFloat(F)V

    #@16
    .line 1284
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeFloat(F)V

    #@19
    .line 1285
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeFloat(F)V

    #@1c
    .line 1286
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeFloat(F)V

    #@1f
    .line 1287
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeFloat(F)V

    #@22
    .line 1288
    invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeFloat(F)V

    #@25
    .line 1289
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/16 v3, 0x19

    #@29
    const/4 v4, 0x0

    #@2a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 1290
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_30
    .catchall {:try_start_8 .. :try_end_30} :catchall_37

    #@30
    .line 1293
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 1294
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 1296
    return-void

    #@37
    .line 1293
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 1294
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2
.end method

.method public setWallpaperPosition(Landroid/os/IBinder;FFFF)V
    .registers 11
    .parameter "windowToken"
    .parameter "x"
    .parameter "y"
    .parameter "xstep"
    .parameter "ystep"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1186
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1187
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1189
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1190
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 1191
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeFloat(F)V

    #@13
    .line 1192
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeFloat(F)V

    #@16
    .line 1193
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeFloat(F)V

    #@19
    .line 1194
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeFloat(F)V

    #@1c
    .line 1195
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v3, 0x15

    #@20
    const/4 v4, 0x0

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 1196
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_27
    .catchall {:try_start_8 .. :try_end_27} :catchall_2e

    #@27
    .line 1199
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1200
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 1202
    return-void

    #@2e
    .line 1199
    :catchall_2e
    move-exception v2

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 1200
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v2
.end method

.method public wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .registers 8
    .parameter "window"
    .parameter "result"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1255
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1256
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1258
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1259
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 1260
    if-eqz p2, :cond_2c

    #@12
    .line 1261
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1262
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 1267
    :goto_1a
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v3, 0x18

    #@1e
    const/4 v4, 0x0

    #@1f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 1268
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_31

    #@25
    .line 1271
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1272
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1274
    return-void

    #@2c
    .line 1265
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_1a

    #@31
    .line 1271
    :catchall_31
    move-exception v2

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 1272
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v2
.end method

.method public wallpaperOffsetsComplete(Landroid/os/IBinder;)V
    .registers 7
    .parameter "window"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1205
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1206
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1208
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.view.IWindowSession"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1209
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 1210
    iget-object v2, p0, Landroid/view/IWindowSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x16

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1211
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 1214
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 1215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 1217
    return-void

    #@22
    .line 1214
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 1215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method
