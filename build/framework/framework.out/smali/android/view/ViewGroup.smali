.class public abstract Landroid/view/ViewGroup;
.super Landroid/view/View;
.source "ViewGroup.java"

# interfaces
.implements Landroid/view/ViewParent;
.implements Landroid/view/ViewManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ViewGroup$ViewLocationHolder;,
        Landroid/view/ViewGroup$ChildListForAccessibility;,
        Landroid/view/ViewGroup$HoverTarget;,
        Landroid/view/ViewGroup$TouchTarget;,
        Landroid/view/ViewGroup$MarginLayoutParams;,
        Landroid/view/ViewGroup$LayoutParams;,
        Landroid/view/ViewGroup$OnHierarchyChangeListener;
    }
.end annotation


# static fields
.field private static final ARRAY_CAPACITY_INCREMENT:I = 0xc

.field private static final ARRAY_INITIAL_CAPACITY:I = 0xc

.field private static final CHILD_LEFT_INDEX:I = 0x0

.field private static final CHILD_TOP_INDEX:I = 0x1

.field public static final CLIP_BOUNDS:I = 0x0

.field protected static final CLIP_TO_PADDING_MASK:I = 0x22

.field private static final DBG:Z = false

.field private static final DESCENDANT_FOCUSABILITY_FLAGS:[I = null

.field private static final FLAG_ADD_STATES_FROM_CHILDREN:I = 0x2000

.field static final FLAG_ALPHA_LOWER_THAN_ONE:I = 0x1000

.field static final FLAG_ALWAYS_DRAWN_WITH_CACHE:I = 0x4000

.field private static final FLAG_ANIMATION_CACHE:I = 0x40

.field static final FLAG_ANIMATION_DONE:I = 0x10

.field static final FLAG_CHILDREN_DRAWN_WITH_CACHE:I = 0x8000

.field static final FLAG_CLEAR_TRANSFORMATION:I = 0x100

.field static final FLAG_CLIP_CHILDREN:I = 0x1

.field private static final FLAG_CLIP_TO_PADDING:I = 0x2

.field protected static final FLAG_DISALLOW_INTERCEPT:I = 0x80000

.field static final FLAG_INVALIDATE_REQUIRED:I = 0x4

.field private static final FLAG_MASK_FOCUSABILITY:I = 0x60000

.field private static final FLAG_NOTIFY_ANIMATION_LISTENER:I = 0x200

.field private static final FLAG_NOTIFY_CHILDREN_ON_DRAWABLE_STATE_CHANGE:I = 0x10000

.field static final FLAG_OPTIMIZE_INVALIDATE:I = 0x80

.field private static final FLAG_PADDING_NOT_NULL:I = 0x20

.field private static final FLAG_PREVENT_DISPATCH_ATTACHED_TO_WINDOW:I = 0x400000

.field private static final FLAG_RUN_ANIMATION:I = 0x8

.field private static final FLAG_SPLIT_MOTION_EVENTS:I = 0x200000

.field protected static final FLAG_SUPPORT_STATIC_TRANSFORMATIONS:I = 0x800

.field protected static final FLAG_USE_CHILD_DRAWING_ORDER:I = 0x400

.field public static final FOCUS_AFTER_DESCENDANTS:I = 0x40000

.field public static final FOCUS_BEFORE_DESCENDANTS:I = 0x20000

.field public static final FOCUS_BLOCK_DESCENDANTS:I = 0x60000

.field public static final OPTICAL_BOUNDS:I = 0x1

.field public static final PERSISTENT_ALL_CACHES:I = 0x3

.field public static final PERSISTENT_ANIMATION_CACHE:I = 0x1

.field public static final PERSISTENT_NO_CACHE:I = 0x0

.field public static final PERSISTENT_SCROLLING_CACHE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ViewGroup"

.field static final mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

.field private static sDebugLines:[F

.field private static sDebugPaint:Landroid/graphics/Paint;


# instance fields
.field private mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field mCachePaint:Landroid/graphics/Paint;

.field private mChildAcceptsDrag:Z

.field private mChildCountWithTransientState:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field final mChildTransformation:Landroid/view/animation/Transformation;

.field private mChildren:[Landroid/view/View;

.field private mChildrenCount:I

.field private mCurrentDrag:Landroid/view/DragEvent;

.field private mCurrentDragView:Landroid/view/View;

.field protected mDisappearingChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mDragNotifiedChildren:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

.field private mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

.field private mFocused:Landroid/view/View;

.field protected mGroupFlags:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        flagMapping = {
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1
                mask = 0x1
                name = "CLIP_CHILDREN"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x2
                mask = 0x2
                name = "CLIP_TO_PADDING"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x20
                mask = 0x20
                name = "PADDING_NOT_NULL"
            .end subannotation
        }
    .end annotation
.end field

.field private mHoveredSelf:Z

.field mInvalidateRegion:Landroid/graphics/RectF;

.field mInvalidationTransformation:Landroid/view/animation/Transformation;

.field private mLastTouchDownIndex:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "events"
    .end annotation
.end field

.field private mLastTouchDownTime:J
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "events"
    .end annotation
.end field

.field private mLastTouchDownX:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "events"
    .end annotation
.end field

.field private mLastTouchDownY:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "events"
    .end annotation
.end field

.field private mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

.field private mLayoutMode:I

.field private mLayoutSuppressed:Z

.field private mLayoutTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

.field private final mLocalPoint:Landroid/graphics/PointF;

.field protected mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field protected mPersistentDrawingCache:I

.field private mTransition:Landroid/animation/LayoutTransition;

.field private mTransitioningViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibilityChangingChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 307
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_10

    #@6
    sput-object v0, Landroid/view/ViewGroup;->DESCENDANT_FOCUSABILITY_FLAGS:[I

    #@8
    .line 422
    new-instance v0, Lcom/android/internal/app/ActivityTrigger;

    #@a
    invoke-direct {v0}, Lcom/android/internal/app/ActivityTrigger;-><init>()V

    #@d
    sput-object v0, Landroid/view/ViewGroup;->mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

    #@f
    return-void

    #@10
    .line 307
    :array_10
    .array-data 0x4
        0x0t 0x0t 0x2t 0x0t
        0x0t 0x0t 0x4t 0x0t
        0x0t 0x0t 0x6t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 425
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@4
    .line 113
    new-instance v0, Landroid/view/animation/Transformation;

    #@6
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@b
    .line 137
    new-instance v0, Landroid/graphics/PointF;

    #@d
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@10
    iput-object v0, p0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@12
    .line 150
    const/4 v0, -0x1

    #@13
    iput v0, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@15
    .line 189
    iput v1, p0, Landroid/view/ViewGroup;->mLayoutMode:I

    #@17
    .line 393
    iput-boolean v1, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@19
    .line 419
    iput v1, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@1b
    .line 5080
    new-instance v0, Landroid/view/ViewGroup$3;

    #@1d
    invoke-direct {v0, p0}, Landroid/view/ViewGroup$3;-><init>(Landroid/view/ViewGroup;)V

    #@20
    iput-object v0, p0, Landroid/view/ViewGroup;->mLayoutTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    #@22
    .line 426
    invoke-direct {p0}, Landroid/view/ViewGroup;->initViewGroup()V

    #@25
    .line 427
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 430
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 113
    new-instance v0, Landroid/view/animation/Transformation;

    #@6
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@b
    .line 137
    new-instance v0, Landroid/graphics/PointF;

    #@d
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@10
    iput-object v0, p0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@12
    .line 150
    const/4 v0, -0x1

    #@13
    iput v0, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@15
    .line 189
    iput v1, p0, Landroid/view/ViewGroup;->mLayoutMode:I

    #@17
    .line 393
    iput-boolean v1, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@19
    .line 419
    iput v1, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@1b
    .line 5080
    new-instance v0, Landroid/view/ViewGroup$3;

    #@1d
    invoke-direct {v0, p0}, Landroid/view/ViewGroup$3;-><init>(Landroid/view/ViewGroup;)V

    #@20
    iput-object v0, p0, Landroid/view/ViewGroup;->mLayoutTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    #@22
    .line 431
    invoke-direct {p0}, Landroid/view/ViewGroup;->initViewGroup()V

    #@25
    .line 432
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@28
    .line 433
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 436
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 113
    new-instance v0, Landroid/view/animation/Transformation;

    #@6
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@b
    .line 137
    new-instance v0, Landroid/graphics/PointF;

    #@d
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@10
    iput-object v0, p0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@12
    .line 150
    const/4 v0, -0x1

    #@13
    iput v0, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@15
    .line 189
    iput v1, p0, Landroid/view/ViewGroup;->mLayoutMode:I

    #@17
    .line 393
    iput-boolean v1, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@19
    .line 419
    iput v1, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@1b
    .line 5080
    new-instance v0, Landroid/view/ViewGroup$3;

    #@1d
    invoke-direct {v0, p0}, Landroid/view/ViewGroup$3;-><init>(Landroid/view/ViewGroup;)V

    #@20
    iput-object v0, p0, Landroid/view/ViewGroup;->mLayoutTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    #@22
    .line 437
    invoke-direct {p0}, Landroid/view/ViewGroup;->initViewGroup()V

    #@25
    .line 438
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@28
    .line 439
    return-void
.end method

.method static synthetic access$000(Landroid/view/ViewGroup;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Landroid/view/ViewGroup;->notifyAnimationListener()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/view/ViewGroup;)Landroid/view/animation/LayoutAnimationController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/view/ViewGroup;)Landroid/view/animation/Animation$AnimationListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Landroid/view/ViewGroup;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/view/ViewGroup;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/view/ViewGroup;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/view/ViewGroup;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/graphics/Canvas;IIIII)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 85
    invoke-static/range {p0 .. p5}, Landroid/view/ViewGroup;->drawRect(Landroid/graphics/Canvas;IIIII)V

    #@3
    return-void
.end method

.method private addDisappearingView(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 4973
    iget-object v0, p0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@2
    .line 4975
    .local v0, disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-nez v0, :cond_b

    #@4
    .line 4976
    new-instance v0, Ljava/util/ArrayList;

    #@6
    .end local v0           #disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@b
    .line 4979
    .restart local v0       #disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_b
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e
    .line 4980
    return-void
.end method

.method private addInArray(Landroid/view/View;I)V
    .registers 9
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3441
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@3
    .line 3442
    .local v0, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 3443
    .local v1, count:I
    array-length v2, v0

    #@6
    .line 3444
    .local v2, size:I
    if-ne p2, v1, :cond_20

    #@8
    .line 3445
    if-ne v2, v1, :cond_17

    #@a
    .line 3446
    add-int/lit8 v3, v2, 0xc

    #@c
    new-array v3, v3, [Landroid/view/View;

    #@e
    iput-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@10
    .line 3447
    iget-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@12
    invoke-static {v0, v4, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@15
    .line 3448
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@17
    .line 3450
    :cond_17
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@19
    add-int/lit8 v4, v3, 0x1

    #@1b
    iput v4, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@1d
    aput-object p1, v0, v3

    #@1f
    .line 3468
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 3451
    :cond_20
    if-ge p2, v1, :cond_55

    #@22
    .line 3452
    if-ne v2, v1, :cond_4d

    #@24
    .line 3453
    add-int/lit8 v3, v2, 0xc

    #@26
    new-array v3, v3, [Landroid/view/View;

    #@28
    iput-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@2a
    .line 3454
    iget-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@2c
    invoke-static {v0, v4, v3, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2f
    .line 3455
    iget-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@31
    add-int/lit8 v4, p2, 0x1

    #@33
    sub-int v5, v1, p2

    #@35
    invoke-static {v0, p2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@38
    .line 3456
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@3a
    .line 3460
    :goto_3a
    aput-object p1, v0, p2

    #@3c
    .line 3461
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@3e
    add-int/lit8 v3, v3, 0x1

    #@40
    iput v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@42
    .line 3462
    iget v3, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@44
    if-lt v3, p2, :cond_1f

    #@46
    .line 3463
    iget v3, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@48
    add-int/lit8 v3, v3, 0x1

    #@4a
    iput v3, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@4c
    goto :goto_1f

    #@4d
    .line 3458
    :cond_4d
    add-int/lit8 v3, p2, 0x1

    #@4f
    sub-int v4, v1, p2

    #@51
    invoke-static {v0, p2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@54
    goto :goto_3a

    #@55
    .line 3466
    :cond_55
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    #@57
    new-instance v4, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v5, "index="

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    const-string v5, " count="

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v4

    #@74
    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@77
    throw v3
.end method

.method private addTouchTarget(Landroid/view/View;I)Landroid/view/ViewGroup$TouchTarget;
    .registers 5
    .parameter "child"
    .parameter "pointerIdBits"

    #@0
    .prologue
    .line 2069
    invoke-static {p1, p2}, Landroid/view/ViewGroup$TouchTarget;->obtain(Landroid/view/View;I)Landroid/view/ViewGroup$TouchTarget;

    #@3
    move-result-object v0

    #@4
    .line 2070
    .local v0, target:Landroid/view/ViewGroup$TouchTarget;
    iget-object v1, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@6
    iput-object v1, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@8
    .line 2071
    iput-object v0, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@a
    .line 2072
    return-object v0
.end method

.method private addViewInner(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V
    .registers 11
    .parameter "child"
    .parameter "index"
    .parameter "params"
    .parameter "preventRequestLayout"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/high16 v4, 0x40

    #@3
    .line 3372
    iget-object v2, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@5
    if-eqz v2, :cond_d

    #@7
    .line 3375
    iget-object v2, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@9
    const/4 v3, 0x3

    #@a
    invoke-virtual {v2, v3}, Landroid/animation/LayoutTransition;->cancel(I)V

    #@d
    .line 3378
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@10
    move-result-object v2

    #@11
    if-eqz v2, :cond_1b

    #@13
    .line 3379
    new-instance v2, Ljava/lang/IllegalStateException;

    #@15
    const-string v3, "The specified child already has a parent. You must call removeView() on the child\'s parent first."

    #@17
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 3383
    :cond_1b
    iget-object v2, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@1d
    if-eqz v2, :cond_24

    #@1f
    .line 3384
    iget-object v2, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@21
    invoke-virtual {v2, p0, p1}, Landroid/animation/LayoutTransition;->addChild(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@24
    .line 3387
    :cond_24
    invoke-virtual {p0, p3}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_2e

    #@2a
    .line 3388
    invoke-virtual {p0, p3}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    #@2d
    move-result-object p3

    #@2e
    .line 3391
    :cond_2e
    if-eqz p4, :cond_8d

    #@30
    .line 3392
    iput-object p3, p1, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@32
    .line 3397
    :goto_32
    if-gez p2, :cond_36

    #@34
    .line 3398
    iget p2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@36
    .line 3401
    :cond_36
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->addInArray(Landroid/view/View;I)V

    #@39
    .line 3404
    if-eqz p4, :cond_91

    #@3b
    .line 3405
    invoke-virtual {p1, p0}, Landroid/view/View;->assignParent(Landroid/view/ViewParent;)V

    #@3e
    .line 3410
    :goto_3e
    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_4b

    #@44
    .line 3411
    invoke-virtual {p1}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {p0, p1, v2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@4b
    .line 3414
    :cond_4b
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4d
    .line 3415
    .local v0, ai:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_6b

    #@4f
    iget v2, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@51
    and-int/2addr v2, v4

    #@52
    if-nez v2, :cond_6b

    #@54
    .line 3416
    iget-boolean v1, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@56
    .line 3417
    .local v1, lastKeepOn:Z
    const/4 v2, 0x0

    #@57
    iput-boolean v2, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@59
    .line 3418
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5b
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@5d
    and-int/lit8 v3, v3, 0xc

    #@5f
    invoke-virtual {p1, v2, v3}, Landroid/view/View;->dispatchAttachedToWindow(Landroid/view/View$AttachInfo;I)V

    #@62
    .line 3419
    iget-boolean v2, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@64
    if-eqz v2, :cond_69

    #@66
    .line 3420
    invoke-virtual {p0, v5}, Landroid/view/ViewGroup;->needGlobalAttributesUpdate(Z)V

    #@69
    .line 3422
    :cond_69
    iput-boolean v1, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@6b
    .line 3425
    .end local v1           #lastKeepOn:Z
    :cond_6b
    invoke-virtual {p1}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@6e
    move-result v2

    #@6f
    if-eqz v2, :cond_74

    #@71
    .line 3426
    invoke-virtual {p1}, Landroid/view/View;->resetRtlProperties()V

    #@74
    .line 3429
    :cond_74
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->onViewAdded(Landroid/view/View;)V

    #@77
    .line 3431
    iget v2, p1, Landroid/view/View;->mViewFlags:I

    #@79
    and-int/2addr v2, v4

    #@7a
    if-ne v2, v4, :cond_83

    #@7c
    .line 3432
    iget v2, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@7e
    const/high16 v3, 0x1

    #@80
    or-int/2addr v2, v3

    #@81
    iput v2, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@83
    .line 3435
    :cond_83
    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    #@86
    move-result v2

    #@87
    if-eqz v2, :cond_8c

    #@89
    .line 3436
    invoke-virtual {p0, p1, v5}, Landroid/view/ViewGroup;->childHasTransientStateChanged(Landroid/view/View;Z)V

    #@8c
    .line 3438
    :cond_8c
    return-void

    #@8d
    .line 3394
    .end local v0           #ai:Landroid/view/View$AttachInfo;
    :cond_8d
    invoke-virtual {p1, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@90
    goto :goto_32

    #@91
    .line 3407
    :cond_91
    iput-object p0, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@93
    goto :goto_3e
.end method

.method private bindLayoutAnimation(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 3528
    iget-object v1, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@2
    invoke-virtual {v1, p1}, Landroid/view/animation/LayoutAnimationController;->getAnimationForView(Landroid/view/View;)Landroid/view/animation/Animation;

    #@5
    move-result-object v0

    #@6
    .line 3529
    .local v0, a:Landroid/view/animation/Animation;
    invoke-virtual {p1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    #@9
    .line 3530
    return-void
.end method

.method private static canViewReceivePointerEvents(Landroid/view/View;)Z
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 2132
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0xc

    #@4
    if-eqz v0, :cond_c

    #@6
    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@9
    move-result-object v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private cancelAndClearTouchTargets(Landroid/view/MotionEvent;)V
    .registers 12
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2029
    iget-object v2, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@3
    if-eqz v2, :cond_36

    #@5
    .line 2030
    const/4 v8, 0x0

    #@6
    .line 2031
    .local v8, syntheticEvent:Z
    if-nez p1, :cond_1a

    #@8
    .line 2032
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v0

    #@c
    .line 2033
    .local v0, now:J
    const/4 v4, 0x3

    #@d
    const/4 v7, 0x0

    #@e
    move-wide v2, v0

    #@f
    move v6, v5

    #@10
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@13
    move-result-object p1

    #@14
    .line 2035
    const/16 v2, 0x1002

    #@16
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setSource(I)V

    #@19
    .line 2036
    const/4 v8, 0x1

    #@1a
    .line 2039
    .end local v0           #now:J
    :cond_1a
    iget-object v9, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@1c
    .local v9, target:Landroid/view/ViewGroup$TouchTarget;
    :goto_1c
    if-eqz v9, :cond_2e

    #@1e
    .line 2040
    iget-object v2, v9, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@20
    invoke-static {v2}, Landroid/view/ViewGroup;->resetCancelNextUpFlag(Landroid/view/View;)Z

    #@23
    .line 2041
    const/4 v2, 0x1

    #@24
    iget-object v3, v9, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@26
    iget v4, v9, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@28
    invoke-direct {p0, p1, v2, v3, v4}, Landroid/view/ViewGroup;->dispatchTransformedTouchEvent(Landroid/view/MotionEvent;ZLandroid/view/View;I)Z

    #@2b
    .line 2039
    iget-object v9, v9, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@2d
    goto :goto_1c

    #@2e
    .line 2043
    :cond_2e
    invoke-direct {p0}, Landroid/view/ViewGroup;->clearTouchTargets()V

    #@31
    .line 2045
    if-eqz v8, :cond_36

    #@33
    .line 2046
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    #@36
    .line 2049
    .end local v8           #syntheticEvent:Z
    .end local v9           #target:Landroid/view/ViewGroup$TouchTarget;
    :cond_36
    return-void
.end method

.method private cancelHoverTarget(Landroid/view/View;)V
    .registers 14
    .parameter "view"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1638
    const/4 v10, 0x0

    #@2
    .line 1639
    .local v10, predecessor:Landroid/view/ViewGroup$HoverTarget;
    iget-object v11, p0, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@4
    .line 1640
    .local v11, target:Landroid/view/ViewGroup$HoverTarget;
    :goto_4
    if-eqz v11, :cond_2b

    #@6
    .line 1641
    iget-object v9, v11, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@8
    .line 1642
    .local v9, next:Landroid/view/ViewGroup$HoverTarget;
    iget-object v2, v11, Landroid/view/ViewGroup$HoverTarget;->child:Landroid/view/View;

    #@a
    if-ne v2, p1, :cond_2f

    #@c
    .line 1643
    if-nez v10, :cond_2c

    #@e
    .line 1644
    iput-object v9, p0, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@10
    .line 1648
    :goto_10
    invoke-virtual {v11}, Landroid/view/ViewGroup$HoverTarget;->recycle()V

    #@13
    .line 1650
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@16
    move-result-wide v0

    #@17
    .line 1651
    .local v0, now:J
    const/16 v4, 0xa

    #@19
    const/4 v7, 0x0

    #@1a
    move-wide v2, v0

    #@1b
    move v6, v5

    #@1c
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@1f
    move-result-object v8

    #@20
    .line 1653
    .local v8, event:Landroid/view/MotionEvent;
    const/16 v2, 0x1002

    #@22
    invoke-virtual {v8, v2}, Landroid/view/MotionEvent;->setSource(I)V

    #@25
    .line 1654
    invoke-virtual {p1, v8}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@28
    .line 1655
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    #@2b
    .line 1661
    .end local v0           #now:J
    .end local v8           #event:Landroid/view/MotionEvent;
    .end local v9           #next:Landroid/view/ViewGroup$HoverTarget;
    :cond_2b
    return-void

    #@2c
    .line 1646
    .restart local v9       #next:Landroid/view/ViewGroup$HoverTarget;
    :cond_2c
    iput-object v9, v10, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@2e
    goto :goto_10

    #@2f
    .line 1658
    :cond_2f
    move-object v10, v11

    #@30
    .line 1659
    move-object v11, v9

    #@31
    .line 1660
    goto :goto_4
.end method

.method private cancelTouchTarget(Landroid/view/View;)V
    .registers 14
    .parameter "view"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2102
    const/4 v10, 0x0

    #@2
    .line 2103
    .local v10, predecessor:Landroid/view/ViewGroup$TouchTarget;
    iget-object v11, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@4
    .line 2104
    .local v11, target:Landroid/view/ViewGroup$TouchTarget;
    :goto_4
    if-eqz v11, :cond_2a

    #@6
    .line 2105
    iget-object v9, v11, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@8
    .line 2106
    .local v9, next:Landroid/view/ViewGroup$TouchTarget;
    iget-object v2, v11, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@a
    if-ne v2, p1, :cond_2e

    #@c
    .line 2107
    if-nez v10, :cond_2b

    #@e
    .line 2108
    iput-object v9, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@10
    .line 2112
    :goto_10
    invoke-virtual {v11}, Landroid/view/ViewGroup$TouchTarget;->recycle()V

    #@13
    .line 2114
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@16
    move-result-wide v0

    #@17
    .line 2115
    .local v0, now:J
    const/4 v4, 0x3

    #@18
    const/4 v7, 0x0

    #@19
    move-wide v2, v0

    #@1a
    move v6, v5

    #@1b
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@1e
    move-result-object v8

    #@1f
    .line 2117
    .local v8, event:Landroid/view/MotionEvent;
    const/16 v2, 0x1002

    #@21
    invoke-virtual {v8, v2}, Landroid/view/MotionEvent;->setSource(I)V

    #@24
    .line 2118
    invoke-virtual {p1, v8}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@27
    .line 2119
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    #@2a
    .line 2125
    .end local v0           #now:J
    .end local v8           #event:Landroid/view/MotionEvent;
    .end local v9           #next:Landroid/view/ViewGroup$TouchTarget;
    :cond_2a
    return-void

    #@2b
    .line 2110
    .restart local v9       #next:Landroid/view/ViewGroup$TouchTarget;
    :cond_2b
    iput-object v9, v10, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@2d
    goto :goto_10

    #@2e
    .line 2122
    :cond_2e
    move-object v10, v11

    #@2f
    .line 2123
    move-object v11, v9

    #@30
    .line 2124
    goto :goto_4
.end method

.method private clearTouchTargets()V
    .registers 4

    #@0
    .prologue
    .line 2014
    iget-object v1, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@2
    .line 2015
    .local v1, target:Landroid/view/ViewGroup$TouchTarget;
    if-eqz v1, :cond_f

    #@4
    .line 2017
    :cond_4
    iget-object v0, v1, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@6
    .line 2018
    .local v0, next:Landroid/view/ViewGroup$TouchTarget;
    invoke-virtual {v1}, Landroid/view/ViewGroup$TouchTarget;->recycle()V

    #@9
    .line 2019
    move-object v1, v0

    #@a
    .line 2020
    if-nez v1, :cond_4

    #@c
    .line 2021
    const/4 v2, 0x0

    #@d
    iput-object v2, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@f
    .line 2023
    .end local v0           #next:Landroid/view/ViewGroup$TouchTarget;
    :cond_f
    return-void
.end method

.method private debugDraw()Z
    .registers 2

    #@0
    .prologue
    .line 442
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mDebugLayout:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .registers 9
    .parameter "event"
    .parameter "child"

    #@0
    .prologue
    .line 1804
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@2
    iget v5, p2, Landroid/view/View;->mLeft:I

    #@4
    sub-int/2addr v4, v5

    #@5
    int-to-float v1, v4

    #@6
    .line 1805
    .local v1, offsetX:F
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@8
    iget v5, p2, Landroid/view/View;->mTop:I

    #@a
    sub-int/2addr v4, v5

    #@b
    int-to-float v2, v4

    #@c
    .line 1808
    .local v2, offsetY:F
    invoke-virtual {p2}, Landroid/view/View;->hasIdentityMatrix()Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_28

    #@12
    .line 1809
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@15
    move-result-object v3

    #@16
    .line 1810
    .local v3, transformedEvent:Landroid/view/MotionEvent;
    invoke-virtual {v3, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@19
    .line 1811
    invoke-virtual {p2}, Landroid/view/View;->getInverseMatrix()Landroid/graphics/Matrix;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    #@20
    .line 1812
    invoke-virtual {p2, v3}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@23
    move-result v0

    #@24
    .line 1813
    .local v0, handled:Z
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    #@27
    .line 1819
    .end local v3           #transformedEvent:Landroid/view/MotionEvent;
    :goto_27
    return v0

    #@28
    .line 1815
    .end local v0           #handled:Z
    :cond_28
    invoke-virtual {p1, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@2b
    .line 1816
    invoke-virtual {p2, p1}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@2e
    move-result v0

    #@2f
    .line 1817
    .restart local v0       #handled:Z
    neg-float v4, v1

    #@30
    neg-float v5, v2

    #@31
    invoke-virtual {p1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@34
    goto :goto_27
.end method

.method private dispatchTransformedTouchEvent(Landroid/view/MotionEvent;ZLandroid/view/View;I)Z
    .registers 14
    .parameter "event"
    .parameter "cancel"
    .parameter "child"
    .parameter "desiredPointerIdBits"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v0, 0x0

    #@2
    .line 2172
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@5
    move-result v4

    #@6
    .line 2173
    .local v4, oldAction:I
    if-nez p2, :cond_a

    #@8
    if-ne v4, v7, :cond_1c

    #@a
    .line 2174
    :cond_a
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    #@d
    .line 2175
    if-nez p3, :cond_17

    #@f
    .line 2176
    invoke-super {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v0

    #@13
    .line 2180
    .local v0, handled:Z
    :goto_13
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@16
    .line 2241
    .end local v0           #handled:Z
    :cond_16
    :goto_16
    return v0

    #@17
    .line 2178
    :cond_17
    invoke-virtual {p3, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@1a
    move-result v0

    #@1b
    .restart local v0       #handled:Z
    goto :goto_13

    #@1c
    .line 2185
    .end local v0           #handled:Z
    :cond_1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerIdBits()I

    #@1f
    move-result v5

    #@20
    .line 2186
    .local v5, oldPointerIdBits:I
    and-int v1, v5, p4

    #@22
    .line 2190
    .local v1, newPointerIdBits:I
    if-eqz v1, :cond_16

    #@24
    .line 2199
    if-ne v1, v5, :cond_5e

    #@26
    .line 2200
    if-eqz p3, :cond_2e

    #@28
    invoke-virtual {p3}, Landroid/view/View;->hasIdentityMatrix()Z

    #@2b
    move-result v7

    #@2c
    if-eqz v7, :cond_4e

    #@2e
    .line 2201
    :cond_2e
    if-nez p3, :cond_35

    #@30
    .line 2202
    invoke-super {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@33
    move-result v0

    #@34
    .restart local v0       #handled:Z
    goto :goto_16

    #@35
    .line 2204
    .end local v0           #handled:Z
    :cond_35
    iget v7, p0, Landroid/view/View;->mScrollX:I

    #@37
    iget v8, p3, Landroid/view/View;->mLeft:I

    #@39
    sub-int/2addr v7, v8

    #@3a
    int-to-float v2, v7

    #@3b
    .line 2205
    .local v2, offsetX:F
    iget v7, p0, Landroid/view/View;->mScrollY:I

    #@3d
    iget v8, p3, Landroid/view/View;->mTop:I

    #@3f
    sub-int/2addr v7, v8

    #@40
    int-to-float v3, v7

    #@41
    .line 2206
    .local v3, offsetY:F
    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@44
    .line 2208
    invoke-virtual {p3, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@47
    move-result v0

    #@48
    .line 2210
    .restart local v0       #handled:Z
    neg-float v7, v2

    #@49
    neg-float v8, v3

    #@4a
    invoke-virtual {p1, v7, v8}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@4d
    goto :goto_16

    #@4e
    .line 2214
    .end local v0           #handled:Z
    .end local v2           #offsetX:F
    .end local v3           #offsetY:F
    :cond_4e
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@51
    move-result-object v6

    #@52
    .line 2220
    .local v6, transformedEvent:Landroid/view/MotionEvent;
    :goto_52
    if-eqz v6, :cond_16

    #@54
    .line 2222
    if-nez p3, :cond_63

    #@56
    .line 2223
    invoke-super {p0, v6}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@59
    move-result v0

    #@5a
    .line 2235
    .restart local v0       #handled:Z
    :goto_5a
    invoke-virtual {v6}, Landroid/view/MotionEvent;->recycle()V

    #@5d
    goto :goto_16

    #@5e
    .line 2216
    .end local v0           #handled:Z
    .end local v6           #transformedEvent:Landroid/view/MotionEvent;
    :cond_5e
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->split(I)Landroid/view/MotionEvent;

    #@61
    move-result-object v6

    #@62
    .restart local v6       #transformedEvent:Landroid/view/MotionEvent;
    goto :goto_52

    #@63
    .line 2225
    :cond_63
    iget v7, p0, Landroid/view/View;->mScrollX:I

    #@65
    iget v8, p3, Landroid/view/View;->mLeft:I

    #@67
    sub-int/2addr v7, v8

    #@68
    int-to-float v2, v7

    #@69
    .line 2226
    .restart local v2       #offsetX:F
    iget v7, p0, Landroid/view/View;->mScrollY:I

    #@6b
    iget v8, p3, Landroid/view/View;->mTop:I

    #@6d
    sub-int/2addr v7, v8

    #@6e
    int-to-float v3, v7

    #@6f
    .line 2227
    .restart local v3       #offsetY:F
    invoke-virtual {v6, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@72
    .line 2228
    invoke-virtual {p3}, Landroid/view/View;->hasIdentityMatrix()Z

    #@75
    move-result v7

    #@76
    if-nez v7, :cond_7f

    #@78
    .line 2229
    invoke-virtual {p3}, Landroid/view/View;->getInverseMatrix()Landroid/graphics/Matrix;

    #@7b
    move-result-object v7

    #@7c
    invoke-virtual {v6, v7}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    #@7f
    .line 2232
    :cond_7f
    invoke-virtual {p3, v6}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@82
    move-result v0

    #@83
    .restart local v0       #handled:Z
    goto :goto_5a
.end method

.method private static drawRect(Landroid/graphics/Canvas;IIIII)V
    .registers 8
    .parameter "canvas"
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"
    .parameter "color"

    #@0
    .prologue
    .line 2726
    invoke-static {}, Landroid/view/ViewGroup;->getDebugPaint()Landroid/graphics/Paint;

    #@3
    move-result-object v0

    #@4
    .line 2727
    .local v0, paint:Landroid/graphics/Paint;
    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setColor(I)V

    #@7
    .line 2729
    invoke-static {p1, p2, p3, p4}, Landroid/view/ViewGroup;->getDebugLines(IIII)[F

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p0, v1, v0}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    #@e
    .line 2730
    return-void
.end method

.method private exitHoverTargets()V
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1627
    iget-boolean v2, p0, Landroid/view/ViewGroup;->mHoveredSelf:Z

    #@3
    if-nez v2, :cond_9

    #@5
    iget-object v2, p0, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@7
    if-eqz v2, :cond_21

    #@9
    .line 1628
    :cond_9
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v0

    #@d
    .line 1629
    .local v0, now:J
    const/16 v4, 0xa

    #@f
    const/4 v7, 0x0

    #@10
    move-wide v2, v0

    #@11
    move v6, v5

    #@12
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    #@15
    move-result-object v8

    #@16
    .line 1631
    .local v8, event:Landroid/view/MotionEvent;
    const/16 v2, 0x1002

    #@18
    invoke-virtual {v8, v2}, Landroid/view/MotionEvent;->setSource(I)V

    #@1b
    .line 1632
    invoke-virtual {p0, v8}, Landroid/view/ViewGroup;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@1e
    .line 1633
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    #@21
    .line 1635
    .end local v0           #now:J
    .end local v8           #event:Landroid/view/MotionEvent;
    :cond_21
    return-void
.end method

.method public static getChildMeasureSpec(III)I
    .registers 12
    .parameter "spec"
    .parameter "padding"
    .parameter "childDimension"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v7, -0x2

    #@2
    .line 4888
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@5
    move-result v3

    #@6
    .line 4889
    .local v3, specMode:I
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9
    move-result v4

    #@a
    .line 4891
    .local v4, specSize:I
    const/4 v5, 0x0

    #@b
    sub-int v6, v4, p1

    #@d
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    #@10
    move-result v2

    #@11
    .line 4893
    .local v2, size:I
    const/4 v1, 0x0

    #@12
    .line 4894
    .local v1, resultSize:I
    const/4 v0, 0x0

    #@13
    .line 4896
    .local v0, resultMode:I
    sparse-switch v3, :sswitch_data_50

    #@16
    .line 4952
    :cond_16
    :goto_16
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@19
    move-result v5

    #@1a
    return v5

    #@1b
    .line 4899
    :sswitch_1b
    if-ltz p2, :cond_21

    #@1d
    .line 4900
    move v1, p2

    #@1e
    .line 4901
    const/high16 v0, 0x4000

    #@20
    goto :goto_16

    #@21
    .line 4902
    :cond_21
    if-ne p2, v8, :cond_27

    #@23
    .line 4904
    move v1, v2

    #@24
    .line 4905
    const/high16 v0, 0x4000

    #@26
    goto :goto_16

    #@27
    .line 4906
    :cond_27
    if-ne p2, v7, :cond_16

    #@29
    .line 4909
    move v1, v2

    #@2a
    .line 4910
    const/high16 v0, -0x8000

    #@2c
    goto :goto_16

    #@2d
    .line 4916
    :sswitch_2d
    if-ltz p2, :cond_33

    #@2f
    .line 4918
    move v1, p2

    #@30
    .line 4919
    const/high16 v0, 0x4000

    #@32
    goto :goto_16

    #@33
    .line 4920
    :cond_33
    if-ne p2, v8, :cond_39

    #@35
    .line 4923
    move v1, v2

    #@36
    .line 4924
    const/high16 v0, -0x8000

    #@38
    goto :goto_16

    #@39
    .line 4925
    :cond_39
    if-ne p2, v7, :cond_16

    #@3b
    .line 4928
    move v1, v2

    #@3c
    .line 4929
    const/high16 v0, -0x8000

    #@3e
    goto :goto_16

    #@3f
    .line 4935
    :sswitch_3f
    if-ltz p2, :cond_45

    #@41
    .line 4937
    move v1, p2

    #@42
    .line 4938
    const/high16 v0, 0x4000

    #@44
    goto :goto_16

    #@45
    .line 4939
    :cond_45
    if-ne p2, v8, :cond_4a

    #@47
    .line 4942
    const/4 v1, 0x0

    #@48
    .line 4943
    const/4 v0, 0x0

    #@49
    goto :goto_16

    #@4a
    .line 4944
    :cond_4a
    if-ne p2, v7, :cond_16

    #@4c
    .line 4947
    const/4 v1, 0x0

    #@4d
    .line 4948
    const/4 v0, 0x0

    #@4e
    goto :goto_16

    #@4f
    .line 4896
    nop

    #@50
    :sswitch_data_50
    .sparse-switch
        -0x80000000 -> :sswitch_2d
        0x0 -> :sswitch_3f
        0x40000000 -> :sswitch_1b
    .end sparse-switch
.end method

.method private static getDebugLines(IIII)[F
    .registers 7
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"

    #@0
    .prologue
    .line 6389
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@2
    if-nez v0, :cond_a

    #@4
    .line 6390
    const/16 v0, 0x10

    #@6
    new-array v0, v0, [F

    #@8
    sput-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@a
    .line 6393
    :cond_a
    add-int/lit8 p2, p2, -0x1

    #@c
    .line 6394
    add-int/lit8 p3, p3, -0x1

    #@e
    .line 6396
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@10
    const/4 v1, 0x0

    #@11
    int-to-float v2, p0

    #@12
    aput v2, v0, v1

    #@14
    .line 6397
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@16
    const/4 v1, 0x1

    #@17
    int-to-float v2, p1

    #@18
    aput v2, v0, v1

    #@1a
    .line 6398
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@1c
    const/4 v1, 0x2

    #@1d
    int-to-float v2, p2

    #@1e
    aput v2, v0, v1

    #@20
    .line 6399
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@22
    const/4 v1, 0x3

    #@23
    int-to-float v2, p1

    #@24
    aput v2, v0, v1

    #@26
    .line 6401
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@28
    const/4 v1, 0x4

    #@29
    int-to-float v2, p2

    #@2a
    aput v2, v0, v1

    #@2c
    .line 6402
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@2e
    const/4 v1, 0x5

    #@2f
    int-to-float v2, p1

    #@30
    aput v2, v0, v1

    #@32
    .line 6403
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@34
    const/4 v1, 0x6

    #@35
    int-to-float v2, p2

    #@36
    aput v2, v0, v1

    #@38
    .line 6404
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@3a
    const/4 v1, 0x7

    #@3b
    add-int/lit8 v2, p3, 0x1

    #@3d
    int-to-float v2, v2

    #@3e
    aput v2, v0, v1

    #@40
    .line 6406
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@42
    const/16 v1, 0x8

    #@44
    add-int/lit8 v2, p2, 0x1

    #@46
    int-to-float v2, v2

    #@47
    aput v2, v0, v1

    #@49
    .line 6407
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@4b
    const/16 v1, 0x9

    #@4d
    int-to-float v2, p3

    #@4e
    aput v2, v0, v1

    #@50
    .line 6408
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@52
    const/16 v1, 0xa

    #@54
    int-to-float v2, p0

    #@55
    aput v2, v0, v1

    #@57
    .line 6409
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@59
    const/16 v1, 0xb

    #@5b
    int-to-float v2, p3

    #@5c
    aput v2, v0, v1

    #@5e
    .line 6411
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@60
    const/16 v1, 0xc

    #@62
    int-to-float v2, p0

    #@63
    aput v2, v0, v1

    #@65
    .line 6412
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@67
    const/16 v1, 0xd

    #@69
    int-to-float v2, p3

    #@6a
    aput v2, v0, v1

    #@6c
    .line 6413
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@6e
    const/16 v1, 0xe

    #@70
    int-to-float v2, p0

    #@71
    aput v2, v0, v1

    #@73
    .line 6414
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@75
    const/16 v1, 0xf

    #@77
    int-to-float v2, p1

    #@78
    aput v2, v0, v1

    #@7a
    .line 6416
    sget-object v0, Landroid/view/ViewGroup;->sDebugLines:[F

    #@7c
    return-object v0
.end method

.method private static getDebugPaint()Landroid/graphics/Paint;
    .registers 2

    #@0
    .prologue
    .line 6381
    sget-object v0, Landroid/view/ViewGroup;->sDebugPaint:Landroid/graphics/Paint;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 6382
    new-instance v0, Landroid/graphics/Paint;

    #@6
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@9
    sput-object v0, Landroid/view/ViewGroup;->sDebugPaint:Landroid/graphics/Paint;

    #@b
    .line 6383
    sget-object v0, Landroid/view/ViewGroup;->sDebugPaint:Landroid/graphics/Paint;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@11
    .line 6385
    :cond_11
    sget-object v0, Landroid/view/ViewGroup;->sDebugPaint:Landroid/graphics/Paint;

    #@13
    return-object v0
.end method

.method private getTouchTarget(Landroid/view/View;)Landroid/view/ViewGroup$TouchTarget;
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 2056
    iget-object v0, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@2
    .local v0, target:Landroid/view/ViewGroup$TouchTarget;
    :goto_2
    if-eqz v0, :cond_c

    #@4
    .line 2057
    iget-object v1, v0, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@6
    if-ne v1, p1, :cond_9

    #@8
    .line 2061
    .end local v0           #target:Landroid/view/ViewGroup$TouchTarget;
    :goto_8
    return-object v0

    #@9
    .line 2056
    .restart local v0       #target:Landroid/view/ViewGroup$TouchTarget;
    :cond_9
    iget-object v0, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@b
    goto :goto_2

    #@c
    .line 2061
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_8
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 13
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 469
    sget-object v6, Lcom/android/internal/R$styleable;->ViewGroup:[I

    #@4
    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@7
    move-result-object v1

    #@8
    .line 472
    .local v1, a:Landroid/content/res/TypedArray;
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@b
    move-result v0

    #@c
    .line 473
    .local v0, N:I
    const/4 v4, 0x0

    #@d
    .local v4, i:I
    :goto_d
    if-ge v4, v0, :cond_7e

    #@f
    .line 474
    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@12
    move-result v3

    #@13
    .line 475
    .local v3, attr:I
    packed-switch v3, :pswitch_data_82

    #@16
    .line 473
    :cond_16
    :goto_16
    add-int/lit8 v4, v4, 0x1

    #@18
    goto :goto_d

    #@19
    .line 477
    :pswitch_19
    invoke-virtual {v1, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1c
    move-result v6

    #@1d
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    #@20
    goto :goto_16

    #@21
    .line 480
    :pswitch_21
    invoke-virtual {v1, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@24
    move-result v6

    #@25
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    #@28
    goto :goto_16

    #@29
    .line 483
    :pswitch_29
    invoke-virtual {v1, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2c
    move-result v6

    #@2d
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setAnimationCacheEnabled(Z)V

    #@30
    goto :goto_16

    #@31
    .line 486
    :pswitch_31
    const/4 v6, 0x2

    #@32
    invoke-virtual {v1, v3, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@35
    move-result v6

    #@36
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setPersistentDrawingCache(I)V

    #@39
    goto :goto_16

    #@3a
    .line 489
    :pswitch_3a
    invoke-virtual {v1, v3, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3d
    move-result v6

    #@3e
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setAddStatesFromChildren(Z)V

    #@41
    goto :goto_16

    #@42
    .line 492
    :pswitch_42
    invoke-virtual {v1, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@45
    move-result v6

    #@46
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setAlwaysDrawnWithCacheEnabled(Z)V

    #@49
    goto :goto_16

    #@4a
    .line 495
    :pswitch_4a
    const/4 v6, -0x1

    #@4b
    invoke-virtual {v1, v3, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@4e
    move-result v5

    #@4f
    .line 496
    .local v5, id:I
    if-lez v5, :cond_16

    #@51
    .line 497
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@53
    invoke-static {v6, v5}, Landroid/view/animation/AnimationUtils;->loadLayoutAnimation(Landroid/content/Context;I)Landroid/view/animation/LayoutAnimationController;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    #@5a
    goto :goto_16

    #@5b
    .line 501
    .end local v5           #id:I
    :pswitch_5b
    sget-object v6, Landroid/view/ViewGroup;->DESCENDANT_FOCUSABILITY_FLAGS:[I

    #@5d
    invoke-virtual {v1, v3, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@60
    move-result v7

    #@61
    aget v6, v6, v7

    #@63
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    #@66
    goto :goto_16

    #@67
    .line 504
    :pswitch_67
    invoke-virtual {v1, v3, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6a
    move-result v6

    #@6b
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setMotionEventSplittingEnabled(Z)V

    #@6e
    goto :goto_16

    #@6f
    .line 507
    :pswitch_6f
    invoke-virtual {v1, v3, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@72
    move-result v2

    #@73
    .line 508
    .local v2, animateLayoutChanges:Z
    if-eqz v2, :cond_16

    #@75
    .line 509
    new-instance v6, Landroid/animation/LayoutTransition;

    #@77
    invoke-direct {v6}, Landroid/animation/LayoutTransition;-><init>()V

    #@7a
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    #@7d
    goto :goto_16

    #@7e
    .line 515
    .end local v2           #animateLayoutChanges:Z
    .end local v3           #attr:I
    :cond_7e
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@81
    .line 516
    return-void

    #@82
    .line 475
    :pswitch_data_82
    .packed-switch 0x0
        :pswitch_19
        :pswitch_21
        :pswitch_4a
        :pswitch_29
        :pswitch_31
        :pswitch_42
        :pswitch_3a
        :pswitch_5b
        :pswitch_67
        :pswitch_6f
    .end packed-switch
.end method

.method private initViewGroup()V
    .registers 3

    #@0
    .prologue
    const/16 v1, 0x80

    #@2
    .line 447
    invoke-direct {p0}, Landroid/view/ViewGroup;->debugDraw()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_b

    #@8
    .line 448
    invoke-virtual {p0, v1, v1}, Landroid/view/ViewGroup;->setFlags(II)V

    #@b
    .line 450
    :cond_b
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@d
    or-int/lit8 v0, v0, 0x1

    #@f
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@11
    .line 451
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@13
    or-int/lit8 v0, v0, 0x2

    #@15
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@17
    .line 452
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@19
    or-int/lit8 v0, v0, 0x10

    #@1b
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1d
    .line 453
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1f
    or-int/lit8 v0, v0, 0x40

    #@21
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@23
    .line 454
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@25
    or-int/lit16 v0, v0, 0x4000

    #@27
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@29
    .line 456
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@2e
    move-result-object v0

    #@2f
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@31
    const/16 v1, 0xb

    #@33
    if-lt v0, v1, :cond_3c

    #@35
    .line 457
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@37
    const/high16 v1, 0x20

    #@39
    or-int/2addr v0, v1

    #@3a
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@3c
    .line 460
    :cond_3c
    const/high16 v0, 0x2

    #@3e
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    #@41
    .line 462
    const/16 v0, 0xc

    #@43
    new-array v0, v0, [Landroid/view/View;

    #@45
    iput-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@47
    .line 463
    const/4 v0, 0x0

    #@48
    iput v0, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4a
    .line 465
    const/4 v0, 0x2

    #@4b
    iput v0, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@4d
    .line 466
    return-void
.end method

.method private invalidateChildInParentFast(IILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .registers 8
    .parameter "left"
    .parameter "top"
    .parameter "dirty"

    #@0
    .prologue
    const v2, 0x8000

    #@3
    const/4 v3, 0x0

    #@4
    .line 4244
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    and-int/lit8 v0, v0, 0x20

    #@8
    const/16 v1, 0x20

    #@a
    if-eq v0, v1, :cond_11

    #@c
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    and-int/2addr v0, v2

    #@f
    if-ne v0, v2, :cond_4b

    #@11
    .line 4246
    :cond_11
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@13
    sub-int v0, p1, v0

    #@15
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@17
    sub-int v1, p2, v1

    #@19
    invoke-virtual {p3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    #@1c
    .line 4248
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1e
    and-int/lit8 v0, v0, 0x1

    #@20
    if-eqz v0, :cond_32

    #@22
    iget v0, p0, Landroid/view/View;->mRight:I

    #@24
    iget v1, p0, Landroid/view/View;->mLeft:I

    #@26
    sub-int/2addr v0, v1

    #@27
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@29
    iget v2, p0, Landroid/view/View;->mTop:I

    #@2b
    sub-int/2addr v1, v2

    #@2c
    invoke-virtual {p3, v3, v3, v0, v1}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_4b

    #@32
    .line 4251
    :cond_32
    iget v0, p0, Landroid/view/View;->mLayerType:I

    #@34
    if-eqz v0, :cond_3b

    #@36
    .line 4252
    iget-object v0, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@38
    invoke-virtual {v0, p3}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@3b
    .line 4254
    :cond_3b
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getMatrix()Landroid/graphics/Matrix;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    #@42
    move-result v0

    #@43
    if-nez v0, :cond_48

    #@45
    .line 4255
    invoke-virtual {p0, p3}, Landroid/view/ViewGroup;->transformRect(Landroid/graphics/Rect;)V

    #@48
    .line 4258
    :cond_48
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@4a
    .line 4262
    :goto_4a
    return-object v0

    #@4b
    :cond_4b
    const/4 v0, 0x0

    #@4c
    goto :goto_4a
.end method

.method private notifyAnimationListener()V
    .registers 4

    #@0
    .prologue
    .line 2909
    iget v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit16 v1, v1, -0x201

    #@4
    iput v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@6
    .line 2910
    iget v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8
    or-int/lit8 v1, v1, 0x10

    #@a
    iput v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@c
    .line 2912
    iget-object v1, p0, Landroid/view/ViewGroup;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    #@e
    if-eqz v1, :cond_18

    #@10
    .line 2913
    new-instance v0, Landroid/view/ViewGroup$2;

    #@12
    invoke-direct {v0, p0}, Landroid/view/ViewGroup$2;-><init>(Landroid/view/ViewGroup;)V

    #@15
    .line 2918
    .local v0, end:Ljava/lang/Runnable;
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    #@18
    .line 2921
    .end local v0           #end:Ljava/lang/Runnable;
    :cond_18
    iget v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1a
    and-int/lit8 v1, v1, 0x40

    #@1c
    const/16 v2, 0x40

    #@1e
    if-ne v1, v2, :cond_32

    #@20
    .line 2922
    iget v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@22
    const v2, -0x8001

    #@25
    and-int/2addr v1, v2

    #@26
    iput v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@28
    .line 2923
    iget v1, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@2a
    and-int/lit8 v1, v1, 0x1

    #@2c
    if-nez v1, :cond_32

    #@2e
    .line 2924
    const/4 v1, 0x0

    #@2f
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->setChildrenDrawingCacheEnabled(Z)V

    #@32
    .line 2928
    :cond_32
    const/4 v1, 0x1

    #@33
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@36
    .line 2929
    return-void
.end method

.method private static obtainMotionEventNoHistoryOrSelf(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 1742
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getHistorySize()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1745
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    invoke-static {p0}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@a
    move-result-object p0

    #@b
    goto :goto_6
.end method

.method private removeFromArray(I)V
    .registers 7
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3472
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@3
    .line 3473
    .local v0, children:[Landroid/view/View;
    iget-object v2, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@5
    if-eqz v2, :cond_11

    #@7
    iget-object v2, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@9
    aget-object v3, v0, p1

    #@b
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_15

    #@11
    .line 3474
    :cond_11
    aget-object v2, v0, p1

    #@13
    iput-object v4, v2, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@15
    .line 3476
    :cond_15
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@17
    .line 3477
    .local v1, count:I
    add-int/lit8 v2, v1, -0x1

    #@19
    if-ne p1, v2, :cond_2f

    #@1b
    .line 3478
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@1d
    add-int/lit8 v2, v2, -0x1

    #@1f
    iput v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@21
    aput-object v4, v0, v2

    #@23
    .line 3485
    :goto_23
    iget v2, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@25
    if-ne v2, p1, :cond_4b

    #@27
    .line 3486
    const-wide/16 v2, 0x0

    #@29
    iput-wide v2, p0, Landroid/view/ViewGroup;->mLastTouchDownTime:J

    #@2b
    .line 3487
    const/4 v2, -0x1

    #@2c
    iput v2, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@2e
    .line 3491
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 3479
    :cond_2f
    if-ltz p1, :cond_45

    #@31
    if-ge p1, v1, :cond_45

    #@33
    .line 3480
    add-int/lit8 v2, p1, 0x1

    #@35
    sub-int v3, v1, p1

    #@37
    add-int/lit8 v3, v3, -0x1

    #@39
    invoke-static {v0, v2, v0, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3c
    .line 3481
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@3e
    add-int/lit8 v2, v2, -0x1

    #@40
    iput v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@42
    aput-object v4, v0, v2

    #@44
    goto :goto_23

    #@45
    .line 3483
    :cond_45
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@47
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@4a
    throw v2

    #@4b
    .line 3488
    :cond_4b
    iget v2, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@4d
    if-le v2, p1, :cond_2e

    #@4f
    .line 3489
    iget v2, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@51
    add-int/lit8 v2, v2, -0x1

    #@53
    iput v2, p0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@55
    goto :goto_2e
.end method

.method private removeFromArray(II)V
    .registers 9
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3495
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@3
    .line 3496
    .local v0, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 3498
    .local v1, childrenCount:I
    const/4 v4, 0x0

    #@6
    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    #@9
    move-result p1

    #@a
    .line 3499
    add-int v4, p1, p2

    #@c
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    #@f
    move-result v2

    #@10
    .line 3501
    .local v2, end:I
    if-ne p1, v2, :cond_13

    #@12
    .line 3525
    :goto_12
    return-void

    #@13
    .line 3505
    :cond_13
    if-ne v2, v1, :cond_21

    #@15
    .line 3506
    move v3, p1

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v2, :cond_3b

    #@18
    .line 3507
    aget-object v4, v0, v3

    #@1a
    iput-object v5, v4, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1c
    .line 3508
    aput-object v5, v0, v3

    #@1e
    .line 3506
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_16

    #@21
    .line 3511
    .end local v3           #i:I
    :cond_21
    move v3, p1

    #@22
    .restart local v3       #i:I
    :goto_22
    if-ge v3, v2, :cond_2b

    #@24
    .line 3512
    aget-object v4, v0, v3

    #@26
    iput-object v5, v4, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@28
    .line 3511
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_22

    #@2b
    .line 3517
    :cond_2b
    sub-int v4, v1, v2

    #@2d
    invoke-static {v0, v2, v0, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@30
    .line 3519
    sub-int v4, v2, p1

    #@32
    sub-int v3, v1, v4

    #@34
    :goto_34
    if-ge v3, v1, :cond_3b

    #@36
    .line 3520
    aput-object v5, v0, v3

    #@38
    .line 3519
    add-int/lit8 v3, v3, 0x1

    #@3a
    goto :goto_34

    #@3b
    .line 3524
    :cond_3b
    iget v4, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@3d
    sub-int v5, v2, p1

    #@3f
    sub-int/2addr v4, v5

    #@40
    iput v4, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@42
    goto :goto_12
.end method

.method private removePointersFromTouchTargets(I)V
    .registers 7
    .parameter "pointerIdBits"

    #@0
    .prologue
    .line 2079
    const/4 v1, 0x0

    #@1
    .line 2080
    .local v1, predecessor:Landroid/view/ViewGroup$TouchTarget;
    iget-object v2, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@3
    .line 2081
    .local v2, target:Landroid/view/ViewGroup$TouchTarget;
    :goto_3
    if-eqz v2, :cond_26

    #@5
    .line 2082
    iget-object v0, v2, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@7
    .line 2083
    .local v0, next:Landroid/view/ViewGroup$TouchTarget;
    iget v3, v2, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@9
    and-int/2addr v3, p1

    #@a
    if-eqz v3, :cond_23

    #@c
    .line 2084
    iget v3, v2, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@e
    xor-int/lit8 v4, p1, -0x1

    #@10
    and-int/2addr v3, v4

    #@11
    iput v3, v2, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@13
    .line 2085
    iget v3, v2, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@15
    if-nez v3, :cond_23

    #@17
    .line 2086
    if-nez v1, :cond_20

    #@19
    .line 2087
    iput-object v0, p0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@1b
    .line 2091
    :goto_1b
    invoke-virtual {v2}, Landroid/view/ViewGroup$TouchTarget;->recycle()V

    #@1e
    .line 2092
    move-object v2, v0

    #@1f
    .line 2093
    goto :goto_3

    #@20
    .line 2089
    :cond_20
    iput-object v0, v1, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@22
    goto :goto_1b

    #@23
    .line 2096
    :cond_23
    move-object v1, v2

    #@24
    .line 2097
    move-object v2, v0

    #@25
    .line 2098
    goto :goto_3

    #@26
    .line 2099
    .end local v0           #next:Landroid/view/ViewGroup$TouchTarget;
    :cond_26
    return-void
.end method

.method private removeViewInternal(ILandroid/view/View;)V
    .registers 6
    .parameter "index"
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3637
    iget-object v1, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@3
    if-eqz v1, :cond_a

    #@5
    .line 3638
    iget-object v1, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@7
    invoke-virtual {v1, p0, p2}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@a
    .line 3641
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 3642
    .local v0, clearChildFocus:Z
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@d
    if-ne p2, v1, :cond_13

    #@f
    .line 3643
    invoke-virtual {p2}, Landroid/view/View;->unFocus()V

    #@12
    .line 3644
    const/4 v0, 0x1

    #@13
    .line 3647
    :cond_13
    invoke-virtual {p2}, Landroid/view/View;->clearAccessibilityFocus()V

    #@16
    .line 3649
    invoke-direct {p0, p2}, Landroid/view/ViewGroup;->cancelTouchTarget(Landroid/view/View;)V

    #@19
    .line 3650
    invoke-direct {p0, p2}, Landroid/view/ViewGroup;->cancelHoverTarget(Landroid/view/View;)V

    #@1c
    .line 3652
    invoke-virtual {p2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@1f
    move-result-object v1

    #@20
    if-nez v1, :cond_2e

    #@22
    iget-object v1, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@24
    if-eqz v1, :cond_55

    #@26
    iget-object v1, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_55

    #@2e
    .line 3654
    :cond_2e
    invoke-direct {p0, p2}, Landroid/view/ViewGroup;->addDisappearingView(Landroid/view/View;)V

    #@31
    .line 3659
    :cond_31
    :goto_31
    invoke-virtual {p2}, Landroid/view/View;->hasTransientState()Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_3a

    #@37
    .line 3660
    invoke-virtual {p0, p2, v2}, Landroid/view/ViewGroup;->childHasTransientStateChanged(Landroid/view/View;Z)V

    #@3a
    .line 3663
    :cond_3a
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->onViewRemoved(Landroid/view/View;)V

    #@3d
    .line 3665
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->needGlobalAttributesUpdate(Z)V

    #@40
    .line 3667
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->removeFromArray(I)V

    #@43
    .line 3669
    if-eqz v0, :cond_4b

    #@45
    .line 3670
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->clearChildFocus(Landroid/view/View;)V

    #@48
    .line 3671
    invoke-virtual {p0}, Landroid/view/ViewGroup;->ensureInputFocusOnFirstFocusable()V

    #@4b
    .line 3674
    :cond_4b
    invoke-virtual {p2}, Landroid/view/View;->isAccessibilityFocused()Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_54

    #@51
    .line 3675
    invoke-virtual {p2}, Landroid/view/View;->clearAccessibilityFocus()V

    #@54
    .line 3677
    :cond_54
    return-void

    #@55
    .line 3655
    :cond_55
    iget-object v1, p2, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@57
    if-eqz v1, :cond_31

    #@59
    .line 3656
    invoke-virtual {p2}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@5c
    goto :goto_31
.end method

.method private removeViewInternal(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 3629
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    #@3
    move-result v0

    #@4
    .line 3630
    .local v0, index:I
    if-ltz v0, :cond_9

    #@6
    .line 3631
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->removeViewInternal(ILandroid/view/View;)V

    #@9
    .line 3633
    :cond_9
    return-void
.end method

.method private removeViewsInternal(II)V
    .registers 12
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 3713
    iget-object v4, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@3
    .line 3714
    .local v4, focused:Landroid/view/View;
    iget-object v8, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    if-eqz v8, :cond_51

    #@7
    const/4 v2, 0x1

    #@8
    .line 3715
    .local v2, detach:Z
    :goto_8
    const/4 v1, 0x0

    #@9
    .line 3717
    .local v1, clearChildFocus:Landroid/view/View;
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@b
    .line 3718
    .local v0, children:[Landroid/view/View;
    add-int v3, p1, p2

    #@d
    .line 3720
    .local v3, end:I
    move v5, p1

    #@e
    .local v5, i:I
    :goto_e
    if-ge v5, v3, :cond_59

    #@10
    .line 3721
    aget-object v6, v0, v5

    #@12
    .line 3723
    .local v6, view:Landroid/view/View;
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@14
    if-eqz v8, :cond_1b

    #@16
    .line 3724
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@18
    invoke-virtual {v8, p0, v6}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@1b
    .line 3727
    :cond_1b
    if-ne v6, v4, :cond_21

    #@1d
    .line 3728
    invoke-virtual {v6}, Landroid/view/View;->unFocus()V

    #@20
    .line 3729
    move-object v1, v6

    #@21
    .line 3732
    :cond_21
    invoke-virtual {v6}, Landroid/view/View;->clearAccessibilityFocus()V

    #@24
    .line 3734
    invoke-direct {p0, v6}, Landroid/view/ViewGroup;->cancelTouchTarget(Landroid/view/View;)V

    #@27
    .line 3735
    invoke-direct {p0, v6}, Landroid/view/ViewGroup;->cancelHoverTarget(Landroid/view/View;)V

    #@2a
    .line 3737
    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@2d
    move-result-object v8

    #@2e
    if-nez v8, :cond_3c

    #@30
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@32
    if-eqz v8, :cond_53

    #@34
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@39
    move-result v8

    #@3a
    if-eqz v8, :cond_53

    #@3c
    .line 3739
    :cond_3c
    invoke-direct {p0, v6}, Landroid/view/ViewGroup;->addDisappearingView(Landroid/view/View;)V

    #@3f
    .line 3744
    :cond_3f
    :goto_3f
    invoke-virtual {v6}, Landroid/view/View;->hasTransientState()Z

    #@42
    move-result v8

    #@43
    if-eqz v8, :cond_48

    #@45
    .line 3745
    invoke-virtual {p0, v6, v7}, Landroid/view/ViewGroup;->childHasTransientStateChanged(Landroid/view/View;Z)V

    #@48
    .line 3748
    :cond_48
    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->needGlobalAttributesUpdate(Z)V

    #@4b
    .line 3750
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->onViewRemoved(Landroid/view/View;)V

    #@4e
    .line 3720
    add-int/lit8 v5, v5, 0x1

    #@50
    goto :goto_e

    #@51
    .end local v0           #children:[Landroid/view/View;
    .end local v1           #clearChildFocus:Landroid/view/View;
    .end local v2           #detach:Z
    .end local v3           #end:I
    .end local v5           #i:I
    .end local v6           #view:Landroid/view/View;
    :cond_51
    move v2, v7

    #@52
    .line 3714
    goto :goto_8

    #@53
    .line 3740
    .restart local v0       #children:[Landroid/view/View;
    .restart local v1       #clearChildFocus:Landroid/view/View;
    .restart local v2       #detach:Z
    .restart local v3       #end:I
    .restart local v5       #i:I
    .restart local v6       #view:Landroid/view/View;
    :cond_53
    if-eqz v2, :cond_3f

    #@55
    .line 3741
    invoke-virtual {v6}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@58
    goto :goto_3f

    #@59
    .line 3753
    .end local v6           #view:Landroid/view/View;
    :cond_59
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->removeFromArray(II)V

    #@5c
    .line 3755
    if-eqz v1, :cond_64

    #@5e
    .line 3756
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->clearChildFocus(Landroid/view/View;)V

    #@61
    .line 3757
    invoke-virtual {p0}, Landroid/view/ViewGroup;->ensureInputFocusOnFirstFocusable()V

    #@64
    .line 3759
    :cond_64
    return-void
.end method

.method private static resetCancelNextUpFlag(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 2003
    if-eqz p0, :cond_13

    #@2
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@4
    const/high16 v1, 0x400

    #@6
    and-int/2addr v0, v1

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 2004
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@b
    const v1, -0x4000001

    #@e
    and-int/2addr v0, v1

    #@f
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@11
    .line 2005
    const/4 v0, 0x1

    #@12
    .line 2007
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method private resetTouchState()V
    .registers 3

    #@0
    .prologue
    .line 1993
    invoke-direct {p0}, Landroid/view/ViewGroup;->clearTouchTargets()V

    #@3
    .line 1994
    invoke-static {p0}, Landroid/view/ViewGroup;->resetCancelNextUpFlag(Landroid/view/View;)Z

    #@6
    .line 1995
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8
    const v1, -0x80001

    #@b
    and-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@e
    .line 1996
    return-void
.end method

.method private setBooleanFlag(IZ)V
    .registers 5
    .parameter "flag"
    .parameter "value"

    #@0
    .prologue
    .line 4609
    if-eqz p2, :cond_8

    #@2
    .line 4610
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4
    or-int/2addr v0, p1

    #@5
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@7
    .line 4614
    :goto_7
    return-void

    #@8
    .line 4612
    :cond_8
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@a
    xor-int/lit8 v1, p1, -0x1

    #@c
    and-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@f
    goto :goto_7
.end method


# virtual methods
.method public addChildrenForAccessibility(Ljava/util/ArrayList;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1671
    .local p1, childrenForAccessibility:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v4, 0x1

    #@1
    invoke-static {p0, v4}, Landroid/view/ViewGroup$ChildListForAccessibility;->obtain(Landroid/view/ViewGroup;Z)Landroid/view/ViewGroup$ChildListForAccessibility;

    #@4
    move-result-object v1

    #@5
    .line 1673
    .local v1, children:Landroid/view/ViewGroup$ChildListForAccessibility;
    :try_start_5
    invoke-virtual {v1}, Landroid/view/ViewGroup$ChildListForAccessibility;->getChildCount()I

    #@8
    move-result v2

    #@9
    .line 1674
    .local v2, childrenCount:I
    const/4 v3, 0x0

    #@a
    .local v3, i:I
    :goto_a
    if-ge v3, v2, :cond_2b

    #@c
    .line 1675
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup$ChildListForAccessibility;->getChildAt(I)Landroid/view/View;

    #@f
    move-result-object v0

    #@10
    .line 1676
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@12
    and-int/lit8 v4, v4, 0xc

    #@14
    if-nez v4, :cond_1f

    #@16
    .line 1677
    invoke-virtual {v0}, Landroid/view/View;->includeForAccessibility()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_22

    #@1c
    .line 1678
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 1674
    :cond_1f
    :goto_1f
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_a

    #@22
    .line 1680
    :cond_22
    invoke-virtual {v0, p1}, Landroid/view/View;->addChildrenForAccessibility(Ljava/util/ArrayList;)V
    :try_end_25
    .catchall {:try_start_5 .. :try_end_25} :catchall_26

    #@25
    goto :goto_1f

    #@26
    .line 1685
    .end local v0           #child:Landroid/view/View;
    .end local v2           #childrenCount:I
    .end local v3           #i:I
    :catchall_26
    move-exception v4

    #@27
    invoke-virtual {v1}, Landroid/view/ViewGroup$ChildListForAccessibility;->recycle()V

    #@2a
    throw v4

    #@2b
    .restart local v2       #childrenCount:I
    .restart local v3       #i:I
    :cond_2b
    invoke-virtual {v1}, Landroid/view/ViewGroup$ChildListForAccessibility;->recycle()V

    #@2e
    .line 1687
    return-void
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 11
    .parameter
    .parameter "direction"
    .parameter "focusableMode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    #@0
    .prologue
    .line 897
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v4

    #@4
    .line 899
    .local v4, focusableCount:I
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@7
    move-result v3

    #@8
    .line 901
    .local v3, descendantFocusability:I
    const/high16 v6, 0x6

    #@a
    if-eq v3, v6, :cond_21

    #@c
    .line 902
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@e
    .line 903
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@10
    .line 905
    .local v1, children:[Landroid/view/View;
    const/4 v5, 0x0

    #@11
    .local v5, i:I
    :goto_11
    if-ge v5, v2, :cond_21

    #@13
    .line 906
    aget-object v0, v1, v5

    #@15
    .line 907
    .local v0, child:Landroid/view/View;
    iget v6, v0, Landroid/view/View;->mViewFlags:I

    #@17
    and-int/lit8 v6, v6, 0xc

    #@19
    if-nez v6, :cond_1e

    #@1b
    .line 908
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@1e
    .line 905
    :cond_1e
    add-int/lit8 v5, v5, 0x1

    #@20
    goto :goto_11

    #@21
    .line 917
    .end local v0           #child:Landroid/view/View;
    .end local v1           #children:[Landroid/view/View;
    .end local v2           #count:I
    .end local v5           #i:I
    :cond_21
    const/high16 v6, 0x4

    #@23
    if-ne v3, v6, :cond_2b

    #@25
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@28
    move-result v6

    #@29
    if-ne v4, v6, :cond_2e

    #@2b
    .line 920
    :cond_2b
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@2e
    .line 922
    :cond_2e
    return-void
.end method

.method public addStatesFromChildren()Z
    .registers 2

    #@0
    .prologue
    .line 5254
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit16 v0, v0, 0x2000

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 974
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-super {p0, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    #@3
    .line 976
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 977
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 979
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v2, :cond_18

    #@a
    .line 980
    aget-object v0, v1, v3

    #@c
    .line 981
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@e
    and-int/lit8 v4, v4, 0xc

    #@10
    if-nez v4, :cond_15

    #@12
    .line 982
    invoke-virtual {v0, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    #@15
    .line 979
    :cond_15
    add-int/lit8 v3, v3, 0x1

    #@17
    goto :goto_8

    #@18
    .line 985
    .end local v0           #child:Landroid/view/View;
    :cond_18
    return-void
.end method

.method public addView(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 3171
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    #@4
    .line 3172
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .registers 6
    .parameter "child"
    .parameter "index"

    #@0
    .prologue
    .line 3188
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 3189
    .local v0, params:Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_14

    #@6
    .line 3190
    invoke-virtual {p0}, Landroid/view/ViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@9
    move-result-object v0

    #@a
    .line 3191
    if-nez v0, :cond_14

    #@c
    .line 3192
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v2, "generateDefaultLayoutParams() cannot return null"

    #@10
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 3195
    :cond_14
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@17
    .line 3196
    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .registers 6
    .parameter "child"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 3209
    invoke-virtual {p0}, Landroid/view/ViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 3210
    .local v0, params:Landroid/view/ViewGroup$LayoutParams;
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@6
    .line 3211
    iput p3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@8
    .line 3212
    const/4 v1, -0x1

    #@9
    invoke-virtual {p0, p1, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@c
    .line 3213
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 3248
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@3
    .line 3249
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@7
    .line 3250
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/view/ViewGroup;->addViewInner(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    #@b
    .line 3251
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "child"
    .parameter "params"

    #@0
    .prologue
    .line 3226
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@4
    .line 3227
    return-void
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z
    .registers 5
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 3336
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/view/ViewGroup;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .registers 7
    .parameter "child"
    .parameter "index"
    .parameter "params"
    .parameter "preventRequestLayout"

    #@0
    .prologue
    .line 3354
    const/4 v0, 0x0

    #@1
    iput-object v0, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@3
    .line 3355
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->addViewInner(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    #@6
    .line 3356
    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@8
    const v1, -0x600001

    #@b
    and-int/2addr v0, v1

    #@c
    or-int/lit8 v0, v0, 0x20

    #@e
    iput v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@10
    .line 3357
    const/4 v0, 0x1

    #@11
    return v0
.end method

.method protected attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V
    .registers 6
    .parameter "child"
    .parameter "params"
    .parameter "index"
    .parameter "count"

    #@0
    .prologue
    .line 3544
    iget-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@2
    .line 3546
    .local v0, animationParams:Landroid/view/animation/LayoutAnimationController$AnimationParameters;
    if-nez v0, :cond_b

    #@4
    .line 3547
    new-instance v0, Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@6
    .end local v0           #animationParams:Landroid/view/animation/LayoutAnimationController$AnimationParameters;
    invoke-direct {v0}, Landroid/view/animation/LayoutAnimationController$AnimationParameters;-><init>()V

    #@9
    .line 3548
    .restart local v0       #animationParams:Landroid/view/animation/LayoutAnimationController$AnimationParameters;
    iput-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@b
    .line 3551
    :cond_b
    iput p4, v0, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->count:I

    #@d
    .line 3552
    iput p3, v0, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->index:I

    #@f
    .line 3553
    return-void
.end method

.method protected attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 7
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    const/high16 v2, -0x8000

    #@2
    .line 3915
    iput-object p3, p1, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@4
    .line 3917
    if-gez p2, :cond_8

    #@6
    .line 3918
    iget p2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@8
    .line 3921
    :cond_8
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->addInArray(Landroid/view/View;I)V

    #@b
    .line 3923
    iput-object p0, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@d
    .line 3924
    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@f
    const v1, -0x600001

    #@12
    and-int/2addr v0, v1

    #@13
    const v1, -0x8001

    #@16
    and-int/2addr v0, v1

    #@17
    or-int/lit8 v0, v0, 0x20

    #@19
    or-int/2addr v0, v2

    #@1a
    iput v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@1c
    .line 3927
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@1e
    or-int/2addr v0, v2

    #@1f
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@21
    .line 3929
    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_2e

    #@27
    .line 3930
    invoke-virtual {p1}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@2e
    .line 3932
    :cond_2e
    return-void
.end method

.method public bringChildToFront(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 1116
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    #@3
    move-result v0

    #@4
    .line 1117
    .local v0, index:I
    if-ltz v0, :cond_10

    #@6
    .line 1118
    invoke-direct {p0, v0}, Landroid/view/ViewGroup;->removeFromArray(I)V

    #@9
    .line 1119
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@b
    invoke-direct {p0, p1, v1}, Landroid/view/ViewGroup;->addInArray(Landroid/view/View;I)V

    #@e
    .line 1120
    iput-object p0, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@10
    .line 1122
    :cond_10
    return-void
.end method

.method protected canAnimate()Z
    .registers 2

    #@0
    .prologue
    .line 4434
    iget-object v0, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 3270
    if-eqz p1, :cond_4

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method public childAccessibilityStateChanged(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 1694
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1695
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->childAccessibilityStateChanged(Landroid/view/View;)V

    #@9
    .line 1697
    :cond_9
    return-void
.end method

.method public childDrawableStateChanged(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 5262
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit16 v0, v0, 0x2000

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 5263
    invoke-virtual {p0}, Landroid/view/ViewGroup;->refreshDrawableState()V

    #@9
    .line 5265
    :cond_9
    return-void
.end method

.method public childHasTransientStateChanged(Landroid/view/View;Z)V
    .registers 9
    .parameter "child"
    .parameter "childHasTransientState"

    #@0
    .prologue
    .line 739
    invoke-virtual {p0}, Landroid/view/ViewGroup;->hasTransientState()Z

    #@3
    move-result v2

    #@4
    .line 740
    .local v2, oldHasTransientState:Z
    if-eqz p2, :cond_1c

    #@6
    .line 741
    iget v3, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@8
    add-int/lit8 v3, v3, 0x1

    #@a
    iput v3, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@c
    .line 746
    :goto_c
    invoke-virtual {p0}, Landroid/view/ViewGroup;->hasTransientState()Z

    #@f
    move-result v1

    #@10
    .line 747
    .local v1, newHasTransientState:Z
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@12
    if-eqz v3, :cond_1b

    #@14
    if-eq v2, v1, :cond_1b

    #@16
    .line 749
    :try_start_16
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@18
    invoke-interface {v3, p0, v1}, Landroid/view/ViewParent;->childHasTransientStateChanged(Landroid/view/View;Z)V
    :try_end_1b
    .catch Ljava/lang/AbstractMethodError; {:try_start_16 .. :try_end_1b} :catch_23

    #@1b
    .line 755
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 743
    .end local v1           #newHasTransientState:Z
    :cond_1c
    iget v3, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@1e
    add-int/lit8 v3, v3, -0x1

    #@20
    iput v3, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@22
    goto :goto_c

    #@23
    .line 750
    .restart local v1       #newHasTransientState:Z
    :catch_23
    move-exception v0

    #@24
    .line 751
    .local v0, e:Ljava/lang/AbstractMethodError;
    const-string v3, "ViewGroup"

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    iget-object v5, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2d
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, " does not fully implement ViewParent"

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_1b
.end method

.method protected cleanupLayoutState(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 3366
    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit16 v0, v0, -0x1001

    #@4
    iput v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@6
    .line 3367
    return-void
.end method

.method public clearChildFocus(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 782
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@3
    .line 783
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 784
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@9
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->clearChildFocus(Landroid/view/View;)V

    #@c
    .line 786
    :cond_c
    return-void
.end method

.method public clearDisappearingChildren()V
    .registers 2

    #@0
    .prologue
    .line 4961
    iget-object v0, p0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 4962
    iget-object v0, p0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@9
    .line 4963
    invoke-virtual {p0}, Landroid/view/ViewGroup;->invalidate()V

    #@c
    .line 4965
    :cond_c
    return-void
.end method

.method public clearFocus()V
    .registers 3

    #@0
    .prologue
    .line 796
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@2
    if-nez v1, :cond_8

    #@4
    .line 797
    invoke-super {p0}, Landroid/view/View;->clearFocus()V

    #@7
    .line 803
    :goto_7
    return-void

    #@8
    .line 799
    :cond_8
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@a
    .line 800
    .local v0, focused:Landroid/view/View;
    const/4 v1, 0x0

    #@b
    iput-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@d
    .line 801
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    #@10
    goto :goto_7
.end method

.method createSnapshot(Landroid/graphics/Bitmap$Config;IZ)Landroid/graphics/Bitmap;
    .registers 11
    .parameter "quality"
    .parameter "backgroundColor"
    .parameter "skipChildren"

    #@0
    .prologue
    .line 2700
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    .line 2701
    .local v2, count:I
    const/4 v4, 0x0

    #@3
    .line 2703
    .local v4, visibilities:[I
    if-eqz p3, :cond_1f

    #@5
    .line 2704
    new-array v4, v2, [I

    #@7
    .line 2705
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v2, :cond_1f

    #@a
    .line 2706
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v1

    #@e
    .line 2707
    .local v1, child:Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    #@11
    move-result v5

    #@12
    aput v5, v4, v3

    #@14
    .line 2708
    aget v5, v4, v3

    #@16
    if-nez v5, :cond_1c

    #@18
    .line 2709
    const/4 v5, 0x4

    #@19
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    #@1c
    .line 2705
    :cond_1c
    add-int/lit8 v3, v3, 0x1

    #@1e
    goto :goto_8

    #@1f
    .line 2714
    .end local v1           #child:Landroid/view/View;
    .end local v3           #i:I
    :cond_1f
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->createSnapshot(Landroid/graphics/Bitmap$Config;IZ)Landroid/graphics/Bitmap;

    #@22
    move-result-object v0

    #@23
    .line 2716
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz p3, :cond_34

    #@25
    .line 2717
    const/4 v3, 0x0

    #@26
    .restart local v3       #i:I
    :goto_26
    if-ge v3, v2, :cond_34

    #@28
    .line 2718
    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@2b
    move-result-object v5

    #@2c
    aget v6, v4, v3

    #@2e
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    #@31
    .line 2717
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_26

    #@34
    .line 2722
    .end local v3           #i:I
    :cond_34
    return-object v0
.end method

.method protected debug(I)V
    .registers 8
    .parameter "depth"

    #@0
    .prologue
    .line 4730
    invoke-super {p0, p1}, Landroid/view/View;->debug(I)V

    #@3
    .line 4733
    iget-object v4, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@5
    if-eqz v4, :cond_24

    #@7
    .line 4734
    invoke-static {p1}, Landroid/view/ViewGroup;->debugIndent(I)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 4735
    .local v3, output:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    const-string/jumbo v5, "mFocused"

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    .line 4736
    const-string v4, "View"

    #@21
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 4738
    .end local v3           #output:Ljava/lang/String;
    :cond_24
    iget v4, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@26
    if-eqz v4, :cond_45

    #@28
    .line 4739
    invoke-static {p1}, Landroid/view/ViewGroup;->debugIndent(I)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    .line 4740
    .restart local v3       #output:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string/jumbo v5, "{"

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    .line 4741
    const-string v4, "View"

    #@42
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 4743
    .end local v3           #output:Ljava/lang/String;
    :cond_45
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@47
    .line 4744
    .local v1, count:I
    const/4 v2, 0x0

    #@48
    .local v2, i:I
    :goto_48
    if-ge v2, v1, :cond_56

    #@4a
    .line 4745
    iget-object v4, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@4c
    aget-object v0, v4, v2

    #@4e
    .line 4746
    .local v0, child:Landroid/view/View;
    add-int/lit8 v4, p1, 0x1

    #@50
    invoke-virtual {v0, v4}, Landroid/view/View;->debug(I)V

    #@53
    .line 4744
    add-int/lit8 v2, v2, 0x1

    #@55
    goto :goto_48

    #@56
    .line 4749
    .end local v0           #child:Landroid/view/View;
    :cond_56
    iget v4, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@58
    if-eqz v4, :cond_77

    #@5a
    .line 4750
    invoke-static {p1}, Landroid/view/ViewGroup;->debugIndent(I)Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    .line 4751
    .restart local v3       #output:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    const-string/jumbo v5, "}"

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    .line 4752
    const-string v4, "View"

    #@74
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 4754
    .end local v3           #output:Ljava/lang/String;
    :cond_77
    return-void
.end method

.method protected detachAllViewsFromParent()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4014
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@3
    .line 4015
    .local v1, count:I
    if-gtz v1, :cond_6

    #@5
    .line 4026
    :cond_5
    return-void

    #@6
    .line 4019
    :cond_6
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@8
    .line 4020
    .local v0, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@9
    iput v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@b
    .line 4022
    add-int/lit8 v2, v1, -0x1

    #@d
    .local v2, i:I
    :goto_d
    if-ltz v2, :cond_5

    #@f
    .line 4023
    aget-object v3, v0, v2

    #@11
    iput-object v4, v3, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@13
    .line 4024
    aput-object v4, v0, v2

    #@15
    .line 4022
    add-int/lit8 v2, v2, -0x1

    #@17
    goto :goto_d
.end method

.method protected detachViewFromParent(I)V
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 3973
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->removeFromArray(I)V

    #@3
    .line 3974
    return-void
.end method

.method protected detachViewFromParent(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 3952
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/view/ViewGroup;->removeFromArray(I)V

    #@7
    .line 3953
    return-void
.end method

.method protected detachViewsFromParent(II)V
    .registers 3
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 3995
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->removeFromArray(II)V

    #@3
    .line 3996
    return-void
.end method

.method dispatchAttachedToWindow(Landroid/view/View$AttachInfo;I)V
    .registers 9
    .parameter "info"
    .parameter "visibility"

    #@0
    .prologue
    .line 2454
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    const/high16 v5, 0x40

    #@4
    or-int/2addr v4, v5

    #@5
    iput v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@7
    .line 2455
    invoke-super {p0, p1, p2}, Landroid/view/View;->dispatchAttachedToWindow(Landroid/view/View$AttachInfo;I)V

    #@a
    .line 2456
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@c
    const v5, -0x400001

    #@f
    and-int/2addr v4, v5

    #@10
    iput v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@12
    .line 2458
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@14
    .line 2459
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@16
    .line 2460
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@17
    .local v3, i:I
    :goto_17
    if-ge v3, v2, :cond_26

    #@19
    .line 2461
    aget-object v0, v1, v3

    #@1b
    .line 2462
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@1d
    and-int/lit8 v4, v4, 0xc

    #@1f
    or-int/2addr v4, p2

    #@20
    invoke-virtual {v0, p1, v4}, Landroid/view/View;->dispatchAttachedToWindow(Landroid/view/View$AttachInfo;I)V

    #@23
    .line 2460
    add-int/lit8 v3, v3, 0x1

    #@25
    goto :goto_17

    #@26
    .line 2465
    .end local v0           #child:Landroid/view/View;
    :cond_26
    return-void
.end method

.method dispatchCollectViewAttributes(Landroid/view/View$AttachInfo;I)V
    .registers 8
    .parameter "attachInfo"
    .parameter "visibility"

    #@0
    .prologue
    .line 1100
    and-int/lit8 v4, p2, 0xc

    #@2
    if-nez v4, :cond_1b

    #@4
    .line 1101
    invoke-super {p0, p1, p2}, Landroid/view/View;->dispatchCollectViewAttributes(Landroid/view/View$AttachInfo;I)V

    #@7
    .line 1102
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@9
    .line 1103
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@b
    .line 1104
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@c
    .local v3, i:I
    :goto_c
    if-ge v3, v2, :cond_1b

    #@e
    .line 1105
    aget-object v0, v1, v3

    #@10
    .line 1106
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@12
    and-int/lit8 v4, v4, 0xc

    #@14
    or-int/2addr v4, p2

    #@15
    invoke-virtual {v0, p1, v4}, Landroid/view/View;->dispatchCollectViewAttributes(Landroid/view/View$AttachInfo;I)V

    #@18
    .line 1104
    add-int/lit8 v3, v3, 0x1

    #@1a
    goto :goto_c

    #@1b
    .line 1110
    .end local v0           #child:Landroid/view/View;
    .end local v1           #children:[Landroid/view/View;
    .end local v2           #count:I
    .end local v3           #i:I
    :cond_1b
    return-void
.end method

.method public dispatchConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 6
    .parameter "newConfig"

    #@0
    .prologue
    .line 1079
    invoke-super {p0, p1}, Landroid/view/View;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 1080
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 1081
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 1082
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_16

    #@a
    .line 1083
    aget-object v3, v0, v2

    #@c
    if-eqz v3, :cond_13

    #@e
    .line 1084
    aget-object v3, v0, v2

    #@10
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    #@13
    .line 1082
    :cond_13
    add-int/lit8 v2, v2, 0x1

    #@15
    goto :goto_8

    #@16
    .line 1086
    :cond_16
    return-void
.end method

.method dispatchDetachedFromWindow()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2552
    invoke-direct {p0, v4}, Landroid/view/ViewGroup;->cancelAndClearTouchTargets(Landroid/view/MotionEvent;)V

    #@4
    .line 2555
    invoke-direct {p0}, Landroid/view/ViewGroup;->exitHoverTargets()V

    #@7
    .line 2558
    const/4 v3, 0x0

    #@8
    iput-boolean v3, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@a
    .line 2561
    iput-object v4, p0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@c
    .line 2562
    iget-object v3, p0, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@e
    if-eqz v3, :cond_17

    #@10
    .line 2563
    iget-object v3, p0, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@12
    invoke-virtual {v3}, Landroid/view/DragEvent;->recycle()V

    #@15
    .line 2564
    iput-object v4, p0, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@17
    .line 2567
    :cond_17
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@19
    .line 2568
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@1b
    .line 2569
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@1c
    .local v2, i:I
    :goto_1c
    if-ge v2, v1, :cond_26

    #@1e
    .line 2570
    aget-object v3, v0, v2

    #@20
    invoke-virtual {v3}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@23
    .line 2569
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_1c

    #@26
    .line 2572
    :cond_26
    invoke-super {p0}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@29
    .line 2573
    return-void
.end method

.method public dispatchDisplayHint(I)V
    .registers 6
    .parameter "hint"

    #@0
    .prologue
    .line 1005
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDisplayHint(I)V

    #@3
    .line 1006
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 1007
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 1008
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 1009
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchDisplayHint(I)V

    #@f
    .line 1008
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 1011
    :cond_12
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .registers 22
    .parameter "event"

    #@0
    .prologue
    .line 1130
    const/4 v11, 0x0

    #@1
    .line 1131
    .local v11, retval:Z
    move-object/from16 v0, p1

    #@3
    iget v14, v0, Landroid/view/DragEvent;->mX:F

    #@5
    .line 1132
    .local v14, tx:F
    move-object/from16 v0, p1

    #@7
    iget v15, v0, Landroid/view/DragEvent;->mY:F

    #@9
    .line 1134
    .local v15, ty:F
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@c
    move-result-object v12

    #@d
    .line 1137
    .local v12, root:Landroid/view/ViewRootImpl;
    move-object/from16 v0, p1

    #@f
    iget v0, v0, Landroid/view/DragEvent;->mAction:I

    #@11
    move/from16 v17, v0

    #@13
    packed-switch v17, :pswitch_data_224

    #@16
    .line 1281
    :cond_16
    :goto_16
    :pswitch_16
    if-nez v11, :cond_1c

    #@18
    .line 1283
    invoke-super/range {p0 .. p1}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@1b
    move-result v11

    #@1c
    .line 1285
    :cond_1c
    return v11

    #@1d
    .line 1140
    :pswitch_1d
    const/16 v17, 0x0

    #@1f
    move-object/from16 v0, v17

    #@21
    move-object/from16 v1, p0

    #@23
    iput-object v0, v1, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@25
    .line 1143
    invoke-static/range {p1 .. p1}, Landroid/view/DragEvent;->obtain(Landroid/view/DragEvent;)Landroid/view/DragEvent;

    #@28
    move-result-object v17

    #@29
    move-object/from16 v0, v17

    #@2b
    move-object/from16 v1, p0

    #@2d
    iput-object v0, v1, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@2f
    .line 1144
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@33
    move-object/from16 v17, v0

    #@35
    if-nez v17, :cond_7e

    #@37
    .line 1145
    new-instance v17, Ljava/util/HashSet;

    #@39
    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    #@3c
    move-object/from16 v0, v17

    #@3e
    move-object/from16 v1, p0

    #@40
    iput-object v0, v1, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@42
    .line 1151
    :goto_42
    const/16 v17, 0x0

    #@44
    move/from16 v0, v17

    #@46
    move-object/from16 v1, p0

    #@48
    iput-boolean v0, v1, Landroid/view/ViewGroup;->mChildAcceptsDrag:Z

    #@4a
    .line 1152
    move-object/from16 v0, p0

    #@4c
    iget v7, v0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4e
    .line 1153
    .local v7, count:I
    move-object/from16 v0, p0

    #@50
    iget-object v6, v0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@52
    .line 1154
    .local v6, children:[Landroid/view/View;
    const/4 v9, 0x0

    #@53
    .local v9, i:I
    :goto_53
    if-ge v9, v7, :cond_88

    #@55
    .line 1155
    aget-object v5, v6, v9

    #@57
    .line 1156
    .local v5, child:Landroid/view/View;
    iget v0, v5, Landroid/view/View;->mPrivateFlags2:I

    #@59
    move/from16 v17, v0

    #@5b
    and-int/lit8 v17, v17, -0x4

    #@5d
    move/from16 v0, v17

    #@5f
    iput v0, v5, Landroid/view/View;->mPrivateFlags2:I

    #@61
    .line 1157
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    #@64
    move-result v17

    #@65
    if-nez v17, :cond_7b

    #@67
    .line 1158
    aget-object v17, v6, v9

    #@69
    move-object/from16 v0, p0

    #@6b
    move-object/from16 v1, v17

    #@6d
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->notifyChildOfDrag(Landroid/view/View;)Z

    #@70
    move-result v8

    #@71
    .line 1159
    .local v8, handled:Z
    if-eqz v8, :cond_7b

    #@73
    .line 1160
    const/16 v17, 0x1

    #@75
    move/from16 v0, v17

    #@77
    move-object/from16 v1, p0

    #@79
    iput-boolean v0, v1, Landroid/view/ViewGroup;->mChildAcceptsDrag:Z

    #@7b
    .line 1154
    .end local v8           #handled:Z
    :cond_7b
    add-int/lit8 v9, v9, 0x1

    #@7d
    goto :goto_53

    #@7e
    .line 1147
    .end local v5           #child:Landroid/view/View;
    .end local v6           #children:[Landroid/view/View;
    .end local v7           #count:I
    .end local v9           #i:I
    :cond_7e
    move-object/from16 v0, p0

    #@80
    iget-object v0, v0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@82
    move-object/from16 v17, v0

    #@84
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->clear()V

    #@87
    goto :goto_42

    #@88
    .line 1166
    .restart local v6       #children:[Landroid/view/View;
    .restart local v7       #count:I
    .restart local v9       #i:I
    :cond_88
    move-object/from16 v0, p0

    #@8a
    iget-boolean v0, v0, Landroid/view/ViewGroup;->mChildAcceptsDrag:Z

    #@8c
    move/from16 v17, v0

    #@8e
    if-eqz v17, :cond_16

    #@90
    .line 1167
    const/4 v11, 0x1

    #@91
    goto :goto_16

    #@92
    .line 1173
    .end local v6           #children:[Landroid/view/View;
    .end local v7           #count:I
    .end local v9           #i:I
    :pswitch_92
    move-object/from16 v0, p0

    #@94
    iget-object v0, v0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@96
    move-object/from16 v17, v0

    #@98
    if-eqz v17, :cond_dd

    #@9a
    .line 1174
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@9e
    move-object/from16 v17, v0

    #@a0
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@a3
    move-result-object v10

    #@a4
    .local v10, i$:Ljava/util/Iterator;
    :goto_a4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@a7
    move-result v17

    #@a8
    if-eqz v17, :cond_c3

    #@aa
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ad
    move-result-object v5

    #@ae
    check-cast v5, Landroid/view/View;

    #@b0
    .line 1176
    .restart local v5       #child:Landroid/view/View;
    move-object/from16 v0, p1

    #@b2
    invoke-virtual {v5, v0}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@b5
    .line 1177
    iget v0, v5, Landroid/view/View;->mPrivateFlags2:I

    #@b7
    move/from16 v17, v0

    #@b9
    and-int/lit8 v17, v17, -0x4

    #@bb
    move/from16 v0, v17

    #@bd
    iput v0, v5, Landroid/view/View;->mPrivateFlags2:I

    #@bf
    .line 1178
    invoke-virtual {v5}, Landroid/view/View;->refreshDrawableState()V

    #@c2
    goto :goto_a4

    #@c3
    .line 1181
    .end local v5           #child:Landroid/view/View;
    :cond_c3
    move-object/from16 v0, p0

    #@c5
    iget-object v0, v0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@c7
    move-object/from16 v17, v0

    #@c9
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->clear()V

    #@cc
    .line 1182
    move-object/from16 v0, p0

    #@ce
    iget-object v0, v0, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@d0
    move-object/from16 v17, v0

    #@d2
    invoke-virtual/range {v17 .. v17}, Landroid/view/DragEvent;->recycle()V

    #@d5
    .line 1183
    const/16 v17, 0x0

    #@d7
    move-object/from16 v0, v17

    #@d9
    move-object/from16 v1, p0

    #@db
    iput-object v0, v1, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@dd
    .line 1188
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_dd
    move-object/from16 v0, p0

    #@df
    iget-boolean v0, v0, Landroid/view/ViewGroup;->mChildAcceptsDrag:Z

    #@e1
    move/from16 v17, v0

    #@e3
    if-eqz v17, :cond_16

    #@e5
    .line 1189
    const/4 v11, 0x1

    #@e6
    goto/16 :goto_16

    #@e8
    .line 1195
    :pswitch_e8
    move-object/from16 v0, p1

    #@ea
    iget v0, v0, Landroid/view/DragEvent;->mX:F

    #@ec
    move/from16 v17, v0

    #@ee
    move-object/from16 v0, p1

    #@f0
    iget v0, v0, Landroid/view/DragEvent;->mY:F

    #@f2
    move/from16 v18, v0

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@f8
    move-object/from16 v19, v0

    #@fa
    move-object/from16 v0, p0

    #@fc
    move/from16 v1, v17

    #@fe
    move/from16 v2, v18

    #@100
    move-object/from16 v3, v19

    #@102
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->findFrontmostDroppableChildAt(FFLandroid/graphics/PointF;)Landroid/view/View;

    #@105
    move-result-object v13

    #@106
    .line 1203
    .local v13, target:Landroid/view/View;
    move-object/from16 v0, p0

    #@108
    iget-object v0, v0, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@10a
    move-object/from16 v17, v0

    #@10c
    move-object/from16 v0, v17

    #@10e
    if-eq v0, v13, :cond_169

    #@110
    .line 1204
    invoke-virtual {v12, v13}, Landroid/view/ViewRootImpl;->setDragFocus(Landroid/view/View;)V

    #@113
    .line 1206
    move-object/from16 v0, p1

    #@115
    iget v4, v0, Landroid/view/DragEvent;->mAction:I

    #@117
    .line 1208
    .local v4, action:I
    move-object/from16 v0, p0

    #@119
    iget-object v0, v0, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@11b
    move-object/from16 v17, v0

    #@11d
    if-eqz v17, :cond_145

    #@11f
    .line 1209
    move-object/from16 v0, p0

    #@121
    iget-object v0, v0, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@123
    move-object/from16 v16, v0

    #@125
    .line 1210
    .local v16, view:Landroid/view/View;
    const/16 v17, 0x6

    #@127
    move/from16 v0, v17

    #@129
    move-object/from16 v1, p1

    #@12b
    iput v0, v1, Landroid/view/DragEvent;->mAction:I

    #@12d
    .line 1211
    move-object/from16 v0, v16

    #@12f
    move-object/from16 v1, p1

    #@131
    invoke-virtual {v0, v1}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@134
    .line 1212
    move-object/from16 v0, v16

    #@136
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@138
    move/from16 v17, v0

    #@13a
    and-int/lit8 v17, v17, -0x3

    #@13c
    move/from16 v0, v17

    #@13e
    move-object/from16 v1, v16

    #@140
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@142
    .line 1213
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->refreshDrawableState()V

    #@145
    .line 1215
    .end local v16           #view:Landroid/view/View;
    :cond_145
    move-object/from16 v0, p0

    #@147
    iput-object v13, v0, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@149
    .line 1218
    if-eqz v13, :cond_165

    #@14b
    .line 1219
    const/16 v17, 0x5

    #@14d
    move/from16 v0, v17

    #@14f
    move-object/from16 v1, p1

    #@151
    iput v0, v1, Landroid/view/DragEvent;->mAction:I

    #@153
    .line 1220
    move-object/from16 v0, p1

    #@155
    invoke-virtual {v13, v0}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@158
    .line 1221
    iget v0, v13, Landroid/view/View;->mPrivateFlags2:I

    #@15a
    move/from16 v17, v0

    #@15c
    or-int/lit8 v17, v17, 0x2

    #@15e
    move/from16 v0, v17

    #@160
    iput v0, v13, Landroid/view/View;->mPrivateFlags2:I

    #@162
    .line 1222
    invoke-virtual {v13}, Landroid/view/View;->refreshDrawableState()V

    #@165
    .line 1224
    :cond_165
    move-object/from16 v0, p1

    #@167
    iput v4, v0, Landroid/view/DragEvent;->mAction:I

    #@169
    .line 1228
    .end local v4           #action:I
    :cond_169
    if-eqz v13, :cond_16

    #@16b
    .line 1229
    move-object/from16 v0, p0

    #@16d
    iget-object v0, v0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@16f
    move-object/from16 v17, v0

    #@171
    move-object/from16 v0, v17

    #@173
    iget v0, v0, Landroid/graphics/PointF;->x:F

    #@175
    move/from16 v17, v0

    #@177
    move/from16 v0, v17

    #@179
    move-object/from16 v1, p1

    #@17b
    iput v0, v1, Landroid/view/DragEvent;->mX:F

    #@17d
    .line 1230
    move-object/from16 v0, p0

    #@17f
    iget-object v0, v0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@181
    move-object/from16 v17, v0

    #@183
    move-object/from16 v0, v17

    #@185
    iget v0, v0, Landroid/graphics/PointF;->y:F

    #@187
    move/from16 v17, v0

    #@189
    move/from16 v0, v17

    #@18b
    move-object/from16 v1, p1

    #@18d
    iput v0, v1, Landroid/view/DragEvent;->mY:F

    #@18f
    .line 1232
    move-object/from16 v0, p1

    #@191
    invoke-virtual {v13, v0}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@194
    move-result v11

    #@195
    .line 1234
    move-object/from16 v0, p1

    #@197
    iput v14, v0, Landroid/view/DragEvent;->mX:F

    #@199
    .line 1235
    move-object/from16 v0, p1

    #@19b
    iput v15, v0, Landroid/view/DragEvent;->mY:F

    #@19d
    goto/16 :goto_16

    #@19f
    .line 1252
    .end local v13           #target:Landroid/view/View;
    :pswitch_19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@1a3
    move-object/from16 v17, v0

    #@1a5
    if-eqz v17, :cond_16

    #@1a7
    .line 1253
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@1ab
    move-object/from16 v16, v0

    #@1ad
    .line 1254
    .restart local v16       #view:Landroid/view/View;
    move-object/from16 v0, v16

    #@1af
    move-object/from16 v1, p1

    #@1b1
    invoke-virtual {v0, v1}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@1b4
    .line 1255
    move-object/from16 v0, v16

    #@1b6
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@1b8
    move/from16 v17, v0

    #@1ba
    and-int/lit8 v17, v17, -0x3

    #@1bc
    move/from16 v0, v17

    #@1be
    move-object/from16 v1, v16

    #@1c0
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@1c2
    .line 1256
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->refreshDrawableState()V

    #@1c5
    .line 1258
    const/16 v17, 0x0

    #@1c7
    move-object/from16 v0, v17

    #@1c9
    move-object/from16 v1, p0

    #@1cb
    iput-object v0, v1, Landroid/view/ViewGroup;->mCurrentDragView:Landroid/view/View;

    #@1cd
    goto/16 :goto_16

    #@1cf
    .line 1264
    .end local v16           #view:Landroid/view/View;
    :pswitch_1cf
    move-object/from16 v0, p1

    #@1d1
    iget v0, v0, Landroid/view/DragEvent;->mX:F

    #@1d3
    move/from16 v17, v0

    #@1d5
    move-object/from16 v0, p1

    #@1d7
    iget v0, v0, Landroid/view/DragEvent;->mY:F

    #@1d9
    move/from16 v18, v0

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v0, v0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@1df
    move-object/from16 v19, v0

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    move/from16 v1, v17

    #@1e5
    move/from16 v2, v18

    #@1e7
    move-object/from16 v3, v19

    #@1e9
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->findFrontmostDroppableChildAt(FFLandroid/graphics/PointF;)Landroid/view/View;

    #@1ec
    move-result-object v13

    #@1ed
    .line 1265
    .restart local v13       #target:Landroid/view/View;
    if-eqz v13, :cond_16

    #@1ef
    .line 1267
    move-object/from16 v0, p0

    #@1f1
    iget-object v0, v0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@1f3
    move-object/from16 v17, v0

    #@1f5
    move-object/from16 v0, v17

    #@1f7
    iget v0, v0, Landroid/graphics/PointF;->x:F

    #@1f9
    move/from16 v17, v0

    #@1fb
    move/from16 v0, v17

    #@1fd
    move-object/from16 v1, p1

    #@1ff
    iput v0, v1, Landroid/view/DragEvent;->mX:F

    #@201
    .line 1268
    move-object/from16 v0, p0

    #@203
    iget-object v0, v0, Landroid/view/ViewGroup;->mLocalPoint:Landroid/graphics/PointF;

    #@205
    move-object/from16 v17, v0

    #@207
    move-object/from16 v0, v17

    #@209
    iget v0, v0, Landroid/graphics/PointF;->y:F

    #@20b
    move/from16 v17, v0

    #@20d
    move/from16 v0, v17

    #@20f
    move-object/from16 v1, p1

    #@211
    iput v0, v1, Landroid/view/DragEvent;->mY:F

    #@213
    .line 1269
    move-object/from16 v0, p1

    #@215
    invoke-virtual {v13, v0}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@218
    move-result v11

    #@219
    .line 1270
    move-object/from16 v0, p1

    #@21b
    iput v14, v0, Landroid/view/DragEvent;->mX:F

    #@21d
    .line 1271
    move-object/from16 v0, p1

    #@21f
    iput v15, v0, Landroid/view/DragEvent;->mY:F

    #@221
    goto/16 :goto_16

    #@223
    .line 1137
    nop

    #@224
    :pswitch_data_224
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_e8
        :pswitch_1cf
        :pswitch_92
        :pswitch_16
        :pswitch_19f
    .end packed-switch
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 29
    .parameter "canvas"

    #@0
    .prologue
    .line 2774
    move-object/from16 v0, p0

    #@2
    iget v11, v0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4
    .line 2775
    .local v11, count:I
    move-object/from16 v0, p0

    #@6
    iget-object v8, v0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@8
    .line 2776
    .local v8, children:[Landroid/view/View;
    move-object/from16 v0, p0

    #@a
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@c
    move/from16 v17, v0

    #@e
    .line 2778
    .local v17, flags:I
    and-int/lit8 v22, v17, 0x8

    #@10
    if-eqz v22, :cond_ce

    #@12
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->canAnimate()Z

    #@15
    move-result v22

    #@16
    if-eqz v22, :cond_ce

    #@18
    .line 2779
    move-object/from16 v0, p0

    #@1a
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1c
    move/from16 v22, v0

    #@1e
    and-int/lit8 v22, v22, 0x40

    #@20
    const/16 v23, 0x40

    #@22
    move/from16 v0, v22

    #@24
    move/from16 v1, v23

    #@26
    if-ne v0, v1, :cond_67

    #@28
    const/4 v6, 0x1

    #@29
    .line 2781
    .local v6, cache:Z
    :goto_29
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->isHardwareAccelerated()Z

    #@2c
    move-result v22

    #@2d
    if-nez v22, :cond_69

    #@2f
    const/4 v5, 0x1

    #@30
    .line 2782
    .local v5, buildCache:Z
    :goto_30
    const/16 v18, 0x0

    #@32
    .local v18, i:I
    :goto_32
    move/from16 v0, v18

    #@34
    if-ge v0, v11, :cond_6b

    #@36
    .line 2783
    aget-object v7, v8, v18

    #@38
    .line 2784
    .local v7, child:Landroid/view/View;
    iget v0, v7, Landroid/view/View;->mViewFlags:I

    #@3a
    move/from16 v22, v0

    #@3c
    and-int/lit8 v22, v22, 0xc

    #@3e
    if-nez v22, :cond_64

    #@40
    .line 2785
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@43
    move-result-object v20

    #@44
    .line 2786
    .local v20, params:Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    #@46
    move-object/from16 v1, v20

    #@48
    move/from16 v2, v18

    #@4a
    invoke-virtual {v0, v7, v1, v2, v11}, Landroid/view/ViewGroup;->attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V

    #@4d
    .line 2787
    move-object/from16 v0, p0

    #@4f
    invoke-direct {v0, v7}, Landroid/view/ViewGroup;->bindLayoutAnimation(Landroid/view/View;)V

    #@52
    .line 2788
    if-eqz v6, :cond_64

    #@54
    .line 2789
    const/16 v22, 0x1

    #@56
    move/from16 v0, v22

    #@58
    invoke-virtual {v7, v0}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    #@5b
    .line 2790
    if-eqz v5, :cond_64

    #@5d
    .line 2791
    const/16 v22, 0x1

    #@5f
    move/from16 v0, v22

    #@61
    invoke-virtual {v7, v0}, Landroid/view/View;->buildDrawingCache(Z)V

    #@64
    .line 2782
    .end local v20           #params:Landroid/view/ViewGroup$LayoutParams;
    :cond_64
    add-int/lit8 v18, v18, 0x1

    #@66
    goto :goto_32

    #@67
    .line 2779
    .end local v5           #buildCache:Z
    .end local v6           #cache:Z
    .end local v7           #child:Landroid/view/View;
    .end local v18           #i:I
    :cond_67
    const/4 v6, 0x0

    #@68
    goto :goto_29

    #@69
    .line 2781
    .restart local v6       #cache:Z
    :cond_69
    const/4 v5, 0x0

    #@6a
    goto :goto_30

    #@6b
    .line 2797
    .restart local v5       #buildCache:Z
    .restart local v18       #i:I
    :cond_6b
    move-object/from16 v0, p0

    #@6d
    iget-object v10, v0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@6f
    .line 2798
    .local v10, controller:Landroid/view/animation/LayoutAnimationController;
    invoke-virtual {v10}, Landroid/view/animation/LayoutAnimationController;->willOverlap()Z

    #@72
    move-result v22

    #@73
    if-eqz v22, :cond_87

    #@75
    .line 2799
    move-object/from16 v0, p0

    #@77
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@79
    move/from16 v22, v0

    #@7b
    move/from16 v0, v22

    #@7d
    or-int/lit16 v0, v0, 0x80

    #@7f
    move/from16 v22, v0

    #@81
    move/from16 v0, v22

    #@83
    move-object/from16 v1, p0

    #@85
    iput v0, v1, Landroid/view/ViewGroup;->mGroupFlags:I

    #@87
    .line 2802
    :cond_87
    invoke-virtual {v10}, Landroid/view/animation/LayoutAnimationController;->start()V

    #@8a
    .line 2804
    move-object/from16 v0, p0

    #@8c
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8e
    move/from16 v22, v0

    #@90
    and-int/lit8 v22, v22, -0x9

    #@92
    move/from16 v0, v22

    #@94
    move-object/from16 v1, p0

    #@96
    iput v0, v1, Landroid/view/ViewGroup;->mGroupFlags:I

    #@98
    .line 2805
    move-object/from16 v0, p0

    #@9a
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@9c
    move/from16 v22, v0

    #@9e
    and-int/lit8 v22, v22, -0x11

    #@a0
    move/from16 v0, v22

    #@a2
    move-object/from16 v1, p0

    #@a4
    iput v0, v1, Landroid/view/ViewGroup;->mGroupFlags:I

    #@a6
    .line 2807
    if-eqz v6, :cond_b9

    #@a8
    .line 2808
    move-object/from16 v0, p0

    #@aa
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@ac
    move/from16 v22, v0

    #@ae
    const v23, 0x8000

    #@b1
    or-int v22, v22, v23

    #@b3
    move/from16 v0, v22

    #@b5
    move-object/from16 v1, p0

    #@b7
    iput v0, v1, Landroid/view/ViewGroup;->mGroupFlags:I

    #@b9
    .line 2811
    :cond_b9
    move-object/from16 v0, p0

    #@bb
    iget-object v0, v0, Landroid/view/ViewGroup;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    #@bd
    move-object/from16 v22, v0

    #@bf
    if-eqz v22, :cond_ce

    #@c1
    .line 2812
    move-object/from16 v0, p0

    #@c3
    iget-object v0, v0, Landroid/view/ViewGroup;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    #@c5
    move-object/from16 v22, v0

    #@c7
    invoke-virtual {v10}, Landroid/view/animation/LayoutAnimationController;->getAnimation()Landroid/view/animation/Animation;

    #@ca
    move-result-object v23

    #@cb
    invoke-interface/range {v22 .. v23}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V

    #@ce
    .line 2816
    .end local v5           #buildCache:Z
    .end local v6           #cache:Z
    .end local v10           #controller:Landroid/view/animation/LayoutAnimationController;
    .end local v18           #i:I
    :cond_ce
    const/16 v21, 0x0

    #@d0
    .line 2817
    .local v21, saveCount:I
    and-int/lit8 v22, v17, 0x22

    #@d2
    const/16 v23, 0x22

    #@d4
    move/from16 v0, v22

    #@d6
    move/from16 v1, v23

    #@d8
    if-ne v0, v1, :cond_195

    #@da
    const/4 v9, 0x1

    #@db
    .line 2818
    .local v9, clipToPadding:Z
    :goto_db
    if-eqz v9, :cond_146

    #@dd
    .line 2819
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@e0
    move-result v21

    #@e1
    .line 2820
    move-object/from16 v0, p0

    #@e3
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@e5
    move/from16 v22, v0

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@eb
    move/from16 v23, v0

    #@ed
    add-int v22, v22, v23

    #@ef
    move-object/from16 v0, p0

    #@f1
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@f3
    move/from16 v23, v0

    #@f5
    move-object/from16 v0, p0

    #@f7
    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    #@f9
    move/from16 v24, v0

    #@fb
    add-int v23, v23, v24

    #@fd
    move-object/from16 v0, p0

    #@ff
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@101
    move/from16 v24, v0

    #@103
    move-object/from16 v0, p0

    #@105
    iget v0, v0, Landroid/view/View;->mRight:I

    #@107
    move/from16 v25, v0

    #@109
    add-int v24, v24, v25

    #@10b
    move-object/from16 v0, p0

    #@10d
    iget v0, v0, Landroid/view/View;->mLeft:I

    #@10f
    move/from16 v25, v0

    #@111
    sub-int v24, v24, v25

    #@113
    move-object/from16 v0, p0

    #@115
    iget v0, v0, Landroid/view/View;->mPaddingRight:I

    #@117
    move/from16 v25, v0

    #@119
    sub-int v24, v24, v25

    #@11b
    move-object/from16 v0, p0

    #@11d
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@11f
    move/from16 v25, v0

    #@121
    move-object/from16 v0, p0

    #@123
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@125
    move/from16 v26, v0

    #@127
    add-int v25, v25, v26

    #@129
    move-object/from16 v0, p0

    #@12b
    iget v0, v0, Landroid/view/View;->mTop:I

    #@12d
    move/from16 v26, v0

    #@12f
    sub-int v25, v25, v26

    #@131
    move-object/from16 v0, p0

    #@133
    iget v0, v0, Landroid/view/View;->mPaddingBottom:I

    #@135
    move/from16 v26, v0

    #@137
    sub-int v25, v25, v26

    #@139
    move-object/from16 v0, p1

    #@13b
    move/from16 v1, v22

    #@13d
    move/from16 v2, v23

    #@13f
    move/from16 v3, v24

    #@141
    move/from16 v4, v25

    #@143
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@146
    .line 2827
    :cond_146
    move-object/from16 v0, p0

    #@148
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@14a
    move/from16 v22, v0

    #@14c
    and-int/lit8 v22, v22, -0x41

    #@14e
    move/from16 v0, v22

    #@150
    move-object/from16 v1, p0

    #@152
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@154
    .line 2828
    move-object/from16 v0, p0

    #@156
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@158
    move/from16 v22, v0

    #@15a
    and-int/lit8 v22, v22, -0x5

    #@15c
    move/from16 v0, v22

    #@15e
    move-object/from16 v1, p0

    #@160
    iput v0, v1, Landroid/view/ViewGroup;->mGroupFlags:I

    #@162
    .line 2830
    const/16 v19, 0x0

    #@164
    .line 2831
    .local v19, more:Z
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getDrawingTime()J

    #@167
    move-result-wide v14

    #@168
    .line 2833
    .local v14, drawingTime:J
    move/from16 v0, v17

    #@16a
    and-int/lit16 v0, v0, 0x400

    #@16c
    move/from16 v22, v0

    #@16e
    if-nez v22, :cond_198

    #@170
    .line 2834
    const/16 v18, 0x0

    #@172
    .restart local v18       #i:I
    :goto_172
    move/from16 v0, v18

    #@174
    if-ge v0, v11, :cond_1c3

    #@176
    .line 2835
    aget-object v7, v8, v18

    #@178
    .line 2836
    .restart local v7       #child:Landroid/view/View;
    if-eqz v7, :cond_192

    #@17a
    iget v0, v7, Landroid/view/View;->mViewFlags:I

    #@17c
    move/from16 v22, v0

    #@17e
    and-int/lit8 v22, v22, 0xc

    #@180
    if-eqz v22, :cond_188

    #@182
    invoke-virtual {v7}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@185
    move-result-object v22

    #@186
    if-eqz v22, :cond_192

    #@188
    .line 2837
    :cond_188
    move-object/from16 v0, p0

    #@18a
    move-object/from16 v1, p1

    #@18c
    invoke-virtual {v0, v1, v7, v14, v15}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@18f
    move-result v22

    #@190
    or-int v19, v19, v22

    #@192
    .line 2834
    :cond_192
    add-int/lit8 v18, v18, 0x1

    #@194
    goto :goto_172

    #@195
    .line 2817
    .end local v7           #child:Landroid/view/View;
    .end local v9           #clipToPadding:Z
    .end local v14           #drawingTime:J
    .end local v18           #i:I
    .end local v19           #more:Z
    :cond_195
    const/4 v9, 0x0

    #@196
    goto/16 :goto_db

    #@198
    .line 2841
    .restart local v9       #clipToPadding:Z
    .restart local v14       #drawingTime:J
    .restart local v19       #more:Z
    :cond_198
    const/16 v18, 0x0

    #@19a
    .restart local v18       #i:I
    :goto_19a
    move/from16 v0, v18

    #@19c
    if-ge v0, v11, :cond_1c3

    #@19e
    .line 2842
    move-object/from16 v0, p0

    #@1a0
    move/from16 v1, v18

    #@1a2
    invoke-virtual {v0, v11, v1}, Landroid/view/ViewGroup;->getChildDrawingOrder(II)I

    #@1a5
    move-result v22

    #@1a6
    aget-object v7, v8, v22

    #@1a8
    .line 2843
    .restart local v7       #child:Landroid/view/View;
    iget v0, v7, Landroid/view/View;->mViewFlags:I

    #@1aa
    move/from16 v22, v0

    #@1ac
    and-int/lit8 v22, v22, 0xc

    #@1ae
    if-eqz v22, :cond_1b6

    #@1b0
    invoke-virtual {v7}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@1b3
    move-result-object v22

    #@1b4
    if-eqz v22, :cond_1c0

    #@1b6
    .line 2844
    :cond_1b6
    move-object/from16 v0, p0

    #@1b8
    move-object/from16 v1, p1

    #@1ba
    invoke-virtual {v0, v1, v7, v14, v15}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@1bd
    move-result v22

    #@1be
    or-int v19, v19, v22

    #@1c0
    .line 2841
    :cond_1c0
    add-int/lit8 v18, v18, 0x1

    #@1c2
    goto :goto_19a

    #@1c3
    .line 2850
    .end local v7           #child:Landroid/view/View;
    :cond_1c3
    move-object/from16 v0, p0

    #@1c5
    iget-object v0, v0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@1c7
    move-object/from16 v22, v0

    #@1c9
    if-eqz v22, :cond_1ee

    #@1cb
    .line 2851
    move-object/from16 v0, p0

    #@1cd
    iget-object v12, v0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@1cf
    .line 2852
    .local v12, disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@1d2
    move-result v22

    #@1d3
    add-int/lit8 v13, v22, -0x1

    #@1d5
    .line 2854
    .local v13, disappearingCount:I
    move/from16 v18, v13

    #@1d7
    :goto_1d7
    if-ltz v18, :cond_1ee

    #@1d9
    .line 2855
    move/from16 v0, v18

    #@1db
    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1de
    move-result-object v7

    #@1df
    check-cast v7, Landroid/view/View;

    #@1e1
    .line 2856
    .restart local v7       #child:Landroid/view/View;
    move-object/from16 v0, p0

    #@1e3
    move-object/from16 v1, p1

    #@1e5
    invoke-virtual {v0, v1, v7, v14, v15}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@1e8
    move-result v22

    #@1e9
    or-int v19, v19, v22

    #@1eb
    .line 2854
    add-int/lit8 v18, v18, -0x1

    #@1ed
    goto :goto_1d7

    #@1ee
    .line 2860
    .end local v7           #child:Landroid/view/View;
    .end local v12           #disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v13           #disappearingCount:I
    :cond_1ee
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewGroup;->debugDraw()Z

    #@1f1
    move-result v22

    #@1f2
    if-eqz v22, :cond_1f7

    #@1f4
    .line 2861
    invoke-virtual/range {p0 .. p1}, Landroid/view/ViewGroup;->onDebugDraw(Landroid/graphics/Canvas;)V

    #@1f7
    .line 2864
    :cond_1f7
    if-eqz v9, :cond_200

    #@1f9
    .line 2865
    move-object/from16 v0, p1

    #@1fb
    move/from16 v1, v21

    #@1fd
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@200
    .line 2869
    :cond_200
    move-object/from16 v0, p0

    #@202
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@204
    move/from16 v17, v0

    #@206
    .line 2871
    and-int/lit8 v22, v17, 0x4

    #@208
    const/16 v23, 0x4

    #@20a
    move/from16 v0, v22

    #@20c
    move/from16 v1, v23

    #@20e
    if-ne v0, v1, :cond_219

    #@210
    .line 2872
    const/16 v22, 0x1

    #@212
    move-object/from16 v0, p0

    #@214
    move/from16 v1, v22

    #@216
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@219
    .line 2875
    :cond_219
    and-int/lit8 v22, v17, 0x10

    #@21b
    if-nez v22, :cond_255

    #@21d
    move/from16 v0, v17

    #@21f
    and-int/lit16 v0, v0, 0x200

    #@221
    move/from16 v22, v0

    #@223
    if-nez v22, :cond_255

    #@225
    move-object/from16 v0, p0

    #@227
    iget-object v0, v0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@229
    move-object/from16 v22, v0

    #@22b
    invoke-virtual/range {v22 .. v22}, Landroid/view/animation/LayoutAnimationController;->isDone()Z

    #@22e
    move-result v22

    #@22f
    if-eqz v22, :cond_255

    #@231
    if-nez v19, :cond_255

    #@233
    .line 2880
    move-object/from16 v0, p0

    #@235
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@237
    move/from16 v22, v0

    #@239
    move/from16 v0, v22

    #@23b
    or-int/lit16 v0, v0, 0x200

    #@23d
    move/from16 v22, v0

    #@23f
    move/from16 v0, v22

    #@241
    move-object/from16 v1, p0

    #@243
    iput v0, v1, Landroid/view/ViewGroup;->mGroupFlags:I

    #@245
    .line 2881
    new-instance v16, Landroid/view/ViewGroup$1;

    #@247
    move-object/from16 v0, v16

    #@249
    move-object/from16 v1, p0

    #@24b
    invoke-direct {v0, v1}, Landroid/view/ViewGroup$1;-><init>(Landroid/view/ViewGroup;)V

    #@24e
    .line 2886
    .local v16, end:Ljava/lang/Runnable;
    move-object/from16 v0, p0

    #@250
    move-object/from16 v1, v16

    #@252
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    #@255
    .line 2888
    .end local v16           #end:Ljava/lang/Runnable;
    :cond_255
    return-void
.end method

.method public dispatchFinishTemporaryDetach()V
    .registers 5

    #@0
    .prologue
    .line 2441
    invoke-super {p0}, Landroid/view/View;->dispatchFinishTemporaryDetach()V

    #@3
    .line 2442
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 2443
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 2444
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 2445
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3}, Landroid/view/View;->dispatchFinishTemporaryDetach()V

    #@f
    .line 2444
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 2447
    :cond_12
    return-void
.end method

.method protected dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2614
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-super {p0, p1}, Landroid/view/View;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    #@3
    .line 2615
    return-void
.end method

.method protected dispatchGenericFocusedEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1785
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x12

    #@4
    const/16 v1, 0x12

    #@6
    if-ne v0, v1, :cond_d

    #@8
    .line 1787
    invoke-super {p0, p1}, Landroid/view/View;->dispatchGenericFocusedEvent(Landroid/view/MotionEvent;)Z

    #@b
    move-result v0

    #@c
    .line 1792
    :goto_c
    return v0

    #@d
    .line 1788
    :cond_d
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@f
    if-eqz v0, :cond_22

    #@11
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@13
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@15
    and-int/lit8 v0, v0, 0x10

    #@17
    const/16 v1, 0x10

    #@19
    if-ne v0, v1, :cond_22

    #@1b
    .line 1790
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1d
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@20
    move-result v0

    #@21
    goto :goto_c

    #@22
    .line 1792
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_c
.end method

.method protected dispatchGenericPointerEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "event"

    #@0
    .prologue
    .line 1754
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    .line 1755
    .local v3, childrenCount:I
    if-eqz v3, :cond_38

    #@4
    .line 1756
    iget-object v2, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@6
    .line 1757
    .local v2, children:[Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@9
    move-result v6

    #@a
    .line 1758
    .local v6, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@d
    move-result v7

    #@e
    .line 1760
    .local v7, y:F
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isChildrenDrawingOrderEnabled()Z

    #@11
    move-result v4

    #@12
    .line 1761
    .local v4, customOrder:Z
    add-int/lit8 v5, v3, -0x1

    #@14
    .local v5, i:I
    :goto_14
    if-ltz v5, :cond_38

    #@16
    .line 1762
    if-eqz v4, :cond_2e

    #@18
    invoke-virtual {p0, v3, v5}, Landroid/view/ViewGroup;->getChildDrawingOrder(II)I

    #@1b
    move-result v1

    #@1c
    .line 1763
    .local v1, childIndex:I
    :goto_1c
    aget-object v0, v2, v1

    #@1e
    .line 1764
    .local v0, child:Landroid/view/View;
    invoke-static {v0}, Landroid/view/ViewGroup;->canViewReceivePointerEvents(Landroid/view/View;)Z

    #@21
    move-result v8

    #@22
    if-eqz v8, :cond_2b

    #@24
    const/4 v8, 0x0

    #@25
    invoke-virtual {p0, v6, v7, v0, v8}, Landroid/view/ViewGroup;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    #@28
    move-result v8

    #@29
    if-nez v8, :cond_30

    #@2b
    .line 1761
    :cond_2b
    add-int/lit8 v5, v5, -0x1

    #@2d
    goto :goto_14

    #@2e
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childIndex:I
    :cond_2e
    move v1, v5

    #@2f
    .line 1762
    goto :goto_1c

    #@30
    .line 1769
    .restart local v0       #child:Landroid/view/View;
    .restart local v1       #childIndex:I
    :cond_30
    invoke-direct {p0, p1, v0}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@33
    move-result v8

    #@34
    if-eqz v8, :cond_2b

    #@36
    .line 1770
    const/4 v8, 0x1

    #@37
    .line 1776
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childIndex:I
    .end local v2           #children:[Landroid/view/View;
    .end local v4           #customOrder:Z
    .end local v5           #i:I
    .end local v6           #x:F
    .end local v7           #y:F
    :goto_37
    return v8

    #@38
    :cond_38
    invoke-super {p0, p1}, Landroid/view/View;->dispatchGenericPointerEvent(Landroid/view/MotionEvent;)Z

    #@3b
    move-result v8

    #@3c
    goto :goto_37
.end method

.method protected dispatchGetDisplayList()V
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/high16 v7, -0x8000

    #@3
    .line 2941
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 2942
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 2943
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v2, :cond_38

    #@a
    .line 2944
    aget-object v0, v1, v3

    #@c
    .line 2945
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@e
    and-int/lit8 v4, v4, 0xc

    #@10
    if-eqz v4, :cond_18

    #@12
    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@15
    move-result-object v4

    #@16
    if-eqz v4, :cond_33

    #@18
    :cond_18
    invoke-virtual {v0}, Landroid/view/View;->hasStaticLayer()Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_33

    #@1e
    .line 2947
    iget v4, v0, Landroid/view/View;->mPrivateFlags:I

    #@20
    and-int/2addr v4, v7

    #@21
    if-ne v4, v7, :cond_36

    #@23
    const/4 v4, 0x1

    #@24
    :goto_24
    iput-boolean v4, v0, Landroid/view/View;->mRecreateDisplayList:Z

    #@26
    .line 2949
    iget v4, v0, Landroid/view/View;->mPrivateFlags:I

    #@28
    const v6, 0x7fffffff

    #@2b
    and-int/2addr v4, v6

    #@2c
    iput v4, v0, Landroid/view/View;->mPrivateFlags:I

    #@2e
    .line 2950
    invoke-virtual {v0}, Landroid/view/View;->getDisplayList()Landroid/view/DisplayList;

    #@31
    .line 2951
    iput-boolean v5, v0, Landroid/view/View;->mRecreateDisplayList:Z

    #@33
    .line 2943
    :cond_33
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_8

    #@36
    :cond_36
    move v4, v5

    #@37
    .line 2947
    goto :goto_24

    #@38
    .line 2954
    .end local v0           #child:Landroid/view/View;
    :cond_38
    return-void
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 24
    .parameter "event"

    #@0
    .prologue
    .line 1450
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v4

    #@4
    .line 1453
    .local v4, action:I
    invoke-virtual/range {p0 .. p1}, Landroid/view/ViewGroup;->onInterceptHoverEvent(Landroid/view/MotionEvent;)Z

    #@7
    move-result v13

    #@8
    .line 1454
    .local v13, interceptHover:Z
    move-object/from16 v0, p1

    #@a
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@d
    .line 1456
    move-object/from16 v8, p1

    #@f
    .line 1457
    .local v8, eventNoHistory:Landroid/view/MotionEvent;
    const/4 v10, 0x0

    #@10
    .line 1461
    .local v10, handled:Z
    move-object/from16 v0, p0

    #@12
    iget-object v9, v0, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@14
    .line 1462
    .local v9, firstOldHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    const/16 v21, 0x0

    #@16
    move-object/from16 v0, v21

    #@18
    move-object/from16 v1, p0

    #@1a
    iput-object v0, v1, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@1c
    .line 1463
    if-nez v13, :cond_7a

    #@1e
    const/16 v21, 0xa

    #@20
    move/from16 v0, v21

    #@22
    if-eq v4, v0, :cond_7a

    #@24
    .line 1464
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@27
    move-result v19

    #@28
    .line 1465
    .local v19, x:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@2b
    move-result v20

    #@2c
    .line 1466
    .local v20, y:F
    move-object/from16 v0, p0

    #@2e
    iget v7, v0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@30
    .line 1467
    .local v7, childrenCount:I
    if-eqz v7, :cond_7a

    #@32
    .line 1468
    move-object/from16 v0, p0

    #@34
    iget-object v6, v0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@36
    .line 1469
    .local v6, children:[Landroid/view/View;
    const/4 v14, 0x0

    #@37
    .line 1470
    .local v14, lastHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    add-int/lit8 v12, v7, -0x1

    #@39
    .local v12, i:I
    :goto_39
    if-ltz v12, :cond_7a

    #@3b
    .line 1471
    aget-object v5, v6, v12

    #@3d
    .line 1472
    .local v5, child:Landroid/view/View;
    invoke-static {v5}, Landroid/view/ViewGroup;->canViewReceivePointerEvents(Landroid/view/View;)Z

    #@40
    move-result v21

    #@41
    if-eqz v21, :cond_53

    #@43
    const/16 v21, 0x0

    #@45
    move-object/from16 v0, p0

    #@47
    move/from16 v1, v19

    #@49
    move/from16 v2, v20

    #@4b
    move-object/from16 v3, v21

    #@4d
    invoke-virtual {v0, v1, v2, v5, v3}, Landroid/view/ViewGroup;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    #@50
    move-result v21

    #@51
    if-nez v21, :cond_56

    #@53
    .line 1470
    :cond_53
    add-int/lit8 v12, v12, -0x1

    #@55
    goto :goto_39

    #@56
    .line 1479
    :cond_56
    move-object v11, v9

    #@57
    .line 1481
    .local v11, hoverTarget:Landroid/view/ViewGroup$HoverTarget;
    const/16 v17, 0x0

    #@59
    .line 1482
    .local v17, predecessor:Landroid/view/ViewGroup$HoverTarget;
    :goto_59
    if-nez v11, :cond_98

    #@5b
    .line 1483
    invoke-static {v5}, Landroid/view/ViewGroup$HoverTarget;->obtain(Landroid/view/View;)Landroid/view/ViewGroup$HoverTarget;

    #@5e
    move-result-object v11

    #@5f
    .line 1484
    const/16 v18, 0x0

    #@61
    .line 1504
    .local v18, wasHovered:Z
    :goto_61
    if-eqz v14, :cond_bd

    #@63
    .line 1505
    iput-object v11, v14, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@65
    .line 1510
    :goto_65
    move-object v14, v11

    #@66
    .line 1514
    const/16 v21, 0x9

    #@68
    move/from16 v0, v21

    #@6a
    if-ne v4, v0, :cond_c2

    #@6c
    .line 1515
    if-nez v18, :cond_78

    #@6e
    .line 1517
    move-object/from16 v0, p0

    #@70
    move-object/from16 v1, p1

    #@72
    invoke-direct {v0, v1, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@75
    move-result v21

    #@76
    or-int v10, v10, v21

    #@78
    .line 1536
    :cond_78
    :goto_78
    if-eqz v10, :cond_53

    #@7a
    .line 1544
    .end local v5           #child:Landroid/view/View;
    .end local v6           #children:[Landroid/view/View;
    .end local v7           #childrenCount:I
    .end local v11           #hoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .end local v12           #i:I
    .end local v14           #lastHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .end local v17           #predecessor:Landroid/view/ViewGroup$HoverTarget;
    .end local v18           #wasHovered:Z
    .end local v19           #x:F
    .end local v20           #y:F
    :cond_7a
    :goto_7a
    if-eqz v9, :cond_116

    #@7c
    .line 1545
    iget-object v5, v9, Landroid/view/ViewGroup$HoverTarget;->child:Landroid/view/View;

    #@7e
    .line 1548
    .restart local v5       #child:Landroid/view/View;
    const/16 v21, 0xa

    #@80
    move/from16 v0, v21

    #@82
    if-ne v4, v0, :cond_f4

    #@84
    .line 1550
    move-object/from16 v0, p0

    #@86
    move-object/from16 v1, p1

    #@88
    invoke-direct {v0, v1, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@8b
    move-result v21

    #@8c
    or-int v10, v10, v21

    #@8e
    .line 1566
    :goto_8e
    iget-object v0, v9, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@90
    move-object/from16 v16, v0

    #@92
    .line 1567
    .local v16, nextOldHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    invoke-virtual {v9}, Landroid/view/ViewGroup$HoverTarget;->recycle()V

    #@95
    .line 1568
    move-object/from16 v9, v16

    #@97
    .line 1569
    goto :goto_7a

    #@98
    .line 1488
    .end local v16           #nextOldHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .restart local v6       #children:[Landroid/view/View;
    .restart local v7       #childrenCount:I
    .restart local v11       #hoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .restart local v12       #i:I
    .restart local v14       #lastHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .restart local v17       #predecessor:Landroid/view/ViewGroup$HoverTarget;
    .restart local v19       #x:F
    .restart local v20       #y:F
    :cond_98
    iget-object v0, v11, Landroid/view/ViewGroup$HoverTarget;->child:Landroid/view/View;

    #@9a
    move-object/from16 v21, v0

    #@9c
    move-object/from16 v0, v21

    #@9e
    if-ne v0, v5, :cond_b8

    #@a0
    .line 1489
    if-eqz v17, :cond_b5

    #@a2
    .line 1490
    iget-object v0, v11, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@a4
    move-object/from16 v21, v0

    #@a6
    move-object/from16 v0, v21

    #@a8
    move-object/from16 v1, v17

    #@aa
    iput-object v0, v1, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@ac
    .line 1494
    :goto_ac
    const/16 v21, 0x0

    #@ae
    move-object/from16 v0, v21

    #@b0
    iput-object v0, v11, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@b2
    .line 1495
    const/16 v18, 0x1

    #@b4
    .line 1496
    .restart local v18       #wasHovered:Z
    goto :goto_61

    #@b5
    .line 1492
    .end local v18           #wasHovered:Z
    :cond_b5
    iget-object v9, v11, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@b7
    goto :goto_ac

    #@b8
    .line 1499
    :cond_b8
    move-object/from16 v17, v11

    #@ba
    .line 1500
    iget-object v11, v11, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@bc
    goto :goto_59

    #@bd
    .line 1507
    .restart local v18       #wasHovered:Z
    :cond_bd
    move-object/from16 v0, p0

    #@bf
    iput-object v11, v0, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@c1
    goto :goto_65

    #@c2
    .line 1520
    :cond_c2
    const/16 v21, 0x7

    #@c4
    move/from16 v0, v21

    #@c6
    if-ne v4, v0, :cond_78

    #@c8
    .line 1521
    if-nez v18, :cond_e9

    #@ca
    .line 1523
    invoke-static {v8}, Landroid/view/ViewGroup;->obtainMotionEventNoHistoryOrSelf(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@cd
    move-result-object v8

    #@ce
    .line 1524
    const/16 v21, 0x9

    #@d0
    move/from16 v0, v21

    #@d2
    invoke-virtual {v8, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@d5
    .line 1525
    move-object/from16 v0, p0

    #@d7
    invoke-direct {v0, v8, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@da
    move-result v21

    #@db
    or-int v10, v10, v21

    #@dd
    .line 1527
    invoke-virtual {v8, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@e0
    .line 1529
    move-object/from16 v0, p0

    #@e2
    invoke-direct {v0, v8, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@e5
    move-result v21

    #@e6
    or-int v10, v10, v21

    #@e8
    goto :goto_78

    #@e9
    .line 1533
    :cond_e9
    move-object/from16 v0, p0

    #@eb
    move-object/from16 v1, p1

    #@ed
    invoke-direct {v0, v1, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@f0
    move-result v21

    #@f1
    or-int v10, v10, v21

    #@f3
    goto :goto_78

    #@f4
    .line 1555
    .end local v6           #children:[Landroid/view/View;
    .end local v7           #childrenCount:I
    .end local v11           #hoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .end local v12           #i:I
    .end local v14           #lastHoverTarget:Landroid/view/ViewGroup$HoverTarget;
    .end local v17           #predecessor:Landroid/view/ViewGroup$HoverTarget;
    .end local v18           #wasHovered:Z
    .end local v19           #x:F
    .end local v20           #y:F
    :cond_f4
    const/16 v21, 0x7

    #@f6
    move/from16 v0, v21

    #@f8
    if-ne v4, v0, :cond_101

    #@fa
    .line 1556
    move-object/from16 v0, p0

    #@fc
    move-object/from16 v1, p1

    #@fe
    invoke-direct {v0, v1, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@101
    .line 1559
    :cond_101
    invoke-static {v8}, Landroid/view/ViewGroup;->obtainMotionEventNoHistoryOrSelf(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@104
    move-result-object v8

    #@105
    .line 1560
    const/16 v21, 0xa

    #@107
    move/from16 v0, v21

    #@109
    invoke-virtual {v8, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@10c
    .line 1561
    move-object/from16 v0, p0

    #@10e
    invoke-direct {v0, v8, v5}, Landroid/view/ViewGroup;->dispatchTransformedGenericPointerEvent(Landroid/view/MotionEvent;Landroid/view/View;)Z

    #@111
    .line 1563
    invoke-virtual {v8, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@114
    goto/16 :goto_8e

    #@116
    .line 1572
    .end local v5           #child:Landroid/view/View;
    :cond_116
    if-nez v10, :cond_133

    #@118
    const/4 v15, 0x1

    #@119
    .line 1573
    .local v15, newHoveredSelf:Z
    :goto_119
    move-object/from16 v0, p0

    #@11b
    iget-boolean v0, v0, Landroid/view/ViewGroup;->mHoveredSelf:Z

    #@11d
    move/from16 v21, v0

    #@11f
    move/from16 v0, v21

    #@121
    if-ne v15, v0, :cond_135

    #@123
    .line 1574
    if-eqz v15, :cond_12b

    #@125
    .line 1576
    invoke-super/range {p0 .. p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@128
    move-result v21

    #@129
    or-int v10, v10, v21

    #@12b
    .line 1618
    :cond_12b
    :goto_12b
    move-object/from16 v0, p1

    #@12d
    if-eq v8, v0, :cond_132

    #@12f
    .line 1619
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    #@132
    .line 1623
    :cond_132
    return v10

    #@133
    .line 1572
    .end local v15           #newHoveredSelf:Z
    :cond_133
    const/4 v15, 0x0

    #@134
    goto :goto_119

    #@135
    .line 1579
    .restart local v15       #newHoveredSelf:Z
    :cond_135
    move-object/from16 v0, p0

    #@137
    iget-boolean v0, v0, Landroid/view/ViewGroup;->mHoveredSelf:Z

    #@139
    move/from16 v21, v0

    #@13b
    if-eqz v21, :cond_151

    #@13d
    .line 1581
    const/16 v21, 0xa

    #@13f
    move/from16 v0, v21

    #@141
    if-ne v4, v0, :cond_168

    #@143
    .line 1583
    invoke-super/range {p0 .. p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@146
    move-result v21

    #@147
    or-int v10, v10, v21

    #@149
    .line 1595
    :goto_149
    const/16 v21, 0x0

    #@14b
    move/from16 v0, v21

    #@14d
    move-object/from16 v1, p0

    #@14f
    iput-boolean v0, v1, Landroid/view/ViewGroup;->mHoveredSelf:Z

    #@151
    .line 1598
    :cond_151
    if-eqz v15, :cond_12b

    #@153
    .line 1600
    const/16 v21, 0x9

    #@155
    move/from16 v0, v21

    #@157
    if-ne v4, v0, :cond_185

    #@159
    .line 1602
    invoke-super/range {p0 .. p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@15c
    move-result v21

    #@15d
    or-int v10, v10, v21

    #@15f
    .line 1603
    const/16 v21, 0x1

    #@161
    move/from16 v0, v21

    #@163
    move-object/from16 v1, p0

    #@165
    iput-boolean v0, v1, Landroid/view/ViewGroup;->mHoveredSelf:Z

    #@167
    goto :goto_12b

    #@168
    .line 1587
    :cond_168
    const/16 v21, 0x7

    #@16a
    move/from16 v0, v21

    #@16c
    if-ne v4, v0, :cond_171

    #@16e
    .line 1588
    invoke-super/range {p0 .. p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@171
    .line 1590
    :cond_171
    invoke-static {v8}, Landroid/view/ViewGroup;->obtainMotionEventNoHistoryOrSelf(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@174
    move-result-object v8

    #@175
    .line 1591
    const/16 v21, 0xa

    #@177
    move/from16 v0, v21

    #@179
    invoke-virtual {v8, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@17c
    .line 1592
    move-object/from16 v0, p0

    #@17e
    invoke-super {v0, v8}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@181
    .line 1593
    invoke-virtual {v8, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@184
    goto :goto_149

    #@185
    .line 1604
    :cond_185
    const/16 v21, 0x7

    #@187
    move/from16 v0, v21

    #@189
    if-ne v4, v0, :cond_12b

    #@18b
    .line 1606
    invoke-static {v8}, Landroid/view/ViewGroup;->obtainMotionEventNoHistoryOrSelf(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@18e
    move-result-object v8

    #@18f
    .line 1607
    const/16 v21, 0x9

    #@191
    move/from16 v0, v21

    #@193
    invoke-virtual {v8, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@196
    .line 1608
    move-object/from16 v0, p0

    #@198
    invoke-super {v0, v8}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@19b
    move-result v21

    #@19c
    or-int v10, v10, v21

    #@19e
    .line 1609
    invoke-virtual {v8, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@1a1
    .line 1611
    move-object/from16 v0, p0

    #@1a3
    invoke-super {v0, v8}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@1a6
    move-result v21

    #@1a7
    or-int v10, v10, v21

    #@1a9
    .line 1612
    const/16 v21, 0x1

    #@1ab
    move/from16 v0, v21

    #@1ad
    move-object/from16 v1, p0

    #@1af
    iput-boolean v0, v1, Landroid/view/ViewGroup;->mHoveredSelf:Z

    #@1b1
    goto/16 :goto_12b
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1380
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@3
    if-eqz v1, :cond_a

    #@5
    .line 1381
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@7
    invoke-virtual {v1, p1, v0}, Landroid/view/InputEventConsistencyVerifier;->onKeyEvent(Landroid/view/KeyEvent;I)V

    #@a
    .line 1384
    :cond_a
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@c
    and-int/lit8 v1, v1, 0x12

    #@e
    const/16 v2, 0x12

    #@10
    if-ne v1, v2, :cond_19

    #@12
    .line 1386
    invoke-super {p0, p1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_2f

    #@18
    .line 1399
    :cond_18
    :goto_18
    return v0

    #@19
    .line 1389
    :cond_19
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1b
    if-eqz v1, :cond_2f

    #@1d
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1f
    iget v1, v1, Landroid/view/View;->mPrivateFlags:I

    #@21
    and-int/lit8 v1, v1, 0x10

    #@23
    const/16 v2, 0x10

    #@25
    if-ne v1, v2, :cond_2f

    #@27
    .line 1391
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@29
    invoke-virtual {v1, p1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_18

    #@2f
    .line 1396
    :cond_2f
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@31
    if-eqz v1, :cond_38

    #@33
    .line 1397
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@35
    invoke-virtual {v1, p1, v0}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@38
    .line 1399
    :cond_38
    const/4 v0, 0x0

    #@39
    goto :goto_18
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1365
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x12

    #@4
    const/16 v1, 0x12

    #@6
    if-ne v0, v1, :cond_d

    #@8
    .line 1367
    invoke-super {p0, p1}, Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    #@b
    move-result v0

    #@c
    .line 1372
    :goto_c
    return v0

    #@d
    .line 1368
    :cond_d
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@f
    if-eqz v0, :cond_22

    #@11
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@13
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@15
    and-int/lit8 v0, v0, 0x10

    #@17
    const/16 v1, 0x10

    #@19
    if-ne v0, v1, :cond_22

    #@1b
    .line 1370
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1d
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    #@20
    move-result v0

    #@21
    goto :goto_c

    #@22
    .line 1372
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_c
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 1407
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x12

    #@4
    const/16 v1, 0x12

    #@6
    if-ne v0, v1, :cond_d

    #@8
    .line 1409
    invoke-super {p0, p1}, Landroid/view/View;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@b
    move-result v0

    #@c
    .line 1414
    :goto_c
    return v0

    #@d
    .line 1410
    :cond_d
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@f
    if-eqz v0, :cond_22

    #@11
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@13
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@15
    and-int/lit8 v0, v0, 0x10

    #@17
    const/16 v1, 0x10

    #@19
    if-ne v0, v1, :cond_22

    #@1b
    .line 1412
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1d
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@20
    move-result v0

    #@21
    goto :goto_c

    #@22
    .line 1414
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_c
.end method

.method dispatchPopulateAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 2480
    const/4 v3, 0x0

    #@1
    .line 2481
    .local v3, handled:Z
    invoke-virtual {p0}, Landroid/view/ViewGroup;->includeForAccessibility()Z

    #@4
    move-result v5

    #@5
    if-eqz v5, :cond_f

    #@7
    .line 2482
    invoke-super {p0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@a
    move-result v3

    #@b
    .line 2483
    if-eqz v3, :cond_f

    #@d
    move v5, v3

    #@e
    .line 2503
    :goto_e
    return v5

    #@f
    .line 2488
    :cond_f
    const/4 v5, 0x1

    #@10
    invoke-static {p0, v5}, Landroid/view/ViewGroup$ChildListForAccessibility;->obtain(Landroid/view/ViewGroup;Z)Landroid/view/ViewGroup$ChildListForAccessibility;

    #@13
    move-result-object v2

    #@14
    .line 2490
    .local v2, children:Landroid/view/ViewGroup$ChildListForAccessibility;
    :try_start_14
    invoke-virtual {v2}, Landroid/view/ViewGroup$ChildListForAccessibility;->getChildCount()I

    #@17
    move-result v1

    #@18
    .line 2491
    .local v1, childCount:I
    const/4 v4, 0x0

    #@19
    .local v4, i:I
    :goto_19
    if-ge v4, v1, :cond_33

    #@1b
    .line 2492
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup$ChildListForAccessibility;->getChildAt(I)Landroid/view/View;

    #@1e
    move-result-object v0

    #@1f
    .line 2493
    .local v0, child:Landroid/view/View;
    iget v5, v0, Landroid/view/View;->mViewFlags:I

    #@21
    and-int/lit8 v5, v5, 0xc

    #@23
    if-nez v5, :cond_30

    #@25
    .line 2494
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    :try_end_28
    .catchall {:try_start_14 .. :try_end_28} :catchall_38

    #@28
    move-result v3

    #@29
    .line 2495
    if-eqz v3, :cond_30

    #@2b
    .line 2501
    invoke-virtual {v2}, Landroid/view/ViewGroup$ChildListForAccessibility;->recycle()V

    #@2e
    move v5, v3

    #@2f
    goto :goto_e

    #@30
    .line 2491
    :cond_30
    add-int/lit8 v4, v4, 0x1

    #@32
    goto :goto_19

    #@33
    .line 2501
    .end local v0           #child:Landroid/view/View;
    :cond_33
    invoke-virtual {v2}, Landroid/view/ViewGroup$ChildListForAccessibility;->recycle()V

    #@36
    .line 2503
    const/4 v5, 0x0

    #@37
    goto :goto_e

    #@38
    .line 2501
    .end local v1           #childCount:I
    .end local v4           #i:I
    :catchall_38
    move-exception v5

    #@39
    invoke-virtual {v2}, Landroid/view/ViewGroup$ChildListForAccessibility;->recycle()V

    #@3c
    throw v5
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    const/high16 v5, 0x2000

    #@2
    .line 2622
    invoke-super {p0, p1}, Landroid/view/View;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    #@5
    .line 2623
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@7
    .line 2624
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@9
    .line 2625
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@a
    .local v3, i:I
    :goto_a
    if-ge v3, v2, :cond_19

    #@c
    .line 2626
    aget-object v0, v1, v3

    #@e
    .line 2627
    .local v0, c:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@10
    and-int/2addr v4, v5

    #@11
    if-eq v4, v5, :cond_16

    #@13
    .line 2628
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    #@16
    .line 2625
    :cond_16
    add-int/lit8 v3, v3, 0x1

    #@18
    goto :goto_a

    #@19
    .line 2631
    .end local v0           #c:Landroid/view/View;
    :cond_19
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    const/high16 v5, 0x2000

    #@2
    .line 2594
    invoke-super {p0, p1}, Landroid/view/View;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    #@5
    .line 2595
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@7
    .line 2596
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@9
    .line 2597
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@a
    .local v3, i:I
    :goto_a
    if-ge v3, v2, :cond_19

    #@c
    .line 2598
    aget-object v0, v1, v3

    #@e
    .line 2599
    .local v0, c:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@10
    and-int/2addr v4, v5

    #@11
    if-eq v4, v5, :cond_16

    #@13
    .line 2600
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    #@16
    .line 2597
    :cond_16
    add-int/lit8 v3, v3, 0x1

    #@18
    goto :goto_a

    #@19
    .line 2603
    .end local v0           #c:Landroid/view/View;
    :cond_19
    return-void
.end method

.method dispatchScreenStateChanged(I)V
    .registers 6
    .parameter "screenState"

    #@0
    .prologue
    .line 2469
    invoke-super {p0, p1}, Landroid/view/View;->dispatchScreenStateChanged(I)V

    #@3
    .line 2471
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 2472
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 2473
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 2474
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchScreenStateChanged(I)V

    #@f
    .line 2473
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 2476
    :cond_12
    return-void
.end method

.method public dispatchSetActivated(Z)V
    .registers 6
    .parameter "activated"

    #@0
    .prologue
    .line 3021
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@2
    .line 3022
    .local v0, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4
    .line 3023
    .local v1, count:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_f

    #@7
    .line 3024
    aget-object v3, v0, v2

    #@9
    invoke-virtual {v3, p1}, Landroid/view/View;->setActivated(Z)V

    #@c
    .line 3023
    add-int/lit8 v2, v2, 0x1

    #@e
    goto :goto_5

    #@f
    .line 3026
    :cond_f
    return-void
.end method

.method protected dispatchSetPressed(Z)V
    .registers 7
    .parameter "pressed"

    #@0
    .prologue
    .line 3030
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@2
    .line 3031
    .local v1, children:[Landroid/view/View;
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4
    .line 3032
    .local v2, count:I
    const/4 v3, 0x0

    #@5
    .local v3, i:I
    :goto_5
    if-ge v3, v2, :cond_1d

    #@7
    .line 3033
    aget-object v0, v1, v3

    #@9
    .line 3037
    .local v0, child:Landroid/view/View;
    if-eqz p1, :cond_17

    #@b
    invoke-virtual {v0}, Landroid/view/View;->isClickable()Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_1a

    #@11
    invoke-virtual {v0}, Landroid/view/View;->isLongClickable()Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_1a

    #@17
    .line 3038
    :cond_17
    invoke-virtual {v0, p1}, Landroid/view/View;->setPressed(Z)V

    #@1a
    .line 3032
    :cond_1a
    add-int/lit8 v3, v3, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 3041
    .end local v0           #child:Landroid/view/View;
    :cond_1d
    return-void
.end method

.method public dispatchSetSelected(Z)V
    .registers 6
    .parameter "selected"

    #@0
    .prologue
    .line 3009
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@2
    .line 3010
    .local v0, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4
    .line 3011
    .local v1, count:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_f

    #@7
    .line 3012
    aget-object v3, v0, v2

    #@9
    invoke-virtual {v3, p1}, Landroid/view/View;->setSelected(Z)V

    #@c
    .line 3011
    add-int/lit8 v2, v2, 0x1

    #@e
    goto :goto_5

    #@f
    .line 3014
    :cond_f
    return-void
.end method

.method public dispatchStartTemporaryDetach()V
    .registers 5

    #@0
    .prologue
    .line 2426
    invoke-super {p0}, Landroid/view/View;->dispatchStartTemporaryDetach()V

    #@3
    .line 2427
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 2428
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 2429
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 2430
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3}, Landroid/view/View;->dispatchStartTemporaryDetach()V

    #@f
    .line 2429
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 2432
    :cond_12
    return-void
.end method

.method public dispatchSystemUiVisibilityChanged(I)V
    .registers 6
    .parameter "visible"

    #@0
    .prologue
    .line 1337
    invoke-super {p0, p1}, Landroid/view/View;->dispatchSystemUiVisibilityChanged(I)V

    #@3
    .line 1339
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 1340
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 1341
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v2, :cond_12

    #@a
    .line 1342
    aget-object v0, v1, v3

    #@c
    .line 1343
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchSystemUiVisibilityChanged(I)V

    #@f
    .line 1341
    add-int/lit8 v3, v3, 0x1

    #@11
    goto :goto_8

    #@12
    .line 1345
    .end local v0           #child:Landroid/view/View;
    :cond_12
    return-void
.end method

.method protected dispatchThawSelfOnly(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2642
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-super {p0, p1}, Landroid/view/View;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    #@3
    .line 2643
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 32
    .parameter "ev"

    #@0
    .prologue
    .line 1827
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@4
    move-object/from16 v28, v0

    #@6
    if-eqz v28, :cond_19

    #@8
    .line 1828
    move-object/from16 v0, p0

    #@a
    iget-object v0, v0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@c
    move-object/from16 v28, v0

    #@e
    const/16 v29, 0x1

    #@10
    move-object/from16 v0, v28

    #@12
    move-object/from16 v1, p1

    #@14
    move/from16 v2, v29

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    #@19
    .line 1831
    :cond_19
    const/16 v16, 0x0

    #@1b
    .line 1832
    .local v16, handled:Z
    invoke-virtual/range {p0 .. p1}, Landroid/view/ViewGroup;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    #@1e
    move-result v28

    #@1f
    if-eqz v28, :cond_1a0

    #@21
    .line 1833
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@24
    move-result v4

    #@25
    .line 1834
    .local v4, action:I
    and-int/lit16 v6, v4, 0xff

    #@27
    .line 1837
    .local v6, actionMasked:I
    if-nez v6, :cond_2f

    #@29
    .line 1841
    invoke-direct/range {p0 .. p1}, Landroid/view/ViewGroup;->cancelAndClearTouchTargets(Landroid/view/MotionEvent;)V

    #@2c
    .line 1842
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewGroup;->resetTouchState()V

    #@2f
    .line 1847
    :cond_2f
    if-eqz v6, :cond_39

    #@31
    move-object/from16 v0, p0

    #@33
    iget-object v0, v0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@35
    move-object/from16 v28, v0

    #@37
    if-eqz v28, :cond_e4

    #@39
    .line 1849
    :cond_39
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@3d
    move/from16 v28, v0

    #@3f
    const/high16 v29, 0x8

    #@41
    and-int v28, v28, v29

    #@43
    if-eqz v28, :cond_dd

    #@45
    const/4 v15, 0x1

    #@46
    .line 1850
    .local v15, disallowIntercept:Z
    :goto_46
    if-nez v15, :cond_e0

    #@48
    .line 1851
    invoke-virtual/range {p0 .. p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@4b
    move-result v20

    #@4c
    .line 1852
    .local v20, intercepted:Z
    move-object/from16 v0, p1

    #@4e
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@51
    .line 1863
    .end local v15           #disallowIntercept:Z
    :goto_51
    invoke-static/range {p0 .. p0}, Landroid/view/ViewGroup;->resetCancelNextUpFlag(Landroid/view/View;)Z

    #@54
    move-result v28

    #@55
    if-nez v28, :cond_5d

    #@57
    const/16 v28, 0x3

    #@59
    move/from16 v0, v28

    #@5b
    if-ne v6, v0, :cond_e8

    #@5d
    :cond_5d
    const/4 v9, 0x1

    #@5e
    .line 1867
    .local v9, canceled:Z
    :goto_5e
    move-object/from16 v0, p0

    #@60
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@62
    move/from16 v28, v0

    #@64
    const/high16 v29, 0x20

    #@66
    and-int v28, v28, v29

    #@68
    if-eqz v28, :cond_eb

    #@6a
    const/16 v24, 0x1

    #@6c
    .line 1868
    .local v24, split:Z
    :goto_6c
    const/16 v21, 0x0

    #@6e
    .line 1869
    .local v21, newTouchTarget:Landroid/view/ViewGroup$TouchTarget;
    const/4 v7, 0x0

    #@6f
    .line 1870
    .local v7, alreadyDispatchedToNewTouchTarget:Z
    if-nez v9, :cond_177

    #@71
    if-nez v20, :cond_177

    #@73
    .line 1871
    if-eqz v6, :cond_83

    #@75
    if-eqz v24, :cond_7d

    #@77
    const/16 v28, 0x5

    #@79
    move/from16 v0, v28

    #@7b
    if-eq v6, v0, :cond_83

    #@7d
    :cond_7d
    const/16 v28, 0x7

    #@7f
    move/from16 v0, v28

    #@81
    if-ne v6, v0, :cond_177

    #@83
    .line 1874
    :cond_83
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@86
    move-result v5

    #@87
    .line 1875
    .local v5, actionIndex:I
    if-eqz v24, :cond_ef

    #@89
    const/16 v28, 0x1

    #@8b
    move-object/from16 v0, p1

    #@8d
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@90
    move-result v29

    #@91
    shl-int v18, v28, v29

    #@93
    .line 1880
    .local v18, idBitsToAssign:I
    :goto_93
    move-object/from16 v0, p0

    #@95
    move/from16 v1, v18

    #@97
    invoke-direct {v0, v1}, Landroid/view/ViewGroup;->removePointersFromTouchTargets(I)V

    #@9a
    .line 1882
    move-object/from16 v0, p0

    #@9c
    iget v13, v0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@9e
    .line 1883
    .local v13, childrenCount:I
    if-eqz v13, :cond_10b

    #@a0
    .line 1886
    move-object/from16 v0, p0

    #@a2
    iget-object v12, v0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@a4
    .line 1887
    .local v12, children:[Landroid/view/View;
    move-object/from16 v0, p1

    #@a6
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    #@a9
    move-result v26

    #@aa
    .line 1888
    .local v26, x:F
    move-object/from16 v0, p1

    #@ac
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getY(I)F

    #@af
    move-result v27

    #@b0
    .line 1890
    .local v27, y:F
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->isChildrenDrawingOrderEnabled()Z

    #@b3
    move-result v14

    #@b4
    .line 1891
    .local v14, customOrder:Z
    add-int/lit8 v17, v13, -0x1

    #@b6
    .local v17, i:I
    :goto_b6
    if-ltz v17, :cond_10b

    #@b8
    .line 1892
    if-eqz v14, :cond_f2

    #@ba
    move-object/from16 v0, p0

    #@bc
    move/from16 v1, v17

    #@be
    invoke-virtual {v0, v13, v1}, Landroid/view/ViewGroup;->getChildDrawingOrder(II)I

    #@c1
    move-result v11

    #@c2
    .line 1894
    .local v11, childIndex:I
    :goto_c2
    aget-object v10, v12, v11

    #@c4
    .line 1895
    .local v10, child:Landroid/view/View;
    invoke-static {v10}, Landroid/view/ViewGroup;->canViewReceivePointerEvents(Landroid/view/View;)Z

    #@c7
    move-result v28

    #@c8
    if-eqz v28, :cond_da

    #@ca
    const/16 v28, 0x0

    #@cc
    move-object/from16 v0, p0

    #@ce
    move/from16 v1, v26

    #@d0
    move/from16 v2, v27

    #@d2
    move-object/from16 v3, v28

    #@d4
    invoke-virtual {v0, v1, v2, v10, v3}, Landroid/view/ViewGroup;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    #@d7
    move-result v28

    #@d8
    if-nez v28, :cond_f5

    #@da
    .line 1891
    :cond_da
    add-int/lit8 v17, v17, -0x1

    #@dc
    goto :goto_b6

    #@dd
    .line 1849
    .end local v5           #actionIndex:I
    .end local v7           #alreadyDispatchedToNewTouchTarget:Z
    .end local v9           #canceled:Z
    .end local v10           #child:Landroid/view/View;
    .end local v11           #childIndex:I
    .end local v12           #children:[Landroid/view/View;
    .end local v13           #childrenCount:I
    .end local v14           #customOrder:Z
    .end local v17           #i:I
    .end local v18           #idBitsToAssign:I
    .end local v20           #intercepted:Z
    .end local v21           #newTouchTarget:Landroid/view/ViewGroup$TouchTarget;
    .end local v24           #split:Z
    .end local v26           #x:F
    .end local v27           #y:F
    :cond_dd
    const/4 v15, 0x0

    #@de
    goto/16 :goto_46

    #@e0
    .line 1854
    .restart local v15       #disallowIntercept:Z
    :cond_e0
    const/16 v20, 0x0

    #@e2
    .restart local v20       #intercepted:Z
    goto/16 :goto_51

    #@e4
    .line 1859
    .end local v15           #disallowIntercept:Z
    .end local v20           #intercepted:Z
    :cond_e4
    const/16 v20, 0x1

    #@e6
    .restart local v20       #intercepted:Z
    goto/16 :goto_51

    #@e8
    .line 1863
    :cond_e8
    const/4 v9, 0x0

    #@e9
    goto/16 :goto_5e

    #@eb
    .line 1867
    .restart local v9       #canceled:Z
    :cond_eb
    const/16 v24, 0x0

    #@ed
    goto/16 :goto_6c

    #@ef
    .line 1875
    .restart local v5       #actionIndex:I
    .restart local v7       #alreadyDispatchedToNewTouchTarget:Z
    .restart local v21       #newTouchTarget:Landroid/view/ViewGroup$TouchTarget;
    .restart local v24       #split:Z
    :cond_ef
    const/16 v18, -0x1

    #@f1
    goto :goto_93

    #@f2
    .restart local v12       #children:[Landroid/view/View;
    .restart local v13       #childrenCount:I
    .restart local v14       #customOrder:Z
    .restart local v17       #i:I
    .restart local v18       #idBitsToAssign:I
    .restart local v26       #x:F
    .restart local v27       #y:F
    :cond_f2
    move/from16 v11, v17

    #@f4
    .line 1892
    goto :goto_c2

    #@f5
    .line 1900
    .restart local v10       #child:Landroid/view/View;
    .restart local v11       #childIndex:I
    :cond_f5
    move-object/from16 v0, p0

    #@f7
    invoke-direct {v0, v10}, Landroid/view/ViewGroup;->getTouchTarget(Landroid/view/View;)Landroid/view/ViewGroup$TouchTarget;

    #@fa
    move-result-object v21

    #@fb
    .line 1901
    if-eqz v21, :cond_12a

    #@fd
    .line 1904
    move-object/from16 v0, v21

    #@ff
    iget v0, v0, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@101
    move/from16 v28, v0

    #@103
    or-int v28, v28, v18

    #@105
    move/from16 v0, v28

    #@107
    move-object/from16 v1, v21

    #@109
    iput v0, v1, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@10b
    .line 1922
    .end local v10           #child:Landroid/view/View;
    .end local v11           #childIndex:I
    .end local v12           #children:[Landroid/view/View;
    .end local v14           #customOrder:Z
    .end local v17           #i:I
    .end local v26           #x:F
    .end local v27           #y:F
    :cond_10b
    :goto_10b
    if-nez v21, :cond_177

    #@10d
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@111
    move-object/from16 v28, v0

    #@113
    if-eqz v28, :cond_177

    #@115
    .line 1925
    move-object/from16 v0, p0

    #@117
    iget-object v0, v0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@119
    move-object/from16 v21, v0

    #@11b
    .line 1926
    :goto_11b
    move-object/from16 v0, v21

    #@11d
    iget-object v0, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@11f
    move-object/from16 v28, v0

    #@121
    if-eqz v28, :cond_169

    #@123
    .line 1927
    move-object/from16 v0, v21

    #@125
    iget-object v0, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@127
    move-object/from16 v21, v0

    #@129
    goto :goto_11b

    #@12a
    .line 1908
    .restart local v10       #child:Landroid/view/View;
    .restart local v11       #childIndex:I
    .restart local v12       #children:[Landroid/view/View;
    .restart local v14       #customOrder:Z
    .restart local v17       #i:I
    .restart local v26       #x:F
    .restart local v27       #y:F
    :cond_12a
    invoke-static {v10}, Landroid/view/ViewGroup;->resetCancelNextUpFlag(Landroid/view/View;)Z

    #@12d
    .line 1909
    const/16 v28, 0x0

    #@12f
    move-object/from16 v0, p0

    #@131
    move-object/from16 v1, p1

    #@133
    move/from16 v2, v28

    #@135
    move/from16 v3, v18

    #@137
    invoke-direct {v0, v1, v2, v10, v3}, Landroid/view/ViewGroup;->dispatchTransformedTouchEvent(Landroid/view/MotionEvent;ZLandroid/view/View;I)Z

    #@13a
    move-result v28

    #@13b
    if-eqz v28, :cond_da

    #@13d
    .line 1911
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    #@140
    move-result-wide v28

    #@141
    move-wide/from16 v0, v28

    #@143
    move-object/from16 v2, p0

    #@145
    iput-wide v0, v2, Landroid/view/ViewGroup;->mLastTouchDownTime:J

    #@147
    .line 1912
    move-object/from16 v0, p0

    #@149
    iput v11, v0, Landroid/view/ViewGroup;->mLastTouchDownIndex:I

    #@14b
    .line 1913
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@14e
    move-result v28

    #@14f
    move/from16 v0, v28

    #@151
    move-object/from16 v1, p0

    #@153
    iput v0, v1, Landroid/view/ViewGroup;->mLastTouchDownX:F

    #@155
    .line 1914
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@158
    move-result v28

    #@159
    move/from16 v0, v28

    #@15b
    move-object/from16 v1, p0

    #@15d
    iput v0, v1, Landroid/view/ViewGroup;->mLastTouchDownY:F

    #@15f
    .line 1915
    move-object/from16 v0, p0

    #@161
    move/from16 v1, v18

    #@163
    invoke-direct {v0, v10, v1}, Landroid/view/ViewGroup;->addTouchTarget(Landroid/view/View;I)Landroid/view/ViewGroup$TouchTarget;

    #@166
    move-result-object v21

    #@167
    .line 1916
    const/4 v7, 0x1

    #@168
    .line 1917
    goto :goto_10b

    #@169
    .line 1929
    .end local v10           #child:Landroid/view/View;
    .end local v11           #childIndex:I
    .end local v12           #children:[Landroid/view/View;
    .end local v14           #customOrder:Z
    .end local v17           #i:I
    .end local v26           #x:F
    .end local v27           #y:F
    :cond_169
    move-object/from16 v0, v21

    #@16b
    iget v0, v0, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@16d
    move/from16 v28, v0

    #@16f
    or-int v28, v28, v18

    #@171
    move/from16 v0, v28

    #@173
    move-object/from16 v1, v21

    #@175
    iput v0, v1, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@177
    .line 1935
    .end local v5           #actionIndex:I
    .end local v13           #childrenCount:I
    .end local v18           #idBitsToAssign:I
    :cond_177
    move-object/from16 v0, p0

    #@179
    iget-object v0, v0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@17b
    move-object/from16 v28, v0

    #@17d
    if-nez v28, :cond_1bc

    #@17f
    .line 1937
    const/16 v28, 0x0

    #@181
    const/16 v29, -0x1

    #@183
    move-object/from16 v0, p0

    #@185
    move-object/from16 v1, p1

    #@187
    move-object/from16 v2, v28

    #@189
    move/from16 v3, v29

    #@18b
    invoke-direct {v0, v1, v9, v2, v3}, Landroid/view/ViewGroup;->dispatchTransformedTouchEvent(Landroid/view/MotionEvent;ZLandroid/view/View;I)Z

    #@18e
    move-result v16

    #@18f
    .line 1972
    :cond_18f
    if-nez v9, :cond_19d

    #@191
    const/16 v28, 0x1

    #@193
    move/from16 v0, v28

    #@195
    if-eq v6, v0, :cond_19d

    #@197
    const/16 v28, 0x7

    #@199
    move/from16 v0, v28

    #@19b
    if-ne v6, v0, :cond_21f

    #@19d
    .line 1975
    :cond_19d
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewGroup;->resetTouchState()V

    #@1a0
    .line 1983
    .end local v4           #action:I
    .end local v6           #actionMasked:I
    .end local v7           #alreadyDispatchedToNewTouchTarget:Z
    .end local v9           #canceled:Z
    .end local v20           #intercepted:Z
    .end local v21           #newTouchTarget:Landroid/view/ViewGroup$TouchTarget;
    .end local v24           #split:Z
    :cond_1a0
    :goto_1a0
    if-nez v16, :cond_1bb

    #@1a2
    move-object/from16 v0, p0

    #@1a4
    iget-object v0, v0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@1a6
    move-object/from16 v28, v0

    #@1a8
    if-eqz v28, :cond_1bb

    #@1aa
    .line 1984
    move-object/from16 v0, p0

    #@1ac
    iget-object v0, v0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@1ae
    move-object/from16 v28, v0

    #@1b0
    const/16 v29, 0x1

    #@1b2
    move-object/from16 v0, v28

    #@1b4
    move-object/from16 v1, p1

    #@1b6
    move/from16 v2, v29

    #@1b8
    invoke-virtual {v0, v1, v2}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@1bb
    .line 1986
    :cond_1bb
    return v16

    #@1bc
    .line 1942
    .restart local v4       #action:I
    .restart local v6       #actionMasked:I
    .restart local v7       #alreadyDispatchedToNewTouchTarget:Z
    .restart local v9       #canceled:Z
    .restart local v20       #intercepted:Z
    .restart local v21       #newTouchTarget:Landroid/view/ViewGroup$TouchTarget;
    .restart local v24       #split:Z
    :cond_1bc
    const/16 v23, 0x0

    #@1be
    .line 1943
    .local v23, predecessor:Landroid/view/ViewGroup$TouchTarget;
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@1c2
    move-object/from16 v25, v0

    #@1c4
    .line 1944
    .local v25, target:Landroid/view/ViewGroup$TouchTarget;
    :goto_1c4
    if-eqz v25, :cond_18f

    #@1c6
    .line 1945
    move-object/from16 v0, v25

    #@1c8
    iget-object v0, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@1ca
    move-object/from16 v22, v0

    #@1cc
    .line 1946
    .local v22, next:Landroid/view/ViewGroup$TouchTarget;
    if-eqz v7, :cond_1db

    #@1ce
    move-object/from16 v0, v25

    #@1d0
    move-object/from16 v1, v21

    #@1d2
    if-ne v0, v1, :cond_1db

    #@1d4
    .line 1947
    const/16 v16, 0x1

    #@1d6
    .line 1966
    :cond_1d6
    move-object/from16 v23, v25

    #@1d8
    .line 1967
    move-object/from16 v25, v22

    #@1da
    .line 1968
    goto :goto_1c4

    #@1db
    .line 1949
    :cond_1db
    move-object/from16 v0, v25

    #@1dd
    iget-object v0, v0, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@1df
    move-object/from16 v28, v0

    #@1e1
    invoke-static/range {v28 .. v28}, Landroid/view/ViewGroup;->resetCancelNextUpFlag(Landroid/view/View;)Z

    #@1e4
    move-result v28

    #@1e5
    if-nez v28, :cond_1e9

    #@1e7
    if-eqz v20, :cond_216

    #@1e9
    :cond_1e9
    const/4 v8, 0x1

    #@1ea
    .line 1951
    .local v8, cancelChild:Z
    :goto_1ea
    move-object/from16 v0, v25

    #@1ec
    iget-object v0, v0, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@1ee
    move-object/from16 v28, v0

    #@1f0
    move-object/from16 v0, v25

    #@1f2
    iget v0, v0, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@1f4
    move/from16 v29, v0

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    move-object/from16 v1, p1

    #@1fa
    move-object/from16 v2, v28

    #@1fc
    move/from16 v3, v29

    #@1fe
    invoke-direct {v0, v1, v8, v2, v3}, Landroid/view/ViewGroup;->dispatchTransformedTouchEvent(Landroid/view/MotionEvent;ZLandroid/view/View;I)Z

    #@201
    move-result v28

    #@202
    if-eqz v28, :cond_206

    #@204
    .line 1953
    const/16 v16, 0x1

    #@206
    .line 1955
    :cond_206
    if-eqz v8, :cond_1d6

    #@208
    .line 1956
    if-nez v23, :cond_218

    #@20a
    .line 1957
    move-object/from16 v0, v22

    #@20c
    move-object/from16 v1, p0

    #@20e
    iput-object v0, v1, Landroid/view/ViewGroup;->mFirstTouchTarget:Landroid/view/ViewGroup$TouchTarget;

    #@210
    .line 1961
    :goto_210
    invoke-virtual/range {v25 .. v25}, Landroid/view/ViewGroup$TouchTarget;->recycle()V

    #@213
    .line 1962
    move-object/from16 v25, v22

    #@215
    .line 1963
    goto :goto_1c4

    #@216
    .line 1949
    .end local v8           #cancelChild:Z
    :cond_216
    const/4 v8, 0x0

    #@217
    goto :goto_1ea

    #@218
    .line 1959
    .restart local v8       #cancelChild:Z
    :cond_218
    move-object/from16 v0, v22

    #@21a
    move-object/from16 v1, v23

    #@21c
    iput-object v0, v1, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@21e
    goto :goto_210

    #@21f
    .line 1976
    .end local v8           #cancelChild:Z
    .end local v22           #next:Landroid/view/ViewGroup$TouchTarget;
    .end local v23           #predecessor:Landroid/view/ViewGroup$TouchTarget;
    .end local v25           #target:Landroid/view/ViewGroup$TouchTarget;
    :cond_21f
    if-eqz v24, :cond_1a0

    #@221
    const/16 v28, 0x6

    #@223
    move/from16 v0, v28

    #@225
    if-ne v6, v0, :cond_1a0

    #@227
    .line 1977
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@22a
    move-result v5

    #@22b
    .line 1978
    .restart local v5       #actionIndex:I
    const/16 v28, 0x1

    #@22d
    move-object/from16 v0, p1

    #@22f
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@232
    move-result v29

    #@233
    shl-int v19, v28, v29

    #@235
    .line 1979
    .local v19, idBitsToRemove:I
    move-object/from16 v0, p0

    #@237
    move/from16 v1, v19

    #@239
    invoke-direct {v0, v1}, Landroid/view/ViewGroup;->removePointersFromTouchTargets(I)V

    #@23c
    goto/16 :goto_1a0
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1422
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@3
    if-eqz v1, :cond_a

    #@5
    .line 1423
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@7
    invoke-virtual {v1, p1, v0}, Landroid/view/InputEventConsistencyVerifier;->onTrackballEvent(Landroid/view/MotionEvent;I)V

    #@a
    .line 1426
    :cond_a
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@c
    and-int/lit8 v1, v1, 0x12

    #@e
    const/16 v2, 0x12

    #@10
    if-ne v1, v2, :cond_19

    #@12
    .line 1428
    invoke-super {p0, p1}, Landroid/view/View;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_2f

    #@18
    .line 1441
    :cond_18
    :goto_18
    return v0

    #@19
    .line 1431
    :cond_19
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1b
    if-eqz v1, :cond_2f

    #@1d
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1f
    iget v1, v1, Landroid/view/View;->mPrivateFlags:I

    #@21
    and-int/lit8 v1, v1, 0x10

    #@23
    const/16 v2, 0x10

    #@25
    if-ne v1, v2, :cond_2f

    #@27
    .line 1433
    iget-object v1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@29
    invoke-virtual {v1, p1}, Landroid/view/View;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_18

    #@2f
    .line 1438
    :cond_2f
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@31
    if-eqz v1, :cond_38

    #@33
    .line 1439
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@35
    invoke-virtual {v1, p1, v0}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@38
    .line 1441
    :cond_38
    const/4 v0, 0x0

    #@39
    goto :goto_18
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 4
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 770
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@6
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->dispatchUnhandledMove(Landroid/view/View;I)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected dispatchVisibilityChanged(Landroid/view/View;I)V
    .registers 7
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 1053
    invoke-super {p0, p1, p2}, Landroid/view/View;->dispatchVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 1054
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 1055
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 1056
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 1057
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3, p1, p2}, Landroid/view/View;->dispatchVisibilityChanged(Landroid/view/View;I)V

    #@f
    .line 1056
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 1059
    :cond_12
    return-void
.end method

.method public dispatchWindowFocusChanged(Z)V
    .registers 6
    .parameter "hasFocus"

    #@0
    .prologue
    .line 961
    invoke-super {p0, p1}, Landroid/view/View;->dispatchWindowFocusChanged(Z)V

    #@3
    .line 962
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 963
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 964
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 965
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchWindowFocusChanged(Z)V

    #@f
    .line 964
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 967
    :cond_12
    return-void
.end method

.method public dispatchWindowSystemUiVisiblityChanged(I)V
    .registers 6
    .parameter "visible"

    #@0
    .prologue
    .line 1325
    invoke-super {p0, p1}, Landroid/view/View;->dispatchWindowSystemUiVisiblityChanged(I)V

    #@3
    .line 1327
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 1328
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 1329
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v2, :cond_12

    #@a
    .line 1330
    aget-object v0, v1, v3

    #@c
    .line 1331
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchWindowSystemUiVisiblityChanged(I)V

    #@f
    .line 1329
    add-int/lit8 v3, v3, 0x1

    #@11
    goto :goto_8

    #@12
    .line 1333
    .end local v0           #child:Landroid/view/View;
    :cond_12
    return-void
.end method

.method public dispatchWindowVisibilityChanged(I)V
    .registers 6
    .parameter "visibility"

    #@0
    .prologue
    .line 1066
    invoke-super {p0, p1}, Landroid/view/View;->dispatchWindowVisibilityChanged(I)V

    #@3
    .line 1067
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 1068
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 1069
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 1070
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchWindowVisibilityChanged(I)V

    #@f
    .line 1069
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 1072
    :cond_12
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 6
    .parameter "canvas"
    .parameter "child"
    .parameter "drawingTime"

    #@0
    .prologue
    .line 2968
    invoke-virtual {p2, p1, p0, p3, p4}, Landroid/view/View;->draw(Landroid/graphics/Canvas;Landroid/view/ViewGroup;J)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected drawableStateChanged()V
    .registers 7

    #@0
    .prologue
    .line 5172
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    #@3
    .line 5174
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5
    const/high16 v5, 0x1

    #@7
    and-int/2addr v4, v5

    #@8
    if-eqz v4, :cond_2e

    #@a
    .line 5175
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@c
    and-int/lit16 v4, v4, 0x2000

    #@e
    if-eqz v4, :cond_18

    #@10
    .line 5176
    new-instance v4, Ljava/lang/IllegalStateException;

    #@12
    const-string v5, "addStateFromChildren cannot be enabled if a child has duplicateParentState set to true"

    #@14
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v4

    #@18
    .line 5180
    :cond_18
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@1a
    .line 5181
    .local v1, children:[Landroid/view/View;
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@1c
    .line 5183
    .local v2, count:I
    const/4 v3, 0x0

    #@1d
    .local v3, i:I
    :goto_1d
    if-ge v3, v2, :cond_2e

    #@1f
    .line 5184
    aget-object v0, v1, v3

    #@21
    .line 5185
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@23
    const/high16 v5, 0x40

    #@25
    and-int/2addr v4, v5

    #@26
    if-eqz v4, :cond_2b

    #@28
    .line 5186
    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    #@2b
    .line 5183
    :cond_2b
    add-int/lit8 v3, v3, 0x1

    #@2d
    goto :goto_1d

    #@2e
    .line 5190
    .end local v0           #child:Landroid/view/View;
    .end local v1           #children:[Landroid/view/View;
    .end local v2           #count:I
    .end local v3           #i:I
    :cond_2e
    return-void
.end method

.method public endViewTransition(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 5059
    iget-object v1, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@2
    if-eqz v1, :cond_2a

    #@4
    .line 5060
    iget-object v1, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@9
    .line 5061
    iget-object v0, p0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@b
    .line 5062
    .local v0, disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz v0, :cond_2a

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_2a

    #@13
    .line 5063
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@16
    .line 5064
    iget-object v1, p0, Landroid/view/ViewGroup;->mVisibilityChangingChildren:Ljava/util/ArrayList;

    #@18
    if-eqz v1, :cond_2b

    #@1a
    iget-object v1, p0, Landroid/view/ViewGroup;->mVisibilityChangingChildren:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_2b

    #@22
    .line 5066
    iget-object v1, p0, Landroid/view/ViewGroup;->mVisibilityChangingChildren:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@27
    .line 5075
    :cond_27
    :goto_27
    invoke-virtual {p0}, Landroid/view/ViewGroup;->invalidate()V

    #@2a
    .line 5078
    .end local v0           #disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_2a
    return-void

    #@2b
    .line 5068
    .restart local v0       #disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_2b
    iget-object v1, p1, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2d
    if-eqz v1, :cond_32

    #@2f
    .line 5069
    invoke-virtual {p1}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@32
    .line 5071
    :cond_32
    iget-object v1, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@34
    if-eqz v1, :cond_27

    #@36
    .line 5072
    const/4 v1, 0x0

    #@37
    iput-object v1, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@39
    goto :goto_27
.end method

.method public findFocus()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 853
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isFocused()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 860
    .end local p0
    :goto_6
    return-object p0

    #@7
    .line 857
    .restart local p0
    :cond_7
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 858
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@d
    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@10
    move-result-object p0

    #@11
    goto :goto_6

    #@12
    .line 860
    :cond_12
    const/4 p0, 0x0

    #@13
    goto :goto_6
.end method

.method findFrontmostDroppableChildAt(FFLandroid/graphics/PointF;)Landroid/view/View;
    .registers 9
    .parameter "x"
    .parameter "y"
    .parameter "outLocalPoint"

    #@0
    .prologue
    .line 1291
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    .line 1292
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@4
    .line 1293
    .local v1, children:[Landroid/view/View;
    add-int/lit8 v3, v2, -0x1

    #@6
    .local v3, i:I
    :goto_6
    if-ltz v3, :cond_1a

    #@8
    .line 1294
    aget-object v0, v1, v3

    #@a
    .line 1295
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->canAcceptDrag()Z

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_13

    #@10
    .line 1293
    :cond_10
    add-int/lit8 v3, v3, -0x1

    #@12
    goto :goto_6

    #@13
    .line 1299
    :cond_13
    invoke-virtual {p0, p1, p2, v0, p3}, Landroid/view/ViewGroup;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_10

    #@19
    .line 1303
    .end local v0           #child:Landroid/view/View;
    :goto_19
    return-object v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method findViewByAccessibilityIdTraversal(I)Landroid/view/View;
    .registers 8
    .parameter "accessibilityId"

    #@0
    .prologue
    .line 940
    invoke-super {p0, p1}, Landroid/view/View;->findViewByAccessibilityIdTraversal(I)Landroid/view/View;

    #@3
    move-result-object v3

    #@4
    .line 941
    .local v3, foundView:Landroid/view/View;
    if-eqz v3, :cond_8

    #@6
    move-object v5, v3

    #@7
    .line 953
    :goto_7
    return-object v5

    #@8
    .line 944
    :cond_8
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@a
    .line 945
    .local v2, childrenCount:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@c
    .line 946
    .local v1, children:[Landroid/view/View;
    const/4 v4, 0x0

    #@d
    .local v4, i:I
    :goto_d
    if-ge v4, v2, :cond_1c

    #@f
    .line 947
    aget-object v0, v1, v4

    #@11
    .line 948
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewByAccessibilityIdTraversal(I)Landroid/view/View;

    #@14
    move-result-object v3

    #@15
    .line 949
    if-eqz v3, :cond_19

    #@17
    move-object v5, v3

    #@18
    .line 950
    goto :goto_7

    #@19
    .line 946
    :cond_19
    add-int/lit8 v4, v4, 0x1

    #@1b
    goto :goto_d

    #@1c
    .line 953
    .end local v0           #child:Landroid/view/View;
    :cond_1c
    const/4 v5, 0x0

    #@1d
    goto :goto_7
.end method

.method protected findViewByPredicateTraversal(Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter "childToSkip"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 3136
    .local p1, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/View;>;"
    invoke-interface {p1, p0}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_7

    #@6
    .line 3155
    .end local p0
    :goto_6
    return-object p0

    #@7
    .line 3140
    .restart local p0
    :cond_7
    iget-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@9
    .line 3141
    .local v3, where:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@b
    .line 3143
    .local v1, len:I
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    if-ge v0, v1, :cond_23

    #@e
    .line 3144
    aget-object v2, v3, v0

    #@10
    .line 3146
    .local v2, v:Landroid/view/View;
    if-eq v2, p2, :cond_20

    #@12
    iget v4, v2, Landroid/view/View;->mPrivateFlags:I

    #@14
    and-int/lit8 v4, v4, 0x8

    #@16
    if-nez v4, :cond_20

    #@18
    .line 3147
    invoke-virtual {v2, p1}, Landroid/view/View;->findViewByPredicate(Lcom/android/internal/util/Predicate;)Landroid/view/View;

    #@1b
    move-result-object v2

    #@1c
    .line 3149
    if-eqz v2, :cond_20

    #@1e
    move-object p0, v2

    #@1f
    .line 3150
    goto :goto_6

    #@20
    .line 3143
    :cond_20
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_c

    #@23
    .line 3155
    .end local v2           #v:Landroid/view/View;
    :cond_23
    const/4 p0, 0x0

    #@24
    goto :goto_6
.end method

.method protected findViewTraversal(I)Landroid/view/View;
    .registers 7
    .parameter "id"

    #@0
    .prologue
    .line 3082
    iget v4, p0, Landroid/view/View;->mID:I

    #@2
    if-ne p1, v4, :cond_5

    #@4
    .line 3101
    .end local p0
    :goto_4
    return-object p0

    #@5
    .line 3086
    .restart local p0
    :cond_5
    iget-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 3087
    .local v3, where:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@9
    .line 3089
    .local v1, len:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_1f

    #@c
    .line 3090
    aget-object v2, v3, v0

    #@e
    .line 3092
    .local v2, v:Landroid/view/View;
    iget v4, v2, Landroid/view/View;->mPrivateFlags:I

    #@10
    and-int/lit8 v4, v4, 0x8

    #@12
    if-nez v4, :cond_1c

    #@14
    .line 3093
    invoke-virtual {v2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@17
    move-result-object v2

    #@18
    .line 3095
    if-eqz v2, :cond_1c

    #@1a
    move-object p0, v2

    #@1b
    .line 3096
    goto :goto_4

    #@1c
    .line 3089
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_a

    #@1f
    .line 3101
    .end local v2           #v:Landroid/view/View;
    :cond_1f
    const/4 p0, 0x0

    #@20
    goto :goto_4
.end method

.method protected findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;
    .registers 7
    .parameter "tag"

    #@0
    .prologue
    .line 3109
    if-eqz p1, :cond_b

    #@2
    iget-object v4, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@4
    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_b

    #@a
    .line 3128
    .end local p0
    :goto_a
    return-object p0

    #@b
    .line 3113
    .restart local p0
    :cond_b
    iget-object v3, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@d
    .line 3114
    .local v3, where:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@f
    .line 3116
    .local v1, len:I
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    if-ge v0, v1, :cond_25

    #@12
    .line 3117
    aget-object v2, v3, v0

    #@14
    .line 3119
    .local v2, v:Landroid/view/View;
    iget v4, v2, Landroid/view/View;->mPrivateFlags:I

    #@16
    and-int/lit8 v4, v4, 0x8

    #@18
    if-nez v4, :cond_22

    #@1a
    .line 3120
    invoke-virtual {v2, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    #@1d
    move-result-object v2

    #@1e
    .line 3122
    if-eqz v2, :cond_22

    #@20
    move-object p0, v2

    #@21
    .line 3123
    goto :goto_a

    #@22
    .line 3116
    :cond_22
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_10

    #@25
    .line 3128
    .end local v2           #v:Landroid/view/View;
    :cond_25
    const/4 p0, 0x0

    #@26
    goto :goto_a
.end method

.method public findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V
    .registers 9
    .parameter
    .parameter "text"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 926
    .local p1, outViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V

    #@3
    .line 927
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 928
    .local v2, childrenCount:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 929
    .local v1, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v2, :cond_1e

    #@a
    .line 930
    aget-object v0, v1, v3

    #@c
    .line 931
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@e
    and-int/lit8 v4, v4, 0xc

    #@10
    if-nez v4, :cond_1b

    #@12
    iget v4, v0, Landroid/view/View;->mPrivateFlags:I

    #@14
    and-int/lit8 v4, v4, 0x8

    #@16
    if-nez v4, :cond_1b

    #@18
    .line 933
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V

    #@1b
    .line 929
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    #@1d
    goto :goto_8

    #@1e
    .line 936
    .end local v0           #child:Landroid/view/View;
    :cond_1e
    return-void
.end method

.method finishAnimatingView(Landroid/view/View;Landroid/view/animation/Animation;)V
    .registers 6
    .parameter "view"
    .parameter "animation"

    #@0
    .prologue
    const/high16 v2, 0x1

    #@2
    .line 4990
    iget-object v0, p0, Landroid/view/ViewGroup;->mDisappearingChildren:Ljava/util/ArrayList;

    #@4
    .line 4991
    .local v0, disappearingChildren:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz v0, :cond_1f

    #@6
    .line 4992
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1f

    #@c
    .line 4993
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@f
    .line 4995
    iget-object v1, p1, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11
    if-eqz v1, :cond_16

    #@13
    .line 4996
    invoke-virtual {p1}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@16
    .line 4999
    :cond_16
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    #@19
    .line 5000
    iget v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1b
    or-int/lit8 v1, v1, 0x4

    #@1d
    iput v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1f
    .line 5004
    :cond_1f
    if-eqz p2, :cond_2a

    #@21
    invoke-virtual {p2}, Landroid/view/animation/Animation;->getFillAfter()Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_2a

    #@27
    .line 5005
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    #@2a
    .line 5008
    :cond_2a
    iget v1, p1, Landroid/view/View;->mPrivateFlags:I

    #@2c
    and-int/2addr v1, v2

    #@2d
    if-ne v1, v2, :cond_40

    #@2f
    .line 5009
    invoke-virtual {p1}, Landroid/view/View;->onAnimationEnd()V

    #@32
    .line 5012
    iget v1, p1, Landroid/view/View;->mPrivateFlags:I

    #@34
    const v2, -0x10001

    #@37
    and-int/2addr v1, v2

    #@38
    iput v1, p1, Landroid/view/View;->mPrivateFlags:I

    #@3a
    .line 5014
    iget v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@3c
    or-int/lit8 v1, v1, 0x4

    #@3e
    iput v1, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@40
    .line 5016
    :cond_40
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .registers 7
    .parameter "insets"

    #@0
    .prologue
    .line 5146
    invoke-super {p0, p1}, Landroid/view/View;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@3
    move-result v2

    #@4
    .line 5147
    .local v2, done:Z
    if-nez v2, :cond_15

    #@6
    .line 5148
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@8
    .line 5149
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@a
    .line 5150
    .local v0, children:[Landroid/view/View;
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v1, :cond_15

    #@d
    .line 5151
    aget-object v4, v0, v3

    #@f
    invoke-virtual {v4, p1}, Landroid/view/View;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@12
    move-result v2

    #@13
    .line 5152
    if-eqz v2, :cond_16

    #@15
    .line 5157
    .end local v0           #children:[Landroid/view/View;
    .end local v1           #count:I
    .end local v3           #i:I
    :cond_15
    return v2

    #@16
    .line 5150
    .restart local v0       #children:[Landroid/view/View;
    .restart local v1       #count:I
    .restart local v3       #i:I
    :cond_16
    add-int/lit8 v3, v3, 0x1

    #@18
    goto :goto_b
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .registers 4
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 666
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isRootNamespace()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 670
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 674
    :goto_e
    return-object v0

    #@f
    .line 671
    :cond_f
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@11
    if-eqz v0, :cond_1a

    #@13
    .line 672
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@15
    invoke-interface {v0, p1, p2}, Landroid/view/ViewParent;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    goto :goto_e

    #@1a
    .line 674
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_e
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 627
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_1f

    #@4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@7
    move-result v0

    #@8
    const/high16 v1, 0x6

    #@a
    if-eq v0, v1, :cond_1f

    #@c
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isFocused()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1a

    #@12
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@15
    move-result v0

    #@16
    const/high16 v1, 0x4

    #@18
    if-ne v0, v1, :cond_1f

    #@1a
    .line 639
    :cond_1a
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1c
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->focusableViewAvailable(Landroid/view/View;)V

    #@1f
    .line 641
    :cond_1f
    return-void
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .registers 11
    .parameter "region"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 5111
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@4
    and-int/lit16 v8, v8, 0x200

    #@6
    if-nez v8, :cond_e

    #@8
    move v4, v7

    #@9
    .line 5112
    .local v4, meOpaque:Z
    :goto_9
    if-eqz v4, :cond_10

    #@b
    if-nez p1, :cond_10

    #@d
    .line 5128
    :goto_d
    return v7

    #@e
    .end local v4           #meOpaque:Z
    :cond_e
    move v4, v6

    #@f
    .line 5111
    goto :goto_9

    #@10
    .line 5116
    .restart local v4       #meOpaque:Z
    :cond_10
    invoke-super {p0, p1}, Landroid/view/View;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    #@13
    .line 5117
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@15
    .line 5118
    .local v1, children:[Landroid/view/View;
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@17
    .line 5119
    .local v2, count:I
    const/4 v5, 0x1

    #@18
    .line 5120
    .local v5, noneOfTheChildrenAreTransparent:Z
    const/4 v3, 0x0

    #@19
    .local v3, i:I
    :goto_19
    if-ge v3, v2, :cond_33

    #@1b
    .line 5121
    aget-object v0, v1, v3

    #@1d
    .line 5122
    .local v0, child:Landroid/view/View;
    iget v8, v0, Landroid/view/View;->mViewFlags:I

    #@1f
    and-int/lit8 v8, v8, 0xc

    #@21
    if-eqz v8, :cond_29

    #@23
    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@26
    move-result-object v8

    #@27
    if-eqz v8, :cond_30

    #@29
    .line 5123
    :cond_29
    invoke-virtual {v0, p1}, Landroid/view/View;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    #@2c
    move-result v8

    #@2d
    if-nez v8, :cond_30

    #@2f
    .line 5124
    const/4 v5, 0x0

    #@30
    .line 5120
    :cond_30
    add-int/lit8 v3, v3, 0x1

    #@32
    goto :goto_19

    #@33
    .line 5128
    .end local v0           #child:Landroid/view/View;
    :cond_33
    if-nez v4, :cond_37

    #@35
    if-eqz v5, :cond_38

    #@37
    :cond_37
    move v6, v7

    #@38
    :cond_38
    move v7, v6

    #@39
    goto :goto_d
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 4722
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@6
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 4693
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 2
    .parameter "p"

    #@0
    .prologue
    .line 4711
    return-object p1
.end method

.method public getChildAt(I)Landroid/view/View;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 4792
    if-ltz p1, :cond_6

    #@2
    iget v0, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4
    if-lt p1, v0, :cond_8

    #@6
    .line 4793
    :cond_6
    const/4 v0, 0x0

    #@7
    .line 4795
    :goto_7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@a
    aget-object v0, v0, p1

    #@c
    goto :goto_7
.end method

.method public getChildCount()I
    .registers 2

    #@0
    .prologue
    .line 4781
    iget v0, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .registers 3
    .parameter "childCount"
    .parameter "i"

    #@0
    .prologue
    .line 2905
    return p2
.end method

.method protected getChildStaticTransformation(Landroid/view/View;Landroid/view/animation/Transformation;)Z
    .registers 4
    .parameter "child"
    .parameter "t"

    #@0
    .prologue
    .line 3074
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z
    .registers 15
    .parameter "child"
    .parameter "r"
    .parameter "offset"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/high16 v9, 0x3f00

    #@5
    .line 4368
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    if-eqz v6, :cond_7c

    #@9
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@b
    iget-object v3, v6, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@d
    .line 4369
    .local v3, rect:Landroid/graphics/RectF;
    :goto_d
    invoke-virtual {v3, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@10
    .line 4371
    invoke-virtual {p1}, Landroid/view/View;->hasIdentityMatrix()Z

    #@13
    move-result v6

    #@14
    if-nez v6, :cond_1d

    #@16
    .line 4372
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v6, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@1d
    .line 4375
    :cond_1d
    iget v6, p1, Landroid/view/View;->mLeft:I

    #@1f
    iget v7, p0, Landroid/view/View;->mScrollX:I

    #@21
    sub-int v0, v6, v7

    #@23
    .line 4376
    .local v0, dx:I
    iget v6, p1, Landroid/view/View;->mTop:I

    #@25
    iget v7, p0, Landroid/view/View;->mScrollY:I

    #@27
    sub-int v1, v6, v7

    #@29
    .line 4378
    .local v1, dy:I
    int-to-float v6, v0

    #@2a
    int-to-float v7, v1

    #@2b
    invoke-virtual {v3, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    #@2e
    .line 4380
    if-eqz p3, :cond_65

    #@30
    .line 4381
    invoke-virtual {p1}, Landroid/view/View;->hasIdentityMatrix()Z

    #@33
    move-result v6

    #@34
    if-nez v6, :cond_5b

    #@36
    .line 4382
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@38
    if-eqz v6, :cond_82

    #@3a
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3c
    iget-object v2, v6, Landroid/view/View$AttachInfo;->mTmpTransformLocation:[F

    #@3e
    .line 4384
    .local v2, position:[F
    :goto_3e
    iget v6, p3, Landroid/graphics/Point;->x:I

    #@40
    int-to-float v6, v6

    #@41
    aput v6, v2, v5

    #@43
    .line 4385
    iget v6, p3, Landroid/graphics/Point;->y:I

    #@45
    int-to-float v6, v6

    #@46
    aput v6, v2, v4

    #@48
    .line 4386
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@4f
    .line 4387
    aget v6, v2, v5

    #@51
    add-float/2addr v6, v9

    #@52
    float-to-int v6, v6

    #@53
    iput v6, p3, Landroid/graphics/Point;->x:I

    #@55
    .line 4388
    aget v6, v2, v4

    #@57
    add-float/2addr v6, v9

    #@58
    float-to-int v6, v6

    #@59
    iput v6, p3, Landroid/graphics/Point;->y:I

    #@5b
    .line 4390
    .end local v2           #position:[F
    :cond_5b
    iget v6, p3, Landroid/graphics/Point;->x:I

    #@5d
    add-int/2addr v6, v0

    #@5e
    iput v6, p3, Landroid/graphics/Point;->x:I

    #@60
    .line 4391
    iget v6, p3, Landroid/graphics/Point;->y:I

    #@62
    add-int/2addr v6, v1

    #@63
    iput v6, p3, Landroid/graphics/Point;->y:I

    #@65
    .line 4394
    :cond_65
    iget v6, p0, Landroid/view/View;->mRight:I

    #@67
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@69
    sub-int/2addr v6, v7

    #@6a
    int-to-float v6, v6

    #@6b
    iget v7, p0, Landroid/view/View;->mBottom:I

    #@6d
    iget v8, p0, Landroid/view/View;->mTop:I

    #@6f
    sub-int/2addr v7, v8

    #@70
    int-to-float v7, v7

    #@71
    invoke-virtual {v3, v10, v10, v6, v7}, Landroid/graphics/RectF;->intersect(FFFF)Z

    #@74
    move-result v6

    #@75
    if-eqz v6, :cond_a0

    #@77
    .line 4395
    iget-object v5, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@79
    if-nez v5, :cond_86

    #@7b
    .line 4401
    :goto_7b
    return v4

    #@7c
    .line 4368
    .end local v0           #dx:I
    .end local v1           #dy:I
    .end local v3           #rect:Landroid/graphics/RectF;
    :cond_7c
    new-instance v3, Landroid/graphics/RectF;

    #@7e
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    #@81
    goto :goto_d

    #@82
    .line 4382
    .restart local v0       #dx:I
    .restart local v1       #dy:I
    .restart local v3       #rect:Landroid/graphics/RectF;
    :cond_82
    const/4 v6, 0x2

    #@83
    new-array v2, v6, [F

    #@85
    goto :goto_3e

    #@86
    .line 4396
    :cond_86
    iget v4, v3, Landroid/graphics/RectF;->left:F

    #@88
    add-float/2addr v4, v9

    #@89
    float-to-int v4, v4

    #@8a
    iget v5, v3, Landroid/graphics/RectF;->top:F

    #@8c
    add-float/2addr v5, v9

    #@8d
    float-to-int v5, v5

    #@8e
    iget v6, v3, Landroid/graphics/RectF;->right:F

    #@90
    add-float/2addr v6, v9

    #@91
    float-to-int v6, v6

    #@92
    iget v7, v3, Landroid/graphics/RectF;->bottom:F

    #@94
    add-float/2addr v7, v9

    #@95
    float-to-int v7, v7

    #@96
    invoke-virtual {p2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@99
    .line 4398
    iget-object v4, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@9b
    invoke-interface {v4, p0, p2, p3}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@9e
    move-result v4

    #@9f
    goto :goto_7b

    #@a0
    :cond_a0
    move v4, v5

    #@a1
    .line 4401
    goto :goto_7b
.end method

.method public getDescendantFocusability()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "focus"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x20000
                to = "FOCUS_BEFORE_DESCENDANTS"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x40000
                to = "FOCUS_AFTER_DESCENDANTS"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x60000
                to = "FOCUS_BLOCK_DESCENDANTS"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 559
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    const/high16 v1, 0x6

    #@4
    and-int/2addr v0, v1

    #@5
    return v0
.end method

.method public getFocusedChild()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 828
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getLayoutAnimation()Landroid/view/animation/LayoutAnimationController;
    .registers 2

    #@0
    .prologue
    .line 4478
    iget-object v0, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@2
    return-object v0
.end method

.method public getLayoutAnimationListener()Landroid/view/animation/Animation$AnimationListener;
    .registers 2

    #@0
    .prologue
    .line 5167
    iget-object v0, p0, Landroid/view/ViewGroup;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    return-object v0
.end method

.method public getLayoutMode()I
    .registers 2

    #@0
    .prologue
    .line 4662
    iget v0, p0, Landroid/view/ViewGroup;->mLayoutMode:I

    #@2
    return v0
.end method

.method public getLayoutTransition()Landroid/animation/LayoutTransition;
    .registers 2

    #@0
    .prologue
    .line 3709
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@2
    return-object v0
.end method

.method public getPersistentDrawingCache()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "ANIMATION"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "SCROLLING"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "ALL"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 4633
    iget v0, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@2
    return v0
.end method

.method handleFocusGainInternal(ILandroid/graphics/Rect;)V
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 589
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 590
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@6
    invoke-virtual {v0}, Landroid/view/View;->unFocus()V

    #@9
    .line 591
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@c
    .line 593
    :cond_c
    invoke-super {p0, p1, p2}, Landroid/view/View;->handleFocusGainInternal(ILandroid/graphics/Rect;)V

    #@f
    .line 594
    return-void
.end method

.method public hasFocus()Z
    .registers 2

    #@0
    .prologue
    .line 838
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-nez v0, :cond_a

    #@6
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public hasFocusable()Z
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 868
    iget v7, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/lit8 v7, v7, 0xc

    #@6
    if-eqz v7, :cond_9

    #@8
    .line 889
    :cond_8
    :goto_8
    return v5

    #@9
    .line 872
    :cond_9
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isFocusable()Z

    #@c
    move-result v7

    #@d
    if-eqz v7, :cond_11

    #@f
    move v5, v6

    #@10
    .line 873
    goto :goto_8

    #@11
    .line 876
    :cond_11
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@14
    move-result v3

    #@15
    .line 877
    .local v3, descendantFocusability:I
    const/high16 v7, 0x6

    #@17
    if-eq v3, v7, :cond_8

    #@19
    .line 878
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@1b
    .line 879
    .local v2, count:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@1d
    .line 881
    .local v1, children:[Landroid/view/View;
    const/4 v4, 0x0

    #@1e
    .local v4, i:I
    :goto_1e
    if-ge v4, v2, :cond_8

    #@20
    .line 882
    aget-object v0, v1, v4

    #@22
    .line 883
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_2a

    #@28
    move v5, v6

    #@29
    .line 884
    goto :goto_8

    #@2a
    .line 881
    :cond_2a
    add-int/lit8 v4, v4, 0x1

    #@2c
    goto :goto_1e
.end method

.method protected hasHoveredChild()Z
    .registers 2

    #@0
    .prologue
    .line 1666
    iget-object v0, p0, Landroid/view/ViewGroup;->mFirstHoverTarget:Landroid/view/ViewGroup$HoverTarget;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public hasTransientState()Z
    .registers 2

    #@0
    .prologue
    .line 762
    iget v0, p0, Landroid/view/ViewGroup;->mChildCountWithTransientState:I

    #@2
    if-gtz v0, :cond_a

    #@4
    invoke-super {p0}, Landroid/view/View;->hasTransientState()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public indexOfChild(Landroid/view/View;)I
    .registers 6
    .parameter "child"

    #@0
    .prologue
    .line 4764
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    .line 4765
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@4
    .line 4766
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_f

    #@7
    .line 4767
    aget-object v3, v0, v2

    #@9
    if-ne v3, p1, :cond_c

    #@b
    .line 4771
    .end local v2           #i:I
    :goto_b
    return v2

    #@c
    .line 4766
    .restart local v2       #i:I
    :cond_c
    add-int/lit8 v2, v2, 0x1

    #@e
    goto :goto_5

    #@f
    .line 4771
    :cond_f
    const/4 v2, -0x1

    #@10
    goto :goto_b
.end method

.method protected internalSetPadding(IIII)V
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2580
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->internalSetPadding(IIII)V

    #@3
    .line 2582
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@5
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@7
    or-int/2addr v0, v1

    #@8
    iget v1, p0, Landroid/view/View;->mPaddingRight:I

    #@a
    or-int/2addr v0, v1

    #@b
    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    #@d
    or-int/2addr v0, v1

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 2583
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@12
    or-int/lit8 v0, v0, 0x20

    #@14
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@16
    .line 2587
    :goto_16
    return-void

    #@17
    .line 2585
    :cond_17
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@19
    and-int/lit8 v0, v0, -0x21

    #@1b
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1d
    goto :goto_16
.end method

.method public final invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 26
    .parameter "child"
    .parameter "dirty"

    #@0
    .prologue
    .line 4033
    move-object/from16 v13, p0

    #@2
    .line 4035
    .local v13, parent:Landroid/view/ViewParent;
    move-object/from16 v0, p0

    #@4
    iget-object v5, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    .line 4036
    .local v5, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v5, :cond_1c9

    #@8
    .line 4040
    move-object/from16 v0, p1

    #@a
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@c
    move/from16 v18, v0

    #@e
    and-int/lit8 v18, v18, 0x40

    #@10
    const/16 v19, 0x40

    #@12
    move/from16 v0, v18

    #@14
    move/from16 v1, v19

    #@16
    if-ne v0, v1, :cond_1ca

    #@18
    const/4 v8, 0x1

    #@19
    .line 4046
    .local v8, drawAnimation:Z
    :goto_19
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@1c
    move-result-object v7

    #@1d
    .line 4047
    .local v7, childMatrix:Landroid/graphics/Matrix;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isOpaque()Z

    #@20
    move-result v18

    #@21
    if-eqz v18, :cond_1cd

    #@23
    if-nez v8, :cond_1cd

    #@25
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@28
    move-result-object v18

    #@29
    if-nez v18, :cond_1cd

    #@2b
    invoke-virtual {v7}, Landroid/graphics/Matrix;->isIdentity()Z

    #@2e
    move-result v18

    #@2f
    if-eqz v18, :cond_1cd

    #@31
    const/4 v9, 0x1

    #@32
    .line 4051
    .local v9, isOpaque:Z
    :goto_32
    if-eqz v9, :cond_1d0

    #@34
    const/high16 v12, 0x40

    #@36
    .line 4053
    .local v12, opaqueFlag:I
    :goto_36
    move-object/from16 v0, p1

    #@38
    iget v0, v0, Landroid/view/View;->mLayerType:I

    #@3a
    move/from16 v18, v0

    #@3c
    if-eqz v18, :cond_6c

    #@3e
    .line 4054
    move-object/from16 v0, p0

    #@40
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@42
    move/from16 v18, v0

    #@44
    const/high16 v19, -0x8000

    #@46
    or-int v18, v18, v19

    #@48
    move/from16 v0, v18

    #@4a
    move-object/from16 v1, p0

    #@4c
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@4e
    .line 4055
    move-object/from16 v0, p0

    #@50
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@52
    move/from16 v18, v0

    #@54
    const v19, -0x8001

    #@57
    and-int v18, v18, v19

    #@59
    move/from16 v0, v18

    #@5b
    move-object/from16 v1, p0

    #@5d
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@5f
    .line 4056
    move-object/from16 v0, p1

    #@61
    iget-object v0, v0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@63
    move-object/from16 v18, v0

    #@65
    move-object/from16 v0, v18

    #@67
    move-object/from16 v1, p2

    #@69
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@6c
    .line 4059
    :cond_6c
    iget-object v10, v5, Landroid/view/View$AttachInfo;->mInvalidateChildLocation:[I

    #@6e
    .line 4060
    .local v10, location:[I
    const/16 v18, 0x0

    #@70
    move-object/from16 v0, p1

    #@72
    iget v0, v0, Landroid/view/View;->mLeft:I

    #@74
    move/from16 v19, v0

    #@76
    aput v19, v10, v18

    #@78
    .line 4061
    const/16 v18, 0x1

    #@7a
    move-object/from16 v0, p1

    #@7c
    iget v0, v0, Landroid/view/View;->mTop:I

    #@7e
    move/from16 v19, v0

    #@80
    aput v19, v10, v18

    #@82
    .line 4062
    invoke-virtual {v7}, Landroid/graphics/Matrix;->isIdentity()Z

    #@85
    move-result v18

    #@86
    if-eqz v18, :cond_96

    #@88
    move-object/from16 v0, p0

    #@8a
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8c
    move/from16 v18, v0

    #@8e
    move/from16 v0, v18

    #@90
    and-int/lit16 v0, v0, 0x800

    #@92
    move/from16 v18, v0

    #@94
    if-eqz v18, :cond_10f

    #@96
    .line 4064
    :cond_96
    iget-object v6, v5, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@98
    .line 4065
    .local v6, boundingRect:Landroid/graphics/RectF;
    move-object/from16 v0, p2

    #@9a
    invoke-virtual {v6, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@9d
    .line 4067
    move-object/from16 v0, p0

    #@9f
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@a1
    move/from16 v18, v0

    #@a3
    move/from16 v0, v18

    #@a5
    and-int/lit16 v0, v0, 0x800

    #@a7
    move/from16 v18, v0

    #@a9
    if-eqz v18, :cond_1d7

    #@ab
    .line 4068
    iget-object v14, v5, Landroid/view/View$AttachInfo;->mTmpTransformation:Landroid/view/animation/Transformation;

    #@ad
    .line 4069
    .local v14, t:Landroid/view/animation/Transformation;
    move-object/from16 v0, p0

    #@af
    move-object/from16 v1, p1

    #@b1
    invoke-virtual {v0, v1, v14}, Landroid/view/ViewGroup;->getChildStaticTransformation(Landroid/view/View;Landroid/view/animation/Transformation;)Z

    #@b4
    move-result v16

    #@b5
    .line 4070
    .local v16, transformed:Z
    if-eqz v16, :cond_1d4

    #@b7
    .line 4071
    iget-object v15, v5, Landroid/view/View$AttachInfo;->mTmpMatrix:Landroid/graphics/Matrix;

    #@b9
    .line 4072
    .local v15, transformMatrix:Landroid/graphics/Matrix;
    invoke-virtual {v14}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@bc
    move-result-object v18

    #@bd
    move-object/from16 v0, v18

    #@bf
    invoke-virtual {v15, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@c2
    .line 4073
    invoke-virtual {v7}, Landroid/graphics/Matrix;->isIdentity()Z

    #@c5
    move-result v18

    #@c6
    if-nez v18, :cond_cb

    #@c8
    .line 4074
    invoke-virtual {v15, v7}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    #@cb
    .line 4082
    .end local v14           #t:Landroid/view/animation/Transformation;
    .end local v16           #transformed:Z
    :cond_cb
    :goto_cb
    invoke-virtual {v15, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@ce
    .line 4083
    iget v0, v6, Landroid/graphics/RectF;->left:F

    #@d0
    move/from16 v18, v0

    #@d2
    const/high16 v19, 0x3f00

    #@d4
    sub-float v18, v18, v19

    #@d6
    move/from16 v0, v18

    #@d8
    float-to-int v0, v0

    #@d9
    move/from16 v18, v0

    #@db
    iget v0, v6, Landroid/graphics/RectF;->top:F

    #@dd
    move/from16 v19, v0

    #@df
    const/high16 v20, 0x3f00

    #@e1
    sub-float v19, v19, v20

    #@e3
    move/from16 v0, v19

    #@e5
    float-to-int v0, v0

    #@e6
    move/from16 v19, v0

    #@e8
    iget v0, v6, Landroid/graphics/RectF;->right:F

    #@ea
    move/from16 v20, v0

    #@ec
    const/high16 v21, 0x3f00

    #@ee
    add-float v20, v20, v21

    #@f0
    move/from16 v0, v20

    #@f2
    float-to-int v0, v0

    #@f3
    move/from16 v20, v0

    #@f5
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    #@f7
    move/from16 v21, v0

    #@f9
    const/high16 v22, 0x3f00

    #@fb
    add-float v21, v21, v22

    #@fd
    move/from16 v0, v21

    #@ff
    float-to-int v0, v0

    #@100
    move/from16 v21, v0

    #@102
    move-object/from16 v0, p2

    #@104
    move/from16 v1, v18

    #@106
    move/from16 v2, v19

    #@108
    move/from16 v3, v20

    #@10a
    move/from16 v4, v21

    #@10c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@10f
    .line 4090
    .end local v6           #boundingRect:Landroid/graphics/RectF;
    .end local v15           #transformMatrix:Landroid/graphics/Matrix;
    :cond_10f
    const/16 v17, 0x0

    #@111
    .line 4091
    .local v17, view:Landroid/view/View;
    instance-of v0, v13, Landroid/view/View;

    #@113
    move/from16 v18, v0

    #@115
    if-eqz v18, :cond_11b

    #@117
    move-object/from16 v17, v13

    #@119
    .line 4092
    check-cast v17, Landroid/view/View;

    #@11b
    .line 4095
    :cond_11b
    if-eqz v8, :cond_12d

    #@11d
    .line 4096
    if-eqz v17, :cond_1da

    #@11f
    .line 4097
    move-object/from16 v0, v17

    #@121
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@123
    move/from16 v18, v0

    #@125
    or-int/lit8 v18, v18, 0x40

    #@127
    move/from16 v0, v18

    #@129
    move-object/from16 v1, v17

    #@12b
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@12d
    .line 4105
    :cond_12d
    :goto_12d
    if-eqz v17, :cond_16a

    #@12f
    .line 4106
    move-object/from16 v0, v17

    #@131
    iget v0, v0, Landroid/view/View;->mViewFlags:I

    #@133
    move/from16 v18, v0

    #@135
    move/from16 v0, v18

    #@137
    and-int/lit16 v0, v0, 0x3000

    #@139
    move/from16 v18, v0

    #@13b
    if-eqz v18, :cond_145

    #@13d
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getSolidColor()I

    #@140
    move-result v18

    #@141
    if-nez v18, :cond_145

    #@143
    .line 4108
    const/high16 v12, 0x20

    #@145
    .line 4110
    :cond_145
    move-object/from16 v0, v17

    #@147
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@149
    move/from16 v18, v0

    #@14b
    const/high16 v19, 0x60

    #@14d
    and-int v18, v18, v19

    #@14f
    const/high16 v19, 0x20

    #@151
    move/from16 v0, v18

    #@153
    move/from16 v1, v19

    #@155
    if-eq v0, v1, :cond_16a

    #@157
    .line 4111
    move-object/from16 v0, v17

    #@159
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@15b
    move/from16 v18, v0

    #@15d
    const v19, -0x600001

    #@160
    and-int v18, v18, v19

    #@162
    or-int v18, v18, v12

    #@164
    move/from16 v0, v18

    #@166
    move-object/from16 v1, v17

    #@168
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@16a
    .line 4115
    :cond_16a
    move-object/from16 v0, p2

    #@16c
    invoke-interface {v13, v10, v0}, Landroid/view/ViewParent;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    #@16f
    move-result-object v13

    #@170
    .line 4116
    if-eqz v17, :cond_1c7

    #@172
    .line 4118
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@175
    move-result-object v11

    #@176
    .line 4119
    .local v11, m:Landroid/graphics/Matrix;
    invoke-virtual {v11}, Landroid/graphics/Matrix;->isIdentity()Z

    #@179
    move-result v18

    #@17a
    if-nez v18, :cond_1c7

    #@17c
    .line 4120
    iget-object v6, v5, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@17e
    .line 4121
    .restart local v6       #boundingRect:Landroid/graphics/RectF;
    move-object/from16 v0, p2

    #@180
    invoke-virtual {v6, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@183
    .line 4122
    invoke-virtual {v11, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@186
    .line 4123
    iget v0, v6, Landroid/graphics/RectF;->left:F

    #@188
    move/from16 v18, v0

    #@18a
    const/high16 v19, 0x3f00

    #@18c
    sub-float v18, v18, v19

    #@18e
    move/from16 v0, v18

    #@190
    float-to-int v0, v0

    #@191
    move/from16 v18, v0

    #@193
    iget v0, v6, Landroid/graphics/RectF;->top:F

    #@195
    move/from16 v19, v0

    #@197
    const/high16 v20, 0x3f00

    #@199
    sub-float v19, v19, v20

    #@19b
    move/from16 v0, v19

    #@19d
    float-to-int v0, v0

    #@19e
    move/from16 v19, v0

    #@1a0
    iget v0, v6, Landroid/graphics/RectF;->right:F

    #@1a2
    move/from16 v20, v0

    #@1a4
    const/high16 v21, 0x3f00

    #@1a6
    add-float v20, v20, v21

    #@1a8
    move/from16 v0, v20

    #@1aa
    float-to-int v0, v0

    #@1ab
    move/from16 v20, v0

    #@1ad
    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    #@1af
    move/from16 v21, v0

    #@1b1
    const/high16 v22, 0x3f00

    #@1b3
    add-float v21, v21, v22

    #@1b5
    move/from16 v0, v21

    #@1b7
    float-to-int v0, v0

    #@1b8
    move/from16 v21, v0

    #@1ba
    move-object/from16 v0, p2

    #@1bc
    move/from16 v1, v18

    #@1be
    move/from16 v2, v19

    #@1c0
    move/from16 v3, v20

    #@1c2
    move/from16 v4, v21

    #@1c4
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@1c7
    .line 4129
    .end local v6           #boundingRect:Landroid/graphics/RectF;
    .end local v11           #m:Landroid/graphics/Matrix;
    :cond_1c7
    if-nez v13, :cond_10f

    #@1c9
    .line 4131
    .end local v7           #childMatrix:Landroid/graphics/Matrix;
    .end local v8           #drawAnimation:Z
    .end local v9           #isOpaque:Z
    .end local v10           #location:[I
    .end local v12           #opaqueFlag:I
    .end local v17           #view:Landroid/view/View;
    :cond_1c9
    return-void

    #@1ca
    .line 4040
    :cond_1ca
    const/4 v8, 0x0

    #@1cb
    goto/16 :goto_19

    #@1cd
    .line 4047
    .restart local v7       #childMatrix:Landroid/graphics/Matrix;
    .restart local v8       #drawAnimation:Z
    :cond_1cd
    const/4 v9, 0x0

    #@1ce
    goto/16 :goto_32

    #@1d0
    .line 4051
    .restart local v9       #isOpaque:Z
    :cond_1d0
    const/high16 v12, 0x20

    #@1d2
    goto/16 :goto_36

    #@1d4
    .line 4077
    .restart local v6       #boundingRect:Landroid/graphics/RectF;
    .restart local v10       #location:[I
    .restart local v12       #opaqueFlag:I
    .restart local v14       #t:Landroid/view/animation/Transformation;
    .restart local v16       #transformed:Z
    :cond_1d4
    move-object v15, v7

    #@1d5
    .restart local v15       #transformMatrix:Landroid/graphics/Matrix;
    goto/16 :goto_cb

    #@1d7
    .line 4080
    .end local v14           #t:Landroid/view/animation/Transformation;
    .end local v15           #transformMatrix:Landroid/graphics/Matrix;
    .end local v16           #transformed:Z
    :cond_1d7
    move-object v15, v7

    #@1d8
    .restart local v15       #transformMatrix:Landroid/graphics/Matrix;
    goto/16 :goto_cb

    #@1da
    .line 4098
    .end local v6           #boundingRect:Landroid/graphics/RectF;
    .end local v15           #transformMatrix:Landroid/graphics/Matrix;
    .restart local v17       #view:Landroid/view/View;
    :cond_1da
    instance-of v0, v13, Landroid/view/ViewRootImpl;

    #@1dc
    move/from16 v18, v0

    #@1de
    if-eqz v18, :cond_12d

    #@1e0
    move-object/from16 v18, v13

    #@1e2
    .line 4099
    check-cast v18, Landroid/view/ViewRootImpl;

    #@1e4
    const/16 v19, 0x1

    #@1e6
    move/from16 v0, v19

    #@1e8
    move-object/from16 v1, v18

    #@1ea
    iput-boolean v0, v1, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@1ec
    goto/16 :goto_12d
.end method

.method public invalidateChildFast(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 10
    .parameter "child"
    .parameter "dirty"

    #@0
    .prologue
    .line 4201
    move-object v3, p0

    #@1
    .line 4203
    .local v3, parent:Landroid/view/ViewParent;
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    .line 4204
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_30

    #@5
    .line 4205
    iget v6, p1, Landroid/view/View;->mLayerType:I

    #@7
    if-eqz v6, :cond_e

    #@9
    .line 4206
    iget-object v6, p1, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@b
    invoke-virtual {v6, p2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@e
    .line 4209
    :cond_e
    iget v1, p1, Landroid/view/View;->mLeft:I

    #@10
    .line 4210
    .local v1, left:I
    iget v5, p1, Landroid/view/View;->mTop:I

    #@12
    .line 4211
    .local v5, top:I
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Landroid/graphics/Matrix;->isIdentity()Z

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_1f

    #@1c
    .line 4212
    invoke-virtual {p1, p2}, Landroid/view/View;->transformRect(Landroid/graphics/Rect;)V

    #@1f
    .line 4216
    :cond_1f
    instance-of v6, v3, Landroid/view/ViewGroup;

    #@21
    if-eqz v6, :cond_3a

    #@23
    move-object v4, v3

    #@24
    .line 4217
    check-cast v4, Landroid/view/ViewGroup;

    #@26
    .line 4218
    .local v4, parentVG:Landroid/view/ViewGroup;
    iget v6, v4, Landroid/view/View;->mLayerType:I

    #@28
    if-eqz v6, :cond_31

    #@2a
    .line 4220
    invoke-virtual {v4}, Landroid/view/ViewGroup;->invalidate()V

    #@2d
    .line 4221
    const/4 v3, 0x0

    #@2e
    .line 4235
    .end local v4           #parentVG:Landroid/view/ViewGroup;
    :goto_2e
    if-nez v3, :cond_1f

    #@30
    .line 4237
    .end local v1           #left:I
    .end local v5           #top:I
    :cond_30
    return-void

    #@31
    .line 4223
    .restart local v1       #left:I
    .restart local v4       #parentVG:Landroid/view/ViewGroup;
    .restart local v5       #top:I
    :cond_31
    invoke-direct {v4, v1, v5, p2}, Landroid/view/ViewGroup;->invalidateChildInParentFast(IILandroid/graphics/Rect;)Landroid/view/ViewParent;

    #@34
    move-result-object v3

    #@35
    .line 4224
    iget v1, v4, Landroid/view/View;->mLeft:I

    #@37
    .line 4225
    iget v5, v4, Landroid/view/View;->mTop:I

    #@39
    goto :goto_2e

    #@3a
    .line 4230
    .end local v4           #parentVG:Landroid/view/ViewGroup;
    :cond_3a
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mInvalidateChildLocation:[I

    #@3c
    .line 4231
    .local v2, location:[I
    const/4 v6, 0x0

    #@3d
    aput v1, v2, v6

    #@3f
    .line 4232
    const/4 v6, 0x1

    #@40
    aput v5, v2, v6

    #@42
    .line 4233
    invoke-interface {v3, v2, p2}, Landroid/view/ViewParent;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    #@45
    move-result-object v3

    #@46
    goto :goto_2e
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .registers 11
    .parameter "location"
    .parameter "dirty"

    #@0
    .prologue
    const v4, 0x8000

    #@3
    const/high16 v7, -0x8000

    #@5
    const/4 v6, 0x1

    #@6
    const/4 v5, 0x0

    #@7
    .line 4142
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@9
    and-int/lit8 v2, v2, 0x20

    #@b
    const/16 v3, 0x20

    #@d
    if-eq v2, v3, :cond_14

    #@f
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@11
    and-int/2addr v2, v4

    #@12
    if-ne v2, v4, :cond_a1

    #@14
    .line 4144
    :cond_14
    iget v2, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@16
    and-int/lit16 v2, v2, 0x90

    #@18
    const/16 v3, 0x80

    #@1a
    if-eq v2, v3, :cond_5f

    #@1c
    .line 4146
    aget v2, p1, v5

    #@1e
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@20
    sub-int/2addr v2, v3

    #@21
    aget v3, p1, v6

    #@23
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@25
    sub-int/2addr v3, v4

    #@26
    invoke-virtual {p2, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@29
    .line 4149
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2b
    .line 4150
    .local v0, left:I
    iget v1, p0, Landroid/view/View;->mTop:I

    #@2d
    .line 4152
    .local v1, top:I
    iget v2, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2f
    and-int/lit8 v2, v2, 0x1

    #@31
    if-ne v2, v6, :cond_42

    #@33
    .line 4153
    iget v2, p0, Landroid/view/View;->mRight:I

    #@35
    sub-int/2addr v2, v0

    #@36
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@38
    sub-int/2addr v3, v1

    #@39
    invoke-virtual {p2, v5, v5, v2, v3}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_42

    #@3f
    .line 4154
    invoke-virtual {p2}, Landroid/graphics/Rect;->setEmpty()V

    #@42
    .line 4157
    :cond_42
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@44
    const v3, -0x8001

    #@47
    and-int/2addr v2, v3

    #@48
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@4a
    .line 4159
    aput v0, p1, v5

    #@4c
    .line 4160
    aput v1, p1, v6

    #@4e
    .line 4162
    iget v2, p0, Landroid/view/View;->mLayerType:I

    #@50
    if-eqz v2, :cond_5c

    #@52
    .line 4163
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@54
    or-int/2addr v2, v7

    #@55
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@57
    .line 4164
    iget-object v2, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@59
    invoke-virtual {v2, p2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@5c
    .line 4167
    :cond_5c
    iget-object v2, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@5e
    .line 4190
    .end local v0           #left:I
    .end local v1           #top:I
    :goto_5e
    return-object v2

    #@5f
    .line 4170
    :cond_5f
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@61
    const v3, -0x8021

    #@64
    and-int/2addr v2, v3

    #@65
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@67
    .line 4172
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@69
    aput v2, p1, v5

    #@6b
    .line 4173
    iget v2, p0, Landroid/view/View;->mTop:I

    #@6d
    aput v2, p1, v6

    #@6f
    .line 4174
    iget v2, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@71
    and-int/lit8 v2, v2, 0x1

    #@73
    if-ne v2, v6, :cond_93

    #@75
    .line 4175
    iget v2, p0, Landroid/view/View;->mRight:I

    #@77
    iget v3, p0, Landroid/view/View;->mLeft:I

    #@79
    sub-int/2addr v2, v3

    #@7a
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@7c
    iget v4, p0, Landroid/view/View;->mTop:I

    #@7e
    sub-int/2addr v3, v4

    #@7f
    invoke-virtual {p2, v5, v5, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@82
    .line 4181
    :goto_82
    iget v2, p0, Landroid/view/View;->mLayerType:I

    #@84
    if-eqz v2, :cond_90

    #@86
    .line 4182
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@88
    or-int/2addr v2, v7

    #@89
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@8b
    .line 4183
    iget-object v2, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@8d
    invoke-virtual {v2, p2}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@90
    .line 4186
    :cond_90
    iget-object v2, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@92
    goto :goto_5e

    #@93
    .line 4178
    :cond_93
    iget v2, p0, Landroid/view/View;->mRight:I

    #@95
    iget v3, p0, Landroid/view/View;->mLeft:I

    #@97
    sub-int/2addr v2, v3

    #@98
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@9a
    iget v4, p0, Landroid/view/View;->mTop:I

    #@9c
    sub-int/2addr v3, v4

    #@9d
    invoke-virtual {p2, v5, v5, v2, v3}, Landroid/graphics/Rect;->union(IIII)V

    #@a0
    goto :goto_82

    #@a1
    .line 4190
    :cond_a1
    const/4 v2, 0x0

    #@a2
    goto :goto_5e
.end method

.method public isAlwaysDrawnWithCacheEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 4524
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit16 v0, v0, 0x4000

    #@4
    const/16 v1, 0x4000

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isAnimationCacheEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 4494
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit8 v0, v0, 0x40

    #@4
    const/16 v1, 0x40

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method protected isChildrenDrawingOrderEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 4591
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit16 v0, v0, 0x400

    #@4
    const/16 v1, 0x400

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method protected isChildrenDrawnWithCacheEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    const v1, 0x8000

    #@3
    .line 4559
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5
    and-int/2addr v0, v1

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isChromeBrowserRunningJavaScripts()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 524
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@3
    .line 525
    .local v2, count:I
    const/4 v3, 0x0

    #@4
    .local v3, i:I
    :goto_4
    if-ge v3, v2, :cond_32

    #@6
    .line 526
    iget-object v6, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@8
    aget-object v1, v6, v3

    #@a
    .line 527
    .local v1, child:Landroid/view/View;
    instance-of v6, v1, Landroid/widget/TextView;

    #@c
    if-eqz v6, :cond_22

    #@e
    move-object v4, v1

    #@f
    .line 528
    check-cast v4, Landroid/widget/TextView;

    #@11
    .line 529
    .local v4, t:Landroid/widget/TextView;
    sget-object v6, Landroid/view/ViewGroup;->mActivityTrigger:Lcom/android/internal/app/ActivityTrigger;

    #@13
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@16
    move-result-object v7

    #@17
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a
    move-result-object v7

    #@1b
    invoke-virtual {v6, v7}, Lcom/android/internal/app/ActivityTrigger;->activityBrowserTrigger(Ljava/lang/String;)I

    #@1e
    move-result v6

    #@1f
    if-ne v6, v5, :cond_22

    #@21
    .line 540
    .end local v1           #child:Landroid/view/View;
    .end local v4           #t:Landroid/widget/TextView;
    :cond_21
    :goto_21
    return v5

    #@22
    .line 533
    .restart local v1       #child:Landroid/view/View;
    :cond_22
    instance-of v6, v1, Landroid/view/ViewGroup;

    #@24
    if-eqz v6, :cond_2f

    #@26
    move-object v0, v1

    #@27
    .line 534
    check-cast v0, Landroid/view/ViewGroup;

    #@29
    .line 535
    .local v0, c:Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->isChromeBrowserRunningJavaScripts()Z

    #@2c
    move-result v6

    #@2d
    if-eq v6, v5, :cond_21

    #@2f
    .line 525
    .end local v0           #c:Landroid/view/ViewGroup;
    :cond_2f
    add-int/lit8 v3, v3, 0x1

    #@31
    goto :goto_4

    #@32
    .line 540
    .end local v1           #child:Landroid/view/View;
    :cond_32
    const/4 v5, 0x0

    #@33
    goto :goto_21
.end method

.method public isMotionEventSplittingEnabled()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x20

    #@2
    .line 2274
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method protected isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z
    .registers 13
    .parameter "x"
    .parameter "y"
    .parameter "child"
    .parameter "outLocalPoint"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2144
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@4
    int-to-float v4, v4

    #@5
    add-float/2addr v4, p1

    #@6
    iget v5, p3, Landroid/view/View;->mLeft:I

    #@8
    int-to-float v5, v5

    #@9
    sub-float v1, v4, v5

    #@b
    .line 2145
    .local v1, localX:F
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@d
    int-to-float v4, v4

    #@e
    add-float/2addr v4, p2

    #@f
    iget v5, p3, Landroid/view/View;->mTop:I

    #@11
    int-to-float v5, v5

    #@12
    sub-float v3, v4, v5

    #@14
    .line 2146
    .local v3, localY:F
    invoke-virtual {p3}, Landroid/view/View;->hasIdentityMatrix()Z

    #@17
    move-result v4

    #@18
    if-nez v4, :cond_31

    #@1a
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1c
    if-eqz v4, :cond_31

    #@1e
    .line 2147
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@20
    iget-object v2, v4, Landroid/view/View$AttachInfo;->mTmpTransformLocation:[F

    #@22
    .line 2148
    .local v2, localXY:[F
    aput v1, v2, v6

    #@24
    .line 2149
    aput v3, v2, v7

    #@26
    .line 2150
    invoke-virtual {p3}, Landroid/view/View;->getInverseMatrix()Landroid/graphics/Matrix;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@2d
    .line 2151
    aget v1, v2, v6

    #@2f
    .line 2152
    aget v3, v2, v7

    #@31
    .line 2154
    .end local v2           #localXY:[F
    :cond_31
    invoke-virtual {p3, v1, v3}, Landroid/view/View;->pointInView(FF)Z

    #@34
    move-result v0

    #@35
    .line 2155
    .local v0, isInView:Z
    if-eqz v0, :cond_3c

    #@37
    if-eqz p4, :cond_3c

    #@39
    .line 2156
    invoke-virtual {p4, v1, v3}, Landroid/graphics/PointF;->set(FF)V

    #@3c
    .line 2158
    :cond_3c
    return v0
.end method

.method isViewTransitioning(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 5024
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 5

    #@0
    .prologue
    .line 5194
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@3
    .line 5195
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@5
    .line 5196
    .local v0, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@7
    .line 5197
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 5198
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@f
    .line 5197
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 5200
    :cond_12
    return-void
.end method

.method public final layout(IIII)V
    .registers 6
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 4409
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@6
    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isChangingLayout()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_19

    #@c
    .line 4410
    :cond_c
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 4411
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@12
    invoke-virtual {v0, p0}, Landroid/animation/LayoutTransition;->layoutChange(Landroid/view/ViewGroup;)V

    #@15
    .line 4413
    :cond_15
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    #@18
    .line 4418
    :goto_18
    return-void

    #@19
    .line 4416
    :cond_19
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Landroid/view/ViewGroup;->mLayoutSuppressed:Z

    #@1c
    goto :goto_18
.end method

.method public makeOptionalFitsSystemWindows()V
    .registers 5

    #@0
    .prologue
    .line 992
    invoke-super {p0}, Landroid/view/View;->makeOptionalFitsSystemWindows()V

    #@3
    .line 993
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@5
    .line 994
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@7
    .line 995
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_12

    #@a
    .line 996
    aget-object v3, v0, v2

    #@c
    invoke-virtual {v3}, Landroid/view/View;->makeOptionalFitsSystemWindows()V

    #@f
    .line 995
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_8

    #@12
    .line 998
    :cond_12
    return-void
.end method

.method protected measureChild(Landroid/view/View;II)V
    .registers 9
    .parameter "child"
    .parameter "parentWidthMeasureSpec"
    .parameter "parentHeightMeasureSpec"

    #@0
    .prologue
    .line 4829
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v2

    #@4
    .line 4831
    .local v2, lp:Landroid/view/ViewGroup$LayoutParams;
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@6
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@8
    add-int/2addr v3, v4

    #@9
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@b
    invoke-static {p2, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@e
    move-result v1

    #@f
    .line 4833
    .local v1, childWidthMeasureSpec:I
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@11
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@13
    add-int/2addr v3, v4

    #@14
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@16
    invoke-static {p3, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@19
    move-result v0

    #@1a
    .line 4836
    .local v0, childHeightMeasureSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@1d
    .line 4837
    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .registers 11
    .parameter "child"
    .parameter "parentWidthMeasureSpec"
    .parameter "widthUsed"
    .parameter "parentHeightMeasureSpec"
    .parameter "heightUsed"

    #@0
    .prologue
    .line 4856
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v2

    #@4
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    #@6
    .line 4858
    .local v2, lp:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@8
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@a
    add-int/2addr v3, v4

    #@b
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@d
    add-int/2addr v3, v4

    #@e
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@10
    add-int/2addr v3, v4

    #@11
    add-int/2addr v3, p3

    #@12
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@14
    invoke-static {p2, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@17
    move-result v1

    #@18
    .line 4861
    .local v1, childWidthMeasureSpec:I
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@1a
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@1c
    add-int/2addr v3, v4

    #@1d
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    #@1f
    add-int/2addr v3, v4

    #@20
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    #@22
    add-int/2addr v3, v4

    #@23
    add-int/2addr v3, p5

    #@24
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@26
    invoke-static {p4, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    #@29
    move-result v0

    #@2a
    .line 4865
    .local v0, childHeightMeasureSpec:I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    #@2d
    .line 4866
    return-void
.end method

.method protected measureChildren(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 4808
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    .line 4809
    .local v3, size:I
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@4
    .line 4810
    .local v1, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v3, :cond_17

    #@7
    .line 4811
    aget-object v0, v1, v2

    #@9
    .line 4812
    .local v0, child:Landroid/view/View;
    iget v4, v0, Landroid/view/View;->mViewFlags:I

    #@b
    and-int/lit8 v4, v4, 0xc

    #@d
    const/16 v5, 0x8

    #@f
    if-eq v4, v5, :cond_14

    #@11
    .line 4813
    invoke-virtual {p0, v0, p1, p2}, Landroid/view/ViewGroup;->measureChild(Landroid/view/View;II)V

    #@14
    .line 4810
    :cond_14
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_5

    #@17
    .line 4816
    .end local v0           #child:Landroid/view/View;
    :cond_17
    return-void
.end method

.method notifyChildOfDrag(Landroid/view/View;)Z
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 1311
    const/4 v0, 0x0

    #@1
    .line 1312
    .local v0, canAccept:Z
    iget-object v1, p0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_25

    #@9
    .line 1313
    iget-object v1, p0, Landroid/view/ViewGroup;->mDragNotifiedChildren:Ljava/util/HashSet;

    #@b
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e
    .line 1314
    iget-object v1, p0, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@10
    invoke-virtual {p1, v1}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@13
    move-result v0

    #@14
    .line 1315
    if-eqz v0, :cond_25

    #@16
    invoke-virtual {p1}, Landroid/view/View;->canAcceptDrag()Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_25

    #@1c
    .line 1316
    iget v1, p1, Landroid/view/View;->mPrivateFlags2:I

    #@1e
    or-int/lit8 v1, v1, 0x1

    #@20
    iput v1, p1, Landroid/view/View;->mPrivateFlags2:I

    #@22
    .line 1317
    invoke-virtual {p1}, Landroid/view/View;->refreshDrawableState()V

    #@25
    .line 1320
    :cond_25
    return v0
.end method

.method public offsetChildrenTopAndBottom(I)V
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 4347
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@3
    .line 4348
    .local v1, count:I
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@5
    .line 4350
    .local v0, children:[Landroid/view/View;
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    if-ge v2, v1, :cond_23

    #@8
    .line 4351
    aget-object v3, v0, v2

    #@a
    .line 4352
    .local v3, v:Landroid/view/View;
    iget v4, v3, Landroid/view/View;->mTop:I

    #@c
    add-int/2addr v4, p1

    #@d
    iput v4, v3, Landroid/view/View;->mTop:I

    #@f
    .line 4353
    iget v4, v3, Landroid/view/View;->mBottom:I

    #@11
    add-int/2addr v4, p1

    #@12
    iput v4, v3, Landroid/view/View;->mBottom:I

    #@14
    .line 4354
    iget-object v4, v3, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@16
    if-eqz v4, :cond_20

    #@18
    .line 4355
    iget-object v4, v3, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1a
    invoke-virtual {v4, p1}, Landroid/view/DisplayList;->offsetTopBottom(I)V

    #@1d
    .line 4356
    invoke-virtual {p0, v5, v5}, Landroid/view/ViewGroup;->invalidateViewProperty(ZZ)V

    #@20
    .line 4350
    :cond_20
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_6

    #@23
    .line 4359
    .end local v3           #v:Landroid/view/View;
    :cond_23
    return-void
.end method

.method public final offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 5
    .parameter "descendant"
    .parameter "rect"

    #@0
    .prologue
    .line 4272
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/ViewGroup;->offsetRectBetweenParentAndChild(Landroid/view/View;Landroid/graphics/Rect;ZZ)V

    #@5
    .line 4273
    return-void
.end method

.method offsetRectBetweenParentAndChild(Landroid/view/View;Landroid/graphics/Rect;ZZ)V
    .registers 11
    .parameter "descendant"
    .parameter "rect"
    .parameter "offsetFromChildToParent"
    .parameter "clipToBounds"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 4293
    if-ne p1, p0, :cond_4

    #@3
    .line 4337
    :goto_3
    return-void

    #@4
    .line 4297
    :cond_4
    iget-object v1, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    .line 4302
    .local v1, theParent:Landroid/view/ViewParent;
    :goto_6
    if-eqz v1, :cond_55

    #@8
    instance-of v2, v1, Landroid/view/View;

    #@a
    if-eqz v2, :cond_55

    #@c
    if-eq v1, p0, :cond_55

    #@e
    .line 4304
    if-eqz p3, :cond_35

    #@10
    .line 4305
    iget v2, p1, Landroid/view/View;->mLeft:I

    #@12
    iget v3, p1, Landroid/view/View;->mScrollX:I

    #@14
    sub-int/2addr v2, v3

    #@15
    iget v3, p1, Landroid/view/View;->mTop:I

    #@17
    iget v4, p1, Landroid/view/View;->mScrollY:I

    #@19
    sub-int/2addr v3, v4

    #@1a
    invoke-virtual {p2, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@1d
    .line 4307
    if-eqz p4, :cond_2f

    #@1f
    move-object v0, v1

    #@20
    .line 4308
    check-cast v0, Landroid/view/View;

    #@22
    .line 4309
    .local v0, p:Landroid/view/View;
    iget v2, v0, Landroid/view/View;->mRight:I

    #@24
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@26
    sub-int/2addr v2, v3

    #@27
    iget v3, v0, Landroid/view/View;->mBottom:I

    #@29
    iget v4, v0, Landroid/view/View;->mTop:I

    #@2b
    sub-int/2addr v3, v4

    #@2c
    invoke-virtual {p2, v5, v5, v2, v3}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@2f
    .end local v0           #p:Landroid/view/View;
    :cond_2f
    :goto_2f
    move-object p1, v1

    #@30
    .line 4320
    check-cast p1, Landroid/view/View;

    #@32
    .line 4321
    iget-object v1, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@34
    goto :goto_6

    #@35
    .line 4312
    :cond_35
    if-eqz p4, :cond_47

    #@37
    move-object v0, v1

    #@38
    .line 4313
    check-cast v0, Landroid/view/View;

    #@3a
    .line 4314
    .restart local v0       #p:Landroid/view/View;
    iget v2, v0, Landroid/view/View;->mRight:I

    #@3c
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@3e
    sub-int/2addr v2, v3

    #@3f
    iget v3, v0, Landroid/view/View;->mBottom:I

    #@41
    iget v4, v0, Landroid/view/View;->mTop:I

    #@43
    sub-int/2addr v3, v4

    #@44
    invoke-virtual {p2, v5, v5, v2, v3}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@47
    .line 4316
    .end local v0           #p:Landroid/view/View;
    :cond_47
    iget v2, p1, Landroid/view/View;->mScrollX:I

    #@49
    iget v3, p1, Landroid/view/View;->mLeft:I

    #@4b
    sub-int/2addr v2, v3

    #@4c
    iget v3, p1, Landroid/view/View;->mScrollY:I

    #@4e
    iget v4, p1, Landroid/view/View;->mTop:I

    #@50
    sub-int/2addr v3, v4

    #@51
    invoke-virtual {p2, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@54
    goto :goto_2f

    #@55
    .line 4326
    :cond_55
    if-ne v1, p0, :cond_75

    #@57
    .line 4327
    if-eqz p3, :cond_67

    #@59
    .line 4328
    iget v2, p1, Landroid/view/View;->mLeft:I

    #@5b
    iget v3, p1, Landroid/view/View;->mScrollX:I

    #@5d
    sub-int/2addr v2, v3

    #@5e
    iget v3, p1, Landroid/view/View;->mTop:I

    #@60
    iget v4, p1, Landroid/view/View;->mScrollY:I

    #@62
    sub-int/2addr v3, v4

    #@63
    invoke-virtual {p2, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@66
    goto :goto_3

    #@67
    .line 4331
    :cond_67
    iget v2, p1, Landroid/view/View;->mScrollX:I

    #@69
    iget v3, p1, Landroid/view/View;->mLeft:I

    #@6b
    sub-int/2addr v2, v3

    #@6c
    iget v3, p1, Landroid/view/View;->mScrollY:I

    #@6e
    iget v4, p1, Landroid/view/View;->mTop:I

    #@70
    sub-int/2addr v3, v4

    #@71
    invoke-virtual {p2, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@74
    goto :goto_3

    #@75
    .line 4335
    :cond_75
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@77
    const-string/jumbo v3, "parameter must be a descendant of this view"

    #@7a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7d
    throw v2
.end method

.method public final offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 4
    .parameter "descendant"
    .parameter "rect"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 4282
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/view/ViewGroup;->offsetRectBetweenParentAndChild(Landroid/view/View;Landroid/graphics/Rect;ZZ)V

    #@4
    .line 4283
    return-void
.end method

.method protected onAnimationEnd()V
    .registers 3

    #@0
    .prologue
    .line 2686
    invoke-super {p0}, Landroid/view/View;->onAnimationEnd()V

    #@3
    .line 2689
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@5
    and-int/lit8 v0, v0, 0x40

    #@7
    const/16 v1, 0x40

    #@9
    if-ne v0, v1, :cond_1d

    #@b
    .line 2690
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@d
    const v1, -0x8001

    #@10
    and-int/2addr v0, v1

    #@11
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@13
    .line 2692
    iget v0, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@15
    and-int/lit8 v0, v0, 0x1

    #@17
    if-nez v0, :cond_1d

    #@19
    .line 2693
    const/4 v0, 0x0

    #@1a
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setChildrenDrawingCacheEnabled(Z)V

    #@1d
    .line 2696
    :cond_1d
    return-void
.end method

.method protected onAnimationStart()V
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 2662
    invoke-super {p0}, Landroid/view/View;->onAnimationStart()V

    #@4
    .line 2665
    iget v6, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@6
    and-int/lit8 v6, v6, 0x40

    #@8
    const/16 v7, 0x40

    #@a
    if-ne v6, v7, :cond_37

    #@c
    .line 2666
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@e
    .line 2667
    .local v3, count:I
    iget-object v2, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@10
    .line 2668
    .local v2, children:[Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isHardwareAccelerated()Z

    #@13
    move-result v6

    #@14
    if-nez v6, :cond_2d

    #@16
    move v0, v5

    #@17
    .line 2670
    .local v0, buildCache:Z
    :goto_17
    const/4 v4, 0x0

    #@18
    .local v4, i:I
    :goto_18
    if-ge v4, v3, :cond_2f

    #@1a
    .line 2671
    aget-object v1, v2, v4

    #@1c
    .line 2672
    .local v1, child:Landroid/view/View;
    iget v6, v1, Landroid/view/View;->mViewFlags:I

    #@1e
    and-int/lit8 v6, v6, 0xc

    #@20
    if-nez v6, :cond_2a

    #@22
    .line 2673
    invoke-virtual {v1, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    #@25
    .line 2674
    if-eqz v0, :cond_2a

    #@27
    .line 2675
    invoke-virtual {v1, v5}, Landroid/view/View;->buildDrawingCache(Z)V

    #@2a
    .line 2670
    :cond_2a
    add-int/lit8 v4, v4, 0x1

    #@2c
    goto :goto_18

    #@2d
    .line 2668
    .end local v0           #buildCache:Z
    .end local v1           #child:Landroid/view/View;
    .end local v4           #i:I
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_17

    #@2f
    .line 2680
    .restart local v0       #buildCache:Z
    .restart local v4       #i:I
    :cond_2f
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@31
    const v6, 0x8000

    #@34
    or-int/2addr v5, v6

    #@35
    iput v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@37
    .line 2682
    .end local v0           #buildCache:Z
    .end local v2           #children:[Landroid/view/View;
    .end local v3           #count:I
    .end local v4           #i:I
    :cond_37
    return-void
.end method

.method protected onChildVisibilityChanged(Landroid/view/View;II)V
    .registers 5
    .parameter "child"
    .parameter "oldVisibility"
    .parameter "newVisibility"

    #@0
    .prologue
    .line 1023
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1024
    if-nez p3, :cond_15

    #@6
    .line 1025
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@8
    invoke-virtual {v0, p0, p1, p2}, Landroid/animation/LayoutTransition;->showChild(Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@b
    .line 1041
    :cond_b
    :goto_b
    iget-object v0, p0, Landroid/view/ViewGroup;->mCurrentDrag:Landroid/view/DragEvent;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 1042
    if-nez p3, :cond_14

    #@11
    .line 1043
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->notifyChildOfDrag(Landroid/view/View;)Z

    #@14
    .line 1046
    :cond_14
    return-void

    #@15
    .line 1027
    :cond_15
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@17
    invoke-virtual {v0, p0, p1, p3}, Landroid/animation/LayoutTransition;->hideChild(Landroid/view/ViewGroup;Landroid/view/View;I)V

    #@1a
    .line 1028
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@1c
    if-eqz v0, :cond_b

    #@1e
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_b

    #@26
    .line 1031
    iget-object v0, p0, Landroid/view/ViewGroup;->mVisibilityChangingChildren:Ljava/util/ArrayList;

    #@28
    if-nez v0, :cond_31

    #@2a
    .line 1032
    new-instance v0, Ljava/util/ArrayList;

    #@2c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2f
    iput-object v0, p0, Landroid/view/ViewGroup;->mVisibilityChangingChildren:Ljava/util/ArrayList;

    #@31
    .line 1034
    :cond_31
    iget-object v0, p0, Landroid/view/ViewGroup;->mVisibilityChangingChildren:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    .line 1035
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->addDisappearingView(Landroid/view/View;)V

    #@39
    goto :goto_b
.end method

.method protected onCreateDrawableState(I)[I
    .registers 8
    .parameter "extraSpace"

    #@0
    .prologue
    .line 5204
    iget v5, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    and-int/lit16 v5, v5, 0x2000

    #@4
    if-nez v5, :cond_b

    #@6
    .line 5205
    invoke-super {p0, p1}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@9
    move-result-object v4

    #@a
    .line 5228
    :cond_a
    return-object v4

    #@b
    .line 5208
    :cond_b
    const/4 v3, 0x0

    #@c
    .line 5209
    .local v3, need:I
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@f
    move-result v2

    #@10
    .line 5210
    .local v2, n:I
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v2, :cond_22

    #@13
    .line 5211
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Landroid/view/View;->getDrawableState()[I

    #@1a
    move-result-object v0

    #@1b
    .line 5213
    .local v0, childState:[I
    if-eqz v0, :cond_1f

    #@1d
    .line 5214
    array-length v5, v0

    #@1e
    add-int/2addr v3, v5

    #@1f
    .line 5210
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_11

    #@22
    .line 5218
    .end local v0           #childState:[I
    :cond_22
    add-int v5, p1, v3

    #@24
    invoke-super {p0, v5}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@27
    move-result-object v4

    #@28
    .line 5220
    .local v4, state:[I
    const/4 v1, 0x0

    #@29
    :goto_29
    if-ge v1, v2, :cond_a

    #@2b
    .line 5221
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Landroid/view/View;->getDrawableState()[I

    #@32
    move-result-object v0

    #@33
    .line 5223
    .restart local v0       #childState:[I
    if-eqz v0, :cond_39

    #@35
    .line 5224
    invoke-static {v4, v0}, Landroid/view/ViewGroup;->mergeDrawableStates([I[I)[I

    #@38
    move-result-object v4

    #@39
    .line 5220
    :cond_39
    add-int/lit8 v1, v1, 0x1

    #@3b
    goto :goto_29
.end method

.method protected onDebugDraw(Landroid/graphics/Canvas;)V
    .registers 11
    .parameter "canvas"

    #@0
    .prologue
    .line 2747
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getLayoutMode()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x1

    #@5
    if-ne v0, v1, :cond_3d

    #@7
    .line 2748
    const/4 v7, 0x0

    #@8
    .local v7, i:I
    :goto_8
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@b
    move-result v0

    #@c
    if-ge v7, v0, :cond_3d

    #@e
    .line 2749
    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v6

    #@12
    .line 2750
    .local v6, c:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getOpticalInsets()Landroid/graphics/Insets;

    #@15
    move-result-object v8

    #@16
    .line 2751
    .local v8, insets:Landroid/graphics/Insets;
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    #@19
    move-result v0

    #@1a
    iget v1, v8, Landroid/graphics/Insets;->left:I

    #@1c
    add-int/2addr v1, v0

    #@1d
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@20
    move-result v0

    #@21
    iget v2, v8, Landroid/graphics/Insets;->top:I

    #@23
    add-int/2addr v2, v0

    #@24
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    #@27
    move-result v0

    #@28
    iget v3, v8, Landroid/graphics/Insets;->right:I

    #@2a
    sub-int v3, v0, v3

    #@2c
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@2f
    move-result v0

    #@30
    iget v4, v8, Landroid/graphics/Insets;->bottom:I

    #@32
    sub-int v4, v0, v4

    #@34
    const/high16 v5, -0x1

    #@36
    move-object v0, p1

    #@37
    invoke-static/range {v0 .. v5}, Landroid/view/ViewGroup;->drawRect(Landroid/graphics/Canvas;IIIII)V

    #@3a
    .line 2748
    add-int/lit8 v7, v7, 0x1

    #@3c
    goto :goto_8

    #@3d
    .line 2760
    .end local v6           #c:Landroid/view/View;
    .end local v7           #i:I
    .end local v8           #insets:Landroid/graphics/Insets;
    :cond_3d
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->onDebugDrawMargins(Landroid/graphics/Canvas;)V

    #@40
    .line 2763
    const/4 v7, 0x0

    #@41
    .restart local v7       #i:I
    :goto_41
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@44
    move-result v0

    #@45
    if-ge v7, v0, :cond_65

    #@47
    .line 2764
    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@4a
    move-result-object v6

    #@4b
    .line 2765
    .restart local v6       #c:Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    #@4e
    move-result v1

    #@4f
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    #@52
    move-result v2

    #@53
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    #@56
    move-result v3

    #@57
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    #@5a
    move-result v4

    #@5b
    const v5, -0xffff01

    #@5e
    move-object v0, p1

    #@5f
    invoke-static/range {v0 .. v5}, Landroid/view/ViewGroup;->drawRect(Landroid/graphics/Canvas;IIIII)V

    #@62
    .line 2763
    add-int/lit8 v7, v7, 0x1

    #@64
    goto :goto_41

    #@65
    .line 2767
    .end local v6           #c:Landroid/view/View;
    :cond_65
    return-void
.end method

.method protected onDebugDrawMargins(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 2736
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@4
    move-result v2

    #@5
    if-ge v1, v2, :cond_15

    #@7
    .line 2737
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    .line 2738
    .local v0, c:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, v0, p1}, Landroid/view/ViewGroup$LayoutParams;->onDebugDraw(Landroid/view/View;Landroid/graphics/Canvas;)V

    #@12
    .line 2736
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_1

    #@15
    .line 2740
    .end local v0           #c:Landroid/view/View;
    :cond_15
    return-void
.end method

.method onInitializeAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2524
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 2525
    const-class v0, Landroid/view/ViewGroup;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 2526
    return-void
.end method

.method onInitializeAccessibilityNodeInfoInternal(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 7
    .parameter "info"

    #@0
    .prologue
    .line 2508
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfoInternal(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 2509
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    if-eqz v4, :cond_27

    #@7
    .line 2510
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    iget-object v1, v4, Landroid/view/View$AttachInfo;->mTempArrayList:Ljava/util/ArrayList;

    #@b
    .line 2511
    .local v1, childrenForAccessibility:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@e
    .line 2512
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->addChildrenForAccessibility(Ljava/util/ArrayList;)V

    #@11
    .line 2513
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v2

    #@15
    .line 2514
    .local v2, childrenForAccessibilityCount:I
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v2, :cond_24

    #@18
    .line 2515
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/view/View;

    #@1e
    .line 2516
    .local v0, child:Landroid/view/View;
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;)V

    #@21
    .line 2514
    add-int/lit8 v3, v3, 0x1

    #@23
    goto :goto_16

    #@24
    .line 2518
    .end local v0           #child:Landroid/view/View;
    :cond_24
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@27
    .line 2520
    .end local v1           #childrenForAccessibility:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v2           #childrenForAccessibilityCount:I
    .end local v3           #i:I
    :cond_27
    return-void
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1738
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 2337
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected abstract onLayout(ZIIII)V
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 11
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 2397
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@2
    .line 2398
    .local v2, count:I
    and-int/lit8 v7, p1, 0x2

    #@4
    if-eqz v7, :cond_1e

    #@6
    .line 2399
    const/4 v6, 0x0

    #@7
    .line 2400
    .local v6, index:I
    const/4 v5, 0x1

    #@8
    .line 2401
    .local v5, increment:I
    move v3, v2

    #@9
    .line 2407
    .local v3, end:I
    :goto_9
    iget-object v1, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@b
    .line 2408
    .local v1, children:[Landroid/view/View;
    move v4, v6

    #@c
    .local v4, i:I
    :goto_c
    if-eq v4, v3, :cond_25

    #@e
    .line 2409
    aget-object v0, v1, v4

    #@10
    .line 2410
    .local v0, child:Landroid/view/View;
    iget v7, v0, Landroid/view/View;->mViewFlags:I

    #@12
    and-int/lit8 v7, v7, 0xc

    #@14
    if-nez v7, :cond_23

    #@16
    .line 2411
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@19
    move-result v7

    #@1a
    if-eqz v7, :cond_23

    #@1c
    .line 2412
    const/4 v7, 0x1

    #@1d
    .line 2416
    .end local v0           #child:Landroid/view/View;
    :goto_1d
    return v7

    #@1e
    .line 2403
    .end local v1           #children:[Landroid/view/View;
    .end local v3           #end:I
    .end local v4           #i:I
    .end local v5           #increment:I
    .end local v6           #index:I
    :cond_1e
    add-int/lit8 v6, v2, -0x1

    #@20
    .line 2404
    .restart local v6       #index:I
    const/4 v5, -0x1

    #@21
    .line 2405
    .restart local v5       #increment:I
    const/4 v3, -0x1

    #@22
    .restart local v3       #end:I
    goto :goto_9

    #@23
    .line 2408
    .restart local v0       #child:Landroid/view/View;
    .restart local v1       #children:[Landroid/view/View;
    .restart local v4       #i:I
    :cond_23
    add-int/2addr v4, v5

    #@24
    goto :goto_c

    #@25
    .line 2416
    .end local v0           #child:Landroid/view/View;
    :cond_25
    const/4 v7, 0x0

    #@26
    goto :goto_1d
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4
    .parameter "child"
    .parameter "event"

    #@0
    .prologue
    .line 717
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 718
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@9
    move-result v0

    #@a
    .line 720
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestSendAccessibilityEventInternal(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method onRequestSendAccessibilityEventInternal(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4
    .parameter "child"
    .parameter "event"

    #@0
    .prologue
    .line 730
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onSetLayoutParams(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "child"
    .parameter "layoutParams"

    #@0
    .prologue
    .line 5504
    return-void
.end method

.method protected onViewAdded(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 3310
    iget-object v0, p0, Landroid/view/ViewGroup;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 3311
    iget-object v0, p0, Landroid/view/ViewGroup;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@6
    invoke-interface {v0, p0, p1}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    #@9
    .line 3313
    :cond_9
    return-void
.end method

.method protected onViewRemoved(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 3319
    iget-object v0, p0, Landroid/view/ViewGroup;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 3320
    iget-object v0, p0, Landroid/view/ViewGroup;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@6
    invoke-interface {v0, p0, p1}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V

    #@9
    .line 3322
    :cond_9
    return-void
.end method

.method public recomputeViewAttributes(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 1092
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v1, :cond_11

    #@4
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-boolean v1, v1, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@8
    if-nez v1, :cond_11

    #@a
    .line 1093
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    .line 1094
    .local v0, parent:Landroid/view/ViewParent;
    if-eqz v0, :cond_11

    #@e
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->recomputeViewAttributes(Landroid/view/View;)V

    #@11
    .line 1096
    .end local v0           #parent:Landroid/view/ViewParent;
    :cond_11
    return-void
.end method

.method public removeAllViews()V
    .registers 2

    #@0
    .prologue
    .line 3770
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    #@3
    .line 3771
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@6
    .line 3772
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@a
    .line 3773
    return-void
.end method

.method public removeAllViewsInLayout()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 3789
    iget v2, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@4
    .line 3790
    .local v2, count:I
    if-gtz v2, :cond_7

    #@6
    .line 3841
    :cond_6
    :goto_6
    return-void

    #@7
    .line 3794
    :cond_7
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@9
    .line 3795
    .local v0, children:[Landroid/view/View;
    iput v7, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@b
    .line 3797
    iget-object v4, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@d
    .line 3798
    .local v4, focused:Landroid/view/View;
    iget-object v8, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@f
    if-eqz v8, :cond_5c

    #@11
    const/4 v3, 0x1

    #@12
    .line 3799
    .local v3, detach:Z
    :goto_12
    const/4 v1, 0x0

    #@13
    .line 3801
    .local v1, clearChildFocus:Landroid/view/View;
    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->needGlobalAttributesUpdate(Z)V

    #@16
    .line 3803
    add-int/lit8 v5, v2, -0x1

    #@18
    .local v5, i:I
    :goto_18
    if-ltz v5, :cond_64

    #@1a
    .line 3804
    aget-object v6, v0, v5

    #@1c
    .line 3806
    .local v6, view:Landroid/view/View;
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@1e
    if-eqz v8, :cond_25

    #@20
    .line 3807
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@22
    invoke-virtual {v8, p0, v6}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@25
    .line 3810
    :cond_25
    if-ne v6, v4, :cond_2b

    #@27
    .line 3811
    invoke-virtual {v6}, Landroid/view/View;->unFocus()V

    #@2a
    .line 3812
    move-object v1, v6

    #@2b
    .line 3815
    :cond_2b
    invoke-virtual {v6}, Landroid/view/View;->clearAccessibilityFocus()V

    #@2e
    .line 3817
    invoke-direct {p0, v6}, Landroid/view/ViewGroup;->cancelTouchTarget(Landroid/view/View;)V

    #@31
    .line 3818
    invoke-direct {p0, v6}, Landroid/view/ViewGroup;->cancelHoverTarget(Landroid/view/View;)V

    #@34
    .line 3820
    invoke-virtual {v6}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@37
    move-result-object v8

    #@38
    if-nez v8, :cond_46

    #@3a
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@3c
    if-eqz v8, :cond_5e

    #@3e
    iget-object v8, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@40
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@43
    move-result v8

    #@44
    if-eqz v8, :cond_5e

    #@46
    .line 3822
    :cond_46
    invoke-direct {p0, v6}, Landroid/view/ViewGroup;->addDisappearingView(Landroid/view/View;)V

    #@49
    .line 3827
    :cond_49
    :goto_49
    invoke-virtual {v6}, Landroid/view/View;->hasTransientState()Z

    #@4c
    move-result v8

    #@4d
    if-eqz v8, :cond_52

    #@4f
    .line 3828
    invoke-virtual {p0, v6, v7}, Landroid/view/ViewGroup;->childHasTransientStateChanged(Landroid/view/View;Z)V

    #@52
    .line 3831
    :cond_52
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->onViewRemoved(Landroid/view/View;)V

    #@55
    .line 3833
    iput-object v9, v6, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@57
    .line 3834
    aput-object v9, v0, v5

    #@59
    .line 3803
    add-int/lit8 v5, v5, -0x1

    #@5b
    goto :goto_18

    #@5c
    .end local v1           #clearChildFocus:Landroid/view/View;
    .end local v3           #detach:Z
    .end local v5           #i:I
    .end local v6           #view:Landroid/view/View;
    :cond_5c
    move v3, v7

    #@5d
    .line 3798
    goto :goto_12

    #@5e
    .line 3823
    .restart local v1       #clearChildFocus:Landroid/view/View;
    .restart local v3       #detach:Z
    .restart local v5       #i:I
    .restart local v6       #view:Landroid/view/View;
    :cond_5e
    if-eqz v3, :cond_49

    #@60
    .line 3824
    invoke-virtual {v6}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@63
    goto :goto_49

    #@64
    .line 3837
    .end local v6           #view:Landroid/view/View;
    :cond_64
    if-eqz v1, :cond_6

    #@66
    .line 3838
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->clearChildFocus(Landroid/view/View;)V

    #@69
    .line 3839
    invoke-virtual {p0}, Landroid/view/ViewGroup;->ensureInputFocusOnFirstFocusable()V

    #@6c
    goto :goto_6
.end method

.method protected removeDetachedView(Landroid/view/View;Z)V
    .registers 4
    .parameter "child"
    .parameter "animate"

    #@0
    .prologue
    .line 3864
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 3865
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/animation/LayoutTransition;->removeChild(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@9
    .line 3868
    :cond_9
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@b
    if-ne p1, v0, :cond_10

    #@d
    .line 3869
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    #@10
    .line 3872
    :cond_10
    invoke-virtual {p1}, Landroid/view/View;->clearAccessibilityFocus()V

    #@13
    .line 3874
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->cancelTouchTarget(Landroid/view/View;)V

    #@16
    .line 3875
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->cancelHoverTarget(Landroid/view/View;)V

    #@19
    .line 3877
    if-eqz p2, :cond_21

    #@1b
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@1e
    move-result-object v0

    #@1f
    if-nez v0, :cond_2d

    #@21
    :cond_21
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@23
    if-eqz v0, :cond_3e

    #@25
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_3e

    #@2d
    .line 3879
    :cond_2d
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->addDisappearingView(Landroid/view/View;)V

    #@30
    .line 3884
    :cond_30
    :goto_30
    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_3a

    #@36
    .line 3885
    const/4 v0, 0x0

    #@37
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->childHasTransientStateChanged(Landroid/view/View;Z)V

    #@3a
    .line 3888
    :cond_3a
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->onViewRemoved(Landroid/view/View;)V

    #@3d
    .line 3889
    return-void

    #@3e
    .line 3880
    :cond_3e
    iget-object v0, p1, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    if-eqz v0, :cond_30

    #@42
    .line 3881
    invoke-virtual {p1}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@45
    goto :goto_30
.end method

.method public removeView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 3563
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->removeViewInternal(Landroid/view/View;)V

    #@3
    .line 3564
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@6
    .line 3565
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@a
    .line 3566
    return-void
.end method

.method public removeViewAt(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 3607
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, p1, v0}, Landroid/view/ViewGroup;->removeViewInternal(ILandroid/view/View;)V

    #@7
    .line 3608
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@a
    .line 3609
    const/4 v0, 0x1

    #@b
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@e
    .line 3610
    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 3579
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;->removeViewInternal(Landroid/view/View;)V

    #@3
    .line 3580
    return-void
.end method

.method public removeViews(II)V
    .registers 4
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 3623
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->removeViewsInternal(II)V

    #@3
    .line 3624
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@6
    .line 3625
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@a
    .line 3626
    return-void
.end method

.method public removeViewsInLayout(II)V
    .registers 3
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 3594
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;->removeViewsInternal(II)V

    #@3
    .line 3595
    return-void
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 5
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 603
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@3
    move-result v0

    #@4
    const/high16 v1, 0x6

    #@6
    if-ne v0, v1, :cond_9

    #@8
    .line 621
    :cond_8
    :goto_8
    return-void

    #@9
    .line 608
    :cond_9
    invoke-super {p0}, Landroid/view/View;->unFocus()V

    #@c
    .line 611
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@e
    if-eq v0, p1, :cond_1b

    #@10
    .line 612
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@12
    if-eqz v0, :cond_19

    #@14
    .line 613
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@16
    invoke-virtual {v0}, Landroid/view/View;->unFocus()V

    #@19
    .line 616
    :cond_19
    iput-object p1, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@1b
    .line 618
    :cond_1b
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1d
    if-eqz v0, :cond_8

    #@1f
    .line 619
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@21
    invoke-interface {v0, p0, p2}, Landroid/view/ViewParent;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@24
    goto :goto_8
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 5
    .parameter "child"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 681
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 4
    .parameter "disallowIntercept"

    #@0
    .prologue
    const/high16 v1, 0x8

    #@2
    .line 2282
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_b

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    if-ne p1, v0, :cond_d

    #@a
    .line 2297
    :cond_a
    :goto_a
    return-void

    #@b
    .line 2282
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_8

    #@d
    .line 2287
    :cond_d
    if-eqz p1, :cond_1e

    #@f
    .line 2288
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@11
    or-int/2addr v0, v1

    #@12
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@14
    .line 2294
    :goto_14
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@16
    if-eqz v0, :cond_a

    #@18
    .line 2295
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1a
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    #@1d
    goto :goto_a

    #@1e
    .line 2290
    :cond_1e
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@20
    const v1, -0x80001

    #@23
    and-int/2addr v0, v1

    #@24
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@26
    goto :goto_14
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .registers 8
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 2360
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@3
    move-result v0

    #@4
    .line 2362
    .local v0, descendantFocusability:I
    sparse-switch v0, :sswitch_data_3c

    #@7
    .line 2374
    new-instance v2, Ljava/lang/IllegalStateException;

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "descendant focusability must be one of FOCUS_BEFORE_DESCENDANTS, FOCUS_AFTER_DESCENDANTS, FOCUS_BLOCK_DESCENDANTS but is "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 2364
    :sswitch_20
    invoke-super {p0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@23
    move-result v1

    #@24
    .line 2371
    :cond_24
    :goto_24
    return v1

    #@25
    .line 2366
    :sswitch_25
    invoke-super {p0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@28
    move-result v1

    #@29
    .line 2367
    .local v1, took:Z
    if-nez v1, :cond_24

    #@2b
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    #@2e
    move-result v1

    #@2f
    goto :goto_24

    #@30
    .line 2370
    .end local v1           #took:Z
    :sswitch_30
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    #@33
    move-result v1

    #@34
    .line 2371
    .restart local v1       #took:Z
    if-nez v1, :cond_24

    #@36
    invoke-super {p0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@39
    move-result v1

    #@3a
    goto :goto_24

    #@3b
    .line 2362
    nop

    #@3c
    :sswitch_data_3c
    .sparse-switch
        0x20000 -> :sswitch_25
        0x40000 -> :sswitch_30
        0x60000 -> :sswitch_20
    .end sparse-switch
.end method

.method public requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 6
    .parameter "child"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 689
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@3
    .line 690
    .local v0, parent:Landroid/view/ViewParent;
    if-nez v0, :cond_6

    #@5
    .line 697
    :cond_5
    :goto_5
    return v2

    #@6
    .line 693
    :cond_6
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@9
    move-result v1

    #@a
    .line 694
    .local v1, propagate:Z
    if-eqz v1, :cond_5

    #@c
    .line 697
    invoke-interface {v0, p0, p2}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@f
    move-result v2

    #@10
    goto :goto_5
.end method

.method public requestTransitionStart(Landroid/animation/LayoutTransition;)V
    .registers 3
    .parameter "transition"

    #@0
    .prologue
    .line 5291
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@3
    move-result-object v0

    #@4
    .line 5292
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_9

    #@6
    .line 5293
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl;->requestTransitionStart(Landroid/animation/LayoutTransition;)V

    #@9
    .line 5295
    :cond_9
    return-void
.end method

.method public requestTransparentRegion(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 5135
    if-eqz p1, :cond_11

    #@2
    .line 5136
    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@4
    or-int/lit16 v0, v0, 0x200

    #@6
    iput v0, p1, Landroid/view/View;->mPrivateFlags:I

    #@8
    .line 5137
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 5138
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@e
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->requestTransparentRegion(Landroid/view/View;)V

    #@11
    .line 5141
    :cond_11
    return-void
.end method

.method public resetAccessibilityStateChanged()V
    .registers 5

    #@0
    .prologue
    .line 2533
    invoke-super {p0}, Landroid/view/View;->resetAccessibilityStateChanged()V

    #@3
    .line 2534
    iget-object v2, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@5
    .line 2535
    .local v2, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@7
    .line 2536
    .local v1, childCount:I
    const/4 v3, 0x0

    #@8
    .local v3, i:I
    :goto_8
    if-ge v3, v1, :cond_12

    #@a
    .line 2537
    aget-object v0, v2, v3

    #@c
    .line 2538
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->resetAccessibilityStateChanged()V

    #@f
    .line 2536
    add-int/lit8 v3, v3, 0x1

    #@11
    goto :goto_8

    #@12
    .line 2540
    .end local v0           #child:Landroid/view/View;
    :cond_12
    return-void
.end method

.method protected resetResolvedDrawables()V
    .registers 5

    #@0
    .prologue
    .line 5478
    invoke-super {p0}, Landroid/view/View;->resetResolvedDrawables()V

    #@3
    .line 5480
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5481
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5482
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5483
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5484
    invoke-virtual {v0}, Landroid/view/View;->resetResolvedDrawables()V

    #@17
    .line 5481
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5487
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resetResolvedLayoutDirection()V
    .registers 5

    #@0
    .prologue
    .line 5414
    invoke-super {p0}, Landroid/view/View;->resetResolvedLayoutDirection()V

    #@3
    .line 5416
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5417
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5418
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5419
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5420
    invoke-virtual {v0}, Landroid/view/View;->resetResolvedLayoutDirection()V

    #@17
    .line 5417
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5423
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resetResolvedPadding()V
    .registers 5

    #@0
    .prologue
    .line 5462
    invoke-super {p0}, Landroid/view/View;->resetResolvedPadding()V

    #@3
    .line 5464
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5465
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5466
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5467
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5468
    invoke-virtual {v0}, Landroid/view/View;->resetResolvedPadding()V

    #@17
    .line 5465
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5471
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resetResolvedTextAlignment()V
    .registers 5

    #@0
    .prologue
    .line 5446
    invoke-super {p0}, Landroid/view/View;->resetResolvedTextAlignment()V

    #@3
    .line 5448
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5449
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5450
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5451
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isTextAlignmentInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5452
    invoke-virtual {v0}, Landroid/view/View;->resetResolvedTextAlignment()V

    #@17
    .line 5449
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5455
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resetResolvedTextDirection()V
    .registers 5

    #@0
    .prologue
    .line 5430
    invoke-super {p0}, Landroid/view/View;->resetResolvedTextDirection()V

    #@3
    .line 5432
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5433
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5434
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5435
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isTextDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5436
    invoke-virtual {v0}, Landroid/view/View;->resetResolvedTextDirection()V

    #@17
    .line 5433
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5439
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method protected resolveDrawables()V
    .registers 5

    #@0
    .prologue
    .line 5386
    invoke-super {p0}, Landroid/view/View;->resolveDrawables()V

    #@3
    .line 5387
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5388
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5389
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5390
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5391
    invoke-virtual {v0}, Landroid/view/View;->resolveDrawables()V

    #@17
    .line 5388
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5394
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resolveLayoutDirection()Z
    .registers 6

    #@0
    .prologue
    .line 5317
    invoke-super {p0}, Landroid/view/View;->resolveLayoutDirection()Z

    #@3
    move-result v3

    #@4
    .line 5318
    .local v3, result:Z
    if-eqz v3, :cond_1d

    #@6
    .line 5319
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@9
    move-result v1

    #@a
    .line 5320
    .local v1, count:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v1, :cond_1d

    #@d
    .line 5321
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 5322
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_1a

    #@17
    .line 5323
    invoke-virtual {v0}, Landroid/view/View;->resolveLayoutDirection()Z

    #@1a
    .line 5320
    :cond_1a
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_b

    #@1d
    .line 5327
    .end local v0           #child:Landroid/view/View;
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_1d
    return v3
.end method

.method public resolveLayoutParams()V
    .registers 4

    #@0
    .prologue
    .line 5401
    invoke-super {p0}, Landroid/view/View;->resolveLayoutParams()V

    #@3
    .line 5402
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5403
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_14

    #@a
    .line 5404
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5405
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->resolveLayoutParams()V

    #@11
    .line 5403
    add-int/lit8 v2, v2, 0x1

    #@13
    goto :goto_8

    #@14
    .line 5407
    .end local v0           #child:Landroid/view/View;
    :cond_14
    return-void
.end method

.method public resolvePadding()V
    .registers 5

    #@0
    .prologue
    .line 5371
    invoke-super {p0}, Landroid/view/View;->resolvePadding()V

    #@3
    .line 5372
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5373
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5374
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5375
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5376
    invoke-virtual {v0}, Landroid/view/View;->resolvePadding()V

    #@17
    .line 5373
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5379
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resolveRtlPropertiesIfNeeded()V
    .registers 5

    #@0
    .prologue
    .line 5302
    invoke-super {p0}, Landroid/view/View;->resolveRtlPropertiesIfNeeded()V

    #@3
    .line 5303
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v1

    #@7
    .line 5304
    .local v1, count:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_1a

    #@a
    .line 5305
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 5306
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isLayoutDirectionInherited()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_17

    #@14
    .line 5307
    invoke-virtual {v0}, Landroid/view/View;->resolveRtlPropertiesIfNeeded()V

    #@17
    .line 5304
    :cond_17
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 5310
    .end local v0           #child:Landroid/view/View;
    :cond_1a
    return-void
.end method

.method public resolveTextAlignment()Z
    .registers 6

    #@0
    .prologue
    .line 5353
    invoke-super {p0}, Landroid/view/View;->resolveTextAlignment()Z

    #@3
    move-result v3

    #@4
    .line 5354
    .local v3, result:Z
    if-eqz v3, :cond_1d

    #@6
    .line 5355
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@9
    move-result v1

    #@a
    .line 5356
    .local v1, count:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v1, :cond_1d

    #@d
    .line 5357
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 5358
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isTextAlignmentInherited()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_1a

    #@17
    .line 5359
    invoke-virtual {v0}, Landroid/view/View;->resolveTextAlignment()Z

    #@1a
    .line 5356
    :cond_1a
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_b

    #@1d
    .line 5363
    .end local v0           #child:Landroid/view/View;
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_1d
    return v3
.end method

.method public resolveTextDirection()Z
    .registers 6

    #@0
    .prologue
    .line 5335
    invoke-super {p0}, Landroid/view/View;->resolveTextDirection()Z

    #@3
    move-result v3

    #@4
    .line 5336
    .local v3, result:Z
    if-eqz v3, :cond_1d

    #@6
    .line 5337
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@9
    move-result v1

    #@a
    .line 5338
    .local v1, count:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v1, :cond_1d

    #@d
    .line 5339
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 5340
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isTextDirectionInherited()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_1a

    #@17
    .line 5341
    invoke-virtual {v0}, Landroid/view/View;->resolveTextDirection()Z

    #@1a
    .line 5338
    :cond_1a
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_b

    #@1d
    .line 5345
    .end local v0           #child:Landroid/view/View;
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_1d
    return v3
.end method

.method public scheduleLayoutAnimation()V
    .registers 2

    #@0
    .prologue
    .line 4455
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2
    or-int/lit8 v0, v0, 0x8

    #@4
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@6
    .line 4456
    return-void
.end method

.method public setAddStatesFromChildren(Z)V
    .registers 3
    .parameter "addsStates"

    #@0
    .prologue
    .line 5238
    if-eqz p1, :cond_c

    #@2
    .line 5239
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4
    or-int/lit16 v0, v0, 0x2000

    #@6
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8
    .line 5244
    :goto_8
    invoke-virtual {p0}, Landroid/view/ViewGroup;->refreshDrawableState()V

    #@b
    .line 5245
    return-void

    #@c
    .line 5241
    :cond_c
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@e
    and-int/lit16 v0, v0, -0x2001

    #@10
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@12
    goto :goto_8
.end method

.method public setAlwaysDrawnWithCacheEnabled(Z)V
    .registers 3
    .parameter "always"

    #@0
    .prologue
    .line 4545
    const/16 v0, 0x4000

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@5
    .line 4546
    return-void
.end method

.method public setAnimationCacheEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 4509
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@5
    .line 4510
    return-void
.end method

.method protected setChildrenDrawingCacheEnabled(Z)V
    .registers 7
    .parameter "enabled"

    #@0
    .prologue
    .line 2651
    if-nez p1, :cond_9

    #@2
    iget v3, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@4
    and-int/lit8 v3, v3, 0x3

    #@6
    const/4 v4, 0x3

    #@7
    if-eq v3, v4, :cond_18

    #@9
    .line 2652
    :cond_9
    iget-object v0, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@b
    .line 2653
    .local v0, children:[Landroid/view/View;
    iget v1, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@d
    .line 2654
    .local v1, count:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v1, :cond_18

    #@10
    .line 2655
    aget-object v3, v0, v2

    #@12
    invoke-virtual {v3, p1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    #@15
    .line 2654
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_e

    #@18
    .line 2658
    .end local v0           #children:[Landroid/view/View;
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_18
    return-void
.end method

.method protected setChildrenDrawingOrderEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 4605
    const/16 v0, 0x400

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@5
    .line 4606
    return-void
.end method

.method protected setChildrenDrawnWithCacheEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 4576
    const v0, 0x8000

    #@3
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@6
    .line 4577
    return-void
.end method

.method public setClipChildren(Z)V
    .registers 7
    .parameter "clipChildren"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2980
    iget v4, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@3
    and-int/lit8 v4, v4, 0x1

    #@5
    if-ne v4, v3, :cond_22

    #@7
    move v2, v3

    #@8
    .line 2981
    .local v2, previousValue:Z
    :goto_8
    if-eq p1, v2, :cond_24

    #@a
    .line 2982
    invoke-direct {p0, v3, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@d
    .line 2983
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@10
    if-ge v1, v3, :cond_24

    #@12
    .line 2984
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 2985
    .local v0, child:Landroid/view/View;
    iget-object v3, v0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@18
    if-eqz v3, :cond_1f

    #@1a
    .line 2986
    iget-object v3, v0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1c
    invoke-virtual {v3, p1}, Landroid/view/DisplayList;->setClipChildren(Z)V

    #@1f
    .line 2983
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_e

    #@22
    .line 2980
    .end local v0           #child:Landroid/view/View;
    .end local v1           #i:I
    .end local v2           #previousValue:Z
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_8

    #@24
    .line 2990
    .restart local v2       #previousValue:Z
    :cond_24
    return-void
.end method

.method public setClipToPadding(Z)V
    .registers 3
    .parameter "clipToPadding"

    #@0
    .prologue
    .line 3001
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@4
    .line 3002
    return-void
.end method

.method public setDescendantFocusability(I)V
    .registers 4
    .parameter "focusability"

    #@0
    .prologue
    .line 571
    sparse-switch p1, :sswitch_data_1e

    #@3
    .line 577
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v1, "must be one of FOCUS_BEFORE_DESCENDANTS, FOCUS_AFTER_DESCENDANTS, FOCUS_BLOCK_DESCENDANTS"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 580
    :sswitch_c
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@e
    const v1, -0x60001

    #@11
    and-int/2addr v0, v1

    #@12
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@14
    .line 581
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@16
    const/high16 v1, 0x6

    #@18
    and-int/2addr v1, p1

    #@19
    or-int/2addr v0, v1

    #@1a
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1c
    .line 582
    return-void

    #@1d
    .line 571
    nop

    #@1e
    :sswitch_data_1e
    .sparse-switch
        0x20000 -> :sswitch_c
        0x40000 -> :sswitch_c
        0x60000 -> :sswitch_c
    .end sparse-switch
.end method

.method public setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V
    .registers 3
    .parameter "controller"

    #@0
    .prologue
    .line 4465
    iput-object p1, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@2
    .line 4466
    iget-object v0, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 4467
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8
    or-int/lit8 v0, v0, 0x8

    #@a
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@c
    .line 4469
    :cond_c
    return-void
.end method

.method public setLayoutAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
    .registers 2
    .parameter "animationListener"

    #@0
    .prologue
    .line 5278
    iput-object p1, p0, Landroid/view/ViewGroup;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    .line 5279
    return-void
.end method

.method public setLayoutMode(I)V
    .registers 3
    .parameter "layoutMode"

    #@0
    .prologue
    .line 4678
    iget v0, p0, Landroid/view/ViewGroup;->mLayoutMode:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 4679
    iput p1, p0, Landroid/view/ViewGroup;->mLayoutMode:I

    #@6
    .line 4680
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@9
    .line 4682
    :cond_9
    return-void
.end method

.method public setLayoutTransition(Landroid/animation/LayoutTransition;)V
    .registers 4
    .parameter "transition"

    #@0
    .prologue
    .line 3690
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 3691
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@6
    iget-object v1, p0, Landroid/view/ViewGroup;->mLayoutTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    #@8
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    #@b
    .line 3693
    :cond_b
    iput-object p1, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@d
    .line 3694
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@f
    if-eqz v0, :cond_18

    #@11
    .line 3695
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransition:Landroid/animation/LayoutTransition;

    #@13
    iget-object v1, p0, Landroid/view/ViewGroup;->mLayoutTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    #@15
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    #@18
    .line 3697
    :cond_18
    return-void
.end method

.method public setMotionEventSplittingEnabled(Z)V
    .registers 4
    .parameter "split"

    #@0
    .prologue
    .line 2262
    if-eqz p1, :cond_a

    #@2
    .line 2263
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4
    const/high16 v1, 0x20

    #@6
    or-int/2addr v0, v1

    #@7
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@9
    .line 2267
    :goto_9
    return-void

    #@a
    .line 2265
    :cond_a
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@c
    const v1, -0x200001

    #@f
    and-int/2addr v0, v1

    #@10
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@12
    goto :goto_9
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 3303
    iput-object p1, p0, Landroid/view/ViewGroup;->mOnHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    #@2
    .line 3304
    return-void
.end method

.method public setPersistentDrawingCache(I)V
    .registers 3
    .parameter "drawingCacheToKeep"

    #@0
    .prologue
    .line 4648
    and-int/lit8 v0, p1, 0x3

    #@2
    iput v0, p0, Landroid/view/ViewGroup;->mPersistentDrawingCache:I

    #@4
    .line 4649
    return-void
.end method

.method protected setStaticTransformationsEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 3058
    const/16 v0, 0x800

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewGroup;->setBooleanFlag(IZ)V

    #@5
    .line 3059
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 5499
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .registers 3
    .parameter "originalView"

    #@0
    .prologue
    .line 647
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 4
    .parameter "originalView"
    .parameter "callback"

    #@0
    .prologue
    .line 654
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v0, p1, p2}, Landroid/view/ViewParent;->startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public startLayoutAnimation()V
    .registers 2

    #@0
    .prologue
    .line 4442
    iget-object v0, p0, Landroid/view/ViewGroup;->mLayoutAnimationController:Landroid/view/animation/LayoutAnimationController;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 4443
    iget v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@6
    or-int/lit8 v0, v0, 0x8

    #@8
    iput v0, p0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@a
    .line 4444
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    #@d
    .line 4446
    :cond_d
    return-void
.end method

.method public startViewTransition(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 5040
    iget-object v0, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-ne v0, p0, :cond_14

    #@4
    .line 5041
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@6
    if-nez v0, :cond_f

    #@8
    .line 5042
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@f
    .line 5044
    :cond_f
    iget-object v0, p0, Landroid/view/ViewGroup;->mTransitioningViews:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 5046
    :cond_14
    return-void
.end method

.method unFocus()V
    .registers 2

    #@0
    .prologue
    .line 813
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@2
    if-nez v0, :cond_8

    #@4
    .line 814
    invoke-super {p0}, Landroid/view/View;->unFocus()V

    #@7
    .line 819
    :goto_7
    return-void

    #@8
    .line 816
    :cond_8
    iget-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@a
    invoke-virtual {v0}, Landroid/view/View;->unFocus()V

    #@d
    .line 817
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Landroid/view/ViewGroup;->mFocused:Landroid/view/View;

    #@10
    goto :goto_7
.end method

.method updateLocalSystemUiVisibility(II)Z
    .registers 9
    .parameter "localValue"
    .parameter "localChanges"

    #@0
    .prologue
    .line 1349
    invoke-super {p0, p1, p2}, Landroid/view/View;->updateLocalSystemUiVisibility(II)Z

    #@3
    move-result v0

    #@4
    .line 1351
    .local v0, changed:Z
    iget v3, p0, Landroid/view/ViewGroup;->mChildrenCount:I

    #@6
    .line 1352
    .local v3, count:I
    iget-object v2, p0, Landroid/view/ViewGroup;->mChildren:[Landroid/view/View;

    #@8
    .line 1353
    .local v2, children:[Landroid/view/View;
    const/4 v4, 0x0

    #@9
    .local v4, i:I
    :goto_9
    if-ge v4, v3, :cond_15

    #@b
    .line 1354
    aget-object v1, v2, v4

    #@d
    .line 1355
    .local v1, child:Landroid/view/View;
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->updateLocalSystemUiVisibility(II)Z

    #@10
    move-result v5

    #@11
    or-int/2addr v0, v5

    #@12
    .line 1353
    add-int/lit8 v4, v4, 0x1

    #@14
    goto :goto_9

    #@15
    .line 1357
    .end local v1           #child:Landroid/view/View;
    :cond_15
    return v0
.end method

.method public updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 6
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 3257
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_1f

    #@6
    .line 3258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Invalid LayoutParams supplied to "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 3260
    :cond_1f
    iget-object v0, p1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@21
    if-eq v0, p0, :cond_3c

    #@23
    .line 3261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@25
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, "Given view not a child of "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v0

    #@3c
    .line 3263
    :cond_3c
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@3f
    .line 3264
    return-void
.end method
