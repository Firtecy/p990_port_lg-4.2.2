.class Landroid/view/GLES20Canvas;
.super Landroid/view/HardwareCanvas;
.source "GLES20Canvas.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/GLES20Canvas$CanvasFinalizer;
    }
.end annotation


# static fields
.field public static final FLUSH_CACHES_FULL:I = 0x2

.field public static final FLUSH_CACHES_LAYERS:I = 0x0

.field public static final FLUSH_CACHES_MODERATE:I = 0x1

.field private static final MODIFIER_COLOR_FILTER:I = 0x4

.field private static final MODIFIER_NONE:I = 0x0

.field private static final MODIFIER_SHADER:I = 0x2

.field private static final MODIFIER_SHADOW:I = 0x1

.field private static sIsAvailable:Z


# instance fields
.field private final mClipBounds:Landroid/graphics/Rect;

.field private mFilter:Landroid/graphics/DrawFilter;

.field private mFinalizer:Landroid/view/GLES20Canvas$CanvasFinalizer;

.field private mHeight:I

.field private final mLine:[F

.field private final mOpaque:Z

.field private final mPathBounds:Landroid/graphics/RectF;

.field private final mPoint:[F

.field private mRenderer:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 74
    invoke-static {}, Landroid/view/GLES20Canvas;->nIsAvailable()Z

    #@3
    move-result v0

    #@4
    sput-boolean v0, Landroid/view/GLES20Canvas;->sIsAvailable:Z

    #@6
    return-void
.end method

.method constructor <init>(IZ)V
    .registers 4
    .parameter "layer"
    .parameter "translucent"

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Landroid/view/HardwareCanvas;-><init>()V

    #@3
    .line 61
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [F

    #@6
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mPoint:[F

    #@8
    .line 62
    const/4 v0, 0x4

    #@9
    new-array v0, v0, [F

    #@b
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@d
    .line 64
    new-instance v0, Landroid/graphics/Rect;

    #@f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@12
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@14
    .line 65
    new-instance v0, Landroid/graphics/RectF;

    #@16
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@19
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@1b
    .line 95
    if-nez p2, :cond_2a

    #@1d
    const/4 v0, 0x1

    #@1e
    :goto_1e
    iput-boolean v0, p0, Landroid/view/GLES20Canvas;->mOpaque:Z

    #@20
    .line 96
    invoke-static {p1}, Landroid/view/GLES20Canvas;->nCreateLayerRenderer(I)I

    #@23
    move-result v0

    #@24
    iput v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@26
    .line 97
    invoke-direct {p0}, Landroid/view/GLES20Canvas;->setupFinalizer()V

    #@29
    .line 98
    return-void

    #@2a
    .line 95
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_1e
.end method

.method constructor <init>(Z)V
    .registers 3
    .parameter "translucent"

    #@0
    .prologue
    .line 88
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/GLES20Canvas;-><init>(ZZ)V

    #@4
    .line 89
    return-void
.end method

.method protected constructor <init>(ZZ)V
    .registers 4
    .parameter "record"
    .parameter "translucent"

    #@0
    .prologue
    .line 100
    invoke-direct {p0}, Landroid/view/HardwareCanvas;-><init>()V

    #@3
    .line 61
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [F

    #@6
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mPoint:[F

    #@8
    .line 62
    const/4 v0, 0x4

    #@9
    new-array v0, v0, [F

    #@b
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@d
    .line 64
    new-instance v0, Landroid/graphics/Rect;

    #@f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@12
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@14
    .line 65
    new-instance v0, Landroid/graphics/RectF;

    #@16
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@19
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@1b
    .line 101
    if-nez p2, :cond_2c

    #@1d
    const/4 v0, 0x1

    #@1e
    :goto_1e
    iput-boolean v0, p0, Landroid/view/GLES20Canvas;->mOpaque:Z

    #@20
    .line 103
    if-eqz p1, :cond_2e

    #@22
    .line 104
    invoke-static {}, Landroid/view/GLES20Canvas;->nCreateDisplayListRenderer()I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@28
    .line 109
    :goto_28
    invoke-direct {p0}, Landroid/view/GLES20Canvas;->setupFinalizer()V

    #@2b
    .line 110
    return-void

    #@2c
    .line 101
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_1e

    #@2e
    .line 106
    :cond_2e
    invoke-static {}, Landroid/view/GLES20Canvas;->nCreateRenderer()I

    #@31
    move-result v0

    #@32
    iput v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@34
    goto :goto_28
.end method

.method static synthetic access$000(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 43
    invoke-static {p0}, Landroid/view/GLES20Canvas;->nDestroyRenderer(I)V

    #@3
    return-void
.end method

.method static destroyDisplayList(I)V
    .registers 1
    .parameter "displayList"

    #@0
    .prologue
    .line 380
    invoke-static {p0}, Landroid/view/GLES20Canvas;->nDestroyDisplayList(I)V

    #@3
    .line 381
    return-void
.end method

.method public static flushCaches(I)V
    .registers 1
    .parameter "level"

    #@0
    .prologue
    .line 337
    invoke-static {p0}, Landroid/view/GLES20Canvas;->nFlushCaches(I)V

    #@3
    .line 338
    return-void
.end method

.method static getDisplayListSize(I)I
    .registers 2
    .parameter "displayList"

    #@0
    .prologue
    .line 386
    invoke-static {p0}, Landroid/view/GLES20Canvas;->nGetDisplayListSize(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getLayerCount(I)I
    .registers 2
    .parameter "ifNeedTerminate"

    #@0
    .prologue
    .line 343
    invoke-static {p0}, Landroid/view/GLES20Canvas;->nGetLayerCount(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getStencilSize()I
    .registers 1

    #@0
    .prologue
    .line 263
    invoke-static {}, Landroid/view/GLES20Canvas;->nGetStencilSize()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static initCaches()V
    .registers 0

    #@0
    .prologue
    .line 364
    invoke-static {}, Landroid/view/GLES20Canvas;->nInitCaches()V

    #@3
    .line 365
    return-void
.end method

.method static isAvailable()Z
    .registers 1

    #@0
    .prologue
    .line 77
    sget-boolean v0, Landroid/view/GLES20Canvas;->sIsAvailable:Z

    #@2
    return v0
.end method

.method private static native nAttachFunctor(II)V
.end method

.method private static native nCallDrawGLFunction(II)I
.end method

.method static native nClearLayerTexture(I)V
.end method

.method private static native nClearLayerUpdates(I)V
.end method

.method private static native nClipRect(IFFFFI)Z
.end method

.method private static native nClipRect(IIIIII)Z
.end method

.method private static native nConcatMatrix(II)V
.end method

.method static native nCopyLayer(II)Z
.end method

.method private static native nCreateDisplayListRenderer()I
.end method

.method static native nCreateLayer(IIZ[I)I
.end method

.method private static native nCreateLayerRenderer(I)I
.end method

.method private static native nCreateRenderer()I
.end method

.method static native nCreateTextureLayer(Z[I)I
.end method

.method private static native nDestroyDisplayList(I)V
.end method

.method static native nDestroyLayer(I)V
.end method

.method static native nDestroyLayerDeferred(I)V
.end method

.method private static native nDestroyRenderer(I)V
.end method

.method private static native nDetachFunctor(II)V
.end method

.method private static native nDrawArc(IFFFFFFZI)V
.end method

.method private static native nDrawBitmap(II[BFFFFFFFFI)V
.end method

.method private static native nDrawBitmap(II[BFFI)V
.end method

.method private static native nDrawBitmap(II[BII)V
.end method

.method private static native nDrawBitmap(I[IIIFFIIZI)V
.end method

.method private static native nDrawBitmapMesh(II[BII[FI[III)V
.end method

.method private static native nDrawCircle(IFFFI)V
.end method

.method private static native nDrawColor(III)V
.end method

.method private static native nDrawDisplayList(IILandroid/graphics/Rect;I)I
.end method

.method private static native nDrawLayer(IIFFI)V
.end method

.method private static native nDrawLines(I[FIII)V
.end method

.method private static native nDrawOval(IFFFFI)V
.end method

.method private static native nDrawPatch(II[B[BFFFFI)V
.end method

.method private static native nDrawPath(III)V
.end method

.method private static native nDrawPoints(I[FIII)V
.end method

.method private static native nDrawPosText(ILjava/lang/String;II[FI)V
.end method

.method private static native nDrawPosText(I[CII[FI)V
.end method

.method private static native nDrawRect(IFFFFI)V
.end method

.method private static native nDrawRects(III)V
.end method

.method private static native nDrawRoundRect(IFFFFFFI)V
.end method

.method private static native nDrawText(ILjava/lang/String;IIFFII)V
.end method

.method private static native nDrawText(I[CIIFFII)V
.end method

.method private static native nDrawTextOnPath(ILjava/lang/String;IIIFFII)V
.end method

.method private static native nDrawTextOnPath(I[CIIIFFII)V
.end method

.method private static native nDrawTextRun(ILjava/lang/String;IIIIFFII)V
.end method

.method private static native nDrawTextRun(I[CIIIIFFII)V
.end method

.method private static native nFinish(I)V
.end method

.method private static native nFlushCaches(I)V
.end method

.method private static native nGetClipBounds(ILandroid/graphics/Rect;)Z
.end method

.method private static native nGetDisplayList(II)I
.end method

.method private static native nGetDisplayListSize(I)I
.end method

.method private static nGetLayerCount(I)I
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private static native nGetMatrix(II)V
.end method

.method private static native nGetMaximumTextureHeight()I
.end method

.method private static native nGetMaximumTextureWidth()I
.end method

.method private static native nGetSaveCount(I)I
.end method

.method private static native nGetStencilSize()I
.end method

.method private static native nInitCaches()V
.end method

.method private static native nInterrupt(I)V
.end method

.method private static native nInvokeFunctors(ILandroid/graphics/Rect;)I
.end method

.method private static native nIsAvailable()Z
.end method

.method private static native nOutputDisplayList(II)V
.end method

.method private static native nPrepare(IZ)I
.end method

.method private static native nPrepareDirty(IIIIIZ)I
.end method

.method private static native nPushLayerUpdate(II)V
.end method

.method private static native nQuickReject(IFFFFI)Z
.end method

.method private static native nResetDisplayListRenderer(I)V
.end method

.method private static native nResetModifiers(II)V
.end method

.method private static native nResetPaintFilter(I)V
.end method

.method static native nResizeLayer(III[I)Z
.end method

.method private static native nRestore(I)V
.end method

.method private static native nRestoreToCount(II)V
.end method

.method private static native nResume(I)V
.end method

.method private static native nRotate(IF)V
.end method

.method private static native nSave(II)I
.end method

.method private static native nSaveLayer(IFFFFII)I
.end method

.method private static native nSaveLayer(III)I
.end method

.method private static native nSaveLayerAlpha(IFFFFII)I
.end method

.method private static native nSaveLayerAlpha(III)I
.end method

.method private static native nScale(IFF)V
.end method

.method private static native nSetDisplayListName(ILjava/lang/String;)V
.end method

.method static native nSetLayerColorFilter(II)V
.end method

.method static native nSetLayerPaint(II)V
.end method

.method private static native nSetMatrix(II)V
.end method

.method static native nSetOpaqueLayer(IZ)V
.end method

.method static native nSetTextureLayerTransform(II)V
.end method

.method private static native nSetViewport(III)V
.end method

.method private static native nSetupColorFilter(II)V
.end method

.method private static native nSetupPaintFilter(III)V
.end method

.method private static native nSetupShader(II)V
.end method

.method private static native nSetupShadow(IFFFI)V
.end method

.method private static native nSkew(IFF)V
.end method

.method private static native nTerminateCaches()V
.end method

.method private static native nTranslate(IFF)V
.end method

.method static native nUpdateRenderLayer(IIIIIII)V
.end method

.method static native nUpdateTextureLayer(IIIZLandroid/graphics/SurfaceTexture;)V
.end method

.method static setDisplayListName(ILjava/lang/String;)V
    .registers 2
    .parameter "displayList"
    .parameter "name"

    #@0
    .prologue
    .line 392
    invoke-static {p0, p1}, Landroid/view/GLES20Canvas;->nSetDisplayListName(ILjava/lang/String;)V

    #@3
    .line 393
    return-void
.end method

.method private setupColorFilter(Landroid/graphics/Paint;)I
    .registers 5
    .parameter "paint"

    #@0
    .prologue
    .line 1395
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    #@3
    move-result-object v0

    #@4
    .line 1396
    .local v0, filter:Landroid/graphics/ColorFilter;
    if-eqz v0, :cond_f

    #@6
    .line 1397
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@8
    iget v2, v0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@a
    invoke-static {v1, v2}, Landroid/view/GLES20Canvas;->nSetupColorFilter(II)V

    #@d
    .line 1398
    const/4 v1, 0x4

    #@e
    .line 1400
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method private setupFinalizer()V
    .registers 3

    #@0
    .prologue
    .line 113
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 114
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Could not create GLES20Canvas renderer"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 116
    :cond_c
    new-instance v0, Landroid/view/GLES20Canvas$CanvasFinalizer;

    #@e
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@10
    invoke-direct {v0, v1}, Landroid/view/GLES20Canvas$CanvasFinalizer;-><init>(I)V

    #@13
    iput-object v0, p0, Landroid/view/GLES20Canvas;->mFinalizer:Landroid/view/GLES20Canvas$CanvasFinalizer;

    #@15
    .line 118
    return-void
.end method

.method private setupModifiers(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)I
    .registers 6
    .parameter "b"
    .parameter "paint"

    #@0
    .prologue
    .line 1333
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@3
    move-result-object v1

    #@4
    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@6
    if-eq v1, v2, :cond_19

    #@8
    .line 1334
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    #@b
    move-result-object v0

    #@c
    .line 1335
    .local v0, filter:Landroid/graphics/ColorFilter;
    if-eqz v0, :cond_17

    #@e
    .line 1336
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@10
    iget v2, v0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@12
    invoke-static {v1, v2}, Landroid/view/GLES20Canvas;->nSetupColorFilter(II)V

    #@15
    .line 1337
    const/4 v1, 0x4

    #@16
    .line 1342
    .end local v0           #filter:Landroid/graphics/ColorFilter;
    :goto_16
    return v1

    #@17
    .line 1340
    .restart local v0       #filter:Landroid/graphics/ColorFilter;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_16

    #@19
    .line 1342
    .end local v0           #filter:Landroid/graphics/ColorFilter;
    :cond_19
    invoke-direct {p0, p2}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@1c
    move-result v1

    #@1d
    goto :goto_16
.end method

.method private setupModifiers(Landroid/graphics/Paint;)I
    .registers 10
    .parameter "paint"

    #@0
    .prologue
    .line 1347
    const/4 v1, 0x0

    #@1
    .line 1349
    .local v1, modifiers:I
    iget-boolean v3, p1, Landroid/graphics/Paint;->hasShadow:Z

    #@3
    if-eqz v3, :cond_14

    #@5
    .line 1350
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget v4, p1, Landroid/graphics/Paint;->shadowRadius:F

    #@9
    iget v5, p1, Landroid/graphics/Paint;->shadowDx:F

    #@b
    iget v6, p1, Landroid/graphics/Paint;->shadowDy:F

    #@d
    iget v7, p1, Landroid/graphics/Paint;->shadowColor:I

    #@f
    invoke-static {v3, v4, v5, v6, v7}, Landroid/view/GLES20Canvas;->nSetupShadow(IFFFI)V

    #@12
    .line 1352
    or-int/lit8 v1, v1, 0x1

    #@14
    .line 1355
    :cond_14
    invoke-virtual {p1}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    #@17
    move-result-object v2

    #@18
    .line 1356
    .local v2, shader:Landroid/graphics/Shader;
    if-eqz v2, :cond_23

    #@1a
    .line 1357
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1c
    iget v4, v2, Landroid/graphics/Shader;->native_shader:I

    #@1e
    invoke-static {v3, v4}, Landroid/view/GLES20Canvas;->nSetupShader(II)V

    #@21
    .line 1358
    or-int/lit8 v1, v1, 0x2

    #@23
    .line 1361
    :cond_23
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    #@26
    move-result-object v0

    #@27
    .line 1362
    .local v0, filter:Landroid/graphics/ColorFilter;
    if-eqz v0, :cond_32

    #@29
    .line 1363
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2b
    iget v4, v0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@2d
    invoke-static {v3, v4}, Landroid/view/GLES20Canvas;->nSetupColorFilter(II)V

    #@30
    .line 1364
    or-int/lit8 v1, v1, 0x4

    #@32
    .line 1367
    :cond_32
    return v1
.end method

.method private setupModifiers(Landroid/graphics/Paint;I)I
    .registers 11
    .parameter "paint"
    .parameter "flags"

    #@0
    .prologue
    .line 1371
    const/4 v1, 0x0

    #@1
    .line 1373
    .local v1, modifiers:I
    iget-boolean v3, p1, Landroid/graphics/Paint;->hasShadow:Z

    #@3
    if-eqz v3, :cond_18

    #@5
    and-int/lit8 v3, p2, 0x1

    #@7
    if-eqz v3, :cond_18

    #@9
    .line 1374
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@b
    iget v4, p1, Landroid/graphics/Paint;->shadowRadius:F

    #@d
    iget v5, p1, Landroid/graphics/Paint;->shadowDx:F

    #@f
    iget v6, p1, Landroid/graphics/Paint;->shadowDy:F

    #@11
    iget v7, p1, Landroid/graphics/Paint;->shadowColor:I

    #@13
    invoke-static {v3, v4, v5, v6, v7}, Landroid/view/GLES20Canvas;->nSetupShadow(IFFFI)V

    #@16
    .line 1376
    or-int/lit8 v1, v1, 0x1

    #@18
    .line 1379
    :cond_18
    invoke-virtual {p1}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    #@1b
    move-result-object v2

    #@1c
    .line 1380
    .local v2, shader:Landroid/graphics/Shader;
    if-eqz v2, :cond_2b

    #@1e
    and-int/lit8 v3, p2, 0x2

    #@20
    if-eqz v3, :cond_2b

    #@22
    .line 1381
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@24
    iget v4, v2, Landroid/graphics/Shader;->native_shader:I

    #@26
    invoke-static {v3, v4}, Landroid/view/GLES20Canvas;->nSetupShader(II)V

    #@29
    .line 1382
    or-int/lit8 v1, v1, 0x2

    #@2b
    .line 1385
    :cond_2b
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    #@2e
    move-result-object v0

    #@2f
    .line 1386
    .local v0, filter:Landroid/graphics/ColorFilter;
    if-eqz v0, :cond_3e

    #@31
    and-int/lit8 v3, p2, 0x4

    #@33
    if-eqz v3, :cond_3e

    #@35
    .line 1387
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@37
    iget v4, v0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@39
    invoke-static {v3, v4}, Landroid/view/GLES20Canvas;->nSetupColorFilter(II)V

    #@3c
    .line 1388
    or-int/lit8 v1, v1, 0x4

    #@3e
    .line 1391
    :cond_3e
    return v1
.end method

.method public static terminateCaches()V
    .registers 0

    #@0
    .prologue
    .line 355
    invoke-static {}, Landroid/view/GLES20Canvas;->nTerminateCaches()V

    #@3
    .line 356
    return-void
.end method


# virtual methods
.method public attachFunctor(I)V
    .registers 3
    .parameter "functor"

    #@0
    .prologue
    .line 295
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nAttachFunctor(II)V

    #@5
    .line 296
    return-void
.end method

.method public callDrawGLFunction(I)I
    .registers 3
    .parameter "drawGLFunction"

    #@0
    .prologue
    .line 274
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nCallDrawGLFunction(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method clearLayerUpdates()V
    .registers 2

    #@0
    .prologue
    .line 158
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nClearLayerUpdates(I)V

    #@5
    .line 159
    return-void
.end method

.method public clipPath(Landroid/graphics/Path;)Z
    .registers 8
    .parameter "path"

    #@0
    .prologue
    .line 443
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@6
    .line 444
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@8
    iget-object v1, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@a
    iget v1, v1, Landroid/graphics/RectF;->left:F

    #@c
    iget-object v2, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@e
    iget v2, v2, Landroid/graphics/RectF;->top:F

    #@10
    iget-object v3, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@12
    iget v3, v3, Landroid/graphics/RectF;->right:F

    #@14
    iget-object v4, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@16
    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    #@18
    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@1a
    iget v5, v5, Landroid/graphics/Region$Op;->nativeInt:I

    #@1c
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IFFFFI)Z

    #@1f
    move-result v0

    #@20
    return v0
.end method

.method public clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z
    .registers 9
    .parameter "path"
    .parameter "op"

    #@0
    .prologue
    .line 451
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@6
    .line 452
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@8
    iget-object v1, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@a
    iget v1, v1, Landroid/graphics/RectF;->left:F

    #@c
    iget-object v2, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@e
    iget v2, v2, Landroid/graphics/RectF;->top:F

    #@10
    iget-object v3, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@12
    iget v3, v3, Landroid/graphics/RectF;->right:F

    #@14
    iget-object v4, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@16
    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    #@18
    iget v5, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@1a
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IFFFFI)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public clipRect(FFFF)Z
    .registers 11
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 458
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@4
    iget v5, v1, Landroid/graphics/Region$Op;->nativeInt:I

    #@6
    move v1, p1

    #@7
    move v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IFFFFI)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public clipRect(FFFFLandroid/graphics/Region$Op;)Z
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "op"

    #@0
    .prologue
    .line 466
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v5, p5, Landroid/graphics/Region$Op;->nativeInt:I

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v3, p3

    #@7
    move v4, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IFFFFI)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public clipRect(IIII)Z
    .registers 11
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 471
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@4
    iget v5, v1, Landroid/graphics/Region$Op;->nativeInt:I

    #@6
    move v1, p1

    #@7
    move v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IIIIII)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public clipRect(Landroid/graphics/Rect;)Z
    .registers 8
    .parameter "rect"

    #@0
    .prologue
    .line 479
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@4
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@6
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@8
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@a
    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@c
    iget v5, v5, Landroid/graphics/Region$Op;->nativeInt:I

    #@e
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IIIIII)Z

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z
    .registers 9
    .parameter "rect"
    .parameter "op"

    #@0
    .prologue
    .line 485
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@4
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@6
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@8
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@a
    iget v5, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@c
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IIIIII)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public clipRect(Landroid/graphics/RectF;)Z
    .registers 8
    .parameter "rect"

    #@0
    .prologue
    .line 490
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@8
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@a
    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@c
    iget v5, v5, Landroid/graphics/Region$Op;->nativeInt:I

    #@e
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IFFFFI)Z

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z
    .registers 9
    .parameter "rect"
    .parameter "op"

    #@0
    .prologue
    .line 496
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@8
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@a
    iget v5, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@c
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IFFFFI)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public clipRegion(Landroid/graphics/Region;)Z
    .registers 8
    .parameter "region"

    #@0
    .prologue
    .line 502
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Region;->getBounds(Landroid/graphics/Rect;)Z

    #@5
    .line 503
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget-object v1, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@9
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@b
    iget-object v2, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@d
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@f
    iget-object v3, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@11
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@13
    iget-object v4, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@15
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@17
    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@19
    iget v5, v5, Landroid/graphics/Region$Op;->nativeInt:I

    #@1b
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IIIIII)Z

    #@1e
    move-result v0

    #@1f
    return v0
.end method

.method public clipRegion(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z
    .registers 9
    .parameter "region"
    .parameter "op"

    #@0
    .prologue
    .line 510
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Region;->getBounds(Landroid/graphics/Rect;)Z

    #@5
    .line 511
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget-object v1, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@9
    iget v1, v1, Landroid/graphics/Rect;->left:I

    #@b
    iget-object v2, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@d
    iget v2, v2, Landroid/graphics/Rect;->top:I

    #@f
    iget-object v3, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@11
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@13
    iget-object v4, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@15
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@17
    iget v5, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@19
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nClipRect(IIIIII)Z

    #@1c
    move-result v0

    #@1d
    return v0
.end method

.method public concat(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 591
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nConcatMatrix(II)V

    #@7
    .line 592
    return-void
.end method

.method public detachFunctor(I)V
    .registers 3
    .parameter "functor"

    #@0
    .prologue
    .line 288
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nDetachFunctor(II)V

    #@5
    .line 289
    return-void
.end method

.method public drawARGB(IIII)V
    .registers 7
    .parameter "a"
    .parameter "r"
    .parameter "g"
    .parameter "b"

    #@0
    .prologue
    .line 740
    and-int/lit16 v0, p1, 0xff

    #@2
    shl-int/lit8 v0, v0, 0x18

    #@4
    and-int/lit16 v1, p2, 0xff

    #@6
    shl-int/lit8 v1, v1, 0x10

    #@8
    or-int/2addr v0, v1

    #@9
    and-int/lit16 v1, p3, 0xff

    #@b
    shl-int/lit8 v1, v1, 0x8

    #@d
    or-int/2addr v0, v1

    #@e
    and-int/lit16 v1, p4, 0xff

    #@10
    or-int/2addr v0, v1

    #@11
    invoke-virtual {p0, v0}, Landroid/view/GLES20Canvas;->drawColor(I)V

    #@14
    .line 741
    return-void
.end method

.method public drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V
    .registers 16
    .parameter "oval"
    .parameter "startAngle"
    .parameter "sweepAngle"
    .parameter "useCenter"
    .parameter "paint"

    #@0
    .prologue
    .line 725
    const/4 v0, 0x6

    #@1
    invoke-direct {p0, p5, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@4
    move-result v9

    #@5
    .line 727
    .local v9, modifiers:I
    :try_start_5
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@9
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@b
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@d
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@f
    iget v8, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@11
    move v5, p2

    #@12
    move v6, p3

    #@13
    move v7, p4

    #@14
    invoke-static/range {v0 .. v8}, Landroid/view/GLES20Canvas;->nDrawArc(IFFFFFFZI)V
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1f

    #@17
    .line 730
    if-eqz v9, :cond_1e

    #@19
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1b
    invoke-static {v0, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1e
    .line 732
    :cond_1e
    return-void

    #@1f
    .line 730
    :catchall_1f
    move-exception v0

    #@20
    if-eqz v9, :cond_27

    #@22
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@24
    invoke-static {v1, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@27
    :cond_27
    throw v0
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    .registers 12
    .parameter "bitmap"
    .parameter "left"
    .parameter "top"
    .parameter "paint"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 762
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_f

    #@7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Cannot draw recycled bitmaps"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 764
    :cond_f
    if-eqz p4, :cond_2b

    #@11
    invoke-direct {p0, p1, p4}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)I

    #@14
    move-result v6

    #@15
    .line 766
    .local v6, modifiers:I
    :goto_15
    if-nez p4, :cond_2d

    #@17
    move v5, v0

    #@18
    .line 767
    .local v5, nativePaint:I
    :goto_18
    :try_start_18
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1a
    iget v1, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@1c
    iget-object v2, p1, Landroid/graphics/Bitmap;->mBuffer:[B

    #@1e
    move v3, p2

    #@1f
    move v4, p3

    #@20
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nDrawBitmap(II[BFFI)V
    :try_end_23
    .catchall {:try_start_18 .. :try_end_23} :catchall_30

    #@23
    .line 769
    if-eqz v6, :cond_2a

    #@25
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@27
    invoke-static {v0, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2a
    .line 771
    :cond_2a
    return-void

    #@2b
    .end local v5           #nativePaint:I
    .end local v6           #modifiers:I
    :cond_2b
    move v6, v0

    #@2c
    .line 764
    goto :goto_15

    #@2d
    .line 766
    .restart local v6       #modifiers:I
    :cond_2d
    :try_start_2d
    iget v5, p4, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_2f
    .catchall {:try_start_2d .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_18

    #@30
    .line 769
    :catchall_30
    move-exception v0

    #@31
    if-eqz v6, :cond_38

    #@33
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@35
    invoke-static {v1, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@38
    :cond_38
    throw v0
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    .registers 10
    .parameter "bitmap"
    .parameter "matrix"
    .parameter "paint"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 778
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_f

    #@7
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v3, "Cannot draw recycled bitmaps"

    #@b
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 780
    :cond_f
    if-eqz p3, :cond_2b

    #@11
    invoke-direct {p0, p1, p3}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)I

    #@14
    move-result v0

    #@15
    .line 782
    .local v0, modifiers:I
    :goto_15
    if-nez p3, :cond_2d

    #@17
    move v1, v2

    #@18
    .line 783
    .local v1, nativePaint:I
    :goto_18
    :try_start_18
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1a
    iget v3, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@1c
    iget-object v4, p1, Landroid/graphics/Bitmap;->mBuffer:[B

    #@1e
    iget v5, p2, Landroid/graphics/Matrix;->native_instance:I

    #@20
    invoke-static {v2, v3, v4, v5, v1}, Landroid/view/GLES20Canvas;->nDrawBitmap(II[BII)V
    :try_end_23
    .catchall {:try_start_18 .. :try_end_23} :catchall_30

    #@23
    .line 786
    if-eqz v0, :cond_2a

    #@25
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@27
    invoke-static {v2, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2a
    .line 788
    :cond_2a
    return-void

    #@2b
    .end local v0           #modifiers:I
    .end local v1           #nativePaint:I
    :cond_2b
    move v0, v2

    #@2c
    .line 780
    goto :goto_15

    #@2d
    .line 782
    .restart local v0       #modifiers:I
    :cond_2d
    :try_start_2d
    iget v1, p3, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_2f
    .catchall {:try_start_2d .. :try_end_2f} :catchall_30

    #@2f
    goto :goto_18

    #@30
    .line 786
    :catchall_30
    move-exception v2

    #@31
    if-eqz v0, :cond_38

    #@33
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@35
    invoke-static {v3, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@38
    :cond_38
    throw v2
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .registers 25
    .parameter "bitmap"
    .parameter "src"
    .parameter "dst"
    .parameter "paint"

    #@0
    .prologue
    .line 795
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_e

    #@6
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v4, "Cannot draw recycled bitmaps"

    #@a
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v3

    #@e
    .line 797
    :cond_e
    if-eqz p4, :cond_64

    #@10
    move-object/from16 v0, p0

    #@12
    move-object/from16 v1, p1

    #@14
    move-object/from16 v2, p4

    #@16
    invoke-direct {v0, v1, v2}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)I

    #@19
    move-result v17

    #@1a
    .line 799
    .local v17, modifiers:I
    :goto_1a
    if-nez p4, :cond_67

    #@1c
    const/4 v14, 0x0

    #@1d
    .line 802
    .local v14, nativePaint:I
    :goto_1d
    if-nez p2, :cond_6c

    #@1f
    .line 803
    const/16 v19, 0x0

    #@21
    .local v19, top:I
    move/from16 v16, v19

    #@23
    .line 804
    .local v16, left:I
    :try_start_23
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@26
    move-result v18

    #@27
    .line 805
    .local v18, right:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@2a
    move-result v15

    #@2b
    .line 813
    .local v15, bottom:I
    :goto_2b
    move-object/from16 v0, p0

    #@2d
    iget v3, v0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2f
    move-object/from16 v0, p1

    #@31
    iget v4, v0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@33
    move-object/from16 v0, p1

    #@35
    iget-object v5, v0, Landroid/graphics/Bitmap;->mBuffer:[B

    #@37
    move/from16 v0, v16

    #@39
    int-to-float v6, v0

    #@3a
    move/from16 v0, v19

    #@3c
    int-to-float v7, v0

    #@3d
    move/from16 v0, v18

    #@3f
    int-to-float v8, v0

    #@40
    int-to-float v9, v15

    #@41
    move-object/from16 v0, p3

    #@43
    iget v10, v0, Landroid/graphics/Rect;->left:I

    #@45
    int-to-float v10, v10

    #@46
    move-object/from16 v0, p3

    #@48
    iget v11, v0, Landroid/graphics/Rect;->top:I

    #@4a
    int-to-float v11, v11

    #@4b
    move-object/from16 v0, p3

    #@4d
    iget v12, v0, Landroid/graphics/Rect;->right:I

    #@4f
    int-to-float v12, v12

    #@50
    move-object/from16 v0, p3

    #@52
    iget v13, v0, Landroid/graphics/Rect;->bottom:I

    #@54
    int-to-float v13, v13

    #@55
    invoke-static/range {v3 .. v14}, Landroid/view/GLES20Canvas;->nDrawBitmap(II[BFFFFFFFFI)V
    :try_end_58
    .catchall {:try_start_23 .. :try_end_58} :catchall_83

    #@58
    .line 816
    if-eqz v17, :cond_63

    #@5a
    move-object/from16 v0, p0

    #@5c
    iget v3, v0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@5e
    move/from16 v0, v17

    #@60
    invoke-static {v3, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@63
    .line 818
    :cond_63
    return-void

    #@64
    .line 797
    .end local v14           #nativePaint:I
    .end local v15           #bottom:I
    .end local v16           #left:I
    .end local v17           #modifiers:I
    .end local v18           #right:I
    .end local v19           #top:I
    :cond_64
    const/16 v17, 0x0

    #@66
    goto :goto_1a

    #@67
    .line 799
    .restart local v17       #modifiers:I
    :cond_67
    :try_start_67
    move-object/from16 v0, p4

    #@69
    iget v14, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@6b
    goto :goto_1d

    #@6c
    .line 807
    .restart local v14       #nativePaint:I
    :cond_6c
    move-object/from16 v0, p2

    #@6e
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@70
    move/from16 v16, v0

    #@72
    .line 808
    .restart local v16       #left:I
    move-object/from16 v0, p2

    #@74
    iget v0, v0, Landroid/graphics/Rect;->right:I

    #@76
    move/from16 v18, v0

    #@78
    .line 809
    .restart local v18       #right:I
    move-object/from16 v0, p2

    #@7a
    iget v0, v0, Landroid/graphics/Rect;->top:I

    #@7c
    move/from16 v19, v0

    #@7e
    .line 810
    .restart local v19       #top:I
    move-object/from16 v0, p2

    #@80
    iget v15, v0, Landroid/graphics/Rect;->bottom:I
    :try_end_82
    .catchall {:try_start_67 .. :try_end_82} :catchall_83

    #@82
    .restart local v15       #bottom:I
    goto :goto_2b

    #@83
    .line 816
    .end local v14           #nativePaint:I
    .end local v15           #bottom:I
    .end local v16           #left:I
    .end local v18           #right:I
    .end local v19           #top:I
    :catchall_83
    move-exception v3

    #@84
    if-eqz v17, :cond_8f

    #@86
    move-object/from16 v0, p0

    #@88
    iget v4, v0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@8a
    move/from16 v0, v17

    #@8c
    invoke-static {v4, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@8f
    :cond_8f
    throw v3
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 19
    .parameter "bitmap"
    .parameter "src"
    .parameter "dst"
    .parameter "paint"

    #@0
    .prologue
    .line 822
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Cannot draw recycled bitmaps"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 824
    :cond_e
    if-eqz p4, :cond_48

    #@10
    move-object/from16 v0, p4

    #@12
    invoke-direct {p0, p1, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)I

    #@15
    move-result v13

    #@16
    .line 826
    .local v13, modifiers:I
    :goto_16
    if-nez p4, :cond_4a

    #@18
    const/4 v12, 0x0

    #@19
    .line 829
    .local v12, nativePaint:I
    :goto_19
    if-nez p2, :cond_4f

    #@1b
    .line 830
    const/4 v5, 0x0

    #@1c
    .local v5, top:F
    move v4, v5

    #@1d
    .line 831
    .local v4, left:F
    :try_start_1d
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@20
    move-result v1

    #@21
    int-to-float v6, v1

    #@22
    .line 832
    .local v6, right:F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@25
    move-result v1

    #@26
    int-to-float v7, v1

    #@27
    .line 840
    .local v7, bottom:F
    :goto_27
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@29
    iget v2, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2b
    iget-object v3, p1, Landroid/graphics/Bitmap;->mBuffer:[B

    #@2d
    move-object/from16 v0, p3

    #@2f
    iget v8, v0, Landroid/graphics/RectF;->left:F

    #@31
    move-object/from16 v0, p3

    #@33
    iget v9, v0, Landroid/graphics/RectF;->top:F

    #@35
    move-object/from16 v0, p3

    #@37
    iget v10, v0, Landroid/graphics/RectF;->right:F

    #@39
    move-object/from16 v0, p3

    #@3b
    iget v11, v0, Landroid/graphics/RectF;->bottom:F

    #@3d
    invoke-static/range {v1 .. v12}, Landroid/view/GLES20Canvas;->nDrawBitmap(II[BFFFFFFFFI)V
    :try_end_40
    .catchall {:try_start_1d .. :try_end_40} :catchall_64

    #@40
    .line 843
    if-eqz v13, :cond_47

    #@42
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@44
    invoke-static {v1, v13}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@47
    .line 845
    :cond_47
    return-void

    #@48
    .line 824
    .end local v4           #left:F
    .end local v5           #top:F
    .end local v6           #right:F
    .end local v7           #bottom:F
    .end local v12           #nativePaint:I
    .end local v13           #modifiers:I
    :cond_48
    const/4 v13, 0x0

    #@49
    goto :goto_16

    #@4a
    .line 826
    .restart local v13       #modifiers:I
    :cond_4a
    :try_start_4a
    move-object/from16 v0, p4

    #@4c
    iget v12, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@4e
    goto :goto_19

    #@4f
    .line 834
    .restart local v12       #nativePaint:I
    :cond_4f
    move-object/from16 v0, p2

    #@51
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@53
    int-to-float v4, v1

    #@54
    .line 835
    .restart local v4       #left:F
    move-object/from16 v0, p2

    #@56
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@58
    int-to-float v6, v1

    #@59
    .line 836
    .restart local v6       #right:F
    move-object/from16 v0, p2

    #@5b
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@5d
    int-to-float v5, v1

    #@5e
    .line 837
    .restart local v5       #top:F
    move-object/from16 v0, p2

    #@60
    iget v1, v0, Landroid/graphics/Rect;->bottom:I
    :try_end_62
    .catchall {:try_start_4a .. :try_end_62} :catchall_64

    #@62
    int-to-float v7, v1

    #@63
    .restart local v7       #bottom:F
    goto :goto_27

    #@64
    .line 843
    .end local v4           #left:F
    .end local v5           #top:F
    .end local v6           #right:F
    .end local v7           #bottom:F
    .end local v12           #nativePaint:I
    :catchall_64
    move-exception v1

    #@65
    if-eqz v13, :cond_6c

    #@67
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@69
    invoke-static {v2, v13}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@6c
    :cond_6c
    throw v1
.end method

.method public drawBitmap([IIIFFIIZLandroid/graphics/Paint;)V
    .registers 24
    .parameter "colors"
    .parameter "offset"
    .parameter "stride"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "hasAlpha"
    .parameter "paint"

    #@0
    .prologue
    .line 854
    if-gez p6, :cond_b

    #@2
    .line 855
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "width must be >= 0"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 858
    :cond_b
    if-gez p7, :cond_15

    #@d
    .line 859
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v2, "height must be >= 0"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 862
    :cond_15
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(I)I

    #@18
    move-result v1

    #@19
    move/from16 v0, p6

    #@1b
    if-ge v1, v0, :cond_25

    #@1d
    .line 863
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v2, "abs(stride) must be >= width"

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 866
    :cond_25
    add-int/lit8 v1, p7, -0x1

    #@27
    mul-int v1, v1, p3

    #@29
    add-int v11, p2, v1

    #@2b
    .line 867
    .local v11, lastScanline:I
    array-length v12, p1

    #@2c
    .line 869
    .local v12, length:I
    if-ltz p2, :cond_38

    #@2e
    add-int v1, p2, p6

    #@30
    if-gt v1, v12, :cond_38

    #@32
    if-ltz v11, :cond_38

    #@34
    add-int v1, v11, p6

    #@36
    if-le v1, v12, :cond_3e

    #@38
    .line 871
    :cond_38
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@3a
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@3d
    throw v1

    #@3e
    .line 875
    :cond_3e
    if-eqz p9, :cond_65

    #@40
    move-object/from16 v0, p9

    #@42
    invoke-direct {p0, v0}, Landroid/view/GLES20Canvas;->setupColorFilter(Landroid/graphics/Paint;)I

    #@45
    move-result v13

    #@46
    .line 877
    .local v13, modifier:I
    :goto_46
    if-nez p9, :cond_67

    #@48
    const/4 v10, 0x0

    #@49
    .line 878
    .local v10, nativePaint:I
    :goto_49
    :try_start_49
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@4b
    move-object v2, p1

    #@4c
    move/from16 v3, p2

    #@4e
    move/from16 v4, p3

    #@50
    move/from16 v5, p4

    #@52
    move/from16 v6, p5

    #@54
    move/from16 v7, p6

    #@56
    move/from16 v8, p7

    #@58
    move/from16 v9, p8

    #@5a
    invoke-static/range {v1 .. v10}, Landroid/view/GLES20Canvas;->nDrawBitmap(I[IIIFFIIZI)V
    :try_end_5d
    .catchall {:try_start_49 .. :try_end_5d} :catchall_6c

    #@5d
    .line 881
    if-eqz v13, :cond_64

    #@5f
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@61
    invoke-static {v1, v13}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@64
    .line 883
    :cond_64
    return-void

    #@65
    .line 875
    .end local v10           #nativePaint:I
    .end local v13           #modifier:I
    :cond_65
    const/4 v13, 0x0

    #@66
    goto :goto_46

    #@67
    .line 877
    .restart local v13       #modifier:I
    :cond_67
    :try_start_67
    move-object/from16 v0, p9

    #@69
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_6b
    .catchall {:try_start_67 .. :try_end_6b} :catchall_6c

    #@6b
    goto :goto_49

    #@6c
    .line 881
    :catchall_6c
    move-exception v1

    #@6d
    if-eqz v13, :cond_74

    #@6f
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@71
    invoke-static {v2, v13}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@74
    :cond_74
    throw v1
.end method

.method public drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V
    .registers 20
    .parameter "colors"
    .parameter "offset"
    .parameter "stride"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "hasAlpha"
    .parameter "paint"

    #@0
    .prologue
    .line 892
    int-to-float v4, p4

    #@1
    int-to-float v5, p5

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move/from16 v6, p6

    #@8
    move/from16 v7, p7

    #@a
    move/from16 v8, p8

    #@c
    move-object/from16 v9, p9

    #@e
    invoke-virtual/range {v0 .. v9}, Landroid/view/GLES20Canvas;->drawBitmap([IIIFFIIZLandroid/graphics/Paint;)V

    #@11
    .line 893
    return-void
.end method

.method public drawBitmapMesh(Landroid/graphics/Bitmap;II[FI[IILandroid/graphics/Paint;)V
    .registers 22
    .parameter "bitmap"
    .parameter "meshWidth"
    .parameter "meshHeight"
    .parameter "verts"
    .parameter "vertOffset"
    .parameter "colors"
    .parameter "colorOffset"
    .parameter "paint"

    #@0
    .prologue
    .line 898
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v2, "Cannot draw recycled bitmaps"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 899
    :cond_e
    if-ltz p2, :cond_16

    #@10
    if-ltz p3, :cond_16

    #@12
    if-ltz p5, :cond_16

    #@14
    if-gez p7, :cond_1c

    #@16
    .line 900
    :cond_16
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@18
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@1b
    throw v1

    #@1c
    .line 903
    :cond_1c
    if-eqz p2, :cond_20

    #@1e
    if-nez p3, :cond_21

    #@20
    .line 922
    :cond_20
    :goto_20
    return-void

    #@21
    .line 907
    :cond_21
    add-int/lit8 v1, p2, 0x1

    #@23
    add-int/lit8 v2, p3, 0x1

    #@25
    mul-int v11, v1, v2

    #@27
    .line 908
    .local v11, count:I
    move-object/from16 v0, p4

    #@29
    array-length v1, v0

    #@2a
    mul-int/lit8 v2, v11, 0x2

    #@2c
    move/from16 v0, p5

    #@2e
    invoke-static {v1, v0, v2}, Landroid/view/GLES20Canvas;->checkRange(III)V

    #@31
    .line 911
    const/16 p6, 0x0

    #@33
    .line 912
    const/16 p7, 0x0

    #@35
    .line 914
    if-eqz p8, :cond_5c

    #@37
    move-object/from16 v0, p8

    #@39
    invoke-direct {p0, p1, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Bitmap;Landroid/graphics/Paint;)I

    #@3c
    move-result v12

    #@3d
    .line 916
    .local v12, modifiers:I
    :goto_3d
    if-nez p8, :cond_5e

    #@3f
    const/4 v10, 0x0

    #@40
    .line 917
    .local v10, nativePaint:I
    :goto_40
    :try_start_40
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@42
    iget v2, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@44
    iget-object v3, p1, Landroid/graphics/Bitmap;->mBuffer:[B

    #@46
    move v4, p2

    #@47
    move/from16 v5, p3

    #@49
    move-object/from16 v6, p4

    #@4b
    move/from16 v7, p5

    #@4d
    move-object/from16 v8, p6

    #@4f
    move/from16 v9, p7

    #@51
    invoke-static/range {v1 .. v10}, Landroid/view/GLES20Canvas;->nDrawBitmapMesh(II[BII[FI[III)V
    :try_end_54
    .catchall {:try_start_40 .. :try_end_54} :catchall_63

    #@54
    .line 920
    if-eqz v12, :cond_20

    #@56
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@58
    invoke-static {v1, v12}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@5b
    goto :goto_20

    #@5c
    .line 914
    .end local v10           #nativePaint:I
    .end local v12           #modifiers:I
    :cond_5c
    const/4 v12, 0x0

    #@5d
    goto :goto_3d

    #@5e
    .line 916
    .restart local v12       #modifiers:I
    :cond_5e
    :try_start_5e
    move-object/from16 v0, p8

    #@60
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_62
    .catchall {:try_start_5e .. :try_end_62} :catchall_63

    #@62
    goto :goto_40

    #@63
    .line 920
    :catchall_63
    move-exception v1

    #@64
    if-eqz v12, :cond_6b

    #@66
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@68
    invoke-static {v2, v12}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@6b
    :cond_6b
    throw v1
.end method

.method public drawCircle(FFFLandroid/graphics/Paint;)V
    .registers 8
    .parameter "cx"
    .parameter "cy"
    .parameter "radius"
    .parameter "paint"

    #@0
    .prologue
    .line 930
    const/4 v1, 0x6

    #@1
    invoke-direct {p0, p4, v1}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@4
    move-result v0

    #@5
    .line 932
    .local v0, modifiers:I
    :try_start_5
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget v2, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@9
    invoke-static {v1, p1, p2, p3, v2}, Landroid/view/GLES20Canvas;->nDrawCircle(IFFFI)V
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_14

    #@c
    .line 934
    if-eqz v0, :cond_13

    #@e
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@10
    invoke-static {v1, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@13
    .line 936
    :cond_13
    return-void

    #@14
    .line 934
    :catchall_14
    move-exception v1

    #@15
    if-eqz v0, :cond_1c

    #@17
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@19
    invoke-static {v2, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1c
    :cond_1c
    throw v1
.end method

.method public drawColor(I)V
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 943
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/view/GLES20Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@5
    .line 944
    return-void
.end method

.method public drawColor(ILandroid/graphics/PorterDuff$Mode;)V
    .registers 5
    .parameter "color"
    .parameter "mode"

    #@0
    .prologue
    .line 948
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p2, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@4
    invoke-static {v0, p1, v1}, Landroid/view/GLES20Canvas;->nDrawColor(III)V

    #@7
    .line 949
    return-void
.end method

.method public drawDisplayList(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I
    .registers 6
    .parameter "displayList"
    .parameter "dirty"
    .parameter "flags"

    #@0
    .prologue
    .line 399
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    check-cast p1, Landroid/view/GLES20DisplayList;

    #@4
    .end local p1
    invoke-virtual {p1}, Landroid/view/GLES20DisplayList;->getNativeDisplayList()I

    #@7
    move-result v1

    #@8
    invoke-static {v0, v1, p2, p3}, Landroid/view/GLES20Canvas;->nDrawDisplayList(IILandroid/graphics/Rect;I)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method drawHardwareLayer(Landroid/view/HardwareLayer;FFLandroid/graphics/Paint;)V
    .registers 9
    .parameter "layer"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 418
    move-object v0, p1

    #@1
    check-cast v0, Landroid/view/GLES20Layer;

    #@3
    .line 419
    .local v0, glLayer:Landroid/view/GLES20Layer;
    if-nez p4, :cond_10

    #@5
    const/4 v1, 0x0

    #@6
    .line 420
    .local v1, nativePaint:I
    :goto_6
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@8
    invoke-virtual {v0}, Landroid/view/GLES20Layer;->getLayer()I

    #@b
    move-result v3

    #@c
    invoke-static {v2, v3, p2, p3, v1}, Landroid/view/GLES20Canvas;->nDrawLayer(IIFFI)V

    #@f
    .line 421
    return-void

    #@10
    .line 419
    .end local v1           #nativePaint:I
    :cond_10
    iget v1, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@12
    goto :goto_6
.end method

.method public drawLine(FFFFLandroid/graphics/Paint;)V
    .registers 9
    .parameter "startX"
    .parameter "startY"
    .parameter "stopX"
    .parameter "stopY"
    .parameter "paint"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 955
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@3
    aput p1, v0, v2

    #@5
    .line 956
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@7
    const/4 v1, 0x1

    #@8
    aput p2, v0, v1

    #@a
    .line 957
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@c
    const/4 v1, 0x2

    #@d
    aput p3, v0, v1

    #@f
    .line 958
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@11
    const/4 v1, 0x3

    #@12
    aput p4, v0, v1

    #@14
    .line 959
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mLine:[F

    #@16
    const/4 v1, 0x4

    #@17
    invoke-virtual {p0, v0, v2, v1, p5}, Landroid/view/GLES20Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    #@1a
    .line 960
    return-void
.end method

.method public drawLines([FIILandroid/graphics/Paint;)V
    .registers 8
    .parameter "pts"
    .parameter "offset"
    .parameter "count"
    .parameter "paint"

    #@0
    .prologue
    .line 964
    or-int v1, p2, p3

    #@2
    if-ltz v1, :cond_9

    #@4
    add-int v1, p2, p3

    #@6
    array-length v2, p1

    #@7
    if-le v1, v2, :cond_11

    #@9
    .line 965
    :cond_9
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v2, "The lines array must contain 4 elements per line."

    #@d
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 967
    :cond_11
    const/4 v1, 0x6

    #@12
    invoke-direct {p0, p4, v1}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@15
    move-result v0

    #@16
    .line 969
    .local v0, modifiers:I
    :try_start_16
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@18
    iget v2, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@1a
    invoke-static {v1, p1, p2, p3, v2}, Landroid/view/GLES20Canvas;->nDrawLines(I[FIII)V
    :try_end_1d
    .catchall {:try_start_16 .. :try_end_1d} :catchall_25

    #@1d
    .line 971
    if-eqz v0, :cond_24

    #@1f
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@21
    invoke-static {v1, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@24
    .line 973
    :cond_24
    return-void

    #@25
    .line 971
    :catchall_25
    move-exception v1

    #@26
    if-eqz v0, :cond_2d

    #@28
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2a
    invoke-static {v2, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2d
    :cond_2d
    throw v1
.end method

.method public drawLines([FLandroid/graphics/Paint;)V
    .registers 5
    .parameter "pts"
    .parameter "paint"

    #@0
    .prologue
    .line 980
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/view/GLES20Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    #@5
    .line 981
    return-void
.end method

.method public drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 10
    .parameter "oval"
    .parameter "paint"

    #@0
    .prologue
    .line 985
    const/4 v0, 0x6

    #@1
    invoke-direct {p0, p2, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@4
    move-result v6

    #@5
    .line 987
    .local v6, modifiers:I
    :try_start_5
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@9
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@b
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@d
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@f
    iget v5, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@11
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nDrawOval(IFFFFI)V
    :try_end_14
    .catchall {:try_start_5 .. :try_end_14} :catchall_1c

    #@14
    .line 989
    if-eqz v6, :cond_1b

    #@16
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@18
    invoke-static {v0, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1b
    .line 991
    :cond_1b
    return-void

    #@1c
    .line 989
    :catchall_1c
    move-exception v0

    #@1d
    if-eqz v6, :cond_24

    #@1f
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@21
    invoke-static {v1, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@24
    :cond_24
    throw v0
.end method

.method public drawPaint(Landroid/graphics/Paint;)V
    .registers 9
    .parameter "paint"

    #@0
    .prologue
    .line 998
    iget-object v6, p0, Landroid/view/GLES20Canvas;->mClipBounds:Landroid/graphics/Rect;

    #@2
    .line 999
    .local v6, r:Landroid/graphics/Rect;
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@4
    invoke-static {v0, v6}, Landroid/view/GLES20Canvas;->nGetClipBounds(ILandroid/graphics/Rect;)Z

    #@7
    .line 1000
    iget v0, v6, Landroid/graphics/Rect;->left:I

    #@9
    int-to-float v1, v0

    #@a
    iget v0, v6, Landroid/graphics/Rect;->top:I

    #@c
    int-to-float v2, v0

    #@d
    iget v0, v6, Landroid/graphics/Rect;->right:I

    #@f
    int-to-float v3, v0

    #@10
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    #@12
    int-to-float v4, v0

    #@13
    move-object v0, p0

    #@14
    move-object v5, p1

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/view/GLES20Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@18
    .line 1001
    return-void
.end method

.method public drawPatch(Landroid/graphics/Bitmap;[BLandroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 15
    .parameter "bitmap"
    .parameter "chunks"
    .parameter "dst"
    .parameter "paint"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 745
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_f

    #@7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Cannot draw recycled bitmaps"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 747
    :cond_f
    if-eqz p4, :cond_32

    #@11
    invoke-direct {p0, p4}, Landroid/view/GLES20Canvas;->setupColorFilter(Landroid/graphics/Paint;)I

    #@14
    move-result v9

    #@15
    .line 749
    .local v9, modifier:I
    :goto_15
    if-nez p4, :cond_34

    #@17
    move v8, v0

    #@18
    .line 750
    .local v8, nativePaint:I
    :goto_18
    :try_start_18
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1a
    iget v1, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@1c
    iget-object v2, p1, Landroid/graphics/Bitmap;->mBuffer:[B

    #@1e
    iget v4, p3, Landroid/graphics/RectF;->left:F

    #@20
    iget v5, p3, Landroid/graphics/RectF;->top:F

    #@22
    iget v6, p3, Landroid/graphics/RectF;->right:F

    #@24
    iget v7, p3, Landroid/graphics/RectF;->bottom:F

    #@26
    move-object v3, p2

    #@27
    invoke-static/range {v0 .. v8}, Landroid/view/GLES20Canvas;->nDrawPatch(II[B[BFFFFI)V
    :try_end_2a
    .catchall {:try_start_18 .. :try_end_2a} :catchall_37

    #@2a
    .line 753
    if-eqz v9, :cond_31

    #@2c
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2e
    invoke-static {v0, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@31
    .line 755
    :cond_31
    return-void

    #@32
    .end local v8           #nativePaint:I
    .end local v9           #modifier:I
    :cond_32
    move v9, v0

    #@33
    .line 747
    goto :goto_15

    #@34
    .line 749
    .restart local v9       #modifier:I
    :cond_34
    :try_start_34
    iget v8, p4, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_36
    .catchall {:try_start_34 .. :try_end_36} :catchall_37

    #@36
    goto :goto_18

    #@37
    .line 753
    :catchall_37
    move-exception v0

    #@38
    if-eqz v9, :cond_3f

    #@3a
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@3c
    invoke-static {v1, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@3f
    :cond_3f
    throw v0
.end method

.method public drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .registers 7
    .parameter "path"
    .parameter "paint"

    #@0
    .prologue
    .line 1005
    const/4 v1, 0x6

    #@1
    invoke-direct {p0, p2, v1}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@4
    move-result v0

    #@5
    .line 1007
    .local v0, modifiers:I
    :try_start_5
    iget-boolean v1, p1, Landroid/graphics/Path;->isSimplePath:Z

    #@7
    if-eqz v1, :cond_20

    #@9
    .line 1008
    iget-object v1, p1, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@b
    if-eqz v1, :cond_18

    #@d
    .line 1009
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@f
    iget-object v2, p1, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@11
    iget v2, v2, Landroid/graphics/Region;->mNativeRegion:I

    #@13
    iget v3, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@15
    invoke-static {v1, v2, v3}, Landroid/view/GLES20Canvas;->nDrawRects(III)V
    :try_end_18
    .catchall {:try_start_5 .. :try_end_18} :catchall_2a

    #@18
    .line 1015
    :cond_18
    :goto_18
    if-eqz v0, :cond_1f

    #@1a
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1c
    invoke-static {v1, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1f
    .line 1017
    :cond_1f
    return-void

    #@20
    .line 1012
    :cond_20
    :try_start_20
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@22
    iget v2, p1, Landroid/graphics/Path;->mNativePath:I

    #@24
    iget v3, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@26
    invoke-static {v1, v2, v3}, Landroid/view/GLES20Canvas;->nDrawPath(III)V
    :try_end_29
    .catchall {:try_start_20 .. :try_end_29} :catchall_2a

    #@29
    goto :goto_18

    #@2a
    .line 1015
    :catchall_2a
    move-exception v1

    #@2b
    if-eqz v0, :cond_32

    #@2d
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2f
    invoke-static {v2, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@32
    :cond_32
    throw v1
.end method

.method public drawPicture(Landroid/graphics/Picture;)V
    .registers 3
    .parameter "picture"

    #@0
    .prologue
    .line 1024
    iget-boolean v0, p1, Landroid/graphics/Picture;->createdFromStream:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 1030
    :goto_4
    return-void

    #@5
    .line 1028
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Picture;->endRecording()V

    #@8
    goto :goto_4
.end method

.method public drawPicture(Landroid/graphics/Picture;Landroid/graphics/Rect;)V
    .registers 6
    .parameter "picture"
    .parameter "dst"

    #@0
    .prologue
    .line 1034
    iget-boolean v0, p1, Landroid/graphics/Picture;->createdFromStream:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 1045
    :goto_4
    return-void

    #@5
    .line 1038
    :cond_5
    invoke-virtual {p0}, Landroid/view/GLES20Canvas;->save()I

    #@8
    .line 1039
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@a
    int-to-float v0, v0

    #@b
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@d
    int-to-float v1, v1

    #@e
    invoke-virtual {p0, v0, v1}, Landroid/view/GLES20Canvas;->translate(FF)V

    #@11
    .line 1040
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@14
    move-result v0

    #@15
    if-lez v0, :cond_34

    #@17
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@1a
    move-result v0

    #@1b
    if-lez v0, :cond_34

    #@1d
    .line 1041
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    #@20
    move-result v0

    #@21
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@24
    move-result v1

    #@25
    div-int/2addr v0, v1

    #@26
    int-to-float v0, v0

    #@27
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    #@2a
    move-result v1

    #@2b
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@2e
    move-result v2

    #@2f
    div-int/2addr v1, v2

    #@30
    int-to-float v1, v1

    #@31
    invoke-virtual {p0, v0, v1}, Landroid/view/GLES20Canvas;->scale(FF)V

    #@34
    .line 1043
    :cond_34
    invoke-virtual {p0, p1}, Landroid/view/GLES20Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@37
    .line 1044
    invoke-virtual {p0}, Landroid/view/GLES20Canvas;->restore()V

    #@3a
    goto :goto_4
.end method

.method public drawPicture(Landroid/graphics/Picture;Landroid/graphics/RectF;)V
    .registers 6
    .parameter "picture"
    .parameter "dst"

    #@0
    .prologue
    .line 1049
    iget-boolean v0, p1, Landroid/graphics/Picture;->createdFromStream:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 1060
    :goto_4
    return-void

    #@5
    .line 1053
    :cond_5
    invoke-virtual {p0}, Landroid/view/GLES20Canvas;->save()I

    #@8
    .line 1054
    iget v0, p2, Landroid/graphics/RectF;->left:F

    #@a
    iget v1, p2, Landroid/graphics/RectF;->top:F

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/view/GLES20Canvas;->translate(FF)V

    #@f
    .line 1055
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@12
    move-result v0

    #@13
    if-lez v0, :cond_32

    #@15
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@18
    move-result v0

    #@19
    if-lez v0, :cond_32

    #@1b
    .line 1056
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    #@1e
    move-result v0

    #@1f
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@22
    move-result v1

    #@23
    int-to-float v1, v1

    #@24
    div-float/2addr v0, v1

    #@25
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    #@28
    move-result v1

    #@29
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@2c
    move-result v2

    #@2d
    int-to-float v2, v2

    #@2e
    div-float/2addr v1, v2

    #@2f
    invoke-virtual {p0, v0, v1}, Landroid/view/GLES20Canvas;->scale(FF)V

    #@32
    .line 1058
    :cond_32
    invoke-virtual {p0, p1}, Landroid/view/GLES20Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@35
    .line 1059
    invoke-virtual {p0}, Landroid/view/GLES20Canvas;->restore()V

    #@38
    goto :goto_4
.end method

.method public drawPoint(FFLandroid/graphics/Paint;)V
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1064
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mPoint:[F

    #@3
    aput p1, v0, v2

    #@5
    .line 1065
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mPoint:[F

    #@7
    const/4 v1, 0x1

    #@8
    aput p2, v0, v1

    #@a
    .line 1066
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mPoint:[F

    #@c
    const/4 v1, 0x2

    #@d
    invoke-virtual {p0, v0, v2, v1, p3}, Landroid/view/GLES20Canvas;->drawPoints([FIILandroid/graphics/Paint;)V

    #@10
    .line 1067
    return-void
.end method

.method public drawPoints([FIILandroid/graphics/Paint;)V
    .registers 8
    .parameter "pts"
    .parameter "offset"
    .parameter "count"
    .parameter "paint"

    #@0
    .prologue
    .line 1076
    const/4 v1, 0x6

    #@1
    invoke-direct {p0, p4, v1}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@4
    move-result v0

    #@5
    .line 1078
    .local v0, modifiers:I
    :try_start_5
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget v2, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@9
    invoke-static {v1, p1, p2, p3, v2}, Landroid/view/GLES20Canvas;->nDrawPoints(I[FIII)V
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_14

    #@c
    .line 1080
    if-eqz v0, :cond_13

    #@e
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@10
    invoke-static {v1, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@13
    .line 1082
    :cond_13
    return-void

    #@14
    .line 1080
    :catchall_14
    move-exception v1

    #@15
    if-eqz v0, :cond_1c

    #@17
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@19
    invoke-static {v2, v0}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1c
    :cond_1c
    throw v1
.end method

.method public drawPoints([FLandroid/graphics/Paint;)V
    .registers 5
    .parameter "pts"
    .parameter "paint"

    #@0
    .prologue
    .line 1071
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/view/GLES20Canvas;->drawPoints([FIILandroid/graphics/Paint;)V

    #@5
    .line 1072
    return-void
.end method

.method public drawPosText(Ljava/lang/String;[FLandroid/graphics/Paint;)V
    .registers 11
    .parameter "text"
    .parameter "pos"
    .parameter "paint"

    #@0
    .prologue
    .line 1108
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    mul-int/lit8 v0, v0, 0x2

    #@6
    array-length v1, p2

    #@7
    if-le v0, v1, :cond_f

    #@9
    .line 1109
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@b
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@e
    throw v0

    #@f
    .line 1112
    :cond_f
    invoke-direct {p0, p3}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@12
    move-result v6

    #@13
    .line 1114
    .local v6, modifiers:I
    :try_start_13
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@15
    const/4 v2, 0x0

    #@16
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@19
    move-result v3

    #@1a
    iget v5, p3, Landroid/graphics/Paint;->mNativePaint:I

    #@1c
    move-object v1, p1

    #@1d
    move-object v4, p2

    #@1e
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nDrawPosText(ILjava/lang/String;II[FI)V
    :try_end_21
    .catchall {:try_start_13 .. :try_end_21} :catchall_29

    #@21
    .line 1116
    if-eqz v6, :cond_28

    #@23
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@25
    invoke-static {v0, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@28
    .line 1118
    :cond_28
    return-void

    #@29
    .line 1116
    :catchall_29
    move-exception v0

    #@2a
    if-eqz v6, :cond_31

    #@2c
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2e
    invoke-static {v1, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@31
    :cond_31
    throw v0
.end method

.method public drawPosText([CII[FLandroid/graphics/Paint;)V
    .registers 13
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "pos"
    .parameter "paint"

    #@0
    .prologue
    .line 1090
    if-ltz p2, :cond_c

    #@2
    add-int v0, p2, p3

    #@4
    array-length v1, p1

    #@5
    if-gt v0, v1, :cond_c

    #@7
    mul-int/lit8 v0, p3, 0x2

    #@9
    array-length v1, p4

    #@a
    if-le v0, v1, :cond_12

    #@c
    .line 1091
    :cond_c
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@e
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@11
    throw v0

    #@12
    .line 1094
    :cond_12
    invoke-direct {p0, p5}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@15
    move-result v6

    #@16
    .line 1096
    .local v6, modifiers:I
    :try_start_16
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@18
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@1a
    move-object v1, p1

    #@1b
    move v2, p2

    #@1c
    move v3, p3

    #@1d
    move-object v4, p4

    #@1e
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nDrawPosText(I[CII[FI)V
    :try_end_21
    .catchall {:try_start_16 .. :try_end_21} :catchall_29

    #@21
    .line 1098
    if-eqz v6, :cond_28

    #@23
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@25
    invoke-static {v0, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@28
    .line 1100
    :cond_28
    return-void

    #@29
    .line 1098
    :catchall_29
    move-exception v0

    #@2a
    if-eqz v6, :cond_31

    #@2c
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2e
    invoke-static {v1, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@31
    :cond_31
    throw v0
.end method

.method public drawRGB(III)V
    .registers 6
    .parameter "r"
    .parameter "g"
    .parameter "b"

    #@0
    .prologue
    .line 1149
    const/high16 v0, -0x100

    #@2
    and-int/lit16 v1, p1, 0xff

    #@4
    shl-int/lit8 v1, v1, 0x10

    #@6
    or-int/2addr v0, v1

    #@7
    and-int/lit16 v1, p2, 0xff

    #@9
    shl-int/lit8 v1, v1, 0x8

    #@b
    or-int/2addr v0, v1

    #@c
    and-int/lit16 v1, p3, 0xff

    #@e
    or-int/2addr v0, v1

    #@f
    invoke-virtual {p0, v0}, Landroid/view/GLES20Canvas;->drawColor(I)V

    #@12
    .line 1150
    return-void
.end method

.method public drawRect(FFFFLandroid/graphics/Paint;)V
    .registers 13
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "paint"

    #@0
    .prologue
    .line 1125
    cmpl-float v0, p1, p3

    #@2
    if-eqz v0, :cond_8

    #@4
    cmpl-float v0, p2, p4

    #@6
    if-nez v0, :cond_9

    #@8
    .line 1132
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1126
    :cond_9
    const/4 v0, 0x6

    #@a
    invoke-direct {p0, p5, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@d
    move-result v6

    #@e
    .line 1128
    .local v6, modifiers:I
    :try_start_e
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@10
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@12
    move v1, p1

    #@13
    move v2, p2

    #@14
    move v3, p3

    #@15
    move v4, p4

    #@16
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nDrawRect(IFFFFI)V
    :try_end_19
    .catchall {:try_start_e .. :try_end_19} :catchall_21

    #@19
    .line 1130
    if-eqz v6, :cond_8

    #@1b
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1d
    invoke-static {v0, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@20
    goto :goto_8

    #@21
    :catchall_21
    move-exception v0

    #@22
    if-eqz v6, :cond_29

    #@24
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@26
    invoke-static {v1, v6}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@29
    :cond_29
    throw v0
.end method

.method public drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .registers 9
    .parameter "r"
    .parameter "paint"

    #@0
    .prologue
    .line 1139
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    int-to-float v1, v0

    #@3
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@5
    int-to-float v2, v0

    #@6
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@8
    int-to-float v3, v0

    #@9
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@b
    int-to-float v4, v0

    #@c
    move-object v0, p0

    #@d
    move-object v5, p2

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/view/GLES20Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@11
    .line 1140
    return-void
.end method

.method public drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 9
    .parameter "r"
    .parameter "paint"

    #@0
    .prologue
    .line 1144
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@2
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@4
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@6
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@8
    move-object v0, p0

    #@9
    move-object v5, p2

    #@a
    invoke-virtual/range {v0 .. v5}, Landroid/view/GLES20Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@d
    .line 1145
    return-void
.end method

.method public drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
    .registers 14
    .parameter "rect"
    .parameter "rx"
    .parameter "ry"
    .parameter "paint"

    #@0
    .prologue
    .line 1154
    const/4 v0, 0x6

    #@1
    invoke-direct {p0, p4, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;I)I

    #@4
    move-result v8

    #@5
    .line 1156
    .local v8, modifiers:I
    :try_start_5
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@7
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@9
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@b
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@d
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@f
    iget v7, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@11
    move v5, p2

    #@12
    move v6, p3

    #@13
    invoke-static/range {v0 .. v7}, Landroid/view/GLES20Canvas;->nDrawRoundRect(IFFFFFFI)V
    :try_end_16
    .catchall {:try_start_5 .. :try_end_16} :catchall_1e

    #@16
    .line 1159
    if-eqz v8, :cond_1d

    #@18
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1a
    invoke-static {v0, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1d
    .line 1161
    :cond_1d
    return-void

    #@1e
    .line 1159
    :catchall_1e
    move-exception v0

    #@1f
    if-eqz v8, :cond_26

    #@21
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@23
    invoke-static {v1, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@26
    :cond_26
    throw v0
.end method

.method public drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V
    .registers 17
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1185
    move-object/from16 v0, p6

    #@2
    invoke-direct {p0, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@5
    move-result v9

    #@6
    .line 1187
    .local v9, modifiers:I
    :try_start_6
    instance-of v1, p1, Ljava/lang/String;

    #@8
    if-nez v1, :cond_12

    #@a
    instance-of v1, p1, Landroid/text/SpannedString;

    #@c
    if-nez v1, :cond_12

    #@e
    instance-of v1, p1, Landroid/text/SpannableString;

    #@10
    if-eqz v1, :cond_2f

    #@12
    .line 1189
    :cond_12
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@14
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    move-object/from16 v0, p6

    #@1a
    iget v7, v0, Landroid/graphics/Paint;->mBidiFlags:I

    #@1c
    move-object/from16 v0, p6

    #@1e
    iget v8, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@20
    move v3, p2

    #@21
    move v4, p3

    #@22
    move v5, p4

    #@23
    move v6, p5

    #@24
    invoke-static/range {v1 .. v8}, Landroid/view/GLES20Canvas;->nDrawText(ILjava/lang/String;IIFFII)V
    :try_end_27
    .catchall {:try_start_6 .. :try_end_27} :catchall_42

    #@27
    .line 1202
    :goto_27
    if-eqz v9, :cond_2e

    #@29
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2b
    invoke-static {v1, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2e
    .line 1204
    :cond_2e
    return-void

    #@2f
    .line 1191
    :cond_2f
    :try_start_2f
    instance-of v1, p1, Landroid/text/GraphicsOperations;

    #@31
    if-eqz v1, :cond_4b

    #@33
    .line 1192
    move-object v0, p1

    #@34
    check-cast v0, Landroid/text/GraphicsOperations;

    #@36
    move-object v1, v0

    #@37
    move-object v2, p0

    #@38
    move v3, p2

    #@39
    move v4, p3

    #@3a
    move v5, p4

    #@3b
    move v6, p5

    #@3c
    move-object/from16 v7, p6

    #@3e
    invoke-interface/range {v1 .. v7}, Landroid/text/GraphicsOperations;->drawText(Landroid/graphics/Canvas;IIFFLandroid/graphics/Paint;)V
    :try_end_41
    .catchall {:try_start_2f .. :try_end_41} :catchall_42

    #@41
    goto :goto_27

    #@42
    .line 1202
    :catchall_42
    move-exception v1

    #@43
    if-eqz v9, :cond_4a

    #@45
    iget v3, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@47
    invoke-static {v3, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@4a
    :cond_4a
    throw v1

    #@4b
    .line 1195
    :cond_4b
    sub-int v1, p3, p2

    #@4d
    :try_start_4d
    invoke-static {v1}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@50
    move-result-object v2

    #@51
    .line 1196
    .local v2, buf:[C
    const/4 v1, 0x0

    #@52
    invoke-static {p1, p2, p3, v2, v1}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@55
    .line 1197
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@57
    const/4 v3, 0x0

    #@58
    sub-int v4, p3, p2

    #@5a
    move-object/from16 v0, p6

    #@5c
    iget v7, v0, Landroid/graphics/Paint;->mBidiFlags:I

    #@5e
    move-object/from16 v0, p6

    #@60
    iget v8, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@62
    move v5, p4

    #@63
    move v6, p5

    #@64
    invoke-static/range {v1 .. v8}, Landroid/view/GLES20Canvas;->nDrawText(I[CIIFFII)V

    #@67
    .line 1199
    invoke-static {v2}, Landroid/graphics/TemporaryBuffer;->recycle([C)V
    :try_end_6a
    .catchall {:try_start_4d .. :try_end_6a} :catchall_42

    #@6a
    goto :goto_27
.end method

.method public drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    .registers 14
    .parameter "text"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1225
    invoke-direct {p0, p4}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@3
    move-result v8

    #@4
    .line 1227
    .local v8, modifiers:I
    :try_start_4
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@a
    move-result v3

    #@b
    iget v6, p4, Landroid/graphics/Paint;->mBidiFlags:I

    #@d
    iget v7, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@f
    move-object v1, p1

    #@10
    move v4, p2

    #@11
    move v5, p3

    #@12
    invoke-static/range {v0 .. v7}, Landroid/view/GLES20Canvas;->nDrawText(ILjava/lang/String;IIFFII)V
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_1d

    #@15
    .line 1230
    if-eqz v8, :cond_1c

    #@17
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@19
    invoke-static {v0, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@1c
    .line 1232
    :cond_1c
    return-void

    #@1d
    .line 1230
    :catchall_1d
    move-exception v0

    #@1e
    if-eqz v8, :cond_25

    #@20
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@22
    invoke-static {v1, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@25
    :cond_25
    throw v0
.end method

.method public drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V
    .registers 16
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1208
    or-int v0, p2, p3

    #@2
    sub-int v1, p3, p2

    #@4
    or-int/2addr v0, v1

    #@5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8
    move-result v1

    #@9
    sub-int/2addr v1, p3

    #@a
    or-int/2addr v0, v1

    #@b
    if-gez v0, :cond_13

    #@d
    .line 1209
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@f
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@12
    throw v0

    #@13
    .line 1212
    :cond_13
    invoke-direct {p0, p6}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@16
    move-result v8

    #@17
    .line 1214
    .local v8, modifiers:I
    :try_start_17
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@19
    iget v6, p6, Landroid/graphics/Paint;->mBidiFlags:I

    #@1b
    iget v7, p6, Landroid/graphics/Paint;->mNativePaint:I

    #@1d
    move-object v1, p1

    #@1e
    move v2, p2

    #@1f
    move v3, p3

    #@20
    move v4, p4

    #@21
    move v5, p5

    #@22
    invoke-static/range {v0 .. v7}, Landroid/view/GLES20Canvas;->nDrawText(ILjava/lang/String;IIFFII)V
    :try_end_25
    .catchall {:try_start_17 .. :try_end_25} :catchall_2d

    #@25
    .line 1216
    if-eqz v8, :cond_2c

    #@27
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@29
    invoke-static {v0, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2c
    .line 1218
    :cond_2c
    return-void

    #@2d
    .line 1216
    :catchall_2d
    move-exception v0

    #@2e
    if-eqz v8, :cond_35

    #@30
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@32
    invoke-static {v1, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@35
    :cond_35
    throw v0
.end method

.method public drawText([CIIFFLandroid/graphics/Paint;)V
    .registers 16
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1168
    or-int v0, p2, p3

    #@2
    add-int v1, p2, p3

    #@4
    or-int/2addr v0, v1

    #@5
    array-length v1, p1

    #@6
    sub-int/2addr v1, p2

    #@7
    sub-int/2addr v1, p3

    #@8
    or-int/2addr v0, v1

    #@9
    if-gez v0, :cond_11

    #@b
    .line 1169
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@d
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@10
    throw v0

    #@11
    .line 1172
    :cond_11
    invoke-direct {p0, p6}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@14
    move-result v8

    #@15
    .line 1174
    .local v8, modifiers:I
    :try_start_15
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@17
    iget v6, p6, Landroid/graphics/Paint;->mBidiFlags:I

    #@19
    iget v7, p6, Landroid/graphics/Paint;->mNativePaint:I

    #@1b
    move-object v1, p1

    #@1c
    move v2, p2

    #@1d
    move v3, p3

    #@1e
    move v4, p4

    #@1f
    move v5, p5

    #@20
    invoke-static/range {v0 .. v7}, Landroid/view/GLES20Canvas;->nDrawText(I[CIIFFII)V
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_2b

    #@23
    .line 1176
    if-eqz v8, :cond_2a

    #@25
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@27
    invoke-static {v0, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2a
    .line 1178
    :cond_2a
    return-void

    #@2b
    .line 1176
    :catchall_2b
    move-exception v0

    #@2c
    if-eqz v8, :cond_33

    #@2e
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@30
    invoke-static {v1, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@33
    :cond_33
    throw v0
.end method

.method public drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V
    .registers 16
    .parameter "text"
    .parameter "path"
    .parameter "hOffset"
    .parameter "vOffset"
    .parameter "paint"

    #@0
    .prologue
    .line 1255
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1264
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1257
    :cond_7
    invoke-direct {p0, p5}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@a
    move-result v9

    #@b
    .line 1259
    .local v9, modifiers:I
    :try_start_b
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@11
    move-result v3

    #@12
    iget v4, p2, Landroid/graphics/Path;->mNativePath:I

    #@14
    iget v7, p5, Landroid/graphics/Paint;->mBidiFlags:I

    #@16
    iget v8, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@18
    move-object v1, p1

    #@19
    move v5, p3

    #@1a
    move v6, p4

    #@1b
    invoke-static/range {v0 .. v8}, Landroid/view/GLES20Canvas;->nDrawTextOnPath(ILjava/lang/String;IIIFFII)V
    :try_end_1e
    .catchall {:try_start_b .. :try_end_1e} :catchall_26

    #@1e
    .line 1262
    if-eqz v9, :cond_6

    #@20
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@22
    invoke-static {v0, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@25
    goto :goto_6

    #@26
    :catchall_26
    move-exception v0

    #@27
    if-eqz v9, :cond_2e

    #@29
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2b
    invoke-static {v1, v9}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@2e
    :cond_2e
    throw v0
.end method

.method public drawTextOnPath([CIILandroid/graphics/Path;FFLandroid/graphics/Paint;)V
    .registers 19
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "path"
    .parameter "hOffset"
    .parameter "vOffset"
    .parameter "paint"

    #@0
    .prologue
    .line 1237
    if-ltz p2, :cond_7

    #@2
    add-int v1, p2, p3

    #@4
    array-length v2, p1

    #@5
    if-le v1, v2, :cond_d

    #@7
    .line 1238
    :cond_7
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@9
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@c
    throw v1

    #@d
    .line 1241
    :cond_d
    move-object/from16 v0, p7

    #@f
    invoke-direct {p0, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@12
    move-result v10

    #@13
    .line 1243
    .local v10, modifiers:I
    :try_start_13
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@15
    iget v5, p4, Landroid/graphics/Path;->mNativePath:I

    #@17
    move-object/from16 v0, p7

    #@19
    iget v8, v0, Landroid/graphics/Paint;->mBidiFlags:I

    #@1b
    move-object/from16 v0, p7

    #@1d
    iget v9, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@1f
    move-object v2, p1

    #@20
    move v3, p2

    #@21
    move v4, p3

    #@22
    move/from16 v6, p5

    #@24
    move/from16 v7, p6

    #@26
    invoke-static/range {v1 .. v9}, Landroid/view/GLES20Canvas;->nDrawTextOnPath(I[CIIIFFII)V
    :try_end_29
    .catchall {:try_start_13 .. :try_end_29} :catchall_31

    #@29
    .line 1246
    if-eqz v10, :cond_30

    #@2b
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2d
    invoke-static {v1, v10}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@30
    .line 1248
    :cond_30
    return-void

    #@31
    .line 1246
    :catchall_31
    move-exception v1

    #@32
    if-eqz v10, :cond_39

    #@34
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@36
    invoke-static {v2, v10}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@39
    :cond_39
    throw v1
.end method

.method public drawTextRun(Ljava/lang/CharSequence;IIIIFFILandroid/graphics/Paint;)V
    .registers 23
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "x"
    .parameter "y"
    .parameter "dir"
    .parameter "paint"

    #@0
    .prologue
    .line 1294
    or-int v2, p2, p3

    #@2
    sub-int v4, p3, p2

    #@4
    or-int/2addr v2, v4

    #@5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@8
    move-result v4

    #@9
    sub-int v4, v4, p3

    #@b
    or-int/2addr v2, v4

    #@c
    if-gez v2, :cond_14

    #@e
    .line 1295
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@10
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@13
    throw v2

    #@14
    .line 1298
    :cond_14
    move-object/from16 v0, p9

    #@16
    invoke-direct {p0, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@19
    move-result v12

    #@1a
    .line 1300
    .local v12, modifiers:I
    if-nez p8, :cond_49

    #@1c
    const/4 v10, 0x0

    #@1d
    .line 1301
    .local v10, flags:I
    :goto_1d
    :try_start_1d
    instance-of v2, p1, Ljava/lang/String;

    #@1f
    if-nez v2, :cond_29

    #@21
    instance-of v2, p1, Landroid/text/SpannedString;

    #@23
    if-nez v2, :cond_29

    #@25
    instance-of v2, p1, Landroid/text/SpannableString;

    #@27
    if-eqz v2, :cond_4b

    #@29
    .line 1303
    :cond_29
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2b
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    move-object/from16 v0, p9

    #@31
    iget v11, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@33
    move v4, p2

    #@34
    move/from16 v5, p3

    #@36
    move/from16 v6, p4

    #@38
    move/from16 v7, p5

    #@3a
    move/from16 v8, p6

    #@3c
    move/from16 v9, p7

    #@3e
    invoke-static/range {v2 .. v11}, Landroid/view/GLES20Canvas;->nDrawTextRun(ILjava/lang/String;IIIIFFII)V
    :try_end_41
    .catchall {:try_start_1d .. :try_end_41} :catchall_65

    #@41
    .line 1318
    :goto_41
    if-eqz v12, :cond_48

    #@43
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@45
    invoke-static {v2, v12}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@48
    .line 1320
    :cond_48
    return-void

    #@49
    .line 1300
    .end local v10           #flags:I
    :cond_49
    const/4 v10, 0x1

    #@4a
    goto :goto_1d

    #@4b
    .line 1305
    .restart local v10       #flags:I
    :cond_4b
    :try_start_4b
    instance-of v2, p1, Landroid/text/GraphicsOperations;

    #@4d
    if-eqz v2, :cond_6e

    #@4f
    .line 1306
    move-object v0, p1

    #@50
    check-cast v0, Landroid/text/GraphicsOperations;

    #@52
    move-object v2, v0

    #@53
    move-object v3, p0

    #@54
    move v4, p2

    #@55
    move/from16 v5, p3

    #@57
    move/from16 v6, p4

    #@59
    move/from16 v7, p5

    #@5b
    move/from16 v8, p6

    #@5d
    move/from16 v9, p7

    #@5f
    move-object/from16 v11, p9

    #@61
    invoke-interface/range {v2 .. v11}, Landroid/text/GraphicsOperations;->drawTextRun(Landroid/graphics/Canvas;IIIIFFILandroid/graphics/Paint;)V
    :try_end_64
    .catchall {:try_start_4b .. :try_end_64} :catchall_65

    #@64
    goto :goto_41

    #@65
    .line 1318
    :catchall_65
    move-exception v2

    #@66
    if-eqz v12, :cond_6d

    #@68
    iget v4, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@6a
    invoke-static {v4, v12}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@6d
    :cond_6d
    throw v2

    #@6e
    .line 1309
    :cond_6e
    sub-int v7, p5, p4

    #@70
    .line 1310
    .local v7, contextLen:I
    sub-int v5, p3, p2

    #@72
    .line 1311
    .local v5, len:I
    :try_start_72
    invoke-static {v7}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@75
    move-result-object v3

    #@76
    .line 1312
    .local v3, buf:[C
    const/4 v2, 0x0

    #@77
    move/from16 v0, p4

    #@79
    move/from16 v1, p5

    #@7b
    invoke-static {p1, v0, v1, v3, v2}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@7e
    .line 1313
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@80
    sub-int v4, p2, p4

    #@82
    const/4 v6, 0x0

    #@83
    move-object/from16 v0, p9

    #@85
    iget v11, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@87
    move/from16 v8, p6

    #@89
    move/from16 v9, p7

    #@8b
    invoke-static/range {v2 .. v11}, Landroid/view/GLES20Canvas;->nDrawTextRun(I[CIIIIFFII)V

    #@8e
    .line 1315
    invoke-static {v3}, Landroid/graphics/TemporaryBuffer;->recycle([C)V
    :try_end_91
    .catchall {:try_start_72 .. :try_end_91} :catchall_65

    #@91
    goto :goto_41
.end method

.method public drawTextRun([CIIIIFFILandroid/graphics/Paint;)V
    .registers 22
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "contextIndex"
    .parameter "contextCount"
    .parameter "x"
    .parameter "y"
    .parameter "dir"
    .parameter "paint"

    #@0
    .prologue
    .line 1272
    or-int v1, p2, p3

    #@2
    array-length v2, p1

    #@3
    sub-int/2addr v2, p2

    #@4
    sub-int/2addr v2, p3

    #@5
    or-int/2addr v1, v2

    #@6
    if-gez v1, :cond_e

    #@8
    .line 1273
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@a
    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@d
    throw v1

    #@e
    .line 1275
    :cond_e
    if-eqz p8, :cond_30

    #@10
    const/4 v1, 0x1

    #@11
    move/from16 v0, p8

    #@13
    if-eq v0, v1, :cond_30

    #@15
    .line 1276
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "Unknown direction: "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    move/from16 v0, p8

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v1

    #@30
    .line 1279
    :cond_30
    move-object/from16 v0, p9

    #@32
    invoke-direct {p0, v0}, Landroid/view/GLES20Canvas;->setupModifiers(Landroid/graphics/Paint;)I

    #@35
    move-result v11

    #@36
    .line 1281
    .local v11, modifiers:I
    :try_start_36
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@38
    move-object/from16 v0, p9

    #@3a
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@3c
    move-object v2, p1

    #@3d
    move v3, p2

    #@3e
    move v4, p3

    #@3f
    move/from16 v5, p4

    #@41
    move/from16 v6, p5

    #@43
    move/from16 v7, p6

    #@45
    move/from16 v8, p7

    #@47
    move/from16 v9, p8

    #@49
    invoke-static/range {v1 .. v10}, Landroid/view/GLES20Canvas;->nDrawTextRun(I[CIIIIFFII)V
    :try_end_4c
    .catchall {:try_start_36 .. :try_end_4c} :catchall_54

    #@4c
    .line 1284
    if-eqz v11, :cond_53

    #@4e
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@50
    invoke-static {v1, v11}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@53
    .line 1286
    :cond_53
    return-void

    #@54
    .line 1284
    :catchall_54
    move-exception v1

    #@55
    if-eqz v11, :cond_5c

    #@57
    iget v2, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@59
    invoke-static {v2, v11}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@5c
    :cond_5c
    throw v1
.end method

.method public drawVertices(Landroid/graphics/Canvas$VertexMode;I[FI[FI[II[SIILandroid/graphics/Paint;)V
    .registers 13
    .parameter "mode"
    .parameter "vertexCount"
    .parameter "verts"
    .parameter "vertOffset"
    .parameter "texs"
    .parameter "texOffset"
    .parameter "colors"
    .parameter "colorOffset"
    .parameter "indices"
    .parameter "indexOffset"
    .parameter "indexCount"
    .parameter "paint"

    #@0
    .prologue
    .line 1330
    return-void
.end method

.method public getClipBounds(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "bounds"

    #@0
    .prologue
    .line 517
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nGetClipBounds(ILandroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getDisplayList(I)I
    .registers 3
    .parameter "displayList"

    #@0
    .prologue
    .line 374
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nGetDisplayList(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDrawFilter()Landroid/graphics/DrawFilter;
    .registers 2

    #@0
    .prologue
    .line 715
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mFilter:Landroid/graphics/DrawFilter;

    #@2
    return-object v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 196
    iget v0, p0, Landroid/view/GLES20Canvas;->mHeight:I

    #@2
    return v0
.end method

.method public getMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 584
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nGetMatrix(II)V

    #@7
    .line 585
    return-void
.end method

.method public getMaximumBitmapHeight()I
    .registers 2

    #@0
    .prologue
    .line 206
    invoke-static {}, Landroid/view/GLES20Canvas;->nGetMaximumTextureHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getMaximumBitmapWidth()I
    .registers 2

    #@0
    .prologue
    .line 201
    invoke-static {}, Landroid/view/GLES20Canvas;->nGetMaximumTextureWidth()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method getRenderer()I
    .registers 2

    #@0
    .prologue
    .line 216
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    return v0
.end method

.method public getSaveCount()I
    .registers 2

    #@0
    .prologue
    .line 690
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nGetSaveCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/view/GLES20Canvas;->mWidth:I

    #@2
    return v0
.end method

.method interrupt()V
    .registers 2

    #@0
    .prologue
    .line 426
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nInterrupt(I)V

    #@5
    .line 427
    return-void
.end method

.method public invokeFunctors(Landroid/graphics/Rect;)I
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 281
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nInvokeFunctors(ILandroid/graphics/Rect;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isOpaque()Z
    .registers 2

    #@0
    .prologue
    .line 186
    iget-boolean v0, p0, Landroid/view/GLES20Canvas;->mOpaque:Z

    #@2
    return v0
.end method

.method public onPostDraw()V
    .registers 2

    #@0
    .prologue
    .line 249
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nFinish(I)V

    #@5
    .line 250
    return-void
.end method

.method public onPreDraw(Landroid/graphics/Rect;)I
    .registers 8
    .parameter "dirty"

    #@0
    .prologue
    .line 235
    if-eqz p1, :cond_13

    #@2
    .line 236
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@4
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@6
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@8
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@a
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@c
    iget-boolean v5, p0, Landroid/view/GLES20Canvas;->mOpaque:Z

    #@e
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nPrepareDirty(IIIIIZ)I

    #@11
    move-result v0

    #@12
    .line 239
    :goto_12
    return v0

    #@13
    :cond_13
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@15
    iget-boolean v1, p0, Landroid/view/GLES20Canvas;->mOpaque:Z

    #@17
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nPrepare(IZ)I

    #@1a
    move-result v0

    #@1b
    goto :goto_12
.end method

.method outputDisplayList(Landroid/view/DisplayList;)V
    .registers 4
    .parameter "displayList"

    #@0
    .prologue
    .line 408
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    check-cast p1, Landroid/view/GLES20DisplayList;

    #@4
    .end local p1
    invoke-virtual {p1}, Landroid/view/GLES20DisplayList;->getNativeDisplayList()I

    #@7
    move-result v1

    #@8
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nOutputDisplayList(II)V

    #@b
    .line 409
    return-void
.end method

.method pushLayerUpdate(Landroid/view/HardwareLayer;)V
    .registers 4
    .parameter "layer"

    #@0
    .prologue
    .line 153
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    check-cast p1, Landroid/view/GLES20RenderLayer;

    #@4
    .end local p1
    iget v1, p1, Landroid/view/GLES20Layer;->mLayer:I

    #@6
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nPushLayerUpdate(II)V

    #@9
    .line 154
    return-void
.end method

.method public quickReject(FFFFLandroid/graphics/Canvas$EdgeType;)Z
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "type"

    #@0
    .prologue
    .line 524
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v5, p5, Landroid/graphics/Canvas$EdgeType;->nativeInt:I

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v3, p3

    #@7
    move v4, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nQuickReject(IFFFFI)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public quickReject(Landroid/graphics/Path;Landroid/graphics/Canvas$EdgeType;)Z
    .registers 9
    .parameter "path"
    .parameter "type"

    #@0
    .prologue
    .line 532
    iget-object v0, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    #@6
    .line 533
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@8
    iget-object v1, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@a
    iget v1, v1, Landroid/graphics/RectF;->left:F

    #@c
    iget-object v2, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@e
    iget v2, v2, Landroid/graphics/RectF;->top:F

    #@10
    iget-object v3, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@12
    iget v3, v3, Landroid/graphics/RectF;->right:F

    #@14
    iget-object v4, p0, Landroid/view/GLES20Canvas;->mPathBounds:Landroid/graphics/RectF;

    #@16
    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    #@18
    iget v5, p2, Landroid/graphics/Canvas$EdgeType;->nativeInt:I

    #@1a
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nQuickReject(IFFFFI)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public quickReject(Landroid/graphics/RectF;Landroid/graphics/Canvas$EdgeType;)Z
    .registers 9
    .parameter "rect"
    .parameter "type"

    #@0
    .prologue
    .line 539
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@8
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@a
    iget v5, p2, Landroid/graphics/Canvas$EdgeType;->nativeInt:I

    #@c
    invoke-static/range {v0 .. v5}, Landroid/view/GLES20Canvas;->nQuickReject(IFFFFI)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method protected resetDisplayListRenderer()V
    .registers 2

    #@0
    .prologue
    .line 121
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nResetDisplayListRenderer(I)V

    #@5
    .line 122
    return-void
.end method

.method public restore()V
    .registers 2

    #@0
    .prologue
    .line 676
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nRestore(I)V

    #@5
    .line 677
    return-void
.end method

.method public restoreToCount(I)V
    .registers 3
    .parameter "saveCount"

    #@0
    .prologue
    .line 683
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nRestoreToCount(II)V

    #@5
    .line 684
    return-void
.end method

.method resume()V
    .registers 2

    #@0
    .prologue
    .line 430
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nResume(I)V

    #@5
    .line 431
    return-void
.end method

.method public rotate(F)V
    .registers 3
    .parameter "degrees"

    #@0
    .prologue
    .line 562
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nRotate(IF)V

    #@5
    .line 563
    return-void
.end method

.method public save()I
    .registers 3

    #@0
    .prologue
    .line 602
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nSave(II)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public save(I)I
    .registers 3
    .parameter "saveFlags"

    #@0
    .prologue
    .line 607
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nSave(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public saveLayer(FFFFLandroid/graphics/Paint;I)I
    .registers 16
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "paint"
    .parameter "saveFlags"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 634
    cmpg-float v1, p1, p3

    #@3
    if-gez v1, :cond_33

    #@5
    cmpg-float v1, p2, p4

    #@7
    if-gez v1, :cond_33

    #@9
    .line 636
    if-eqz p5, :cond_25

    #@b
    invoke-direct {p0, p5}, Landroid/view/GLES20Canvas;->setupColorFilter(Landroid/graphics/Paint;)I

    #@e
    move-result v8

    #@f
    .line 638
    .local v8, modifier:I
    :goto_f
    if-nez p5, :cond_27

    #@11
    move v5, v0

    #@12
    .line 639
    .local v5, nativePaint:I
    :goto_12
    :try_start_12
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@14
    move v1, p1

    #@15
    move v2, p2

    #@16
    move v3, p3

    #@17
    move v4, p4

    #@18
    move v6, p6

    #@19
    invoke-static/range {v0 .. v6}, Landroid/view/GLES20Canvas;->nSaveLayer(IFFFFII)I
    :try_end_1c
    .catchall {:try_start_12 .. :try_end_1c} :catchall_2a

    #@1c
    move-result v7

    #@1d
    .line 641
    .local v7, count:I
    if-eqz v8, :cond_24

    #@1f
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@21
    invoke-static {v0, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@24
    .line 645
    .end local v5           #nativePaint:I
    .end local v7           #count:I
    .end local v8           #modifier:I
    :cond_24
    :goto_24
    return v7

    #@25
    :cond_25
    move v8, v0

    #@26
    .line 636
    goto :goto_f

    #@27
    .line 638
    .restart local v8       #modifier:I
    :cond_27
    :try_start_27
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_29
    .catchall {:try_start_27 .. :try_end_29} :catchall_2a

    #@29
    goto :goto_12

    #@2a
    .line 641
    :catchall_2a
    move-exception v0

    #@2b
    if-eqz v8, :cond_32

    #@2d
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2f
    invoke-static {v1, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@32
    :cond_32
    throw v0

    #@33
    .line 645
    .end local v8           #modifier:I
    :cond_33
    invoke-virtual {p0, p6}, Landroid/view/GLES20Canvas;->save(I)I

    #@36
    move-result v7

    #@37
    goto :goto_24
.end method

.method public saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;I)I
    .registers 14
    .parameter "bounds"
    .parameter "paint"
    .parameter "saveFlags"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 614
    if-eqz p1, :cond_13

    #@3
    .line 615
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@5
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@7
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@9
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@b
    move-object v0, p0

    #@c
    move-object v5, p2

    #@d
    move v6, p3

    #@e
    invoke-virtual/range {v0 .. v6}, Landroid/view/GLES20Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@11
    move-result v7

    #@12
    .line 626
    :cond_12
    :goto_12
    return v7

    #@13
    .line 619
    :cond_13
    if-eqz p2, :cond_2a

    #@15
    invoke-direct {p0, p2}, Landroid/view/GLES20Canvas;->setupColorFilter(Landroid/graphics/Paint;)I

    #@18
    move-result v8

    #@19
    .line 621
    .local v8, modifier:I
    :goto_19
    if-nez p2, :cond_2c

    #@1b
    move v9, v0

    #@1c
    .line 622
    .local v9, nativePaint:I
    :goto_1c
    :try_start_1c
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@1e
    invoke-static {v0, v9, p3}, Landroid/view/GLES20Canvas;->nSaveLayer(III)I
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_2f

    #@21
    move-result v7

    #@22
    .line 624
    .local v7, count:I
    if-eqz v8, :cond_12

    #@24
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@26
    invoke-static {v0, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@29
    goto :goto_12

    #@2a
    .end local v7           #count:I
    .end local v8           #modifier:I
    .end local v9           #nativePaint:I
    :cond_2a
    move v8, v0

    #@2b
    .line 619
    goto :goto_19

    #@2c
    .line 621
    .restart local v8       #modifier:I
    :cond_2c
    :try_start_2c
    iget v9, p2, Landroid/graphics/Paint;->mNativePaint:I
    :try_end_2e
    .catchall {:try_start_2c .. :try_end_2e} :catchall_2f

    #@2e
    goto :goto_1c

    #@2f
    .line 624
    :catchall_2f
    move-exception v0

    #@30
    if-eqz v8, :cond_37

    #@32
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@34
    invoke-static {v1, v8}, Landroid/view/GLES20Canvas;->nResetModifiers(II)V

    #@37
    :cond_37
    throw v0
.end method

.method public saveLayerAlpha(FFFFII)I
    .registers 14
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "alpha"
    .parameter "saveFlags"

    #@0
    .prologue
    .line 665
    cmpg-float v0, p1, p3

    #@2
    if-gez v0, :cond_15

    #@4
    cmpg-float v0, p2, p4

    #@6
    if-gez v0, :cond_15

    #@8
    .line 666
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@a
    move v1, p1

    #@b
    move v2, p2

    #@c
    move v3, p3

    #@d
    move v4, p4

    #@e
    move v5, p5

    #@f
    move v6, p6

    #@10
    invoke-static/range {v0 .. v6}, Landroid/view/GLES20Canvas;->nSaveLayerAlpha(IFFFFII)I

    #@13
    move-result v0

    #@14
    .line 668
    :goto_14
    return v0

    #@15
    :cond_15
    invoke-virtual {p0, p6}, Landroid/view/GLES20Canvas;->save(I)I

    #@18
    move-result v0

    #@19
    goto :goto_14
.end method

.method public saveLayerAlpha(Landroid/graphics/RectF;II)I
    .registers 11
    .parameter "bounds"
    .parameter "alpha"
    .parameter "saveFlags"

    #@0
    .prologue
    .line 653
    if-eqz p1, :cond_12

    #@2
    .line 654
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@8
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@a
    move-object v0, p0

    #@b
    move v5, p2

    #@c
    move v6, p3

    #@d
    invoke-virtual/range {v0 .. v6}, Landroid/view/GLES20Canvas;->saveLayerAlpha(FFFFII)I

    #@10
    move-result v0

    #@11
    .line 657
    :goto_11
    return v0

    #@12
    :cond_12
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@14
    invoke-static {v0, p2, p3}, Landroid/view/GLES20Canvas;->nSaveLayerAlpha(III)I

    #@17
    move-result v0

    #@18
    goto :goto_11
.end method

.method public scale(FF)V
    .registers 4
    .parameter "sx"
    .parameter "sy"

    #@0
    .prologue
    .line 569
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/view/GLES20Canvas;->nScale(IFF)V

    #@5
    .line 570
    return-void
.end method

.method public setDrawFilter(Landroid/graphics/DrawFilter;)V
    .registers 6
    .parameter "filter"

    #@0
    .prologue
    .line 701
    iput-object p1, p0, Landroid/view/GLES20Canvas;->mFilter:Landroid/graphics/DrawFilter;

    #@2
    .line 702
    if-nez p1, :cond_a

    #@4
    .line 703
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@6
    invoke-static {v1}, Landroid/view/GLES20Canvas;->nResetPaintFilter(I)V

    #@9
    .line 708
    :cond_9
    :goto_9
    return-void

    #@a
    .line 704
    :cond_a
    instance-of v1, p1, Landroid/graphics/PaintFlagsDrawFilter;

    #@c
    if-eqz v1, :cond_9

    #@e
    move-object v0, p1

    #@f
    .line 705
    check-cast v0, Landroid/graphics/PaintFlagsDrawFilter;

    #@11
    .line 706
    .local v0, flagsFilter:Landroid/graphics/PaintFlagsDrawFilter;
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@13
    iget v2, v0, Landroid/graphics/PaintFlagsDrawFilter;->clearBits:I

    #@15
    iget v3, v0, Landroid/graphics/PaintFlagsDrawFilter;->setBits:I

    #@17
    invoke-static {v1, v2, v3}, Landroid/view/GLES20Canvas;->nSetupPaintFilter(III)V

    #@1a
    goto :goto_9
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 576
    iget v1, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    if-nez p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-static {v1, v0}, Landroid/view/GLES20Canvas;->nSetMatrix(II)V

    #@8
    .line 577
    return-void

    #@9
    .line 576
    :cond_9
    iget v0, p1, Landroid/graphics/Matrix;->native_instance:I

    #@b
    goto :goto_5
.end method

.method public setViewport(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 225
    iput p1, p0, Landroid/view/GLES20Canvas;->mWidth:I

    #@2
    .line 226
    iput p2, p0, Landroid/view/GLES20Canvas;->mHeight:I

    #@4
    .line 228
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@6
    invoke-static {v0, p1, p2}, Landroid/view/GLES20Canvas;->nSetViewport(III)V

    #@9
    .line 229
    return-void
.end method

.method public skew(FF)V
    .registers 4
    .parameter "sx"
    .parameter "sy"

    #@0
    .prologue
    .line 555
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/view/GLES20Canvas;->nSkew(IFF)V

    #@5
    .line 556
    return-void
.end method

.method public translate(FF)V
    .registers 5
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 548
    cmpl-float v0, p1, v1

    #@3
    if-nez v0, :cond_9

    #@5
    cmpl-float v0, p2, v1

    #@7
    if-eqz v0, :cond_e

    #@9
    :cond_9
    iget v0, p0, Landroid/view/GLES20Canvas;->mRenderer:I

    #@b
    invoke-static {v0, p1, p2}, Landroid/view/GLES20Canvas;->nTranslate(IFF)V

    #@e
    .line 549
    :cond_e
    return-void
.end method
