.class final Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;
.super Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;
.source "ViewRootImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AccessibilityInteractionConnection"
.end annotation


# instance fields
.field private final mViewRootImpl:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewRootImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 3
    .parameter "viewRootImpl"

    #@0
    .prologue
    .line 5565
    invoke-direct {p0}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;-><init>()V

    #@3
    .line 5566
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@a
    .line 5567
    return-void
.end method


# virtual methods
.method public findAccessibilityNodeInfoByAccessibilityId(JILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 19
    .parameter "accessibilityNodeId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 5573
    iget-object v0, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v9

    #@6
    check-cast v9, Landroid/view/ViewRootImpl;

    #@8
    .line 5574
    .local v9, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v9, :cond_1e

    #@a
    iget-object v0, v9, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v0, :cond_1e

    #@e
    .line 5575
    invoke-virtual {v9}, Landroid/view/ViewRootImpl;->getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;

    #@11
    move-result-object v0

    #@12
    move-wide v1, p1

    #@13
    move v3, p3

    #@14
    move-object v4, p4

    #@15
    move v5, p5

    #@16
    move/from16 v6, p6

    #@18
    move-wide/from16 v7, p7

    #@1a
    invoke-virtual/range {v0 .. v8}, Landroid/view/AccessibilityInteractionController;->findAccessibilityNodeInfoByAccessibilityIdClientThread(JILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@1d
    .line 5586
    :goto_1d
    return-void

    #@1e
    .line 5581
    :cond_1e
    const/4 v0, 0x0

    #@1f
    :try_start_1f
    invoke-interface {p4, v0, p3}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_22} :catch_23

    #@22
    goto :goto_1d

    #@23
    .line 5582
    :catch_23
    move-exception v0

    #@24
    goto :goto_1d
.end method

.method public findAccessibilityNodeInfoByViewId(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 23
    .parameter "accessibilityNodeId"
    .parameter "viewId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 5612
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v12

    #@6
    check-cast v12, Landroid/view/ViewRootImpl;

    #@8
    .line 5613
    .local v12, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v12, :cond_23

    #@a
    iget-object v2, v12, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_23

    #@e
    .line 5614
    invoke-virtual {v12}, Landroid/view/ViewRootImpl;->getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;

    #@11
    move-result-object v2

    #@12
    move-wide v3, p1

    #@13
    move/from16 v5, p3

    #@15
    move/from16 v6, p4

    #@17
    move-object/from16 v7, p5

    #@19
    move/from16 v8, p6

    #@1b
    move/from16 v9, p7

    #@1d
    move-wide/from16 v10, p8

    #@1f
    invoke-virtual/range {v2 .. v11}, Landroid/view/AccessibilityInteractionController;->findAccessibilityNodeInfoByViewIdClientThread(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@22
    .line 5625
    :goto_22
    return-void

    #@23
    .line 5620
    :cond_23
    const/4 v2, 0x0

    #@24
    :try_start_24
    move-object/from16 v0, p5

    #@26
    move/from16 v1, p4

    #@28
    invoke-interface {v0, v2, v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_22

    #@2c
    .line 5621
    :catch_2c
    move-exception v2

    #@2d
    goto :goto_22
.end method

.method public findAccessibilityNodeInfosByText(JLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 23
    .parameter "accessibilityNodeId"
    .parameter "text"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 5631
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v12

    #@6
    check-cast v12, Landroid/view/ViewRootImpl;

    #@8
    .line 5632
    .local v12, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v12, :cond_23

    #@a
    iget-object v2, v12, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_23

    #@e
    .line 5633
    invoke-virtual {v12}, Landroid/view/ViewRootImpl;->getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;

    #@11
    move-result-object v2

    #@12
    move-wide v3, p1

    #@13
    move-object/from16 v5, p3

    #@15
    move/from16 v6, p4

    #@17
    move-object/from16 v7, p5

    #@19
    move/from16 v8, p6

    #@1b
    move/from16 v9, p7

    #@1d
    move-wide/from16 v10, p8

    #@1f
    invoke-virtual/range {v2 .. v11}, Landroid/view/AccessibilityInteractionController;->findAccessibilityNodeInfosByTextClientThread(JLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@22
    .line 5644
    :goto_22
    return-void

    #@23
    .line 5639
    :cond_23
    const/4 v2, 0x0

    #@24
    :try_start_24
    move-object/from16 v0, p5

    #@26
    move/from16 v1, p4

    #@28
    invoke-interface {v0, v2, v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_22

    #@2c
    .line 5640
    :catch_2c
    move-exception v2

    #@2d
    goto :goto_22
.end method

.method public findFocus(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 23
    .parameter "accessibilityNodeId"
    .parameter "focusType"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 5650
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v12

    #@6
    check-cast v12, Landroid/view/ViewRootImpl;

    #@8
    .line 5651
    .local v12, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v12, :cond_23

    #@a
    iget-object v2, v12, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_23

    #@e
    .line 5652
    invoke-virtual {v12}, Landroid/view/ViewRootImpl;->getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;

    #@11
    move-result-object v2

    #@12
    move-wide v3, p1

    #@13
    move/from16 v5, p3

    #@15
    move/from16 v6, p4

    #@17
    move-object/from16 v7, p5

    #@19
    move/from16 v8, p6

    #@1b
    move/from16 v9, p7

    #@1d
    move-wide/from16 v10, p8

    #@1f
    invoke-virtual/range {v2 .. v11}, Landroid/view/AccessibilityInteractionController;->findFocusClientThread(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@22
    .line 5663
    :goto_22
    return-void

    #@23
    .line 5658
    :cond_23
    const/4 v2, 0x0

    #@24
    :try_start_24
    move-object/from16 v0, p5

    #@26
    move/from16 v1, p4

    #@28
    invoke-interface {v0, v2, v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_22

    #@2c
    .line 5659
    :catch_2c
    move-exception v2

    #@2d
    goto :goto_22
.end method

.method public focusSearch(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 23
    .parameter "accessibilityNodeId"
    .parameter "direction"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 5669
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v12

    #@6
    check-cast v12, Landroid/view/ViewRootImpl;

    #@8
    .line 5670
    .local v12, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v12, :cond_23

    #@a
    iget-object v2, v12, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_23

    #@e
    .line 5671
    invoke-virtual {v12}, Landroid/view/ViewRootImpl;->getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;

    #@11
    move-result-object v2

    #@12
    move-wide v3, p1

    #@13
    move/from16 v5, p3

    #@15
    move/from16 v6, p4

    #@17
    move-object/from16 v7, p5

    #@19
    move/from16 v8, p6

    #@1b
    move/from16 v9, p7

    #@1d
    move-wide/from16 v10, p8

    #@1f
    invoke-virtual/range {v2 .. v11}, Landroid/view/AccessibilityInteractionController;->focusSearchClientThread(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@22
    .line 5682
    :goto_22
    return-void

    #@23
    .line 5677
    :cond_23
    const/4 v2, 0x0

    #@24
    :try_start_24
    move-object/from16 v0, p5

    #@26
    move/from16 v1, p4

    #@28
    invoke-interface {v0, v2, v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_22

    #@2c
    .line 5678
    :catch_2c
    move-exception v2

    #@2d
    goto :goto_22
.end method

.method public performAccessibilityAction(JILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 25
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 5593
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;->mViewRootImpl:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v13

    #@6
    check-cast v13, Landroid/view/ViewRootImpl;

    #@8
    .line 5594
    .local v13, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v13, :cond_25

    #@a
    iget-object v2, v13, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_25

    #@e
    .line 5595
    invoke-virtual {v13}, Landroid/view/ViewRootImpl;->getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;

    #@11
    move-result-object v2

    #@12
    move-wide v3, p1

    #@13
    move/from16 v5, p3

    #@15
    move-object/from16 v6, p4

    #@17
    move/from16 v7, p5

    #@19
    move-object/from16 v8, p6

    #@1b
    move/from16 v9, p7

    #@1d
    move/from16 v10, p8

    #@1f
    move-wide/from16 v11, p9

    #@21
    invoke-virtual/range {v2 .. v12}, Landroid/view/AccessibilityInteractionController;->performAccessibilityActionClientThread(JILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@24
    .line 5606
    :goto_24
    return-void

    #@25
    .line 5601
    :cond_25
    const/4 v2, 0x0

    #@26
    :try_start_26
    move-object/from16 v0, p6

    #@28
    move/from16 v1, p5

    #@2a
    invoke-interface {v0, v2, v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setPerformAccessibilityActionResult(ZI)V
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2d} :catch_2e

    #@2d
    goto :goto_24

    #@2e
    .line 5602
    :catch_2e
    move-exception v2

    #@2f
    goto :goto_24
.end method
