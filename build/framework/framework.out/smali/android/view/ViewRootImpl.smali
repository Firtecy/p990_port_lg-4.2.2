.class public final Landroid/view/ViewRootImpl;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"

# interfaces
.implements Landroid/view/ViewParent;
.implements Landroid/view/View$AttachInfo$Callbacks;
.implements Landroid/view/HardwareRenderer$HardwareDrawCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ViewRootImpl$IObjectFinderEngine;,
        Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;,
        Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;,
        Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;,
        Landroid/view/ViewRootImpl$RunQueue;,
        Landroid/view/ViewRootImpl$CalledFromWrongThreadException;,
        Landroid/view/ViewRootImpl$TrackballAxis;,
        Landroid/view/ViewRootImpl$W;,
        Landroid/view/ViewRootImpl$InputMethodCallback;,
        Landroid/view/ViewRootImpl$TakenSurfaceHolder;,
        Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;,
        Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;,
        Landroid/view/ViewRootImpl$WindowInputEventReceiver;,
        Landroid/view/ViewRootImpl$TraversalRunnable;,
        Landroid/view/ViewRootImpl$QueuedInputEvent;,
        Landroid/view/ViewRootImpl$ViewRootHandler;,
        Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;
    }
.end annotation


# static fields
.field private static final CHROME_VIEW_NAME:Ljava/lang/String; = "chromium.content.browser.ChromeView"

.field private static final DBG:Z = false

.field private static final DEBUG_CONFIGURATION:Z = false

.field private static final DEBUG_DIALOG:Z = false

.field private static final DEBUG_DRAW:Z = false

.field private static final DEBUG_FPS:Z = false

.field private static final DEBUG_IMF:Z = false

.field private static final DEBUG_INPUT_RESIZE:Z = false

.field private static final DEBUG_LAYOUT:Z = false

.field private static final DEBUG_ORIENTATION:Z = false

.field private static final DEBUG_TRACKBALL:Z = false

.field private static final LOCAL_LOGV:Z = false

.field private static final MAX_QUEUED_INPUT_EVENT_POOL_SIZE:I = 0xa

.field static final MAX_TRACKBALL_DELAY:I = 0xfa

.field private static final MEASURE_LATENCY:Z = false

.field private static final MSG_CHECK_FOCUS:I = 0xd

.field private static final MSG_CLEAR_ACCESSIBILITY_FOCUS_HOST:I = 0x16

.field private static final MSG_CLOSE_SYSTEM_DIALOGS:I = 0xe

.field private static final MSG_DIE:I = 0x3

.field private static final MSG_DISPATCH_APP_VISIBILITY:I = 0x8

.field private static final MSG_DISPATCH_DONE_ANIMATING:I = 0x17

.field private static final MSG_DISPATCH_DRAG_EVENT:I = 0xf

.field private static final MSG_DISPATCH_DRAG_LOCATION_EVENT:I = 0x10

.field private static final MSG_DISPATCH_GET_NEW_SURFACE:I = 0x9

.field private static final MSG_DISPATCH_KEY:I = 0x7

.field private static final MSG_DISPATCH_KEY_FROM_IME:I = 0xb

.field private static final MSG_DISPATCH_SCREEN_STATE:I = 0x14

.field private static final MSG_DISPATCH_SYSTEM_UI_VISIBILITY:I = 0x11

.field private static final MSG_FINISH_INPUT_CONNECTION:I = 0xc

.field private static final MSG_IME_FINISHED_EVENT:I = 0xa

.field private static final MSG_INVALIDATE:I = 0x1

.field private static final MSG_INVALIDATE_DISPLAY_LIST:I = 0x15

.field private static final MSG_INVALIDATE_RECT:I = 0x2

.field private static final MSG_INVALIDATE_WORLD:I = 0x18

.field private static final MSG_PROCESS_INPUT_EVENTS:I = 0x13

.field private static final MSG_RESIZED:I = 0x4

.field private static final MSG_RESIZED_REPORT:I = 0x5

.field private static final MSG_UPDATE_CONFIGURATION:I = 0x12

.field private static final MSG_WINDOW_FOCUS_CHANGED:I = 0x6

.field private static final MSG_WINDOW_MOVED:I = 0x19

#the value of this static final field might be set in the static constructor
.field static final NOTUSER_DEBUG:Z = false

.field private static final PROPERTY_PROFILE_RENDERING:Ljava/lang/String; = "viewancestor.profile_rendering"

.field private static final TAG:Ljava/lang/String; = "ViewRootImpl"

.field private static final USE_RENDER_THREAD:Z

.field private static lt:Landroid/os/LatencyTimer;

.field static final mResizeInterpolator:Landroid/view/animation/Interpolator;

.field static final sConfigCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ComponentCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field static sFirstDrawComplete:Z

.field static final sFirstDrawHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static sObjectFinderEngine:Landroid/view/ViewRootImpl$IObjectFinderEngine;

.field private static sRenderThreadQueried:Z

.field private static final sRenderThreadQueryLock:[Ljava/lang/Object;

.field static final sRunQueues:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/view/ViewRootImpl$RunQueue;",
            ">;"
        }
    .end annotation
.end field

.field private static sUseRenderThread:Z


# instance fields
.field private DEBUG_FPS_FROM_BUILD_PROPORTY:Z

.field private DURATION_OF_TIMER:I

.field private isFilterEnable:Z

.field private isPortrait:Z

.field mAccessibilityFocusedHost:Landroid/view/View;

.field mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

.field mAccessibilityInteractionConnectionManager:Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

.field mAccessibilityInteractionController:Landroid/view/AccessibilityInteractionController;

.field final mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActiveTimer:Ljava/util/Timer;

.field mAdded:Z

.field mAddedTouchMode:Z

.field mAppVisible:Z

.field final mAttachInfo:Landroid/view/View$AttachInfo;

.field mAudioManager:Landroid/media/AudioManager;

.field mChoreographer:Landroid/view/Choreographer;

.field mClientWindowLayoutFlags:I

.field final mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

.field mConsumeBatchedInputScheduled:Z

.field final mConsumedBatchedInputRunnable:Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;

.field mCurScrollY:I

.field final mCurrentDirty:Landroid/graphics/Rect;

.field mCurrentDragView:Landroid/view/View;

.field mCurrentInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

.field private final mDensity:I

.field mDirty:Landroid/graphics/Rect;

.field final mDisplay:Landroid/view/Display;

.field private final mDisplayLists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/DisplayList;",
            ">;"
        }
    .end annotation
.end field

.field mDoDispatch:Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;

.field mDragDescription:Landroid/content/ClipDescription;

.field final mDragPoint:Landroid/graphics/PointF;

.field mDrawingAllowed:Z

.field mFallbackEventHandler:Landroid/view/FallbackEventHandler;

.field mFirst:Z

.field mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

.field final mFitSystemWindowsInsets:Landroid/graphics/Rect;

.field mFitSystemWindowsRequested:Z

.field mFocusedView:Landroid/view/View;

.field private mFpsNumFrames:I

.field private mFpsPrevTime:J

.field private mFpsStartTime:J

.field mFullRedrawNeeded:Z

.field final mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

.field mHardwareYOffset:I

.field mHasHadWindowFocus:Z

.field mHeight:I

.field private mHolder:Landroid/view/SurfaceHolder;

.field mInputChannel:Landroid/view/InputChannel;

.field protected final mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

.field mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;

.field final mInputMethodCallback:Landroid/view/ViewRootImpl$InputMethodCallback;

.field mInputQueue:Landroid/view/InputQueue;

.field mInputQueueCallback:Landroid/view/InputQueue$Callback;

.field mInteractionManager:Lcom/lge/loader/interaction/IInteractionManager;

.field final mInvalidateOnAnimationRunnable:Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;

.field private mIsActive:Z

.field mIsAnimating:Z

.field mIsCreating:Z

.field mIsDrawing:Z

.field mIsInTraversal:Z

.field private mIsScheduled:Z

.field final mLastConfiguration:Landroid/content/res/Configuration;

.field final mLastGivenInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

.field mLastInCompatMode:Z

.field mLastJoystickXDirection:I

.field mLastJoystickXKeyCode:I

.field mLastJoystickYDirection:I

.field mLastJoystickYKeyCode:I

.field mLastScrolledFocus:Landroid/view/View;

.field mLastSystemUiVisibility:I

.field final mLastTouchPoint:Landroid/graphics/PointF;

.field mLastTrackballTime:J

.field mLastWasImTarget:Z

.field mLayoutRequested:Z

.field volatile mLocalDragState:Ljava/lang/Object;

.field final mLocation:Landroid/view/WindowLeaked;

.field mNewSurfaceNeeded:Z

.field private final mNoncompatDensity:I

.field mOldFocusedView:Landroid/view/View;

.field mOrigWindowType:I

.field final mPendingConfiguration:Landroid/content/res/Configuration;

.field final mPendingContentInsets:Landroid/graphics/Rect;

.field private mPendingTransitions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/LayoutTransition;",
            ">;"
        }
    .end annotation
.end field

.field final mPendingVisibleInsets:Landroid/graphics/Rect;

.field private mPerfObject:Lorg/codeaurora/Performance;

.field final mPreviousDirty:Landroid/graphics/Rect;

.field final mPreviousTransparentRegion:Landroid/graphics/Region;

.field mProcessInputEventsScheduled:Z

.field private mProfile:Z

.field private mProfileRendering:Z

.field private mQueuedInputEventPool:Landroid/view/ViewRootImpl$QueuedInputEvent;

.field private mQueuedInputEventPoolSize:I

.field mRealFocusedView:Landroid/view/View;

.field private mRenderProfiler:Ljava/lang/Thread;

.field private volatile mRenderProfilingEnabled:Z

.field mReportNextDraw:Z

.field mResizeAlpha:I

.field mResizeBuffer:Landroid/view/HardwareLayer;

.field mResizeBufferDuration:I

.field mResizeBufferStartTime:J

.field final mResizePaint:Landroid/graphics/Paint;

.field mScrollMayChange:Z

.field mScrollY:I

.field mScroller:Landroid/widget/Scroller;

.field mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

.field mSeq:I

.field mSoftInputMode:I

.field mStopped:Z

.field private final mSurface:Landroid/view/Surface;

.field mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

.field mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

.field final mTargetSdkVersion:I

.field mTempHashSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final mTempRect:Landroid/graphics/Rect;

.field final mThread:Ljava/lang/Thread;

.field final mTmpLocation:[I

.field final mTmpValue:Landroid/util/TypedValue;

.field private mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

.field final mTrackballAxisX:Landroid/view/ViewRootImpl$TrackballAxis;

.field final mTrackballAxisY:Landroid/view/ViewRootImpl$TrackballAxis;

.field mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

.field final mTransparentRegion:Landroid/graphics/Region;

.field mTraversalBarrier:I

.field final mTraversalRunnable:Landroid/view/ViewRootImpl$TraversalRunnable;

.field mTraversalScheduled:Z

.field mView:Landroid/view/View;

.field final mViewConfiguration:Landroid/view/ViewConfiguration;

.field private mViewLayoutDirectionInitial:I

.field mViewVisibility:I

.field final mVisRect:Landroid/graphics/Rect;

.field mWidth:I

.field mWillDrawSoon:Z

.field final mWinFrame:Landroid/graphics/Rect;

.field final mWindow:Landroid/view/ViewRootImpl$W;

.field final mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

.field mWindowAttributesChanged:Z

.field mWindowAttributesChangesFlag:I

.field final mWindowSession:Landroid/view/IWindowSession;

.field mWindowsAnimating:Z


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 147
    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@4
    const-string/jumbo v5, "user"

    #@7
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v3

    #@b
    if-nez v3, :cond_58

    #@d
    const/4 v3, 0x1

    #@e
    :goto_e
    sput-boolean v3, Landroid/view/ViewRootImpl;->NOTUSER_DEBUG:Z

    #@10
    .line 155
    new-instance v3, Ljava/lang/ThreadLocal;

    #@12
    invoke-direct {v3}, Ljava/lang/ThreadLocal;-><init>()V

    #@15
    sput-object v3, Landroid/view/ViewRootImpl;->sRunQueues:Ljava/lang/ThreadLocal;

    #@17
    .line 157
    new-instance v3, Ljava/util/ArrayList;

    #@19
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@1c
    sput-object v3, Landroid/view/ViewRootImpl;->sFirstDrawHandlers:Ljava/util/ArrayList;

    #@1e
    .line 158
    sput-boolean v4, Landroid/view/ViewRootImpl;->sFirstDrawComplete:Z

    #@20
    .line 160
    new-instance v3, Ljava/util/ArrayList;

    #@22
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@25
    sput-object v3, Landroid/view/ViewRootImpl;->sConfigCallbacks:Ljava/util/ArrayList;

    #@27
    .line 163
    sput-boolean v4, Landroid/view/ViewRootImpl;->sUseRenderThread:Z

    #@29
    .line 164
    sput-boolean v4, Landroid/view/ViewRootImpl;->sRenderThreadQueried:Z

    #@2b
    .line 165
    new-array v3, v4, [Ljava/lang/Object;

    #@2d
    sput-object v3, Landroid/view/ViewRootImpl;->sRenderThreadQueryLock:[Ljava/lang/Object;

    #@2f
    .line 304
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@31
    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@34
    sput-object v3, Landroid/view/ViewRootImpl;->mResizeInterpolator:Landroid/view/animation/Interpolator;

    #@36
    .line 5713
    sput-object v6, Landroid/view/ViewRootImpl;->sObjectFinderEngine:Landroid/view/ViewRootImpl$IObjectFinderEngine;

    #@38
    .line 5717
    :try_start_38
    const-string v3, "android.view.ObjectFinderEngine"

    #@3a
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@3d
    move-result-object v0

    #@3e
    .line 5718
    .local v0, c:Ljava/lang/Class;
    if-eqz v0, :cond_57

    #@40
    .line 5719
    const-string v3, "getInstance"

    #@42
    const/4 v4, 0x0

    #@43
    new-array v4, v4, [Ljava/lang/Class;

    #@45
    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@48
    move-result-object v2

    #@49
    .line 5720
    .local v2, mainMethod:Ljava/lang/reflect/Method;
    if-eqz v2, :cond_57

    #@4b
    .line 5721
    const/4 v3, 0x0

    #@4c
    const/4 v4, 0x0

    #@4d
    new-array v4, v4, [Ljava/lang/Object;

    #@4f
    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@52
    move-result-object v3

    #@53
    check-cast v3, Landroid/view/ViewRootImpl$IObjectFinderEngine;

    #@55
    sput-object v3, Landroid/view/ViewRootImpl;->sObjectFinderEngine:Landroid/view/ViewRootImpl$IObjectFinderEngine;
    :try_end_57
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_57} :catch_5a

    #@57
    .line 5729
    .end local v2           #mainMethod:Ljava/lang/reflect/Method;
    :cond_57
    :goto_57
    return-void

    #@58
    .end local v0           #c:Ljava/lang/Class;
    :cond_58
    move v3, v4

    #@59
    .line 147
    goto :goto_e

    #@5a
    .line 5725
    .restart local v0       #c:Ljava/lang/Class;
    :catch_5a
    move-exception v1

    #@5b
    .line 5726
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "ViewRootImpl"

    #@5d
    const-string v4, "cannot instantiate ObjectFinderEngine object"

    #@5f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 5727
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@65
    goto :goto_57
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;)V
    .registers 14
    .parameter "context"
    .parameter "display"

    #@0
    .prologue
    const-wide/16 v9, -0x1

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, -0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 125
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->DEBUG_FPS_FROM_BUILD_PROPORTY:Z

    #@b
    .line 137
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mIsScheduled:Z

    #@d
    .line 138
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mIsActive:Z

    #@f
    .line 139
    const/16 v0, 0x708

    #@11
    iput v0, p0, Landroid/view/ViewRootImpl;->DURATION_OF_TIMER:I

    #@13
    .line 141
    new-instance v0, Lorg/codeaurora/Performance;

    #@15
    invoke-direct {v0}, Lorg/codeaurora/Performance;-><init>()V

    #@18
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPerfObject:Lorg/codeaurora/Performance;

    #@1a
    .line 170
    const-wide/16 v2, 0x0

    #@1c
    iput-wide v2, p0, Landroid/view/ViewRootImpl;->mLastTrackballTime:J

    #@1e
    .line 171
    new-instance v0, Landroid/view/ViewRootImpl$TrackballAxis;

    #@20
    invoke-direct {v0}, Landroid/view/ViewRootImpl$TrackballAxis;-><init>()V

    #@23
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTrackballAxisX:Landroid/view/ViewRootImpl$TrackballAxis;

    #@25
    .line 172
    new-instance v0, Landroid/view/ViewRootImpl$TrackballAxis;

    #@27
    invoke-direct {v0}, Landroid/view/ViewRootImpl$TrackballAxis;-><init>()V

    #@2a
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTrackballAxisY:Landroid/view/ViewRootImpl$TrackballAxis;

    #@2c
    .line 179
    const/4 v0, 0x2

    #@2d
    new-array v0, v0, [I

    #@2f
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTmpLocation:[I

    #@31
    .line 181
    new-instance v0, Landroid/util/TypedValue;

    #@33
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@36
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTmpValue:Landroid/util/TypedValue;

    #@38
    .line 188
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@3a
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@3d
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@3f
    .line 205
    iput-boolean v6, p0, Landroid/view/ViewRootImpl;->mAppVisible:Z

    #@41
    .line 206
    iput v5, p0, Landroid/view/ViewRootImpl;->mOrigWindowType:I

    #@43
    .line 210
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mStopped:Z

    #@45
    .line 212
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mLastInCompatMode:Z

    #@47
    .line 225
    new-instance v0, Landroid/graphics/Rect;

    #@49
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@4c
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mCurrentDirty:Landroid/graphics/Rect;

    #@4e
    .line 226
    new-instance v0, Landroid/graphics/Rect;

    #@50
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@53
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPreviousDirty:Landroid/graphics/Rect;

    #@55
    .line 270
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z

    #@57
    .line 271
    iput v4, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChangesFlag:I

    #@59
    .line 275
    new-instance v0, Landroid/view/Surface;

    #@5b
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@5e
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@60
    .line 285
    new-instance v0, Landroid/graphics/Rect;

    #@62
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@65
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@67
    .line 286
    new-instance v0, Landroid/graphics/Rect;

    #@69
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@6c
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@6e
    .line 287
    new-instance v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@70
    invoke-direct {v0}, Landroid/view/ViewTreeObserver$InternalInsetsInfo;-><init>()V

    #@73
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mLastGivenInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@75
    .line 290
    new-instance v0, Landroid/graphics/Rect;

    #@77
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@7a
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@7c
    .line 292
    new-instance v0, Landroid/content/res/Configuration;

    #@7e
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@81
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@83
    .line 293
    new-instance v0, Landroid/content/res/Configuration;

    #@85
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@88
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@8a
    .line 313
    new-instance v0, Landroid/graphics/PointF;

    #@8c
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@8f
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mDragPoint:Landroid/graphics/PointF;

    #@91
    .line 314
    new-instance v0, Landroid/graphics/PointF;

    #@93
    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    #@96
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mLastTouchPoint:Landroid/graphics/PointF;

    #@98
    .line 324
    iput-wide v9, p0, Landroid/view/ViewRootImpl;->mFpsStartTime:J

    #@9a
    .line 325
    iput-wide v9, p0, Landroid/view/ViewRootImpl;->mFpsPrevTime:J

    #@9c
    .line 328
    new-instance v0, Ljava/util/ArrayList;

    #@9e
    const/16 v2, 0x18

    #@a0
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@a3
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mDisplayLists:Ljava/util/ArrayList;

    #@a5
    .line 351
    new-instance v0, Landroid/view/ViewRootImpl$1;

    #@a7
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$1;-><init>(Landroid/view/ViewRootImpl;)V

    #@aa
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mDoDispatch:Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;

    #@ac
    .line 358
    new-instance v0, Lcom/lge/view/TouchEventFilter;

    #@ae
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mDoDispatch:Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;

    #@b0
    invoke-direct {v0, v2}, Lcom/lge/view/TouchEventFilter;-><init>(Lcom/lge/view/TouchEventFilter$DoDispatchMotionEvent;)V

    #@b3
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@b5
    .line 360
    iput-boolean v6, p0, Landroid/view/ViewRootImpl;->isPortrait:Z

    #@b7
    .line 361
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->isFilterEnable:Z

    #@b9
    .line 367
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@bc
    move-result v0

    #@bd
    if-eqz v0, :cond_1d6

    #@bf
    new-instance v0, Landroid/view/InputEventConsistencyVerifier;

    #@c1
    invoke-direct {v0, p0, v4}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;I)V

    #@c4
    :goto_c4
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@c6
    .line 476
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mProfile:Z

    #@c8
    .line 2052
    new-instance v0, Landroid/graphics/Paint;

    #@ca
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@cd
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mResizePaint:Landroid/graphics/Paint;

    #@cf
    .line 3140
    new-instance v0, Landroid/view/ViewRootImpl$ViewRootHandler;

    #@d1
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$ViewRootHandler;-><init>(Landroid/view/ViewRootImpl;)V

    #@d4
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@d6
    .line 4556
    new-instance v0, Landroid/view/ViewRootImpl$TraversalRunnable;

    #@d8
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$TraversalRunnable;-><init>(Landroid/view/ViewRootImpl;)V

    #@db
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTraversalRunnable:Landroid/view/ViewRootImpl$TraversalRunnable;

    #@dd
    .line 4596
    new-instance v0, Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;

    #@df
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;-><init>(Landroid/view/ViewRootImpl;)V

    #@e2
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mConsumedBatchedInputRunnable:Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;

    #@e4
    .line 4682
    new-instance v0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;

    #@e6
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;-><init>(Landroid/view/ViewRootImpl;)V

    #@e9
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mInvalidateOnAnimationRunnable:Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;

    #@eb
    .line 5380
    new-instance v0, Landroid/view/ViewRootImpl$4;

    #@ed
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$4;-><init>(Landroid/view/ViewRootImpl;)V

    #@f0
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mHolder:Landroid/view/SurfaceHolder;

    #@f2
    .line 390
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@f5
    move-result-object v0

    #@f6
    invoke-static {v0}, Landroid/view/WindowManagerGlobal;->getWindowSession(Landroid/os/Looper;)Landroid/view/IWindowSession;

    #@f9
    move-result-object v0

    #@fa
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@fc
    .line 391
    iput-object p2, p0, Landroid/view/ViewRootImpl;->mDisplay:Landroid/view/Display;

    #@fe
    .line 393
    invoke-virtual {p2}, Landroid/view/Display;->getCompatibilityInfo()Landroid/view/CompatibilityInfoHolder;

    #@101
    move-result-object v7

    #@102
    .line 394
    .local v7, cih:Landroid/view/CompatibilityInfoHolder;
    if-eqz v7, :cond_1d9

    #@104
    .end local v7           #cih:Landroid/view/CompatibilityInfoHolder;
    :goto_104
    iput-object v7, p0, Landroid/view/ViewRootImpl;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@106
    .line 396
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@109
    move-result-object v0

    #@10a
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mThread:Ljava/lang/Thread;

    #@10c
    .line 397
    new-instance v0, Landroid/view/WindowLeaked;

    #@10e
    invoke-direct {v0, v1}, Landroid/view/WindowLeaked;-><init>(Ljava/lang/String;)V

    #@111
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mLocation:Landroid/view/WindowLeaked;

    #@113
    .line 398
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mLocation:Landroid/view/WindowLeaked;

    #@115
    invoke-virtual {v0}, Landroid/view/WindowLeaked;->fillInStackTrace()Ljava/lang/Throwable;

    #@118
    .line 399
    iput v5, p0, Landroid/view/ViewRootImpl;->mWidth:I

    #@11a
    .line 400
    iput v5, p0, Landroid/view/ViewRootImpl;->mHeight:I

    #@11c
    .line 401
    new-instance v0, Landroid/graphics/Rect;

    #@11e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@121
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mDirty:Landroid/graphics/Rect;

    #@123
    .line 402
    new-instance v0, Landroid/graphics/Rect;

    #@125
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@128
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@12a
    .line 403
    new-instance v0, Landroid/graphics/Rect;

    #@12c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@12f
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mVisRect:Landroid/graphics/Rect;

    #@131
    .line 404
    new-instance v0, Landroid/graphics/Rect;

    #@133
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@136
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@138
    .line 405
    new-instance v0, Landroid/view/ViewRootImpl$W;

    #@13a
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$W;-><init>(Landroid/view/ViewRootImpl;)V

    #@13d
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@13f
    .line 406
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@142
    move-result-object v0

    #@143
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@145
    iput v0, p0, Landroid/view/ViewRootImpl;->mTargetSdkVersion:I

    #@147
    .line 407
    new-instance v0, Landroid/view/ViewRootImpl$InputMethodCallback;

    #@149
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$InputMethodCallback;-><init>(Landroid/view/ViewRootImpl;)V

    #@14c
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mInputMethodCallback:Landroid/view/ViewRootImpl$InputMethodCallback;

    #@14e
    .line 408
    const/16 v0, 0x8

    #@150
    iput v0, p0, Landroid/view/ViewRootImpl;->mViewVisibility:I

    #@152
    .line 409
    new-instance v0, Landroid/graphics/Region;

    #@154
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@157
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@159
    .line 410
    new-instance v0, Landroid/graphics/Region;

    #@15b
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@15e
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPreviousTransparentRegion:Landroid/graphics/Region;

    #@160
    .line 411
    iput-boolean v6, p0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@162
    .line 412
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@164
    .line 413
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@167
    move-result-object v0

    #@168
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@16a
    .line 414
    new-instance v0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

    #@16c
    invoke-direct {v0, p0}, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;-><init>(Landroid/view/ViewRootImpl;)V

    #@16f
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionConnectionManager:Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

    #@171
    .line 416
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@173
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionConnectionManager:Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

    #@175
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    #@178
    .line 418
    new-instance v0, Landroid/view/View$AttachInfo;

    #@17a
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@17c
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@17e
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@180
    move-object v3, p2

    #@181
    move-object v4, p0

    #@182
    move-object v6, p0

    #@183
    invoke-direct/range {v0 .. v6}, Landroid/view/View$AttachInfo;-><init>(Landroid/view/IWindowSession;Landroid/view/IWindow;Landroid/view/Display;Landroid/view/ViewRootImpl;Landroid/os/Handler;Landroid/view/View$AttachInfo$Callbacks;)V

    #@186
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@188
    .line 419
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@18b
    move-result-object v0

    #@18c
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mViewConfiguration:Landroid/view/ViewConfiguration;

    #@18e
    .line 420
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@191
    move-result-object v0

    #@192
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@195
    move-result-object v0

    #@196
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@198
    iput v0, p0, Landroid/view/ViewRootImpl;->mDensity:I

    #@19a
    .line 421
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19d
    move-result-object v0

    #@19e
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1a1
    move-result-object v0

    #@1a2
    iget v0, v0, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@1a4
    iput v0, p0, Landroid/view/ViewRootImpl;->mNoncompatDensity:I

    #@1a6
    .line 422
    invoke-static {p1}, Lcom/android/internal/policy/PolicyManager;->makeNewFallbackEventHandler(Landroid/content/Context;)Landroid/view/FallbackEventHandler;

    #@1a9
    move-result-object v0

    #@1aa
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mFallbackEventHandler:Landroid/view/FallbackEventHandler;

    #@1ac
    .line 423
    const-string/jumbo v0, "viewancestor.profile_rendering"

    #@1af
    const-string v1, "false"

    #@1b1
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b4
    move-result-object v0

    #@1b5
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@1b8
    move-result v0

    #@1b9
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mProfileRendering:Z

    #@1bb
    .line 425
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    #@1be
    move-result-object v0

    #@1bf
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@1c1
    .line 427
    const-string/jumbo v0, "power"

    #@1c4
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c7
    move-result-object v8

    #@1c8
    check-cast v8, Landroid/os/PowerManager;

    #@1ca
    .line 428
    .local v8, powerManager:Landroid/os/PowerManager;
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1cc
    invoke-virtual {v8}, Landroid/os/PowerManager;->isScreenOn()Z

    #@1cf
    move-result v1

    #@1d0
    iput-boolean v1, v0, Landroid/view/View$AttachInfo;->mScreenOn:Z

    #@1d2
    .line 429
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->loadSystemProperties()V

    #@1d5
    .line 430
    return-void

    #@1d6
    .end local v8           #powerManager:Landroid/os/PowerManager;
    :cond_1d6
    move-object v0, v1

    #@1d7
    .line 367
    goto/16 :goto_c4

    #@1d9
    .line 394
    .restart local v7       #cih:Landroid/view/CompatibilityInfoHolder;
    :cond_1d9
    new-instance v7, Landroid/view/CompatibilityInfoHolder;

    #@1db
    .end local v7           #cih:Landroid/view/CompatibilityInfoHolder;
    invoke-direct {v7}, Landroid/view/CompatibilityInfoHolder;-><init>()V

    #@1de
    goto/16 :goto_104
.end method

.method static synthetic access$000(Landroid/view/ViewRootImpl;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 108
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mRenderProfilingEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/view/View;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 108
    invoke-static {p0}, Landroid/view/ViewRootImpl;->forceLayout(Landroid/view/View;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/view/ViewRootImpl;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 108
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->profileRendering(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/view/ViewRootImpl;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 108
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->ensureTouchModeLocally(Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Landroid/view/ViewRootImpl;)Landroid/view/Surface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/view/ViewRootImpl;)Landroid/view/SurfaceHolder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHolder:Landroid/view/SurfaceHolder;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/view/ViewRootImpl;Landroid/view/DragEvent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 108
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->handleDragEvent(Landroid/view/DragEvent;)V

    #@3
    return-void
.end method

.method static synthetic access$900()Landroid/view/ViewRootImpl$IObjectFinderEngine;
    .registers 1

    #@0
    .prologue
    .line 108
    sget-object v0, Landroid/view/ViewRootImpl;->sObjectFinderEngine:Landroid/view/ViewRootImpl$IObjectFinderEngine;

    #@2
    return-object v0
.end method

.method public static addConfigCallback(Landroid/content/ComponentCallbacks;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 470
    sget-object v1, Landroid/view/ViewRootImpl;->sConfigCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 471
    :try_start_3
    sget-object v0, Landroid/view/ViewRootImpl;->sConfigCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8
    .line 472
    monitor-exit v1

    #@9
    .line 473
    return-void

    #@a
    .line 472
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public static addFirstDrawHandler(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 462
    sget-object v1, Landroid/view/ViewRootImpl;->sFirstDrawHandlers:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 463
    :try_start_3
    sget-boolean v0, Landroid/view/ViewRootImpl;->sFirstDrawComplete:Z

    #@5
    if-nez v0, :cond_c

    #@7
    .line 464
    sget-object v0, Landroid/view/ViewRootImpl;->sFirstDrawHandlers:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c
    .line 466
    :cond_c
    monitor-exit v1

    #@d
    .line 467
    return-void

    #@e
    .line 466
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private applyKeepScreenOnFlag(Landroid/view/WindowManager$LayoutParams;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 1066
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 1067
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@8
    or-int/lit16 v0, v0, 0x80

    #@a
    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@c
    .line 1072
    :goto_c
    return-void

    #@d
    .line 1069
    :cond_d
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@f
    and-int/lit16 v0, v0, -0x81

    #@11
    iget v1, p0, Landroid/view/ViewRootImpl;->mClientWindowLayoutFlags:I

    #@13
    and-int/lit16 v1, v1, 0x80

    #@15
    or-int/2addr v0, v1

    #@16
    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@18
    goto :goto_c
.end method

.method private checkForLeavingTouchModeAndConsume(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3707
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    iget-boolean v2, v2, Landroid/view/View$AttachInfo;->mInTouchMode:Z

    #@5
    if-nez v2, :cond_8

    #@7
    .line 3739
    :cond_7
    :goto_7
    return v1

    #@8
    .line 3712
    :cond_8
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@b
    move-result v0

    #@c
    .line 3713
    .local v0, action:I
    if-eqz v0, :cond_11

    #@e
    const/4 v2, 0x2

    #@f
    if-ne v0, v2, :cond_7

    #@11
    .line 3718
    :cond_11
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    #@14
    move-result v2

    #@15
    and-int/lit8 v2, v2, 0x4

    #@17
    if-nez v2, :cond_7

    #@19
    .line 3727
    invoke-static {p1}, Landroid/view/ViewRootImpl;->isNavigationKey(Landroid/view/KeyEvent;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_24

    #@1f
    .line 3728
    invoke-virtual {p0, v1}, Landroid/view/ViewRootImpl;->ensureTouchMode(Z)Z

    #@22
    move-result v1

    #@23
    goto :goto_7

    #@24
    .line 3734
    :cond_24
    invoke-static {p1}, Landroid/view/ViewRootImpl;->isTypingKey(Landroid/view/KeyEvent;)Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_7

    #@2a
    .line 3735
    invoke-virtual {p0, v1}, Landroid/view/ViewRootImpl;->ensureTouchMode(Z)Z

    #@2d
    goto :goto_7
.end method

.method private collectViewAttributes()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1075
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    .line 1076
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    iget-boolean v4, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@5
    if-eqz v4, :cond_44

    #@7
    .line 1078
    iput-boolean v3, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@9
    .line 1079
    iget-boolean v1, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@b
    .line 1080
    .local v1, oldScreenOn:Z
    iput-boolean v3, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@d
    .line 1081
    iput v3, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@f
    .line 1082
    iput-boolean v3, v0, Landroid/view/View$AttachInfo;->mHasSystemUiListeners:Z

    #@11
    .line 1083
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@13
    invoke-virtual {v4, v0, v3}, Landroid/view/View;->dispatchCollectViewAttributes(Landroid/view/View$AttachInfo;I)V

    #@16
    .line 1084
    iget v4, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@18
    iget v5, v0, Landroid/view/View$AttachInfo;->mDisabledSystemUiVisibility:I

    #@1a
    xor-int/lit8 v5, v5, -0x1

    #@1c
    and-int/2addr v4, v5

    #@1d
    iput v4, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@1f
    .line 1085
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@21
    .line 1086
    .local v2, params:Landroid/view/WindowManager$LayoutParams;
    iget-boolean v4, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@23
    if-ne v4, v1, :cond_31

    #@25
    iget v4, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@27
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@29
    if-ne v4, v5, :cond_31

    #@2b
    iget-boolean v4, v0, Landroid/view/View$AttachInfo;->mHasSystemUiListeners:Z

    #@2d
    iget-boolean v5, v2, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@2f
    if-eq v4, v5, :cond_44

    #@31
    .line 1089
    :cond_31
    invoke-direct {p0, v2}, Landroid/view/ViewRootImpl;->applyKeepScreenOnFlag(Landroid/view/WindowManager$LayoutParams;)V

    #@34
    .line 1090
    iget v3, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@36
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@38
    .line 1091
    iget-boolean v3, v0, Landroid/view/View$AttachInfo;->mHasSystemUiListeners:Z

    #@3a
    iput-boolean v3, v2, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@3c
    .line 1092
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3e
    iget v4, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@40
    invoke-virtual {v3, v4}, Landroid/view/View;->dispatchWindowSystemUiVisiblityChanged(I)V

    #@43
    .line 1093
    const/4 v3, 0x1

    #@44
    .line 1096
    .end local v1           #oldScreenOn:Z
    .end local v2           #params:Landroid/view/WindowManager$LayoutParams;
    :cond_44
    return v3
.end method

.method private deliverGenericMotionEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 7
    .parameter "q"

    #@0
    .prologue
    .line 3540
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@2
    check-cast v0, Landroid/view/MotionEvent;

    #@4
    .line 3541
    .local v0, event:Landroid/view/MotionEvent;
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@6
    if-eqz v3, :cond_e

    #@8
    .line 3542
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@a
    const/4 v4, 0x0

    #@b
    invoke-virtual {v3, v0, v4}, Landroid/view/InputEventConsistencyVerifier;->onGenericMotionEvent(Landroid/view/MotionEvent;I)V

    #@e
    .line 3545
    :cond_e
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@10
    if-eqz v3, :cond_36

    #@12
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@14
    if-eqz v3, :cond_36

    #@16
    iget v3, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mFlags:I

    #@18
    and-int/lit8 v3, v3, 0x1

    #@1a
    if-nez v3, :cond_36

    #@1c
    .line 3551
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@1e
    if-eqz v3, :cond_36

    #@20
    .line 3552
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@23
    move-result-object v1

    #@24
    .line 3553
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_36

    #@26
    .line 3554
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getSequenceNumber()I

    #@29
    move-result v2

    #@2a
    .line 3558
    .local v2, seq:I
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2c
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v3

    #@30
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mInputMethodCallback:Landroid/view/ViewRootImpl$InputMethodCallback;

    #@32
    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/view/inputmethod/InputMethodManager;->dispatchGenericMotionEvent(Landroid/content/Context;ILandroid/view/MotionEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@35
    .line 3567
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v2           #seq:I
    :goto_35
    return-void

    #@36
    .line 3566
    :cond_36
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverGenericMotionEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@39
    goto :goto_35
.end method

.method private deliverGenericMotionEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 7
    .parameter "q"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3570
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@4
    check-cast v0, Landroid/view/MotionEvent;

    #@6
    .line 3571
    .local v0, event:Landroid/view/MotionEvent;
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getSource()I

    #@9
    move-result v4

    #@a
    and-int/lit8 v4, v4, 0x10

    #@c
    if-eqz v4, :cond_20

    #@e
    move v1, v2

    #@f
    .line 3574
    .local v1, isJoystick:Z
    :goto_f
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@11
    if-eqz v4, :cond_17

    #@13
    iget-boolean v4, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@15
    if-nez v4, :cond_22

    #@17
    .line 3575
    :cond_17
    if-eqz v1, :cond_1c

    #@19
    .line 3576
    invoke-direct {p0, v0, v3}, Landroid/view/ViewRootImpl;->updateJoystickDirection(Landroid/view/MotionEvent;Z)V

    #@1c
    .line 3578
    :cond_1c
    invoke-direct {p0, p1, v3}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@1f
    .line 3599
    :goto_1f
    return-void

    #@20
    .end local v1           #isJoystick:Z
    :cond_20
    move v1, v3

    #@21
    .line 3571
    goto :goto_f

    #@22
    .line 3583
    .restart local v1       #isJoystick:Z
    :cond_22
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@24
    invoke-virtual {v4, v0}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_33

    #@2a
    .line 3584
    if-eqz v1, :cond_2f

    #@2c
    .line 3585
    invoke-direct {p0, v0, v3}, Landroid/view/ViewRootImpl;->updateJoystickDirection(Landroid/view/MotionEvent;Z)V

    #@2f
    .line 3587
    :cond_2f
    invoke-direct {p0, p1, v2}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@32
    goto :goto_1f

    #@33
    .line 3591
    :cond_33
    if-eqz v1, :cond_3c

    #@35
    .line 3594
    invoke-direct {p0, v0, v2}, Landroid/view/ViewRootImpl;->updateJoystickDirection(Landroid/view/MotionEvent;Z)V

    #@38
    .line 3595
    invoke-direct {p0, p1, v2}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@3b
    goto :goto_1f

    #@3c
    .line 3597
    :cond_3c
    invoke-direct {p0, p1, v3}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@3f
    goto :goto_1f
.end method

.method private deliverInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 6
    .parameter "q"

    #@0
    .prologue
    const-wide/16 v2, 0x8

    #@2
    .line 3264
    const-string v1, "deliverInputEvent"

    #@4
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@7
    .line 3266
    :try_start_7
    iget-object v1, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@9
    instance-of v1, v1, Landroid/view/KeyEvent;

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 3267
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverKeyEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_22

    #@10
    .line 3279
    :goto_10
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    #@13
    .line 3281
    return-void

    #@14
    .line 3269
    :cond_14
    :try_start_14
    iget-object v1, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@16
    invoke-virtual {v1}, Landroid/view/InputEvent;->getSource()I

    #@19
    move-result v0

    #@1a
    .line 3270
    .local v0, source:I
    and-int/lit8 v1, v0, 0x2

    #@1c
    if-eqz v1, :cond_27

    #@1e
    .line 3271
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverPointerEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    :try_end_21
    .catchall {:try_start_14 .. :try_end_21} :catchall_22

    #@21
    goto :goto_10

    #@22
    .line 3279
    .end local v0           #source:I
    :catchall_22
    move-exception v1

    #@23
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    #@26
    throw v1

    #@27
    .line 3272
    .restart local v0       #source:I
    :cond_27
    and-int/lit8 v1, v0, 0x4

    #@29
    if-eqz v1, :cond_2f

    #@2b
    .line 3273
    :try_start_2b
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverTrackballEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@2e
    goto :goto_10

    #@2f
    .line 3275
    :cond_2f
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverGenericMotionEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    :try_end_32
    .catchall {:try_start_2b .. :try_end_32} :catchall_22

    #@32
    goto :goto_10
.end method

.method private deliverKeyEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 7
    .parameter "q"

    #@0
    .prologue
    .line 3743
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@2
    check-cast v0, Landroid/view/KeyEvent;

    #@4
    .line 3744
    .local v0, event:Landroid/view/KeyEvent;
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@6
    if-eqz v3, :cond_e

    #@8
    .line 3745
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@a
    const/4 v4, 0x0

    #@b
    invoke-virtual {v3, v0, v4}, Landroid/view/InputEventConsistencyVerifier;->onKeyEvent(Landroid/view/KeyEvent;I)V

    #@e
    .line 3748
    :cond_e
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@10
    if-eqz v3, :cond_43

    #@12
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@14
    if-eqz v3, :cond_43

    #@16
    iget v3, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mFlags:I

    #@18
    and-int/lit8 v3, v3, 0x1

    #@1a
    if-nez v3, :cond_43

    #@1c
    .line 3752
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1e
    invoke-virtual {v3, v0}, Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_29

    #@24
    .line 3753
    const/4 v3, 0x1

    #@25
    invoke-direct {p0, p1, v3}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@28
    .line 3773
    :goto_28
    return-void

    #@29
    .line 3759
    :cond_29
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@2b
    if-eqz v3, :cond_43

    #@2d
    .line 3760
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@30
    move-result-object v1

    #@31
    .line 3761
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_43

    #@33
    .line 3762
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getSequenceNumber()I

    #@36
    move-result v2

    #@37
    .line 3765
    .local v2, seq:I
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@39
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@3c
    move-result-object v3

    #@3d
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mInputMethodCallback:Landroid/view/ViewRootImpl$InputMethodCallback;

    #@3f
    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/view/inputmethod/InputMethodManager;->dispatchKeyEvent(Landroid/content/Context;ILandroid/view/KeyEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@42
    goto :goto_28

    #@43
    .line 3772
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v2           #seq:I
    :cond_43
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverKeyEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@46
    goto :goto_28
.end method

.method private deliverKeyEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 10
    .parameter "q"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 3828
    iget-object v1, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@4
    check-cast v1, Landroid/view/KeyEvent;

    #@6
    .line 3831
    .local v1, event:Landroid/view/KeyEvent;
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@8
    if-eqz v4, :cond_e

    #@a
    iget-boolean v4, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@c
    if-nez v4, :cond_12

    #@e
    .line 3832
    :cond_e
    invoke-direct {p0, p1, v7}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@11
    .line 3935
    :goto_11
    return-void

    #@12
    .line 3837
    :cond_12
    invoke-direct {p0, v1}, Landroid/view/ViewRootImpl;->checkForLeavingTouchModeAndConsume(Landroid/view/KeyEvent;)Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_1c

    #@18
    .line 3838
    invoke-direct {p0, p1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@1b
    goto :goto_11

    #@1c
    .line 3844
    :cond_1c
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mFallbackEventHandler:Landroid/view/FallbackEventHandler;

    #@1e
    invoke-interface {v4, v1}, Landroid/view/FallbackEventHandler;->preDispatchKeyEvent(Landroid/view/KeyEvent;)V

    #@21
    .line 3847
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@23
    invoke-virtual {v4, v1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_2d

    #@29
    .line 3848
    invoke-direct {p0, p1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@2c
    goto :goto_11

    #@2d
    .line 3853
    :cond_2d
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getAction()I

    #@30
    move-result v4

    #@31
    if-nez v4, :cond_55

    #@33
    invoke-virtual {v1}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_55

    #@39
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@3c
    move-result v4

    #@3d
    if-nez v4, :cond_55

    #@3f
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@42
    move-result v4

    #@43
    invoke-static {v4}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    #@46
    move-result v4

    #@47
    if-nez v4, :cond_55

    #@49
    .line 3857
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4b
    invoke-virtual {v4, v1}, Landroid/view/View;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@4e
    move-result v4

    #@4f
    if-eqz v4, :cond_55

    #@51
    .line 3858
    invoke-direct {p0, p1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@54
    goto :goto_11

    #@55
    .line 3864
    :cond_55
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mFallbackEventHandler:Landroid/view/FallbackEventHandler;

    #@57
    invoke-interface {v4, v1}, Landroid/view/FallbackEventHandler;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@5a
    move-result v4

    #@5b
    if-eqz v4, :cond_61

    #@5d
    .line 3865
    invoke-direct {p0, p1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@60
    goto :goto_11

    #@61
    .line 3870
    :cond_61
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getAction()I

    #@64
    move-result v4

    #@65
    if-nez v4, :cond_f3

    #@67
    .line 3871
    const/4 v0, 0x0

    #@68
    .line 3872
    .local v0, direction:I
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@6b
    move-result v4

    #@6c
    sparse-switch v4, :sswitch_data_f8

    #@6f
    .line 3901
    :cond_6f
    :goto_6f
    if-eqz v0, :cond_f3

    #@71
    .line 3902
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@73
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@76
    move-result-object v2

    #@77
    .line 3903
    .local v2, focused:Landroid/view/View;
    if-eqz v2, :cond_f3

    #@79
    .line 3904
    invoke-virtual {v2, v0}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    #@7c
    move-result-object v3

    #@7d
    .line 3905
    .local v3, v:Landroid/view/View;
    if-eqz v3, :cond_e6

    #@7f
    if-eq v3, v2, :cond_e6

    #@81
    .line 3909
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@83
    invoke-virtual {v2, v4}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@86
    .line 3910
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@88
    instance-of v4, v4, Landroid/view/ViewGroup;

    #@8a
    if-eqz v4, :cond_9e

    #@8c
    .line 3911
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@8e
    check-cast v4, Landroid/view/ViewGroup;

    #@90
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@92
    invoke-virtual {v4, v2, v5}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@95
    .line 3913
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@97
    check-cast v4, Landroid/view/ViewGroup;

    #@99
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@9b
    invoke-virtual {v4, v3, v5}, Landroid/view/ViewGroup;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@9e
    .line 3916
    :cond_9e
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@a0
    invoke-virtual {v3, v0, v4}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@a3
    move-result v4

    #@a4
    if-eqz v4, :cond_e6

    #@a6
    .line 3917
    invoke-static {v0}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    #@a9
    move-result v4

    #@aa
    invoke-virtual {p0, v4}, Landroid/view/ViewRootImpl;->playSoundEffect(I)V

    #@ad
    .line 3919
    invoke-direct {p0, p1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@b0
    goto/16 :goto_11

    #@b2
    .line 3874
    .end local v2           #focused:Landroid/view/View;
    .end local v3           #v:Landroid/view/View;
    :sswitch_b2
    invoke-virtual {v1}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@b5
    move-result v4

    #@b6
    if-eqz v4, :cond_6f

    #@b8
    .line 3875
    const/16 v0, 0x11

    #@ba
    goto :goto_6f

    #@bb
    .line 3879
    :sswitch_bb
    invoke-virtual {v1}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@be
    move-result v4

    #@bf
    if-eqz v4, :cond_6f

    #@c1
    .line 3880
    const/16 v0, 0x42

    #@c3
    goto :goto_6f

    #@c4
    .line 3884
    :sswitch_c4
    invoke-virtual {v1}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@c7
    move-result v4

    #@c8
    if-eqz v4, :cond_6f

    #@ca
    .line 3885
    const/16 v0, 0x21

    #@cc
    goto :goto_6f

    #@cd
    .line 3889
    :sswitch_cd
    invoke-virtual {v1}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@d0
    move-result v4

    #@d1
    if-eqz v4, :cond_6f

    #@d3
    .line 3890
    const/16 v0, 0x82

    #@d5
    goto :goto_6f

    #@d6
    .line 3894
    :sswitch_d6
    invoke-virtual {v1}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    #@d9
    move-result v4

    #@da
    if-eqz v4, :cond_de

    #@dc
    .line 3895
    const/4 v0, 0x2

    #@dd
    goto :goto_6f

    #@de
    .line 3896
    :cond_de
    invoke-virtual {v1, v6}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    #@e1
    move-result v4

    #@e2
    if-eqz v4, :cond_6f

    #@e4
    .line 3897
    const/4 v0, 0x1

    #@e5
    goto :goto_6f

    #@e6
    .line 3925
    .restart local v2       #focused:Landroid/view/View;
    .restart local v3       #v:Landroid/view/View;
    :cond_e6
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@e8
    invoke-virtual {v4, v2, v0}, Landroid/view/View;->dispatchUnhandledMove(Landroid/view/View;I)Z

    #@eb
    move-result v4

    #@ec
    if-eqz v4, :cond_f3

    #@ee
    .line 3926
    invoke-direct {p0, p1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@f1
    goto/16 :goto_11

    #@f3
    .line 3934
    .end local v0           #direction:I
    .end local v2           #focused:Landroid/view/View;
    .end local v3           #v:Landroid/view/View;
    :cond_f3
    invoke-direct {p0, p1, v7}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@f6
    goto/16 :goto_11

    #@f8
    .line 3872
    :sswitch_data_f8
    .sparse-switch
        0x13 -> :sswitch_c4
        0x14 -> :sswitch_cd
        0x15 -> :sswitch_b2
        0x16 -> :sswitch_bb
        0x3d -> :sswitch_d6
    .end sparse-switch
.end method

.method private deliverPointerEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 11
    .parameter "q"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 3302
    iget-object v1, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@4
    check-cast v1, Landroid/view/MotionEvent;

    #@6
    .line 3303
    .local v1, event:Landroid/view/MotionEvent;
    invoke-virtual {v1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    #@9
    move-result v3

    #@a
    .line 3305
    .local v3, isTouchEvent:Z
    if-eqz v1, :cond_30

    #@c
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v4

    #@10
    const/4 v5, 0x2

    #@11
    if-eq v4, v5, :cond_30

    #@13
    .line 3306
    sget-boolean v4, Landroid/view/ViewRootImpl;->NOTUSER_DEBUG:Z

    #@15
    if-eqz v4, :cond_47

    #@17
    .line 3307
    const-string/jumbo v4, "touchCheck"

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "ViewRoot\'s Touch Event : "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 3319
    :cond_30
    :goto_30
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@32
    if-eqz v4, :cond_3b

    #@34
    .line 3320
    if-eqz v3, :cond_83

    #@36
    .line 3321
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@38
    invoke-virtual {v4, v1, v7}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    #@3b
    .line 3328
    :cond_3b
    :goto_3b
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3d
    if-eqz v4, :cond_43

    #@3f
    iget-boolean v4, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@41
    if-nez v4, :cond_89

    #@43
    .line 3329
    :cond_43
    invoke-direct {p0, p1, v7}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@46
    .line 3377
    :goto_46
    return-void

    #@47
    .line 3309
    :cond_47
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@4a
    move-result v4

    #@4b
    if-nez v4, :cond_56

    #@4d
    .line 3310
    const-string/jumbo v4, "touchCheck"

    #@50
    const-string v5, "ViewRoot\'s Touch Event : Touch Down"

    #@52
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_30

    #@56
    .line 3311
    :cond_56
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@59
    move-result v4

    #@5a
    if-ne v4, v8, :cond_65

    #@5c
    .line 3312
    const-string/jumbo v4, "touchCheck"

    #@5f
    const-string v5, "ViewRoot\'s Touch Event : Touch UP  "

    #@61
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_30

    #@65
    .line 3314
    :cond_65
    const-string/jumbo v4, "touchCheck"

    #@68
    new-instance v5, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v6, "ViewRoot\'s Touch Event :"

    #@6f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@76
    move-result v6

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_30

    #@83
    .line 3323
    :cond_83
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@85
    invoke-virtual {v4, v1, v7}, Landroid/view/InputEventConsistencyVerifier;->onGenericMotionEvent(Landroid/view/MotionEvent;I)V

    #@88
    goto :goto_3b

    #@89
    .line 3334
    :cond_89
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@8b
    if-eqz v4, :cond_92

    #@8d
    .line 3335
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@8f
    invoke-virtual {v4, v1}, Landroid/content/res/CompatibilityInfo$Translator;->translateEventInScreenToAppWindow(Landroid/view/MotionEvent;)V

    #@92
    .line 3339
    :cond_92
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    #@95
    move-result v0

    #@96
    .line 3340
    .local v0, action:I
    if-eqz v0, :cond_9c

    #@98
    const/16 v4, 0x8

    #@9a
    if-ne v0, v4, :cond_9f

    #@9c
    .line 3341
    :cond_9c
    invoke-virtual {p0, v8}, Landroid/view/ViewRootImpl;->ensureTouchMode(Z)Z

    #@9f
    .line 3345
    :cond_9f
    iget v4, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@a1
    if-eqz v4, :cond_aa

    #@a3
    .line 3346
    const/4 v4, 0x0

    #@a4
    iget v5, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@a6
    int-to-float v5, v5

    #@a7
    invoke-virtual {v1, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@aa
    .line 3353
    :cond_aa
    if-eqz v3, :cond_bc

    #@ac
    .line 3354
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mLastTouchPoint:Landroid/graphics/PointF;

    #@ae
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getRawX()F

    #@b1
    move-result v5

    #@b2
    iput v5, v4, Landroid/graphics/PointF;->x:F

    #@b4
    .line 3355
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mLastTouchPoint:Landroid/graphics/PointF;

    #@b6
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getRawY()F

    #@b9
    move-result v5

    #@ba
    iput v5, v4, Landroid/graphics/PointF;->y:F

    #@bc
    .line 3359
    :cond_bc
    const/4 v2, 0x0

    #@bd
    .line 3360
    .local v2, handled:Z
    iget-boolean v4, p0, Landroid/view/ViewRootImpl;->isFilterEnable:Z

    #@bf
    if-eqz v4, :cond_d2

    #@c1
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@c3
    if-eqz v4, :cond_d2

    #@c5
    .line 3361
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@c7
    invoke-virtual {v4, v1}, Lcom/lge/view/TouchEventFilter;->dispatchFilteredTouchEvent(Landroid/view/MotionEvent;)Z

    #@ca
    move-result v2

    #@cb
    .line 3370
    :goto_cb
    if-eqz v2, :cond_d9

    #@cd
    .line 3371
    invoke-direct {p0, p1, v8}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@d0
    goto/16 :goto_46

    #@d2
    .line 3363
    :cond_d2
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@d4
    invoke-virtual {v4, v1}, Landroid/view/View;->dispatchPointerEvent(Landroid/view/MotionEvent;)Z

    #@d7
    move-result v2

    #@d8
    goto :goto_cb

    #@d9
    .line 3376
    :cond_d9
    invoke-direct {p0, p1, v7}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@dc
    goto/16 :goto_46
.end method

.method private deliverTrackballEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 7
    .parameter "q"

    #@0
    .prologue
    .line 3380
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@2
    check-cast v0, Landroid/view/MotionEvent;

    #@4
    .line 3381
    .local v0, event:Landroid/view/MotionEvent;
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@6
    if-eqz v3, :cond_e

    #@8
    .line 3382
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@a
    const/4 v4, 0x0

    #@b
    invoke-virtual {v3, v0, v4}, Landroid/view/InputEventConsistencyVerifier;->onTrackballEvent(Landroid/view/MotionEvent;I)V

    #@e
    .line 3385
    :cond_e
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@10
    if-eqz v3, :cond_36

    #@12
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@14
    if-eqz v3, :cond_36

    #@16
    iget v3, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mFlags:I

    #@18
    and-int/lit8 v3, v3, 0x1

    #@1a
    if-nez v3, :cond_36

    #@1c
    .line 3391
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@1e
    if-eqz v3, :cond_36

    #@20
    .line 3392
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@23
    move-result-object v1

    #@24
    .line 3393
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_36

    #@26
    .line 3394
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getSequenceNumber()I

    #@29
    move-result v2

    #@2a
    .line 3398
    .local v2, seq:I
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2c
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v3

    #@30
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mInputMethodCallback:Landroid/view/ViewRootImpl$InputMethodCallback;

    #@32
    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/view/inputmethod/InputMethodManager;->dispatchTrackballEvent(Landroid/content/Context;ILandroid/view/MotionEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@35
    .line 3407
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v2           #seq:I
    :goto_35
    return-void

    #@36
    .line 3406
    :cond_36
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->deliverTrackballEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@39
    goto :goto_35
.end method

.method private deliverTrackballEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 35
    .parameter "q"

    #@0
    .prologue
    .line 3410
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@4
    move-object/from16 v27, v0

    #@6
    check-cast v27, Landroid/view/MotionEvent;

    #@8
    .line 3413
    .local v27, event:Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    #@a
    iget-object v2, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    if-eqz v2, :cond_14

    #@e
    move-object/from16 v0, p0

    #@10
    iget-boolean v2, v0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@12
    if-nez v2, :cond_1d

    #@14
    .line 3414
    :cond_14
    const/4 v2, 0x0

    #@15
    move-object/from16 v0, p0

    #@17
    move-object/from16 v1, p1

    #@19
    invoke-direct {v0, v1, v2}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@1c
    .line 3537
    :goto_1c
    return-void

    #@1d
    .line 3419
    :cond_1d
    move-object/from16 v0, p0

    #@1f
    iget-object v2, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@21
    move-object/from16 v0, v27

    #@23
    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_3f

    #@29
    .line 3424
    const/4 v2, 0x0

    #@2a
    move-object/from16 v0, p0

    #@2c
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl;->ensureTouchMode(Z)Z

    #@2f
    .line 3426
    const/4 v2, 0x1

    #@30
    move-object/from16 v0, p0

    #@32
    move-object/from16 v1, p1

    #@34
    invoke-direct {v0, v1, v2}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@37
    .line 3427
    const-wide/32 v5, -0x80000000

    #@3a
    move-object/from16 v0, p0

    #@3c
    iput-wide v5, v0, Landroid/view/ViewRootImpl;->mLastTrackballTime:J

    #@3e
    goto :goto_1c

    #@3f
    .line 3432
    :cond_3f
    move-object/from16 v0, p0

    #@41
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mTrackballAxisX:Landroid/view/ViewRootImpl$TrackballAxis;

    #@43
    move-object/from16 v29, v0

    #@45
    .line 3433
    .local v29, x:Landroid/view/ViewRootImpl$TrackballAxis;
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mTrackballAxisY:Landroid/view/ViewRootImpl$TrackballAxis;

    #@49
    move-object/from16 v31, v0

    #@4b
    .line 3435
    .local v31, y:Landroid/view/ViewRootImpl$TrackballAxis;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4e
    move-result-wide v3

    #@4f
    .line 3436
    .local v3, curTime:J
    move-object/from16 v0, p0

    #@51
    iget-wide v5, v0, Landroid/view/ViewRootImpl;->mLastTrackballTime:J

    #@53
    const-wide/16 v11, 0xfa

    #@55
    add-long/2addr v5, v11

    #@56
    cmp-long v2, v5, v3

    #@58
    if-gez v2, :cond_6a

    #@5a
    .line 3439
    const/4 v2, 0x0

    #@5b
    move-object/from16 v0, v29

    #@5d
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@60
    .line 3440
    const/4 v2, 0x0

    #@61
    move-object/from16 v0, v31

    #@63
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@66
    .line 3441
    move-object/from16 v0, p0

    #@68
    iput-wide v3, v0, Landroid/view/ViewRootImpl;->mLastTrackballTime:J

    #@6a
    .line 3444
    :cond_6a
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getAction()I

    #@6d
    move-result v26

    #@6e
    .line 3445
    .local v26, action:I
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getMetaState()I

    #@71
    move-result v10

    #@72
    .line 3446
    .local v10, metaState:I
    packed-switch v26, :pswitch_data_1ac

    #@75
    .line 3471
    :goto_75
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getX()F

    #@78
    move-result v2

    #@79
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getEventTime()J

    #@7c
    move-result-wide v5

    #@7d
    const-string v7, "X"

    #@7f
    move-object/from16 v0, v29

    #@81
    invoke-virtual {v0, v2, v5, v6, v7}, Landroid/view/ViewRootImpl$TrackballAxis;->collect(FJLjava/lang/String;)F

    #@84
    move-result v30

    #@85
    .line 3472
    .local v30, xOff:F
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getY()F

    #@88
    move-result v2

    #@89
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getEventTime()J

    #@8c
    move-result-wide v5

    #@8d
    const-string v7, "Y"

    #@8f
    move-object/from16 v0, v31

    #@91
    invoke-virtual {v0, v2, v5, v6, v7}, Landroid/view/ViewRootImpl$TrackballAxis;->collect(FJLjava/lang/String;)F

    #@94
    move-result v32

    #@95
    .line 3480
    .local v32, yOff:F
    const/4 v8, 0x0

    #@96
    .line 3481
    .local v8, keycode:I
    const/16 v28, 0x0

    #@98
    .line 3482
    .local v28, movement:I
    const/high16 v24, 0x3f80

    #@9a
    .line 3483
    .local v24, accel:F
    cmpl-float v2, v30, v32

    #@9c
    if-lez v2, :cond_175

    #@9e
    .line 3484
    const/high16 v2, 0x4000

    #@a0
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getXPrecision()F

    #@a3
    move-result v5

    #@a4
    div-float/2addr v2, v5

    #@a5
    move-object/from16 v0, v29

    #@a7
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->generate(F)I

    #@aa
    move-result v28

    #@ab
    .line 3485
    if-eqz v28, :cond_bd

    #@ad
    .line 3486
    if-lez v28, :cond_171

    #@af
    const/16 v8, 0x16

    #@b1
    .line 3488
    :goto_b1
    move-object/from16 v0, v29

    #@b3
    iget v0, v0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@b5
    move/from16 v24, v0

    #@b7
    .line 3489
    const/4 v2, 0x2

    #@b8
    move-object/from16 v0, v31

    #@ba
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@bd
    .line 3501
    :cond_bd
    :goto_bd
    if-eqz v8, :cond_1a2

    #@bf
    .line 3502
    if-gez v28, :cond_c6

    #@c1
    move/from16 v0, v28

    #@c3
    neg-int v0, v0

    #@c4
    move/from16 v28, v0

    #@c6
    .line 3503
    :cond_c6
    move/from16 v0, v28

    #@c8
    int-to-float v2, v0

    #@c9
    mul-float v2, v2, v24

    #@cb
    float-to-int v0, v2

    #@cc
    move/from16 v25, v0

    #@ce
    .line 3507
    .local v25, accelMovement:I
    move/from16 v0, v25

    #@d0
    move/from16 v1, v28

    #@d2
    if-le v0, v1, :cond_ea

    #@d4
    .line 3510
    add-int/lit8 v28, v28, -0x1

    #@d6
    .line 3511
    sub-int v9, v25, v28

    #@d8
    .line 3512
    .local v9, repeatCount:I
    new-instance v2, Landroid/view/KeyEvent;

    #@da
    const/4 v7, 0x2

    #@db
    const/4 v11, -0x1

    #@dc
    const/4 v12, 0x0

    #@dd
    const/16 v13, 0x400

    #@df
    const/16 v14, 0x101

    #@e1
    move-wide v5, v3

    #@e2
    invoke-direct/range {v2 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@e5
    move-object/from16 v0, p0

    #@e7
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@ea
    .line 3517
    .end local v9           #repeatCount:I
    :cond_ea
    :goto_ea
    if-lez v28, :cond_19e

    #@ec
    .line 3520
    add-int/lit8 v28, v28, -0x1

    #@ee
    .line 3521
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@f1
    move-result-wide v3

    #@f2
    .line 3522
    new-instance v11, Landroid/view/KeyEvent;

    #@f4
    const/16 v16, 0x0

    #@f6
    const/16 v18, 0x0

    #@f8
    const/16 v20, -0x1

    #@fa
    const/16 v21, 0x0

    #@fc
    const/16 v22, 0x400

    #@fe
    const/16 v23, 0x101

    #@100
    move-wide v12, v3

    #@101
    move-wide v14, v3

    #@102
    move/from16 v17, v8

    #@104
    move/from16 v19, v10

    #@106
    invoke-direct/range {v11 .. v23}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@109
    move-object/from16 v0, p0

    #@10b
    invoke-virtual {v0, v11}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@10e
    .line 3526
    new-instance v11, Landroid/view/KeyEvent;

    #@110
    const/16 v16, 0x1

    #@112
    const/16 v18, 0x0

    #@114
    const/16 v20, -0x1

    #@116
    const/16 v21, 0x0

    #@118
    const/16 v22, 0x400

    #@11a
    const/16 v23, 0x101

    #@11c
    move-wide v12, v3

    #@11d
    move-wide v14, v3

    #@11e
    move/from16 v17, v8

    #@120
    move/from16 v19, v10

    #@122
    invoke-direct/range {v11 .. v23}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@125
    move-object/from16 v0, p0

    #@127
    invoke-virtual {v0, v11}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@12a
    goto :goto_ea

    #@12b
    .line 3448
    .end local v8           #keycode:I
    .end local v24           #accel:F
    .end local v25           #accelMovement:I
    .end local v28           #movement:I
    .end local v30           #xOff:F
    .end local v32           #yOff:F
    :pswitch_12b
    const/4 v2, 0x2

    #@12c
    move-object/from16 v0, v29

    #@12e
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@131
    .line 3449
    const/4 v2, 0x2

    #@132
    move-object/from16 v0, v31

    #@134
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@137
    .line 3450
    new-instance v2, Landroid/view/KeyEvent;

    #@139
    const/4 v7, 0x0

    #@13a
    const/16 v8, 0x17

    #@13c
    const/4 v9, 0x0

    #@13d
    const/4 v11, -0x1

    #@13e
    const/4 v12, 0x0

    #@13f
    const/16 v13, 0x400

    #@141
    const/16 v14, 0x101

    #@143
    move-wide v5, v3

    #@144
    invoke-direct/range {v2 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@147
    move-object/from16 v0, p0

    #@149
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@14c
    goto/16 :goto_75

    #@14e
    .line 3456
    :pswitch_14e
    const/4 v2, 0x2

    #@14f
    move-object/from16 v0, v29

    #@151
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@154
    .line 3457
    const/4 v2, 0x2

    #@155
    move-object/from16 v0, v31

    #@157
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@15a
    .line 3458
    new-instance v2, Landroid/view/KeyEvent;

    #@15c
    const/4 v7, 0x1

    #@15d
    const/16 v8, 0x17

    #@15f
    const/4 v9, 0x0

    #@160
    const/4 v11, -0x1

    #@161
    const/4 v12, 0x0

    #@162
    const/16 v13, 0x400

    #@164
    const/16 v14, 0x101

    #@166
    move-wide v5, v3

    #@167
    invoke-direct/range {v2 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@16a
    move-object/from16 v0, p0

    #@16c
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@16f
    goto/16 :goto_75

    #@171
    .line 3486
    .restart local v8       #keycode:I
    .restart local v24       #accel:F
    .restart local v28       #movement:I
    .restart local v30       #xOff:F
    .restart local v32       #yOff:F
    :cond_171
    const/16 v8, 0x15

    #@173
    goto/16 :goto_b1

    #@175
    .line 3491
    :cond_175
    const/4 v2, 0x0

    #@176
    cmpl-float v2, v32, v2

    #@178
    if-lez v2, :cond_bd

    #@17a
    .line 3492
    const/high16 v2, 0x4000

    #@17c
    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->getYPrecision()F

    #@17f
    move-result v5

    #@180
    div-float/2addr v2, v5

    #@181
    move-object/from16 v0, v31

    #@183
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->generate(F)I

    #@186
    move-result v28

    #@187
    .line 3493
    if-eqz v28, :cond_bd

    #@189
    .line 3494
    if-lez v28, :cond_19b

    #@18b
    const/16 v8, 0x14

    #@18d
    .line 3496
    :goto_18d
    move-object/from16 v0, v31

    #@18f
    iget v0, v0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@191
    move/from16 v24, v0

    #@193
    .line 3497
    const/4 v2, 0x2

    #@194
    move-object/from16 v0, v29

    #@196
    invoke-virtual {v0, v2}, Landroid/view/ViewRootImpl$TrackballAxis;->reset(I)V

    #@199
    goto/16 :goto_bd

    #@19b
    .line 3494
    :cond_19b
    const/16 v8, 0x13

    #@19d
    goto :goto_18d

    #@19e
    .line 3531
    .restart local v25       #accelMovement:I
    :cond_19e
    move-object/from16 v0, p0

    #@1a0
    iput-wide v3, v0, Landroid/view/ViewRootImpl;->mLastTrackballTime:J

    #@1a2
    .line 3536
    .end local v25           #accelMovement:I
    :cond_1a2
    const/4 v2, 0x1

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    move-object/from16 v1, p1

    #@1a7
    invoke-direct {v0, v1, v2}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@1aa
    goto/16 :goto_1c

    #@1ac
    .line 3446
    :pswitch_data_1ac
    .packed-switch 0x0
        :pswitch_12b
        :pswitch_14e
    .end packed-switch
.end method

.method private destroyHardwareRenderer()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4337
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    .line 4338
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@5
    .line 4340
    .local v1, hardwareRenderer:Landroid/view/HardwareRenderer;
    if-eqz v1, :cond_1c

    #@7
    .line 4341
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@9
    if-eqz v2, :cond_10

    #@b
    .line 4342
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@d
    invoke-virtual {v1, v2}, Landroid/view/HardwareRenderer;->destroyHardwareResources(Landroid/view/View;)V

    #@10
    .line 4344
    :cond_10
    const/4 v2, 0x1

    #@11
    invoke-virtual {v1, v2}, Landroid/view/HardwareRenderer;->destroy(Z)V

    #@14
    .line 4345
    invoke-virtual {v1, v3}, Landroid/view/HardwareRenderer;->setRequested(Z)V

    #@17
    .line 4347
    const/4 v2, 0x0

    #@18
    iput-object v2, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1a
    .line 4348
    iput-boolean v3, v0, Landroid/view/View$AttachInfo;->mHardwareAccelerated:Z

    #@1c
    .line 4350
    :cond_1c
    return-void
.end method

.method private draw(Z)V
    .registers 24
    .parameter "fullRedrawNeeded"

    #@0
    .prologue
    .line 2183
    move-object/from16 v0, p0

    #@2
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@4
    .line 2184
    .local v5, surface:Landroid/view/Surface;
    if-eqz v5, :cond_c

    #@6
    invoke-virtual {v5}, Landroid/view/Surface;->isValid()Z

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_d

    #@c
    .line 2292
    :cond_c
    :goto_c
    return-void

    #@d
    .line 2188
    :cond_d
    move-object/from16 v0, p0

    #@f
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->DEBUG_FPS_FROM_BUILD_PROPORTY:Z

    #@11
    if-eqz v4, :cond_16

    #@13
    .line 2189
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewRootImpl;->trackFPS()V

    #@16
    .line 2192
    :cond_16
    sget-boolean v4, Landroid/view/ViewRootImpl;->sFirstDrawComplete:Z

    #@18
    if-nez v4, :cond_45

    #@1a
    .line 2193
    sget-object v18, Landroid/view/ViewRootImpl;->sFirstDrawHandlers:Ljava/util/ArrayList;

    #@1c
    monitor-enter v18

    #@1d
    .line 2194
    const/4 v4, 0x1

    #@1e
    :try_start_1e
    sput-boolean v4, Landroid/view/ViewRootImpl;->sFirstDrawComplete:Z

    #@20
    .line 2195
    sget-object v4, Landroid/view/ViewRootImpl;->sFirstDrawHandlers:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@25
    move-result v13

    #@26
    .line 2196
    .local v13, count:I
    const/16 v16, 0x0

    #@28
    .local v16, i:I
    :goto_28
    move/from16 v0, v16

    #@2a
    if-ge v0, v13, :cond_44

    #@2c
    .line 2197
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@30
    move-object/from16 v19, v0

    #@32
    sget-object v4, Landroid/view/ViewRootImpl;->sFirstDrawHandlers:Ljava/util/ArrayList;

    #@34
    move/from16 v0, v16

    #@36
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v4

    #@3a
    check-cast v4, Ljava/lang/Runnable;

    #@3c
    move-object/from16 v0, v19

    #@3e
    invoke-virtual {v0, v4}, Landroid/view/ViewRootImpl$ViewRootHandler;->post(Ljava/lang/Runnable;)Z

    #@41
    .line 2196
    add-int/lit8 v16, v16, 0x1

    #@43
    goto :goto_28

    #@44
    .line 2199
    :cond_44
    monitor-exit v18
    :try_end_45
    .catchall {:try_start_1e .. :try_end_45} :catchall_e7

    #@45
    .line 2202
    .end local v13           #count:I
    .end local v16           #i:I
    :cond_45
    const/4 v4, 0x0

    #@46
    const/16 v18, 0x0

    #@48
    move-object/from16 v0, p0

    #@4a
    move/from16 v1, v18

    #@4c
    invoke-virtual {v0, v4, v1}, Landroid/view/ViewRootImpl;->scrollToRectOrFocus(Landroid/graphics/Rect;Z)Z

    #@4f
    .line 2204
    move-object/from16 v0, p0

    #@51
    iget-object v6, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@53
    .line 2205
    .local v6, attachInfo:Landroid/view/View$AttachInfo;
    iget-boolean v4, v6, Landroid/view/View$AttachInfo;->mViewScrollChanged:Z

    #@55
    if-eqz v4, :cond_5f

    #@57
    .line 2206
    const/4 v4, 0x0

    #@58
    iput-boolean v4, v6, Landroid/view/View$AttachInfo;->mViewScrollChanged:Z

    #@5a
    .line 2207
    iget-object v4, v6, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@5c
    invoke-virtual {v4}, Landroid/view/ViewTreeObserver;->dispatchOnScrollChanged()V

    #@5f
    .line 2211
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@63
    if-eqz v4, :cond_ea

    #@65
    move-object/from16 v0, p0

    #@67
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@69
    invoke-virtual {v4}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@6c
    move-result v4

    #@6d
    if-eqz v4, :cond_ea

    #@6f
    const/4 v11, 0x1

    #@70
    .line 2212
    .local v11, animating:Z
    :goto_70
    if-eqz v11, :cond_ec

    #@72
    .line 2213
    move-object/from16 v0, p0

    #@74
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@76
    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    #@79
    move-result v7

    #@7a
    .line 2217
    .local v7, yoff:I
    :goto_7a
    move-object/from16 v0, p0

    #@7c
    iget v4, v0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@7e
    if-eq v4, v7, :cond_86

    #@80
    .line 2218
    move-object/from16 v0, p0

    #@82
    iput v7, v0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@84
    .line 2219
    const/16 p1, 0x1

    #@86
    .line 2222
    :cond_86
    iget v12, v6, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@88
    .line 2223
    .local v12, appScale:F
    iget-boolean v8, v6, Landroid/view/View$AttachInfo;->mScalingRequired:Z

    #@8a
    .line 2225
    .local v8, scalingRequired:Z
    const/16 v17, 0x0

    #@8c
    .line 2226
    .local v17, resizeAlpha:I
    move-object/from16 v0, p0

    #@8e
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@90
    if-eqz v4, :cond_c6

    #@92
    .line 2227
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@95
    move-result-wide v18

    #@96
    move-object/from16 v0, p0

    #@98
    iget-wide v0, v0, Landroid/view/ViewRootImpl;->mResizeBufferStartTime:J

    #@9a
    move-wide/from16 v20, v0

    #@9c
    sub-long v14, v18, v20

    #@9e
    .line 2228
    .local v14, deltaTime:J
    move-object/from16 v0, p0

    #@a0
    iget v4, v0, Landroid/view/ViewRootImpl;->mResizeBufferDuration:I

    #@a2
    int-to-long v0, v4

    #@a3
    move-wide/from16 v18, v0

    #@a5
    cmp-long v4, v14, v18

    #@a7
    if-gez v4, :cond_f1

    #@a9
    .line 2229
    long-to-float v4, v14

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget v0, v0, Landroid/view/ViewRootImpl;->mResizeBufferDuration:I

    #@ae
    move/from16 v18, v0

    #@b0
    move/from16 v0, v18

    #@b2
    int-to-float v0, v0

    #@b3
    move/from16 v18, v0

    #@b5
    div-float v10, v4, v18

    #@b7
    .line 2230
    .local v10, amt:F
    sget-object v4, Landroid/view/ViewRootImpl;->mResizeInterpolator:Landroid/view/animation/Interpolator;

    #@b9
    invoke-interface {v4, v10}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@bc
    move-result v10

    #@bd
    .line 2231
    const/4 v11, 0x1

    #@be
    .line 2232
    const/high16 v4, 0x437f

    #@c0
    mul-float/2addr v4, v10

    #@c1
    float-to-int v4, v4

    #@c2
    rsub-int v0, v4, 0xff

    #@c4
    move/from16 v17, v0

    #@c6
    .line 2238
    .end local v10           #amt:F
    .end local v14           #deltaTime:J
    :cond_c6
    :goto_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v9, v0, Landroid/view/ViewRootImpl;->mDirty:Landroid/graphics/Rect;

    #@ca
    .line 2239
    .local v9, dirty:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@cc
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@ce
    if-eqz v4, :cond_f5

    #@d0
    .line 2241
    invoke-virtual {v9}, Landroid/graphics/Rect;->setEmpty()V

    #@d3
    .line 2242
    if-eqz v11, :cond_c

    #@d5
    .line 2243
    move-object/from16 v0, p0

    #@d7
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@d9
    if-eqz v4, :cond_e2

    #@db
    .line 2244
    move-object/from16 v0, p0

    #@dd
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@df
    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    #@e2
    .line 2246
    :cond_e2
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->disposeResizeBuffer()V

    #@e5
    goto/16 :goto_c

    #@e7
    .line 2199
    .end local v6           #attachInfo:Landroid/view/View$AttachInfo;
    .end local v7           #yoff:I
    .end local v8           #scalingRequired:Z
    .end local v9           #dirty:Landroid/graphics/Rect;
    .end local v11           #animating:Z
    .end local v12           #appScale:F
    .end local v17           #resizeAlpha:I
    :catchall_e7
    move-exception v4

    #@e8
    :try_start_e8
    monitor-exit v18
    :try_end_e9
    .catchall {:try_start_e8 .. :try_end_e9} :catchall_e7

    #@e9
    throw v4

    #@ea
    .line 2211
    .restart local v6       #attachInfo:Landroid/view/View$AttachInfo;
    :cond_ea
    const/4 v11, 0x0

    #@eb
    goto :goto_70

    #@ec
    .line 2215
    .restart local v11       #animating:Z
    :cond_ec
    move-object/from16 v0, p0

    #@ee
    iget v7, v0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@f0
    .restart local v7       #yoff:I
    goto :goto_7a

    #@f1
    .line 2234
    .restart local v8       #scalingRequired:Z
    .restart local v12       #appScale:F
    .restart local v14       #deltaTime:J
    .restart local v17       #resizeAlpha:I
    :cond_f1
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->disposeResizeBuffer()V

    #@f4
    goto :goto_c6

    #@f5
    .line 2251
    .end local v14           #deltaTime:J
    .restart local v9       #dirty:Landroid/graphics/Rect;
    :cond_f5
    if-eqz p1, :cond_132

    #@f7
    .line 2252
    const/4 v4, 0x1

    #@f8
    iput-boolean v4, v6, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@fa
    .line 2253
    const/4 v4, 0x0

    #@fb
    const/16 v18, 0x0

    #@fd
    move-object/from16 v0, p0

    #@ff
    iget v0, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@101
    move/from16 v19, v0

    #@103
    move/from16 v0, v19

    #@105
    int-to-float v0, v0

    #@106
    move/from16 v19, v0

    #@108
    mul-float v19, v19, v12

    #@10a
    const/high16 v20, 0x3f00

    #@10c
    add-float v19, v19, v20

    #@10e
    move/from16 v0, v19

    #@110
    float-to-int v0, v0

    #@111
    move/from16 v19, v0

    #@113
    move-object/from16 v0, p0

    #@115
    iget v0, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@117
    move/from16 v20, v0

    #@119
    move/from16 v0, v20

    #@11b
    int-to-float v0, v0

    #@11c
    move/from16 v20, v0

    #@11e
    mul-float v20, v20, v12

    #@120
    const/high16 v21, 0x3f00

    #@122
    add-float v20, v20, v21

    #@124
    move/from16 v0, v20

    #@126
    float-to-int v0, v0

    #@127
    move/from16 v20, v0

    #@129
    move/from16 v0, v18

    #@12b
    move/from16 v1, v19

    #@12d
    move/from16 v2, v20

    #@12f
    invoke-virtual {v9, v4, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@132
    .line 2265
    :cond_132
    iget-object v4, v6, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@134
    invoke-virtual {v4}, Landroid/view/ViewTreeObserver;->dispatchOnDraw()V

    #@137
    .line 2267
    invoke-virtual {v9}, Landroid/graphics/Rect;->isEmpty()Z

    #@13a
    move-result v4

    #@13b
    if-eqz v4, :cond_143

    #@13d
    move-object/from16 v0, p0

    #@13f
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@141
    if-eqz v4, :cond_1b6

    #@143
    .line 2268
    :cond_143
    iget-object v4, v6, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@145
    if-eqz v4, :cond_1c7

    #@147
    iget-object v4, v6, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@149
    invoke-virtual {v4}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@14c
    move-result v4

    #@14d
    if-eqz v4, :cond_1c7

    #@14f
    .line 2270
    const/4 v4, 0x0

    #@150
    move-object/from16 v0, p0

    #@152
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@154
    .line 2271
    move-object/from16 v0, p0

    #@156
    iput v7, v0, Landroid/view/ViewRootImpl;->mHardwareYOffset:I

    #@158
    .line 2272
    move/from16 v0, v17

    #@15a
    move-object/from16 v1, p0

    #@15c
    iput v0, v1, Landroid/view/ViewRootImpl;->mResizeAlpha:I

    #@15e
    .line 2274
    move-object/from16 v0, p0

    #@160
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mCurrentDirty:Landroid/graphics/Rect;

    #@162
    invoke-virtual {v4, v9}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@165
    .line 2275
    move-object/from16 v0, p0

    #@167
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mCurrentDirty:Landroid/graphics/Rect;

    #@169
    move-object/from16 v0, p0

    #@16b
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mPreviousDirty:Landroid/graphics/Rect;

    #@16d
    move-object/from16 v18, v0

    #@16f
    move-object/from16 v0, v18

    #@171
    invoke-virtual {v4, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    #@174
    .line 2276
    move-object/from16 v0, p0

    #@176
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPreviousDirty:Landroid/graphics/Rect;

    #@178
    invoke-virtual {v4, v9}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@17b
    .line 2277
    invoke-virtual {v9}, Landroid/graphics/Rect;->setEmpty()V

    #@17e
    .line 2279
    iget-object v0, v6, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@180
    move-object/from16 v18, v0

    #@182
    move-object/from16 v0, p0

    #@184
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@186
    move-object/from16 v19, v0

    #@188
    if-eqz v11, :cond_1c2

    #@18a
    const/4 v4, 0x0

    #@18b
    :goto_18b
    move-object/from16 v0, v18

    #@18d
    move-object/from16 v1, v19

    #@18f
    move-object/from16 v2, p0

    #@191
    invoke-virtual {v0, v1, v6, v2, v4}, Landroid/view/HardwareRenderer;->draw(Landroid/view/View;Landroid/view/View$AttachInfo;Landroid/view/HardwareRenderer$HardwareDrawCallbacks;Landroid/graphics/Rect;)Z

    #@194
    move-result v4

    #@195
    if-eqz v4, :cond_1b6

    #@197
    .line 2281
    move-object/from16 v0, p0

    #@199
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPreviousDirty:Landroid/graphics/Rect;

    #@19b
    const/16 v18, 0x0

    #@19d
    const/16 v19, 0x0

    #@19f
    move-object/from16 v0, p0

    #@1a1
    iget v0, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@1a3
    move/from16 v20, v0

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iget v0, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@1a9
    move/from16 v21, v0

    #@1ab
    move/from16 v0, v18

    #@1ad
    move/from16 v1, v19

    #@1af
    move/from16 v2, v20

    #@1b1
    move/from16 v3, v21

    #@1b3
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@1b6
    .line 2288
    :cond_1b6
    if-eqz v11, :cond_c

    #@1b8
    .line 2289
    const/4 v4, 0x1

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@1bd
    .line 2290
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@1c0
    goto/16 :goto_c

    #@1c2
    .line 2279
    :cond_1c2
    move-object/from16 v0, p0

    #@1c4
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mCurrentDirty:Landroid/graphics/Rect;

    #@1c6
    goto :goto_18b

    #@1c7
    :cond_1c7
    move-object/from16 v4, p0

    #@1c9
    .line 2283
    invoke-direct/range {v4 .. v9}, Landroid/view/ViewRootImpl;->drawSoftware(Landroid/view/Surface;Landroid/view/View$AttachInfo;IZLandroid/graphics/Rect;)Z

    #@1cc
    move-result v4

    #@1cd
    if-nez v4, :cond_1b6

    #@1cf
    goto/16 :goto_c
.end method

.method private drawAccessibilityFocusedDrawableIfNeeded(Landroid/graphics/Canvas;)V
    .registers 9
    .parameter "canvas"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2424
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    iget-object v4, v4, Landroid/view/View;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@8
    move-result-object v2

    #@9
    .line 2425
    .local v2, manager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_15

    #@f
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@12
    move-result v4

    #@13
    if-nez v4, :cond_16

    #@15
    .line 2450
    :cond_15
    :goto_15
    return-void

    #@16
    .line 2428
    :cond_16
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@18
    if-eqz v4, :cond_15

    #@1a
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@1c
    iget-object v4, v4, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1e
    if-eqz v4, :cond_15

    #@20
    .line 2431
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->getAccessibilityFocusedDrawable()Landroid/graphics/drawable/Drawable;

    #@23
    move-result-object v1

    #@24
    .line 2432
    .local v1, drawable:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_15

    #@26
    .line 2435
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@28
    invoke-virtual {v4}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@2b
    move-result-object v3

    #@2c
    .line 2437
    .local v3, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2e
    iget-object v4, v4, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@30
    iget-object v0, v4, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@32
    .line 2438
    .local v0, bounds:Landroid/graphics/Rect;
    if-nez v3, :cond_5c

    #@34
    .line 2439
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@36
    invoke-virtual {v4, v0}, Landroid/view/View;->getBoundsOnScreen(Landroid/graphics/Rect;)V

    #@39
    .line 2446
    :goto_39
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3b
    iget v4, v4, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@3d
    neg-int v4, v4

    #@3e
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    iget v5, v5, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@42
    neg-int v5, v5

    #@43
    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    #@46
    .line 2447
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@48
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@4a
    iget v4, v4, Landroid/view/ViewRootImpl;->mWidth:I

    #@4c
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4e
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@50
    iget v5, v5, Landroid/view/ViewRootImpl;->mHeight:I

    #@52
    invoke-virtual {v0, v6, v6, v4, v5}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@55
    .line 2448
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@58
    .line 2449
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@5b
    goto :goto_15

    #@5c
    .line 2441
    :cond_5c
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@5e
    if-eqz v4, :cond_15

    #@60
    .line 2444
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@62
    invoke-virtual {v4, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    #@65
    goto :goto_39
.end method

.method private drawSoftware(Landroid/view/Surface;Landroid/view/View$AttachInfo;IZLandroid/graphics/Rect;)Z
    .registers 16
    .parameter "surface"
    .parameter "attachInfo"
    .parameter "yoff"
    .parameter "scalingRequired"
    .parameter "dirty"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2305
    iget-object v8, p2, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    if-eqz v8, :cond_1c

    #@6
    iget-object v8, p2, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8
    invoke-virtual {v8}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@b
    move-result v8

    #@c
    if-nez v8, :cond_1c

    #@e
    iget-object v8, p2, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@10
    invoke-virtual {v8}, Landroid/view/HardwareRenderer;->isRequested()Z

    #@13
    move-result v8

    #@14
    if-eqz v8, :cond_1c

    #@16
    .line 2307
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@18
    .line 2308
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@1b
    .line 2410
    :goto_1b
    return v6

    #@1c
    .line 2315
    :cond_1c
    :try_start_1c
    iget v3, p5, Landroid/graphics/Rect;->left:I

    #@1e
    .line 2316
    .local v3, left:I
    iget v5, p5, Landroid/graphics/Rect;->top:I

    #@20
    .line 2317
    .local v5, top:I
    iget v4, p5, Landroid/graphics/Rect;->right:I

    #@22
    .line 2318
    .local v4, right:I
    iget v0, p5, Landroid/graphics/Rect;->bottom:I

    #@24
    .line 2320
    .local v0, bottom:I
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@26
    invoke-virtual {v8, p5}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@29
    move-result-object v1

    #@2a
    .line 2322
    .local v1, canvas:Landroid/graphics/Canvas;
    iget v8, p5, Landroid/graphics/Rect;->left:I

    #@2c
    if-ne v3, v8, :cond_3a

    #@2e
    iget v8, p5, Landroid/graphics/Rect;->top:I

    #@30
    if-ne v5, v8, :cond_3a

    #@32
    iget v8, p5, Landroid/graphics/Rect;->right:I

    #@34
    if-ne v4, v8, :cond_3a

    #@36
    iget v8, p5, Landroid/graphics/Rect;->bottom:I

    #@38
    if-eq v0, v8, :cond_3d

    #@3a
    .line 2324
    :cond_3a
    const/4 v8, 0x1

    #@3b
    iput-boolean v8, p2, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@3d
    .line 2328
    :cond_3d
    iget v8, p0, Landroid/view/ViewRootImpl;->mDensity:I

    #@3f
    invoke-virtual {v1, v8}, Landroid/graphics/Canvas;->setDensity(I)V
    :try_end_42
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_1c .. :try_end_42} :catch_91
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1c .. :try_end_42} :catch_b5

    #@42
    .line 2364
    :try_start_42
    invoke-virtual {v1}, Landroid/graphics/Canvas;->isOpaque()Z

    #@45
    move-result v8

    #@46
    if-eqz v8, :cond_4a

    #@48
    if-eqz p3, :cond_50

    #@4a
    .line 2365
    :cond_4a
    const/4 v8, 0x0

    #@4b
    sget-object v9, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@4d
    invoke-virtual {v1, v8, v9}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@50
    .line 2368
    :cond_50
    invoke-virtual {p5}, Landroid/graphics/Rect;->setEmpty()V

    #@53
    .line 2369
    const/4 v8, 0x0

    #@54
    iput-boolean v8, p0, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@56
    .line 2370
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@59
    move-result-wide v8

    #@5a
    iput-wide v8, p2, Landroid/view/View$AttachInfo;->mDrawingTime:J

    #@5c
    .line 2371
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5e
    iget v9, v8, Landroid/view/View;->mPrivateFlags:I

    #@60
    or-int/lit8 v9, v9, 0x20

    #@62
    iput v9, v8, Landroid/view/View;->mPrivateFlags:I
    :try_end_64
    .catchall {:try_start_42 .. :try_end_64} :catchall_cc

    #@64
    .line 2380
    const/4 v8, 0x0

    #@65
    neg-int v9, p3

    #@66
    int-to-float v9, v9

    #@67
    :try_start_67
    invoke-virtual {v1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    #@6a
    .line 2381
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@6c
    if-eqz v8, :cond_73

    #@6e
    .line 2382
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@70
    invoke-virtual {v8, v1}, Landroid/content/res/CompatibilityInfo$Translator;->translateCanvas(Landroid/graphics/Canvas;)V

    #@73
    .line 2384
    :cond_73
    if-eqz p4, :cond_c1

    #@75
    iget v8, p0, Landroid/view/ViewRootImpl;->mNoncompatDensity:I

    #@77
    :goto_77
    invoke-virtual {v1, v8}, Landroid/graphics/Canvas;->setScreenDensity(I)V

    #@7a
    .line 2385
    const/4 v8, 0x0

    #@7b
    iput-boolean v8, p2, Landroid/view/View$AttachInfo;->mSetIgnoreDirtyState:Z

    #@7d
    .line 2387
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@7f
    invoke-virtual {v8, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    #@82
    .line 2389
    invoke-direct {p0, v1}, Landroid/view/ViewRootImpl;->drawAccessibilityFocusedDrawableIfNeeded(Landroid/graphics/Canvas;)V
    :try_end_85
    .catchall {:try_start_67 .. :try_end_85} :catchall_c3

    #@85
    .line 2391
    :try_start_85
    iget-boolean v8, p2, Landroid/view/View$AttachInfo;->mSetIgnoreDirtyState:Z

    #@87
    if-nez v8, :cond_8c

    #@89
    .line 2393
    const/4 v8, 0x0

    #@8a
    iput-boolean v8, p2, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z
    :try_end_8c
    .catchall {:try_start_85 .. :try_end_8c} :catchall_cc

    #@8c
    .line 2398
    :cond_8c
    :try_start_8c
    invoke-virtual {p1, v1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_8f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8c .. :try_end_8f} :catch_d1

    #@8f
    move v6, v7

    #@90
    .line 2410
    goto :goto_1b

    #@91
    .line 2329
    .end local v0           #bottom:I
    .end local v1           #canvas:Landroid/graphics/Canvas;
    .end local v3           #left:I
    .end local v4           #right:I
    .end local v5           #top:I
    :catch_91
    move-exception v2

    #@92
    .line 2330
    .local v2, e:Landroid/view/Surface$OutOfResourcesException;
    const-string v8, "ViewRootImpl"

    #@94
    const-string v9, "OutOfResourcesException locking surface"

    #@96
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@99
    .line 2332
    :try_start_99
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@9b
    iget-object v9, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@9d
    invoke-interface {v8, v9}, Landroid/view/IWindowSession;->outOfMemory(Landroid/view/IWindow;)Z

    #@a0
    move-result v8

    #@a1
    if-nez v8, :cond_b1

    #@a3
    .line 2333
    const-string v8, "ViewRootImpl"

    #@a5
    const-string v9, "No processes killed for memory; killing self"

    #@a7
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 2334
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@ad
    move-result v8

    #@ae
    invoke-static {v8}, Landroid/os/Process;->killProcess(I)V
    :try_end_b1
    .catch Landroid/os/RemoteException; {:try_start_99 .. :try_end_b1} :catch_e9

    #@b1
    .line 2338
    :cond_b1
    :goto_b1
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@b3
    goto/16 :goto_1b

    #@b5
    .line 2340
    .end local v2           #e:Landroid/view/Surface$OutOfResourcesException;
    :catch_b5
    move-exception v2

    #@b6
    .line 2341
    .local v2, e:Ljava/lang/IllegalArgumentException;
    const-string v8, "ViewRootImpl"

    #@b8
    const-string v9, "Could not lock surface"

    #@ba
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@bd
    .line 2345
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@bf
    goto/16 :goto_1b

    #@c1
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    .restart local v0       #bottom:I
    .restart local v1       #canvas:Landroid/graphics/Canvas;
    .restart local v3       #left:I
    .restart local v4       #right:I
    .restart local v5       #top:I
    :cond_c1
    move v8, v6

    #@c2
    .line 2384
    goto :goto_77

    #@c3
    .line 2391
    :catchall_c3
    move-exception v8

    #@c4
    :try_start_c4
    iget-boolean v9, p2, Landroid/view/View$AttachInfo;->mSetIgnoreDirtyState:Z

    #@c6
    if-nez v9, :cond_cb

    #@c8
    .line 2393
    const/4 v9, 0x0

    #@c9
    iput-boolean v9, p2, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@cb
    :cond_cb
    throw v8
    :try_end_cc
    .catchall {:try_start_c4 .. :try_end_cc} :catchall_cc

    #@cc
    .line 2397
    :catchall_cc
    move-exception v8

    #@cd
    .line 2398
    :try_start_cd
    invoke-virtual {p1, v1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_d0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_cd .. :try_end_d0} :catch_dd

    #@d0
    .line 2403
    throw v8

    #@d1
    .line 2399
    :catch_d1
    move-exception v2

    #@d2
    .line 2400
    .restart local v2       #e:Ljava/lang/IllegalArgumentException;
    const-string v8, "ViewRootImpl"

    #@d4
    const-string v9, "Could not unlock surface"

    #@d6
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d9
    .line 2401
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@db
    goto/16 :goto_1b

    #@dd
    .line 2399
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_dd
    move-exception v2

    #@de
    .line 2400
    .restart local v2       #e:Ljava/lang/IllegalArgumentException;
    const-string v8, "ViewRootImpl"

    #@e0
    const-string v9, "Could not unlock surface"

    #@e2
    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e5
    .line 2401
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@e7
    goto/16 :goto_1b

    #@e9
    .line 2336
    .end local v0           #bottom:I
    .end local v1           #canvas:Landroid/graphics/Canvas;
    .end local v3           #left:I
    .end local v4           #right:I
    .end local v5           #top:I
    .local v2, e:Landroid/view/Surface$OutOfResourcesException;
    :catch_e9
    move-exception v8

    #@ea
    goto :goto_b1
.end method

.method private enableHardwareAcceleration(Landroid/content/Context;Landroid/view/WindowManager$LayoutParams;)V
    .registers 13
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 736
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    iput-boolean v6, v7, Landroid/view/View$AttachInfo;->mHardwareAccelerated:Z

    #@6
    .line 737
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iput-boolean v6, v7, Landroid/view/View$AttachInfo;->mHardwareAccelerationRequested:Z

    #@a
    .line 740
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@c
    if-eqz v7, :cond_f

    #@e
    .line 801
    :cond_e
    :goto_e
    return-void

    #@f
    .line 743
    :cond_f
    iget v7, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@11
    const/high16 v8, 0x100

    #@13
    and-int/2addr v7, v8

    #@14
    if-eqz v7, :cond_4d

    #@16
    move v2, v5

    #@17
    .line 746
    .local v2, hardwareAccelerated:Z
    :goto_17
    if-eqz v2, :cond_e

    #@19
    .line 747
    invoke-static {}, Landroid/view/HardwareRenderer;->isAvailable()Z

    #@1c
    move-result v7

    #@1d
    if-eqz v7, :cond_e

    #@1f
    .line 760
    iget v7, p2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@21
    and-int/lit8 v7, v7, 0x1

    #@23
    if-eqz v7, :cond_4f

    #@25
    move v0, v5

    #@26
    .line 762
    .local v0, fakeHwAccelerated:Z
    :goto_26
    iget v7, p2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@28
    and-int/lit8 v7, v7, 0x2

    #@2a
    if-eqz v7, :cond_51

    #@2c
    move v1, v5

    #@2d
    .line 765
    .local v1, forceHwAccelerated:Z
    :goto_2d
    sget-boolean v7, Landroid/view/HardwareRenderer;->sRendererDisabled:Z

    #@2f
    if-eqz v7, :cond_37

    #@31
    sget-boolean v7, Landroid/view/HardwareRenderer;->sSystemRendererDisabled:Z

    #@33
    if-eqz v7, :cond_a3

    #@35
    if-eqz v1, :cond_a3

    #@37
    .line 768
    :cond_37
    sget-boolean v7, Landroid/view/HardwareRenderer;->sSystemRendererDisabled:Z

    #@39
    if-nez v7, :cond_53

    #@3b
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@3e
    move-result-object v7

    #@3f
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@42
    move-result-object v8

    #@43
    if-eq v7, v8, :cond_53

    #@45
    .line 770
    const-string v5, "HardwareRenderer"

    #@47
    const-string v6, "Attempting to initialize hardware acceleration outside of the main thread, aborting"

    #@49
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_e

    #@4d
    .end local v0           #fakeHwAccelerated:Z
    .end local v1           #forceHwAccelerated:Z
    .end local v2           #hardwareAccelerated:Z
    :cond_4d
    move v2, v6

    #@4e
    .line 743
    goto :goto_17

    #@4f
    .restart local v2       #hardwareAccelerated:Z
    :cond_4f
    move v0, v6

    #@50
    .line 760
    goto :goto_26

    #@51
    .restart local v0       #fakeHwAccelerated:Z
    :cond_51
    move v1, v6

    #@52
    .line 762
    goto :goto_2d

    #@53
    .line 775
    .restart local v1       #forceHwAccelerated:Z
    :cond_53
    invoke-static {p1}, Landroid/view/ViewRootImpl;->isRenderThreadRequested(Landroid/content/Context;)Z

    #@56
    move-result v3

    #@57
    .line 776
    .local v3, renderThread:Z
    if-eqz v3, :cond_60

    #@59
    .line 777
    const-string v7, "HardwareRenderer"

    #@5b
    const-string v8, "Render threat initiated"

    #@5d
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 780
    :cond_60
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@62
    iget-object v7, v7, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@64
    if-eqz v7, :cond_80

    #@66
    .line 781
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@68
    iget-object v7, v7, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@6a
    invoke-virtual {v7}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@6d
    move-result v7

    #@6e
    if-eqz v7, :cond_79

    #@70
    .line 782
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@72
    iget-object v7, v7, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@74
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@76
    invoke-virtual {v7, v8}, Landroid/view/HardwareRenderer;->destroyLayers(Landroid/view/View;)V

    #@79
    .line 783
    :cond_79
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7b
    iget-object v7, v7, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@7d
    invoke-virtual {v7, v5}, Landroid/view/HardwareRenderer;->destroy(Z)V

    #@80
    .line 787
    :cond_80
    iget v7, p2, Landroid/view/WindowManager$LayoutParams;->format:I

    #@82
    const/4 v8, -0x1

    #@83
    if-eq v7, v8, :cond_9f

    #@85
    move v4, v5

    #@86
    .line 788
    .local v4, translucent:Z
    :goto_86
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@88
    const/4 v8, 0x2

    #@89
    invoke-static {v8, v4}, Landroid/view/HardwareRenderer;->createGlRenderer(IZ)Landroid/view/HardwareRenderer;

    #@8c
    move-result-object v8

    #@8d
    iput-object v8, v7, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8f
    .line 789
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@91
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@93
    iget-object v9, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@95
    iget-object v9, v9, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@97
    if-eqz v9, :cond_a1

    #@99
    :goto_99
    iput-boolean v5, v8, Landroid/view/View$AttachInfo;->mHardwareAccelerationRequested:Z

    #@9b
    iput-boolean v5, v7, Landroid/view/View$AttachInfo;->mHardwareAccelerated:Z

    #@9d
    goto/16 :goto_e

    #@9f
    .end local v4           #translucent:Z
    :cond_9f
    move v4, v6

    #@a0
    .line 787
    goto :goto_86

    #@a1
    .restart local v4       #translucent:Z
    :cond_a1
    move v5, v6

    #@a2
    .line 789
    goto :goto_99

    #@a3
    .line 792
    .end local v3           #renderThread:Z
    .end local v4           #translucent:Z
    :cond_a3
    if-eqz v0, :cond_e

    #@a5
    .line 798
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a7
    iput-boolean v5, v6, Landroid/view/View$AttachInfo;->mHardwareAccelerationRequested:Z

    #@a9
    goto/16 :goto_e
.end method

.method private ensureTouchModeLocally(Z)Z
    .registers 3
    .parameter "inTouchMode"

    #@0
    .prologue
    .line 3177
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mInTouchMode:Z

    #@4
    if-ne v0, p1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 3182
    :goto_7
    return v0

    #@8
    .line 3179
    :cond_8
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iput-boolean p1, v0, Landroid/view/View$AttachInfo;->mInTouchMode:Z

    #@c
    .line 3180
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@e
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@10
    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->dispatchOnTouchModeChanged(Z)V

    #@13
    .line 3182
    if-eqz p1, :cond_1a

    #@15
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->enterTouchMode()Z

    #@18
    move-result v0

    #@19
    goto :goto_7

    #@1a
    :cond_1a
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->leaveTouchMode()Z

    #@1d
    move-result v0

    #@1e
    goto :goto_7
.end method

.method private enterTouchMode()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3186
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    if-eqz v2, :cond_38

    #@5
    .line 3187
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@7
    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_38

    #@d
    .line 3191
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@f
    invoke-virtual {v2}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@12
    move-result-object v1

    #@13
    .line 3192
    .local v1, focused:Landroid/view/View;
    if-eqz v1, :cond_38

    #@15
    invoke-virtual {v1}, Landroid/view/View;->isFocusableInTouchMode()Z

    #@18
    move-result v2

    #@19
    if-nez v2, :cond_38

    #@1b
    .line 3194
    invoke-static {v1}, Landroid/view/ViewRootImpl;->findAncestorToTakeFocusInTouchMode(Landroid/view/View;)Landroid/view/ViewGroup;

    #@1e
    move-result-object v0

    #@1f
    .line 3196
    .local v0, ancestorToTakeFocus:Landroid/view/ViewGroup;
    if-eqz v0, :cond_26

    #@21
    .line 3199
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    #@24
    move-result v2

    #@25
    .line 3211
    .end local v0           #ancestorToTakeFocus:Landroid/view/ViewGroup;
    .end local v1           #focused:Landroid/view/View;
    :goto_25
    return v2

    #@26
    .line 3202
    .restart local v0       #ancestorToTakeFocus:Landroid/view/ViewGroup;
    .restart local v1       #focused:Landroid/view/View;
    :cond_26
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@28
    invoke-virtual {v2}, Landroid/view/View;->unFocus()V

    #@2b
    .line 3203
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2d
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@2f
    invoke-virtual {v2, v1, v3}, Landroid/view/ViewTreeObserver;->dispatchOnGlobalFocusChange(Landroid/view/View;Landroid/view/View;)V

    #@32
    .line 3204
    iput-object v3, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@34
    .line 3205
    iput-object v3, p0, Landroid/view/ViewRootImpl;->mOldFocusedView:Landroid/view/View;

    #@36
    .line 3206
    const/4 v2, 0x1

    #@37
    goto :goto_25

    #@38
    .line 3211
    .end local v0           #ancestorToTakeFocus:Landroid/view/ViewGroup;
    .end local v1           #focused:Landroid/view/View;
    :cond_38
    const/4 v2, 0x0

    #@39
    goto :goto_25
.end method

.method private static findAncestorToTakeFocusInTouchMode(Landroid/view/View;)Landroid/view/ViewGroup;
    .registers 6
    .parameter "focused"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3221
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@4
    move-result-object v0

    #@5
    .line 3222
    .local v0, parent:Landroid/view/ViewParent;
    :goto_5
    instance-of v3, v0, Landroid/view/ViewGroup;

    #@7
    if-eqz v3, :cond_28

    #@9
    move-object v1, v0

    #@a
    .line 3223
    check-cast v1, Landroid/view/ViewGroup;

    #@c
    .line 3224
    .local v1, vgParent:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@f
    move-result v3

    #@10
    const/high16 v4, 0x4

    #@12
    if-ne v3, v4, :cond_1b

    #@14
    invoke-virtual {v1}, Landroid/view/ViewGroup;->isFocusableInTouchMode()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_1b

    #@1a
    .line 3234
    .end local v1           #vgParent:Landroid/view/ViewGroup;
    :goto_1a
    return-object v1

    #@1b
    .line 3228
    .restart local v1       #vgParent:Landroid/view/ViewGroup;
    :cond_1b
    invoke-virtual {v1}, Landroid/view/ViewGroup;->isRootNamespace()Z

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_23

    #@21
    move-object v1, v2

    #@22
    .line 3229
    goto :goto_1a

    #@23
    .line 3231
    :cond_23
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    #@26
    move-result-object v0

    #@27
    .line 3233
    goto :goto_5

    #@28
    .end local v1           #vgParent:Landroid/view/ViewGroup;
    :cond_28
    move-object v1, v2

    #@29
    .line 3234
    goto :goto_1a
.end method

.method private finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V
    .registers 5
    .parameter "q"
    .parameter "handled"

    #@0
    .prologue
    .line 4506
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mCurrentInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@2
    if-eq p1, v0, :cond_c

    #@4
    .line 4507
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "finished input event out of order"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 4510
    :cond_c
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mReceiver:Landroid/view/InputEventReceiver;

    #@e
    if-eqz v0, :cond_25

    #@10
    .line 4511
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mReceiver:Landroid/view/InputEventReceiver;

    #@12
    iget-object v1, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@14
    invoke-virtual {v0, v1, p2}, Landroid/view/InputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@17
    .line 4516
    :goto_17
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->recycleQueuedInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@1a
    .line 4518
    const/4 v0, 0x0

    #@1b
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mCurrentInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@1d
    .line 4519
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@1f
    if-eqz v0, :cond_24

    #@21
    .line 4520
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->scheduleProcessInputEvents()V

    #@24
    .line 4522
    :cond_24
    return-void

    #@25
    .line 4513
    :cond_25
    iget-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@27
    invoke-virtual {v0}, Landroid/view/InputEvent;->recycleIfNeededAfterDispatch()V

    #@2a
    goto :goto_17
.end method

.method private static forceLayout(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    .line 2820
    invoke-virtual {p0}, Landroid/view/View;->forceLayout()V

    #@3
    .line 2821
    instance-of v3, p0, Landroid/view/ViewGroup;

    #@5
    if-eqz v3, :cond_1b

    #@7
    move-object v1, p0

    #@8
    .line 2822
    check-cast v1, Landroid/view/ViewGroup;

    #@a
    .line 2823
    .local v1, group:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@d
    move-result v0

    #@e
    .line 2824
    .local v0, count:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v0, :cond_1b

    #@11
    .line 2825
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v3

    #@15
    invoke-static {v3}, Landroid/view/ViewRootImpl;->forceLayout(Landroid/view/View;)V

    #@18
    .line 2824
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_f

    #@1b
    .line 2828
    .end local v0           #count:I
    .end local v1           #group:Landroid/view/ViewGroup;
    .end local v2           #i:I
    :cond_1b
    return-void
.end method

.method private getAccessibilityFocusedDrawable()Landroid/graphics/drawable/Drawable;
    .registers 6

    #@0
    .prologue
    .line 2453
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v2, :cond_38

    #@4
    .line 2455
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mAccessibilityFocusDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    if-nez v2, :cond_33

    #@a
    .line 2456
    new-instance v1, Landroid/util/TypedValue;

    #@c
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@f
    .line 2457
    .local v1, value:Landroid/util/TypedValue;
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@11
    iget-object v2, v2, Landroid/view/View;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@16
    move-result-object v2

    #@17
    const v3, 0x10103f8

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@1e
    move-result v0

    #@1f
    .line 2459
    .local v0, resolved:Z
    if-eqz v0, :cond_33

    #@21
    .line 2460
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@23
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@25
    iget-object v3, v3, Landroid/view/View;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2a
    move-result-object v3

    #@2b
    iget v4, v1, Landroid/util/TypedValue;->resourceId:I

    #@2d
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@30
    move-result-object v3

    #@31
    iput-object v3, v2, Landroid/view/View$AttachInfo;->mAccessibilityFocusDrawable:Landroid/graphics/drawable/Drawable;

    #@33
    .line 2464
    .end local v0           #resolved:Z
    .end local v1           #value:Landroid/util/TypedValue;
    :cond_33
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@35
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mAccessibilityFocusDrawable:Landroid/graphics/drawable/Drawable;

    #@37
    .line 2466
    :goto_37
    return-object v2

    #@38
    :cond_38
    const/4 v2, 0x0

    #@39
    goto :goto_37
.end method

.method private getAudioManager()Landroid/media/AudioManager;
    .registers 3

    #@0
    .prologue
    .line 4107
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 4108
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "getAudioManager called when there is no mView"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 4110
    :cond_c
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAudioManager:Landroid/media/AudioManager;

    #@e
    if-nez v0, :cond_20

    #@10
    .line 4111
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@12
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@15
    move-result-object v0

    #@16
    const-string v1, "audio"

    #@18
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/media/AudioManager;

    #@1e
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mAudioManager:Landroid/media/AudioManager;

    #@20
    .line 4113
    :cond_20
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAudioManager:Landroid/media/AudioManager;

    #@22
    return-object v0
.end method

.method private getCommonPredecessor(Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .registers 9
    .parameter "first"
    .parameter "second"

    #@0
    .prologue
    .line 4921
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v5, :cond_42

    #@4
    .line 4922
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mTempHashSet:Ljava/util/HashSet;

    #@6
    if-nez v5, :cond_f

    #@8
    .line 4923
    new-instance v5, Ljava/util/HashSet;

    #@a
    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    #@d
    iput-object v5, p0, Landroid/view/ViewRootImpl;->mTempHashSet:Ljava/util/HashSet;

    #@f
    .line 4925
    :cond_f
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTempHashSet:Ljava/util/HashSet;

    #@11
    .line 4926
    .local v4, seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@14
    .line 4927
    move-object v0, p1

    #@15
    .line 4928
    .local v0, firstCurrent:Landroid/view/View;
    :goto_15
    if-eqz v0, :cond_26

    #@17
    .line 4929
    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1a
    .line 4930
    iget-object v1, v0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1c
    .line 4931
    .local v1, firstCurrentParent:Landroid/view/ViewParent;
    instance-of v5, v1, Landroid/view/View;

    #@1e
    if-eqz v5, :cond_24

    #@20
    move-object v0, v1

    #@21
    .line 4932
    check-cast v0, Landroid/view/View;

    #@23
    goto :goto_15

    #@24
    .line 4934
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_15

    #@26
    .line 4937
    .end local v1           #firstCurrentParent:Landroid/view/ViewParent;
    :cond_26
    move-object v2, p2

    #@27
    .line 4938
    .local v2, secondCurrent:Landroid/view/View;
    :goto_27
    if-eqz v2, :cond_3f

    #@29
    .line 4939
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_33

    #@2f
    .line 4940
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@32
    .line 4952
    .end local v0           #firstCurrent:Landroid/view/View;
    .end local v2           #secondCurrent:Landroid/view/View;
    .end local v4           #seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/View;>;"
    :goto_32
    return-object v2

    #@33
    .line 4943
    .restart local v0       #firstCurrent:Landroid/view/View;
    .restart local v2       #secondCurrent:Landroid/view/View;
    .restart local v4       #seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/View;>;"
    :cond_33
    iget-object v3, v2, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@35
    .line 4944
    .local v3, secondCurrentParent:Landroid/view/ViewParent;
    instance-of v5, v3, Landroid/view/View;

    #@37
    if-eqz v5, :cond_3d

    #@39
    move-object v2, v3

    #@3a
    .line 4945
    check-cast v2, Landroid/view/View;

    #@3c
    goto :goto_27

    #@3d
    .line 4947
    :cond_3d
    const/4 v2, 0x0

    #@3e
    goto :goto_27

    #@3f
    .line 4950
    .end local v3           #secondCurrentParent:Landroid/view/ViewParent;
    :cond_3f
    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    #@42
    .line 4952
    .end local v0           #firstCurrent:Landroid/view/View;
    .end local v2           #secondCurrent:Landroid/view/View;
    .end local v4           #seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/View;>;"
    :cond_42
    const/4 v2, 0x0

    #@43
    goto :goto_32
.end method

.method private static getGfxInfo(Landroid/view/View;[I)V
    .registers 9
    .parameter "view"
    .parameter "info"

    #@0
    .prologue
    .line 4248
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2
    .line 4249
    .local v1, displayList:Landroid/view/DisplayList;
    const/4 v4, 0x0

    #@3
    aget v5, p1, v4

    #@5
    add-int/lit8 v5, v5, 0x1

    #@7
    aput v5, p1, v4

    #@9
    .line 4250
    if-eqz v1, :cond_15

    #@b
    .line 4251
    const/4 v4, 0x1

    #@c
    aget v5, p1, v4

    #@e
    invoke-virtual {v1}, Landroid/view/DisplayList;->getSize()I

    #@11
    move-result v6

    #@12
    add-int/2addr v5, v6

    #@13
    aput v5, p1, v4

    #@15
    .line 4254
    :cond_15
    instance-of v4, p0, Landroid/view/ViewGroup;

    #@17
    if-eqz v4, :cond_2d

    #@19
    move-object v2, p0

    #@1a
    .line 4255
    check-cast v2, Landroid/view/ViewGroup;

    #@1c
    .line 4257
    .local v2, group:Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    #@1f
    move-result v0

    #@20
    .line 4258
    .local v0, count:I
    const/4 v3, 0x0

    #@21
    .local v3, i:I
    :goto_21
    if-ge v3, v0, :cond_2d

    #@23
    .line 4259
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@26
    move-result-object v4

    #@27
    invoke-static {v4, p1}, Landroid/view/ViewRootImpl;->getGfxInfo(Landroid/view/View;[I)V

    #@2a
    .line 4258
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_21

    #@2d
    .line 4262
    .end local v0           #count:I
    .end local v2           #group:Landroid/view/ViewGroup;
    .end local v3           #i:I
    :cond_2d
    return-void
.end method

.method private getInteractionManager()Lcom/lge/loader/interaction/IInteractionManager;
    .registers 3

    #@0
    .prologue
    .line 4051
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 4052
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "InteractionManager called when there is no mView"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 4054
    :cond_c
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInteractionManager:Lcom/lge/loader/interaction/IInteractionManager;

    #@e
    if-nez v0, :cond_16

    #@10
    .line 4055
    invoke-static {}, Lcom/lge/loader/interaction/InteractionManagerLoader;->getServiceManager()Lcom/lge/loader/interaction/IInteractionManager;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mInteractionManager:Lcom/lge/loader/interaction/IInteractionManager;

    #@16
    .line 4057
    :cond_16
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInteractionManager:Lcom/lge/loader/interaction/IInteractionManager;

    #@18
    if-nez v0, :cond_21

    #@1a
    .line 4058
    const-string v0, "ViewRootImpl"

    #@1c
    const-string v1, "FATAL ERROR - InteractionManager is null"

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 4060
    :cond_21
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInteractionManager:Lcom/lge/loader/interaction/IInteractionManager;

    #@23
    return-object v0
.end method

.method private static getRootMeasureSpec(II)I
    .registers 4
    .parameter "windowSize"
    .parameter "rootDimension"

    #@0
    .prologue
    const/high16 v1, 0x4000

    #@2
    .line 2032
    packed-switch p1, :pswitch_data_16

    #@5
    .line 2044
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@8
    move-result v0

    #@9
    .line 2047
    .local v0, measureSpec:I
    :goto_9
    return v0

    #@a
    .line 2036
    .end local v0           #measureSpec:I
    :pswitch_a
    invoke-static {p0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@d
    move-result v0

    #@e
    .line 2037
    .restart local v0       #measureSpec:I
    goto :goto_9

    #@f
    .line 2040
    .end local v0           #measureSpec:I
    :pswitch_f
    const/high16 v1, -0x8000

    #@11
    invoke-static {p0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@14
    move-result v0

    #@15
    .line 2041
    .restart local v0       #measureSpec:I
    goto :goto_9

    #@16
    .line 2032
    :pswitch_data_16
    .packed-switch -0x2
        :pswitch_f
        :pswitch_a
    .end packed-switch
.end method

.method static getRunQueue()Landroid/view/ViewRootImpl$RunQueue;
    .registers 2

    #@0
    .prologue
    .line 5430
    sget-object v1, Landroid/view/ViewRootImpl;->sRunQueues:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl$RunQueue;

    #@8
    .line 5431
    .local v0, rq:Landroid/view/ViewRootImpl$RunQueue;
    if-eqz v0, :cond_b

    #@a
    .line 5436
    :goto_a
    return-object v0

    #@b
    .line 5434
    :cond_b
    new-instance v0, Landroid/view/ViewRootImpl$RunQueue;

    #@d
    .end local v0           #rq:Landroid/view/ViewRootImpl$RunQueue;
    invoke-direct {v0}, Landroid/view/ViewRootImpl$RunQueue;-><init>()V

    #@10
    .line 5435
    .restart local v0       #rq:Landroid/view/ViewRootImpl$RunQueue;
    sget-object v1, Landroid/view/ViewRootImpl;->sRunQueues:Ljava/lang/ThreadLocal;

    #@12
    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@15
    goto :goto_a
.end method

.method private handleDragEvent(Landroid/view/DragEvent;)V
    .registers 14
    .parameter "event"

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x0

    #@2
    .line 3946
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4
    if-eqz v7, :cond_14

    #@6
    iget-boolean v7, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@8
    if-eqz v7, :cond_14

    #@a
    .line 3947
    iget v4, p1, Landroid/view/DragEvent;->mAction:I

    #@c
    .line 3949
    .local v4, what:I
    const/4 v7, 0x6

    #@d
    if-ne v4, v7, :cond_18

    #@f
    .line 3954
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@11
    invoke-virtual {v7, p1}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@14
    .line 4032
    .end local v4           #what:I
    :cond_14
    :goto_14
    invoke-virtual {p1}, Landroid/view/DragEvent;->recycle()V

    #@17
    .line 4033
    return-void

    #@18
    .line 3958
    .restart local v4       #what:I
    :cond_18
    const/4 v7, 0x1

    #@19
    if-ne v4, v7, :cond_ce

    #@1b
    .line 3959
    iput-object v10, p0, Landroid/view/ViewRootImpl;->mCurrentDragView:Landroid/view/View;

    #@1d
    .line 3960
    iget-object v7, p1, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@1f
    iput-object v7, p0, Landroid/view/ViewRootImpl;->mDragDescription:Landroid/content/ClipDescription;

    #@21
    .line 3966
    :goto_21
    const/4 v7, 0x2

    #@22
    if-eq v4, v7, :cond_26

    #@24
    if-ne v4, v11, :cond_53

    #@26
    .line 3967
    :cond_26
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mDragPoint:Landroid/graphics/PointF;

    #@28
    iget v8, p1, Landroid/view/DragEvent;->mX:F

    #@2a
    iget v9, p1, Landroid/view/DragEvent;->mY:F

    #@2c
    invoke-virtual {v7, v8, v9}, Landroid/graphics/PointF;->set(FF)V

    #@2f
    .line 3968
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@31
    if-eqz v7, :cond_3a

    #@33
    .line 3969
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@35
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mDragPoint:Landroid/graphics/PointF;

    #@37
    invoke-virtual {v7, v8}, Landroid/content/res/CompatibilityInfo$Translator;->translatePointInScreenToAppWindow(Landroid/graphics/PointF;)V

    #@3a
    .line 3972
    :cond_3a
    iget v7, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@3c
    if-eqz v7, :cond_47

    #@3e
    .line 3973
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mDragPoint:Landroid/graphics/PointF;

    #@40
    const/4 v8, 0x0

    #@41
    iget v9, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@43
    int-to-float v9, v9

    #@44
    invoke-virtual {v7, v8, v9}, Landroid/graphics/PointF;->offset(FF)V

    #@47
    .line 3976
    :cond_47
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mDragPoint:Landroid/graphics/PointF;

    #@49
    iget v7, v7, Landroid/graphics/PointF;->x:F

    #@4b
    iput v7, p1, Landroid/view/DragEvent;->mX:F

    #@4d
    .line 3977
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mDragPoint:Landroid/graphics/PointF;

    #@4f
    iget v7, v7, Landroid/graphics/PointF;->y:F

    #@51
    iput v7, p1, Landroid/view/DragEvent;->mY:F

    #@53
    .line 3981
    :cond_53
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mCurrentDragView:Landroid/view/View;

    #@55
    .line 3984
    .local v2, prevDragView:Landroid/view/View;
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@57
    invoke-virtual {v7, p1}, Landroid/view/View;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    #@5a
    move-result v3

    #@5b
    .line 3987
    .local v3, result:Z
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mCurrentDragView:Landroid/view/View;

    #@5d
    if-eq v2, v7, :cond_73

    #@5f
    .line 3989
    if-eqz v2, :cond_68

    #@61
    .line 3990
    :try_start_61
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@63
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@65
    invoke-interface {v7, v8}, Landroid/view/IWindowSession;->dragRecipientExited(Landroid/view/IWindow;)V

    #@68
    .line 3992
    :cond_68
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mCurrentDragView:Landroid/view/View;

    #@6a
    if-eqz v7, :cond_73

    #@6c
    .line 3993
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@6e
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@70
    invoke-interface {v7, v8}, Landroid/view/IWindowSession;->dragRecipientEntered(Landroid/view/IWindow;)V
    :try_end_73
    .catch Landroid/os/RemoteException; {:try_start_61 .. :try_end_73} :catch_d4

    #@73
    .line 4001
    :cond_73
    :goto_73
    if-ne v4, v11, :cond_c6

    #@75
    .line 4004
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_ZDI_O:Z

    #@77
    if-eqz v7, :cond_a5

    #@79
    if-nez v3, :cond_a5

    #@7b
    .line 4005
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@7d
    invoke-virtual {v7}, Landroid/view/View;->getX()F

    #@80
    move-result v7

    #@81
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    #@84
    move-result v8

    #@85
    add-float v5, v7, v8

    #@87
    .line 4006
    .local v5, x:F
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@89
    invoke-virtual {v7}, Landroid/view/View;->getY()F

    #@8c
    move-result v7

    #@8d
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    #@90
    move-result v8

    #@91
    add-float v6, v7, v8

    #@93
    .line 4007
    .local v6, y:F
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@95
    iget-object v1, v7, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@97
    .line 4008
    .local v1, info:Landroid/view/View$AttachInfo;
    if-eqz v1, :cond_a1

    #@99
    .line 4009
    iget v7, v1, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@9b
    int-to-float v7, v7

    #@9c
    add-float/2addr v5, v7

    #@9d
    .line 4010
    iget v7, v1, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@9f
    int-to-float v7, v7

    #@a0
    add-float/2addr v6, v7

    #@a1
    .line 4012
    :cond_a1
    invoke-direct {p0, p1, v5, v6}, Landroid/view/ViewRootImpl;->handleDropEvent(Landroid/view/DragEvent;FF)Z

    #@a4
    move-result v3

    #@a5
    .line 4016
    .end local v1           #info:Landroid/view/View$AttachInfo;
    .end local v5           #x:F
    .end local v6           #y:F
    :cond_a5
    iput-object v10, p0, Landroid/view/ViewRootImpl;->mDragDescription:Landroid/content/ClipDescription;

    #@a7
    .line 4018
    :try_start_a7
    const-string v7, "ViewRootImpl"

    #@a9
    new-instance v8, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    const-string v9, "Reporting drop result: "

    #@b0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v8

    #@b8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v8

    #@bc
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 4019
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@c1
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@c3
    invoke-interface {v7, v8, v3}, Landroid/view/IWindowSession;->reportDropResult(Landroid/view/IWindow;Z)V
    :try_end_c6
    .catch Landroid/os/RemoteException; {:try_start_a7 .. :try_end_c6} :catch_dd

    #@c6
    .line 4027
    :cond_c6
    :goto_c6
    const/4 v7, 0x4

    #@c7
    if-ne v4, v7, :cond_14

    #@c9
    .line 4028
    invoke-virtual {p0, v10}, Landroid/view/ViewRootImpl;->setLocalDragState(Ljava/lang/Object;)V

    #@cc
    goto/16 :goto_14

    #@ce
    .line 3962
    .end local v2           #prevDragView:Landroid/view/View;
    .end local v3           #result:Z
    :cond_ce
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mDragDescription:Landroid/content/ClipDescription;

    #@d0
    iput-object v7, p1, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@d2
    goto/16 :goto_21

    #@d4
    .line 3995
    .restart local v2       #prevDragView:Landroid/view/View;
    .restart local v3       #result:Z
    :catch_d4
    move-exception v0

    #@d5
    .line 3996
    .local v0, e:Landroid/os/RemoteException;
    const-string v7, "ViewRootImpl"

    #@d7
    const-string v8, "Unable to note drag target change"

    #@d9
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    goto :goto_73

    #@dd
    .line 4020
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_dd
    move-exception v0

    #@de
    .line 4021
    .restart local v0       #e:Landroid/os/RemoteException;
    const-string v7, "ViewRootImpl"

    #@e0
    const-string v8, "Unable to report drop result"

    #@e2
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    goto :goto_c6
.end method

.method private handleDropEvent(Landroid/view/DragEvent;FF)Z
    .registers 9
    .parameter "event"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 4038
    :try_start_0
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->getInteractionManager()Lcom/lge/loader/interaction/IInteractionManager;

    #@3
    move-result-object v1

    #@4
    .line 4040
    .local v1, interactionManager:Lcom/lge/loader/interaction/IInteractionManager;
    if-eqz v1, :cond_2b

    #@6
    .line 4041
    invoke-static {p1}, Landroid/view/DragEvent;->obtain(Landroid/view/DragEvent;)Landroid/view/DragEvent;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v1, v2, p2, p3}, Lcom/lge/loader/interaction/IInteractionManager;->handleDropEvent(Landroid/view/DragEvent;FF)Z
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result v2

    #@e
    .line 4047
    .end local v1           #interactionManager:Lcom/lge/loader/interaction/IInteractionManager;
    :goto_e
    return v2

    #@f
    .line 4043
    :catch_f
    move-exception v0

    #@10
    .line 4044
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v2, "ViewRootImpl"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "FATAL EXCEPTION UNEXPECTED "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 4045
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2b
    .line 4047
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :cond_2b
    const/4 v2, 0x0

    #@2c
    goto :goto_e
.end method

.method static isInTouchMode()Z
    .registers 2

    #@0
    .prologue
    .line 495
    invoke-static {}, Landroid/view/WindowManagerGlobal;->peekWindowSession()Landroid/view/IWindowSession;

    #@3
    move-result-object v0

    #@4
    .line 496
    .local v0, windowSession:Landroid/view/IWindowSession;
    if-eqz v0, :cond_c

    #@6
    .line 498
    :try_start_6
    invoke-interface {v0}, Landroid/view/IWindowSession;->getInTouchMode()Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 502
    :goto_a
    return v1

    #@b
    .line 499
    :catch_b
    move-exception v1

    #@c
    .line 502
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method private static isNavigationKey(Landroid/view/KeyEvent;)Z
    .registers 2
    .parameter "keyEvent"

    #@0
    .prologue
    .line 3672
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v0

    #@4
    sparse-switch v0, :sswitch_data_c

    #@7
    .line 3687
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    .line 3685
    :sswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_8

    #@b
    .line 3672
    nop

    #@c
    :sswitch_data_c
    .sparse-switch
        0x13 -> :sswitch_9
        0x14 -> :sswitch_9
        0x15 -> :sswitch_9
        0x16 -> :sswitch_9
        0x17 -> :sswitch_9
        0x3d -> :sswitch_9
        0x3e -> :sswitch_9
        0x42 -> :sswitch_9
        0x5c -> :sswitch_9
        0x5d -> :sswitch_9
        0x7a -> :sswitch_9
        0x7b -> :sswitch_9
    .end sparse-switch
.end method

.method private static isRenderThreadRequested(Landroid/content/Context;)Z
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 457
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method private static isTypingKey(Landroid/view/KeyEvent;)Z
    .registers 2
    .parameter "keyEvent"

    #@0
    .prologue
    .line 3696
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getUnicodeChar()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public static isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z
    .registers 5
    .parameter "child"
    .parameter "parent"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2811
    if-ne p0, p1, :cond_4

    #@3
    .line 2816
    :cond_3
    :goto_3
    return v1

    #@4
    .line 2815
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@7
    move-result-object v0

    #@8
    .line 2816
    .local v0, theParent:Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    #@a
    if-eqz v2, :cond_14

    #@c
    check-cast v0, Landroid/view/View;

    #@e
    .end local v0           #theParent:Landroid/view/ViewParent;
    invoke-static {v0, p1}, Landroid/view/ViewRootImpl;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_3

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_3
.end method

.method private static joystickAxisValueToDirection(F)I
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 3657
    const/high16 v0, 0x3f00

    #@2
    cmpl-float v0, p0, v0

    #@4
    if-ltz v0, :cond_8

    #@6
    .line 3658
    const/4 v0, 0x1

    #@7
    .line 3662
    :goto_7
    return v0

    #@8
    .line 3659
    :cond_8
    const/high16 v0, -0x4100

    #@a
    cmpg-float v0, p0, v0

    #@c
    if-gtz v0, :cond_10

    #@e
    .line 3660
    const/4 v0, -0x1

    #@f
    goto :goto_7

    #@10
    .line 3662
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_7
.end method

.method private leaveTouchMode()Z
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x82

    #@2
    const/4 v2, 0x0

    #@3
    .line 3238
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5
    if-eqz v1, :cond_39

    #@7
    .line 3239
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@9
    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2d

    #@f
    .line 3241
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@11
    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@17
    .line 3242
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@19
    instance-of v1, v1, Landroid/view/ViewGroup;

    #@1b
    if-nez v1, :cond_1f

    #@1d
    move v1, v2

    #@1e
    .line 3260
    :goto_1e
    return v1

    #@1f
    .line 3245
    :cond_1f
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@21
    check-cast v1, Landroid/view/ViewGroup;

    #@23
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@26
    move-result v1

    #@27
    const/high16 v3, 0x4

    #@29
    if-eq v1, v3, :cond_2d

    #@2b
    move v1, v2

    #@2c
    .line 3249
    goto :goto_1e

    #@2d
    .line 3255
    :cond_2d
    const/4 v1, 0x0

    #@2e
    invoke-virtual {p0, v1, v4}, Landroid/view/ViewRootImpl;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    #@31
    move-result-object v0

    #@32
    .line 3256
    .local v0, focused:Landroid/view/View;
    if-eqz v0, :cond_39

    #@34
    .line 3257
    invoke-virtual {v0, v4}, Landroid/view/View;->requestFocus(I)Z

    #@37
    move-result v1

    #@38
    goto :goto_1e

    #@39
    .end local v0           #focused:Landroid/view/View;
    :cond_39
    move v1, v2

    #@3a
    .line 3260
    goto :goto_1e
.end method

.method private measureHierarchy(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/content/res/Resources;II)Z
    .registers 16
    .parameter "host"
    .parameter "lp"
    .parameter "res"
    .parameter "desiredWindowWidth"
    .parameter "desiredWindowHeight"

    #@0
    .prologue
    const/high16 v9, 0x100

    #@2
    .line 1103
    const/4 v5, 0x0

    #@3
    .line 1109
    .local v5, windowSizeMayChange:Z
    const/4 v3, 0x0

    #@4
    .line 1110
    .local v3, goodMeasure:Z
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    #@6
    const/4 v7, -0x2

    #@7
    if-ne v6, v7, :cond_40

    #@9
    .line 1115
    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@c
    move-result-object v4

    #@d
    .line 1116
    .local v4, packageMetrics:Landroid/util/DisplayMetrics;
    const v6, 0x1050007

    #@10
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mTmpValue:Landroid/util/TypedValue;

    #@12
    const/4 v8, 0x1

    #@13
    invoke-virtual {p3, v6, v7, v8}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    #@16
    .line 1117
    const/4 v0, 0x0

    #@17
    .line 1118
    .local v0, baseSize:I
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTmpValue:Landroid/util/TypedValue;

    #@19
    iget v6, v6, Landroid/util/TypedValue;->type:I

    #@1b
    const/4 v7, 0x5

    #@1c
    if-ne v6, v7, :cond_25

    #@1e
    .line 1119
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTmpValue:Landroid/util/TypedValue;

    #@20
    invoke-virtual {v6, v4}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@23
    move-result v6

    #@24
    float-to-int v0, v6

    #@25
    .line 1122
    :cond_25
    if-eqz v0, :cond_40

    #@27
    if-le p4, v0, :cond_40

    #@29
    .line 1123
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    #@2b
    invoke-static {v0, v6}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@2e
    move-result v2

    #@2f
    .line 1124
    .local v2, childWidthMeasureSpec:I
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    #@31
    invoke-static {p5, v6}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@34
    move-result v1

    #@35
    .line 1125
    .local v1, childHeightMeasureSpec:I
    invoke-direct {p0, v2, v1}, Landroid/view/ViewRootImpl;->performMeasure(II)V

    #@38
    .line 1128
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidthAndState()I

    #@3b
    move-result v6

    #@3c
    and-int/2addr v6, v9

    #@3d
    if-nez v6, :cond_63

    #@3f
    .line 1129
    const/4 v3, 0x1

    #@40
    .line 1147
    .end local v0           #baseSize:I
    .end local v1           #childHeightMeasureSpec:I
    .end local v2           #childWidthMeasureSpec:I
    .end local v4           #packageMetrics:Landroid/util/DisplayMetrics;
    :cond_40
    :goto_40
    if-nez v3, :cond_62

    #@42
    .line 1148
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    #@44
    invoke-static {p4, v6}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@47
    move-result v2

    #@48
    .line 1149
    .restart local v2       #childWidthMeasureSpec:I
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    #@4a
    invoke-static {p5, v6}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@4d
    move-result v1

    #@4e
    .line 1150
    .restart local v1       #childHeightMeasureSpec:I
    invoke-direct {p0, v2, v1}, Landroid/view/ViewRootImpl;->performMeasure(II)V

    #@51
    .line 1151
    iget v6, p0, Landroid/view/ViewRootImpl;->mWidth:I

    #@53
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@56
    move-result v7

    #@57
    if-ne v6, v7, :cond_61

    #@59
    iget v6, p0, Landroid/view/ViewRootImpl;->mHeight:I

    #@5b
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@5e
    move-result v7

    #@5f
    if-eq v6, v7, :cond_62

    #@61
    .line 1152
    :cond_61
    const/4 v5, 0x1

    #@62
    .line 1162
    .end local v1           #childHeightMeasureSpec:I
    .end local v2           #childWidthMeasureSpec:I
    :cond_62
    return v5

    #@63
    .line 1132
    .restart local v0       #baseSize:I
    .restart local v1       #childHeightMeasureSpec:I
    .restart local v2       #childWidthMeasureSpec:I
    .restart local v4       #packageMetrics:Landroid/util/DisplayMetrics;
    :cond_63
    add-int v6, v0, p4

    #@65
    div-int/lit8 v0, v6, 0x2

    #@67
    .line 1135
    iget v6, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    #@69
    invoke-static {v0, v6}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@6c
    move-result v2

    #@6d
    .line 1136
    invoke-direct {p0, v2, v1}, Landroid/view/ViewRootImpl;->performMeasure(II)V

    #@70
    .line 1139
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidthAndState()I

    #@73
    move-result v6

    #@74
    and-int/2addr v6, v9

    #@75
    if-nez v6, :cond_40

    #@77
    .line 1141
    const/4 v3, 0x1

    #@78
    goto :goto_40
.end method

.method private obtainQueuedInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;I)Landroid/view/ViewRootImpl$QueuedInputEvent;
    .registers 7
    .parameter "event"
    .parameter "receiver"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4423
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPool:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@3
    .line 4424
    .local v0, q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    if-eqz v0, :cond_18

    #@5
    .line 4425
    iget v1, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPoolSize:I

    #@7
    add-int/lit8 v1, v1, -0x1

    #@9
    iput v1, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPoolSize:I

    #@b
    .line 4426
    iget-object v1, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@d
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPool:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@f
    .line 4427
    iput-object v2, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@11
    .line 4432
    :goto_11
    iput-object p1, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@13
    .line 4433
    iput-object p2, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mReceiver:Landroid/view/InputEventReceiver;

    #@15
    .line 4434
    iput p3, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mFlags:I

    #@17
    .line 4435
    return-object v0

    #@18
    .line 4429
    :cond_18
    new-instance v0, Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@1a
    .end local v0           #q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    invoke-direct {v0, v2}, Landroid/view/ViewRootImpl$QueuedInputEvent;-><init>(Landroid/view/ViewRootImpl$1;)V

    #@1d
    .restart local v0       #q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    goto :goto_11
.end method

.method private performDraw()V
    .registers 11

    #@0
    .prologue
    const-wide/16 v8, 0x8

    #@2
    const/4 v7, 0x0

    #@3
    .line 2141
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    iget-boolean v6, v6, Landroid/view/View$AttachInfo;->mScreenOn:Z

    #@7
    if-nez v6, :cond_e

    #@9
    iget-boolean v6, p0, Landroid/view/ViewRootImpl;->mReportNextDraw:Z

    #@b
    if-nez v6, :cond_e

    #@d
    .line 2180
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2145
    :cond_e
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@10
    .line 2146
    .local v3, fullRedrawNeeded:Z
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@12
    .line 2148
    const/4 v6, 0x1

    #@13
    iput-boolean v6, p0, Landroid/view/ViewRootImpl;->mIsDrawing:Z

    #@15
    .line 2149
    const-string v6, "draw"

    #@17
    invoke-static {v8, v9, v6}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@1a
    .line 2151
    :try_start_1a
    invoke-direct {p0, v3}, Landroid/view/ViewRootImpl;->draw(Z)V
    :try_end_1d
    .catchall {:try_start_1a .. :try_end_1d} :catchall_58

    #@1d
    .line 2153
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mIsDrawing:Z

    #@1f
    .line 2154
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@22
    .line 2157
    iget-boolean v6, p0, Landroid/view/ViewRootImpl;->mReportNextDraw:Z

    #@24
    if-eqz v6, :cond_d

    #@26
    .line 2158
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mReportNextDraw:Z

    #@28
    .line 2163
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2a
    if-eqz v6, :cond_5f

    #@2c
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@2e
    invoke-virtual {v6}, Landroid/view/Surface;->isValid()Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_5f

    #@34
    .line 2164
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

    #@36
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@38
    invoke-interface {v6, v7}, Landroid/view/SurfaceHolder$Callback2;->surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V

    #@3b
    .line 2165
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@3d
    invoke-virtual {v6}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@40
    move-result-object v2

    #@41
    .line 2166
    .local v2, callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v2, :cond_5f

    #@43
    .line 2167
    move-object v0, v2

    #@44
    .local v0, arr$:[Landroid/view/SurfaceHolder$Callback;
    array-length v5, v0

    #@45
    .local v5, len$:I
    const/4 v4, 0x0

    #@46
    .local v4, i$:I
    :goto_46
    if-ge v4, v5, :cond_5f

    #@48
    aget-object v1, v0, v4

    #@4a
    .line 2168
    .local v1, c:Landroid/view/SurfaceHolder$Callback;
    instance-of v6, v1, Landroid/view/SurfaceHolder$Callback2;

    #@4c
    if-eqz v6, :cond_55

    #@4e
    .line 2169
    check-cast v1, Landroid/view/SurfaceHolder$Callback2;

    #@50
    .end local v1           #c:Landroid/view/SurfaceHolder$Callback;
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@52
    invoke-interface {v1, v6}, Landroid/view/SurfaceHolder$Callback2;->surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V

    #@55
    .line 2167
    :cond_55
    add-int/lit8 v4, v4, 0x1

    #@57
    goto :goto_46

    #@58
    .line 2153
    .end local v0           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v2           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :catchall_58
    move-exception v6

    #@59
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mIsDrawing:Z

    #@5b
    .line 2154
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@5e
    throw v6

    #@5f
    .line 2176
    :cond_5f
    :try_start_5f
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@61
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@63
    invoke-interface {v6, v7}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V
    :try_end_66
    .catch Landroid/os/RemoteException; {:try_start_5f .. :try_end_66} :catch_67

    #@66
    goto :goto_d

    #@67
    .line 2177
    :catch_67
    move-exception v6

    #@68
    goto :goto_d
.end method

.method private performLayout()V
    .registers 8

    #@0
    .prologue
    const-wide/16 v5, 0x8

    #@2
    const/4 v1, 0x0

    #@3
    .line 1984
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@5
    .line 1985
    const/4 v1, 0x1

    #@6
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mScrollMayChange:Z

    #@8
    .line 1987
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@a
    if-nez v1, :cond_d

    #@c
    .line 2002
    :goto_c
    return-void

    #@d
    .line 1990
    :cond_d
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@f
    .line 1996
    .local v0, host:Landroid/view/View;
    const-string/jumbo v1, "layout"

    #@12
    invoke-static {v5, v6, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@15
    .line 1998
    const/4 v1, 0x0

    #@16
    const/4 v2, 0x0

    #@17
    :try_start_17
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@1a
    move-result v3

    #@1b
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@1e
    move-result v4

    #@1f
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V
    :try_end_22
    .catchall {:try_start_17 .. :try_end_22} :catchall_26

    #@22
    .line 2000
    invoke-static {v5, v6}, Landroid/os/Trace;->traceEnd(J)V

    #@25
    goto :goto_c

    #@26
    :catchall_26
    move-exception v1

    #@27
    invoke-static {v5, v6}, Landroid/os/Trace;->traceEnd(J)V

    #@2a
    throw v1
.end method

.method private performMeasure(II)V
    .registers 6
    .parameter "childWidthMeasureSpec"
    .parameter "childHeightMeasureSpec"

    #@0
    .prologue
    const-wide/16 v1, 0x8

    #@2
    .line 1973
    const-string/jumbo v0, "measure"

    #@5
    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@8
    .line 1975
    :try_start_8
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 1976
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@e
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V
    :try_end_11
    .catchall {:try_start_8 .. :try_end_11} :catchall_15

    #@11
    .line 1979
    :cond_11
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    #@14
    .line 1981
    return-void

    #@15
    .line 1979
    :catchall_15
    move-exception v0

    #@16
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    #@19
    throw v0
.end method

.method private performTraversals()V
    .registers 77

    #@0
    .prologue
    .line 1181
    move-object/from16 v0, p0

    #@2
    iget-object v11, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4
    .line 1189
    .local v11, host:Landroid/view/View;
    if-eqz v11, :cond_c

    #@6
    move-object/from16 v0, p0

    #@8
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@a
    if-nez v4, :cond_d

    #@c
    .line 1970
    :cond_c
    :goto_c
    return-void

    #@d
    .line 1192
    :cond_d
    move-object/from16 v0, p0

    #@f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@11
    if-eqz v4, :cond_65

    #@13
    move-object/from16 v0, p0

    #@15
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@17
    invoke-virtual {v4}, Landroid/view/View;->toString()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, "chromium.content.browser.ChromeView"

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@20
    move-result v4

    #@21
    if-eqz v4, :cond_65

    #@23
    instance-of v4, v11, Landroid/view/ViewGroup;

    #@25
    if-eqz v4, :cond_65

    #@27
    move-object/from16 v58, v11

    #@29
    .line 1193
    check-cast v58, Landroid/view/ViewGroup;

    #@2b
    .line 1194
    .local v58, parent:Landroid/view/ViewGroup;
    invoke-virtual/range {v58 .. v58}, Landroid/view/ViewGroup;->isChromeBrowserRunningJavaScripts()Z

    #@2e
    move-result v4

    #@2f
    if-eqz v4, :cond_36

    #@31
    .line 1195
    const/4 v4, 0x1

    #@32
    move-object/from16 v0, p0

    #@34
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsActive:Z

    #@36
    .line 1197
    :cond_36
    move-object/from16 v0, p0

    #@38
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsScheduled:Z

    #@3a
    if-nez v4, :cond_65

    #@3c
    move-object/from16 v0, p0

    #@3e
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsActive:Z

    #@40
    if-eqz v4, :cond_65

    #@42
    .line 1198
    new-instance v4, Ljava/util/Timer;

    #@44
    invoke-direct {v4}, Ljava/util/Timer;-><init>()V

    #@47
    move-object/from16 v0, p0

    #@49
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mActiveTimer:Ljava/util/Timer;

    #@4b
    .line 1199
    move-object/from16 v0, p0

    #@4d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mActiveTimer:Ljava/util/Timer;

    #@4f
    new-instance v5, Landroid/view/ViewRootImpl$2;

    #@51
    move-object/from16 v0, p0

    #@53
    invoke-direct {v5, v0}, Landroid/view/ViewRootImpl$2;-><init>(Landroid/view/ViewRootImpl;)V

    #@56
    const-wide/16 v6, 0x0

    #@58
    move-object/from16 v0, p0

    #@5a
    iget v10, v0, Landroid/view/ViewRootImpl;->DURATION_OF_TIMER:I

    #@5c
    int-to-long v8, v10

    #@5d
    invoke-virtual/range {v4 .. v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    #@60
    .line 1205
    const/4 v4, 0x1

    #@61
    move-object/from16 v0, p0

    #@63
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsScheduled:Z

    #@65
    .line 1209
    .end local v58           #parent:Landroid/view/ViewGroup;
    :cond_65
    const/4 v4, 0x1

    #@66
    move-object/from16 v0, p0

    #@68
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsInTraversal:Z

    #@6a
    .line 1210
    const/4 v4, 0x1

    #@6b
    move-object/from16 v0, p0

    #@6d
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mWillDrawSoon:Z

    #@6f
    .line 1211
    const/16 v74, 0x0

    #@71
    .line 1212
    .local v74, windowSizeMayChange:Z
    const/16 v55, 0x0

    #@73
    .line 1213
    .local v55, newSurface:Z
    const/16 v65, 0x0

    #@75
    .line 1214
    .local v65, surfaceChanged:Z
    move-object/from16 v0, p0

    #@77
    iget-object v6, v0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@79
    .line 1219
    .local v6, lp:Landroid/view/WindowManager$LayoutParams;
    move-object/from16 v0, p0

    #@7b
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7d
    move-object/from16 v26, v0

    #@7f
    .line 1221
    .local v26, attachInfo:Landroid/view/View$AttachInfo;
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->getHostVisibility()I

    #@82
    move-result v68

    #@83
    .line 1222
    .local v68, viewVisibility:I
    move-object/from16 v0, p0

    #@85
    iget v4, v0, Landroid/view/ViewRootImpl;->mViewVisibility:I

    #@87
    move/from16 v0, v68

    #@89
    if-ne v4, v0, :cond_91

    #@8b
    move-object/from16 v0, p0

    #@8d
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mNewSurfaceNeeded:Z

    #@8f
    if-eqz v4, :cond_24c

    #@91
    :cond_91
    const/16 v69, 0x1

    #@93
    .line 1225
    .local v69, viewVisibilityChanged:Z
    :goto_93
    const/16 v57, 0x0

    #@95
    .line 1226
    .local v57, params:Landroid/view/WindowManager$LayoutParams;
    move-object/from16 v0, p0

    #@97
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z

    #@99
    if-eqz v4, :cond_a4

    #@9b
    .line 1227
    const/4 v4, 0x0

    #@9c
    move-object/from16 v0, p0

    #@9e
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z

    #@a0
    .line 1228
    const/16 v65, 0x1

    #@a2
    .line 1229
    move-object/from16 v57, v6

    #@a4
    .line 1231
    :cond_a4
    move-object/from16 v0, p0

    #@a6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@a8
    invoke-virtual {v4}, Landroid/view/CompatibilityInfoHolder;->get()Landroid/content/res/CompatibilityInfo;

    #@ab
    move-result-object v32

    #@ac
    .line 1232
    .local v32, compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    invoke-virtual/range {v32 .. v32}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@af
    move-result v4

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget-boolean v5, v0, Landroid/view/ViewRootImpl;->mLastInCompatMode:Z

    #@b4
    if-ne v4, v5, :cond_e9

    #@b6
    .line 1233
    move-object/from16 v57, v6

    #@b8
    .line 1234
    const/4 v4, 0x1

    #@b9
    move-object/from16 v0, p0

    #@bb
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@bd
    .line 1235
    const/4 v4, 0x1

    #@be
    move-object/from16 v0, p0

    #@c0
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@c2
    .line 1237
    invoke-virtual/range {v32 .. v32}, Landroid/content/res/CompatibilityInfo;->requiresWvgaAspect()Z

    #@c5
    move-result v4

    #@c6
    if-eqz v4, :cond_250

    #@c8
    .line 1238
    move-object/from16 v0, v57

    #@ca
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@cc
    or-int/lit16 v4, v4, 0x1000

    #@ce
    move-object/from16 v0, v57

    #@d0
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@d2
    .line 1244
    :goto_d2
    move-object/from16 v0, p0

    #@d4
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mLastInCompatMode:Z

    #@d6
    if-eqz v4, :cond_25c

    #@d8
    .line 1245
    move-object/from16 v0, v57

    #@da
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@dc
    const v5, -0x20000001

    #@df
    and-int/2addr v4, v5

    #@e0
    move-object/from16 v0, v57

    #@e2
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@e4
    .line 1246
    const/4 v4, 0x0

    #@e5
    move-object/from16 v0, p0

    #@e7
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLastInCompatMode:Z

    #@e9
    .line 1253
    :cond_e9
    :goto_e9
    const/4 v4, 0x0

    #@ea
    move-object/from16 v0, p0

    #@ec
    iput v4, v0, Landroid/view/ViewRootImpl;->mWindowAttributesChangesFlag:I

    #@ee
    .line 1255
    move-object/from16 v0, p0

    #@f0
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@f2
    move-object/from16 v40, v0

    #@f4
    .line 1256
    .local v40, frame:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@f6
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@f8
    if-eqz v4, :cond_288

    #@fa
    .line 1257
    const/4 v4, 0x1

    #@fb
    move-object/from16 v0, p0

    #@fd
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@ff
    .line 1258
    const/4 v4, 0x1

    #@100
    move-object/from16 v0, p0

    #@102
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@104
    .line 1260
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@106
    const/16 v5, 0x7de

    #@108
    if-ne v4, v5, :cond_26e

    #@10a
    .line 1262
    new-instance v63, Landroid/graphics/Point;

    #@10c
    invoke-direct/range {v63 .. v63}, Landroid/graphics/Point;-><init>()V

    #@10f
    .line 1263
    .local v63, size:Landroid/graphics/Point;
    move-object/from16 v0, p0

    #@111
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mDisplay:Landroid/view/Display;

    #@113
    move-object/from16 v0, v63

    #@115
    invoke-virtual {v4, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@118
    .line 1264
    move-object/from16 v0, v63

    #@11a
    iget v8, v0, Landroid/graphics/Point;->x:I

    #@11c
    .line 1265
    .local v8, desiredWindowWidth:I
    move-object/from16 v0, v63

    #@11e
    iget v9, v0, Landroid/graphics/Point;->y:I

    #@120
    .line 1277
    .end local v63           #size:Landroid/graphics/Point;
    .local v9, desiredWindowHeight:I
    :goto_120
    move-object/from16 v0, p0

    #@122
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@124
    move-object/from16 v0, v26

    #@126
    iput-object v4, v0, Landroid/view/View$AttachInfo;->mSurface:Landroid/view/Surface;

    #@128
    .line 1281
    const/4 v4, 0x1

    #@129
    move-object/from16 v0, v26

    #@12b
    iput-boolean v4, v0, Landroid/view/View$AttachInfo;->mUse32BitDrawingCache:Z

    #@12d
    .line 1282
    const/4 v4, 0x0

    #@12e
    move-object/from16 v0, v26

    #@130
    iput-boolean v4, v0, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@132
    .line 1283
    move/from16 v0, v68

    #@134
    move-object/from16 v1, v26

    #@136
    iput v0, v1, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@138
    .line 1284
    const/4 v4, 0x0

    #@139
    move-object/from16 v0, v26

    #@13b
    iput-boolean v4, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@13d
    .line 1285
    const/16 v69, 0x0

    #@13f
    .line 1286
    move-object/from16 v0, p0

    #@141
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@143
    invoke-virtual {v11}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@146
    move-result-object v5

    #@147
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@14a
    move-result-object v5

    #@14b
    invoke-virtual {v4, v5}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@14e
    .line 1287
    move-object/from16 v0, p0

    #@150
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@152
    iget v4, v4, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@154
    move-object/from16 v0, p0

    #@156
    iput v4, v0, Landroid/view/ViewRootImpl;->mLastSystemUiVisibility:I

    #@158
    .line 1289
    move-object/from16 v0, p0

    #@15a
    iget v4, v0, Landroid/view/ViewRootImpl;->mViewLayoutDirectionInitial:I

    #@15c
    const/4 v5, 0x2

    #@15d
    if-ne v4, v5, :cond_16a

    #@15f
    .line 1290
    move-object/from16 v0, p0

    #@161
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@163
    invoke-virtual {v4}, Landroid/content/res/Configuration;->getLayoutDirection()I

    #@166
    move-result v4

    #@167
    invoke-virtual {v11, v4}, Landroid/view/View;->setLayoutDirection(I)V

    #@16a
    .line 1292
    :cond_16a
    const/4 v4, 0x0

    #@16b
    move-object/from16 v0, v26

    #@16d
    invoke-virtual {v11, v0, v4}, Landroid/view/View;->dispatchAttachedToWindow(Landroid/view/View$AttachInfo;I)V

    #@170
    .line 1293
    move-object/from16 v0, p0

    #@172
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@174
    move-object/from16 v0, p0

    #@176
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@178
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@17a
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@17d
    .line 1294
    move-object/from16 v0, p0

    #@17f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@181
    invoke-virtual {v11, v4}, Landroid/view/View;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@184
    .line 1309
    :cond_184
    :goto_184
    if-eqz v69, :cond_1a7

    #@186
    .line 1310
    move/from16 v0, v68

    #@188
    move-object/from16 v1, v26

    #@18a
    iput v0, v1, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@18c
    .line 1311
    move/from16 v0, v68

    #@18e
    invoke-virtual {v11, v0}, Landroid/view/View;->dispatchWindowVisibilityChanged(I)V

    #@191
    .line 1312
    if-nez v68, :cond_199

    #@193
    move-object/from16 v0, p0

    #@195
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mNewSurfaceNeeded:Z

    #@197
    if-eqz v4, :cond_19c

    #@199
    .line 1313
    :cond_199
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->destroyHardwareResources()V

    #@19c
    .line 1315
    :cond_19c
    const/16 v4, 0x8

    #@19e
    move/from16 v0, v68

    #@1a0
    if-ne v0, v4, :cond_1a7

    #@1a2
    .line 1318
    const/4 v4, 0x0

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mHasHadWindowFocus:Z

    #@1a7
    .line 1323
    :cond_1a7
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@1aa
    move-result-object v4

    #@1ab
    move-object/from16 v0, v26

    #@1ad
    iget-object v5, v0, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@1af
    invoke-virtual {v4, v5}, Landroid/view/ViewRootImpl$RunQueue;->executeActions(Landroid/os/Handler;)V

    #@1b2
    .line 1325
    const/16 v49, 0x0

    #@1b4
    .line 1327
    .local v49, insetsChanged:Z
    move-object/from16 v0, p0

    #@1b6
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@1b8
    if-eqz v4, :cond_2aa

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mStopped:Z

    #@1be
    if-nez v4, :cond_2aa

    #@1c0
    const/16 v52, 0x1

    #@1c2
    .line 1328
    .local v52, layoutRequested:Z
    :goto_1c2
    if-eqz v52, :cond_1f5

    #@1c4
    .line 1330
    move-object/from16 v0, p0

    #@1c6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1c8
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@1cb
    move-result-object v4

    #@1cc
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1cf
    move-result-object v7

    #@1d0
    .line 1332
    .local v7, res:Landroid/content/res/Resources;
    move-object/from16 v0, p0

    #@1d2
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@1d4
    if-eqz v4, :cond_2b1

    #@1d6
    .line 1335
    move-object/from16 v0, p0

    #@1d8
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1da
    move-object/from16 v0, p0

    #@1dc
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mAddedTouchMode:Z

    #@1de
    if-nez v4, :cond_2ae

    #@1e0
    const/4 v4, 0x1

    #@1e1
    :goto_1e1
    iput-boolean v4, v5, Landroid/view/View$AttachInfo;->mInTouchMode:Z

    #@1e3
    .line 1336
    move-object/from16 v0, p0

    #@1e5
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mAddedTouchMode:Z

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    invoke-direct {v0, v4}, Landroid/view/ViewRootImpl;->ensureTouchModeLocally(Z)Z

    #@1ec
    :cond_1ec
    :goto_1ec
    move-object/from16 v4, p0

    #@1ee
    move-object v5, v11

    #@1ef
    .line 1365
    invoke-direct/range {v4 .. v9}, Landroid/view/ViewRootImpl;->measureHierarchy(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/content/res/Resources;II)Z

    #@1f2
    move-result v4

    #@1f3
    or-int v74, v74, v4

    #@1f5
    .line 1369
    .end local v7           #res:Landroid/content/res/Resources;
    :cond_1f5
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewRootImpl;->collectViewAttributes()Z

    #@1f8
    move-result v4

    #@1f9
    if-eqz v4, :cond_1fd

    #@1fb
    .line 1370
    move-object/from16 v57, v6

    #@1fd
    .line 1372
    :cond_1fd
    move-object/from16 v0, v26

    #@1ff
    iget-boolean v4, v0, Landroid/view/View$AttachInfo;->mForceReportNewAttributes:Z

    #@201
    if-eqz v4, :cond_20a

    #@203
    .line 1373
    const/4 v4, 0x0

    #@204
    move-object/from16 v0, v26

    #@206
    iput-boolean v4, v0, Landroid/view/View$AttachInfo;->mForceReportNewAttributes:Z

    #@208
    .line 1374
    move-object/from16 v57, v6

    #@20a
    .line 1377
    :cond_20a
    move-object/from16 v0, p0

    #@20c
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@20e
    if-nez v4, :cond_216

    #@210
    move-object/from16 v0, v26

    #@212
    iget-boolean v4, v0, Landroid/view/View$AttachInfo;->mViewVisibilityChanged:Z

    #@214
    if-eqz v4, :cond_32e

    #@216
    .line 1378
    :cond_216
    const/4 v4, 0x0

    #@217
    move-object/from16 v0, v26

    #@219
    iput-boolean v4, v0, Landroid/view/View$AttachInfo;->mViewVisibilityChanged:Z

    #@21b
    .line 1379
    move-object/from16 v0, p0

    #@21d
    iget v4, v0, Landroid/view/ViewRootImpl;->mSoftInputMode:I

    #@21f
    and-int/lit16 v0, v4, 0xf0

    #@221
    move/from16 v60, v0

    #@223
    .line 1383
    .local v60, resizeMode:I
    if-nez v60, :cond_32e

    #@225
    .line 1384
    move-object/from16 v0, v26

    #@227
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@229
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@22c
    move-result v24

    #@22d
    .line 1385
    .local v24, N:I
    const/16 v45, 0x0

    #@22f
    .local v45, i:I
    :goto_22f
    move/from16 v0, v45

    #@231
    move/from16 v1, v24

    #@233
    if-ge v0, v1, :cond_318

    #@235
    .line 1386
    move-object/from16 v0, v26

    #@237
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@239
    move/from16 v0, v45

    #@23b
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@23e
    move-result-object v4

    #@23f
    check-cast v4, Landroid/view/View;

    #@241
    invoke-virtual {v4}, Landroid/view/View;->isShown()Z

    #@244
    move-result v4

    #@245
    if-eqz v4, :cond_249

    #@247
    .line 1387
    const/16 v60, 0x10

    #@249
    .line 1385
    :cond_249
    add-int/lit8 v45, v45, 0x1

    #@24b
    goto :goto_22f

    #@24c
    .line 1222
    .end local v8           #desiredWindowWidth:I
    .end local v9           #desiredWindowHeight:I
    .end local v24           #N:I
    .end local v32           #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .end local v40           #frame:Landroid/graphics/Rect;
    .end local v45           #i:I
    .end local v49           #insetsChanged:Z
    .end local v52           #layoutRequested:Z
    .end local v57           #params:Landroid/view/WindowManager$LayoutParams;
    .end local v60           #resizeMode:I
    .end local v69           #viewVisibilityChanged:Z
    :cond_24c
    const/16 v69, 0x0

    #@24e
    goto/16 :goto_93

    #@250
    .line 1241
    .restart local v32       #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .restart local v57       #params:Landroid/view/WindowManager$LayoutParams;
    .restart local v69       #viewVisibilityChanged:Z
    :cond_250
    move-object/from16 v0, v57

    #@252
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@254
    and-int/lit16 v4, v4, -0x1001

    #@256
    move-object/from16 v0, v57

    #@258
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@25a
    goto/16 :goto_d2

    #@25c
    .line 1248
    :cond_25c
    move-object/from16 v0, v57

    #@25e
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@260
    const/high16 v5, 0x2000

    #@262
    or-int/2addr v4, v5

    #@263
    move-object/from16 v0, v57

    #@265
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@267
    .line 1249
    const/4 v4, 0x1

    #@268
    move-object/from16 v0, p0

    #@26a
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLastInCompatMode:Z

    #@26c
    goto/16 :goto_e9

    #@26e
    .line 1267
    .restart local v40       #frame:Landroid/graphics/Rect;
    :cond_26e
    move-object/from16 v0, p0

    #@270
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@272
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@275
    move-result-object v4

    #@276
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@279
    move-result-object v4

    #@27a
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@27d
    move-result-object v56

    #@27e
    .line 1269
    .local v56, packageMetrics:Landroid/util/DisplayMetrics;
    move-object/from16 v0, v56

    #@280
    iget v8, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@282
    .line 1270
    .restart local v8       #desiredWindowWidth:I
    move-object/from16 v0, v56

    #@284
    iget v9, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@286
    .restart local v9       #desiredWindowHeight:I
    goto/16 :goto_120

    #@288
    .line 1298
    .end local v8           #desiredWindowWidth:I
    .end local v9           #desiredWindowHeight:I
    .end local v56           #packageMetrics:Landroid/util/DisplayMetrics;
    :cond_288
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->width()I

    #@28b
    move-result v8

    #@28c
    .line 1299
    .restart local v8       #desiredWindowWidth:I
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->height()I

    #@28f
    move-result v9

    #@290
    .line 1300
    .restart local v9       #desiredWindowHeight:I
    move-object/from16 v0, p0

    #@292
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@294
    if-ne v8, v4, :cond_29c

    #@296
    move-object/from16 v0, p0

    #@298
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@29a
    if-eq v9, v4, :cond_184

    #@29c
    .line 1303
    :cond_29c
    const/4 v4, 0x1

    #@29d
    move-object/from16 v0, p0

    #@29f
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@2a1
    .line 1304
    const/4 v4, 0x1

    #@2a2
    move-object/from16 v0, p0

    #@2a4
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@2a6
    .line 1305
    const/16 v74, 0x1

    #@2a8
    goto/16 :goto_184

    #@2aa
    .line 1327
    .restart local v49       #insetsChanged:Z
    :cond_2aa
    const/16 v52, 0x0

    #@2ac
    goto/16 :goto_1c2

    #@2ae
    .line 1335
    .restart local v7       #res:Landroid/content/res/Resources;
    .restart local v52       #layoutRequested:Z
    :cond_2ae
    const/4 v4, 0x0

    #@2af
    goto/16 :goto_1e1

    #@2b1
    .line 1338
    :cond_2b1
    move-object/from16 v0, p0

    #@2b3
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@2b5
    move-object/from16 v0, p0

    #@2b7
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2b9
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@2bb
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@2be
    move-result v4

    #@2bf
    if-nez v4, :cond_2c3

    #@2c1
    .line 1339
    const/16 v49, 0x1

    #@2c3
    .line 1341
    :cond_2c3
    move-object/from16 v0, p0

    #@2c5
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@2c7
    move-object/from16 v0, p0

    #@2c9
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2cb
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@2cd
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@2d0
    move-result v4

    #@2d1
    if-nez v4, :cond_2e0

    #@2d3
    .line 1342
    move-object/from16 v0, p0

    #@2d5
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2d7
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@2d9
    move-object/from16 v0, p0

    #@2db
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@2dd
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@2e0
    .line 1346
    :cond_2e0
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    #@2e2
    const/4 v5, -0x2

    #@2e3
    if-eq v4, v5, :cond_2ea

    #@2e5
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    #@2e7
    const/4 v5, -0x2

    #@2e8
    if-ne v4, v5, :cond_1ec

    #@2ea
    .line 1348
    :cond_2ea
    const/16 v74, 0x1

    #@2ec
    .line 1350
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2ee
    const/16 v5, 0x7de

    #@2f0
    if-ne v4, v5, :cond_30a

    #@2f2
    .line 1352
    new-instance v63, Landroid/graphics/Point;

    #@2f4
    invoke-direct/range {v63 .. v63}, Landroid/graphics/Point;-><init>()V

    #@2f7
    .line 1353
    .restart local v63       #size:Landroid/graphics/Point;
    move-object/from16 v0, p0

    #@2f9
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mDisplay:Landroid/view/Display;

    #@2fb
    move-object/from16 v0, v63

    #@2fd
    invoke-virtual {v4, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@300
    .line 1354
    move-object/from16 v0, v63

    #@302
    iget v8, v0, Landroid/graphics/Point;->x:I

    #@304
    .line 1355
    move-object/from16 v0, v63

    #@306
    iget v9, v0, Landroid/graphics/Point;->y:I

    #@308
    .line 1356
    goto/16 :goto_1ec

    #@30a
    .line 1357
    .end local v63           #size:Landroid/graphics/Point;
    :cond_30a
    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@30d
    move-result-object v56

    #@30e
    .line 1358
    .restart local v56       #packageMetrics:Landroid/util/DisplayMetrics;
    move-object/from16 v0, v56

    #@310
    iget v8, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@312
    .line 1359
    move-object/from16 v0, v56

    #@314
    iget v9, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@316
    goto/16 :goto_1ec

    #@318
    .line 1390
    .end local v7           #res:Landroid/content/res/Resources;
    .end local v56           #packageMetrics:Landroid/util/DisplayMetrics;
    .restart local v24       #N:I
    .restart local v45       #i:I
    .restart local v60       #resizeMode:I
    :cond_318
    if-nez v60, :cond_31c

    #@31a
    .line 1391
    const/16 v60, 0x20

    #@31c
    .line 1393
    :cond_31c
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@31e
    and-int/lit16 v4, v4, 0xf0

    #@320
    move/from16 v0, v60

    #@322
    if-eq v4, v0, :cond_32e

    #@324
    .line 1395
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@326
    and-int/lit16 v4, v4, -0xf1

    #@328
    or-int v4, v4, v60

    #@32a
    iput v4, v6, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@32c
    .line 1398
    move-object/from16 v57, v6

    #@32e
    .line 1403
    .end local v24           #N:I
    .end local v45           #i:I
    .end local v60           #resizeMode:I
    :cond_32e
    if-eqz v57, :cond_345

    #@330
    iget v4, v11, Landroid/view/View;->mPrivateFlags:I

    #@332
    and-int/lit16 v4, v4, 0x200

    #@334
    if-eqz v4, :cond_345

    #@336
    .line 1404
    move-object/from16 v0, v57

    #@338
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@33a
    invoke-static {v4}, Landroid/graphics/PixelFormat;->formatHasAlpha(I)Z

    #@33d
    move-result v4

    #@33e
    if-nez v4, :cond_345

    #@340
    .line 1405
    const/4 v4, -0x3

    #@341
    move-object/from16 v0, v57

    #@343
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@345
    .line 1409
    :cond_345
    move-object/from16 v0, p0

    #@347
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsRequested:Z

    #@349
    if-eqz v4, :cond_381

    #@34b
    .line 1410
    const/4 v4, 0x0

    #@34c
    move-object/from16 v0, p0

    #@34e
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsRequested:Z

    #@350
    .line 1411
    move-object/from16 v0, p0

    #@352
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@354
    move-object/from16 v0, p0

    #@356
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@358
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@35a
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@35d
    .line 1412
    move-object/from16 v0, p0

    #@35f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@361
    invoke-virtual {v11, v4}, Landroid/view/View;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@364
    .line 1413
    move-object/from16 v0, p0

    #@366
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@368
    if-eqz v4, :cond_381

    #@36a
    .line 1417
    move-object/from16 v0, p0

    #@36c
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@36e
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@371
    move-result-object v4

    #@372
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@375
    move-result-object v13

    #@376
    move-object/from16 v10, p0

    #@378
    move-object v12, v6

    #@379
    move v14, v8

    #@37a
    move v15, v9

    #@37b
    invoke-direct/range {v10 .. v15}, Landroid/view/ViewRootImpl;->measureHierarchy(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/content/res/Resources;II)Z

    #@37e
    move-result v4

    #@37f
    or-int v74, v74, v4

    #@381
    .line 1423
    :cond_381
    if-eqz v52, :cond_388

    #@383
    .line 1427
    const/4 v4, 0x0

    #@384
    move-object/from16 v0, p0

    #@386
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@388
    .line 1430
    :cond_388
    if-eqz v52, :cond_6e7

    #@38a
    if-eqz v74, :cond_6e7

    #@38c
    move-object/from16 v0, p0

    #@38e
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@390
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    #@393
    move-result v5

    #@394
    if-ne v4, v5, :cond_3ca

    #@396
    move-object/from16 v0, p0

    #@398
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@39a
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    #@39d
    move-result v5

    #@39e
    if-ne v4, v5, :cond_3ca

    #@3a0
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    #@3a2
    const/4 v5, -0x2

    #@3a3
    if-ne v4, v5, :cond_3b5

    #@3a5
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->width()I

    #@3a8
    move-result v4

    #@3a9
    if-ge v4, v8, :cond_3b5

    #@3ab
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->width()I

    #@3ae
    move-result v4

    #@3af
    move-object/from16 v0, p0

    #@3b1
    iget v5, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@3b3
    if-ne v4, v5, :cond_3ca

    #@3b5
    :cond_3b5
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    #@3b7
    const/4 v5, -0x2

    #@3b8
    if-ne v4, v5, :cond_6e7

    #@3ba
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->height()I

    #@3bd
    move-result v4

    #@3be
    if-ge v4, v9, :cond_6e7

    #@3c0
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->height()I

    #@3c3
    move-result v4

    #@3c4
    move-object/from16 v0, p0

    #@3c6
    iget v5, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@3c8
    if-eq v4, v5, :cond_6e7

    #@3ca
    :cond_3ca
    const/16 v73, 0x1

    #@3cc
    .line 1437
    .local v73, windowShouldResize:Z
    :goto_3cc
    move-object/from16 v0, v26

    #@3ce
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@3d0
    invoke-virtual {v4}, Landroid/view/ViewTreeObserver;->hasComputeInternalInsetsListeners()Z

    #@3d3
    move-result v34

    #@3d4
    .line 1440
    .local v34, computesInternalInsets:Z
    const/16 v50, 0x0

    #@3d6
    .line 1441
    .local v50, insetsPending:Z
    const/16 v59, 0x0

    #@3d8
    .line 1443
    .local v59, relayoutResult:I
    move-object/from16 v0, p0

    #@3da
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@3dc
    if-nez v4, :cond_3e6

    #@3de
    if-nez v73, :cond_3e6

    #@3e0
    if-nez v49, :cond_3e6

    #@3e2
    if-nez v69, :cond_3e6

    #@3e4
    if-eqz v57, :cond_c44

    #@3e6
    .line 1446
    :cond_3e6
    if-nez v68, :cond_3f4

    #@3e8
    .line 1456
    if-eqz v34, :cond_6eb

    #@3ea
    move-object/from16 v0, p0

    #@3ec
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@3ee
    if-nez v4, :cond_3f2

    #@3f0
    if-eqz v69, :cond_6eb

    #@3f2
    :cond_3f2
    const/16 v50, 0x1

    #@3f4
    .line 1459
    :cond_3f4
    :goto_3f4
    move-object/from16 v0, p0

    #@3f6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@3f8
    if-eqz v4, :cond_408

    #@3fa
    .line 1460
    move-object/from16 v0, p0

    #@3fc
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@3fe
    iget-object v4, v4, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@400
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@403
    .line 1461
    const/4 v4, 0x1

    #@404
    move-object/from16 v0, p0

    #@406
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mDrawingAllowed:Z

    #@408
    .line 1464
    :cond_408
    const/16 v43, 0x0

    #@40a
    .line 1465
    .local v43, hwInitialized:Z
    const/16 v35, 0x0

    #@40c
    .line 1467
    .local v35, contentInsetsChanged:Z
    move-object/from16 v0, p0

    #@40e
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@410
    invoke-virtual {v4}, Landroid/view/Surface;->isValid()Z

    #@413
    move-result v41

    #@414
    .line 1475
    .local v41, hadSurface:Z
    :try_start_414
    move-object/from16 v0, p0

    #@416
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@418
    invoke-virtual {v4}, Landroid/view/Surface;->getGenerationId()I

    #@41b
    move-result v66

    #@41c
    .line 1476
    .local v66, surfaceGenerationId:I
    move-object/from16 v0, p0

    #@41e
    move-object/from16 v1, v57

    #@420
    move/from16 v2, v68

    #@422
    move/from16 v3, v50

    #@424
    invoke-direct {v0, v1, v2, v3}, Landroid/view/ViewRootImpl;->relayoutWindow(Landroid/view/WindowManager$LayoutParams;IZ)I

    #@427
    move-result v59

    #@428
    .line 1483
    move-object/from16 v0, p0

    #@42a
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@42c
    iget v4, v4, Landroid/content/res/Configuration;->seq:I

    #@42e
    if-eqz v4, :cond_447

    #@430
    .line 1486
    move-object/from16 v0, p0

    #@432
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@434
    move-object/from16 v0, p0

    #@436
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@438
    if-nez v4, :cond_6ef

    #@43a
    const/4 v4, 0x1

    #@43b
    :goto_43b
    move-object/from16 v0, p0

    #@43d
    invoke-virtual {v0, v5, v4}, Landroid/view/ViewRootImpl;->updateConfiguration(Landroid/content/res/Configuration;Z)V

    #@440
    .line 1487
    move-object/from16 v0, p0

    #@442
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@444
    const/4 v5, 0x0

    #@445
    iput v5, v4, Landroid/content/res/Configuration;->seq:I

    #@447
    .line 1490
    :cond_447
    move-object/from16 v0, p0

    #@449
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@44b
    move-object/from16 v0, p0

    #@44d
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@44f
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@451
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@454
    move-result v4

    #@455
    if-nez v4, :cond_6f2

    #@457
    const/16 v35, 0x1

    #@459
    .line 1492
    :goto_459
    move-object/from16 v0, p0

    #@45b
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@45d
    move-object/from16 v0, p0

    #@45f
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@461
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@463
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@466
    move-result v4

    #@467
    if-nez v4, :cond_6f6

    #@469
    const/16 v70, 0x1

    #@46b
    .line 1494
    .local v70, visibleInsetsChanged:Z
    :goto_46b
    if-eqz v35, :cond_5bd

    #@46d
    .line 1495
    move-object/from16 v0, p0

    #@46f
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@471
    if-lez v4, :cond_5b0

    #@473
    move-object/from16 v0, p0

    #@475
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@477
    if-lez v4, :cond_5b0

    #@479
    if-eqz v6, :cond_5b0

    #@47b
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@47d
    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@47f
    or-int/2addr v4, v5

    #@480
    and-int/lit16 v4, v4, 0x600

    #@482
    if-nez v4, :cond_5b0

    #@484
    move-object/from16 v0, p0

    #@486
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@488
    if-eqz v4, :cond_5b0

    #@48a
    move-object/from16 v0, p0

    #@48c
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@48e
    invoke-virtual {v4}, Landroid/view/Surface;->isValid()Z

    #@491
    move-result v4

    #@492
    if-eqz v4, :cond_5b0

    #@494
    move-object/from16 v0, p0

    #@496
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@498
    iget-boolean v4, v4, Landroid/view/View$AttachInfo;->mTurnOffWindowResizeAnim:Z

    #@49a
    if-nez v4, :cond_5b0

    #@49c
    move-object/from16 v0, p0

    #@49e
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4a0
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4a2
    if-eqz v4, :cond_5b0

    #@4a4
    move-object/from16 v0, p0

    #@4a6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4a8
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4aa
    invoke-virtual {v4}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@4ad
    move-result v4

    #@4ae
    if-eqz v4, :cond_5b0

    #@4b0
    move-object/from16 v0, p0

    #@4b2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4b4
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4b6
    invoke-virtual {v4}, Landroid/view/HardwareRenderer;->validate()Z

    #@4b9
    move-result v4

    #@4ba
    if-eqz v4, :cond_5b0

    #@4bc
    if-eqz v6, :cond_5b0

    #@4be
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->format:I

    #@4c0
    invoke-static {v4}, Landroid/graphics/PixelFormat;->formatHasAlpha(I)Z

    #@4c3
    move-result v4

    #@4c4
    if-nez v4, :cond_5b0

    #@4c6
    .line 1505
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->disposeResizeBuffer()V

    #@4c9
    .line 1507
    const/16 v33, 0x0

    #@4cb
    .line 1508
    .local v33, completed:Z
    move-object/from16 v0, p0

    #@4cd
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4cf
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4d1
    invoke-virtual {v4}, Landroid/view/HardwareRenderer;->getCanvas()Landroid/view/HardwareCanvas;
    :try_end_4d4
    .catch Landroid/os/RemoteException; {:try_start_414 .. :try_end_4d4} :catch_755

    #@4d4
    move-result-object v44

    #@4d5
    .line 1509
    .local v44, hwRendererCanvas:Landroid/view/HardwareCanvas;
    const/16 v51, 0x0

    #@4d7
    .line 1511
    .local v51, layerCanvas:Landroid/view/HardwareCanvas;
    :try_start_4d7
    move-object/from16 v0, p0

    #@4d9
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@4db
    if-nez v4, :cond_6fa

    #@4dd
    .line 1512
    move-object/from16 v0, p0

    #@4df
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4e1
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4e3
    move-object/from16 v0, p0

    #@4e5
    iget v5, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@4e7
    move-object/from16 v0, p0

    #@4e9
    iget v10, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@4eb
    const/4 v12, 0x0

    #@4ec
    invoke-virtual {v4, v5, v10, v12}, Landroid/view/HardwareRenderer;->createHardwareLayer(IIZ)Landroid/view/HardwareLayer;

    #@4ef
    move-result-object v4

    #@4f0
    move-object/from16 v0, p0

    #@4f2
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@4f4
    .line 1519
    :cond_4f4
    :goto_4f4
    move-object/from16 v0, p0

    #@4f6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@4f8
    move-object/from16 v0, v44

    #@4fa
    invoke-virtual {v4, v0}, Landroid/view/HardwareLayer;->start(Landroid/graphics/Canvas;)Landroid/view/HardwareCanvas;

    #@4fd
    move-result-object v51

    #@4fe
    .line 1520
    move-object/from16 v0, p0

    #@500
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@502
    move-object/from16 v0, p0

    #@504
    iget v5, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@506
    move-object/from16 v0, v51

    #@508
    invoke-virtual {v0, v4, v5}, Landroid/view/HardwareCanvas;->setViewport(II)V

    #@50b
    .line 1521
    const/4 v4, 0x0

    #@50c
    move-object/from16 v0, v51

    #@50e
    invoke-virtual {v0, v4}, Landroid/view/HardwareCanvas;->onPreDraw(Landroid/graphics/Rect;)I

    #@511
    .line 1522
    invoke-virtual/range {v51 .. v51}, Landroid/view/HardwareCanvas;->save()I

    #@514
    move-result v61

    #@515
    .line 1525
    .local v61, restoreCount:I
    move-object/from16 v0, p0

    #@517
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@519
    if-eqz v4, :cond_758

    #@51b
    move-object/from16 v0, p0

    #@51d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@51f
    invoke-virtual {v4}, Landroid/widget/Scroller;->computeScrollOffset()Z

    #@522
    move-result v4

    #@523
    if-eqz v4, :cond_758

    #@525
    const/16 v62, 0x1

    #@527
    .line 1527
    .local v62, scrolling:Z
    :goto_527
    if-eqz v62, :cond_75c

    #@529
    .line 1528
    move-object/from16 v0, p0

    #@52b
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@52d
    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    #@530
    move-result v75

    #@531
    .line 1529
    .local v75, yoff:I
    move-object/from16 v0, p0

    #@533
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@535
    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    #@538
    .line 1534
    :goto_538
    const/4 v4, 0x0

    #@539
    move/from16 v0, v75

    #@53b
    neg-int v5, v0

    #@53c
    int-to-float v5, v5

    #@53d
    move-object/from16 v0, v51

    #@53f
    invoke-virtual {v0, v4, v5}, Landroid/view/HardwareCanvas;->translate(FF)V

    #@542
    .line 1535
    move-object/from16 v0, p0

    #@544
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@546
    if-eqz v4, :cond_551

    #@548
    .line 1536
    move-object/from16 v0, p0

    #@54a
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@54c
    move-object/from16 v0, v51

    #@54e
    invoke-virtual {v4, v0}, Landroid/content/res/CompatibilityInfo$Translator;->translateCanvas(Landroid/graphics/Canvas;)V

    #@551
    .line 1539
    :cond_551
    move-object/from16 v0, p0

    #@553
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@555
    iget-object v0, v4, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@557
    move-object/from16 v37, v0

    #@559
    .line 1540
    .local v37, displayList:Landroid/view/DisplayList;
    if-eqz v37, :cond_764

    #@55b
    .line 1541
    const/4 v4, 0x0

    #@55c
    const/4 v5, 0x1

    #@55d
    move-object/from16 v0, v51

    #@55f
    move-object/from16 v1, v37

    #@561
    invoke-virtual {v0, v1, v4, v5}, Landroid/view/HardwareCanvas;->drawDisplayList(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I

    #@564
    .line 1547
    :goto_564
    move-object/from16 v0, p0

    #@566
    move-object/from16 v1, v51

    #@568
    invoke-direct {v0, v1}, Landroid/view/ViewRootImpl;->drawAccessibilityFocusedDrawableIfNeeded(Landroid/graphics/Canvas;)V

    #@56b
    .line 1549
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@56e
    move-result-wide v4

    #@56f
    move-object/from16 v0, p0

    #@571
    iput-wide v4, v0, Landroid/view/ViewRootImpl;->mResizeBufferStartTime:J

    #@573
    .line 1550
    move-object/from16 v0, p0

    #@575
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@577
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@57a
    move-result-object v4

    #@57b
    const/high16 v5, 0x10e

    #@57d
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    #@580
    move-result v4

    #@581
    move-object/from16 v0, p0

    #@583
    iput v4, v0, Landroid/view/ViewRootImpl;->mResizeBufferDuration:I

    #@585
    .line 1552
    const/16 v33, 0x1

    #@587
    .line 1554
    move-object/from16 v0, v51

    #@589
    move/from16 v1, v61

    #@58b
    invoke-virtual {v0, v1}, Landroid/view/HardwareCanvas;->restoreToCount(I)V
    :try_end_58e
    .catchall {:try_start_4d7 .. :try_end_58e} :catchall_76f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4d7 .. :try_end_58e} :catch_727

    #@58e
    .line 1558
    if-eqz v51, :cond_593

    #@590
    .line 1559
    :try_start_590
    invoke-virtual/range {v51 .. v51}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@593
    .line 1561
    :cond_593
    move-object/from16 v0, p0

    #@595
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@597
    if-eqz v4, :cond_5b0

    #@599
    .line 1562
    move-object/from16 v0, p0

    #@59b
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@59d
    move-object/from16 v0, v44

    #@59f
    invoke-virtual {v4, v0}, Landroid/view/HardwareLayer;->end(Landroid/graphics/Canvas;)V

    #@5a2
    .line 1563
    if-nez v33, :cond_5b0

    #@5a4
    .line 1564
    move-object/from16 v0, p0

    #@5a6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@5a8
    invoke-virtual {v4}, Landroid/view/HardwareLayer;->destroy()V

    #@5ab
    .line 1565
    const/4 v4, 0x0

    #@5ac
    move-object/from16 v0, p0

    #@5ae
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@5b0
    .line 1570
    .end local v33           #completed:Z
    .end local v37           #displayList:Landroid/view/DisplayList;
    .end local v44           #hwRendererCanvas:Landroid/view/HardwareCanvas;
    .end local v51           #layerCanvas:Landroid/view/HardwareCanvas;
    .end local v61           #restoreCount:I
    .end local v62           #scrolling:Z
    .end local v75           #yoff:I
    :cond_5b0
    :goto_5b0
    move-object/from16 v0, p0

    #@5b2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5b4
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@5b6
    move-object/from16 v0, p0

    #@5b8
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@5ba
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5bd
    .line 1574
    :cond_5bd
    if-nez v35, :cond_5d1

    #@5bf
    move-object/from16 v0, p0

    #@5c1
    iget v4, v0, Landroid/view/ViewRootImpl;->mLastSystemUiVisibility:I

    #@5c3
    move-object/from16 v0, p0

    #@5c5
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5c7
    iget v5, v5, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@5c9
    if-ne v4, v5, :cond_5d1

    #@5cb
    move-object/from16 v0, p0

    #@5cd
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsRequested:Z

    #@5cf
    if-eqz v4, :cond_5f4

    #@5d1
    .line 1576
    :cond_5d1
    move-object/from16 v0, p0

    #@5d3
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5d5
    iget v4, v4, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@5d7
    move-object/from16 v0, p0

    #@5d9
    iput v4, v0, Landroid/view/ViewRootImpl;->mLastSystemUiVisibility:I

    #@5db
    .line 1577
    const/4 v4, 0x0

    #@5dc
    move-object/from16 v0, p0

    #@5de
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsRequested:Z

    #@5e0
    .line 1578
    move-object/from16 v0, p0

    #@5e2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@5e4
    move-object/from16 v0, p0

    #@5e6
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5e8
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@5ea
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5ed
    .line 1579
    move-object/from16 v0, p0

    #@5ef
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mFitSystemWindowsInsets:Landroid/graphics/Rect;

    #@5f1
    invoke-virtual {v11, v4}, Landroid/view/View;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@5f4
    .line 1581
    :cond_5f4
    if-eqz v70, :cond_603

    #@5f6
    .line 1582
    move-object/from16 v0, p0

    #@5f8
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5fa
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@5fc
    move-object/from16 v0, p0

    #@5fe
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@600
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@603
    .line 1587
    :cond_603
    if-nez v41, :cond_7c8

    #@605
    .line 1588
    move-object/from16 v0, p0

    #@607
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@609
    invoke-virtual {v4}, Landroid/view/Surface;->isValid()Z

    #@60c
    move-result v4

    #@60d
    if-eqz v4, :cond_637

    #@60f
    .line 1596
    const/16 v55, 0x1

    #@611
    .line 1597
    const/4 v4, 0x1

    #@612
    move-object/from16 v0, p0

    #@614
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@616
    .line 1598
    move-object/from16 v0, p0

    #@618
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPreviousTransparentRegion:Landroid/graphics/Region;

    #@61a
    invoke-virtual {v4}, Landroid/graphics/Region;->setEmpty()V

    #@61d
    .line 1600
    move-object/from16 v0, p0

    #@61f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@621
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;
    :try_end_623
    .catch Landroid/os/RemoteException; {:try_start_590 .. :try_end_623} :catch_755

    #@623
    if-eqz v4, :cond_637

    #@625
    .line 1602
    :try_start_625
    move-object/from16 v0, p0

    #@627
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@629
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@62b
    move-object/from16 v0, p0

    #@62d
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mHolder:Landroid/view/SurfaceHolder;

    #@62f
    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@632
    move-result-object v5

    #@633
    invoke-virtual {v4, v5}, Landroid/view/HardwareRenderer;->initialize(Landroid/view/Surface;)Z
    :try_end_636
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_625 .. :try_end_636} :catch_793
    .catch Landroid/os/RemoteException; {:try_start_625 .. :try_end_636} :catch_755

    #@636
    move-result v43

    #@637
    .line 1658
    .end local v66           #surfaceGenerationId:I
    .end local v70           #visibleInsetsChanged:Z
    :cond_637
    :goto_637
    move-object/from16 v0, v40

    #@639
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@63b
    move-object/from16 v0, v26

    #@63d
    iput v4, v0, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@63f
    .line 1659
    move-object/from16 v0, v40

    #@641
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@643
    move-object/from16 v0, v26

    #@645
    iput v4, v0, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@647
    .line 1664
    move-object/from16 v0, p0

    #@649
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@64b
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->width()I

    #@64e
    move-result v5

    #@64f
    if-ne v4, v5, :cond_65b

    #@651
    move-object/from16 v0, p0

    #@653
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@655
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->height()I

    #@658
    move-result v5

    #@659
    if-eq v4, v5, :cond_66b

    #@65b
    .line 1665
    :cond_65b
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->width()I

    #@65e
    move-result v4

    #@65f
    move-object/from16 v0, p0

    #@661
    iput v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@663
    .line 1666
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Rect;->height()I

    #@666
    move-result v4

    #@667
    move-object/from16 v0, p0

    #@669
    iput v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@66b
    .line 1669
    :cond_66b
    move-object/from16 v0, p0

    #@66d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@66f
    if-eqz v4, :cond_8cb

    #@671
    .line 1671
    move-object/from16 v0, p0

    #@673
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@675
    invoke-virtual {v4}, Landroid/view/Surface;->isValid()Z

    #@678
    move-result v4

    #@679
    if-eqz v4, :cond_685

    #@67b
    .line 1674
    move-object/from16 v0, p0

    #@67d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@67f
    move-object/from16 v0, p0

    #@681
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@683
    iput-object v5, v4, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@685
    .line 1676
    :cond_685
    move-object/from16 v0, p0

    #@687
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@689
    move-object/from16 v0, p0

    #@68b
    iget v5, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@68d
    move-object/from16 v0, p0

    #@68f
    iget v10, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@691
    invoke-virtual {v4, v5, v10}, Lcom/android/internal/view/BaseSurfaceHolder;->setSurfaceFrameSize(II)V

    #@694
    .line 1677
    move-object/from16 v0, p0

    #@696
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@698
    iget-object v4, v4, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@69a
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@69d
    .line 1678
    move-object/from16 v0, p0

    #@69f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@6a1
    invoke-virtual {v4}, Landroid/view/Surface;->isValid()Z

    #@6a4
    move-result v4

    #@6a5
    if-eqz v4, :cond_bdc

    #@6a7
    .line 1679
    if-nez v41, :cond_87e

    #@6a9
    .line 1680
    move-object/from16 v0, p0

    #@6ab
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@6ad
    invoke-virtual {v4}, Lcom/android/internal/view/BaseSurfaceHolder;->ungetCallbacks()V

    #@6b0
    .line 1682
    const/4 v4, 0x1

    #@6b1
    move-object/from16 v0, p0

    #@6b3
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsCreating:Z

    #@6b5
    .line 1683
    move-object/from16 v0, p0

    #@6b7
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

    #@6b9
    move-object/from16 v0, p0

    #@6bb
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@6bd
    invoke-interface {v4, v5}, Landroid/view/SurfaceHolder$Callback2;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    #@6c0
    .line 1684
    move-object/from16 v0, p0

    #@6c2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@6c4
    invoke-virtual {v4}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@6c7
    move-result-object v28

    #@6c8
    .line 1685
    .local v28, callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v28, :cond_87c

    #@6ca
    .line 1686
    move-object/from16 v25, v28

    #@6cc
    .local v25, arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v25

    #@6ce
    array-length v0, v0

    #@6cf
    move/from16 v53, v0

    #@6d1
    .local v53, len$:I
    const/16 v46, 0x0

    #@6d3
    .local v46, i$:I
    :goto_6d3
    move/from16 v0, v46

    #@6d5
    move/from16 v1, v53

    #@6d7
    if-ge v0, v1, :cond_87c

    #@6d9
    aget-object v27, v25, v46

    #@6db
    .line 1687
    .local v27, c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@6dd
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@6df
    move-object/from16 v0, v27

    #@6e1
    invoke-interface {v0, v4}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    #@6e4
    .line 1686
    add-int/lit8 v46, v46, 0x1

    #@6e6
    goto :goto_6d3

    #@6e7
    .line 1430
    .end local v25           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v27           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v28           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v34           #computesInternalInsets:Z
    .end local v35           #contentInsetsChanged:Z
    .end local v41           #hadSurface:Z
    .end local v43           #hwInitialized:Z
    .end local v46           #i$:I
    .end local v50           #insetsPending:Z
    .end local v53           #len$:I
    .end local v59           #relayoutResult:I
    .end local v73           #windowShouldResize:Z
    :cond_6e7
    const/16 v73, 0x0

    #@6e9
    goto/16 :goto_3cc

    #@6eb
    .line 1456
    .restart local v34       #computesInternalInsets:Z
    .restart local v50       #insetsPending:Z
    .restart local v59       #relayoutResult:I
    .restart local v73       #windowShouldResize:Z
    :cond_6eb
    const/16 v50, 0x0

    #@6ed
    goto/16 :goto_3f4

    #@6ef
    .line 1486
    .restart local v35       #contentInsetsChanged:Z
    .restart local v41       #hadSurface:Z
    .restart local v43       #hwInitialized:Z
    .restart local v66       #surfaceGenerationId:I
    :cond_6ef
    const/4 v4, 0x0

    #@6f0
    goto/16 :goto_43b

    #@6f2
    .line 1490
    :cond_6f2
    const/16 v35, 0x0

    #@6f4
    goto/16 :goto_459

    #@6f6
    .line 1492
    :cond_6f6
    const/16 v70, 0x0

    #@6f8
    goto/16 :goto_46b

    #@6fa
    .line 1514
    .restart local v33       #completed:Z
    .restart local v44       #hwRendererCanvas:Landroid/view/HardwareCanvas;
    .restart local v51       #layerCanvas:Landroid/view/HardwareCanvas;
    .restart local v70       #visibleInsetsChanged:Z
    :cond_6fa
    :try_start_6fa
    move-object/from16 v0, p0

    #@6fc
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@6fe
    invoke-virtual {v4}, Landroid/view/HardwareLayer;->getWidth()I

    #@701
    move-result v4

    #@702
    move-object/from16 v0, p0

    #@704
    iget v5, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@706
    if-ne v4, v5, :cond_716

    #@708
    move-object/from16 v0, p0

    #@70a
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@70c
    invoke-virtual {v4}, Landroid/view/HardwareLayer;->getHeight()I

    #@70f
    move-result v4

    #@710
    move-object/from16 v0, p0

    #@712
    iget v5, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@714
    if-eq v4, v5, :cond_4f4

    #@716
    .line 1516
    :cond_716
    move-object/from16 v0, p0

    #@718
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@71a
    move-object/from16 v0, p0

    #@71c
    iget v5, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@71e
    move-object/from16 v0, p0

    #@720
    iget v10, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@722
    invoke-virtual {v4, v5, v10}, Landroid/view/HardwareLayer;->resize(II)Z
    :try_end_725
    .catchall {:try_start_6fa .. :try_end_725} :catchall_76f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6fa .. :try_end_725} :catch_727

    #@725
    goto/16 :goto_4f4

    #@727
    .line 1555
    :catch_727
    move-exception v38

    #@728
    .line 1556
    .local v38, e:Ljava/lang/OutOfMemoryError;
    :try_start_728
    const-string v4, "ViewRootImpl"

    #@72a
    const-string v5, "Not enough memory for content change anim buffer"

    #@72c
    move-object/from16 v0, v38

    #@72e
    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_731
    .catchall {:try_start_728 .. :try_end_731} :catchall_76f

    #@731
    .line 1558
    if-eqz v51, :cond_736

    #@733
    .line 1559
    :try_start_733
    invoke-virtual/range {v51 .. v51}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@736
    .line 1561
    :cond_736
    move-object/from16 v0, p0

    #@738
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@73a
    if-eqz v4, :cond_5b0

    #@73c
    .line 1562
    move-object/from16 v0, p0

    #@73e
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@740
    move-object/from16 v0, v44

    #@742
    invoke-virtual {v4, v0}, Landroid/view/HardwareLayer;->end(Landroid/graphics/Canvas;)V

    #@745
    .line 1563
    if-nez v33, :cond_5b0

    #@747
    .line 1564
    move-object/from16 v0, p0

    #@749
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@74b
    invoke-virtual {v4}, Landroid/view/HardwareLayer;->destroy()V

    #@74e
    .line 1565
    const/4 v4, 0x0

    #@74f
    move-object/from16 v0, p0

    #@751
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;
    :try_end_753
    .catch Landroid/os/RemoteException; {:try_start_733 .. :try_end_753} :catch_755

    #@753
    goto/16 :goto_5b0

    #@755
    .line 1652
    .end local v33           #completed:Z
    .end local v38           #e:Ljava/lang/OutOfMemoryError;
    .end local v44           #hwRendererCanvas:Landroid/view/HardwareCanvas;
    .end local v51           #layerCanvas:Landroid/view/HardwareCanvas;
    .end local v66           #surfaceGenerationId:I
    .end local v70           #visibleInsetsChanged:Z
    :catch_755
    move-exception v4

    #@756
    goto/16 :goto_637

    #@758
    .line 1525
    .restart local v33       #completed:Z
    .restart local v44       #hwRendererCanvas:Landroid/view/HardwareCanvas;
    .restart local v51       #layerCanvas:Landroid/view/HardwareCanvas;
    .restart local v61       #restoreCount:I
    .restart local v66       #surfaceGenerationId:I
    .restart local v70       #visibleInsetsChanged:Z
    :cond_758
    const/16 v62, 0x0

    #@75a
    goto/16 :goto_527

    #@75c
    .line 1531
    .restart local v62       #scrolling:Z
    :cond_75c
    :try_start_75c
    move-object/from16 v0, p0

    #@75e
    iget v0, v0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@760
    move/from16 v75, v0

    #@762
    .restart local v75       #yoff:I
    goto/16 :goto_538

    #@764
    .line 1544
    .restart local v37       #displayList:Landroid/view/DisplayList;
    :cond_764
    move-object/from16 v0, p0

    #@766
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@768
    move-object/from16 v0, v51

    #@76a
    invoke-virtual {v4, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_76d
    .catchall {:try_start_75c .. :try_end_76d} :catchall_76f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_75c .. :try_end_76d} :catch_727

    #@76d
    goto/16 :goto_564

    #@76f
    .line 1558
    .end local v37           #displayList:Landroid/view/DisplayList;
    .end local v61           #restoreCount:I
    .end local v62           #scrolling:Z
    .end local v75           #yoff:I
    :catchall_76f
    move-exception v4

    #@770
    if-eqz v51, :cond_775

    #@772
    .line 1559
    :try_start_772
    invoke-virtual/range {v51 .. v51}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@775
    .line 1561
    :cond_775
    move-object/from16 v0, p0

    #@777
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@779
    if-eqz v5, :cond_792

    #@77b
    .line 1562
    move-object/from16 v0, p0

    #@77d
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@77f
    move-object/from16 v0, v44

    #@781
    invoke-virtual {v5, v0}, Landroid/view/HardwareLayer;->end(Landroid/graphics/Canvas;)V

    #@784
    .line 1563
    if-nez v33, :cond_792

    #@786
    .line 1564
    move-object/from16 v0, p0

    #@788
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@78a
    invoke-virtual {v5}, Landroid/view/HardwareLayer;->destroy()V

    #@78d
    .line 1565
    const/4 v5, 0x0

    #@78e
    move-object/from16 v0, p0

    #@790
    iput-object v5, v0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@792
    :cond_792
    throw v4

    #@793
    .line 1604
    .end local v33           #completed:Z
    .end local v44           #hwRendererCanvas:Landroid/view/HardwareCanvas;
    .end local v51           #layerCanvas:Landroid/view/HardwareCanvas;
    :catch_793
    move-exception v38

    #@794
    .line 1605
    .local v38, e:Landroid/view/Surface$OutOfResourcesException;
    const-string v4, "ViewRootImpl"

    #@796
    const-string v5, "OutOfResourcesException initializing HW surface"

    #@798
    move-object/from16 v0, v38

    #@79a
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_79d
    .catch Landroid/os/RemoteException; {:try_start_772 .. :try_end_79d} :catch_755

    #@79d
    .line 1607
    :try_start_79d
    move-object/from16 v0, p0

    #@79f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@7a1
    move-object/from16 v0, p0

    #@7a3
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@7a5
    invoke-interface {v4, v5}, Landroid/view/IWindowSession;->outOfMemory(Landroid/view/IWindow;)Z

    #@7a8
    move-result v4

    #@7a9
    if-nez v4, :cond_7c1

    #@7ab
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@7ae
    move-result v4

    #@7af
    const/16 v5, 0x3e8

    #@7b1
    if-eq v4, v5, :cond_7c1

    #@7b3
    .line 1609
    const-string v4, "ViewRootImpl"

    #@7b5
    const-string v5, "No processes killed for memory; killing self"

    #@7b7
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7ba
    .line 1610
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@7bd
    move-result v4

    #@7be
    invoke-static {v4}, Landroid/os/Process;->killProcess(I)V
    :try_end_7c1
    .catch Landroid/os/RemoteException; {:try_start_79d .. :try_end_7c1} :catch_d12

    #@7c1
    .line 1614
    :cond_7c1
    :goto_7c1
    const/4 v4, 0x1

    #@7c2
    :try_start_7c2
    move-object/from16 v0, p0

    #@7c4
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@7c6
    goto/16 :goto_c

    #@7c8
    .line 1619
    .end local v38           #e:Landroid/view/Surface$OutOfResourcesException;
    :cond_7c8
    move-object/from16 v0, p0

    #@7ca
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@7cc
    invoke-virtual {v4}, Landroid/view/Surface;->isValid()Z

    #@7cf
    move-result v4

    #@7d0
    if-nez v4, :cond_81d

    #@7d2
    .line 1622
    const/4 v4, 0x0

    #@7d3
    move-object/from16 v0, p0

    #@7d5
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mLastScrolledFocus:Landroid/view/View;

    #@7d7
    .line 1623
    const/4 v4, 0x0

    #@7d8
    move-object/from16 v0, p0

    #@7da
    iput v4, v0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@7dc
    move-object/from16 v0, p0

    #@7de
    iput v4, v0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@7e0
    .line 1624
    move-object/from16 v0, p0

    #@7e2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@7e4
    if-eqz v4, :cond_7ed

    #@7e6
    .line 1625
    move-object/from16 v0, p0

    #@7e8
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@7ea
    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    #@7ed
    .line 1627
    :cond_7ed
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->disposeResizeBuffer()V

    #@7f0
    .line 1629
    move-object/from16 v0, p0

    #@7f2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7f4
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@7f6
    if-eqz v4, :cond_637

    #@7f8
    move-object/from16 v0, p0

    #@7fa
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7fc
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@7fe
    invoke-virtual {v4}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@801
    move-result v4

    #@802
    if-eqz v4, :cond_637

    #@804
    .line 1631
    move-object/from16 v0, p0

    #@806
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@808
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@80a
    move-object/from16 v0, p0

    #@80c
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@80e
    invoke-virtual {v4, v5}, Landroid/view/HardwareRenderer;->destroyLayers(Landroid/view/View;)V

    #@811
    .line 1632
    move-object/from16 v0, p0

    #@813
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@815
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@817
    const/4 v5, 0x1

    #@818
    invoke-virtual {v4, v5}, Landroid/view/HardwareRenderer;->destroy(Z)V

    #@81b
    goto/16 :goto_637

    #@81d
    .line 1634
    :cond_81d
    move-object/from16 v0, p0

    #@81f
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@821
    invoke-virtual {v4}, Landroid/view/Surface;->getGenerationId()I

    #@824
    move-result v4

    #@825
    move/from16 v0, v66

    #@827
    if-eq v0, v4, :cond_637

    #@829
    move-object/from16 v0, p0

    #@82b
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@82d
    if-nez v4, :cond_637

    #@82f
    move-object/from16 v0, p0

    #@831
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@833
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@835
    if-eqz v4, :cond_637

    #@837
    .line 1636
    const/4 v4, 0x1

    #@838
    move-object/from16 v0, p0

    #@83a
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z
    :try_end_83c
    .catch Landroid/os/RemoteException; {:try_start_7c2 .. :try_end_83c} :catch_755

    #@83c
    .line 1638
    :try_start_83c
    move-object/from16 v0, p0

    #@83e
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@840
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@842
    move-object/from16 v0, p0

    #@844
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mHolder:Landroid/view/SurfaceHolder;

    #@846
    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@849
    move-result-object v5

    #@84a
    invoke-virtual {v4, v5}, Landroid/view/HardwareRenderer;->updateSurface(Landroid/view/Surface;)V
    :try_end_84d
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_83c .. :try_end_84d} :catch_84f
    .catch Landroid/os/RemoteException; {:try_start_83c .. :try_end_84d} :catch_755

    #@84d
    goto/16 :goto_637

    #@84f
    .line 1639
    :catch_84f
    move-exception v38

    #@850
    .line 1640
    .restart local v38       #e:Landroid/view/Surface$OutOfResourcesException;
    :try_start_850
    const-string v4, "ViewRootImpl"

    #@852
    const-string v5, "OutOfResourcesException updating HW surface"

    #@854
    move-object/from16 v0, v38

    #@856
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_859
    .catch Landroid/os/RemoteException; {:try_start_850 .. :try_end_859} :catch_755

    #@859
    .line 1642
    :try_start_859
    move-object/from16 v0, p0

    #@85b
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@85d
    move-object/from16 v0, p0

    #@85f
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@861
    invoke-interface {v4, v5}, Landroid/view/IWindowSession;->outOfMemory(Landroid/view/IWindow;)Z

    #@864
    move-result v4

    #@865
    if-nez v4, :cond_875

    #@867
    .line 1643
    const-string v4, "ViewRootImpl"

    #@869
    const-string v5, "No processes killed for memory; killing self"

    #@86b
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@86e
    .line 1644
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@871
    move-result v4

    #@872
    invoke-static {v4}, Landroid/os/Process;->killProcess(I)V
    :try_end_875
    .catch Landroid/os/RemoteException; {:try_start_859 .. :try_end_875} :catch_d0f

    #@875
    .line 1648
    :cond_875
    :goto_875
    const/4 v4, 0x1

    #@876
    :try_start_876
    move-object/from16 v0, p0

    #@878
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z
    :try_end_87a
    .catch Landroid/os/RemoteException; {:try_start_876 .. :try_end_87a} :catch_755

    #@87a
    goto/16 :goto_c

    #@87c
    .line 1690
    .end local v38           #e:Landroid/view/Surface$OutOfResourcesException;
    .end local v66           #surfaceGenerationId:I
    .end local v70           #visibleInsetsChanged:Z
    .restart local v28       #callbacks:[Landroid/view/SurfaceHolder$Callback;
    :cond_87c
    const/16 v65, 0x1

    #@87e
    .line 1692
    .end local v28           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    :cond_87e
    if-eqz v65, :cond_8c6

    #@880
    .line 1693
    move-object/from16 v0, p0

    #@882
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

    #@884
    move-object/from16 v0, p0

    #@886
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@888
    iget v10, v6, Landroid/view/WindowManager$LayoutParams;->format:I

    #@88a
    move-object/from16 v0, p0

    #@88c
    iget v12, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@88e
    move-object/from16 v0, p0

    #@890
    iget v13, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@892
    invoke-interface {v4, v5, v10, v12, v13}, Landroid/view/SurfaceHolder$Callback2;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    #@895
    .line 1695
    move-object/from16 v0, p0

    #@897
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@899
    invoke-virtual {v4}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@89c
    move-result-object v28

    #@89d
    .line 1696
    .restart local v28       #callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v28, :cond_8c6

    #@89f
    .line 1697
    move-object/from16 v25, v28

    #@8a1
    .restart local v25       #arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v25

    #@8a3
    array-length v0, v0

    #@8a4
    move/from16 v53, v0

    #@8a6
    .restart local v53       #len$:I
    const/16 v46, 0x0

    #@8a8
    .restart local v46       #i$:I
    :goto_8a8
    move/from16 v0, v46

    #@8aa
    move/from16 v1, v53

    #@8ac
    if-ge v0, v1, :cond_8c6

    #@8ae
    aget-object v27, v25, v46

    #@8b0
    .line 1698
    .restart local v27       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@8b2
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@8b4
    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->format:I

    #@8b6
    move-object/from16 v0, p0

    #@8b8
    iget v10, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@8ba
    move-object/from16 v0, p0

    #@8bc
    iget v12, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@8be
    move-object/from16 v0, v27

    #@8c0
    invoke-interface {v0, v4, v5, v10, v12}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    #@8c3
    .line 1697
    add-int/lit8 v46, v46, 0x1

    #@8c5
    goto :goto_8a8

    #@8c6
    .line 1703
    .end local v25           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v27           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v28           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v46           #i$:I
    .end local v53           #len$:I
    :cond_8c6
    const/4 v4, 0x0

    #@8c7
    move-object/from16 v0, p0

    #@8c9
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsCreating:Z

    #@8cb
    .line 1722
    :cond_8cb
    :goto_8cb
    move-object/from16 v0, p0

    #@8cd
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8cf
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8d1
    if-eqz v4, :cond_92c

    #@8d3
    move-object/from16 v0, p0

    #@8d5
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8d7
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8d9
    invoke-virtual {v4}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@8dc
    move-result v4

    #@8dd
    if-eqz v4, :cond_92c

    #@8df
    .line 1724
    if-nez v43, :cond_903

    #@8e1
    if-nez v73, :cond_903

    #@8e3
    move-object/from16 v0, p0

    #@8e5
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@8e7
    move-object/from16 v0, p0

    #@8e9
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8eb
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8ed
    invoke-virtual {v5}, Landroid/view/HardwareRenderer;->getWidth()I

    #@8f0
    move-result v5

    #@8f1
    if-ne v4, v5, :cond_903

    #@8f3
    move-object/from16 v0, p0

    #@8f5
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@8f7
    move-object/from16 v0, p0

    #@8f9
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8fb
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8fd
    invoke-virtual {v5}, Landroid/view/HardwareRenderer;->getHeight()I

    #@900
    move-result v5

    #@901
    if-eq v4, v5, :cond_92c

    #@903
    .line 1727
    :cond_903
    move-object/from16 v0, p0

    #@905
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@907
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@909
    move-object/from16 v0, p0

    #@90b
    iget v5, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@90d
    move-object/from16 v0, p0

    #@90f
    iget v10, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@911
    invoke-virtual {v4, v5, v10}, Landroid/view/HardwareRenderer;->setup(II)V

    #@914
    .line 1728
    if-nez v43, :cond_92c

    #@916
    .line 1729
    move-object/from16 v0, p0

    #@918
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@91a
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@91c
    move-object/from16 v0, p0

    #@91e
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mHolder:Landroid/view/SurfaceHolder;

    #@920
    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@923
    move-result-object v5

    #@924
    invoke-virtual {v4, v5}, Landroid/view/HardwareRenderer;->invalidate(Landroid/view/Surface;)V

    #@927
    .line 1730
    const/4 v4, 0x1

    #@928
    move-object/from16 v0, p0

    #@92a
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@92c
    .line 1735
    :cond_92c
    move-object/from16 v0, p0

    #@92e
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mStopped:Z

    #@930
    if-nez v4, :cond_9c5

    #@932
    .line 1736
    and-int/lit8 v4, v59, 0x1

    #@934
    if-eqz v4, :cond_c41

    #@936
    const/4 v4, 0x1

    #@937
    :goto_937
    move-object/from16 v0, p0

    #@939
    invoke-direct {v0, v4}, Landroid/view/ViewRootImpl;->ensureTouchModeLocally(Z)Z

    #@93c
    move-result v39

    #@93d
    .line 1738
    .local v39, focusChangedDueToTouchMode:Z
    if-nez v39, :cond_955

    #@93f
    move-object/from16 v0, p0

    #@941
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@943
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    #@946
    move-result v5

    #@947
    if-ne v4, v5, :cond_955

    #@949
    move-object/from16 v0, p0

    #@94b
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@94d
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    #@950
    move-result v5

    #@951
    if-ne v4, v5, :cond_955

    #@953
    if-eqz v35, :cond_9c5

    #@955
    .line 1740
    :cond_955
    move-object/from16 v0, p0

    #@957
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@959
    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    #@95b
    invoke-static {v4, v5}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@95e
    move-result v31

    #@95f
    .line 1741
    .local v31, childWidthMeasureSpec:I
    move-object/from16 v0, p0

    #@961
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@963
    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    #@965
    invoke-static {v4, v5}, Landroid/view/ViewRootImpl;->getRootMeasureSpec(II)I

    #@968
    move-result v30

    #@969
    .line 1750
    .local v30, childHeightMeasureSpec:I
    move-object/from16 v0, p0

    #@96b
    move/from16 v1, v31

    #@96d
    move/from16 v2, v30

    #@96f
    invoke-direct {v0, v1, v2}, Landroid/view/ViewRootImpl;->performMeasure(II)V

    #@972
    .line 1755
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    #@975
    move-result v71

    #@976
    .line 1756
    .local v71, width:I
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    #@979
    move-result v42

    #@97a
    .line 1757
    .local v42, height:I
    const/16 v54, 0x0

    #@97c
    .line 1759
    .local v54, measureAgain:Z
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@97e
    const/4 v5, 0x0

    #@97f
    cmpl-float v4, v4, v5

    #@981
    if-lez v4, :cond_99a

    #@983
    .line 1760
    move-object/from16 v0, p0

    #@985
    iget v4, v0, Landroid/view/ViewRootImpl;->mWidth:I

    #@987
    sub-int v4, v4, v71

    #@989
    int-to-float v4, v4

    #@98a
    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@98c
    mul-float/2addr v4, v5

    #@98d
    float-to-int v4, v4

    #@98e
    add-int v71, v71, v4

    #@990
    .line 1761
    const/high16 v4, 0x4000

    #@992
    move/from16 v0, v71

    #@994
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@997
    move-result v31

    #@998
    .line 1763
    const/16 v54, 0x1

    #@99a
    .line 1765
    :cond_99a
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@99c
    const/4 v5, 0x0

    #@99d
    cmpl-float v4, v4, v5

    #@99f
    if-lez v4, :cond_9b8

    #@9a1
    .line 1766
    move-object/from16 v0, p0

    #@9a3
    iget v4, v0, Landroid/view/ViewRootImpl;->mHeight:I

    #@9a5
    sub-int v4, v4, v42

    #@9a7
    int-to-float v4, v4

    #@9a8
    iget v5, v6, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@9aa
    mul-float/2addr v4, v5

    #@9ab
    float-to-int v4, v4

    #@9ac
    add-int v42, v42, v4

    #@9ae
    .line 1767
    const/high16 v4, 0x4000

    #@9b0
    move/from16 v0, v42

    #@9b2
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@9b5
    move-result v30

    #@9b6
    .line 1769
    const/16 v54, 0x1

    #@9b8
    .line 1772
    :cond_9b8
    if-eqz v54, :cond_9c3

    #@9ba
    .line 1776
    move-object/from16 v0, p0

    #@9bc
    move/from16 v1, v31

    #@9be
    move/from16 v2, v30

    #@9c0
    invoke-direct {v0, v1, v2}, Landroid/view/ViewRootImpl;->performMeasure(II)V

    #@9c3
    .line 1779
    :cond_9c3
    const/16 v52, 0x1

    #@9c5
    .line 1808
    .end local v30           #childHeightMeasureSpec:I
    .end local v31           #childWidthMeasureSpec:I
    .end local v35           #contentInsetsChanged:Z
    .end local v39           #focusChangedDueToTouchMode:Z
    .end local v41           #hadSurface:Z
    .end local v42           #height:I
    .end local v43           #hwInitialized:Z
    .end local v54           #measureAgain:Z
    .end local v71           #width:I
    :cond_9c5
    :goto_9c5
    if-eqz v52, :cond_c80

    #@9c7
    move-object/from16 v0, p0

    #@9c9
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mStopped:Z

    #@9cb
    if-nez v4, :cond_c80

    #@9cd
    const/16 v36, 0x1

    #@9cf
    .line 1809
    .local v36, didLayout:Z
    :goto_9cf
    if-nez v36, :cond_9d7

    #@9d1
    move-object/from16 v0, v26

    #@9d3
    iget-boolean v4, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@9d5
    if-eqz v4, :cond_c84

    #@9d7
    :cond_9d7
    const/16 v67, 0x1

    #@9d9
    .line 1811
    .local v67, triggerGlobalLayoutListener:Z
    :goto_9d9
    if-eqz v36, :cond_a5a

    #@9db
    .line 1812
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewRootImpl;->performLayout()V

    #@9de
    .line 1817
    iget v4, v11, Landroid/view/View;->mPrivateFlags:I

    #@9e0
    and-int/lit16 v4, v4, 0x200

    #@9e2
    if-eqz v4, :cond_a5a

    #@9e4
    .line 1820
    move-object/from16 v0, p0

    #@9e6
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTmpLocation:[I

    #@9e8
    invoke-virtual {v11, v4}, Landroid/view/View;->getLocationInWindow([I)V

    #@9eb
    .line 1821
    move-object/from16 v0, p0

    #@9ed
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@9ef
    move-object/from16 v0, p0

    #@9f1
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mTmpLocation:[I

    #@9f3
    const/4 v10, 0x0

    #@9f4
    aget v5, v5, v10

    #@9f6
    move-object/from16 v0, p0

    #@9f8
    iget-object v10, v0, Landroid/view/ViewRootImpl;->mTmpLocation:[I

    #@9fa
    const/4 v12, 0x1

    #@9fb
    aget v10, v10, v12

    #@9fd
    move-object/from16 v0, p0

    #@9ff
    iget-object v12, v0, Landroid/view/ViewRootImpl;->mTmpLocation:[I

    #@a01
    const/4 v13, 0x0

    #@a02
    aget v12, v12, v13

    #@a04
    iget v13, v11, Landroid/view/View;->mRight:I

    #@a06
    add-int/2addr v12, v13

    #@a07
    iget v13, v11, Landroid/view/View;->mLeft:I

    #@a09
    sub-int/2addr v12, v13

    #@a0a
    move-object/from16 v0, p0

    #@a0c
    iget-object v13, v0, Landroid/view/ViewRootImpl;->mTmpLocation:[I

    #@a0e
    const/4 v14, 0x1

    #@a0f
    aget v13, v13, v14

    #@a11
    iget v14, v11, Landroid/view/View;->mBottom:I

    #@a13
    add-int/2addr v13, v14

    #@a14
    iget v14, v11, Landroid/view/View;->mTop:I

    #@a16
    sub-int/2addr v13, v14

    #@a17
    invoke-virtual {v4, v5, v10, v12, v13}, Landroid/graphics/Region;->set(IIII)Z

    #@a1a
    .line 1825
    move-object/from16 v0, p0

    #@a1c
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@a1e
    invoke-virtual {v11, v4}, Landroid/view/View;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    #@a21
    .line 1826
    move-object/from16 v0, p0

    #@a23
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@a25
    if-eqz v4, :cond_a32

    #@a27
    .line 1827
    move-object/from16 v0, p0

    #@a29
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@a2b
    move-object/from16 v0, p0

    #@a2d
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@a2f
    invoke-virtual {v4, v5}, Landroid/content/res/CompatibilityInfo$Translator;->translateRegionInWindowToScreen(Landroid/graphics/Region;)V

    #@a32
    .line 1830
    :cond_a32
    move-object/from16 v0, p0

    #@a34
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@a36
    move-object/from16 v0, p0

    #@a38
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mPreviousTransparentRegion:Landroid/graphics/Region;

    #@a3a
    invoke-virtual {v4, v5}, Landroid/graphics/Region;->equals(Ljava/lang/Object;)Z

    #@a3d
    move-result v4

    #@a3e
    if-nez v4, :cond_a5a

    #@a40
    .line 1831
    move-object/from16 v0, p0

    #@a42
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPreviousTransparentRegion:Landroid/graphics/Region;

    #@a44
    move-object/from16 v0, p0

    #@a46
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@a48
    invoke-virtual {v4, v5}, Landroid/graphics/Region;->set(Landroid/graphics/Region;)Z

    #@a4b
    .line 1834
    :try_start_a4b
    move-object/from16 v0, p0

    #@a4d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@a4f
    move-object/from16 v0, p0

    #@a51
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@a53
    move-object/from16 v0, p0

    #@a55
    iget-object v10, v0, Landroid/view/ViewRootImpl;->mTransparentRegion:Landroid/graphics/Region;

    #@a57
    invoke-interface {v4, v5, v10}, Landroid/view/IWindowSession;->setTransparentRegion(Landroid/view/IWindow;Landroid/graphics/Region;)V
    :try_end_a5a
    .catch Landroid/os/RemoteException; {:try_start_a4b .. :try_end_a5a} :catch_d0c

    #@a5a
    .line 1847
    :cond_a5a
    :goto_a5a
    if-eqz v67, :cond_a7d

    #@a5c
    .line 1848
    const/4 v4, 0x0

    #@a5d
    move-object/from16 v0, v26

    #@a5f
    iput-boolean v4, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@a61
    .line 1849
    move-object/from16 v0, v26

    #@a63
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@a65
    invoke-virtual {v4}, Landroid/view/ViewTreeObserver;->dispatchOnGlobalLayout()V

    #@a68
    .line 1851
    iget-object v4, v11, Landroid/view/View;->mContext:Landroid/content/Context;

    #@a6a
    invoke-static {v4}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@a6d
    move-result-object v4

    #@a6e
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@a71
    move-result v4

    #@a72
    if-eqz v4, :cond_a7d

    #@a74
    .line 1852
    move-object/from16 v0, p0

    #@a76
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@a78
    move-object/from16 v0, p0

    #@a7a
    invoke-direct {v0, v4}, Landroid/view/ViewRootImpl;->postSendWindowContentChangedCallback(Landroid/view/View;)V

    #@a7d
    .line 1856
    :cond_a7d
    if-eqz v34, :cond_ae1

    #@a7f
    .line 1858
    move-object/from16 v0, v26

    #@a81
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mGivenInternalInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@a83
    move-object/from16 v48, v0

    #@a85
    .line 1859
    .local v48, insets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    invoke-virtual/range {v48 .. v48}, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->reset()V

    #@a88
    .line 1862
    move-object/from16 v0, v26

    #@a8a
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@a8c
    move-object/from16 v0, v48

    #@a8e
    invoke-virtual {v4, v0}, Landroid/view/ViewTreeObserver;->dispatchOnComputeInternalInsets(Landroid/view/ViewTreeObserver$InternalInsetsInfo;)V

    #@a91
    .line 1865
    if-nez v50, :cond_a9f

    #@a93
    move-object/from16 v0, p0

    #@a95
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mLastGivenInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@a97
    move-object/from16 v0, v48

    #@a99
    invoke-virtual {v4, v0}, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->equals(Ljava/lang/Object;)Z

    #@a9c
    move-result v4

    #@a9d
    if-nez v4, :cond_ae1

    #@a9f
    .line 1866
    :cond_a9f
    move-object/from16 v0, p0

    #@aa1
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mLastGivenInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@aa3
    move-object/from16 v0, v48

    #@aa5
    invoke-virtual {v4, v0}, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->set(Landroid/view/ViewTreeObserver$InternalInsetsInfo;)V

    #@aa8
    .line 1872
    move-object/from16 v0, p0

    #@aaa
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@aac
    if-eqz v4, :cond_c88

    #@aae
    .line 1873
    move-object/from16 v0, p0

    #@ab0
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@ab2
    move-object/from16 v0, v48

    #@ab4
    iget-object v5, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->contentInsets:Landroid/graphics/Rect;

    #@ab6
    invoke-virtual {v4, v5}, Landroid/content/res/CompatibilityInfo$Translator;->getTranslatedContentInsets(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@ab9
    move-result-object v15

    #@aba
    .line 1874
    .local v15, contentInsets:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@abc
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@abe
    move-object/from16 v0, v48

    #@ac0
    iget-object v5, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->visibleInsets:Landroid/graphics/Rect;

    #@ac2
    invoke-virtual {v4, v5}, Landroid/content/res/CompatibilityInfo$Translator;->getTranslatedVisibleInsets(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@ac5
    move-result-object v16

    #@ac6
    .line 1875
    .local v16, visibleInsets:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@ac8
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@aca
    move-object/from16 v0, v48

    #@acc
    iget-object v5, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->touchableRegion:Landroid/graphics/Region;

    #@ace
    invoke-virtual {v4, v5}, Landroid/content/res/CompatibilityInfo$Translator;->getTranslatedTouchableArea(Landroid/graphics/Region;)Landroid/graphics/Region;

    #@ad1
    move-result-object v17

    #@ad2
    .line 1883
    .local v17, touchableRegion:Landroid/graphics/Region;
    :goto_ad2
    :try_start_ad2
    move-object/from16 v0, p0

    #@ad4
    iget-object v12, v0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@ad6
    move-object/from16 v0, p0

    #@ad8
    iget-object v13, v0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@ada
    move-object/from16 v0, v48

    #@adc
    iget v14, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->mTouchableInsets:I

    #@ade
    invoke-interface/range {v12 .. v17}, Landroid/view/IWindowSession;->setInsets(Landroid/view/IWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V
    :try_end_ae1
    .catch Landroid/os/RemoteException; {:try_start_ad2 .. :try_end_ae1} :catch_d09

    #@ae1
    .line 1890
    .end local v15           #contentInsets:Landroid/graphics/Rect;
    .end local v16           #visibleInsets:Landroid/graphics/Rect;
    .end local v17           #touchableRegion:Landroid/graphics/Region;
    .end local v48           #insets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    :cond_ae1
    :goto_ae1
    const/16 v64, 0x0

    #@ae3
    .line 1892
    .local v64, skipDraw:Z
    move-object/from16 v0, p0

    #@ae5
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@ae7
    if-eqz v4, :cond_ca8

    #@ae9
    .line 1896
    move-object/from16 v0, p0

    #@aeb
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@aed
    if-eqz v4, :cond_b11

    #@aef
    .line 1897
    move-object/from16 v0, p0

    #@af1
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@af3
    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    #@af6
    move-result v4

    #@af7
    if-nez v4, :cond_c9a

    #@af9
    .line 1898
    move-object/from16 v0, p0

    #@afb
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@afd
    const/4 v5, 0x2

    #@afe
    invoke-virtual {v4, v5}, Landroid/view/View;->requestFocus(I)Z

    #@b01
    .line 1899
    move-object/from16 v0, p0

    #@b03
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@b05
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@b08
    move-result-object v4

    #@b09
    move-object/from16 v0, p0

    #@b0b
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mRealFocusedView:Landroid/view/View;

    #@b0d
    move-object/from16 v0, p0

    #@b0f
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@b11
    .line 1908
    :cond_b11
    :goto_b11
    and-int/lit8 v4, v59, 0x8

    #@b13
    if-eqz v4, :cond_b1a

    #@b15
    .line 1912
    const/4 v4, 0x1

    #@b16
    move-object/from16 v0, p0

    #@b18
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mWindowsAnimating:Z

    #@b1a
    .line 1918
    :cond_b1a
    :goto_b1a
    const/4 v4, 0x0

    #@b1b
    move-object/from16 v0, p0

    #@b1d
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@b1f
    .line 1919
    const/4 v4, 0x0

    #@b20
    move-object/from16 v0, p0

    #@b22
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mWillDrawSoon:Z

    #@b24
    .line 1920
    const/4 v4, 0x0

    #@b25
    move-object/from16 v0, p0

    #@b27
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mNewSurfaceNeeded:Z

    #@b29
    .line 1921
    move/from16 v0, v68

    #@b2b
    move-object/from16 v1, p0

    #@b2d
    iput v0, v1, Landroid/view/ViewRootImpl;->mViewVisibility:I

    #@b2f
    .line 1923
    move-object/from16 v0, p0

    #@b31
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@b33
    iget-boolean v4, v4, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@b35
    if-eqz v4, :cond_b89

    #@b37
    .line 1924
    move-object/from16 v0, p0

    #@b39
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@b3b
    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@b3d
    invoke-static {v4}, Landroid/view/WindowManager$LayoutParams;->mayUseInputMethod(I)Z

    #@b40
    move-result v47

    #@b41
    .line 1926
    .local v47, imTarget:Z
    move-object/from16 v0, p0

    #@b43
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@b45
    move/from16 v0, v47

    #@b47
    if-eq v0, v4, :cond_b89

    #@b49
    .line 1927
    move/from16 v0, v47

    #@b4b
    move-object/from16 v1, p0

    #@b4d
    iput-boolean v0, v1, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@b4f
    .line 1928
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@b52
    move-result-object v18

    #@b53
    .line 1929
    .local v18, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v18, :cond_b89

    #@b55
    if-eqz v47, :cond_b89

    #@b57
    .line 1930
    move-object/from16 v0, p0

    #@b59
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@b5b
    move-object/from16 v0, v18

    #@b5d
    invoke-virtual {v0, v4}, Landroid/view/inputmethod/InputMethodManager;->startGettingWindowFocus(Landroid/view/View;)V

    #@b60
    .line 1931
    move-object/from16 v0, p0

    #@b62
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@b64
    move-object/from16 v19, v0

    #@b66
    move-object/from16 v0, p0

    #@b68
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@b6a
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@b6d
    move-result-object v20

    #@b6e
    move-object/from16 v0, p0

    #@b70
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@b72
    iget v0, v4, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@b74
    move/from16 v21, v0

    #@b76
    move-object/from16 v0, p0

    #@b78
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mHasHadWindowFocus:Z

    #@b7a
    if-nez v4, :cond_cb2

    #@b7c
    const/16 v22, 0x1

    #@b7e
    :goto_b7e
    move-object/from16 v0, p0

    #@b80
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@b82
    iget v0, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@b84
    move/from16 v23, v0

    #@b86
    invoke-virtual/range {v18 .. v23}, Landroid/view/inputmethod/InputMethodManager;->onWindowFocus(Landroid/view/View;Landroid/view/View;IZI)V

    #@b89
    .line 1939
    .end local v18           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v47           #imTarget:Z
    :cond_b89
    and-int/lit8 v4, v59, 0x2

    #@b8b
    if-eqz v4, :cond_b92

    #@b8d
    .line 1940
    const/4 v4, 0x1

    #@b8e
    move-object/from16 v0, p0

    #@b90
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mReportNextDraw:Z

    #@b92
    .line 1943
    :cond_b92
    move-object/from16 v0, v26

    #@b94
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@b96
    invoke-virtual {v4}, Landroid/view/ViewTreeObserver;->dispatchOnPreDraw()Z

    #@b99
    move-result v4

    #@b9a
    if-nez v4, :cond_b9e

    #@b9c
    if-eqz v68, :cond_cb6

    #@b9e
    :cond_b9e
    const/16 v29, 0x1

    #@ba0
    .line 1946
    .local v29, cancelDraw:Z
    :goto_ba0
    if-nez v29, :cond_ccb

    #@ba2
    if-nez v55, :cond_ccb

    #@ba4
    .line 1947
    if-eqz v64, :cond_bac

    #@ba6
    move-object/from16 v0, p0

    #@ba8
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mReportNextDraw:Z

    #@baa
    if-eqz v4, :cond_cc4

    #@bac
    .line 1948
    :cond_bac
    move-object/from16 v0, p0

    #@bae
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@bb0
    if-eqz v4, :cond_cc1

    #@bb2
    move-object/from16 v0, p0

    #@bb4
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@bb6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@bb9
    move-result v4

    #@bba
    if-lez v4, :cond_cc1

    #@bbc
    .line 1949
    const/16 v45, 0x0

    #@bbe
    .restart local v45       #i:I
    :goto_bbe
    move-object/from16 v0, p0

    #@bc0
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@bc2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@bc5
    move-result v4

    #@bc6
    move/from16 v0, v45

    #@bc8
    if-ge v0, v4, :cond_cba

    #@bca
    .line 1950
    move-object/from16 v0, p0

    #@bcc
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@bce
    move/from16 v0, v45

    #@bd0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@bd3
    move-result-object v4

    #@bd4
    check-cast v4, Landroid/animation/LayoutTransition;

    #@bd6
    invoke-virtual {v4}, Landroid/animation/LayoutTransition;->startChangingAnimations()V

    #@bd9
    .line 1949
    add-int/lit8 v45, v45, 0x1

    #@bdb
    goto :goto_bbe

    #@bdc
    .line 1704
    .end local v29           #cancelDraw:Z
    .end local v36           #didLayout:Z
    .end local v45           #i:I
    .end local v64           #skipDraw:Z
    .end local v67           #triggerGlobalLayoutListener:Z
    .restart local v35       #contentInsetsChanged:Z
    .restart local v41       #hadSurface:Z
    .restart local v43       #hwInitialized:Z
    :cond_bdc
    if-eqz v41, :cond_8cb

    #@bde
    .line 1705
    move-object/from16 v0, p0

    #@be0
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@be2
    invoke-virtual {v4}, Lcom/android/internal/view/BaseSurfaceHolder;->ungetCallbacks()V

    #@be5
    .line 1706
    move-object/from16 v0, p0

    #@be7
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@be9
    invoke-virtual {v4}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@bec
    move-result-object v28

    #@bed
    .line 1707
    .restart local v28       #callbacks:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@bef
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

    #@bf1
    move-object/from16 v0, p0

    #@bf3
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@bf5
    invoke-interface {v4, v5}, Landroid/view/SurfaceHolder$Callback2;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    #@bf8
    .line 1708
    if-eqz v28, :cond_c17

    #@bfa
    .line 1709
    move-object/from16 v25, v28

    #@bfc
    .restart local v25       #arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v25

    #@bfe
    array-length v0, v0

    #@bff
    move/from16 v53, v0

    #@c01
    .restart local v53       #len$:I
    const/16 v46, 0x0

    #@c03
    .restart local v46       #i$:I
    :goto_c03
    move/from16 v0, v46

    #@c05
    move/from16 v1, v53

    #@c07
    if-ge v0, v1, :cond_c17

    #@c09
    aget-object v27, v25, v46

    #@c0b
    .line 1710
    .restart local v27       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@c0d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@c0f
    move-object/from16 v0, v27

    #@c11
    invoke-interface {v0, v4}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    #@c14
    .line 1709
    add-int/lit8 v46, v46, 0x1

    #@c16
    goto :goto_c03

    #@c17
    .line 1713
    .end local v25           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v27           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v46           #i$:I
    .end local v53           #len$:I
    :cond_c17
    move-object/from16 v0, p0

    #@c19
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@c1b
    iget-object v4, v4, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@c1d
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@c20
    .line 1715
    :try_start_c20
    move-object/from16 v0, p0

    #@c22
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@c24
    new-instance v5, Landroid/view/Surface;

    #@c26
    invoke-direct {v5}, Landroid/view/Surface;-><init>()V

    #@c29
    iput-object v5, v4, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;
    :try_end_c2b
    .catchall {:try_start_c20 .. :try_end_c2b} :catchall_c36

    #@c2b
    .line 1717
    move-object/from16 v0, p0

    #@c2d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@c2f
    iget-object v4, v4, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@c31
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@c34
    goto/16 :goto_8cb

    #@c36
    :catchall_c36
    move-exception v4

    #@c37
    move-object/from16 v0, p0

    #@c39
    iget-object v5, v0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@c3b
    iget-object v5, v5, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@c3d
    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@c40
    throw v4

    #@c41
    .line 1736
    .end local v28           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    :cond_c41
    const/4 v4, 0x0

    #@c42
    goto/16 :goto_937

    #@c44
    .line 1797
    .end local v35           #contentInsetsChanged:Z
    .end local v41           #hadSurface:Z
    .end local v43           #hwInitialized:Z
    :cond_c44
    move-object/from16 v0, v26

    #@c46
    iget v4, v0, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@c48
    move-object/from16 v0, v40

    #@c4a
    iget v5, v0, Landroid/graphics/Rect;->left:I

    #@c4c
    if-ne v4, v5, :cond_c58

    #@c4e
    move-object/from16 v0, v26

    #@c50
    iget v4, v0, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@c52
    move-object/from16 v0, v40

    #@c54
    iget v5, v0, Landroid/graphics/Rect;->top:I

    #@c56
    if-eq v4, v5, :cond_c7d

    #@c58
    :cond_c58
    const/16 v72, 0x1

    #@c5a
    .line 1799
    .local v72, windowMoved:Z
    :goto_c5a
    if-eqz v72, :cond_9c5

    #@c5c
    .line 1800
    move-object/from16 v0, p0

    #@c5e
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@c60
    if-eqz v4, :cond_c6b

    #@c62
    .line 1801
    move-object/from16 v0, p0

    #@c64
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@c66
    move-object/from16 v0, v40

    #@c68
    invoke-virtual {v4, v0}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWinFrame(Landroid/graphics/Rect;)V

    #@c6b
    .line 1803
    :cond_c6b
    move-object/from16 v0, v40

    #@c6d
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@c6f
    move-object/from16 v0, v26

    #@c71
    iput v4, v0, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@c73
    .line 1804
    move-object/from16 v0, v40

    #@c75
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@c77
    move-object/from16 v0, v26

    #@c79
    iput v4, v0, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@c7b
    goto/16 :goto_9c5

    #@c7d
    .line 1797
    .end local v72           #windowMoved:Z
    :cond_c7d
    const/16 v72, 0x0

    #@c7f
    goto :goto_c5a

    #@c80
    .line 1808
    :cond_c80
    const/16 v36, 0x0

    #@c82
    goto/16 :goto_9cf

    #@c84
    .line 1809
    .restart local v36       #didLayout:Z
    :cond_c84
    const/16 v67, 0x0

    #@c86
    goto/16 :goto_9d9

    #@c88
    .line 1877
    .restart local v48       #insets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    .restart local v67       #triggerGlobalLayoutListener:Z
    :cond_c88
    move-object/from16 v0, v48

    #@c8a
    iget-object v15, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->contentInsets:Landroid/graphics/Rect;

    #@c8c
    .line 1878
    .restart local v15       #contentInsets:Landroid/graphics/Rect;
    move-object/from16 v0, v48

    #@c8e
    iget-object v0, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->visibleInsets:Landroid/graphics/Rect;

    #@c90
    move-object/from16 v16, v0

    #@c92
    .line 1879
    .restart local v16       #visibleInsets:Landroid/graphics/Rect;
    move-object/from16 v0, v48

    #@c94
    iget-object v0, v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;->touchableRegion:Landroid/graphics/Region;

    #@c96
    move-object/from16 v17, v0

    #@c98
    .restart local v17       #touchableRegion:Landroid/graphics/Region;
    goto/16 :goto_ad2

    #@c9a
    .line 1903
    .end local v15           #contentInsets:Landroid/graphics/Rect;
    .end local v16           #visibleInsets:Landroid/graphics/Rect;
    .end local v17           #touchableRegion:Landroid/graphics/Region;
    .end local v48           #insets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    .restart local v64       #skipDraw:Z
    :cond_c9a
    move-object/from16 v0, p0

    #@c9c
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c9e
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@ca1
    move-result-object v4

    #@ca2
    move-object/from16 v0, p0

    #@ca4
    iput-object v4, v0, Landroid/view/ViewRootImpl;->mRealFocusedView:Landroid/view/View;

    #@ca6
    goto/16 :goto_b11

    #@ca8
    .line 1914
    :cond_ca8
    move-object/from16 v0, p0

    #@caa
    iget-boolean v4, v0, Landroid/view/ViewRootImpl;->mWindowsAnimating:Z

    #@cac
    if-eqz v4, :cond_b1a

    #@cae
    .line 1915
    const/16 v64, 0x1

    #@cb0
    goto/16 :goto_b1a

    #@cb2
    .line 1931
    .restart local v18       #imm:Landroid/view/inputmethod/InputMethodManager;
    .restart local v47       #imTarget:Z
    :cond_cb2
    const/16 v22, 0x0

    #@cb4
    goto/16 :goto_b7e

    #@cb6
    .line 1943
    .end local v18           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v47           #imTarget:Z
    :cond_cb6
    const/16 v29, 0x0

    #@cb8
    goto/16 :goto_ba0

    #@cba
    .line 1952
    .restart local v29       #cancelDraw:Z
    .restart local v45       #i:I
    :cond_cba
    move-object/from16 v0, p0

    #@cbc
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@cbe
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@cc1
    .line 1955
    .end local v45           #i:I
    :cond_cc1
    invoke-direct/range {p0 .. p0}, Landroid/view/ViewRootImpl;->performDraw()V

    #@cc4
    .line 1969
    :cond_cc4
    :goto_cc4
    const/4 v4, 0x0

    #@cc5
    move-object/from16 v0, p0

    #@cc7
    iput-boolean v4, v0, Landroid/view/ViewRootImpl;->mIsInTraversal:Z

    #@cc9
    goto/16 :goto_c

    #@ccb
    .line 1958
    :cond_ccb
    if-nez v68, :cond_cd1

    #@ccd
    .line 1960
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@cd0
    goto :goto_cc4

    #@cd1
    .line 1961
    :cond_cd1
    move-object/from16 v0, p0

    #@cd3
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@cd5
    if-eqz v4, :cond_cc4

    #@cd7
    move-object/from16 v0, p0

    #@cd9
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@cdb
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@cde
    move-result v4

    #@cdf
    if-lez v4, :cond_cc4

    #@ce1
    .line 1962
    const/16 v45, 0x0

    #@ce3
    .restart local v45       #i:I
    :goto_ce3
    move-object/from16 v0, p0

    #@ce5
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@ce7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@cea
    move-result v4

    #@ceb
    move/from16 v0, v45

    #@ced
    if-ge v0, v4, :cond_d01

    #@cef
    .line 1963
    move-object/from16 v0, p0

    #@cf1
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@cf3
    move/from16 v0, v45

    #@cf5
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@cf8
    move-result-object v4

    #@cf9
    check-cast v4, Landroid/animation/LayoutTransition;

    #@cfb
    invoke-virtual {v4}, Landroid/animation/LayoutTransition;->endChangingAnimations()V

    #@cfe
    .line 1962
    add-int/lit8 v45, v45, 0x1

    #@d00
    goto :goto_ce3

    #@d01
    .line 1965
    :cond_d01
    move-object/from16 v0, p0

    #@d03
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@d05
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@d08
    goto :goto_cc4

    #@d09
    .line 1885
    .end local v29           #cancelDraw:Z
    .end local v45           #i:I
    .end local v64           #skipDraw:Z
    .restart local v15       #contentInsets:Landroid/graphics/Rect;
    .restart local v16       #visibleInsets:Landroid/graphics/Rect;
    .restart local v17       #touchableRegion:Landroid/graphics/Region;
    .restart local v48       #insets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    :catch_d09
    move-exception v4

    #@d0a
    goto/16 :goto_ae1

    #@d0c
    .line 1835
    .end local v15           #contentInsets:Landroid/graphics/Rect;
    .end local v16           #visibleInsets:Landroid/graphics/Rect;
    .end local v17           #touchableRegion:Landroid/graphics/Region;
    .end local v48           #insets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    :catch_d0c
    move-exception v4

    #@d0d
    goto/16 :goto_a5a

    #@d0f
    .line 1646
    .end local v36           #didLayout:Z
    .end local v67           #triggerGlobalLayoutListener:Z
    .restart local v35       #contentInsetsChanged:Z
    .restart local v38       #e:Landroid/view/Surface$OutOfResourcesException;
    .restart local v41       #hadSurface:Z
    .restart local v43       #hwInitialized:Z
    .restart local v66       #surfaceGenerationId:I
    .restart local v70       #visibleInsetsChanged:Z
    :catch_d0f
    move-exception v4

    #@d10
    goto/16 :goto_875

    #@d12
    .line 1612
    :catch_d12
    move-exception v4

    #@d13
    goto/16 :goto_7c1
.end method

.method private postSendWindowContentChangedCallback(Landroid/view/View;)V
    .registers 7
    .parameter "source"

    #@0
    .prologue
    .line 4837
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 4838
    new-instance v1, Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-direct {v1, p0, v2}, Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;-><init>(Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$1;)V

    #@a
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@c
    .line 4841
    :cond_c
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@e
    iget-object v0, v1, Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;->mSource:Landroid/view/View;

    #@10
    .line 4842
    .local v0, oldSource:Landroid/view/View;
    if-nez v0, :cond_22

    #@12
    .line 4843
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@14
    iput-object p1, v1, Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;->mSource:Landroid/view/View;

    #@16
    .line 4844
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@18
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@1a
    invoke-static {}, Landroid/view/ViewConfiguration;->getSendRecurringAccessibilityEventsInterval()J

    #@1d
    move-result-wide v3

    #@1e
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/ViewRootImpl$ViewRootHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@21
    .line 4850
    :goto_21
    return-void

    #@22
    .line 4847
    :cond_22
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@24
    invoke-direct {p0, v0, p1}, Landroid/view/ViewRootImpl;->getCommonPredecessor(Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    #@27
    move-result-object v2

    #@28
    iput-object v2, v1, Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;->mSource:Landroid/view/View;

    #@2a
    goto :goto_21
.end method

.method private profileRendering(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 2082
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mProfileRendering:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    .line 2083
    iput-boolean p1, p0, Landroid/view/ViewRootImpl;->mRenderProfilingEnabled:Z

    #@6
    .line 2084
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mRenderProfiler:Ljava/lang/Thread;

    #@8
    if-nez v0, :cond_1e

    #@a
    .line 2085
    new-instance v0, Ljava/lang/Thread;

    #@c
    new-instance v1, Landroid/view/ViewRootImpl$3;

    #@e
    invoke-direct {v1, p0}, Landroid/view/ViewRootImpl$3;-><init>(Landroid/view/ViewRootImpl;)V

    #@11
    const-string v2, "Rendering Profiler"

    #@13
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@16
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mRenderProfiler:Ljava/lang/Thread;

    #@18
    .line 2106
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mRenderProfiler:Ljava/lang/Thread;

    #@1a
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@1d
    .line 2112
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 2108
    :cond_1e
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mRenderProfiler:Ljava/lang/Thread;

    #@20
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    #@23
    .line 2109
    const/4 v0, 0x0

    #@24
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mRenderProfiler:Ljava/lang/Thread;

    #@26
    goto :goto_1d
.end method

.method private recycleQueuedInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V
    .registers 4
    .parameter "q"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 4439
    iput-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@3
    .line 4440
    iput-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mReceiver:Landroid/view/InputEventReceiver;

    #@5
    .line 4442
    iget v0, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPoolSize:I

    #@7
    const/16 v1, 0xa

    #@9
    if-ge v0, v1, :cond_17

    #@b
    .line 4443
    iget v0, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPoolSize:I

    #@d
    add-int/lit8 v0, v0, 0x1

    #@f
    iput v0, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPoolSize:I

    #@11
    .line 4444
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPool:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@13
    iput-object v0, p1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@15
    .line 4445
    iput-object p1, p0, Landroid/view/ViewRootImpl;->mQueuedInputEventPool:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@17
    .line 4447
    :cond_17
    return-void
.end method

.method private relayoutWindow(Landroid/view/WindowManager$LayoutParams;IZ)I
    .registers 21
    .parameter "params"
    .parameter "viewVisibility"
    .parameter "insetsPending"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4130
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    iget v14, v1, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@6
    .line 4131
    .local v14, appScale:F
    const/16 v16, 0x0

    #@8
    .line 4132
    .local v16, restore:Z
    if-eqz p1, :cond_1e

    #@a
    move-object/from16 v0, p0

    #@c
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@e
    if-eqz v1, :cond_1e

    #@10
    .line 4133
    const/16 v16, 0x1

    #@12
    .line 4134
    invoke-virtual/range {p1 .. p1}, Landroid/view/WindowManager$LayoutParams;->backup()V

    #@15
    .line 4135
    move-object/from16 v0, p0

    #@17
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-virtual {v1, v0}, Landroid/content/res/CompatibilityInfo$Translator;->translateWindowLayout(Landroid/view/WindowManager$LayoutParams;)V

    #@1e
    .line 4137
    :cond_1e
    if-eqz p1, :cond_20

    #@20
    .line 4140
    :cond_20
    move-object/from16 v0, p0

    #@22
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@24
    const/4 v2, 0x0

    #@25
    iput v2, v1, Landroid/content/res/Configuration;->seq:I

    #@27
    .line 4142
    if-eqz p1, :cond_5f

    #@29
    move-object/from16 v0, p0

    #@2b
    iget v1, v0, Landroid/view/ViewRootImpl;->mOrigWindowType:I

    #@2d
    move-object/from16 v0, p1

    #@2f
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@31
    if-eq v1, v2, :cond_5f

    #@33
    .line 4144
    move-object/from16 v0, p0

    #@35
    iget v1, v0, Landroid/view/ViewRootImpl;->mTargetSdkVersion:I

    #@37
    const/16 v2, 0xe

    #@39
    if-ge v1, v2, :cond_5f

    #@3b
    .line 4145
    const-string v1, "ViewRootImpl"

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v3, "Window type can not be changed after the window is added; ignoring change of "

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    move-object/from16 v0, p0

    #@4a
    iget-object v3, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 4147
    move-object/from16 v0, p0

    #@59
    iget v1, v0, Landroid/view/ViewRootImpl;->mOrigWindowType:I

    #@5b
    move-object/from16 v0, p1

    #@5d
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@5f
    .line 4150
    :cond_5f
    move-object/from16 v0, p0

    #@61
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@63
    move-object/from16 v0, p0

    #@65
    iget-object v2, v0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@67
    move-object/from16 v0, p0

    #@69
    iget v3, v0, Landroid/view/ViewRootImpl;->mSeq:I

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@6f
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@72
    move-result v4

    #@73
    int-to-float v4, v4

    #@74
    mul-float/2addr v4, v14

    #@75
    const/high16 v5, 0x3f00

    #@77
    add-float/2addr v4, v5

    #@78
    float-to-int v5, v4

    #@79
    move-object/from16 v0, p0

    #@7b
    iget-object v4, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@7d
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    #@80
    move-result v4

    #@81
    int-to-float v4, v4

    #@82
    mul-float/2addr v4, v14

    #@83
    const/high16 v6, 0x3f00

    #@85
    add-float/2addr v4, v6

    #@86
    float-to-int v6, v4

    #@87
    if-eqz p3, :cond_d3

    #@89
    const/4 v8, 0x1

    #@8a
    :goto_8a
    move-object/from16 v0, p0

    #@8c
    iget-object v9, v0, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-object v10, v0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@92
    move-object/from16 v0, p0

    #@94
    iget-object v11, v0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@96
    move-object/from16 v0, p0

    #@98
    iget-object v12, v0, Landroid/view/ViewRootImpl;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v13, v0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@9e
    move-object/from16 v4, p1

    #@a0
    move/from16 v7, p2

    #@a2
    invoke-interface/range {v1 .. v13}, Landroid/view/IWindowSession;->relayout(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I

    #@a5
    move-result v15

    #@a6
    .line 4158
    .local v15, relayoutResult:I
    if-eqz v16, :cond_ab

    #@a8
    .line 4159
    invoke-virtual/range {p1 .. p1}, Landroid/view/WindowManager$LayoutParams;->restore()V

    #@ab
    .line 4162
    :cond_ab
    move-object/from16 v0, p0

    #@ad
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@af
    if-eqz v1, :cond_d2

    #@b1
    .line 4163
    move-object/from16 v0, p0

    #@b3
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@b5
    move-object/from16 v0, p0

    #@b7
    iget-object v2, v0, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@b9
    invoke-virtual {v1, v2}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWinFrame(Landroid/graphics/Rect;)V

    #@bc
    .line 4164
    move-object/from16 v0, p0

    #@be
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@c0
    move-object/from16 v0, p0

    #@c2
    iget-object v2, v0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@c4
    invoke-virtual {v1, v2}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V

    #@c7
    .line 4165
    move-object/from16 v0, p0

    #@c9
    iget-object v1, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@cb
    move-object/from16 v0, p0

    #@cd
    iget-object v2, v0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@cf
    invoke-virtual {v1, v2}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V

    #@d2
    .line 4167
    :cond_d2
    return v15

    #@d3
    .line 4150
    .end local v15           #relayoutResult:I
    :cond_d3
    const/4 v8, 0x0

    #@d4
    goto :goto_8a
.end method

.method private removeSendWindowContentChangedCallback()V
    .registers 3

    #@0
    .prologue
    .line 4857
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 4858
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@6
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSendWindowContentChangedAccessibilityEvent:Landroid/view/ViewRootImpl$SendWindowContentChangedAccessibilityEvent;

    #@8
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@b
    .line 4860
    :cond_b
    return-void
.end method

.method private scheduleProcessInputEvents()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 4480
    iget-boolean v1, p0, Landroid/view/ViewRootImpl;->mProcessInputEventsScheduled:Z

    #@3
    if-nez v1, :cond_17

    #@5
    .line 4481
    iput-boolean v3, p0, Landroid/view/ViewRootImpl;->mProcessInputEventsScheduled:Z

    #@7
    .line 4482
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@9
    const/16 v2, 0x13

    #@b
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    .line 4483
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    #@12
    .line 4484
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@14
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 4486
    .end local v0           #msg:Landroid/os/Message;
    :cond_17
    return-void
.end method

.method private setTouchEventFilterDefault()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 3285
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    if-eqz v1, :cond_9

    #@5
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@7
    if-nez v1, :cond_a

    #@9
    .line 3298
    :cond_9
    :goto_9
    return-void

    #@a
    .line 3288
    :cond_a
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->isFilterEnable:Z

    #@c
    .line 3290
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@e
    iget-object v1, v1, Landroid/view/View;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 3291
    .local v0, packageName:Ljava/lang/String;
    const-string v1, "com.lge.QuickClip"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_26

    #@1c
    .line 3292
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@1e
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@20
    const/16 v3, 0x8

    #@22
    invoke-virtual {v1, v2, v3}, Lcom/lge/view/TouchEventFilter;->addTouchEventFilter(Landroid/view/View;I)V

    #@25
    goto :goto_9

    #@26
    .line 3295
    :cond_26
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@28
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2a
    const/4 v3, 0x2

    #@2b
    invoke-virtual {v1, v2, v3}, Lcom/lge/view/TouchEventFilter;->addTouchEventFilter(Landroid/view/View;I)V

    #@2e
    .line 3296
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTouchEventFilter:Lcom/lge/view/TouchEventFilter;

    #@30
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@32
    invoke-virtual {v1, v2, v4}, Lcom/lge/view/TouchEventFilter;->addTouchEventFilter(Landroid/view/View;I)V

    #@35
    goto :goto_9
.end method

.method private trackFPS()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 2120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4
    move-result-wide v3

    #@5
    .line 2121
    .local v3, nowTime:J
    iget-wide v8, p0, Landroid/view/ViewRootImpl;->mFpsStartTime:J

    #@7
    const-wide/16 v10, 0x0

    #@9
    cmp-long v8, v8, v10

    #@b
    if-gez v8, :cond_14

    #@d
    .line 2122
    iput-wide v3, p0, Landroid/view/ViewRootImpl;->mFpsPrevTime:J

    #@f
    iput-wide v3, p0, Landroid/view/ViewRootImpl;->mFpsStartTime:J

    #@11
    .line 2123
    iput v12, p0, Landroid/view/ViewRootImpl;->mFpsNumFrames:I

    #@13
    .line 2138
    :cond_13
    :goto_13
    return-void

    #@14
    .line 2125
    :cond_14
    iget v8, p0, Landroid/view/ViewRootImpl;->mFpsNumFrames:I

    #@16
    add-int/lit8 v8, v8, 0x1

    #@18
    iput v8, p0, Landroid/view/ViewRootImpl;->mFpsNumFrames:I

    #@1a
    .line 2126
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@1d
    move-result v8

    #@1e
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    .line 2127
    .local v5, thisHash:Ljava/lang/String;
    iget-wide v8, p0, Landroid/view/ViewRootImpl;->mFpsPrevTime:J

    #@24
    sub-long v1, v3, v8

    #@26
    .line 2128
    .local v1, frameTime:J
    iget-wide v8, p0, Landroid/view/ViewRootImpl;->mFpsStartTime:J

    #@28
    sub-long v6, v3, v8

    #@2a
    .line 2129
    .local v6, totalTime:J
    const-string v8, "ViewRootImpl"

    #@2c
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "0x"

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    const-string v10, "\tFrame time:\t"

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@44
    move-result-object v9

    #@45
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 2130
    iput-wide v3, p0, Landroid/view/ViewRootImpl;->mFpsPrevTime:J

    #@4e
    .line 2131
    const-wide/16 v8, 0x3e8

    #@50
    cmp-long v8, v6, v8

    #@52
    if-lez v8, :cond_13

    #@54
    .line 2132
    iget v8, p0, Landroid/view/ViewRootImpl;->mFpsNumFrames:I

    #@56
    int-to-float v8, v8

    #@57
    const/high16 v9, 0x447a

    #@59
    mul-float/2addr v8, v9

    #@5a
    long-to-float v9, v6

    #@5b
    div-float v0, v8, v9

    #@5d
    .line 2133
    .local v0, fps:F
    const-string v8, "ViewRootImpl"

    #@5f
    new-instance v9, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v10, "0x"

    #@66
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v9

    #@6e
    const-string v10, "\tFPS:\t"

    #@70
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@77
    move-result-object v9

    #@78
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v9

    #@7c
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 2134
    iput-wide v3, p0, Landroid/view/ViewRootImpl;->mFpsStartTime:J

    #@81
    .line 2135
    iput v12, p0, Landroid/view/ViewRootImpl;->mFpsNumFrames:I

    #@83
    goto :goto_13
.end method

.method private updateJoystickDirection(Landroid/view/MotionEvent;Z)V
    .registers 19
    .parameter "event"
    .parameter "synthesizeNewKeys"

    #@0
    .prologue
    .line 3602
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@3
    move-result-wide v2

    #@4
    .line 3603
    .local v2, time:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@7
    move-result v9

    #@8
    .line 3604
    .local v9, metaState:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    #@b
    move-result v10

    #@c
    .line 3605
    .local v10, deviceId:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    #@f
    move-result v13

    #@10
    .line 3607
    .local v13, source:I
    const/16 v1, 0xf

    #@12
    move-object/from16 v0, p1

    #@14
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@17
    move-result v1

    #@18
    invoke-static {v1}, Landroid/view/ViewRootImpl;->joystickAxisValueToDirection(F)I

    #@1b
    move-result v14

    #@1c
    .line 3608
    .local v14, xDirection:I
    if-nez v14, :cond_26

    #@1e
    .line 3609
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@21
    move-result v1

    #@22
    invoke-static {v1}, Landroid/view/ViewRootImpl;->joystickAxisValueToDirection(F)I

    #@25
    move-result v14

    #@26
    .line 3612
    :cond_26
    const/16 v1, 0x10

    #@28
    move-object/from16 v0, p1

    #@2a
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    #@2d
    move-result v1

    #@2e
    invoke-static {v1}, Landroid/view/ViewRootImpl;->joystickAxisValueToDirection(F)I

    #@31
    move-result v15

    #@32
    .line 3613
    .local v15, yDirection:I
    if-nez v15, :cond_3c

    #@34
    .line 3614
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@37
    move-result v1

    #@38
    invoke-static {v1}, Landroid/view/ViewRootImpl;->joystickAxisValueToDirection(F)I

    #@3b
    move-result v15

    #@3c
    .line 3617
    :cond_3c
    move-object/from16 v0, p0

    #@3e
    iget v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickXDirection:I

    #@40
    if-eq v14, v1, :cond_85

    #@42
    .line 3618
    move-object/from16 v0, p0

    #@44
    iget v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickXKeyCode:I

    #@46
    if-eqz v1, :cond_61

    #@48
    .line 3619
    new-instance v1, Landroid/view/KeyEvent;

    #@4a
    const/4 v6, 0x1

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget v7, v0, Landroid/view/ViewRootImpl;->mLastJoystickXKeyCode:I

    #@4f
    const/4 v8, 0x0

    #@50
    const/4 v11, 0x0

    #@51
    const/16 v12, 0x400

    #@53
    move-wide v4, v2

    #@54
    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@57
    move-object/from16 v0, p0

    #@59
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@5c
    .line 3622
    const/4 v1, 0x0

    #@5d
    move-object/from16 v0, p0

    #@5f
    iput v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickXKeyCode:I

    #@61
    .line 3625
    :cond_61
    move-object/from16 v0, p0

    #@63
    iput v14, v0, Landroid/view/ViewRootImpl;->mLastJoystickXDirection:I

    #@65
    .line 3627
    if-eqz v14, :cond_85

    #@67
    if-eqz p2, :cond_85

    #@69
    .line 3628
    if-lez v14, :cond_cf

    #@6b
    const/16 v1, 0x16

    #@6d
    :goto_6d
    move-object/from16 v0, p0

    #@6f
    iput v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickXKeyCode:I

    #@71
    .line 3630
    new-instance v1, Landroid/view/KeyEvent;

    #@73
    const/4 v6, 0x0

    #@74
    move-object/from16 v0, p0

    #@76
    iget v7, v0, Landroid/view/ViewRootImpl;->mLastJoystickXKeyCode:I

    #@78
    const/4 v8, 0x0

    #@79
    const/4 v11, 0x0

    #@7a
    const/16 v12, 0x400

    #@7c
    move-wide v4, v2

    #@7d
    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@80
    move-object/from16 v0, p0

    #@82
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@85
    .line 3636
    :cond_85
    move-object/from16 v0, p0

    #@87
    iget v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickYDirection:I

    #@89
    if-eq v15, v1, :cond_ce

    #@8b
    .line 3637
    move-object/from16 v0, p0

    #@8d
    iget v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickYKeyCode:I

    #@8f
    if-eqz v1, :cond_aa

    #@91
    .line 3638
    new-instance v1, Landroid/view/KeyEvent;

    #@93
    const/4 v6, 0x1

    #@94
    move-object/from16 v0, p0

    #@96
    iget v7, v0, Landroid/view/ViewRootImpl;->mLastJoystickYKeyCode:I

    #@98
    const/4 v8, 0x0

    #@99
    const/4 v11, 0x0

    #@9a
    const/16 v12, 0x400

    #@9c
    move-wide v4, v2

    #@9d
    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@a0
    move-object/from16 v0, p0

    #@a2
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@a5
    .line 3641
    const/4 v1, 0x0

    #@a6
    move-object/from16 v0, p0

    #@a8
    iput v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickYKeyCode:I

    #@aa
    .line 3644
    :cond_aa
    move-object/from16 v0, p0

    #@ac
    iput v15, v0, Landroid/view/ViewRootImpl;->mLastJoystickYDirection:I

    #@ae
    .line 3646
    if-eqz v15, :cond_ce

    #@b0
    if-eqz p2, :cond_ce

    #@b2
    .line 3647
    if-lez v15, :cond_d2

    #@b4
    const/16 v1, 0x14

    #@b6
    :goto_b6
    move-object/from16 v0, p0

    #@b8
    iput v1, v0, Landroid/view/ViewRootImpl;->mLastJoystickYKeyCode:I

    #@ba
    .line 3649
    new-instance v1, Landroid/view/KeyEvent;

    #@bc
    const/4 v6, 0x0

    #@bd
    move-object/from16 v0, p0

    #@bf
    iget v7, v0, Landroid/view/ViewRootImpl;->mLastJoystickYKeyCode:I

    #@c1
    const/4 v8, 0x0

    #@c2
    const/4 v11, 0x0

    #@c3
    const/16 v12, 0x400

    #@c5
    move-wide v4, v2

    #@c6
    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@c9
    move-object/from16 v0, p0

    #@cb
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    #@ce
    .line 3654
    :cond_ce
    return-void

    #@cf
    .line 3628
    :cond_cf
    const/16 v1, 0x15

    #@d1
    goto :goto_6d

    #@d2
    .line 3647
    :cond_d2
    const/16 v1, 0x13

    #@d4
    goto :goto_b6
.end method


# virtual methods
.method public attachFunctor(I)Z
    .registers 4
    .parameter "functor"

    #@0
    .prologue
    .line 723
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    if-eqz v0, :cond_1b

    #@6
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@a
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1b

    #@10
    .line 724
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@12
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@14
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@16
    invoke-virtual {v0, v1, p1}, Landroid/view/HardwareRenderer;->attachFunctor(Landroid/view/View$AttachInfo;I)Z

    #@19
    move-result v0

    #@1a
    .line 726
    :goto_1a
    return v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_1a
.end method

.method public bringChildToFront(Landroid/view/View;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 985
    return-void
.end method

.method public cancelInvalidate(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 4722
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeMessages(ILjava/lang/Object;)V

    #@6
    .line 4725
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@8
    const/4 v1, 0x2

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeMessages(ILjava/lang/Object;)V

    #@c
    .line 4726
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInvalidateOnAnimationRunnable:Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;

    #@e
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->removeView(Landroid/view/View;)V

    #@11
    .line 4727
    return-void
.end method

.method checkThread()V
    .registers 3

    #@0
    .prologue
    .line 4956
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mThread:Ljava/lang/Thread;

    #@2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@5
    move-result-object v1

    #@6
    if-eq v0, v1, :cond_10

    #@8
    .line 4957
    new-instance v0, Landroid/view/ViewRootImpl$CalledFromWrongThreadException;

    #@a
    const-string v1, "Only the original thread that created a view hierarchy can touch its views."

    #@c
    invoke-direct {v0, v1}, Landroid/view/ViewRootImpl$CalledFromWrongThreadException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 4960
    :cond_10
    return-void
.end method

.method public childAccessibilityStateChanged(Landroid/view/View;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 4917
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->postSendWindowContentChangedCallback(Landroid/view/View;)V

    #@3
    .line 4918
    return-void
.end method

.method public childDrawableStateChanged(Landroid/view/View;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 4874
    return-void
.end method

.method public childHasTransientStateChanged(Landroid/view/View;Z)V
    .registers 3
    .parameter "child"
    .parameter "hasTransientState"

    #@0
    .prologue
    .line 4983
    return-void
.end method

.method public clearChildFocus(Landroid/view/View;)V
    .registers 5
    .parameter "child"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2672
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@4
    .line 2678
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@6
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mOldFocusedView:Landroid/view/View;

    #@8
    .line 2681
    const/4 v0, 0x2

    #@9
    invoke-virtual {p0, v2, v0}, Landroid/view/ViewRootImpl;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    if-nez v0, :cond_18

    #@f
    .line 2682
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@13
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mOldFocusedView:Landroid/view/View;

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewTreeObserver;->dispatchOnGlobalFocusChange(Landroid/view/View;Landroid/view/View;)V

    #@18
    .line 2685
    :cond_18
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mRealFocusedView:Landroid/view/View;

    #@1a
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@1c
    .line 2686
    return-void
.end method

.method public createContextMenu(Landroid/view/ContextMenu;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 4871
    return-void
.end method

.method public debug()V
    .registers 2

    #@0
    .prologue
    .line 4237
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2
    invoke-virtual {v0}, Landroid/view/View;->debug()V

    #@5
    .line 4238
    return-void
.end method

.method public dequeueDisplayList(Landroid/view/DisplayList;)V
    .registers 4
    .parameter "displayList"

    #@0
    .prologue
    .line 4713
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mDisplayLists:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 4714
    invoke-virtual {p1}, Landroid/view/DisplayList;->invalidate()V

    #@b
    .line 4715
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mDisplayLists:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1a

    #@13
    .line 4716
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@15
    const/16 v1, 0x15

    #@17
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeMessages(I)V

    #@1a
    .line 4719
    :cond_1a
    return-void
.end method

.method destroyHardwareLayers()V
    .registers 3

    #@0
    .prologue
    .line 702
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mThread:Ljava/lang/Thread;

    #@2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@5
    move-result-object v1

    #@6
    if-eq v0, v1, :cond_1e

    #@8
    .line 703
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@c
    if-eqz v0, :cond_1d

    #@e
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@10
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@12
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1d

    #@18
    .line 705
    const/16 v0, 0x3c

    #@1a
    invoke-static {v0}, Landroid/view/HardwareRenderer;->trimMemory(I)V

    #@1d
    .line 713
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 708
    :cond_1e
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@20
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@22
    if-eqz v0, :cond_1d

    #@24
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@26
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@28
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_1d

    #@2e
    .line 710
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@30
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@32
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@34
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->destroyLayers(Landroid/view/View;)V

    #@37
    goto :goto_1d
.end method

.method destroyHardwareResources()V
    .registers 3

    #@0
    .prologue
    .line 686
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    if-eqz v0, :cond_21

    #@6
    .line 687
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@a
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_19

    #@10
    .line 688
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@12
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@14
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@16
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->destroyLayers(Landroid/view/View;)V

    #@19
    .line 690
    :cond_19
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1b
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1d
    const/4 v1, 0x0

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->destroy(Z)V

    #@21
    .line 692
    :cond_21
    return-void
.end method

.method public detachFunctor(I)V
    .registers 3
    .parameter "functor"

    #@0
    .prologue
    .line 730
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 731
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@a
    invoke-virtual {v0, p1}, Landroid/view/HardwareRenderer;->detachFunctor(I)V

    #@d
    .line 733
    :cond_d
    return-void
.end method

.method public die(Z)V
    .registers 5
    .parameter "immediate"

    #@0
    .prologue
    .line 4267
    if-eqz p1, :cond_a

    #@2
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mIsInTraversal:Z

    #@4
    if-nez v0, :cond_a

    #@6
    .line 4268
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->doDie()V

    #@9
    .line 4278
    :goto_9
    return-void

    #@a
    .line 4270
    :cond_a
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mIsDrawing:Z

    #@c
    if-nez v0, :cond_18

    #@e
    .line 4271
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->destroyHardwareRenderer()V

    #@11
    .line 4276
    :goto_11
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@13
    const/4 v1, 0x3

    #@14
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendEmptyMessage(I)Z

    #@17
    goto :goto_9

    #@18
    .line 4273
    :cond_18
    const-string v0, "ViewRootImpl"

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "Attempting to destroy the window while drawing!\n  window="

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ", title="

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@31
    invoke-virtual {v2}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_11
.end method

.method public dispatchAppVisibility(Z)V
    .registers 5
    .parameter "visible"

    #@0
    .prologue
    .line 4766
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v2, 0x8

    #@4
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4767
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_13

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 4768
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@f
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 4769
    return-void

    #@13
    .line 4767
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_b
.end method

.method public dispatchCheckFocus()V
    .registers 3

    #@0
    .prologue
    const/16 v1, 0xd

    #@2
    .line 4824
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@4
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->hasMessages(I)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_f

    #@a
    .line 4826
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@c
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendEmptyMessage(I)Z

    #@f
    .line 4828
    :cond_f
    return-void
.end method

.method public dispatchCloseSystemDialogs(Ljava/lang/String;)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 4791
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 4792
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0xe

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 4793
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 4794
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@c
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@f
    .line 4795
    return-void
.end method

.method dispatchDetachedFromWindow()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2726
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    if-eqz v0, :cond_27

    #@5
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@7
    iget-object v0, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    if-eqz v0, :cond_27

    #@b
    .line 2727
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@d
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@f
    if-eqz v0, :cond_22

    #@11
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@13
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@15
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_22

    #@1b
    .line 2729
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1d
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1f
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->validate()Z

    #@22
    .line 2731
    :cond_22
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@24
    invoke-virtual {v0}, Landroid/view/View;->dispatchDetachedFromWindow()V

    #@27
    .line 2734
    :cond_27
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionConnectionManager:Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

    #@29
    invoke-virtual {v0}, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->ensureNoConnection()V

    #@2c
    .line 2735
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@2e
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionConnectionManager:Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

    #@30
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    #@33
    .line 2737
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->removeSendWindowContentChangedCallback()V

    #@36
    .line 2739
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->destroyHardwareRenderer()V

    #@39
    .line 2741
    invoke-virtual {p0, v2, v2}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3c
    .line 2743
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3e
    .line 2744
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    iput-object v2, v0, Landroid/view/View$AttachInfo;->mRootView:Landroid/view/View;

    #@42
    .line 2745
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@44
    iput-object v2, v0, Landroid/view/View$AttachInfo;->mSurface:Landroid/view/Surface;

    #@46
    .line 2747
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@48
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    #@4b
    .line 2749
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@4d
    if-eqz v0, :cond_74

    #@4f
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputQueue:Landroid/view/InputQueue;

    #@51
    if-eqz v0, :cond_74

    #@53
    .line 2750
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@55
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mInputQueue:Landroid/view/InputQueue;

    #@57
    invoke-interface {v0, v1}, Landroid/view/InputQueue$Callback;->onInputQueueDestroyed(Landroid/view/InputQueue;)V

    #@5a
    .line 2751
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@5c
    .line 2752
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mInputQueue:Landroid/view/InputQueue;

    #@5e
    .line 2758
    :cond_5e
    :goto_5e
    :try_start_5e
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@60
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@62
    invoke-interface {v0, v1}, Landroid/view/IWindowSession;->remove(Landroid/view/IWindow;)V
    :try_end_65
    .catch Landroid/os/RemoteException; {:try_start_5e .. :try_end_65} :catch_80

    #@65
    .line 2764
    :goto_65
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@67
    if-eqz v0, :cond_70

    #@69
    .line 2765
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@6b
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@6e
    .line 2766
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@70
    .line 2769
    :cond_70
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->unscheduleTraversals()V

    #@73
    .line 2770
    return-void

    #@74
    .line 2753
    :cond_74
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;

    #@76
    if-eqz v0, :cond_5e

    #@78
    .line 2754
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;

    #@7a
    invoke-virtual {v0}, Landroid/view/ViewRootImpl$WindowInputEventReceiver;->dispose()V

    #@7d
    .line 2755
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;

    #@7f
    goto :goto_5e

    #@80
    .line 2759
    :catch_80
    move-exception v0

    #@81
    goto :goto_65
.end method

.method public dispatchDoneAnimating()V
    .registers 3

    #@0
    .prologue
    .line 4820
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v1, 0x17

    #@4
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendEmptyMessage(I)Z

    #@7
    .line 4821
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 4799
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x2

    #@5
    if-ne v2, v3, :cond_1a

    #@7
    .line 4800
    const/16 v1, 0x10

    #@9
    .line 4801
    .local v1, what:I
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@b
    invoke-virtual {v2, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeMessages(I)V

    #@e
    .line 4805
    :goto_e
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@10
    invoke-virtual {v2, v1, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 4806
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@16
    invoke-virtual {v2, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@19
    .line 4807
    return-void

    #@1a
    .line 4803
    .end local v0           #msg:Landroid/os/Message;
    .end local v1           #what:I
    :cond_1a
    const/16 v1, 0xf

    #@1c
    .restart local v1       #what:I
    goto :goto_e
.end method

.method public dispatchFinishInputConnection(Landroid/view/inputmethod/InputConnection;)V
    .registers 5
    .parameter "connection"

    #@0
    .prologue
    .line 4361
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v2, 0xc

    #@4
    invoke-virtual {v1, v2, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4362
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@a
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 4363
    return-void
.end method

.method public dispatchGetNewSurface()V
    .registers 4

    #@0
    .prologue
    .line 4778
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v2, 0x9

    #@4
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4779
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@a
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 4780
    return-void
.end method

.method dispatchImeFinishedEvent(IZ)V
    .registers 7
    .parameter "seq"
    .parameter "handled"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 4353
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@3
    const/16 v3, 0xa

    #@5
    invoke-virtual {v1, v3}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 4354
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 4355
    if-eqz p2, :cond_19

    #@d
    move v1, v2

    #@e
    :goto_e
    iput v1, v0, Landroid/os/Message;->arg2:I

    #@10
    .line 4356
    invoke-virtual {v0, v2}, Landroid/os/Message;->setAsynchronous(Z)V

    #@13
    .line 4357
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@15
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 4358
    return-void

    #@19
    .line 4355
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_e
.end method

.method public dispatchInvalidateDelayed(Landroid/view/View;J)V
    .registers 7
    .parameter "view"
    .parameter "delayMilliseconds"

    #@0
    .prologue
    .line 4686
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-virtual {v1, v2, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 4687
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@9
    invoke-virtual {v1, v0, p2, p3}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c
    .line 4688
    return-void
.end method

.method public dispatchInvalidateOnAnimation(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 4697
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInvalidateOnAnimationRunnable:Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->addView(Landroid/view/View;)V

    #@5
    .line 4698
    return-void
.end method

.method public dispatchInvalidateRectDelayed(Landroid/view/View$AttachInfo$InvalidateInfo;J)V
    .registers 7
    .parameter "info"
    .parameter "delayMilliseconds"

    #@0
    .prologue
    .line 4692
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/4 v2, 0x2

    #@3
    invoke-virtual {v1, v2, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 4693
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@9
    invoke-virtual {v1, v0, p2, p3}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c
    .line 4694
    return-void
.end method

.method public dispatchInvalidateRectOnAnimation(Landroid/view/View$AttachInfo$InvalidateInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 4701
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInvalidateOnAnimationRunnable:Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->addViewRect(Landroid/view/View$AttachInfo$InvalidateInfo;)V

    #@5
    .line 4702
    return-void
.end method

.method public dispatchKey(Landroid/view/KeyEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 4730
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/4 v2, 0x7

    #@3
    invoke-virtual {v1, v2, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 4731
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@b
    .line 4732
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@d
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 4733
    return-void
.end method

.method public dispatchKeyFromIme(Landroid/view/KeyEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 4736
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v2, 0xb

    #@4
    invoke-virtual {v1, v2, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4737
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Landroid/os/Message;->setAsynchronous(Z)V

    #@c
    .line 4738
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@e
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 4739
    return-void
.end method

.method public dispatchMoved(II)V
    .registers 9
    .parameter "newX"
    .parameter "newY"

    #@0
    .prologue
    const-wide/high16 v4, 0x3fe0

    #@2
    .line 4389
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@4
    if-eqz v2, :cond_1c

    #@6
    .line 4390
    new-instance v1, Landroid/graphics/PointF;

    #@8
    int-to-float v2, p1

    #@9
    int-to-float v3, p2

    #@a
    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    #@d
    .line 4391
    .local v1, point:Landroid/graphics/PointF;
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@f
    invoke-virtual {v2, v1}, Landroid/content/res/CompatibilityInfo$Translator;->translatePointInScreenToAppWindow(Landroid/graphics/PointF;)V

    #@12
    .line 4392
    iget v2, v1, Landroid/graphics/PointF;->x:F

    #@14
    float-to-double v2, v2

    #@15
    add-double/2addr v2, v4

    #@16
    double-to-int p1, v2

    #@17
    .line 4393
    iget v2, v1, Landroid/graphics/PointF;->y:F

    #@19
    float-to-double v2, v2

    #@1a
    add-double/2addr v2, v4

    #@1b
    double-to-int p2, v2

    #@1c
    .line 4395
    .end local v1           #point:Landroid/graphics/PointF;
    :cond_1c
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@1e
    const/16 v3, 0x19

    #@20
    invoke-virtual {v2, v3, p1, p2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(III)Landroid/os/Message;

    #@23
    move-result-object v0

    #@24
    .line 4396
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@26
    invoke-virtual {v2, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@29
    .line 4397
    return-void
.end method

.method public dispatchResized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V
    .registers 11
    .parameter "frame"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "reportDraw"
    .parameter "newConfig"

    #@0
    .prologue
    .line 4371
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    if-eqz p4, :cond_5d

    #@4
    const/4 v3, 0x5

    #@5
    :goto_5
    invoke-virtual {v4, v3}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v1

    #@9
    .line 4372
    .local v1, msg:Landroid/os/Message;
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@b
    if-eqz v3, :cond_1c

    #@d
    .line 4373
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@f
    invoke-virtual {v3, p1}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V

    #@12
    .line 4374
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@14
    invoke-virtual {v3, p2}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V

    #@17
    .line 4375
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@19
    invoke-virtual {v3, p3}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V

    #@1c
    .line 4377
    :cond_1c
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@1f
    move-result-object v0

    #@20
    .line 4378
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@23
    move-result v3

    #@24
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@27
    move-result v4

    #@28
    if-ne v3, v4, :cond_5f

    #@2a
    const/4 v2, 0x1

    #@2b
    .line 4379
    .local v2, sameProcessCall:Z
    :goto_2b
    if-eqz v2, :cond_33

    #@2d
    new-instance v3, Landroid/graphics/Rect;

    #@2f
    invoke-direct {v3, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@32
    move-object p1, v3

    #@33
    .end local p1
    :cond_33
    iput-object p1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@35
    .line 4380
    if-eqz v2, :cond_3d

    #@37
    new-instance v3, Landroid/graphics/Rect;

    #@39
    invoke-direct {v3, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@3c
    move-object p2, v3

    #@3d
    .end local p2
    :cond_3d
    iput-object p2, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@3f
    .line 4381
    if-eqz v2, :cond_47

    #@41
    new-instance v3, Landroid/graphics/Rect;

    #@43
    invoke-direct {v3, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@46
    move-object p3, v3

    #@47
    .end local p3
    :cond_47
    iput-object p3, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@49
    .line 4382
    if-eqz v2, :cond_53

    #@4b
    if-eqz p5, :cond_53

    #@4d
    new-instance v3, Landroid/content/res/Configuration;

    #@4f
    invoke-direct {v3, p5}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@52
    move-object p5, v3

    #@53
    .end local p5
    :cond_53
    iput-object p5, v0, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@55
    .line 4383
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    .line 4384
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@59
    invoke-virtual {v3, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@5c
    .line 4385
    return-void

    #@5d
    .line 4371
    .end local v0           #args:Lcom/android/internal/os/SomeArgs;
    .end local v1           #msg:Landroid/os/Message;
    .end local v2           #sameProcessCall:Z
    .restart local p1
    .restart local p2
    .restart local p3
    .restart local p5
    :cond_5d
    const/4 v3, 0x4

    #@5e
    goto :goto_5

    #@5f
    .line 4378
    .restart local v0       #args:Lcom/android/internal/os/SomeArgs;
    .restart local v1       #msg:Landroid/os/Message;
    :cond_5f
    const/4 v2, 0x0

    #@60
    goto :goto_2b
.end method

.method public dispatchScreenStateChange(Z)V
    .registers 5
    .parameter "on"

    #@0
    .prologue
    .line 4772
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v2, 0x14

    #@4
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4773
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_13

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 4774
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@f
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 4775
    return-void

    #@13
    .line 4773
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_b
.end method

.method public dispatchSystemUiVisibilityChanged(IIII)V
    .registers 9
    .parameter "seq"
    .parameter "globalVisibility"
    .parameter "localValue"
    .parameter "localChanges"

    #@0
    .prologue
    .line 4811
    new-instance v0, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;

    #@2
    invoke-direct {v0}, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;-><init>()V

    #@5
    .line 4812
    .local v0, args:Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;
    iput p1, v0, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->seq:I

    #@7
    .line 4813
    iput p2, v0, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->globalVisibility:I

    #@9
    .line 4814
    iput p3, v0, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->localValue:I

    #@b
    .line 4815
    iput p4, v0, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->localChanges:I

    #@d
    .line 4816
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@f
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@11
    const/16 v3, 0x11

    #@13
    invoke-virtual {v2, v3, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 4817
    return-void
.end method

.method public dispatchUnhandledKey(Landroid/view/KeyEvent;)V
    .registers 21
    .parameter "event"

    #@0
    .prologue
    .line 4742
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@3
    move-result v1

    #@4
    and-int/lit16 v1, v1, 0x400

    #@6
    if-nez v1, :cond_4d

    #@8
    .line 4743
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@b
    move-result-object v16

    #@c
    .line 4744
    .local v16, kcm:Landroid/view/KeyCharacterMap;
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@f
    move-result v17

    #@10
    .line 4745
    .local v17, keyCode:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getMetaState()I

    #@13
    move-result v18

    #@14
    .line 4748
    .local v18, metaState:I
    invoke-virtual/range {v16 .. v18}, Landroid/view/KeyCharacterMap;->getFallbackAction(II)Landroid/view/KeyCharacterMap$FallbackAction;

    #@17
    move-result-object v14

    #@18
    .line 4750
    .local v14, fallbackAction:Landroid/view/KeyCharacterMap$FallbackAction;
    if-eqz v14, :cond_4d

    #@1a
    .line 4751
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@1d
    move-result v1

    #@1e
    or-int/lit16 v11, v1, 0x400

    #@20
    .line 4752
    .local v11, flags:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@23
    move-result-wide v1

    #@24
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getEventTime()J

    #@27
    move-result-wide v3

    #@28
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    #@2b
    move-result v5

    #@2c
    iget v6, v14, Landroid/view/KeyCharacterMap$FallbackAction;->keyCode:I

    #@2e
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@31
    move-result v7

    #@32
    iget v8, v14, Landroid/view/KeyCharacterMap$FallbackAction;->metaState:I

    #@34
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDeviceId()I

    #@37
    move-result v9

    #@38
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getScanCode()I

    #@3b
    move-result v10

    #@3c
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getSource()I

    #@3f
    move-result v12

    #@40
    const/4 v13, 0x0

    #@41
    invoke-static/range {v1 .. v13}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;

    #@44
    move-result-object v15

    #@45
    .line 4758
    .local v15, fallbackEvent:Landroid/view/KeyEvent;
    invoke-virtual {v14}, Landroid/view/KeyCharacterMap$FallbackAction;->recycle()V

    #@48
    .line 4760
    move-object/from16 v0, p0

    #@4a
    invoke-virtual {v0, v15}, Landroid/view/ViewRootImpl;->dispatchKey(Landroid/view/KeyEvent;)V

    #@4d
    .line 4763
    .end local v11           #flags:I
    .end local v14           #fallbackAction:Landroid/view/KeyCharacterMap$FallbackAction;
    .end local v15           #fallbackEvent:Landroid/view/KeyEvent;
    .end local v16           #kcm:Landroid/view/KeyCharacterMap;
    .end local v17           #keyCode:I
    .end local v18           #metaState:I
    :cond_4d
    return-void
.end method

.method disposeResizeBuffer()V
    .registers 2

    #@0
    .prologue
    .line 992
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 993
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@6
    invoke-virtual {v0}, Landroid/view/HardwareLayer;->destroy()V

    #@9
    .line 994
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@c
    .line 996
    :cond_c
    return-void
.end method

.method doConsumeBatchedInput(J)V
    .registers 4
    .parameter "frameTimeNanos"

    #@0
    .prologue
    .line 4541
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mConsumeBatchedInputScheduled:Z

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 4542
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mConsumeBatchedInputScheduled:Z

    #@7
    .line 4543
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 4544
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewRootImpl$WindowInputEventReceiver;->consumeBatchedInputEvents(J)V

    #@10
    .line 4546
    :cond_10
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->doProcessInputEvents()V

    #@13
    .line 4548
    :cond_13
    return-void
.end method

.method doDie()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4281
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@4
    .line 4283
    monitor-enter p0

    #@5
    .line 4284
    :try_start_5
    iget-boolean v2, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@7
    if-eqz v2, :cond_c

    #@9
    .line 4285
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->dispatchDetachedFromWindow()V

    #@c
    .line 4288
    :cond_c
    iget-boolean v2, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@e
    if-eqz v2, :cond_43

    #@10
    iget-boolean v2, p0, Landroid/view/ViewRootImpl;->mFirst:Z

    #@12
    if-nez v2, :cond_43

    #@14
    .line 4289
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->destroyHardwareRenderer()V

    #@17
    .line 4291
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@19
    if-eqz v2, :cond_43

    #@1b
    .line 4292
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1d
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@20
    move-result v0

    #@21
    .line 4293
    .local v0, viewVisibility:I
    iget v2, p0, Landroid/view/ViewRootImpl;->mViewVisibility:I

    #@23
    if-eq v2, v0, :cond_26

    #@25
    const/4 v1, 0x1

    #@26
    .line 4294
    .local v1, viewVisibilityChanged:Z
    :cond_26
    iget-boolean v2, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z
    :try_end_28
    .catchall {:try_start_5 .. :try_end_28} :catchall_48

    #@28
    if-nez v2, :cond_2c

    #@2a
    if-eqz v1, :cond_3e

    #@2c
    .line 4299
    :cond_2c
    :try_start_2c
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@2e
    const/4 v3, 0x0

    #@2f
    invoke-direct {p0, v2, v0, v3}, Landroid/view/ViewRootImpl;->relayoutWindow(Landroid/view/WindowManager$LayoutParams;IZ)I

    #@32
    move-result v2

    #@33
    and-int/lit8 v2, v2, 0x2

    #@35
    if-eqz v2, :cond_3e

    #@37
    .line 4301
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@39
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@3b
    invoke-interface {v2, v3}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V
    :try_end_3e
    .catchall {:try_start_2c .. :try_end_3e} :catchall_48
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_3e} :catch_4b

    #@3e
    .line 4307
    :cond_3e
    :goto_3e
    :try_start_3e
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@40
    invoke-virtual {v2}, Landroid/view/Surface;->release()V

    #@43
    .line 4311
    .end local v0           #viewVisibility:I
    .end local v1           #viewVisibilityChanged:Z
    :cond_43
    const/4 v2, 0x0

    #@44
    iput-boolean v2, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@46
    .line 4312
    monitor-exit p0

    #@47
    .line 4313
    return-void

    #@48
    .line 4312
    :catchall_48
    move-exception v2

    #@49
    monitor-exit p0
    :try_end_4a
    .catchall {:try_start_3e .. :try_end_4a} :catchall_48

    #@4a
    throw v2

    #@4b
    .line 4303
    .restart local v0       #viewVisibility:I
    .restart local v1       #viewVisibilityChanged:Z
    :catch_4b
    move-exception v2

    #@4c
    goto :goto_3e
.end method

.method doProcessInputEvents()V
    .registers 4

    #@0
    .prologue
    .line 4489
    :goto_0
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mCurrentInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@2
    if-nez v1, :cond_17

    #@4
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@6
    if-eqz v1, :cond_17

    #@8
    .line 4490
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@a
    .line 4491
    .local v0, q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    iget-object v1, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@c
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@e
    .line 4492
    const/4 v1, 0x0

    #@f
    iput-object v1, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@11
    .line 4493
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mCurrentInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@13
    .line 4494
    invoke-direct {p0, v0}, Landroid/view/ViewRootImpl;->deliverInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@16
    goto :goto_0

    #@17
    .line 4499
    .end local v0           #q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    :cond_17
    iget-boolean v1, p0, Landroid/view/ViewRootImpl;->mProcessInputEventsScheduled:Z

    #@19
    if-eqz v1, :cond_25

    #@1b
    .line 4500
    const/4 v1, 0x0

    #@1c
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mProcessInputEventsScheduled:Z

    #@1e
    .line 4501
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@20
    const/16 v2, 0x13

    #@22
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeMessages(I)V

    #@25
    .line 4503
    :cond_25
    return-void
.end method

.method doTraversal()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const-wide/16 v2, 0x8

    #@3
    .line 1040
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mTraversalScheduled:Z

    #@5
    if-eqz v0, :cond_32

    #@7
    .line 1041
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mTraversalScheduled:Z

    #@9
    .line 1042
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@b
    invoke-virtual {v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->getLooper()Landroid/os/Looper;

    #@e
    move-result-object v0

    #@f
    iget v1, p0, Landroid/view/ViewRootImpl;->mTraversalBarrier:I

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Looper;->removeSyncBarrier(I)V

    #@14
    .line 1044
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mProfile:Z

    #@16
    if-eqz v0, :cond_1d

    #@18
    .line 1045
    const-string v0, "ViewAncestor"

    #@1a
    invoke-static {v0}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;)V

    #@1d
    .line 1048
    :cond_1d
    const-string/jumbo v0, "performTraversals"

    #@20
    invoke-static {v2, v3, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@23
    .line 1050
    :try_start_23
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->performTraversals()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_33

    #@26
    .line 1052
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    #@29
    .line 1055
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mProfile:Z

    #@2b
    if-eqz v0, :cond_32

    #@2d
    .line 1056
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    #@30
    .line 1057
    iput-boolean v4, p0, Landroid/view/ViewRootImpl;->mProfile:Z

    #@32
    .line 1060
    :cond_32
    return-void

    #@33
    .line 1052
    :catchall_33
    move-exception v0

    #@34
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    #@37
    throw v0
.end method

.method public dumpGfxInfo([I)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4241
    const/4 v0, 0x1

    #@2
    aput v1, p1, v0

    #@4
    aput v1, p1, v1

    #@6
    .line 4242
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 4243
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    invoke-static {v0, p1}, Landroid/view/ViewRootImpl;->getGfxInfo(Landroid/view/View;[I)V

    #@f
    .line 4245
    :cond_f
    return-void
.end method

.method public enqueueDisplayList(Landroid/view/DisplayList;)V
    .registers 5
    .parameter "displayList"

    #@0
    .prologue
    const/16 v2, 0x15

    #@2
    .line 4705
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mDisplayLists:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7
    .line 4707
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@9
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->removeMessages(I)V

    #@c
    .line 4708
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@e
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 4709
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@14
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 4710
    return-void
.end method

.method enqueueInputEvent(Landroid/view/InputEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4450
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1, v1}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V

    #@5
    .line 4451
    return-void
.end method

.method enqueueInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V
    .registers 8
    .parameter "event"
    .parameter "receiver"
    .parameter "flags"
    .parameter "processImmediately"

    #@0
    .prologue
    .line 4455
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewRootImpl;->obtainQueuedInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;I)Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@3
    move-result-object v1

    #@4
    .line 4462
    .local v1, q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@6
    .line 4463
    .local v0, last:Landroid/view/ViewRootImpl$QueuedInputEvent;
    if-nez v0, :cond_10

    #@8
    .line 4464
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mFirstPendingInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@a
    .line 4472
    :goto_a
    if-eqz p4, :cond_1a

    #@c
    .line 4473
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->doProcessInputEvents()V

    #@f
    .line 4477
    :goto_f
    return-void

    #@10
    .line 4466
    :cond_10
    :goto_10
    iget-object v2, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@12
    if-eqz v2, :cond_17

    #@14
    .line 4467
    iget-object v0, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@16
    goto :goto_10

    #@17
    .line 4469
    :cond_17
    iput-object v1, v0, Landroid/view/ViewRootImpl$QueuedInputEvent;->mNext:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@19
    goto :goto_a

    #@1a
    .line 4475
    :cond_1a
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->scheduleProcessInputEvents()V

    #@1d
    goto :goto_f
.end method

.method ensureTouchMode(Z)Z
    .registers 4
    .parameter "inTouchMode"

    #@0
    .prologue
    .line 3154
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-boolean v1, v1, Landroid/view/View$AttachInfo;->mInTouchMode:Z

    #@4
    if-ne v1, p1, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    .line 3164
    :goto_7
    return v1

    #@8
    .line 3158
    :cond_8
    :try_start_8
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@a
    invoke-interface {v1, p1}, Landroid/view/IWindowSession;->setInTouchMode(Z)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_d} :catch_12

    #@d
    .line 3164
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->ensureTouchModeLocally(Z)Z

    #@10
    move-result v1

    #@11
    goto :goto_7

    #@12
    .line 3159
    :catch_12
    move-exception v0

    #@13
    .line 3160
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@15
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@18
    throw v1
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .registers 5
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 4229
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 4230
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@7
    if-nez v0, :cond_b

    #@9
    .line 4231
    const/4 v0, 0x0

    #@a
    .line 4233
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    #@e
    move-result-object v1

    #@f
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@11
    check-cast v0, Landroid/view/ViewGroup;

    #@13
    invoke-virtual {v1, v0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    goto :goto_a
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 2694
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 2695
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5
    if-eqz v1, :cond_12

    #@7
    .line 2696
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@9
    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_13

    #@f
    .line 2697
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    #@12
    .line 2713
    :cond_12
    :goto_12
    return-void

    #@13
    .line 2702
    :cond_13
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@15
    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@18
    move-result-object v1

    #@19
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@1b
    .line 2703
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@1d
    instance-of v1, v1, Landroid/view/ViewGroup;

    #@1f
    if-eqz v1, :cond_3c

    #@21
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@23
    check-cast v1, Landroid/view/ViewGroup;

    #@25
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@28
    move-result v1

    #@29
    const/high16 v2, 0x4

    #@2b
    if-ne v1, v2, :cond_3c

    #@2d
    const/4 v0, 0x1

    #@2e
    .line 2707
    .local v0, descendantsHaveDibsOnFocus:Z
    :goto_2e
    if-eqz v0, :cond_12

    #@30
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@32
    invoke-static {p1, v1}, Landroid/view/ViewRootImpl;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@35
    move-result v1

    #@36
    if-eqz v1, :cond_12

    #@38
    .line 2709
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    #@3b
    goto :goto_12

    #@3c
    .line 2703
    .end local v0           #descendantsHaveDibsOnFocus:Z
    :cond_3c
    const/4 v0, 0x0

    #@3d
    goto :goto_2e
.end method

.method public getAccessibilityFocusedHost()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 2608
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getAccessibilityFocusedVirtualView()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 2

    #@0
    .prologue
    .line 2615
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@2
    return-object v0
.end method

.method public getAccessibilityInteractionController()Landroid/view/AccessibilityInteractionController;
    .registers 3

    #@0
    .prologue
    .line 4117
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 4118
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "getAccessibilityInteractionController called when there is no mView"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 4121
    :cond_c
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionController:Landroid/view/AccessibilityInteractionController;

    #@e
    if-nez v0, :cond_17

    #@10
    .line 4122
    new-instance v0, Landroid/view/AccessibilityInteractionController;

    #@12
    invoke-direct {v0, p0}, Landroid/view/AccessibilityInteractionController;-><init>(Landroid/view/ViewRootImpl;)V

    #@15
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionController:Landroid/view/AccessibilityInteractionController;

    #@17
    .line 4124
    :cond_17
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionController:Landroid/view/AccessibilityInteractionController;

    #@19
    return-object v0
.end method

.method public getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z
    .registers 7
    .parameter "child"
    .parameter "r"
    .parameter "offset"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 976
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    if-eq p1, v0, :cond_d

    #@5
    .line 977
    new-instance v0, Ljava/lang/RuntimeException;

    #@7
    const-string v1, "child is not mine, honest!"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 981
    :cond_d
    iget v0, p0, Landroid/view/ViewRootImpl;->mWidth:I

    #@f
    iget v1, p0, Landroid/view/ViewRootImpl;->mHeight:I

    #@11
    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@14
    move-result v0

    #@15
    return v0
.end method

.method getHostVisibility()I
    .registers 2

    #@0
    .prologue
    .line 988
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mAppVisible:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/16 v0, 0x8

    #@d
    goto :goto_a
.end method

.method public getLastTouchPoint(Landroid/graphics/Point;)V
    .registers 3
    .parameter "outLocation"

    #@0
    .prologue
    .line 4096
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mLastTouchPoint:Landroid/graphics/PointF;

    #@2
    iget v0, v0, Landroid/graphics/PointF;->x:F

    #@4
    float-to-int v0, v0

    #@5
    iput v0, p1, Landroid/graphics/Point;->x:I

    #@7
    .line 4097
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mLastTouchPoint:Landroid/graphics/PointF;

    #@9
    iget v0, v0, Landroid/graphics/PointF;->y:F

    #@b
    float-to-int v0, v0

    #@c
    iput v0, p1, Landroid/graphics/Point;->y:I

    #@e
    .line 4098
    return-void
.end method

.method final getLocation()Landroid/view/WindowLeaked;
    .registers 2

    #@0
    .prologue
    .line 808
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mLocation:Landroid/view/WindowLeaked;

    #@2
    return-object v0
.end method

.method public getParent()Landroid/view/ViewParent;
    .registers 2

    #@0
    .prologue
    .line 972
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getParentForAccessibility()Landroid/view/ViewParent;
    .registers 2

    #@0
    .prologue
    .line 2690
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 804
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method handleAppVisibility(Z)V
    .registers 3
    .parameter "visible"

    #@0
    .prologue
    .line 850
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mAppVisible:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 851
    iput-boolean p1, p0, Landroid/view/ViewRootImpl;->mAppVisible:Z

    #@6
    .line 852
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@9
    .line 854
    :cond_9
    return-void
.end method

.method public handleDispatchDoneAnimating()V
    .registers 2

    #@0
    .prologue
    .line 4087
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mWindowsAnimating:Z

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 4088
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mWindowsAnimating:Z

    #@7
    .line 4089
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mDirty:Landroid/graphics/Rect;

    #@9
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_13

    #@f
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@11
    if-eqz v0, :cond_16

    #@13
    .line 4090
    :cond_13
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@16
    .line 4093
    :cond_16
    return-void
.end method

.method public handleDispatchSystemUiVisibilityChanged(Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;)V
    .registers 6
    .parameter "args"

    #@0
    .prologue
    .line 4065
    iget v1, p0, Landroid/view/ViewRootImpl;->mSeq:I

    #@2
    iget v2, p1, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->seq:I

    #@4
    if-eq v1, v2, :cond_12

    #@6
    .line 4069
    iget v1, p1, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->seq:I

    #@8
    iput v1, p0, Landroid/view/ViewRootImpl;->mSeq:I

    #@a
    .line 4070
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@c
    const/4 v2, 0x1

    #@d
    iput-boolean v2, v1, Landroid/view/View$AttachInfo;->mForceReportNewAttributes:Z

    #@f
    .line 4071
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@12
    .line 4073
    :cond_12
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@14
    if-nez v1, :cond_17

    #@16
    .line 4084
    :cond_16
    :goto_16
    return-void

    #@17
    .line 4074
    :cond_17
    iget v1, p1, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->localChanges:I

    #@19
    if-eqz v1, :cond_24

    #@1b
    .line 4075
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1d
    iget v2, p1, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->localValue:I

    #@1f
    iget v3, p1, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->localChanges:I

    #@21
    invoke-virtual {v1, v2, v3}, Landroid/view/View;->updateLocalSystemUiVisibility(II)Z

    #@24
    .line 4077
    :cond_24
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@26
    if-eqz v1, :cond_16

    #@28
    .line 4078
    iget v1, p1, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;->globalVisibility:I

    #@2a
    and-int/lit8 v0, v1, 0xf

    #@2c
    .line 4079
    .local v0, visibility:I
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2e
    iget v1, v1, Landroid/view/View$AttachInfo;->mGlobalSystemUiVisibility:I

    #@30
    if-eq v0, v1, :cond_16

    #@32
    .line 4080
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@34
    iput v0, v1, Landroid/view/View$AttachInfo;->mGlobalSystemUiVisibility:I

    #@36
    .line 4081
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@38
    invoke-virtual {v1, v0}, Landroid/view/View;->dispatchSystemUiVisibilityChanged(I)V

    #@3b
    goto :goto_16
.end method

.method handleGetNewSurface()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 857
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mNewSurfaceNeeded:Z

    #@3
    .line 858
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@5
    .line 859
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@8
    .line 860
    return-void
.end method

.method handleImeFinishedEvent(IZ)V
    .registers 10
    .parameter "seq"
    .parameter "handled"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 3776
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mCurrentInputEvent:Landroid/view/ViewRootImpl$QueuedInputEvent;

    #@3
    .line 3777
    .local v1, q:Landroid/view/ViewRootImpl$QueuedInputEvent;
    if-eqz v1, :cond_12

    #@5
    iget-object v3, v1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@7
    invoke-virtual {v3}, Landroid/view/InputEvent;->getSequenceNumber()I

    #@a
    move-result v3

    #@b
    if-ne v3, p1, :cond_12

    #@d
    .line 3782
    if-eqz p2, :cond_13

    #@f
    .line 3783
    invoke-direct {p0, v1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@12
    .line 3825
    :cond_12
    :goto_12
    return-void

    #@13
    .line 3785
    :cond_13
    iget-object v3, v1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@15
    instance-of v3, v3, Landroid/view/KeyEvent;

    #@17
    if-eqz v3, :cond_49

    #@19
    .line 3786
    iget-object v0, v1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@1b
    check-cast v0, Landroid/view/KeyEvent;

    #@1d
    .line 3787
    .local v0, event:Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    #@20
    move-result v3

    #@21
    if-eq v3, v6, :cond_45

    #@23
    .line 3791
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@25
    iget-boolean v3, v3, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@27
    if-nez v3, :cond_45

    #@29
    .line 3792
    const-string v3, "ViewRootImpl"

    #@2b
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v5, "Dropping event due to no window focus: "

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 3793
    invoke-direct {p0, v1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@44
    goto :goto_12

    #@45
    .line 3797
    :cond_45
    invoke-direct {p0, v1}, Landroid/view/ViewRootImpl;->deliverKeyEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@48
    goto :goto_12

    #@49
    .line 3799
    .end local v0           #event:Landroid/view/KeyEvent;
    :cond_49
    iget-object v0, v1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@4b
    check-cast v0, Landroid/view/MotionEvent;

    #@4d
    .line 3800
    .local v0, event:Landroid/view/MotionEvent;
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    #@50
    move-result v3

    #@51
    const/4 v4, 0x3

    #@52
    if-eq v3, v4, :cond_7c

    #@54
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    #@57
    move-result v3

    #@58
    if-eq v3, v6, :cond_7c

    #@5a
    .line 3805
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5c
    iget-boolean v3, v3, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@5e
    if-nez v3, :cond_7c

    #@60
    .line 3806
    const-string v3, "ViewRootImpl"

    #@62
    new-instance v4, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v5, "Dropping event due to no window focus: "

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 3807
    invoke-direct {p0, v1, v6}, Landroid/view/ViewRootImpl;->finishInputEvent(Landroid/view/ViewRootImpl$QueuedInputEvent;Z)V

    #@7b
    goto :goto_12

    #@7c
    .line 3811
    :cond_7c
    iget-object v3, v1, Landroid/view/ViewRootImpl$QueuedInputEvent;->mEvent:Landroid/view/InputEvent;

    #@7e
    invoke-virtual {v3}, Landroid/view/InputEvent;->getSource()I

    #@81
    move-result v2

    #@82
    .line 3812
    .local v2, source:I
    and-int/lit8 v3, v2, 0x4

    #@84
    if-eqz v3, :cond_8a

    #@86
    .line 3813
    invoke-direct {p0, v1}, Landroid/view/ViewRootImpl;->deliverTrackballEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@89
    goto :goto_12

    #@8a
    .line 3815
    :cond_8a
    invoke-direct {p0, v1}, Landroid/view/ViewRootImpl;->deliverGenericMotionEventPostIme(Landroid/view/ViewRootImpl$QueuedInputEvent;)V

    #@8d
    goto :goto_12
.end method

.method handleScreenStateChange(Z)V
    .registers 5
    .parameter "on"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 863
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mScreenOn:Z

    #@5
    if-eq p1, v0, :cond_1e

    #@7
    .line 864
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    iput-boolean p1, v0, Landroid/view/View$AttachInfo;->mScreenOn:Z

    #@b
    .line 865
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@d
    if-eqz v0, :cond_17

    #@f
    .line 866
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@11
    if-eqz p1, :cond_1f

    #@13
    move v0, v1

    #@14
    :goto_14
    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchScreenStateChanged(I)V

    #@17
    .line 868
    :cond_17
    if-eqz p1, :cond_1e

    #@19
    .line 869
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@1b
    .line 870
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@1e
    .line 873
    :cond_1e
    return-void

    #@1f
    .line 866
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_14
.end method

.method invalidate()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 895
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mDirty:Landroid/graphics/Rect;

    #@3
    iget v1, p0, Landroid/view/ViewRootImpl;->mWidth:I

    #@5
    iget v2, p0, Landroid/view/ViewRootImpl;->mHeight:I

    #@7
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@a
    .line 896
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@d
    .line 897
    return-void
.end method

.method public invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 4
    .parameter "child"
    .parameter "dirty"

    #@0
    .prologue
    .line 911
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p2}, Landroid/view/ViewRootImpl;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    #@4
    .line 912
    return-void
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .registers 13
    .parameter "location"
    .parameter "dirty"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, -0x1

    #@2
    const/high16 v9, 0x3f00

    #@4
    const/4 v8, 0x0

    #@5
    const/4 v7, 0x0

    #@6
    .line 915
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@9
    .line 918
    if-nez p2, :cond_f

    #@b
    .line 919
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->invalidate()V

    #@e
    .line 959
    :cond_e
    :goto_e
    return-object v8

    #@f
    .line 921
    :cond_f
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_19

    #@15
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@17
    if-eqz v3, :cond_e

    #@19
    .line 925
    :cond_19
    iget v3, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@1b
    if-nez v3, :cond_21

    #@1d
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@1f
    if-eqz v3, :cond_44

    #@21
    .line 926
    :cond_21
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@23
    invoke-virtual {v3, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@26
    .line 927
    iget-object p2, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@28
    .line 928
    iget v3, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@2a
    if-eqz v3, :cond_32

    #@2c
    .line 929
    iget v3, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@2e
    neg-int v3, v3

    #@2f
    invoke-virtual {p2, v7, v3}, Landroid/graphics/Rect;->offset(II)V

    #@32
    .line 931
    :cond_32
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@34
    if-eqz v3, :cond_3b

    #@36
    .line 932
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@38
    invoke-virtual {v3, p2}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInAppWindowToScreen(Landroid/graphics/Rect;)V

    #@3b
    .line 934
    :cond_3b
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3d
    iget-boolean v3, v3, Landroid/view/View$AttachInfo;->mScalingRequired:Z

    #@3f
    if-eqz v3, :cond_44

    #@41
    .line 935
    invoke-virtual {p2, v4, v4}, Landroid/graphics/Rect;->inset(II)V

    #@44
    .line 939
    :cond_44
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mDirty:Landroid/graphics/Rect;

    #@46
    .line 940
    .local v2, localDirty:Landroid/graphics/Rect;
    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    #@49
    move-result v3

    #@4a
    if-nez v3, :cond_5a

    #@4c
    invoke-virtual {v2, p2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@4f
    move-result v3

    #@50
    if-nez v3, :cond_5a

    #@52
    .line 941
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@54
    iput-boolean v5, v3, Landroid/view/View$AttachInfo;->mSetIgnoreDirtyState:Z

    #@56
    .line 942
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@58
    iput-boolean v5, v3, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@5a
    .line 946
    :cond_5a
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@5c
    iget v4, p2, Landroid/graphics/Rect;->top:I

    #@5e
    iget v5, p2, Landroid/graphics/Rect;->right:I

    #@60
    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    #@62
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->union(IIII)V

    #@65
    .line 949
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@67
    iget v0, v3, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@69
    .line 950
    .local v0, appScale:F
    iget v3, p0, Landroid/view/ViewRootImpl;->mWidth:I

    #@6b
    int-to-float v3, v3

    #@6c
    mul-float/2addr v3, v0

    #@6d
    add-float/2addr v3, v9

    #@6e
    float-to-int v3, v3

    #@6f
    iget v4, p0, Landroid/view/ViewRootImpl;->mHeight:I

    #@71
    int-to-float v4, v4

    #@72
    mul-float/2addr v4, v0

    #@73
    add-float/2addr v4, v9

    #@74
    float-to-int v4, v4

    #@75
    invoke-virtual {v2, v7, v7, v3, v4}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@78
    move-result v1

    #@79
    .line 952
    .local v1, intersected:Z
    if-nez v1, :cond_7e

    #@7b
    .line 953
    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    #@7e
    .line 955
    :cond_7e
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mWillDrawSoon:Z

    #@80
    if-nez v3, :cond_e

    #@82
    if-nez v1, :cond_88

    #@84
    iget-boolean v3, p0, Landroid/view/ViewRootImpl;->mIsAnimating:Z

    #@86
    if-eqz v3, :cond_e

    #@88
    .line 956
    :cond_88
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@8b
    goto :goto_e
.end method

.method invalidateDisplayLists()V
    .registers 5

    #@0
    .prologue
    .line 2470
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mDisplayLists:Ljava/util/ArrayList;

    #@2
    .line 2471
    .local v2, displayLists:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/DisplayList;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 2473
    .local v0, count:I
    const/4 v3, 0x0

    #@7
    .local v3, i:I
    :goto_7
    if-ge v3, v0, :cond_18

    #@9
    .line 2474
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/view/DisplayList;

    #@f
    .line 2475
    .local v1, displayList:Landroid/view/DisplayList;
    invoke-virtual {v1}, Landroid/view/DisplayList;->invalidate()V

    #@12
    .line 2476
    invoke-virtual {v1}, Landroid/view/DisplayList;->clear()V

    #@15
    .line 2473
    add-int/lit8 v3, v3, 0x1

    #@17
    goto :goto_7

    #@18
    .line 2479
    .end local v1           #displayList:Landroid/view/DisplayList;
    :cond_18
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@1b
    .line 2480
    return-void
.end method

.method invalidateWorld(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    .line 900
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    #@3
    .line 901
    instance-of v2, p1, Landroid/view/ViewGroup;

    #@5
    if-eqz v2, :cond_1b

    #@7
    move-object v1, p1

    #@8
    .line 902
    check-cast v1, Landroid/view/ViewGroup;

    #@a
    .line 903
    .local v1, parent:Landroid/view/ViewGroup;
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@e
    move-result v2

    #@f
    if-ge v0, v2, :cond_1b

    #@11
    .line 904
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {p0, v2}, Landroid/view/ViewRootImpl;->invalidateWorld(Landroid/view/View;)V

    #@18
    .line 903
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_b

    #@1b
    .line 907
    .end local v0           #i:I
    .end local v1           #parent:Landroid/view/ViewGroup;
    :cond_1b
    return-void
.end method

.method public isLayoutRequested()Z
    .registers 2

    #@0
    .prologue
    .line 891
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@2
    return v0
.end method

.method public loadSystemProperties()V
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x18

    #@2
    const/4 v4, 0x0

    #@3
    .line 4321
    const-string v1, "debug.layout"

    #@5
    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8
    move-result v0

    #@9
    .line 4323
    .local v0, layout:Z
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@b
    iget-boolean v1, v1, Landroid/view/View$AttachInfo;->mDebugLayout:Z

    #@d
    if-eq v0, v1, :cond_22

    #@f
    .line 4324
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mDebugLayout:Z

    #@13
    .line 4325
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@15
    invoke-virtual {v1, v5}, Landroid/view/ViewRootImpl$ViewRootHandler;->hasMessages(I)Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_22

    #@1b
    .line 4326
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@1d
    const-wide/16 v2, 0xc8

    #@1f
    invoke-virtual {v1, v5, v2, v3}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendEmptyMessageDelayed(IJ)Z

    #@22
    .line 4330
    :cond_22
    const-string v1, "debug.view.fps_log"

    #@24
    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_2d

    #@2a
    .line 4331
    const/4 v1, 0x1

    #@2b
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->DEBUG_FPS_FROM_BUILD_PROPORTY:Z

    #@2d
    .line 4334
    :cond_2d
    return-void
.end method

.method public onHardwarePostDraw(Landroid/view/HardwareCanvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 2059
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@2
    if-eqz v0, :cond_16

    #@4
    .line 2060
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mResizePaint:Landroid/graphics/Paint;

    #@6
    iget v1, p0, Landroid/view/ViewRootImpl;->mResizeAlpha:I

    #@8
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    #@b
    .line 2061
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@d
    const/4 v1, 0x0

    #@e
    iget v2, p0, Landroid/view/ViewRootImpl;->mHardwareYOffset:I

    #@10
    int-to-float v2, v2

    #@11
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mResizePaint:Landroid/graphics/Paint;

    #@13
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/HardwareCanvas;->drawHardwareLayer(Landroid/view/HardwareLayer;FFLandroid/graphics/Paint;)V

    #@16
    .line 2063
    :cond_16
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl;->drawAccessibilityFocusedDrawableIfNeeded(Landroid/graphics/Canvas;)V

    #@19
    .line 2064
    return-void
.end method

.method public onHardwarePreDraw(Landroid/view/HardwareCanvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 2055
    const/4 v0, 0x0

    #@1
    iget v1, p0, Landroid/view/ViewRootImpl;->mHardwareYOffset:I

    #@3
    neg-int v1, v1

    #@4
    int-to-float v1, v1

    #@5
    invoke-virtual {p1, v0, v1}, Landroid/view/HardwareCanvas;->translate(FF)V

    #@8
    .line 2056
    return-void
.end method

.method outputDisplayList(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 2070
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v1, :cond_17

    #@4
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mHardwareCanvas:Landroid/view/HardwareCanvas;

    #@8
    if-eqz v1, :cond_17

    #@a
    .line 2071
    invoke-virtual {p1}, Landroid/view/View;->getDisplayList()Landroid/view/DisplayList;

    #@d
    move-result-object v0

    #@e
    .line 2072
    .local v0, displayList:Landroid/view/DisplayList;
    if-eqz v0, :cond_17

    #@10
    .line 2073
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@12
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mHardwareCanvas:Landroid/view/HardwareCanvas;

    #@14
    invoke-virtual {v1, v0}, Landroid/view/HardwareCanvas;->outputDisplayList(Landroid/view/DisplayList;)V

    #@17
    .line 2076
    .end local v0           #displayList:Landroid/view/DisplayList;
    :cond_17
    return-void
.end method

.method public performHapticFeedback(IZ)Z
    .registers 6
    .parameter "effectId"
    .parameter "always"

    #@0
    .prologue
    .line 4219
    :try_start_0
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@2
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@4
    invoke-interface {v1, v2, p1, p2}, Landroid/view/IWindowSession;->performHapticFeedback(Landroid/view/IWindow;IZ)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 4221
    :goto_8
    return v1

    #@9
    .line 4220
    :catch_9
    move-exception v0

    #@a
    .line 4221
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public playSoundEffect(I)V
    .registers 7
    .parameter "effectId"

    #@0
    .prologue
    .line 4174
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 4177
    :try_start_3
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->getAudioManager()Landroid/media/AudioManager;

    #@6
    move-result-object v0

    #@7
    .line 4179
    .local v0, audioManager:Landroid/media/AudioManager;
    packed-switch p1, :pswitch_data_76

    #@a
    .line 4204
    :pswitch_a
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string/jumbo v4, "unknown effect id "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, " not defined in "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-class v4, Landroid/view/SoundEffectConstants;

    #@24
    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@33
    throw v2
    :try_end_34
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_34} :catch_34

    #@34
    .line 4207
    .end local v0           #audioManager:Landroid/media/AudioManager;
    :catch_34
    move-exception v1

    #@35
    .line 4209
    .local v1, e:Ljava/lang/IllegalStateException;
    const-string v2, "ViewRootImpl"

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v4, "FATAL EXCEPTION when attempting to play sound effect: "

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 4210
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@50
    .line 4212
    .end local v1           #e:Ljava/lang/IllegalStateException;
    :goto_50
    return-void

    #@51
    .line 4181
    .restart local v0       #audioManager:Landroid/media/AudioManager;
    :pswitch_51
    const/4 v2, 0x0

    #@52
    :try_start_52
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@55
    goto :goto_50

    #@56
    .line 4184
    :pswitch_56
    const/4 v2, 0x2

    #@57
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@5a
    goto :goto_50

    #@5b
    .line 4187
    :pswitch_5b
    const/4 v2, 0x3

    #@5c
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@5f
    goto :goto_50

    #@60
    .line 4190
    :pswitch_60
    const/4 v2, 0x4

    #@61
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@64
    goto :goto_50

    #@65
    .line 4193
    :pswitch_65
    const/4 v2, 0x1

    #@66
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@69
    goto :goto_50

    #@6a
    .line 4197
    :pswitch_6a
    const/16 v2, 0x9

    #@6c
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    #@6f
    goto :goto_50

    #@70
    .line 4200
    :pswitch_70
    const/16 v2, 0xa

    #@72
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V
    :try_end_75
    .catch Ljava/lang/IllegalStateException; {:try_start_52 .. :try_end_75} :catch_34

    #@75
    goto :goto_50

    #@76
    .line 4179
    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_51
        :pswitch_5b
        :pswitch_65
        :pswitch_60
        :pswitch_56
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_6a
        :pswitch_70
    .end packed-switch
.end method

.method public profile()V
    .registers 2

    #@0
    .prologue
    .line 483
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mProfile:Z

    #@3
    .line 484
    return-void
.end method

.method pushHardwareLayerUpdate(Landroid/view/HardwareLayer;)V
    .registers 3
    .parameter "layer"

    #@0
    .prologue
    .line 716
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    if-eqz v0, :cond_17

    #@6
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@a
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 717
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@12
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@14
    invoke-virtual {v0, p1}, Landroid/view/HardwareRenderer;->pushLayerUpdate(Landroid/view/HardwareLayer;)V

    #@17
    .line 719
    :cond_17
    return-void
.end method

.method public recomputeViewAttributes(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 2716
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 2717
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5
    if-ne v0, p1, :cond_13

    #@7
    .line 2718
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    const/4 v1, 0x1

    #@a
    iput-boolean v1, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@c
    .line 2719
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mWillDrawSoon:Z

    #@e
    if-nez v0, :cond_13

    #@10
    .line 2720
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@13
    .line 2723
    :cond_13
    return-void
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 5
    .parameter "child"
    .parameter "focused"

    #@0
    .prologue
    .line 2659
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 2665
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@7
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mOldFocusedView:Landroid/view/View;

    #@9
    invoke-virtual {v0, v1, p2}, Landroid/view/ViewTreeObserver;->dispatchOnGlobalFocusChange(Landroid/view/View;Landroid/view/View;)V

    #@c
    .line 2666
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@f
    .line 2668
    iput-object p2, p0, Landroid/view/ViewRootImpl;->mRealFocusedView:Landroid/view/View;

    #@11
    iput-object p2, p0, Landroid/view/ViewRootImpl;->mFocusedView:Landroid/view/View;

    #@13
    .line 2669
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 8
    .parameter "child"
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 4967
    invoke-virtual {p0, p2, p3}, Landroid/view/ViewRootImpl;->scrollToRectOrFocus(Landroid/graphics/Rect;Z)Z

    #@3
    move-result v0

    #@4
    .line 4968
    .local v0, scrolled:Z
    if-eqz p2, :cond_2a

    #@6
    .line 4969
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@8
    invoke-virtual {v1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@b
    .line 4970
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@d
    const/4 v2, 0x0

    #@e
    iget v3, p0, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@10
    neg-int v3, v3

    #@11
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@14
    .line 4971
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@16
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@18
    iget v2, v2, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@1a
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1c
    iget v3, v3, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@1e
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    #@21
    .line 4973
    :try_start_21
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@23
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@25
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@27
    invoke-interface {v1, v2, v3, p3}, Landroid/view/IWindowSession;->onRectangleOnScreenRequested(Landroid/os/IBinder;Landroid/graphics/Rect;Z)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2a} :catch_2b

    #@2a
    .line 4978
    :cond_2a
    :goto_2a
    return v0

    #@2b
    .line 4974
    :catch_2b
    move-exception v1

    #@2c
    goto :goto_2a
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 2
    .parameter "disallowIntercept"

    #@0
    .prologue
    .line 4964
    return-void
.end method

.method public requestFitSystemWindows()V
    .registers 2

    #@0
    .prologue
    .line 877
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 878
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mFitSystemWindowsRequested:Z

    #@6
    .line 879
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@9
    .line 880
    return-void
.end method

.method public requestLayout()V
    .registers 2

    #@0
    .prologue
    .line 884
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 885
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mLayoutRequested:Z

    #@6
    .line 886
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@9
    .line 887
    return-void
.end method

.method public requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 12
    .parameter "child"
    .parameter "event"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 4877
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    if-nez v7, :cond_7

    #@5
    .line 4878
    const/4 v7, 0x0

    #@6
    .line 4912
    :goto_6
    return v7

    #@7
    .line 4882
    :cond_7
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@a
    move-result v1

    #@b
    .line 4883
    .local v1, eventType:I
    sparse-switch v1, :sswitch_data_52

    #@e
    .line 4911
    :cond_e
    :goto_e
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@10
    invoke-virtual {v7, p2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@13
    .line 4912
    const/4 v7, 0x1

    #@14
    goto :goto_6

    #@15
    .line 4885
    :sswitch_15
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getSourceNodeId()J

    #@18
    move-result-wide v5

    #@19
    .line 4886
    .local v5, sourceNodeId:J
    invoke-static {v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@1c
    move-result v0

    #@1d
    .line 4888
    .local v0, accessibilityViewId:I
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1f
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewByAccessibilityId(I)Landroid/view/View;

    #@22
    move-result-object v4

    #@23
    .line 4889
    .local v4, source:Landroid/view/View;
    if-eqz v4, :cond_e

    #@25
    .line 4890
    invoke-virtual {v4}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@28
    move-result-object v3

    #@29
    .line 4891
    .local v3, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v3, :cond_e

    #@2b
    .line 4892
    invoke-static {v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@2e
    move-result v7

    #@2f
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@32
    move-result-object v2

    #@33
    .line 4894
    .local v2, node:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {p0, v4, v2}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@36
    goto :goto_e

    #@37
    .line 4899
    .end local v0           #accessibilityViewId:I
    .end local v2           #node:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v3           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    .end local v4           #source:Landroid/view/View;
    .end local v5           #sourceNodeId:J
    :sswitch_37
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getSourceNodeId()J

    #@3a
    move-result-wide v5

    #@3b
    .line 4900
    .restart local v5       #sourceNodeId:J
    invoke-static {v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@3e
    move-result v0

    #@3f
    .line 4902
    .restart local v0       #accessibilityViewId:I
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@41
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewByAccessibilityId(I)Landroid/view/View;

    #@44
    move-result-object v4

    #@45
    .line 4903
    .restart local v4       #source:Landroid/view/View;
    if-eqz v4, :cond_e

    #@47
    .line 4904
    invoke-virtual {v4}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@4a
    move-result-object v3

    #@4b
    .line 4905
    .restart local v3       #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v3, :cond_e

    #@4d
    .line 4906
    invoke-virtual {p0, v8, v8}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@50
    goto :goto_e

    #@51
    .line 4883
    nop

    #@52
    :sswitch_data_52
    .sparse-switch
        0x8000 -> :sswitch_15
        0x10000 -> :sswitch_37
    .end sparse-switch
.end method

.method public requestTransitionStart(Landroid/animation/LayoutTransition;)V
    .registers 3
    .parameter "transition"

    #@0
    .prologue
    .line 1012
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_1c

    #@c
    .line 1013
    :cond_c
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@e
    if-nez v0, :cond_17

    #@10
    .line 1014
    new-instance v0, Ljava/util/ArrayList;

    #@12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@15
    iput-object v0, p0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@17
    .line 1016
    :cond_17
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mPendingTransitions:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 1018
    :cond_1c
    return-void
.end method

.method public requestTransparentRegion(Landroid/view/View;)V
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 2006
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->checkThread()V

    #@3
    .line 2007
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5
    if-ne v0, p1, :cond_18

    #@7
    .line 2008
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@9
    iget v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@b
    or-int/lit16 v1, v1, 0x200

    #@d
    iput v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@f
    .line 2011
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z

    #@12
    .line 2012
    const/4 v0, 0x0

    #@13
    iput v0, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChangesFlag:I

    #@15
    .line 2013
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->requestLayout()V

    #@18
    .line 2015
    :cond_18
    return-void
.end method

.method public requestUpdateConfiguration(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "config"

    #@0
    .prologue
    .line 4316
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@2
    const/16 v2, 0x12

    #@4
    invoke-virtual {v1, v2, p1}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 4317
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@a
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 4318
    return-void
.end method

.method scheduleConsumeBatchedInput()V
    .registers 5

    #@0
    .prologue
    .line 4525
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mConsumeBatchedInputScheduled:Z

    #@2
    if-nez v0, :cond_10

    #@4
    .line 4526
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mConsumeBatchedInputScheduled:Z

    #@7
    .line 4527
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@9
    const/4 v1, 0x0

    #@a
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mConsumedBatchedInputRunnable:Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@10
    .line 4530
    :cond_10
    return-void
.end method

.method scheduleTraversals()V
    .registers 5

    #@0
    .prologue
    .line 1021
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mTraversalScheduled:Z

    #@2
    if-nez v0, :cond_1f

    #@4
    .line 1022
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mTraversalScheduled:Z

    #@7
    .line 1023
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@9
    invoke-virtual {v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->getLooper()Landroid/os/Looper;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/os/Looper;->postSyncBarrier()I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/view/ViewRootImpl;->mTraversalBarrier:I

    #@13
    .line 1024
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@15
    const/4 v1, 0x2

    #@16
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mTraversalRunnable:Landroid/view/ViewRootImpl$TraversalRunnable;

    #@18
    const/4 v3, 0x0

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@1c
    .line 1026
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleConsumeBatchedInput()V

    #@1f
    .line 1028
    :cond_1f
    return-void
.end method

.method scrollToRectOrFocus(Landroid/graphics/Rect;Z)Z
    .registers 13
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 2483
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    .line 2484
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@6
    .line 2485
    .local v1, ci:Landroid/graphics/Rect;
    iget-object v5, v0, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@8
    .line 2486
    .local v5, vi:Landroid/graphics/Rect;
    const/4 v4, 0x0

    #@9
    .line 2487
    .local v4, scrollY:I
    const/4 v3, 0x0

    #@a
    .line 2489
    .local v3, handled:Z
    iget v6, v5, Landroid/graphics/Rect;->left:I

    #@c
    iget v8, v1, Landroid/graphics/Rect;->left:I

    #@e
    if-gt v6, v8, :cond_22

    #@10
    iget v6, v5, Landroid/graphics/Rect;->top:I

    #@12
    iget v8, v1, Landroid/graphics/Rect;->top:I

    #@14
    if-gt v6, v8, :cond_22

    #@16
    iget v6, v5, Landroid/graphics/Rect;->right:I

    #@18
    iget v8, v1, Landroid/graphics/Rect;->right:I

    #@1a
    if-gt v6, v8, :cond_22

    #@1c
    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    #@1e
    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    #@20
    if-le v6, v8, :cond_41

    #@22
    .line 2495
    :cond_22
    iget v4, p0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@24
    .line 2502
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mRealFocusedView:Landroid/view/View;

    #@26
    .line 2507
    .local v2, focus:Landroid/view/View;
    if-eqz v2, :cond_2e

    #@28
    iget-object v6, v2, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2a
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2c
    if-eq v6, v8, :cond_32

    #@2e
    .line 2508
    :cond_2e
    iput-object v9, p0, Landroid/view/ViewRootImpl;->mRealFocusedView:Landroid/view/View;

    #@30
    move v6, v7

    #@31
    .line 2601
    .end local v2           #focus:Landroid/view/View;
    :goto_31
    return v6

    #@32
    .line 2512
    .restart local v2       #focus:Landroid/view/View;
    :cond_32
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mLastScrolledFocus:Landroid/view/View;

    #@34
    if-eq v2, v6, :cond_37

    #@36
    .line 2516
    const/4 p1, 0x0

    #@37
    .line 2521
    :cond_37
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mLastScrolledFocus:Landroid/view/View;

    #@39
    if-ne v2, v6, :cond_6b

    #@3b
    iget-boolean v6, p0, Landroid/view/ViewRootImpl;->mScrollMayChange:Z

    #@3d
    if-nez v6, :cond_6b

    #@3f
    if-nez p1, :cond_6b

    #@41
    .line 2587
    .end local v2           #focus:Landroid/view/View;
    :cond_41
    :goto_41
    iget v6, p0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@43
    if-eq v4, v6, :cond_69

    #@45
    .line 2590
    if-nez p2, :cond_e9

    #@47
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mResizeBuffer:Landroid/view/HardwareLayer;

    #@49
    if-nez v6, :cond_e9

    #@4b
    .line 2591
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@4d
    if-nez v6, :cond_5c

    #@4f
    .line 2592
    new-instance v6, Landroid/widget/Scroller;

    #@51
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@53
    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@56
    move-result-object v8

    #@57
    invoke-direct {v6, v8}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    #@5a
    iput-object v6, p0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@5c
    .line 2594
    :cond_5c
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@5e
    iget v8, p0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@60
    iget v9, p0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@62
    sub-int v9, v4, v9

    #@64
    invoke-virtual {v6, v7, v8, v7, v9}, Landroid/widget/Scroller;->startScroll(IIII)V

    #@67
    .line 2598
    :cond_67
    :goto_67
    iput v4, p0, Landroid/view/ViewRootImpl;->mScrollY:I

    #@69
    :cond_69
    move v6, v3

    #@6a
    .line 2601
    goto :goto_31

    #@6b
    .line 2528
    .restart local v2       #focus:Landroid/view/View;
    :cond_6b
    if-eqz v2, :cond_41

    #@6d
    .line 2532
    iput-object v2, p0, Landroid/view/ViewRootImpl;->mLastScrolledFocus:Landroid/view/View;

    #@6f
    .line 2533
    iput-boolean v7, p0, Landroid/view/ViewRootImpl;->mScrollMayChange:Z

    #@71
    .line 2536
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mVisRect:Landroid/graphics/Rect;

    #@73
    invoke-virtual {v2, v6, v9}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@76
    move-result v6

    #@77
    if-eqz v6, :cond_41

    #@79
    .line 2541
    if-nez p1, :cond_af

    #@7b
    .line 2542
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@7d
    invoke-virtual {v2, v6}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@80
    .line 2545
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@82
    instance-of v6, v6, Landroid/view/ViewGroup;

    #@84
    if-eqz v6, :cond_8f

    #@86
    .line 2546
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@88
    check-cast v6, Landroid/view/ViewGroup;

    #@8a
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@8c
    invoke-virtual {v6, v2, v8}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@8f
    .line 2560
    :cond_8f
    :goto_8f
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@91
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mVisRect:Landroid/graphics/Rect;

    #@93
    invoke-virtual {v6, v8}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    #@96
    move-result v6

    #@97
    if-eqz v6, :cond_41

    #@99
    .line 2564
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@9b
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    #@9e
    move-result v6

    #@9f
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@a1
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    #@a4
    move-result v8

    #@a5
    iget v9, v5, Landroid/graphics/Rect;->top:I

    #@a7
    sub-int/2addr v8, v9

    #@a8
    iget v9, v5, Landroid/graphics/Rect;->bottom:I

    #@aa
    sub-int/2addr v8, v9

    #@ab
    if-le v6, v8, :cond_b5

    #@ad
    .line 2581
    :cond_ad
    :goto_ad
    const/4 v3, 0x1

    #@ae
    goto :goto_41

    #@af
    .line 2554
    :cond_af
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@b1
    invoke-virtual {v6, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@b4
    goto :goto_8f

    #@b5
    .line 2570
    :cond_b5
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@b7
    iget v6, v6, Landroid/graphics/Rect;->top:I

    #@b9
    sub-int/2addr v6, v4

    #@ba
    iget v8, v5, Landroid/graphics/Rect;->top:I

    #@bc
    if-ge v6, v8, :cond_c8

    #@be
    .line 2571
    iget v6, v5, Landroid/graphics/Rect;->top:I

    #@c0
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@c2
    iget v8, v8, Landroid/graphics/Rect;->top:I

    #@c4
    sub-int/2addr v8, v4

    #@c5
    sub-int/2addr v6, v8

    #@c6
    sub-int/2addr v4, v6

    #@c7
    goto :goto_ad

    #@c8
    .line 2574
    :cond_c8
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@ca
    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    #@cc
    sub-int/2addr v6, v4

    #@cd
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@cf
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    #@d2
    move-result v8

    #@d3
    iget v9, v5, Landroid/graphics/Rect;->bottom:I

    #@d5
    sub-int/2addr v8, v9

    #@d6
    if-le v6, v8, :cond_ad

    #@d8
    .line 2576
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@da
    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    #@dc
    sub-int/2addr v6, v4

    #@dd
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@df
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    #@e2
    move-result v8

    #@e3
    iget v9, v5, Landroid/graphics/Rect;->bottom:I

    #@e5
    sub-int/2addr v8, v9

    #@e6
    sub-int/2addr v6, v8

    #@e7
    add-int/2addr v4, v6

    #@e8
    goto :goto_ad

    #@e9
    .line 2595
    .end local v2           #focus:Landroid/view/View;
    :cond_e9
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@eb
    if-eqz v6, :cond_67

    #@ed
    .line 2596
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mScroller:Landroid/widget/Scroller;

    #@ef
    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    #@f2
    goto/16 :goto_67
.end method

.method setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 10
    .parameter "view"
    .parameter "node"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2621
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3
    if-eqz v4, :cond_30

    #@5
    .line 2623
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@7
    .line 2624
    .local v1, focusNode:Landroid/view/accessibility/AccessibilityNodeInfo;
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@9
    .line 2625
    .local v0, focusHost:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->clearAccessibilityFocusNoCallbacks()V

    #@c
    .line 2632
    iput-object v6, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@e
    .line 2633
    iput-object v6, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@10
    .line 2635
    invoke-virtual {v0}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@13
    move-result-object v2

    #@14
    .line 2636
    .local v2, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v2, :cond_2d

    #@16
    .line 2638
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@18
    invoke-virtual {v1, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInParent(Landroid/graphics/Rect;)V

    #@1b
    .line 2639
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mTempRect:Landroid/graphics/Rect;

    #@1d
    invoke-virtual {v0, v4}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    #@20
    .line 2641
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@23
    move-result-wide v4

    #@24
    invoke-static {v4, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@27
    move-result v3

    #@28
    .line 2643
    .local v3, virtualNodeId:I
    const/16 v4, 0x80

    #@2a
    invoke-virtual {v2, v3, v4, v6}, Landroid/view/accessibility/AccessibilityNodeProvider;->performAction(IILandroid/os/Bundle;)Z

    #@2d
    .line 2646
    .end local v3           #virtualNodeId:I
    :cond_2d
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    #@30
    .line 2648
    .end local v0           #focusHost:Landroid/view/View;
    .end local v1           #focusNode:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v2           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_30
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@32
    if-eqz v4, :cond_39

    #@34
    .line 2650
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@36
    invoke-virtual {v4}, Landroid/view/View;->clearAccessibilityFocusNoCallbacks()V

    #@39
    .line 2654
    :cond_39
    iput-object p1, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@3b
    .line 2655
    iput-object p2, p0, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3d
    .line 2656
    return-void
.end method

.method public setDragFocus(Landroid/view/View;)V
    .registers 3
    .parameter "newDragTarget"

    #@0
    .prologue
    .line 4101
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mCurrentDragView:Landroid/view/View;

    #@2
    if-eq v0, p1, :cond_6

    #@4
    .line 4102
    iput-object p1, p0, Landroid/view/ViewRootImpl;->mCurrentDragView:Landroid/view/View;

    #@6
    .line 4104
    :cond_6
    return-void
.end method

.method setLayoutParams(Landroid/view/WindowManager$LayoutParams;Z)V
    .registers 9
    .parameter "attrs"
    .parameter "newView"

    #@0
    .prologue
    .line 812
    monitor-enter p0

    #@1
    .line 813
    :try_start_1
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@3
    iget v1, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@5
    .line 815
    .local v1, oldSoftInputMode:I
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@7
    iput v3, p0, Landroid/view/ViewRootImpl;->mClientWindowLayoutFlags:I

    #@9
    .line 817
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@b
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@d
    const/high16 v4, 0x2000

    #@f
    and-int v0, v3, v4

    #@11
    .line 820
    .local v0, compatibleWindowFlag:I
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@13
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@15
    iput v3, p1, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@17
    .line 821
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@19
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@1b
    iput v3, p1, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@1d
    .line 822
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@1f
    invoke-virtual {v3, p1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    #@22
    move-result v3

    #@23
    iput v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChangesFlag:I

    #@25
    .line 823
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@27
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@29
    or-int/2addr v4, v0

    #@2a
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2c
    .line 825
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@2e
    invoke-direct {p0, v3}, Landroid/view/ViewRootImpl;->applyKeepScreenOnFlag(Landroid/view/WindowManager$LayoutParams;)V

    #@31
    .line 828
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@33
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@35
    and-int/lit16 v2, v3, 0x1000

    #@37
    .line 830
    .local v2, wvgaAspectFlag:I
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@39
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@3b
    or-int/2addr v4, v2

    #@3c
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@3e
    .line 832
    if-eqz p2, :cond_47

    #@40
    .line 833
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@42
    iput v3, p0, Landroid/view/ViewRootImpl;->mSoftInputMode:I

    #@44
    .line 834
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->requestLayout()V

    #@47
    .line 837
    :cond_47
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@49
    and-int/lit16 v3, v3, 0xf0

    #@4b
    if-nez v3, :cond_5a

    #@4d
    .line 839
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@4f
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@51
    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@53
    and-int/lit16 v4, v4, -0xf1

    #@55
    and-int/lit16 v5, v1, 0xf0

    #@57
    or-int/2addr v4, v5

    #@58
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@5a
    .line 844
    :cond_5a
    const/4 v3, 0x1

    #@5b
    iput-boolean v3, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z

    #@5d
    .line 845
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@60
    .line 846
    monitor-exit p0

    #@61
    .line 847
    return-void

    #@62
    .line 846
    .end local v0           #compatibleWindowFlag:I
    .end local v1           #oldSoftInputMode:I
    .end local v2           #wvgaAspectFlag:I
    :catchall_62
    move-exception v3

    #@63
    monitor-exit p0
    :try_end_64
    .catchall {:try_start_1 .. :try_end_64} :catchall_62

    #@64
    throw v3
.end method

.method setLocalDragState(Ljava/lang/Object;)V
    .registers 2
    .parameter "obj"

    #@0
    .prologue
    .line 3939
    iput-object p1, p0, Landroid/view/ViewRootImpl;->mLocalDragState:Ljava/lang/Object;

    #@2
    .line 3940
    return-void
.end method

.method setStopped(Z)V
    .registers 3
    .parameter "stopped"

    #@0
    .prologue
    .line 963
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mStopped:Z

    #@2
    if-eq v0, p1, :cond_b

    #@4
    .line 964
    iput-boolean p1, p0, Landroid/view/ViewRootImpl;->mStopped:Z

    #@6
    .line 965
    if-nez p1, :cond_b

    #@8
    .line 966
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->scheduleTraversals()V

    #@b
    .line 969
    :cond_b
    return-void
.end method

.method public setView(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/View;)V
    .registers 17
    .parameter "view"
    .parameter "attrs"
    .parameter "panelParentView"

    #@0
    .prologue
    .line 509
    monitor-enter p0

    #@1
    .line 510
    :try_start_1
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3
    if-nez v1, :cond_2c1

    #@5
    .line 511
    iput-object p1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@7
    .line 512
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@9
    invoke-virtual {v1}, Landroid/view/View;->getRawLayoutDirection()I

    #@c
    move-result v1

    #@d
    iput v1, p0, Landroid/view/ViewRootImpl;->mViewLayoutDirectionInitial:I

    #@f
    .line 513
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFallbackEventHandler:Landroid/view/FallbackEventHandler;

    #@11
    invoke-interface {v1, p1}, Landroid/view/FallbackEventHandler;->setView(Landroid/view/View;)V

    #@14
    .line 514
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@16
    invoke-virtual {v1, p2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    #@19
    .line 515
    iget-object p2, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@1b
    .line 517
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1d
    iput v1, p0, Landroid/view/ViewRootImpl;->mClientWindowLayoutFlags:I

    #@1f
    .line 519
    const/4 v1, 0x0

    #@20
    const/4 v2, 0x0

    #@21
    invoke-virtual {p0, v1, v2}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@24
    .line 521
    instance-of v1, p1, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@26
    if-eqz v1, :cond_43

    #@28
    .line 522
    move-object v0, p1

    #@29
    check-cast v0, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@2b
    move-object v1, v0

    #@2c
    invoke-interface {v1}, Lcom/android/internal/view/RootViewSurfaceTaker;->willYouTakeTheSurface()Landroid/view/SurfaceHolder$Callback2;

    #@2f
    move-result-object v1

    #@30
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

    #@32
    .line 524
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback2;

    #@34
    if-eqz v1, :cond_43

    #@36
    .line 525
    new-instance v1, Landroid/view/ViewRootImpl$TakenSurfaceHolder;

    #@38
    invoke-direct {v1, p0}, Landroid/view/ViewRootImpl$TakenSurfaceHolder;-><init>(Landroid/view/ViewRootImpl;)V

    #@3b
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@3d
    .line 526
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@3f
    const/4 v2, 0x0

    #@40
    invoke-virtual {v1, v2}, Lcom/android/internal/view/BaseSurfaceHolder;->setFormat(I)V

    #@43
    .line 530
    :cond_43
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@45
    invoke-virtual {v1}, Landroid/view/CompatibilityInfoHolder;->get()Landroid/content/res/CompatibilityInfo;

    #@48
    move-result-object v9

    #@49
    .line 531
    .local v9, compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    invoke-virtual {v9}, Landroid/content/res/CompatibilityInfo;->getTranslator()Landroid/content/res/CompatibilityInfo$Translator;

    #@4c
    move-result-object v1

    #@4d
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@4f
    .line 534
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@51
    if-nez v1, :cond_5c

    #@53
    .line 535
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@55
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@58
    move-result-object v1

    #@59
    invoke-direct {p0, v1, p2}, Landroid/view/ViewRootImpl;->enableHardwareAcceleration(Landroid/content/Context;Landroid/view/WindowManager$LayoutParams;)V

    #@5c
    .line 538
    :cond_5c
    const/4 v12, 0x0

    #@5d
    .line 539
    .local v12, restore:Z
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@5f
    if-eqz v1, :cond_71

    #@61
    .line 540
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mSurface:Landroid/view/Surface;

    #@63
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@65
    invoke-virtual {v1, v2}, Landroid/view/Surface;->setCompatibilityTranslator(Landroid/content/res/CompatibilityInfo$Translator;)V

    #@68
    .line 541
    const/4 v12, 0x1

    #@69
    .line 542
    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->backup()V

    #@6c
    .line 543
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@6e
    invoke-virtual {v1, p2}, Landroid/content/res/CompatibilityInfo$Translator;->translateWindowLayout(Landroid/view/WindowManager$LayoutParams;)V

    #@71
    .line 547
    :cond_71
    invoke-virtual {v9}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@74
    move-result v1

    #@75
    if-nez v1, :cond_8d

    #@77
    .line 548
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@79
    const/high16 v2, 0x2000

    #@7b
    or-int/2addr v1, v2

    #@7c
    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@7e
    .line 550
    invoke-virtual {v9}, Landroid/content/res/CompatibilityInfo;->requiresWvgaAspect()Z

    #@81
    move-result v1

    #@82
    if-eqz v1, :cond_8a

    #@84
    .line 551
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@86
    or-int/lit16 v1, v1, 0x1000

    #@88
    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@8a
    .line 554
    :cond_8a
    const/4 v1, 0x1

    #@8b
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mLastInCompatMode:Z

    #@8d
    .line 557
    :cond_8d
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@8f
    iput v1, p0, Landroid/view/ViewRootImpl;->mSoftInputMode:I

    #@91
    .line 558
    const/4 v1, 0x1

    #@92
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChanged:Z

    #@94
    .line 559
    const/4 v1, -0x1

    #@95
    iput v1, p0, Landroid/view/ViewRootImpl;->mWindowAttributesChangesFlag:I

    #@97
    .line 560
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@99
    iput-object p1, v1, Landroid/view/View$AttachInfo;->mRootView:Landroid/view/View;

    #@9b
    .line 561
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9d
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@9f
    if-eqz v1, :cond_152

    #@a1
    const/4 v1, 0x1

    #@a2
    :goto_a2
    iput-boolean v1, v2, Landroid/view/View$AttachInfo;->mScalingRequired:Z

    #@a4
    .line 562
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a6
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@a8
    if-nez v1, :cond_155

    #@aa
    const/high16 v1, 0x3f80

    #@ac
    :goto_ac
    iput v1, v2, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@ae
    .line 564
    if-eqz p3, :cond_b8

    #@b0
    .line 565
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@b2
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    #@b5
    move-result-object v2

    #@b6
    iput-object v2, v1, Landroid/view/View$AttachInfo;->mPanelParentWindowToken:Landroid/os/IBinder;

    #@b8
    .line 568
    :cond_b8
    const/4 v1, 0x1

    #@b9
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@bb
    .line 574
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->requestLayout()V

    #@be
    .line 575
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@c0
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@c2
    and-int/lit8 v1, v1, 0x2

    #@c4
    if-nez v1, :cond_cd

    #@c6
    .line 577
    new-instance v1, Landroid/view/InputChannel;

    #@c8
    invoke-direct {v1}, Landroid/view/InputChannel;-><init>()V

    #@cb
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;
    :try_end_cd
    .catchall {:try_start_1 .. :try_end_cd} :catchall_14f

    #@cd
    .line 580
    :cond_cd
    :try_start_cd
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@cf
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@d1
    iput v1, p0, Landroid/view/ViewRootImpl;->mOrigWindowType:I

    #@d3
    .line 581
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@d5
    const/4 v2, 0x1

    #@d6
    iput-boolean v2, v1, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@d8
    .line 582
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->collectViewAttributes()Z

    #@db
    .line 583
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@dd
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@df
    iget v3, p0, Landroid/view/ViewRootImpl;->mSeq:I

    #@e1
    iget-object v4, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@e3
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->getHostVisibility()I

    #@e6
    move-result v5

    #@e7
    iget-object v6, p0, Landroid/view/ViewRootImpl;->mDisplay:Landroid/view/Display;

    #@e9
    invoke-virtual {v6}, Landroid/view/Display;->getDisplayId()I

    #@ec
    move-result v6

    #@ed
    iget-object v7, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@ef
    iget-object v7, v7, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@f1
    iget-object v8, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@f3
    invoke-interface/range {v1 .. v8}, Landroid/view/IWindowSession;->addToDisplay(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I
    :try_end_f6
    .catchall {:try_start_cd .. :try_end_f6} :catchall_180
    .catch Landroid/os/RemoteException; {:try_start_cd .. :try_end_f6} :catch_15b

    #@f6
    move-result v11

    #@f7
    .line 596
    .local v11, res:I
    if-eqz v12, :cond_fc

    #@f9
    .line 597
    :try_start_f9
    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->restore()V

    #@fc
    .line 601
    :cond_fc
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@fe
    if-eqz v1, :cond_109

    #@100
    .line 602
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@102
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@104
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@106
    invoke-virtual {v1, v2}, Landroid/content/res/CompatibilityInfo$Translator;->translateRectInScreenToAppWindow(Landroid/graphics/Rect;)V

    #@109
    .line 604
    :cond_109
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@10b
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@10d
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@10f
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@112
    .line 605
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@114
    const/4 v2, 0x0

    #@115
    const/4 v3, 0x0

    #@116
    const/4 v4, 0x0

    #@117
    const/4 v5, 0x0

    #@118
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@11b
    .line 607
    if-gez v11, :cond_270

    #@11d
    .line 608
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11f
    const/4 v2, 0x0

    #@120
    iput-object v2, v1, Landroid/view/View$AttachInfo;->mRootView:Landroid/view/View;

    #@122
    .line 609
    const/4 v1, 0x0

    #@123
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@125
    .line 610
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFallbackEventHandler:Landroid/view/FallbackEventHandler;

    #@127
    const/4 v2, 0x0

    #@128
    invoke-interface {v1, v2}, Landroid/view/FallbackEventHandler;->setView(Landroid/view/View;)V

    #@12b
    .line 611
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->unscheduleTraversals()V

    #@12e
    .line 612
    const/4 v1, 0x0

    #@12f
    const/4 v2, 0x0

    #@130
    invoke-virtual {p0, v1, v2}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@133
    .line 613
    packed-switch v11, :pswitch_data_2d6

    #@136
    .line 648
    new-instance v1, Ljava/lang/RuntimeException;

    #@138
    new-instance v2, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    const-string v3, "Unable to add window -- unknown error code "

    #@13f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v2

    #@143
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@146
    move-result-object v2

    #@147
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v2

    #@14b
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@14e
    throw v1

    #@14f
    .line 682
    .end local v9           #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .end local v11           #res:I
    .end local v12           #restore:Z
    :catchall_14f
    move-exception v1

    #@150
    monitor-exit p0
    :try_end_151
    .catchall {:try_start_f9 .. :try_end_151} :catchall_14f

    #@151
    throw v1

    #@152
    .line 561
    .restart local v9       #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .restart local v12       #restore:Z
    :cond_152
    const/4 v1, 0x0

    #@153
    goto/16 :goto_a2

    #@155
    .line 562
    :cond_155
    :try_start_155
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@157
    iget v1, v1, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F
    :try_end_159
    .catchall {:try_start_155 .. :try_end_159} :catchall_14f

    #@159
    goto/16 :goto_ac

    #@15b
    .line 586
    :catch_15b
    move-exception v10

    #@15c
    .line 587
    .local v10, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@15d
    :try_start_15d
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mAdded:Z

    #@15f
    .line 588
    const/4 v1, 0x0

    #@160
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@162
    .line 589
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@164
    const/4 v2, 0x0

    #@165
    iput-object v2, v1, Landroid/view/View$AttachInfo;->mRootView:Landroid/view/View;

    #@167
    .line 590
    const/4 v1, 0x0

    #@168
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@16a
    .line 591
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mFallbackEventHandler:Landroid/view/FallbackEventHandler;

    #@16c
    const/4 v2, 0x0

    #@16d
    invoke-interface {v1, v2}, Landroid/view/FallbackEventHandler;->setView(Landroid/view/View;)V

    #@170
    .line 592
    invoke-virtual {p0}, Landroid/view/ViewRootImpl;->unscheduleTraversals()V

    #@173
    .line 593
    const/4 v1, 0x0

    #@174
    const/4 v2, 0x0

    #@175
    invoke-virtual {p0, v1, v2}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@178
    .line 594
    new-instance v1, Ljava/lang/RuntimeException;

    #@17a
    const-string v2, "Adding window failed"

    #@17c
    invoke-direct {v1, v2, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@17f
    throw v1
    :try_end_180
    .catchall {:try_start_15d .. :try_end_180} :catchall_180

    #@180
    .line 596
    .end local v10           #e:Landroid/os/RemoteException;
    :catchall_180
    move-exception v1

    #@181
    if-eqz v12, :cond_186

    #@183
    .line 597
    :try_start_183
    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->restore()V

    #@186
    :cond_186
    throw v1

    #@187
    .line 616
    .restart local v11       #res:I
    :pswitch_187
    new-instance v1, Landroid/view/WindowManager$BadTokenException;

    #@189
    new-instance v2, Ljava/lang/StringBuilder;

    #@18b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18e
    const-string v3, "Unable to add window -- token "

    #@190
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@193
    move-result-object v2

    #@194
    iget-object v3, p2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@196
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@199
    move-result-object v2

    #@19a
    const-string v3, " is not valid; is your activity running?"

    #@19c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v2

    #@1a0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a3
    move-result-object v2

    #@1a4
    invoke-direct {v1, v2}, Landroid/view/WindowManager$BadTokenException;-><init>(Ljava/lang/String;)V

    #@1a7
    throw v1

    #@1a8
    .line 620
    :pswitch_1a8
    new-instance v1, Landroid/view/WindowManager$BadTokenException;

    #@1aa
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ac
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1af
    const-string v3, "Unable to add window -- token "

    #@1b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v2

    #@1b5
    iget-object v3, p2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@1b7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v2

    #@1bb
    const-string v3, " is not for an application"

    #@1bd
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v2

    #@1c1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c4
    move-result-object v2

    #@1c5
    invoke-direct {v1, v2}, Landroid/view/WindowManager$BadTokenException;-><init>(Ljava/lang/String;)V

    #@1c8
    throw v1

    #@1c9
    .line 624
    :pswitch_1c9
    new-instance v1, Landroid/view/WindowManager$BadTokenException;

    #@1cb
    new-instance v2, Ljava/lang/StringBuilder;

    #@1cd
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d0
    const-string v3, "Unable to add window -- app for token "

    #@1d2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v2

    #@1d6
    iget-object v3, p2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@1d8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v2

    #@1dc
    const-string v3, " is exiting"

    #@1de
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v2

    #@1e2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e5
    move-result-object v2

    #@1e6
    invoke-direct {v1, v2}, Landroid/view/WindowManager$BadTokenException;-><init>(Ljava/lang/String;)V

    #@1e9
    throw v1

    #@1ea
    .line 628
    :pswitch_1ea
    new-instance v1, Landroid/view/WindowManager$BadTokenException;

    #@1ec
    new-instance v2, Ljava/lang/StringBuilder;

    #@1ee
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f1
    const-string v3, "Unable to add window -- window "

    #@1f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v2

    #@1f7
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@1f9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v2

    #@1fd
    const-string v3, " has already been added"

    #@1ff
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v2

    #@203
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@206
    move-result-object v2

    #@207
    invoke-direct {v1, v2}, Landroid/view/WindowManager$BadTokenException;-><init>(Ljava/lang/String;)V

    #@20a
    throw v1

    #@20b
    .line 634
    :pswitch_20b
    monitor-exit p0

    #@20c
    .line 683
    .end local v9           #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .end local v11           #res:I
    .end local v12           #restore:Z
    :goto_20c
    return-void

    #@20d
    .line 636
    .restart local v9       #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .restart local v11       #res:I
    .restart local v12       #restore:Z
    :pswitch_20d
    new-instance v1, Landroid/view/WindowManager$BadTokenException;

    #@20f
    new-instance v2, Ljava/lang/StringBuilder;

    #@211
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@214
    const-string v3, "Unable to add window "

    #@216
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v2

    #@21a
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@21c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v2

    #@220
    const-string v3, " -- another window of this type already exists"

    #@222
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v2

    #@226
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@229
    move-result-object v2

    #@22a
    invoke-direct {v1, v2}, Landroid/view/WindowManager$BadTokenException;-><init>(Ljava/lang/String;)V

    #@22d
    throw v1

    #@22e
    .line 640
    :pswitch_22e
    new-instance v1, Landroid/view/WindowManager$BadTokenException;

    #@230
    new-instance v2, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v3, "Unable to add window "

    #@237
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v2

    #@23b
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@23d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v2

    #@241
    const-string v3, " -- permission denied for this window type"

    #@243
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@246
    move-result-object v2

    #@247
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24a
    move-result-object v2

    #@24b
    invoke-direct {v1, v2}, Landroid/view/WindowManager$BadTokenException;-><init>(Ljava/lang/String;)V

    #@24e
    throw v1

    #@24f
    .line 644
    :pswitch_24f
    new-instance v1, Landroid/view/WindowManager$InvalidDisplayException;

    #@251
    new-instance v2, Ljava/lang/StringBuilder;

    #@253
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@256
    const-string v3, "Unable to add window "

    #@258
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25b
    move-result-object v2

    #@25c
    iget-object v3, p0, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@25e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v2

    #@262
    const-string v3, " -- the specified display can not be found"

    #@264
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@267
    move-result-object v2

    #@268
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26b
    move-result-object v2

    #@26c
    invoke-direct {v1, v2}, Landroid/view/WindowManager$InvalidDisplayException;-><init>(Ljava/lang/String;)V

    #@26f
    throw v1

    #@270
    .line 652
    :cond_270
    instance-of v1, p1, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@272
    if-eqz v1, :cond_27e

    #@274
    .line 653
    move-object v0, p1

    #@275
    check-cast v0, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@277
    move-object v1, v0

    #@278
    invoke-interface {v1}, Lcom/android/internal/view/RootViewSurfaceTaker;->willYouTakeTheInputQueue()Landroid/view/InputQueue$Callback;

    #@27b
    move-result-object v1

    #@27c
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@27e
    .line 656
    :cond_27e
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@280
    if-eqz v1, :cond_296

    #@282
    .line 657
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@284
    if-eqz v1, :cond_2c4

    #@286
    .line 658
    new-instance v1, Landroid/view/InputQueue;

    #@288
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@28a
    invoke-direct {v1, v2}, Landroid/view/InputQueue;-><init>(Landroid/view/InputChannel;)V

    #@28d
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mInputQueue:Landroid/view/InputQueue;

    #@28f
    .line 659
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mInputQueueCallback:Landroid/view/InputQueue$Callback;

    #@291
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mInputQueue:Landroid/view/InputQueue;

    #@293
    invoke-interface {v1, v2}, Landroid/view/InputQueue$Callback;->onInputQueueCreated(Landroid/view/InputQueue;)V

    #@296
    .line 666
    :cond_296
    :goto_296
    invoke-virtual {p1, p0}, Landroid/view/View;->assignParent(Landroid/view/ViewParent;)V

    #@299
    .line 667
    and-int/lit8 v1, v11, 0x1

    #@29b
    if-eqz v1, :cond_2d2

    #@29d
    const/4 v1, 0x1

    #@29e
    :goto_29e
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mAddedTouchMode:Z

    #@2a0
    .line 668
    and-int/lit8 v1, v11, 0x2

    #@2a2
    if-eqz v1, :cond_2d4

    #@2a4
    const/4 v1, 0x1

    #@2a5
    :goto_2a5
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mAppVisible:Z

    #@2a7
    .line 670
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@2a9
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@2ac
    move-result v1

    #@2ad
    if-eqz v1, :cond_2b4

    #@2af
    .line 671
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mAccessibilityInteractionConnectionManager:Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;

    #@2b1
    invoke-virtual {v1}, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->ensureConnection()V

    #@2b4
    .line 674
    :cond_2b4
    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    #@2b7
    move-result v1

    #@2b8
    if-nez v1, :cond_2be

    #@2ba
    .line 675
    const/4 v1, 0x1

    #@2bb
    invoke-virtual {p1, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@2be
    .line 679
    :cond_2be
    invoke-direct {p0}, Landroid/view/ViewRootImpl;->setTouchEventFilterDefault()V

    #@2c1
    .line 682
    .end local v9           #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .end local v11           #res:I
    .end local v12           #restore:Z
    :cond_2c1
    monitor-exit p0

    #@2c2
    goto/16 :goto_20c

    #@2c4
    .line 661
    .restart local v9       #compatibilityInfo:Landroid/content/res/CompatibilityInfo;
    .restart local v11       #res:I
    .restart local v12       #restore:Z
    :cond_2c4
    new-instance v1, Landroid/view/ViewRootImpl$WindowInputEventReceiver;

    #@2c6
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mInputChannel:Landroid/view/InputChannel;

    #@2c8
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@2cb
    move-result-object v3

    #@2cc
    invoke-direct {v1, p0, v2, v3}, Landroid/view/ViewRootImpl$WindowInputEventReceiver;-><init>(Landroid/view/ViewRootImpl;Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@2cf
    iput-object v1, p0, Landroid/view/ViewRootImpl;->mInputEventReceiver:Landroid/view/ViewRootImpl$WindowInputEventReceiver;
    :try_end_2d1
    .catchall {:try_start_183 .. :try_end_2d1} :catchall_14f

    #@2d1
    goto :goto_296

    #@2d2
    .line 667
    :cond_2d2
    const/4 v1, 0x0

    #@2d3
    goto :goto_29e

    #@2d4
    .line 668
    :cond_2d4
    const/4 v1, 0x0

    #@2d5
    goto :goto_2a5

    #@2d6
    .line 613
    :pswitch_data_2d6
    .packed-switch -0x9
        :pswitch_24f
        :pswitch_22e
        :pswitch_20d
        :pswitch_20b
        :pswitch_1ea
        :pswitch_1c9
        :pswitch_1a8
        :pswitch_187
        :pswitch_187
    .end packed-switch
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .registers 3
    .parameter "originalView"

    #@0
    .prologue
    .line 4863
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 4
    .parameter "originalView"
    .parameter "callback"

    #@0
    .prologue
    .line 4867
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method terminateHardwareResources()V
    .registers 3

    #@0
    .prologue
    .line 695
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    if-eqz v0, :cond_17

    #@6
    .line 696
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@a
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->destroyHardwareResources(Landroid/view/View;)V

    #@f
    .line 697
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->destroy(Z)V

    #@17
    .line 699
    :cond_17
    return-void
.end method

.method public timerMethod()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1166
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mIsActive:Z

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 1167
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mPerfObject:Lorg/codeaurora/Performance;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1168
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mPerfObject:Lorg/codeaurora/Performance;

    #@b
    invoke-virtual {v0}, Lorg/codeaurora/Performance;->setCpuBoost()V

    #@e
    .line 1170
    :cond_e
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mIsActive:Z

    #@10
    .line 1176
    :goto_10
    return-void

    #@11
    .line 1173
    :cond_11
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mActiveTimer:Ljava/util/Timer;

    #@13
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@16
    .line 1174
    iput-boolean v1, p0, Landroid/view/ViewRootImpl;->mIsScheduled:Z

    #@18
    goto :goto_10
.end method

.method unscheduleConsumeBatchedInput()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4533
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mConsumeBatchedInputScheduled:Z

    #@3
    if-eqz v0, :cond_f

    #@5
    .line 4534
    iput-boolean v3, p0, Landroid/view/ViewRootImpl;->mConsumeBatchedInputScheduled:Z

    #@7
    .line 4535
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@9
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mConsumedBatchedInputRunnable:Landroid/view/ViewRootImpl$ConsumeBatchedInputRunnable;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v0, v3, v1, v2}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@f
    .line 4538
    :cond_f
    return-void
.end method

.method unscheduleTraversals()V
    .registers 5

    #@0
    .prologue
    .line 1031
    iget-boolean v0, p0, Landroid/view/ViewRootImpl;->mTraversalScheduled:Z

    #@2
    if-eqz v0, :cond_1b

    #@4
    .line 1032
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/view/ViewRootImpl;->mTraversalScheduled:Z

    #@7
    .line 1033
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@9
    invoke-virtual {v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->getLooper()Landroid/os/Looper;

    #@c
    move-result-object v0

    #@d
    iget v1, p0, Landroid/view/ViewRootImpl;->mTraversalBarrier:I

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Looper;->removeSyncBarrier(I)V

    #@12
    .line 1034
    iget-object v0, p0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@14
    const/4 v1, 0x2

    #@15
    iget-object v2, p0, Landroid/view/ViewRootImpl;->mTraversalRunnable:Landroid/view/ViewRootImpl$TraversalRunnable;

    #@17
    const/4 v3, 0x0

    #@18
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@1b
    .line 1037
    :cond_1b
    return-void
.end method

.method updateConfiguration(Landroid/content/res/Configuration;Z)V
    .registers 10
    .parameter "config"
    .parameter "force"

    #@0
    .prologue
    .line 2778
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@2
    invoke-virtual {v5}, Landroid/view/CompatibilityInfoHolder;->getIfNeeded()Landroid/content/res/CompatibilityInfo;

    #@5
    move-result-object v0

    #@6
    .line 2779
    .local v0, ci:Landroid/content/res/CompatibilityInfo;
    if-eqz v0, :cond_13

    #@8
    .line 2780
    new-instance v1, Landroid/content/res/Configuration;

    #@a
    invoke-direct {v1, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@d
    .line 2781
    .end local p1
    .local v1, config:Landroid/content/res/Configuration;
    iget v5, p0, Landroid/view/ViewRootImpl;->mNoncompatDensity:I

    #@f
    invoke-virtual {v0, v5, v1}, Landroid/content/res/CompatibilityInfo;->applyToConfiguration(ILandroid/content/res/Configuration;)V

    #@12
    move-object p1, v1

    #@13
    .line 2784
    .end local v1           #config:Landroid/content/res/Configuration;
    .restart local p1
    :cond_13
    sget-object v6, Landroid/view/ViewRootImpl;->sConfigCallbacks:Ljava/util/ArrayList;

    #@15
    monitor-enter v6

    #@16
    .line 2785
    :try_start_16
    sget-object v5, Landroid/view/ViewRootImpl;->sConfigCallbacks:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v5

    #@1c
    add-int/lit8 v3, v5, -0x1

    #@1e
    .local v3, i:I
    :goto_1e
    if-ltz v3, :cond_2e

    #@20
    .line 2786
    sget-object v5, Landroid/view/ViewRootImpl;->sConfigCallbacks:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v5

    #@26
    check-cast v5, Landroid/content/ComponentCallbacks;

    #@28
    invoke-interface {v5, p1}, Landroid/content/ComponentCallbacks;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@2b
    .line 2785
    add-int/lit8 v3, v3, -0x1

    #@2d
    goto :goto_1e

    #@2e
    .line 2788
    :cond_2e
    monitor-exit v6
    :try_end_2f
    .catchall {:try_start_16 .. :try_end_2f} :catchall_68

    #@2f
    .line 2789
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@31
    if-eqz v5, :cond_67

    #@33
    .line 2793
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@35
    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@3c
    move-result-object p1

    #@3d
    .line 2794
    if-nez p2, :cond_47

    #@3f
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@41
    invoke-virtual {v5, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@44
    move-result v5

    #@45
    if-eqz v5, :cond_67

    #@47
    .line 2795
    :cond_47
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@49
    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    #@4c
    move-result v4

    #@4d
    .line 2796
    .local v4, lastLayoutDirection:I
    invoke-virtual {p1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    #@50
    move-result v2

    #@51
    .line 2797
    .local v2, currentLayoutDirection:I
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@53
    invoke-virtual {v5, p1}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@56
    .line 2798
    if-eq v4, v2, :cond_62

    #@58
    iget v5, p0, Landroid/view/ViewRootImpl;->mViewLayoutDirectionInitial:I

    #@5a
    const/4 v6, 0x2

    #@5b
    if-ne v5, v6, :cond_62

    #@5d
    .line 2800
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5f
    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutDirection(I)V

    #@62
    .line 2802
    :cond_62
    iget-object v5, p0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@64
    invoke-virtual {v5, p1}, Landroid/view/View;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    #@67
    .line 2805
    .end local v2           #currentLayoutDirection:I
    .end local v4           #lastLayoutDirection:I
    :cond_67
    return-void

    #@68
    .line 2788
    .end local v3           #i:I
    :catchall_68
    move-exception v5

    #@69
    :try_start_69
    monitor-exit v6
    :try_end_6a
    .catchall {:try_start_69 .. :try_end_6a} :catchall_68

    #@6a
    throw v5
.end method

.method public windowFocusChanged(ZZ)V
    .registers 7
    .parameter "hasFocus"
    .parameter "inTouchMode"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 4783
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 4784
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x6

    #@7
    iput v1, v0, Landroid/os/Message;->what:I

    #@9
    .line 4785
    if-eqz p1, :cond_18

    #@b
    move v1, v2

    #@c
    :goto_c
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@e
    .line 4786
    if-eqz p2, :cond_1a

    #@10
    :goto_10
    iput v2, v0, Landroid/os/Message;->arg2:I

    #@12
    .line 4787
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@14
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 4788
    return-void

    #@18
    :cond_18
    move v1, v3

    #@19
    .line 4785
    goto :goto_c

    #@1a
    :cond_1a
    move v2, v3

    #@1b
    .line 4786
    goto :goto_10
.end method
