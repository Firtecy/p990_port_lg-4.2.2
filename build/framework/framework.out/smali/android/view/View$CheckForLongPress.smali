.class Landroid/view/View$CheckForLongPress;
.super Ljava/lang/Object;
.source "View.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckForLongPress"
.end annotation


# instance fields
.field private mOriginalWindowAttachCount:I

.field final synthetic this$0:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 17585
    iput-object p1, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public rememberWindowAttachCount()V
    .registers 2

    #@0
    .prologue
    .line 17599
    iget-object v0, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@2
    iget v0, v0, Landroid/view/View;->mWindowAttachCount:I

    #@4
    iput v0, p0, Landroid/view/View$CheckForLongPress;->mOriginalWindowAttachCount:I

    #@6
    .line 17600
    return-void
.end method

.method public run()V
    .registers 3

    #@0
    .prologue
    .line 17590
    iget-object v0, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@2
    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_24

    #@8
    iget-object v0, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@a
    iget-object v0, v0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    if-eqz v0, :cond_24

    #@e
    iget v0, p0, Landroid/view/View$CheckForLongPress;->mOriginalWindowAttachCount:I

    #@10
    iget-object v1, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@12
    iget v1, v1, Landroid/view/View;->mWindowAttachCount:I

    #@14
    if-ne v0, v1, :cond_24

    #@16
    .line 17592
    iget-object v0, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@18
    invoke-virtual {v0}, Landroid/view/View;->performLongClick()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_24

    #@1e
    .line 17593
    iget-object v0, p0, Landroid/view/View$CheckForLongPress;->this$0:Landroid/view/View;

    #@20
    const/4 v1, 0x1

    #@21
    invoke-static {v0, v1}, Landroid/view/View;->access$2302(Landroid/view/View;Z)Z

    #@24
    .line 17596
    :cond_24
    return-void
.end method
