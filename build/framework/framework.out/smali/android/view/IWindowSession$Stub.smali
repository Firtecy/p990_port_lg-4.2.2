.class public abstract Landroid/view/IWindowSession$Stub;
.super Landroid/os/Binder;
.source "IWindowSession.java"

# interfaces
.implements Landroid/view/IWindowSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IWindowSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/IWindowSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.IWindowSession"

.field static final TRANSACTION_add:I = 0x1

.field static final TRANSACTION_addToDisplay:I = 0x2

.field static final TRANSACTION_addToDisplayWithoutInputChannel:I = 0x4

.field static final TRANSACTION_addWithoutInputChannel:I = 0x3

.field static final TRANSACTION_dragRecipientEntered:I = 0x13

.field static final TRANSACTION_dragRecipientExited:I = 0x14

.field static final TRANSACTION_finishDrawing:I = 0xc

.field static final TRANSACTION_getDisplayFrame:I = 0xb

.field static final TRANSACTION_getInTouchMode:I = 0xe

.field static final TRANSACTION_isWindowSplit:I = 0x1b

.field static final TRANSACTION_onRectangleOnScreenRequested:I = 0x1a

.field static final TRANSACTION_outOfMemory:I = 0x8

.field static final TRANSACTION_performDeferredDestroy:I = 0x7

.field static final TRANSACTION_performDrag:I = 0x11

.field static final TRANSACTION_performHapticFeedback:I = 0xf

.field static final TRANSACTION_prepareDrag:I = 0x10

.field static final TRANSACTION_relayout:I = 0x6

.field static final TRANSACTION_remove:I = 0x5

.field static final TRANSACTION_reportDropResult:I = 0x12

.field static final TRANSACTION_sendWallpaperCommand:I = 0x17

.field static final TRANSACTION_setInTouchMode:I = 0xd

.field static final TRANSACTION_setInsets:I = 0xa

.field static final TRANSACTION_setTransparentRegion:I = 0x9

.field static final TRANSACTION_setUniverseTransform:I = 0x19

.field static final TRANSACTION_setWallpaperPosition:I = 0x15

.field static final TRANSACTION_wallpaperCommandComplete:I = 0x18

.field static final TRANSACTION_wallpaperOffsetsComplete:I = 0x16


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.view.IWindowSession"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/IWindowSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/IWindowSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.view.IWindowSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/IWindowSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/view/IWindowSession;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/view/IWindowSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/IWindowSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 20
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    sparse-switch p1, :sswitch_data_610

    #@3
    .line 615
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 47
    :sswitch_8
    const-string v1, "android.view.IWindowSession"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 48
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 52
    :sswitch_11
    const-string v1, "android.view.IWindowSession"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 54
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@1f
    move-result-object v2

    #@20
    .line 56
    .local v2, _arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    .line 58
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_6d

    #@2a
    .line 59
    sget-object v1, Landroid/view/WindowManager$LayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c
    move-object/from16 v0, p2

    #@2e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31
    move-result-object v4

    #@32
    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    #@34
    .line 65
    .local v4, _arg2:Landroid/view/WindowManager$LayoutParams;
    :goto_34
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v5

    #@38
    .line 67
    .local v5, _arg3:I
    new-instance v6, Landroid/graphics/Rect;

    #@3a
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@3d
    .line 69
    .local v6, _arg4:Landroid/graphics/Rect;
    new-instance v7, Landroid/view/InputChannel;

    #@3f
    invoke-direct {v7}, Landroid/view/InputChannel;-><init>()V

    #@42
    .local v7, _arg5:Landroid/view/InputChannel;
    move-object v1, p0

    #@43
    .line 70
    invoke-virtual/range {v1 .. v7}, Landroid/view/IWindowSession$Stub;->add(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;Landroid/view/InputChannel;)I

    #@46
    move-result v14

    #@47
    .line 71
    .local v14, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a
    .line 72
    move-object/from16 v0, p3

    #@4c
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    .line 73
    if-eqz v6, :cond_6f

    #@51
    .line 74
    const/4 v1, 0x1

    #@52
    move-object/from16 v0, p3

    #@54
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    .line 75
    const/4 v1, 0x1

    #@58
    move-object/from16 v0, p3

    #@5a
    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@5d
    .line 80
    :goto_5d
    if-eqz v7, :cond_76

    #@5f
    .line 81
    const/4 v1, 0x1

    #@60
    move-object/from16 v0, p3

    #@62
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    .line 82
    const/4 v1, 0x1

    #@66
    move-object/from16 v0, p3

    #@68
    invoke-virtual {v7, v0, v1}, Landroid/view/InputChannel;->writeToParcel(Landroid/os/Parcel;I)V

    #@6b
    .line 87
    :goto_6b
    const/4 v1, 0x1

    #@6c
    goto :goto_7

    #@6d
    .line 62
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/graphics/Rect;
    .end local v7           #_arg5:Landroid/view/InputChannel;
    .end local v14           #_result:I
    :cond_6d
    const/4 v4, 0x0

    #@6e
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    goto :goto_34

    #@6f
    .line 78
    .restart local v5       #_arg3:I
    .restart local v6       #_arg4:Landroid/graphics/Rect;
    .restart local v7       #_arg5:Landroid/view/InputChannel;
    .restart local v14       #_result:I
    :cond_6f
    const/4 v1, 0x0

    #@70
    move-object/from16 v0, p3

    #@72
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@75
    goto :goto_5d

    #@76
    .line 85
    :cond_76
    const/4 v1, 0x0

    #@77
    move-object/from16 v0, p3

    #@79
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@7c
    goto :goto_6b

    #@7d
    .line 91
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/graphics/Rect;
    .end local v7           #_arg5:Landroid/view/InputChannel;
    .end local v14           #_result:I
    :sswitch_7d
    const-string v1, "android.view.IWindowSession"

    #@7f
    move-object/from16 v0, p2

    #@81
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 93
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@87
    move-result-object v1

    #@88
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@8b
    move-result-object v2

    #@8c
    .line 95
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8f
    move-result v3

    #@90
    .line 97
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@93
    move-result v1

    #@94
    if-eqz v1, :cond_de

    #@96
    .line 98
    sget-object v1, Landroid/view/WindowManager$LayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@98
    move-object/from16 v0, p2

    #@9a
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@9d
    move-result-object v4

    #@9e
    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    #@a0
    .line 104
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    :goto_a0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a3
    move-result v5

    #@a4
    .line 106
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a7
    move-result v6

    #@a8
    .line 108
    .local v6, _arg4:I
    new-instance v7, Landroid/graphics/Rect;

    #@aa
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@ad
    .line 110
    .local v7, _arg5:Landroid/graphics/Rect;
    new-instance v8, Landroid/view/InputChannel;

    #@af
    invoke-direct {v8}, Landroid/view/InputChannel;-><init>()V

    #@b2
    .local v8, _arg6:Landroid/view/InputChannel;
    move-object v1, p0

    #@b3
    .line 111
    invoke-virtual/range {v1 .. v8}, Landroid/view/IWindowSession$Stub;->addToDisplay(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I

    #@b6
    move-result v14

    #@b7
    .line 112
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ba
    .line 113
    move-object/from16 v0, p3

    #@bc
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@bf
    .line 114
    if-eqz v7, :cond_e0

    #@c1
    .line 115
    const/4 v1, 0x1

    #@c2
    move-object/from16 v0, p3

    #@c4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@c7
    .line 116
    const/4 v1, 0x1

    #@c8
    move-object/from16 v0, p3

    #@ca
    invoke-virtual {v7, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@cd
    .line 121
    :goto_cd
    if-eqz v8, :cond_e7

    #@cf
    .line 122
    const/4 v1, 0x1

    #@d0
    move-object/from16 v0, p3

    #@d2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@d5
    .line 123
    const/4 v1, 0x1

    #@d6
    move-object/from16 v0, p3

    #@d8
    invoke-virtual {v8, v0, v1}, Landroid/view/InputChannel;->writeToParcel(Landroid/os/Parcel;I)V

    #@db
    .line 128
    :goto_db
    const/4 v1, 0x1

    #@dc
    goto/16 :goto_7

    #@de
    .line 101
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Landroid/graphics/Rect;
    .end local v8           #_arg6:Landroid/view/InputChannel;
    .end local v14           #_result:I
    :cond_de
    const/4 v4, 0x0

    #@df
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    goto :goto_a0

    #@e0
    .line 119
    .restart local v5       #_arg3:I
    .restart local v6       #_arg4:I
    .restart local v7       #_arg5:Landroid/graphics/Rect;
    .restart local v8       #_arg6:Landroid/view/InputChannel;
    .restart local v14       #_result:I
    :cond_e0
    const/4 v1, 0x0

    #@e1
    move-object/from16 v0, p3

    #@e3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e6
    goto :goto_cd

    #@e7
    .line 126
    :cond_e7
    const/4 v1, 0x0

    #@e8
    move-object/from16 v0, p3

    #@ea
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@ed
    goto :goto_db

    #@ee
    .line 132
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Landroid/graphics/Rect;
    .end local v8           #_arg6:Landroid/view/InputChannel;
    .end local v14           #_result:I
    :sswitch_ee
    const-string v1, "android.view.IWindowSession"

    #@f0
    move-object/from16 v0, p2

    #@f2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f5
    .line 134
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@f8
    move-result-object v1

    #@f9
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@fc
    move-result-object v2

    #@fd
    .line 136
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@100
    move-result v3

    #@101
    .line 138
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@104
    move-result v1

    #@105
    if-eqz v1, :cond_138

    #@107
    .line 139
    sget-object v1, Landroid/view/WindowManager$LayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@109
    move-object/from16 v0, p2

    #@10b
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10e
    move-result-object v4

    #@10f
    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    #@111
    .line 145
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    :goto_111
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@114
    move-result v5

    #@115
    .line 147
    .restart local v5       #_arg3:I
    new-instance v6, Landroid/graphics/Rect;

    #@117
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@11a
    .local v6, _arg4:Landroid/graphics/Rect;
    move-object v1, p0

    #@11b
    .line 148
    invoke-virtual/range {v1 .. v6}, Landroid/view/IWindowSession$Stub;->addWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;ILandroid/graphics/Rect;)I

    #@11e
    move-result v14

    #@11f
    .line 149
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@122
    .line 150
    move-object/from16 v0, p3

    #@124
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@127
    .line 151
    if-eqz v6, :cond_13a

    #@129
    .line 152
    const/4 v1, 0x1

    #@12a
    move-object/from16 v0, p3

    #@12c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12f
    .line 153
    const/4 v1, 0x1

    #@130
    move-object/from16 v0, p3

    #@132
    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@135
    .line 158
    :goto_135
    const/4 v1, 0x1

    #@136
    goto/16 :goto_7

    #@138
    .line 142
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/graphics/Rect;
    .end local v14           #_result:I
    :cond_138
    const/4 v4, 0x0

    #@139
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    goto :goto_111

    #@13a
    .line 156
    .restart local v5       #_arg3:I
    .restart local v6       #_arg4:Landroid/graphics/Rect;
    .restart local v14       #_result:I
    :cond_13a
    const/4 v1, 0x0

    #@13b
    move-object/from16 v0, p3

    #@13d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@140
    goto :goto_135

    #@141
    .line 162
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/graphics/Rect;
    .end local v14           #_result:I
    :sswitch_141
    const-string v1, "android.view.IWindowSession"

    #@143
    move-object/from16 v0, p2

    #@145
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@148
    .line 164
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@14b
    move-result-object v1

    #@14c
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@14f
    move-result-object v2

    #@150
    .line 166
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@153
    move-result v3

    #@154
    .line 168
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@157
    move-result v1

    #@158
    if-eqz v1, :cond_18f

    #@15a
    .line 169
    sget-object v1, Landroid/view/WindowManager$LayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15c
    move-object/from16 v0, p2

    #@15e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@161
    move-result-object v4

    #@162
    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    #@164
    .line 175
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    :goto_164
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@167
    move-result v5

    #@168
    .line 177
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16b
    move-result v6

    #@16c
    .line 179
    .local v6, _arg4:I
    new-instance v7, Landroid/graphics/Rect;

    #@16e
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@171
    .restart local v7       #_arg5:Landroid/graphics/Rect;
    move-object v1, p0

    #@172
    .line 180
    invoke-virtual/range {v1 .. v7}, Landroid/view/IWindowSession$Stub;->addToDisplayWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;)I

    #@175
    move-result v14

    #@176
    .line 181
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@179
    .line 182
    move-object/from16 v0, p3

    #@17b
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@17e
    .line 183
    if-eqz v7, :cond_191

    #@180
    .line 184
    const/4 v1, 0x1

    #@181
    move-object/from16 v0, p3

    #@183
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@186
    .line 185
    const/4 v1, 0x1

    #@187
    move-object/from16 v0, p3

    #@189
    invoke-virtual {v7, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@18c
    .line 190
    :goto_18c
    const/4 v1, 0x1

    #@18d
    goto/16 :goto_7

    #@18f
    .line 172
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Landroid/graphics/Rect;
    .end local v14           #_result:I
    :cond_18f
    const/4 v4, 0x0

    #@190
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    goto :goto_164

    #@191
    .line 188
    .restart local v5       #_arg3:I
    .restart local v6       #_arg4:I
    .restart local v7       #_arg5:Landroid/graphics/Rect;
    .restart local v14       #_result:I
    :cond_191
    const/4 v1, 0x0

    #@192
    move-object/from16 v0, p3

    #@194
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@197
    goto :goto_18c

    #@198
    .line 194
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Landroid/graphics/Rect;
    .end local v14           #_result:I
    :sswitch_198
    const-string v1, "android.view.IWindowSession"

    #@19a
    move-object/from16 v0, p2

    #@19c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19f
    .line 196
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1a2
    move-result-object v1

    #@1a3
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@1a6
    move-result-object v2

    #@1a7
    .line 197
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->remove(Landroid/view/IWindow;)V

    #@1aa
    .line 198
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ad
    .line 199
    const/4 v1, 0x1

    #@1ae
    goto/16 :goto_7

    #@1b0
    .line 203
    .end local v2           #_arg0:Landroid/view/IWindow;
    :sswitch_1b0
    const-string v1, "android.view.IWindowSession"

    #@1b2
    move-object/from16 v0, p2

    #@1b4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b7
    .line 205
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1ba
    move-result-object v1

    #@1bb
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@1be
    move-result-object v2

    #@1bf
    .line 207
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c2
    move-result v3

    #@1c3
    .line 209
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c6
    move-result v1

    #@1c7
    if-eqz v1, :cond_252

    #@1c9
    .line 210
    sget-object v1, Landroid/view/WindowManager$LayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1cb
    move-object/from16 v0, p2

    #@1cd
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d0
    move-result-object v4

    #@1d1
    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    #@1d3
    .line 216
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    :goto_1d3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1d6
    move-result v5

    #@1d7
    .line 218
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1da
    move-result v6

    #@1db
    .line 220
    .restart local v6       #_arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1de
    move-result v7

    #@1df
    .line 222
    .local v7, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e2
    move-result v8

    #@1e3
    .line 224
    .local v8, _arg6:I
    new-instance v9, Landroid/graphics/Rect;

    #@1e5
    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    #@1e8
    .line 226
    .local v9, _arg7:Landroid/graphics/Rect;
    new-instance v10, Landroid/graphics/Rect;

    #@1ea
    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    #@1ed
    .line 228
    .local v10, _arg8:Landroid/graphics/Rect;
    new-instance v11, Landroid/graphics/Rect;

    #@1ef
    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    #@1f2
    .line 230
    .local v11, _arg9:Landroid/graphics/Rect;
    new-instance v12, Landroid/content/res/Configuration;

    #@1f4
    invoke-direct {v12}, Landroid/content/res/Configuration;-><init>()V

    #@1f7
    .line 232
    .local v12, _arg10:Landroid/content/res/Configuration;
    new-instance v13, Landroid/view/Surface;

    #@1f9
    invoke-direct {v13}, Landroid/view/Surface;-><init>()V

    #@1fc
    .local v13, _arg11:Landroid/view/Surface;
    move-object v1, p0

    #@1fd
    .line 233
    invoke-virtual/range {v1 .. v13}, Landroid/view/IWindowSession$Stub;->relayout(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I

    #@200
    move-result v14

    #@201
    .line 234
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@204
    .line 235
    move-object/from16 v0, p3

    #@206
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@209
    .line 236
    if-eqz v9, :cond_254

    #@20b
    .line 237
    const/4 v1, 0x1

    #@20c
    move-object/from16 v0, p3

    #@20e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@211
    .line 238
    const/4 v1, 0x1

    #@212
    move-object/from16 v0, p3

    #@214
    invoke-virtual {v9, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@217
    .line 243
    :goto_217
    if-eqz v10, :cond_25b

    #@219
    .line 244
    const/4 v1, 0x1

    #@21a
    move-object/from16 v0, p3

    #@21c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@21f
    .line 245
    const/4 v1, 0x1

    #@220
    move-object/from16 v0, p3

    #@222
    invoke-virtual {v10, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@225
    .line 250
    :goto_225
    if-eqz v11, :cond_262

    #@227
    .line 251
    const/4 v1, 0x1

    #@228
    move-object/from16 v0, p3

    #@22a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@22d
    .line 252
    const/4 v1, 0x1

    #@22e
    move-object/from16 v0, p3

    #@230
    invoke-virtual {v11, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@233
    .line 257
    :goto_233
    if-eqz v12, :cond_269

    #@235
    .line 258
    const/4 v1, 0x1

    #@236
    move-object/from16 v0, p3

    #@238
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@23b
    .line 259
    const/4 v1, 0x1

    #@23c
    move-object/from16 v0, p3

    #@23e
    invoke-virtual {v12, v0, v1}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@241
    .line 264
    :goto_241
    if-eqz v13, :cond_270

    #@243
    .line 265
    const/4 v1, 0x1

    #@244
    move-object/from16 v0, p3

    #@246
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@249
    .line 266
    const/4 v1, 0x1

    #@24a
    move-object/from16 v0, p3

    #@24c
    invoke-virtual {v13, v0, v1}, Landroid/view/Surface;->writeToParcel(Landroid/os/Parcel;I)V

    #@24f
    .line 271
    :goto_24f
    const/4 v1, 0x1

    #@250
    goto/16 :goto_7

    #@252
    .line 213
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:I
    .end local v8           #_arg6:I
    .end local v9           #_arg7:Landroid/graphics/Rect;
    .end local v10           #_arg8:Landroid/graphics/Rect;
    .end local v11           #_arg9:Landroid/graphics/Rect;
    .end local v12           #_arg10:Landroid/content/res/Configuration;
    .end local v13           #_arg11:Landroid/view/Surface;
    .end local v14           #_result:I
    :cond_252
    const/4 v4, 0x0

    #@253
    .restart local v4       #_arg2:Landroid/view/WindowManager$LayoutParams;
    goto :goto_1d3

    #@254
    .line 241
    .restart local v5       #_arg3:I
    .restart local v6       #_arg4:I
    .restart local v7       #_arg5:I
    .restart local v8       #_arg6:I
    .restart local v9       #_arg7:Landroid/graphics/Rect;
    .restart local v10       #_arg8:Landroid/graphics/Rect;
    .restart local v11       #_arg9:Landroid/graphics/Rect;
    .restart local v12       #_arg10:Landroid/content/res/Configuration;
    .restart local v13       #_arg11:Landroid/view/Surface;
    .restart local v14       #_result:I
    :cond_254
    const/4 v1, 0x0

    #@255
    move-object/from16 v0, p3

    #@257
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@25a
    goto :goto_217

    #@25b
    .line 248
    :cond_25b
    const/4 v1, 0x0

    #@25c
    move-object/from16 v0, p3

    #@25e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@261
    goto :goto_225

    #@262
    .line 255
    :cond_262
    const/4 v1, 0x0

    #@263
    move-object/from16 v0, p3

    #@265
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@268
    goto :goto_233

    #@269
    .line 262
    :cond_269
    const/4 v1, 0x0

    #@26a
    move-object/from16 v0, p3

    #@26c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@26f
    goto :goto_241

    #@270
    .line 269
    :cond_270
    const/4 v1, 0x0

    #@271
    move-object/from16 v0, p3

    #@273
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@276
    goto :goto_24f

    #@277
    .line 275
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/view/WindowManager$LayoutParams;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:I
    .end local v8           #_arg6:I
    .end local v9           #_arg7:Landroid/graphics/Rect;
    .end local v10           #_arg8:Landroid/graphics/Rect;
    .end local v11           #_arg9:Landroid/graphics/Rect;
    .end local v12           #_arg10:Landroid/content/res/Configuration;
    .end local v13           #_arg11:Landroid/view/Surface;
    .end local v14           #_result:I
    :sswitch_277
    const-string v1, "android.view.IWindowSession"

    #@279
    move-object/from16 v0, p2

    #@27b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27e
    .line 277
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@281
    move-result-object v1

    #@282
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@285
    move-result-object v2

    #@286
    .line 278
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->performDeferredDestroy(Landroid/view/IWindow;)V

    #@289
    .line 279
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@28c
    .line 280
    const/4 v1, 0x1

    #@28d
    goto/16 :goto_7

    #@28f
    .line 284
    .end local v2           #_arg0:Landroid/view/IWindow;
    :sswitch_28f
    const-string v1, "android.view.IWindowSession"

    #@291
    move-object/from16 v0, p2

    #@293
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@296
    .line 286
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@299
    move-result-object v1

    #@29a
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@29d
    move-result-object v2

    #@29e
    .line 287
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->outOfMemory(Landroid/view/IWindow;)Z

    #@2a1
    move-result v14

    #@2a2
    .line 288
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a5
    .line 289
    if-eqz v14, :cond_2b0

    #@2a7
    const/4 v1, 0x1

    #@2a8
    :goto_2a8
    move-object/from16 v0, p3

    #@2aa
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2ad
    .line 290
    const/4 v1, 0x1

    #@2ae
    goto/16 :goto_7

    #@2b0
    .line 289
    :cond_2b0
    const/4 v1, 0x0

    #@2b1
    goto :goto_2a8

    #@2b2
    .line 294
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v14           #_result:Z
    :sswitch_2b2
    const-string v1, "android.view.IWindowSession"

    #@2b4
    move-object/from16 v0, p2

    #@2b6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b9
    .line 296
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2bc
    move-result-object v1

    #@2bd
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@2c0
    move-result-object v2

    #@2c1
    .line 298
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2c4
    move-result v1

    #@2c5
    if-eqz v1, :cond_2da

    #@2c7
    .line 299
    sget-object v1, Landroid/graphics/Region;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c9
    move-object/from16 v0, p2

    #@2cb
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2ce
    move-result-object v3

    #@2cf
    check-cast v3, Landroid/graphics/Region;

    #@2d1
    .line 304
    .local v3, _arg1:Landroid/graphics/Region;
    :goto_2d1
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowSession$Stub;->setTransparentRegion(Landroid/view/IWindow;Landroid/graphics/Region;)V

    #@2d4
    .line 305
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d7
    .line 306
    const/4 v1, 0x1

    #@2d8
    goto/16 :goto_7

    #@2da
    .line 302
    .end local v3           #_arg1:Landroid/graphics/Region;
    :cond_2da
    const/4 v3, 0x0

    #@2db
    .restart local v3       #_arg1:Landroid/graphics/Region;
    goto :goto_2d1

    #@2dc
    .line 310
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:Landroid/graphics/Region;
    :sswitch_2dc
    const-string v1, "android.view.IWindowSession"

    #@2de
    move-object/from16 v0, p2

    #@2e0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e3
    .line 312
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2e6
    move-result-object v1

    #@2e7
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@2ea
    move-result-object v2

    #@2eb
    .line 314
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2ee
    move-result v3

    #@2ef
    .line 316
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2f2
    move-result v1

    #@2f3
    if-eqz v1, :cond_329

    #@2f5
    .line 317
    sget-object v1, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2f7
    move-object/from16 v0, p2

    #@2f9
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2fc
    move-result-object v4

    #@2fd
    check-cast v4, Landroid/graphics/Rect;

    #@2ff
    .line 323
    .local v4, _arg2:Landroid/graphics/Rect;
    :goto_2ff
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@302
    move-result v1

    #@303
    if-eqz v1, :cond_32b

    #@305
    .line 324
    sget-object v1, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@307
    move-object/from16 v0, p2

    #@309
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@30c
    move-result-object v5

    #@30d
    check-cast v5, Landroid/graphics/Rect;

    #@30f
    .line 330
    .local v5, _arg3:Landroid/graphics/Rect;
    :goto_30f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@312
    move-result v1

    #@313
    if-eqz v1, :cond_32d

    #@315
    .line 331
    sget-object v1, Landroid/graphics/Region;->CREATOR:Landroid/os/Parcelable$Creator;

    #@317
    move-object/from16 v0, p2

    #@319
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31c
    move-result-object v6

    #@31d
    check-cast v6, Landroid/graphics/Region;

    #@31f
    .local v6, _arg4:Landroid/graphics/Region;
    :goto_31f
    move-object v1, p0

    #@320
    .line 336
    invoke-virtual/range {v1 .. v6}, Landroid/view/IWindowSession$Stub;->setInsets(Landroid/view/IWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V

    #@323
    .line 337
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@326
    .line 338
    const/4 v1, 0x1

    #@327
    goto/16 :goto_7

    #@329
    .line 320
    .end local v4           #_arg2:Landroid/graphics/Rect;
    .end local v5           #_arg3:Landroid/graphics/Rect;
    .end local v6           #_arg4:Landroid/graphics/Region;
    :cond_329
    const/4 v4, 0x0

    #@32a
    .restart local v4       #_arg2:Landroid/graphics/Rect;
    goto :goto_2ff

    #@32b
    .line 327
    :cond_32b
    const/4 v5, 0x0

    #@32c
    .restart local v5       #_arg3:Landroid/graphics/Rect;
    goto :goto_30f

    #@32d
    .line 334
    :cond_32d
    const/4 v6, 0x0

    #@32e
    .restart local v6       #_arg4:Landroid/graphics/Region;
    goto :goto_31f

    #@32f
    .line 342
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/graphics/Rect;
    .end local v5           #_arg3:Landroid/graphics/Rect;
    .end local v6           #_arg4:Landroid/graphics/Region;
    :sswitch_32f
    const-string v1, "android.view.IWindowSession"

    #@331
    move-object/from16 v0, p2

    #@333
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@336
    .line 344
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@339
    move-result-object v1

    #@33a
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@33d
    move-result-object v2

    #@33e
    .line 346
    .restart local v2       #_arg0:Landroid/view/IWindow;
    new-instance v3, Landroid/graphics/Rect;

    #@340
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@343
    .line 347
    .local v3, _arg1:Landroid/graphics/Rect;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowSession$Stub;->getDisplayFrame(Landroid/view/IWindow;Landroid/graphics/Rect;)V

    #@346
    .line 348
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@349
    .line 349
    if-eqz v3, :cond_35a

    #@34b
    .line 350
    const/4 v1, 0x1

    #@34c
    move-object/from16 v0, p3

    #@34e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@351
    .line 351
    const/4 v1, 0x1

    #@352
    move-object/from16 v0, p3

    #@354
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@357
    .line 356
    :goto_357
    const/4 v1, 0x1

    #@358
    goto/16 :goto_7

    #@35a
    .line 354
    :cond_35a
    const/4 v1, 0x0

    #@35b
    move-object/from16 v0, p3

    #@35d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@360
    goto :goto_357

    #@361
    .line 360
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:Landroid/graphics/Rect;
    :sswitch_361
    const-string v1, "android.view.IWindowSession"

    #@363
    move-object/from16 v0, p2

    #@365
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@368
    .line 362
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@36b
    move-result-object v1

    #@36c
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@36f
    move-result-object v2

    #@370
    .line 363
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->finishDrawing(Landroid/view/IWindow;)V

    #@373
    .line 364
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@376
    .line 365
    const/4 v1, 0x1

    #@377
    goto/16 :goto_7

    #@379
    .line 369
    .end local v2           #_arg0:Landroid/view/IWindow;
    :sswitch_379
    const-string v1, "android.view.IWindowSession"

    #@37b
    move-object/from16 v0, p2

    #@37d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@380
    .line 371
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@383
    move-result v1

    #@384
    if-eqz v1, :cond_390

    #@386
    const/4 v2, 0x1

    #@387
    .line 372
    .local v2, _arg0:Z
    :goto_387
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->setInTouchMode(Z)V

    #@38a
    .line 373
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@38d
    .line 374
    const/4 v1, 0x1

    #@38e
    goto/16 :goto_7

    #@390
    .line 371
    .end local v2           #_arg0:Z
    :cond_390
    const/4 v2, 0x0

    #@391
    goto :goto_387

    #@392
    .line 378
    :sswitch_392
    const-string v1, "android.view.IWindowSession"

    #@394
    move-object/from16 v0, p2

    #@396
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@399
    .line 379
    invoke-virtual {p0}, Landroid/view/IWindowSession$Stub;->getInTouchMode()Z

    #@39c
    move-result v14

    #@39d
    .line 380
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a0
    .line 381
    if-eqz v14, :cond_3ab

    #@3a2
    const/4 v1, 0x1

    #@3a3
    :goto_3a3
    move-object/from16 v0, p3

    #@3a5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3a8
    .line 382
    const/4 v1, 0x1

    #@3a9
    goto/16 :goto_7

    #@3ab
    .line 381
    :cond_3ab
    const/4 v1, 0x0

    #@3ac
    goto :goto_3a3

    #@3ad
    .line 386
    .end local v14           #_result:Z
    :sswitch_3ad
    const-string v1, "android.view.IWindowSession"

    #@3af
    move-object/from16 v0, p2

    #@3b1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b4
    .line 388
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3b7
    move-result-object v1

    #@3b8
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@3bb
    move-result-object v2

    #@3bc
    .line 390
    .local v2, _arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3bf
    move-result v3

    #@3c0
    .line 392
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3c3
    move-result v1

    #@3c4
    if-eqz v1, :cond_3d9

    #@3c6
    const/4 v4, 0x1

    #@3c7
    .line 393
    .local v4, _arg2:Z
    :goto_3c7
    invoke-virtual {p0, v2, v3, v4}, Landroid/view/IWindowSession$Stub;->performHapticFeedback(Landroid/view/IWindow;IZ)Z

    #@3ca
    move-result v14

    #@3cb
    .line 394
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3ce
    .line 395
    if-eqz v14, :cond_3db

    #@3d0
    const/4 v1, 0x1

    #@3d1
    :goto_3d1
    move-object/from16 v0, p3

    #@3d3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3d6
    .line 396
    const/4 v1, 0x1

    #@3d7
    goto/16 :goto_7

    #@3d9
    .line 392
    .end local v4           #_arg2:Z
    .end local v14           #_result:Z
    :cond_3d9
    const/4 v4, 0x0

    #@3da
    goto :goto_3c7

    #@3db
    .line 395
    .restart local v4       #_arg2:Z
    .restart local v14       #_result:Z
    :cond_3db
    const/4 v1, 0x0

    #@3dc
    goto :goto_3d1

    #@3dd
    .line 400
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Z
    .end local v14           #_result:Z
    :sswitch_3dd
    const-string v1, "android.view.IWindowSession"

    #@3df
    move-object/from16 v0, p2

    #@3e1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e4
    .line 402
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3e7
    move-result-object v1

    #@3e8
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@3eb
    move-result-object v2

    #@3ec
    .line 404
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ef
    move-result v3

    #@3f0
    .line 406
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3f3
    move-result v4

    #@3f4
    .line 408
    .local v4, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3f7
    move-result v5

    #@3f8
    .line 410
    .local v5, _arg3:I
    new-instance v6, Landroid/view/Surface;

    #@3fa
    invoke-direct {v6}, Landroid/view/Surface;-><init>()V

    #@3fd
    .local v6, _arg4:Landroid/view/Surface;
    move-object v1, p0

    #@3fe
    .line 411
    invoke-virtual/range {v1 .. v6}, Landroid/view/IWindowSession$Stub;->prepareDrag(Landroid/view/IWindow;IIILandroid/view/Surface;)Landroid/os/IBinder;

    #@401
    move-result-object v14

    #@402
    .line 412
    .local v14, _result:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@405
    .line 413
    move-object/from16 v0, p3

    #@407
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@40a
    .line 414
    if-eqz v6, :cond_41b

    #@40c
    .line 415
    const/4 v1, 0x1

    #@40d
    move-object/from16 v0, p3

    #@40f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@412
    .line 416
    const/4 v1, 0x1

    #@413
    move-object/from16 v0, p3

    #@415
    invoke-virtual {v6, v0, v1}, Landroid/view/Surface;->writeToParcel(Landroid/os/Parcel;I)V

    #@418
    .line 421
    :goto_418
    const/4 v1, 0x1

    #@419
    goto/16 :goto_7

    #@41b
    .line 419
    :cond_41b
    const/4 v1, 0x0

    #@41c
    move-object/from16 v0, p3

    #@41e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@421
    goto :goto_418

    #@422
    .line 425
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/view/Surface;
    .end local v14           #_result:Landroid/os/IBinder;
    :sswitch_422
    const-string v1, "android.view.IWindowSession"

    #@424
    move-object/from16 v0, p2

    #@426
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@429
    .line 427
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@42c
    move-result-object v1

    #@42d
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@430
    move-result-object v2

    #@431
    .line 429
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@434
    move-result-object v3

    #@435
    .line 431
    .local v3, _arg1:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@438
    move-result v4

    #@439
    .line 433
    .local v4, _arg2:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@43c
    move-result v5

    #@43d
    .line 435
    .local v5, _arg3:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@440
    move-result v6

    #@441
    .line 437
    .local v6, _arg4:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@444
    move-result v7

    #@445
    .line 439
    .local v7, _arg5:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@448
    move-result v1

    #@449
    if-eqz v1, :cond_468

    #@44b
    .line 440
    sget-object v1, Landroid/content/ClipData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@44d
    move-object/from16 v0, p2

    #@44f
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@452
    move-result-object v8

    #@453
    check-cast v8, Landroid/content/ClipData;

    #@455
    .local v8, _arg6:Landroid/content/ClipData;
    :goto_455
    move-object v1, p0

    #@456
    .line 445
    invoke-virtual/range {v1 .. v8}, Landroid/view/IWindowSession$Stub;->performDrag(Landroid/view/IWindow;Landroid/os/IBinder;FFFFLandroid/content/ClipData;)Z

    #@459
    move-result v14

    #@45a
    .line 446
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@45d
    .line 447
    if-eqz v14, :cond_46a

    #@45f
    const/4 v1, 0x1

    #@460
    :goto_460
    move-object/from16 v0, p3

    #@462
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@465
    .line 448
    const/4 v1, 0x1

    #@466
    goto/16 :goto_7

    #@468
    .line 443
    .end local v8           #_arg6:Landroid/content/ClipData;
    .end local v14           #_result:Z
    :cond_468
    const/4 v8, 0x0

    #@469
    .restart local v8       #_arg6:Landroid/content/ClipData;
    goto :goto_455

    #@46a
    .line 447
    .restart local v14       #_result:Z
    :cond_46a
    const/4 v1, 0x0

    #@46b
    goto :goto_460

    #@46c
    .line 452
    .end local v2           #_arg0:Landroid/view/IWindow;
    .end local v3           #_arg1:Landroid/os/IBinder;
    .end local v4           #_arg2:F
    .end local v5           #_arg3:F
    .end local v6           #_arg4:F
    .end local v7           #_arg5:F
    .end local v8           #_arg6:Landroid/content/ClipData;
    .end local v14           #_result:Z
    :sswitch_46c
    const-string v1, "android.view.IWindowSession"

    #@46e
    move-object/from16 v0, p2

    #@470
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@473
    .line 454
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@476
    move-result-object v1

    #@477
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@47a
    move-result-object v2

    #@47b
    .line 456
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@47e
    move-result v1

    #@47f
    if-eqz v1, :cond_48b

    #@481
    const/4 v3, 0x1

    #@482
    .line 457
    .local v3, _arg1:Z
    :goto_482
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowSession$Stub;->reportDropResult(Landroid/view/IWindow;Z)V

    #@485
    .line 458
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@488
    .line 459
    const/4 v1, 0x1

    #@489
    goto/16 :goto_7

    #@48b
    .line 456
    .end local v3           #_arg1:Z
    :cond_48b
    const/4 v3, 0x0

    #@48c
    goto :goto_482

    #@48d
    .line 463
    .end local v2           #_arg0:Landroid/view/IWindow;
    :sswitch_48d
    const-string v1, "android.view.IWindowSession"

    #@48f
    move-object/from16 v0, p2

    #@491
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@494
    .line 465
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@497
    move-result-object v1

    #@498
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@49b
    move-result-object v2

    #@49c
    .line 466
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->dragRecipientEntered(Landroid/view/IWindow;)V

    #@49f
    .line 467
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a2
    .line 468
    const/4 v1, 0x1

    #@4a3
    goto/16 :goto_7

    #@4a5
    .line 472
    .end local v2           #_arg0:Landroid/view/IWindow;
    :sswitch_4a5
    const-string v1, "android.view.IWindowSession"

    #@4a7
    move-object/from16 v0, p2

    #@4a9
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ac
    .line 474
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4af
    move-result-object v1

    #@4b0
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@4b3
    move-result-object v2

    #@4b4
    .line 475
    .restart local v2       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->dragRecipientExited(Landroid/view/IWindow;)V

    #@4b7
    .line 476
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4ba
    .line 477
    const/4 v1, 0x1

    #@4bb
    goto/16 :goto_7

    #@4bd
    .line 481
    .end local v2           #_arg0:Landroid/view/IWindow;
    :sswitch_4bd
    const-string v1, "android.view.IWindowSession"

    #@4bf
    move-object/from16 v0, p2

    #@4c1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c4
    .line 483
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4c7
    move-result-object v2

    #@4c8
    .line 485
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@4cb
    move-result v3

    #@4cc
    .line 487
    .local v3, _arg1:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@4cf
    move-result v4

    #@4d0
    .line 489
    .restart local v4       #_arg2:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@4d3
    move-result v5

    #@4d4
    .line 491
    .restart local v5       #_arg3:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@4d7
    move-result v6

    #@4d8
    .restart local v6       #_arg4:F
    move-object v1, p0

    #@4d9
    .line 492
    invoke-virtual/range {v1 .. v6}, Landroid/view/IWindowSession$Stub;->setWallpaperPosition(Landroid/os/IBinder;FFFF)V

    #@4dc
    .line 493
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4df
    .line 494
    const/4 v1, 0x1

    #@4e0
    goto/16 :goto_7

    #@4e2
    .line 498
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:F
    .end local v4           #_arg2:F
    .end local v5           #_arg3:F
    .end local v6           #_arg4:F
    :sswitch_4e2
    const-string v1, "android.view.IWindowSession"

    #@4e4
    move-object/from16 v0, p2

    #@4e6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e9
    .line 500
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4ec
    move-result-object v2

    #@4ed
    .line 501
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowSession$Stub;->wallpaperOffsetsComplete(Landroid/os/IBinder;)V

    #@4f0
    .line 502
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f3
    .line 503
    const/4 v1, 0x1

    #@4f4
    goto/16 :goto_7

    #@4f6
    .line 507
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_4f6
    const-string v1, "android.view.IWindowSession"

    #@4f8
    move-object/from16 v0, p2

    #@4fa
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4fd
    .line 509
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@500
    move-result-object v2

    #@501
    .line 511
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@504
    move-result-object v3

    #@505
    .line 513
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@508
    move-result v4

    #@509
    .line 515
    .local v4, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@50c
    move-result v5

    #@50d
    .line 517
    .local v5, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@510
    move-result v6

    #@511
    .line 519
    .local v6, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@514
    move-result v1

    #@515
    if-eqz v1, :cond_541

    #@517
    .line 520
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@519
    move-object/from16 v0, p2

    #@51b
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@51e
    move-result-object v7

    #@51f
    check-cast v7, Landroid/os/Bundle;

    #@521
    .line 526
    .local v7, _arg5:Landroid/os/Bundle;
    :goto_521
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@524
    move-result v1

    #@525
    if-eqz v1, :cond_543

    #@527
    const/4 v8, 0x1

    #@528
    .local v8, _arg6:Z
    :goto_528
    move-object v1, p0

    #@529
    .line 527
    invoke-virtual/range {v1 .. v8}, Landroid/view/IWindowSession$Stub;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;

    #@52c
    move-result-object v14

    #@52d
    .line 528
    .local v14, _result:Landroid/os/Bundle;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@530
    .line 529
    if-eqz v14, :cond_545

    #@532
    .line 530
    const/4 v1, 0x1

    #@533
    move-object/from16 v0, p3

    #@535
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@538
    .line 531
    const/4 v1, 0x1

    #@539
    move-object/from16 v0, p3

    #@53b
    invoke-virtual {v14, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@53e
    .line 536
    :goto_53e
    const/4 v1, 0x1

    #@53f
    goto/16 :goto_7

    #@541
    .line 523
    .end local v7           #_arg5:Landroid/os/Bundle;
    .end local v8           #_arg6:Z
    .end local v14           #_result:Landroid/os/Bundle;
    :cond_541
    const/4 v7, 0x0

    #@542
    .restart local v7       #_arg5:Landroid/os/Bundle;
    goto :goto_521

    #@543
    .line 526
    :cond_543
    const/4 v8, 0x0

    #@544
    goto :goto_528

    #@545
    .line 534
    .restart local v8       #_arg6:Z
    .restart local v14       #_result:Landroid/os/Bundle;
    :cond_545
    const/4 v1, 0x0

    #@546
    move-object/from16 v0, p3

    #@548
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@54b
    goto :goto_53e

    #@54c
    .line 540
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Landroid/os/Bundle;
    .end local v8           #_arg6:Z
    .end local v14           #_result:Landroid/os/Bundle;
    :sswitch_54c
    const-string v1, "android.view.IWindowSession"

    #@54e
    move-object/from16 v0, p2

    #@550
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@553
    .line 542
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@556
    move-result-object v2

    #@557
    .line 544
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@55a
    move-result v1

    #@55b
    if-eqz v1, :cond_570

    #@55d
    .line 545
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@55f
    move-object/from16 v0, p2

    #@561
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@564
    move-result-object v3

    #@565
    check-cast v3, Landroid/os/Bundle;

    #@567
    .line 550
    .local v3, _arg1:Landroid/os/Bundle;
    :goto_567
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowSession$Stub;->wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V

    #@56a
    .line 551
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@56d
    .line 552
    const/4 v1, 0x1

    #@56e
    goto/16 :goto_7

    #@570
    .line 548
    .end local v3           #_arg1:Landroid/os/Bundle;
    :cond_570
    const/4 v3, 0x0

    #@571
    .restart local v3       #_arg1:Landroid/os/Bundle;
    goto :goto_567

    #@572
    .line 556
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Landroid/os/Bundle;
    :sswitch_572
    const-string v1, "android.view.IWindowSession"

    #@574
    move-object/from16 v0, p2

    #@576
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@579
    .line 558
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@57c
    move-result-object v2

    #@57d
    .line 560
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@580
    move-result v3

    #@581
    .line 562
    .local v3, _arg1:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@584
    move-result v4

    #@585
    .line 564
    .local v4, _arg2:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@588
    move-result v5

    #@589
    .line 566
    .local v5, _arg3:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@58c
    move-result v6

    #@58d
    .line 568
    .local v6, _arg4:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@590
    move-result v7

    #@591
    .line 570
    .local v7, _arg5:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@594
    move-result v8

    #@595
    .line 572
    .local v8, _arg6:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@598
    move-result v9

    #@599
    .local v9, _arg7:F
    move-object v1, p0

    #@59a
    .line 573
    invoke-virtual/range {v1 .. v9}, Landroid/view/IWindowSession$Stub;->setUniverseTransform(Landroid/os/IBinder;FFFFFFF)V

    #@59d
    .line 574
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a0
    .line 575
    const/4 v1, 0x1

    #@5a1
    goto/16 :goto_7

    #@5a3
    .line 579
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:F
    .end local v4           #_arg2:F
    .end local v5           #_arg3:F
    .end local v6           #_arg4:F
    .end local v7           #_arg5:F
    .end local v8           #_arg6:F
    .end local v9           #_arg7:F
    :sswitch_5a3
    const-string v1, "android.view.IWindowSession"

    #@5a5
    move-object/from16 v0, p2

    #@5a7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5aa
    .line 581
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5ad
    move-result-object v2

    #@5ae
    .line 583
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5b1
    move-result v1

    #@5b2
    if-eqz v1, :cond_5ce

    #@5b4
    .line 584
    sget-object v1, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5b6
    move-object/from16 v0, p2

    #@5b8
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5bb
    move-result-object v3

    #@5bc
    check-cast v3, Landroid/graphics/Rect;

    #@5be
    .line 590
    .local v3, _arg1:Landroid/graphics/Rect;
    :goto_5be
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5c1
    move-result v1

    #@5c2
    if-eqz v1, :cond_5d0

    #@5c4
    const/4 v4, 0x1

    #@5c5
    .line 591
    .local v4, _arg2:Z
    :goto_5c5
    invoke-virtual {p0, v2, v3, v4}, Landroid/view/IWindowSession$Stub;->onRectangleOnScreenRequested(Landroid/os/IBinder;Landroid/graphics/Rect;Z)V

    #@5c8
    .line 592
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5cb
    .line 593
    const/4 v1, 0x1

    #@5cc
    goto/16 :goto_7

    #@5ce
    .line 587
    .end local v3           #_arg1:Landroid/graphics/Rect;
    .end local v4           #_arg2:Z
    :cond_5ce
    const/4 v3, 0x0

    #@5cf
    .restart local v3       #_arg1:Landroid/graphics/Rect;
    goto :goto_5be

    #@5d0
    .line 590
    :cond_5d0
    const/4 v4, 0x0

    #@5d1
    goto :goto_5c5

    #@5d2
    .line 597
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Landroid/graphics/Rect;
    :sswitch_5d2
    const-string v1, "android.view.IWindowSession"

    #@5d4
    move-object/from16 v0, p2

    #@5d6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d9
    .line 599
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5dc
    move-result-object v1

    #@5dd
    invoke-static {v1}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@5e0
    move-result-object v2

    #@5e1
    .line 601
    .local v2, _arg0:Landroid/view/IWindow;
    new-instance v3, Landroid/graphics/Rect;

    #@5e3
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@5e6
    .line 602
    .restart local v3       #_arg1:Landroid/graphics/Rect;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowSession$Stub;->isWindowSplit(Landroid/view/IWindow;Landroid/graphics/Rect;)Z

    #@5e9
    move-result v14

    #@5ea
    .line 603
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5ed
    .line 604
    if-eqz v14, :cond_606

    #@5ef
    const/4 v1, 0x1

    #@5f0
    :goto_5f0
    move-object/from16 v0, p3

    #@5f2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5f5
    .line 605
    if-eqz v3, :cond_608

    #@5f7
    .line 606
    const/4 v1, 0x1

    #@5f8
    move-object/from16 v0, p3

    #@5fa
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5fd
    .line 607
    const/4 v1, 0x1

    #@5fe
    move-object/from16 v0, p3

    #@600
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@603
    .line 612
    :goto_603
    const/4 v1, 0x1

    #@604
    goto/16 :goto_7

    #@606
    .line 604
    :cond_606
    const/4 v1, 0x0

    #@607
    goto :goto_5f0

    #@608
    .line 610
    :cond_608
    const/4 v1, 0x0

    #@609
    move-object/from16 v0, p3

    #@60b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@60e
    goto :goto_603

    #@60f
    .line 43
    nop

    #@610
    :sswitch_data_610
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_7d
        0x3 -> :sswitch_ee
        0x4 -> :sswitch_141
        0x5 -> :sswitch_198
        0x6 -> :sswitch_1b0
        0x7 -> :sswitch_277
        0x8 -> :sswitch_28f
        0x9 -> :sswitch_2b2
        0xa -> :sswitch_2dc
        0xb -> :sswitch_32f
        0xc -> :sswitch_361
        0xd -> :sswitch_379
        0xe -> :sswitch_392
        0xf -> :sswitch_3ad
        0x10 -> :sswitch_3dd
        0x11 -> :sswitch_422
        0x12 -> :sswitch_46c
        0x13 -> :sswitch_48d
        0x14 -> :sswitch_4a5
        0x15 -> :sswitch_4bd
        0x16 -> :sswitch_4e2
        0x17 -> :sswitch_4f6
        0x18 -> :sswitch_54c
        0x19 -> :sswitch_572
        0x1a -> :sswitch_5a3
        0x1b -> :sswitch_5d2
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
