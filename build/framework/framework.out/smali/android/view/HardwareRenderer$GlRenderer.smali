.class abstract Landroid/view/HardwareRenderer$GlRenderer;
.super Landroid/view/HardwareRenderer;
.source "HardwareRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/HardwareRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "GlRenderer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;
    }
.end annotation


# static fields
.field static final FUNCTOR_PROCESS_DELAY:I = 0x4

.field static final SURFACE_STATE_ERROR:I = 0x0

.field static final SURFACE_STATE_SUCCESS:I = 0x1

.field static final SURFACE_STATE_UPDATED:I = 0x2

.field static sDirtyRegions:Z

.field static final sDirtyRegionsRequested:Z

.field static sEgl:Ljavax/microedition/khronos/egl/EGL10;

.field static sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field static final sEglContextStorage:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/opengl/ManagedEGLContext;",
            ">;"
        }
    .end annotation
.end field

.field static sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field static final sEglLock:[Ljava/lang/Object;


# instance fields
.field mCanvas:Landroid/view/HardwareCanvas;

.field final mDebugDirtyRegions:Z

.field mDebugPaint:Landroid/graphics/Paint;

.field private mDestroyed:Z

.field mDirtyRegionsEnabled:Z

.field mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field mEglThread:Ljava/lang/Thread;

.field mFrameCount:J

.field private final mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

.field mGl:Ljavax/microedition/khronos/opengles/GL;

.field final mGlVersion:I

.field mHeight:I

.field mProfileCurrentFrame:I

.field final mProfileData:[F

.field final mProfileEnabled:Z

.field final mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final mRedrawClip:Landroid/graphics/Rect;

.field final mShowOverdraw:Z

.field private final mSurfaceSize:[I

.field final mTranslucent:Z

.field mUpdateDirtyRegions:Z

.field final mVsyncDisabled:Z

.field mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 627
    const/4 v1, 0x0

    #@1
    new-array v1, v1, [Ljava/lang/Object;

    #@3
    sput-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEglLock:[Ljava/lang/Object;

    #@5
    .line 630
    new-instance v1, Ljava/lang/ThreadLocal;

    #@7
    invoke-direct {v1}, Ljava/lang/ThreadLocal;-><init>()V

    #@a
    sput-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEglContextStorage:Ljava/lang/ThreadLocal;

    #@c
    .line 647
    const-string v1, "debug.hwui.render_dirty_regions"

    #@e
    const-string/jumbo v2, "true"

    #@11
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 649
    .local v0, dirtyProperty:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@18
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v1

    #@1c
    sput-boolean v1, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegions:Z

    #@1e
    .line 650
    sget-boolean v1, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegions:Z

    #@20
    sput-boolean v1, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegionsRequested:Z

    #@22
    .line 651
    return-void
.end method

.method constructor <init>(IZ)V
    .registers 12
    .parameter "glVersion"
    .parameter "translucent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 676
    invoke-direct {p0}, Landroid/view/HardwareRenderer;-><init>()V

    #@5
    .line 628
    iput v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mWidth:I

    #@7
    iput v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mHeight:I

    #@9
    .line 661
    const/4 v3, -0x3

    #@a
    iput v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@c
    .line 671
    new-instance v3, Landroid/graphics/Rect;

    #@e
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mRedrawClip:Landroid/graphics/Rect;

    #@13
    .line 673
    const/4 v3, 0x2

    #@14
    new-array v3, v3, [I

    #@16
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mSurfaceSize:[I

    #@18
    .line 674
    new-instance v3, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@1a
    invoke-direct {v3, p0}, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;-><init>(Landroid/view/HardwareRenderer$GlRenderer;)V

    #@1d
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@1f
    .line 677
    iput p1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGlVersion:I

    #@21
    .line 678
    iput-boolean p2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mTranslucent:Z

    #@23
    .line 682
    const-string v3, "debug.hwui.disable_vsync"

    #@25
    const-string v4, "false"

    #@27
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    .line 683
    .local v2, property:Ljava/lang/String;
    const-string/jumbo v3, "true"

    #@2e
    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@31
    move-result v3

    #@32
    iput-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mVsyncDisabled:Z

    #@34
    .line 684
    iget-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mVsyncDisabled:Z

    #@36
    if-eqz v3, :cond_3f

    #@38
    .line 685
    const-string v3, "HardwareRenderer"

    #@3a
    const-string v4, "Disabling v-sync"

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 688
    :cond_3f
    const-string v3, "debug.hwui.profile"

    #@41
    const-string v4, "false"

    #@43
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    .line 689
    const-string/jumbo v3, "true"

    #@4a
    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@4d
    move-result v3

    #@4e
    iput-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@50
    .line 690
    iget-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@52
    if-eqz v3, :cond_5b

    #@54
    .line 691
    const-string v3, "HardwareRenderer"

    #@56
    const-string v4, "Profiling hardware renderer"

    #@58
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 694
    :cond_5b
    iget-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@5d
    if-eqz v3, :cond_c1

    #@5f
    .line 695
    const-string v3, "debug.hwui.profile.maxframes"

    #@61
    const/16 v4, 0x80

    #@63
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@66
    move-result-object v4

    #@67
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    .line 697
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@72
    move-result v1

    #@73
    .line 698
    .local v1, maxProfileFrames:I
    mul-int/lit8 v3, v1, 0x3

    #@75
    new-array v3, v3, [F

    #@77
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@79
    .line 699
    const/4 v0, 0x0

    #@7a
    .local v0, i:I
    :goto_7a
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@7c
    array-length v3, v3

    #@7d
    if-ge v0, v3, :cond_94

    #@7f
    .line 700
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@81
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@83
    add-int/lit8 v5, v0, 0x1

    #@85
    iget-object v6, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@87
    add-int/lit8 v7, v0, 0x2

    #@89
    const/high16 v8, -0x4080

    #@8b
    aput v8, v6, v7

    #@8d
    aput v8, v4, v5

    #@8f
    aput v8, v3, v0

    #@91
    .line 699
    add-int/lit8 v0, v0, 0x3

    #@93
    goto :goto_7a

    #@94
    .line 703
    :cond_94
    new-instance v3, Ljava/util/concurrent/locks/ReentrantLock;

    #@96
    invoke-direct {v3}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@99
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@9b
    .line 709
    .end local v0           #i:I
    .end local v1           #maxProfileFrames:I
    :goto_9b
    const-string v3, "debug.hwui.show_dirty_regions"

    #@9d
    const-string v4, "false"

    #@9f
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a2
    move-result-object v2

    #@a3
    .line 710
    const-string/jumbo v3, "true"

    #@a6
    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a9
    move-result v3

    #@aa
    iput-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugDirtyRegions:Z

    #@ac
    .line 711
    iget-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugDirtyRegions:Z

    #@ae
    if-eqz v3, :cond_b7

    #@b0
    .line 712
    const-string v3, "HardwareRenderer"

    #@b2
    const-string v4, "Debugging dirty regions"

    #@b4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 715
    :cond_b7
    const-string v3, "debug.hwui.show_overdraw"

    #@b9
    const/4 v4, 0x0

    #@ba
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@bd
    move-result v3

    #@be
    iput-boolean v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mShowOverdraw:Z

    #@c0
    .line 717
    return-void

    #@c1
    .line 705
    :cond_c1
    iput-object v5, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@c3
    .line 706
    iput-object v5, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@c5
    goto :goto_9b
.end method

.method static synthetic access$000(Landroid/view/HardwareRenderer$GlRenderer;)Landroid/graphics/Rect;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 617
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mRedrawClip:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/view/HardwareRenderer$GlRenderer;Landroid/view/View$AttachInfo;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 617
    invoke-direct {p0, p1, p2}, Landroid/view/HardwareRenderer$GlRenderer;->handleFunctorStatus(Landroid/view/View$AttachInfo;I)V

    #@3
    return-void
.end method

.method private checkEglErrorsForced()V
    .registers 5

    #@0
    .prologue
    .line 765
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@2
    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@5
    move-result v0

    #@6
    .line 766
    .local v0, error:I
    const/16 v1, 0x3000

    #@8
    if-eq v0, v1, :cond_2e

    #@a
    .line 769
    const-string v1, "HardwareRenderer"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "EGL error: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-static {v0}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 770
    const/16 v1, 0x300e

    #@28
    if-eq v0, v1, :cond_2f

    #@2a
    const/4 v1, 0x1

    #@2b
    :goto_2b
    invoke-direct {p0, v1}, Landroid/view/HardwareRenderer$GlRenderer;->fallback(Z)V

    #@2e
    .line 772
    :cond_2e
    return-void

    #@2f
    .line 770
    :cond_2f
    const/4 v1, 0x0

    #@30
    goto :goto_2b
.end method

.method private chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;
    .registers 20

    #@0
    .prologue
    .line 875
    const/4 v2, 0x1

    #@1
    new-array v11, v2, [Ljavax/microedition/khronos/egl/EGLConfig;

    #@3
    .line 876
    .local v11, configs:[Ljavax/microedition/khronos/egl/EGLConfig;
    const/4 v2, 0x1

    #@4
    new-array v7, v2, [I

    #@6
    .line 877
    .local v7, configsCount:[I
    sget-boolean v2, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegions:Z

    #@8
    move-object/from16 v0, p0

    #@a
    invoke-virtual {v0, v2}, Landroid/view/HardwareRenderer$GlRenderer;->getConfig(Z)[I

    #@d
    move-result-object v4

    #@e
    .line 880
    .local v4, configSpec:[I
    const-string v2, "debug.hwui.print_config"

    #@10
    const-string v3, ""

    #@12
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v16

    #@16
    .line 881
    .local v16, debug:Ljava/lang/String;
    const-string v2, "all"

    #@18
    move-object/from16 v0, v16

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_4c

    #@20
    .line 882
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@22
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@24
    const/4 v5, 0x0

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface/range {v2 .. v7}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@29
    .line 884
    const/4 v2, 0x0

    #@2a
    aget v2, v7, v2

    #@2c
    new-array v5, v2, [Ljavax/microedition/khronos/egl/EGLConfig;

    #@2e
    .line 885
    .local v5, debugConfigs:[Ljavax/microedition/khronos/egl/EGLConfig;
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@30
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@32
    const/4 v6, 0x0

    #@33
    aget v6, v7, v6

    #@35
    invoke-interface/range {v2 .. v7}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@38
    .line 888
    move-object v14, v5

    #@39
    .local v14, arr$:[Ljavax/microedition/khronos/egl/EGLConfig;
    array-length v0, v14

    #@3a
    move/from16 v18, v0

    #@3c
    .local v18, len$:I
    const/16 v17, 0x0

    #@3e
    .local v17, i$:I
    :goto_3e
    move/from16 v0, v17

    #@40
    move/from16 v1, v18

    #@42
    if-ge v0, v1, :cond_4c

    #@44
    aget-object v15, v14, v17

    #@46
    .line 889
    .local v15, config:Ljavax/microedition/khronos/egl/EGLConfig;
    invoke-static {v15}, Landroid/view/HardwareRenderer$GlRenderer;->printConfig(Ljavax/microedition/khronos/egl/EGLConfig;)V

    #@49
    .line 888
    add-int/lit8 v17, v17, 0x1

    #@4b
    goto :goto_3e

    #@4c
    .line 893
    .end local v5           #debugConfigs:[Ljavax/microedition/khronos/egl/EGLConfig;
    .end local v14           #arr$:[Ljavax/microedition/khronos/egl/EGLConfig;
    .end local v15           #config:Ljavax/microedition/khronos/egl/EGLConfig;
    .end local v17           #i$:I
    .end local v18           #len$:I
    :cond_4c
    sget-object v8, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@4e
    sget-object v9, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@50
    const/4 v12, 0x1

    #@51
    move-object v10, v4

    #@52
    move-object v13, v7

    #@53
    invoke-interface/range {v8 .. v13}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@56
    move-result v2

    #@57
    if-nez v2, :cond_7c

    #@59
    .line 894
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5b
    new-instance v3, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v6, "eglChooseConfig failed "

    #@62
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    sget-object v6, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@68
    invoke-interface {v6}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@6b
    move-result v6

    #@6c
    invoke-static {v6}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7b
    throw v2

    #@7c
    .line 896
    :cond_7c
    const/4 v2, 0x0

    #@7d
    aget v2, v7, v2

    #@7f
    if-lez v2, :cond_95

    #@81
    .line 897
    const-string v2, "choice"

    #@83
    move-object/from16 v0, v16

    #@85
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@88
    move-result v2

    #@89
    if-eqz v2, :cond_91

    #@8b
    .line 898
    const/4 v2, 0x0

    #@8c
    aget-object v2, v11, v2

    #@8e
    invoke-static {v2}, Landroid/view/HardwareRenderer$GlRenderer;->printConfig(Ljavax/microedition/khronos/egl/EGLConfig;)V

    #@91
    .line 900
    :cond_91
    const/4 v2, 0x0

    #@92
    aget-object v2, v11, v2

    #@94
    .line 903
    :goto_94
    return-object v2

    #@95
    :cond_95
    const/4 v2, 0x0

    #@96
    goto :goto_94
.end method

.method private createSurface(Landroid/view/Surface;)Z
    .registers 8
    .parameter "surface"

    #@0
    .prologue
    .line 1057
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@2
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@4
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@6
    const/4 v4, 0x0

    #@7
    invoke-interface {v1, v2, v3, p1, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    #@a
    move-result-object v1

    #@b
    iput-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@d
    .line 1059
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@f
    if-eqz v1, :cond_17

    #@11
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@13
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@15
    if-ne v1, v2, :cond_47

    #@17
    .line 1060
    :cond_17
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@19
    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@1c
    move-result v0

    #@1d
    .line 1061
    .local v0, error:I
    const/16 v1, 0x300b

    #@1f
    if-ne v0, v1, :cond_2a

    #@21
    .line 1062
    const-string v1, "HardwareRenderer"

    #@23
    const-string v2, "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1063
    const/4 v1, 0x0

    #@29
    .line 1076
    .end local v0           #error:I
    :goto_29
    return v1

    #@2a
    .line 1065
    .restart local v0       #error:I
    :cond_2a
    new-instance v1, Ljava/lang/RuntimeException;

    #@2c
    new-instance v2, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v3, "createWindowSurface failed "

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-static {v0}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@46
    throw v1

    #@47
    .line 1069
    .end local v0           #error:I
    :cond_47
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@49
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@4b
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@4d
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@4f
    iget-object v5, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@51
    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@54
    move-result v1

    #@55
    if-nez v1, :cond_7a

    #@57
    .line 1070
    new-instance v1, Ljava/lang/IllegalStateException;

    #@59
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v3, "eglMakeCurrent failed "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@66
    invoke-interface {v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@69
    move-result v3

    #@6a
    invoke-static {v3}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@79
    throw v1

    #@7a
    .line 1074
    :cond_7a
    invoke-direct {p0}, Landroid/view/HardwareRenderer$GlRenderer;->enableDirtyRegions()V

    #@7d
    .line 1076
    const/4 v1, 0x1

    #@7e
    goto :goto_29
.end method

.method private enableDirtyRegions()V
    .registers 3

    #@0
    .prologue
    .line 974
    sget-boolean v0, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegions:Z

    #@2
    if-eqz v0, :cond_14

    #@4
    .line 975
    invoke-static {}, Landroid/view/HardwareRenderer$GlRenderer;->preserveBackBuffer()Z

    #@7
    move-result v0

    #@8
    iput-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDirtyRegionsEnabled:Z

    #@a
    if-nez v0, :cond_13

    #@c
    .line 976
    const-string v0, "HardwareRenderer"

    #@e
    const-string v1, "Backbuffer cannot be preserved"

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 987
    :cond_13
    :goto_13
    return-void

    #@14
    .line 978
    :cond_14
    sget-boolean v0, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegionsRequested:Z

    #@16
    if-eqz v0, :cond_13

    #@18
    .line 985
    invoke-static {}, Landroid/view/HardwareRenderer$GlRenderer;->isBackBufferPreserved()Z

    #@1b
    move-result v0

    #@1c
    iput-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDirtyRegionsEnabled:Z

    #@1e
    goto :goto_13
.end method

.method private fallback(Z)V
    .registers 4
    .parameter "fallback"

    #@0
    .prologue
    .line 775
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/view/HardwareRenderer$GlRenderer;->destroy(Z)V

    #@4
    .line 776
    if-eqz p1, :cond_11

    #@6
    .line 778
    const/4 v0, 0x0

    #@7
    invoke-virtual {p0, v0}, Landroid/view/HardwareRenderer$GlRenderer;->setRequested(Z)V

    #@a
    .line 779
    const-string v0, "HardwareRenderer"

    #@c
    const-string v1, "Mountain View, we\'ve had a problem here. Switching back to software rendering."

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 782
    :cond_11
    return-void
.end method

.method private handleFunctorStatus(Landroid/view/View$AttachInfo;I)V
    .registers 7
    .parameter "attachInfo"
    .parameter "status"

    #@0
    .prologue
    .line 1323
    and-int/lit8 v0, p2, 0x1

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 1324
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mRedrawClip:Landroid/graphics/Rect;

    #@6
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_34

    #@c
    .line 1325
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@e
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->invalidate()V

    #@11
    .line 1332
    :cond_11
    :goto_11
    and-int/lit8 v0, p2, 0x2

    #@13
    if-nez v0, :cond_1f

    #@15
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@17
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@19
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_33

    #@1f
    .line 1334
    :cond_1f
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@21
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@23
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@26
    .line 1335
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@28
    iput-object p1, v0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->attachInfo:Landroid/view/View$AttachInfo;

    #@2a
    .line 1336
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@2c
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@2e
    const-wide/16 v2, 0x4

    #@30
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@33
    .line 1338
    :cond_33
    return-void

    #@34
    .line 1327
    :cond_34
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@36
    const/4 v1, 0x0

    #@37
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mRedrawClip:Landroid/graphics/Rect;

    #@39
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewRootImpl;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    #@3c
    .line 1328
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mRedrawClip:Landroid/graphics/Rect;

    #@3e
    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    #@41
    goto :goto_11
.end method

.method private static printConfig(Ljavax/microedition/khronos/egl/EGLConfig;)V
    .registers 6
    .parameter "config"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 907
    const/4 v1, 0x1

    #@2
    new-array v0, v1, [I

    #@4
    .line 909
    .local v0, value:[I
    const-string v1, "HardwareRenderer"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "EGL configuration "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ":"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 911
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@24
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@26
    const/16 v3, 0x3024

    #@28
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@2b
    .line 912
    const-string v1, "HardwareRenderer"

    #@2d
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "  RED_SIZE = "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    aget v3, v0, v4

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 914
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@47
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@49
    const/16 v3, 0x3023

    #@4b
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@4e
    .line 915
    const-string v1, "HardwareRenderer"

    #@50
    new-instance v2, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v3, "  GREEN_SIZE = "

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    aget v3, v0, v4

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v2

    #@65
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 917
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@6a
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@6c
    const/16 v3, 0x3022

    #@6e
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@71
    .line 918
    const-string v1, "HardwareRenderer"

    #@73
    new-instance v2, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v3, "  BLUE_SIZE = "

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    aget v3, v0, v4

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v2

    #@88
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 920
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@8d
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@8f
    const/16 v3, 0x3021

    #@91
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@94
    .line 921
    const-string v1, "HardwareRenderer"

    #@96
    new-instance v2, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v3, "  ALPHA_SIZE = "

    #@9d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v2

    #@a1
    aget v3, v0, v4

    #@a3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v2

    #@a7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v2

    #@ab
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 923
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@b0
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@b2
    const/16 v3, 0x3025

    #@b4
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@b7
    .line 924
    const-string v1, "HardwareRenderer"

    #@b9
    new-instance v2, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v3, "  DEPTH_SIZE = "

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v2

    #@c4
    aget v3, v0, v4

    #@c6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v2

    #@ca
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v2

    #@ce
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d1
    .line 926
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@d3
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@d5
    const/16 v3, 0x3026

    #@d7
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@da
    .line 927
    const-string v1, "HardwareRenderer"

    #@dc
    new-instance v2, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v3, "  STENCIL_SIZE = "

    #@e3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v2

    #@e7
    aget v3, v0, v4

    #@e9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v2

    #@ed
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f0
    move-result-object v2

    #@f1
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 929
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@f6
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@f8
    const/16 v3, 0x3032

    #@fa
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@fd
    .line 930
    const-string v1, "HardwareRenderer"

    #@ff
    new-instance v2, Ljava/lang/StringBuilder;

    #@101
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v3, "  SAMPLE_BUFFERS = "

    #@106
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v2

    #@10a
    aget v3, v0, v4

    #@10c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v2

    #@110
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v2

    #@114
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 932
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@119
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@11b
    const/16 v3, 0x3031

    #@11d
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@120
    .line 933
    const-string v1, "HardwareRenderer"

    #@122
    new-instance v2, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v3, "  SAMPLES = "

    #@129
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v2

    #@12d
    aget v3, v0, v4

    #@12f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v2

    #@133
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@136
    move-result-object v2

    #@137
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 935
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@13c
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@13e
    const/16 v3, 0x3033

    #@140
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@143
    .line 936
    const-string v1, "HardwareRenderer"

    #@145
    new-instance v2, Ljava/lang/StringBuilder;

    #@147
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14a
    const-string v3, "  SURFACE_TYPE = 0x"

    #@14c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v2

    #@150
    aget v3, v0, v4

    #@152
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@155
    move-result-object v3

    #@156
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v2

    #@15a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v2

    #@15e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@161
    .line 938
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@163
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@165
    const/16 v3, 0x3027

    #@167
    invoke-interface {v1, v2, p0, v3, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    #@16a
    .line 939
    const-string v1, "HardwareRenderer"

    #@16c
    new-instance v2, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v3, "  CONFIG_CAVEAT = 0x"

    #@173
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v2

    #@177
    aget v3, v0, v4

    #@179
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@17c
    move-result-object v3

    #@17d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v2

    #@181
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v2

    #@185
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@188
    .line 940
    return-void
.end method


# virtual methods
.method attachFunctor(Landroid/view/View$AttachInfo;I)Z
    .registers 7
    .parameter "attachInfo"
    .parameter "functor"

    #@0
    .prologue
    .line 1349
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 1350
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@6
    invoke-virtual {v0, p2}, Landroid/view/HardwareCanvas;->attachFunctor(I)V

    #@9
    .line 1351
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@b
    iput-object p1, v0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->attachInfo:Landroid/view/View$AttachInfo;

    #@d
    .line 1352
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@f
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@14
    .line 1353
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@16
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFunctorsRunnable:Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;

    #@18
    const-wide/16 v2, 0x0

    #@1a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@1d
    .line 1354
    const/4 v0, 0x1

    #@1e
    .line 1356
    :goto_1e
    return v0

    #@1f
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_1e
.end method

.method canDraw()Z
    .registers 2

    #@0
    .prologue
    .line 1109
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGl:Ljavax/microedition/khronos/opengles/GL;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method checkCurrent()I
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1367
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglThread:Ljava/lang/Thread;

    #@4
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@7
    move-result-object v3

    #@8
    if-eq v2, v3, :cond_39

    #@a
    .line 1368
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Hardware acceleration can only be used with a single UI thread.\nOriginal thread: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglThread:Ljava/lang/Thread;

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, "\n"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, "Current thread: "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@38
    throw v0

    #@39
    .line 1373
    :cond_39
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@3b
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@3d
    invoke-interface {v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v2

    #@45
    if-eqz v2, :cond_57

    #@47
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@49
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@4b
    const/16 v4, 0x3059

    #@4d
    invoke-interface {v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentSurface(I)Ljavax/microedition/khronos/egl/EGLSurface;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v2

    #@55
    if-nez v2, :cond_8d

    #@57
    .line 1375
    :cond_57
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@59
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@5b
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@5d
    iget-object v5, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@5f
    iget-object v6, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@61
    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@64
    move-result v2

    #@65
    if-nez v2, :cond_8e

    #@67
    .line 1376
    const-string v2, "HardwareRenderer"

    #@69
    new-instance v3, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v4, "eglMakeCurrent failed "

    #@70
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    sget-object v4, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@76
    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@79
    move-result v4

    #@7a
    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v3

    #@86
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 1378
    invoke-direct {p0, v0}, Landroid/view/HardwareRenderer$GlRenderer;->fallback(Z)V

    #@8c
    move v0, v1

    #@8d
    .line 1388
    :cond_8d
    :goto_8d
    return v0

    #@8e
    .line 1381
    :cond_8e
    iget-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mUpdateDirtyRegions:Z

    #@90
    if-eqz v0, :cond_97

    #@92
    .line 1382
    invoke-direct {p0}, Landroid/view/HardwareRenderer$GlRenderer;->enableDirtyRegions()V

    #@95
    .line 1383
    iput-boolean v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mUpdateDirtyRegions:Z

    #@97
    .line 1385
    :cond_97
    const/4 v0, 0x2

    #@98
    goto :goto_8d
.end method

.method checkEglErrors()V
    .registers 2

    #@0
    .prologue
    .line 759
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 760
    invoke-direct {p0}, Landroid/view/HardwareRenderer$GlRenderer;->checkEglErrorsForced()V

    #@9
    .line 762
    :cond_9
    return-void
.end method

.method abstract createCanvas()Landroid/view/HardwareCanvas;
.end method

.method createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;
    .registers 9
    .parameter "egl"
    .parameter "eglDisplay"
    .parameter "eglConfig"

    #@0
    .prologue
    .line 992
    const/4 v2, 0x3

    #@1
    new-array v0, v2, [I

    #@3
    const/4 v2, 0x0

    #@4
    const/16 v3, 0x3098

    #@6
    aput v3, v0, v2

    #@8
    const/4 v2, 0x1

    #@9
    iget v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGlVersion:I

    #@b
    aput v3, v0, v2

    #@d
    const/4 v2, 0x2

    #@e
    const/16 v3, 0x3038

    #@10
    aput v3, v0, v2

    #@12
    .line 994
    .local v0, attribs:[I
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@14
    iget v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGlVersion:I

    #@16
    if-eqz v3, :cond_45

    #@18
    .end local v0           #attribs:[I
    :goto_18
    invoke-interface {p1, p2, p3, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    #@1b
    move-result-object v1

    #@1c
    .line 996
    .local v1, context:Ljavax/microedition/khronos/egl/EGLContext;
    if-eqz v1, :cond_22

    #@1e
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@20
    if-ne v1, v2, :cond_47

    #@22
    .line 998
    :cond_22
    new-instance v2, Ljava/lang/IllegalStateException;

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "Could not create an EGL context. eglCreateContext failed with error: "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    sget-object v4, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@31
    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@34
    move-result v4

    #@35
    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@44
    throw v2

    #@45
    .line 994
    .end local v1           #context:Ljavax/microedition/khronos/egl/EGLContext;
    .restart local v0       #attribs:[I
    :cond_45
    const/4 v0, 0x0

    #@46
    goto :goto_18

    #@47
    .line 1002
    .end local v0           #attribs:[I
    .restart local v1       #context:Ljavax/microedition/khronos/egl/EGLContext;
    :cond_47
    return-object v1
.end method

.method createEglSurface(Landroid/view/Surface;)Ljavax/microedition/khronos/opengles/GL;
    .registers 4
    .parameter "surface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    .line 944
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 945
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "egl not initialized"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 947
    :cond_c
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@e
    if-nez v0, :cond_18

    #@10
    .line 948
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "eglDisplay not initialized"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 950
    :cond_18
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@1a
    if-nez v0, :cond_24

    #@1c
    .line 951
    new-instance v0, Ljava/lang/RuntimeException;

    #@1e
    const-string v1, "eglConfig not initialized"

    #@20
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 953
    :cond_24
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@27
    move-result-object v0

    #@28
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglThread:Ljava/lang/Thread;

    #@2a
    if-eq v0, v1, :cond_34

    #@2c
    .line 954
    new-instance v0, Ljava/lang/IllegalStateException;

    #@2e
    const-string v1, "HardwareRenderer cannot be used from multiple threads"

    #@30
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0

    #@34
    .line 959
    :cond_34
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->destroySurface()V

    #@37
    .line 962
    invoke-direct {p0, p1}, Landroid/view/HardwareRenderer$GlRenderer;->createSurface(Landroid/view/Surface;)Z

    #@3a
    move-result v0

    #@3b
    if-nez v0, :cond_3f

    #@3d
    .line 963
    const/4 v0, 0x0

    #@3e
    .line 968
    :goto_3e
    return-object v0

    #@3f
    .line 966
    :cond_3f
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->initCaches()V

    #@42
    .line 968
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@44
    invoke-virtual {v0}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    #@47
    move-result-object v0

    #@48
    goto :goto_3e
.end method

.method abstract createManagedContext(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/opengl/ManagedEGLContext;
.end method

.method destroy(Z)V
    .registers 5
    .parameter "full"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1007
    if-eqz p1, :cond_a

    #@4
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 1008
    iput-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@a
    .line 1011
    :cond_a
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->isEnabled()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_14

    #@10
    iget-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDestroyed:Z

    #@12
    if-eqz v0, :cond_18

    #@14
    .line 1012
    :cond_14
    invoke-virtual {p0, v1}, Landroid/view/HardwareRenderer$GlRenderer;->setEnabled(Z)V

    #@17
    .line 1021
    :goto_17
    return-void

    #@18
    .line 1016
    :cond_18
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->destroySurface()V

    #@1b
    .line 1017
    invoke-virtual {p0, v1}, Landroid/view/HardwareRenderer$GlRenderer;->setEnabled(Z)V

    #@1e
    .line 1019
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDestroyed:Z

    #@21
    .line 1020
    iput-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGl:Ljavax/microedition/khronos/opengles/GL;

    #@23
    goto :goto_17
.end method

.method destroySurface()V
    .registers 6

    #@0
    .prologue
    .line 1024
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@2
    if-eqz v0, :cond_23

    #@4
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@6
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@8
    if-eq v0, v1, :cond_23

    #@a
    .line 1025
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@c
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@e
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@10
    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@12
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@14
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@17
    .line 1026
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@19
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@1b
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@1d
    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    #@20
    .line 1027
    const/4 v0, 0x0

    #@21
    iput-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@23
    .line 1029
    :cond_23
    return-void
.end method

.method detachFunctor(I)V
    .registers 3
    .parameter "functor"

    #@0
    .prologue
    .line 1342
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1343
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@6
    invoke-virtual {v0, p1}, Landroid/view/HardwareCanvas;->detachFunctor(I)V

    #@9
    .line 1345
    :cond_9
    return-void
.end method

.method draw(Landroid/view/View;Landroid/view/View$AttachInfo;Landroid/view/HardwareRenderer$HardwareDrawCallbacks;Landroid/graphics/Rect;)Z
    .registers 28
    .parameter "view"
    .parameter "attachInfo"
    .parameter "callbacks"
    .parameter "dirty"

    #@0
    .prologue
    .line 1140
    invoke-virtual/range {p0 .. p0}, Landroid/view/HardwareRenderer$GlRenderer;->canDraw()Z

    #@3
    move-result v18

    #@4
    if-eqz v18, :cond_3ad

    #@6
    .line 1141
    invoke-virtual/range {p0 .. p0}, Landroid/view/HardwareRenderer$GlRenderer;->hasDirtyRegions()Z

    #@9
    move-result v18

    #@a
    if-nez v18, :cond_e

    #@c
    .line 1142
    const/16 p4, 0x0

    #@e
    .line 1144
    :cond_e
    const/16 v18, 0x1

    #@10
    move/from16 v0, v18

    #@12
    move-object/from16 v1, p2

    #@14
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@16
    .line 1145
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@19
    move-result-wide v18

    #@1a
    move-wide/from16 v0, v18

    #@1c
    move-object/from16 v2, p2

    #@1e
    iput-wide v0, v2, Landroid/view/View$AttachInfo;->mDrawingTime:J

    #@20
    .line 1147
    move-object/from16 v0, p1

    #@22
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@24
    move/from16 v18, v0

    #@26
    or-int/lit8 v18, v18, 0x20

    #@28
    move/from16 v0, v18

    #@2a
    move-object/from16 v1, p1

    #@2c
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@2e
    .line 1149
    invoke-virtual/range {p0 .. p0}, Landroid/view/HardwareRenderer$GlRenderer;->checkCurrent()I

    #@31
    move-result v16

    #@32
    .line 1150
    .local v16, surfaceState:I
    if-eqz v16, :cond_3ad

    #@34
    .line 1151
    move-object/from16 v0, p0

    #@36
    iget-object v3, v0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@38
    .line 1152
    .local v3, canvas:Landroid/view/HardwareCanvas;
    move-object/from16 v0, p2

    #@3a
    iput-object v3, v0, Landroid/view/View$AttachInfo;->mHardwareCanvas:Landroid/view/HardwareCanvas;

    #@3c
    .line 1154
    move-object/from16 v0, p0

    #@3e
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@40
    move/from16 v18, v0

    #@42
    if-eqz v18, :cond_4d

    #@44
    .line 1155
    move-object/from16 v0, p0

    #@46
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@48
    move-object/from16 v18, v0

    #@4a
    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@4d
    .line 1159
    :cond_4d
    const/16 v18, 0x2

    #@4f
    move/from16 v0, v16

    #@51
    move/from16 v1, v18

    #@53
    if-ne v0, v1, :cond_2d0

    #@55
    .line 1160
    const/16 p4, 0x0

    #@57
    .line 1161
    const/16 v18, 0x0

    #@59
    invoke-static/range {v18 .. v18}, Landroid/view/HardwareRenderer;->access$200([I)V

    #@5c
    .line 1176
    :cond_5c
    :goto_5c
    const/4 v13, 0x0

    #@5d
    .line 1177
    .local v13, saveCount:I
    const/4 v15, 0x0

    #@5e
    .line 1180
    .local v15, status:I
    :try_start_5e
    move-object/from16 v0, p1

    #@60
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@62
    move/from16 v18, v0

    #@64
    const/high16 v19, -0x8000

    #@66
    and-int v18, v18, v19

    #@68
    const/high16 v19, -0x8000

    #@6a
    move/from16 v0, v18

    #@6c
    move/from16 v1, v19

    #@6e
    if-ne v0, v1, :cond_322

    #@70
    const/16 v18, 0x1

    #@72
    :goto_72
    move/from16 v0, v18

    #@74
    move-object/from16 v1, p1

    #@76
    iput-boolean v0, v1, Landroid/view/View;->mRecreateDisplayList:Z

    #@78
    .line 1182
    move-object/from16 v0, p1

    #@7a
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@7c
    move/from16 v18, v0

    #@7e
    const v19, 0x7fffffff

    #@81
    and-int v18, v18, v19

    #@83
    move/from16 v0, v18

    #@85
    move-object/from16 v1, p1

    #@87
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@89
    .line 1184
    const-wide/16 v9, 0x0

    #@8b
    .line 1185
    .local v9, getDisplayListStartTime:J
    move-object/from16 v0, p0

    #@8d
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@8f
    move/from16 v18, v0

    #@91
    if-eqz v18, :cond_c4

    #@93
    .line 1186
    move-object/from16 v0, p0

    #@95
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@97
    move/from16 v18, v0

    #@99
    add-int/lit8 v18, v18, 0x3

    #@9b
    move/from16 v0, v18

    #@9d
    move-object/from16 v1, p0

    #@9f
    iput v0, v1, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@a1
    .line 1187
    move-object/from16 v0, p0

    #@a3
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@a5
    move/from16 v18, v0

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@ab
    move-object/from16 v19, v0

    #@ad
    move-object/from16 v0, v19

    #@af
    array-length v0, v0

    #@b0
    move/from16 v19, v0

    #@b2
    move/from16 v0, v18

    #@b4
    move/from16 v1, v19

    #@b6
    if-lt v0, v1, :cond_c0

    #@b8
    .line 1188
    const/16 v18, 0x0

    #@ba
    move/from16 v0, v18

    #@bc
    move-object/from16 v1, p0

    #@be
    iput v0, v1, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@c0
    .line 1191
    :cond_c0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@c3
    move-result-wide v9

    #@c4
    .line 1193
    :cond_c4
    const/16 v18, 0x8

    #@c6
    const-string v19, "hwrend"

    #@c8
    const-string/jumbo v20, "update"

    #@cb
    const-string/jumbo v21, "update DL"

    #@ce
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@d1
    .line 1195
    invoke-virtual {v3}, Landroid/view/HardwareCanvas;->clearLayerUpdates()V

    #@d4
    .line 1198
    const-wide/16 v18, 0x8

    #@d6
    const-string v20, "getDisplayList"

    #@d8
    invoke-static/range {v18 .. v20}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V
    :try_end_db
    .catchall {:try_start_5e .. :try_end_db} :catchall_32d

    #@db
    .line 1200
    :try_start_db
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getDisplayList()Landroid/view/DisplayList;
    :try_end_de
    .catchall {:try_start_db .. :try_end_de} :catchall_326

    #@de
    move-result-object v4

    #@df
    .line 1202
    .local v4, displayList:Landroid/view/DisplayList;
    const-wide/16 v18, 0x8

    #@e1
    :try_start_e1
    invoke-static/range {v18 .. v19}, Landroid/os/Trace;->traceEnd(J)V

    #@e4
    .line 1205
    const-wide/16 v18, 0x8

    #@e6
    const-string/jumbo v20, "prepareFrame"

    #@e9
    invoke-static/range {v18 .. v20}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@ec
    .line 1206
    const/16 v18, 0x8

    #@ee
    const-string v19, "hwrend"

    #@f0
    const-string/jumbo v20, "predraw"

    #@f3
    const-string/jumbo v21, "predraw start"

    #@f6
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f9
    .catchall {:try_start_e1 .. :try_end_f9} :catchall_32d

    #@f9
    .line 1209
    :try_start_f9
    move-object/from16 v0, p0

    #@fb
    move-object/from16 v1, p4

    #@fd
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer$GlRenderer;->onPreDraw(Landroid/graphics/Rect;)I
    :try_end_100
    .catchall {:try_start_f9 .. :try_end_100} :catchall_394

    #@100
    move-result v15

    #@101
    .line 1211
    const-wide/16 v18, 0x8

    #@103
    :try_start_103
    invoke-static/range {v18 .. v19}, Landroid/os/Trace;->traceEnd(J)V

    #@106
    .line 1213
    invoke-virtual {v3}, Landroid/view/HardwareCanvas;->save()I

    #@109
    move-result v13

    #@10a
    .line 1214
    move-object/from16 v0, p3

    #@10c
    invoke-interface {v0, v3}, Landroid/view/HardwareRenderer$HardwareDrawCallbacks;->onHardwarePreDraw(Landroid/view/HardwareCanvas;)V

    #@10f
    .line 1216
    const/16 v18, 0x10

    #@111
    const-string v19, "hwrend"

    #@113
    const-string/jumbo v20, "predraw"

    #@116
    const-string/jumbo v21, "predraw end"

    #@119
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 1219
    move-object/from16 v0, p0

    #@11e
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@120
    move/from16 v18, v0

    #@122
    if-eqz v18, :cond_142

    #@124
    .line 1220
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@127
    move-result-wide v11

    #@128
    .line 1221
    .local v11, now:J
    sub-long v18, v11, v9

    #@12a
    move-wide/from16 v0, v18

    #@12c
    long-to-float v0, v0

    #@12d
    move/from16 v18, v0

    #@12f
    const v19, 0x358637bd

    #@132
    mul-float v17, v18, v19

    #@134
    .line 1223
    .local v17, total:F
    move-object/from16 v0, p0

    #@136
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@138
    move-object/from16 v18, v0

    #@13a
    move-object/from16 v0, p0

    #@13c
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@13e
    move/from16 v19, v0

    #@140
    aput v17, v18, v19

    #@142
    .line 1226
    .end local v11           #now:J
    .end local v17           #total:F
    :cond_142
    const/16 v18, 0x10

    #@144
    const-string v19, "hwrend"

    #@146
    const-string/jumbo v20, "update"

    #@149
    const-string/jumbo v21, "update DL"

    #@14c
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@14f
    .line 1229
    if-eqz v4, :cond_3a2

    #@151
    .line 1230
    const-wide/16 v5, 0x0

    #@153
    .line 1231
    .local v5, drawDisplayListStartTime:J
    const/16 v18, 0x8

    #@155
    const-string v19, "hwrend"

    #@157
    const-string/jumbo v20, "process"

    #@15a
    const-string/jumbo v21, "process DL"

    #@15d
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@160
    .line 1234
    move-object/from16 v0, p0

    #@162
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@164
    move/from16 v18, v0

    #@166
    if-eqz v18, :cond_16c

    #@168
    .line 1235
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@16b
    move-result-wide v5

    #@16c
    .line 1238
    :cond_16c
    const-wide/16 v18, 0x8

    #@16e
    const-string v20, "drawDisplayList"

    #@170
    invoke-static/range {v18 .. v20}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V
    :try_end_173
    .catchall {:try_start_103 .. :try_end_173} :catchall_32d

    #@173
    .line 1240
    :try_start_173
    move-object/from16 v0, p0

    #@175
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mRedrawClip:Landroid/graphics/Rect;

    #@177
    move-object/from16 v18, v0

    #@179
    const/16 v19, 0x1

    #@17b
    move-object/from16 v0, v18

    #@17d
    move/from16 v1, v19

    #@17f
    invoke-virtual {v3, v4, v0, v1}, Landroid/view/HardwareCanvas;->drawDisplayList(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I
    :try_end_182
    .catchall {:try_start_173 .. :try_end_182} :catchall_39b

    #@182
    move-result v18

    #@183
    or-int v15, v15, v18

    #@185
    .line 1243
    const-wide/16 v18, 0x8

    #@187
    :try_start_187
    invoke-static/range {v18 .. v19}, Landroid/os/Trace;->traceEnd(J)V

    #@18a
    .line 1246
    move-object/from16 v0, p0

    #@18c
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@18e
    move/from16 v18, v0

    #@190
    if-eqz v18, :cond_1b2

    #@192
    .line 1247
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@195
    move-result-wide v11

    #@196
    .line 1248
    .restart local v11       #now:J
    sub-long v18, v11, v5

    #@198
    move-wide/from16 v0, v18

    #@19a
    long-to-float v0, v0

    #@19b
    move/from16 v18, v0

    #@19d
    const v19, 0x358637bd

    #@1a0
    mul-float v17, v18, v19

    #@1a2
    .line 1249
    .restart local v17       #total:F
    move-object/from16 v0, p0

    #@1a4
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@1a6
    move-object/from16 v18, v0

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@1ac
    move/from16 v19, v0

    #@1ae
    add-int/lit8 v19, v19, 0x1

    #@1b0
    aput v17, v18, v19

    #@1b2
    .line 1252
    .end local v11           #now:J
    .end local v17           #total:F
    :cond_1b2
    move-object/from16 v0, p0

    #@1b4
    move-object/from16 v1, p2

    #@1b6
    invoke-direct {v0, v1, v15}, Landroid/view/HardwareRenderer$GlRenderer;->handleFunctorStatus(Landroid/view/View$AttachInfo;I)V

    #@1b9
    .line 1253
    const/16 v18, 0x10

    #@1bb
    const-string v19, "hwrend"

    #@1bd
    const-string/jumbo v20, "process"

    #@1c0
    const-string/jumbo v21, "process DL"

    #@1c3
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c6
    .catchall {:try_start_187 .. :try_end_1c6} :catchall_32d

    #@1c6
    .line 1261
    .end local v5           #drawDisplayListStartTime:J
    :goto_1c6
    move-object/from16 v0, p3

    #@1c8
    invoke-interface {v0, v3}, Landroid/view/HardwareRenderer$HardwareDrawCallbacks;->onHardwarePostDraw(Landroid/view/HardwareCanvas;)V

    #@1cb
    .line 1262
    invoke-virtual {v3, v13}, Landroid/view/HardwareCanvas;->restoreToCount(I)V

    #@1ce
    .line 1263
    const/16 v18, 0x0

    #@1d0
    move/from16 v0, v18

    #@1d2
    move-object/from16 v1, p1

    #@1d4
    iput-boolean v0, v1, Landroid/view/View;->mRecreateDisplayList:Z

    #@1d6
    .line 1265
    move-object/from16 v0, p0

    #@1d8
    iget-wide v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@1da
    move-wide/from16 v18, v0

    #@1dc
    const-wide/16 v20, 0x1

    #@1de
    add-long v18, v18, v20

    #@1e0
    move-wide/from16 v0, v18

    #@1e2
    move-object/from16 v2, p0

    #@1e4
    iput-wide v0, v2, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@1e6
    .line 1267
    move-object/from16 v0, p0

    #@1e8
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugDirtyRegions:Z

    #@1ea
    move/from16 v18, v0

    #@1ec
    if-eqz v18, :cond_22b

    #@1ee
    .line 1268
    move-object/from16 v0, p0

    #@1f0
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@1f2
    move-object/from16 v18, v0

    #@1f4
    if-nez v18, :cond_20c

    #@1f6
    .line 1269
    new-instance v18, Landroid/graphics/Paint;

    #@1f8
    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    #@1fb
    move-object/from16 v0, v18

    #@1fd
    move-object/from16 v1, p0

    #@1ff
    iput-object v0, v1, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@201
    .line 1270
    move-object/from16 v0, p0

    #@203
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@205
    move-object/from16 v18, v0

    #@207
    const/high16 v19, 0x7fff

    #@209
    invoke-virtual/range {v18 .. v19}, Landroid/graphics/Paint;->setColor(I)V

    #@20c
    .line 1273
    :cond_20c
    if-eqz p4, :cond_22b

    #@20e
    move-object/from16 v0, p0

    #@210
    iget-wide v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@212
    move-wide/from16 v18, v0

    #@214
    const-wide/16 v20, 0x1

    #@216
    and-long v18, v18, v20

    #@218
    const-wide/16 v20, 0x0

    #@21a
    cmp-long v18, v18, v20

    #@21c
    if-nez v18, :cond_22b

    #@21e
    .line 1274
    move-object/from16 v0, p0

    #@220
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@222
    move-object/from16 v18, v0

    #@224
    move-object/from16 v0, p4

    #@226
    move-object/from16 v1, v18

    #@228
    invoke-virtual {v3, v0, v1}, Landroid/view/HardwareCanvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@22b
    .line 1280
    :cond_22b
    const/16 v18, 0x8

    #@22d
    const-string v19, "hwrend"

    #@22f
    const-string/jumbo v20, "postdraw"

    #@232
    const-string/jumbo v21, "postdraw start"

    #@235
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@238
    .line 1282
    invoke-virtual/range {p0 .. p0}, Landroid/view/HardwareRenderer$GlRenderer;->onPostDraw()V

    #@23b
    .line 1284
    const/16 v18, 0x10

    #@23d
    const-string v19, "hwrend"

    #@23f
    const-string/jumbo v20, "postdraw"

    #@242
    const-string/jumbo v21, "postdraw end"

    #@245
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@248
    .line 1286
    const/16 v18, 0x0

    #@24a
    move/from16 v0, v18

    #@24c
    move-object/from16 v1, p2

    #@24e
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@250
    .line 1288
    and-int/lit8 v18, v15, 0x4

    #@252
    const/16 v19, 0x4

    #@254
    move/from16 v0, v18

    #@256
    move/from16 v1, v19

    #@258
    if-ne v0, v1, :cond_2ba

    #@25a
    .line 1289
    const-wide/16 v7, 0x0

    #@25c
    .line 1290
    .local v7, eglSwapBuffersStartTime:J
    move-object/from16 v0, p0

    #@25e
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@260
    move/from16 v18, v0

    #@262
    if-eqz v18, :cond_268

    #@264
    .line 1291
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@267
    move-result-wide v7

    #@268
    .line 1293
    :cond_268
    const/16 v18, 0x8

    #@26a
    const-string v19, "hwrend"

    #@26c
    const-string/jumbo v20, "swapbuffers"

    #@26f
    const-string/jumbo v21, "swapbuffers DL"

    #@272
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@275
    .line 1296
    sget-object v18, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@277
    sget-object v19, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@279
    move-object/from16 v0, p0

    #@27b
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@27d
    move-object/from16 v20, v0

    #@27f
    invoke-interface/range {v18 .. v20}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    #@282
    .line 1298
    move-object/from16 v0, p0

    #@284
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@286
    move/from16 v18, v0

    #@288
    if-eqz v18, :cond_2aa

    #@28a
    .line 1299
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@28d
    move-result-wide v11

    #@28e
    .line 1300
    .restart local v11       #now:J
    sub-long v18, v11, v7

    #@290
    move-wide/from16 v0, v18

    #@292
    long-to-float v0, v0

    #@293
    move/from16 v18, v0

    #@295
    const v19, 0x358637bd

    #@298
    mul-float v17, v18, v19

    #@29a
    .line 1301
    .restart local v17       #total:F
    move-object/from16 v0, p0

    #@29c
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@29e
    move-object/from16 v18, v0

    #@2a0
    move-object/from16 v0, p0

    #@2a2
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I

    #@2a4
    move/from16 v19, v0

    #@2a6
    add-int/lit8 v19, v19, 0x2

    #@2a8
    aput v17, v18, v19

    #@2aa
    .line 1303
    .end local v11           #now:J
    .end local v17           #total:F
    :cond_2aa
    const/16 v18, 0x10

    #@2ac
    const-string v19, "hwrend"

    #@2ae
    const-string/jumbo v20, "swapbuffers"

    #@2b1
    const-string/jumbo v21, "swapbuffers DL"

    #@2b4
    invoke-static/range {v18 .. v21}, Landroid/util/jTestFramework;->print(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@2b7
    .line 1306
    invoke-virtual/range {p0 .. p0}, Landroid/view/HardwareRenderer$GlRenderer;->checkEglErrors()V

    #@2ba
    .line 1309
    .end local v7           #eglSwapBuffersStartTime:J
    :cond_2ba
    move-object/from16 v0, p0

    #@2bc
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@2be
    move/from16 v18, v0

    #@2c0
    if-eqz v18, :cond_2cb

    #@2c2
    .line 1310
    move-object/from16 v0, p0

    #@2c4
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@2c6
    move-object/from16 v18, v0

    #@2c8
    invoke-virtual/range {v18 .. v18}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@2cb
    .line 1313
    :cond_2cb
    if-nez p4, :cond_3a9

    #@2cd
    const/16 v18, 0x1

    #@2cf
    .line 1317
    .end local v3           #canvas:Landroid/view/HardwareCanvas;
    .end local v4           #displayList:Landroid/view/DisplayList;
    .end local v9           #getDisplayListStartTime:J
    .end local v13           #saveCount:I
    .end local v15           #status:I
    .end local v16           #surfaceState:I
    :goto_2cf
    return v18

    #@2d0
    .line 1163
    .restart local v3       #canvas:Landroid/view/HardwareCanvas;
    .restart local v16       #surfaceState:I
    :cond_2d0
    move-object/from16 v0, p0

    #@2d2
    iget-object v14, v0, Landroid/view/HardwareRenderer$GlRenderer;->mSurfaceSize:[I

    #@2d4
    .line 1164
    .local v14, size:[I
    invoke-static {v14}, Landroid/view/HardwareRenderer;->access$200([I)V

    #@2d7
    .line 1166
    const/16 v18, 0x1

    #@2d9
    aget v18, v14, v18

    #@2db
    move-object/from16 v0, p0

    #@2dd
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mHeight:I

    #@2df
    move/from16 v19, v0

    #@2e1
    move/from16 v0, v18

    #@2e3
    move/from16 v1, v19

    #@2e5
    if-ne v0, v1, :cond_2f7

    #@2e7
    const/16 v18, 0x0

    #@2e9
    aget v18, v14, v18

    #@2eb
    move-object/from16 v0, p0

    #@2ed
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mWidth:I

    #@2ef
    move/from16 v19, v0

    #@2f1
    move/from16 v0, v18

    #@2f3
    move/from16 v1, v19

    #@2f5
    if-eq v0, v1, :cond_5c

    #@2f7
    .line 1167
    :cond_2f7
    const/16 v18, 0x0

    #@2f9
    aget v18, v14, v18

    #@2fb
    move/from16 v0, v18

    #@2fd
    move-object/from16 v1, p0

    #@2ff
    iput v0, v1, Landroid/view/HardwareRenderer$GlRenderer;->mWidth:I

    #@301
    .line 1168
    const/16 v18, 0x1

    #@303
    aget v18, v14, v18

    #@305
    move/from16 v0, v18

    #@307
    move-object/from16 v1, p0

    #@309
    iput v0, v1, Landroid/view/HardwareRenderer$GlRenderer;->mHeight:I

    #@30b
    .line 1170
    move-object/from16 v0, p0

    #@30d
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mWidth:I

    #@30f
    move/from16 v18, v0

    #@311
    move-object/from16 v0, p0

    #@313
    iget v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mHeight:I

    #@315
    move/from16 v19, v0

    #@317
    move/from16 v0, v18

    #@319
    move/from16 v1, v19

    #@31b
    invoke-virtual {v3, v0, v1}, Landroid/view/HardwareCanvas;->setViewport(II)V

    #@31e
    .line 1172
    const/16 p4, 0x0

    #@320
    goto/16 :goto_5c

    #@322
    .line 1180
    .end local v14           #size:[I
    .restart local v13       #saveCount:I
    .restart local v15       #status:I
    :cond_322
    const/16 v18, 0x0

    #@324
    goto/16 :goto_72

    #@326
    .line 1202
    .restart local v9       #getDisplayListStartTime:J
    :catchall_326
    move-exception v18

    #@327
    const-wide/16 v19, 0x8

    #@329
    :try_start_329
    invoke-static/range {v19 .. v20}, Landroid/os/Trace;->traceEnd(J)V

    #@32c
    throw v18
    :try_end_32d
    .catchall {:try_start_329 .. :try_end_32d} :catchall_32d

    #@32d
    .line 1261
    .end local v9           #getDisplayListStartTime:J
    :catchall_32d
    move-exception v18

    #@32e
    move-object/from16 v0, p3

    #@330
    invoke-interface {v0, v3}, Landroid/view/HardwareRenderer$HardwareDrawCallbacks;->onHardwarePostDraw(Landroid/view/HardwareCanvas;)V

    #@333
    .line 1262
    invoke-virtual {v3, v13}, Landroid/view/HardwareCanvas;->restoreToCount(I)V

    #@336
    .line 1263
    const/16 v19, 0x0

    #@338
    move/from16 v0, v19

    #@33a
    move-object/from16 v1, p1

    #@33c
    iput-boolean v0, v1, Landroid/view/View;->mRecreateDisplayList:Z

    #@33e
    .line 1265
    move-object/from16 v0, p0

    #@340
    iget-wide v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@342
    move-wide/from16 v19, v0

    #@344
    const-wide/16 v21, 0x1

    #@346
    add-long v19, v19, v21

    #@348
    move-wide/from16 v0, v19

    #@34a
    move-object/from16 v2, p0

    #@34c
    iput-wide v0, v2, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@34e
    .line 1267
    move-object/from16 v0, p0

    #@350
    iget-boolean v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugDirtyRegions:Z

    #@352
    move/from16 v19, v0

    #@354
    if-eqz v19, :cond_393

    #@356
    .line 1268
    move-object/from16 v0, p0

    #@358
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@35a
    move-object/from16 v19, v0

    #@35c
    if-nez v19, :cond_374

    #@35e
    .line 1269
    new-instance v19, Landroid/graphics/Paint;

    #@360
    invoke-direct/range {v19 .. v19}, Landroid/graphics/Paint;-><init>()V

    #@363
    move-object/from16 v0, v19

    #@365
    move-object/from16 v1, p0

    #@367
    iput-object v0, v1, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@369
    .line 1270
    move-object/from16 v0, p0

    #@36b
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@36d
    move-object/from16 v19, v0

    #@36f
    const/high16 v20, 0x7fff

    #@371
    invoke-virtual/range {v19 .. v20}, Landroid/graphics/Paint;->setColor(I)V

    #@374
    .line 1273
    :cond_374
    if-eqz p4, :cond_393

    #@376
    move-object/from16 v0, p0

    #@378
    iget-wide v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@37a
    move-wide/from16 v19, v0

    #@37c
    const-wide/16 v21, 0x1

    #@37e
    and-long v19, v19, v21

    #@380
    const-wide/16 v21, 0x0

    #@382
    cmp-long v19, v19, v21

    #@384
    if-nez v19, :cond_393

    #@386
    .line 1274
    move-object/from16 v0, p0

    #@388
    iget-object v0, v0, Landroid/view/HardwareRenderer$GlRenderer;->mDebugPaint:Landroid/graphics/Paint;

    #@38a
    move-object/from16 v19, v0

    #@38c
    move-object/from16 v0, p4

    #@38e
    move-object/from16 v1, v19

    #@390
    invoke-virtual {v3, v0, v1}, Landroid/view/HardwareCanvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@393
    .line 1261
    :cond_393
    throw v18

    #@394
    .line 1211
    .restart local v4       #displayList:Landroid/view/DisplayList;
    .restart local v9       #getDisplayListStartTime:J
    :catchall_394
    move-exception v18

    #@395
    const-wide/16 v19, 0x8

    #@397
    :try_start_397
    invoke-static/range {v19 .. v20}, Landroid/os/Trace;->traceEnd(J)V

    #@39a
    throw v18

    #@39b
    .line 1243
    .restart local v5       #drawDisplayListStartTime:J
    :catchall_39b
    move-exception v18

    #@39c
    const-wide/16 v19, 0x8

    #@39e
    invoke-static/range {v19 .. v20}, Landroid/os/Trace;->traceEnd(J)V

    #@3a1
    throw v18

    #@3a2
    .line 1258
    .end local v5           #drawDisplayListStartTime:J
    :cond_3a2
    move-object/from16 v0, p1

    #@3a4
    invoke-virtual {v0, v3}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_3a7
    .catchall {:try_start_397 .. :try_end_3a7} :catchall_32d

    #@3a7
    goto/16 :goto_1c6

    #@3a9
    .line 1313
    :cond_3a9
    const/16 v18, 0x0

    #@3ab
    goto/16 :goto_2cf

    #@3ad
    .line 1317
    .end local v3           #canvas:Landroid/view/HardwareCanvas;
    .end local v4           #displayList:Landroid/view/DisplayList;
    .end local v9           #getDisplayListStartTime:J
    .end local v13           #saveCount:I
    .end local v15           #status:I
    .end local v16           #surfaceState:I
    :cond_3ad
    const/16 v18, 0x0

    #@3af
    goto/16 :goto_2cf
.end method

.method dumpGfxInfo(Ljava/io/PrintWriter;)V
    .registers 9
    .parameter "pw"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 721
    iget-boolean v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileEnabled:Z

    #@3
    if-eqz v1, :cond_2a

    #@5
    .line 722
    const-string v1, "\n\tDraw\tProcess\tExecute\n"

    #@7
    new-array v2, v2, [Ljava/lang/Object;

    #@9
    invoke-virtual {p1, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@c
    .line 724
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@e
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@11
    .line 726
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    :try_start_12
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@14
    array-length v1, v1

    #@15
    if-ge v0, v1, :cond_20

    #@17
    .line 727
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@19
    aget v1, v1, v0

    #@1b
    const/4 v2, 0x0

    #@1c
    cmpg-float v1, v1, v2

    #@1e
    if-gez v1, :cond_2b

    #@20
    .line 734
    :cond_20
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@22
    array-length v1, v1

    #@23
    iput v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileCurrentFrame:I
    :try_end_25
    .catchall {:try_start_12 .. :try_end_25} :catchall_6d

    #@25
    .line 736
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@27
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@2a
    .line 739
    .end local v0           #i:I
    :cond_2a
    return-void

    #@2b
    .line 730
    .restart local v0       #i:I
    :cond_2b
    :try_start_2b
    const-string v1, "\t%3.2f\t%3.2f\t%3.2f\n"

    #@2d
    const/4 v2, 0x3

    #@2e
    new-array v2, v2, [Ljava/lang/Object;

    #@30
    const/4 v3, 0x0

    #@31
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@33
    aget v4, v4, v0

    #@35
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@38
    move-result-object v4

    #@39
    aput-object v4, v2, v3

    #@3b
    const/4 v3, 0x1

    #@3c
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@3e
    add-int/lit8 v5, v0, 0x1

    #@40
    aget v4, v4, v5

    #@42
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@45
    move-result-object v4

    #@46
    aput-object v4, v2, v3

    #@48
    const/4 v3, 0x2

    #@49
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@4b
    add-int/lit8 v5, v0, 0x2

    #@4d
    aget v4, v4, v5

    #@4f
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@52
    move-result-object v4

    #@53
    aput-object v4, v2, v3

    #@55
    invoke-virtual {p1, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@58
    .line 732
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@5a
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@5c
    add-int/lit8 v3, v0, 0x1

    #@5e
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileData:[F

    #@60
    add-int/lit8 v5, v0, 0x2

    #@62
    const/high16 v6, -0x4080

    #@64
    aput v6, v4, v5

    #@66
    aput v6, v2, v3

    #@68
    aput v6, v1, v0
    :try_end_6a
    .catchall {:try_start_2b .. :try_end_6a} :catchall_6d

    #@6a
    .line 726
    add-int/lit8 v0, v0, 0x3

    #@6c
    goto :goto_12

    #@6d
    .line 736
    :catchall_6d
    move-exception v1

    #@6e
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mProfileLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@70
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@73
    throw v1
.end method

.method getCanvas()Landroid/view/HardwareCanvas;
    .registers 2

    #@0
    .prologue
    .line 1105
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@2
    return-object v0
.end method

.method abstract getConfig(Z)[I
.end method

.method getFrameCount()J
    .registers 3

    #@0
    .prologue
    .line 743
    iget-wide v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mFrameCount:J

    #@2
    return-wide v0
.end method

.method getHeight()I
    .registers 2

    #@0
    .prologue
    .line 1100
    iget v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mHeight:I

    #@2
    return v0
.end method

.method getWidth()I
    .registers 2

    #@0
    .prologue
    .line 1095
    iget v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mWidth:I

    #@2
    return v0
.end method

.method hasDirtyRegions()Z
    .registers 2

    #@0
    .prologue
    .line 750
    iget-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDirtyRegionsEnabled:Z

    #@2
    return v0
.end method

.method abstract initCaches()V
.end method

.method initialize(Landroid/view/Surface;)Z
    .registers 7
    .parameter "surface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 786
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->isRequested()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_4e

    #@8
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->isEnabled()Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_4e

    #@e
    .line 787
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->initializeEgl()V

    #@11
    .line 788
    invoke-virtual {p0, p1}, Landroid/view/HardwareRenderer$GlRenderer;->createEglSurface(Landroid/view/Surface;)Ljavax/microedition/khronos/opengles/GL;

    #@14
    move-result-object v3

    #@15
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGl:Ljavax/microedition/khronos/opengles/GL;

    #@17
    .line 789
    iput-boolean v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mDestroyed:Z

    #@19
    .line 791
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mGl:Ljavax/microedition/khronos/opengles/GL;

    #@1b
    if-eqz v3, :cond_4e

    #@1d
    .line 792
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@1f
    invoke-interface {v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@22
    move-result v0

    #@23
    .line 793
    .local v0, err:I
    const/16 v3, 0x3000

    #@25
    if-eq v0, v3, :cond_32

    #@27
    .line 794
    invoke-virtual {p0, v1}, Landroid/view/HardwareRenderer$GlRenderer;->destroy(Z)V

    #@2a
    .line 795
    invoke-virtual {p0, v2}, Landroid/view/HardwareRenderer$GlRenderer;->setRequested(Z)V

    #@2d
    .line 807
    :goto_2d
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@2f
    if-eqz v3, :cond_4c

    #@31
    .line 810
    .end local v0           #err:I
    :goto_31
    return v1

    #@32
    .line 797
    .restart local v0       #err:I
    :cond_32
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@34
    if-nez v3, :cond_3c

    #@36
    .line 798
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->createCanvas()Landroid/view/HardwareCanvas;

    #@39
    move-result-object v3

    #@3a
    iput-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@3c
    .line 800
    :cond_3c
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@3e
    if-eqz v3, :cond_44

    #@40
    .line 801
    invoke-virtual {p0, v1}, Landroid/view/HardwareRenderer$GlRenderer;->setEnabled(Z)V

    #@43
    goto :goto_2d

    #@44
    .line 803
    :cond_44
    const-string v3, "HardwareRenderer"

    #@46
    const-string v4, "Hardware accelerated Canvas could not be created"

    #@48
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_2d

    #@4c
    :cond_4c
    move v1, v2

    #@4d
    .line 807
    goto :goto_31

    #@4e
    .end local v0           #err:I
    :cond_4e
    move v1, v2

    #@4f
    .line 810
    goto :goto_31
.end method

.method initializeEgl()V
    .registers 7

    #@0
    .prologue
    .line 825
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEglLock:[Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 826
    :try_start_3
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@5
    if-nez v2, :cond_a7

    #@7
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@9
    if-nez v2, :cond_a7

    #@b
    .line 827
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Ljavax/microedition/khronos/egl/EGL10;

    #@11
    sput-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@13
    .line 830
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@15
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    #@17
    invoke-interface {v2, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    #@1a
    move-result-object v2

    #@1b
    sput-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@1d
    .line 832
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@1f
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@21
    if-ne v2, v4, :cond_49

    #@23
    .line 833
    new-instance v2, Ljava/lang/RuntimeException;

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "eglGetDisplay failed "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    sget-object v5, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@32
    invoke-interface {v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@35
    move-result v5

    #@36
    invoke-static {v5}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@45
    throw v2

    #@46
    .line 860
    :catchall_46
    move-exception v2

    #@47
    monitor-exit v3
    :try_end_48
    .catchall {:try_start_3 .. :try_end_48} :catchall_46

    #@48
    throw v2

    #@49
    .line 838
    :cond_49
    const/4 v2, 0x2

    #@4a
    :try_start_4a
    new-array v1, v2, [I

    #@4c
    .line 839
    .local v1, version:[I
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@4e
    sget-object v4, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@50
    invoke-interface {v2, v4, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    #@53
    move-result v2

    #@54
    if-nez v2, :cond_79

    #@56
    .line 840
    new-instance v2, Ljava/lang/RuntimeException;

    #@58
    new-instance v4, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v5, "eglInitialize failed "

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    sget-object v5, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@65
    invoke-interface {v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    #@68
    move-result v5

    #@69
    invoke-static {v5}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@78
    throw v2

    #@79
    .line 844
    :cond_79
    invoke-direct {p0}, Landroid/view/HardwareRenderer$GlRenderer;->checkEglErrorsForced()V

    #@7c
    .line 846
    invoke-direct {p0}, Landroid/view/HardwareRenderer$GlRenderer;->chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;

    #@7f
    move-result-object v2

    #@80
    sput-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@82
    .line 847
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@84
    if-nez v2, :cond_a7

    #@86
    .line 849
    sget-boolean v2, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegions:Z

    #@88
    if-eqz v2, :cond_9f

    #@8a
    .line 850
    const/4 v2, 0x0

    #@8b
    sput-boolean v2, Landroid/view/HardwareRenderer$GlRenderer;->sDirtyRegions:Z

    #@8d
    .line 851
    invoke-direct {p0}, Landroid/view/HardwareRenderer$GlRenderer;->chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;

    #@90
    move-result-object v2

    #@91
    sput-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@93
    .line 852
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@95
    if-nez v2, :cond_a7

    #@97
    .line 853
    new-instance v2, Ljava/lang/RuntimeException;

    #@99
    const-string v4, "eglConfig not initialized"

    #@9b
    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@9e
    throw v2

    #@9f
    .line 856
    :cond_9f
    new-instance v2, Ljava/lang/RuntimeException;

    #@a1
    const-string v4, "eglConfig not initialized"

    #@a3
    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@a6
    throw v2

    #@a7
    .line 860
    .end local v1           #version:[I
    :cond_a7
    monitor-exit v3
    :try_end_a8
    .catchall {:try_start_4a .. :try_end_a8} :catchall_46

    #@a8
    .line 862
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglContextStorage:Ljava/lang/ThreadLocal;

    #@aa
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@ad
    move-result-object v0

    #@ae
    check-cast v0, Landroid/opengl/ManagedEGLContext;

    #@b0
    .line 863
    .local v0, managedContext:Landroid/opengl/ManagedEGLContext;
    if-eqz v0, :cond_da

    #@b2
    invoke-virtual {v0}, Landroid/opengl/ManagedEGLContext;->getContext()Ljavax/microedition/khronos/egl/EGLContext;

    #@b5
    move-result-object v2

    #@b6
    :goto_b6
    iput-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@b8
    .line 864
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@bb
    move-result-object v2

    #@bc
    iput-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglThread:Ljava/lang/Thread;

    #@be
    .line 866
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@c0
    if-nez v2, :cond_d9

    #@c2
    .line 867
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@c4
    sget-object v3, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@c6
    sget-object v4, Landroid/view/HardwareRenderer$GlRenderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@c8
    invoke-virtual {p0, v2, v3, v4}, Landroid/view/HardwareRenderer$GlRenderer;->createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    #@cb
    move-result-object v2

    #@cc
    iput-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@ce
    .line 868
    sget-object v2, Landroid/view/HardwareRenderer$GlRenderer;->sEglContextStorage:Ljava/lang/ThreadLocal;

    #@d0
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@d2
    invoke-virtual {p0, v3}, Landroid/view/HardwareRenderer$GlRenderer;->createManagedContext(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/opengl/ManagedEGLContext;

    #@d5
    move-result-object v3

    #@d6
    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@d9
    .line 870
    :cond_d9
    return-void

    #@da
    .line 863
    :cond_da
    const/4 v2, 0x0

    #@db
    goto :goto_b6
.end method

.method invalidate(Landroid/view/Surface;)V
    .registers 8
    .parameter "surface"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1035
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@3
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@5
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@7
    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@9
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@b
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@e
    .line 1037
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@10
    if-eqz v0, :cond_28

    #@12
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@14
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@16
    if-eq v0, v1, :cond_28

    #@18
    .line 1038
    sget-object v0, Landroid/view/HardwareRenderer$GlRenderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@1a
    sget-object v1, Landroid/view/HardwareRenderer$GlRenderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@1c
    iget-object v2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@1e
    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    #@21
    .line 1039
    const/4 v0, 0x0

    #@22
    iput-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    #@24
    .line 1040
    const/4 v0, 0x0

    #@25
    invoke-virtual {p0, v0}, Landroid/view/HardwareRenderer$GlRenderer;->setEnabled(Z)V

    #@28
    .line 1043
    :cond_28
    invoke-virtual {p1}, Landroid/view/Surface;->isValid()Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_34

    #@2e
    .line 1044
    invoke-direct {p0, p1}, Landroid/view/HardwareRenderer$GlRenderer;->createSurface(Landroid/view/Surface;)Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_35

    #@34
    .line 1054
    :cond_34
    :goto_34
    return-void

    #@35
    .line 1048
    :cond_35
    iput-boolean v5, p0, Landroid/view/HardwareRenderer$GlRenderer;->mUpdateDirtyRegions:Z

    #@37
    .line 1050
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@39
    if-eqz v0, :cond_34

    #@3b
    .line 1051
    invoke-virtual {p0, v5}, Landroid/view/HardwareRenderer$GlRenderer;->setEnabled(Z)V

    #@3e
    goto :goto_34
.end method

.method onPostDraw()V
    .registers 1

    #@0
    .prologue
    .line 1117
    return-void
.end method

.method onPreDraw(Landroid/graphics/Rect;)I
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 1113
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method setup(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1086
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->validate()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 1087
    iget-object v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@8
    invoke-virtual {v0, p1, p2}, Landroid/view/HardwareCanvas;->setViewport(II)V

    #@b
    .line 1088
    iput p1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mWidth:I

    #@d
    .line 1089
    iput p2, p0, Landroid/view/HardwareRenderer$GlRenderer;->mHeight:I

    #@f
    .line 1091
    :cond_f
    return-void
.end method

.method updateSurface(Landroid/view/Surface;)V
    .registers 3
    .parameter "surface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    .line 815
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->isRequested()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 816
    invoke-virtual {p0, p1}, Landroid/view/HardwareRenderer$GlRenderer;->createEglSurface(Landroid/view/Surface;)Ljavax/microedition/khronos/opengles/GL;

    #@f
    .line 818
    :cond_f
    return-void
.end method

.method validate()Z
    .registers 2

    #@0
    .prologue
    .line 1081
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$GlRenderer;->checkCurrent()I

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method
