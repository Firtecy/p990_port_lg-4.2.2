.class public final Landroid/view/Surface$PhysicalDisplayInfo;
.super Ljava/lang/Object;
.source "Surface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Surface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PhysicalDisplayInfo"
.end annotation


# instance fields
.field public density:F

.field public height:I

.field public refreshRate:F

.field public secure:Z

.field public width:I

.field public xDpi:F

.field public yDpi:F


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 787
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 788
    return-void
.end method

.method public constructor <init>(Landroid/view/Surface$PhysicalDisplayInfo;)V
    .registers 2
    .parameter "other"

    #@0
    .prologue
    .line 790
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 791
    invoke-virtual {p0, p1}, Landroid/view/Surface$PhysicalDisplayInfo;->copyFrom(Landroid/view/Surface$PhysicalDisplayInfo;)V

    #@6
    .line 792
    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/view/Surface$PhysicalDisplayInfo;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 816
    iget v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->width:I

    #@2
    iput v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->width:I

    #@4
    .line 817
    iget v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->height:I

    #@6
    iput v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->height:I

    #@8
    .line 818
    iget v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->refreshRate:F

    #@a
    iput v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->refreshRate:F

    #@c
    .line 819
    iget v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->density:F

    #@e
    iput v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->density:F

    #@10
    .line 820
    iget v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->xDpi:F

    #@12
    iput v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->xDpi:F

    #@14
    .line 821
    iget v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->yDpi:F

    #@16
    iput v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->yDpi:F

    #@18
    .line 822
    iget-boolean v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@1a
    iput-boolean v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@1c
    .line 823
    return-void
.end method

.method public equals(Landroid/view/Surface$PhysicalDisplayInfo;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 800
    if-eqz p1, :cond_36

    #@2
    iget v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->width:I

    #@4
    iget v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->width:I

    #@6
    if-ne v0, v1, :cond_36

    #@8
    iget v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->height:I

    #@a
    iget v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->height:I

    #@c
    if-ne v0, v1, :cond_36

    #@e
    iget v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->refreshRate:F

    #@10
    iget v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->refreshRate:F

    #@12
    cmpl-float v0, v0, v1

    #@14
    if-nez v0, :cond_36

    #@16
    iget v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->density:F

    #@18
    iget v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->density:F

    #@1a
    cmpl-float v0, v0, v1

    #@1c
    if-nez v0, :cond_36

    #@1e
    iget v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->xDpi:F

    #@20
    iget v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->xDpi:F

    #@22
    cmpl-float v0, v0, v1

    #@24
    if-nez v0, :cond_36

    #@26
    iget v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->yDpi:F

    #@28
    iget v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->yDpi:F

    #@2a
    cmpl-float v0, v0, v1

    #@2c
    if-nez v0, :cond_36

    #@2e
    iget-boolean v0, p0, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@30
    iget-boolean v1, p1, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@32
    if-ne v0, v1, :cond_36

    #@34
    const/4 v0, 0x1

    #@35
    :goto_35
    return v0

    #@36
    :cond_36
    const/4 v0, 0x0

    #@37
    goto :goto_35
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 796
    instance-of v0, p1, Landroid/view/Surface$PhysicalDisplayInfo;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Landroid/view/Surface$PhysicalDisplayInfo;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/Surface$PhysicalDisplayInfo;->equals(Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 812
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "PhysicalDisplayInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->width:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " x "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->height:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->refreshRate:F

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " fps, "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, "density "

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    iget v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->density:F

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, ", "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    iget v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->xDpi:F

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, " x "

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->yDpi:F

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, " dpi, secure "

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    iget-boolean v1, p0, Landroid/view/Surface$PhysicalDisplayInfo;->secure:Z

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    const-string/jumbo v1, "}"

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    return-object v0
.end method
