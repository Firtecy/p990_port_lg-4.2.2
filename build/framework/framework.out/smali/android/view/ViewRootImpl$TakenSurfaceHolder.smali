.class Landroid/view/ViewRootImpl$TakenSurfaceHolder;
.super Lcom/android/internal/view/BaseSurfaceHolder;
.source "ViewRootImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TakenSurfaceHolder"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 4985
    iput-object p1, p0, Landroid/view/ViewRootImpl$TakenSurfaceHolder;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-direct {p0}, Lcom/android/internal/view/BaseSurfaceHolder;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public isCreating()Z
    .registers 2

    #@0
    .prologue
    .line 5011
    iget-object v0, p0, Landroid/view/ViewRootImpl$TakenSurfaceHolder;->this$0:Landroid/view/ViewRootImpl;

    #@2
    iget-boolean v0, v0, Landroid/view/ViewRootImpl;->mIsCreating:Z

    #@4
    return v0
.end method

.method public onAllowLockCanvas()Z
    .registers 2

    #@0
    .prologue
    .line 4988
    iget-object v0, p0, Landroid/view/ViewRootImpl$TakenSurfaceHolder;->this$0:Landroid/view/ViewRootImpl;

    #@2
    iget-boolean v0, v0, Landroid/view/ViewRootImpl;->mDrawingAllowed:Z

    #@4
    return v0
.end method

.method public onRelayoutContainer()V
    .registers 1

    #@0
    .prologue
    .line 4994
    return-void
.end method

.method public onUpdateSurface()V
    .registers 3

    #@0
    .prologue
    .line 5007
    new-instance v0, Ljava/lang/IllegalStateException;

    #@2
    const-string v1, "Shouldn\'t be here"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setFixedSize(II)V
    .registers 5
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 5016
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Currently only support sizing from layout"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setFormat(I)V
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 4997
    iget-object v0, p0, Landroid/view/ViewRootImpl$TakenSurfaceHolder;->this$0:Landroid/view/ViewRootImpl;

    #@2
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4
    check-cast v0, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@6
    invoke-interface {v0, p1}, Lcom/android/internal/view/RootViewSurfaceTaker;->setSurfaceFormat(I)V

    #@9
    .line 4998
    return-void
.end method

.method public setKeepScreenOn(Z)V
    .registers 3
    .parameter "screenOn"

    #@0
    .prologue
    .line 5021
    iget-object v0, p0, Landroid/view/ViewRootImpl$TakenSurfaceHolder;->this$0:Landroid/view/ViewRootImpl;

    #@2
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4
    check-cast v0, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@6
    invoke-interface {v0, p1}, Lcom/android/internal/view/RootViewSurfaceTaker;->setSurfaceKeepScreenOn(Z)V

    #@9
    .line 5022
    return-void
.end method

.method public setType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 5001
    iget-object v0, p0, Landroid/view/ViewRootImpl$TakenSurfaceHolder;->this$0:Landroid/view/ViewRootImpl;

    #@2
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@4
    check-cast v0, Lcom/android/internal/view/RootViewSurfaceTaker;

    #@6
    invoke-interface {v0, p1}, Lcom/android/internal/view/RootViewSurfaceTaker;->setSurfaceType(I)V

    #@9
    .line 5002
    return-void
.end method
