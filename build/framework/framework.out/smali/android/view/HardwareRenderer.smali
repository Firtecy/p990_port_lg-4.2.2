.class public abstract Landroid/view/HardwareRenderer;
.super Ljava/lang/Object;
.source "HardwareRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/HardwareRenderer$Gl20Renderer;,
        Landroid/view/HardwareRenderer$GlRenderer;,
        Landroid/view/HardwareRenderer$HardwareDrawCallbacks;
    }
.end annotation


# static fields
.field private static final CACHE_PATH_SHADERS:Ljava/lang/String; = "com.android.opengl.shaders_cache"

.field public static final DEBUG_DIRTY_REGIONS_PROPERTY:Ljava/lang/String; = "debug.hwui.show_dirty_regions"

.field public static final DEBUG_SHOW_LAYERS_UPDATES_PROPERTY:Ljava/lang/String; = "debug.hwui.show_layers_updates"

.field public static final DEBUG_SHOW_OVERDRAW_PROPERTY:Ljava/lang/String; = "debug.hwui.show_overdraw"

.field static final DISABLE_VSYNC_PROPERTY:Ljava/lang/String; = "debug.hwui.disable_vsync"

.field static final LOG_TAG:Ljava/lang/String; = "HardwareRenderer"

.field static final PRINT_CONFIG_PROPERTY:Ljava/lang/String; = "debug.hwui.print_config"

.field private static final PROFILE_FRAME_DATA_COUNT:I = 0x3

.field static final PROFILE_MAXFRAMES_PROPERTY:Ljava/lang/String; = "debug.hwui.profile.maxframes"

.field private static final PROFILE_MAX_FRAMES:I = 0x80

.field public static final PROFILE_PROPERTY:Ljava/lang/String; = "debug.hwui.profile"

.field public static final RENDER_DIRTY_REGIONS:Z = true

.field static final RENDER_DIRTY_REGIONS_PROPERTY:Ljava/lang/String; = "debug.hwui.render_dirty_regions"

.field public static sRendererDisabled:Z

.field public static sSystemRendererDisabled:Z


# instance fields
.field private mEnabled:Z

.field private mRequested:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 170
    sput-boolean v0, Landroid/view/HardwareRenderer;->sRendererDisabled:Z

    #@3
    .line 177
    sput-boolean v0, Landroid/view/HardwareRenderer;->sSystemRendererDisabled:Z

    #@5
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 190
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/view/HardwareRenderer;->mRequested:Z

    #@6
    .line 1395
    return-void
.end method

.method static synthetic access$200([I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 55
    invoke-static {p0}, Landroid/view/HardwareRenderer;->beginFrame([I)V

    #@3
    return-void
.end method

.method private static beginFrame([I)V
    .registers 1
    .parameter "size"

    #@0
    .prologue
    .line 343
    invoke-static {p0}, Landroid/view/HardwareRenderer;->nBeginFrame([I)V

    #@3
    .line 344
    return-void
.end method

.method static createGlRenderer(IZ)Landroid/view/HardwareRenderer;
    .registers 5
    .parameter "glVersion"
    .parameter "translucent"

    #@0
    .prologue
    .line 538
    packed-switch p0, :pswitch_data_22

    #@3
    .line 542
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown GL version: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 540
    :pswitch_1c
    invoke-static {p1}, Landroid/view/HardwareRenderer$Gl20Renderer;->create(Z)Landroid/view/HardwareRenderer;

    #@1f
    move-result-object v0

    #@20
    return-object v0

    #@21
    .line 538
    nop

    #@22
    :pswitch_data_22
    .packed-switch 0x2
        :pswitch_1c
    .end packed-switch
.end method

.method public static disable(Z)V
    .registers 2
    .parameter "system"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 198
    sput-boolean v0, Landroid/view/HardwareRenderer;->sRendererDisabled:Z

    #@3
    .line 199
    if-eqz p0, :cond_7

    #@5
    .line 200
    sput-boolean v0, Landroid/view/HardwareRenderer;->sSystemRendererDisabled:Z

    #@7
    .line 202
    :cond_7
    return-void
.end method

.method static disableVsync()V
    .registers 0

    #@0
    .prologue
    .line 380
    invoke-static {}, Landroid/view/HardwareRenderer;->nDisableVsync()V

    #@3
    .line 381
    return-void
.end method

.method static endTrimMemory()V
    .registers 0

    #@0
    .prologue
    .line 575
    invoke-static {}, Landroid/view/HardwareRenderer$Gl20Renderer;->endTrimMemory()V

    #@3
    .line 576
    return-void
.end method

.method public static isAvailable()Z
    .registers 1

    #@0
    .prologue
    .line 212
    invoke-static {}, Landroid/view/GLES20Canvas;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static isBackBufferPreserved()Z
    .registers 1

    #@0
    .prologue
    .line 371
    invoke-static {}, Landroid/view/HardwareRenderer;->nIsBackBufferPreserved()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static native nBeginFrame([I)V
.end method

.method private static native nDisableVsync()V
.end method

.method private static native nIsBackBufferPreserved()Z
.end method

.method private static native nPreserveBackBuffer()Z
.end method

.method private static native nSetupShadersDiskCache(Ljava/lang/String;)V
.end method

.method static preserveBackBuffer()Z
    .registers 1

    #@0
    .prologue
    .line 358
    invoke-static {}, Landroid/view/HardwareRenderer;->nPreserveBackBuffer()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static setupDiskCache(Ljava/io/File;)V
    .registers 3
    .parameter "cacheDir"

    #@0
    .prologue
    .line 333
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "com.android.opengl.shaders_cache"

    #@4
    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/view/HardwareRenderer;->nSetupShadersDiskCache(Ljava/lang/String;)V

    #@e
    .line 334
    return-void
.end method

.method static startTrimMemory(I)V
    .registers 1
    .parameter "level"

    #@0
    .prologue
    .line 567
    invoke-static {p0}, Landroid/view/HardwareRenderer$Gl20Renderer;->startTrimMemory(I)V

    #@3
    .line 568
    return-void
.end method

.method static trimMemory(I)V
    .registers 1
    .parameter "level"

    #@0
    .prologue
    .line 554
    invoke-static {p0}, Landroid/view/HardwareRenderer;->startTrimMemory(I)V

    #@3
    .line 555
    invoke-static {}, Landroid/view/HardwareRenderer;->endTrimMemory()V

    #@6
    .line 556
    return-void
.end method


# virtual methods
.method abstract attachFunctor(Landroid/view/View$AttachInfo;I)Z
.end method

.method public abstract createDisplayList(Ljava/lang/String;)Landroid/view/DisplayList;
.end method

.method abstract createHardwareLayer(IIZ)Landroid/view/HardwareLayer;
.end method

.method abstract createHardwareLayer(Z)Landroid/view/HardwareLayer;
.end method

.method abstract createSurfaceTexture(Landroid/view/HardwareLayer;)Landroid/graphics/SurfaceTexture;
.end method

.method abstract destroy(Z)V
.end method

.method abstract destroyHardwareResources(Landroid/view/View;)V
.end method

.method abstract destroyLayers(Landroid/view/View;)V
.end method

.method abstract detachFunctor(I)V
.end method

.method abstract draw(Landroid/view/View;Landroid/view/View$AttachInfo;Landroid/view/HardwareRenderer$HardwareDrawCallbacks;Landroid/graphics/Rect;)Z
.end method

.method abstract dumpGfxInfo(Ljava/io/PrintWriter;)V
.end method

.method abstract getCanvas()Landroid/view/HardwareCanvas;
.end method

.method abstract getFrameCount()J
.end method

.method abstract getHeight()I
.end method

.method abstract getWidth()I
.end method

.method abstract initialize(Landroid/view/Surface;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation
.end method

.method initializeIfNeeded(IILandroid/view/Surface;)Z
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "surface"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    .line 517
    invoke-virtual {p0}, Landroid/view/HardwareRenderer;->isRequested()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_17

    #@6
    .line 519
    invoke-virtual {p0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_17

    #@c
    .line 520
    invoke-virtual {p0, p3}, Landroid/view/HardwareRenderer;->initialize(Landroid/view/Surface;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 521
    invoke-virtual {p0, p1, p2}, Landroid/view/HardwareRenderer;->setup(II)V

    #@15
    .line 522
    const/4 v0, 0x1

    #@16
    .line 526
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method abstract invalidate(Landroid/view/Surface;)V
.end method

.method isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 584
    iget-boolean v0, p0, Landroid/view/HardwareRenderer;->mEnabled:Z

    #@2
    return v0
.end method

.method isRequested()Z
    .registers 2

    #@0
    .prologue
    .line 603
    iget-boolean v0, p0, Landroid/view/HardwareRenderer;->mRequested:Z

    #@2
    return v0
.end method

.method abstract pushLayerUpdate(Landroid/view/HardwareLayer;)V
.end method

.method abstract safelyRun(Ljava/lang/Runnable;)Z
.end method

.method setEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 593
    iput-boolean p1, p0, Landroid/view/HardwareRenderer;->mEnabled:Z

    #@2
    .line 594
    return-void
.end method

.method setRequested(Z)V
    .registers 2
    .parameter "requested"

    #@0
    .prologue
    .line 613
    iput-boolean p1, p0, Landroid/view/HardwareRenderer;->mRequested:Z

    #@2
    .line 614
    return-void
.end method

.method abstract setSurfaceTexture(Landroid/view/HardwareLayer;Landroid/graphics/SurfaceTexture;)V
.end method

.method abstract setup(II)V
.end method

.method abstract updateSurface(Landroid/view/Surface;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation
.end method

.method abstract validate()Z
.end method
