.class public final Landroid/view/MotionEvent$PointerCoords;
.super Ljava/lang/Object;
.source "MotionEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/MotionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PointerCoords"
.end annotation


# static fields
.field private static final INITIAL_PACKED_AXIS_VALUES:I = 0x8


# instance fields
.field private mPackedAxisBits:J

.field private mPackedAxisValues:[F

.field public orientation:F

.field public pressure:F

.field public size:F

.field public toolMajor:F

.field public toolMinor:F

.field public touchMajor:F

.field public touchMinor:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3280
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3281
    return-void
.end method

.method public constructor <init>(Landroid/view/MotionEvent$PointerCoords;)V
    .registers 2
    .parameter "other"

    #@0
    .prologue
    .line 3289
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3290
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent$PointerCoords;->copyFrom(Landroid/view/MotionEvent$PointerCoords;)V

    #@6
    .line 3291
    return-void
.end method

.method public static createArray(I)[Landroid/view/MotionEvent$PointerCoords;
    .registers 4
    .parameter "size"

    #@0
    .prologue
    .line 3295
    new-array v0, p0, [Landroid/view/MotionEvent$PointerCoords;

    #@2
    .line 3296
    .local v0, array:[Landroid/view/MotionEvent$PointerCoords;
    const/4 v1, 0x0

    #@3
    .local v1, i:I
    :goto_3
    if-ge v1, p0, :cond_f

    #@5
    .line 3297
    new-instance v2, Landroid/view/MotionEvent$PointerCoords;

    #@7
    invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    #@a
    aput-object v2, v0, v1

    #@c
    .line 3296
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_3

    #@f
    .line 3299
    :cond_f
    return-object v0
.end method


# virtual methods
.method public clear()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3402
    const-wide/16 v0, 0x0

    #@3
    iput-wide v0, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisBits:J

    #@5
    .line 3404
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@7
    .line 3405
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@9
    .line 3406
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@b
    .line 3407
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@d
    .line 3408
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@f
    .line 3409
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@11
    .line 3410
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@13
    .line 3411
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@15
    .line 3412
    iput v2, p0, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@17
    .line 3413
    return-void
.end method

.method public copyFrom(Landroid/view/MotionEvent$PointerCoords;)V
    .registers 10
    .parameter "other"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 3421
    iget-wide v0, p1, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisBits:J

    #@3
    .line 3422
    .local v0, bits:J
    iput-wide v0, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisBits:J

    #@5
    .line 3423
    const-wide/16 v5, 0x0

    #@7
    cmp-long v5, v0, v5

    #@9
    if-eqz v5, :cond_20

    #@b
    .line 3424
    iget-object v3, p1, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@d
    .line 3425
    .local v3, otherValues:[F
    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    #@10
    move-result v2

    #@11
    .line 3426
    .local v2, count:I
    iget-object v4, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@13
    .line 3427
    .local v4, values:[F
    if-eqz v4, :cond_18

    #@15
    array-length v5, v4

    #@16
    if-le v2, v5, :cond_1d

    #@18
    .line 3428
    :cond_18
    array-length v5, v3

    #@19
    new-array v4, v5, [F

    #@1b
    .line 3429
    iput-object v4, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@1d
    .line 3431
    :cond_1d
    invoke-static {v3, v7, v4, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@20
    .line 3434
    .end local v2           #count:I
    .end local v3           #otherValues:[F
    .end local v4           #values:[F
    :cond_20
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@22
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@24
    .line 3435
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@26
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@28
    .line 3436
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@2a
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@2c
    .line 3437
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@2e
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@30
    .line 3438
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@32
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@34
    .line 3439
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@36
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@38
    .line 3440
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@3a
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@3c
    .line 3441
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@3e
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@40
    .line 3442
    iget v5, p1, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@42
    iput v5, p0, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@44
    .line 3443
    return-void
.end method

.method public getAxisValue(I)F
    .registers 13
    .parameter "axis"

    #@0
    .prologue
    const-wide/16 v9, 0x1

    #@2
    .line 3455
    packed-switch p1, :pswitch_data_48

    #@5
    .line 3475
    if-ltz p1, :cond_b

    #@7
    const/16 v5, 0x3f

    #@9
    if-le p1, v5, :cond_2e

    #@b
    .line 3476
    :cond_b
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@d
    const-string v6, "Axis out of range."

    #@f
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v5

    #@13
    .line 3457
    :pswitch_13
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@15
    .line 3484
    :goto_15
    return v5

    #@16
    .line 3459
    :pswitch_16
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@18
    goto :goto_15

    #@19
    .line 3461
    :pswitch_19
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@1b
    goto :goto_15

    #@1c
    .line 3463
    :pswitch_1c
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@1e
    goto :goto_15

    #@1f
    .line 3465
    :pswitch_1f
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@21
    goto :goto_15

    #@22
    .line 3467
    :pswitch_22
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@24
    goto :goto_15

    #@25
    .line 3469
    :pswitch_25
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@27
    goto :goto_15

    #@28
    .line 3471
    :pswitch_28
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@2a
    goto :goto_15

    #@2b
    .line 3473
    :pswitch_2b
    iget v5, p0, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@2d
    goto :goto_15

    #@2e
    .line 3478
    :cond_2e
    iget-wide v2, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisBits:J

    #@30
    .line 3479
    .local v2, bits:J
    shl-long v0, v9, p1

    #@32
    .line 3480
    .local v0, axisBit:J
    and-long v5, v2, v0

    #@34
    const-wide/16 v7, 0x0

    #@36
    cmp-long v5, v5, v7

    #@38
    if-nez v5, :cond_3c

    #@3a
    .line 3481
    const/4 v5, 0x0

    #@3b
    goto :goto_15

    #@3c
    .line 3483
    :cond_3c
    sub-long v5, v0, v9

    #@3e
    and-long/2addr v5, v2

    #@3f
    invoke-static {v5, v6}, Ljava/lang/Long;->bitCount(J)I

    #@42
    move-result v4

    #@43
    .line 3484
    .local v4, index:I
    iget-object v5, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@45
    aget v5, v5, v4

    #@47
    goto :goto_15

    #@48
    .line 3455
    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_13
        :pswitch_16
        :pswitch_19
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
    .end packed-switch
.end method

.method public setAxisValue(IF)V
    .registers 16
    .parameter "axis"
    .parameter "value"

    #@0
    .prologue
    const-wide/16 v9, 0x1

    #@2
    const/4 v12, 0x0

    #@3
    .line 3499
    packed-switch p1, :pswitch_data_76

    #@6
    .line 3528
    if-ltz p1, :cond_c

    #@8
    const/16 v8, 0x3f

    #@a
    if-le p1, v8, :cond_2f

    #@c
    .line 3529
    :cond_c
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v9, "Axis out of range."

    #@10
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v8

    #@14
    .line 3501
    :pswitch_14
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@16
    .line 3560
    :goto_16
    return-void

    #@17
    .line 3504
    :pswitch_17
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@19
    goto :goto_16

    #@1a
    .line 3507
    :pswitch_1a
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@1c
    goto :goto_16

    #@1d
    .line 3510
    :pswitch_1d
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@1f
    goto :goto_16

    #@20
    .line 3513
    :pswitch_20
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@22
    goto :goto_16

    #@23
    .line 3516
    :pswitch_23
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@25
    goto :goto_16

    #@26
    .line 3519
    :pswitch_26
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@28
    goto :goto_16

    #@29
    .line 3522
    :pswitch_29
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@2b
    goto :goto_16

    #@2c
    .line 3525
    :pswitch_2c
    iput p2, p0, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@2e
    goto :goto_16

    #@2f
    .line 3531
    :cond_2f
    iget-wide v2, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisBits:J

    #@31
    .line 3532
    .local v2, bits:J
    shl-long v0, v9, p1

    #@33
    .line 3533
    .local v0, axisBit:J
    sub-long v8, v0, v9

    #@35
    and-long/2addr v8, v2

    #@36
    invoke-static {v8, v9}, Ljava/lang/Long;->bitCount(J)I

    #@39
    move-result v5

    #@3a
    .line 3534
    .local v5, index:I
    iget-object v7, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@3c
    .line 3535
    .local v7, values:[F
    and-long v8, v2, v0

    #@3e
    const-wide/16 v10, 0x0

    #@40
    cmp-long v8, v8, v10

    #@42
    if-nez v8, :cond_50

    #@44
    .line 3536
    if-nez v7, :cond_53

    #@46
    .line 3537
    const/16 v8, 0x8

    #@48
    new-array v7, v8, [F

    #@4a
    .line 3538
    iput-object v7, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@4c
    .line 3555
    :cond_4c
    :goto_4c
    or-long v8, v2, v0

    #@4e
    iput-wide v8, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisBits:J

    #@50
    .line 3557
    :cond_50
    aput p2, v7, v5

    #@52
    goto :goto_16

    #@53
    .line 3540
    :cond_53
    invoke-static {v2, v3}, Ljava/lang/Long;->bitCount(J)I

    #@56
    move-result v4

    #@57
    .line 3541
    .local v4, count:I
    array-length v8, v7

    #@58
    if-ge v4, v8, :cond_64

    #@5a
    .line 3542
    if-eq v5, v4, :cond_4c

    #@5c
    .line 3543
    add-int/lit8 v8, v5, 0x1

    #@5e
    sub-int v9, v4, v5

    #@60
    invoke-static {v7, v5, v7, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@63
    goto :goto_4c

    #@64
    .line 3547
    :cond_64
    mul-int/lit8 v8, v4, 0x2

    #@66
    new-array v6, v8, [F

    #@68
    .line 3548
    .local v6, newValues:[F
    invoke-static {v7, v12, v6, v12, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@6b
    .line 3549
    add-int/lit8 v8, v5, 0x1

    #@6d
    sub-int v9, v4, v5

    #@6f
    invoke-static {v7, v5, v6, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@72
    .line 3551
    move-object v7, v6

    #@73
    .line 3552
    iput-object v7, p0, Landroid/view/MotionEvent$PointerCoords;->mPackedAxisValues:[F

    #@75
    goto :goto_4c

    #@76
    .line 3499
    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
        :pswitch_26
        :pswitch_29
        :pswitch_2c
    .end packed-switch
.end method
