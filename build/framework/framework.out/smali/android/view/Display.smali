.class public final Landroid/view/Display;
.super Ljava/lang/Object;
.source "Display.java"


# static fields
.field private static final CACHED_APP_SIZE_DURATION_MILLIS:I = 0x14

.field private static final DEBUG:Z = false

.field public static final DEFAULT_DISPLAY:I = 0x0

.field public static final FLAG_SECURE:I = 0x2

.field public static final FLAG_SUPPORTS_PROTECTED_BUFFERS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Display"

.field public static final TYPE_BUILT_IN:I = 0x1

.field public static final TYPE_HDMI:I = 0x2

.field public static final TYPE_OVERLAY:I = 0x4

.field public static final TYPE_UNKNOWN:I = 0x0

.field public static final TYPE_WIFI:I = 0x3

.field public static final dsdrCodeEnabled:Z = true

.field private static final sStaticInit:Ljava/lang/Object;

.field private static sWindowManager:Landroid/view/IWindowManager;


# instance fields
.field private final mAddress:Ljava/lang/String;

.field private mCachedAppHeightCompat:I

.field private mCachedAppWidthCompat:I

.field private final mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

.field private final mDisplayId:I

.field private mDisplayInfo:Landroid/view/DisplayInfo;

.field private final mFlags:I

.field private final mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

.field private mIsValid:Z

.field private mLastCachedAppSizeUpdate:J

.field private final mLayerStack:I

.field private final mTempMetrics:Landroid/util/DisplayMetrics;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 635
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/Display;->sStaticInit:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/hardware/display/DisplayManagerGlobal;ILandroid/view/DisplayInfo;Landroid/view/CompatibilityInfoHolder;)V
    .registers 6
    .parameter "global"
    .parameter "displayId"
    .parameter "displayInfo"
    .parameter "compatibilityInfo"

    #@0
    .prologue
    .line 199
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    new-instance v0, Landroid/util/DisplayMetrics;

    #@5
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@a
    .line 200
    iput-object p1, p0, Landroid/view/Display;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@c
    .line 201
    iput p2, p0, Landroid/view/Display;->mDisplayId:I

    #@e
    .line 202
    iput-object p3, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@10
    .line 203
    iput-object p4, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@12
    .line 204
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Landroid/view/Display;->mIsValid:Z

    #@15
    .line 207
    iget v0, p3, Landroid/view/DisplayInfo;->layerStack:I

    #@17
    iput v0, p0, Landroid/view/Display;->mLayerStack:I

    #@19
    .line 208
    iget v0, p3, Landroid/view/DisplayInfo;->flags:I

    #@1b
    iput v0, p0, Landroid/view/Display;->mFlags:I

    #@1d
    .line 209
    iget v0, p3, Landroid/view/DisplayInfo;->type:I

    #@1f
    iput v0, p0, Landroid/view/Display;->mType:I

    #@21
    .line 210
    iget-object v0, p3, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@23
    iput-object v0, p0, Landroid/view/Display;->mAddress:Ljava/lang/String;

    #@25
    .line 211
    return-void
.end method

.method static getWindowManager()Landroid/view/IWindowManager;
    .registers 2

    #@0
    .prologue
    .line 638
    sget-object v1, Landroid/view/Display;->sStaticInit:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 639
    :try_start_3
    sget-object v0, Landroid/view/Display;->sWindowManager:Landroid/view/IWindowManager;

    #@5
    if-nez v0, :cond_14

    #@7
    .line 640
    const-string/jumbo v0, "window"

    #@a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/view/Display;->sWindowManager:Landroid/view/IWindowManager;

    #@14
    .line 642
    :cond_14
    sget-object v0, Landroid/view/Display;->sWindowManager:Landroid/view/IWindowManager;

    #@16
    monitor-exit v1

    #@17
    return-object v0

    #@18
    .line 643
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public static typeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 616
    packed-switch p0, :pswitch_data_18

    #@3
    .line 628
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 618
    :pswitch_8
    const-string v0, "UNKNOWN"

    #@a
    goto :goto_7

    #@b
    .line 620
    :pswitch_b
    const-string v0, "BUILT_IN"

    #@d
    goto :goto_7

    #@e
    .line 622
    :pswitch_e
    const-string v0, "HDMI"

    #@10
    goto :goto_7

    #@11
    .line 624
    :pswitch_11
    const-string v0, "WIFI"

    #@13
    goto :goto_7

    #@14
    .line 626
    :pswitch_14
    const-string v0, "OVERLAY"

    #@16
    goto :goto_7

    #@17
    .line 616
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
    .end packed-switch
.end method

.method private updateCachedAppSizeIfNeededLocked()V
    .registers 7

    #@0
    .prologue
    .line 591
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    .line 592
    .local v0, now:J
    iget-wide v2, p0, Landroid/view/Display;->mLastCachedAppSizeUpdate:J

    #@6
    const-wide/16 v4, 0x14

    #@8
    add-long/2addr v2, v4

    #@9
    cmp-long v2, v0, v2

    #@b
    if-lez v2, :cond_27

    #@d
    .line 593
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@10
    .line 594
    iget-object v2, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@12
    iget-object v3, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@14
    iget-object v4, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@16
    invoke-virtual {v2, v3, v4}, Landroid/view/DisplayInfo;->getAppMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V

    #@19
    .line 595
    iget-object v2, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@1b
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1d
    iput v2, p0, Landroid/view/Display;->mCachedAppWidthCompat:I

    #@1f
    .line 596
    iget-object v2, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@21
    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    #@23
    iput v2, p0, Landroid/view/Display;->mCachedAppHeightCompat:I

    #@25
    .line 597
    iput-wide v0, p0, Landroid/view/Display;->mLastCachedAppSizeUpdate:J

    #@27
    .line 599
    :cond_27
    return-void
.end method

.method private updateDisplayInfoLocked()V
    .registers 4

    #@0
    .prologue
    .line 569
    iget-object v1, p0, Landroid/view/Display;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    iget v2, p0, Landroid/view/Display;->mDisplayId:I

    #@4
    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@7
    move-result-object v0

    #@8
    .line 570
    .local v0, newInfo:Landroid/view/DisplayInfo;
    if-nez v0, :cond_12

    #@a
    .line 572
    iget-boolean v1, p0, Landroid/view/Display;->mIsValid:Z

    #@c
    if-eqz v1, :cond_11

    #@e
    .line 573
    const/4 v1, 0x0

    #@f
    iput-boolean v1, p0, Landroid/view/Display;->mIsValid:Z

    #@11
    .line 588
    :cond_11
    :goto_11
    return-void

    #@12
    .line 580
    :cond_12
    iput-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@14
    .line 581
    iget-boolean v1, p0, Landroid/view/Display;->mIsValid:Z

    #@16
    if-nez v1, :cond_11

    #@18
    .line 582
    const/4 v1, 0x1

    #@19
    iput-boolean v1, p0, Landroid/view/Display;->mIsValid:Z

    #@1b
    goto :goto_11
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 308
    iget-object v0, p0, Landroid/view/Display;->mAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCompatibilityInfo()Landroid/view/CompatibilityInfoHolder;
    .registers 2

    #@0
    .prologue
    .line 318
    iget-object v0, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@2
    return-object v0
.end method

.method public getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .registers 4
    .parameter "outSmallestSize"
    .parameter "outLargestSize"

    #@0
    .prologue
    .line 410
    monitor-enter p0

    #@1
    .line 411
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 412
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget v0, v0, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@8
    iput v0, p1, Landroid/graphics/Point;->x:I

    #@a
    .line 413
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@c
    iget v0, v0, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@e
    iput v0, p1, Landroid/graphics/Point;->y:I

    #@10
    .line 414
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@12
    iget v0, v0, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@14
    iput v0, p2, Landroid/graphics/Point;->x:I

    #@16
    .line 415
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@18
    iget v0, v0, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@1a
    iput v0, p2, Landroid/graphics/Point;->y:I

    #@1c
    .line 416
    monitor-exit p0

    #@1d
    .line 417
    return-void

    #@1e
    .line 416
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method

.method public getDisplayId()I
    .registers 2

    #@0
    .prologue
    .line 221
    iget v0, p0, Landroid/view/Display;->mDisplayId:I

    #@2
    return v0
.end method

.method public getDisplayInfo(Landroid/view/DisplayInfo;)Z
    .registers 3
    .parameter "outDisplayInfo"

    #@0
    .prologue
    .line 252
    monitor-enter p0

    #@1
    .line 253
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 254
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    invoke-virtual {p1, v0}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@9
    .line 255
    iget-boolean v0, p0, Landroid/view/Display;->mIsValid:Z

    #@b
    monitor-exit p0

    #@c
    return v0

    #@d
    .line 256
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getDsdrExternalHeight()I
    .registers 5

    #@0
    .prologue
    .line 766
    const/4 v1, 0x0

    #@1
    .line 769
    .local v1, h:I
    :try_start_1
    invoke-static {}, Landroid/view/Display;->getWindowManager()Landroid/view/IWindowManager;

    #@4
    move-result-object v2

    #@5
    .line 770
    .local v2, wm:Landroid/view/IWindowManager;
    if-eqz v2, :cond_c

    #@7
    .line 771
    const/4 v3, 0x0

    #@8
    invoke-interface {v2, v3}, Landroid/view/IWindowManager;->getDsdrExtWidthHeight(Z)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 775
    .end local v2           #wm:Landroid/view/IWindowManager;
    :cond_c
    :goto_c
    return v1

    #@d
    .line 773
    :catch_d
    move-exception v0

    #@e
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@11
    goto :goto_c
.end method

.method public getDsdrExternalWidth()I
    .registers 5

    #@0
    .prologue
    .line 748
    const/4 v1, 0x0

    #@1
    .line 751
    .local v1, w:I
    :try_start_1
    invoke-static {}, Landroid/view/Display;->getWindowManager()Landroid/view/IWindowManager;

    #@4
    move-result-object v2

    #@5
    .line 752
    .local v2, wm:Landroid/view/IWindowManager;
    if-eqz v2, :cond_c

    #@7
    .line 753
    const/4 v3, 0x1

    #@8
    invoke-interface {v2, v3}, Landroid/view/IWindowManager;->getDsdrExtWidthHeight(Z)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 757
    .end local v2           #wm:Landroid/view/IWindowManager;
    :cond_c
    :goto_c
    return v1

    #@d
    .line 755
    :catch_d
    move-exception v0

    #@e
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@11
    goto :goto_c
.end method

.method public getDsdrStatus()I
    .registers 4

    #@0
    .prologue
    .line 711
    const/4 v1, 0x0

    #@1
    .line 714
    .local v1, rv:I
    :try_start_1
    invoke-static {}, Landroid/view/Display;->getWindowManager()Landroid/view/IWindowManager;

    #@4
    move-result-object v2

    #@5
    .line 715
    .local v2, wm:Landroid/view/IWindowManager;
    if-eqz v2, :cond_b

    #@7
    .line 716
    invoke-interface {v2}, Landroid/view/IWindowManager;->getDsdrStatus()I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 723
    .end local v2           #wm:Landroid/view/IWindowManager;
    :cond_b
    :goto_b
    return v1

    #@c
    .line 721
    :catch_c
    move-exception v0

    #@d
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@10
    goto :goto_b
.end method

.method public getFlags()I
    .registers 2

    #@0
    .prologue
    .line 281
    iget v0, p0, Landroid/view/Display;->mFlags:I

    #@2
    return v0
.end method

.method public getHeight()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 447
    monitor-enter p0

    #@1
    .line 448
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateCachedAppSizeIfNeededLocked()V

    #@4
    .line 449
    iget v0, p0, Landroid/view/Display;->mCachedAppHeightCompat:I

    #@6
    monitor-exit p0

    #@7
    return v0

    #@8
    .line 450
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public getLayerStack()I
    .registers 2

    #@0
    .prologue
    .line 269
    iget v0, p0, Landroid/view/Display;->mLayerStack:I

    #@2
    return v0
.end method

.method public getMaximumSizeDimension()I
    .registers 3

    #@0
    .prologue
    .line 425
    monitor-enter p0

    #@1
    .line 426
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 427
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget v0, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@8
    iget-object v1, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@a
    iget v1, v1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@f
    move-result v0

    #@10
    monitor-exit p0

    #@11
    return v0

    #@12
    .line 428
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public getMetrics(Landroid/util/DisplayMetrics;)V
    .registers 4
    .parameter "outMetrics"

    #@0
    .prologue
    .line 523
    monitor-enter p0

    #@1
    .line 524
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 525
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget-object v1, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@8
    invoke-virtual {v0, p1, v1}, Landroid/view/DisplayInfo;->getAppMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V

    #@b
    .line 526
    monitor-exit p0

    #@c
    .line 527
    return-void

    #@d
    .line 526
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 330
    monitor-enter p0

    #@1
    .line 331
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 332
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget-object v0, v0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@8
    monitor-exit p0

    #@9
    return-object v0

    #@a
    .line 333
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public getOrientation()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 483
    invoke-virtual {p0}, Landroid/view/Display;->getRotation()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getPixelFormat()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 495
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getRealMetrics(Landroid/util/DisplayMetrics;)V
    .registers 4
    .parameter "outMetrics"

    #@0
    .prologue
    .line 561
    monitor-enter p0

    #@1
    .line 562
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 563
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, p1, v1}, Landroid/view/DisplayInfo;->getLogicalMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V

    #@a
    .line 564
    monitor-exit p0

    #@b
    .line 565
    return-void

    #@c
    .line 564
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public getRealSize(Landroid/graphics/Point;)V
    .registers 3
    .parameter "outSize"

    #@0
    .prologue
    .line 542
    monitor-enter p0

    #@1
    .line 543
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 544
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget v0, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@8
    iput v0, p1, Landroid/graphics/Point;->x:I

    #@a
    .line 545
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@c
    iget v0, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@e
    iput v0, p1, Landroid/graphics/Point;->y:I

    #@10
    .line 546
    monitor-exit p0

    #@11
    .line 547
    return-void

    #@12
    .line 546
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public getRectSize(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "outSize"

    #@0
    .prologue
    .line 372
    monitor-enter p0

    #@1
    .line 373
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 374
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget-object v1, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@8
    iget-object v2, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/view/DisplayInfo;->getAppMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V

    #@d
    .line 375
    const/4 v0, 0x0

    #@e
    const/4 v1, 0x0

    #@f
    iget-object v2, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@11
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@13
    iget-object v3, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@15
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    #@17
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@1a
    .line 376
    monitor-exit p0

    #@1b
    .line 377
    return-void

    #@1c
    .line 376
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public getRefreshRate()F
    .registers 2

    #@0
    .prologue
    .line 502
    monitor-enter p0

    #@1
    .line 503
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 504
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget v0, v0, Landroid/view/DisplayInfo;->refreshRate:F

    #@8
    monitor-exit p0

    #@9
    return v0

    #@a
    .line 505
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public getRotation()I
    .registers 2

    #@0
    .prologue
    .line 471
    monitor-enter p0

    #@1
    .line 472
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 473
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget v0, v0, Landroid/view/DisplayInfo;->rotation:I

    #@8
    monitor-exit p0

    #@9
    return v0

    #@a
    .line 474
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public getSize(Landroid/graphics/Point;)V
    .registers 5
    .parameter "outSize"

    #@0
    .prologue
    .line 357
    monitor-enter p0

    #@1
    .line 358
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 359
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget-object v1, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@8
    iget-object v2, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/view/DisplayInfo;->getAppMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V

    #@d
    .line 360
    iget-object v0, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@f
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@11
    iput v0, p1, Landroid/graphics/Point;->x:I

    #@13
    .line 361
    iget-object v0, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@15
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    #@17
    iput v0, p1, Landroid/graphics/Point;->y:I

    #@19
    .line 362
    monitor-exit p0

    #@1a
    .line 363
    return-void

    #@1b
    .line 362
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 297
    iget v0, p0, Landroid/view/Display;->mType:I

    #@2
    return v0
.end method

.method public getVisibleStatus()I
    .registers 4

    #@0
    .prologue
    .line 730
    const/4 v1, 0x0

    #@1
    .line 733
    .local v1, rv:I
    :try_start_1
    invoke-static {}, Landroid/view/Display;->getWindowManager()Landroid/view/IWindowManager;

    #@4
    move-result-object v2

    #@5
    .line 734
    .local v2, wm:Landroid/view/IWindowManager;
    if-eqz v2, :cond_b

    #@7
    .line 735
    invoke-interface {v2}, Landroid/view/IWindowManager;->getVisibleStatus()I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 739
    .end local v2           #wm:Landroid/view/IWindowManager;
    :cond_b
    :goto_b
    return v1

    #@c
    .line 737
    :catch_c
    move-exception v0

    #@d
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@10
    goto :goto_b
.end method

.method public getWidth()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 436
    monitor-enter p0

    #@1
    .line 437
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateCachedAppSizeIfNeededLocked()V

    #@4
    .line 438
    iget v0, p0, Landroid/view/Display;->mCachedAppWidthCompat:I

    #@6
    monitor-exit p0

    #@7
    return v0

    #@8
    .line 439
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public isValid()Z
    .registers 2

    #@0
    .prologue
    .line 238
    monitor-enter p0

    #@1
    .line 239
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 240
    iget-boolean v0, p0, Landroid/view/Display;->mIsValid:Z

    #@6
    monitor-exit p0

    #@7
    return v0

    #@8
    .line 241
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public setDsdrActivated(I)I
    .registers 14
    .parameter "enable"

    #@0
    .prologue
    .line 658
    const/4 v3, -0x1

    #@1
    .line 659
    .local v3, ERROR_NOT_SUPPORTED:I
    const/4 v2, -0x1

    #@2
    .line 660
    .local v2, ERROR_NOT_PERMITTED:I
    const/4 v4, 0x0

    #@3
    .line 661
    .local v4, NOT_READY:I
    const/4 v1, 0x1

    #@4
    .line 662
    .local v1, ALREADY_ACTIVATED:I
    const/4 v0, 0x1

    #@5
    .line 663
    .local v0, ACTIVATING:I
    const/4 v7, 0x0

    #@6
    .line 664
    .local v7, status:I
    const/4 v6, -0x1

    #@7
    .line 668
    .local v6, rv:I
    invoke-virtual {p0}, Landroid/view/Display;->getDsdrStatus()I

    #@a
    move-result v7

    #@b
    .line 669
    const-string v9, "Display"

    #@d
    new-instance v10, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v11, "[DSDR][Display.java]setDsdrActivated() : status = "

    #@14
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v10

    #@18
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v10

    #@1c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v10

    #@20
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 670
    packed-switch p1, :pswitch_data_ba

    #@26
    .line 695
    const-string v9, "Display"

    #@28
    new-instance v10, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v11, "[DSDR] setDsdrActivated("

    #@2f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v10

    #@33
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v10

    #@37
    const-string v11, "): ERROR_NOT_PERMITTED (rv = -1)"

    #@39
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v10

    #@3d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v10

    #@41
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 696
    const/4 v6, -0x1

    #@45
    .line 702
    :goto_45
    return v6

    #@46
    .line 672
    :pswitch_46
    const/4 v9, -0x1

    #@47
    if-ne v7, v9, :cond_69

    #@49
    .line 673
    const-string v9, "Display"

    #@4b
    new-instance v10, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v11, "[DSDR][Display.java]setDsdrActivated("

    #@52
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v10

    #@56
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v10

    #@5a
    const-string v11, "): NOT_READY (rv = -1)"

    #@5c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v10

    #@60
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v10

    #@64
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 674
    const/4 v6, 0x0

    #@68
    .line 675
    goto :goto_45

    #@69
    .line 677
    :cond_69
    const/4 v9, 0x2

    #@6a
    if-lt v7, v9, :cond_8c

    #@6c
    .line 678
    const-string v9, "Display"

    #@6e
    new-instance v10, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v11, "[DSDR][Display.java]setDsdrActivated("

    #@75
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v10

    #@79
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v10

    #@7d
    const-string v11, "): ALREADY_ACTIVATED (rv = 1)"

    #@7f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v10

    #@83
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v10

    #@87
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 679
    const/4 v6, 0x1

    #@8b
    .line 680
    goto :goto_45

    #@8c
    .line 685
    :cond_8c
    :try_start_8c
    invoke-static {}, Landroid/view/Display;->getWindowManager()Landroid/view/IWindowManager;

    #@8f
    move-result-object v8

    #@90
    .line 686
    .local v8, wm:Landroid/view/IWindowManager;
    if-eqz v8, :cond_95

    #@92
    .line 687
    invoke-interface {v8}, Landroid/view/IWindowManager;->setDsdrActivated()V
    :try_end_95
    .catch Landroid/os/RemoteException; {:try_start_8c .. :try_end_95} :catch_b5

    #@95
    .line 690
    .end local v8           #wm:Landroid/view/IWindowManager;
    :cond_95
    :goto_95
    const-string v9, "Display"

    #@97
    new-instance v10, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v11, "[DSDR][Display.java]setDsdrActivated("

    #@9e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v10

    #@a2
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v10

    #@a6
    const-string v11, "): ACTIVATING (rv = 1)"

    #@a8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v10

    #@ac
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v10

    #@b0
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 691
    const/4 v6, 0x1

    #@b4
    .line 692
    goto :goto_45

    #@b5
    .line 689
    :catch_b5
    move-exception v5

    #@b6
    .local v5, e:Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    #@b9
    goto :goto_95

    #@ba
    .line 670
    :pswitch_data_ba
    .packed-switch 0x1
        :pswitch_46
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 604
    monitor-enter p0

    #@1
    .line 605
    :try_start_1
    invoke-direct {p0}, Landroid/view/Display;->updateDisplayInfoLocked()V

    #@4
    .line 606
    iget-object v0, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@6
    iget-object v1, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@8
    iget-object v2, p0, Landroid/view/Display;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/view/DisplayInfo;->getAppMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V

    #@d
    .line 607
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v1, "Display id "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget v1, p0, Landroid/view/Display;->mDisplayId:I

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, ": "

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    iget-object v1, p0, Landroid/view/Display;->mDisplayInfo:Landroid/view/DisplayInfo;

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string v1, ", "

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget-object v1, p0, Landroid/view/Display;->mTempMetrics:Landroid/util/DisplayMetrics;

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v1, ", isValid="

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    iget-boolean v1, p0, Landroid/view/Display;->mIsValid:Z

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    monitor-exit p0

    #@47
    return-object v0

    #@48
    .line 609
    :catchall_48
    move-exception v0

    #@49
    monitor-exit p0
    :try_end_4a
    .catchall {:try_start_1 .. :try_end_4a} :catchall_48

    #@4a
    throw v0
.end method
