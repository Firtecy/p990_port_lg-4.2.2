.class public abstract Landroid/view/IWindow$Stub;
.super Landroid/os/Binder;
.source "IWindow.java"

# interfaces
.implements Landroid/view/IWindow;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/IWindow$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.IWindow"

.field static final TRANSACTION_closeSystemDialogs:I = 0x8

.field static final TRANSACTION_dispatchAppVisibility:I = 0x4

.field static final TRANSACTION_dispatchDragEvent:I = 0xb

.field static final TRANSACTION_dispatchGetNewSurface:I = 0x5

.field static final TRANSACTION_dispatchScreenState:I = 0x6

.field static final TRANSACTION_dispatchSystemUiVisibilityChanged:I = 0xc

.field static final TRANSACTION_dispatchWallpaperCommand:I = 0xa

.field static final TRANSACTION_dispatchWallpaperOffsets:I = 0x9

.field static final TRANSACTION_doneAnimating:I = 0xd

.field static final TRANSACTION_executeCommand:I = 0x1

.field static final TRANSACTION_moved:I = 0x3

.field static final TRANSACTION_resized:I = 0x2

.field static final TRANSACTION_windowFocusChanged:I = 0x7


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.view.IWindow"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/IWindow$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/IWindow;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/view/IWindow;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/view/IWindow$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/IWindow$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 44
    sparse-switch p1, :sswitch_data_186

    #@5
    .line 227
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v7

    #@9
    :goto_9
    return v7

    #@a
    .line 48
    :sswitch_a
    const-string v0, "android.view.IWindow"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 53
    :sswitch_10
    const-string v0, "android.view.IWindow"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 57
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    .line 59
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_2f

    #@23
    .line 60
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@25
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@28
    move-result-object v3

    #@29
    check-cast v3, Landroid/os/ParcelFileDescriptor;

    #@2b
    .line 65
    .local v3, _arg2:Landroid/os/ParcelFileDescriptor;
    :goto_2b
    invoke-virtual {p0, v1, v2, v3}, Landroid/view/IWindow$Stub;->executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    #@2e
    goto :goto_9

    #@2f
    .line 63
    .end local v3           #_arg2:Landroid/os/ParcelFileDescriptor;
    :cond_2f
    const/4 v3, 0x0

    #@30
    .restart local v3       #_arg2:Landroid/os/ParcelFileDescriptor;
    goto :goto_2b

    #@31
    .line 70
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Landroid/os/ParcelFileDescriptor;
    :sswitch_31
    const-string v8, "android.view.IWindow"

    #@33
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v8

    #@3a
    if-eqz v8, :cond_7a

    #@3c
    .line 73
    sget-object v8, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3e
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@41
    move-result-object v1

    #@42
    check-cast v1, Landroid/graphics/Rect;

    #@44
    .line 79
    .local v1, _arg0:Landroid/graphics/Rect;
    :goto_44
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v8

    #@48
    if-eqz v8, :cond_7c

    #@4a
    .line 80
    sget-object v8, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4c
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4f
    move-result-object v2

    #@50
    check-cast v2, Landroid/graphics/Rect;

    #@52
    .line 86
    .local v2, _arg1:Landroid/graphics/Rect;
    :goto_52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v8

    #@56
    if-eqz v8, :cond_7e

    #@58
    .line 87
    sget-object v8, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5a
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5d
    move-result-object v3

    #@5e
    check-cast v3, Landroid/graphics/Rect;

    #@60
    .line 93
    .local v3, _arg2:Landroid/graphics/Rect;
    :goto_60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v8

    #@64
    if-eqz v8, :cond_80

    #@66
    move v4, v7

    #@67
    .line 95
    .local v4, _arg3:Z
    :goto_67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6a
    move-result v0

    #@6b
    if-eqz v0, :cond_82

    #@6d
    .line 96
    sget-object v0, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6f
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@72
    move-result-object v5

    #@73
    check-cast v5, Landroid/content/res/Configuration;

    #@75
    .local v5, _arg4:Landroid/content/res/Configuration;
    :goto_75
    move-object v0, p0

    #@76
    .line 101
    invoke-virtual/range {v0 .. v5}, Landroid/view/IWindow$Stub;->resized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V

    #@79
    goto :goto_9

    #@7a
    .line 76
    .end local v1           #_arg0:Landroid/graphics/Rect;
    .end local v2           #_arg1:Landroid/graphics/Rect;
    .end local v3           #_arg2:Landroid/graphics/Rect;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Landroid/content/res/Configuration;
    :cond_7a
    const/4 v1, 0x0

    #@7b
    .restart local v1       #_arg0:Landroid/graphics/Rect;
    goto :goto_44

    #@7c
    .line 83
    :cond_7c
    const/4 v2, 0x0

    #@7d
    .restart local v2       #_arg1:Landroid/graphics/Rect;
    goto :goto_52

    #@7e
    .line 90
    :cond_7e
    const/4 v3, 0x0

    #@7f
    .restart local v3       #_arg2:Landroid/graphics/Rect;
    goto :goto_60

    #@80
    :cond_80
    move v4, v0

    #@81
    .line 93
    goto :goto_67

    #@82
    .line 99
    .restart local v4       #_arg3:Z
    :cond_82
    const/4 v5, 0x0

    #@83
    .restart local v5       #_arg4:Landroid/content/res/Configuration;
    goto :goto_75

    #@84
    .line 106
    .end local v1           #_arg0:Landroid/graphics/Rect;
    .end local v2           #_arg1:Landroid/graphics/Rect;
    .end local v3           #_arg2:Landroid/graphics/Rect;
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Landroid/content/res/Configuration;
    :sswitch_84
    const-string v0, "android.view.IWindow"

    #@86
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@89
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8c
    move-result v1

    #@8d
    .line 110
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@90
    move-result v2

    #@91
    .line 111
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/view/IWindow$Stub;->moved(II)V

    #@94
    goto/16 :goto_9

    #@96
    .line 116
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_96
    const-string v8, "android.view.IWindow"

    #@98
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9e
    move-result v8

    #@9f
    if-eqz v8, :cond_a7

    #@a1
    move v1, v7

    #@a2
    .line 119
    .local v1, _arg0:Z
    :goto_a2
    invoke-virtual {p0, v1}, Landroid/view/IWindow$Stub;->dispatchAppVisibility(Z)V

    #@a5
    goto/16 :goto_9

    #@a7
    .end local v1           #_arg0:Z
    :cond_a7
    move v1, v0

    #@a8
    .line 118
    goto :goto_a2

    #@a9
    .line 124
    :sswitch_a9
    const-string v0, "android.view.IWindow"

    #@ab
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 125
    invoke-virtual {p0}, Landroid/view/IWindow$Stub;->dispatchGetNewSurface()V

    #@b1
    goto/16 :goto_9

    #@b3
    .line 130
    :sswitch_b3
    const-string v8, "android.view.IWindow"

    #@b5
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b8
    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@bb
    move-result v8

    #@bc
    if-eqz v8, :cond_c4

    #@be
    move v1, v7

    #@bf
    .line 133
    .restart local v1       #_arg0:Z
    :goto_bf
    invoke-virtual {p0, v1}, Landroid/view/IWindow$Stub;->dispatchScreenState(Z)V

    #@c2
    goto/16 :goto_9

    #@c4
    .end local v1           #_arg0:Z
    :cond_c4
    move v1, v0

    #@c5
    .line 132
    goto :goto_bf

    #@c6
    .line 138
    :sswitch_c6
    const-string v8, "android.view.IWindow"

    #@c8
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cb
    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ce
    move-result v8

    #@cf
    if-eqz v8, :cond_de

    #@d1
    move v1, v7

    #@d2
    .line 142
    .restart local v1       #_arg0:Z
    :goto_d2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@d5
    move-result v8

    #@d6
    if-eqz v8, :cond_e0

    #@d8
    move v2, v7

    #@d9
    .line 143
    .local v2, _arg1:Z
    :goto_d9
    invoke-virtual {p0, v1, v2}, Landroid/view/IWindow$Stub;->windowFocusChanged(ZZ)V

    #@dc
    goto/16 :goto_9

    #@de
    .end local v1           #_arg0:Z
    .end local v2           #_arg1:Z
    :cond_de
    move v1, v0

    #@df
    .line 140
    goto :goto_d2

    #@e0
    .restart local v1       #_arg0:Z
    :cond_e0
    move v2, v0

    #@e1
    .line 142
    goto :goto_d9

    #@e2
    .line 148
    .end local v1           #_arg0:Z
    :sswitch_e2
    const-string v0, "android.view.IWindow"

    #@e4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e7
    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ea
    move-result-object v1

    #@eb
    .line 151
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/view/IWindow$Stub;->closeSystemDialogs(Ljava/lang/String;)V

    #@ee
    goto/16 :goto_9

    #@f0
    .line 156
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_f0
    const-string v8, "android.view.IWindow"

    #@f2
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f5
    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@f8
    move-result v1

    #@f9
    .line 160
    .local v1, _arg0:F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@fc
    move-result v2

    #@fd
    .line 162
    .local v2, _arg1:F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@100
    move-result v3

    #@101
    .line 164
    .local v3, _arg2:F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@104
    move-result v4

    #@105
    .line 166
    .local v4, _arg3:F
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@108
    move-result v8

    #@109
    if-eqz v8, :cond_112

    #@10b
    move v5, v7

    #@10c
    .local v5, _arg4:Z
    :goto_10c
    move-object v0, p0

    #@10d
    .line 167
    invoke-virtual/range {v0 .. v5}, Landroid/view/IWindow$Stub;->dispatchWallpaperOffsets(FFFFZ)V

    #@110
    goto/16 :goto_9

    #@112
    .end local v5           #_arg4:Z
    :cond_112
    move v5, v0

    #@113
    .line 166
    goto :goto_10c

    #@114
    .line 172
    .end local v1           #_arg0:F
    .end local v2           #_arg1:F
    .end local v3           #_arg2:F
    .end local v4           #_arg3:F
    :sswitch_114
    const-string v8, "android.view.IWindow"

    #@116
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@119
    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11c
    move-result-object v1

    #@11d
    .line 176
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@120
    move-result v2

    #@121
    .line 178
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@124
    move-result v3

    #@125
    .line 180
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@128
    move-result v4

    #@129
    .line 182
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12c
    move-result v8

    #@12d
    if-eqz v8, :cond_144

    #@12f
    .line 183
    sget-object v8, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@131
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@134
    move-result-object v5

    #@135
    check-cast v5, Landroid/os/Bundle;

    #@137
    .line 189
    .local v5, _arg4:Landroid/os/Bundle;
    :goto_137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13a
    move-result v8

    #@13b
    if-eqz v8, :cond_146

    #@13d
    move v6, v7

    #@13e
    .local v6, _arg5:Z
    :goto_13e
    move-object v0, p0

    #@13f
    .line 190
    invoke-virtual/range {v0 .. v6}, Landroid/view/IWindow$Stub;->dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)V

    #@142
    goto/16 :goto_9

    #@144
    .line 186
    .end local v5           #_arg4:Landroid/os/Bundle;
    .end local v6           #_arg5:Z
    :cond_144
    const/4 v5, 0x0

    #@145
    .restart local v5       #_arg4:Landroid/os/Bundle;
    goto :goto_137

    #@146
    :cond_146
    move v6, v0

    #@147
    .line 189
    goto :goto_13e

    #@148
    .line 195
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Landroid/os/Bundle;
    :sswitch_148
    const-string v0, "android.view.IWindow"

    #@14a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14d
    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@150
    move-result v0

    #@151
    if-eqz v0, :cond_160

    #@153
    .line 198
    sget-object v0, Landroid/view/DragEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@155
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@158
    move-result-object v1

    #@159
    check-cast v1, Landroid/view/DragEvent;

    #@15b
    .line 203
    .local v1, _arg0:Landroid/view/DragEvent;
    :goto_15b
    invoke-virtual {p0, v1}, Landroid/view/IWindow$Stub;->dispatchDragEvent(Landroid/view/DragEvent;)V

    #@15e
    goto/16 :goto_9

    #@160
    .line 201
    .end local v1           #_arg0:Landroid/view/DragEvent;
    :cond_160
    const/4 v1, 0x0

    #@161
    .restart local v1       #_arg0:Landroid/view/DragEvent;
    goto :goto_15b

    #@162
    .line 208
    .end local v1           #_arg0:Landroid/view/DragEvent;
    :sswitch_162
    const-string v0, "android.view.IWindow"

    #@164
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@167
    .line 210
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16a
    move-result v1

    #@16b
    .line 212
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16e
    move-result v2

    #@16f
    .line 214
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@172
    move-result v3

    #@173
    .line 216
    .restart local v3       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@176
    move-result v4

    #@177
    .line 217
    .restart local v4       #_arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/view/IWindow$Stub;->dispatchSystemUiVisibilityChanged(IIII)V

    #@17a
    goto/16 :goto_9

    #@17c
    .line 222
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    :sswitch_17c
    const-string v0, "android.view.IWindow"

    #@17e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@181
    .line 223
    invoke-virtual {p0}, Landroid/view/IWindow$Stub;->doneAnimating()V

    #@184
    goto/16 :goto_9

    #@186
    .line 44
    :sswitch_data_186
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_31
        0x3 -> :sswitch_84
        0x4 -> :sswitch_96
        0x5 -> :sswitch_a9
        0x6 -> :sswitch_b3
        0x7 -> :sswitch_c6
        0x8 -> :sswitch_e2
        0x9 -> :sswitch_f0
        0xa -> :sswitch_114
        0xb -> :sswitch_148
        0xc -> :sswitch_162
        0xd -> :sswitch_17c
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
