.class public Landroid/view/ViewDebug;
.super Ljava/lang/Object;
.source "ViewDebug.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ViewDebug$ViewOperation;,
        Landroid/view/ViewDebug$RecyclerTraceType;,
        Landroid/view/ViewDebug$HierarchyTraceType;,
        Landroid/view/ViewDebug$HierarchyHandler;,
        Landroid/view/ViewDebug$CapturedViewProperty;,
        Landroid/view/ViewDebug$FlagToString;,
        Landroid/view/ViewDebug$IntToString;,
        Landroid/view/ViewDebug$ExportedProperty;
    }
.end annotation


# static fields
.field private static final CAPTURE_TIMEOUT:I = 0xfa0

.field public static final DEBUG_DRAG:Z = false

.field private static final REMOTE_COMMAND_CAPTURE:Ljava/lang/String; = "CAPTURE"

.field private static final REMOTE_COMMAND_CAPTURE_LAYERS:Ljava/lang/String; = "CAPTURE_LAYERS"

.field private static final REMOTE_COMMAND_DUMP:Ljava/lang/String; = "DUMP"

.field private static final REMOTE_COMMAND_INVALIDATE:Ljava/lang/String; = "INVALIDATE"

.field private static final REMOTE_COMMAND_OUTPUT_DISPLAYLIST:Ljava/lang/String; = "OUTPUT_DISPLAYLIST"

.field private static final REMOTE_COMMAND_REQUEST_LAYOUT:Ljava/lang/String; = "REQUEST_LAYOUT"

.field private static final REMOTE_PROFILE:Ljava/lang/String; = "PROFILE"

.field public static final TRACE_HIERARCHY:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRACE_RECYCLER:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static mCapturedViewFieldsForClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field

.field private static mCapturedViewMethodsForClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field

.field private static sAnnotations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/reflect/AccessibleObject;",
            "Landroid/view/ViewDebug$ExportedProperty;",
            ">;"
        }
    .end annotation
.end field

.field private static sFieldsForClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field

.field private static sMethodsForClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 287
    sput-object v0, Landroid/view/ViewDebug;->mCapturedViewMethodsForClasses:Ljava/util/HashMap;

    #@3
    .line 288
    sput-object v0, Landroid/view/ViewDebug;->mCapturedViewFieldsForClasses:Ljava/util/HashMap;

    #@5
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 586
    return-void
.end method

.method private static capture(Landroid/view/View;Ljava/io/OutputStream;Ljava/lang/String;)V
    .registers 10
    .parameter "root"
    .parameter "clientStream"
    .parameter "parameter"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 701
    invoke-static {p0, p2}, Landroid/view/ViewDebug;->findView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    #@4
    move-result-object v1

    #@5
    .line 702
    .local v1, captureView:Landroid/view/View;
    const/4 v4, 0x0

    #@6
    invoke-static {v1, v4}, Landroid/view/ViewDebug;->performViewCapture(Landroid/view/View;Z)Landroid/graphics/Bitmap;

    #@9
    move-result-object v0

    #@a
    .line 704
    .local v0, b:Landroid/graphics/Bitmap;
    if-nez v0, :cond_21

    #@c
    .line 705
    const-string v4, "View"

    #@e
    const-string v5, "Failed to create capture bitmap!"

    #@10
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 708
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1a
    move-result-object v4

    #@1b
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@1d
    invoke-static {v4, v6, v6, v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@20
    move-result-object v0

    #@21
    .line 712
    :cond_21
    const/4 v2, 0x0

    #@22
    .line 714
    .local v2, out:Ljava/io/BufferedOutputStream;
    :try_start_22
    new-instance v3, Ljava/io/BufferedOutputStream;

    #@24
    const v4, 0x8000

    #@27
    invoke-direct {v3, p1, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2a
    .catchall {:try_start_22 .. :try_end_2a} :catchall_3d

    #@2a
    .line 715
    .end local v2           #out:Ljava/io/BufferedOutputStream;
    .local v3, out:Ljava/io/BufferedOutputStream;
    :try_start_2a
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@2c
    const/16 v5, 0x64

    #@2e
    invoke-virtual {v0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@31
    .line 716
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_34
    .catchall {:try_start_2a .. :try_end_34} :catchall_47

    #@34
    .line 718
    if-eqz v3, :cond_39

    #@36
    .line 719
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    #@39
    .line 721
    :cond_39
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@3c
    .line 723
    return-void

    #@3d
    .line 718
    .end local v3           #out:Ljava/io/BufferedOutputStream;
    .restart local v2       #out:Ljava/io/BufferedOutputStream;
    :catchall_3d
    move-exception v4

    #@3e
    :goto_3e
    if-eqz v2, :cond_43

    #@40
    .line 719
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    #@43
    .line 721
    :cond_43
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@46
    throw v4

    #@47
    .line 718
    .end local v2           #out:Ljava/io/BufferedOutputStream;
    .restart local v3       #out:Ljava/io/BufferedOutputStream;
    :catchall_47
    move-exception v4

    #@48
    move-object v2, v3

    #@49
    .end local v3           #out:Ljava/io/BufferedOutputStream;
    .restart local v2       #out:Ljava/io/BufferedOutputStream;
    goto :goto_3e
.end method

.method private static captureLayers(Landroid/view/View;Ljava/io/DataOutputStream;)V
    .registers 5
    .parameter "root"
    .parameter "clientStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 630
    :try_start_0
    new-instance v0, Landroid/graphics/Rect;

    #@2
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_2a

    #@5
    .line 632
    .local v0, outRect:Landroid/graphics/Rect;
    :try_start_5
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@9
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@b
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mWindow:Landroid/view/IWindow;

    #@d
    invoke-interface {v1, v2, v0}, Landroid/view/IWindowSession;->getDisplayFrame(Landroid/view/IWindow;Landroid/graphics/Rect;)V
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_2a
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_10} :catch_2f

    #@10
    .line 637
    :goto_10
    :try_start_10
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    #@13
    move-result v1

    #@14
    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@17
    .line 638
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@1e
    .line 640
    const/4 v1, 0x1

    #@1f
    invoke-static {p0, p1, v1}, Landroid/view/ViewDebug;->captureViewLayer(Landroid/view/View;Ljava/io/DataOutputStream;Z)V

    #@22
    .line 642
    const/4 v1, 0x2

    #@23
    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->write(I)V
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_2a

    #@26
    .line 644
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->close()V

    #@29
    .line 646
    return-void

    #@2a
    .line 644
    .end local v0           #outRect:Landroid/graphics/Rect;
    :catchall_2a
    move-exception v1

    #@2b
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->close()V

    #@2e
    throw v1

    #@2f
    .line 633
    .restart local v0       #outRect:Landroid/graphics/Rect;
    :catch_2f
    move-exception v1

    #@30
    goto :goto_10
.end method

.method private static captureViewLayer(Landroid/view/View;Ljava/io/DataOutputStream;Z)V
    .registers 16
    .parameter "view"
    .parameter "clientStream"
    .parameter "visible"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 651
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    #@5
    move-result v11

    #@6
    if-nez v11, :cond_8f

    #@8
    if-eqz p2, :cond_8f

    #@a
    move v6, v9

    #@b
    .line 653
    .local v6, localVisible:Z
    :goto_b
    iget v11, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    and-int/lit16 v11, v11, 0x80

    #@f
    const/16 v12, 0x80

    #@11
    if-eq v11, v12, :cond_77

    #@13
    .line 654
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    #@16
    move-result v5

    #@17
    .line 655
    .local v5, id:I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v11

    #@1b
    invoke-virtual {v11}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@1e
    move-result-object v7

    #@1f
    .line 656
    .local v7, name:Ljava/lang/String;
    const/4 v11, -0x1

    #@20
    if-eq v5, v11, :cond_2e

    #@22
    .line 657
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@25
    move-result-object v11

    #@26
    invoke-static {v11, v5}, Landroid/view/ViewDebug;->resolveId(Landroid/content/Context;I)Ljava/lang/Object;

    #@29
    move-result-object v11

    #@2a
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    .line 660
    :cond_2e
    invoke-virtual {p1, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@31
    .line 661
    invoke-virtual {p1, v7}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    #@34
    .line 662
    if-eqz v6, :cond_92

    #@36
    move v11, v9

    #@37
    :goto_37
    invoke-virtual {p1, v11}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@3a
    .line 664
    const/4 v11, 0x2

    #@3b
    new-array v8, v11, [I

    #@3d
    .line 666
    .local v8, position:[I
    invoke-virtual {p0, v8}, Landroid/view/View;->getLocationInWindow([I)V

    #@40
    .line 668
    aget v10, v8, v10

    #@42
    invoke-virtual {p1, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@45
    .line 669
    aget v10, v8, v9

    #@47
    invoke-virtual {p1, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4a
    .line 670
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V

    #@4d
    .line 672
    invoke-static {p0, v9}, Landroid/view/ViewDebug;->performViewCapture(Landroid/view/View;Z)Landroid/graphics/Bitmap;

    #@50
    move-result-object v1

    #@51
    .line 673
    .local v1, b:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_74

    #@53
    .line 674
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@55
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    #@58
    move-result v9

    #@59
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    #@5c
    move-result v10

    #@5d
    mul-int/2addr v9, v10

    #@5e
    mul-int/lit8 v9, v9, 0x2

    #@60
    invoke-direct {v0, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@63
    .line 676
    .local v0, arrayOut:Ljava/io/ByteArrayOutputStream;
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@65
    const/16 v10, 0x64

    #@67
    invoke-virtual {v1, v9, v10, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@6a
    .line 677
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    #@6d
    move-result v9

    #@6e
    invoke-virtual {p1, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@71
    .line 678
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    #@74
    .line 680
    .end local v0           #arrayOut:Ljava/io/ByteArrayOutputStream;
    :cond_74
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V

    #@77
    .line 683
    .end local v1           #b:Landroid/graphics/Bitmap;
    .end local v5           #id:I
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #position:[I
    :cond_77
    instance-of v9, p0, Landroid/view/ViewGroup;

    #@79
    if-eqz v9, :cond_94

    #@7b
    move-object v3, p0

    #@7c
    .line 684
    check-cast v3, Landroid/view/ViewGroup;

    #@7e
    .line 685
    .local v3, group:Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    #@81
    move-result v2

    #@82
    .line 687
    .local v2, count:I
    const/4 v4, 0x0

    #@83
    .local v4, i:I
    :goto_83
    if-ge v4, v2, :cond_94

    #@85
    .line 688
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@88
    move-result-object v9

    #@89
    invoke-static {v9, p1, v6}, Landroid/view/ViewDebug;->captureViewLayer(Landroid/view/View;Ljava/io/DataOutputStream;Z)V

    #@8c
    .line 687
    add-int/lit8 v4, v4, 0x1

    #@8e
    goto :goto_83

    #@8f
    .end local v2           #count:I
    .end local v3           #group:Landroid/view/ViewGroup;
    .end local v4           #i:I
    .end local v6           #localVisible:Z
    :cond_8f
    move v6, v10

    #@90
    .line 651
    goto/16 :goto_b

    #@92
    .restart local v5       #id:I
    .restart local v6       #localVisible:Z
    .restart local v7       #name:Ljava/lang/String;
    :cond_92
    move v11, v10

    #@93
    .line 662
    goto :goto_37

    #@94
    .line 691
    .end local v5           #id:I
    .end local v7           #name:Ljava/lang/String;
    :cond_94
    return-void
.end method

.method private static capturedViewExportFields(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "obj"
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 1304
    .local p1, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    if-nez p0, :cond_6

    #@2
    .line 1305
    const-string/jumbo v7, "null"

    #@5
    .line 1333
    :goto_5
    return-object v7

    #@6
    .line 1308
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    .line 1309
    .local v5, sb:Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/view/ViewDebug;->capturedViewGetPropertyFields(Ljava/lang/Class;)[Ljava/lang/reflect/Field;

    #@e
    move-result-object v3

    #@f
    .line 1311
    .local v3, fields:[Ljava/lang/reflect/Field;
    array-length v0, v3

    #@10
    .line 1312
    .local v0, count:I
    const/4 v4, 0x0

    #@11
    .local v4, i:I
    :goto_11
    if-ge v4, v0, :cond_4a

    #@13
    .line 1313
    aget-object v1, v3, v4

    #@15
    .line 1315
    .local v1, field:Ljava/lang/reflect/Field;
    :try_start_15
    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    .line 1317
    .local v2, fieldValue:Ljava/lang/Object;
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 1318
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 1319
    const-string v7, "="

    #@25
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 1321
    if-eqz v2, :cond_41

    #@2a
    .line 1322
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    const-string v8, "\n"

    #@30
    const-string v9, "\\n"

    #@32
    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    .line 1323
    .local v6, value:Ljava/lang/String;
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    .line 1327
    .end local v6           #value:Ljava/lang/String;
    :goto_39
    const/16 v7, 0x20

    #@3b
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3e
    .line 1312
    .end local v2           #fieldValue:Ljava/lang/Object;
    :goto_3e
    add-int/lit8 v4, v4, 0x1

    #@40
    goto :goto_11

    #@41
    .line 1325
    .restart local v2       #fieldValue:Ljava/lang/Object;
    :cond_41
    const-string/jumbo v7, "null"

    #@44
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_47
    .catch Ljava/lang/IllegalAccessException; {:try_start_15 .. :try_end_47} :catch_48

    #@47
    goto :goto_39

    #@48
    .line 1328
    .end local v2           #fieldValue:Ljava/lang/Object;
    :catch_48
    move-exception v7

    #@49
    goto :goto_3e

    #@4a
    .line 1333
    .end local v1           #field:Ljava/lang/reflect/Field;
    :cond_4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v7

    #@4e
    goto :goto_5
.end method

.method private static capturedViewExportMethods(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;
    .registers 15
    .parameter "obj"
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 1261
    .local p1, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    if-nez p0, :cond_6

    #@2
    .line 1262
    const-string/jumbo v9, "null"

    #@5
    .line 1300
    :goto_5
    return-object v9

    #@6
    .line 1265
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    .line 1266
    .local v7, sb:Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/view/ViewDebug;->capturedViewGetPropertyMethods(Ljava/lang/Class;)[Ljava/lang/reflect/Method;

    #@e
    move-result-object v4

    #@f
    .line 1268
    .local v4, methods:[Ljava/lang/reflect/Method;
    array-length v0, v4

    #@10
    .line 1269
    .local v0, count:I
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_80

    #@13
    .line 1270
    aget-object v2, v4, v1

    #@15
    .line 1272
    .local v2, method:Ljava/lang/reflect/Method;
    const/4 v9, 0x0

    #@16
    :try_start_16
    check-cast v9, [Ljava/lang/Object;

    #@18
    invoke-virtual {v2, p0, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    .line 1273
    .local v3, methodValue:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    #@1f
    move-result-object v6

    #@20
    .line 1275
    .local v6, returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-class v9, Landroid/view/ViewDebug$CapturedViewProperty;

    #@22
    invoke-virtual {v2, v9}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@25
    move-result-object v5

    #@26
    check-cast v5, Landroid/view/ViewDebug$CapturedViewProperty;

    #@28
    .line 1276
    .local v5, property:Landroid/view/ViewDebug$CapturedViewProperty;
    invoke-interface {v5}, Landroid/view/ViewDebug$CapturedViewProperty;->retrieveReturn()Z

    #@2b
    move-result v9

    #@2c
    if-eqz v9, :cond_4f

    #@2e
    .line 1278
    new-instance v9, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    const-string v10, "#"

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v9

    #@45
    invoke-static {v3, v6, v9}, Landroid/view/ViewDebug;->capturedViewExportMethods(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    .line 1269
    .end local v3           #methodValue:Ljava/lang/Object;
    .end local v5           #property:Landroid/view/ViewDebug$CapturedViewProperty;
    .end local v6           #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_4c
    add-int/lit8 v1, v1, 0x1

    #@4e
    goto :goto_11

    #@4f
    .line 1280
    .restart local v3       #methodValue:Ljava/lang/Object;
    .restart local v5       #property:Landroid/view/ViewDebug$CapturedViewProperty;
    .restart local v6       #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_4f
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    .line 1281
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    #@55
    move-result-object v9

    #@56
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    .line 1282
    const-string v9, "()="

    #@5b
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 1284
    if-eqz v3, :cond_77

    #@60
    .line 1285
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@63
    move-result-object v9

    #@64
    const-string v10, "\n"

    #@66
    const-string v11, "\\n"

    #@68
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@6b
    move-result-object v8

    #@6c
    .line 1286
    .local v8, value:Ljava/lang/String;
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    .line 1290
    .end local v8           #value:Ljava/lang/String;
    :goto_6f
    const-string v9, "; "

    #@71
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    goto :goto_4c

    #@75
    .line 1292
    .end local v3           #methodValue:Ljava/lang/Object;
    .end local v5           #property:Landroid/view/ViewDebug$CapturedViewProperty;
    .end local v6           #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_75
    move-exception v9

    #@76
    goto :goto_4c

    #@77
    .line 1288
    .restart local v3       #methodValue:Ljava/lang/Object;
    .restart local v5       #property:Landroid/view/ViewDebug$CapturedViewProperty;
    .restart local v6       #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_77
    const-string/jumbo v9, "null"

    #@7a
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_7d
    .catch Ljava/lang/IllegalAccessException; {:try_start_16 .. :try_end_7d} :catch_75
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_16 .. :try_end_7d} :catch_7e

    #@7d
    goto :goto_6f

    #@7e
    .line 1295
    .end local v3           #methodValue:Ljava/lang/Object;
    .end local v5           #property:Landroid/view/ViewDebug$CapturedViewProperty;
    .end local v6           #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_7e
    move-exception v9

    #@7f
    goto :goto_4c

    #@80
    .line 1300
    .end local v2           #method:Ljava/lang/reflect/Method;
    :cond_80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v9

    #@84
    goto :goto_5
.end method

.method private static capturedViewGetPropertyFields(Ljava/lang/Class;)[Ljava/lang/reflect/Field;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    #@0
    .prologue
    .line 1199
    .local p0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v7, Landroid/view/ViewDebug;->mCapturedViewFieldsForClasses:Ljava/util/HashMap;

    #@2
    if-nez v7, :cond_b

    #@4
    .line 1200
    new-instance v7, Ljava/util/HashMap;

    #@6
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@9
    sput-object v7, Landroid/view/ViewDebug;->mCapturedViewFieldsForClasses:Ljava/util/HashMap;

    #@b
    .line 1202
    :cond_b
    sget-object v6, Landroid/view/ViewDebug;->mCapturedViewFieldsForClasses:Ljava/util/HashMap;

    #@d
    .line 1204
    .local v6, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Class<*>;[Ljava/lang/reflect/Field;>;"
    invoke-virtual {v6, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v2

    #@11
    check-cast v2, [Ljava/lang/reflect/Field;

    #@13
    .line 1205
    .local v2, fields:[Ljava/lang/reflect/Field;
    if-eqz v2, :cond_17

    #@15
    move-object v3, v2

    #@16
    .line 1224
    .end local v2           #fields:[Ljava/lang/reflect/Field;
    .local v3, fields:[Ljava/lang/reflect/Field;
    :goto_16
    return-object v3

    #@17
    .line 1209
    .end local v3           #fields:[Ljava/lang/reflect/Field;
    .restart local v2       #fields:[Ljava/lang/reflect/Field;
    :cond_17
    new-instance v4, Ljava/util/ArrayList;

    #@19
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@1c
    .line 1210
    .local v4, foundFields:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/reflect/Field;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    #@1f
    move-result-object v2

    #@20
    .line 1212
    array-length v0, v2

    #@21
    .line 1213
    .local v0, count:I
    const/4 v5, 0x0

    #@22
    .local v5, i:I
    :goto_22
    if-ge v5, v0, :cond_38

    #@24
    .line 1214
    aget-object v1, v2, v5

    #@26
    .line 1215
    .local v1, field:Ljava/lang/reflect/Field;
    const-class v7, Landroid/view/ViewDebug$CapturedViewProperty;

    #@28
    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@2b
    move-result v7

    #@2c
    if-eqz v7, :cond_35

    #@2e
    .line 1216
    const/4 v7, 0x1

    #@2f
    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    #@32
    .line 1217
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    .line 1213
    :cond_35
    add-int/lit8 v5, v5, 0x1

    #@37
    goto :goto_22

    #@38
    .line 1221
    .end local v1           #field:Ljava/lang/reflect/Field;
    :cond_38
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v7

    #@3c
    new-array v7, v7, [Ljava/lang/reflect/Field;

    #@3e
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@41
    move-result-object v2

    #@42
    .end local v2           #fields:[Ljava/lang/reflect/Field;
    check-cast v2, [Ljava/lang/reflect/Field;

    #@44
    .line 1222
    .restart local v2       #fields:[Ljava/lang/reflect/Field;
    invoke-virtual {v6, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    move-object v3, v2

    #@48
    .line 1224
    .end local v2           #fields:[Ljava/lang/reflect/Field;
    .restart local v3       #fields:[Ljava/lang/reflect/Field;
    goto :goto_16
.end method

.method private static capturedViewGetPropertyMethods(Ljava/lang/Class;)[Ljava/lang/reflect/Method;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    #@0
    .prologue
    .line 1228
    .local p0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v7, Landroid/view/ViewDebug;->mCapturedViewMethodsForClasses:Ljava/util/HashMap;

    #@2
    if-nez v7, :cond_b

    #@4
    .line 1229
    new-instance v7, Ljava/util/HashMap;

    #@6
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@9
    sput-object v7, Landroid/view/ViewDebug;->mCapturedViewMethodsForClasses:Ljava/util/HashMap;

    #@b
    .line 1231
    :cond_b
    sget-object v3, Landroid/view/ViewDebug;->mCapturedViewMethodsForClasses:Ljava/util/HashMap;

    #@d
    .line 1233
    .local v3, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Class<*>;[Ljava/lang/reflect/Method;>;"
    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v5

    #@11
    check-cast v5, [Ljava/lang/reflect/Method;

    #@13
    .line 1234
    .local v5, methods:[Ljava/lang/reflect/Method;
    if-eqz v5, :cond_17

    #@15
    move-object v6, v5

    #@16
    .line 1255
    .end local v5           #methods:[Ljava/lang/reflect/Method;
    .local v6, methods:[Ljava/lang/reflect/Method;
    :goto_16
    return-object v6

    #@17
    .line 1238
    .end local v6           #methods:[Ljava/lang/reflect/Method;
    .restart local v5       #methods:[Ljava/lang/reflect/Method;
    :cond_17
    new-instance v1, Ljava/util/ArrayList;

    #@19
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@1c
    .line 1239
    .local v1, foundMethods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/reflect/Method;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    #@1f
    move-result-object v5

    #@20
    .line 1241
    array-length v0, v5

    #@21
    .line 1242
    .local v0, count:I
    const/4 v2, 0x0

    #@22
    .local v2, i:I
    :goto_22
    if-ge v2, v0, :cond_47

    #@24
    .line 1243
    aget-object v4, v5, v2

    #@26
    .line 1244
    .local v4, method:Ljava/lang/reflect/Method;
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    #@29
    move-result-object v7

    #@2a
    array-length v7, v7

    #@2b
    if-nez v7, :cond_44

    #@2d
    const-class v7, Landroid/view/ViewDebug$CapturedViewProperty;

    #@2f
    invoke-virtual {v4, v7}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@32
    move-result v7

    #@33
    if-eqz v7, :cond_44

    #@35
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    #@38
    move-result-object v7

    #@39
    const-class v8, Ljava/lang/Void;

    #@3b
    if-eq v7, v8, :cond_44

    #@3d
    .line 1247
    const/4 v7, 0x1

    #@3e
    invoke-virtual {v4, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    #@41
    .line 1248
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@44
    .line 1242
    :cond_44
    add-int/lit8 v2, v2, 0x1

    #@46
    goto :goto_22

    #@47
    .line 1252
    .end local v4           #method:Ljava/lang/reflect/Method;
    :cond_47
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@4a
    move-result v7

    #@4b
    new-array v7, v7, [Ljava/lang/reflect/Method;

    #@4d
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@50
    move-result-object v5

    #@51
    .end local v5           #methods:[Ljava/lang/reflect/Method;
    check-cast v5, [Ljava/lang/reflect/Method;

    #@53
    .line 1253
    .restart local v5       #methods:[Ljava/lang/reflect/Method;
    invoke-virtual {v3, p0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    move-object v6, v5

    #@57
    .line 1255
    .end local v5           #methods:[Ljava/lang/reflect/Method;
    .restart local v6       #methods:[Ljava/lang/reflect/Method;
    goto :goto_16
.end method

.method static dispatchCommand(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;)V
    .registers 7
    .parameter "view"
    .parameter "command"
    .parameter "parameters"
    .parameter "clientStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 406
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@4
    move-result-object p0

    #@5
    .line 408
    const-string v1, "DUMP"

    #@7
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_11

    #@d
    .line 409
    invoke-static {p0, p3}, Landroid/view/ViewDebug;->dump(Landroid/view/View;Ljava/io/OutputStream;)V

    #@10
    .line 426
    :cond_10
    :goto_10
    return-void

    #@11
    .line 410
    :cond_11
    const-string v1, "CAPTURE_LAYERS"

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_22

    #@19
    .line 411
    new-instance v1, Ljava/io/DataOutputStream;

    #@1b
    invoke-direct {v1, p3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@1e
    invoke-static {p0, v1}, Landroid/view/ViewDebug;->captureLayers(Landroid/view/View;Ljava/io/DataOutputStream;)V

    #@21
    goto :goto_10

    #@22
    .line 413
    :cond_22
    const-string v1, " "

    #@24
    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 414
    .local v0, params:[Ljava/lang/String;
    const-string v1, "CAPTURE"

    #@2a
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2d
    move-result v1

    #@2e
    if-eqz v1, :cond_36

    #@30
    .line 415
    aget-object v1, v0, v2

    #@32
    invoke-static {p0, p3, v1}, Landroid/view/ViewDebug;->capture(Landroid/view/View;Ljava/io/OutputStream;Ljava/lang/String;)V

    #@35
    goto :goto_10

    #@36
    .line 416
    :cond_36
    const-string v1, "OUTPUT_DISPLAYLIST"

    #@38
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_44

    #@3e
    .line 417
    aget-object v1, v0, v2

    #@40
    invoke-static {p0, v1}, Landroid/view/ViewDebug;->outputDisplayList(Landroid/view/View;Ljava/lang/String;)V

    #@43
    goto :goto_10

    #@44
    .line 418
    :cond_44
    const-string v1, "INVALIDATE"

    #@46
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@49
    move-result v1

    #@4a
    if-eqz v1, :cond_52

    #@4c
    .line 419
    aget-object v1, v0, v2

    #@4e
    invoke-static {p0, v1}, Landroid/view/ViewDebug;->invalidate(Landroid/view/View;Ljava/lang/String;)V

    #@51
    goto :goto_10

    #@52
    .line 420
    :cond_52
    const-string v1, "REQUEST_LAYOUT"

    #@54
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@57
    move-result v1

    #@58
    if-eqz v1, :cond_60

    #@5a
    .line 421
    aget-object v1, v0, v2

    #@5c
    invoke-static {p0, v1}, Landroid/view/ViewDebug;->requestLayout(Landroid/view/View;Ljava/lang/String;)V

    #@5f
    goto :goto_10

    #@60
    .line 422
    :cond_60
    const-string v1, "PROFILE"

    #@62
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@65
    move-result v1

    #@66
    if-eqz v1, :cond_10

    #@68
    .line 423
    aget-object v1, v0, v2

    #@6a
    invoke-static {p0, p3, v1}, Landroid/view/ViewDebug;->profile(Landroid/view/View;Ljava/io/OutputStream;Ljava/lang/String;)V

    #@6d
    goto :goto_10
.end method

.method private static dump(Landroid/view/View;Ljava/io/OutputStream;)V
    .registers 10
    .parameter "root"
    .parameter "clientStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 756
    const/4 v3, 0x0

    #@1
    .line 758
    .local v3, out:Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v4, Ljava/io/BufferedWriter;

    #@3
    new-instance v6, Ljava/io/OutputStreamWriter;

    #@5
    const-string/jumbo v7, "utf-8"

    #@8
    invoke-direct {v6, p1, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@b
    const v7, 0x8000

    #@e
    invoke-direct {v4, v6, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_42
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_11} :catch_34

    #@11
    .line 759
    .end local v3           #out:Ljava/io/BufferedWriter;
    .local v4, out:Ljava/io/BufferedWriter;
    :try_start_11
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@14
    move-result-object v5

    #@15
    .line 760
    .local v5, view:Landroid/view/View;
    instance-of v6, v5, Landroid/view/ViewGroup;

    #@17
    if-eqz v6, :cond_25

    #@19
    .line 761
    move-object v0, v5

    #@1a
    check-cast v0, Landroid/view/ViewGroup;

    #@1c
    move-object v2, v0

    #@1d
    .line 762
    .local v2, group:Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@20
    move-result-object v6

    #@21
    const/4 v7, 0x0

    #@22
    invoke-static {v6, v2, v4, v7}, Landroid/view/ViewDebug;->dumpViewHierarchyWithProperties(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/io/BufferedWriter;I)V

    #@25
    .line 764
    .end local v2           #group:Landroid/view/ViewGroup;
    :cond_25
    const-string v6, "DONE."

    #@27
    invoke-virtual {v4, v6}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@2a
    .line 765
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_2d
    .catchall {:try_start_11 .. :try_end_2d} :catchall_49
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_2d} :catch_4c

    #@2d
    .line 769
    if-eqz v4, :cond_4f

    #@2f
    .line 770
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V

    #@32
    move-object v3, v4

    #@33
    .line 773
    .end local v4           #out:Ljava/io/BufferedWriter;
    .end local v5           #view:Landroid/view/View;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    :cond_33
    :goto_33
    return-void

    #@34
    .line 766
    :catch_34
    move-exception v1

    #@35
    .line 767
    .local v1, e:Ljava/lang/Exception;
    :goto_35
    :try_start_35
    const-string v6, "View"

    #@37
    const-string v7, "Problem dumping the view:"

    #@39
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3c
    .catchall {:try_start_35 .. :try_end_3c} :catchall_42

    #@3c
    .line 769
    if-eqz v3, :cond_33

    #@3e
    .line 770
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V

    #@41
    goto :goto_33

    #@42
    .line 769
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_42
    move-exception v6

    #@43
    :goto_43
    if-eqz v3, :cond_48

    #@45
    .line 770
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V

    #@48
    :cond_48
    throw v6

    #@49
    .line 769
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v4       #out:Ljava/io/BufferedWriter;
    :catchall_49
    move-exception v6

    #@4a
    move-object v3, v4

    #@4b
    .end local v4           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    goto :goto_43

    #@4c
    .line 766
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v4       #out:Ljava/io/BufferedWriter;
    :catch_4c
    move-exception v1

    #@4d
    move-object v3, v4

    #@4e
    .end local v4           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    goto :goto_35

    #@4f
    .end local v3           #out:Ljava/io/BufferedWriter;
    .restart local v4       #out:Ljava/io/BufferedWriter;
    .restart local v5       #view:Landroid/view/View;
    :cond_4f
    move-object v3, v4

    #@50
    .end local v4           #out:Ljava/io/BufferedWriter;
    .restart local v3       #out:Ljava/io/BufferedWriter;
    goto :goto_33
.end method

.method public static dumpCapturedView(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6
    .parameter "tag"
    .parameter "view"

    #@0
    .prologue
    .line 1344
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    .line 1345
    .local v0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, ": "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@20
    .line 1346
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, ""

    #@22
    invoke-static {p1, v0, v2}, Landroid/view/ViewDebug;->capturedViewExportFields(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    .line 1347
    const-string v2, ""

    #@2b
    invoke-static {p1, v0, v2}, Landroid/view/ViewDebug;->capturedViewExportMethods(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 1348
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {p0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 1349
    return-void
.end method

.method private static dumpViewHierarchyWithProperties(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/io/BufferedWriter;I)V
    .registers 8
    .parameter "context"
    .parameter "group"
    .parameter "out"
    .parameter "level"

    #@0
    .prologue
    .line 809
    invoke-static {p0, p1, p2, p3}, Landroid/view/ViewDebug;->dumpViewWithProperties(Landroid/content/Context;Landroid/view/View;Ljava/io/BufferedWriter;I)Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 825
    .end local p1
    :cond_6
    :goto_6
    return-void

    #@7
    .line 813
    .restart local p1
    :cond_7
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    #@a
    move-result v0

    #@b
    .line 814
    .local v0, count:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_26

    #@e
    .line 815
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v2

    #@12
    .line 816
    .local v2, view:Landroid/view/View;
    instance-of v3, v2, Landroid/view/ViewGroup;

    #@14
    if-eqz v3, :cond_20

    #@16
    .line 817
    check-cast v2, Landroid/view/ViewGroup;

    #@18
    .end local v2           #view:Landroid/view/View;
    add-int/lit8 v3, p3, 0x1

    #@1a
    invoke-static {p0, v2, p2, v3}, Landroid/view/ViewDebug;->dumpViewHierarchyWithProperties(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/io/BufferedWriter;I)V

    #@1d
    .line 814
    :goto_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_c

    #@20
    .line 819
    .restart local v2       #view:Landroid/view/View;
    :cond_20
    add-int/lit8 v3, p3, 0x1

    #@22
    invoke-static {p0, v2, p2, v3}, Landroid/view/ViewDebug;->dumpViewWithProperties(Landroid/content/Context;Landroid/view/View;Ljava/io/BufferedWriter;I)Z

    #@25
    goto :goto_1d

    #@26
    .line 822
    .end local v2           #view:Landroid/view/View;
    :cond_26
    instance-of v3, p1, Landroid/view/ViewDebug$HierarchyHandler;

    #@28
    if-eqz v3, :cond_6

    #@2a
    .line 823
    check-cast p1, Landroid/view/ViewDebug$HierarchyHandler;

    #@2c
    .end local p1
    add-int/lit8 v3, p3, 0x1

    #@2e
    invoke-interface {p1, p2, v3}, Landroid/view/ViewDebug$HierarchyHandler;->dumpViewHierarchyWithProperties(Ljava/io/BufferedWriter;I)V

    #@31
    goto :goto_6
.end method

.method private static dumpViewProperties(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;)V
    .registers 4
    .parameter "context"
    .parameter "view"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 920
    const-string v0, ""

    #@2
    invoke-static {p0, p1, p2, v0}, Landroid/view/ViewDebug;->dumpViewProperties(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/String;)V

    #@5
    .line 921
    return-void
.end method

.method private static dumpViewProperties(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "view"
    .parameter "out"
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 926
    if-nez p1, :cond_19

    #@2
    .line 927
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    const-string v2, "=4,null "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p2, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@18
    .line 937
    :goto_18
    return-void

    #@19
    .line 931
    :cond_19
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c
    move-result-object v0

    #@1d
    .line 933
    .local v0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_1d
    invoke-static {p0, p1, p2, v0, p3}, Landroid/view/ViewDebug;->exportFields(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/Class;Ljava/lang/String;)V

    #@20
    .line 934
    invoke-static {p0, p1, p2, v0, p3}, Landroid/view/ViewDebug;->exportMethods(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/Class;Ljava/lang/String;)V

    #@23
    .line 935
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    #@26
    move-result-object v0

    #@27
    .line 936
    const-class v1, Ljava/lang/Object;

    #@29
    if-ne v0, v1, :cond_1d

    #@2b
    goto :goto_18
.end method

.method private static dumpViewWithProperties(Landroid/content/Context;Landroid/view/View;Ljava/io/BufferedWriter;I)Z
    .registers 8
    .parameter "context"
    .parameter "view"
    .parameter "out"
    .parameter "level"

    #@0
    .prologue
    .line 831
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    if-ge v1, p3, :cond_b

    #@3
    .line 832
    const/16 v2, 0x20

    #@5
    :try_start_5
    invoke-virtual {p2, v2}, Ljava/io/BufferedWriter;->write(I)V

    #@8
    .line 831
    add-int/lit8 v1, v1, 0x1

    #@a
    goto :goto_1

    #@b
    .line 834
    :cond_b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {p2, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@16
    .line 835
    const/16 v2, 0x40

    #@18
    invoke-virtual {p2, v2}, Ljava/io/BufferedWriter;->write(I)V

    #@1b
    .line 836
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    #@1e
    move-result v2

    #@1f
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {p2, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@26
    .line 837
    const/16 v2, 0x20

    #@28
    invoke-virtual {p2, v2}, Ljava/io/BufferedWriter;->write(I)V

    #@2b
    .line 838
    invoke-static {p0, p1, p2}, Landroid/view/ViewDebug;->dumpViewProperties(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;)V

    #@2e
    .line 839
    invoke-virtual {p2}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_31} :catch_33

    #@31
    .line 844
    const/4 v2, 0x1

    #@32
    :goto_32
    return v2

    #@33
    .line 840
    :catch_33
    move-exception v0

    #@34
    .line 841
    .local v0, e:Ljava/io/IOException;
    const-string v2, "View"

    #@36
    const-string v3, "Error while dumping hierarchy tree"

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 842
    const/4 v2, 0x0

    #@3c
    goto :goto_32
.end method

.method private static exportFields(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/Class;Ljava/lang/String;)V
    .registers 28
    .parameter "context"
    .parameter "view"
    .parameter "out"
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Object;",
            "Ljava/io/BufferedWriter;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1015
    .local p3, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-static/range {p3 .. p3}, Landroid/view/ViewDebug;->getExportedPropertyFields(Ljava/lang/Class;)[Ljava/lang/reflect/Field;

    #@3
    move-result-object v12

    #@4
    .line 1017
    .local v12, fields:[Ljava/lang/reflect/Field;
    array-length v9, v12

    #@5
    .line 1018
    .local v9, count:I
    const/4 v14, 0x0

    #@6
    .local v14, i:I
    :goto_6
    if-ge v14, v9, :cond_126

    #@8
    .line 1019
    aget-object v10, v12, v14

    #@a
    .line 1023
    .local v10, field:Ljava/lang/reflect/Field;
    const/4 v11, 0x0

    #@b
    .line 1024
    .local v11, fieldValue:Ljava/lang/Object;
    :try_start_b
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@e
    move-result-object v22

    #@f
    .line 1025
    .local v22, type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v2, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@11
    invoke-virtual {v2, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Landroid/view/ViewDebug$ExportedProperty;

    #@17
    .line 1026
    .local v4, property:Landroid/view/ViewDebug$ExportedProperty;
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->category()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_7b

    #@21
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->category()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, ":"

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    .line 1029
    .local v8, categoryPrefix:Ljava/lang/String;
    :goto_38
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@3a
    move-object/from16 v0, v22

    #@3c
    if-ne v0, v2, :cond_e8

    #@3e
    .line 1031
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->resolveId()Z

    #@41
    move-result v2

    #@42
    if-eqz v2, :cond_7e

    #@44
    if-eqz p0, :cond_7e

    #@46
    .line 1032
    move-object/from16 v0, p1

    #@48
    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    #@4b
    move-result v15

    #@4c
    .line 1033
    .local v15, id:I
    move-object/from16 v0, p0

    #@4e
    invoke-static {v0, v15}, Landroid/view/ViewDebug;->resolveId(Landroid/content/Context;I)Ljava/lang/Object;

    #@51
    move-result-object v11

    #@52
    .line 1077
    .end local v11           #fieldValue:Ljava/lang/Object;
    .end local v15           #id:I
    :cond_52
    :goto_52
    if-nez v11, :cond_5a

    #@54
    .line 1078
    move-object/from16 v0, p1

    #@56
    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@59
    move-result-object v11

    #@5a
    .line 1081
    :cond_5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    move-object/from16 v0, p4

    #@65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    const-string v7, ""

    #@73
    move-object/from16 v0, p2

    #@75
    invoke-static {v0, v2, v3, v7, v11}, Landroid/view/ViewDebug;->writeEntry(Ljava/io/BufferedWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    #@78
    .line 1018
    .end local v4           #property:Landroid/view/ViewDebug$ExportedProperty;
    .end local v8           #categoryPrefix:Ljava/lang/String;
    .end local v22           #type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_78
    add-int/lit8 v14, v14, 0x1

    #@7a
    goto :goto_6

    #@7b
    .line 1026
    .restart local v4       #property:Landroid/view/ViewDebug$ExportedProperty;
    .restart local v11       #fieldValue:Ljava/lang/Object;
    .restart local v22       #type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_7b
    const-string v8, ""

    #@7d
    goto :goto_38

    #@7e
    .line 1035
    .restart local v8       #categoryPrefix:Ljava/lang/String;
    :cond_7e
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->flagMapping()[Landroid/view/ViewDebug$FlagToString;

    #@81
    move-result-object v13

    #@82
    .line 1036
    .local v13, flagsMapping:[Landroid/view/ViewDebug$FlagToString;
    array-length v2, v13

    #@83
    if-lez v2, :cond_b3

    #@85
    .line 1037
    move-object/from16 v0, p1

    #@87
    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    #@8a
    move-result v16

    #@8b
    .line 1038
    .local v16, intValue:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    move-object/from16 v0, p4

    #@96
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@9d
    move-result-object v3

    #@9e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v2

    #@a2
    const/16 v3, 0x5f

    #@a4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v2

    #@a8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v6

    #@ac
    .line 1040
    .local v6, valuePrefix:Ljava/lang/String;
    move-object/from16 v0, p2

    #@ae
    move/from16 v1, v16

    #@b0
    invoke-static {v0, v13, v1, v6}, Landroid/view/ViewDebug;->exportUnrolledFlags(Ljava/io/BufferedWriter;[Landroid/view/ViewDebug$FlagToString;ILjava/lang/String;)V

    #@b3
    .line 1043
    .end local v6           #valuePrefix:Ljava/lang/String;
    .end local v16           #intValue:I
    :cond_b3
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->mapping()[Landroid/view/ViewDebug$IntToString;

    #@b6
    move-result-object v19

    #@b7
    .line 1044
    .local v19, mapping:[Landroid/view/ViewDebug$IntToString;
    move-object/from16 v0, v19

    #@b9
    array-length v2, v0

    #@ba
    if-lez v2, :cond_52

    #@bc
    .line 1045
    move-object/from16 v0, p1

    #@be
    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    #@c1
    move-result v16

    #@c2
    .line 1046
    .restart local v16       #intValue:I
    move-object/from16 v0, v19

    #@c4
    array-length v0, v0

    #@c5
    move/from16 v20, v0

    #@c7
    .line 1047
    .local v20, mappingCount:I
    const/16 v17, 0x0

    #@c9
    .local v17, j:I
    :goto_c9
    move/from16 v0, v17

    #@cb
    move/from16 v1, v20

    #@cd
    if-ge v0, v1, :cond_dd

    #@cf
    .line 1048
    aget-object v18, v19, v17

    #@d1
    .line 1049
    .local v18, mapped:Landroid/view/ViewDebug$IntToString;
    invoke-interface/range {v18 .. v18}, Landroid/view/ViewDebug$IntToString;->from()I

    #@d4
    move-result v2

    #@d5
    move/from16 v0, v16

    #@d7
    if-ne v2, v0, :cond_e5

    #@d9
    .line 1050
    invoke-interface/range {v18 .. v18}, Landroid/view/ViewDebug$IntToString;->to()Ljava/lang/String;

    #@dc
    move-result-object v11

    #@dd
    .line 1055
    .end local v11           #fieldValue:Ljava/lang/Object;
    .end local v18           #mapped:Landroid/view/ViewDebug$IntToString;
    :cond_dd
    if-nez v11, :cond_52

    #@df
    .line 1056
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e2
    move-result-object v11

    #@e3
    .local v11, fieldValue:Ljava/lang/Integer;
    goto/16 :goto_52

    #@e5
    .line 1047
    .local v11, fieldValue:Ljava/lang/Object;
    .restart local v18       #mapped:Landroid/view/ViewDebug$IntToString;
    :cond_e5
    add-int/lit8 v17, v17, 0x1

    #@e7
    goto :goto_c9

    #@e8
    .line 1060
    .end local v13           #flagsMapping:[Landroid/view/ViewDebug$FlagToString;
    .end local v16           #intValue:I
    .end local v17           #j:I
    .end local v18           #mapped:Landroid/view/ViewDebug$IntToString;
    .end local v19           #mapping:[Landroid/view/ViewDebug$IntToString;
    .end local v20           #mappingCount:I
    :cond_e8
    const-class v2, [I

    #@ea
    move-object/from16 v0, v22

    #@ec
    if-ne v0, v2, :cond_127

    #@ee
    .line 1061
    move-object/from16 v0, p1

    #@f0
    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f3
    move-result-object v2

    #@f4
    check-cast v2, [I

    #@f6
    move-object v0, v2

    #@f7
    check-cast v0, [I

    #@f9
    move-object v5, v0

    #@fa
    .line 1062
    .local v5, array:[I
    new-instance v2, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    move-object/from16 v0, p4

    #@105
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@10c
    move-result-object v3

    #@10d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v2

    #@111
    const/16 v3, 0x5f

    #@113
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@116
    move-result-object v2

    #@117
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a
    move-result-object v6

    #@11b
    .line 1063
    .restart local v6       #valuePrefix:Ljava/lang/String;
    const-string v21, ""

    #@11d
    .line 1065
    .local v21, suffix:Ljava/lang/String;
    const-string v7, ""

    #@11f
    move-object/from16 v2, p0

    #@121
    move-object/from16 v3, p2

    #@123
    invoke-static/range {v2 .. v7}, Landroid/view/ViewDebug;->exportUnrolledArray(Landroid/content/Context;Ljava/io/BufferedWriter;Landroid/view/ViewDebug$ExportedProperty;[ILjava/lang/String;Ljava/lang/String;)V

    #@126
    .line 1085
    .end local v4           #property:Landroid/view/ViewDebug$ExportedProperty;
    .end local v5           #array:[I
    .end local v6           #valuePrefix:Ljava/lang/String;
    .end local v8           #categoryPrefix:Ljava/lang/String;
    .end local v10           #field:Ljava/lang/reflect/Field;
    .end local v11           #fieldValue:Ljava/lang/Object;
    .end local v21           #suffix:Ljava/lang/String;
    .end local v22           #type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_126
    return-void

    #@127
    .line 1069
    .restart local v4       #property:Landroid/view/ViewDebug$ExportedProperty;
    .restart local v8       #categoryPrefix:Ljava/lang/String;
    .restart local v10       #field:Ljava/lang/reflect/Field;
    .restart local v11       #fieldValue:Ljava/lang/Object;
    .restart local v22       #type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_127
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Class;->isPrimitive()Z

    #@12a
    move-result v2

    #@12b
    if-nez v2, :cond_52

    #@12d
    .line 1070
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->deepExport()Z

    #@130
    move-result v2

    #@131
    if-eqz v2, :cond_52

    #@133
    .line 1071
    move-object/from16 v0, p1

    #@135
    invoke-virtual {v10, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@138
    move-result-object v2

    #@139
    new-instance v3, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    move-object/from16 v0, p4

    #@140
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v3

    #@144
    invoke-interface {v4}, Landroid/view/ViewDebug$ExportedProperty;->prefix()Ljava/lang/String;

    #@147
    move-result-object v7

    #@148
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v3

    #@14c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v3

    #@150
    move-object/from16 v0, p0

    #@152
    move-object/from16 v1, p2

    #@154
    invoke-static {v0, v2, v1, v3}, Landroid/view/ViewDebug;->dumpViewProperties(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/String;)V
    :try_end_157
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_157} :catch_159

    #@157
    goto/16 :goto_78

    #@159
    .line 1082
    .end local v4           #property:Landroid/view/ViewDebug$ExportedProperty;
    .end local v8           #categoryPrefix:Ljava/lang/String;
    .end local v11           #fieldValue:Ljava/lang/Object;
    .end local v22           #type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_159
    move-exception v2

    #@15a
    goto/16 :goto_78
.end method

.method private static exportMethods(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/Class;Ljava/lang/String;)V
    .registers 30
    .parameter "context"
    .parameter "view"
    .parameter "out"
    .parameter
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Object;",
            "Ljava/io/BufferedWriter;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 942
    .local p3, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-static/range {p3 .. p3}, Landroid/view/ViewDebug;->getExportedPropertyMethods(Ljava/lang/Class;)[Ljava/lang/reflect/Method;

    #@3
    move-result-object v22

    #@4
    .line 944
    .local v22, methods:[Ljava/lang/reflect/Method;
    move-object/from16 v0, v22

    #@6
    array-length v10, v0

    #@7
    .line 945
    .local v10, count:I
    const/4 v12, 0x0

    #@8
    .local v12, i:I
    :goto_8
    if-ge v12, v10, :cond_12c

    #@a
    .line 946
    aget-object v20, v22, v12

    #@c
    .line 950
    .local v20, method:Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    #@d
    :try_start_d
    check-cast v3, [Ljava/lang/Object;

    #@f
    move-object/from16 v0, v20

    #@11
    move-object/from16 v1, p1

    #@13
    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v21

    #@17
    .line 951
    .local v21, methodValue:Ljava/lang/Object;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    #@1a
    move-result-object v23

    #@1b
    .line 952
    .local v23, returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v3, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@1d
    move-object/from16 v0, v20

    #@1f
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v5

    #@23
    check-cast v5, Landroid/view/ViewDebug$ExportedProperty;

    #@25
    .line 953
    .local v5, property:Landroid/view/ViewDebug$ExportedProperty;
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->category()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_83

    #@2f
    new-instance v3, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->category()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, ":"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v9

    #@46
    .line 956
    .local v9, categoryPrefix:Ljava/lang/String;
    :goto_46
    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@48
    move-object/from16 v0, v23

    #@4a
    if-ne v0, v3, :cond_f3

    #@4c
    .line 958
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->resolveId()Z

    #@4f
    move-result v3

    #@50
    if-eqz v3, :cond_86

    #@52
    if-eqz p0, :cond_86

    #@54
    .line 959
    check-cast v21, Ljava/lang/Integer;

    #@56
    .end local v21           #methodValue:Ljava/lang/Object;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    #@59
    move-result v13

    #@5a
    .line 960
    .local v13, id:I
    move-object/from16 v0, p0

    #@5c
    invoke-static {v0, v13}, Landroid/view/ViewDebug;->resolveId(Landroid/content/Context;I)Ljava/lang/Object;

    #@5f
    move-result-object v21

    #@60
    .line 1005
    .end local v13           #id:I
    :cond_60
    :goto_60
    new-instance v3, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    move-object/from16 v0, p4

    #@6b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-virtual/range {v20 .. v20}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    #@76
    move-result-object v4

    #@77
    const-string v8, "()"

    #@79
    move-object/from16 v0, p2

    #@7b
    move-object/from16 v1, v21

    #@7d
    invoke-static {v0, v3, v4, v8, v1}, Landroid/view/ViewDebug;->writeEntry(Ljava/io/BufferedWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    #@80
    .line 945
    .end local v5           #property:Landroid/view/ViewDebug$ExportedProperty;
    .end local v9           #categoryPrefix:Ljava/lang/String;
    .end local v23           #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_80
    add-int/lit8 v12, v12, 0x1

    #@82
    goto :goto_8

    #@83
    .line 953
    .restart local v5       #property:Landroid/view/ViewDebug$ExportedProperty;
    .restart local v21       #methodValue:Ljava/lang/Object;
    .restart local v23       #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_83
    const-string v9, ""

    #@85
    goto :goto_46

    #@86
    .line 962
    .restart local v9       #categoryPrefix:Ljava/lang/String;
    :cond_86
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->flagMapping()[Landroid/view/ViewDebug$FlagToString;

    #@89
    move-result-object v11

    #@8a
    .line 963
    .local v11, flagsMapping:[Landroid/view/ViewDebug$FlagToString;
    array-length v3, v11

    #@8b
    if-lez v3, :cond_bc

    #@8d
    .line 964
    move-object/from16 v0, v21

    #@8f
    check-cast v0, Ljava/lang/Integer;

    #@91
    move-object v3, v0

    #@92
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@95
    move-result v14

    #@96
    .line 965
    .local v14, intValue:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    move-object/from16 v0, p4

    #@a1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual/range {v20 .. v20}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    #@a8
    move-result-object v4

    #@a9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v3

    #@ad
    const/16 v4, 0x5f

    #@af
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v7

    #@b7
    .line 967
    .local v7, valuePrefix:Ljava/lang/String;
    move-object/from16 v0, p2

    #@b9
    invoke-static {v0, v11, v14, v7}, Landroid/view/ViewDebug;->exportUnrolledFlags(Ljava/io/BufferedWriter;[Landroid/view/ViewDebug$FlagToString;ILjava/lang/String;)V

    #@bc
    .line 970
    .end local v7           #valuePrefix:Ljava/lang/String;
    .end local v14           #intValue:I
    :cond_bc
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->mapping()[Landroid/view/ViewDebug$IntToString;

    #@bf
    move-result-object v18

    #@c0
    .line 971
    .local v18, mapping:[Landroid/view/ViewDebug$IntToString;
    move-object/from16 v0, v18

    #@c2
    array-length v3, v0

    #@c3
    if-lez v3, :cond_60

    #@c5
    .line 972
    move-object/from16 v0, v21

    #@c7
    check-cast v0, Ljava/lang/Integer;

    #@c9
    move-object v3, v0

    #@ca
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@cd
    move-result v14

    #@ce
    .line 973
    .restart local v14       #intValue:I
    const/16 v16, 0x0

    #@d0
    .line 974
    .local v16, mapped:Z
    move-object/from16 v0, v18

    #@d2
    array-length v0, v0

    #@d3
    move/from16 v19, v0

    #@d5
    .line 975
    .local v19, mappingCount:I
    const/4 v15, 0x0

    #@d6
    .local v15, j:I
    :goto_d6
    move/from16 v0, v19

    #@d8
    if-ge v15, v0, :cond_e8

    #@da
    .line 976
    aget-object v17, v18, v15

    #@dc
    .line 977
    .local v17, mapper:Landroid/view/ViewDebug$IntToString;
    invoke-interface/range {v17 .. v17}, Landroid/view/ViewDebug$IntToString;->from()I

    #@df
    move-result v3

    #@e0
    if-ne v3, v14, :cond_f0

    #@e2
    .line 978
    invoke-interface/range {v17 .. v17}, Landroid/view/ViewDebug$IntToString;->to()Ljava/lang/String;

    #@e5
    move-result-object v21

    #@e6
    .line 979
    .local v21, methodValue:Ljava/lang/String;
    const/16 v16, 0x1

    #@e8
    .line 984
    .end local v17           #mapper:Landroid/view/ViewDebug$IntToString;
    .end local v21           #methodValue:Ljava/lang/String;
    :cond_e8
    if-nez v16, :cond_60

    #@ea
    .line 985
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ed
    move-result-object v21

    #@ee
    .local v21, methodValue:Ljava/lang/Integer;
    goto/16 :goto_60

    #@f0
    .line 975
    .restart local v17       #mapper:Landroid/view/ViewDebug$IntToString;
    .local v21, methodValue:Ljava/lang/Object;
    :cond_f0
    add-int/lit8 v15, v15, 0x1

    #@f2
    goto :goto_d6

    #@f3
    .line 989
    .end local v11           #flagsMapping:[Landroid/view/ViewDebug$FlagToString;
    .end local v14           #intValue:I
    .end local v15           #j:I
    .end local v16           #mapped:Z
    .end local v17           #mapper:Landroid/view/ViewDebug$IntToString;
    .end local v18           #mapping:[Landroid/view/ViewDebug$IntToString;
    .end local v19           #mappingCount:I
    :cond_f3
    const-class v3, [I

    #@f5
    move-object/from16 v0, v23

    #@f7
    if-ne v0, v3, :cond_12d

    #@f9
    .line 990
    check-cast v21, [I

    #@fb
    .end local v21           #methodValue:Ljava/lang/Object;
    move-object/from16 v0, v21

    #@fd
    check-cast v0, [I

    #@ff
    move-object v6, v0

    #@100
    .line 991
    .local v6, array:[I
    new-instance v3, Ljava/lang/StringBuilder;

    #@102
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@105
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v3

    #@109
    move-object/from16 v0, p4

    #@10b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v3

    #@10f
    invoke-virtual/range {v20 .. v20}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    #@112
    move-result-object v4

    #@113
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v3

    #@117
    const/16 v4, 0x5f

    #@119
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v3

    #@11d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v7

    #@121
    .line 992
    .restart local v7       #valuePrefix:Ljava/lang/String;
    const-string v24, "()"

    #@123
    .line 994
    .local v24, suffix:Ljava/lang/String;
    const-string v8, "()"

    #@125
    move-object/from16 v3, p0

    #@127
    move-object/from16 v4, p2

    #@129
    invoke-static/range {v3 .. v8}, Landroid/view/ViewDebug;->exportUnrolledArray(Landroid/content/Context;Ljava/io/BufferedWriter;Landroid/view/ViewDebug$ExportedProperty;[ILjava/lang/String;Ljava/lang/String;)V

    #@12c
    .line 1010
    .end local v5           #property:Landroid/view/ViewDebug$ExportedProperty;
    .end local v6           #array:[I
    .end local v7           #valuePrefix:Ljava/lang/String;
    .end local v9           #categoryPrefix:Ljava/lang/String;
    .end local v20           #method:Ljava/lang/reflect/Method;
    .end local v23           #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v24           #suffix:Ljava/lang/String;
    :cond_12c
    return-void

    #@12d
    .line 998
    .restart local v5       #property:Landroid/view/ViewDebug$ExportedProperty;
    .restart local v9       #categoryPrefix:Ljava/lang/String;
    .restart local v20       #method:Ljava/lang/reflect/Method;
    .restart local v21       #methodValue:Ljava/lang/Object;
    .restart local v23       #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_12d
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->isPrimitive()Z

    #@130
    move-result v3

    #@131
    if-nez v3, :cond_60

    #@133
    .line 999
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->deepExport()Z

    #@136
    move-result v3

    #@137
    if-eqz v3, :cond_60

    #@139
    .line 1000
    new-instance v3, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    move-object/from16 v0, p4

    #@140
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v3

    #@144
    invoke-interface {v5}, Landroid/view/ViewDebug$ExportedProperty;->prefix()Ljava/lang/String;

    #@147
    move-result-object v4

    #@148
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v3

    #@14c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v3

    #@150
    move-object/from16 v0, p0

    #@152
    move-object/from16 v1, v21

    #@154
    move-object/from16 v2, p2

    #@156
    invoke-static {v0, v1, v2, v3}, Landroid/view/ViewDebug;->dumpViewProperties(Landroid/content/Context;Ljava/lang/Object;Ljava/io/BufferedWriter;Ljava/lang/String;)V
    :try_end_159
    .catch Ljava/lang/IllegalAccessException; {:try_start_d .. :try_end_159} :catch_15b
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_d .. :try_end_159} :catch_15e

    #@159
    goto/16 :goto_80

    #@15b
    .line 1006
    .end local v5           #property:Landroid/view/ViewDebug$ExportedProperty;
    .end local v9           #categoryPrefix:Ljava/lang/String;
    .end local v21           #methodValue:Ljava/lang/Object;
    .end local v23           #returnType:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_15b
    move-exception v3

    #@15c
    goto/16 :goto_80

    #@15e
    .line 1007
    :catch_15e
    move-exception v3

    #@15f
    goto/16 :goto_80
.end method

.method private static exportUnrolledArray(Landroid/content/Context;Ljava/io/BufferedWriter;Landroid/view/ViewDebug$ExportedProperty;[ILjava/lang/String;Ljava/lang/String;)V
    .registers 23
    .parameter "context"
    .parameter "out"
    .parameter "property"
    .parameter "array"
    .parameter "prefix"
    .parameter "suffix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1119
    invoke-interface/range {p2 .. p2}, Landroid/view/ViewDebug$ExportedProperty;->indexMapping()[Landroid/view/ViewDebug$IntToString;

    #@3
    move-result-object v5

    #@4
    .line 1120
    .local v5, indexMapping:[Landroid/view/ViewDebug$IntToString;
    array-length v0, v5

    #@5
    move/from16 v16, v0

    #@7
    if-lez v16, :cond_6a

    #@9
    const/4 v3, 0x1

    #@a
    .line 1122
    .local v3, hasIndexMapping:Z
    :goto_a
    invoke-interface/range {p2 .. p2}, Landroid/view/ViewDebug$ExportedProperty;->mapping()[Landroid/view/ViewDebug$IntToString;

    #@d
    move-result-object v10

    #@e
    .line 1123
    .local v10, mapping:[Landroid/view/ViewDebug$IntToString;
    array-length v0, v10

    #@f
    move/from16 v16, v0

    #@11
    if-lez v16, :cond_6c

    #@13
    const/4 v4, 0x1

    #@14
    .line 1125
    .local v4, hasMapping:Z
    :goto_14
    invoke-interface/range {p2 .. p2}, Landroid/view/ViewDebug$ExportedProperty;->resolveId()Z

    #@17
    move-result v16

    #@18
    if-eqz v16, :cond_6e

    #@1a
    if-eqz p0, :cond_6e

    #@1c
    const/4 v13, 0x1

    #@1d
    .line 1126
    .local v13, resolveId:Z
    :goto_1d
    move-object/from16 v0, p3

    #@1f
    array-length v15, v0

    #@20
    .line 1128
    .local v15, valuesCount:I
    const/4 v7, 0x0

    #@21
    .local v7, j:I
    :goto_21
    if-ge v7, v15, :cond_7b

    #@23
    .line 1130
    const/4 v14, 0x0

    #@24
    .line 1132
    .local v14, value:Ljava/lang/String;
    aget v6, p3, v7

    #@26
    .line 1134
    .local v6, intValue:I
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@29
    move-result-object v12

    #@2a
    .line 1135
    .local v12, name:Ljava/lang/String;
    if-eqz v3, :cond_3e

    #@2c
    .line 1136
    array-length v11, v5

    #@2d
    .line 1137
    .local v11, mappingCount:I
    const/4 v8, 0x0

    #@2e
    .local v8, k:I
    :goto_2e
    if-ge v8, v11, :cond_3e

    #@30
    .line 1138
    aget-object v9, v5, v8

    #@32
    .line 1139
    .local v9, mapped:Landroid/view/ViewDebug$IntToString;
    invoke-interface {v9}, Landroid/view/ViewDebug$IntToString;->from()I

    #@35
    move-result v16

    #@36
    move/from16 v0, v16

    #@38
    if-ne v0, v7, :cond_70

    #@3a
    .line 1140
    invoke-interface {v9}, Landroid/view/ViewDebug$IntToString;->to()Ljava/lang/String;

    #@3d
    move-result-object v12

    #@3e
    .line 1146
    .end local v8           #k:I
    .end local v9           #mapped:Landroid/view/ViewDebug$IntToString;
    .end local v11           #mappingCount:I
    :cond_3e
    if-eqz v4, :cond_52

    #@40
    .line 1147
    array-length v11, v10

    #@41
    .line 1148
    .restart local v11       #mappingCount:I
    const/4 v8, 0x0

    #@42
    .restart local v8       #k:I
    :goto_42
    if-ge v8, v11, :cond_52

    #@44
    .line 1149
    aget-object v9, v10, v8

    #@46
    .line 1150
    .restart local v9       #mapped:Landroid/view/ViewDebug$IntToString;
    invoke-interface {v9}, Landroid/view/ViewDebug$IntToString;->from()I

    #@49
    move-result v16

    #@4a
    move/from16 v0, v16

    #@4c
    if-ne v0, v6, :cond_73

    #@4e
    .line 1151
    invoke-interface {v9}, Landroid/view/ViewDebug$IntToString;->to()Ljava/lang/String;

    #@51
    move-result-object v14

    #@52
    .line 1157
    .end local v8           #k:I
    .end local v9           #mapped:Landroid/view/ViewDebug$IntToString;
    .end local v11           #mappingCount:I
    :cond_52
    if-eqz v13, :cond_76

    #@54
    .line 1158
    if-nez v14, :cond_5e

    #@56
    move-object/from16 v0, p0

    #@58
    invoke-static {v0, v6}, Landroid/view/ViewDebug;->resolveId(Landroid/content/Context;I)Ljava/lang/Object;

    #@5b
    move-result-object v14

    #@5c
    .end local v14           #value:Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    #@5e
    .line 1163
    .restart local v14       #value:Ljava/lang/String;
    :cond_5e
    :goto_5e
    move-object/from16 v0, p1

    #@60
    move-object/from16 v1, p4

    #@62
    move-object/from16 v2, p5

    #@64
    invoke-static {v0, v1, v12, v2, v14}, Landroid/view/ViewDebug;->writeEntry(Ljava/io/BufferedWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    #@67
    .line 1128
    add-int/lit8 v7, v7, 0x1

    #@69
    goto :goto_21

    #@6a
    .line 1120
    .end local v3           #hasIndexMapping:Z
    .end local v4           #hasMapping:Z
    .end local v6           #intValue:I
    .end local v7           #j:I
    .end local v10           #mapping:[Landroid/view/ViewDebug$IntToString;
    .end local v12           #name:Ljava/lang/String;
    .end local v13           #resolveId:Z
    .end local v14           #value:Ljava/lang/String;
    .end local v15           #valuesCount:I
    :cond_6a
    const/4 v3, 0x0

    #@6b
    goto :goto_a

    #@6c
    .line 1123
    .restart local v3       #hasIndexMapping:Z
    .restart local v10       #mapping:[Landroid/view/ViewDebug$IntToString;
    :cond_6c
    const/4 v4, 0x0

    #@6d
    goto :goto_14

    #@6e
    .line 1125
    .restart local v4       #hasMapping:Z
    :cond_6e
    const/4 v13, 0x0

    #@6f
    goto :goto_1d

    #@70
    .line 1137
    .restart local v6       #intValue:I
    .restart local v7       #j:I
    .restart local v8       #k:I
    .restart local v9       #mapped:Landroid/view/ViewDebug$IntToString;
    .restart local v11       #mappingCount:I
    .restart local v12       #name:Ljava/lang/String;
    .restart local v13       #resolveId:Z
    .restart local v14       #value:Ljava/lang/String;
    .restart local v15       #valuesCount:I
    :cond_70
    add-int/lit8 v8, v8, 0x1

    #@72
    goto :goto_2e

    #@73
    .line 1148
    :cond_73
    add-int/lit8 v8, v8, 0x1

    #@75
    goto :goto_42

    #@76
    .line 1160
    .end local v8           #k:I
    .end local v9           #mapped:Landroid/view/ViewDebug$IntToString;
    .end local v11           #mappingCount:I
    :cond_76
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@79
    move-result-object v14

    #@7a
    goto :goto_5e

    #@7b
    .line 1165
    .end local v6           #intValue:I
    .end local v12           #name:Ljava/lang/String;
    .end local v14           #value:Ljava/lang/String;
    :cond_7b
    return-void
.end method

.method private static exportUnrolledFlags(Ljava/io/BufferedWriter;[Landroid/view/ViewDebug$FlagToString;ILjava/lang/String;)V
    .registers 14
    .parameter "out"
    .parameter "mapping"
    .parameter "intValue"
    .parameter "prefix"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1101
    array-length v0, p1

    #@1
    .line 1102
    .local v0, count:I
    const/4 v3, 0x0

    #@2
    .local v3, j:I
    :goto_2
    if-ge v3, v0, :cond_44

    #@4
    .line 1103
    aget-object v1, p1, v3

    #@6
    .line 1104
    .local v1, flagMapping:Landroid/view/ViewDebug$FlagToString;
    invoke-interface {v1}, Landroid/view/ViewDebug$FlagToString;->outputIf()Z

    #@9
    move-result v2

    #@a
    .line 1105
    .local v2, ifTrue:Z
    invoke-interface {v1}, Landroid/view/ViewDebug$FlagToString;->mask()I

    #@d
    move-result v8

    #@e
    and-int v4, p2, v8

    #@10
    .line 1106
    .local v4, maskResult:I
    invoke-interface {v1}, Landroid/view/ViewDebug$FlagToString;->equals()I

    #@13
    move-result v8

    #@14
    if-ne v4, v8, :cond_42

    #@16
    const/4 v6, 0x1

    #@17
    .line 1107
    .local v6, test:Z
    :goto_17
    if-eqz v6, :cond_1b

    #@19
    if-nez v2, :cond_1f

    #@1b
    :cond_1b
    if-nez v6, :cond_3f

    #@1d
    if-nez v2, :cond_3f

    #@1f
    .line 1108
    :cond_1f
    invoke-interface {v1}, Landroid/view/ViewDebug$FlagToString;->name()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    .line 1109
    .local v5, name:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v9, "0x"

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@31
    move-result-object v9

    #@32
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v8

    #@36
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v7

    #@3a
    .line 1110
    .local v7, value:Ljava/lang/String;
    const-string v8, ""

    #@3c
    invoke-static {p0, p3, v5, v8, v7}, Landroid/view/ViewDebug;->writeEntry(Ljava/io/BufferedWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    #@3f
    .line 1102
    .end local v5           #name:Ljava/lang/String;
    .end local v7           #value:Ljava/lang/String;
    :cond_3f
    add-int/lit8 v3, v3, 0x1

    #@41
    goto :goto_2

    #@42
    .line 1106
    .end local v6           #test:Z
    :cond_42
    const/4 v6, 0x0

    #@43
    goto :goto_17

    #@44
    .line 1113
    .end local v1           #flagMapping:Landroid/view/ViewDebug$FlagToString;
    .end local v2           #ifTrue:Z
    .end local v4           #maskResult:I
    :cond_44
    return-void
.end method

.method private static findView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
    .registers 10
    .parameter "root"
    .parameter "parameter"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 430
    const/16 v6, 0x40

    #@3
    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(I)I

    #@6
    move-result v6

    #@7
    const/4 v7, -0x1

    #@8
    if-eq v6, v7, :cond_2c

    #@a
    .line 431
    const-string v6, "@"

    #@c
    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    .line 432
    .local v3, ids:[Ljava/lang/String;
    const/4 v6, 0x0

    #@11
    aget-object v0, v3, v6

    #@13
    .line 433
    .local v0, className:Ljava/lang/String;
    const/4 v6, 0x1

    #@14
    aget-object v6, v3, v6

    #@16
    const/16 v7, 0x10

    #@18
    invoke-static {v6, v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@1b
    move-result-wide v6

    #@1c
    long-to-int v1, v6

    #@1d
    .line 435
    .local v1, hashCode:I
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@20
    move-result-object v4

    #@21
    .line 436
    .local v4, view:Landroid/view/View;
    instance-of v6, v4, Landroid/view/ViewGroup;

    #@23
    if-eqz v6, :cond_2b

    #@25
    .line 437
    check-cast v4, Landroid/view/ViewGroup;

    #@27
    .end local v4           #view:Landroid/view/View;
    invoke-static {v4, v0, v1}, Landroid/view/ViewDebug;->findView(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/view/View;

    #@2a
    move-result-object v5

    #@2b
    .line 445
    .end local v0           #className:Ljava/lang/String;
    .end local v1           #hashCode:I
    .end local v3           #ids:[Ljava/lang/String;
    :cond_2b
    :goto_2b
    return-object v5

    #@2c
    .line 441
    :cond_2c
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6, p1, v5, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@33
    move-result v2

    #@34
    .line 442
    .local v2, id:I
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3b
    move-result-object v5

    #@3c
    goto :goto_2b
.end method

.method private static findView(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/view/View;
    .registers 8
    .parameter "group"
    .parameter "className"
    .parameter "hashCode"

    #@0
    .prologue
    .line 776
    invoke-static {p0, p1, p2}, Landroid/view/ViewDebug;->isRequestedView(Landroid/view/View;Ljava/lang/String;I)Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_8

    #@6
    move-object v1, p0

    #@7
    .line 800
    :cond_7
    :goto_7
    return-object v1

    #@8
    .line 780
    :cond_8
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 781
    .local v0, count:I
    const/4 v2, 0x0

    #@d
    .local v2, i:I
    :goto_d
    if-ge v2, v0, :cond_37

    #@f
    .line 782
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@12
    move-result-object v3

    #@13
    .line 783
    .local v3, view:Landroid/view/View;
    instance-of v4, v3, Landroid/view/ViewGroup;

    #@15
    if-eqz v4, :cond_2f

    #@17
    move-object v4, v3

    #@18
    .line 784
    check-cast v4, Landroid/view/ViewGroup;

    #@1a
    invoke-static {v4, p1, p2}, Landroid/view/ViewDebug;->findView(Landroid/view/ViewGroup;Ljava/lang/String;I)Landroid/view/View;

    #@1d
    move-result-object v1

    #@1e
    .line 785
    .local v1, found:Landroid/view/View;
    if-nez v1, :cond_7

    #@20
    .line 791
    .end local v1           #found:Landroid/view/View;
    :cond_20
    instance-of v4, v3, Landroid/view/ViewDebug$HierarchyHandler;

    #@22
    if-eqz v4, :cond_2c

    #@24
    .line 792
    check-cast v3, Landroid/view/ViewDebug$HierarchyHandler;

    #@26
    .end local v3           #view:Landroid/view/View;
    invoke-interface {v3, p1, p2}, Landroid/view/ViewDebug$HierarchyHandler;->findHierarchyView(Ljava/lang/String;I)Landroid/view/View;

    #@29
    move-result-object v1

    #@2a
    .line 794
    .restart local v1       #found:Landroid/view/View;
    if-nez v1, :cond_7

    #@2c
    .line 781
    .end local v1           #found:Landroid/view/View;
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_d

    #@2f
    .line 788
    .restart local v3       #view:Landroid/view/View;
    :cond_2f
    invoke-static {v3, p1, p2}, Landroid/view/ViewDebug;->isRequestedView(Landroid/view/View;Ljava/lang/String;I)Z

    #@32
    move-result v4

    #@33
    if-eqz v4, :cond_20

    #@35
    move-object v1, v3

    #@36
    .line 789
    goto :goto_7

    #@37
    .line 800
    .end local v3           #view:Landroid/view/View;
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_7
.end method

.method private static getExportedPropertyFields(Ljava/lang/Class;)[Ljava/lang/reflect/Field;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    #@0
    .prologue
    .line 848
    .local p0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v7, Landroid/view/ViewDebug;->sFieldsForClasses:Ljava/util/HashMap;

    #@2
    if-nez v7, :cond_b

    #@4
    .line 849
    new-instance v7, Ljava/util/HashMap;

    #@6
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@9
    sput-object v7, Landroid/view/ViewDebug;->sFieldsForClasses:Ljava/util/HashMap;

    #@b
    .line 851
    :cond_b
    sget-object v7, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@d
    if-nez v7, :cond_18

    #@f
    .line 852
    new-instance v7, Ljava/util/HashMap;

    #@11
    const/16 v8, 0x200

    #@13
    invoke-direct {v7, v8}, Ljava/util/HashMap;-><init>(I)V

    #@16
    sput-object v7, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@18
    .line 855
    :cond_18
    sget-object v6, Landroid/view/ViewDebug;->sFieldsForClasses:Ljava/util/HashMap;

    #@1a
    .line 857
    .local v6, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Class<*>;[Ljava/lang/reflect/Field;>;"
    invoke-virtual {v6, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, [Ljava/lang/reflect/Field;

    #@20
    .line 858
    .local v2, fields:[Ljava/lang/reflect/Field;
    if-eqz v2, :cond_24

    #@22
    move-object v3, v2

    #@23
    .line 878
    .end local v2           #fields:[Ljava/lang/reflect/Field;
    .local v3, fields:[Ljava/lang/reflect/Field;
    :goto_23
    return-object v3

    #@24
    .line 862
    .end local v3           #fields:[Ljava/lang/reflect/Field;
    .restart local v2       #fields:[Ljava/lang/reflect/Field;
    :cond_24
    new-instance v4, Ljava/util/ArrayList;

    #@26
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@29
    .line 863
    .local v4, foundFields:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/reflect/Field;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    #@2c
    move-result-object v2

    #@2d
    .line 865
    array-length v0, v2

    #@2e
    .line 866
    .local v0, count:I
    const/4 v5, 0x0

    #@2f
    .local v5, i:I
    :goto_2f
    if-ge v5, v0, :cond_50

    #@31
    .line 867
    aget-object v1, v2, v5

    #@33
    .line 868
    .local v1, field:Ljava/lang/reflect/Field;
    const-class v7, Landroid/view/ViewDebug$ExportedProperty;

    #@35
    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@38
    move-result v7

    #@39
    if-eqz v7, :cond_4d

    #@3b
    .line 869
    const/4 v7, 0x1

    #@3c
    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    #@3f
    .line 870
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    .line 871
    sget-object v7, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@44
    const-class v8, Landroid/view/ViewDebug$ExportedProperty;

    #@46
    invoke-virtual {v1, v8}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@49
    move-result-object v8

    #@4a
    invoke-virtual {v7, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    .line 866
    :cond_4d
    add-int/lit8 v5, v5, 0x1

    #@4f
    goto :goto_2f

    #@50
    .line 875
    .end local v1           #field:Ljava/lang/reflect/Field;
    :cond_50
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v7

    #@54
    new-array v7, v7, [Ljava/lang/reflect/Field;

    #@56
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@59
    move-result-object v2

    #@5a
    .end local v2           #fields:[Ljava/lang/reflect/Field;
    check-cast v2, [Ljava/lang/reflect/Field;

    #@5c
    .line 876
    .restart local v2       #fields:[Ljava/lang/reflect/Field;
    invoke-virtual {v6, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5f
    move-object v3, v2

    #@60
    .line 878
    .end local v2           #fields:[Ljava/lang/reflect/Field;
    .restart local v3       #fields:[Ljava/lang/reflect/Field;
    goto :goto_23
.end method

.method private static getExportedPropertyMethods(Ljava/lang/Class;)[Ljava/lang/reflect/Method;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    #@0
    .prologue
    .line 882
    .local p0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v7, Landroid/view/ViewDebug;->sMethodsForClasses:Ljava/util/HashMap;

    #@2
    if-nez v7, :cond_d

    #@4
    .line 883
    new-instance v7, Ljava/util/HashMap;

    #@6
    const/16 v8, 0x64

    #@8
    invoke-direct {v7, v8}, Ljava/util/HashMap;-><init>(I)V

    #@b
    sput-object v7, Landroid/view/ViewDebug;->sMethodsForClasses:Ljava/util/HashMap;

    #@d
    .line 885
    :cond_d
    sget-object v7, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@f
    if-nez v7, :cond_1a

    #@11
    .line 886
    new-instance v7, Ljava/util/HashMap;

    #@13
    const/16 v8, 0x200

    #@15
    invoke-direct {v7, v8}, Ljava/util/HashMap;-><init>(I)V

    #@18
    sput-object v7, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@1a
    .line 889
    :cond_1a
    sget-object v3, Landroid/view/ViewDebug;->sMethodsForClasses:Ljava/util/HashMap;

    #@1c
    .line 891
    .local v3, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Class<*>;[Ljava/lang/reflect/Method;>;"
    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v5

    #@20
    check-cast v5, [Ljava/lang/reflect/Method;

    #@22
    .line 892
    .local v5, methods:[Ljava/lang/reflect/Method;
    if-eqz v5, :cond_26

    #@24
    move-object v6, v5

    #@25
    .line 914
    .end local v5           #methods:[Ljava/lang/reflect/Method;
    .local v6, methods:[Ljava/lang/reflect/Method;
    :goto_25
    return-object v6

    #@26
    .line 896
    .end local v6           #methods:[Ljava/lang/reflect/Method;
    .restart local v5       #methods:[Ljava/lang/reflect/Method;
    :cond_26
    new-instance v1, Ljava/util/ArrayList;

    #@28
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@2b
    .line 897
    .local v1, foundMethods:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/reflect/Method;>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    #@2e
    move-result-object v5

    #@2f
    .line 899
    array-length v0, v5

    #@30
    .line 900
    .local v0, count:I
    const/4 v2, 0x0

    #@31
    .local v2, i:I
    :goto_31
    if-ge v2, v0, :cond_61

    #@33
    .line 901
    aget-object v4, v5, v2

    #@35
    .line 902
    .local v4, method:Ljava/lang/reflect/Method;
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    #@38
    move-result-object v7

    #@39
    array-length v7, v7

    #@3a
    if-nez v7, :cond_5e

    #@3c
    const-class v7, Landroid/view/ViewDebug$ExportedProperty;

    #@3e
    invoke-virtual {v4, v7}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@41
    move-result v7

    #@42
    if-eqz v7, :cond_5e

    #@44
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    #@47
    move-result-object v7

    #@48
    const-class v8, Ljava/lang/Void;

    #@4a
    if-eq v7, v8, :cond_5e

    #@4c
    .line 905
    const/4 v7, 0x1

    #@4d
    invoke-virtual {v4, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    #@50
    .line 906
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53
    .line 907
    sget-object v7, Landroid/view/ViewDebug;->sAnnotations:Ljava/util/HashMap;

    #@55
    const-class v8, Landroid/view/ViewDebug$ExportedProperty;

    #@57
    invoke-virtual {v4, v8}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@5a
    move-result-object v8

    #@5b
    invoke-virtual {v7, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5e
    .line 900
    :cond_5e
    add-int/lit8 v2, v2, 0x1

    #@60
    goto :goto_31

    #@61
    .line 911
    .end local v4           #method:Ljava/lang/reflect/Method;
    :cond_61
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@64
    move-result v7

    #@65
    new-array v7, v7, [Ljava/lang/reflect/Method;

    #@67
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@6a
    move-result-object v5

    #@6b
    .end local v5           #methods:[Ljava/lang/reflect/Method;
    check-cast v5, [Ljava/lang/reflect/Method;

    #@6d
    .line 912
    .restart local v5       #methods:[Ljava/lang/reflect/Method;
    invoke-virtual {v3, p0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@70
    move-object v6, v5

    #@71
    .line 914
    .end local v5           #methods:[Ljava/lang/reflect/Method;
    .restart local v6       #methods:[Ljava/lang/reflect/Method;
    goto :goto_25
.end method

.method public static getViewInstanceCount()J
    .registers 2

    #@0
    .prologue
    .line 341
    const-class v0, Landroid/view/View;

    #@2
    invoke-static {v0}, Landroid/os/Debug;->countInstancesOfClass(Ljava/lang/Class;)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public static getViewRootImplCount()J
    .registers 2

    #@0
    .prologue
    .line 352
    const-class v0, Landroid/view/ViewRootImpl;

    #@2
    invoke-static {v0}, Landroid/os/Debug;->countInstancesOfClass(Ljava/lang/Class;)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method private static invalidate(Landroid/view/View;Ljava/lang/String;)V
    .registers 3
    .parameter "root"
    .parameter "parameter"

    #@0
    .prologue
    .line 449
    invoke-static {p0, p1}, Landroid/view/ViewDebug;->findView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 450
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_9

    #@6
    .line 451
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    #@9
    .line 453
    :cond_9
    return-void
.end method

.method private static isRequestedView(Landroid/view/View;Ljava/lang/String;I)Z
    .registers 4
    .parameter "view"
    .parameter "className"
    .parameter "hashCode"

    #@0
    .prologue
    .line 804
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_16

    #@e
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@11
    move-result v0

    #@12
    if-ne v0, p2, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method private static outputDisplayList(Landroid/view/View;Ljava/lang/String;)V
    .registers 4
    .parameter "root"
    .parameter "parameter"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 694
    invoke-static {p0, p1}, Landroid/view/ViewDebug;->findView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 695
    .local v0, view:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, v0}, Landroid/view/ViewRootImpl;->outputDisplayList(Landroid/view/View;)V

    #@b
    .line 696
    return-void
.end method

.method private static performViewCapture(Landroid/view/View;Z)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "captureView"
    .parameter "skpiChildren"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 726
    if-eqz p0, :cond_3d

    #@3
    .line 727
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    #@5
    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@8
    .line 728
    .local v2, latch:Ljava/util/concurrent/CountDownLatch;
    new-array v0, v3, [Landroid/graphics/Bitmap;

    #@a
    .line 730
    .local v0, cache:[Landroid/graphics/Bitmap;
    new-instance v3, Landroid/view/ViewDebug$6;

    #@c
    invoke-direct {v3, v0, p0, p1, v2}, Landroid/view/ViewDebug$6;-><init>([Landroid/graphics/Bitmap;Landroid/view/View;ZLjava/util/concurrent/CountDownLatch;)V

    #@f
    invoke-virtual {p0, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@12
    .line 744
    const-wide/16 v3, 0xfa0

    #@14
    :try_start_14
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@16
    invoke-virtual {v2, v3, v4, v5}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@19
    .line 745
    const/4 v3, 0x0

    #@1a
    aget-object v3, v0, v3
    :try_end_1c
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_1c} :catch_1d

    #@1c
    .line 752
    .end local v0           #cache:[Landroid/graphics/Bitmap;
    .end local v2           #latch:Ljava/util/concurrent/CountDownLatch;
    :goto_1c
    return-object v3

    #@1d
    .line 746
    .restart local v0       #cache:[Landroid/graphics/Bitmap;
    .restart local v2       #latch:Ljava/util/concurrent/CountDownLatch;
    :catch_1d
    move-exception v1

    #@1e
    .line 747
    .local v1, e:Ljava/lang/InterruptedException;
    const-string v3, "View"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Could not complete the capture of the view "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 748
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    #@3d
    .line 752
    .end local v0           #cache:[Landroid/graphics/Bitmap;
    .end local v1           #e:Ljava/lang/InterruptedException;
    .end local v2           #latch:Ljava/util/concurrent/CountDownLatch;
    :cond_3d
    const/4 v3, 0x0

    #@3e
    goto :goto_1c
.end method

.method private static profile(Landroid/view/View;Ljava/io/OutputStream;Ljava/lang/String;)V
    .registers 9
    .parameter "root"
    .parameter "clientStream"
    .parameter "parameter"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 469
    invoke-static {p0, p2}, Landroid/view/ViewDebug;->findView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    #@3
    move-result-object v3

    #@4
    .line 470
    .local v3, view:Landroid/view/View;
    const/4 v1, 0x0

    #@5
    .line 472
    .local v1, out:Ljava/io/BufferedWriter;
    :try_start_5
    new-instance v2, Ljava/io/BufferedWriter;

    #@7
    new-instance v4, Ljava/io/OutputStreamWriter;

    #@9
    invoke-direct {v4, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@c
    const v5, 0x8000

    #@f
    invoke-direct {v2, v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_12
    .catchall {:try_start_5 .. :try_end_12} :catchall_3e
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_12} :catch_48

    #@12
    .line 474
    .end local v1           #out:Ljava/io/BufferedWriter;
    .local v2, out:Ljava/io/BufferedWriter;
    if-eqz v3, :cond_26

    #@14
    .line 475
    :try_start_14
    invoke-static {v3, v2}, Landroid/view/ViewDebug;->profileViewAndChildren(Landroid/view/View;Ljava/io/BufferedWriter;)V

    #@17
    .line 480
    :goto_17
    const-string v4, "DONE."

    #@19
    invoke-virtual {v2, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@1c
    .line 481
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_1f
    .catchall {:try_start_14 .. :try_end_1f} :catchall_45
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_1f} :catch_2f

    #@1f
    .line 485
    if-eqz v2, :cond_4a

    #@21
    .line 486
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V

    #@24
    move-object v1, v2

    #@25
    .line 489
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    :cond_25
    :goto_25
    return-void

    #@26
    .line 477
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :cond_26
    :try_start_26
    const-string v4, "-1 -1 -1"

    #@28
    invoke-virtual {v2, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@2b
    .line 478
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_2e
    .catchall {:try_start_26 .. :try_end_2e} :catchall_45
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_17

    #@2f
    .line 482
    :catch_2f
    move-exception v0

    #@30
    move-object v1, v2

    #@31
    .line 483
    .end local v2           #out:Ljava/io/BufferedWriter;
    .local v0, e:Ljava/lang/Exception;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    :goto_31
    :try_start_31
    const-string v4, "View"

    #@33
    const-string v5, "Problem profiling the view:"

    #@35
    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_38
    .catchall {:try_start_31 .. :try_end_38} :catchall_3e

    #@38
    .line 485
    if-eqz v1, :cond_25

    #@3a
    .line 486
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    #@3d
    goto :goto_25

    #@3e
    .line 485
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_3e
    move-exception v4

    #@3f
    :goto_3f
    if-eqz v1, :cond_44

    #@41
    .line 486
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    #@44
    :cond_44
    throw v4

    #@45
    .line 485
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :catchall_45
    move-exception v4

    #@46
    move-object v1, v2

    #@47
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    goto :goto_3f

    #@48
    .line 482
    :catch_48
    move-exception v0

    #@49
    goto :goto_31

    #@4a
    .end local v1           #out:Ljava/io/BufferedWriter;
    .restart local v2       #out:Ljava/io/BufferedWriter;
    :cond_4a
    move-object v1, v2

    #@4b
    .end local v2           #out:Ljava/io/BufferedWriter;
    .restart local v1       #out:Ljava/io/BufferedWriter;
    goto :goto_25
.end method

.method private static profileViewAndChildren(Landroid/view/View;Ljava/io/BufferedWriter;)V
    .registers 3
    .parameter "view"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 493
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, v0}, Landroid/view/ViewDebug;->profileViewAndChildren(Landroid/view/View;Ljava/io/BufferedWriter;Z)V

    #@4
    .line 494
    return-void
.end method

.method private static profileViewAndChildren(Landroid/view/View;Ljava/io/BufferedWriter;Z)V
    .registers 14
    .parameter "view"
    .parameter "out"
    .parameter "root"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v10, 0x20

    #@2
    const-wide/16 v1, 0x0

    #@4
    .line 499
    if-nez p2, :cond_c

    #@6
    iget v9, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    and-int/lit16 v9, v9, 0x800

    #@a
    if-eqz v9, :cond_74

    #@c
    :cond_c
    new-instance v9, Landroid/view/ViewDebug$2;

    #@e
    invoke-direct {v9, p0}, Landroid/view/ViewDebug$2;-><init>(Landroid/view/View;)V

    #@11
    invoke-static {p0, v9}, Landroid/view/ViewDebug;->profileViewOperation(Landroid/view/View;Landroid/view/ViewDebug$ViewOperation;)J

    #@14
    move-result-wide v5

    #@15
    .line 526
    .local v5, durationMeasure:J
    :goto_15
    if-nez p2, :cond_1d

    #@17
    iget v9, p0, Landroid/view/View;->mPrivateFlags:I

    #@19
    and-int/lit16 v9, v9, 0x2000

    #@1b
    if-eqz v9, :cond_76

    #@1d
    :cond_1d
    new-instance v9, Landroid/view/ViewDebug$3;

    #@1f
    invoke-direct {v9, p0}, Landroid/view/ViewDebug$3;-><init>(Landroid/view/View;)V

    #@22
    invoke-static {p0, v9}, Landroid/view/ViewDebug;->profileViewOperation(Landroid/view/View;Landroid/view/ViewDebug$ViewOperation;)J

    #@25
    move-result-wide v3

    #@26
    .line 540
    .local v3, durationLayout:J
    :goto_26
    if-nez p2, :cond_34

    #@28
    invoke-virtual {p0}, Landroid/view/View;->willNotDraw()Z

    #@2b
    move-result v9

    #@2c
    if-eqz v9, :cond_34

    #@2e
    iget v9, p0, Landroid/view/View;->mPrivateFlags:I

    #@30
    and-int/lit8 v9, v9, 0x20

    #@32
    if-eqz v9, :cond_3d

    #@34
    :cond_34
    new-instance v9, Landroid/view/ViewDebug$4;

    #@36
    invoke-direct {v9, p0}, Landroid/view/ViewDebug$4;-><init>(Landroid/view/View;)V

    #@39
    invoke-static {p0, v9}, Landroid/view/ViewDebug;->profileViewOperation(Landroid/view/View;Landroid/view/ViewDebug$ViewOperation;)J

    #@3c
    move-result-wide v1

    #@3d
    .line 571
    .local v1, durationDraw:J
    :cond_3d
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {p1, v9}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@44
    .line 572
    invoke-virtual {p1, v10}, Ljava/io/BufferedWriter;->write(I)V

    #@47
    .line 573
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {p1, v9}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@4e
    .line 574
    invoke-virtual {p1, v10}, Ljava/io/BufferedWriter;->write(I)V

    #@51
    .line 575
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@54
    move-result-object v9

    #@55
    invoke-virtual {p1, v9}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@58
    .line 576
    invoke-virtual {p1}, Ljava/io/BufferedWriter;->newLine()V

    #@5b
    .line 577
    instance-of v9, p0, Landroid/view/ViewGroup;

    #@5d
    if-eqz v9, :cond_78

    #@5f
    move-object v7, p0

    #@60
    .line 578
    check-cast v7, Landroid/view/ViewGroup;

    #@62
    .line 579
    .local v7, group:Landroid/view/ViewGroup;
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    #@65
    move-result v0

    #@66
    .line 580
    .local v0, count:I
    const/4 v8, 0x0

    #@67
    .local v8, i:I
    :goto_67
    if-ge v8, v0, :cond_78

    #@69
    .line 581
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@6c
    move-result-object v9

    #@6d
    const/4 v10, 0x0

    #@6e
    invoke-static {v9, p1, v10}, Landroid/view/ViewDebug;->profileViewAndChildren(Landroid/view/View;Ljava/io/BufferedWriter;Z)V

    #@71
    .line 580
    add-int/lit8 v8, v8, 0x1

    #@73
    goto :goto_67

    #@74
    .end local v0           #count:I
    .end local v1           #durationDraw:J
    .end local v3           #durationLayout:J
    .end local v5           #durationMeasure:J
    .end local v7           #group:Landroid/view/ViewGroup;
    .end local v8           #i:I
    :cond_74
    move-wide v5, v1

    #@75
    .line 499
    goto :goto_15

    #@76
    .restart local v5       #durationMeasure:J
    :cond_76
    move-wide v3, v1

    #@77
    .line 526
    goto :goto_26

    #@78
    .line 584
    .restart local v1       #durationDraw:J
    .restart local v3       #durationLayout:J
    :cond_78
    return-void
.end method

.method private static profileViewOperation(Landroid/view/View;Landroid/view/ViewDebug$ViewOperation;)J
    .registers 10
    .parameter "view"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "Landroid/view/ViewDebug$ViewOperation",
            "<TT;>;)J"
        }
    .end annotation

    #@0
    .prologue
    .local p1, operation:Landroid/view/ViewDebug$ViewOperation;,"Landroid/view/ViewDebug$ViewOperation<TT;>;"
    const-wide/16 v3, -0x1

    #@2
    const/4 v5, 0x1

    #@3
    .line 593
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    #@5
    invoke-direct {v2, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    #@8
    .line 594
    .local v2, latch:Ljava/util/concurrent/CountDownLatch;
    new-array v0, v5, [J

    #@a
    .line 596
    .local v0, duration:[J
    new-instance v5, Landroid/view/ViewDebug$5;

    #@c
    invoke-direct {v5, p1, v0, v2}, Landroid/view/ViewDebug$5;-><init>(Landroid/view/ViewDebug$ViewOperation;[JLjava/util/concurrent/CountDownLatch;)V

    #@f
    invoke-virtual {p0, v5}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@12
    .line 613
    const-wide/16 v5, 0xfa0

    #@14
    :try_start_14
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@16
    invoke-virtual {v2, v5, v6, v7}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    #@19
    move-result v5

    #@1a
    if-nez v5, :cond_56

    #@1c
    .line 614
    const-string v5, "View"

    #@1e
    new-instance v6, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v7, "Could not complete the profiling of the view "

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v6

    #@31
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_34} :catch_35

    #@34
    .line 623
    :goto_34
    return-wide v3

    #@35
    .line 617
    :catch_35
    move-exception v1

    #@36
    .line 618
    .local v1, e:Ljava/lang/InterruptedException;
    const-string v5, "View"

    #@38
    new-instance v6, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v7, "Could not complete the profiling of the view "

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 619
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    #@55
    goto :goto_34

    #@56
    .line 623
    .end local v1           #e:Ljava/lang/InterruptedException;
    :cond_56
    const/4 v3, 0x0

    #@57
    aget-wide v3, v0, v3

    #@59
    goto :goto_34
.end method

.method private static requestLayout(Landroid/view/View;Ljava/lang/String;)V
    .registers 4
    .parameter "root"
    .parameter "parameter"

    #@0
    .prologue
    .line 456
    invoke-static {p0, p1}, Landroid/view/ViewDebug;->findView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 457
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_e

    #@6
    .line 458
    new-instance v1, Landroid/view/ViewDebug$1;

    #@8
    invoke-direct {v1, v0}, Landroid/view/ViewDebug$1;-><init>(Landroid/view/View;)V

    #@b
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@e
    .line 464
    :cond_e
    return-void
.end method

.method static resolveId(Landroid/content/Context;I)Ljava/lang/Object;
    .registers 7
    .parameter "context"
    .parameter "id"

    #@0
    .prologue
    .line 1169
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v2

    #@4
    .line 1170
    .local v2, resources:Landroid/content/res/Resources;
    if-ltz p1, :cond_3f

    #@6
    .line 1172
    :try_start_6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const/16 v4, 0x2f

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_24
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6 .. :try_end_24} :catch_26

    #@24
    move-result-object v1

    #@25
    .line 1180
    .local v1, fieldValue:Ljava/lang/String;
    :goto_25
    return-object v1

    #@26
    .line 1174
    .end local v1           #fieldValue:Ljava/lang/String;
    :catch_26
    move-exception v0

    #@27
    .line 1175
    .local v0, e:Landroid/content/res/Resources$NotFoundException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "id/0x"

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    .line 1176
    .restart local v1       #fieldValue:Ljava/lang/String;
    goto :goto_25

    #@3f
    .line 1178
    .end local v0           #e:Landroid/content/res/Resources$NotFoundException;
    .end local v1           #fieldValue:Ljava/lang/String;
    :cond_3f
    const-string v1, "NO_ID"

    #@41
    .restart local v1       #fieldValue:Ljava/lang/String;
    goto :goto_25
.end method

.method public static startHierarchyTracing(Ljava/lang/String;Landroid/view/View;)V
    .registers 2
    .parameter "prefix"
    .parameter "view"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 393
    return-void
.end method

.method public static startRecyclerTracing(Ljava/lang/String;Landroid/view/View;)V
    .registers 2
    .parameter "prefix"
    .parameter "view"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 369
    return-void
.end method

.method public static stopHierarchyTracing()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 400
    return-void
.end method

.method public static stopRecyclerTracing()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 377
    return-void
.end method

.method public static trace(Landroid/view/View;Landroid/view/ViewDebug$HierarchyTraceType;)V
    .registers 2
    .parameter "view"
    .parameter "type"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 385
    return-void
.end method

.method public static varargs trace(Landroid/view/View;Landroid/view/ViewDebug$RecyclerTraceType;[I)V
    .registers 3
    .parameter "view"
    .parameter "type"
    .parameter "parameters"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 361
    return-void
.end method

.method private static writeEntry(Ljava/io/BufferedWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6
    .parameter "out"
    .parameter "prefix"
    .parameter "name"
    .parameter "suffix"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1090
    invoke-virtual {p0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@3
    .line 1091
    invoke-virtual {p0, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@6
    .line 1092
    invoke-virtual {p0, p3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@9
    .line 1093
    const-string v0, "="

    #@b
    invoke-virtual {p0, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@e
    .line 1094
    invoke-static {p0, p4}, Landroid/view/ViewDebug;->writeValue(Ljava/io/BufferedWriter;Ljava/lang/Object;)V

    #@11
    .line 1095
    const/16 v0, 0x20

    #@13
    invoke-virtual {p0, v0}, Ljava/io/BufferedWriter;->write(I)V

    #@16
    .line 1096
    return-void
.end method

.method private static writeValue(Ljava/io/BufferedWriter;Ljava/lang/Object;)V
    .registers 6
    .parameter "out"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1184
    if-eqz p1, :cond_39

    #@2
    .line 1185
    const-string v0, "[EXCEPTION]"

    #@4
    .line 1187
    .local v0, output:Ljava/lang/String;
    :try_start_4
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string v2, "\n"

    #@a
    const-string v3, "\\n"

    #@c
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_24

    #@f
    move-result-object v0

    #@10
    .line 1189
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@13
    move-result v1

    #@14
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {p0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@1b
    .line 1190
    const-string v1, ","

    #@1d
    invoke-virtual {p0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@20
    .line 1191
    invoke-virtual {p0, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@23
    .line 1196
    .end local v0           #output:Ljava/lang/String;
    :goto_23
    return-void

    #@24
    .line 1189
    .restart local v0       #output:Ljava/lang/String;
    :catchall_24
    move-exception v1

    #@25
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@28
    move-result v2

    #@29
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {p0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@30
    .line 1190
    const-string v2, ","

    #@32
    invoke-virtual {p0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@35
    .line 1191
    invoke-virtual {p0, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@38
    throw v1

    #@39
    .line 1194
    .end local v0           #output:Ljava/lang/String;
    :cond_39
    const-string v1, "4,null"

    #@3b
    invoke-virtual {p0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@3e
    goto :goto_23
.end method
