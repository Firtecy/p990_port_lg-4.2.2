.class final Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "AccessibilityInteractionConnectionManager"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 5516
    iput-object p1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public ensureConnection()V
    .registers 7

    #@0
    .prologue
    .line 5535
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@2
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    if-eqz v1, :cond_2b

    #@6
    .line 5536
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@8
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iget v1, v1, Landroid/view/View$AttachInfo;->mAccessibilityWindowId:I

    #@c
    const/4 v2, -0x1

    #@d
    if-eq v1, v2, :cond_2c

    #@f
    const/4 v0, 0x1

    #@10
    .line 5538
    .local v0, registered:Z
    :goto_10
    if-nez v0, :cond_2b

    #@12
    .line 5539
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@14
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@16
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@18
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@1a
    iget-object v3, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@1c
    iget-object v3, v3, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@1e
    new-instance v4, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;

    #@20
    iget-object v5, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@22
    invoke-direct {v4, v5}, Landroid/view/ViewRootImpl$AccessibilityInteractionConnection;-><init>(Landroid/view/ViewRootImpl;)V

    #@25
    invoke-virtual {v2, v3, v4}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityInteractionConnection(Landroid/view/IWindow;Landroid/view/accessibility/IAccessibilityInteractionConnection;)I

    #@28
    move-result v2

    #@29
    iput v2, v1, Landroid/view/View$AttachInfo;->mAccessibilityWindowId:I

    #@2b
    .line 5544
    .end local v0           #registered:Z
    :cond_2b
    return-void

    #@2c
    .line 5536
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_10
.end method

.method public ensureNoConnection()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 5547
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@3
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    iget v1, v1, Landroid/view/View$AttachInfo;->mAccessibilityWindowId:I

    #@7
    if-eq v1, v2, :cond_1e

    #@9
    const/4 v0, 0x1

    #@a
    .line 5549
    .local v0, registered:Z
    :goto_a
    if-eqz v0, :cond_1d

    #@c
    .line 5550
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@e
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@10
    iput v2, v1, Landroid/view/View$AttachInfo;->mAccessibilityWindowId:I

    #@12
    .line 5551
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@14
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@16
    iget-object v2, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@18
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@1a
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityInteractionConnection(Landroid/view/IWindow;)V

    #@1d
    .line 5553
    :cond_1d
    return-void

    #@1e
    .line 5547
    .end local v0           #registered:Z
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_a
.end method

.method public onAccessibilityStateChanged(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 5519
    if-eqz p1, :cond_32

    #@2
    .line 5520
    invoke-virtual {p0}, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->ensureConnection()V

    #@5
    .line 5521
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@7
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    if-eqz v1, :cond_31

    #@b
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@d
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@f
    iget-boolean v1, v1, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@11
    if-eqz v1, :cond_31

    #@13
    .line 5522
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@15
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@17
    const/16 v2, 0x20

    #@19
    invoke-virtual {v1, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@1c
    .line 5523
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@1e
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@20
    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@23
    move-result-object v0

    #@24
    .line 5524
    .local v0, focusedView:Landroid/view/View;
    if-eqz v0, :cond_31

    #@26
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@28
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2a
    if-eq v0, v1, :cond_31

    #@2c
    .line 5525
    const/16 v1, 0x8

    #@2e
    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@31
    .line 5532
    .end local v0           #focusedView:Landroid/view/View;
    :cond_31
    :goto_31
    return-void

    #@32
    .line 5529
    :cond_32
    invoke-virtual {p0}, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->ensureNoConnection()V

    #@35
    .line 5530
    iget-object v1, p0, Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;->this$0:Landroid/view/ViewRootImpl;

    #@37
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@39
    const/16 v2, 0x16

    #@3b
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(I)Landroid/os/Message;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@42
    goto :goto_31
.end method
