.class Landroid/view/ViewGroup$ViewLocationHolder;
.super Ljava/lang/Object;
.source "ViewGroup.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewLocationHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Landroid/view/ViewGroup$ViewLocationHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_POOL_SIZE:I = 0x20

.field private static sPool:Landroid/view/ViewGroup$ViewLocationHolder;

.field private static final sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field private mIsPooled:Z

.field private mLayoutDirection:I

.field private final mLocation:Landroid/graphics/Rect;

.field private mNext:Landroid/view/ViewGroup$ViewLocationHolder;

.field public mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 6267
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 6263
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 6277
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@a
    return-void
.end method

.method private clear()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 6375
    const/4 v0, 0x0

    #@2
    iput-object v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mView:Landroid/view/View;

    #@4
    .line 6376
    iget-object v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@6
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    #@9
    .line 6377
    return-void
.end method

.method private init(Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 5
    .parameter "root"
    .parameter "view"

    #@0
    .prologue
    .line 6367
    iget-object v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@2
    .line 6368
    .local v0, viewLocation:Landroid/graphics/Rect;
    invoke-virtual {p2, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@5
    .line 6369
    invoke-virtual {p1, p2, v0}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@8
    .line 6370
    iput-object p2, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mView:Landroid/view/View;

    #@a
    .line 6371
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutDirection()I

    #@d
    move-result v1

    #@e
    iput v1, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLayoutDirection:I

    #@10
    .line 6372
    return-void
.end method

.method public static obtain(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/view/ViewGroup$ViewLocationHolder;
    .registers 6
    .parameter "root"
    .parameter "view"

    #@0
    .prologue
    .line 6284
    const/4 v0, 0x0

    #@1
    .line 6285
    .local v0, holder:Landroid/view/ViewGroup$ViewLocationHolder;
    sget-object v3, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 6286
    :try_start_4
    sget-object v2, Landroid/view/ViewGroup$ViewLocationHolder;->sPool:Landroid/view/ViewGroup$ViewLocationHolder;

    #@6
    if-eqz v2, :cond_1f

    #@8
    .line 6287
    sget-object v0, Landroid/view/ViewGroup$ViewLocationHolder;->sPool:Landroid/view/ViewGroup$ViewLocationHolder;

    #@a
    .line 6288
    iget-object v2, v0, Landroid/view/ViewGroup$ViewLocationHolder;->mNext:Landroid/view/ViewGroup$ViewLocationHolder;

    #@c
    sput-object v2, Landroid/view/ViewGroup$ViewLocationHolder;->sPool:Landroid/view/ViewGroup$ViewLocationHolder;

    #@e
    .line 6289
    const/4 v2, 0x0

    #@f
    iput-object v2, v0, Landroid/view/ViewGroup$ViewLocationHolder;->mNext:Landroid/view/ViewGroup$ViewLocationHolder;

    #@11
    .line 6290
    const/4 v2, 0x0

    #@12
    iput-boolean v2, v0, Landroid/view/ViewGroup$ViewLocationHolder;->mIsPooled:Z

    #@14
    .line 6291
    sget v2, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolSize:I

    #@16
    add-int/lit8 v2, v2, -0x1

    #@18
    sput v2, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolSize:I

    #@1a
    .line 6295
    :goto_1a
    invoke-direct {v0, p0, p1}, Landroid/view/ViewGroup$ViewLocationHolder;->init(Landroid/view/ViewGroup;Landroid/view/View;)V

    #@1d
    .line 6296
    monitor-exit v3

    #@1e
    return-object v0

    #@1f
    .line 6293
    :cond_1f
    new-instance v1, Landroid/view/ViewGroup$ViewLocationHolder;

    #@21
    invoke-direct {v1}, Landroid/view/ViewGroup$ViewLocationHolder;-><init>()V

    #@24
    .end local v0           #holder:Landroid/view/ViewGroup$ViewLocationHolder;
    .local v1, holder:Landroid/view/ViewGroup$ViewLocationHolder;
    move-object v0, v1

    #@25
    .end local v1           #holder:Landroid/view/ViewGroup$ViewLocationHolder;
    .restart local v0       #holder:Landroid/view/ViewGroup$ViewLocationHolder;
    goto :goto_1a

    #@26
    .line 6297
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_4 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method


# virtual methods
.method public compareTo(Landroid/view/ViewGroup$ViewLocationHolder;)I
    .registers 9
    .parameter "another"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 6318
    if-nez p1, :cond_4

    #@3
    .line 6363
    :cond_3
    :goto_3
    return v1

    #@4
    .line 6321
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@7
    move-result-object v5

    #@8
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b
    move-result-object v6

    #@c
    if-ne v5, v6, :cond_3

    #@e
    .line 6325
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@10
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    #@12
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@14
    iget v6, v6, Landroid/graphics/Rect;->top:I

    #@16
    sub-int/2addr v5, v6

    #@17
    if-gtz v5, :cond_1b

    #@19
    .line 6326
    const/4 v1, -0x1

    #@1a
    goto :goto_3

    #@1b
    .line 6329
    :cond_1b
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@1d
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@1f
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@21
    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    #@23
    sub-int/2addr v5, v6

    #@24
    if-gez v5, :cond_3

    #@26
    .line 6333
    iget v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLayoutDirection:I

    #@28
    if-nez v5, :cond_44

    #@2a
    .line 6334
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@2c
    iget v5, v5, Landroid/graphics/Rect;->left:I

    #@2e
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@30
    iget v6, v6, Landroid/graphics/Rect;->left:I

    #@32
    sub-int v1, v5, v6

    #@34
    .line 6336
    .local v1, leftDifference:I
    if-nez v1, :cond_3

    #@36
    .line 6347
    .end local v1           #leftDifference:I
    :cond_36
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@38
    iget v5, v5, Landroid/graphics/Rect;->top:I

    #@3a
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@3c
    iget v6, v6, Landroid/graphics/Rect;->top:I

    #@3e
    sub-int v3, v5, v6

    #@40
    .line 6348
    .local v3, topDiference:I
    if-eqz v3, :cond_52

    #@42
    move v1, v3

    #@43
    .line 6349
    goto :goto_3

    #@44
    .line 6340
    .end local v3           #topDiference:I
    :cond_44
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@46
    iget v5, v5, Landroid/graphics/Rect;->right:I

    #@48
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@4a
    iget v6, v6, Landroid/graphics/Rect;->right:I

    #@4c
    sub-int v2, v5, v6

    #@4e
    .line 6342
    .local v2, rightDifference:I
    if-eqz v2, :cond_36

    #@50
    .line 6343
    neg-int v1, v2

    #@51
    goto :goto_3

    #@52
    .line 6352
    .end local v2           #rightDifference:I
    .restart local v3       #topDiference:I
    :cond_52
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@54
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    #@57
    move-result v5

    #@58
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@5a
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    #@5d
    move-result v6

    #@5e
    sub-int v0, v5, v6

    #@60
    .line 6353
    .local v0, heightDiference:I
    if-eqz v0, :cond_64

    #@62
    .line 6354
    neg-int v1, v0

    #@63
    goto :goto_3

    #@64
    .line 6357
    :cond_64
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@66
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@69
    move-result v5

    #@6a
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mLocation:Landroid/graphics/Rect;

    #@6c
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    #@6f
    move-result v6

    #@70
    sub-int v4, v5, v6

    #@72
    .line 6358
    .local v4, widthDiference:I
    if-eqz v4, :cond_76

    #@74
    .line 6359
    neg-int v1, v4

    #@75
    goto :goto_3

    #@76
    .line 6363
    :cond_76
    iget-object v5, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mView:Landroid/view/View;

    #@78
    invoke-virtual {v5}, Landroid/view/View;->getAccessibilityViewId()I

    #@7b
    move-result v5

    #@7c
    iget-object v6, p1, Landroid/view/ViewGroup$ViewLocationHolder;->mView:Landroid/view/View;

    #@7e
    invoke-virtual {v6}, Landroid/view/View;->getAccessibilityViewId()I

    #@81
    move-result v6

    #@82
    sub-int v1, v5, v6

    #@84
    goto/16 :goto_3
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 6263
    check-cast p1, Landroid/view/ViewGroup$ViewLocationHolder;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup$ViewLocationHolder;->compareTo(Landroid/view/ViewGroup$ViewLocationHolder;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 6301
    iget-boolean v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mIsPooled:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 6302
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Instance already recycled."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 6304
    :cond_c
    invoke-direct {p0}, Landroid/view/ViewGroup$ViewLocationHolder;->clear()V

    #@f
    .line 6305
    sget-object v1, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 6306
    :try_start_12
    sget v0, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolSize:I

    #@14
    const/16 v2, 0x20

    #@16
    if-ge v0, v2, :cond_27

    #@18
    .line 6307
    sget-object v0, Landroid/view/ViewGroup$ViewLocationHolder;->sPool:Landroid/view/ViewGroup$ViewLocationHolder;

    #@1a
    iput-object v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mNext:Landroid/view/ViewGroup$ViewLocationHolder;

    #@1c
    .line 6308
    const/4 v0, 0x1

    #@1d
    iput-boolean v0, p0, Landroid/view/ViewGroup$ViewLocationHolder;->mIsPooled:Z

    #@1f
    .line 6309
    sput-object p0, Landroid/view/ViewGroup$ViewLocationHolder;->sPool:Landroid/view/ViewGroup$ViewLocationHolder;

    #@21
    .line 6310
    sget v0, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Landroid/view/ViewGroup$ViewLocationHolder;->sPoolSize:I

    #@27
    .line 6312
    :cond_27
    monitor-exit v1

    #@28
    .line 6313
    return-void

    #@29
    .line 6312
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method
