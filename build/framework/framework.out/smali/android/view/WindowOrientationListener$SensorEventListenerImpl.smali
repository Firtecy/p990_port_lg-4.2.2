.class final Landroid/view/WindowOrientationListener$SensorEventListenerImpl;
.super Ljava/lang/Object;
.source "WindowOrientationListener.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/WindowOrientationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SensorEventListenerImpl"
.end annotation


# static fields
.field private static final ACCELERATION_TOLERANCE:F = 4.0f

.field private static final ACCELEROMETER_DATA_X:I = 0x0

.field private static final ACCELEROMETER_DATA_Y:I = 0x1

.field private static final ACCELEROMETER_DATA_Z:I = 0x2

.field private static final ADJACENT_ORIENTATION_ANGLE_GAP:I = 0x1e

.field private static final FILTER_TIME_CONSTANT_MS:F = 200.0f

.field private static final FLAT_ANGLE:F = 60.0f

.field private static final FLAT_TIME_NANOS:J = 0x3b9aca00L

#the value of this static final field might be set in the static constructor
.field private static final MAX_ACCELERATION_MAGNITUDE:F = 0.0f

.field private static final MAX_FILTER_DELTA_TIME_NANOS:J = 0x3b9aca00L

.field private static final MAX_TILT:I = 0x3c

#the value of this static final field might be set in the static constructor
.field private static final MIN_ACCELERATION_MAGNITUDE:F = 0.0f

.field private static final NANOS_PER_MS:J = 0xf4240L

.field private static final NEAR_ZERO_MAGNITUDE:F = 1.0f

.field private static final PROPOSAL_MIN_TIME_SINCE_ACCELERATION_ENDED_NANOS:J = 0x1dcd6500L

.field private static final PROPOSAL_MIN_TIME_SINCE_FLAT_ENDED_NANOS:J = 0x1dcd6500L

.field private static final PROPOSAL_MIN_TIME_SINCE_SWING_ENDED_NANOS:J = 0x11e1a300L

.field private static final PROPOSAL_SETTLE_TIME_NANOS:J = 0x2625a00L

.field private static final RADIANS_TO_DEGREES:F = 57.29578f

#the value of this static final field might be set in the static constructor
.field private static final SWING_AWAY_ANGLE_DELTA:F = 0.0f

.field private static final SWING_TIME_NANOS:J = 0x11e1a300L

.field private static final TILT_HISTORY_SIZE:I = 0x28

.field private static final TILT_TOLERANCE:[[I


# instance fields
.field private mAccelerationTimestampNanos:J

.field private mFlatTimestampNanos:J

.field private mLastFilteredTimestampNanos:J

.field private mLastFilteredX:F

.field private mLastFilteredY:F

.field private mLastFilteredZ:F

.field private final mOrientationListener:Landroid/view/WindowOrientationListener;

.field private mPredictedRotation:I

.field private mPredictedRotationTimestampNanos:J

.field private mProposedRotation:I

.field private mSwingTimestampNanos:J

.field private mTiltHistory:[F

.field private mTiltHistoryIndex:I

.field private mTiltHistoryTimestampNanos:[J


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const v4, 0x411ce80a

    #@3
    const/high16 v1, 0x40e0

    #@5
    const/high16 v2, 0x4080

    #@7
    const/4 v3, 0x2

    #@8
    .line 267
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$000()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_4c

    #@e
    const/high16 v0, 0x41f0

    #@10
    :goto_10
    sput v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->SWING_AWAY_ANGLE_DELTA:F

    #@12
    .line 322
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$000()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_4f

    #@18
    move v0, v1

    #@19
    :goto_19
    sub-float v0, v4, v0

    #@1b
    sput v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->MIN_ACCELERATION_MAGNITUDE:F

    #@1d
    .line 324
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$000()Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_51

    #@23
    :goto_23
    add-float v0, v4, v1

    #@25
    sput v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->MAX_ACCELERATION_MAGNITUDE:F

    #@27
    .line 347
    const/4 v0, 0x4

    #@28
    new-array v0, v0, [[I

    #@2a
    const/4 v1, 0x0

    #@2b
    new-array v2, v3, [I

    #@2d
    fill-array-data v2, :array_54

    #@30
    aput-object v2, v0, v1

    #@32
    const/4 v1, 0x1

    #@33
    new-array v2, v3, [I

    #@35
    fill-array-data v2, :array_5c

    #@38
    aput-object v2, v0, v1

    #@3a
    new-array v1, v3, [I

    #@3c
    fill-array-data v1, :array_64

    #@3f
    aput-object v1, v0, v3

    #@41
    const/4 v1, 0x3

    #@42
    new-array v2, v3, [I

    #@44
    fill-array-data v2, :array_6c

    #@47
    aput-object v2, v0, v1

    #@49
    sput-object v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->TILT_TOLERANCE:[[I

    #@4b
    return-void

    #@4c
    .line 267
    :cond_4c
    const/high16 v0, 0x41a0

    #@4e
    goto :goto_10

    #@4f
    :cond_4f
    move v0, v2

    #@50
    .line 322
    goto :goto_19

    #@51
    :cond_51
    move v1, v2

    #@52
    .line 324
    goto :goto_23

    #@53
    .line 347
    nop

    #@54
    :array_54
    .array-data 0x4
        0xe7t 0xfft 0xfft 0xfft
        0x3ct 0x0t 0x0t 0x0t
    .end array-data

    #@5c
    :array_5c
    .array-data 0x4
        0xe7t 0xfft 0xfft 0xfft
        0x3ct 0x0t 0x0t 0x0t
    .end array-data

    #@64
    :array_64
    .array-data 0x4
        0xe7t 0xfft 0xfft 0xfft
        0x3ct 0x0t 0x0t 0x0t
    .end array-data

    #@6c
    :array_6c
    .array-data 0x4
        0xe7t 0xfft 0xfft 0xfft
        0x3ct 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/view/WindowOrientationListener;)V
    .registers 4
    .parameter "orientationListener"

    #@0
    .prologue
    const/16 v1, 0x28

    #@2
    .line 399
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 395
    new-array v0, v1, [F

    #@7
    iput-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistory:[F

    #@9
    .line 396
    new-array v0, v1, [J

    #@b
    iput-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@d
    .line 400
    iput-object p1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mOrientationListener:Landroid/view/WindowOrientationListener;

    #@f
    .line 401
    invoke-direct {p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->reset()V

    #@12
    .line 402
    return-void
.end method

.method private addTiltHistoryEntry(JF)V
    .registers 8
    .parameter "now"
    .parameter "tilt"

    #@0
    .prologue
    .line 710
    iget-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistory:[F

    #@2
    iget v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@4
    aput p3, v0, v1

    #@6
    .line 711
    iget-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@8
    iget v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@a
    aput-wide p1, v0, v1

    #@c
    .line 712
    iget v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    rem-int/lit8 v0, v0, 0x28

    #@12
    iput v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@14
    .line 713
    iget-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@16
    iget v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@18
    const-wide/high16 v2, -0x8000

    #@1a
    aput-wide v2, v0, v1

    #@1c
    .line 714
    return-void
.end method

.method private clearPredictedRotation()V
    .registers 3

    #@0
    .prologue
    .line 688
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@3
    .line 689
    const-wide/high16 v0, -0x8000

    #@5
    iput-wide v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotationTimestampNanos:J

    #@7
    .line 690
    return-void
.end method

.method private clearTiltHistory()V
    .registers 5

    #@0
    .prologue
    .line 705
    iget-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@2
    const/4 v1, 0x0

    #@3
    const-wide/high16 v2, -0x8000

    #@5
    aput-wide v2, v0, v1

    #@7
    .line 706
    const/4 v0, 0x1

    #@8
    iput v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@a
    .line 707
    return-void
.end method

.method private isAccelerating(F)Z
    .registers 3
    .parameter "magnitude"

    #@0
    .prologue
    .line 700
    sget v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->MIN_ACCELERATION_MAGNITUDE:F

    #@2
    cmpg-float v0, p1, v0

    #@4
    if-ltz v0, :cond_c

    #@6
    sget v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->MAX_ACCELERATION_MAGNITUDE:F

    #@8
    cmpl-float v0, p1, v0

    #@a
    if-lez v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isFlat(J)Z
    .registers 8
    .parameter "now"

    #@0
    .prologue
    .line 717
    iget v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@2
    .local v0, i:I
    :cond_2
    invoke-direct {p0, v0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->nextTiltHistoryIndex(I)I

    #@5
    move-result v0

    #@6
    if-ltz v0, :cond_12

    #@8
    .line 718
    iget-object v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistory:[F

    #@a
    aget v1, v1, v0

    #@c
    const/high16 v2, 0x4270

    #@e
    cmpg-float v1, v1, v2

    #@10
    if-gez v1, :cond_14

    #@12
    .line 726
    :cond_12
    const/4 v1, 0x0

    #@13
    :goto_13
    return v1

    #@14
    .line 721
    :cond_14
    iget-object v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@16
    aget-wide v1, v1, v0

    #@18
    const-wide/32 v3, 0x3b9aca00

    #@1b
    add-long/2addr v1, v3

    #@1c
    cmp-long v1, v1, p1

    #@1e
    if-gtz v1, :cond_2

    #@20
    .line 723
    const/4 v1, 0x1

    #@21
    goto :goto_13
.end method

.method private isOrientationAngleAcceptable(II)Z
    .registers 8
    .parameter "rotation"
    .parameter "orientationAngle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 603
    iget-object v4, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mOrientationListener:Landroid/view/WindowOrientationListener;

    #@3
    iget v0, v4, Landroid/view/WindowOrientationListener;->mCurrentRotation:I

    #@5
    .line 604
    .local v0, currentRotation:I
    if-ltz v0, :cond_38

    #@7
    .line 609
    if-eq p1, v0, :cond_f

    #@9
    add-int/lit8 v4, v0, 0x1

    #@b
    rem-int/lit8 v4, v4, 0x4

    #@d
    if-ne p1, v4, :cond_22

    #@f
    .line 611
    :cond_f
    mul-int/lit8 v4, p1, 0x5a

    #@11
    add-int/lit8 v4, v4, -0x2d

    #@13
    add-int/lit8 v1, v4, 0xf

    #@15
    .line 613
    .local v1, lowerBound:I
    if-nez p1, :cond_20

    #@17
    .line 614
    const/16 v4, 0x13b

    #@19
    if-lt p2, v4, :cond_22

    #@1b
    add-int/lit16 v4, v1, 0x168

    #@1d
    if-ge p2, v4, :cond_22

    #@1f
    .line 643
    .end local v1           #lowerBound:I
    :cond_1f
    :goto_1f
    return v3

    #@20
    .line 618
    .restart local v1       #lowerBound:I
    :cond_20
    if-lt p2, v1, :cond_1f

    #@22
    .line 628
    .end local v1           #lowerBound:I
    :cond_22
    if-eq p1, v0, :cond_2a

    #@24
    add-int/lit8 v4, v0, 0x3

    #@26
    rem-int/lit8 v4, v4, 0x4

    #@28
    if-ne p1, v4, :cond_38

    #@2a
    .line 630
    :cond_2a
    mul-int/lit8 v4, p1, 0x5a

    #@2c
    add-int/lit8 v4, v4, 0x2d

    #@2e
    add-int/lit8 v2, v4, -0xf

    #@30
    .line 632
    .local v2, upperBound:I
    if-nez p1, :cond_3a

    #@32
    .line 633
    const/16 v4, 0x2d

    #@34
    if-gt p2, v4, :cond_38

    #@36
    if-gt p2, v2, :cond_1f

    #@38
    .line 643
    .end local v2           #upperBound:I
    :cond_38
    const/4 v3, 0x1

    #@39
    goto :goto_1f

    #@3a
    .line 637
    .restart local v2       #upperBound:I
    :cond_3a
    if-le p2, v2, :cond_38

    #@3c
    goto :goto_1f
.end method

.method private isPredictedRotationAcceptable(J)Z
    .registers 10
    .parameter "now"

    #@0
    .prologue
    const-wide/32 v5, 0x1dcd6500

    #@3
    const/4 v0, 0x0

    #@4
    .line 652
    iget-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotationTimestampNanos:J

    #@6
    const-wide/32 v3, 0x2625a00

    #@9
    add-long/2addr v1, v3

    #@a
    cmp-long v1, p1, v1

    #@c
    if-gez v1, :cond_f

    #@e
    .line 674
    :cond_e
    :goto_e
    return v0

    #@f
    .line 657
    :cond_f
    iget-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mFlatTimestampNanos:J

    #@11
    add-long/2addr v1, v5

    #@12
    cmp-long v1, p1, v1

    #@14
    if-ltz v1, :cond_e

    #@16
    .line 663
    iget-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mSwingTimestampNanos:J

    #@18
    const-wide/32 v3, 0x11e1a300

    #@1b
    add-long/2addr v1, v3

    #@1c
    cmp-long v1, p1, v1

    #@1e
    if-ltz v1, :cond_e

    #@20
    .line 668
    iget-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mAccelerationTimestampNanos:J

    #@22
    add-long/2addr v1, v5

    #@23
    cmp-long v1, p1, v1

    #@25
    if-ltz v1, :cond_e

    #@27
    .line 674
    const/4 v0, 0x1

    #@28
    goto :goto_e
.end method

.method private isSwinging(JF)Z
    .registers 9
    .parameter "now"
    .parameter "tilt"

    #@0
    .prologue
    .line 730
    iget v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryIndex:I

    #@2
    .local v0, i:I
    :cond_2
    invoke-direct {p0, v0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->nextTiltHistoryIndex(I)I

    #@5
    move-result v0

    #@6
    if-ltz v0, :cond_14

    #@8
    .line 731
    iget-object v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@a
    aget-wide v1, v1, v0

    #@c
    const-wide/32 v3, 0x11e1a300

    #@f
    add-long/2addr v1, v3

    #@10
    cmp-long v1, v1, p1

    #@12
    if-gez v1, :cond_16

    #@14
    .line 739
    :cond_14
    const/4 v1, 0x0

    #@15
    :goto_15
    return v1

    #@16
    .line 734
    :cond_16
    iget-object v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistory:[F

    #@18
    aget v1, v1, v0

    #@1a
    sget v2, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->SWING_AWAY_ANGLE_DELTA:F

    #@1c
    add-float/2addr v1, v2

    #@1d
    cmpg-float v1, v1, p3

    #@1f
    if-gtz v1, :cond_2

    #@21
    .line 736
    const/4 v1, 0x1

    #@22
    goto :goto_15
.end method

.method private isTiltAngleAcceptable(II)Z
    .registers 6
    .parameter "rotation"
    .parameter "tiltAngle"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 589
    sget-object v2, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->TILT_TOLERANCE:[[I

    #@4
    aget-object v2, v2, p1

    #@6
    aget v2, v2, v1

    #@8
    if-lt p2, v2, :cond_13

    #@a
    sget-object v2, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->TILT_TOLERANCE:[[I

    #@c
    aget-object v2, v2, p1

    #@e
    aget v2, v2, v0

    #@10
    if-gt p2, v2, :cond_13

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    move v0, v1

    #@14
    goto :goto_12
.end method

.method private nextTiltHistoryIndex(I)I
    .registers 6
    .parameter "index"

    #@0
    .prologue
    .line 743
    if-nez p1, :cond_4

    #@2
    const/16 p1, 0x28

    #@4
    .end local p1
    :cond_4
    add-int/lit8 p1, p1, -0x1

    #@6
    .line 744
    .restart local p1
    iget-object v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mTiltHistoryTimestampNanos:[J

    #@8
    aget-wide v0, v0, p1

    #@a
    const-wide/high16 v2, -0x8000

    #@c
    cmp-long v0, v0, v2

    #@e
    if-eqz v0, :cond_11

    #@10
    .end local p1
    :goto_10
    return p1

    #@11
    .restart local p1
    :cond_11
    const/4 p1, -0x1

    #@12
    goto :goto_10
.end method

.method private static remainingMS(JJ)F
    .registers 6
    .parameter "now"
    .parameter "until"

    #@0
    .prologue
    .line 748
    cmp-long v0, p0, p2

    #@2
    if-ltz v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    sub-long v0, p2, p0

    #@8
    long-to-float v0, v0

    #@9
    const v1, 0x358637bd

    #@c
    mul-float/2addr v0, v1

    #@d
    goto :goto_5
.end method

.method private reset()V
    .registers 4

    #@0
    .prologue
    const-wide/high16 v1, -0x8000

    #@2
    .line 678
    iput-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredTimestampNanos:J

    #@4
    .line 679
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@7
    .line 680
    iput-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mFlatTimestampNanos:J

    #@9
    .line 681
    iput-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mSwingTimestampNanos:J

    #@b
    .line 682
    iput-wide v1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mAccelerationTimestampNanos:J

    #@d
    .line 683
    invoke-direct {p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->clearPredictedRotation()V

    #@10
    .line 684
    invoke-direct {p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->clearTiltHistory()V

    #@13
    .line 685
    return-void
.end method

.method private updatePredictedRotation(JI)V
    .registers 5
    .parameter "now"
    .parameter "rotation"

    #@0
    .prologue
    .line 693
    iget v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@2
    if-eq v0, p3, :cond_8

    #@4
    .line 694
    iput p3, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@6
    .line 695
    iput-wide p1, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotationTimestampNanos:J

    #@8
    .line 697
    :cond_8
    return-void
.end method


# virtual methods
.method public getProposedRotation()I
    .registers 2

    #@0
    .prologue
    .line 405
    iget v0, p0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@2
    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 410
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 28
    .parameter "event"

    #@0
    .prologue
    .line 416
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/hardware/SensorEvent;->values:[F

    #@4
    move-object/from16 v20, v0

    #@6
    const/16 v21, 0x0

    #@8
    aget v17, v20, v21

    #@a
    .line 417
    .local v17, x:F
    move-object/from16 v0, p1

    #@c
    iget-object v0, v0, Landroid/hardware/SensorEvent;->values:[F

    #@e
    move-object/from16 v20, v0

    #@10
    const/16 v21, 0x1

    #@12
    aget v18, v20, v21

    #@14
    .line 418
    .local v18, y:F
    move-object/from16 v0, p1

    #@16
    iget-object v0, v0, Landroid/hardware/SensorEvent;->values:[F

    #@18
    move-object/from16 v20, v0

    #@1a
    const/16 v21, 0x2

    #@1c
    aget v19, v20, v21

    #@1e
    .line 420
    .local v19, z:F
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@21
    move-result v20

    #@22
    if-eqz v20, :cond_74

    #@24
    .line 421
    const-string v20, "WindowOrientationListener"

    #@26
    new-instance v21, Ljava/lang/StringBuilder;

    #@28
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v22, "Raw acceleration vector: x="

    #@2d
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v21

    #@31
    move-object/from16 v0, v21

    #@33
    move/from16 v1, v17

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@38
    move-result-object v21

    #@39
    const-string v22, ", y="

    #@3b
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v21

    #@3f
    move-object/from16 v0, v21

    #@41
    move/from16 v1, v18

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@46
    move-result-object v21

    #@47
    const-string v22, ", z="

    #@49
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v21

    #@4d
    move-object/from16 v0, v21

    #@4f
    move/from16 v1, v19

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@54
    move-result-object v21

    #@55
    const-string v22, ", magnitude="

    #@57
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v21

    #@5b
    mul-float v22, v17, v17

    #@5d
    mul-float v23, v18, v18

    #@5f
    add-float v22, v22, v23

    #@61
    mul-float v23, v19, v19

    #@63
    add-float v22, v22, v23

    #@65
    invoke-static/range {v22 .. v22}, Landroid/util/FloatMath;->sqrt(F)F

    #@68
    move-result v22

    #@69
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v21

    #@6d
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v21

    #@71
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 430
    :cond_74
    move-object/from16 v0, p1

    #@76
    iget-wide v8, v0, Landroid/hardware/SensorEvent;->timestamp:J

    #@78
    .line 431
    .local v8, now:J
    move-object/from16 v0, p0

    #@7a
    iget-wide v13, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredTimestampNanos:J

    #@7c
    .line 432
    .local v13, then:J
    sub-long v20, v8, v13

    #@7e
    move-wide/from16 v0, v20

    #@80
    long-to-float v0, v0

    #@81
    move/from16 v20, v0

    #@83
    const v21, 0x358637bd

    #@86
    mul-float v16, v20, v21

    #@88
    .line 434
    .local v16, timeDeltaMS:F
    cmp-long v20, v8, v13

    #@8a
    if-ltz v20, :cond_a7

    #@8c
    const-wide/32 v20, 0x3b9aca00

    #@8f
    add-long v20, v20, v13

    #@91
    cmp-long v20, v8, v20

    #@93
    if-gtz v20, :cond_a7

    #@95
    const/16 v20, 0x0

    #@97
    cmpl-float v20, v17, v20

    #@99
    if-nez v20, :cond_251

    #@9b
    const/16 v20, 0x0

    #@9d
    cmpl-float v20, v18, v20

    #@9f
    if-nez v20, :cond_251

    #@a1
    const/16 v20, 0x0

    #@a3
    cmpl-float v20, v19, v20

    #@a5
    if-nez v20, :cond_251

    #@a7
    .line 437
    :cond_a7
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@aa
    move-result v20

    #@ab
    if-eqz v20, :cond_b4

    #@ad
    .line 438
    const-string v20, "WindowOrientationListener"

    #@af
    const-string v21, "Resetting orientation listener."

    #@b1
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 440
    :cond_b4
    invoke-direct/range {p0 .. p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->reset()V

    #@b7
    .line 441
    const/4 v12, 0x1

    #@b8
    .line 456
    .local v12, skipSample:Z
    :goto_b8
    move-object/from16 v0, p0

    #@ba
    iput-wide v8, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredTimestampNanos:J

    #@bc
    .line 457
    move/from16 v0, v17

    #@be
    move-object/from16 v1, p0

    #@c0
    iput v0, v1, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredX:F

    #@c2
    .line 458
    move/from16 v0, v18

    #@c4
    move-object/from16 v1, p0

    #@c6
    iput v0, v1, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredY:F

    #@c8
    .line 459
    move/from16 v0, v19

    #@ca
    move-object/from16 v1, p0

    #@cc
    iput v0, v1, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredZ:F

    #@ce
    .line 461
    const/4 v3, 0x0

    #@cf
    .line 462
    .local v3, isAccelerating:Z
    const/4 v4, 0x0

    #@d0
    .line 463
    .local v4, isFlat:Z
    const/4 v5, 0x0

    #@d1
    .line 464
    .local v5, isSwinging:Z
    if-nez v12, :cond_f7

    #@d3
    .line 466
    mul-float v20, v17, v17

    #@d5
    mul-float v21, v18, v18

    #@d7
    add-float v20, v20, v21

    #@d9
    mul-float v21, v19, v19

    #@db
    add-float v20, v20, v21

    #@dd
    invoke-static/range {v20 .. v20}, Landroid/util/FloatMath;->sqrt(F)F

    #@e0
    move-result v6

    #@e1
    .line 467
    .local v6, magnitude:F
    const/high16 v20, 0x3f80

    #@e3
    cmpg-float v20, v6, v20

    #@e5
    if-gez v20, :cond_2ec

    #@e7
    .line 468
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@ea
    move-result v20

    #@eb
    if-eqz v20, :cond_f4

    #@ed
    .line 469
    const-string v20, "WindowOrientationListener"

    #@ef
    const-string v21, "Ignoring sensor data, magnitude too close to zero."

    #@f1
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 471
    :cond_f4
    invoke-direct/range {p0 .. p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->clearPredictedRotation()V

    #@f7
    .line 551
    .end local v6           #magnitude:F
    :cond_f7
    :goto_f7
    move-object/from16 v0, p0

    #@f9
    iget v10, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@fb
    .line 552
    .local v10, oldProposedRotation:I
    move-object/from16 v0, p0

    #@fd
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@ff
    move/from16 v20, v0

    #@101
    if-ltz v20, :cond_10b

    #@103
    move-object/from16 v0, p0

    #@105
    invoke-direct {v0, v8, v9}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->isPredictedRotationAcceptable(J)Z

    #@108
    move-result v20

    #@109
    if-eqz v20, :cond_117

    #@10b
    .line 553
    :cond_10b
    move-object/from16 v0, p0

    #@10d
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@10f
    move/from16 v20, v0

    #@111
    move/from16 v0, v20

    #@113
    move-object/from16 v1, p0

    #@115
    iput v0, v1, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@117
    .line 557
    :cond_117
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@11a
    move-result v20

    #@11b
    if-eqz v20, :cond_1ff

    #@11d
    .line 558
    const-string v20, "WindowOrientationListener"

    #@11f
    new-instance v21, Ljava/lang/StringBuilder;

    #@121
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@124
    const-string v22, "Result: currentRotation="

    #@126
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v21

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-object v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mOrientationListener:Landroid/view/WindowOrientationListener;

    #@12e
    move-object/from16 v22, v0

    #@130
    move-object/from16 v0, v22

    #@132
    iget v0, v0, Landroid/view/WindowOrientationListener;->mCurrentRotation:I

    #@134
    move/from16 v22, v0

    #@136
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@139
    move-result-object v21

    #@13a
    const-string v22, ", proposedRotation="

    #@13c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v21

    #@140
    move-object/from16 v0, p0

    #@142
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@144
    move/from16 v22, v0

    #@146
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@149
    move-result-object v21

    #@14a
    const-string v22, ", predictedRotation="

    #@14c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v21

    #@150
    move-object/from16 v0, p0

    #@152
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@154
    move/from16 v22, v0

    #@156
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@159
    move-result-object v21

    #@15a
    const-string v22, ", timeDeltaMS="

    #@15c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v21

    #@160
    move-object/from16 v0, v21

    #@162
    move/from16 v1, v16

    #@164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@167
    move-result-object v21

    #@168
    const-string v22, ", isAccelerating="

    #@16a
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v21

    #@16e
    move-object/from16 v0, v21

    #@170
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@173
    move-result-object v21

    #@174
    const-string v22, ", isFlat="

    #@176
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v21

    #@17a
    move-object/from16 v0, v21

    #@17c
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v21

    #@180
    const-string v22, ", isSwinging="

    #@182
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v21

    #@186
    move-object/from16 v0, v21

    #@188
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v21

    #@18c
    const-string v22, ", timeUntilSettledMS="

    #@18e
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v21

    #@192
    move-object/from16 v0, p0

    #@194
    iget-wide v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotationTimestampNanos:J

    #@196
    move-wide/from16 v22, v0

    #@198
    const-wide/32 v24, 0x2625a00

    #@19b
    add-long v22, v22, v24

    #@19d
    move-wide/from16 v0, v22

    #@19f
    invoke-static {v8, v9, v0, v1}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->remainingMS(JJ)F

    #@1a2
    move-result v22

    #@1a3
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v21

    #@1a7
    const-string v22, ", timeUntilAccelerationDelayExpiredMS="

    #@1a9
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v21

    #@1ad
    move-object/from16 v0, p0

    #@1af
    iget-wide v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mAccelerationTimestampNanos:J

    #@1b1
    move-wide/from16 v22, v0

    #@1b3
    const-wide/32 v24, 0x1dcd6500

    #@1b6
    add-long v22, v22, v24

    #@1b8
    move-wide/from16 v0, v22

    #@1ba
    invoke-static {v8, v9, v0, v1}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->remainingMS(JJ)F

    #@1bd
    move-result v22

    #@1be
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v21

    #@1c2
    const-string v22, ", timeUntilFlatDelayExpiredMS="

    #@1c4
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v21

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iget-wide v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mFlatTimestampNanos:J

    #@1cc
    move-wide/from16 v22, v0

    #@1ce
    const-wide/32 v24, 0x1dcd6500

    #@1d1
    add-long v22, v22, v24

    #@1d3
    move-wide/from16 v0, v22

    #@1d5
    invoke-static {v8, v9, v0, v1}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->remainingMS(JJ)F

    #@1d8
    move-result v22

    #@1d9
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v21

    #@1dd
    const-string v22, ", timeUntilSwingDelayExpiredMS="

    #@1df
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v21

    #@1e3
    move-object/from16 v0, p0

    #@1e5
    iget-wide v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mSwingTimestampNanos:J

    #@1e7
    move-wide/from16 v22, v0

    #@1e9
    const-wide/32 v24, 0x11e1a300

    #@1ec
    add-long v22, v22, v24

    #@1ee
    move-wide/from16 v0, v22

    #@1f0
    invoke-static {v8, v9, v0, v1}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->remainingMS(JJ)F

    #@1f3
    move-result v22

    #@1f4
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1f7
    move-result-object v21

    #@1f8
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fb
    move-result-object v21

    #@1fc
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1ff
    .line 576
    :cond_1ff
    move-object/from16 v0, p0

    #@201
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@203
    move/from16 v20, v0

    #@205
    move/from16 v0, v20

    #@207
    if-eq v0, v10, :cond_250

    #@209
    move-object/from16 v0, p0

    #@20b
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@20d
    move/from16 v20, v0

    #@20f
    if-ltz v20, :cond_250

    #@211
    .line 577
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@214
    move-result v20

    #@215
    if-eqz v20, :cond_241

    #@217
    .line 578
    const-string v20, "WindowOrientationListener"

    #@219
    new-instance v21, Ljava/lang/StringBuilder;

    #@21b
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@21e
    const-string v22, "Proposed rotation changed!  proposedRotation="

    #@220
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v21

    #@224
    move-object/from16 v0, p0

    #@226
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@228
    move/from16 v22, v0

    #@22a
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v21

    #@22e
    const-string v22, ", oldProposedRotation="

    #@230
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v21

    #@234
    move-object/from16 v0, v21

    #@236
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@239
    move-result-object v21

    #@23a
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23d
    move-result-object v21

    #@23e
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@241
    .line 581
    :cond_241
    move-object/from16 v0, p0

    #@243
    iget-object v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mOrientationListener:Landroid/view/WindowOrientationListener;

    #@245
    move-object/from16 v20, v0

    #@247
    move-object/from16 v0, p0

    #@249
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mProposedRotation:I

    #@24b
    move/from16 v21, v0

    #@24d
    invoke-virtual/range {v20 .. v21}, Landroid/view/WindowOrientationListener;->onProposedRotationChanged(I)V

    #@250
    .line 583
    :cond_250
    return-void

    #@251
    .line 443
    .end local v3           #isAccelerating:Z
    .end local v4           #isFlat:Z
    .end local v5           #isSwinging:Z
    .end local v10           #oldProposedRotation:I
    .end local v12           #skipSample:Z
    :cond_251
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$000()Z

    #@254
    move-result v20

    #@255
    if-nez v20, :cond_293

    #@257
    .line 444
    const/high16 v20, 0x4348

    #@259
    add-float v20, v20, v16

    #@25b
    div-float v2, v16, v20

    #@25d
    .line 445
    .local v2, alpha:F
    move-object/from16 v0, p0

    #@25f
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredX:F

    #@261
    move/from16 v20, v0

    #@263
    sub-float v20, v17, v20

    #@265
    mul-float v20, v20, v2

    #@267
    move-object/from16 v0, p0

    #@269
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredX:F

    #@26b
    move/from16 v21, v0

    #@26d
    add-float v17, v20, v21

    #@26f
    .line 446
    move-object/from16 v0, p0

    #@271
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredY:F

    #@273
    move/from16 v20, v0

    #@275
    sub-float v20, v18, v20

    #@277
    mul-float v20, v20, v2

    #@279
    move-object/from16 v0, p0

    #@27b
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredY:F

    #@27d
    move/from16 v21, v0

    #@27f
    add-float v18, v20, v21

    #@281
    .line 447
    move-object/from16 v0, p0

    #@283
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredZ:F

    #@285
    move/from16 v20, v0

    #@287
    sub-float v20, v19, v20

    #@289
    mul-float v20, v20, v2

    #@28b
    move-object/from16 v0, p0

    #@28d
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mLastFilteredZ:F

    #@28f
    move/from16 v21, v0

    #@291
    add-float v19, v20, v21

    #@293
    .line 449
    .end local v2           #alpha:F
    :cond_293
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@296
    move-result v20

    #@297
    if-eqz v20, :cond_2e9

    #@299
    .line 450
    const-string v20, "WindowOrientationListener"

    #@29b
    new-instance v21, Ljava/lang/StringBuilder;

    #@29d
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@2a0
    const-string v22, "Filtered acceleration vector: x="

    #@2a2
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a5
    move-result-object v21

    #@2a6
    move-object/from16 v0, v21

    #@2a8
    move/from16 v1, v17

    #@2aa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v21

    #@2ae
    const-string v22, ", y="

    #@2b0
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v21

    #@2b4
    move-object/from16 v0, v21

    #@2b6
    move/from16 v1, v18

    #@2b8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v21

    #@2bc
    const-string v22, ", z="

    #@2be
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v21

    #@2c2
    move-object/from16 v0, v21

    #@2c4
    move/from16 v1, v19

    #@2c6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2c9
    move-result-object v21

    #@2ca
    const-string v22, ", magnitude="

    #@2cc
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cf
    move-result-object v21

    #@2d0
    mul-float v22, v17, v17

    #@2d2
    mul-float v23, v18, v18

    #@2d4
    add-float v22, v22, v23

    #@2d6
    mul-float v23, v19, v19

    #@2d8
    add-float v22, v22, v23

    #@2da
    invoke-static/range {v22 .. v22}, Landroid/util/FloatMath;->sqrt(F)F

    #@2dd
    move-result v22

    #@2de
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2e1
    move-result-object v21

    #@2e2
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e5
    move-result-object v21

    #@2e6
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e9
    .line 454
    :cond_2e9
    const/4 v12, 0x0

    #@2ea
    .restart local v12       #skipSample:Z
    goto/16 :goto_b8

    #@2ec
    .line 474
    .restart local v3       #isAccelerating:Z
    .restart local v4       #isFlat:Z
    .restart local v5       #isSwinging:Z
    .restart local v6       #magnitude:F
    :cond_2ec
    move-object/from16 v0, p0

    #@2ee
    invoke-direct {v0, v6}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->isAccelerating(F)Z

    #@2f1
    move-result v20

    #@2f2
    if-eqz v20, :cond_2f9

    #@2f4
    .line 475
    const/4 v3, 0x1

    #@2f5
    .line 476
    move-object/from16 v0, p0

    #@2f7
    iput-wide v8, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mAccelerationTimestampNanos:J

    #@2f9
    .line 485
    :cond_2f9
    div-float v20, v19, v6

    #@2fb
    move/from16 v0, v20

    #@2fd
    float-to-double v0, v0

    #@2fe
    move-wide/from16 v20, v0

    #@300
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->asin(D)D

    #@303
    move-result-wide v20

    #@304
    const-wide v22, 0x404ca5dc20000000L

    #@309
    mul-double v20, v20, v22

    #@30b
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->round(D)J

    #@30e
    move-result-wide v20

    #@30f
    move-wide/from16 v0, v20

    #@311
    long-to-int v15, v0

    #@312
    .line 487
    .local v15, tiltAngle:I
    int-to-float v0, v15

    #@313
    move/from16 v20, v0

    #@315
    move-object/from16 v0, p0

    #@317
    move/from16 v1, v20

    #@319
    invoke-direct {v0, v8, v9, v1}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->addTiltHistoryEntry(JF)V

    #@31c
    .line 490
    move-object/from16 v0, p0

    #@31e
    invoke-direct {v0, v8, v9}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->isFlat(J)Z

    #@321
    move-result v20

    #@322
    if-eqz v20, :cond_329

    #@324
    .line 491
    const/4 v4, 0x1

    #@325
    .line 492
    move-object/from16 v0, p0

    #@327
    iput-wide v8, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mFlatTimestampNanos:J

    #@329
    .line 494
    :cond_329
    int-to-float v0, v15

    #@32a
    move/from16 v20, v0

    #@32c
    move-object/from16 v0, p0

    #@32e
    move/from16 v1, v20

    #@330
    invoke-direct {v0, v8, v9, v1}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->isSwinging(JF)Z

    #@333
    move-result v20

    #@334
    if-eqz v20, :cond_33b

    #@336
    .line 495
    const/4 v5, 0x1

    #@337
    .line 496
    move-object/from16 v0, p0

    #@339
    iput-wide v8, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mSwingTimestampNanos:J

    #@33b
    .line 501
    :cond_33b
    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    #@33e
    move-result v20

    #@33f
    const/16 v21, 0x3c

    #@341
    move/from16 v0, v20

    #@343
    move/from16 v1, v21

    #@345
    if-le v0, v1, :cond_36c

    #@347
    .line 502
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@34a
    move-result v20

    #@34b
    if-eqz v20, :cond_367

    #@34d
    .line 503
    const-string v20, "WindowOrientationListener"

    #@34f
    new-instance v21, Ljava/lang/StringBuilder;

    #@351
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@354
    const-string v22, "Ignoring sensor data, tilt angle too high: tiltAngle="

    #@356
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v21

    #@35a
    move-object/from16 v0, v21

    #@35c
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35f
    move-result-object v21

    #@360
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@363
    move-result-object v21

    #@364
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@367
    .line 506
    :cond_367
    invoke-direct/range {p0 .. p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->clearPredictedRotation()V

    #@36a
    goto/16 :goto_f7

    #@36c
    .line 511
    :cond_36c
    move/from16 v0, v17

    #@36e
    neg-float v0, v0

    #@36f
    move/from16 v20, v0

    #@371
    move/from16 v0, v20

    #@373
    float-to-double v0, v0

    #@374
    move-wide/from16 v20, v0

    #@376
    move/from16 v0, v18

    #@378
    float-to-double v0, v0

    #@379
    move-wide/from16 v22, v0

    #@37b
    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->atan2(DD)D

    #@37e
    move-result-wide v20

    #@37f
    move-wide/from16 v0, v20

    #@381
    neg-double v0, v0

    #@382
    move-wide/from16 v20, v0

    #@384
    const-wide v22, 0x404ca5dc20000000L

    #@389
    mul-double v20, v20, v22

    #@38b
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->round(D)J

    #@38e
    move-result-wide v20

    #@38f
    move-wide/from16 v0, v20

    #@391
    long-to-int v11, v0

    #@392
    .line 513
    .local v11, orientationAngle:I
    if-gez v11, :cond_396

    #@394
    .line 515
    add-int/lit16 v11, v11, 0x168

    #@396
    .line 519
    :cond_396
    add-int/lit8 v20, v11, 0x2d

    #@398
    div-int/lit8 v7, v20, 0x5a

    #@39a
    .line 520
    .local v7, nearestRotation:I
    const/16 v20, 0x4

    #@39c
    move/from16 v0, v20

    #@39e
    if-ne v7, v0, :cond_3a1

    #@3a0
    .line 521
    const/4 v7, 0x0

    #@3a1
    .line 525
    :cond_3a1
    move-object/from16 v0, p0

    #@3a3
    invoke-direct {v0, v7, v15}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->isTiltAngleAcceptable(II)Z

    #@3a6
    move-result v20

    #@3a7
    if-eqz v20, :cond_410

    #@3a9
    move-object/from16 v0, p0

    #@3ab
    invoke-direct {v0, v7, v11}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->isOrientationAngleAcceptable(II)Z

    #@3ae
    move-result v20

    #@3af
    if-eqz v20, :cond_410

    #@3b1
    .line 528
    move-object/from16 v0, p0

    #@3b3
    invoke-direct {v0, v8, v9, v7}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->updatePredictedRotation(JI)V

    #@3b6
    .line 529
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@3b9
    move-result v20

    #@3ba
    if-eqz v20, :cond_f7

    #@3bc
    .line 530
    const-string v20, "WindowOrientationListener"

    #@3be
    new-instance v21, Ljava/lang/StringBuilder;

    #@3c0
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@3c3
    const-string v22, "Predicted: tiltAngle="

    #@3c5
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c8
    move-result-object v21

    #@3c9
    move-object/from16 v0, v21

    #@3cb
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3ce
    move-result-object v21

    #@3cf
    const-string v22, ", orientationAngle="

    #@3d1
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d4
    move-result-object v21

    #@3d5
    move-object/from16 v0, v21

    #@3d7
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3da
    move-result-object v21

    #@3db
    const-string v22, ", predictedRotation="

    #@3dd
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e0
    move-result-object v21

    #@3e1
    move-object/from16 v0, p0

    #@3e3
    iget v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotation:I

    #@3e5
    move/from16 v22, v0

    #@3e7
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3ea
    move-result-object v21

    #@3eb
    const-string v22, ", predictedRotationAgeMS="

    #@3ed
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f0
    move-result-object v21

    #@3f1
    move-object/from16 v0, p0

    #@3f3
    iget-wide v0, v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->mPredictedRotationTimestampNanos:J

    #@3f5
    move-wide/from16 v22, v0

    #@3f7
    sub-long v22, v8, v22

    #@3f9
    move-wide/from16 v0, v22

    #@3fb
    long-to-float v0, v0

    #@3fc
    move/from16 v22, v0

    #@3fe
    const v23, 0x358637bd

    #@401
    mul-float v22, v22, v23

    #@403
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@406
    move-result-object v21

    #@407
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40a
    move-result-object v21

    #@40b
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@40e
    goto/16 :goto_f7

    #@410
    .line 539
    :cond_410
    invoke-static {}, Landroid/view/WindowOrientationListener;->access$100()Z

    #@413
    move-result v20

    #@414
    if-eqz v20, :cond_43c

    #@416
    .line 540
    const-string v20, "WindowOrientationListener"

    #@418
    new-instance v21, Ljava/lang/StringBuilder;

    #@41a
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@41d
    const-string v22, "Ignoring sensor data, no predicted rotation: tiltAngle="

    #@41f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@422
    move-result-object v21

    #@423
    move-object/from16 v0, v21

    #@425
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@428
    move-result-object v21

    #@429
    const-string v22, ", orientationAngle="

    #@42b
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42e
    move-result-object v21

    #@42f
    move-object/from16 v0, v21

    #@431
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@434
    move-result-object v21

    #@435
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@438
    move-result-object v21

    #@439
    invoke-static/range {v20 .. v21}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43c
    .line 544
    :cond_43c
    invoke-direct/range {p0 .. p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->clearPredictedRotation()V

    #@43f
    goto/16 :goto_f7
.end method
