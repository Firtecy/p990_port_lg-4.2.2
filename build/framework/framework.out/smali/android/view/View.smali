.class public Landroid/view/View;
.super Ljava/lang/Object;
.source "View.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements Landroid/view/KeyEvent$Callback;
.implements Landroid/view/accessibility/AccessibilityEventSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/View$MatchLabelForPredicate;,
        Landroid/view/View$MatchIdPredicate;,
        Landroid/view/View$AccessibilityDelegate;,
        Landroid/view/View$SendViewScrolledAccessibilityEvent;,
        Landroid/view/View$ScrollabilityCache;,
        Landroid/view/View$AttachInfo;,
        Landroid/view/View$BaseSavedState;,
        Landroid/view/View$UnsetPressedState;,
        Landroid/view/View$OnAttachStateChangeListener;,
        Landroid/view/View$OnSystemUiVisibilityChangeListener;,
        Landroid/view/View$OnCreateContextMenuListener;,
        Landroid/view/View$OnClickListener;,
        Landroid/view/View$OnFocusChangeListener;,
        Landroid/view/View$OnDragListener;,
        Landroid/view/View$OnLongClickListener;,
        Landroid/view/View$OnGenericMotionListener;,
        Landroid/view/View$OnHoverListener;,
        Landroid/view/View$OnTouchListener;,
        Landroid/view/View$OnKeyListener;,
        Landroid/view/View$PerformClick;,
        Landroid/view/View$CheckForTap;,
        Landroid/view/View$CheckForLongPress;,
        Landroid/view/View$MeasureSpec;,
        Landroid/view/View$DragShadowBuilder;,
        Landroid/view/View$OnLayoutChangeListener;,
        Landroid/view/View$ListenerInfo;,
        Landroid/view/View$TransformationInfo;
    }
.end annotation


# static fields
.field private static final ACCESSIBILITY_CURSOR_POSITION_UNDEFINED:I = -0x1

.field static final ALL_RTL_PROPERTIES_RESOLVED:I = 0x60010220

.field public static final ALPHA:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static final CLICKABLE:I = 0x4000

.field private static final DBG:Z = false

.field public static final DEBUG_LAYOUT_PROPERTY:Ljava/lang/String; = "debug.layout"

.field static final DISABLED:I = 0x20

.field public static final DRAG_FLAG_GLOBAL:I = 0x1

.field static final DRAG_MASK:I = 0x3

.field static final DRAWING_CACHE_ENABLED:I = 0x8000

.field public static final DRAWING_CACHE_QUALITY_AUTO:I = 0x0

.field private static final DRAWING_CACHE_QUALITY_FLAGS:[I = null

.field public static final DRAWING_CACHE_QUALITY_HIGH:I = 0x100000

.field public static final DRAWING_CACHE_QUALITY_LOW:I = 0x80000

.field static final DRAWING_CACHE_QUALITY_MASK:I = 0x180000

.field static final DRAW_MASK:I = 0x80

.field static final DUPLICATE_PARENT_STATE:I = 0x400000

.field protected static final EMPTY_STATE_SET:[I = null

.field static final ENABLED:I = 0x0

.field protected static final ENABLED_FOCUSED_SELECTED_STATE_SET:[I = null

.field protected static final ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final ENABLED_FOCUSED_STATE_SET:[I = null

.field protected static final ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET:[I = null

.field static final ENABLED_MASK:I = 0x20

.field protected static final ENABLED_SELECTED_STATE_SET:[I = null

.field protected static final ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final ENABLED_STATE_SET:[I = null

.field protected static final ENABLED_WINDOW_FOCUSED_STATE_SET:[I = null

.field static final FADING_EDGE_HORIZONTAL:I = 0x1000

.field static final FADING_EDGE_MASK:I = 0x3000

.field static final FADING_EDGE_NONE:I = 0x0

.field static final FADING_EDGE_VERTICAL:I = 0x2000

.field static final FILTER_TOUCHES_WHEN_OBSCURED:I = 0x400

.field public static final FIND_VIEWS_WITH_ACCESSIBILITY_NODE_PROVIDERS:I = 0x4

.field public static final FIND_VIEWS_WITH_CONTENT_DESCRIPTION:I = 0x2

.field public static final FIND_VIEWS_WITH_TEXT:I = 0x1

.field private static final FITS_SYSTEM_WINDOWS:I = 0x2

.field private static final FOCUSABLE:I = 0x1

.field public static final FOCUSABLES_ALL:I = 0x0

.field public static final FOCUSABLES_TOUCH_MODE:I = 0x1

.field static final FOCUSABLE_IN_TOUCH_MODE:I = 0x40000

.field private static final FOCUSABLE_MASK:I = 0x1

.field protected static final FOCUSED_SELECTED_STATE_SET:[I = null

.field protected static final FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final FOCUSED_STATE_SET:[I = null

.field protected static final FOCUSED_WINDOW_FOCUSED_STATE_SET:[I = null

.field public static final FOCUS_BACKWARD:I = 0x1

.field public static final FOCUS_DOWN:I = 0x82

.field public static final FOCUS_FORWARD:I = 0x2

.field public static final FOCUS_LEFT:I = 0x11

.field public static final FOCUS_RIGHT:I = 0x42

.field public static final FOCUS_UP:I = 0x21

.field public static final GONE:I = 0x8

.field public static final HAPTIC_FEEDBACK_ENABLED:I = 0x10000000

.field public static final IMPORTANT_FOR_ACCESSIBILITY_AUTO:I = 0x0

.field static final IMPORTANT_FOR_ACCESSIBILITY_DEFAULT:I = 0x0

.field public static final IMPORTANT_FOR_ACCESSIBILITY_NO:I = 0x2

.field public static final IMPORTANT_FOR_ACCESSIBILITY_YES:I = 0x1

.field public static final INVISIBLE:I = 0x4

.field public static final KEEP_SCREEN_ON:I = 0x4000000

.field public static final LAYER_TYPE_HARDWARE:I = 0x2

.field public static final LAYER_TYPE_NONE:I = 0x0

.field public static final LAYER_TYPE_SOFTWARE:I = 0x1

.field private static final LAYOUT_DIRECTION_DEFAULT:I = 0x2

.field private static final LAYOUT_DIRECTION_FLAGS:[I = null

.field public static final LAYOUT_DIRECTION_INHERIT:I = 0x2

.field public static final LAYOUT_DIRECTION_LOCALE:I = 0x3

.field public static final LAYOUT_DIRECTION_LTR:I = 0x0

.field public static final LAYOUT_DIRECTION_RTL:I = 0x1

.field static final LONG_CLICKABLE:I = 0x200000

.field public static final MEASURED_HEIGHT_STATE_SHIFT:I = 0x10

.field public static final MEASURED_SIZE_MASK:I = 0xffffff

.field public static final MEASURED_STATE_MASK:I = -0x1000000

.field public static final MEASURED_STATE_TOO_SMALL:I = 0x1000000

.field private static final NONZERO_EPSILON:F = 0.001f

.field private static final NOT_FOCUSABLE:I = 0x0

.field public static final NO_ID:I = -0x1

.field static final OPTIONAL_FITS_SYSTEM_WINDOWS:I = 0x800

.field public static final OVER_SCROLL_ALWAYS:I = 0x0

.field public static final OVER_SCROLL_IF_CONTENT_SCROLLS:I = 0x1

.field public static final OVER_SCROLL_NEVER:I = 0x2

.field static final PARENT_SAVE_DISABLED:I = 0x20000000

.field static final PARENT_SAVE_DISABLED_MASK:I = 0x20000000

.field static final PFLAG2_ACCESSIBILITY_FOCUSED:I = 0x4000000

.field static final PFLAG2_ACCESSIBILITY_STATE_CHANGED:I = 0x8000000

.field static final PFLAG2_DRAG_CAN_ACCEPT:I = 0x1

.field static final PFLAG2_DRAG_HOVERED:I = 0x2

.field static final PFLAG2_DRAWABLE_RESOLVED:I = 0x40000000

.field static final PFLAG2_HAS_TRANSIENT_STATE:I = 0x400000

.field static final PFLAG2_IMPORTANT_FOR_ACCESSIBILITY_MASK:I = 0x300000

.field static final PFLAG2_IMPORTANT_FOR_ACCESSIBILITY_SHIFT:I = 0x14

.field static final PFLAG2_LAYOUT_DIRECTION_MASK:I = 0xc

.field static final PFLAG2_LAYOUT_DIRECTION_MASK_SHIFT:I = 0x2

.field static final PFLAG2_LAYOUT_DIRECTION_RESOLVED:I = 0x20

.field static final PFLAG2_LAYOUT_DIRECTION_RESOLVED_MASK:I = 0x30

.field static final PFLAG2_LAYOUT_DIRECTION_RESOLVED_RTL:I = 0x10

.field static final PFLAG2_PADDING_RESOLVED:I = 0x20000000

.field private static final PFLAG2_TEXT_ALIGNMENT_FLAGS:[I = null

.field static final PFLAG2_TEXT_ALIGNMENT_MASK:I = 0xe000

.field static final PFLAG2_TEXT_ALIGNMENT_MASK_SHIFT:I = 0xd

.field static final PFLAG2_TEXT_ALIGNMENT_RESOLVED:I = 0x10000

.field private static final PFLAG2_TEXT_ALIGNMENT_RESOLVED_DEFAULT:I = 0x20000

.field static final PFLAG2_TEXT_ALIGNMENT_RESOLVED_MASK:I = 0xe0000

.field static final PFLAG2_TEXT_ALIGNMENT_RESOLVED_MASK_SHIFT:I = 0x11

.field private static final PFLAG2_TEXT_DIRECTION_FLAGS:[I = null

.field static final PFLAG2_TEXT_DIRECTION_MASK:I = 0x1c0

.field static final PFLAG2_TEXT_DIRECTION_MASK_SHIFT:I = 0x6

.field static final PFLAG2_TEXT_DIRECTION_RESOLVED:I = 0x200

.field static final PFLAG2_TEXT_DIRECTION_RESOLVED_DEFAULT:I = 0x400

.field static final PFLAG2_TEXT_DIRECTION_RESOLVED_MASK:I = 0x1c00

.field static final PFLAG2_TEXT_DIRECTION_RESOLVED_MASK_SHIFT:I = 0xa

.field static final PFLAG2_VIEW_QUICK_REJECTED:I = 0x10000000

.field static final PFLAG3_VIEW_IS_ANIMATING_ALPHA:I = 0x2

.field static final PFLAG3_VIEW_IS_ANIMATING_TRANSFORM:I = 0x1

.field static final PFLAG_ACTIVATED:I = 0x40000000

.field static final PFLAG_ALPHA_SET:I = 0x40000

.field static final PFLAG_ANIMATION_STARTED:I = 0x10000

.field private static final PFLAG_AWAKEN_SCROLL_BARS_ON_ATTACH:I = 0x8000000

.field static final PFLAG_CANCEL_NEXT_UP_EVENT:I = 0x4000000

.field static final PFLAG_DIRTY:I = 0x200000

.field static final PFLAG_DIRTY_MASK:I = 0x600000

.field static final PFLAG_DIRTY_OPAQUE:I = 0x400000

.field static final PFLAG_DRAWABLE_STATE_DIRTY:I = 0x400

.field static final PFLAG_DRAWING_CACHE_VALID:I = 0x8000

.field static final PFLAG_DRAWN:I = 0x20

.field static final PFLAG_DRAW_ANIMATION:I = 0x40

.field static final PFLAG_FOCUSED:I = 0x2

.field static final PFLAG_FORCE_LAYOUT:I = 0x1000

.field static final PFLAG_HAS_BOUNDS:I = 0x10

.field private static final PFLAG_HOVERED:I = 0x10000000

.field static final PFLAG_INVALIDATED:I = -0x80000000

.field static final PFLAG_IS_ROOT_NAMESPACE:I = 0x8

.field static final PFLAG_LAYOUT_REQUIRED:I = 0x2000

.field static final PFLAG_MEASURED_DIMENSION_SET:I = 0x800

.field static final PFLAG_ONLY_DRAWS_BACKGROUND:I = 0x100

.field static final PFLAG_OPAQUE_BACKGROUND:I = 0x800000

.field static final PFLAG_OPAQUE_MASK:I = 0x1800000

.field static final PFLAG_OPAQUE_SCROLLBARS:I = 0x1000000

.field private static final PFLAG_PIVOT_EXPLICITLY_SET:I = 0x20000000

.field private static final PFLAG_PREPRESSED:I = 0x2000000

.field private static final PFLAG_PRESSED:I = 0x4000

.field static final PFLAG_REQUEST_TRANSPARENT_REGIONS:I = 0x200

.field private static final PFLAG_SAVE_STATE_CALLED:I = 0x20000

.field static final PFLAG_SCROLL_CONTAINER:I = 0x80000

.field static final PFLAG_SCROLL_CONTAINER_ADDED:I = 0x100000

.field static final PFLAG_SELECTED:I = 0x4

.field static final PFLAG_SKIP_DRAW:I = 0x80

.field static final PFLAG_WANTS_FOCUS:I = 0x1

.field private static final POPULATING_ACCESSIBILITY_EVENT_TYPES:I = 0x2a1bf

.field protected static final PRESSED_ENABLED_FOCUSED_SELECTED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_SELECTED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_STATE_SET:[I = null

.field protected static final PRESSED_ENABLED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_FOCUSED_SELECTED_STATE_SET:[I = null

.field protected static final PRESSED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_FOCUSED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_SELECTED_STATE_SET:[I = null

.field protected static final PRESSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field protected static final PRESSED_STATE_SET:[I = null

.field protected static final PRESSED_WINDOW_FOCUSED_STATE_SET:[I = null

.field public static final PUBLIC_STATUS_BAR_VISIBILITY_MASK:I = 0xffff

.field public static final ROTATION:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final ROTATION_X:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final ROTATION_Y:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static final SAVE_DISABLED:I = 0x10000

.field static final SAVE_DISABLED_MASK:I = 0x10000

.field public static final SCALE_X:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final SCALE_Y:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final SCREEN_STATE_OFF:I = 0x0

.field public static final SCREEN_STATE_ON:I = 0x1

.field static final SCROLLBARS_HORIZONTAL:I = 0x100

.field static final SCROLLBARS_INSET_MASK:I = 0x1000000

.field public static final SCROLLBARS_INSIDE_INSET:I = 0x1000000

.field public static final SCROLLBARS_INSIDE_OVERLAY:I = 0x0

.field static final SCROLLBARS_MASK:I = 0x300

.field static final SCROLLBARS_NONE:I = 0x0

.field public static final SCROLLBARS_OUTSIDE_INSET:I = 0x3000000

.field static final SCROLLBARS_OUTSIDE_MASK:I = 0x2000000

.field public static final SCROLLBARS_OUTSIDE_OVERLAY:I = 0x2000000

.field static final SCROLLBARS_STYLE_MASK:I = 0x3000000

.field static final SCROLLBARS_VERTICAL:I = 0x200

.field public static final SCROLLBAR_POSITION_DEFAULT:I = 0x0

.field public static final SCROLLBAR_POSITION_LEFT:I = 0x1

.field public static final SCROLLBAR_POSITION_RIGHT:I = 0x2

.field protected static final SELECTED_STATE_SET:[I = null

.field protected static final SELECTED_WINDOW_FOCUSED_STATE_SET:[I = null

.field public static final SOUND_EFFECTS_ENABLED:I = 0x8000000

.field public static final STATUS_BAR_DISABLE_BACK:I = 0x400000

.field public static final STATUS_BAR_DISABLE_CLOCK:I = 0x800000

.field public static final STATUS_BAR_DISABLE_EXPAND:I = 0x10000

.field public static final STATUS_BAR_DISABLE_HOME:I = 0x200000

.field public static final STATUS_BAR_DISABLE_NOTIFICATION:I = 0x4000000

.field public static final STATUS_BAR_DISABLE_NOTIFICATION_ALERTS:I = 0x40000

.field public static final STATUS_BAR_DISABLE_NOTIFICATION_ICONS:I = 0x20000

.field public static final STATUS_BAR_DISABLE_NOTIFICATION_TICKER:I = 0x80000

.field public static final STATUS_BAR_DISABLE_RECENT:I = 0x1000000

.field public static final STATUS_BAR_DISABLE_SEARCH:I = 0x2000000

.field public static final STATUS_BAR_DISABLE_SYSTEM_INFO:I = 0x100000

.field public static final STATUS_BAR_HIDDEN:I = 0x1

.field public static final STATUS_BAR_VISIBLE:I = 0x0

.field public static final SYSTEM_UI_CLEARABLE_FLAGS:I = 0xf

.field public static final SYSTEM_UI_FLAG_FORCIBLY_HIDE_NAVIGATION:I = 0x8

.field public static final SYSTEM_UI_FLAG_FULLSCREEN:I = 0x4

.field public static final SYSTEM_UI_FLAG_HIDE_NAVIGATION:I = 0x2

.field public static final SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN:I = 0x400

.field public static final SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION:I = 0x200

.field public static final SYSTEM_UI_FLAG_LAYOUT_STABLE:I = 0x100

.field public static final SYSTEM_UI_FLAG_LOW_PROFILE:I = 0x1

.field public static final SYSTEM_UI_FLAG_NAVIGATION_ROTATION:I = 0x20

.field public static final SYSTEM_UI_FLAG_TRANSPARENT_SYSTEMBAR:I = 0x10

.field public static final SYSTEM_UI_FLAG_VISIBLE:I = 0x0

.field public static final SYSTEM_UI_LAYOUT_FLAGS:I = 0x600

.field public static final TEXT_ALIGNMENT_CENTER:I = 0x4

.field private static final TEXT_ALIGNMENT_DEFAULT:I = 0x1

.field public static final TEXT_ALIGNMENT_GRAVITY:I = 0x1

.field public static final TEXT_ALIGNMENT_INHERIT:I = 0x0

.field public static final TEXT_ALIGNMENT_TEXT_END:I = 0x3

.field public static final TEXT_ALIGNMENT_TEXT_START:I = 0x2

.field public static final TEXT_ALIGNMENT_VIEW_END:I = 0x6

.field public static final TEXT_ALIGNMENT_VIEW_START:I = 0x5

.field public static final TEXT_DIRECTION_ANY_RTL:I = 0x2

.field private static final TEXT_DIRECTION_DEFAULT:I = 0x0

.field public static final TEXT_DIRECTION_FIRST_STRONG:I = 0x1

.field public static final TEXT_DIRECTION_INHERIT:I = 0x0

.field public static final TEXT_DIRECTION_LOCALE:I = 0x5

.field public static final TEXT_DIRECTION_LTR:I = 0x3

.field public static final TEXT_DIRECTION_RTL:I = 0x4

.field public static final TRANSLATION_X:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final TRANSLATION_Y:Landroid/util/Property; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final UNDEFINED_PADDING:I = -0x80000000

.field protected static final VIEW_LOG_TAG:Ljava/lang/String; = "View"

.field static final VIEW_STATE_ACCELERATED:I = 0x40

.field static final VIEW_STATE_ACTIVATED:I = 0x20

.field static final VIEW_STATE_DRAG_CAN_ACCEPT:I = 0x100

.field static final VIEW_STATE_DRAG_HOVERED:I = 0x200

.field static final VIEW_STATE_ENABLED:I = 0x8

.field static final VIEW_STATE_FOCUSED:I = 0x4

.field static final VIEW_STATE_HOVERED:I = 0x80

.field static final VIEW_STATE_IDS:[I = null

.field static final VIEW_STATE_PRESSED:I = 0x10

.field static final VIEW_STATE_SELECTED:I = 0x2

.field private static final VIEW_STATE_SETS:[[I = null

.field static final VIEW_STATE_WINDOW_FOCUSED:I = 0x1

.field private static final VISIBILITY_FLAGS:[I = null

.field static final VISIBILITY_MASK:I = 0xc

.field public static final VISIBLE:I = 0x0

.field static final WILL_NOT_CACHE_DRAWING:I = 0x20000

.field static final WILL_NOT_DRAW:I = 0x80

.field protected static final WINDOW_FOCUSED_STATE_SET:[I

.field public static final X:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final Y:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field protected static mRtlLangAndLocaleMetaDataExist:Z

.field private static sNextAccessibilityViewId:I

.field private static final sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static sR2LTracker:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field static final sThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccessibilityCursorPosition:I

.field mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

.field mAccessibilityViewId:I

.field private mAnimator:Landroid/view/ViewPropertyAnimator;

.field mAttachInfo:Landroid/view/View$AttachInfo;

.field private mBackground:Landroid/graphics/drawable/Drawable;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        deepExport = true
        prefix = "bg_"
    .end annotation
.end field

.field private mBackgroundResource:I

.field private mBackgroundSizeChanged:Z

.field protected mBottom:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field public mCachingFailed:Z

.field private mContentDescription:Ljava/lang/CharSequence;

.field protected mContext:Landroid/content/Context;

.field protected mCurrentAnimation:Landroid/view/animation/Animation;

.field mDisplayList:Landroid/view/DisplayList;

.field private mDrawableState:[I

.field private mDrawingCache:Landroid/graphics/Bitmap;

.field private mDrawingCacheBackgroundColor:I

.field private mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

.field private mHardwareLayer:Landroid/view/HardwareLayer;

.field private mHasPerformedLongPress:Z

.field mID:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        resolveId = true
    .end annotation
.end field

.field protected final mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

.field private mKeyedTags:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mLabelForId:I

.field private mLastIsOpaque:Z

.field mLayerPaint:Landroid/graphics/Paint;

.field mLayerType:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "SOFTWARE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "HARDWARE"
            .end subannotation
        }
    .end annotation
.end field

.field private mLayoutInsets:Landroid/graphics/Insets;

.field protected mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

.field protected mLeft:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field mListenerInfo:Landroid/view/View$ListenerInfo;

.field mLocalDirtyRect:Landroid/graphics/Rect;

.field private mMatchIdPredicate:Landroid/view/View$MatchIdPredicate;

.field private mMatchLabelForPredicate:Landroid/view/View$MatchLabelForPredicate;

.field mMeasuredHeight:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field mMeasuredWidth:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mMinHeight:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mMinWidth:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mNextFocusDownId:I

.field mNextFocusForwardId:I

.field private mNextFocusLeftId:I

.field private mNextFocusRightId:I

.field private mNextFocusUpId:I

.field mOldHeightMeasureSpec:I

.field mOldWidthMeasureSpec:I

.field private mOverScrollMode:I

.field protected mPaddingBottom:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field protected mPaddingLeft:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field protected mPaddingRight:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field protected mPaddingTop:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field protected mParent:Landroid/view/ViewParent;

.field private mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

.field private mPendingCheckForTap:Landroid/view/View$CheckForTap;

.field private mPerformClick:Landroid/view/View$PerformClick;

.field mPrivateFlags:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        flagMapping = {
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1000
                mask = 0x1000
                name = "FORCE_LAYOUT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x2000
                mask = 0x2000
                name = "LAYOUT_REQUIRED"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x8000
                mask = 0x8000
                name = "DRAWING_CACHE_INVALID"
                outputIf = false
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x20
                mask = 0x20
                name = "DRAWN"
                outputIf = true
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x20
                mask = 0x20
                name = "NOT_DRAWN"
                outputIf = false
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x400000
                mask = 0x600000
                name = "DIRTY_OPAQUE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x200000
                mask = 0x600000
                name = "DIRTY"
            .end subannotation
        }
    .end annotation
.end field

.field mPrivateFlags2:I

.field mPrivateFlags3:I

.field mRecreateDisplayList:Z

.field private final mResources:Landroid/content/res/Resources;

.field protected mRight:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mScrollCache:Landroid/view/View$ScrollabilityCache;

.field protected mScrollX:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field protected mScrollY:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field private mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

.field private mSendingHoverAccessibilityEvents:Z

.field mSystemUiVisibility:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        flagMapping = {
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1
                mask = 0x1
                name = "SYSTEM_UI_FLAG_LOW_PROFILE"
                outputIf = true
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x2
                mask = 0x2
                name = "SYSTEM_UI_FLAG_HIDE_NAVIGATION"
                outputIf = true
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x0
                mask = 0xffff
                name = "SYSTEM_UI_FLAG_VISIBLE"
                outputIf = true
            .end subannotation
        }
    .end annotation
.end field

.field protected mTag:Ljava/lang/Object;

.field protected mTop:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mTouchDelegate:Landroid/view/TouchDelegate;

.field private mTouchSlop:I

.field mTransformationInfo:Landroid/view/View$TransformationInfo;

.field mTransientStateCount:I

.field private mUnscaledDrawingCache:Landroid/graphics/Bitmap;

.field private mUnsetPressedState:Landroid/view/View$UnsetPressedState;

.field protected mUserPaddingBottom:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field mUserPaddingEnd:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field protected mUserPaddingLeft:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field mUserPaddingLeftInitial:I

.field protected mUserPaddingRight:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field mUserPaddingRightInitial:I

.field mUserPaddingStart:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "padding"
    .end annotation
.end field

.field private mVerticalScrollFactor:F

.field private mVerticalScrollbarPosition:I

.field mViewFlags:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mWindowAttachCount:I


# direct methods
.method static constructor <clinit>()V
    .registers 12

    #@0
    .prologue
    .line 755
    const/4 v9, 0x3

    #@1
    new-array v9, v9, [I

    #@3
    fill-array-data v9, :array_216

    #@6
    sput-object v9, Landroid/view/View;->VISIBILITY_FLAGS:[I

    #@8
    .line 910
    const/4 v9, 0x3

    #@9
    new-array v9, v9, [I

    #@b
    fill-array-data v9, :array_220

    #@e
    sput-object v9, Landroid/view/View;->DRAWING_CACHE_QUALITY_FLAGS:[I

    #@10
    .line 1380
    const/16 v9, 0x14

    #@12
    new-array v9, v9, [I

    #@14
    fill-array-data v9, :array_22a

    #@17
    sput-object v9, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@19
    .line 1394
    sget-object v9, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@1b
    array-length v9, v9

    #@1c
    div-int/lit8 v9, v9, 0x2

    #@1e
    sget-object v10, Lcom/android/internal/R$styleable;->ViewDrawableStates:[I

    #@20
    array-length v10, v10

    #@21
    if-eq v9, v10, :cond_2b

    #@23
    .line 1395
    new-instance v9, Ljava/lang/IllegalStateException;

    #@25
    const-string v10, "VIEW_STATE_IDs array length does not match ViewDrawableStates style array"

    #@27
    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v9

    #@2b
    .line 1398
    :cond_2b
    sget-object v9, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@2d
    array-length v9, v9

    #@2e
    new-array v4, v9, [I

    #@30
    .line 1399
    .local v4, orderedIds:[I
    const/4 v1, 0x0

    #@31
    .local v1, i:I
    :goto_31
    sget-object v9, Lcom/android/internal/R$styleable;->ViewDrawableStates:[I

    #@33
    array-length v9, v9

    #@34
    if-ge v1, v9, :cond_5c

    #@36
    .line 1400
    sget-object v9, Lcom/android/internal/R$styleable;->ViewDrawableStates:[I

    #@38
    aget v8, v9, v1

    #@3a
    .line 1401
    .local v8, viewState:I
    const/4 v2, 0x0

    #@3b
    .local v2, j:I
    :goto_3b
    sget-object v9, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@3d
    array-length v9, v9

    #@3e
    if-ge v2, v9, :cond_59

    #@40
    .line 1402
    sget-object v9, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@42
    aget v9, v9, v2

    #@44
    if-ne v9, v8, :cond_56

    #@46
    .line 1403
    mul-int/lit8 v9, v1, 0x2

    #@48
    aput v8, v4, v9

    #@4a
    .line 1404
    mul-int/lit8 v9, v1, 0x2

    #@4c
    add-int/lit8 v9, v9, 0x1

    #@4e
    sget-object v10, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@50
    add-int/lit8 v11, v2, 0x1

    #@52
    aget v10, v10, v11

    #@54
    aput v10, v4, v9

    #@56
    .line 1401
    :cond_56
    add-int/lit8 v2, v2, 0x2

    #@58
    goto :goto_3b

    #@59
    .line 1399
    :cond_59
    add-int/lit8 v1, v1, 0x1

    #@5b
    goto :goto_31

    #@5c
    .line 1408
    .end local v2           #j:I
    .end local v8           #viewState:I
    :cond_5c
    sget-object v9, Landroid/view/View;->VIEW_STATE_IDS:[I

    #@5e
    array-length v9, v9

    #@5f
    div-int/lit8 v0, v9, 0x2

    #@61
    .line 1409
    .local v0, NUM_BITS:I
    const/4 v9, 0x1

    #@62
    shl-int/2addr v9, v0

    #@63
    new-array v9, v9, [[I

    #@65
    sput-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@67
    .line 1410
    const/4 v1, 0x0

    #@68
    :goto_68
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@6a
    array-length v9, v9

    #@6b
    if-ge v1, v9, :cond_90

    #@6d
    .line 1411
    invoke-static {v1}, Ljava/lang/Integer;->bitCount(I)I

    #@70
    move-result v3

    #@71
    .line 1412
    .local v3, numBits:I
    new-array v7, v3, [I

    #@73
    .line 1413
    .local v7, set:[I
    const/4 v5, 0x0

    #@74
    .line 1414
    .local v5, pos:I
    const/4 v2, 0x0

    #@75
    .restart local v2       #j:I
    :goto_75
    array-length v9, v4

    #@76
    if-ge v2, v9, :cond_89

    #@78
    .line 1415
    add-int/lit8 v9, v2, 0x1

    #@7a
    aget v9, v4, v9

    #@7c
    and-int/2addr v9, v1

    #@7d
    if-eqz v9, :cond_86

    #@7f
    .line 1416
    add-int/lit8 v6, v5, 0x1

    #@81
    .end local v5           #pos:I
    .local v6, pos:I
    aget v9, v4, v2

    #@83
    aput v9, v7, v5

    #@85
    move v5, v6

    #@86
    .line 1414
    .end local v6           #pos:I
    .restart local v5       #pos:I
    :cond_86
    add-int/lit8 v2, v2, 0x2

    #@88
    goto :goto_75

    #@89
    .line 1419
    :cond_89
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@8b
    aput-object v7, v9, v1

    #@8d
    .line 1410
    add-int/lit8 v1, v1, 0x1

    #@8f
    goto :goto_68

    #@90
    .line 1422
    .end local v2           #j:I
    .end local v3           #numBits:I
    .end local v5           #pos:I
    .end local v7           #set:[I
    :cond_90
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@92
    const/4 v10, 0x0

    #@93
    aget-object v9, v9, v10

    #@95
    sput-object v9, Landroid/view/View;->EMPTY_STATE_SET:[I

    #@97
    .line 1423
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@99
    const/4 v10, 0x1

    #@9a
    aget-object v9, v9, v10

    #@9c
    sput-object v9, Landroid/view/View;->WINDOW_FOCUSED_STATE_SET:[I

    #@9e
    .line 1424
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@a0
    const/4 v10, 0x2

    #@a1
    aget-object v9, v9, v10

    #@a3
    sput-object v9, Landroid/view/View;->SELECTED_STATE_SET:[I

    #@a5
    .line 1425
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@a7
    const/4 v10, 0x3

    #@a8
    aget-object v9, v9, v10

    #@aa
    sput-object v9, Landroid/view/View;->SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@ac
    .line 1427
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@ae
    const/4 v10, 0x4

    #@af
    aget-object v9, v9, v10

    #@b1
    sput-object v9, Landroid/view/View;->FOCUSED_STATE_SET:[I

    #@b3
    .line 1428
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@b5
    const/4 v10, 0x5

    #@b6
    aget-object v9, v9, v10

    #@b8
    sput-object v9, Landroid/view/View;->FOCUSED_WINDOW_FOCUSED_STATE_SET:[I

    #@ba
    .line 1430
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@bc
    const/4 v10, 0x6

    #@bd
    aget-object v9, v9, v10

    #@bf
    sput-object v9, Landroid/view/View;->FOCUSED_SELECTED_STATE_SET:[I

    #@c1
    .line 1432
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@c3
    const/4 v10, 0x7

    #@c4
    aget-object v9, v9, v10

    #@c6
    sput-object v9, Landroid/view/View;->FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@c8
    .line 1435
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@ca
    const/16 v10, 0x8

    #@cc
    aget-object v9, v9, v10

    #@ce
    sput-object v9, Landroid/view/View;->ENABLED_STATE_SET:[I

    #@d0
    .line 1436
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@d2
    const/16 v10, 0x9

    #@d4
    aget-object v9, v9, v10

    #@d6
    sput-object v9, Landroid/view/View;->ENABLED_WINDOW_FOCUSED_STATE_SET:[I

    #@d8
    .line 1438
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@da
    const/16 v10, 0xa

    #@dc
    aget-object v9, v9, v10

    #@de
    sput-object v9, Landroid/view/View;->ENABLED_SELECTED_STATE_SET:[I

    #@e0
    .line 1440
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@e2
    const/16 v10, 0xb

    #@e4
    aget-object v9, v9, v10

    #@e6
    sput-object v9, Landroid/view/View;->ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@e8
    .line 1443
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@ea
    const/16 v10, 0xc

    #@ec
    aget-object v9, v9, v10

    #@ee
    sput-object v9, Landroid/view/View;->ENABLED_FOCUSED_STATE_SET:[I

    #@f0
    .line 1445
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@f2
    const/16 v10, 0xd

    #@f4
    aget-object v9, v9, v10

    #@f6
    sput-object v9, Landroid/view/View;->ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET:[I

    #@f8
    .line 1448
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@fa
    const/16 v10, 0xe

    #@fc
    aget-object v9, v9, v10

    #@fe
    sput-object v9, Landroid/view/View;->ENABLED_FOCUSED_SELECTED_STATE_SET:[I

    #@100
    .line 1451
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@102
    const/16 v10, 0xf

    #@104
    aget-object v9, v9, v10

    #@106
    sput-object v9, Landroid/view/View;->ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@108
    .line 1455
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@10a
    const/16 v10, 0x10

    #@10c
    aget-object v9, v9, v10

    #@10e
    sput-object v9, Landroid/view/View;->PRESSED_STATE_SET:[I

    #@110
    .line 1456
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@112
    const/16 v10, 0x11

    #@114
    aget-object v9, v9, v10

    #@116
    sput-object v9, Landroid/view/View;->PRESSED_WINDOW_FOCUSED_STATE_SET:[I

    #@118
    .line 1458
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@11a
    const/16 v10, 0x12

    #@11c
    aget-object v9, v9, v10

    #@11e
    sput-object v9, Landroid/view/View;->PRESSED_SELECTED_STATE_SET:[I

    #@120
    .line 1460
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@122
    const/16 v10, 0x13

    #@124
    aget-object v9, v9, v10

    #@126
    sput-object v9, Landroid/view/View;->PRESSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@128
    .line 1463
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@12a
    const/16 v10, 0x14

    #@12c
    aget-object v9, v9, v10

    #@12e
    sput-object v9, Landroid/view/View;->PRESSED_FOCUSED_STATE_SET:[I

    #@130
    .line 1465
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@132
    const/16 v10, 0x15

    #@134
    aget-object v9, v9, v10

    #@136
    sput-object v9, Landroid/view/View;->PRESSED_FOCUSED_WINDOW_FOCUSED_STATE_SET:[I

    #@138
    .line 1468
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@13a
    const/16 v10, 0x16

    #@13c
    aget-object v9, v9, v10

    #@13e
    sput-object v9, Landroid/view/View;->PRESSED_FOCUSED_SELECTED_STATE_SET:[I

    #@140
    .line 1471
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@142
    const/16 v10, 0x17

    #@144
    aget-object v9, v9, v10

    #@146
    sput-object v9, Landroid/view/View;->PRESSED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@148
    .line 1474
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@14a
    const/16 v10, 0x18

    #@14c
    aget-object v9, v9, v10

    #@14e
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    #@150
    .line 1476
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@152
    const/16 v10, 0x19

    #@154
    aget-object v9, v9, v10

    #@156
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_WINDOW_FOCUSED_STATE_SET:[I

    #@158
    .line 1479
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@15a
    const/16 v10, 0x1a

    #@15c
    aget-object v9, v9, v10

    #@15e
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_SELECTED_STATE_SET:[I

    #@160
    .line 1482
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@162
    const/16 v10, 0x1b

    #@164
    aget-object v9, v9, v10

    #@166
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@168
    .line 1485
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@16a
    const/16 v10, 0x1c

    #@16c
    aget-object v9, v9, v10

    #@16e
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_FOCUSED_STATE_SET:[I

    #@170
    .line 1488
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@172
    const/16 v10, 0x1d

    #@174
    aget-object v9, v9, v10

    #@176
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_FOCUSED_WINDOW_FOCUSED_STATE_SET:[I

    #@178
    .line 1491
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@17a
    const/16 v10, 0x1e

    #@17c
    aget-object v9, v9, v10

    #@17e
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_FOCUSED_SELECTED_STATE_SET:[I

    #@180
    .line 1494
    sget-object v9, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@182
    const/16 v10, 0x1f

    #@184
    aget-object v9, v9, v10

    #@186
    sput-object v9, Landroid/view/View;->PRESSED_ENABLED_FOCUSED_SELECTED_WINDOW_FOCUSED_STATE_SET:[I

    #@188
    .line 1521
    new-instance v9, Ljava/lang/ThreadLocal;

    #@18a
    invoke-direct {v9}, Ljava/lang/ThreadLocal;-><init>()V

    #@18d
    sput-object v9, Landroid/view/View;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@18f
    .line 1875
    const/4 v9, 0x4

    #@190
    new-array v9, v9, [I

    #@192
    fill-array-data v9, :array_256

    #@195
    sput-object v9, Landroid/view/View;->LAYOUT_DIRECTION_FLAGS:[I

    #@197
    .line 1952
    const/4 v9, 0x6

    #@198
    new-array v9, v9, [I

    #@19a
    fill-array-data v9, :array_262

    #@19d
    sput-object v9, Landroid/view/View;->PFLAG2_TEXT_DIRECTION_FLAGS:[I

    #@19f
    .line 2061
    const/4 v9, 0x7

    #@1a0
    new-array v9, v9, [I

    #@1a2
    fill-array-data v9, :array_272

    #@1a5
    sput-object v9, Landroid/view/View;->PFLAG2_TEXT_ALIGNMENT_FLAGS:[I

    #@1a7
    .line 3267
    new-instance v9, Ljava/util/concurrent/atomic/AtomicInteger;

    #@1a9
    const/4 v10, 0x1

    #@1aa
    invoke-direct {v9, v10}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@1ad
    sput-object v9, Landroid/view/View;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    #@1af
    .line 11846
    const/4 v9, 0x0

    #@1b0
    sput-boolean v9, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@1b2
    .line 17312
    new-instance v9, Landroid/view/View$3;

    #@1b4
    const-string v10, "alpha"

    #@1b6
    invoke-direct {v9, v10}, Landroid/view/View$3;-><init>(Ljava/lang/String;)V

    #@1b9
    sput-object v9, Landroid/view/View;->ALPHA:Landroid/util/Property;

    #@1bb
    .line 17328
    new-instance v9, Landroid/view/View$4;

    #@1bd
    const-string/jumbo v10, "translationX"

    #@1c0
    invoke-direct {v9, v10}, Landroid/view/View$4;-><init>(Ljava/lang/String;)V

    #@1c3
    sput-object v9, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    #@1c5
    .line 17344
    new-instance v9, Landroid/view/View$5;

    #@1c7
    const-string/jumbo v10, "translationY"

    #@1ca
    invoke-direct {v9, v10}, Landroid/view/View$5;-><init>(Ljava/lang/String;)V

    #@1cd
    sput-object v9, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    #@1cf
    .line 17360
    new-instance v9, Landroid/view/View$6;

    #@1d1
    const-string/jumbo v10, "x"

    #@1d4
    invoke-direct {v9, v10}, Landroid/view/View$6;-><init>(Ljava/lang/String;)V

    #@1d7
    sput-object v9, Landroid/view/View;->X:Landroid/util/Property;

    #@1d9
    .line 17376
    new-instance v9, Landroid/view/View$7;

    #@1db
    const-string/jumbo v10, "y"

    #@1de
    invoke-direct {v9, v10}, Landroid/view/View$7;-><init>(Ljava/lang/String;)V

    #@1e1
    sput-object v9, Landroid/view/View;->Y:Landroid/util/Property;

    #@1e3
    .line 17392
    new-instance v9, Landroid/view/View$8;

    #@1e5
    const-string/jumbo v10, "rotation"

    #@1e8
    invoke-direct {v9, v10}, Landroid/view/View$8;-><init>(Ljava/lang/String;)V

    #@1eb
    sput-object v9, Landroid/view/View;->ROTATION:Landroid/util/Property;

    #@1ed
    .line 17408
    new-instance v9, Landroid/view/View$9;

    #@1ef
    const-string/jumbo v10, "rotationX"

    #@1f2
    invoke-direct {v9, v10}, Landroid/view/View$9;-><init>(Ljava/lang/String;)V

    #@1f5
    sput-object v9, Landroid/view/View;->ROTATION_X:Landroid/util/Property;

    #@1f7
    .line 17424
    new-instance v9, Landroid/view/View$10;

    #@1f9
    const-string/jumbo v10, "rotationY"

    #@1fc
    invoke-direct {v9, v10}, Landroid/view/View$10;-><init>(Ljava/lang/String;)V

    #@1ff
    sput-object v9, Landroid/view/View;->ROTATION_Y:Landroid/util/Property;

    #@201
    .line 17440
    new-instance v9, Landroid/view/View$11;

    #@203
    const-string/jumbo v10, "scaleX"

    #@206
    invoke-direct {v9, v10}, Landroid/view/View$11;-><init>(Ljava/lang/String;)V

    #@209
    sput-object v9, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    #@20b
    .line 17456
    new-instance v9, Landroid/view/View$12;

    #@20d
    const-string/jumbo v10, "scaleY"

    #@210
    invoke-direct {v9, v10}, Landroid/view/View$12;-><init>(Ljava/lang/String;)V

    #@213
    sput-object v9, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    #@215
    return-void

    #@216
    .line 755
    :array_216
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    #@220
    .line 910
    :array_220
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x8t 0x0t
        0x0t 0x0t 0x10t 0x0t
    .end array-data

    #@22a
    .line 1380
    :array_22a
    .array-data 0x4
        0x9dt 0x0t 0x1t 0x1t
        0x1t 0x0t 0x0t 0x0t
        0xa1t 0x0t 0x1t 0x1t
        0x2t 0x0t 0x0t 0x0t
        0x9ct 0x0t 0x1t 0x1t
        0x4t 0x0t 0x0t 0x0t
        0x9et 0x0t 0x1t 0x1t
        0x8t 0x0t 0x0t 0x0t
        0xa7t 0x0t 0x1t 0x1t
        0x10t 0x0t 0x0t 0x0t
        0xfet 0x2t 0x1t 0x1t
        0x20t 0x0t 0x0t 0x0t
        0x1bt 0x3t 0x1t 0x1t
        0x40t 0x0t 0x0t 0x0t
        0x67t 0x3t 0x1t 0x1t
        0x80t 0x0t 0x0t 0x0t
        0x68t 0x3t 0x1t 0x1t
        0x0t 0x1t 0x0t 0x0t
        0x69t 0x3t 0x1t 0x1t
        0x0t 0x2t 0x0t 0x0t
    .end array-data

    #@256
    .line 1875
    :array_256
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@262
    .line 1952
    :array_262
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
        0xc0t 0x0t 0x0t 0x0t
        0x0t 0x1t 0x0t 0x0t
        0x40t 0x1t 0x0t 0x0t
    .end array-data

    #@272
    .line 2061
    :array_272
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x20t 0x0t 0x0t
        0x0t 0x40t 0x0t 0x0t
        0x0t 0x60t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0xa0t 0x0t 0x0t
        0x0t 0xc0t 0x0t 0x0t
    .end array-data
.end method

.method constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/high16 v3, -0x8000

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v0, -0x1

    #@5
    .line 3851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 1537
    iput-object v1, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@a
    .line 1562
    iput-boolean v2, p0, Landroid/view/View;->mRecreateDisplayList:Z

    #@c
    .line 1571
    iput v0, p0, Landroid/view/View;->mID:I

    #@e
    .line 1577
    iput v0, p0, Landroid/view/View;->mAccessibilityViewId:I

    #@10
    .line 1582
    iput v0, p0, Landroid/view/View;->mAccessibilityCursorPosition:I

    #@12
    .line 2647
    iput v2, p0, Landroid/view/View;->mTransientStateCount:I

    #@14
    .line 2852
    iput v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@16
    .line 2859
    iput v2, p0, Landroid/view/View;->mPaddingRight:I

    #@18
    .line 2891
    iput v0, p0, Landroid/view/View;->mLabelForId:I

    #@1a
    .line 2947
    iput v2, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@1c
    .line 2954
    iput v2, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@1e
    .line 2964
    iput v3, p0, Landroid/view/View;->mOldWidthMeasureSpec:I

    #@20
    .line 2968
    iput v3, p0, Landroid/view/View;->mOldHeightMeasureSpec:I

    #@22
    .line 3041
    iput-object v1, p0, Landroid/view/View;->mDrawableState:[I

    #@24
    .line 3059
    iput v0, p0, Landroid/view/View;->mNextFocusLeftId:I

    #@26
    .line 3065
    iput v0, p0, Landroid/view/View;->mNextFocusRightId:I

    #@28
    .line 3071
    iput v0, p0, Landroid/view/View;->mNextFocusUpId:I

    #@2a
    .line 3077
    iput v0, p0, Landroid/view/View;->mNextFocusDownId:I

    #@2c
    .line 3083
    iput v0, p0, Landroid/view/View;->mNextFocusForwardId:I

    #@2e
    .line 3086
    iput-object v1, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@30
    .line 3118
    iput-object v1, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@32
    .line 3124
    iput v2, p0, Landroid/view/View;->mDrawingCacheBackgroundColor:I

    #@34
    .line 3139
    iput-object v1, p0, Landroid/view/View;->mAnimator:Landroid/view/ViewPropertyAnimator;

    #@36
    .line 3239
    iput v2, p0, Landroid/view/View;->mLayerType:I

    #@38
    .line 3263
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_48

    #@3e
    new-instance v0, Landroid/view/InputEventConsistencyVerifier;

    #@40
    invoke-direct {v0, p0, v2}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;I)V

    #@43
    :goto_43
    iput-object v0, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@45
    .line 3852
    iput-object v1, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@47
    .line 3853
    return-void

    #@48
    :cond_48
    move-object v0, v1

    #@49
    .line 3263
    goto :goto_43
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/high16 v3, -0x8000

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v0, -0x1

    #@5
    .line 3275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 1537
    iput-object v1, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@a
    .line 1562
    iput-boolean v2, p0, Landroid/view/View;->mRecreateDisplayList:Z

    #@c
    .line 1571
    iput v0, p0, Landroid/view/View;->mID:I

    #@e
    .line 1577
    iput v0, p0, Landroid/view/View;->mAccessibilityViewId:I

    #@10
    .line 1582
    iput v0, p0, Landroid/view/View;->mAccessibilityCursorPosition:I

    #@12
    .line 2647
    iput v2, p0, Landroid/view/View;->mTransientStateCount:I

    #@14
    .line 2852
    iput v2, p0, Landroid/view/View;->mPaddingLeft:I

    #@16
    .line 2859
    iput v2, p0, Landroid/view/View;->mPaddingRight:I

    #@18
    .line 2891
    iput v0, p0, Landroid/view/View;->mLabelForId:I

    #@1a
    .line 2947
    iput v2, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@1c
    .line 2954
    iput v2, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@1e
    .line 2964
    iput v3, p0, Landroid/view/View;->mOldWidthMeasureSpec:I

    #@20
    .line 2968
    iput v3, p0, Landroid/view/View;->mOldHeightMeasureSpec:I

    #@22
    .line 3041
    iput-object v1, p0, Landroid/view/View;->mDrawableState:[I

    #@24
    .line 3059
    iput v0, p0, Landroid/view/View;->mNextFocusLeftId:I

    #@26
    .line 3065
    iput v0, p0, Landroid/view/View;->mNextFocusRightId:I

    #@28
    .line 3071
    iput v0, p0, Landroid/view/View;->mNextFocusUpId:I

    #@2a
    .line 3077
    iput v0, p0, Landroid/view/View;->mNextFocusDownId:I

    #@2c
    .line 3083
    iput v0, p0, Landroid/view/View;->mNextFocusForwardId:I

    #@2e
    .line 3086
    iput-object v1, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@30
    .line 3118
    iput-object v1, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@32
    .line 3124
    iput v2, p0, Landroid/view/View;->mDrawingCacheBackgroundColor:I

    #@34
    .line 3139
    iput-object v1, p0, Landroid/view/View;->mAnimator:Landroid/view/ViewPropertyAnimator;

    #@36
    .line 3239
    iput v2, p0, Landroid/view/View;->mLayerType:I

    #@38
    .line 3263
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_6b

    #@3e
    new-instance v0, Landroid/view/InputEventConsistencyVerifier;

    #@40
    invoke-direct {v0, p0, v2}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;I)V

    #@43
    :goto_43
    iput-object v0, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@45
    .line 3276
    iput-object p1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@47
    .line 3277
    if-eqz p1, :cond_4d

    #@49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4c
    move-result-object v1

    #@4d
    :cond_4d
    iput-object v1, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@4f
    .line 3278
    const/high16 v0, 0x1800

    #@51
    iput v0, p0, Landroid/view/View;->mViewFlags:I

    #@53
    .line 3280
    const v0, 0x22408

    #@56
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@58
    .line 3287
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@5f
    move-result v0

    #@60
    iput v0, p0, Landroid/view/View;->mTouchSlop:I

    #@62
    .line 3288
    const/4 v0, 0x1

    #@63
    invoke-virtual {p0, v0}, Landroid/view/View;->setOverScrollMode(I)V

    #@66
    .line 3289
    iput v3, p0, Landroid/view/View;->mUserPaddingStart:I

    #@68
    .line 3290
    iput v3, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@6a
    .line 3291
    return-void

    #@6b
    :cond_6b
    move-object v0, v1

    #@6c
    .line 3263
    goto :goto_43
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 3310
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 3311
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 54
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 3332
    invoke-direct/range {p0 .. p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@3
    .line 3334
    sget-object v47, Lcom/android/internal/R$styleable;->View:[I

    #@5
    const/16 v48, 0x0

    #@7
    move-object/from16 v0, p1

    #@9
    move-object/from16 v1, p2

    #@b
    move-object/from16 v2, v47

    #@d
    move/from16 v3, p3

    #@f
    move/from16 v4, v48

    #@11
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@14
    move-result-object v6

    #@15
    .line 3337
    .local v6, a:Landroid/content/res/TypedArray;
    const/4 v8, 0x0

    #@16
    .line 3339
    .local v8, background:Landroid/graphics/drawable/Drawable;
    const/16 v18, -0x1

    #@18
    .line 3340
    .local v18, leftPadding:I
    const/16 v37, -0x1

    #@1a
    .line 3341
    .local v37, topPadding:I
    const/16 v22, -0x1

    #@1c
    .line 3342
    .local v22, rightPadding:I
    const/4 v9, -0x1

    #@1d
    .line 3343
    .local v9, bottomPadding:I
    const/high16 v30, -0x8000

    #@1f
    .line 3344
    .local v30, startPadding:I
    const/high16 v11, -0x8000

    #@21
    .line 3346
    .local v11, endPadding:I
    const/16 v21, -0x1

    #@23
    .line 3348
    .local v21, padding:I
    const/16 v43, 0x0

    #@25
    .line 3349
    .local v43, viewFlagValues:I
    const/16 v42, 0x0

    #@27
    .line 3351
    .local v42, viewFlagMasks:I
    const/16 v29, 0x0

    #@29
    .line 3353
    .local v29, setScrollContainer:Z
    const/16 v45, 0x0

    #@2b
    .line 3354
    .local v45, x:I
    const/16 v46, 0x0

    #@2d
    .line 3356
    .local v46, y:I
    const/16 v39, 0x0

    #@2f
    .line 3357
    .local v39, tx:F
    const/16 v40, 0x0

    #@31
    .line 3358
    .local v40, ty:F
    const/16 v24, 0x0

    #@33
    .line 3359
    .local v24, rotation:F
    const/16 v25, 0x0

    #@35
    .line 3360
    .local v25, rotationX:F
    const/16 v26, 0x0

    #@37
    .line 3361
    .local v26, rotationY:F
    const/high16 v32, 0x3f80

    #@39
    .line 3362
    .local v32, sx:F
    const/high16 v33, 0x3f80

    #@3b
    .line 3363
    .local v33, sy:F
    const/16 v38, 0x0

    #@3d
    .line 3365
    .local v38, transformSet:Z
    const/16 v27, 0x0

    #@3f
    .line 3366
    .local v27, scrollbarStyle:I
    move-object/from16 v0, p0

    #@41
    iget v0, v0, Landroid/view/View;->mOverScrollMode:I

    #@43
    move/from16 v20, v0

    #@45
    .line 3367
    .local v20, overScrollMode:I
    const/16 v16, 0x0

    #@47
    .line 3369
    .local v16, initializeScrollbars:Z
    const/16 v19, 0x0

    #@49
    .line 3370
    .local v19, leftPaddingDefined:Z
    const/16 v23, 0x0

    #@4b
    .line 3371
    .local v23, rightPaddingDefined:Z
    const/16 v31, 0x0

    #@4d
    .line 3372
    .local v31, startPaddingDefined:Z
    const/4 v12, 0x0

    #@4e
    .line 3374
    .local v12, endPaddingDefined:Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@51
    move-result-object v47

    #@52
    move-object/from16 v0, v47

    #@54
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@56
    move/from16 v34, v0

    #@58
    .line 3375
    .local v34, targetSdkVersion:I
    invoke-direct/range {p0 .. p0}, Landroid/view/View;->isRtlLangAndLocaleMetaDataExist()Z

    #@5b
    move-result v47

    #@5c
    sput-boolean v47, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@5e
    .line 3377
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@61
    move-result v5

    #@62
    .line 3378
    .local v5, N:I
    const/4 v15, 0x0

    #@63
    .local v15, i:I
    :goto_63
    if-ge v15, v5, :cond_550

    #@65
    .line 3379
    invoke-virtual {v6, v15}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@68
    move-result v7

    #@69
    .line 3380
    .local v7, attr:I
    packed-switch v7, :pswitch_data_6de

    #@6c
    .line 3378
    :cond_6c
    :goto_6c
    :pswitch_6c
    add-int/lit8 v15, v15, 0x1

    #@6e
    goto :goto_63

    #@6f
    .line 3382
    :pswitch_6f
    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@72
    move-result-object v8

    #@73
    .line 3383
    goto :goto_6c

    #@74
    .line 3385
    :pswitch_74
    const/16 v47, -0x1

    #@76
    move/from16 v0, v47

    #@78
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@7b
    move-result v21

    #@7c
    .line 3386
    move/from16 v0, v21

    #@7e
    move-object/from16 v1, p0

    #@80
    iput v0, v1, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@82
    .line 3387
    move/from16 v0, v21

    #@84
    move-object/from16 v1, p0

    #@86
    iput v0, v1, Landroid/view/View;->mUserPaddingRightInitial:I

    #@88
    .line 3388
    const/16 v19, 0x1

    #@8a
    .line 3389
    const/16 v23, 0x1

    #@8c
    .line 3390
    goto :goto_6c

    #@8d
    .line 3392
    :pswitch_8d
    sget-boolean v47, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@8f
    if-eqz v47, :cond_9c

    #@91
    .line 3393
    const/high16 v47, -0x8000

    #@93
    move/from16 v0, v47

    #@95
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@98
    move-result v30

    #@99
    .line 3394
    const/16 v31, 0x1

    #@9b
    goto :goto_6c

    #@9c
    .line 3397
    :cond_9c
    const/16 v47, -0x1

    #@9e
    move/from16 v0, v47

    #@a0
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@a3
    move-result v18

    #@a4
    .line 3398
    move/from16 v0, v18

    #@a6
    move-object/from16 v1, p0

    #@a8
    iput v0, v1, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@aa
    .line 3399
    const/16 v19, 0x1

    #@ac
    .line 3401
    goto :goto_6c

    #@ad
    .line 3403
    :pswitch_ad
    const/16 v47, -0x1

    #@af
    move/from16 v0, v47

    #@b1
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@b4
    move-result v37

    #@b5
    .line 3404
    goto :goto_6c

    #@b6
    .line 3406
    :pswitch_b6
    sget-boolean v47, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@b8
    if-eqz v47, :cond_c4

    #@ba
    .line 3407
    const/high16 v47, -0x8000

    #@bc
    move/from16 v0, v47

    #@be
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@c1
    move-result v11

    #@c2
    .line 3408
    const/4 v12, 0x1

    #@c3
    goto :goto_6c

    #@c4
    .line 3411
    :cond_c4
    const/16 v47, -0x1

    #@c6
    move/from16 v0, v47

    #@c8
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@cb
    move-result v22

    #@cc
    .line 3412
    move/from16 v0, v22

    #@ce
    move-object/from16 v1, p0

    #@d0
    iput v0, v1, Landroid/view/View;->mUserPaddingRightInitial:I

    #@d2
    .line 3413
    const/16 v23, 0x1

    #@d4
    .line 3415
    goto :goto_6c

    #@d5
    .line 3417
    :pswitch_d5
    const/16 v47, -0x1

    #@d7
    move/from16 v0, v47

    #@d9
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@dc
    move-result v9

    #@dd
    .line 3418
    goto :goto_6c

    #@de
    .line 3420
    :pswitch_de
    const/high16 v47, -0x8000

    #@e0
    move/from16 v0, v47

    #@e2
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@e5
    move-result v30

    #@e6
    .line 3421
    const/16 v31, 0x1

    #@e8
    .line 3422
    goto :goto_6c

    #@e9
    .line 3424
    :pswitch_e9
    const/high16 v47, -0x8000

    #@eb
    move/from16 v0, v47

    #@ed
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@f0
    move-result v11

    #@f1
    .line 3425
    const/4 v12, 0x1

    #@f2
    .line 3426
    goto/16 :goto_6c

    #@f4
    .line 3428
    :pswitch_f4
    const/16 v47, 0x0

    #@f6
    move/from16 v0, v47

    #@f8
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@fb
    move-result v45

    #@fc
    .line 3429
    goto/16 :goto_6c

    #@fe
    .line 3431
    :pswitch_fe
    const/16 v47, 0x0

    #@100
    move/from16 v0, v47

    #@102
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@105
    move-result v46

    #@106
    .line 3432
    goto/16 :goto_6c

    #@108
    .line 3434
    :pswitch_108
    const/high16 v47, 0x3f80

    #@10a
    move/from16 v0, v47

    #@10c
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@10f
    move-result v47

    #@110
    move-object/from16 v0, p0

    #@112
    move/from16 v1, v47

    #@114
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    #@117
    goto/16 :goto_6c

    #@119
    .line 3437
    :pswitch_119
    const/16 v47, 0x0

    #@11b
    move/from16 v0, v47

    #@11d
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@120
    move-result v47

    #@121
    move/from16 v0, v47

    #@123
    int-to-float v0, v0

    #@124
    move/from16 v47, v0

    #@126
    move-object/from16 v0, p0

    #@128
    move/from16 v1, v47

    #@12a
    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    #@12d
    goto/16 :goto_6c

    #@12f
    .line 3440
    :pswitch_12f
    const/16 v47, 0x0

    #@131
    move/from16 v0, v47

    #@133
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@136
    move-result v47

    #@137
    move/from16 v0, v47

    #@139
    int-to-float v0, v0

    #@13a
    move/from16 v47, v0

    #@13c
    move-object/from16 v0, p0

    #@13e
    move/from16 v1, v47

    #@140
    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    #@143
    goto/16 :goto_6c

    #@145
    .line 3443
    :pswitch_145
    const/16 v47, 0x0

    #@147
    move/from16 v0, v47

    #@149
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@14c
    move-result v47

    #@14d
    move/from16 v0, v47

    #@14f
    int-to-float v0, v0

    #@150
    move/from16 v39, v0

    #@152
    .line 3444
    const/16 v38, 0x1

    #@154
    .line 3445
    goto/16 :goto_6c

    #@156
    .line 3447
    :pswitch_156
    const/16 v47, 0x0

    #@158
    move/from16 v0, v47

    #@15a
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@15d
    move-result v47

    #@15e
    move/from16 v0, v47

    #@160
    int-to-float v0, v0

    #@161
    move/from16 v40, v0

    #@163
    .line 3448
    const/16 v38, 0x1

    #@165
    .line 3449
    goto/16 :goto_6c

    #@167
    .line 3451
    :pswitch_167
    const/16 v47, 0x0

    #@169
    move/from16 v0, v47

    #@16b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@16e
    move-result v24

    #@16f
    .line 3452
    const/16 v38, 0x1

    #@171
    .line 3453
    goto/16 :goto_6c

    #@173
    .line 3455
    :pswitch_173
    const/16 v47, 0x0

    #@175
    move/from16 v0, v47

    #@177
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@17a
    move-result v25

    #@17b
    .line 3456
    const/16 v38, 0x1

    #@17d
    .line 3457
    goto/16 :goto_6c

    #@17f
    .line 3459
    :pswitch_17f
    const/16 v47, 0x0

    #@181
    move/from16 v0, v47

    #@183
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@186
    move-result v26

    #@187
    .line 3460
    const/16 v38, 0x1

    #@189
    .line 3461
    goto/16 :goto_6c

    #@18b
    .line 3463
    :pswitch_18b
    const/high16 v47, 0x3f80

    #@18d
    move/from16 v0, v47

    #@18f
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@192
    move-result v32

    #@193
    .line 3464
    const/16 v38, 0x1

    #@195
    .line 3465
    goto/16 :goto_6c

    #@197
    .line 3467
    :pswitch_197
    const/high16 v47, 0x3f80

    #@199
    move/from16 v0, v47

    #@19b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@19e
    move-result v33

    #@19f
    .line 3468
    const/16 v38, 0x1

    #@1a1
    .line 3469
    goto/16 :goto_6c

    #@1a3
    .line 3471
    :pswitch_1a3
    const/16 v47, -0x1

    #@1a5
    move/from16 v0, v47

    #@1a7
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1aa
    move-result v47

    #@1ab
    move/from16 v0, v47

    #@1ad
    move-object/from16 v1, p0

    #@1af
    iput v0, v1, Landroid/view/View;->mID:I

    #@1b1
    goto/16 :goto_6c

    #@1b3
    .line 3474
    :pswitch_1b3
    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@1b6
    move-result-object v47

    #@1b7
    move-object/from16 v0, v47

    #@1b9
    move-object/from16 v1, p0

    #@1bb
    iput-object v0, v1, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@1bd
    .line 3475
    sget-boolean v47, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@1bf
    if-eqz v47, :cond_6c

    #@1c1
    .line 3476
    move-object/from16 v0, p0

    #@1c3
    iget-object v0, v0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@1c5
    move-object/from16 v47, v0

    #@1c7
    if-eqz v47, :cond_6c

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@1cd
    move-object/from16 v47, v0

    #@1cf
    move-object/from16 v0, v47

    #@1d1
    instance-of v0, v0, Ljava/lang/String;

    #@1d3
    move/from16 v47, v0

    #@1d5
    if-eqz v47, :cond_6c

    #@1d7
    move-object/from16 v0, p0

    #@1d9
    iget-object v0, v0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@1db
    move-object/from16 v47, v0

    #@1dd
    const-string v48, "NO_R2L"

    #@1df
    invoke-virtual/range {v47 .. v48}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1e2
    move-result v47

    #@1e3
    if-eqz v47, :cond_6c

    #@1e5
    .line 3477
    const/16 v47, 0x0

    #@1e7
    sput-boolean v47, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@1e9
    .line 3479
    move-object/from16 v0, p0

    #@1eb
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@1ed
    move/from16 v47, v0

    #@1ef
    and-int/lit8 v47, v47, -0x3d

    #@1f1
    move/from16 v0, v47

    #@1f3
    move-object/from16 v1, p0

    #@1f5
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@1f7
    .line 3481
    const/16 v17, 0x0

    #@1f9
    .line 3482
    .local v17, layoutDirection:I
    sget-object v47, Landroid/view/View;->LAYOUT_DIRECTION_FLAGS:[I

    #@1fb
    const/16 v48, 0x0

    #@1fd
    aget v41, v47, v48

    #@1ff
    .line 3484
    .local v41, value:I
    move-object/from16 v0, p0

    #@201
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@203
    move/from16 v47, v0

    #@205
    shl-int/lit8 v48, v41, 0x2

    #@207
    or-int v47, v47, v48

    #@209
    move/from16 v0, v47

    #@20b
    move-object/from16 v1, p0

    #@20d
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@20f
    .line 3487
    move-object/from16 v0, p0

    #@211
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@213
    move/from16 v47, v0

    #@215
    move/from16 v0, v47

    #@217
    and-int/lit16 v0, v0, -0x1c1

    #@219
    move/from16 v47, v0

    #@21b
    move/from16 v0, v47

    #@21d
    move-object/from16 v1, p0

    #@21f
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@221
    .line 3489
    const/16 v36, 0x3

    #@223
    .line 3490
    .local v36, textDirection:I
    move-object/from16 v0, p0

    #@225
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@227
    move/from16 v47, v0

    #@229
    sget-object v48, Landroid/view/View;->PFLAG2_TEXT_DIRECTION_FLAGS:[I

    #@22b
    const/16 v49, 0x3

    #@22d
    aget v48, v48, v49

    #@22f
    or-int v47, v47, v48

    #@231
    move/from16 v0, v47

    #@233
    move-object/from16 v1, p0

    #@235
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@237
    .line 3492
    if-eqz v31, :cond_245

    #@239
    .line 3493
    move/from16 v18, v30

    #@23b
    .line 3494
    move/from16 v0, v18

    #@23d
    move-object/from16 v1, p0

    #@23f
    iput v0, v1, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@241
    .line 3495
    const/16 v19, 0x1

    #@243
    .line 3496
    const/16 v31, 0x0

    #@245
    .line 3499
    :cond_245
    if-eqz v12, :cond_6c

    #@247
    .line 3500
    move/from16 v22, v11

    #@249
    .line 3501
    move/from16 v0, v22

    #@24b
    move-object/from16 v1, p0

    #@24d
    iput v0, v1, Landroid/view/View;->mUserPaddingRightInitial:I

    #@24f
    .line 3502
    const/16 v23, 0x1

    #@251
    .line 3503
    const/4 v12, 0x0

    #@252
    goto/16 :goto_6c

    #@254
    .line 3509
    .end local v17           #layoutDirection:I
    .end local v36           #textDirection:I
    .end local v41           #value:I
    :pswitch_254
    const/16 v47, 0x0

    #@256
    move/from16 v0, v47

    #@258
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@25b
    move-result v47

    #@25c
    if-eqz v47, :cond_6c

    #@25e
    .line 3510
    or-int/lit8 v43, v43, 0x2

    #@260
    .line 3511
    or-int/lit8 v42, v42, 0x2

    #@262
    goto/16 :goto_6c

    #@264
    .line 3515
    :pswitch_264
    const/16 v47, 0x0

    #@266
    move/from16 v0, v47

    #@268
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@26b
    move-result v47

    #@26c
    if-eqz v47, :cond_6c

    #@26e
    .line 3516
    or-int/lit8 v43, v43, 0x1

    #@270
    .line 3517
    or-int/lit8 v42, v42, 0x1

    #@272
    goto/16 :goto_6c

    #@274
    .line 3521
    :pswitch_274
    const/16 v47, 0x0

    #@276
    move/from16 v0, v47

    #@278
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@27b
    move-result v47

    #@27c
    if-eqz v47, :cond_6c

    #@27e
    .line 3522
    const v47, 0x40001

    #@281
    or-int v43, v43, v47

    #@283
    .line 3523
    const v47, 0x40001

    #@286
    or-int v42, v42, v47

    #@288
    goto/16 :goto_6c

    #@28a
    .line 3527
    :pswitch_28a
    const/16 v47, 0x0

    #@28c
    move/from16 v0, v47

    #@28e
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@291
    move-result v47

    #@292
    if-eqz v47, :cond_6c

    #@294
    .line 3528
    move/from16 v0, v43

    #@296
    or-int/lit16 v0, v0, 0x4000

    #@298
    move/from16 v43, v0

    #@29a
    .line 3529
    move/from16 v0, v42

    #@29c
    or-int/lit16 v0, v0, 0x4000

    #@29e
    move/from16 v42, v0

    #@2a0
    goto/16 :goto_6c

    #@2a2
    .line 3533
    :pswitch_2a2
    const/16 v47, 0x0

    #@2a4
    move/from16 v0, v47

    #@2a6
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2a9
    move-result v47

    #@2aa
    if-eqz v47, :cond_6c

    #@2ac
    .line 3534
    const/high16 v47, 0x20

    #@2ae
    or-int v43, v43, v47

    #@2b0
    .line 3535
    const/high16 v47, 0x20

    #@2b2
    or-int v42, v42, v47

    #@2b4
    goto/16 :goto_6c

    #@2b6
    .line 3539
    :pswitch_2b6
    const/16 v47, 0x1

    #@2b8
    move/from16 v0, v47

    #@2ba
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2bd
    move-result v47

    #@2be
    if-nez v47, :cond_6c

    #@2c0
    .line 3540
    const/high16 v47, 0x1

    #@2c2
    or-int v43, v43, v47

    #@2c4
    .line 3541
    const/high16 v47, 0x1

    #@2c6
    or-int v42, v42, v47

    #@2c8
    goto/16 :goto_6c

    #@2ca
    .line 3545
    :pswitch_2ca
    const/16 v47, 0x0

    #@2cc
    move/from16 v0, v47

    #@2ce
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2d1
    move-result v47

    #@2d2
    if-eqz v47, :cond_6c

    #@2d4
    .line 3546
    const/high16 v47, 0x40

    #@2d6
    or-int v43, v43, v47

    #@2d8
    .line 3547
    const/high16 v47, 0x40

    #@2da
    or-int v42, v42, v47

    #@2dc
    goto/16 :goto_6c

    #@2de
    .line 3551
    :pswitch_2de
    const/16 v47, 0x0

    #@2e0
    move/from16 v0, v47

    #@2e2
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2e5
    move-result v44

    #@2e6
    .line 3552
    .local v44, visibility:I
    if-eqz v44, :cond_6c

    #@2e8
    .line 3553
    sget-object v47, Landroid/view/View;->VISIBILITY_FLAGS:[I

    #@2ea
    aget v47, v47, v44

    #@2ec
    or-int v43, v43, v47

    #@2ee
    .line 3554
    or-int/lit8 v42, v42, 0xc

    #@2f0
    goto/16 :goto_6c

    #@2f2
    .line 3559
    .end local v44           #visibility:I
    :pswitch_2f2
    move-object/from16 v0, p0

    #@2f4
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@2f6
    move/from16 v47, v0

    #@2f8
    and-int/lit8 v47, v47, -0x3d

    #@2fa
    move/from16 v0, v47

    #@2fc
    move-object/from16 v1, p0

    #@2fe
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@300
    .line 3562
    const/16 v47, -0x1

    #@302
    move/from16 v0, v47

    #@304
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@307
    move-result v17

    #@308
    .line 3563
    .restart local v17       #layoutDirection:I
    const/16 v47, -0x1

    #@30a
    move/from16 v0, v17

    #@30c
    move/from16 v1, v47

    #@30e
    if-eq v0, v1, :cond_326

    #@310
    sget-object v47, Landroid/view/View;->LAYOUT_DIRECTION_FLAGS:[I

    #@312
    aget v41, v47, v17

    #@314
    .line 3565
    .restart local v41       #value:I
    :goto_314
    move-object/from16 v0, p0

    #@316
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@318
    move/from16 v47, v0

    #@31a
    shl-int/lit8 v48, v41, 0x2

    #@31c
    or-int v47, v47, v48

    #@31e
    move/from16 v0, v47

    #@320
    move-object/from16 v1, p0

    #@322
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@324
    goto/16 :goto_6c

    #@326
    .line 3563
    .end local v41           #value:I
    :cond_326
    const/16 v41, 0x2

    #@328
    goto :goto_314

    #@329
    .line 3568
    .end local v17           #layoutDirection:I
    :pswitch_329
    const/16 v47, 0x0

    #@32b
    move/from16 v0, v47

    #@32d
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@330
    move-result v10

    #@331
    .line 3569
    .local v10, cacheQuality:I
    if-eqz v10, :cond_6c

    #@333
    .line 3570
    sget-object v47, Landroid/view/View;->DRAWING_CACHE_QUALITY_FLAGS:[I

    #@335
    aget v47, v47, v10

    #@337
    or-int v43, v43, v47

    #@339
    .line 3571
    const/high16 v47, 0x18

    #@33b
    or-int v42, v42, v47

    #@33d
    goto/16 :goto_6c

    #@33f
    .line 3575
    .end local v10           #cacheQuality:I
    :pswitch_33f
    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@342
    move-result-object v47

    #@343
    move-object/from16 v0, p0

    #@345
    move-object/from16 v1, v47

    #@347
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    #@34a
    goto/16 :goto_6c

    #@34c
    .line 3578
    :pswitch_34c
    const/16 v47, -0x1

    #@34e
    move/from16 v0, v47

    #@350
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@353
    move-result v47

    #@354
    move-object/from16 v0, p0

    #@356
    move/from16 v1, v47

    #@358
    invoke-virtual {v0, v1}, Landroid/view/View;->setLabelFor(I)V

    #@35b
    goto/16 :goto_6c

    #@35d
    .line 3581
    :pswitch_35d
    const/16 v47, 0x1

    #@35f
    move/from16 v0, v47

    #@361
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@364
    move-result v47

    #@365
    if-nez v47, :cond_6c

    #@367
    .line 3582
    const v47, -0x8000001

    #@36a
    and-int v43, v43, v47

    #@36c
    .line 3583
    const/high16 v47, 0x800

    #@36e
    or-int v42, v42, v47

    #@370
    goto/16 :goto_6c

    #@372
    .line 3587
    :pswitch_372
    const/16 v47, 0x1

    #@374
    move/from16 v0, v47

    #@376
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@379
    move-result v47

    #@37a
    if-nez v47, :cond_6c

    #@37c
    .line 3588
    const v47, -0x10000001

    #@37f
    and-int v43, v43, v47

    #@381
    .line 3589
    const/high16 v47, 0x1000

    #@383
    or-int v42, v42, v47

    #@385
    goto/16 :goto_6c

    #@387
    .line 3593
    :pswitch_387
    const/16 v47, 0x0

    #@389
    move/from16 v0, v47

    #@38b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@38e
    move-result v28

    #@38f
    .line 3594
    .local v28, scrollbars:I
    if-eqz v28, :cond_6c

    #@391
    .line 3595
    or-int v43, v43, v28

    #@393
    .line 3596
    move/from16 v0, v42

    #@395
    or-int/lit16 v0, v0, 0x300

    #@397
    move/from16 v42, v0

    #@399
    .line 3597
    const/16 v16, 0x1

    #@39b
    goto/16 :goto_6c

    #@39d
    .line 3602
    .end local v28           #scrollbars:I
    :pswitch_39d
    const/16 v47, 0xe

    #@39f
    move/from16 v0, v34

    #@3a1
    move/from16 v1, v47

    #@3a3
    if-ge v0, v1, :cond_6c

    #@3a5
    .line 3608
    :pswitch_3a5
    const/16 v47, 0x0

    #@3a7
    move/from16 v0, v47

    #@3a9
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3ac
    move-result v13

    #@3ad
    .line 3609
    .local v13, fadingEdge:I
    if-eqz v13, :cond_6c

    #@3af
    .line 3610
    or-int v43, v43, v13

    #@3b1
    .line 3611
    move/from16 v0, v42

    #@3b3
    or-int/lit16 v0, v0, 0x3000

    #@3b5
    move/from16 v42, v0

    #@3b7
    .line 3612
    move-object/from16 v0, p0

    #@3b9
    invoke-virtual {v0, v6}, Landroid/view/View;->initializeFadingEdge(Landroid/content/res/TypedArray;)V

    #@3bc
    goto/16 :goto_6c

    #@3be
    .line 3616
    .end local v13           #fadingEdge:I
    :pswitch_3be
    const/16 v47, 0x0

    #@3c0
    move/from16 v0, v47

    #@3c2
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3c5
    move-result v27

    #@3c6
    .line 3617
    if-eqz v27, :cond_6c

    #@3c8
    .line 3618
    const/high16 v47, 0x300

    #@3ca
    and-int v47, v47, v27

    #@3cc
    or-int v43, v43, v47

    #@3ce
    .line 3619
    const/high16 v47, 0x300

    #@3d0
    or-int v42, v42, v47

    #@3d2
    goto/16 :goto_6c

    #@3d4
    .line 3623
    :pswitch_3d4
    const/16 v29, 0x1

    #@3d6
    .line 3624
    const/16 v47, 0x0

    #@3d8
    move/from16 v0, v47

    #@3da
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3dd
    move-result v47

    #@3de
    if-eqz v47, :cond_6c

    #@3e0
    .line 3625
    const/16 v47, 0x1

    #@3e2
    move-object/from16 v0, p0

    #@3e4
    move/from16 v1, v47

    #@3e6
    invoke-virtual {v0, v1}, Landroid/view/View;->setScrollContainer(Z)V

    #@3e9
    goto/16 :goto_6c

    #@3eb
    .line 3629
    :pswitch_3eb
    const/16 v47, 0x0

    #@3ed
    move/from16 v0, v47

    #@3ef
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@3f2
    move-result v47

    #@3f3
    if-eqz v47, :cond_6c

    #@3f5
    .line 3630
    const/high16 v47, 0x400

    #@3f7
    or-int v43, v43, v47

    #@3f9
    .line 3631
    const/high16 v47, 0x400

    #@3fb
    or-int v42, v42, v47

    #@3fd
    goto/16 :goto_6c

    #@3ff
    .line 3635
    :pswitch_3ff
    const/16 v47, 0x0

    #@401
    move/from16 v0, v47

    #@403
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@406
    move-result v47

    #@407
    if-eqz v47, :cond_6c

    #@409
    .line 3636
    move/from16 v0, v43

    #@40b
    or-int/lit16 v0, v0, 0x400

    #@40d
    move/from16 v43, v0

    #@40f
    .line 3637
    move/from16 v0, v42

    #@411
    or-int/lit16 v0, v0, 0x400

    #@413
    move/from16 v42, v0

    #@415
    goto/16 :goto_6c

    #@417
    .line 3641
    :pswitch_417
    const/16 v47, -0x1

    #@419
    move/from16 v0, v47

    #@41b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@41e
    move-result v47

    #@41f
    move/from16 v0, v47

    #@421
    move-object/from16 v1, p0

    #@423
    iput v0, v1, Landroid/view/View;->mNextFocusLeftId:I

    #@425
    goto/16 :goto_6c

    #@427
    .line 3644
    :pswitch_427
    const/16 v47, -0x1

    #@429
    move/from16 v0, v47

    #@42b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@42e
    move-result v47

    #@42f
    move/from16 v0, v47

    #@431
    move-object/from16 v1, p0

    #@433
    iput v0, v1, Landroid/view/View;->mNextFocusRightId:I

    #@435
    goto/16 :goto_6c

    #@437
    .line 3647
    :pswitch_437
    const/16 v47, -0x1

    #@439
    move/from16 v0, v47

    #@43b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@43e
    move-result v47

    #@43f
    move/from16 v0, v47

    #@441
    move-object/from16 v1, p0

    #@443
    iput v0, v1, Landroid/view/View;->mNextFocusUpId:I

    #@445
    goto/16 :goto_6c

    #@447
    .line 3650
    :pswitch_447
    const/16 v47, -0x1

    #@449
    move/from16 v0, v47

    #@44b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@44e
    move-result v47

    #@44f
    move/from16 v0, v47

    #@451
    move-object/from16 v1, p0

    #@453
    iput v0, v1, Landroid/view/View;->mNextFocusDownId:I

    #@455
    goto/16 :goto_6c

    #@457
    .line 3653
    :pswitch_457
    const/16 v47, -0x1

    #@459
    move/from16 v0, v47

    #@45b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@45e
    move-result v47

    #@45f
    move/from16 v0, v47

    #@461
    move-object/from16 v1, p0

    #@463
    iput v0, v1, Landroid/view/View;->mNextFocusForwardId:I

    #@465
    goto/16 :goto_6c

    #@467
    .line 3656
    :pswitch_467
    const/16 v47, 0x0

    #@469
    move/from16 v0, v47

    #@46b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@46e
    move-result v47

    #@46f
    move/from16 v0, v47

    #@471
    move-object/from16 v1, p0

    #@473
    iput v0, v1, Landroid/view/View;->mMinWidth:I

    #@475
    goto/16 :goto_6c

    #@477
    .line 3659
    :pswitch_477
    const/16 v47, 0x0

    #@479
    move/from16 v0, v47

    #@47b
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@47e
    move-result v47

    #@47f
    move/from16 v0, v47

    #@481
    move-object/from16 v1, p0

    #@483
    iput v0, v1, Landroid/view/View;->mMinHeight:I

    #@485
    goto/16 :goto_6c

    #@487
    .line 3662
    :pswitch_487
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->isRestricted()Z

    #@48a
    move-result v47

    #@48b
    if-eqz v47, :cond_495

    #@48d
    .line 3663
    new-instance v47, Ljava/lang/IllegalStateException;

    #@48f
    const-string v48, "The android:onClick attribute cannot be used within a restricted context"

    #@491
    invoke-direct/range {v47 .. v48}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@494
    throw v47

    #@495
    .line 3667
    :cond_495
    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@498
    move-result-object v14

    #@499
    .line 3668
    .local v14, handlerName:Ljava/lang/String;
    if-eqz v14, :cond_6c

    #@49b
    .line 3669
    new-instance v47, Landroid/view/View$1;

    #@49d
    move-object/from16 v0, v47

    #@49f
    move-object/from16 v1, p0

    #@4a1
    invoke-direct {v0, v1, v14}, Landroid/view/View$1;-><init>(Landroid/view/View;Ljava/lang/String;)V

    #@4a4
    move-object/from16 v0, p0

    #@4a6
    move-object/from16 v1, v47

    #@4a8
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@4ab
    goto/16 :goto_6c

    #@4ad
    .line 3703
    .end local v14           #handlerName:Ljava/lang/String;
    :pswitch_4ad
    const/16 v47, 0x1

    #@4af
    move/from16 v0, v47

    #@4b1
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4b4
    move-result v20

    #@4b5
    .line 3704
    goto/16 :goto_6c

    #@4b7
    .line 3706
    :pswitch_4b7
    const/16 v47, 0x0

    #@4b9
    move/from16 v0, v47

    #@4bb
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4be
    move-result v47

    #@4bf
    move/from16 v0, v47

    #@4c1
    move-object/from16 v1, p0

    #@4c3
    iput v0, v1, Landroid/view/View;->mVerticalScrollbarPosition:I

    #@4c5
    goto/16 :goto_6c

    #@4c7
    .line 3709
    :pswitch_4c7
    const/16 v47, 0x0

    #@4c9
    move/from16 v0, v47

    #@4cb
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4ce
    move-result v47

    #@4cf
    const/16 v48, 0x0

    #@4d1
    move-object/from16 v0, p0

    #@4d3
    move/from16 v1, v47

    #@4d5
    move-object/from16 v2, v48

    #@4d7
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    #@4da
    goto/16 :goto_6c

    #@4dc
    .line 3713
    :pswitch_4dc
    move-object/from16 v0, p0

    #@4de
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@4e0
    move/from16 v47, v0

    #@4e2
    move/from16 v0, v47

    #@4e4
    and-int/lit16 v0, v0, -0x1c1

    #@4e6
    move/from16 v47, v0

    #@4e8
    move/from16 v0, v47

    #@4ea
    move-object/from16 v1, p0

    #@4ec
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@4ee
    .line 3715
    const/16 v47, -0x1

    #@4f0
    move/from16 v0, v47

    #@4f2
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4f5
    move-result v36

    #@4f6
    .line 3716
    .restart local v36       #textDirection:I
    const/16 v47, -0x1

    #@4f8
    move/from16 v0, v36

    #@4fa
    move/from16 v1, v47

    #@4fc
    if-eq v0, v1, :cond_6c

    #@4fe
    .line 3717
    move-object/from16 v0, p0

    #@500
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@502
    move/from16 v47, v0

    #@504
    sget-object v48, Landroid/view/View;->PFLAG2_TEXT_DIRECTION_FLAGS:[I

    #@506
    aget v48, v48, v36

    #@508
    or-int v47, v47, v48

    #@50a
    move/from16 v0, v47

    #@50c
    move-object/from16 v1, p0

    #@50e
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@510
    goto/16 :goto_6c

    #@512
    .line 3722
    .end local v36           #textDirection:I
    :pswitch_512
    move-object/from16 v0, p0

    #@514
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@516
    move/from16 v47, v0

    #@518
    const v48, -0xe001

    #@51b
    and-int v47, v47, v48

    #@51d
    move/from16 v0, v47

    #@51f
    move-object/from16 v1, p0

    #@521
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@523
    .line 3724
    const/16 v47, 0x1

    #@525
    move/from16 v0, v47

    #@527
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@52a
    move-result v35

    #@52b
    .line 3725
    .local v35, textAlignment:I
    move-object/from16 v0, p0

    #@52d
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@52f
    move/from16 v47, v0

    #@531
    sget-object v48, Landroid/view/View;->PFLAG2_TEXT_ALIGNMENT_FLAGS:[I

    #@533
    aget v48, v48, v35

    #@535
    or-int v47, v47, v48

    #@537
    move/from16 v0, v47

    #@539
    move-object/from16 v1, p0

    #@53b
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@53d
    goto/16 :goto_6c

    #@53f
    .line 3728
    .end local v35           #textAlignment:I
    :pswitch_53f
    const/16 v47, 0x0

    #@541
    move/from16 v0, v47

    #@543
    invoke-virtual {v6, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@546
    move-result v47

    #@547
    move-object/from16 v0, p0

    #@549
    move/from16 v1, v47

    #@54b
    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@54e
    goto/16 :goto_6c

    #@550
    .line 3737
    .end local v7           #attr:I
    :cond_550
    invoke-direct/range {p0 .. p0}, Landroid/view/View;->isRtlMetaDataExist()Z

    #@553
    move-result v47

    #@554
    if-eqz v47, :cond_5ce

    #@556
    move-object/from16 v0, p0

    #@558
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@55a
    move/from16 v47, v0

    #@55c
    if-eqz v47, :cond_5ce

    #@55e
    .line 3738
    move-object/from16 v0, p0

    #@560
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@562
    move/from16 v47, v0

    #@564
    and-int/lit8 v47, v47, 0xc

    #@566
    shr-int/lit8 v47, v47, 0x2

    #@568
    const/16 v48, 0x2

    #@56a
    move/from16 v0, v47

    #@56c
    move/from16 v1, v48

    #@56e
    if-ne v0, v1, :cond_596

    #@570
    .line 3740
    move-object/from16 v0, p0

    #@572
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@574
    move/from16 v47, v0

    #@576
    and-int/lit8 v47, v47, -0x3d

    #@578
    move/from16 v0, v47

    #@57a
    move-object/from16 v1, p0

    #@57c
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@57e
    .line 3741
    const/16 v17, 0x3

    #@580
    .line 3742
    .restart local v17       #layoutDirection:I
    sget-object v47, Landroid/view/View;->LAYOUT_DIRECTION_FLAGS:[I

    #@582
    const/16 v48, 0x3

    #@584
    aget v41, v47, v48

    #@586
    .line 3744
    .restart local v41       #value:I
    move-object/from16 v0, p0

    #@588
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@58a
    move/from16 v47, v0

    #@58c
    shl-int/lit8 v48, v41, 0x2

    #@58e
    or-int v47, v47, v48

    #@590
    move/from16 v0, v47

    #@592
    move-object/from16 v1, p0

    #@594
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@596
    .line 3746
    .end local v17           #layoutDirection:I
    .end local v41           #value:I
    :cond_596
    move-object/from16 v0, p0

    #@598
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@59a
    move/from16 v47, v0

    #@59c
    move/from16 v0, v47

    #@59e
    and-int/lit16 v0, v0, 0x1c0

    #@5a0
    move/from16 v47, v0

    #@5a2
    shr-int/lit8 v47, v47, 0x6

    #@5a4
    if-nez v47, :cond_5ce

    #@5a6
    .line 3748
    move-object/from16 v0, p0

    #@5a8
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@5aa
    move/from16 v47, v0

    #@5ac
    move/from16 v0, v47

    #@5ae
    and-int/lit16 v0, v0, -0x1c1

    #@5b0
    move/from16 v47, v0

    #@5b2
    move/from16 v0, v47

    #@5b4
    move-object/from16 v1, p0

    #@5b6
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@5b8
    .line 3749
    const/16 v36, 0x1

    #@5ba
    .line 3751
    .restart local v36       #textDirection:I
    move-object/from16 v0, p0

    #@5bc
    iget v0, v0, Landroid/view/View;->mPrivateFlags2:I

    #@5be
    move/from16 v47, v0

    #@5c0
    sget-object v48, Landroid/view/View;->PFLAG2_TEXT_DIRECTION_FLAGS:[I

    #@5c2
    const/16 v49, 0x1

    #@5c4
    aget v48, v48, v49

    #@5c6
    or-int v47, v47, v48

    #@5c8
    move/from16 v0, v47

    #@5ca
    move-object/from16 v1, p0

    #@5cc
    iput v0, v1, Landroid/view/View;->mPrivateFlags2:I

    #@5ce
    .line 3755
    .end local v36           #textDirection:I
    :cond_5ce
    move-object/from16 v0, p0

    #@5d0
    move/from16 v1, v20

    #@5d2
    invoke-virtual {v0, v1}, Landroid/view/View;->setOverScrollMode(I)V

    #@5d5
    .line 3760
    move/from16 v0, v30

    #@5d7
    move-object/from16 v1, p0

    #@5d9
    iput v0, v1, Landroid/view/View;->mUserPaddingStart:I

    #@5db
    .line 3761
    move-object/from16 v0, p0

    #@5dd
    iput v11, v0, Landroid/view/View;->mUserPaddingEnd:I

    #@5df
    .line 3763
    if-eqz v8, :cond_5e6

    #@5e1
    .line 3764
    move-object/from16 v0, p0

    #@5e3
    invoke-virtual {v0, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@5e6
    .line 3767
    :cond_5e6
    if-ltz v21, :cond_5fc

    #@5e8
    .line 3768
    move/from16 v18, v21

    #@5ea
    .line 3769
    move/from16 v37, v21

    #@5ec
    .line 3770
    move/from16 v22, v21

    #@5ee
    .line 3771
    move/from16 v9, v21

    #@5f0
    .line 3772
    move/from16 v0, v21

    #@5f2
    move-object/from16 v1, p0

    #@5f4
    iput v0, v1, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@5f6
    .line 3773
    move/from16 v0, v21

    #@5f8
    move-object/from16 v1, p0

    #@5fa
    iput v0, v1, Landroid/view/View;->mUserPaddingRightInitial:I

    #@5fc
    .line 3776
    :cond_5fc
    invoke-direct/range {p0 .. p0}, Landroid/view/View;->isRtlCompatibilityMode()Z

    #@5ff
    move-result v47

    #@600
    if-eqz v47, :cond_6be

    #@602
    .line 3784
    if-nez v19, :cond_608

    #@604
    if-eqz v31, :cond_608

    #@606
    .line 3785
    move/from16 v18, v30

    #@608
    .line 3787
    :cond_608
    if-ltz v18, :cond_6ae

    #@60a
    move/from16 v47, v18

    #@60c
    :goto_60c
    move/from16 v0, v47

    #@60e
    move-object/from16 v1, p0

    #@610
    iput v0, v1, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@612
    .line 3788
    if-nez v23, :cond_618

    #@614
    if-eqz v12, :cond_618

    #@616
    .line 3789
    move/from16 v22, v11

    #@618
    .line 3791
    :cond_618
    if-ltz v22, :cond_6b6

    #@61a
    move/from16 v47, v22

    #@61c
    :goto_61c
    move/from16 v0, v47

    #@61e
    move-object/from16 v1, p0

    #@620
    iput v0, v1, Landroid/view/View;->mUserPaddingRightInitial:I

    #@622
    .line 3806
    :cond_622
    :goto_622
    move-object/from16 v0, p0

    #@624
    iget v0, v0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@626
    move/from16 v47, v0

    #@628
    if-ltz v37, :cond_6d0

    #@62a
    .end local v37           #topPadding:I
    :goto_62a
    move-object/from16 v0, p0

    #@62c
    iget v0, v0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@62e
    move/from16 v48, v0

    #@630
    if-ltz v9, :cond_6d8

    #@632
    .end local v9           #bottomPadding:I
    :goto_632
    move-object/from16 v0, p0

    #@634
    move/from16 v1, v47

    #@636
    move/from16 v2, v37

    #@638
    move/from16 v3, v48

    #@63a
    invoke-virtual {v0, v1, v2, v3, v9}, Landroid/view/View;->internalSetPadding(IIII)V

    #@63d
    .line 3812
    if-eqz v42, :cond_648

    #@63f
    .line 3813
    move-object/from16 v0, p0

    #@641
    move/from16 v1, v43

    #@643
    move/from16 v2, v42

    #@645
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setFlags(II)V

    #@648
    .line 3816
    :cond_648
    if-eqz v16, :cond_64f

    #@64a
    .line 3817
    move-object/from16 v0, p0

    #@64c
    invoke-virtual {v0, v6}, Landroid/view/View;->initializeScrollbars(Landroid/content/res/TypedArray;)V

    #@64f
    .line 3820
    :cond_64f
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    #@652
    .line 3823
    if-eqz v27, :cond_657

    #@654
    .line 3824
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->recomputePadding()V

    #@657
    .line 3827
    :cond_657
    if-nez v45, :cond_65b

    #@659
    if-eqz v46, :cond_664

    #@65b
    .line 3828
    :cond_65b
    move-object/from16 v0, p0

    #@65d
    move/from16 v1, v45

    #@65f
    move/from16 v2, v46

    #@661
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->scrollTo(II)V

    #@664
    .line 3831
    :cond_664
    if-eqz v38, :cond_697

    #@666
    .line 3832
    move-object/from16 v0, p0

    #@668
    move/from16 v1, v39

    #@66a
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    #@66d
    .line 3833
    move-object/from16 v0, p0

    #@66f
    move/from16 v1, v40

    #@671
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    #@674
    .line 3834
    move-object/from16 v0, p0

    #@676
    move/from16 v1, v24

    #@678
    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    #@67b
    .line 3835
    move-object/from16 v0, p0

    #@67d
    move/from16 v1, v25

    #@67f
    invoke-virtual {v0, v1}, Landroid/view/View;->setRotationX(F)V

    #@682
    .line 3836
    move-object/from16 v0, p0

    #@684
    move/from16 v1, v26

    #@686
    invoke-virtual {v0, v1}, Landroid/view/View;->setRotationY(F)V

    #@689
    .line 3837
    move-object/from16 v0, p0

    #@68b
    move/from16 v1, v32

    #@68d
    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    #@690
    .line 3838
    move-object/from16 v0, p0

    #@692
    move/from16 v1, v33

    #@694
    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    #@697
    .line 3841
    :cond_697
    if-nez v29, :cond_6aa

    #@699
    move/from16 v0, v43

    #@69b
    and-int/lit16 v0, v0, 0x200

    #@69d
    move/from16 v47, v0

    #@69f
    if-eqz v47, :cond_6aa

    #@6a1
    .line 3842
    const/16 v47, 0x1

    #@6a3
    move-object/from16 v0, p0

    #@6a5
    move/from16 v1, v47

    #@6a7
    invoke-virtual {v0, v1}, Landroid/view/View;->setScrollContainer(Z)V

    #@6aa
    .line 3845
    :cond_6aa
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@6ad
    .line 3846
    return-void

    #@6ae
    .line 3787
    .restart local v9       #bottomPadding:I
    .restart local v37       #topPadding:I
    :cond_6ae
    move-object/from16 v0, p0

    #@6b0
    iget v0, v0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@6b2
    move/from16 v47, v0

    #@6b4
    goto/16 :goto_60c

    #@6b6
    .line 3791
    :cond_6b6
    move-object/from16 v0, p0

    #@6b8
    iget v0, v0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@6ba
    move/from16 v47, v0

    #@6bc
    goto/16 :goto_61c

    #@6be
    .line 3798
    :cond_6be
    if-eqz v19, :cond_6c6

    #@6c0
    .line 3799
    move/from16 v0, v18

    #@6c2
    move-object/from16 v1, p0

    #@6c4
    iput v0, v1, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@6c6
    .line 3801
    :cond_6c6
    if-eqz v23, :cond_622

    #@6c8
    .line 3802
    move/from16 v0, v22

    #@6ca
    move-object/from16 v1, p0

    #@6cc
    iput v0, v1, Landroid/view/View;->mUserPaddingRightInitial:I

    #@6ce
    goto/16 :goto_622

    #@6d0
    .line 3806
    :cond_6d0
    move-object/from16 v0, p0

    #@6d2
    iget v0, v0, Landroid/view/View;->mPaddingTop:I

    #@6d4
    move/from16 v37, v0

    #@6d6
    goto/16 :goto_62a

    #@6d8
    .end local v37           #topPadding:I
    :cond_6d8
    move-object/from16 v0, p0

    #@6da
    iget v9, v0, Landroid/view/View;->mPaddingBottom:I

    #@6dc
    goto/16 :goto_632

    #@6de
    .line 3380
    :pswitch_data_6de
    .packed-switch 0x7
        :pswitch_3be
        :pswitch_1a3
        :pswitch_1b3
        :pswitch_f4
        :pswitch_fe
        :pswitch_6f
        :pswitch_74
        :pswitch_8d
        :pswitch_ad
        :pswitch_b6
        :pswitch_d5
        :pswitch_264
        :pswitch_274
        :pswitch_2de
        :pswitch_254
        :pswitch_387
        :pswitch_39d
        :pswitch_6c
        :pswitch_417
        :pswitch_427
        :pswitch_437
        :pswitch_447
        :pswitch_28a
        :pswitch_2a2
        :pswitch_2b6
        :pswitch_329
        :pswitch_2ca
        :pswitch_467
        :pswitch_477
        :pswitch_35d
        :pswitch_3eb
        :pswitch_3d4
        :pswitch_372
        :pswitch_487
        :pswitch_33f
        :pswitch_6c
        :pswitch_6c
        :pswitch_6c
        :pswitch_4ad
        :pswitch_3ff
        :pswitch_108
        :pswitch_119
        :pswitch_12f
        :pswitch_145
        :pswitch_156
        :pswitch_18b
        :pswitch_197
        :pswitch_167
        :pswitch_173
        :pswitch_17f
        :pswitch_4b7
        :pswitch_457
        :pswitch_4c7
        :pswitch_3a5
        :pswitch_53f
        :pswitch_4dc
        :pswitch_512
        :pswitch_2f2
        :pswitch_de
        :pswitch_e9
        :pswitch_34c
    .end packed-switch
.end method

.method static synthetic access$2302(Landroid/view/View;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 685
    iput-boolean p1, p0, Landroid/view/View;->mHasPerformedLongPress:Z

    #@2
    return p1
.end method

.method static synthetic access$2400(Landroid/view/View;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 685
    invoke-direct {p0, p1}, Landroid/view/View;->checkForLongClick(I)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Landroid/view/View;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 685
    iget v0, p0, Landroid/view/View;->mLabelForId:I

    #@2
    return v0
.end method

.method private canResolveTextAlignment()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 17249
    invoke-virtual {p0}, Landroid/view/View;->getRawTextAlignment()I

    #@4
    move-result v0

    #@5
    packed-switch v0, :pswitch_data_22

    #@8
    move v0, v1

    #@9
    .line 17254
    :goto_9
    return v0

    #@a
    .line 17251
    :pswitch_a
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    if-eqz v0, :cond_20

    #@e
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@10
    instance-of v0, v0, Landroid/view/View;

    #@12
    if-eqz v0, :cond_20

    #@14
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@16
    check-cast v0, Landroid/view/View;

    #@18
    invoke-direct {v0}, Landroid/view/View;->canResolveTextAlignment()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    move v0, v1

    #@1f
    goto :goto_9

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_9

    #@22
    .line 17249
    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_a
    .end packed-switch
.end method

.method private canResolveTextDirection()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 17036
    invoke-virtual {p0}, Landroid/view/View;->getRawTextDirection()I

    #@4
    move-result v0

    #@5
    packed-switch v0, :pswitch_data_22

    #@8
    move v0, v1

    #@9
    .line 17041
    :goto_9
    return v0

    #@a
    .line 17038
    :pswitch_a
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    if-eqz v0, :cond_20

    #@e
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@10
    instance-of v0, v0, Landroid/view/View;

    #@12
    if-eqz v0, :cond_20

    #@14
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@16
    check-cast v0, Landroid/view/View;

    #@18
    invoke-direct {v0}, Landroid/view/View;->canResolveTextDirection()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    move v0, v1

    #@1f
    goto :goto_9

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_9

    #@22
    .line 17036
    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_a
    .end packed-switch
.end method

.method private checkForLongClick(I)V
    .registers 5
    .parameter "delayOffset"

    #@0
    .prologue
    const/high16 v1, 0x20

    #@2
    .line 16699
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_25

    #@7
    .line 16700
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/view/View;->mHasPerformedLongPress:Z

    #@a
    .line 16702
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

    #@c
    if-nez v0, :cond_15

    #@e
    .line 16703
    new-instance v0, Landroid/view/View$CheckForLongPress;

    #@10
    invoke-direct {v0, p0}, Landroid/view/View$CheckForLongPress;-><init>(Landroid/view/View;)V

    #@13
    iput-object v0, p0, Landroid/view/View;->mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

    #@15
    .line 16705
    :cond_15
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

    #@17
    invoke-virtual {v0}, Landroid/view/View$CheckForLongPress;->rememberWindowAttachCount()V

    #@1a
    .line 16706
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

    #@1c
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@1f
    move-result v1

    #@20
    sub-int/2addr v1, p1

    #@21
    int-to-long v1, v1

    #@22
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    #@25
    .line 16709
    :cond_25
    return-void
.end method

.method private clearDisplayList()V
    .registers 2

    #@0
    .prologue
    .line 12927
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 12928
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@6
    invoke-virtual {v0}, Landroid/view/DisplayList;->invalidate()V

    #@9
    .line 12929
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@b
    invoke-virtual {v0}, Landroid/view/DisplayList;->clear()V

    #@e
    .line 12931
    :cond_e
    return-void
.end method

.method public static combineMeasuredStates(II)I
    .registers 3
    .parameter "curState"
    .parameter "newState"

    #@0
    .prologue
    .line 15864
    or-int v0, p0, p1

    #@2
    return v0
.end method

.method protected static debugIndent(I)Ljava/lang/String;
    .registers 5
    .parameter "depth"

    #@0
    .prologue
    const/16 v3, 0x20

    #@2
    .line 15691
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    mul-int/lit8 v2, p0, 0x2

    #@6
    add-int/lit8 v2, v2, 0x3

    #@8
    mul-int/lit8 v2, v2, 0x2

    #@a
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 15692
    .local v1, spaces:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    mul-int/lit8 v2, p0, 0x2

    #@10
    add-int/lit8 v2, v2, 0x3

    #@12
    if-ge v0, v2, :cond_1e

    #@14
    .line 15693
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1b
    .line 15692
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_e

    #@1e
    .line 15695
    :cond_1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    return-object v2
.end method

.method private dispatchGenericMotionEventInternal(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 7463
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@4
    .line 7464
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_1d

    #@6
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$400(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnGenericMotionListener;

    #@9
    move-result-object v3

    #@a
    if-eqz v3, :cond_1d

    #@c
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@e
    and-int/lit8 v3, v3, 0x20

    #@10
    if-nez v3, :cond_1d

    #@12
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$400(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnGenericMotionListener;

    #@15
    move-result-object v3

    #@16
    invoke-interface {v3, p0, p1}, Landroid/view/View$OnGenericMotionListener;->onGenericMotion(Landroid/view/View;Landroid/view/MotionEvent;)Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_1d

    #@1c
    .line 7477
    :cond_1c
    :goto_1c
    return v1

    #@1d
    .line 7470
    :cond_1d
    invoke-virtual {p0, p1}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@20
    move-result v3

    #@21
    if-nez v3, :cond_1c

    #@23
    .line 7474
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@25
    if-eqz v1, :cond_2c

    #@27
    .line 7475
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@29
    invoke-virtual {v1, p1, v2}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@2c
    :cond_2c
    move v1, v2

    #@2d
    .line 7477
    goto :goto_1c
.end method

.method private drawAnimation(Landroid/view/ViewGroup;JLandroid/view/animation/Animation;Z)Z
    .registers 21
    .parameter "parent"
    .parameter "drawingTime"
    .parameter "a"
    .parameter "scalingRequired"

    #@0
    .prologue
    .line 13424
    move-object/from16 v0, p1

    #@2
    iget v10, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@4
    .line 13425
    .local v10, flags:I
    invoke-virtual/range {p4 .. p4}, Landroid/view/animation/Animation;->isInitialized()Z

    #@7
    move-result v11

    #@8
    .line 13426
    .local v11, initialized:Z
    if-nez v11, :cond_42

    #@a
    .line 13427
    iget v3, p0, Landroid/view/View;->mRight:I

    #@c
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@e
    sub-int/2addr v3, v4

    #@f
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@11
    iget v5, p0, Landroid/view/View;->mTop:I

    #@13
    sub-int/2addr v4, v5

    #@14
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getWidth()I

    #@17
    move-result v5

    #@18
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getHeight()I

    #@1b
    move-result v6

    #@1c
    move-object/from16 v0, p4

    #@1e
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@21
    .line 13428
    const/4 v3, 0x0

    #@22
    const/4 v4, 0x0

    #@23
    iget v5, p0, Landroid/view/View;->mRight:I

    #@25
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@27
    sub-int/2addr v5, v6

    #@28
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@2a
    iget v7, p0, Landroid/view/View;->mTop:I

    #@2c
    sub-int/2addr v6, v7

    #@2d
    move-object/from16 v0, p4

    #@2f
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/animation/Animation;->initializeInvalidateRegion(IIII)V

    #@32
    .line 13429
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@34
    if-eqz v3, :cond_3f

    #@36
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@38
    iget-object v3, v3, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@3a
    move-object/from16 v0, p4

    #@3c
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setListenerHandler(Landroid/os/Handler;)V

    #@3f
    .line 13430
    :cond_3f
    invoke-virtual {p0}, Landroid/view/View;->onAnimationStart()V

    #@42
    .line 13433
    :cond_42
    move-object/from16 v0, p1

    #@44
    iget-object v3, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@46
    const/high16 v4, 0x3f80

    #@48
    move-object/from16 v0, p4

    #@4a
    move-wide/from16 v1, p2

    #@4c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;F)Z

    #@4f
    move-result v13

    #@50
    .line 13434
    .local v13, more:Z
    if-eqz p5, :cond_91

    #@52
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@54
    iget v3, v3, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@56
    const/high16 v4, 0x3f80

    #@58
    cmpl-float v3, v3, v4

    #@5a
    if-eqz v3, :cond_91

    #@5c
    .line 13435
    move-object/from16 v0, p1

    #@5e
    iget-object v3, v0, Landroid/view/ViewGroup;->mInvalidationTransformation:Landroid/view/animation/Transformation;

    #@60
    if-nez v3, :cond_6b

    #@62
    .line 13436
    new-instance v3, Landroid/view/animation/Transformation;

    #@64
    invoke-direct {v3}, Landroid/view/animation/Transformation;-><init>()V

    #@67
    move-object/from16 v0, p1

    #@69
    iput-object v3, v0, Landroid/view/ViewGroup;->mInvalidationTransformation:Landroid/view/animation/Transformation;

    #@6b
    .line 13438
    :cond_6b
    move-object/from16 v0, p1

    #@6d
    iget-object v9, v0, Landroid/view/ViewGroup;->mInvalidationTransformation:Landroid/view/animation/Transformation;

    #@6f
    .line 13439
    .local v9, invalidationTransform:Landroid/view/animation/Transformation;
    const/high16 v3, 0x3f80

    #@71
    move-object/from16 v0, p4

    #@73
    move-wide/from16 v1, p2

    #@75
    invoke-virtual {v0, v1, v2, v9, v3}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;F)Z

    #@78
    .line 13444
    :goto_78
    if-eqz v13, :cond_90

    #@7a
    .line 13445
    invoke-virtual/range {p4 .. p4}, Landroid/view/animation/Animation;->willChangeBounds()Z

    #@7d
    move-result v3

    #@7e
    if-nez v3, :cond_b2

    #@80
    .line 13446
    and-int/lit16 v3, v10, 0x90

    #@82
    const/16 v4, 0x80

    #@84
    if-ne v3, v4, :cond_96

    #@86
    .line 13448
    move-object/from16 v0, p1

    #@88
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@8a
    or-int/lit8 v3, v3, 0x4

    #@8c
    move-object/from16 v0, p1

    #@8e
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@90
    .line 13473
    :cond_90
    :goto_90
    return v13

    #@91
    .line 13441
    .end local v9           #invalidationTransform:Landroid/view/animation/Transformation;
    :cond_91
    move-object/from16 v0, p1

    #@93
    iget-object v9, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@95
    .restart local v9       #invalidationTransform:Landroid/view/animation/Transformation;
    goto :goto_78

    #@96
    .line 13449
    :cond_96
    and-int/lit8 v3, v10, 0x4

    #@98
    if-nez v3, :cond_90

    #@9a
    .line 13452
    move-object/from16 v0, p1

    #@9c
    iget v3, v0, Landroid/view/ViewGroup;->mPrivateFlags:I

    #@9e
    or-int/lit8 v3, v3, 0x40

    #@a0
    move-object/from16 v0, p1

    #@a2
    iput v3, v0, Landroid/view/ViewGroup;->mPrivateFlags:I

    #@a4
    .line 13453
    iget v3, p0, Landroid/view/View;->mLeft:I

    #@a6
    iget v4, p0, Landroid/view/View;->mTop:I

    #@a8
    iget v5, p0, Landroid/view/View;->mRight:I

    #@aa
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@ac
    move-object/from16 v0, p1

    #@ae
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/ViewGroup;->invalidate(IIII)V

    #@b1
    goto :goto_90

    #@b2
    .line 13456
    :cond_b2
    move-object/from16 v0, p1

    #@b4
    iget-object v3, v0, Landroid/view/ViewGroup;->mInvalidateRegion:Landroid/graphics/RectF;

    #@b6
    if-nez v3, :cond_c1

    #@b8
    .line 13457
    new-instance v3, Landroid/graphics/RectF;

    #@ba
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    #@bd
    move-object/from16 v0, p1

    #@bf
    iput-object v3, v0, Landroid/view/ViewGroup;->mInvalidateRegion:Landroid/graphics/RectF;

    #@c1
    .line 13459
    :cond_c1
    move-object/from16 v0, p1

    #@c3
    iget-object v8, v0, Landroid/view/ViewGroup;->mInvalidateRegion:Landroid/graphics/RectF;

    #@c5
    .line 13460
    .local v8, region:Landroid/graphics/RectF;
    const/4 v4, 0x0

    #@c6
    const/4 v5, 0x0

    #@c7
    iget v3, p0, Landroid/view/View;->mRight:I

    #@c9
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@cb
    sub-int v6, v3, v6

    #@cd
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@cf
    iget v7, p0, Landroid/view/View;->mTop:I

    #@d1
    sub-int v7, v3, v7

    #@d3
    move-object/from16 v3, p4

    #@d5
    invoke-virtual/range {v3 .. v9}, Landroid/view/animation/Animation;->getInvalidateRegion(IIIILandroid/graphics/RectF;Landroid/view/animation/Transformation;)V

    #@d8
    .line 13465
    move-object/from16 v0, p1

    #@da
    iget v3, v0, Landroid/view/ViewGroup;->mPrivateFlags:I

    #@dc
    or-int/lit8 v3, v3, 0x40

    #@de
    move-object/from16 v0, p1

    #@e0
    iput v3, v0, Landroid/view/ViewGroup;->mPrivateFlags:I

    #@e2
    .line 13467
    iget v3, p0, Landroid/view/View;->mLeft:I

    #@e4
    iget v4, v8, Landroid/graphics/RectF;->left:F

    #@e6
    float-to-int v4, v4

    #@e7
    add-int v12, v3, v4

    #@e9
    .line 13468
    .local v12, left:I
    iget v3, p0, Landroid/view/View;->mTop:I

    #@eb
    iget v4, v8, Landroid/graphics/RectF;->top:F

    #@ed
    float-to-int v4, v4

    #@ee
    add-int v14, v3, v4

    #@f0
    .line 13469
    .local v14, top:I
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    #@f3
    move-result v3

    #@f4
    const/high16 v4, 0x3f00

    #@f6
    add-float/2addr v3, v4

    #@f7
    float-to-int v3, v3

    #@f8
    add-int/2addr v3, v12

    #@f9
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    #@fc
    move-result v4

    #@fd
    const/high16 v5, 0x3f00

    #@ff
    add-float/2addr v4, v5

    #@100
    float-to-int v4, v4

    #@101
    add-int/2addr v4, v14

    #@102
    move-object/from16 v0, p1

    #@104
    invoke-virtual {v0, v12, v14, v3, v4}, Landroid/view/ViewGroup;->invalidate(IIII)V

    #@107
    goto :goto_90
.end method

.method private static dumpFlag(Ljava/util/HashMap;Ljava/lang/String;I)V
    .registers 11
    .parameter
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .local p0, found:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v7, 0x0

    #@1
    .line 18655
    const-string v4, "%32s"

    #@3
    const/4 v5, 0x1

    #@4
    new-array v5, v5, [Ljava/lang/Object;

    #@6
    invoke-static {p2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    aput-object v6, v5, v7

    #@c
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@f
    move-result-object v4

    #@10
    const/16 v5, 0x30

    #@12
    const/16 v6, 0x20

    #@14
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 18656
    .local v0, bits:Ljava/lang/String;
    const/16 v4, 0x5f

    #@1a
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@1d
    move-result v3

    #@1e
    .line 18657
    .local v3, prefix:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    if-lez v3, :cond_54

    #@25
    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    :goto_29
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    .line 18658
    .local v1, key:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, " "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    .line 18659
    .local v2, output:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    .line 18660
    return-void

    #@54
    .end local v1           #key:Ljava/lang/String;
    .end local v2           #output:Ljava/lang/String;
    :cond_54
    move-object v4, p1

    #@55
    .line 18657
    goto :goto_29
.end method

.method private static dumpFlags()V
    .registers 15

    #@0
    .prologue
    .line 18625
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v4

    #@4
    .line 18627
    .local v4, found:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_4
    const-class v13, Landroid/view/View;

    #@6
    invoke-virtual {v13}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    #@9
    move-result-object v1

    #@a
    .local v1, arr$:[Ljava/lang/reflect/Field;
    array-length v9, v1

    #@b
    .local v9, len$:I
    const/4 v6, 0x0

    #@c
    .local v6, i$:I
    :goto_c
    if-ge v6, v9, :cond_86

    #@e
    aget-object v3, v1, v6

    #@10
    .line 18628
    .local v3, field:Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I

    #@13
    move-result v10

    #@14
    .line 18629
    .local v10, modifiers:I
    invoke-static {v10}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    #@17
    move-result v13

    #@18
    if-eqz v13, :cond_38

    #@1a
    invoke-static {v10}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    #@1d
    move-result v13

    #@1e
    if-eqz v13, :cond_38

    #@20
    .line 18630
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@23
    move-result-object v13

    #@24
    sget-object v14, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@26
    invoke-virtual {v13, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v13

    #@2a
    if-eqz v13, :cond_3b

    #@2c
    .line 18631
    const/4 v13, 0x0

    #@2d
    invoke-virtual {v3, v13}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    #@30
    move-result v11

    #@31
    .line 18632
    .local v11, value:I
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@34
    move-result-object v13

    #@35
    invoke-static {v4, v13, v11}, Landroid/view/View;->dumpFlag(Ljava/util/HashMap;Ljava/lang/String;I)V

    #@38
    .line 18627
    .end local v11           #value:I
    :cond_38
    add-int/lit8 v6, v6, 0x1

    #@3a
    goto :goto_c

    #@3b
    .line 18633
    :cond_3b
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@3e
    move-result-object v13

    #@3f
    const-class v14, [I

    #@41
    invoke-virtual {v13, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v13

    #@45
    if-eqz v13, :cond_38

    #@47
    .line 18634
    const/4 v13, 0x0

    #@48
    invoke-virtual {v3, v13}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    move-result-object v13

    #@4c
    check-cast v13, [I

    #@4e
    move-object v0, v13

    #@4f
    check-cast v0, [I

    #@51
    move-object v12, v0

    #@52
    .line 18635
    .local v12, values:[I
    const/4 v5, 0x0

    #@53
    .local v5, i:I
    :goto_53
    array-length v13, v12

    #@54
    if-ge v5, v13, :cond_38

    #@56
    .line 18636
    new-instance v13, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@5e
    move-result-object v14

    #@5f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v13

    #@63
    const-string v14, "["

    #@65
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v13

    #@69
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v13

    #@6d
    const-string v14, "]"

    #@6f
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v13

    #@73
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v13

    #@77
    aget v14, v12, v5

    #@79
    invoke-static {v4, v13, v14}, Landroid/view/View;->dumpFlag(Ljava/util/HashMap;Ljava/lang/String;I)V
    :try_end_7c
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_7c} :catch_7f

    #@7c
    .line 18635
    add-int/lit8 v5, v5, 0x1

    #@7e
    goto :goto_53

    #@7f
    .line 18641
    .end local v1           #arr$:[Ljava/lang/reflect/Field;
    .end local v3           #field:Ljava/lang/reflect/Field;
    .end local v5           #i:I
    .end local v6           #i$:I
    .end local v9           #len$:I
    .end local v10           #modifiers:I
    .end local v12           #values:[I
    :catch_7f
    move-exception v2

    #@80
    .line 18642
    .local v2, e:Ljava/lang/IllegalAccessException;
    new-instance v13, Ljava/lang/RuntimeException;

    #@82
    invoke-direct {v13, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@85
    throw v13

    #@86
    .line 18645
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    .restart local v1       #arr$:[Ljava/lang/reflect/Field;
    .restart local v6       #i$:I
    .restart local v9       #len$:I
    :cond_86
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@89
    move-result-object v8

    #@8a
    .line 18646
    .local v8, keys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@8d
    move-result-object v13

    #@8e
    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@91
    .line 18647
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@94
    .line 18648
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@97
    move-result-object v6

    #@98
    .local v6, i$:Ljava/util/Iterator;
    :goto_98
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@9b
    move-result v13

    #@9c
    if-eqz v13, :cond_b0

    #@9e
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a1
    move-result-object v7

    #@a2
    check-cast v7, Ljava/lang/String;

    #@a4
    .line 18649
    .local v7, key:Ljava/lang/String;
    const-string v14, "View"

    #@a6
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a9
    move-result-object v13

    #@aa
    check-cast v13, Ljava/lang/String;

    #@ac
    invoke-static {v14, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    goto :goto_98

    #@b0
    .line 18651
    .end local v7           #key:Ljava/lang/String;
    :cond_b0
    return-void
.end method

.method private findLabelForView(Landroid/view/View;I)Landroid/view/View;
    .registers 5
    .parameter "view"
    .parameter "labeledId"

    #@0
    .prologue
    .line 5179
    iget-object v0, p0, Landroid/view/View;->mMatchLabelForPredicate:Landroid/view/View$MatchLabelForPredicate;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 5180
    new-instance v0, Landroid/view/View$MatchLabelForPredicate;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Landroid/view/View$MatchLabelForPredicate;-><init>(Landroid/view/View;Landroid/view/View$1;)V

    #@a
    iput-object v0, p0, Landroid/view/View;->mMatchLabelForPredicate:Landroid/view/View$MatchLabelForPredicate;

    #@c
    .line 5182
    :cond_c
    iget-object v0, p0, Landroid/view/View;->mMatchLabelForPredicate:Landroid/view/View$MatchLabelForPredicate;

    #@e
    invoke-static {v0, p2}, Landroid/view/View$MatchLabelForPredicate;->access$802(Landroid/view/View$MatchLabelForPredicate;I)I

    #@11
    .line 5183
    iget-object v0, p0, Landroid/view/View;->mMatchLabelForPredicate:Landroid/view/View$MatchLabelForPredicate;

    #@13
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->findViewByPredicateInsideOut(Landroid/view/View;Lcom/android/internal/util/Predicate;)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method private findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;
    .registers 7
    .parameter "root"
    .parameter "id"

    #@0
    .prologue
    .line 6469
    iget-object v1, p0, Landroid/view/View;->mMatchIdPredicate:Landroid/view/View$MatchIdPredicate;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 6470
    new-instance v1, Landroid/view/View$MatchIdPredicate;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-direct {v1, p0, v2}, Landroid/view/View$MatchIdPredicate;-><init>(Landroid/view/View;Landroid/view/View$1;)V

    #@a
    iput-object v1, p0, Landroid/view/View;->mMatchIdPredicate:Landroid/view/View$MatchIdPredicate;

    #@c
    .line 6472
    :cond_c
    iget-object v1, p0, Landroid/view/View;->mMatchIdPredicate:Landroid/view/View$MatchIdPredicate;

    #@e
    iput p2, v1, Landroid/view/View$MatchIdPredicate;->mId:I

    #@10
    .line 6473
    iget-object v1, p0, Landroid/view/View;->mMatchIdPredicate:Landroid/view/View$MatchIdPredicate;

    #@12
    invoke-virtual {p1, p0, v1}, Landroid/view/View;->findViewByPredicateInsideOut(Landroid/view/View;Lcom/android/internal/util/Predicate;)Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 6474
    .local v0, result:Landroid/view/View;
    if-nez v0, :cond_30

    #@18
    .line 6475
    const-string v1, "View"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "couldn\'t find view with id "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 6477
    :cond_30
    return-object v0
.end method

.method public static generateViewId()I
    .registers 3

    #@0
    .prologue
    .line 17295
    .local v0, newValue:I
    .local v1, result:I
    :cond_0
    sget-object v2, Landroid/view/View;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v1

    #@6
    .line 17297
    add-int/lit8 v0, v1, 0x1

    #@8
    .line 17298
    const v2, 0xffffff

    #@b
    if-le v0, v2, :cond_e

    #@d
    const/4 v0, 0x1

    #@e
    .line 17299
    :cond_e
    sget-object v2, Landroid/view/View;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    #@10
    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_0

    #@16
    .line 17300
    return v1
.end method

.method public static getDefaultSize(II)I
    .registers 5
    .parameter "size"
    .parameter "measureSpec"

    #@0
    .prologue
    .line 15920
    move v0, p0

    #@1
    .line 15921
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v1

    #@5
    .line 15922
    .local v1, specMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v2

    #@9
    .line 15924
    .local v2, specSize:I
    sparse-switch v1, :sswitch_data_12

    #@c
    .line 15933
    :goto_c
    return v0

    #@d
    .line 15926
    :sswitch_d
    move v0, p0

    #@e
    .line 15927
    goto :goto_c

    #@f
    .line 15930
    :sswitch_f
    move v0, v2

    #@10
    goto :goto_c

    #@11
    .line 15924
    nop

    #@12
    :sswitch_data_12
    .sparse-switch
        -0x80000000 -> :sswitch_f
        0x0 -> :sswitch_d
        0x40000000 -> :sswitch_f
    .end sparse-switch
.end method

.method private getDisplayList(Landroid/view/DisplayList;Z)Landroid/view/DisplayList;
    .registers 17
    .parameter "displayList"
    .parameter "isLayer"

    #@0
    .prologue
    .line 12803
    invoke-virtual {p0}, Landroid/view/View;->canHaveDisplayList()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    .line 12804
    const/4 v1, 0x0

    #@7
    .line 12897
    :goto_7
    return-object v1

    #@8
    .line 12807
    :cond_8
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    const v2, 0x8000

    #@d
    and-int/2addr v1, v2

    #@e
    if-eqz v1, :cond_1e

    #@10
    if-eqz p1, :cond_1e

    #@12
    invoke-virtual {p1}, Landroid/view/DisplayList;->isValid()Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1e

    #@18
    if-nez p2, :cond_119

    #@1a
    iget-boolean v1, p0, Landroid/view/View;->mRecreateDisplayList:Z

    #@1c
    if-eqz v1, :cond_119

    #@1e
    .line 12812
    :cond_1e
    if-eqz p1, :cond_41

    #@20
    invoke-virtual {p1}, Landroid/view/DisplayList;->isValid()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_41

    #@26
    if-nez p2, :cond_41

    #@28
    iget-boolean v1, p0, Landroid/view/View;->mRecreateDisplayList:Z

    #@2a
    if-nez v1, :cond_41

    #@2c
    .line 12814
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2e
    const v2, 0x8020

    #@31
    or-int/2addr v1, v2

    #@32
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@34
    .line 12815
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@36
    const v2, -0x600001

    #@39
    and-int/2addr v1, v2

    #@3a
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@3c
    .line 12816
    invoke-virtual {p0}, Landroid/view/View;->dispatchGetDisplayList()V

    #@3f
    move-object v1, p1

    #@40
    .line 12818
    goto :goto_7

    #@41
    .line 12821
    :cond_41
    if-nez p2, :cond_46

    #@43
    .line 12824
    const/4 v1, 0x1

    #@44
    iput-boolean v1, p0, Landroid/view/View;->mRecreateDisplayList:Z

    #@46
    .line 12826
    :cond_46
    if-nez p1, :cond_5b

    #@48
    .line 12827
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@4f
    move-result-object v12

    #@50
    .line 12828
    .local v12, name:Ljava/lang/String;
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@52
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@54
    invoke-virtual {v1, v12}, Landroid/view/HardwareRenderer;->createDisplayList(Ljava/lang/String;)Landroid/view/DisplayList;

    #@57
    move-result-object p1

    #@58
    .line 12832
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@5b
    .line 12835
    .end local v12           #name:Ljava/lang/String;
    :cond_5b
    const/4 v8, 0x0

    #@5c
    .line 12836
    .local v8, caching:Z
    invoke-virtual {p1}, Landroid/view/DisplayList;->start()Landroid/view/HardwareCanvas;

    #@5f
    move-result-object v0

    #@60
    .line 12837
    .local v0, canvas:Landroid/view/HardwareCanvas;
    iget v1, p0, Landroid/view/View;->mRight:I

    #@62
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@64
    sub-int v13, v1, v2

    #@66
    .line 12838
    .local v13, width:I
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@68
    iget v2, p0, Landroid/view/View;->mTop:I

    #@6a
    sub-int v9, v1, v2

    #@6c
    .line 12841
    .local v9, height:I
    :try_start_6c
    invoke-virtual {v0, v13, v9}, Landroid/view/HardwareCanvas;->setViewport(II)V

    #@6f
    .line 12843
    const/4 v1, 0x0

    #@70
    invoke-virtual {v0, v1}, Landroid/view/HardwareCanvas;->onPreDraw(Landroid/graphics/Rect;)I

    #@73
    .line 12844
    invoke-virtual {p0}, Landroid/view/View;->getLayerType()I

    #@76
    move-result v11

    #@77
    .line 12845
    .local v11, layerType:I
    if-nez p2, :cond_e1

    #@79
    if-eqz v11, :cond_e1

    #@7b
    .line 12846
    const/4 v1, 0x2

    #@7c
    if-ne v11, v1, :cond_cd

    #@7e
    .line 12847
    invoke-virtual {p0}, Landroid/view/View;->getHardwareLayer()Landroid/view/HardwareLayer;

    #@81
    move-result-object v10

    #@82
    .line 12848
    .local v10, layer:Landroid/view/HardwareLayer;
    if-eqz v10, :cond_a5

    #@84
    invoke-virtual {v10}, Landroid/view/HardwareLayer;->isValid()Z

    #@87
    move-result v1

    #@88
    if-eqz v1, :cond_a5

    #@8a
    .line 12849
    const/4 v1, 0x0

    #@8b
    const/4 v2, 0x0

    #@8c
    iget-object v3, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@8e
    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/view/HardwareCanvas;->drawHardwareLayer(Landroid/view/HardwareLayer;FFLandroid/graphics/Paint;)V
    :try_end_91
    .catchall {:try_start_6c .. :try_end_91} :catchall_bb

    #@91
    .line 12855
    :goto_91
    const/4 v8, 0x1

    #@92
    .line 12882
    .end local v10           #layer:Landroid/view/HardwareLayer;
    :cond_92
    :goto_92
    invoke-virtual {v0}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@95
    .line 12884
    invoke-virtual {p1}, Landroid/view/DisplayList;->end()V

    #@98
    .line 12885
    invoke-virtual {p1, v8}, Landroid/view/DisplayList;->setCaching(Z)V

    #@9b
    .line 12886
    if-eqz p2, :cond_111

    #@9d
    .line 12887
    const/4 v1, 0x0

    #@9e
    const/4 v2, 0x0

    #@9f
    invoke-virtual {p1, v1, v2, v13, v9}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    #@a2
    .end local v0           #canvas:Landroid/view/HardwareCanvas;
    .end local v8           #caching:Z
    .end local v9           #height:I
    .end local v11           #layerType:I
    .end local v13           #width:I
    :cond_a2
    :goto_a2
    move-object v1, p1

    #@a3
    .line 12897
    goto/16 :goto_7

    #@a5
    .line 12851
    .restart local v0       #canvas:Landroid/view/HardwareCanvas;
    .restart local v8       #caching:Z
    .restart local v9       #height:I
    .restart local v10       #layer:Landroid/view/HardwareLayer;
    .restart local v11       #layerType:I
    .restart local v13       #width:I
    :cond_a5
    const/4 v1, 0x0

    #@a6
    const/4 v2, 0x0

    #@a7
    :try_start_a7
    iget v3, p0, Landroid/view/View;->mRight:I

    #@a9
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@ab
    sub-int/2addr v3, v4

    #@ac
    int-to-float v3, v3

    #@ad
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@af
    iget v5, p0, Landroid/view/View;->mTop:I

    #@b1
    sub-int/2addr v4, v5

    #@b2
    int-to-float v4, v4

    #@b3
    iget-object v5, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@b5
    const/16 v6, 0x14

    #@b7
    invoke-virtual/range {v0 .. v6}, Landroid/view/HardwareCanvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I
    :try_end_ba
    .catchall {:try_start_a7 .. :try_end_ba} :catchall_bb

    #@ba
    goto :goto_91

    #@bb
    .line 12882
    .end local v10           #layer:Landroid/view/HardwareLayer;
    .end local v11           #layerType:I
    :catchall_bb
    move-exception v1

    #@bc
    invoke-virtual {v0}, Landroid/view/HardwareCanvas;->onPostDraw()V

    #@bf
    .line 12884
    invoke-virtual {p1}, Landroid/view/DisplayList;->end()V

    #@c2
    .line 12885
    invoke-virtual {p1, v8}, Landroid/view/DisplayList;->setCaching(Z)V

    #@c5
    .line 12886
    if-eqz p2, :cond_115

    #@c7
    .line 12887
    const/4 v2, 0x0

    #@c8
    const/4 v3, 0x0

    #@c9
    invoke-virtual {p1, v2, v3, v13, v9}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    #@cc
    .line 12889
    :goto_cc
    throw v1

    #@cd
    .line 12857
    .restart local v11       #layerType:I
    :cond_cd
    const/4 v1, 0x1

    #@ce
    :try_start_ce
    invoke-virtual {p0, v1}, Landroid/view/View;->buildDrawingCache(Z)V

    #@d1
    .line 12858
    const/4 v1, 0x1

    #@d2
    invoke-virtual {p0, v1}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    #@d5
    move-result-object v7

    #@d6
    .line 12859
    .local v7, cache:Landroid/graphics/Bitmap;
    if-eqz v7, :cond_92

    #@d8
    .line 12860
    const/4 v1, 0x0

    #@d9
    const/4 v2, 0x0

    #@da
    iget-object v3, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@dc
    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/view/HardwareCanvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@df
    .line 12861
    const/4 v8, 0x1

    #@e0
    goto :goto_92

    #@e1
    .line 12866
    .end local v7           #cache:Landroid/graphics/Bitmap;
    :cond_e1
    invoke-virtual {p0}, Landroid/view/View;->computeScroll()V

    #@e4
    .line 12868
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@e6
    neg-int v1, v1

    #@e7
    int-to-float v1, v1

    #@e8
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@ea
    neg-int v2, v2

    #@eb
    int-to-float v2, v2

    #@ec
    invoke-virtual {v0, v1, v2}, Landroid/view/HardwareCanvas;->translate(FF)V

    #@ef
    .line 12869
    if-nez p2, :cond_101

    #@f1
    .line 12870
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@f3
    const v2, 0x8020

    #@f6
    or-int/2addr v1, v2

    #@f7
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@f9
    .line 12871
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@fb
    const v2, -0x600001

    #@fe
    and-int/2addr v1, v2

    #@ff
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@101
    .line 12875
    :cond_101
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@103
    and-int/lit16 v1, v1, 0x80

    #@105
    const/16 v2, 0x80

    #@107
    if-ne v1, v2, :cond_10d

    #@109
    .line 12876
    invoke-virtual {p0, v0}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@10c
    goto :goto_92

    #@10d
    .line 12878
    :cond_10d
    invoke-virtual {p0, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_110
    .catchall {:try_start_ce .. :try_end_110} :catchall_bb

    #@110
    goto :goto_92

    #@111
    .line 12889
    :cond_111
    invoke-virtual {p0, p1}, Landroid/view/View;->setDisplayListProperties(Landroid/view/DisplayList;)V

    #@114
    goto :goto_a2

    #@115
    .end local v11           #layerType:I
    :cond_115
    invoke-virtual {p0, p1}, Landroid/view/View;->setDisplayListProperties(Landroid/view/DisplayList;)V

    #@118
    goto :goto_cc

    #@119
    .line 12892
    .end local v0           #canvas:Landroid/view/HardwareCanvas;
    .end local v8           #caching:Z
    .end local v9           #height:I
    .end local v13           #width:I
    :cond_119
    if-nez p2, :cond_a2

    #@11b
    .line 12893
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@11d
    const v2, 0x8020

    #@120
    or-int/2addr v1, v2

    #@121
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@123
    .line 12894
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@125
    const v2, -0x600001

    #@128
    and-int/2addr v1, v2

    #@129
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@12b
    goto/16 :goto_a2
.end method

.method private getHardwareLayerDisplayList(Landroid/view/HardwareLayer;)Landroid/view/DisplayList;
    .registers 5
    .parameter "layer"

    #@0
    .prologue
    .line 12907
    invoke-virtual {p1}, Landroid/view/HardwareLayer;->getDisplayList()Landroid/view/DisplayList;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x1

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/View;->getDisplayList(Landroid/view/DisplayList;Z)Landroid/view/DisplayList;

    #@8
    move-result-object v0

    #@9
    .line 12908
    .local v0, displayList:Landroid/view/DisplayList;
    invoke-virtual {p1, v0}, Landroid/view/HardwareLayer;->setDisplayList(Landroid/view/DisplayList;)V

    #@c
    .line 12909
    return-object v0
.end method

.method private getScrollCache()Landroid/view/View$ScrollabilityCache;
    .registers 2

    #@0
    .prologue
    .line 4137
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@3
    .line 4138
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@5
    return-object v0
.end method

.method private hasAncestorThatBlocksDescendantFocus()Z
    .registers 5

    #@0
    .prologue
    .line 6823
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    .line 6824
    .local v0, ancestor:Landroid/view/ViewParent;
    :goto_2
    instance-of v2, v0, Landroid/view/ViewGroup;

    #@4
    if-eqz v2, :cond_18

    #@6
    move-object v1, v0

    #@7
    .line 6825
    check-cast v1, Landroid/view/ViewGroup;

    #@9
    .line 6826
    .local v1, vgAncestor:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getDescendantFocusability()I

    #@c
    move-result v2

    #@d
    const/high16 v3, 0x6

    #@f
    if-ne v2, v3, :cond_13

    #@11
    .line 6827
    const/4 v2, 0x1

    #@12
    .line 6832
    .end local v1           #vgAncestor:Landroid/view/ViewGroup;
    :goto_12
    return v2

    #@13
    .line 6829
    .restart local v1       #vgAncestor:Landroid/view/ViewGroup;
    :cond_13
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    #@16
    move-result-object v0

    #@17
    .line 6831
    goto :goto_2

    #@18
    .line 6832
    .end local v1           #vgAncestor:Landroid/view/ViewGroup;
    :cond_18
    const/4 v2, 0x0

    #@19
    goto :goto_12
.end method

.method private hasListenersForAccessibility()Z
    .registers 3

    #@0
    .prologue
    .line 6974
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    .line 6975
    .local v0, info:Landroid/view/View$ListenerInfo;
    iget-object v1, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@6
    if-nez v1, :cond_26

    #@8
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$200(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnKeyListener;

    #@b
    move-result-object v1

    #@c
    if-nez v1, :cond_26

    #@e
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$300(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnTouchListener;

    #@11
    move-result-object v1

    #@12
    if-nez v1, :cond_26

    #@14
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$400(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnGenericMotionListener;

    #@17
    move-result-object v1

    #@18
    if-nez v1, :cond_26

    #@1a
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$500(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnHoverListener;

    #@1d
    move-result-object v1

    #@1e
    if-nez v1, :cond_26

    #@20
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$600(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnDragListener;

    #@23
    move-result-object v1

    #@24
    if-eqz v1, :cond_28

    #@26
    :cond_26
    const/4 v1, 0x1

    #@27
    :goto_27
    return v1

    #@28
    :cond_28
    const/4 v1, 0x0

    #@29
    goto :goto_27
.end method

.method private hasRtlSupport()Z
    .registers 2

    #@0
    .prologue
    .line 11804
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/pm/ApplicationInfo;->hasRtlSupport()Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public static inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "context"
    .parameter "resource"
    .parameter "root"

    #@0
    .prologue
    .line 16722
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@3
    move-result-object v0

    #@4
    .line 16723
    .local v0, factory:Landroid/view/LayoutInflater;
    invoke-virtual {v0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method private initScrollCache()V
    .registers 3

    #@0
    .prologue
    .line 4131
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 4132
    new-instance v0, Landroid/view/View$ScrollabilityCache;

    #@6
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@8
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@b
    move-result-object v1

    #@c
    invoke-direct {v0, v1, p0}, Landroid/view/View$ScrollabilityCache;-><init>(Landroid/view/ViewConfiguration;Landroid/view/View;)V

    #@f
    iput-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@11
    .line 4134
    :cond_11
    return-void
.end method

.method private initialAwakenScrollBars()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 10284
    iget-object v1, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@3
    if-eqz v1, :cond_e

    #@5
    const/16 v1, 0x1b58

    #@7
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->awakenScrollBars(IZ)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isDrawablesResolved()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x4000

    #@2
    .line 14478
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private isHoverable()Z
    .registers 6

    #@0
    .prologue
    const/high16 v4, 0x20

    #@2
    const/4 v1, 0x0

    #@3
    .line 8248
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@5
    .line 8249
    .local v0, viewFlags:I
    and-int/lit8 v2, v0, 0x20

    #@7
    const/16 v3, 0x20

    #@9
    if-ne v2, v3, :cond_c

    #@b
    .line 8253
    :cond_b
    :goto_b
    return v1

    #@c
    :cond_c
    and-int/lit16 v2, v0, 0x4000

    #@e
    const/16 v3, 0x4000

    #@10
    if-eq v2, v3, :cond_16

    #@12
    and-int v2, v0, v4

    #@14
    if-ne v2, v4, :cond_b

    #@16
    :cond_16
    const/4 v1, 0x1

    #@17
    goto :goto_b
.end method

.method private isLayoutDirectionResolved()Z
    .registers 3

    #@0
    .prologue
    .line 11991
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    const/16 v1, 0x20

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isRtlCompatibilityMode()Z
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 11812
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@8
    move-result-object v2

    #@9
    iget v0, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@b
    .line 11813
    .local v0, targetSdkVersion:I
    sget-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@d
    if-eqz v2, :cond_10

    #@f
    .line 11815
    :cond_f
    :goto_f
    return v1

    #@10
    :cond_10
    const/16 v2, 0x11

    #@12
    if-lt v0, v2, :cond_1a

    #@14
    invoke-direct {p0}, Landroid/view/View;->hasRtlSupport()Z

    #@17
    move-result v2

    #@18
    if-nez v2, :cond_f

    #@1a
    :cond_1a
    const/4 v1, 0x1

    #@1b
    goto :goto_f
.end method

.method private isRtlLangAndLocaleMetaDataExist()Z
    .registers 11

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v6, 0x0

    #@2
    .line 11848
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@5
    move-result-object v7

    #@6
    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 11849
    .local v1, currentLanguage:Ljava/lang/String;
    if-eqz v1, :cond_95

    #@c
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@f
    move-result v7

    #@10
    if-lt v7, v8, :cond_95

    #@12
    .line 11850
    invoke-virtual {v1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 11851
    const-string v7, "ar"

    #@18
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v7

    #@1c
    if-nez v7, :cond_38

    #@1e
    const-string v7, "fa"

    #@20
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v7

    #@24
    if-nez v7, :cond_38

    #@26
    const-string/jumbo v7, "iw"

    #@29
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v7

    #@2d
    if-nez v7, :cond_38

    #@2f
    const-string/jumbo v7, "ku"

    #@32
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v7

    #@36
    if-eqz v7, :cond_95

    #@38
    .line 11852
    :cond_38
    sget-object v7, Landroid/view/View;->sR2LTracker:Ljava/util/HashMap;

    #@3a
    if-nez v7, :cond_43

    #@3c
    .line 11853
    new-instance v7, Ljava/util/HashMap;

    #@3e
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@41
    sput-object v7, Landroid/view/View;->sR2LTracker:Ljava/util/HashMap;

    #@43
    .line 11856
    :cond_43
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    .line 11858
    .local v4, packageName:Ljava/lang/String;
    sget-object v7, Landroid/view/View;->sR2LTracker:Ljava/util/HashMap;

    #@4d
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@50
    move-result v7

    #@51
    if-nez v7, :cond_81

    #@53
    .line 11859
    const/4 v0, 0x0

    #@54
    .line 11861
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_54
    const-string v3, ""

    #@56
    .line 11862
    .local v3, metaDataString:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@59
    move-result-object v7

    #@5a
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5d
    move-result-object v7

    #@5e
    const/16 v8, 0x80

    #@60
    invoke-virtual {v7, v4, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@63
    move-result-object v0

    #@64
    .line 11863
    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@66
    if-eqz v7, :cond_72

    #@68
    .line 11864
    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@6a
    const-string v8, "com.lge.layoutdirection"

    #@6c
    const-string v9, "Default"

    #@6e
    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    .line 11866
    :cond_72
    sget-object v7, Landroid/view/View;->sR2LTracker:Ljava/util/HashMap;

    #@74
    const-string v8, "Locale"

    #@76
    invoke-virtual {v3, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@79
    move-result v8

    #@7a
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@7d
    move-result-object v8

    #@7e
    invoke-virtual {v7, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_81
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_54 .. :try_end_81} :catch_96

    #@81
    .line 11872
    .end local v0           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v3           #metaDataString:Ljava/lang/String;
    :cond_81
    :goto_81
    sget-object v7, Landroid/view/View;->sR2LTracker:Ljava/util/HashMap;

    #@83
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@86
    move-result-object v5

    #@87
    check-cast v5, Ljava/lang/Boolean;

    #@89
    .line 11873
    .local v5, value:Ljava/lang/Boolean;
    if-eqz v5, :cond_95

    #@8b
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    #@8e
    move-result v7

    #@8f
    if-eqz v7, :cond_95

    #@91
    .line 11875
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    #@94
    move-result v6

    #@95
    .line 11879
    .end local v4           #packageName:Ljava/lang/String;
    .end local v5           #value:Ljava/lang/Boolean;
    :cond_95
    return v6

    #@96
    .line 11867
    .restart local v0       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v4       #packageName:Ljava/lang/String;
    :catch_96
    move-exception v2

    #@97
    .line 11868
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "View"

    #@99
    new-instance v8, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v9, "Exception caught package name not resolved:"

    #@a0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v8

    #@a4
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v8

    #@ac
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    goto :goto_81
.end method

.method private isRtlMetaDataExist()Z
    .registers 8

    #@0
    .prologue
    .line 11883
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@3
    move-result-object v4

    #@4
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 11884
    .local v3, packageName:Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    .line 11887
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_9
    const-string v2, ""

    #@b
    .line 11888
    .local v2, metaDataString:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@12
    move-result-object v4

    #@13
    const/16 v5, 0x80

    #@15
    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@18
    move-result-object v0

    #@19
    .line 11890
    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@1b
    if-eqz v4, :cond_27

    #@1d
    .line 11891
    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    #@1f
    const-string v5, "com.lge.layoutdirection"

    #@21
    const-string v6, "Default"

    #@23
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    .line 11894
    :cond_27
    const-string v4, "Locale"

    #@29
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_2c} :catch_31

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_4a

    #@2f
    .line 11895
    const/4 v4, 0x1

    #@30
    .line 11900
    .end local v2           #metaDataString:Ljava/lang/String;
    :goto_30
    return v4

    #@31
    .line 11896
    :catch_31
    move-exception v1

    #@32
    .line 11897
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "View"

    #@34
    new-instance v5, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v6, "Exception caught package name not resolved:"

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 11900
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4a
    const/4 v4, 0x0

    #@4b
    goto :goto_30
.end method

.method private isTextAlignmentResolved()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x1

    #@2
    .line 17284
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private isTextDirectionResolved()Z
    .registers 3

    #@0
    .prologue
    .line 17071
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit16 v0, v0, 0x200

    #@4
    const/16 v1, 0x200

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method protected static mergeDrawableStates([I[I)[I
    .registers 7
    .parameter "baseState"
    .parameter "additionalState"

    #@0
    .prologue
    .line 14652
    array-length v0, p0

    #@1
    .line 14653
    .local v0, N:I
    add-int/lit8 v1, v0, -0x1

    #@3
    .line 14654
    .local v1, i:I
    :goto_3
    if-ltz v1, :cond_c

    #@5
    aget v2, p0, v1

    #@7
    if-nez v2, :cond_c

    #@9
    .line 14655
    add-int/lit8 v1, v1, -0x1

    #@b
    goto :goto_3

    #@c
    .line 14657
    :cond_c
    const/4 v2, 0x0

    #@d
    add-int/lit8 v3, v1, 0x1

    #@f
    array-length v4, p1

    #@10
    invoke-static {p1, v2, p0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@13
    .line 14658
    return-object p0
.end method

.method private needRtlPropertiesResolution()Z
    .registers 3

    #@0
    .prologue
    const v1, 0x60010220

    #@3
    .line 11822
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@5
    and-int/2addr v0, v1

    #@6
    if-eq v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private nextAtGranularity(I)Z
    .registers 11
    .parameter "granularity"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 7113
    invoke-virtual {p0}, Landroid/view/View;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@5
    move-result-object v5

    #@6
    .line 7114
    .local v5, text:Ljava/lang/CharSequence;
    if-eqz v5, :cond_e

    #@8
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@b
    move-result v8

    #@c
    if-nez v8, :cond_f

    #@e
    .line 7132
    :cond_e
    :goto_e
    return v6

    #@f
    .line 7117
    :cond_f
    invoke-virtual {p0, p1}, Landroid/view/View;->getIteratorForGranularity(I)Landroid/view/AccessibilityIterators$TextSegmentIterator;

    #@12
    move-result-object v2

    #@13
    .line 7118
    .local v2, iterator:Landroid/view/AccessibilityIterators$TextSegmentIterator;
    if-eqz v2, :cond_e

    #@15
    .line 7121
    invoke-virtual {p0}, Landroid/view/View;->getAccessibilityCursorPosition()I

    #@18
    move-result v0

    #@19
    .line 7122
    .local v0, current:I
    invoke-interface {v2, v0}, Landroid/view/AccessibilityIterators$TextSegmentIterator;->following(I)[I

    #@1c
    move-result-object v3

    #@1d
    .line 7123
    .local v3, range:[I
    if-eqz v3, :cond_e

    #@1f
    .line 7126
    aget v4, v3, v6

    #@21
    .line 7127
    .local v4, start:I
    aget v1, v3, v7

    #@23
    .line 7128
    .local v1, end:I
    invoke-virtual {p0, v1}, Landroid/view/View;->setAccessibilityCursorPosition(I)V

    #@26
    .line 7129
    const/16 v6, 0x100

    #@28
    invoke-direct {p0, v6, p1, v4, v1}, Landroid/view/View;->sendViewTextTraversedAtGranularityEvent(IIII)V

    #@2b
    move v6, v7

    #@2c
    .line 7132
    goto :goto_e
.end method

.method private static nonzero(F)Z
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 8953
    const v0, -0x457ced91

    #@3
    cmpg-float v0, p0, v0

    #@5
    if-ltz v0, :cond_e

    #@7
    const v0, 0x3a83126f

    #@a
    cmpl-float v0, p0, v0

    #@c
    if-lez v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method private pointInView(FFF)Z
    .registers 6
    .parameter "localX"
    .parameter "localY"
    .parameter "slop"

    #@0
    .prologue
    .line 9993
    neg-float v0, p3

    #@1
    cmpl-float v0, p1, v0

    #@3
    if-ltz v0, :cond_22

    #@5
    neg-float v0, p3

    #@6
    cmpl-float v0, p2, v0

    #@8
    if-ltz v0, :cond_22

    #@a
    iget v0, p0, Landroid/view/View;->mRight:I

    #@c
    iget v1, p0, Landroid/view/View;->mLeft:I

    #@e
    sub-int/2addr v0, v1

    #@f
    int-to-float v0, v0

    #@10
    add-float/2addr v0, p3

    #@11
    cmpg-float v0, p1, v0

    #@13
    if-gez v0, :cond_22

    #@15
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@17
    iget v1, p0, Landroid/view/View;->mTop:I

    #@19
    sub-int/2addr v0, v1

    #@1a
    int-to-float v0, v0

    #@1b
    add-float/2addr v0, p3

    #@1c
    cmpg-float v0, p2, v0

    #@1e
    if-gez v0, :cond_22

    #@20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method private postSendViewScrolledAccessibilityEventCallback()V
    .registers 4

    #@0
    .prologue
    .line 10985
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 10986
    new-instance v0, Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, p0, v1}, Landroid/view/View$SendViewScrolledAccessibilityEvent;-><init>(Landroid/view/View;Landroid/view/View$1;)V

    #@a
    iput-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@c
    .line 10988
    :cond_c
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@e
    iget-boolean v0, v0, Landroid/view/View$SendViewScrolledAccessibilityEvent;->mIsPending:Z

    #@10
    if-nez v0, :cond_20

    #@12
    .line 10989
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@14
    const/4 v1, 0x1

    #@15
    iput-boolean v1, v0, Landroid/view/View$SendViewScrolledAccessibilityEvent;->mIsPending:Z

    #@17
    .line 10990
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@19
    invoke-static {}, Landroid/view/ViewConfiguration;->getSendRecurringAccessibilityEventsInterval()J

    #@1c
    move-result-wide v1

    #@1d
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    #@20
    .line 10993
    :cond_20
    return-void
.end method

.method private previousAtGranularity(I)Z
    .registers 11
    .parameter "granularity"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 7136
    invoke-virtual {p0}, Landroid/view/View;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@5
    move-result-object v5

    #@6
    .line 7137
    .local v5, text:Ljava/lang/CharSequence;
    if-eqz v5, :cond_e

    #@8
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@b
    move-result v8

    #@c
    if-nez v8, :cond_f

    #@e
    .line 7169
    :cond_e
    :goto_e
    return v6

    #@f
    .line 7140
    :cond_f
    invoke-virtual {p0, p1}, Landroid/view/View;->getIteratorForGranularity(I)Landroid/view/AccessibilityIterators$TextSegmentIterator;

    #@12
    move-result-object v2

    #@13
    .line 7141
    .local v2, iterator:Landroid/view/AccessibilityIterators$TextSegmentIterator;
    if-eqz v2, :cond_e

    #@15
    .line 7144
    invoke-virtual {p0}, Landroid/view/View;->getAccessibilityCursorPosition()I

    #@18
    move-result v0

    #@19
    .line 7145
    .local v0, current:I
    const/4 v8, -0x1

    #@1a
    if-ne v0, v8, :cond_39

    #@1c
    .line 7146
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@1f
    move-result v0

    #@20
    .line 7147
    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityCursorPosition(I)V

    #@23
    .line 7154
    :cond_23
    :goto_23
    invoke-interface {v2, v0}, Landroid/view/AccessibilityIterators$TextSegmentIterator;->preceding(I)[I

    #@26
    move-result-object v3

    #@27
    .line 7155
    .local v3, range:[I
    if-eqz v3, :cond_e

    #@29
    .line 7158
    aget v4, v3, v6

    #@2b
    .line 7159
    .local v4, start:I
    aget v1, v3, v7

    #@2d
    .line 7161
    .local v1, end:I
    if-ne p1, v7, :cond_41

    #@2f
    .line 7162
    invoke-virtual {p0, v1}, Landroid/view/View;->setAccessibilityCursorPosition(I)V

    #@32
    .line 7166
    :goto_32
    const/16 v6, 0x200

    #@34
    invoke-direct {p0, v6, p1, v4, v1}, Landroid/view/View;->sendViewTextTraversedAtGranularityEvent(IIII)V

    #@37
    move v6, v7

    #@38
    .line 7169
    goto :goto_e

    #@39
    .line 7148
    .end local v1           #end:I
    .end local v3           #range:[I
    .end local v4           #start:I
    :cond_39
    if-ne p1, v7, :cond_23

    #@3b
    .line 7151
    add-int/lit8 v0, v0, -0x1

    #@3d
    .line 7152
    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityCursorPosition(I)V

    #@40
    goto :goto_23

    #@41
    .line 7164
    .restart local v1       #end:I
    .restart local v3       #range:[I
    .restart local v4       #start:I
    :cond_41
    invoke-virtual {p0, v4}, Landroid/view/View;->setAccessibilityCursorPosition(I)V

    #@44
    goto :goto_32
.end method

.method private static printFlags(I)Ljava/lang/String;
    .registers 5
    .parameter "flags"

    #@0
    .prologue
    .line 14112
    const-string v1, ""

    #@2
    .line 14113
    .local v1, output:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .line 14114
    .local v0, numFlags:I
    and-int/lit8 v2, p0, 0x1

    #@5
    const/4 v3, 0x1

    #@6
    if-ne v2, v3, :cond_1d

    #@8
    .line 14115
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "TAKES_FOCUS"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 14116
    add-int/lit8 v0, v0, 0x1

    #@1d
    .line 14119
    :cond_1d
    and-int/lit8 v2, p0, 0xc

    #@1f
    sparse-switch v2, :sswitch_data_76

    #@22
    .line 14137
    :goto_22
    return-object v1

    #@23
    .line 14121
    :sswitch_23
    if-lez v0, :cond_38

    #@25
    .line 14122
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, " "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    .line 14124
    :cond_38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    const-string v3, "INVISIBLE"

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    .line 14126
    goto :goto_22

    #@4c
    .line 14128
    :sswitch_4c
    if-lez v0, :cond_61

    #@4e
    .line 14129
    new-instance v2, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    const-string v3, " "

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    .line 14131
    :cond_61
    new-instance v2, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    const-string v3, "GONE"

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    .line 14133
    goto :goto_22

    #@75
    .line 14119
    nop

    #@76
    :sswitch_data_76
    .sparse-switch
        0x4 -> :sswitch_23
        0x8 -> :sswitch_4c
    .end sparse-switch
.end method

.method private static printPrivateFlags(I)Ljava/lang/String;
    .registers 5
    .parameter "privateFlags"

    #@0
    .prologue
    .line 14148
    const-string v1, ""

    #@2
    .line 14149
    .local v1, output:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .line 14151
    .local v0, numFlags:I
    and-int/lit8 v2, p0, 0x1

    #@5
    const/4 v3, 0x1

    #@6
    if-ne v2, v3, :cond_1d

    #@8
    .line 14152
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "WANTS_FOCUS"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 14153
    add-int/lit8 v0, v0, 0x1

    #@1d
    .line 14156
    :cond_1d
    and-int/lit8 v2, p0, 0x2

    #@1f
    const/4 v3, 0x2

    #@20
    if-ne v2, v3, :cond_4c

    #@22
    .line 14157
    if-lez v0, :cond_37

    #@24
    .line 14158
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, " "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    .line 14160
    :cond_37
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    const-string v3, "FOCUSED"

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    .line 14161
    add-int/lit8 v0, v0, 0x1

    #@4c
    .line 14164
    :cond_4c
    and-int/lit8 v2, p0, 0x4

    #@4e
    const/4 v3, 0x4

    #@4f
    if-ne v2, v3, :cond_7b

    #@51
    .line 14165
    if-lez v0, :cond_66

    #@53
    .line 14166
    new-instance v2, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    const-string v3, " "

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    .line 14168
    :cond_66
    new-instance v2, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    const-string v3, "SELECTED"

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    .line 14169
    add-int/lit8 v0, v0, 0x1

    #@7b
    .line 14172
    :cond_7b
    and-int/lit8 v2, p0, 0x8

    #@7d
    const/16 v3, 0x8

    #@7f
    if-ne v2, v3, :cond_ab

    #@81
    .line 14173
    if-lez v0, :cond_96

    #@83
    .line 14174
    new-instance v2, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    const-string v3, " "

    #@8e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v2

    #@92
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v1

    #@96
    .line 14176
    :cond_96
    new-instance v2, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v2

    #@9f
    const-string v3, "IS_ROOT_NAMESPACE"

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v1

    #@a9
    .line 14177
    add-int/lit8 v0, v0, 0x1

    #@ab
    .line 14180
    :cond_ab
    and-int/lit8 v2, p0, 0x10

    #@ad
    const/16 v3, 0x10

    #@af
    if-ne v2, v3, :cond_db

    #@b1
    .line 14181
    if-lez v0, :cond_c6

    #@b3
    .line 14182
    new-instance v2, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    const-string v3, " "

    #@be
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v2

    #@c2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v1

    #@c6
    .line 14184
    :cond_c6
    new-instance v2, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v2

    #@cf
    const-string v3, "HAS_BOUNDS"

    #@d1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v2

    #@d5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v1

    #@d9
    .line 14185
    add-int/lit8 v0, v0, 0x1

    #@db
    .line 14188
    :cond_db
    and-int/lit8 v2, p0, 0x20

    #@dd
    const/16 v3, 0x20

    #@df
    if-ne v2, v3, :cond_109

    #@e1
    .line 14189
    if-lez v0, :cond_f6

    #@e3
    .line 14190
    new-instance v2, Ljava/lang/StringBuilder;

    #@e5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e8
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v2

    #@ec
    const-string v3, " "

    #@ee
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v2

    #@f2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v1

    #@f6
    .line 14192
    :cond_f6
    new-instance v2, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v2

    #@ff
    const-string v3, "DRAWN"

    #@101
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v2

    #@105
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v1

    #@109
    .line 14195
    :cond_109
    return-object v1
.end method

.method private removeLongPressCallback()V
    .registers 2

    #@0
    .prologue
    .line 8467
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 8468
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForLongPress:Landroid/view/View$CheckForLongPress;

    #@6
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 8470
    :cond_9
    return-void
.end method

.method private removePerformClickCallback()V
    .registers 2

    #@0
    .prologue
    .line 8476
    iget-object v0, p0, Landroid/view/View;->mPerformClick:Landroid/view/View$PerformClick;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 8477
    iget-object v0, p0, Landroid/view/View;->mPerformClick:Landroid/view/View$PerformClick;

    #@6
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 8479
    :cond_9
    return-void
.end method

.method private removeSendViewScrolledAccessibilityEventCallback()V
    .registers 3

    #@0
    .prologue
    .line 8523
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 8524
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@6
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@9
    .line 8525
    iget-object v0, p0, Landroid/view/View;->mSendViewScrolledAccessibilityEvent:Landroid/view/View$SendViewScrolledAccessibilityEvent;

    #@b
    const/4 v1, 0x0

    #@c
    iput-boolean v1, v0, Landroid/view/View$SendViewScrolledAccessibilityEvent;->mIsPending:Z

    #@e
    .line 8527
    :cond_e
    return-void
.end method

.method private removeTapCallback()V
    .registers 3

    #@0
    .prologue
    .line 8495
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 8496
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    const v1, -0x2000001

    #@9
    and-int/2addr v0, v1

    #@a
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@c
    .line 8497
    iget-object v0, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@e
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@11
    .line 8499
    :cond_11
    return-void
.end method

.method private removeUnsetPressCallback()V
    .registers 2

    #@0
    .prologue
    .line 8485
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit16 v0, v0, 0x4000

    #@4
    if-eqz v0, :cond_13

    #@6
    iget-object v0, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 8486
    const/4 v0, 0x0

    #@b
    invoke-virtual {p0, v0}, Landroid/view/View;->setPressed(Z)V

    #@e
    .line 8487
    iget-object v0, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@10
    invoke-virtual {p0, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@13
    .line 8489
    :cond_13
    return-void
.end method

.method private requestFocusNoSearch(ILandroid/graphics/Rect;)Z
    .registers 7
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    const/high16 v3, 0x4

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v0, 0x0

    #@4
    .line 6778
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@6
    and-int/lit8 v2, v2, 0x1

    #@8
    if-ne v2, v1, :cond_10

    #@a
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@c
    and-int/lit8 v2, v2, 0xc

    #@e
    if-eqz v2, :cond_11

    #@10
    .line 6795
    :cond_10
    :goto_10
    return v0

    #@11
    .line 6784
    :cond_11
    invoke-virtual {p0}, Landroid/view/View;->isInTouchMode()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_1c

    #@17
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@19
    and-int/2addr v2, v3

    #@1a
    if-ne v3, v2, :cond_10

    #@1c
    .line 6790
    :cond_1c
    invoke-direct {p0}, Landroid/view/View;->hasAncestorThatBlocksDescendantFocus()Z

    #@1f
    move-result v2

    #@20
    if-nez v2, :cond_10

    #@22
    .line 6794
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->handleFocusGainInternal(ILandroid/graphics/Rect;)V

    #@25
    move v0, v1

    #@26
    .line 6795
    goto :goto_10
.end method

.method private resetPressedState()V
    .registers 3

    #@0
    .prologue
    .line 5419
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    const/16 v1, 0x20

    #@6
    if-ne v0, v1, :cond_9

    #@8
    .line 5430
    :cond_8
    :goto_8
    return-void

    #@9
    .line 5423
    :cond_9
    invoke-virtual {p0}, Landroid/view/View;->isPressed()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_8

    #@f
    .line 5424
    const/4 v0, 0x0

    #@10
    invoke-virtual {p0, v0}, Landroid/view/View;->setPressed(Z)V

    #@13
    .line 5426
    iget-boolean v0, p0, Landroid/view/View;->mHasPerformedLongPress:Z

    #@15
    if-nez v0, :cond_8

    #@17
    .line 5427
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@1a
    goto :goto_8
.end method

.method public static resolveSize(II)I
    .registers 4
    .parameter "size"
    .parameter "measureSpec"

    #@0
    .prologue
    .line 15872
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/view/View;->resolveSizeAndState(III)I

    #@4
    move-result v0

    #@5
    const v1, 0xffffff

    #@8
    and-int/2addr v0, v1

    #@9
    return v0
.end method

.method public static resolveSizeAndState(III)I
    .registers 7
    .parameter "size"
    .parameter "measureSpec"
    .parameter "childMeasuredState"

    #@0
    .prologue
    .line 15889
    move v0, p0

    #@1
    .line 15890
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@4
    move-result v1

    #@5
    .line 15891
    .local v1, specMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v2

    #@9
    .line 15892
    .local v2, specSize:I
    sparse-switch v1, :sswitch_data_1e

    #@c
    .line 15907
    :goto_c
    const/high16 v3, -0x100

    #@e
    and-int/2addr v3, p2

    #@f
    or-int/2addr v3, v0

    #@10
    return v3

    #@11
    .line 15894
    :sswitch_11
    move v0, p0

    #@12
    .line 15895
    goto :goto_c

    #@13
    .line 15897
    :sswitch_13
    if-ge v2, p0, :cond_1a

    #@15
    .line 15898
    const/high16 v3, 0x100

    #@17
    or-int v0, v2, v3

    #@19
    goto :goto_c

    #@1a
    .line 15900
    :cond_1a
    move v0, p0

    #@1b
    .line 15902
    goto :goto_c

    #@1c
    .line 15904
    :sswitch_1c
    move v0, v2

    #@1d
    goto :goto_c

    #@1e
    .line 15892
    :sswitch_data_1e
    .sparse-switch
        -0x80000000 -> :sswitch_13
        0x0 -> :sswitch_11
        0x40000000 -> :sswitch_1c
    .end sparse-switch
.end method

.method private sendAccessibilityHoverEvent(I)V
    .registers 5
    .parameter "eventType"

    #@0
    .prologue
    .line 6674
    move-object v1, p0

    #@1
    .line 6676
    .local v1, source:Landroid/view/View;
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->includeForAccessibility()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_b

    #@7
    .line 6677
    invoke-virtual {v1, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@a
    .line 6684
    :cond_a
    return-void

    #@b
    .line 6680
    :cond_b
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@e
    move-result-object v0

    #@f
    .line 6681
    .local v0, parent:Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/View;

    #@11
    if-eqz v2, :cond_a

    #@13
    move-object v1, v0

    #@14
    .line 6682
    check-cast v1, Landroid/view/View;

    #@16
    goto :goto_1
.end method

.method private sendViewTextTraversedAtGranularityEvent(IIII)V
    .registers 7
    .parameter "action"
    .parameter "granularity"
    .parameter "fromIndex"
    .parameter "toIndex"

    #@0
    .prologue
    .line 7199
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 7211
    :goto_4
    return-void

    #@5
    .line 7202
    :cond_5
    const/high16 v1, 0x2

    #@7
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@a
    move-result-object v0

    #@b
    .line 7204
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0, v0}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@e
    .line 7205
    invoke-virtual {p0, v0}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@11
    .line 7206
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    #@14
    .line 7207
    invoke-virtual {v0, p4}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    #@17
    .line 7208
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityEvent;->setAction(I)V

    #@1a
    .line 7209
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityEvent;->setMovementGranularity(I)V

    #@1d
    .line 7210
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1f
    invoke-interface {v1, p0, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@22
    goto :goto_4
.end method

.method private setKeyedTag(ILjava/lang/Object;)V
    .registers 4
    .parameter "key"
    .parameter "tag"

    #@0
    .prologue
    .line 15597
    iget-object v0, p0, Landroid/view/View;->mKeyedTags:Landroid/util/SparseArray;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 15598
    new-instance v0, Landroid/util/SparseArray;

    #@6
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/View;->mKeyedTags:Landroid/util/SparseArray;

    #@b
    .line 15601
    :cond_b
    iget-object v0, p0, Landroid/view/View;->mKeyedTags:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@10
    .line 15602
    return-void
.end method

.method private skipInvalidate()Z
    .registers 2

    #@0
    .prologue
    .line 10413
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0xc

    #@4
    if-eqz v0, :cond_1c

    #@6
    iget-object v0, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@8
    if-nez v0, :cond_1c

    #@a
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@e
    if-eqz v0, :cond_1a

    #@10
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@12
    check-cast v0, Landroid/view/ViewGroup;

    #@14
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->isViewTransitioning(Landroid/view/View;)Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_1c

    #@1a
    :cond_1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method private updateMatrix()V
    .registers 7

    #@0
    .prologue
    const/high16 v3, 0x4000

    #@2
    .line 8980
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@4
    .line 8981
    .local v0, info:Landroid/view/View$TransformationInfo;
    if-nez v0, :cond_7

    #@6
    .line 9021
    :cond_6
    :goto_6
    return-void

    #@7
    .line 8984
    :cond_7
    iget-boolean v1, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@9
    if-eqz v1, :cond_6

    #@b
    .line 8989
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    const/high16 v2, 0x2000

    #@f
    and-int/2addr v1, v2

    #@10
    if-nez v1, :cond_48

    #@12
    .line 8990
    iget v1, p0, Landroid/view/View;->mRight:I

    #@14
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@16
    sub-int/2addr v1, v2

    #@17
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1600(Landroid/view/View$TransformationInfo;)I

    #@1a
    move-result v2

    #@1b
    if-ne v1, v2, :cond_28

    #@1d
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@1f
    iget v2, p0, Landroid/view/View;->mTop:I

    #@21
    sub-int/2addr v1, v2

    #@22
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1700(Landroid/view/View$TransformationInfo;)I

    #@25
    move-result v2

    #@26
    if-eq v1, v2, :cond_48

    #@28
    .line 8991
    :cond_28
    iget v1, p0, Landroid/view/View;->mRight:I

    #@2a
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@2c
    sub-int/2addr v1, v2

    #@2d
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$1602(Landroid/view/View$TransformationInfo;I)I

    #@30
    .line 8992
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@32
    iget v2, p0, Landroid/view/View;->mTop:I

    #@34
    sub-int/2addr v1, v2

    #@35
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$1702(Landroid/view/View$TransformationInfo;I)I

    #@38
    .line 8993
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1600(Landroid/view/View$TransformationInfo;)I

    #@3b
    move-result v1

    #@3c
    int-to-float v1, v1

    #@3d
    div-float/2addr v1, v3

    #@3e
    iput v1, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@40
    .line 8994
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1700(Landroid/view/View$TransformationInfo;)I

    #@43
    move-result v1

    #@44
    int-to-float v1, v1

    #@45
    div-float/2addr v1, v3

    #@46
    iput v1, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@48
    .line 8997
    :cond_48
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    #@4f
    .line 8998
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@51
    invoke-static {v1}, Landroid/view/View;->nonzero(F)Z

    #@54
    move-result v1

    #@55
    if-nez v1, :cond_9a

    #@57
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@59
    invoke-static {v1}, Landroid/view/View;->nonzero(F)Z

    #@5c
    move-result v1

    #@5d
    if-nez v1, :cond_9a

    #@5f
    .line 8999
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@62
    move-result-object v1

    #@63
    iget v2, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@65
    iget v3, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@67
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@6a
    .line 9000
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@6d
    move-result-object v1

    #@6e
    iget v2, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@70
    iget v3, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@72
    iget v4, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@74
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    #@77
    .line 9001
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@7a
    move-result-object v1

    #@7b
    iget v2, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@7d
    iget v3, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@7f
    iget v4, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@81
    iget v5, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@83
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    #@86
    .line 9017
    :goto_86
    const/4 v1, 0x0

    #@87
    iput-boolean v1, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@89
    .line 9018
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@8c
    move-result-object v1

    #@8d
    invoke-virtual {v1}, Landroid/graphics/Matrix;->isIdentity()Z

    #@90
    move-result v1

    #@91
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$1502(Landroid/view/View$TransformationInfo;Z)Z

    #@94
    .line 9019
    const/4 v1, 0x1

    #@95
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$2002(Landroid/view/View$TransformationInfo;Z)Z

    #@98
    goto/16 :goto_6

    #@9a
    .line 9003
    :cond_9a
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@9d
    move-result-object v1

    #@9e
    if-nez v1, :cond_b0

    #@a0
    .line 9004
    new-instance v1, Landroid/graphics/Camera;

    #@a2
    invoke-direct {v1}, Landroid/graphics/Camera;-><init>()V

    #@a5
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$1802(Landroid/view/View$TransformationInfo;Landroid/graphics/Camera;)Landroid/graphics/Camera;

    #@a8
    .line 9005
    new-instance v1, Landroid/graphics/Matrix;

    #@aa
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@ad
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$1902(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    #@b0
    .line 9007
    :cond_b0
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@b3
    move-result-object v1

    #@b4
    invoke-virtual {v1}, Landroid/graphics/Camera;->save()V

    #@b7
    .line 9008
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@ba
    move-result-object v1

    #@bb
    iget v2, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@bd
    iget v3, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@bf
    iget v4, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@c1
    iget v5, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@c3
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    #@c6
    .line 9009
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@c9
    move-result-object v1

    #@ca
    iget v2, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@cc
    iget v3, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@ce
    iget v4, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@d0
    neg-float v4, v4

    #@d1
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Camera;->rotate(FFF)V

    #@d4
    .line 9010
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@d7
    move-result-object v1

    #@d8
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1900(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v1, v2}, Landroid/graphics/Camera;->getMatrix(Landroid/graphics/Matrix;)V

    #@df
    .line 9011
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1900(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@e2
    move-result-object v1

    #@e3
    iget v2, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@e5
    neg-float v2, v2

    #@e6
    iget v3, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@e8
    neg-float v3, v3

    #@e9
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@ec
    .line 9012
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1900(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@ef
    move-result-object v1

    #@f0
    iget v2, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@f2
    iget v3, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@f4
    add-float/2addr v2, v3

    #@f5
    iget v3, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@f7
    iget v4, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@f9
    add-float/2addr v3, v4

    #@fa
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@fd
    .line 9014
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@100
    move-result-object v1

    #@101
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1900(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@104
    move-result-object v2

    #@105
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@108
    .line 9015
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@10b
    move-result-object v1

    #@10c
    invoke-virtual {v1}, Landroid/graphics/Camera;->restore()V

    #@10f
    goto/16 :goto_86
.end method


# virtual methods
.method public addChildrenForAccessibility(Ljava/util/ArrayList;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 6932
    .local p1, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/view/View;->includeForAccessibility()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 6933
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9
    .line 6935
    :cond_9
    return-void
.end method

.method public addFocusables(Ljava/util/ArrayList;I)V
    .registers 4
    .parameter
    .parameter "direction"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 6502
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@4
    .line 6503
    return-void
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 6
    .parameter
    .parameter "direction"
    .parameter "focusableMode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    #@0
    .prologue
    .line 6522
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-nez p1, :cond_3

    #@2
    .line 6533
    :cond_2
    :goto_2
    return-void

    #@3
    .line 6525
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_2

    #@9
    .line 6528
    and-int/lit8 v0, p3, 0x1

    #@b
    const/4 v1, 0x1

    #@c
    if-ne v0, v1, :cond_1a

    #@e
    invoke-virtual {p0}, Landroid/view/View;->isInTouchMode()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1a

    #@14
    invoke-virtual {p0}, Landroid/view/View;->isFocusableInTouchMode()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_2

    #@1a
    .line 6532
    :cond_1a
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    goto :goto_2
.end method

.method public addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 4221
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    .line 4222
    .local v0, li:Landroid/view/View$ListenerInfo;
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$100(Landroid/view/View$ListenerInfo;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 4223
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@c
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@f
    invoke-static {v0, v1}, Landroid/view/View$ListenerInfo;->access$102(Landroid/view/View$ListenerInfo;Ljava/util/concurrent/CopyOnWriteArrayList;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@12
    .line 4226
    :cond_12
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$100(Landroid/view/View$ListenerInfo;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    #@19
    .line 4227
    return-void
.end method

.method public addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 4188
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    .line 4189
    .local v0, li:Landroid/view/View$ListenerInfo;
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_12

    #@a
    .line 4190
    new-instance v1, Ljava/util/ArrayList;

    #@c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@f
    invoke-static {v0, v1}, Landroid/view/View$ListenerInfo;->access$002(Landroid/view/View$ListenerInfo;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    #@12
    .line 4192
    :cond_12
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_23

    #@1c
    .line 4193
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 4195
    :cond_23
    return-void
.end method

.method public addTouchables(Ljava/util/ArrayList;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, views:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const/high16 v3, 0x20

    #@2
    .line 6585
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    .line 6587
    .local v0, viewFlags:I
    and-int/lit16 v1, v0, 0x4000

    #@6
    const/16 v2, 0x4000

    #@8
    if-eq v1, v2, :cond_e

    #@a
    and-int v1, v0, v3

    #@c
    if-ne v1, v3, :cond_15

    #@e
    :cond_e
    and-int/lit8 v1, v0, 0x20

    #@10
    if-nez v1, :cond_15

    #@12
    .line 6589
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 6591
    :cond_15
    return-void
.end method

.method public animate()Landroid/view/ViewPropertyAnimator;
    .registers 2

    #@0
    .prologue
    .line 17629
    iget-object v0, p0, Landroid/view/View;->mAnimator:Landroid/view/ViewPropertyAnimator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 17630
    new-instance v0, Landroid/view/ViewPropertyAnimator;

    #@6
    invoke-direct {v0, p0}, Landroid/view/ViewPropertyAnimator;-><init>(Landroid/view/View;)V

    #@9
    iput-object v0, p0, Landroid/view/View;->mAnimator:Landroid/view/ViewPropertyAnimator;

    #@b
    .line 17632
    :cond_b
    iget-object v0, p0, Landroid/view/View;->mAnimator:Landroid/view/ViewPropertyAnimator;

    #@d
    return-object v0
.end method

.method public announceForAccessibility(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 4767
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_29

    #@c
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@e
    if-eqz v1, :cond_29

    #@10
    .line 4768
    const/16 v1, 0x4000

    #@12
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@15
    move-result-object v0

    #@16
    .line 4770
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0, v0}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@19
    .line 4771
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@1c
    move-result-object v1

    #@1d
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@20
    .line 4772
    const/4 v1, 0x0

    #@21
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    #@24
    .line 4773
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@26
    invoke-interface {v1, p0, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@29
    .line 4775
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    :cond_29
    return-void
.end method

.method public applyDrawableToTransparentRegion(Landroid/graphics/drawable/Drawable;Landroid/graphics/Region;)V
    .registers 18
    .parameter "dr"
    .parameter "region"

    #@0
    .prologue
    .line 16667
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/drawable/Drawable;->getTransparentRegion()Landroid/graphics/Region;

    #@3
    move-result-object v1

    #@4
    .line 16668
    .local v1, r:Landroid/graphics/Region;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@7
    move-result-object v13

    #@8
    .line 16669
    .local v13, db:Landroid/graphics/Rect;
    iget-object v12, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    .line 16670
    .local v12, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v1, :cond_6d

    #@c
    if-eqz v12, :cond_6d

    #@e
    .line 16671
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    #@11
    move-result v2

    #@12
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    #@15
    move-result v3

    #@16
    sub-int v9, v2, v3

    #@18
    .line 16672
    .local v9, w:I
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    #@1b
    move-result v2

    #@1c
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    #@1f
    move-result v3

    #@20
    sub-int v5, v2, v3

    #@22
    .line 16673
    .local v5, h:I
    iget v2, v13, Landroid/graphics/Rect;->left:I

    #@24
    if-lez v2, :cond_2f

    #@26
    .line 16675
    const/4 v2, 0x0

    #@27
    const/4 v3, 0x0

    #@28
    iget v4, v13, Landroid/graphics/Rect;->left:I

    #@2a
    sget-object v6, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@2c
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@2f
    .line 16677
    :cond_2f
    iget v2, v13, Landroid/graphics/Rect;->right:I

    #@31
    if-ge v2, v9, :cond_3c

    #@33
    .line 16679
    iget v2, v13, Landroid/graphics/Rect;->right:I

    #@35
    const/4 v3, 0x0

    #@36
    sget-object v6, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@38
    move v4, v9

    #@39
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@3c
    .line 16681
    :cond_3c
    iget v2, v13, Landroid/graphics/Rect;->top:I

    #@3e
    if-lez v2, :cond_4a

    #@40
    .line 16683
    const/4 v7, 0x0

    #@41
    const/4 v8, 0x0

    #@42
    iget v10, v13, Landroid/graphics/Rect;->top:I

    #@44
    sget-object v11, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@46
    move-object v6, v1

    #@47
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@4a
    .line 16685
    :cond_4a
    iget v2, v13, Landroid/graphics/Rect;->bottom:I

    #@4c
    if-ge v2, v5, :cond_57

    #@4e
    .line 16687
    const/4 v2, 0x0

    #@4f
    iget v3, v13, Landroid/graphics/Rect;->bottom:I

    #@51
    sget-object v6, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@53
    move v4, v9

    #@54
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@57
    .line 16689
    :cond_57
    iget-object v14, v12, Landroid/view/View$AttachInfo;->mTransparentLocation:[I

    #@59
    .line 16690
    .local v14, location:[I
    invoke-virtual {p0, v14}, Landroid/view/View;->getLocationInWindow([I)V

    #@5c
    .line 16691
    const/4 v2, 0x0

    #@5d
    aget v2, v14, v2

    #@5f
    const/4 v3, 0x1

    #@60
    aget v3, v14, v3

    #@62
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Region;->translate(II)V

    #@65
    .line 16692
    sget-object v2, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@67
    move-object/from16 v0, p2

    #@69
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    #@6c
    .line 16696
    .end local v5           #h:I
    .end local v9           #w:I
    .end local v14           #location:[I
    :goto_6c
    return-void

    #@6d
    .line 16694
    :cond_6d
    sget-object v2, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    #@6f
    move-object/from16 v0, p2

    #@71
    invoke-virtual {v0, v13, v2}, Landroid/graphics/Region;->op(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    #@74
    goto :goto_6c
.end method

.method assignParent(Landroid/view/ViewParent;)V
    .registers 5
    .parameter "parent"

    #@0
    .prologue
    .line 11697
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 11698
    iput-object p1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    .line 11705
    :goto_6
    return-void

    #@7
    .line 11699
    :cond_7
    if-nez p1, :cond_d

    #@9
    .line 11700
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    goto :goto_6

    #@d
    .line 11702
    :cond_d
    new-instance v0, Ljava/lang/RuntimeException;

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string/jumbo v2, "view "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " being added, but"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " it already has a parent"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v0
.end method

.method protected awakenScrollBars()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 10268
    iget-object v1, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@3
    if-eqz v1, :cond_e

    #@5
    const/16 v1, 0xbb8

    #@7
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->awakenScrollBars(IZ)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected awakenScrollBars(I)Z
    .registers 3
    .parameter "startDelay"

    #@0
    .prologue
    .line 10323
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->awakenScrollBars(IZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method protected awakenScrollBars(IZ)Z
    .registers 13
    .parameter "startDelay"
    .parameter "invalidate"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 10365
    iget-object v3, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@4
    .line 10367
    .local v3, scrollCache:Landroid/view/View$ScrollabilityCache;
    if-eqz v3, :cond_a

    #@6
    iget-boolean v6, v3, Landroid/view/View$ScrollabilityCache;->fadeScrollBars:Z

    #@8
    if-nez v6, :cond_b

    #@a
    .line 10405
    :cond_a
    :goto_a
    return v4

    #@b
    .line 10371
    :cond_b
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@d
    if-nez v6, :cond_16

    #@f
    .line 10372
    new-instance v6, Landroid/widget/ScrollBarDrawable;

    #@11
    invoke-direct {v6}, Landroid/widget/ScrollBarDrawable;-><init>()V

    #@14
    iput-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@16
    .line 10375
    :cond_16
    invoke-virtual {p0}, Landroid/view/View;->isHorizontalScrollBarEnabled()Z

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_22

    #@1c
    invoke-virtual {p0}, Landroid/view/View;->isVerticalScrollBarEnabled()Z

    #@1f
    move-result v6

    #@20
    if-eqz v6, :cond_a

    #@22
    .line 10377
    :cond_22
    if-eqz p2, :cond_27

    #@24
    .line 10379
    invoke-virtual {p0}, Landroid/view/View;->postInvalidateOnAnimation()V

    #@27
    .line 10382
    :cond_27
    iget v4, v3, Landroid/view/View$ScrollabilityCache;->state:I

    #@29
    if-nez v4, :cond_33

    #@2b
    .line 10386
    const/16 v0, 0x2ee

    #@2d
    .line 10387
    .local v0, KEY_REPEAT_FIRST_DELAY:I
    const/16 v4, 0x2ee

    #@2f
    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    #@32
    move-result p1

    #@33
    .line 10392
    .end local v0           #KEY_REPEAT_FIRST_DELAY:I
    :cond_33
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@36
    move-result-wide v6

    #@37
    int-to-long v8, p1

    #@38
    add-long v1, v6, v8

    #@3a
    .line 10393
    .local v1, fadeStartTime:J
    iput-wide v1, v3, Landroid/view/View$ScrollabilityCache;->fadeStartTime:J

    #@3c
    .line 10394
    iput v5, v3, Landroid/view/View$ScrollabilityCache;->state:I

    #@3e
    .line 10397
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    if-eqz v4, :cond_50

    #@42
    .line 10398
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@44
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@46
    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@49
    .line 10399
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4b
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@4d
    invoke-virtual {v4, v3, v1, v2}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    #@50
    :cond_50
    move v4, v5

    #@51
    .line 10402
    goto :goto_a
.end method

.method public bringToFront()V
    .registers 2

    #@0
    .prologue
    .line 8703
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 8704
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->bringChildToFront(Landroid/view/View;)V

    #@9
    .line 8706
    :cond_9
    return-void
.end method

.method public buildDrawingCache()V
    .registers 2

    #@0
    .prologue
    .line 13035
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/view/View;->buildDrawingCache(Z)V

    #@4
    .line 13036
    return-void
.end method

.method public buildDrawingCache(Z)V
    .registers 25
    .parameter "autoScale"

    #@0
    .prologue
    .line 13062
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@4
    move/from16 v20, v0

    #@6
    const v21, 0x8000

    #@9
    and-int v20, v20, v21

    #@b
    if-eqz v20, :cond_17

    #@d
    if-eqz p1, :cond_ea

    #@f
    move-object/from16 v0, p0

    #@11
    iget-object v0, v0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@13
    move-object/from16 v20, v0

    #@15
    if-nez v20, :cond_e9

    #@17
    .line 13064
    :cond_17
    :goto_17
    const/16 v20, 0x0

    #@19
    move/from16 v0, v20

    #@1b
    move-object/from16 v1, p0

    #@1d
    iput-boolean v0, v1, Landroid/view/View;->mCachingFailed:Z

    #@1f
    .line 13066
    move-object/from16 v0, p0

    #@21
    iget v0, v0, Landroid/view/View;->mRight:I

    #@23
    move/from16 v20, v0

    #@25
    move-object/from16 v0, p0

    #@27
    iget v0, v0, Landroid/view/View;->mLeft:I

    #@29
    move/from16 v21, v0

    #@2b
    sub-int v19, v20, v21

    #@2d
    .line 13067
    .local v19, width:I
    move-object/from16 v0, p0

    #@2f
    iget v0, v0, Landroid/view/View;->mBottom:I

    #@31
    move/from16 v20, v0

    #@33
    move-object/from16 v0, p0

    #@35
    iget v0, v0, Landroid/view/View;->mTop:I

    #@37
    move/from16 v21, v0

    #@39
    sub-int v10, v20, v21

    #@3b
    .line 13069
    .local v10, height:I
    move-object/from16 v0, p0

    #@3d
    iget-object v2, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3f
    .line 13070
    .local v2, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v2, :cond_f4

    #@41
    iget-boolean v0, v2, Landroid/view/View$AttachInfo;->mScalingRequired:Z

    #@43
    move/from16 v20, v0

    #@45
    if-eqz v20, :cond_f4

    #@47
    const/16 v17, 0x1

    #@49
    .line 13072
    .local v17, scalingRequired:Z
    :goto_49
    if-eqz p1, :cond_71

    #@4b
    if-eqz v17, :cond_71

    #@4d
    .line 13073
    move/from16 v0, v19

    #@4f
    int-to-float v0, v0

    #@50
    move/from16 v20, v0

    #@52
    iget v0, v2, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@54
    move/from16 v21, v0

    #@56
    mul-float v20, v20, v21

    #@58
    const/high16 v21, 0x3f00

    #@5a
    add-float v20, v20, v21

    #@5c
    move/from16 v0, v20

    #@5e
    float-to-int v0, v0

    #@5f
    move/from16 v19, v0

    #@61
    .line 13074
    int-to-float v0, v10

    #@62
    move/from16 v20, v0

    #@64
    iget v0, v2, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@66
    move/from16 v21, v0

    #@68
    mul-float v20, v20, v21

    #@6a
    const/high16 v21, 0x3f00

    #@6c
    add-float v20, v20, v21

    #@6e
    move/from16 v0, v20

    #@70
    float-to-int v10, v0

    #@71
    .line 13077
    :cond_71
    move-object/from16 v0, p0

    #@73
    iget v6, v0, Landroid/view/View;->mDrawingCacheBackgroundColor:I

    #@75
    .line 13078
    .local v6, drawingCacheBackgroundColor:I
    if-nez v6, :cond_7d

    #@77
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->isOpaque()Z

    #@7a
    move-result v20

    #@7b
    if-eqz v20, :cond_f8

    #@7d
    :cond_7d
    const/4 v11, 0x1

    #@7e
    .line 13079
    .local v11, opaque:Z
    :goto_7e
    if-eqz v2, :cond_fa

    #@80
    iget-boolean v0, v2, Landroid/view/View$AttachInfo;->mUse32BitDrawingCache:Z

    #@82
    move/from16 v20, v0

    #@84
    if-eqz v20, :cond_fa

    #@86
    const/16 v18, 0x1

    #@88
    .line 13081
    .local v18, use32BitCache:Z
    :goto_88
    mul-int v21, v19, v10

    #@8a
    if-eqz v11, :cond_fd

    #@8c
    if-nez v18, :cond_fd

    #@8e
    const/16 v20, 0x2

    #@90
    :goto_90
    mul-int v20, v20, v21

    #@92
    move/from16 v0, v20

    #@94
    int-to-long v12, v0

    #@95
    .line 13082
    .local v12, projectedBitmapSize:J
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@99
    move-object/from16 v20, v0

    #@9b
    invoke-static/range {v20 .. v20}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@9e
    move-result-object v20

    #@9f
    invoke-virtual/range {v20 .. v20}, Landroid/view/ViewConfiguration;->getScaledMaximumDrawingCacheSize()I

    #@a2
    move-result v20

    #@a3
    move/from16 v0, v20

    #@a5
    int-to-long v7, v0

    #@a6
    .line 13084
    .local v7, drawingCacheSize:J
    if-lez v19, :cond_ae

    #@a8
    if-lez v10, :cond_ae

    #@aa
    cmp-long v20, v12, v7

    #@ac
    if-lez v20, :cond_100

    #@ae
    .line 13085
    :cond_ae
    if-lez v19, :cond_de

    #@b0
    if-lez v10, :cond_de

    #@b2
    .line 13086
    const-string v20, "View"

    #@b4
    new-instance v21, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v22, "View too large to fit into drawing cache, needs "

    #@bb
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v21

    #@bf
    move-object/from16 v0, v21

    #@c1
    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v21

    #@c5
    const-string v22, " bytes, only "

    #@c7
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v21

    #@cb
    move-object/from16 v0, v21

    #@cd
    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v21

    #@d1
    const-string v22, " available"

    #@d3
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v21

    #@d7
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v21

    #@db
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 13090
    :cond_de
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->destroyDrawingCache()V

    #@e1
    .line 13091
    const/16 v20, 0x1

    #@e3
    move/from16 v0, v20

    #@e5
    move-object/from16 v1, p0

    #@e7
    iput-boolean v0, v1, Landroid/view/View;->mCachingFailed:Z

    #@e9
    .line 13205
    .end local v2           #attachInfo:Landroid/view/View$AttachInfo;
    .end local v6           #drawingCacheBackgroundColor:I
    .end local v7           #drawingCacheSize:J
    .end local v10           #height:I
    .end local v11           #opaque:Z
    .end local v12           #projectedBitmapSize:J
    .end local v17           #scalingRequired:Z
    .end local v18           #use32BitCache:Z
    .end local v19           #width:I
    :cond_e9
    :goto_e9
    return-void

    #@ea
    .line 13062
    :cond_ea
    move-object/from16 v0, p0

    #@ec
    iget-object v0, v0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@ee
    move-object/from16 v20, v0

    #@f0
    if-nez v20, :cond_e9

    #@f2
    goto/16 :goto_17

    #@f4
    .line 13070
    .restart local v2       #attachInfo:Landroid/view/View$AttachInfo;
    .restart local v10       #height:I
    .restart local v19       #width:I
    :cond_f4
    const/16 v17, 0x0

    #@f6
    goto/16 :goto_49

    #@f8
    .line 13078
    .restart local v6       #drawingCacheBackgroundColor:I
    .restart local v17       #scalingRequired:Z
    :cond_f8
    const/4 v11, 0x0

    #@f9
    goto :goto_7e

    #@fa
    .line 13079
    .restart local v11       #opaque:Z
    :cond_fa
    const/16 v18, 0x0

    #@fc
    goto :goto_88

    #@fd
    .line 13081
    .restart local v18       #use32BitCache:Z
    :cond_fd
    const/16 v20, 0x4

    #@ff
    goto :goto_90

    #@100
    .line 13095
    .restart local v7       #drawingCacheSize:J
    .restart local v12       #projectedBitmapSize:J
    :cond_100
    const/4 v5, 0x1

    #@101
    .line 13096
    .local v5, clear:Z
    if-eqz p1, :cond_237

    #@103
    move-object/from16 v0, p0

    #@105
    iget-object v3, v0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@107
    .line 13098
    .local v3, bitmap:Landroid/graphics/Bitmap;
    :goto_107
    if-eqz v3, :cond_11b

    #@109
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    #@10c
    move-result v20

    #@10d
    move/from16 v0, v20

    #@10f
    move/from16 v1, v19

    #@111
    if-ne v0, v1, :cond_11b

    #@113
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    #@116
    move-result v20

    #@117
    move/from16 v0, v20

    #@119
    if-eq v0, v10, :cond_16a

    #@11b
    .line 13100
    :cond_11b
    if-nez v11, :cond_249

    #@11d
    .line 13103
    move-object/from16 v0, p0

    #@11f
    iget v0, v0, Landroid/view/View;->mViewFlags:I

    #@121
    move/from16 v20, v0

    #@123
    const/high16 v21, 0x18

    #@125
    and-int v20, v20, v21

    #@127
    sparse-switch v20, :sswitch_data_286

    #@12a
    .line 13114
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@12c
    .line 13124
    .local v14, quality:Landroid/graphics/Bitmap$Config;
    :goto_12c
    if-eqz v3, :cond_131

    #@12e
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    #@131
    .line 13127
    :cond_131
    :try_start_131
    move-object/from16 v0, p0

    #@133
    iget-object v0, v0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@135
    move-object/from16 v20, v0

    #@137
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@13a
    move-result-object v20

    #@13b
    move-object/from16 v0, v20

    #@13d
    move/from16 v1, v19

    #@13f
    invoke-static {v0, v1, v10, v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@142
    move-result-object v3

    #@143
    .line 13129
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@146
    move-result-object v20

    #@147
    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@14a
    move-result-object v20

    #@14b
    move-object/from16 v0, v20

    #@14d
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@14f
    move/from16 v20, v0

    #@151
    move/from16 v0, v20

    #@153
    invoke-virtual {v3, v0}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@156
    .line 13130
    if-eqz p1, :cond_252

    #@158
    .line 13131
    move-object/from16 v0, p0

    #@15a
    iput-object v3, v0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@15c
    .line 13135
    :goto_15c
    if-eqz v11, :cond_167

    #@15e
    if-eqz v18, :cond_167

    #@160
    const/16 v20, 0x0

    #@162
    move/from16 v0, v20

    #@164
    invoke-virtual {v3, v0}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V
    :try_end_167
    .catch Ljava/lang/OutOfMemoryError; {:try_start_131 .. :try_end_167} :catch_258

    #@167
    .line 13149
    :cond_167
    if-eqz v6, :cond_276

    #@169
    const/4 v5, 0x1

    #@16a
    .line 13153
    .end local v14           #quality:Landroid/graphics/Bitmap$Config;
    :cond_16a
    :goto_16a
    if-eqz v2, :cond_279

    #@16c
    .line 13154
    iget-object v4, v2, Landroid/view/View$AttachInfo;->mCanvas:Landroid/graphics/Canvas;

    #@16e
    .line 13155
    .local v4, canvas:Landroid/graphics/Canvas;
    if-nez v4, :cond_175

    #@170
    .line 13156
    new-instance v4, Landroid/graphics/Canvas;

    #@172
    .end local v4           #canvas:Landroid/graphics/Canvas;
    invoke-direct {v4}, Landroid/graphics/Canvas;-><init>()V

    #@175
    .line 13158
    .restart local v4       #canvas:Landroid/graphics/Canvas;
    :cond_175
    invoke-virtual {v4, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@178
    .line 13163
    const/16 v20, 0x0

    #@17a
    move-object/from16 v0, v20

    #@17c
    iput-object v0, v2, Landroid/view/View$AttachInfo;->mCanvas:Landroid/graphics/Canvas;

    #@17e
    .line 13169
    :goto_17e
    if-eqz v5, :cond_183

    #@180
    .line 13170
    invoke-virtual {v3, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    #@183
    .line 13173
    :cond_183
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeScroll()V

    #@186
    .line 13174
    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    #@189
    move-result v15

    #@18a
    .line 13176
    .local v15, restoreCount:I
    if-eqz p1, :cond_199

    #@18c
    if-eqz v17, :cond_199

    #@18e
    .line 13177
    iget v0, v2, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@190
    move/from16 v16, v0

    #@192
    .line 13178
    .local v16, scale:F
    move/from16 v0, v16

    #@194
    move/from16 v1, v16

    #@196
    invoke-virtual {v4, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    #@199
    .line 13181
    .end local v16           #scale:F
    :cond_199
    move-object/from16 v0, p0

    #@19b
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@19d
    move/from16 v20, v0

    #@19f
    move/from16 v0, v20

    #@1a1
    neg-int v0, v0

    #@1a2
    move/from16 v20, v0

    #@1a4
    move/from16 v0, v20

    #@1a6
    int-to-float v0, v0

    #@1a7
    move/from16 v20, v0

    #@1a9
    move-object/from16 v0, p0

    #@1ab
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@1ad
    move/from16 v21, v0

    #@1af
    move/from16 v0, v21

    #@1b1
    neg-int v0, v0

    #@1b2
    move/from16 v21, v0

    #@1b4
    move/from16 v0, v21

    #@1b6
    int-to-float v0, v0

    #@1b7
    move/from16 v21, v0

    #@1b9
    move/from16 v0, v20

    #@1bb
    move/from16 v1, v21

    #@1bd
    invoke-virtual {v4, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@1c0
    .line 13183
    move-object/from16 v0, p0

    #@1c2
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@1c4
    move/from16 v20, v0

    #@1c6
    or-int/lit8 v20, v20, 0x20

    #@1c8
    move/from16 v0, v20

    #@1ca
    move-object/from16 v1, p0

    #@1cc
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@1ce
    .line 13184
    move-object/from16 v0, p0

    #@1d0
    iget-object v0, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1d2
    move-object/from16 v20, v0

    #@1d4
    if-eqz v20, :cond_1ec

    #@1d6
    move-object/from16 v0, p0

    #@1d8
    iget-object v0, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1da
    move-object/from16 v20, v0

    #@1dc
    move-object/from16 v0, v20

    #@1de
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mHardwareAccelerated:Z

    #@1e0
    move/from16 v20, v0

    #@1e2
    if-eqz v20, :cond_1ec

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iget v0, v0, Landroid/view/View;->mLayerType:I

    #@1e8
    move/from16 v20, v0

    #@1ea
    if-eqz v20, :cond_1fd

    #@1ec
    .line 13186
    :cond_1ec
    move-object/from16 v0, p0

    #@1ee
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@1f0
    move/from16 v20, v0

    #@1f2
    const v21, 0x8000

    #@1f5
    or-int v20, v20, v21

    #@1f7
    move/from16 v0, v20

    #@1f9
    move-object/from16 v1, p0

    #@1fb
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@1fd
    .line 13190
    :cond_1fd
    move-object/from16 v0, p0

    #@1ff
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@201
    move/from16 v20, v0

    #@203
    move/from16 v0, v20

    #@205
    and-int/lit16 v0, v0, 0x80

    #@207
    move/from16 v20, v0

    #@209
    const/16 v21, 0x80

    #@20b
    move/from16 v0, v20

    #@20d
    move/from16 v1, v21

    #@20f
    if-ne v0, v1, :cond_280

    #@211
    .line 13191
    move-object/from16 v0, p0

    #@213
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@215
    move/from16 v20, v0

    #@217
    const v21, -0x600001

    #@21a
    and-int v20, v20, v21

    #@21c
    move/from16 v0, v20

    #@21e
    move-object/from16 v1, p0

    #@220
    iput v0, v1, Landroid/view/View;->mPrivateFlags:I

    #@222
    .line 13192
    move-object/from16 v0, p0

    #@224
    invoke-virtual {v0, v4}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@227
    .line 13197
    :goto_227
    invoke-virtual {v4, v15}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@22a
    .line 13198
    const/16 v20, 0x0

    #@22c
    move-object/from16 v0, v20

    #@22e
    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@231
    .line 13200
    if-eqz v2, :cond_e9

    #@233
    .line 13202
    iput-object v4, v2, Landroid/view/View$AttachInfo;->mCanvas:Landroid/graphics/Canvas;

    #@235
    goto/16 :goto_e9

    #@237
    .line 13096
    .end local v3           #bitmap:Landroid/graphics/Bitmap;
    .end local v4           #canvas:Landroid/graphics/Canvas;
    .end local v15           #restoreCount:I
    :cond_237
    move-object/from16 v0, p0

    #@239
    iget-object v3, v0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@23b
    goto/16 :goto_107

    #@23d
    .line 13105
    .restart local v3       #bitmap:Landroid/graphics/Bitmap;
    :sswitch_23d
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@23f
    .line 13106
    .restart local v14       #quality:Landroid/graphics/Bitmap$Config;
    goto/16 :goto_12c

    #@241
    .line 13108
    .end local v14           #quality:Landroid/graphics/Bitmap$Config;
    :sswitch_241
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@243
    .line 13109
    .restart local v14       #quality:Landroid/graphics/Bitmap$Config;
    goto/16 :goto_12c

    #@245
    .line 13111
    .end local v14           #quality:Landroid/graphics/Bitmap$Config;
    :sswitch_245
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@247
    .line 13112
    .restart local v14       #quality:Landroid/graphics/Bitmap$Config;
    goto/16 :goto_12c

    #@249
    .line 13120
    .end local v14           #quality:Landroid/graphics/Bitmap$Config;
    :cond_249
    if-eqz v18, :cond_24f

    #@24b
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@24d
    .restart local v14       #quality:Landroid/graphics/Bitmap$Config;
    :goto_24d
    goto/16 :goto_12c

    #@24f
    .end local v14           #quality:Landroid/graphics/Bitmap$Config;
    :cond_24f
    sget-object v14, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@251
    goto :goto_24d

    #@252
    .line 13133
    .restart local v14       #quality:Landroid/graphics/Bitmap$Config;
    :cond_252
    :try_start_252
    move-object/from16 v0, p0

    #@254
    iput-object v3, v0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;
    :try_end_256
    .catch Ljava/lang/OutOfMemoryError; {:try_start_252 .. :try_end_256} :catch_258

    #@256
    goto/16 :goto_15c

    #@258
    .line 13136
    :catch_258
    move-exception v9

    #@259
    .line 13140
    .local v9, e:Ljava/lang/OutOfMemoryError;
    if-eqz p1, :cond_26d

    #@25b
    .line 13141
    const/16 v20, 0x0

    #@25d
    move-object/from16 v0, v20

    #@25f
    move-object/from16 v1, p0

    #@261
    iput-object v0, v1, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@263
    .line 13145
    :goto_263
    const/16 v20, 0x1

    #@265
    move/from16 v0, v20

    #@267
    move-object/from16 v1, p0

    #@269
    iput-boolean v0, v1, Landroid/view/View;->mCachingFailed:Z

    #@26b
    goto/16 :goto_e9

    #@26d
    .line 13143
    :cond_26d
    const/16 v20, 0x0

    #@26f
    move-object/from16 v0, v20

    #@271
    move-object/from16 v1, p0

    #@273
    iput-object v0, v1, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@275
    goto :goto_263

    #@276
    .line 13149
    .end local v9           #e:Ljava/lang/OutOfMemoryError;
    :cond_276
    const/4 v5, 0x0

    #@277
    goto/16 :goto_16a

    #@279
    .line 13166
    .end local v14           #quality:Landroid/graphics/Bitmap$Config;
    :cond_279
    new-instance v4, Landroid/graphics/Canvas;

    #@27b
    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@27e
    .restart local v4       #canvas:Landroid/graphics/Canvas;
    goto/16 :goto_17e

    #@280
    .line 13194
    .restart local v15       #restoreCount:I
    :cond_280
    move-object/from16 v0, p0

    #@282
    invoke-virtual {v0, v4}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    #@285
    goto :goto_227

    #@286
    .line 13103
    :sswitch_data_286
    .sparse-switch
        0x0 -> :sswitch_23d
        0x80000 -> :sswitch_241
        0x100000 -> :sswitch_245
    .end sparse-switch
.end method

.method public buildLayer()V
    .registers 3

    #@0
    .prologue
    .line 12561
    iget v0, p0, Landroid/view/View;->mLayerType:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 12579
    :cond_4
    :goto_4
    return-void

    #@5
    .line 12563
    :cond_5
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    if-nez v0, :cond_11

    #@9
    .line 12564
    new-instance v0, Ljava/lang/IllegalStateException;

    #@b
    const-string v1, "This view must be attached to a window first"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 12567
    :cond_11
    iget v0, p0, Landroid/view/View;->mLayerType:I

    #@13
    packed-switch v0, :pswitch_data_3a

    #@16
    goto :goto_4

    #@17
    .line 12576
    :pswitch_17
    const/4 v0, 0x1

    #@18
    invoke-virtual {p0, v0}, Landroid/view/View;->buildDrawingCache(Z)V

    #@1b
    goto :goto_4

    #@1c
    .line 12569
    :pswitch_1c
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1e
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@20
    if-eqz v0, :cond_4

    #@22
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@24
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@26
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_4

    #@2c
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2e
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@30
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->validate()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_4

    #@36
    .line 12572
    invoke-virtual {p0}, Landroid/view/View;->getHardwareLayer()Landroid/view/HardwareLayer;

    #@39
    goto :goto_4

    #@3a
    .line 12567
    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_17
        :pswitch_1c
    .end packed-switch
.end method

.method public callOnClick()Z
    .registers 3

    #@0
    .prologue
    .line 4345
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 4346
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_f

    #@4
    iget-object v1, v0, Landroid/view/View$ListenerInfo;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 4347
    iget-object v1, v0, Landroid/view/View$ListenerInfo;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@a
    invoke-interface {v1, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    #@d
    .line 4348
    const/4 v1, 0x1

    #@e
    .line 4350
    :goto_e
    return v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method canAcceptDrag()Z
    .registers 2

    #@0
    .prologue
    .line 16637
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public canHaveDisplayList()Z
    .registers 2

    #@0
    .prologue
    .line 12776
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public canResolveLayoutDirection()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 11958
    invoke-virtual {p0}, Landroid/view/View;->getRawLayoutDirection()I

    #@4
    move-result v0

    #@5
    packed-switch v0, :pswitch_data_22

    #@8
    move v0, v1

    #@9
    .line 11963
    :goto_9
    return v0

    #@a
    .line 11960
    :pswitch_a
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@c
    if-eqz v0, :cond_20

    #@e
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@10
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@12
    if-eqz v0, :cond_20

    #@14
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@16
    check-cast v0, Landroid/view/ViewGroup;

    #@18
    invoke-virtual {v0}, Landroid/view/ViewGroup;->canResolveLayoutDirection()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    move v0, v1

    #@1f
    goto :goto_9

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_9

    #@22
    .line 11958
    :pswitch_data_22
    .packed-switch 0x2
        :pswitch_a
    .end packed-switch
.end method

.method public canScrollHorizontally(I)Z
    .registers 8
    .parameter "direction"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 11483
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollOffset()I

    #@5
    move-result v0

    #@6
    .line 11484
    .local v0, offset:I
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollRange()I

    #@9
    move-result v4

    #@a
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollExtent()I

    #@d
    move-result v5

    #@e
    sub-int v1, v4, v5

    #@10
    .line 11485
    .local v1, range:I
    if-nez v1, :cond_14

    #@12
    move v2, v3

    #@13
    .line 11489
    :cond_13
    :goto_13
    return v2

    #@14
    .line 11486
    :cond_14
    if-gez p1, :cond_1a

    #@16
    .line 11487
    if-gtz v0, :cond_13

    #@18
    move v2, v3

    #@19
    goto :goto_13

    #@1a
    .line 11489
    :cond_1a
    add-int/lit8 v4, v1, -0x1

    #@1c
    if-lt v0, v4, :cond_13

    #@1e
    move v2, v3

    #@1f
    goto :goto_13
.end method

.method public canScrollVertically(I)Z
    .registers 8
    .parameter "direction"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 11500
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollOffset()I

    #@5
    move-result v0

    #@6
    .line 11501
    .local v0, offset:I
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollRange()I

    #@9
    move-result v4

    #@a
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollExtent()I

    #@d
    move-result v5

    #@e
    sub-int v1, v4, v5

    #@10
    .line 11502
    .local v1, range:I
    if-nez v1, :cond_14

    #@12
    move v2, v3

    #@13
    .line 11506
    :cond_13
    :goto_13
    return v2

    #@14
    .line 11503
    :cond_14
    if-gez p1, :cond_1a

    #@16
    .line 11504
    if-gtz v0, :cond_13

    #@18
    move v2, v3

    #@19
    goto :goto_13

    #@1a
    .line 11506
    :cond_1a
    add-int/lit8 v4, v1, -0x1

    #@1c
    if-lt v0, v4, :cond_13

    #@1e
    move v2, v3

    #@1f
    goto :goto_13
.end method

.method public cancelLongPress()V
    .registers 1

    #@0
    .prologue
    .line 8508
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@3
    .line 8515
    invoke-direct {p0}, Landroid/view/View;->removeTapCallback()V

    #@6
    .line 8516
    return-void
.end method

.method public checkInputConnectionProxy(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 8039
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public clearAccessibilityFocus()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 6647
    iget v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3
    const/high16 v3, 0x400

    #@5
    and-int/2addr v2, v3

    #@6
    if-eqz v2, :cond_1b

    #@8
    .line 6648
    iget v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@a
    const v3, -0x4000001

    #@d
    and-int/2addr v2, v3

    #@e
    iput v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@10
    .line 6649
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    #@13
    .line 6650
    const/high16 v2, 0x1

    #@15
    invoke-virtual {p0, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@18
    .line 6651
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@1b
    .line 6655
    :cond_1b
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@1e
    move-result-object v1

    #@1f
    .line 6656
    .local v1, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v1, :cond_30

    #@21
    .line 6657
    invoke-virtual {v1}, Landroid/view/ViewRootImpl;->getAccessibilityFocusedHost()Landroid/view/View;

    #@24
    move-result-object v0

    #@25
    .line 6658
    .local v0, focusHost:Landroid/view/View;
    if-eqz v0, :cond_30

    #@27
    invoke-static {v0, p0}, Landroid/view/ViewRootImpl;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_30

    #@2d
    .line 6659
    invoke-virtual {v1, v4, v4}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@30
    .line 6662
    .end local v0           #focusHost:Landroid/view/View;
    :cond_30
    return-void
.end method

.method clearAccessibilityFocusNoCallbacks()V
    .registers 3

    #@0
    .prologue
    .line 6696
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const/high16 v1, 0x400

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 6697
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@9
    const v1, -0x4000001

    #@c
    and-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@f
    .line 6698
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    #@12
    .line 6700
    :cond_12
    return-void
.end method

.method public clearAnimation()V
    .registers 2

    #@0
    .prologue
    .line 16052
    iget-object v0, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 16053
    iget-object v0, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@6
    invoke-virtual {v0}, Landroid/view/animation/Animation;->detach()V

    #@9
    .line 16055
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@c
    .line 16056
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@f
    .line 16057
    return-void
.end method

.method public clearFocus()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4597
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@3
    and-int/lit8 v0, v0, 0x2

    #@5
    if-eqz v0, :cond_2f

    #@7
    .line 4598
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@9
    and-int/lit8 v0, v0, -0x3

    #@b
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    .line 4600
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 4601
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@13
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->clearChildFocus(Landroid/view/View;)V

    #@16
    .line 4604
    :cond_16
    const/4 v0, 0x0

    #@17
    invoke-virtual {p0, v1, v1, v0}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@1a
    .line 4606
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@1d
    .line 4608
    invoke-virtual {p0}, Landroid/view/View;->ensureInputFocusOnFirstFocusable()V

    #@20
    .line 4610
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@22
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_2f

    #@2c
    .line 4611
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@2f
    .line 4614
    :cond_2f
    return-void
.end method

.method protected computeHorizontalScrollExtent()I
    .registers 2

    #@0
    .prologue
    .line 11412
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected computeHorizontalScrollOffset()I
    .registers 2

    #@0
    .prologue
    .line 11391
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .registers 2

    #@0
    .prologue
    .line 11370
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected computeOpaqueFlags()V
    .registers 4

    #@0
    .prologue
    .line 10668
    iget-object v1, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v1, :cond_2b

    #@4
    iget-object v1, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@9
    move-result v1

    #@a
    const/4 v2, -0x1

    #@b
    if-ne v1, v2, :cond_2b

    #@d
    .line 10669
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@f
    const/high16 v2, 0x80

    #@11
    or-int/2addr v1, v2

    #@12
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@14
    .line 10674
    :goto_14
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@16
    .line 10675
    .local v0, flags:I
    and-int/lit16 v1, v0, 0x200

    #@18
    if-nez v1, :cond_1e

    #@1a
    and-int/lit16 v1, v0, 0x100

    #@1c
    if-eqz v1, :cond_23

    #@1e
    :cond_1e
    const/high16 v1, 0x300

    #@20
    and-int/2addr v1, v0

    #@21
    if-nez v1, :cond_34

    #@23
    .line 10677
    :cond_23
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@25
    const/high16 v2, 0x100

    #@27
    or-int/2addr v1, v2

    #@28
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2a
    .line 10681
    :goto_2a
    return-void

    #@2b
    .line 10671
    .end local v0           #flags:I
    :cond_2b
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2d
    const v2, -0x800001

    #@30
    and-int/2addr v1, v2

    #@31
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@33
    goto :goto_14

    #@34
    .line 10679
    .restart local v0       #flags:I
    :cond_34
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@36
    const v2, -0x1000001

    #@39
    and-int/2addr v1, v2

    #@3a
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@3c
    goto :goto_2a
.end method

.method public computeScroll()V
    .registers 1

    #@0
    .prologue
    .line 11002
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .registers 2

    #@0
    .prologue
    .line 11473
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected computeVerticalScrollOffset()I
    .registers 2

    #@0
    .prologue
    .line 11452
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method protected computeVerticalScrollRange()I
    .registers 2

    #@0
    .prologue
    .line 11431
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 4

    #@0
    .prologue
    .line 4993
    invoke-virtual {p0}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@3
    move-result-object v1

    #@4
    .line 4994
    .local v1, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v1, :cond_c

    #@6
    .line 4995
    const/4 v2, -0x1

    #@7
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@a
    move-result-object v0

    #@b
    .line 4999
    :goto_b
    return-object v0

    #@c
    .line 4997
    :cond_c
    invoke-static {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@f
    move-result-object v0

    #@10
    .line 4998
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {p0, v0}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@13
    goto :goto_b
.end method

.method public createContextMenu(Landroid/view/ContextMenu;)V
    .registers 6
    .parameter "menu"

    #@0
    .prologue
    .line 8053
    invoke-virtual {p0}, Landroid/view/View;->getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    #@3
    move-result-object v1

    #@4
    .local v1, menuInfo:Landroid/view/ContextMenu$ContextMenuInfo;
    move-object v2, p1

    #@5
    .line 8057
    check-cast v2, Lcom/android/internal/view/menu/MenuBuilder;

    #@7
    invoke-virtual {v2, v1}, Lcom/android/internal/view/menu/MenuBuilder;->setCurrentMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V

    #@a
    .line 8059
    invoke-virtual {p0, p1}, Landroid/view/View;->onCreateContextMenu(Landroid/view/ContextMenu;)V

    #@d
    .line 8060
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@f
    .line 8061
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_1a

    #@11
    iget-object v2, v0, Landroid/view/View$ListenerInfo;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    #@13
    if-eqz v2, :cond_1a

    #@15
    .line 8062
    iget-object v2, v0, Landroid/view/View$ListenerInfo;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    #@17
    invoke-interface {v2, p1, p0, v1}, Landroid/view/View$OnCreateContextMenuListener;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    #@1a
    :cond_1a
    move-object v2, p1

    #@1b
    .line 8067
    check-cast v2, Lcom/android/internal/view/menu/MenuBuilder;

    #@1d
    const/4 v3, 0x0

    #@1e
    invoke-virtual {v2, v3}, Lcom/android/internal/view/menu/MenuBuilder;->setCurrentMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V

    #@21
    .line 8069
    iget-object v2, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@23
    if-eqz v2, :cond_2a

    #@25
    .line 8070
    iget-object v2, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@27
    invoke-interface {v2, p1}, Landroid/view/ViewParent;->createContextMenu(Landroid/view/ContextMenu;)V

    #@2a
    .line 8072
    :cond_2a
    return-void
.end method

.method createSnapshot(Landroid/graphics/Bitmap$Config;IZ)Landroid/graphics/Bitmap;
    .registers 15
    .parameter "quality"
    .parameter "backgroundColor"
    .parameter "skipChildren"

    #@0
    .prologue
    .line 13212
    iget v9, p0, Landroid/view/View;->mRight:I

    #@2
    iget v10, p0, Landroid/view/View;->mLeft:I

    #@4
    sub-int v8, v9, v10

    #@6
    .line 13213
    .local v8, width:I
    iget v9, p0, Landroid/view/View;->mBottom:I

    #@8
    iget v10, p0, Landroid/view/View;->mTop:I

    #@a
    sub-int v4, v9, v10

    #@c
    .line 13215
    .local v4, height:I
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@e
    .line 13216
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_34

    #@10
    iget v7, v0, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@12
    .line 13217
    .local v7, scale:F
    :goto_12
    int-to-float v9, v8

    #@13
    mul-float/2addr v9, v7

    #@14
    const/high16 v10, 0x3f00

    #@16
    add-float/2addr v9, v10

    #@17
    float-to-int v8, v9

    #@18
    .line 13218
    int-to-float v9, v4

    #@19
    mul-float/2addr v9, v7

    #@1a
    const/high16 v10, 0x3f00

    #@1c
    add-float/2addr v9, v10

    #@1d
    float-to-int v4, v9

    #@1e
    .line 13220
    iget-object v9, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@20
    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@23
    move-result-object v9

    #@24
    if-lez v8, :cond_37

    #@26
    .end local v8           #width:I
    :goto_26
    if-lez v4, :cond_39

    #@28
    .end local v4           #height:I
    :goto_28
    invoke-static {v9, v8, v4, p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@2b
    move-result-object v1

    #@2c
    .line 13222
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-nez v1, :cond_3b

    #@2e
    .line 13223
    new-instance v9, Ljava/lang/OutOfMemoryError;

    #@30
    invoke-direct {v9}, Ljava/lang/OutOfMemoryError;-><init>()V

    #@33
    throw v9

    #@34
    .line 13216
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    .end local v7           #scale:F
    .restart local v4       #height:I
    .restart local v8       #width:I
    :cond_34
    const/high16 v7, 0x3f80

    #@36
    goto :goto_12

    #@37
    .line 13220
    .restart local v7       #scale:F
    :cond_37
    const/4 v8, 0x1

    #@38
    goto :goto_26

    #@39
    .end local v8           #width:I
    :cond_39
    const/4 v4, 0x1

    #@3a
    goto :goto_28

    #@3b
    .line 13226
    .end local v4           #height:I
    .restart local v1       #bitmap:Landroid/graphics/Bitmap;
    :cond_3b
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v5

    #@3f
    .line 13227
    .local v5, resources:Landroid/content/res/Resources;
    if-eqz v5, :cond_4a

    #@41
    .line 13228
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@44
    move-result-object v9

    #@45
    iget v9, v9, Landroid/util/DisplayMetrics;->densityDpi:I

    #@47
    invoke-virtual {v1, v9}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@4a
    .line 13232
    :cond_4a
    if-eqz v0, :cond_9b

    #@4c
    .line 13233
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mCanvas:Landroid/graphics/Canvas;

    #@4e
    .line 13234
    .local v2, canvas:Landroid/graphics/Canvas;
    if-nez v2, :cond_55

    #@50
    .line 13235
    new-instance v2, Landroid/graphics/Canvas;

    #@52
    .end local v2           #canvas:Landroid/graphics/Canvas;
    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    #@55
    .line 13237
    .restart local v2       #canvas:Landroid/graphics/Canvas;
    :cond_55
    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@58
    .line 13242
    const/4 v9, 0x0

    #@59
    iput-object v9, v0, Landroid/view/View$AttachInfo;->mCanvas:Landroid/graphics/Canvas;

    #@5b
    .line 13248
    :goto_5b
    const/high16 v9, -0x100

    #@5d
    and-int/2addr v9, p2

    #@5e
    if-eqz v9, :cond_63

    #@60
    .line 13249
    invoke-virtual {v1, p2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    #@63
    .line 13252
    :cond_63
    invoke-virtual {p0}, Landroid/view/View;->computeScroll()V

    #@66
    .line 13253
    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    #@69
    move-result v6

    #@6a
    .line 13254
    .local v6, restoreCount:I
    invoke-virtual {v2, v7, v7}, Landroid/graphics/Canvas;->scale(FF)V

    #@6d
    .line 13255
    iget v9, p0, Landroid/view/View;->mScrollX:I

    #@6f
    neg-int v9, v9

    #@70
    int-to-float v9, v9

    #@71
    iget v10, p0, Landroid/view/View;->mScrollY:I

    #@73
    neg-int v10, v10

    #@74
    int-to-float v10, v10

    #@75
    invoke-virtual {v2, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    #@78
    .line 13258
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@7a
    .line 13259
    .local v3, flags:I
    iget v9, p0, Landroid/view/View;->mPrivateFlags:I

    #@7c
    const v10, -0x600001

    #@7f
    and-int/2addr v9, v10

    #@80
    iput v9, p0, Landroid/view/View;->mPrivateFlags:I

    #@82
    .line 13262
    iget v9, p0, Landroid/view/View;->mPrivateFlags:I

    #@84
    and-int/lit16 v9, v9, 0x80

    #@86
    const/16 v10, 0x80

    #@88
    if-ne v9, v10, :cond_a1

    #@8a
    .line 13263
    invoke-virtual {p0, v2}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@8d
    .line 13268
    :goto_8d
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@8f
    .line 13270
    invoke-virtual {v2, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@92
    .line 13271
    const/4 v9, 0x0

    #@93
    invoke-virtual {v2, v9}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@96
    .line 13273
    if-eqz v0, :cond_9a

    #@98
    .line 13275
    iput-object v2, v0, Landroid/view/View$AttachInfo;->mCanvas:Landroid/graphics/Canvas;

    #@9a
    .line 13278
    :cond_9a
    return-object v1

    #@9b
    .line 13245
    .end local v2           #canvas:Landroid/graphics/Canvas;
    .end local v3           #flags:I
    .end local v6           #restoreCount:I
    :cond_9b
    new-instance v2, Landroid/graphics/Canvas;

    #@9d
    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@a0
    .restart local v2       #canvas:Landroid/graphics/Canvas;
    goto :goto_5b

    #@a1
    .line 13265
    .restart local v3       #flags:I
    .restart local v6       #restoreCount:I
    :cond_a1
    invoke-virtual {p0, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    #@a4
    goto :goto_8d
.end method

.method public debug()V
    .registers 2

    #@0
    .prologue
    .line 15611
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/view/View;->debug(I)V

    #@4
    .line 15612
    return-void
.end method

.method protected debug(I)V
    .registers 7
    .parameter "depth"

    #@0
    .prologue
    .line 15624
    add-int/lit8 v3, p1, -0x1

    #@2
    invoke-static {v3}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 15626
    .local v1, output:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    const-string v4, "+ "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 15627
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    #@20
    move-result v0

    #@21
    .line 15628
    .local v0, id:I
    const/4 v3, -0x1

    #@22
    if-eq v0, v3, :cond_41

    #@24
    .line 15629
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, " (id="

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, ")"

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    .line 15631
    :cond_41
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@44
    move-result-object v2

    #@45
    .line 15632
    .local v2, tag:Ljava/lang/Object;
    if-eqz v2, :cond_64

    #@47
    .line 15633
    new-instance v3, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    const-string v4, " (tag="

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    const-string v4, ")"

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    .line 15635
    :cond_64
    const-string v3, "View"

    #@66
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 15637
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@6b
    and-int/lit8 v3, v3, 0x2

    #@6d
    if-eqz v3, :cond_8b

    #@6f
    .line 15638
    new-instance v3, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    const-string v4, " FOCUSED"

    #@7e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v1

    #@86
    .line 15639
    const-string v3, "View"

    #@88
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 15642
    :cond_8b
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    .line 15643
    new-instance v3, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v3

    #@98
    const-string v4, "frame={"

    #@9a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@a0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    const-string v4, ", "

    #@a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    iget v4, p0, Landroid/view/View;->mTop:I

    #@ac
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    const-string v4, ", "

    #@b2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v3

    #@b6
    iget v4, p0, Landroid/view/View;->mRight:I

    #@b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v3

    #@bc
    const-string v4, ", "

    #@be
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@c4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v3

    #@c8
    const-string/jumbo v4, "} scroll={"

    #@cb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v3

    #@cf
    iget v4, p0, Landroid/view/View;->mScrollX:I

    #@d1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v3

    #@d5
    const-string v4, ", "

    #@d7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v3

    #@db
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@dd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v3

    #@e1
    const-string/jumbo v4, "} "

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v1

    #@ec
    .line 15646
    const-string v3, "View"

    #@ee
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    .line 15648
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@f3
    if-nez v3, :cond_101

    #@f5
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@f7
    if-nez v3, :cond_101

    #@f9
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@fb
    if-nez v3, :cond_101

    #@fd
    iget v3, p0, Landroid/view/View;->mPaddingBottom:I

    #@ff
    if-eqz v3, :cond_14f

    #@101
    .line 15650
    :cond_101
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@104
    move-result-object v1

    #@105
    .line 15651
    new-instance v3, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    const-string/jumbo v4, "padding={"

    #@111
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v3

    #@115
    iget v4, p0, Landroid/view/View;->mPaddingLeft:I

    #@117
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v3

    #@11b
    const-string v4, ", "

    #@11d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v3

    #@121
    iget v4, p0, Landroid/view/View;->mPaddingTop:I

    #@123
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@126
    move-result-object v3

    #@127
    const-string v4, ", "

    #@129
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v3

    #@12d
    iget v4, p0, Landroid/view/View;->mPaddingRight:I

    #@12f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v3

    #@133
    const-string v4, ", "

    #@135
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v3

    #@139
    iget v4, p0, Landroid/view/View;->mPaddingBottom:I

    #@13b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v3

    #@13f
    const-string/jumbo v4, "}"

    #@142
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v3

    #@146
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v1

    #@14a
    .line 15653
    const-string v3, "View"

    #@14c
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14f
    .line 15656
    :cond_14f
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@152
    move-result-object v1

    #@153
    .line 15657
    new-instance v3, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v3

    #@15c
    const-string/jumbo v4, "mMeasureWidth="

    #@15f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v3

    #@163
    iget v4, p0, Landroid/view/View;->mMeasuredWidth:I

    #@165
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@168
    move-result-object v3

    #@169
    const-string v4, " mMeasureHeight="

    #@16b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v3

    #@16f
    iget v4, p0, Landroid/view/View;->mMeasuredHeight:I

    #@171
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@174
    move-result-object v3

    #@175
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@178
    move-result-object v1

    #@179
    .line 15659
    const-string v3, "View"

    #@17b
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17e
    .line 15661
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@181
    move-result-object v1

    #@182
    .line 15662
    iget-object v3, p0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@184
    if-nez v3, :cond_22e

    #@186
    .line 15663
    new-instance v3, Ljava/lang/StringBuilder;

    #@188
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v3

    #@18f
    const-string v4, "BAD! no layout params"

    #@191
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v3

    #@195
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v1

    #@199
    .line 15667
    :goto_199
    const-string v3, "View"

    #@19b
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19e
    .line 15669
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@1a1
    move-result-object v1

    #@1a2
    .line 15670
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a7
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v3

    #@1ab
    const-string v4, "flags={"

    #@1ad
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v3

    #@1b1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v1

    #@1b5
    .line 15671
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v3

    #@1be
    iget v4, p0, Landroid/view/View;->mViewFlags:I

    #@1c0
    invoke-static {v4}, Landroid/view/View;->printFlags(I)Ljava/lang/String;

    #@1c3
    move-result-object v4

    #@1c4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v3

    #@1c8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cb
    move-result-object v1

    #@1cc
    .line 15672
    new-instance v3, Ljava/lang/StringBuilder;

    #@1ce
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v3

    #@1d5
    const-string/jumbo v4, "}"

    #@1d8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v3

    #@1dc
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v1

    #@1e0
    .line 15673
    const-string v3, "View"

    #@1e2
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e5
    .line 15675
    invoke-static {p1}, Landroid/view/View;->debugIndent(I)Ljava/lang/String;

    #@1e8
    move-result-object v1

    #@1e9
    .line 15676
    new-instance v3, Ljava/lang/StringBuilder;

    #@1eb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ee
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v3

    #@1f2
    const-string/jumbo v4, "privateFlags={"

    #@1f5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v3

    #@1f9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fc
    move-result-object v1

    #@1fd
    .line 15677
    new-instance v3, Ljava/lang/StringBuilder;

    #@1ff
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@202
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v3

    #@206
    iget v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@208
    invoke-static {v4}, Landroid/view/View;->printPrivateFlags(I)Ljava/lang/String;

    #@20b
    move-result-object v4

    #@20c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v3

    #@210
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@213
    move-result-object v1

    #@214
    .line 15678
    new-instance v3, Ljava/lang/StringBuilder;

    #@216
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@219
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v3

    #@21d
    const-string/jumbo v4, "}"

    #@220
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v3

    #@224
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@227
    move-result-object v1

    #@228
    .line 15679
    const-string v3, "View"

    #@22a
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22d
    .line 15680
    return-void

    #@22e
    .line 15665
    :cond_22e
    iget-object v3, p0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@230
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup$LayoutParams;->debug(Ljava/lang/String;)Ljava/lang/String;

    #@233
    move-result-object v1

    #@234
    goto/16 :goto_199
.end method

.method public destroyDrawingCache()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 12992
    iget-object v0, p0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 12993
    iget-object v0, p0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@a
    .line 12994
    iput-object v1, p0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@c
    .line 12996
    :cond_c
    iget-object v0, p0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 12997
    iget-object v0, p0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@12
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@15
    .line 12998
    iput-object v1, p0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@17
    .line 13000
    :cond_17
    return-void
.end method

.method protected destroyHardwareResources()V
    .registers 2

    #@0
    .prologue
    .line 12684
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/view/View;->destroyLayer(Z)Z

    #@4
    .line 12685
    return-void
.end method

.method destroyLayer(Z)Z
    .registers 5
    .parameter "valid"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 12653
    iget-object v2, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@3
    if-eqz v2, :cond_37

    #@5
    .line 12654
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    .line 12655
    .local v0, info:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_36

    #@9
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@b
    if-eqz v2, :cond_36

    #@d
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@f
    invoke-virtual {v2}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_36

    #@15
    if-nez p1, :cond_1f

    #@17
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@19
    invoke-virtual {v2}, Landroid/view/HardwareRenderer;->validate()Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_36

    #@1f
    .line 12658
    :cond_1f
    iget-object v2, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@21
    invoke-virtual {v2}, Landroid/view/HardwareLayer;->destroy()V

    #@24
    .line 12659
    const/4 v2, 0x0

    #@25
    iput-object v2, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@27
    .line 12661
    iget-object v2, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@29
    if-eqz v2, :cond_30

    #@2b
    .line 12662
    iget-object v2, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2d
    invoke-virtual {v2}, Landroid/view/DisplayList;->reset()V

    #@30
    .line 12664
    :cond_30
    invoke-virtual {p0, v1}, Landroid/view/View;->invalidate(Z)V

    #@33
    .line 12665
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@36
    .line 12669
    .end local v0           #info:Landroid/view/View$AttachInfo;
    :cond_36
    :goto_36
    return v1

    #@37
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_36
.end method

.method dispatchAttachedToWindow(Landroid/view/View$AttachInfo;I)V
    .registers 10
    .parameter "info"
    .parameter "visibility"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 12152
    iput-object p1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    .line 12153
    iget v5, p0, Landroid/view/View;->mWindowAttachCount:I

    #@5
    add-int/lit8 v5, v5, 0x1

    #@7
    iput v5, p0, Landroid/view/View;->mWindowAttachCount:I

    #@9
    .line 12155
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@b
    or-int/lit16 v5, v5, 0x400

    #@d
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@f
    .line 12156
    iget-object v5, p0, Landroid/view/View;->mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

    #@11
    if-eqz v5, :cond_1c

    #@13
    .line 12157
    iget-object v5, p1, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@15
    iget-object v6, p0, Landroid/view/View;->mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

    #@17
    invoke-virtual {v5, v6}, Landroid/view/ViewTreeObserver;->merge(Landroid/view/ViewTreeObserver;)V

    #@1a
    .line 12158
    iput-object v3, p0, Landroid/view/View;->mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

    #@1c
    .line 12160
    :cond_1c
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@1e
    const/high16 v6, 0x8

    #@20
    and-int/2addr v5, v6

    #@21
    if-eqz v5, :cond_31

    #@23
    .line 12161
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@25
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v5, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 12162
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@2c
    const/high16 v6, 0x10

    #@2e
    or-int/2addr v5, v6

    #@2f
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@31
    .line 12164
    :cond_31
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@33
    invoke-virtual {p0, v5, p2}, Landroid/view/View;->performCollectViewAttributes(Landroid/view/View$AttachInfo;I)V

    #@36
    .line 12165
    invoke-virtual {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@39
    .line 12167
    iget-object v1, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@3b
    .line 12168
    .local v1, li:Landroid/view/View$ListenerInfo;
    if-eqz v1, :cond_41

    #@3d
    invoke-static {v1}, Landroid/view/View$ListenerInfo;->access$100(Landroid/view/View$ListenerInfo;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@40
    move-result-object v3

    #@41
    .line 12170
    .local v3, listeners:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/view/View$OnAttachStateChangeListener;>;"
    :cond_41
    if-eqz v3, :cond_5d

    #@43
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@46
    move-result v5

    #@47
    if-lez v5, :cond_5d

    #@49
    .line 12175
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@4c
    move-result-object v0

    #@4d
    .local v0, i$:Ljava/util/Iterator;
    :goto_4d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@50
    move-result v5

    #@51
    if-eqz v5, :cond_5d

    #@53
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@56
    move-result-object v2

    #@57
    check-cast v2, Landroid/view/View$OnAttachStateChangeListener;

    #@59
    .line 12176
    .local v2, listener:Landroid/view/View$OnAttachStateChangeListener;
    invoke-interface {v2, p0}, Landroid/view/View$OnAttachStateChangeListener;->onViewAttachedToWindow(Landroid/view/View;)V

    #@5c
    goto :goto_4d

    #@5d
    .line 12180
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #listener:Landroid/view/View$OnAttachStateChangeListener;
    :cond_5d
    iget v4, p1, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@5f
    .line 12181
    .local v4, vis:I
    const/16 v5, 0x8

    #@61
    if-eq v4, v5, :cond_66

    #@63
    .line 12182
    invoke-virtual {p0, v4}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    #@66
    .line 12184
    :cond_66
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@68
    and-int/lit16 v5, v5, 0x400

    #@6a
    if-eqz v5, :cond_6f

    #@6c
    .line 12186
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@6f
    .line 12188
    :cond_6f
    const/4 v5, 0x0

    #@70
    invoke-virtual {p0, v5}, Landroid/view/View;->needGlobalAttributesUpdate(Z)V

    #@73
    .line 12189
    return-void
.end method

.method dispatchCollectViewAttributes(Landroid/view/View$AttachInfo;I)V
    .registers 3
    .parameter "attachInfo"
    .parameter "visibility"

    #@0
    .prologue
    .line 7803
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->performCollectViewAttributes(Landroid/view/View$AttachInfo;I)V

    #@3
    .line 7804
    return-void
.end method

.method public dispatchConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 7782
    invoke-virtual {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 7783
    return-void
.end method

.method dispatchDetachedFromWindow()V
    .registers 10

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/16 v7, 0x8

    #@3
    .line 12192
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    .line 12193
    .local v1, info:Landroid/view/View$AttachInfo;
    if-eqz v1, :cond_e

    #@7
    .line 12194
    iget v5, v1, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@9
    .line 12195
    .local v5, vis:I
    if-eq v5, v7, :cond_e

    #@b
    .line 12196
    invoke-virtual {p0, v7}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    #@e
    .line 12200
    .end local v5           #vis:I
    :cond_e
    invoke-virtual {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@11
    .line 12202
    iget-object v2, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@13
    .line 12203
    .local v2, li:Landroid/view/View$ListenerInfo;
    if-eqz v2, :cond_35

    #@15
    invoke-static {v2}, Landroid/view/View$ListenerInfo;->access$100(Landroid/view/View$ListenerInfo;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@18
    move-result-object v4

    #@19
    .line 12205
    .local v4, listeners:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/view/View$OnAttachStateChangeListener;>;"
    :goto_19
    if-eqz v4, :cond_37

    #@1b
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@1e
    move-result v7

    #@1f
    if-lez v7, :cond_37

    #@21
    .line 12210
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v0

    #@25
    .local v0, i$:Ljava/util/Iterator;
    :goto_25
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_37

    #@2b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/view/View$OnAttachStateChangeListener;

    #@31
    .line 12211
    .local v3, listener:Landroid/view/View$OnAttachStateChangeListener;
    invoke-interface {v3, p0}, Landroid/view/View$OnAttachStateChangeListener;->onViewDetachedFromWindow(Landroid/view/View;)V

    #@34
    goto :goto_25

    #@35
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v3           #listener:Landroid/view/View$OnAttachStateChangeListener;
    .end local v4           #listeners:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/view/View$OnAttachStateChangeListener;>;"
    :cond_35
    move-object v4, v6

    #@36
    .line 12203
    goto :goto_19

    #@37
    .line 12215
    .restart local v4       #listeners:Ljava/util/concurrent/CopyOnWriteArrayList;,"Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/view/View$OnAttachStateChangeListener;>;"
    :cond_37
    iget v7, p0, Landroid/view/View;->mPrivateFlags:I

    #@39
    const/high16 v8, 0x10

    #@3b
    and-int/2addr v7, v8

    #@3c
    if-eqz v7, :cond_4d

    #@3e
    .line 12216
    iget-object v7, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    iget-object v7, v7, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v7, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@45
    .line 12217
    iget v7, p0, Landroid/view/View;->mPrivateFlags:I

    #@47
    const v8, -0x100001

    #@4a
    and-int/2addr v7, v8

    #@4b
    iput v7, p0, Landroid/view/View;->mPrivateFlags:I

    #@4d
    .line 12220
    :cond_4d
    iput-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4f
    .line 12221
    return-void
.end method

.method public dispatchDisplayHint(I)V
    .registers 2
    .parameter "hint"

    #@0
    .prologue
    .line 7649
    invoke-virtual {p0, p1}, Landroid/view/View;->onDisplayHint(I)V

    #@3
    .line 7650
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 16628
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 16629
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_1c

    #@4
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$600(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnDragListener;

    #@7
    move-result-object v1

    #@8
    if-eqz v1, :cond_1c

    #@a
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@c
    and-int/lit8 v1, v1, 0x20

    #@e
    if-nez v1, :cond_1c

    #@10
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$600(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnDragListener;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v1, p0, p1}, Landroid/view/View$OnDragListener;->onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    .line 16631
    const/4 v1, 0x1

    #@1b
    .line 16633
    :goto_1b
    return v1

    #@1c
    :cond_1c
    invoke-virtual {p0, p1}, Landroid/view/View;->onDragEvent(Landroid/view/DragEvent;)Z

    #@1f
    move-result v1

    #@20
    goto :goto_1b
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 8775
    return-void
.end method

.method public dispatchFinishTemporaryDetach()V
    .registers 1

    #@0
    .prologue
    .line 7276
    invoke-virtual {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    #@3
    .line 7277
    return-void
.end method

.method protected dispatchGenericFocusedEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 7537
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 7431
    iget-object v4, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@4
    if-eqz v4, :cond_b

    #@6
    .line 7432
    iget-object v4, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@8
    invoke-virtual {v4, p1, v3}, Landroid/view/InputEventConsistencyVerifier;->onGenericMotionEvent(Landroid/view/MotionEvent;I)V

    #@b
    .line 7435
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@e
    move-result v1

    #@f
    .line 7436
    .local v1, source:I
    and-int/lit8 v4, v1, 0x2

    #@11
    if-eqz v4, :cond_40

    #@13
    .line 7437
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@16
    move-result v0

    #@17
    .line 7438
    .local v0, action:I
    const/16 v4, 0x9

    #@19
    if-eq v0, v4, :cond_22

    #@1b
    const/4 v4, 0x7

    #@1c
    if-eq v0, v4, :cond_22

    #@1e
    const/16 v4, 0xa

    #@20
    if-ne v0, v4, :cond_29

    #@22
    .line 7441
    :cond_22
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_2f

    #@28
    .line 7458
    .end local v0           #action:I
    :cond_28
    :goto_28
    return v2

    #@29
    .line 7444
    .restart local v0       #action:I
    :cond_29
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchGenericPointerEvent(Landroid/view/MotionEvent;)Z

    #@2c
    move-result v4

    #@2d
    if-nez v4, :cond_28

    #@2f
    .line 7451
    .end local v0           #action:I
    :cond_2f
    invoke-direct {p0, p1}, Landroid/view/View;->dispatchGenericMotionEventInternal(Landroid/view/MotionEvent;)Z

    #@32
    move-result v4

    #@33
    if-nez v4, :cond_28

    #@35
    .line 7455
    iget-object v2, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@37
    if-eqz v2, :cond_3e

    #@39
    .line 7456
    iget-object v2, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@3b
    invoke-virtual {v2, p1, v3}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    .line 7458
    goto :goto_28

    #@40
    .line 7447
    :cond_40
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchGenericFocusedEvent(Landroid/view/MotionEvent;)Z

    #@43
    move-result v4

    #@44
    if-eqz v4, :cond_2f

    #@46
    goto :goto_28
.end method

.method protected dispatchGenericPointerEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 7523
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected dispatchGetDisplayList()V
    .registers 1

    #@0
    .prologue
    .line 12765
    return-void
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 7492
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 7493
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_1c

    #@4
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$500(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnHoverListener;

    #@7
    move-result-object v1

    #@8
    if-eqz v1, :cond_1c

    #@a
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@c
    and-int/lit8 v1, v1, 0x20

    #@e
    if-nez v1, :cond_1c

    #@10
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$500(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnHoverListener;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v1, p0, p1}, Landroid/view/View$OnHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    .line 7496
    const/4 v1, 0x1

    #@1b
    .line 7499
    :goto_1b
    return v1

    #@1c
    :cond_1c
    invoke-virtual {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@1f
    move-result v1

    #@20
    goto :goto_1b
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 7322
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 7323
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@8
    invoke-virtual {v1, p1, v3}, Landroid/view/InputEventConsistencyVerifier;->onKeyEvent(Landroid/view/KeyEvent;I)V

    #@b
    .line 7328
    :cond_b
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@d
    .line 7329
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_2b

    #@f
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$200(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnKeyListener;

    #@12
    move-result-object v1

    #@13
    if-eqz v1, :cond_2b

    #@15
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@17
    and-int/lit8 v1, v1, 0x20

    #@19
    if-nez v1, :cond_2b

    #@1b
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$200(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnKeyListener;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@22
    move-result v4

    #@23
    invoke-interface {v1, p0, v4, p1}, Landroid/view/View$OnKeyListener;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_2b

    #@29
    move v1, v2

    #@2a
    .line 7342
    :goto_2a
    return v1

    #@2b
    .line 7334
    :cond_2b
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2d
    if-eqz v1, :cond_3b

    #@2f
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@31
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mKeyDispatchState:Landroid/view/KeyEvent$DispatcherState;

    #@33
    :goto_33
    invoke-virtual {p1, p0, v1, p0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_3d

    #@39
    move v1, v2

    #@3a
    .line 7336
    goto :goto_2a

    #@3b
    .line 7334
    :cond_3b
    const/4 v1, 0x0

    #@3c
    goto :goto_33

    #@3d
    .line 7339
    :cond_3d
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@3f
    if-eqz v1, :cond_46

    #@41
    .line 7340
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@43
    invoke-virtual {v1, p1, v3}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@46
    :cond_46
    move v1, v3

    #@47
    .line 7342
    goto :goto_2a
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 7308
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 7352
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final dispatchPointerEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 7554
    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 7555
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@9
    move-result v0

    #@a
    .line 7557
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 4855
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 4856
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/view/View$AccessibilityDelegate;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@9
    move-result v0

    #@a
    .line 4858
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method dispatchPopulateAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 4868
    invoke-virtual {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 4869
    const/4 v0, 0x0

    #@4
    return v0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 12315
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    iget v1, p0, Landroid/view/View;->mID:I

    #@2
    const/4 v2, -0x1

    #@3
    if-eq v1, v2, :cond_29

    #@5
    .line 12316
    iget v1, p0, Landroid/view/View;->mID:I

    #@7
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/os/Parcelable;

    #@d
    .line 12317
    .local v0, state:Landroid/os/Parcelable;
    if-eqz v0, :cond_29

    #@f
    .line 12320
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@11
    const v2, -0x20001

    #@14
    and-int/2addr v1, v2

    #@15
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@17
    .line 12321
    invoke-virtual {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1a
    .line 12322
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@1c
    const/high16 v2, 0x2

    #@1e
    and-int/2addr v1, v2

    #@1f
    if-nez v1, :cond_29

    #@21
    .line 12323
    new-instance v1, Ljava/lang/IllegalStateException;

    #@23
    const-string v2, "Derived class did not call super.onRestoreInstanceState()"

    #@25
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1

    #@29
    .line 12328
    .end local v0           #state:Landroid/os/Parcelable;
    :cond_29
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 12248
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    iget v1, p0, Landroid/view/View;->mID:I

    #@2
    const/4 v2, -0x1

    #@3
    if-eq v1, v2, :cond_2e

    #@5
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@7
    const/high16 v2, 0x1

    #@9
    and-int/2addr v1, v2

    #@a
    if-nez v1, :cond_2e

    #@c
    .line 12249
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    const v2, -0x20001

    #@11
    and-int/2addr v1, v2

    #@12
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@14
    .line 12250
    invoke-virtual {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    #@17
    move-result-object v0

    #@18
    .line 12251
    .local v0, state:Landroid/os/Parcelable;
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@1a
    const/high16 v2, 0x2

    #@1c
    and-int/2addr v1, v2

    #@1d
    if-nez v1, :cond_27

    #@1f
    .line 12252
    new-instance v1, Ljava/lang/IllegalStateException;

    #@21
    const-string v2, "Derived class did not call super.onSaveInstanceState()"

    #@23
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 12255
    :cond_27
    if-eqz v0, :cond_2e

    #@29
    .line 12258
    iget v1, p0, Landroid/view/View;->mID:I

    #@2b
    invoke-virtual {p1, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2e
    .line 12261
    .end local v0           #state:Landroid/os/Parcelable;
    :cond_2e
    return-void
.end method

.method dispatchScreenStateChanged(I)V
    .registers 2
    .parameter "screenState"

    #@0
    .prologue
    .line 11785
    invoke-virtual {p0, p1}, Landroid/view/View;->onScreenStateChanged(I)V

    #@3
    .line 11786
    return-void
.end method

.method protected dispatchSetActivated(Z)V
    .registers 2
    .parameter "activated"

    #@0
    .prologue
    .line 15160
    return-void
.end method

.method protected dispatchSetPressed(Z)V
    .registers 2
    .parameter "pressed"

    #@0
    .prologue
    .line 6261
    return-void
.end method

.method protected dispatchSetSelected(Z)V
    .registers 2
    .parameter "selected"

    #@0
    .prologue
    .line 15118
    return-void
.end method

.method public dispatchStartTemporaryDetach()V
    .registers 1

    #@0
    .prologue
    .line 7255
    invoke-virtual {p0}, Landroid/view/View;->clearAccessibilityFocus()V

    #@3
    .line 7256
    invoke-direct {p0}, Landroid/view/View;->clearDisplayList()V

    #@6
    .line 7258
    invoke-virtual {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    #@9
    .line 7259
    return-void
.end method

.method public dispatchSystemUiVisibilityChanged(I)V
    .registers 5
    .parameter "visibility"

    #@0
    .prologue
    .line 16340
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 16341
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_15

    #@4
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$1000(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnSystemUiVisibilityChangeListener;

    #@7
    move-result-object v1

    #@8
    if-eqz v1, :cond_15

    #@a
    .line 16342
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$1000(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnSystemUiVisibilityChangeListener;

    #@d
    move-result-object v1

    #@e
    const v2, 0xffff

    #@11
    and-int/2addr v2, p1

    #@12
    invoke-interface {v1, v2}, Landroid/view/View$OnSystemUiVisibilityChangeListener;->onSystemUiVisibilityChange(I)V

    #@15
    .line 16345
    :cond_15
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 7363
    iget-object v3, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@4
    if-eqz v3, :cond_b

    #@6
    .line 7364
    iget-object v3, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@8
    invoke-virtual {v3, p1, v2}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    #@b
    .line 7367
    :cond_b
    invoke-virtual {p0, p1}, Landroid/view/View;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_32

    #@11
    .line 7369
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@13
    .line 7370
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_2c

    #@15
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$300(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnTouchListener;

    #@18
    move-result-object v3

    #@19
    if-eqz v3, :cond_2c

    #@1b
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@1d
    and-int/lit8 v3, v3, 0x20

    #@1f
    if-nez v3, :cond_2c

    #@21
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$300(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnTouchListener;

    #@24
    move-result-object v3

    #@25
    invoke-interface {v3, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_2c

    #@2b
    .line 7383
    .end local v0           #li:Landroid/view/View$ListenerInfo;
    :cond_2b
    :goto_2b
    return v1

    #@2c
    .line 7375
    .restart local v0       #li:Landroid/view/View$ListenerInfo;
    :cond_2c
    invoke-virtual {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@2f
    move-result v3

    #@30
    if-nez v3, :cond_2b

    #@32
    .line 7380
    .end local v0           #li:Landroid/view/View$ListenerInfo;
    :cond_32
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@34
    if-eqz v1, :cond_3b

    #@36
    .line 7381
    iget-object v1, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@38
    invoke-virtual {v1, p1, v2}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@3b
    :cond_3b
    move v1, v2

    #@3c
    .line 7383
    goto :goto_2b
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 7411
    iget-object v0, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 7412
    iget-object v0, p0, Landroid/view/View;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, p1, v1}, Landroid/view/InputEventConsistencyVerifier;->onTrackballEvent(Landroid/view/MotionEvent;I)V

    #@a
    .line 7415
    :cond_a
    invoke-virtual {p0, p1}, Landroid/view/View;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 4
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 6426
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected dispatchVisibilityChanged(Landroid/view/View;I)V
    .registers 3
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 7619
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 7620
    return-void
.end method

.method public dispatchWindowFocusChanged(Z)V
    .registers 2
    .parameter "hasFocus"

    #@0
    .prologue
    .line 7569
    invoke-virtual {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    #@3
    .line 7570
    return-void
.end method

.method public dispatchWindowSystemUiVisiblityChanged(I)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 16321
    invoke-virtual {p0, p1}, Landroid/view/View;->onWindowSystemUiVisibilityChanged(I)V

    #@3
    .line 16322
    return-void
.end method

.method public dispatchWindowVisibilityChanged(I)V
    .registers 2
    .parameter "visibility"

    #@0
    .prologue
    .line 7673
    invoke-virtual {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    #@3
    .line 7674
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 41
    .parameter "canvas"

    #@0
    .prologue
    .line 13893
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Landroid/view/View;->mPrivateFlags:I

    #@4
    move/from16 v27, v0

    #@6
    .line 13894
    .local v27, privateFlags:I
    const/high16 v2, 0x60

    #@8
    and-int v2, v2, v27

    #@a
    const/high16 v3, 0x40

    #@c
    if-ne v2, v3, :cond_8d

    #@e
    move-object/from16 v0, p0

    #@10
    iget-object v2, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@12
    if-eqz v2, :cond_1c

    #@14
    move-object/from16 v0, p0

    #@16
    iget-object v2, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@18
    iget-boolean v2, v2, Landroid/view/View$AttachInfo;->mIgnoreDirtyState:Z

    #@1a
    if-nez v2, :cond_8d

    #@1c
    :cond_1c
    const/4 v12, 0x1

    #@1d
    .line 13896
    .local v12, dirtyOpaque:Z
    :goto_1d
    const v2, -0x600001

    #@20
    and-int v2, v2, v27

    #@22
    or-int/lit8 v2, v2, 0x20

    #@24
    move-object/from16 v0, p0

    #@26
    iput v2, v0, Landroid/view/View;->mPrivateFlags:I

    #@28
    .line 13913
    if-nez v12, :cond_67

    #@2a
    .line 13914
    move-object/from16 v0, p0

    #@2c
    iget-object v9, v0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2e
    .line 13915
    .local v9, background:Landroid/graphics/drawable/Drawable;
    if-eqz v9, :cond_67

    #@30
    .line 13916
    move-object/from16 v0, p0

    #@32
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@34
    move/from16 v31, v0

    #@36
    .line 13917
    .local v31, scrollX:I
    move-object/from16 v0, p0

    #@38
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@3a
    move/from16 v32, v0

    #@3c
    .line 13919
    .local v32, scrollY:I
    move-object/from16 v0, p0

    #@3e
    iget-boolean v2, v0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@40
    if-eqz v2, :cond_5e

    #@42
    .line 13920
    const/4 v2, 0x0

    #@43
    const/4 v3, 0x0

    #@44
    move-object/from16 v0, p0

    #@46
    iget v4, v0, Landroid/view/View;->mRight:I

    #@48
    move-object/from16 v0, p0

    #@4a
    iget v5, v0, Landroid/view/View;->mLeft:I

    #@4c
    sub-int/2addr v4, v5

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget v5, v0, Landroid/view/View;->mBottom:I

    #@51
    move-object/from16 v0, p0

    #@53
    iget v6, v0, Landroid/view/View;->mTop:I

    #@55
    sub-int/2addr v5, v6

    #@56
    invoke-virtual {v9, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@59
    .line 13921
    const/4 v2, 0x0

    #@5a
    move-object/from16 v0, p0

    #@5c
    iput-boolean v2, v0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@5e
    .line 13924
    :cond_5e
    or-int v2, v31, v32

    #@60
    if-nez v2, :cond_8f

    #@62
    .line 13925
    move-object/from16 v0, p1

    #@64
    invoke-virtual {v9, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@67
    .line 13935
    .end local v9           #background:Landroid/graphics/drawable/Drawable;
    .end local v31           #scrollX:I
    .end local v32           #scrollY:I
    :cond_67
    :goto_67
    move-object/from16 v0, p0

    #@69
    iget v0, v0, Landroid/view/View;->mViewFlags:I

    #@6b
    move/from16 v38, v0

    #@6d
    .line 13936
    .local v38, viewFlags:I
    move/from16 v0, v38

    #@6f
    and-int/lit16 v2, v0, 0x1000

    #@71
    if-eqz v2, :cond_ad

    #@73
    const/16 v20, 0x1

    #@75
    .line 13937
    .local v20, horizontalEdges:Z
    :goto_75
    move/from16 v0, v38

    #@77
    and-int/lit16 v2, v0, 0x2000

    #@79
    if-eqz v2, :cond_b0

    #@7b
    const/16 v37, 0x1

    #@7d
    .line 13938
    .local v37, verticalEdges:Z
    :goto_7d
    if-nez v37, :cond_b3

    #@7f
    if-nez v20, :cond_b3

    #@81
    .line 13940
    if-nez v12, :cond_86

    #@83
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@86
    .line 13943
    :cond_86
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@89
    .line 13946
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->onDrawScrollBars(Landroid/graphics/Canvas;)V

    #@8c
    .line 14087
    :goto_8c
    return-void

    #@8d
    .line 13894
    .end local v12           #dirtyOpaque:Z
    .end local v20           #horizontalEdges:Z
    .end local v37           #verticalEdges:Z
    .end local v38           #viewFlags:I
    :cond_8d
    const/4 v12, 0x0

    #@8e
    goto :goto_1d

    #@8f
    .line 13927
    .restart local v9       #background:Landroid/graphics/drawable/Drawable;
    .restart local v12       #dirtyOpaque:Z
    .restart local v31       #scrollX:I
    .restart local v32       #scrollY:I
    :cond_8f
    move/from16 v0, v31

    #@91
    int-to-float v2, v0

    #@92
    move/from16 v0, v32

    #@94
    int-to-float v3, v0

    #@95
    move-object/from16 v0, p1

    #@97
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@9a
    .line 13928
    move-object/from16 v0, p1

    #@9c
    invoke-virtual {v9, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@9f
    .line 13929
    move/from16 v0, v31

    #@a1
    neg-int v2, v0

    #@a2
    int-to-float v2, v2

    #@a3
    move/from16 v0, v32

    #@a5
    neg-int v3, v0

    #@a6
    int-to-float v3, v3

    #@a7
    move-object/from16 v0, p1

    #@a9
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    #@ac
    goto :goto_67

    #@ad
    .line 13936
    .end local v9           #background:Landroid/graphics/drawable/Drawable;
    .end local v31           #scrollX:I
    .end local v32           #scrollY:I
    .restart local v38       #viewFlags:I
    :cond_ad
    const/16 v20, 0x0

    #@af
    goto :goto_75

    #@b0
    .line 13937
    .restart local v20       #horizontalEdges:Z
    :cond_b0
    const/16 v37, 0x0

    #@b2
    goto :goto_7d

    #@b3
    .line 13959
    .restart local v37       #verticalEdges:Z
    :cond_b3
    const/16 v16, 0x0

    #@b5
    .line 13960
    .local v16, drawTop:Z
    const/4 v13, 0x0

    #@b6
    .line 13961
    .local v13, drawBottom:Z
    const/4 v14, 0x0

    #@b7
    .line 13962
    .local v14, drawLeft:Z
    const/4 v15, 0x0

    #@b8
    .line 13964
    .local v15, drawRight:Z
    const/16 v36, 0x0

    #@ba
    .line 13965
    .local v36, topFadeStrength:F
    const/4 v11, 0x0

    #@bb
    .line 13966
    .local v11, bottomFadeStrength:F
    const/16 v22, 0x0

    #@bd
    .line 13967
    .local v22, leftFadeStrength:F
    const/16 v29, 0x0

    #@bf
    .line 13970
    .local v29, rightFadeStrength:F
    move-object/from16 v0, p0

    #@c1
    iget v0, v0, Landroid/view/View;->mPaddingLeft:I

    #@c3
    move/from16 v26, v0

    #@c5
    .line 13972
    .local v26, paddingLeft:I
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->isPaddingOffsetRequired()Z

    #@c8
    move-result v25

    #@c9
    .line 13973
    .local v25, offsetRequired:Z
    if-eqz v25, :cond_d1

    #@cb
    .line 13974
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLeftPaddingOffset()I

    #@ce
    move-result v2

    #@cf
    add-int v26, v26, v2

    #@d1
    .line 13977
    :cond_d1
    move-object/from16 v0, p0

    #@d3
    iget v2, v0, Landroid/view/View;->mScrollX:I

    #@d5
    add-int v21, v2, v26

    #@d7
    .line 13978
    .local v21, left:I
    move-object/from16 v0, p0

    #@d9
    iget v2, v0, Landroid/view/View;->mRight:I

    #@db
    add-int v2, v2, v21

    #@dd
    move-object/from16 v0, p0

    #@df
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@e1
    sub-int/2addr v2, v3

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget v3, v0, Landroid/view/View;->mPaddingRight:I

    #@e6
    sub-int/2addr v2, v3

    #@e7
    sub-int v28, v2, v26

    #@e9
    .line 13979
    .local v28, right:I
    move-object/from16 v0, p0

    #@eb
    iget v2, v0, Landroid/view/View;->mScrollY:I

    #@ed
    move-object/from16 v0, p0

    #@ef
    move/from16 v1, v25

    #@f1
    invoke-virtual {v0, v1}, Landroid/view/View;->getFadeTop(Z)I

    #@f4
    move-result v3

    #@f5
    add-int v35, v2, v3

    #@f7
    .line 13980
    .local v35, top:I
    move-object/from16 v0, p0

    #@f9
    move/from16 v1, v25

    #@fb
    invoke-virtual {v0, v1}, Landroid/view/View;->getFadeHeight(Z)I

    #@fe
    move-result v2

    #@ff
    add-int v10, v35, v2

    #@101
    .line 13982
    .local v10, bottom:I
    if-eqz v25, :cond_10e

    #@103
    .line 13983
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getRightPaddingOffset()I

    #@106
    move-result v2

    #@107
    add-int v28, v28, v2

    #@109
    .line 13984
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getBottomPaddingOffset()I

    #@10c
    move-result v2

    #@10d
    add-int/2addr v10, v2

    #@10e
    .line 13987
    :cond_10e
    move-object/from16 v0, p0

    #@110
    iget-object v0, v0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@112
    move-object/from16 v33, v0

    #@114
    .line 13988
    .local v33, scrollabilityCache:Landroid/view/View$ScrollabilityCache;
    move-object/from16 v0, v33

    #@116
    iget v2, v0, Landroid/view/View$ScrollabilityCache;->fadingEdgeLength:I

    #@118
    int-to-float v0, v2

    #@119
    move/from16 v18, v0

    #@11b
    .line 13989
    .local v18, fadeHeight:F
    move/from16 v0, v18

    #@11d
    float-to-int v0, v0

    #@11e
    move/from16 v23, v0

    #@120
    .line 13993
    .local v23, length:I
    if-eqz v37, :cond_12c

    #@122
    add-int v2, v35, v23

    #@124
    sub-int v3, v10, v23

    #@126
    if-le v2, v3, :cond_12c

    #@128
    .line 13994
    sub-int v2, v10, v35

    #@12a
    div-int/lit8 v23, v2, 0x2

    #@12c
    .line 13998
    :cond_12c
    if-eqz v20, :cond_138

    #@12e
    add-int v2, v21, v23

    #@130
    sub-int v3, v28, v23

    #@132
    if-le v2, v3, :cond_138

    #@134
    .line 13999
    sub-int v2, v28, v21

    #@136
    div-int/lit8 v23, v2, 0x2

    #@138
    .line 14002
    :cond_138
    if-eqz v37, :cond_16b

    #@13a
    .line 14003
    const/4 v2, 0x0

    #@13b
    const/high16 v3, 0x3f80

    #@13d
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTopFadingEdgeStrength()F

    #@140
    move-result v4

    #@141
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@144
    move-result v3

    #@145
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    #@148
    move-result v36

    #@149
    .line 14004
    mul-float v2, v36, v18

    #@14b
    const/high16 v3, 0x3f80

    #@14d
    cmpl-float v2, v2, v3

    #@14f
    if-lez v2, :cond_2e0

    #@151
    const/16 v16, 0x1

    #@153
    .line 14005
    :goto_153
    const/4 v2, 0x0

    #@154
    const/high16 v3, 0x3f80

    #@156
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getBottomFadingEdgeStrength()F

    #@159
    move-result v4

    #@15a
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@15d
    move-result v3

    #@15e
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    #@161
    move-result v11

    #@162
    .line 14006
    mul-float v2, v11, v18

    #@164
    const/high16 v3, 0x3f80

    #@166
    cmpl-float v2, v2, v3

    #@168
    if-lez v2, :cond_2e4

    #@16a
    const/4 v13, 0x1

    #@16b
    .line 14009
    :cond_16b
    :goto_16b
    if-eqz v20, :cond_19d

    #@16d
    .line 14010
    const/4 v2, 0x0

    #@16e
    const/high16 v3, 0x3f80

    #@170
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLeftFadingEdgeStrength()F

    #@173
    move-result v4

    #@174
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@177
    move-result v3

    #@178
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    #@17b
    move-result v22

    #@17c
    .line 14011
    mul-float v2, v22, v18

    #@17e
    const/high16 v3, 0x3f80

    #@180
    cmpl-float v2, v2, v3

    #@182
    if-lez v2, :cond_2e7

    #@184
    const/4 v14, 0x1

    #@185
    .line 14012
    :goto_185
    const/4 v2, 0x0

    #@186
    const/high16 v3, 0x3f80

    #@188
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getRightFadingEdgeStrength()F

    #@18b
    move-result v4

    #@18c
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@18f
    move-result v3

    #@190
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    #@193
    move-result v29

    #@194
    .line 14013
    mul-float v2, v29, v18

    #@196
    const/high16 v3, 0x3f80

    #@198
    cmpl-float v2, v2, v3

    #@19a
    if-lez v2, :cond_2ea

    #@19c
    const/4 v15, 0x1

    #@19d
    .line 14016
    :cond_19d
    :goto_19d
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    #@1a0
    move-result v30

    #@1a1
    .line 14018
    .local v30, saveCount:I
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getSolidColor()I

    #@1a4
    move-result v34

    #@1a5
    .line 14019
    .local v34, solidColor:I
    if-nez v34, :cond_2ed

    #@1a7
    .line 14020
    const/16 v19, 0x4

    #@1a9
    .line 14022
    .local v19, flags:I
    if-eqz v16, :cond_1be

    #@1ab
    .line 14023
    move/from16 v0, v21

    #@1ad
    int-to-float v3, v0

    #@1ae
    move/from16 v0, v35

    #@1b0
    int-to-float v4, v0

    #@1b1
    move/from16 v0, v28

    #@1b3
    int-to-float v5, v0

    #@1b4
    add-int v2, v35, v23

    #@1b6
    int-to-float v6, v2

    #@1b7
    const/4 v7, 0x0

    #@1b8
    const/4 v8, 0x4

    #@1b9
    move-object/from16 v2, p1

    #@1bb
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@1be
    .line 14026
    :cond_1be
    if-eqz v13, :cond_1d1

    #@1c0
    .line 14027
    move/from16 v0, v21

    #@1c2
    int-to-float v3, v0

    #@1c3
    sub-int v2, v10, v23

    #@1c5
    int-to-float v4, v2

    #@1c6
    move/from16 v0, v28

    #@1c8
    int-to-float v5, v0

    #@1c9
    int-to-float v6, v10

    #@1ca
    const/4 v7, 0x0

    #@1cb
    const/4 v8, 0x4

    #@1cc
    move-object/from16 v2, p1

    #@1ce
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@1d1
    .line 14030
    :cond_1d1
    if-eqz v14, :cond_1e4

    #@1d3
    .line 14031
    move/from16 v0, v21

    #@1d5
    int-to-float v3, v0

    #@1d6
    move/from16 v0, v35

    #@1d8
    int-to-float v4, v0

    #@1d9
    add-int v2, v21, v23

    #@1db
    int-to-float v5, v2

    #@1dc
    int-to-float v6, v10

    #@1dd
    const/4 v7, 0x0

    #@1de
    const/4 v8, 0x4

    #@1df
    move-object/from16 v2, p1

    #@1e1
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@1e4
    .line 14034
    :cond_1e4
    if-eqz v15, :cond_1f7

    #@1e6
    .line 14035
    sub-int v2, v28, v23

    #@1e8
    int-to-float v3, v2

    #@1e9
    move/from16 v0, v35

    #@1eb
    int-to-float v4, v0

    #@1ec
    move/from16 v0, v28

    #@1ee
    int-to-float v5, v0

    #@1ef
    int-to-float v6, v10

    #@1f0
    const/4 v7, 0x0

    #@1f1
    const/4 v8, 0x4

    #@1f2
    move-object/from16 v2, p1

    #@1f4
    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@1f7
    .line 14042
    .end local v19           #flags:I
    :cond_1f7
    :goto_1f7
    if-nez v12, :cond_1fc

    #@1f9
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@1fc
    .line 14045
    :cond_1fc
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@1ff
    .line 14048
    move-object/from16 v0, v33

    #@201
    iget-object v7, v0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@203
    .line 14049
    .local v7, p:Landroid/graphics/Paint;
    move-object/from16 v0, v33

    #@205
    iget-object v0, v0, Landroid/view/View$ScrollabilityCache;->matrix:Landroid/graphics/Matrix;

    #@207
    move-object/from16 v24, v0

    #@209
    .line 14050
    .local v24, matrix:Landroid/graphics/Matrix;
    move-object/from16 v0, v33

    #@20b
    iget-object v0, v0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@20d
    move-object/from16 v17, v0

    #@20f
    .line 14052
    .local v17, fade:Landroid/graphics/Shader;
    if-eqz v16, :cond_23d

    #@211
    .line 14053
    const/high16 v2, 0x3f80

    #@213
    mul-float v3, v18, v36

    #@215
    move-object/from16 v0, v24

    #@217
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    #@21a
    .line 14054
    move/from16 v0, v21

    #@21c
    int-to-float v2, v0

    #@21d
    move/from16 v0, v35

    #@21f
    int-to-float v3, v0

    #@220
    move-object/from16 v0, v24

    #@222
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@225
    .line 14055
    move-object/from16 v0, v17

    #@227
    move-object/from16 v1, v24

    #@229
    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    #@22c
    .line 14056
    move/from16 v0, v21

    #@22e
    int-to-float v3, v0

    #@22f
    move/from16 v0, v35

    #@231
    int-to-float v4, v0

    #@232
    move/from16 v0, v28

    #@234
    int-to-float v5, v0

    #@235
    add-int v2, v35, v23

    #@237
    int-to-float v6, v2

    #@238
    move-object/from16 v2, p1

    #@23a
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@23d
    .line 14059
    :cond_23d
    if-eqz v13, :cond_26e

    #@23f
    .line 14060
    const/high16 v2, 0x3f80

    #@241
    mul-float v3, v18, v11

    #@243
    move-object/from16 v0, v24

    #@245
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    #@248
    .line 14061
    const/high16 v2, 0x4334

    #@24a
    move-object/from16 v0, v24

    #@24c
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    #@24f
    .line 14062
    move/from16 v0, v21

    #@251
    int-to-float v2, v0

    #@252
    int-to-float v3, v10

    #@253
    move-object/from16 v0, v24

    #@255
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@258
    .line 14063
    move-object/from16 v0, v17

    #@25a
    move-object/from16 v1, v24

    #@25c
    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    #@25f
    .line 14064
    move/from16 v0, v21

    #@261
    int-to-float v3, v0

    #@262
    sub-int v2, v10, v23

    #@264
    int-to-float v4, v2

    #@265
    move/from16 v0, v28

    #@267
    int-to-float v5, v0

    #@268
    int-to-float v6, v10

    #@269
    move-object/from16 v2, p1

    #@26b
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@26e
    .line 14067
    :cond_26e
    if-eqz v14, :cond_2a1

    #@270
    .line 14068
    const/high16 v2, 0x3f80

    #@272
    mul-float v3, v18, v22

    #@274
    move-object/from16 v0, v24

    #@276
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    #@279
    .line 14069
    const/high16 v2, -0x3d4c

    #@27b
    move-object/from16 v0, v24

    #@27d
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    #@280
    .line 14070
    move/from16 v0, v21

    #@282
    int-to-float v2, v0

    #@283
    move/from16 v0, v35

    #@285
    int-to-float v3, v0

    #@286
    move-object/from16 v0, v24

    #@288
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@28b
    .line 14071
    move-object/from16 v0, v17

    #@28d
    move-object/from16 v1, v24

    #@28f
    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    #@292
    .line 14072
    move/from16 v0, v21

    #@294
    int-to-float v3, v0

    #@295
    move/from16 v0, v35

    #@297
    int-to-float v4, v0

    #@298
    add-int v2, v21, v23

    #@29a
    int-to-float v5, v2

    #@29b
    int-to-float v6, v10

    #@29c
    move-object/from16 v2, p1

    #@29e
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@2a1
    .line 14075
    :cond_2a1
    if-eqz v15, :cond_2d4

    #@2a3
    .line 14076
    const/high16 v2, 0x3f80

    #@2a5
    mul-float v3, v18, v29

    #@2a7
    move-object/from16 v0, v24

    #@2a9
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    #@2ac
    .line 14077
    const/high16 v2, 0x42b4

    #@2ae
    move-object/from16 v0, v24

    #@2b0
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    #@2b3
    .line 14078
    move/from16 v0, v28

    #@2b5
    int-to-float v2, v0

    #@2b6
    move/from16 v0, v35

    #@2b8
    int-to-float v3, v0

    #@2b9
    move-object/from16 v0, v24

    #@2bb
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@2be
    .line 14079
    move-object/from16 v0, v17

    #@2c0
    move-object/from16 v1, v24

    #@2c2
    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    #@2c5
    .line 14080
    sub-int v2, v28, v23

    #@2c7
    int-to-float v3, v2

    #@2c8
    move/from16 v0, v35

    #@2ca
    int-to-float v4, v0

    #@2cb
    move/from16 v0, v28

    #@2cd
    int-to-float v5, v0

    #@2ce
    int-to-float v6, v10

    #@2cf
    move-object/from16 v2, p1

    #@2d1
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@2d4
    .line 14083
    :cond_2d4
    move-object/from16 v0, p1

    #@2d6
    move/from16 v1, v30

    #@2d8
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@2db
    .line 14086
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->onDrawScrollBars(Landroid/graphics/Canvas;)V

    #@2de
    goto/16 :goto_8c

    #@2e0
    .line 14004
    .end local v7           #p:Landroid/graphics/Paint;
    .end local v17           #fade:Landroid/graphics/Shader;
    .end local v24           #matrix:Landroid/graphics/Matrix;
    .end local v30           #saveCount:I
    .end local v34           #solidColor:I
    :cond_2e0
    const/16 v16, 0x0

    #@2e2
    goto/16 :goto_153

    #@2e4
    .line 14006
    :cond_2e4
    const/4 v13, 0x0

    #@2e5
    goto/16 :goto_16b

    #@2e7
    .line 14011
    :cond_2e7
    const/4 v14, 0x0

    #@2e8
    goto/16 :goto_185

    #@2ea
    .line 14013
    :cond_2ea
    const/4 v15, 0x0

    #@2eb
    goto/16 :goto_19d

    #@2ed
    .line 14038
    .restart local v30       #saveCount:I
    .restart local v34       #solidColor:I
    :cond_2ed
    invoke-virtual/range {v33 .. v34}, Landroid/view/View$ScrollabilityCache;->setFadeColor(I)V

    #@2f0
    goto/16 :goto_1f7
.end method

.method draw(Landroid/graphics/Canvas;Landroid/view/ViewGroup;J)Z
    .registers 57
    .parameter "canvas"
    .parameter "parent"
    .parameter "drawingTime"

    #@0
    .prologue
    .line 13543
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    if-eqz v3, :cond_cd

    #@6
    move-object/from16 v0, p0

    #@8
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iget-boolean v3, v3, Landroid/view/View$AttachInfo;->mHardwareAccelerated:Z

    #@c
    if-eqz v3, :cond_cd

    #@e
    const/16 v51, 0x1

    #@10
    .line 13544
    .local v51, useDisplayListProperties:Z
    :goto_10
    const/16 v38, 0x0

    #@12
    .line 13545
    .local v38, more:Z
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->hasIdentityMatrix()Z

    #@15
    move-result v27

    #@16
    .line 13546
    .local v27, childHasIdentityMatrix:Z
    move-object/from16 v0, p2

    #@18
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@1a
    move/from16 v30, v0

    #@1c
    .line 13548
    .local v30, flags:I
    move/from16 v0, v30

    #@1e
    and-int/lit16 v3, v0, 0x100

    #@20
    const/16 v4, 0x100

    #@22
    if-ne v3, v4, :cond_35

    #@24
    .line 13549
    move-object/from16 v0, p2

    #@26
    iget-object v3, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@28
    invoke-virtual {v3}, Landroid/view/animation/Transformation;->clear()V

    #@2b
    .line 13550
    move-object/from16 v0, p2

    #@2d
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@2f
    and-int/lit16 v3, v3, -0x101

    #@31
    move-object/from16 v0, p2

    #@33
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@35
    .line 13553
    :cond_35
    const/16 v49, 0x0

    #@37
    .line 13554
    .local v49, transformToApply:Landroid/view/animation/Transformation;
    const/16 v28, 0x0

    #@39
    .line 13556
    .local v28, concatMatrix:Z
    const/4 v8, 0x0

    #@3a
    .line 13558
    .local v8, scalingRequired:Z
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayerType()I

    #@3d
    move-result v37

    #@3e
    .line 13560
    .local v37, layerType:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@41
    move-result v31

    #@42
    .line 13561
    .local v31, hardwareAccelerated:Z
    const v3, 0x8000

    #@45
    and-int v3, v3, v30

    #@47
    if-nez v3, :cond_4f

    #@49
    move/from16 v0, v30

    #@4b
    and-int/lit16 v3, v0, 0x4000

    #@4d
    if-eqz v3, :cond_d1

    #@4f
    .line 13563
    :cond_4f
    const/16 v26, 0x1

    #@51
    .line 13565
    .local v26, caching:Z
    move-object/from16 v0, p0

    #@53
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@55
    if-eqz v3, :cond_5d

    #@57
    move-object/from16 v0, p0

    #@59
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5b
    iget-boolean v8, v3, Landroid/view/View$AttachInfo;->mScalingRequired:Z

    #@5d
    .line 13570
    :cond_5d
    :goto_5d
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    #@60
    move-result-object v7

    #@61
    .line 13571
    .local v7, a:Landroid/view/animation/Animation;
    if-eqz v7, :cond_db

    #@63
    move-object/from16 v3, p0

    #@65
    move-object/from16 v4, p2

    #@67
    move-wide/from16 v5, p3

    #@69
    .line 13572
    invoke-direct/range {v3 .. v8}, Landroid/view/View;->drawAnimation(Landroid/view/ViewGroup;JLandroid/view/animation/Animation;Z)Z

    #@6c
    move-result v38

    #@6d
    .line 13573
    invoke-virtual {v7}, Landroid/view/animation/Animation;->willChangeTransformationMatrix()Z

    #@70
    move-result v28

    #@71
    .line 13574
    if-eqz v28, :cond_7d

    #@73
    .line 13575
    move-object/from16 v0, p0

    #@75
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@77
    or-int/lit8 v3, v3, 0x1

    #@79
    move-object/from16 v0, p0

    #@7b
    iput v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@7d
    .line 13577
    :cond_7d
    move-object/from16 v0, p2

    #@7f
    iget-object v0, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@81
    move-object/from16 v49, v0

    #@83
    .line 13598
    :cond_83
    :goto_83
    if-nez v27, :cond_136

    #@85
    const/4 v3, 0x1

    #@86
    :goto_86
    or-int v28, v28, v3

    #@88
    .line 13602
    move-object/from16 v0, p0

    #@8a
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@8c
    or-int/lit8 v3, v3, 0x20

    #@8e
    move-object/from16 v0, p0

    #@90
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@92
    .line 13604
    if-nez v28, :cond_139

    #@94
    move/from16 v0, v30

    #@96
    and-int/lit16 v3, v0, 0x801

    #@98
    const/4 v4, 0x1

    #@99
    if-ne v3, v4, :cond_139

    #@9b
    move-object/from16 v0, p0

    #@9d
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@9f
    int-to-float v10, v3

    #@a0
    move-object/from16 v0, p0

    #@a2
    iget v3, v0, Landroid/view/View;->mTop:I

    #@a4
    int-to-float v11, v3

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget v3, v0, Landroid/view/View;->mRight:I

    #@a9
    int-to-float v12, v3

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget v3, v0, Landroid/view/View;->mBottom:I

    #@ae
    int-to-float v13, v3

    #@af
    sget-object v14, Landroid/graphics/Canvas$EdgeType;->BW:Landroid/graphics/Canvas$EdgeType;

    #@b1
    move-object/from16 v9, p1

    #@b3
    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->quickReject(FFFFLandroid/graphics/Canvas$EdgeType;)Z

    #@b6
    move-result v3

    #@b7
    if-eqz v3, :cond_139

    #@b9
    move-object/from16 v0, p0

    #@bb
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@bd
    and-int/lit8 v3, v3, 0x40

    #@bf
    if-nez v3, :cond_139

    #@c1
    .line 13609
    move-object/from16 v0, p0

    #@c3
    iget v3, v0, Landroid/view/View;->mPrivateFlags2:I

    #@c5
    const/high16 v4, 0x1000

    #@c7
    or-int/2addr v3, v4

    #@c8
    move-object/from16 v0, p0

    #@ca
    iput v3, v0, Landroid/view/View;->mPrivateFlags2:I

    #@cc
    .line 13880
    :goto_cc
    return v38

    #@cd
    .line 13543
    .end local v7           #a:Landroid/view/animation/Animation;
    .end local v8           #scalingRequired:Z
    .end local v26           #caching:Z
    .end local v27           #childHasIdentityMatrix:Z
    .end local v28           #concatMatrix:Z
    .end local v30           #flags:I
    .end local v31           #hardwareAccelerated:Z
    .end local v37           #layerType:I
    .end local v38           #more:Z
    .end local v49           #transformToApply:Landroid/view/animation/Transformation;
    .end local v51           #useDisplayListProperties:Z
    :cond_cd
    const/16 v51, 0x0

    #@cf
    goto/16 :goto_10

    #@d1
    .line 13567
    .restart local v8       #scalingRequired:Z
    .restart local v27       #childHasIdentityMatrix:Z
    .restart local v28       #concatMatrix:Z
    .restart local v30       #flags:I
    .restart local v31       #hardwareAccelerated:Z
    .restart local v37       #layerType:I
    .restart local v38       #more:Z
    .restart local v49       #transformToApply:Landroid/view/animation/Transformation;
    .restart local v51       #useDisplayListProperties:Z
    :cond_d1
    if-nez v37, :cond_d5

    #@d3
    if-eqz v31, :cond_d8

    #@d5
    :cond_d5
    const/16 v26, 0x1

    #@d7
    .restart local v26       #caching:Z
    :goto_d7
    goto :goto_5d

    #@d8
    .end local v26           #caching:Z
    :cond_d8
    const/16 v26, 0x0

    #@da
    goto :goto_d7

    #@db
    .line 13579
    .restart local v7       #a:Landroid/view/animation/Animation;
    .restart local v26       #caching:Z
    :cond_db
    move-object/from16 v0, p0

    #@dd
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@df
    and-int/lit8 v3, v3, 0x1

    #@e1
    const/4 v4, 0x1

    #@e2
    if-ne v3, v4, :cond_fc

    #@e4
    move-object/from16 v0, p0

    #@e6
    iget-object v3, v0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@e8
    if-eqz v3, :cond_fc

    #@ea
    .line 13582
    move-object/from16 v0, p0

    #@ec
    iget-object v3, v0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@ee
    const/4 v4, 0x0

    #@ef
    invoke-virtual {v3, v4}, Landroid/view/DisplayList;->setAnimationMatrix(Landroid/graphics/Matrix;)V

    #@f2
    .line 13583
    move-object/from16 v0, p0

    #@f4
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@f6
    and-int/lit8 v3, v3, -0x2

    #@f8
    move-object/from16 v0, p0

    #@fa
    iput v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@fc
    .line 13585
    :cond_fc
    if-nez v51, :cond_83

    #@fe
    move/from16 v0, v30

    #@100
    and-int/lit16 v3, v0, 0x800

    #@102
    if-eqz v3, :cond_83

    #@104
    .line 13587
    move-object/from16 v0, p2

    #@106
    iget-object v3, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@108
    move-object/from16 v0, p2

    #@10a
    move-object/from16 v1, p0

    #@10c
    invoke-virtual {v0, v1, v3}, Landroid/view/ViewGroup;->getChildStaticTransformation(Landroid/view/View;Landroid/view/animation/Transformation;)Z

    #@10f
    move-result v34

    #@110
    .line 13589
    .local v34, hasTransform:Z
    if-eqz v34, :cond_83

    #@112
    .line 13590
    move-object/from16 v0, p2

    #@114
    iget-object v3, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@116
    invoke-virtual {v3}, Landroid/view/animation/Transformation;->getTransformationType()I

    #@119
    move-result v50

    #@11a
    .line 13591
    .local v50, transformType:I
    sget v3, Landroid/view/animation/Transformation;->TYPE_IDENTITY:I

    #@11c
    move/from16 v0, v50

    #@11e
    if-eq v0, v3, :cond_130

    #@120
    move-object/from16 v0, p2

    #@122
    iget-object v0, v0, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@124
    move-object/from16 v49, v0

    #@126
    .line 13593
    :goto_126
    sget v3, Landroid/view/animation/Transformation;->TYPE_MATRIX:I

    #@128
    and-int v3, v3, v50

    #@12a
    if-eqz v3, :cond_133

    #@12c
    const/16 v28, 0x1

    #@12e
    :goto_12e
    goto/16 :goto_83

    #@130
    .line 13591
    :cond_130
    const/16 v49, 0x0

    #@132
    goto :goto_126

    #@133
    .line 13593
    :cond_133
    const/16 v28, 0x0

    #@135
    goto :goto_12e

    #@136
    .line 13598
    .end local v34           #hasTransform:Z
    .end local v50           #transformType:I
    :cond_136
    const/4 v3, 0x0

    #@137
    goto/16 :goto_86

    #@139
    .line 13612
    :cond_139
    move-object/from16 v0, p0

    #@13b
    iget v3, v0, Landroid/view/View;->mPrivateFlags2:I

    #@13d
    const v4, -0x10000001

    #@140
    and-int/2addr v3, v4

    #@141
    move-object/from16 v0, p0

    #@143
    iput v3, v0, Landroid/view/View;->mPrivateFlags2:I

    #@145
    .line 13614
    if-eqz v31, :cond_163

    #@147
    .line 13617
    move-object/from16 v0, p0

    #@149
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@14b
    const/high16 v4, -0x8000

    #@14d
    and-int/2addr v3, v4

    #@14e
    const/high16 v4, -0x8000

    #@150
    if-ne v3, v4, :cond_37e

    #@152
    const/4 v3, 0x1

    #@153
    :goto_153
    move-object/from16 v0, p0

    #@155
    iput-boolean v3, v0, Landroid/view/View;->mRecreateDisplayList:Z

    #@157
    .line 13618
    move-object/from16 v0, p0

    #@159
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@15b
    const v4, 0x7fffffff

    #@15e
    and-int/2addr v3, v4

    #@15f
    move-object/from16 v0, p0

    #@161
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@163
    .line 13621
    :cond_163
    const/16 v29, 0x0

    #@165
    .line 13622
    .local v29, displayList:Landroid/view/DisplayList;
    const/16 v24, 0x0

    #@167
    .line 13623
    .local v24, cache:Landroid/graphics/Bitmap;
    const/16 v32, 0x0

    #@169
    .line 13624
    .local v32, hasDisplayList:Z
    if-eqz v26, :cond_17e

    #@16b
    .line 13625
    if-nez v31, :cond_381

    #@16d
    .line 13626
    if-eqz v37, :cond_177

    #@16f
    .line 13627
    const/16 v37, 0x1

    #@171
    .line 13628
    const/4 v3, 0x1

    #@172
    move-object/from16 v0, p0

    #@174
    invoke-virtual {v0, v3}, Landroid/view/View;->buildDrawingCache(Z)V

    #@177
    .line 13630
    :cond_177
    const/4 v3, 0x1

    #@178
    move-object/from16 v0, p0

    #@17a
    invoke-virtual {v0, v3}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    #@17d
    move-result-object v24

    #@17e
    .line 13654
    :cond_17e
    :goto_17e
    and-int v51, v51, v32

    #@180
    .line 13655
    if-eqz v51, :cond_192

    #@182
    .line 13656
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getDisplayList()Landroid/view/DisplayList;

    #@185
    move-result-object v29

    #@186
    .line 13657
    invoke-virtual/range {v29 .. v29}, Landroid/view/DisplayList;->isValid()Z

    #@189
    move-result v3

    #@18a
    if-nez v3, :cond_192

    #@18c
    .line 13661
    const/16 v29, 0x0

    #@18e
    .line 13662
    const/16 v32, 0x0

    #@190
    .line 13663
    const/16 v51, 0x0

    #@192
    .line 13667
    :cond_192
    const/16 v44, 0x0

    #@194
    .line 13668
    .local v44, sx:I
    const/16 v45, 0x0

    #@196
    .line 13669
    .local v45, sy:I
    if-nez v32, :cond_1a7

    #@198
    .line 13670
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeScroll()V

    #@19b
    .line 13671
    move-object/from16 v0, p0

    #@19d
    iget v0, v0, Landroid/view/View;->mScrollX:I

    #@19f
    move/from16 v44, v0

    #@1a1
    .line 13672
    move-object/from16 v0, p0

    #@1a3
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@1a5
    move/from16 v45, v0

    #@1a7
    .line 13675
    :cond_1a7
    if-eqz v24, :cond_1ab

    #@1a9
    if-eqz v32, :cond_3ab

    #@1ab
    :cond_1ab
    const/16 v33, 0x1

    #@1ad
    .line 13676
    .local v33, hasNoCache:Z
    :goto_1ad
    if-nez v24, :cond_3af

    #@1af
    if-nez v32, :cond_3af

    #@1b1
    const/4 v3, 0x2

    #@1b2
    move/from16 v0, v37

    #@1b4
    if-eq v0, v3, :cond_3af

    #@1b6
    const/16 v39, 0x1

    #@1b8
    .line 13679
    .local v39, offsetForScroll:Z
    :goto_1b8
    const/16 v40, -0x1

    #@1ba
    .line 13680
    .local v40, restoreTo:I
    if-eqz v51, :cond_1be

    #@1bc
    if-eqz v49, :cond_1c2

    #@1be
    .line 13681
    :cond_1be
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@1c1
    move-result v40

    #@1c2
    .line 13683
    :cond_1c2
    if-eqz v39, :cond_3b3

    #@1c4
    .line 13684
    move-object/from16 v0, p0

    #@1c6
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@1c8
    sub-int v3, v3, v44

    #@1ca
    int-to-float v3, v3

    #@1cb
    move-object/from16 v0, p0

    #@1cd
    iget v4, v0, Landroid/view/View;->mTop:I

    #@1cf
    sub-int v4, v4, v45

    #@1d1
    int-to-float v4, v4

    #@1d2
    move-object/from16 v0, p1

    #@1d4
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@1d7
    .line 13700
    :cond_1d7
    :goto_1d7
    if-eqz v51, :cond_3e1

    #@1d9
    const/high16 v23, 0x3f80

    #@1db
    .line 13701
    .local v23, alpha:F
    :goto_1db
    if-nez v49, :cond_1f2

    #@1dd
    const/high16 v3, 0x3f80

    #@1df
    cmpg-float v3, v23, v3

    #@1e1
    if-ltz v3, :cond_1f2

    #@1e3
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->hasIdentityMatrix()Z

    #@1e6
    move-result v3

    #@1e7
    if-eqz v3, :cond_1f2

    #@1e9
    move-object/from16 v0, p0

    #@1eb
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@1ed
    and-int/lit8 v3, v3, 0x2

    #@1ef
    const/4 v4, 0x2

    #@1f0
    if-ne v3, v4, :cond_458

    #@1f2
    .line 13703
    :cond_1f2
    if-nez v49, :cond_1f6

    #@1f4
    if-nez v27, :cond_25a

    #@1f6
    .line 13704
    :cond_1f6
    const/16 v46, 0x0

    #@1f8
    .line 13705
    .local v46, transX:I
    const/16 v47, 0x0

    #@1fa
    .line 13707
    .local v47, transY:I
    if-eqz v39, :cond_206

    #@1fc
    .line 13708
    move/from16 v0, v44

    #@1fe
    neg-int v0, v0

    #@1ff
    move/from16 v46, v0

    #@201
    .line 13709
    move/from16 v0, v45

    #@203
    neg-int v0, v0

    #@204
    move/from16 v47, v0

    #@206
    .line 13712
    :cond_206
    if-eqz v49, :cond_235

    #@208
    .line 13713
    if-eqz v28, :cond_21f

    #@20a
    .line 13714
    if-eqz v51, :cond_3e7

    #@20c
    .line 13715
    invoke-virtual/range {v49 .. v49}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@20f
    move-result-object v3

    #@210
    move-object/from16 v0, v29

    #@212
    invoke-virtual {v0, v3}, Landroid/view/DisplayList;->setAnimationMatrix(Landroid/graphics/Matrix;)V

    #@215
    .line 13723
    :goto_215
    move-object/from16 v0, p2

    #@217
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@219
    or-int/lit16 v3, v3, 0x100

    #@21b
    move-object/from16 v0, p2

    #@21d
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@21f
    .line 13726
    :cond_21f
    invoke-virtual/range {v49 .. v49}, Landroid/view/animation/Transformation;->getAlpha()F

    #@222
    move-result v48

    #@223
    .line 13727
    .local v48, transformAlpha:F
    const/high16 v3, 0x3f80

    #@225
    cmpg-float v3, v48, v3

    #@227
    if-gez v3, :cond_235

    #@229
    .line 13728
    mul-float v23, v23, v48

    #@22b
    .line 13729
    move-object/from16 v0, p2

    #@22d
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@22f
    or-int/lit16 v3, v3, 0x100

    #@231
    move-object/from16 v0, p2

    #@233
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@235
    .line 13733
    .end local v48           #transformAlpha:F
    :cond_235
    if-nez v27, :cond_25a

    #@237
    if-nez v51, :cond_25a

    #@239
    .line 13734
    move/from16 v0, v46

    #@23b
    neg-int v3, v0

    #@23c
    int-to-float v3, v3

    #@23d
    move/from16 v0, v47

    #@23f
    neg-int v4, v0

    #@240
    int-to-float v4, v4

    #@241
    move-object/from16 v0, p1

    #@243
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@246
    .line 13735
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@249
    move-result-object v3

    #@24a
    move-object/from16 v0, p1

    #@24c
    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    #@24f
    .line 13736
    move/from16 v0, v46

    #@251
    int-to-float v3, v0

    #@252
    move/from16 v0, v47

    #@254
    int-to-float v4, v0

    #@255
    move-object/from16 v0, p1

    #@257
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@25a
    .line 13741
    .end local v46           #transX:I
    .end local v47           #transY:I
    :cond_25a
    const/high16 v3, 0x3f80

    #@25c
    cmpg-float v3, v23, v3

    #@25e
    if-ltz v3, :cond_269

    #@260
    move-object/from16 v0, p0

    #@262
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@264
    and-int/lit8 v3, v3, 0x2

    #@266
    const/4 v4, 0x2

    #@267
    if-ne v3, v4, :cond_2a8

    #@269
    .line 13743
    :cond_269
    const/high16 v3, 0x3f80

    #@26b
    cmpg-float v3, v23, v3

    #@26d
    if-gez v3, :cond_40a

    #@26f
    .line 13744
    move-object/from16 v0, p0

    #@271
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@273
    or-int/lit8 v3, v3, 0x2

    #@275
    move-object/from16 v0, p0

    #@277
    iput v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@279
    .line 13748
    :goto_279
    move-object/from16 v0, p2

    #@27b
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@27d
    or-int/lit16 v3, v3, 0x100

    #@27f
    move-object/from16 v0, p2

    #@281
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@283
    .line 13749
    if-eqz v33, :cond_2a8

    #@285
    .line 13750
    const/high16 v3, 0x437f

    #@287
    mul-float v3, v3, v23

    #@289
    float-to-int v14, v3

    #@28a
    .line 13751
    .local v14, multipliedAlpha:I
    move-object/from16 v0, p0

    #@28c
    invoke-virtual {v0, v14}, Landroid/view/View;->onSetAlpha(I)Z

    #@28f
    move-result v3

    #@290
    if-nez v3, :cond_44b

    #@292
    .line 13752
    const/4 v15, 0x4

    #@293
    .line 13753
    .local v15, layerFlags:I
    and-int/lit8 v3, v30, 0x1

    #@295
    if-nez v3, :cond_299

    #@297
    if-eqz v37, :cond_29b

    #@299
    .line 13755
    :cond_299
    or-int/lit8 v15, v15, 0x10

    #@29b
    .line 13757
    :cond_29b
    if-eqz v51, :cond_416

    #@29d
    .line 13758
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getAlpha()F

    #@2a0
    move-result v3

    #@2a1
    mul-float v3, v3, v23

    #@2a3
    move-object/from16 v0, v29

    #@2a5
    invoke-virtual {v0, v3}, Landroid/view/DisplayList;->setAlpha(F)V

    #@2a8
    .line 13776
    .end local v14           #multipliedAlpha:I
    .end local v15           #layerFlags:I
    :cond_2a8
    :goto_2a8
    and-int/lit8 v3, v30, 0x1

    #@2aa
    const/4 v4, 0x1

    #@2ab
    if-ne v3, v4, :cond_2d0

    #@2ad
    if-nez v51, :cond_2d0

    #@2af
    .line 13778
    if-eqz v39, :cond_478

    #@2b1
    .line 13779
    move-object/from16 v0, p0

    #@2b3
    iget v3, v0, Landroid/view/View;->mRight:I

    #@2b5
    move-object/from16 v0, p0

    #@2b7
    iget v4, v0, Landroid/view/View;->mLeft:I

    #@2b9
    sub-int/2addr v3, v4

    #@2ba
    add-int v3, v3, v44

    #@2bc
    move-object/from16 v0, p0

    #@2be
    iget v4, v0, Landroid/view/View;->mBottom:I

    #@2c0
    move-object/from16 v0, p0

    #@2c2
    iget v5, v0, Landroid/view/View;->mTop:I

    #@2c4
    sub-int/2addr v4, v5

    #@2c5
    add-int v4, v4, v45

    #@2c7
    move-object/from16 v0, p1

    #@2c9
    move/from16 v1, v44

    #@2cb
    move/from16 v2, v45

    #@2cd
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@2d0
    .line 13789
    :cond_2d0
    :goto_2d0
    if-nez v51, :cond_2e2

    #@2d2
    if-eqz v32, :cond_2e2

    #@2d4
    .line 13790
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getDisplayList()Landroid/view/DisplayList;

    #@2d7
    move-result-object v29

    #@2d8
    .line 13791
    invoke-virtual/range {v29 .. v29}, Landroid/view/DisplayList;->isValid()Z

    #@2db
    move-result v3

    #@2dc
    if-nez v3, :cond_2e2

    #@2de
    .line 13795
    const/16 v29, 0x0

    #@2e0
    .line 13796
    const/16 v32, 0x0

    #@2e2
    .line 13800
    :cond_2e2
    if-eqz v33, :cond_50a

    #@2e4
    .line 13801
    const/16 v36, 0x0

    #@2e6
    .line 13802
    .local v36, layerRendered:Z
    const/4 v3, 0x2

    #@2e7
    move/from16 v0, v37

    #@2e9
    if-ne v0, v3, :cond_316

    #@2eb
    if-nez v51, :cond_316

    #@2ed
    .line 13803
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHardwareLayer()Landroid/view/HardwareLayer;

    #@2f0
    move-result-object v35

    #@2f1
    .line 13804
    .local v35, layer:Landroid/view/HardwareLayer;
    if-eqz v35, :cond_4a8

    #@2f3
    invoke-virtual/range {v35 .. v35}, Landroid/view/HardwareLayer;->isValid()Z

    #@2f6
    move-result v3

    #@2f7
    if-eqz v3, :cond_4a8

    #@2f9
    .line 13805
    move-object/from16 v0, p0

    #@2fb
    iget-object v3, v0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@2fd
    const/high16 v4, 0x437f

    #@2ff
    mul-float v4, v4, v23

    #@301
    float-to-int v4, v4

    #@302
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    #@305
    move-object/from16 v3, p1

    #@307
    .line 13806
    check-cast v3, Landroid/view/HardwareCanvas;

    #@309
    const/4 v4, 0x0

    #@30a
    const/4 v5, 0x0

    #@30b
    move-object/from16 v0, p0

    #@30d
    iget-object v6, v0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@30f
    move-object/from16 v0, v35

    #@311
    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/view/HardwareCanvas;->drawHardwareLayer(Landroid/view/HardwareLayer;FFLandroid/graphics/Paint;)V

    #@314
    .line 13807
    const/16 v36, 0x1

    #@316
    .line 13817
    .end local v35           #layer:Landroid/view/HardwareLayer;
    :cond_316
    :goto_316
    if-nez v36, :cond_333

    #@318
    .line 13818
    if-nez v32, :cond_4f0

    #@31a
    .line 13820
    move-object/from16 v0, p0

    #@31c
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@31e
    and-int/lit16 v3, v3, 0x80

    #@320
    const/16 v4, 0x80

    #@322
    if-ne v3, v4, :cond_4eb

    #@324
    .line 13821
    move-object/from16 v0, p0

    #@326
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@328
    const v4, -0x600001

    #@32b
    and-int/2addr v3, v4

    #@32c
    move-object/from16 v0, p0

    #@32e
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@330
    .line 13822
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@333
    .line 13856
    .end local v36           #layerRendered:Z
    :cond_333
    :goto_333
    if-ltz v40, :cond_33c

    #@335
    .line 13857
    move-object/from16 v0, p1

    #@337
    move/from16 v1, v40

    #@339
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@33c
    .line 13860
    :cond_33c
    if-eqz v7, :cond_356

    #@33e
    if-nez v38, :cond_356

    #@340
    .line 13861
    if-nez v31, :cond_34f

    #@342
    invoke-virtual {v7}, Landroid/view/animation/Animation;->getFillAfter()Z

    #@345
    move-result v3

    #@346
    if-nez v3, :cond_34f

    #@348
    .line 13862
    const/16 v3, 0xff

    #@34a
    move-object/from16 v0, p0

    #@34c
    invoke-virtual {v0, v3}, Landroid/view/View;->onSetAlpha(I)Z

    #@34f
    .line 13864
    :cond_34f
    move-object/from16 v0, p2

    #@351
    move-object/from16 v1, p0

    #@353
    invoke-virtual {v0, v1, v7}, Landroid/view/ViewGroup;->finishAnimatingView(Landroid/view/View;Landroid/view/animation/Animation;)V

    #@356
    .line 13867
    :cond_356
    if-eqz v38, :cond_377

    #@358
    if-eqz v31, :cond_377

    #@35a
    .line 13871
    const/4 v3, 0x1

    #@35b
    move-object/from16 v0, p2

    #@35d
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->invalidate(Z)V

    #@360
    .line 13872
    invoke-virtual {v7}, Landroid/view/animation/Animation;->hasAlpha()Z

    #@363
    move-result v3

    #@364
    if-eqz v3, :cond_377

    #@366
    move-object/from16 v0, p0

    #@368
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@36a
    const/high16 v4, 0x4

    #@36c
    and-int/2addr v3, v4

    #@36d
    const/high16 v4, 0x4

    #@36f
    if-ne v3, v4, :cond_377

    #@371
    .line 13874
    const/4 v3, 0x1

    #@372
    move-object/from16 v0, p0

    #@374
    invoke-virtual {v0, v3}, Landroid/view/View;->invalidate(Z)V

    #@377
    .line 13878
    :cond_377
    const/4 v3, 0x0

    #@378
    move-object/from16 v0, p0

    #@37a
    iput-boolean v3, v0, Landroid/view/View;->mRecreateDisplayList:Z

    #@37c
    goto/16 :goto_cc

    #@37e
    .line 13617
    .end local v23           #alpha:F
    .end local v24           #cache:Landroid/graphics/Bitmap;
    .end local v29           #displayList:Landroid/view/DisplayList;
    .end local v32           #hasDisplayList:Z
    .end local v33           #hasNoCache:Z
    .end local v39           #offsetForScroll:Z
    .end local v40           #restoreTo:I
    .end local v44           #sx:I
    .end local v45           #sy:I
    :cond_37e
    const/4 v3, 0x0

    #@37f
    goto/16 :goto_153

    #@381
    .line 13632
    .restart local v24       #cache:Landroid/graphics/Bitmap;
    .restart local v29       #displayList:Landroid/view/DisplayList;
    .restart local v32       #hasDisplayList:Z
    :cond_381
    packed-switch v37, :pswitch_data_584

    #@384
    goto/16 :goto_17e

    #@386
    .line 13649
    :pswitch_386
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->canHaveDisplayList()Z

    #@389
    move-result v32

    #@38a
    goto/16 :goto_17e

    #@38c
    .line 13634
    :pswitch_38c
    if-eqz v51, :cond_394

    #@38e
    .line 13635
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->canHaveDisplayList()Z

    #@391
    move-result v32

    #@392
    goto/16 :goto_17e

    #@394
    .line 13637
    :cond_394
    const/4 v3, 0x1

    #@395
    move-object/from16 v0, p0

    #@397
    invoke-virtual {v0, v3}, Landroid/view/View;->buildDrawingCache(Z)V

    #@39a
    .line 13638
    const/4 v3, 0x1

    #@39b
    move-object/from16 v0, p0

    #@39d
    invoke-virtual {v0, v3}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    #@3a0
    move-result-object v24

    #@3a1
    .line 13640
    goto/16 :goto_17e

    #@3a3
    .line 13642
    :pswitch_3a3
    if-eqz v51, :cond_17e

    #@3a5
    .line 13643
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->canHaveDisplayList()Z

    #@3a8
    move-result v32

    #@3a9
    goto/16 :goto_17e

    #@3ab
    .line 13675
    .restart local v44       #sx:I
    .restart local v45       #sy:I
    :cond_3ab
    const/16 v33, 0x0

    #@3ad
    goto/16 :goto_1ad

    #@3af
    .line 13676
    .restart local v33       #hasNoCache:Z
    :cond_3af
    const/16 v39, 0x0

    #@3b1
    goto/16 :goto_1b8

    #@3b3
    .line 13686
    .restart local v39       #offsetForScroll:Z
    .restart local v40       #restoreTo:I
    :cond_3b3
    if-nez v51, :cond_3c4

    #@3b5
    .line 13687
    move-object/from16 v0, p0

    #@3b7
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@3b9
    int-to-float v3, v3

    #@3ba
    move-object/from16 v0, p0

    #@3bc
    iget v4, v0, Landroid/view/View;->mTop:I

    #@3be
    int-to-float v4, v4

    #@3bf
    move-object/from16 v0, p1

    #@3c1
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@3c4
    .line 13689
    :cond_3c4
    if-eqz v8, :cond_1d7

    #@3c6
    .line 13690
    if-eqz v51, :cond_3cc

    #@3c8
    .line 13692
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    #@3cb
    move-result v40

    #@3cc
    .line 13695
    :cond_3cc
    const/high16 v3, 0x3f80

    #@3ce
    move-object/from16 v0, p0

    #@3d0
    iget-object v4, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3d2
    iget v4, v4, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@3d4
    div-float v41, v3, v4

    #@3d6
    .line 13696
    .local v41, scale:F
    move-object/from16 v0, p1

    #@3d8
    move/from16 v1, v41

    #@3da
    move/from16 v2, v41

    #@3dc
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    #@3df
    goto/16 :goto_1d7

    #@3e1
    .line 13700
    .end local v41           #scale:F
    :cond_3e1
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getAlpha()F

    #@3e4
    move-result v23

    #@3e5
    goto/16 :goto_1db

    #@3e7
    .line 13719
    .restart local v23       #alpha:F
    .restart local v46       #transX:I
    .restart local v47       #transY:I
    :cond_3e7
    move/from16 v0, v46

    #@3e9
    neg-int v3, v0

    #@3ea
    int-to-float v3, v3

    #@3eb
    move/from16 v0, v47

    #@3ed
    neg-int v4, v0

    #@3ee
    int-to-float v4, v4

    #@3ef
    move-object/from16 v0, p1

    #@3f1
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@3f4
    .line 13720
    invoke-virtual/range {v49 .. v49}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@3f7
    move-result-object v3

    #@3f8
    move-object/from16 v0, p1

    #@3fa
    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    #@3fd
    .line 13721
    move/from16 v0, v46

    #@3ff
    int-to-float v3, v0

    #@400
    move/from16 v0, v47

    #@402
    int-to-float v4, v0

    #@403
    move-object/from16 v0, p1

    #@405
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@408
    goto/16 :goto_215

    #@40a
    .line 13746
    .end local v46           #transX:I
    .end local v47           #transY:I
    :cond_40a
    move-object/from16 v0, p0

    #@40c
    iget v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@40e
    and-int/lit8 v3, v3, -0x3

    #@410
    move-object/from16 v0, p0

    #@412
    iput v3, v0, Landroid/view/View;->mPrivateFlags3:I

    #@414
    goto/16 :goto_279

    #@416
    .line 13759
    .restart local v14       #multipliedAlpha:I
    .restart local v15       #layerFlags:I
    :cond_416
    if-nez v37, :cond_2a8

    #@418
    .line 13760
    if-eqz v32, :cond_445

    #@41a
    const/16 v42, 0x0

    #@41c
    .line 13761
    .local v42, scrollX:I
    :goto_41c
    if-eqz v32, :cond_448

    #@41e
    const/16 v43, 0x0

    #@420
    .line 13762
    .local v43, scrollY:I
    :goto_420
    move/from16 v0, v42

    #@422
    int-to-float v10, v0

    #@423
    move/from16 v0, v43

    #@425
    int-to-float v11, v0

    #@426
    move-object/from16 v0, p0

    #@428
    iget v3, v0, Landroid/view/View;->mRight:I

    #@42a
    add-int v3, v3, v42

    #@42c
    move-object/from16 v0, p0

    #@42e
    iget v4, v0, Landroid/view/View;->mLeft:I

    #@430
    sub-int/2addr v3, v4

    #@431
    int-to-float v12, v3

    #@432
    move-object/from16 v0, p0

    #@434
    iget v3, v0, Landroid/view/View;->mBottom:I

    #@436
    add-int v3, v3, v43

    #@438
    move-object/from16 v0, p0

    #@43a
    iget v4, v0, Landroid/view/View;->mTop:I

    #@43c
    sub-int/2addr v3, v4

    #@43d
    int-to-float v13, v3

    #@43e
    move-object/from16 v9, p1

    #@440
    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Canvas;->saveLayerAlpha(FFFFII)I

    #@443
    goto/16 :goto_2a8

    #@445
    .end local v42           #scrollX:I
    .end local v43           #scrollY:I
    :cond_445
    move/from16 v42, v44

    #@447
    .line 13760
    goto :goto_41c

    #@448
    .restart local v42       #scrollX:I
    :cond_448
    move/from16 v43, v45

    #@44a
    .line 13761
    goto :goto_420

    #@44b
    .line 13767
    .end local v15           #layerFlags:I
    .end local v42           #scrollX:I
    :cond_44b
    move-object/from16 v0, p0

    #@44d
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@44f
    const/high16 v4, 0x4

    #@451
    or-int/2addr v3, v4

    #@452
    move-object/from16 v0, p0

    #@454
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@456
    goto/16 :goto_2a8

    #@458
    .line 13771
    .end local v14           #multipliedAlpha:I
    :cond_458
    move-object/from16 v0, p0

    #@45a
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@45c
    const/high16 v4, 0x4

    #@45e
    and-int/2addr v3, v4

    #@45f
    const/high16 v4, 0x4

    #@461
    if-ne v3, v4, :cond_2a8

    #@463
    .line 13772
    const/16 v3, 0xff

    #@465
    move-object/from16 v0, p0

    #@467
    invoke-virtual {v0, v3}, Landroid/view/View;->onSetAlpha(I)Z

    #@46a
    .line 13773
    move-object/from16 v0, p0

    #@46c
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@46e
    const v4, -0x40001

    #@471
    and-int/2addr v3, v4

    #@472
    move-object/from16 v0, p0

    #@474
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@476
    goto/16 :goto_2a8

    #@478
    .line 13781
    :cond_478
    if-eqz v8, :cond_47c

    #@47a
    if-nez v24, :cond_497

    #@47c
    .line 13782
    :cond_47c
    const/4 v3, 0x0

    #@47d
    const/4 v4, 0x0

    #@47e
    move-object/from16 v0, p0

    #@480
    iget v5, v0, Landroid/view/View;->mRight:I

    #@482
    move-object/from16 v0, p0

    #@484
    iget v6, v0, Landroid/view/View;->mLeft:I

    #@486
    sub-int/2addr v5, v6

    #@487
    move-object/from16 v0, p0

    #@489
    iget v6, v0, Landroid/view/View;->mBottom:I

    #@48b
    move-object/from16 v0, p0

    #@48d
    iget v9, v0, Landroid/view/View;->mTop:I

    #@48f
    sub-int/2addr v6, v9

    #@490
    move-object/from16 v0, p1

    #@492
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@495
    goto/16 :goto_2d0

    #@497
    .line 13784
    :cond_497
    const/4 v3, 0x0

    #@498
    const/4 v4, 0x0

    #@499
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    #@49c
    move-result v5

    #@49d
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    #@4a0
    move-result v6

    #@4a1
    move-object/from16 v0, p1

    #@4a3
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    #@4a6
    goto/16 :goto_2d0

    #@4a8
    .line 13809
    .restart local v35       #layer:Landroid/view/HardwareLayer;
    .restart local v36       #layerRendered:Z
    :cond_4a8
    if-eqz v32, :cond_4e5

    #@4aa
    const/16 v42, 0x0

    #@4ac
    .line 13810
    .restart local v42       #scrollX:I
    :goto_4ac
    if-eqz v32, :cond_4e8

    #@4ae
    const/16 v43, 0x0

    #@4b0
    .line 13811
    .restart local v43       #scrollY:I
    :goto_4b0
    move/from16 v0, v42

    #@4b2
    int-to-float v0, v0

    #@4b3
    move/from16 v17, v0

    #@4b5
    move/from16 v0, v43

    #@4b7
    int-to-float v0, v0

    #@4b8
    move/from16 v18, v0

    #@4ba
    move-object/from16 v0, p0

    #@4bc
    iget v3, v0, Landroid/view/View;->mRight:I

    #@4be
    add-int v3, v3, v42

    #@4c0
    move-object/from16 v0, p0

    #@4c2
    iget v4, v0, Landroid/view/View;->mLeft:I

    #@4c4
    sub-int/2addr v3, v4

    #@4c5
    int-to-float v0, v3

    #@4c6
    move/from16 v19, v0

    #@4c8
    move-object/from16 v0, p0

    #@4ca
    iget v3, v0, Landroid/view/View;->mBottom:I

    #@4cc
    add-int v3, v3, v43

    #@4ce
    move-object/from16 v0, p0

    #@4d0
    iget v4, v0, Landroid/view/View;->mTop:I

    #@4d2
    sub-int/2addr v3, v4

    #@4d3
    int-to-float v0, v3

    #@4d4
    move/from16 v20, v0

    #@4d6
    move-object/from16 v0, p0

    #@4d8
    iget-object v0, v0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@4da
    move-object/from16 v21, v0

    #@4dc
    const/16 v22, 0x14

    #@4de
    move-object/from16 v16, p1

    #@4e0
    invoke-virtual/range {v16 .. v22}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@4e3
    goto/16 :goto_316

    #@4e5
    .end local v42           #scrollX:I
    .end local v43           #scrollY:I
    :cond_4e5
    move/from16 v42, v44

    #@4e7
    .line 13809
    goto :goto_4ac

    #@4e8
    .restart local v42       #scrollX:I
    :cond_4e8
    move/from16 v43, v45

    #@4ea
    .line 13810
    goto :goto_4b0

    #@4eb
    .line 13824
    .end local v35           #layer:Landroid/view/HardwareLayer;
    .end local v42           #scrollX:I
    :cond_4eb
    invoke-virtual/range {p0 .. p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    #@4ee
    goto/16 :goto_333

    #@4f0
    .line 13827
    :cond_4f0
    move-object/from16 v0, p0

    #@4f2
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@4f4
    const v4, -0x600001

    #@4f7
    and-int/2addr v3, v4

    #@4f8
    move-object/from16 v0, p0

    #@4fa
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@4fc
    move-object/from16 v3, p1

    #@4fe
    .line 13828
    check-cast v3, Landroid/view/HardwareCanvas;

    #@500
    const/4 v4, 0x0

    #@501
    move-object/from16 v0, v29

    #@503
    move/from16 v1, v30

    #@505
    invoke-virtual {v3, v0, v4, v1}, Landroid/view/HardwareCanvas;->drawDisplayList(Landroid/view/DisplayList;Landroid/graphics/Rect;I)I

    #@508
    goto/16 :goto_333

    #@50a
    .line 13831
    .end local v36           #layerRendered:Z
    :cond_50a
    if-eqz v24, :cond_333

    #@50c
    .line 13832
    move-object/from16 v0, p0

    #@50e
    iget v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@510
    const v4, -0x600001

    #@513
    and-int/2addr v3, v4

    #@514
    move-object/from16 v0, p0

    #@516
    iput v3, v0, Landroid/view/View;->mPrivateFlags:I

    #@518
    .line 13835
    if-nez v37, :cond_572

    #@51a
    .line 13836
    move-object/from16 v0, p2

    #@51c
    iget-object v0, v0, Landroid/view/ViewGroup;->mCachePaint:Landroid/graphics/Paint;

    #@51e
    move-object/from16 v25, v0

    #@520
    .line 13837
    .local v25, cachePaint:Landroid/graphics/Paint;
    if-nez v25, :cond_533

    #@522
    .line 13838
    new-instance v25, Landroid/graphics/Paint;

    #@524
    .end local v25           #cachePaint:Landroid/graphics/Paint;
    invoke-direct/range {v25 .. v25}, Landroid/graphics/Paint;-><init>()V

    #@527
    .line 13839
    .restart local v25       #cachePaint:Landroid/graphics/Paint;
    const/4 v3, 0x0

    #@528
    move-object/from16 v0, v25

    #@52a
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setDither(Z)V

    #@52d
    .line 13840
    move-object/from16 v0, v25

    #@52f
    move-object/from16 v1, p2

    #@531
    iput-object v0, v1, Landroid/view/ViewGroup;->mCachePaint:Landroid/graphics/Paint;

    #@533
    .line 13842
    :cond_533
    const/high16 v3, 0x3f80

    #@535
    cmpg-float v3, v23, v3

    #@537
    if-gez v3, :cond_55a

    #@539
    .line 13843
    const/high16 v3, 0x437f

    #@53b
    mul-float v3, v3, v23

    #@53d
    float-to-int v3, v3

    #@53e
    move-object/from16 v0, v25

    #@540
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    #@543
    .line 13844
    move-object/from16 v0, p2

    #@545
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@547
    or-int/lit16 v3, v3, 0x1000

    #@549
    move-object/from16 v0, p2

    #@54b
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@54d
    .line 13853
    :cond_54d
    :goto_54d
    const/4 v3, 0x0

    #@54e
    const/4 v4, 0x0

    #@54f
    move-object/from16 v0, p1

    #@551
    move-object/from16 v1, v24

    #@553
    move-object/from16 v2, v25

    #@555
    invoke-virtual {v0, v1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@558
    goto/16 :goto_333

    #@55a
    .line 13845
    :cond_55a
    move/from16 v0, v30

    #@55c
    and-int/lit16 v3, v0, 0x1000

    #@55e
    if-eqz v3, :cond_54d

    #@560
    .line 13846
    const/16 v3, 0xff

    #@562
    move-object/from16 v0, v25

    #@564
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    #@567
    .line 13847
    move-object/from16 v0, p2

    #@569
    iget v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@56b
    and-int/lit16 v3, v3, -0x1001

    #@56d
    move-object/from16 v0, p2

    #@56f
    iput v3, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@571
    goto :goto_54d

    #@572
    .line 13850
    .end local v25           #cachePaint:Landroid/graphics/Paint;
    :cond_572
    move-object/from16 v0, p0

    #@574
    iget-object v0, v0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@576
    move-object/from16 v25, v0

    #@578
    .line 13851
    .restart local v25       #cachePaint:Landroid/graphics/Paint;
    const/high16 v3, 0x437f

    #@57a
    mul-float v3, v3, v23

    #@57c
    float-to-int v3, v3

    #@57d
    move-object/from16 v0, v25

    #@57f
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    #@582
    goto :goto_54d

    #@583
    .line 13632
    nop

    #@584
    :pswitch_data_584
    .packed-switch 0x0
        :pswitch_386
        :pswitch_38c
        :pswitch_3a3
    .end packed-switch
.end method

.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 14514
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    .line 14515
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_11

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 14516
    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@11
    .line 14518
    :cond_11
    return-void
.end method

.method ensureInputFocusOnFirstFocusable()V
    .registers 2

    #@0
    .prologue
    .line 4617
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 4618
    .local v0, root:Landroid/view/View;
    if-eqz v0, :cond_9

    #@6
    .line 4619
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    #@9
    .line 4621
    :cond_9
    return-void
.end method

.method ensureTransformationInfo()V
    .registers 2

    #@0
    .prologue
    .line 8971
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 8972
    new-instance v0, Landroid/view/View$TransformationInfo;

    #@6
    invoke-direct {v0}, Landroid/view/View$TransformationInfo;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@b
    .line 8974
    :cond_b
    return-void
.end method

.method public findFocus()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 5450
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_7

    #@6
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    const/4 p0, 0x0

    #@8
    goto :goto_6
.end method

.method findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;
    .registers 7
    .parameter "root"
    .parameter "direction"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 6438
    sparse-switch p2, :sswitch_data_4e

    #@5
    .line 6465
    :cond_5
    :goto_5
    return-object v1

    #@6
    .line 6440
    :sswitch_6
    iget v2, p0, Landroid/view/View;->mNextFocusLeftId:I

    #@8
    if-eq v2, v3, :cond_5

    #@a
    .line 6441
    iget v1, p0, Landroid/view/View;->mNextFocusLeftId:I

    #@c
    invoke-direct {p0, p1, v1}, Landroid/view/View;->findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;

    #@f
    move-result-object v1

    #@10
    goto :goto_5

    #@11
    .line 6443
    :sswitch_11
    iget v2, p0, Landroid/view/View;->mNextFocusRightId:I

    #@13
    if-eq v2, v3, :cond_5

    #@15
    .line 6444
    iget v1, p0, Landroid/view/View;->mNextFocusRightId:I

    #@17
    invoke-direct {p0, p1, v1}, Landroid/view/View;->findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;

    #@1a
    move-result-object v1

    #@1b
    goto :goto_5

    #@1c
    .line 6446
    :sswitch_1c
    iget v2, p0, Landroid/view/View;->mNextFocusUpId:I

    #@1e
    if-eq v2, v3, :cond_5

    #@20
    .line 6447
    iget v1, p0, Landroid/view/View;->mNextFocusUpId:I

    #@22
    invoke-direct {p0, p1, v1}, Landroid/view/View;->findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;

    #@25
    move-result-object v1

    #@26
    goto :goto_5

    #@27
    .line 6449
    :sswitch_27
    iget v2, p0, Landroid/view/View;->mNextFocusDownId:I

    #@29
    if-eq v2, v3, :cond_5

    #@2b
    .line 6450
    iget v1, p0, Landroid/view/View;->mNextFocusDownId:I

    #@2d
    invoke-direct {p0, p1, v1}, Landroid/view/View;->findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;

    #@30
    move-result-object v1

    #@31
    goto :goto_5

    #@32
    .line 6452
    :sswitch_32
    iget v2, p0, Landroid/view/View;->mNextFocusForwardId:I

    #@34
    if-eq v2, v3, :cond_5

    #@36
    .line 6453
    iget v1, p0, Landroid/view/View;->mNextFocusForwardId:I

    #@38
    invoke-direct {p0, p1, v1}, Landroid/view/View;->findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;

    #@3b
    move-result-object v1

    #@3c
    goto :goto_5

    #@3d
    .line 6455
    :sswitch_3d
    iget v2, p0, Landroid/view/View;->mID:I

    #@3f
    if-eq v2, v3, :cond_5

    #@41
    .line 6456
    iget v0, p0, Landroid/view/View;->mID:I

    #@43
    .line 6457
    .local v0, id:I
    new-instance v1, Landroid/view/View$2;

    #@45
    invoke-direct {v1, p0, v0}, Landroid/view/View$2;-><init>(Landroid/view/View;I)V

    #@48
    invoke-virtual {p1, p0, v1}, Landroid/view/View;->findViewByPredicateInsideOut(Landroid/view/View;Lcom/android/internal/util/Predicate;)Landroid/view/View;

    #@4b
    move-result-object v1

    #@4c
    goto :goto_5

    #@4d
    .line 6438
    nop

    #@4e
    :sswitch_data_4e
    .sparse-switch
        0x1 -> :sswitch_3d
        0x2 -> :sswitch_32
        0x11 -> :sswitch_6
        0x21 -> :sswitch_1c
        0x42 -> :sswitch_11
        0x82 -> :sswitch_27
    .end sparse-switch
.end method

.method final findViewByAccessibilityId(I)Landroid/view/View;
    .registers 3
    .parameter "accessibilityId"

    #@0
    .prologue
    .line 15346
    if-gez p1, :cond_4

    #@2
    .line 15347
    const/4 v0, 0x0

    #@3
    .line 15349
    :goto_3
    return-object v0

    #@4
    :cond_4
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewByAccessibilityIdTraversal(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    goto :goto_3
.end method

.method findViewByAccessibilityIdTraversal(I)Landroid/view/View;
    .registers 3
    .parameter "accessibilityId"

    #@0
    .prologue
    .line 15366
    invoke-virtual {p0}, Landroid/view/View;->getAccessibilityViewId()I

    #@3
    move-result v0

    #@4
    if-ne v0, p1, :cond_7

    #@6
    .line 15369
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    const/4 p0, 0x0

    #@8
    goto :goto_6
.end method

.method public final findViewById(I)Landroid/view/View;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 15333
    if-gez p1, :cond_4

    #@2
    .line 15334
    const/4 v0, 0x0

    #@3
    .line 15336
    :goto_3
    return-object v0

    #@4
    :cond_4
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewTraversal(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    goto :goto_3
.end method

.method public final findViewByPredicate(Lcom/android/internal/util/Predicate;)Landroid/view/View;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 15395
    .local p1, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/View;>;"
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->findViewByPredicateTraversal(Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final findViewByPredicateInsideOut(Landroid/view/View;Lcom/android/internal/util/Predicate;)Landroid/view/View;
    .registers 7
    .parameter "start"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 15415
    .local p2, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/View;>;"
    const/4 v0, 0x0

    #@1
    .line 15417
    .local v0, childToSkip:Landroid/view/View;
    :goto_1
    invoke-virtual {p1, p2, v0}, Landroid/view/View;->findViewByPredicateTraversal(Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;

    #@4
    move-result-object v2

    #@5
    .line 15418
    .local v2, view:Landroid/view/View;
    if-nez v2, :cond_9

    #@7
    if-ne p1, p0, :cond_a

    #@9
    .line 15424
    .end local v2           #view:Landroid/view/View;
    :cond_9
    :goto_9
    return-object v2

    #@a
    .line 15422
    .restart local v2       #view:Landroid/view/View;
    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@d
    move-result-object v1

    #@e
    .line 15423
    .local v1, parent:Landroid/view/ViewParent;
    if-eqz v1, :cond_14

    #@10
    instance-of v3, v1, Landroid/view/View;

    #@12
    if-nez v3, :cond_16

    #@14
    .line 15424
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_9

    #@16
    .line 15427
    :cond_16
    move-object v0, p1

    #@17
    move-object p1, v1

    #@18
    .line 15428
    check-cast p1, Landroid/view/View;

    #@1a
    .line 15429
    goto :goto_1
.end method

.method protected findViewByPredicateTraversal(Lcom/android/internal/util/Predicate;Landroid/view/View;)Landroid/view/View;
    .registers 4
    .parameter
    .parameter "childToSkip"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 15319
    .local p1, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/View;>;"
    invoke-interface {p1, p0}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 15322
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    const/4 p0, 0x0

    #@8
    goto :goto_6
.end method

.method protected findViewTraversal(I)Landroid/view/View;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 15294
    iget v0, p0, Landroid/view/View;->mID:I

    #@2
    if-ne p1, v0, :cond_5

    #@4
    .line 15297
    .end local p0
    :goto_4
    return-object p0

    #@5
    .restart local p0
    :cond_5
    const/4 p0, 0x0

    #@6
    goto :goto_4
.end method

.method public final findViewWithTag(Ljava/lang/Object;)Landroid/view/View;
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 15380
    if-nez p1, :cond_4

    #@2
    .line 15381
    const/4 v0, 0x0

    #@3
    .line 15383
    :goto_3
    return-object v0

    #@4
    :cond_4
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    goto :goto_3
.end method

.method protected findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 15306
    if-eqz p1, :cond_b

    #@2
    iget-object v0, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_b

    #@a
    .line 15309
    .end local p0
    :goto_a
    return-object p0

    #@b
    .restart local p0
    :cond_b
    const/4 p0, 0x0

    #@c
    goto :goto_a
.end method

.method public findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V
    .registers 7
    .parameter
    .parameter "searched"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 6551
    .local p1, outViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@3
    move-result-object v2

    #@4
    if-eqz v2, :cond_e

    #@6
    .line 6552
    and-int/lit8 v2, p3, 0x4

    #@8
    if-eqz v2, :cond_d

    #@a
    .line 6553
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 6564
    :cond_d
    :goto_d
    return-void

    #@e
    .line 6555
    :cond_e
    and-int/lit8 v2, p3, 0x2

    #@10
    if-eqz v2, :cond_d

    #@12
    if-eqz p2, :cond_d

    #@14
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    #@17
    move-result v2

    #@18
    if-lez v2, :cond_d

    #@1a
    iget-object v2, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@1c
    if-eqz v2, :cond_d

    #@1e
    iget-object v2, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@20
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@23
    move-result v2

    #@24
    if-lez v2, :cond_d

    #@26
    .line 6558
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 6559
    .local v1, searchedLowerCase:Ljava/lang/String;
    iget-object v2, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@30
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    .line 6560
    .local v0, contentDescriptionLowerCase:Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_d

    #@3e
    .line 6561
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    goto :goto_d
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .registers 6
    .parameter "insets"

    #@0
    .prologue
    const/high16 v3, -0x8000

    #@2
    const/4 v0, 0x0

    #@3
    .line 5740
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@5
    and-int/lit8 v1, v1, 0x2

    #@7
    const/4 v2, 0x2

    #@8
    if-ne v1, v2, :cond_2c

    #@a
    .line 5741
    iput v3, p0, Landroid/view/View;->mUserPaddingStart:I

    #@c
    .line 5742
    iput v3, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@e
    .line 5743
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@10
    and-int/lit16 v1, v1, 0x800

    #@12
    if-eqz v1, :cond_20

    #@14
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@16
    if-eqz v1, :cond_20

    #@18
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1a
    iget v1, v1, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@1c
    and-int/lit16 v1, v1, 0x600

    #@1e
    if-nez v1, :cond_2d

    #@20
    .line 5746
    :cond_20
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@22
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@24
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@26
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@28
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->internalSetPadding(IIII)V

    #@2b
    .line 5747
    const/4 v0, 0x1

    #@2c
    .line 5753
    :cond_2c
    :goto_2c
    return v0

    #@2d
    .line 5749
    :cond_2d
    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/view/View;->internalSetPadding(IIII)V

    #@30
    goto :goto_2c
.end method

.method public fitsSystemWindows()Z
    .registers 2

    #@0
    .prologue
    .line 5798
    invoke-virtual {p0}, Landroid/view/View;->getFitsSystemWindows()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public focusSearch(I)Landroid/view/View;
    .registers 3
    .parameter "direction"

    #@0
    .prologue
    .line 6407
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 6408
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v0, p0, p1}, Landroid/view/ViewParent;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 6410
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public forceLayout()V
    .registers 3

    #@0
    .prologue
    .line 15731
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    or-int/lit16 v0, v0, 0x1000

    #@4
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    .line 15732
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    const/high16 v1, -0x8000

    #@a
    or-int/2addr v0, v1

    #@b
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    .line 15733
    return-void
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .registers 12
    .parameter "region"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 16142
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    .line 16143
    .local v6, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz p1, :cond_2f

    #@6
    if-eqz v6, :cond_2f

    #@8
    .line 16144
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    .line 16145
    .local v8, pflags:I
    and-int/lit16 v0, v8, 0x80

    #@c
    if-nez v0, :cond_30

    #@e
    .line 16148
    iget-object v7, v6, Landroid/view/View$AttachInfo;->mTransparentLocation:[I

    #@10
    .line 16149
    .local v7, location:[I
    invoke-virtual {p0, v7}, Landroid/view/View;->getLocationInWindow([I)V

    #@13
    .line 16150
    aget v1, v7, v3

    #@15
    aget v2, v7, v9

    #@17
    aget v0, v7, v3

    #@19
    iget v3, p0, Landroid/view/View;->mRight:I

    #@1b
    add-int/2addr v0, v3

    #@1c
    iget v3, p0, Landroid/view/View;->mLeft:I

    #@1e
    sub-int v3, v0, v3

    #@20
    aget v0, v7, v9

    #@22
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@24
    add-int/2addr v0, v4

    #@25
    iget v4, p0, Landroid/view/View;->mTop:I

    #@27
    sub-int v4, v0, v4

    #@29
    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    #@2b
    move-object v0, p1

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@2f
    .line 16159
    .end local v7           #location:[I
    .end local v8           #pflags:I
    :cond_2f
    :goto_2f
    return v9

    #@30
    .line 16152
    .restart local v8       #pflags:I
    :cond_30
    and-int/lit16 v0, v8, 0x100

    #@32
    if-eqz v0, :cond_2f

    #@34
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@36
    if-eqz v0, :cond_2f

    #@38
    .line 16156
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@3a
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->applyDrawableToTransparentRegion(Landroid/graphics/drawable/Drawable;Landroid/graphics/Region;)V

    #@3d
    goto :goto_2f
.end method

.method public getAccessibilityCursorPosition()I
    .registers 2

    #@0
    .prologue
    .line 7187
    iget v0, p0, Landroid/view/View;->mAccessibilityCursorPosition:I

    #@2
    return v0
.end method

.method public getAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .registers 2

    #@0
    .prologue
    .line 5259
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    return-object v0
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .registers 2

    #@0
    .prologue
    .line 5298
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 5299
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0}, Landroid/view/View$AccessibilityDelegate;->getAccessibilityNodeProvider(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeProvider;

    #@9
    move-result-object v0

    #@a
    .line 5301
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getAccessibilityViewId()I
    .registers 3

    #@0
    .prologue
    .line 5314
    iget v0, p0, Landroid/view/View;->mAccessibilityViewId:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_d

    #@5
    .line 5315
    sget v0, Landroid/view/View;->sNextAccessibilityViewId:I

    #@7
    add-int/lit8 v1, v0, 0x1

    #@9
    sput v1, Landroid/view/View;->sNextAccessibilityViewId:I

    #@b
    iput v0, p0, Landroid/view/View;->mAccessibilityViewId:I

    #@d
    .line 5317
    :cond_d
    iget v0, p0, Landroid/view/View;->mAccessibilityViewId:I

    #@f
    return v0
.end method

.method public getAccessibilityWindowId()I
    .registers 2

    #@0
    .prologue
    .line 5328
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget v0, v0, Landroid/view/View$AttachInfo;->mAccessibilityWindowId:I

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, -0x1

    #@a
    goto :goto_8
.end method

.method public getAlpha()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9472
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/high16 v0, 0x3f80

    #@b
    goto :goto_8
.end method

.method public getAnimation()Landroid/view/animation/Animation;
    .registers 2

    #@0
    .prologue
    .line 16033
    iget-object v0, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@2
    return-object v0
.end method

.method public getApplicationWindowToken()Landroid/os/IBinder;
    .registers 3

    #@0
    .prologue
    .line 12117
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 12118
    .local v0, ai:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_b

    #@4
    .line 12119
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mPanelParentWindowToken:Landroid/os/IBinder;

    #@6
    .line 12120
    .local v1, appWindowToken:Landroid/os/IBinder;
    if-nez v1, :cond_a

    #@8
    .line 12121
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mWindowToken:Landroid/os/IBinder;

    #@a
    .line 12125
    .end local v1           #appWindowToken:Landroid/os/IBinder;
    :cond_a
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 14835
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getBaseline()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    #@0
    .prologue
    .line 15708
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public final getBottom()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 9632
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    return v0
.end method

.method protected getBottomFadingEdgeStrength()F
    .registers 3

    #@0
    .prologue
    .line 11103
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollOffset()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollExtent()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollRange()I

    #@c
    move-result v1

    #@d
    if-ge v0, v1, :cond_12

    #@f
    const/high16 v0, 0x3f80

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method protected getBottomPaddingOffset()I
    .registers 2

    #@0
    .prologue
    .line 13375
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method getBoundsOnScreen(Landroid/graphics/Rect;)V
    .registers 11
    .parameter "outRect"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/high16 v8, 0x3f00

    #@3
    .line 5047
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    if-nez v4, :cond_8

    #@7
    .line 5084
    :goto_7
    return-void

    #@8
    .line 5051
    :cond_8
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iget-object v2, v4, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@c
    .line 5052
    .local v2, position:Landroid/graphics/RectF;
    iget v4, p0, Landroid/view/View;->mRight:I

    #@e
    iget v5, p0, Landroid/view/View;->mLeft:I

    #@10
    sub-int/2addr v4, v5

    #@11
    int-to-float v4, v4

    #@12
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@14
    iget v6, p0, Landroid/view/View;->mTop:I

    #@16
    sub-int/2addr v5, v6

    #@17
    int-to-float v5, v5

    #@18
    invoke-virtual {v2, v7, v7, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    #@1b
    .line 5054
    invoke-virtual {p0}, Landroid/view/View;->hasIdentityMatrix()Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_28

    #@21
    .line 5055
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@28
    .line 5058
    :cond_28
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@2a
    int-to-float v4, v4

    #@2b
    iget v5, p0, Landroid/view/View;->mTop:I

    #@2d
    int-to-float v5, v5

    #@2e
    invoke-virtual {v2, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    #@31
    .line 5060
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@33
    .line 5061
    .local v0, parent:Landroid/view/ViewParent;
    :goto_33
    instance-of v4, v0, Landroid/view/View;

    #@35
    if-eqz v4, :cond_5e

    #@37
    move-object v1, v0

    #@38
    .line 5062
    check-cast v1, Landroid/view/View;

    #@3a
    .line 5064
    .local v1, parentView:Landroid/view/View;
    iget v4, v1, Landroid/view/View;->mScrollX:I

    #@3c
    neg-int v4, v4

    #@3d
    int-to-float v4, v4

    #@3e
    iget v5, v1, Landroid/view/View;->mScrollY:I

    #@40
    neg-int v5, v5

    #@41
    int-to-float v5, v5

    #@42
    invoke-virtual {v2, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    #@45
    .line 5066
    invoke-virtual {v1}, Landroid/view/View;->hasIdentityMatrix()Z

    #@48
    move-result v4

    #@49
    if-nez v4, :cond_52

    #@4b
    .line 5067
    invoke-virtual {v1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@52
    .line 5070
    :cond_52
    iget v4, v1, Landroid/view/View;->mLeft:I

    #@54
    int-to-float v4, v4

    #@55
    iget v5, v1, Landroid/view/View;->mTop:I

    #@57
    int-to-float v5, v5

    #@58
    invoke-virtual {v2, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    #@5b
    .line 5072
    iget-object v0, v1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@5d
    .line 5073
    goto :goto_33

    #@5e
    .line 5075
    .end local v1           #parentView:Landroid/view/View;
    :cond_5e
    instance-of v4, v0, Landroid/view/ViewRootImpl;

    #@60
    if-eqz v4, :cond_6c

    #@62
    move-object v3, v0

    #@63
    .line 5076
    check-cast v3, Landroid/view/ViewRootImpl;

    #@65
    .line 5077
    .local v3, viewRootImpl:Landroid/view/ViewRootImpl;
    iget v4, v3, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@67
    neg-int v4, v4

    #@68
    int-to-float v4, v4

    #@69
    invoke-virtual {v2, v7, v4}, Landroid/graphics/RectF;->offset(FF)V

    #@6c
    .line 5080
    .end local v3           #viewRootImpl:Landroid/view/ViewRootImpl;
    :cond_6c
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6e
    iget v4, v4, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@70
    int-to-float v4, v4

    #@71
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@73
    iget v5, v5, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@75
    int-to-float v5, v5

    #@76
    invoke-virtual {v2, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    #@79
    .line 5082
    iget v4, v2, Landroid/graphics/RectF;->left:F

    #@7b
    add-float/2addr v4, v8

    #@7c
    float-to-int v4, v4

    #@7d
    iget v5, v2, Landroid/graphics/RectF;->top:F

    #@7f
    add-float/2addr v5, v8

    #@80
    float-to-int v5, v5

    #@81
    iget v6, v2, Landroid/graphics/RectF;->right:F

    #@83
    add-float/2addr v6, v8

    #@84
    float-to-int v6, v6

    #@85
    iget v7, v2, Landroid/graphics/RectF;->bottom:F

    #@87
    add-float/2addr v7, v8

    #@88
    float-to-int v7, v7

    #@89
    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@8c
    goto/16 :goto_7
.end method

.method public getCameraDistance()F
    .registers 4

    #@0
    .prologue
    .line 9054
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@3
    .line 9055
    iget-object v2, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@5
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@8
    move-result-object v2

    #@9
    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    #@b
    int-to-float v0, v2

    #@c
    .line 9056
    .local v0, dpi:F
    iget-object v1, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@e
    .line 9057
    .local v1, info:Landroid/view/View$TransformationInfo;
    invoke-static {v1}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@11
    move-result-object v2

    #@12
    if-nez v2, :cond_24

    #@14
    .line 9058
    new-instance v2, Landroid/graphics/Camera;

    #@16
    invoke-direct {v2}, Landroid/graphics/Camera;-><init>()V

    #@19
    invoke-static {v1, v2}, Landroid/view/View$TransformationInfo;->access$1802(Landroid/view/View$TransformationInfo;Landroid/graphics/Camera;)Landroid/graphics/Camera;

    #@1c
    .line 9059
    new-instance v2, Landroid/graphics/Matrix;

    #@1e
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    #@21
    invoke-static {v1, v2}, Landroid/view/View$TransformationInfo;->access$1902(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    #@24
    .line 9061
    :cond_24
    invoke-static {v1}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Landroid/graphics/Camera;->getLocationZ()F

    #@2b
    move-result v2

    #@2c
    mul-float/2addr v2, v0

    #@2d
    neg-float v2, v2

    #@2e
    return v2
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    #@0
    .prologue
    .line 5344
    iget-object v0, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 7853
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    #@0
    .prologue
    .line 8085
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getDisplay()Landroid/view/Display;
    .registers 2

    #@0
    .prologue
    .line 12134
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mDisplay:Landroid/view/Display;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getDisplayList()Landroid/view/DisplayList;
    .registers 3

    #@0
    .prologue
    .line 12922
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Landroid/view/View;->getDisplayList(Landroid/view/DisplayList;Z)Landroid/view/DisplayList;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@9
    .line 12923
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@b
    return-object v0
.end method

.method public final getDrawableState()[I
    .registers 2

    #@0
    .prologue
    .line 14549
    iget-object v0, p0, Landroid/view/View;->mDrawableState:[I

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    and-int/lit16 v0, v0, 0x400

    #@8
    if-nez v0, :cond_d

    #@a
    .line 14550
    iget-object v0, p0, Landroid/view/View;->mDrawableState:[I

    #@c
    .line 14554
    :goto_c
    return-object v0

    #@d
    .line 14552
    :cond_d
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Landroid/view/View;->mDrawableState:[I

    #@14
    .line 14553
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@16
    and-int/lit16 v0, v0, -0x401

    #@18
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@1a
    .line 14554
    iget-object v0, p0, Landroid/view/View;->mDrawableState:[I

    #@1c
    goto :goto_c
.end method

.method public getDrawingCache()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 12941
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getDrawingCache(Z)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "autoScale"

    #@0
    .prologue
    const/high16 v2, 0x2

    #@2
    const v1, 0x8000

    #@5
    .line 12972
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@7
    and-int/2addr v0, v2

    #@8
    if-ne v0, v2, :cond_c

    #@a
    .line 12973
    const/4 v0, 0x0

    #@b
    .line 12978
    :goto_b
    return-object v0

    #@c
    .line 12975
    :cond_c
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@e
    and-int/2addr v0, v1

    #@f
    if-ne v0, v1, :cond_14

    #@11
    .line 12976
    invoke-virtual {p0, p1}, Landroid/view/View;->buildDrawingCache(Z)V

    #@14
    .line 12978
    :cond_14
    if-eqz p1, :cond_19

    #@16
    iget-object v0, p0, Landroid/view/View;->mDrawingCache:Landroid/graphics/Bitmap;

    #@18
    goto :goto_b

    #@19
    :cond_19
    iget-object v0, p0, Landroid/view/View;->mUnscaledDrawingCache:Landroid/graphics/Bitmap;

    #@1b
    goto :goto_b
.end method

.method public getDrawingCacheBackgroundColor()I
    .registers 2

    #@0
    .prologue
    .line 13026
    iget v0, p0, Landroid/view/View;->mDrawingCacheBackgroundColor:I

    #@2
    return v0
.end method

.method public getDrawingCacheQuality()I
    .registers 3

    #@0
    .prologue
    .line 5503
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    const/high16 v1, 0x18

    #@4
    and-int/2addr v0, v1

    #@5
    return v0
.end method

.method public getDrawingRect(Landroid/graphics/Rect;)V
    .registers 5
    .parameter "outRect"

    #@0
    .prologue
    .line 8860
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    iput v0, p1, Landroid/graphics/Rect;->left:I

    #@4
    .line 8861
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@6
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@8
    .line 8862
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@a
    iget v1, p0, Landroid/view/View;->mRight:I

    #@c
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@e
    sub-int/2addr v1, v2

    #@f
    add-int/2addr v0, v1

    #@10
    iput v0, p1, Landroid/graphics/Rect;->right:I

    #@12
    .line 8863
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@14
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@16
    iget v2, p0, Landroid/view/View;->mTop:I

    #@18
    sub-int/2addr v1, v2

    #@19
    add-int/2addr v0, v1

    #@1a
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@1c
    .line 8864
    return-void
.end method

.method public getDrawingTime()J
    .registers 3

    #@0
    .prologue
    .line 12359
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-wide v0, v0, Landroid/view/View$AttachInfo;->mDrawingTime:J

    #@8
    :goto_8
    return-wide v0

    #@9
    :cond_9
    const-wide/16 v0, 0x0

    #@b
    goto :goto_8
.end method

.method protected getFadeHeight(Z)I
    .registers 5
    .parameter "offsetRequired"

    #@0
    .prologue
    .line 13393
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@2
    .line 13394
    .local v0, padding:I
    if-eqz p1, :cond_9

    #@4
    invoke-virtual {p0}, Landroid/view/View;->getTopPaddingOffset()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    .line 13395
    :cond_9
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@b
    iget v2, p0, Landroid/view/View;->mTop:I

    #@d
    sub-int/2addr v1, v2

    #@e
    iget v2, p0, Landroid/view/View;->mPaddingBottom:I

    #@10
    sub-int/2addr v1, v2

    #@11
    sub-int/2addr v1, v0

    #@12
    return v1
.end method

.method protected getFadeTop(Z)I
    .registers 4
    .parameter "offsetRequired"

    #@0
    .prologue
    .line 13383
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@2
    .line 13384
    .local v0, top:I
    if-eqz p1, :cond_9

    #@4
    invoke-virtual {p0}, Landroid/view/View;->getTopPaddingOffset()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    .line 13385
    :cond_9
    return v0
.end method

.method public getFilterTouchesWhenObscured()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 6323
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x400

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getFitsSystemWindows()Z
    .registers 3

    #@0
    .prologue
    .line 5793
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getFocusables(I)Ljava/util/ArrayList;
    .registers 4
    .parameter "direction"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 6488
    new-instance v0, Ljava/util/ArrayList;

    #@2
    const/16 v1, 0x18

    #@4
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@7
    .line 6489
    .local v0, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    #@a
    .line 6490
    return-object v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .registers 2
    .parameter "r"

    #@0
    .prologue
    .line 10009
    invoke-virtual {p0, p1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@3
    .line 10010
    return-void
.end method

.method public final getGlobalVisibleRect(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 10040
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z
    .registers 8
    .parameter "r"
    .parameter "globalOffset"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 10027
    iget v3, p0, Landroid/view/View;->mRight:I

    #@3
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@5
    sub-int v1, v3, v4

    #@7
    .line 10028
    .local v1, width:I
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@9
    iget v4, p0, Landroid/view/View;->mTop:I

    #@b
    sub-int v0, v3, v4

    #@d
    .line 10029
    .local v0, height:I
    if-lez v1, :cond_2c

    #@f
    if-lez v0, :cond_2c

    #@11
    .line 10030
    invoke-virtual {p1, v2, v2, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@14
    .line 10031
    if-eqz p2, :cond_1f

    #@16
    .line 10032
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@18
    neg-int v3, v3

    #@19
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@1b
    neg-int v4, v4

    #@1c
    invoke-virtual {p2, v3, v4}, Landroid/graphics/Point;->set(II)V

    #@1f
    .line 10034
    :cond_1f
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@21
    if-eqz v3, :cond_2b

    #@23
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@25
    invoke-interface {v3, p0, p1, p2}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_2c

    #@2b
    :cond_2b
    const/4 v2, 0x1

    #@2c
    .line 10036
    :cond_2c
    return v2
.end method

.method public getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 10695
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 10696
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@8
    .line 10698
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method getHardwareLayer()Landroid/view/HardwareLayer;
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 12588
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    if-eqz v5, :cond_16

    #@6
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@a
    if-eqz v5, :cond_16

    #@c
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@e
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@10
    invoke-virtual {v5}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@13
    move-result v5

    #@14
    if-nez v5, :cond_17

    #@16
    .line 12641
    :cond_16
    :goto_16
    return-object v4

    #@17
    .line 12593
    :cond_17
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@19
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1b
    invoke-virtual {v5}, Landroid/view/HardwareRenderer;->validate()Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_16

    #@21
    .line 12595
    iget v5, p0, Landroid/view/View;->mRight:I

    #@23
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@25
    sub-int v3, v5, v6

    #@27
    .line 12596
    .local v3, width:I
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@29
    iget v6, p0, Landroid/view/View;->mTop:I

    #@2b
    sub-int v0, v5, v6

    #@2d
    .line 12598
    .local v0, height:I
    if-eqz v3, :cond_16

    #@2f
    if-eqz v0, :cond_16

    #@31
    .line 12602
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@33
    const v6, 0x8000

    #@36
    and-int/2addr v5, v6

    #@37
    if-eqz v5, :cond_3d

    #@39
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@3b
    if-nez v5, :cond_80

    #@3d
    .line 12603
    :cond_3d
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@3f
    if-nez v5, :cond_83

    #@41
    .line 12604
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@43
    iget-object v5, v5, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@45
    invoke-virtual {p0}, Landroid/view/View;->isOpaque()Z

    #@48
    move-result v6

    #@49
    invoke-virtual {v5, v3, v0, v6}, Landroid/view/HardwareRenderer;->createHardwareLayer(IIZ)Landroid/view/HardwareLayer;

    #@4c
    move-result-object v5

    #@4d
    iput-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@4f
    .line 12606
    iget-object v5, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@51
    invoke-virtual {v5, v7, v7, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@54
    .line 12629
    :cond_54
    :goto_54
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@56
    invoke-virtual {v5}, Landroid/view/HardwareLayer;->isValid()Z

    #@59
    move-result v5

    #@5a
    if-eqz v5, :cond_16

    #@5c
    .line 12633
    iget-object v4, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@5e
    iget-object v5, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@60
    invoke-virtual {v4, v5}, Landroid/view/HardwareLayer;->setLayerPaint(Landroid/graphics/Paint;)V

    #@63
    .line 12634
    iget-object v4, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@65
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@67
    invoke-direct {p0, v5}, Landroid/view/View;->getHardwareLayerDisplayList(Landroid/view/HardwareLayer;)Landroid/view/DisplayList;

    #@6a
    move-result-object v5

    #@6b
    iget-object v6, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@6d
    invoke-virtual {v4, v5, v6}, Landroid/view/HardwareLayer;->redrawLater(Landroid/view/DisplayList;Landroid/graphics/Rect;)V

    #@70
    .line 12635
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@73
    move-result-object v2

    #@74
    .line 12636
    .local v2, viewRoot:Landroid/view/ViewRootImpl;
    if-eqz v2, :cond_7b

    #@76
    iget-object v4, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@78
    invoke-virtual {v2, v4}, Landroid/view/ViewRootImpl;->pushHardwareLayerUpdate(Landroid/view/HardwareLayer;)V

    #@7b
    .line 12638
    :cond_7b
    iget-object v4, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@7d
    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    #@80
    .line 12641
    .end local v2           #viewRoot:Landroid/view/ViewRootImpl;
    :cond_80
    iget-object v4, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@82
    goto :goto_16

    #@83
    .line 12608
    :cond_83
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@85
    invoke-virtual {v5}, Landroid/view/HardwareLayer;->getWidth()I

    #@88
    move-result v5

    #@89
    if-ne v5, v3, :cond_93

    #@8b
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@8d
    invoke-virtual {v5}, Landroid/view/HardwareLayer;->getHeight()I

    #@90
    move-result v5

    #@91
    if-eq v5, v0, :cond_a0

    #@93
    .line 12609
    :cond_93
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@95
    invoke-virtual {v5, v3, v0}, Landroid/view/HardwareLayer;->resize(II)Z

    #@98
    move-result v5

    #@99
    if-eqz v5, :cond_a0

    #@9b
    .line 12610
    iget-object v5, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@9d
    invoke-virtual {v5, v7, v7, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@a0
    .line 12619
    :cond_a0
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@a3
    .line 12621
    invoke-virtual {p0}, Landroid/view/View;->isOpaque()Z

    #@a6
    move-result v1

    #@a7
    .line 12622
    .local v1, opaque:Z
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@a9
    invoke-virtual {v5}, Landroid/view/HardwareLayer;->isValid()Z

    #@ac
    move-result v5

    #@ad
    if-eqz v5, :cond_54

    #@af
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@b1
    invoke-virtual {v5}, Landroid/view/HardwareLayer;->isOpaque()Z

    #@b4
    move-result v5

    #@b5
    if-eq v5, v1, :cond_54

    #@b7
    .line 12623
    iget-object v5, p0, Landroid/view/View;->mHardwareLayer:Landroid/view/HardwareLayer;

    #@b9
    invoke-virtual {v5, v1}, Landroid/view/HardwareLayer;->setOpaque(Z)V

    #@bc
    .line 12624
    iget-object v5, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@be
    invoke-virtual {v5, v7, v7, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@c1
    goto :goto_54
.end method

.method public getHardwareRenderer()Landroid/view/HardwareRenderer;
    .registers 2

    #@0
    .prologue
    .line 12786
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 12787
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8
    .line 12789
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final getHeight()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    #@0
    .prologue
    .line 8847
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@2
    iget v1, p0, Landroid/view/View;->mTop:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public getHitRect(Landroid/graphics/Rect;)V
    .registers 9
    .parameter "outRect"

    #@0
    .prologue
    .line 9964
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@3
    .line 9965
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5
    .line 9966
    .local v0, info:Landroid/view/View$TransformationInfo;
    if-eqz v0, :cond_11

    #@7
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_11

    #@d
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@f
    if-nez v2, :cond_1d

    #@11
    .line 9967
    :cond_11
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@13
    iget v3, p0, Landroid/view/View;->mTop:I

    #@15
    iget v4, p0, Landroid/view/View;->mRight:I

    #@17
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@19
    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@1c
    .line 9976
    :goto_1c
    return-void

    #@1d
    .line 9969
    :cond_1d
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1f
    iget-object v1, v2, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@21
    .line 9970
    .local v1, tmpRect:Landroid/graphics/RectF;
    iget v2, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@23
    neg-float v2, v2

    #@24
    iget v3, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@26
    neg-float v3, v3

    #@27
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    #@2a
    move-result v4

    #@2b
    int-to-float v4, v4

    #@2c
    iget v5, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@2e
    sub-float/2addr v4, v5

    #@2f
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    #@32
    move-result v5

    #@33
    int-to-float v5, v5

    #@34
    iget v6, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@36
    sub-float/2addr v5, v6

    #@37
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    #@3a
    .line 9972
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@41
    .line 9973
    iget v2, v1, Landroid/graphics/RectF;->left:F

    #@43
    float-to-int v2, v2

    #@44
    iget v3, p0, Landroid/view/View;->mLeft:I

    #@46
    add-int/2addr v2, v3

    #@47
    iget v3, v1, Landroid/graphics/RectF;->top:F

    #@49
    float-to-int v3, v3

    #@4a
    iget v4, p0, Landroid/view/View;->mTop:I

    #@4c
    add-int/2addr v3, v4

    #@4d
    iget v4, v1, Landroid/graphics/RectF;->right:F

    #@4f
    float-to-int v4, v4

    #@50
    iget v5, p0, Landroid/view/View;->mLeft:I

    #@52
    add-int/2addr v4, v5

    #@53
    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    #@55
    float-to-int v5, v5

    #@56
    iget v6, p0, Landroid/view/View;->mTop:I

    #@58
    add-int/2addr v5, v6

    #@59
    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    #@5c
    goto :goto_1c
.end method

.method public getHorizontalFadingEdgeLength()I
    .registers 3

    #@0
    .prologue
    .line 3991
    invoke-virtual {p0}, Landroid/view/View;->isHorizontalFadingEdgeEnabled()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 3992
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@8
    .line 3993
    .local v0, cache:Landroid/view/View$ScrollabilityCache;
    if-eqz v0, :cond_d

    #@a
    .line 3994
    iget v1, v0, Landroid/view/View$ScrollabilityCache;->fadingEdgeLength:I

    #@c
    .line 3997
    .end local v0           #cache:Landroid/view/View$ScrollabilityCache;
    :goto_c
    return v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method protected getHorizontalScrollFactor()F
    .registers 2

    #@0
    .prologue
    .line 16877
    invoke-virtual {p0}, Landroid/view/View;->getVerticalScrollFactor()F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected getHorizontalScrollbarHeight()I
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4029
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@3
    .line 4030
    .local v0, cache:Landroid/view/View$ScrollabilityCache;
    if-eqz v0, :cond_11

    #@5
    .line 4031
    iget-object v1, v0, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@7
    .line 4032
    .local v1, scrollBar:Landroid/widget/ScrollBarDrawable;
    if-eqz v1, :cond_11

    #@9
    .line 4033
    invoke-virtual {v1, v2}, Landroid/widget/ScrollBarDrawable;->getSize(Z)I

    #@c
    move-result v2

    #@d
    .line 4034
    .local v2, size:I
    if-gtz v2, :cond_11

    #@f
    .line 4035
    iget v2, v0, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@11
    .line 4041
    .end local v1           #scrollBar:Landroid/widget/ScrollBarDrawable;
    .end local v2           #size:I
    :cond_11
    return v2
.end method

.method public getId()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 15487
    iget v0, p0, Landroid/view/View;->mID:I

    #@2
    return v0
.end method

.method public getImportantForAccessibility()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "auto"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "yes"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "no"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 6854
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const/high16 v1, 0x30

    #@4
    and-int/2addr v0, v1

    #@5
    shr-int/lit8 v0, v0, 0x14

    #@7
    return v0
.end method

.method final getInverseMatrix()Landroid/graphics/Matrix;
    .registers 4

    #@0
    .prologue
    .line 9031
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    .line 9032
    .local v0, info:Landroid/view/View$TransformationInfo;
    if-eqz v0, :cond_2f

    #@4
    .line 9033
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@7
    .line 9034
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$2000(Landroid/view/View$TransformationInfo;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_2a

    #@d
    .line 9035
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$2100(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@10
    move-result-object v1

    #@11
    if-nez v1, :cond_1b

    #@13
    .line 9036
    new-instance v1, Landroid/graphics/Matrix;

    #@15
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@18
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$2102(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    #@1b
    .line 9038
    :cond_1b
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$2100(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    #@26
    .line 9039
    const/4 v1, 0x0

    #@27
    invoke-static {v0, v1}, Landroid/view/View$TransformationInfo;->access$2002(Landroid/view/View$TransformationInfo;Z)Z

    #@2a
    .line 9041
    :cond_2a
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$2100(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@2d
    move-result-object v1

    #@2e
    .line 9043
    :goto_2e
    return-object v1

    #@2f
    :cond_2f
    sget-object v1, Landroid/graphics/Matrix;->IDENTITY_MATRIX:Landroid/graphics/Matrix;

    #@31
    goto :goto_2e
.end method

.method public getIterableTextForAccessibility()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 7180
    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getIteratorForGranularity(I)Landroid/view/AccessibilityIterators$TextSegmentIterator;
    .registers 5
    .parameter "granularity"

    #@0
    .prologue
    .line 7217
    sparse-switch p1, :sswitch_data_66

    #@3
    .line 7248
    :cond_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 7219
    :sswitch_5
    invoke-virtual {p0}, Landroid/view/View;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@8
    move-result-object v1

    #@9
    .line 7220
    .local v1, text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_3

    #@b
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@e
    move-result v2

    #@f
    if-lez v2, :cond_3

    #@11
    .line 7221
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@1a
    move-result-object v2

    #@1b
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@1d
    invoke-static {v2}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->getInstance(Ljava/util/Locale;)Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;

    #@20
    move-result-object v0

    #@21
    .line 7224
    .local v0, iterator:Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v0, v2}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->initialize(Ljava/lang/String;)V

    #@28
    goto :goto_4

    #@29
    .line 7229
    .end local v0           #iterator:Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;
    .end local v1           #text:Ljava/lang/CharSequence;
    :sswitch_29
    invoke-virtual {p0}, Landroid/view/View;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@2c
    move-result-object v1

    #@2d
    .line 7230
    .restart local v1       #text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_3

    #@2f
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@32
    move-result v2

    #@33
    if-lez v2, :cond_3

    #@35
    .line 7231
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@37
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@3e
    move-result-object v2

    #@3f
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@41
    invoke-static {v2}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->getInstance(Ljava/util/Locale;)Landroid/view/AccessibilityIterators$WordTextSegmentIterator;

    #@44
    move-result-object v0

    #@45
    .line 7234
    .local v0, iterator:Landroid/view/AccessibilityIterators$WordTextSegmentIterator;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v0, v2}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->initialize(Ljava/lang/String;)V

    #@4c
    goto :goto_4

    #@4d
    .line 7239
    .end local v0           #iterator:Landroid/view/AccessibilityIterators$WordTextSegmentIterator;
    .end local v1           #text:Ljava/lang/CharSequence;
    :sswitch_4d
    invoke-virtual {p0}, Landroid/view/View;->getIterableTextForAccessibility()Ljava/lang/CharSequence;

    #@50
    move-result-object v1

    #@51
    .line 7240
    .restart local v1       #text:Ljava/lang/CharSequence;
    if-eqz v1, :cond_3

    #@53
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@56
    move-result v2

    #@57
    if-lez v2, :cond_3

    #@59
    .line 7241
    invoke-static {}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->getInstance()Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;

    #@5c
    move-result-object v0

    #@5d
    .line 7243
    .local v0, iterator:Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {v0, v2}, Landroid/view/AccessibilityIterators$ParagraphTextSegmentIterator;->initialize(Ljava/lang/String;)V

    #@64
    goto :goto_4

    #@65
    .line 7217
    nop

    #@66
    :sswitch_data_66
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_29
        0x8 -> :sswitch_4d
    .end sparse-switch
.end method

.method public getKeepScreenOn()Z
    .registers 3

    #@0
    .prologue
    .line 5534
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    const/high16 v1, 0x400

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;
    .registers 2

    #@0
    .prologue
    .line 7294
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mKeyDispatchState:Landroid/view/KeyEvent$DispatcherState;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getLabelFor()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    #@0
    .prologue
    .line 5383
    iget v0, p0, Landroid/view/View;->mLabelForId:I

    #@2
    return v0
.end method

.method public getLayerType()I
    .registers 2

    #@0
    .prologue
    .line 12544
    iget v0, p0, Landroid/view/View;->mLayerType:I

    #@2
    return v0
.end method

.method public getLayoutDirection()I
    .registers 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "RESOLVED_DIRECTION_LTR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "RESOLVED_DIRECTION_RTL"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 6048
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@8
    move-result-object v2

    #@9
    iget v0, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@b
    .line 6049
    .local v0, targetSdkVersion:I
    sget-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@d
    if-nez v2, :cond_15

    #@f
    .line 6050
    invoke-direct {p0}, Landroid/view/View;->isRtlLangAndLocaleMetaDataExist()Z

    #@12
    move-result v2

    #@13
    sput-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@15
    .line 6051
    :cond_15
    const/16 v2, 0x11

    #@17
    if-ge v0, v2, :cond_24

    #@19
    sget-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@1b
    if-nez v2, :cond_24

    #@1d
    .line 6052
    iget v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1f
    or-int/lit8 v2, v2, 0x20

    #@21
    iput v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@23
    .line 6057
    :cond_23
    :goto_23
    return v1

    #@24
    .line 6055
    :cond_24
    sget-boolean v2, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@26
    if-eqz v2, :cond_31

    #@28
    invoke-direct {p0}, Landroid/view/View;->isLayoutDirectionResolved()Z

    #@2b
    move-result v2

    #@2c
    if-nez v2, :cond_31

    #@2e
    .line 6056
    invoke-virtual {p0}, Landroid/view/View;->resolveLayoutDirection()Z

    #@31
    .line 6057
    :cond_31
    iget v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@33
    and-int/lit8 v2, v2, 0x10

    #@35
    const/16 v3, 0x10

    #@37
    if-ne v2, v3, :cond_23

    #@39
    const/4 v1, 0x1

    #@3a
    goto :goto_23
.end method

.method public getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        deepExport = true
        prefix = "layout_"
    .end annotation

    #@0
    .prologue
    .line 10168
    iget-object v0, p0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@2
    return-object v0
.end method

.method public final getLeft()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 9705
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2
    return v0
.end method

.method protected getLeftFadingEdgeStrength()F
    .registers 2

    #@0
    .prologue
    .line 11118
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollOffset()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_9

    #@6
    const/high16 v0, 0x3f80

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method protected getLeftPaddingOffset()I
    .registers 2

    #@0
    .prologue
    .line 13333
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method getListenerInfo()Landroid/view/View$ListenerInfo;
    .registers 2

    #@0
    .prologue
    .line 4165
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 4166
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@6
    .line 4169
    :goto_6
    return-object v0

    #@7
    .line 4168
    :cond_7
    new-instance v0, Landroid/view/View$ListenerInfo;

    #@9
    invoke-direct {v0}, Landroid/view/View$ListenerInfo;-><init>()V

    #@c
    iput-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@e
    .line 4169
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@10
    goto :goto_6
.end method

.method public final getLocalVisibleRect(Landroid/graphics/Rect;)Z
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 10044
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v1, :cond_19

    #@4
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v1, Landroid/view/View$AttachInfo;->mPoint:Landroid/graphics/Point;

    #@8
    .line 10045
    .local v0, offset:Landroid/graphics/Point;
    :goto_8
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_1f

    #@e
    .line 10046
    iget v1, v0, Landroid/graphics/Point;->x:I

    #@10
    neg-int v1, v1

    #@11
    iget v2, v0, Landroid/graphics/Point;->y:I

    #@13
    neg-int v2, v2

    #@14
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    #@17
    .line 10047
    const/4 v1, 0x1

    #@18
    .line 10049
    :goto_18
    return v1

    #@19
    .line 10044
    .end local v0           #offset:Landroid/graphics/Point;
    :cond_19
    new-instance v0, Landroid/graphics/Point;

    #@1b
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@1e
    goto :goto_8

    #@1f
    .line 10049
    .restart local v0       #offset:Landroid/graphics/Point;
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_18
.end method

.method public getLocationInWindow([I)V
    .registers 11
    .parameter "location"

    #@0
    .prologue
    const/high16 v8, 0x3f00

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 15241
    if-eqz p1, :cond_a

    #@6
    array-length v4, p1

    #@7
    const/4 v5, 0x2

    #@8
    if-ge v4, v5, :cond_13

    #@a
    .line 15242
    :cond_a
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@c
    const-string/jumbo v5, "location must be an array of two integers"

    #@f
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12
    throw v4

    #@13
    .line 15245
    :cond_13
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@15
    if-nez v4, :cond_1c

    #@17
    .line 15247
    aput v6, p1, v7

    #@19
    aput v6, p1, v6

    #@1b
    .line 15286
    :goto_1b
    return-void

    #@1c
    .line 15251
    :cond_1c
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1e
    iget-object v0, v4, Landroid/view/View$AttachInfo;->mTmpTransformLocation:[F

    #@20
    .line 15252
    .local v0, position:[F
    const/4 v4, 0x0

    #@21
    aput v4, v0, v7

    #@23
    aput v4, v0, v6

    #@25
    .line 15254
    invoke-virtual {p0}, Landroid/view/View;->hasIdentityMatrix()Z

    #@28
    move-result v4

    #@29
    if-nez v4, :cond_32

    #@2b
    .line 15255
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@32
    .line 15258
    :cond_32
    aget v4, v0, v6

    #@34
    iget v5, p0, Landroid/view/View;->mLeft:I

    #@36
    int-to-float v5, v5

    #@37
    add-float/2addr v4, v5

    #@38
    aput v4, v0, v6

    #@3a
    .line 15259
    aget v4, v0, v7

    #@3c
    iget v5, p0, Landroid/view/View;->mTop:I

    #@3e
    int-to-float v5, v5

    #@3f
    add-float/2addr v4, v5

    #@40
    aput v4, v0, v7

    #@42
    .line 15261
    iget-object v2, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@44
    .line 15262
    .local v2, viewParent:Landroid/view/ViewParent;
    :goto_44
    instance-of v4, v2, Landroid/view/View;

    #@46
    if-eqz v4, :cond_7b

    #@48
    move-object v1, v2

    #@49
    .line 15263
    check-cast v1, Landroid/view/View;

    #@4b
    .line 15265
    .local v1, view:Landroid/view/View;
    aget v4, v0, v6

    #@4d
    iget v5, v1, Landroid/view/View;->mScrollX:I

    #@4f
    int-to-float v5, v5

    #@50
    sub-float/2addr v4, v5

    #@51
    aput v4, v0, v6

    #@53
    .line 15266
    aget v4, v0, v7

    #@55
    iget v5, v1, Landroid/view/View;->mScrollY:I

    #@57
    int-to-float v5, v5

    #@58
    sub-float/2addr v4, v5

    #@59
    aput v4, v0, v7

    #@5b
    .line 15268
    invoke-virtual {v1}, Landroid/view/View;->hasIdentityMatrix()Z

    #@5e
    move-result v4

    #@5f
    if-nez v4, :cond_68

    #@61
    .line 15269
    invoke-virtual {v1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    #@68
    .line 15272
    :cond_68
    aget v4, v0, v6

    #@6a
    iget v5, v1, Landroid/view/View;->mLeft:I

    #@6c
    int-to-float v5, v5

    #@6d
    add-float/2addr v4, v5

    #@6e
    aput v4, v0, v6

    #@70
    .line 15273
    aget v4, v0, v7

    #@72
    iget v5, v1, Landroid/view/View;->mTop:I

    #@74
    int-to-float v5, v5

    #@75
    add-float/2addr v4, v5

    #@76
    aput v4, v0, v7

    #@78
    .line 15275
    iget-object v2, v1, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@7a
    .line 15276
    goto :goto_44

    #@7b
    .line 15278
    .end local v1           #view:Landroid/view/View;
    :cond_7b
    instance-of v4, v2, Landroid/view/ViewRootImpl;

    #@7d
    if-eqz v4, :cond_8a

    #@7f
    move-object v3, v2

    #@80
    .line 15280
    check-cast v3, Landroid/view/ViewRootImpl;

    #@82
    .line 15281
    .local v3, vr:Landroid/view/ViewRootImpl;
    aget v4, v0, v7

    #@84
    iget v5, v3, Landroid/view/ViewRootImpl;->mCurScrollY:I

    #@86
    int-to-float v5, v5

    #@87
    sub-float/2addr v4, v5

    #@88
    aput v4, v0, v7

    #@8a
    .line 15284
    .end local v3           #vr:Landroid/view/ViewRootImpl;
    :cond_8a
    aget v4, v0, v6

    #@8c
    add-float/2addr v4, v8

    #@8d
    float-to-int v4, v4

    #@8e
    aput v4, p1, v6

    #@90
    .line 15285
    aget v4, v0, v7

    #@92
    add-float/2addr v4, v8

    #@93
    float-to-int v4, v4

    #@94
    aput v4, p1, v7

    #@96
    goto :goto_1b
.end method

.method public getLocationOnScreen([I)V
    .registers 6
    .parameter "location"

    #@0
    .prologue
    .line 15224
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationInWindow([I)V

    #@3
    .line 15226
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    .line 15227
    .local v0, info:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_17

    #@7
    .line 15228
    const/4 v1, 0x0

    #@8
    aget v2, p1, v1

    #@a
    iget v3, v0, Landroid/view/View$AttachInfo;->mWindowLeft:I

    #@c
    add-int/2addr v2, v3

    #@d
    aput v2, p1, v1

    #@f
    .line 15229
    const/4 v1, 0x1

    #@10
    aget v2, p1, v1

    #@12
    iget v3, v0, Landroid/view/View$AttachInfo;->mWindowTop:I

    #@14
    add-int/2addr v2, v3

    #@15
    aput v2, p1, v1

    #@17
    .line 15231
    :cond_17
    return-void
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .registers 2

    #@0
    .prologue
    .line 8939
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 8940
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@7
    .line 8941
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;

    #@c
    move-result-object v0

    #@d
    .line 8943
    :goto_d
    return-object v0

    #@e
    :cond_e
    sget-object v0, Landroid/graphics/Matrix;->IDENTITY_MATRIX:Landroid/graphics/Matrix;

    #@10
    goto :goto_d
.end method

.method public final getMeasuredHeight()I
    .registers 3

    #@0
    .prologue
    .line 8898
    iget v0, p0, Landroid/view/View;->mMeasuredHeight:I

    #@2
    const v1, 0xffffff

    #@5
    and-int/2addr v0, v1

    #@6
    return v0
.end method

.method public final getMeasuredHeightAndState()I
    .registers 2

    #@0
    .prologue
    .line 8911
    iget v0, p0, Landroid/view/View;->mMeasuredHeight:I

    #@2
    return v0
.end method

.method public final getMeasuredState()I
    .registers 3

    #@0
    .prologue
    .line 8922
    iget v0, p0, Landroid/view/View;->mMeasuredWidth:I

    #@2
    const/high16 v1, -0x100

    #@4
    and-int/2addr v0, v1

    #@5
    iget v1, p0, Landroid/view/View;->mMeasuredHeight:I

    #@7
    shr-int/lit8 v1, v1, 0x10

    #@9
    and-int/lit16 v1, v1, -0x100

    #@b
    or-int/2addr v0, v1

    #@c
    return v0
.end method

.method public final getMeasuredWidth()I
    .registers 3

    #@0
    .prologue
    .line 8874
    iget v0, p0, Landroid/view/View;->mMeasuredWidth:I

    #@2
    const v1, 0xffffff

    #@5
    and-int/2addr v0, v1

    #@6
    return v0
.end method

.method public final getMeasuredWidthAndState()I
    .registers 2

    #@0
    .prologue
    .line 8887
    iget v0, p0, Landroid/view/View;->mMeasuredWidth:I

    #@2
    return v0
.end method

.method public getMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 15977
    iget v0, p0, Landroid/view/View;->mMinHeight:I

    #@2
    return v0
.end method

.method public getMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 16006
    iget v0, p0, Landroid/view/View;->mMinWidth:I

    #@2
    return v0
.end method

.method public getNextFocusDownId()I
    .registers 2

    #@0
    .prologue
    .line 5621
    iget v0, p0, Landroid/view/View;->mNextFocusDownId:I

    #@2
    return v0
.end method

.method public getNextFocusForwardId()I
    .registers 2

    #@0
    .prologue
    .line 5642
    iget v0, p0, Landroid/view/View;->mNextFocusForwardId:I

    #@2
    return v0
.end method

.method public getNextFocusLeftId()I
    .registers 2

    #@0
    .prologue
    .line 5558
    iget v0, p0, Landroid/view/View;->mNextFocusLeftId:I

    #@2
    return v0
.end method

.method public getNextFocusRightId()I
    .registers 2

    #@0
    .prologue
    .line 5579
    iget v0, p0, Landroid/view/View;->mNextFocusRightId:I

    #@2
    return v0
.end method

.method public getNextFocusUpId()I
    .registers 2

    #@0
    .prologue
    .line 5600
    iget v0, p0, Landroid/view/View;->mNextFocusUpId:I

    #@2
    return v0
.end method

.method public getOnFocusChangeListener()Landroid/view/View$OnFocusChangeListener;
    .registers 3

    #@0
    .prologue
    .line 4250
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 4251
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_7

    #@4
    iget-object v1, v0, Landroid/view/View$ListenerInfo;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    #@6
    :goto_6
    return-object v1

    #@7
    :cond_7
    const/4 v1, 0x0

    #@8
    goto :goto_6
.end method

.method public getOpticalInsets()Landroid/graphics/Insets;
    .registers 2

    #@0
    .prologue
    .line 15076
    iget-object v0, p0, Landroid/view/View;->mLayoutInsets:Landroid/graphics/Insets;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 15077
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@6
    if-nez v0, :cond_f

    #@8
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@a
    :goto_a
    iput-object v0, p0, Landroid/view/View;->mLayoutInsets:Landroid/graphics/Insets;

    #@c
    .line 15079
    :cond_c
    iget-object v0, p0, Landroid/view/View;->mLayoutInsets:Landroid/graphics/Insets;

    #@e
    return-object v0

    #@f
    .line 15077
    :cond_f
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getLayoutInsets()Landroid/graphics/Insets;

    #@14
    move-result-object v0

    #@15
    goto :goto_a
.end method

.method public getOverScrollMode()I
    .registers 2

    #@0
    .prologue
    .line 16826
    iget v0, p0, Landroid/view/View;->mOverScrollMode:I

    #@2
    return v0
.end method

.method public getPaddingBottom()I
    .registers 2

    #@0
    .prologue
    .line 14981
    iget v0, p0, Landroid/view/View;->mPaddingBottom:I

    #@2
    return v0
.end method

.method public getPaddingEnd()I
    .registers 3

    #@0
    .prologue
    .line 15035
    invoke-virtual {p0}, Landroid/view/View;->isPaddingResolved()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 15036
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@9
    .line 15038
    :cond_9
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@c
    move-result v0

    #@d
    const/4 v1, 0x1

    #@e
    if-ne v0, v1, :cond_13

    #@10
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@15
    goto :goto_12
.end method

.method public getPaddingLeft()I
    .registers 2

    #@0
    .prologue
    .line 14992
    invoke-virtual {p0}, Landroid/view/View;->isPaddingResolved()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 14993
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@9
    .line 14995
    :cond_9
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@b
    return v0
.end method

.method public getPaddingRight()I
    .registers 2

    #@0
    .prologue
    .line 15021
    invoke-virtual {p0}, Landroid/view/View;->isPaddingResolved()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 15022
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@9
    .line 15024
    :cond_9
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@b
    return v0
.end method

.method public getPaddingStart()I
    .registers 3

    #@0
    .prologue
    .line 15006
    invoke-virtual {p0}, Landroid/view/View;->isPaddingResolved()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 15007
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@9
    .line 15009
    :cond_9
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@c
    move-result v0

    #@d
    const/4 v1, 0x1

    #@e
    if-ne v0, v1, :cond_13

    #@10
    iget v0, p0, Landroid/view/View;->mPaddingRight:I

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    iget v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@15
    goto :goto_12
.end method

.method public getPaddingTop()I
    .registers 2

    #@0
    .prologue
    .line 14970
    iget v0, p0, Landroid/view/View;->mPaddingTop:I

    #@2
    return v0
.end method

.method public final getParent()Landroid/view/ViewParent;
    .registers 2

    #@0
    .prologue
    .line 8784
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    return-object v0
.end method

.method public getParentForAccessibility()Landroid/view/ViewParent;
    .registers 3

    #@0
    .prologue
    .line 6912
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    instance-of v1, v1, Landroid/view/View;

    #@4
    if-eqz v1, :cond_1a

    #@6
    .line 6913
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@8
    check-cast v0, Landroid/view/View;

    #@a
    .line 6914
    .local v0, parentView:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->includeForAccessibility()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 6915
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@12
    .line 6920
    .end local v0           #parentView:Landroid/view/View;
    :goto_12
    return-object v1

    #@13
    .line 6917
    .restart local v0       #parentView:Landroid/view/View;
    :cond_13
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@15
    invoke-interface {v1}, Landroid/view/ViewParent;->getParentForAccessibility()Landroid/view/ViewParent;

    #@18
    move-result-object v1

    #@19
    goto :goto_12

    #@1a
    .line 6920
    .end local v0           #parentView:Landroid/view/View;
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_12
.end method

.method public getPivotX()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9376
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getPivotY()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9427
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getRawLayoutDirection()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "LTR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "RTL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "INHERIT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "LOCALE"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 5996
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit8 v0, v0, 0xc

    #@4
    shr-int/lit8 v0, v0, 0x2

    #@6
    return v0
.end method

.method public getRawTextAlignment()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "INHERIT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "GRAVITY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "TEXT_START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "TEXT_END"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "CENTER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "VIEW_START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x6
                to = "VIEW_END"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 17102
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const v1, 0xe000

    #@5
    and-int/2addr v0, v1

    #@6
    shr-int/lit8 v0, v0, 0xd

    #@8
    return v0
.end method

.method public getRawTextDirection()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "INHERIT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "FIRST_STRONG"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "ANY_RTL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "LTR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "RTL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "LOCALE"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 16906
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit16 v0, v0, 0x1c0

    #@4
    shr-int/lit8 v0, v0, 0x6

    #@6
    return v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 2

    #@0
    .prologue
    .line 14364
    iget-object v0, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@2
    return-object v0
.end method

.method public final getRight()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 9772
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    return v0
.end method

.method protected getRightFadingEdgeStrength()F
    .registers 3

    #@0
    .prologue
    .line 11132
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollOffset()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollExtent()I

    #@7
    move-result v1

    #@8
    add-int/2addr v0, v1

    #@9
    invoke-virtual {p0}, Landroid/view/View;->computeHorizontalScrollRange()I

    #@c
    move-result v1

    #@d
    if-ge v0, v1, :cond_12

    #@f
    const/high16 v0, 0x3f80

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method protected getRightPaddingOffset()I
    .registers 2

    #@0
    .prologue
    .line 13347
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getRootView()Landroid/view/View;
    .registers 4

    #@0
    .prologue
    .line 15200
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v2, :cond_b

    #@4
    .line 15201
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v1, v2, Landroid/view/View$AttachInfo;->mRootView:Landroid/view/View;

    #@8
    .line 15202
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_b

    #@a
    .line 15213
    .end local v1           #v:Landroid/view/View;
    :goto_a
    return-object v1

    #@b
    .line 15207
    :cond_b
    move-object v0, p0

    #@c
    .line 15209
    .local v0, parent:Landroid/view/View;
    :goto_c
    iget-object v2, v0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@e
    if-eqz v2, :cond_1b

    #@10
    iget-object v2, v0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@12
    instance-of v2, v2, Landroid/view/View;

    #@14
    if-eqz v2, :cond_1b

    #@16
    .line 15210
    iget-object v0, v0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@18
    .end local v0           #parent:Landroid/view/View;
    check-cast v0, Landroid/view/View;

    #@1a
    .restart local v0       #parent:Landroid/view/View;
    goto :goto_c

    #@1b
    :cond_1b
    move-object v1, v0

    #@1c
    .line 15213
    goto :goto_a
.end method

.method public getRotation()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9138
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getRotationX()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9236
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getRotationY()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9185
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getScaleX()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9288
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/high16 v0, 0x3f80

    #@b
    goto :goto_8
.end method

.method public getScaleY()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9331
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/high16 v0, 0x3f80

    #@b
    goto :goto_8
.end method

.method public getScrollBarDefaultDelayBeforeFade()I
    .registers 2

    #@0
    .prologue
    .line 11242
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@2
    if-nez v0, :cond_9

    #@4
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollDefaultDelay()I

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@b
    iget v0, v0, Landroid/view/View$ScrollabilityCache;->scrollBarDefaultDelayBeforeFade:I

    #@d
    goto :goto_8
.end method

.method public getScrollBarFadeDuration()I
    .registers 2

    #@0
    .prologue
    .line 11266
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@2
    if-nez v0, :cond_9

    #@4
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollBarFadeDuration()I

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@b
    iget v0, v0, Landroid/view/View$ScrollabilityCache;->scrollBarFadeDuration:I

    #@d
    goto :goto_8
.end method

.method public getScrollBarSize()I
    .registers 2

    #@0
    .prologue
    .line 11290
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@2
    if-nez v0, :cond_f

    #@4
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@6
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledScrollBarSize()I

    #@d
    move-result v0

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@11
    iget v0, v0, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@13
    goto :goto_e
.end method

.method public getScrollBarStyle()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "INSIDE_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1000000
                to = "INSIDE_INSET"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2000000
                to = "OUTSIDE_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3000000
                to = "OUTSIDE_INSET"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 11349
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    const/high16 v1, 0x300

    #@4
    and-int/2addr v0, v1

    #@5
    return v0
.end method

.method public final getScrollX()I
    .registers 2

    #@0
    .prologue
    .line 8816
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    return v0
.end method

.method public final getScrollY()I
    .registers 2

    #@0
    .prologue
    .line 8827
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    return v0
.end method

.method public getSolidColor()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 14102
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected getSuggestedMinimumHeight()I
    .registers 3

    #@0
    .prologue
    .line 15948
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-nez v0, :cond_7

    #@4
    iget v0, p0, Landroid/view/View;->mMinHeight:I

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    iget v0, p0, Landroid/view/View;->mMinHeight:I

    #@9
    iget-object v1, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@b
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@e
    move-result v1

    #@f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@12
    move-result v0

    #@13
    goto :goto_6
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 3

    #@0
    .prologue
    .line 15964
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-nez v0, :cond_7

    #@4
    iget v0, p0, Landroid/view/View;->mMinWidth:I

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    iget v0, p0, Landroid/view/View;->mMinWidth:I

    #@9
    iget-object v1, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@b
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@e
    move-result v1

    #@f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@12
    move-result v0

    #@13
    goto :goto_6
.end method

.method public getSystemUiVisibility()I
    .registers 2

    #@0
    .prologue
    .line 16292
    iget v0, p0, Landroid/view/View;->mSystemUiVisibility:I

    #@2
    return v0
.end method

.method public getTag()Ljava/lang/Object;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 15500
    iget-object v0, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getTag(I)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 15545
    iget-object v0, p0, Landroid/view/View;->mKeyedTags:Landroid/util/SparseArray;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/view/View;->mKeyedTags:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    .line 15546
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getTextAlignment()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "text"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "INHERIT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "GRAVITY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "TEXT_START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "TEXT_END"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "CENTER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "VIEW_START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x6
                to = "VIEW_END"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 17166
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const/high16 v1, 0xe

    #@4
    and-int/2addr v0, v1

    #@5
    shr-int/lit8 v0, v0, 0x11

    #@7
    return v0
.end method

.method public getTextDirection()I
    .registers 2

    #@0
    .prologue
    .line 16958
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit16 v0, v0, 0x1c00

    #@4
    shr-int/lit8 v0, v0, 0xa

    #@6
    return v0
.end method

.method public final getTop()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 9565
    iget v0, p0, Landroid/view/View;->mTop:I

    #@2
    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .registers 2

    #@0
    .prologue
    .line 11089
    invoke-virtual {p0}, Landroid/view/View;->computeVerticalScrollOffset()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_9

    #@6
    const/high16 v0, 0x3f80

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method protected getTopPaddingOffset()I
    .registers 2

    #@0
    .prologue
    .line 13361
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getTouchDelegate()Landroid/view/TouchDelegate;
    .registers 2

    #@0
    .prologue
    .line 8540
    iget-object v0, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@2
    return-object v0
.end method

.method public getTouchables()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 6573
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 6574
    .local v0, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0, v0}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    #@8
    .line 6575
    return-object v0
.end method

.method public getTranslationX()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9885
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getTranslationY()F
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9927
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getVerticalFadingEdgeLength()I
    .registers 3

    #@0
    .prologue
    .line 3958
    invoke-virtual {p0}, Landroid/view/View;->isVerticalFadingEdgeEnabled()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 3959
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@8
    .line 3960
    .local v0, cache:Landroid/view/View$ScrollabilityCache;
    if-eqz v0, :cond_d

    #@a
    .line 3961
    iget v1, v0, Landroid/view/View$ScrollabilityCache;->fadingEdgeLength:I

    #@c
    .line 3964
    .end local v0           #cache:Landroid/view/View$ScrollabilityCache;
    :goto_c
    return v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method protected getVerticalScrollFactor()F
    .registers 5

    #@0
    .prologue
    .line 16856
    iget v1, p0, Landroid/view/View;->mVerticalScrollFactor:F

    #@2
    const/4 v2, 0x0

    #@3
    cmpl-float v1, v1, v2

    #@5
    if-nez v1, :cond_34

    #@7
    .line 16857
    new-instance v0, Landroid/util/TypedValue;

    #@9
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@c
    .line 16858
    .local v0, outValue:Landroid/util/TypedValue;
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@11
    move-result-object v1

    #@12
    const v2, 0x101004d

    #@15
    const/4 v3, 0x1

    #@16
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_24

    #@1c
    .line 16860
    new-instance v1, Ljava/lang/IllegalStateException;

    #@1e
    const-string v2, "Expected theme to define listPreferredItemHeight."

    #@20
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    throw v1

    #@24
    .line 16863
    :cond_24
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@26
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v0, v1}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@31
    move-result v1

    #@32
    iput v1, p0, Landroid/view/View;->mVerticalScrollFactor:F

    #@34
    .line 16866
    .end local v0           #outValue:Landroid/util/TypedValue;
    :cond_34
    iget v1, p0, Landroid/view/View;->mVerticalScrollFactor:F

    #@36
    return v1
.end method

.method public getVerticalScrollbarPosition()I
    .registers 2

    #@0
    .prologue
    .line 4161
    iget v0, p0, Landroid/view/View;->mVerticalScrollbarPosition:I

    #@2
    return v0
.end method

.method public getVerticalScrollbarWidth()I
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4007
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@3
    .line 4008
    .local v0, cache:Landroid/view/View$ScrollabilityCache;
    if-eqz v0, :cond_12

    #@5
    .line 4009
    iget-object v1, v0, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@7
    .line 4010
    .local v1, scrollBar:Landroid/widget/ScrollBarDrawable;
    if-eqz v1, :cond_12

    #@9
    .line 4011
    const/4 v3, 0x1

    #@a
    invoke-virtual {v1, v3}, Landroid/widget/ScrollBarDrawable;->getSize(Z)I

    #@d
    move-result v2

    #@e
    .line 4012
    .local v2, size:I
    if-gtz v2, :cond_12

    #@10
    .line 4013
    iget v2, v0, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@12
    .line 4019
    .end local v1           #scrollBar:Landroid/widget/ScrollBarDrawable;
    .end local v2           #size:I
    :cond_12
    return v2
.end method

.method public getViewRootImpl()Landroid/view/ViewRootImpl;
    .registers 2

    #@0
    .prologue
    .line 10707
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 10708
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@8
    .line 10710
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getViewTreeObserver()Landroid/view/ViewTreeObserver;
    .registers 2

    #@0
    .prologue
    .line 15185
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 15186
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@8
    .line 15191
    :goto_8
    return-object v0

    #@9
    .line 15188
    :cond_9
    iget-object v0, p0, Landroid/view/View;->mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

    #@b
    if-nez v0, :cond_14

    #@d
    .line 15189
    new-instance v0, Landroid/view/ViewTreeObserver;

    #@f
    invoke-direct {v0}, Landroid/view/ViewTreeObserver;-><init>()V

    #@12
    iput-object v0, p0, Landroid/view/View;->mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

    #@14
    .line 15191
    :cond_14
    iget-object v0, p0, Landroid/view/View;->mFloatingTreeObserver:Landroid/view/ViewTreeObserver;

    #@16
    goto :goto_8
.end method

.method public getVisibility()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "VISIBLE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "INVISIBLE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x8
                to = "GONE"
            .end subannotation
        }
    .end annotation

    #@0
    .prologue
    .line 5830
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0xc

    #@4
    return v0
.end method

.method public final getWidth()I
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    #@0
    .prologue
    .line 8837
    iget v0, p0, Landroid/view/View;->mRight:I

    #@2
    iget v1, p0, Landroid/view/View;->mLeft:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method protected getWindowAttachCount()I
    .registers 2

    #@0
    .prologue
    .line 12094
    iget v0, p0, Landroid/view/View;->mWindowAttachCount:I

    #@2
    return v0
.end method

.method getWindowSession()Landroid/view/IWindowSession;
    .registers 2

    #@0
    .prologue
    .line 12143
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getWindowSystemUiVisibility()I
    .registers 2

    #@0
    .prologue
    .line 16302
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget v0, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getWindowToken()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 12103
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mWindowToken:Landroid/os/IBinder;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getWindowVisibility()I
    .registers 2

    #@0
    .prologue
    .line 7699
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget v0, v0, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/16 v0, 0x8

    #@b
    goto :goto_8
.end method

.method public getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "outRect"

    #@0
    .prologue
    .line 7719
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v3, :cond_32

    #@4
    .line 7721
    :try_start_4
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v3, v3, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@8
    iget-object v4, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mWindow:Landroid/view/IWindow;

    #@c
    invoke-interface {v3, v4, p1}, Landroid/view/IWindowSession;->getDisplayFrame(Landroid/view/IWindow;Landroid/graphics/Rect;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_30

    #@f
    .line 7728
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11
    iget-object v2, v3, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@13
    .line 7729
    .local v2, insets:Landroid/graphics/Rect;
    iget v3, p1, Landroid/graphics/Rect;->left:I

    #@15
    iget v4, v2, Landroid/graphics/Rect;->left:I

    #@17
    add-int/2addr v3, v4

    #@18
    iput v3, p1, Landroid/graphics/Rect;->left:I

    #@1a
    .line 7730
    iget v3, p1, Landroid/graphics/Rect;->top:I

    #@1c
    iget v4, v2, Landroid/graphics/Rect;->top:I

    #@1e
    add-int/2addr v3, v4

    #@1f
    iput v3, p1, Landroid/graphics/Rect;->top:I

    #@21
    .line 7731
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@23
    iget v4, v2, Landroid/graphics/Rect;->right:I

    #@25
    sub-int/2addr v3, v4

    #@26
    iput v3, p1, Landroid/graphics/Rect;->right:I

    #@28
    .line 7732
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@2a
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    #@2c
    sub-int/2addr v3, v4

    #@2d
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    #@2f
    .line 7739
    .end local v2           #insets:Landroid/graphics/Rect;
    :goto_2f
    return-void

    #@30
    .line 7722
    :catch_30
    move-exception v1

    #@31
    .line 7723
    .local v1, e:Landroid/os/RemoteException;
    goto :goto_2f

    #@32
    .line 7737
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_32
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    #@35
    move-result-object v3

    #@36
    const/4 v4, 0x0

    #@37
    invoke-virtual {v3, v4}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    #@3a
    move-result-object v0

    #@3b
    .line 7738
    .local v0, d:Landroid/view/Display;
    invoke-virtual {v0, p1}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    #@3e
    goto :goto_2f
.end method

.method public getX()F
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9838
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2
    int-to-float v1, v0

    #@3
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5
    if-eqz v0, :cond_d

    #@7
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    iget v0, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@b
    :goto_b
    add-float/2addr v0, v1

    #@c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_b
.end method

.method public getY()F
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 9861
    iget v0, p0, Landroid/view/View;->mTop:I

    #@2
    int-to-float v1, v0

    #@3
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5
    if-eqz v0, :cond_d

    #@7
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    iget v0, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@b
    :goto_b
    add-float/2addr v0, v1

    #@c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_b
.end method

.method public hackTurnOffWindowResizeAnim(Z)V
    .registers 3
    .parameter "off"

    #@0
    .prologue
    .line 17619
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    iput-boolean p1, v0, Landroid/view/View$AttachInfo;->mTurnOffWindowResizeAnim:Z

    #@4
    .line 17620
    return-void
.end method

.method handleFocusGainInternal(ILandroid/graphics/Rect;)V
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 4495
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-nez v0, :cond_2b

    #@6
    .line 4496
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    or-int/lit8 v0, v0, 0x2

    #@a
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@c
    .line 4498
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 4499
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@12
    invoke-interface {v0, p0, p0}, Landroid/view/ViewParent;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    #@15
    .line 4502
    :cond_15
    const/4 v0, 0x1

    #@16
    invoke-virtual {p0, v0, p1, p2}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@19
    .line 4503
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@1c
    .line 4505
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@1e
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2b

    #@28
    .line 4506
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@2b
    .line 4509
    :cond_2b
    return-void
.end method

.method public hasFocus()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "focus"
    .end annotation

    #@0
    .prologue
    .line 4652
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public hasFocusable()Z
    .registers 2

    #@0
    .prologue
    .line 4668
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0xc

    #@4
    if-nez v0, :cond_e

    #@6
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected hasHoveredChild()Z
    .registers 2

    #@0
    .prologue
    .line 7509
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method final hasIdentityMatrix()Z
    .registers 2

    #@0
    .prologue
    .line 8963
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 8964
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@7
    .line 8965
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@c
    move-result v0

    #@d
    .line 8967
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    goto :goto_d
.end method

.method public hasOnClickListeners()Z
    .registers 3

    #@0
    .prologue
    .line 4274
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 4275
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_a

    #@4
    iget-object v1, v0, Landroid/view/View$ListenerInfo;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@6
    if-eqz v1, :cond_a

    #@8
    const/4 v1, 0x1

    #@9
    :goto_9
    return v1

    #@a
    :cond_a
    const/4 v1, 0x0

    #@b
    goto :goto_9
.end method

.method protected hasOpaqueScrollbars()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x100

    #@2
    .line 10687
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public hasOverlappingRendering()Z
    .registers 2

    #@0
    .prologue
    .line 9489
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method hasStaticLayer()Z
    .registers 2

    #@0
    .prologue
    .line 12525
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public hasTransientState()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    #@0
    .prologue
    const/high16 v1, 0x40

    #@2
    .line 6088
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public hasWindowFocus()Z
    .registers 2

    #@0
    .prologue
    .line 7607
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public includeForAccessibility()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6948
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    if-eqz v1, :cond_12

    #@5
    .line 6949
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    iget-boolean v1, v1, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@9
    if-nez v1, :cond_11

    #@b
    invoke-virtual {p0}, Landroid/view/View;->isImportantForAccessibility()Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    :cond_11
    const/4 v0, 0x1

    #@12
    .line 6951
    :cond_12
    return v0
.end method

.method protected initializeFadingEdge(Landroid/content/res/TypedArray;)V
    .registers 5
    .parameter "a"

    #@0
    .prologue
    .line 3942
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@3
    .line 3944
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@5
    const/16 v1, 0x18

    #@7
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@9
    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledFadingEdgeLength()I

    #@10
    move-result v2

    #@11
    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@14
    move-result v1

    #@15
    iput v1, v0, Landroid/view/View$ScrollabilityCache;->fadingEdgeLength:I

    #@17
    .line 3947
    return-void
.end method

.method protected initializeScrollbars(Landroid/content/res/TypedArray;)V
    .registers 12
    .parameter "a"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 4056
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@5
    .line 4058
    iget-object v3, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@7
    .line 4060
    .local v3, scrollabilityCache:Landroid/view/View$ScrollabilityCache;
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@9
    if-nez v6, :cond_12

    #@b
    .line 4061
    new-instance v6, Landroid/widget/ScrollBarDrawable;

    #@d
    invoke-direct {v6}, Landroid/widget/ScrollBarDrawable;-><init>()V

    #@10
    iput-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@12
    .line 4064
    :cond_12
    const/16 v6, 0x2c

    #@14
    invoke-virtual {p1, v6, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@17
    move-result v1

    #@18
    .line 4066
    .local v1, fadeScrollbars:Z
    if-nez v1, :cond_1c

    #@1a
    .line 4067
    iput v8, v3, Landroid/view/View$ScrollabilityCache;->state:I

    #@1c
    .line 4069
    :cond_1c
    iput-boolean v1, v3, Landroid/view/View$ScrollabilityCache;->fadeScrollBars:Z

    #@1e
    .line 4072
    const/16 v6, 0x2a

    #@20
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollBarFadeDuration()I

    #@23
    move-result v7

    #@24
    invoke-virtual {p1, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@27
    move-result v6

    #@28
    iput v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBarFadeDuration:I

    #@2a
    .line 4075
    const/16 v6, 0x2b

    #@2c
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollDefaultDelay()I

    #@2f
    move-result v7

    #@30
    invoke-virtual {p1, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@33
    move-result v6

    #@34
    iput v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBarDefaultDelayBeforeFade:I

    #@36
    .line 4080
    iget-object v6, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@38
    invoke-static {v6}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->getScaledScrollBarSize()I

    #@3f
    move-result v6

    #@40
    invoke-virtual {p1, v9, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@43
    move-result v6

    #@44
    iput v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@46
    .line 4084
    const/4 v6, 0x3

    #@47
    invoke-virtual {p1, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@4a
    move-result-object v5

    #@4b
    .line 4085
    .local v5, track:Landroid/graphics/drawable/Drawable;
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@4d
    invoke-virtual {v6, v5}, Landroid/widget/ScrollBarDrawable;->setHorizontalTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    #@50
    .line 4087
    invoke-virtual {p1, v8}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@53
    move-result-object v4

    #@54
    .line 4088
    .local v4, thumb:Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_5b

    #@56
    .line 4089
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@58
    invoke-virtual {v6, v4}, Landroid/widget/ScrollBarDrawable;->setHorizontalThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    #@5b
    .line 4092
    :cond_5b
    const/4 v6, 0x5

    #@5c
    invoke-virtual {p1, v6, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@5f
    move-result v0

    #@60
    .line 4094
    .local v0, alwaysDraw:Z
    if-eqz v0, :cond_67

    #@62
    .line 4095
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@64
    invoke-virtual {v6, v8}, Landroid/widget/ScrollBarDrawable;->setAlwaysDrawHorizontalTrack(Z)V

    #@67
    .line 4098
    :cond_67
    const/4 v6, 0x4

    #@68
    invoke-virtual {p1, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@6b
    move-result-object v5

    #@6c
    .line 4099
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@6e
    invoke-virtual {v6, v5}, Landroid/widget/ScrollBarDrawable;->setVerticalTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    #@71
    .line 4101
    const/4 v6, 0x2

    #@72
    invoke-virtual {p1, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@75
    move-result-object v4

    #@76
    .line 4102
    if-eqz v4, :cond_7d

    #@78
    .line 4103
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@7a
    invoke-virtual {v6, v4}, Landroid/widget/ScrollBarDrawable;->setVerticalThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    #@7d
    .line 4106
    :cond_7d
    const/4 v6, 0x6

    #@7e
    invoke-virtual {p1, v6, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@81
    move-result v0

    #@82
    .line 4108
    if-eqz v0, :cond_89

    #@84
    .line 4109
    iget-object v6, v3, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@86
    invoke-virtual {v6, v8}, Landroid/widget/ScrollBarDrawable;->setAlwaysDrawVerticalTrack(Z)V

    #@89
    .line 4113
    :cond_89
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@8c
    move-result v2

    #@8d
    .line 4114
    .local v2, layoutDirection:I
    if-eqz v5, :cond_92

    #@8f
    .line 4115
    invoke-virtual {v5, v2}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@92
    .line 4117
    :cond_92
    if-eqz v4, :cond_97

    #@94
    .line 4118
    invoke-virtual {v4, v2}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@97
    .line 4122
    :cond_97
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@9a
    .line 4123
    return-void
.end method

.method protected internalSetPadding(IIII)V
    .registers 11
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/high16 v5, 0x100

    #@2
    const/4 v3, 0x0

    #@3
    .line 14871
    iput p1, p0, Landroid/view/View;->mUserPaddingLeft:I

    #@5
    .line 14872
    iput p3, p0, Landroid/view/View;->mUserPaddingRight:I

    #@7
    .line 14873
    iput p4, p0, Landroid/view/View;->mUserPaddingBottom:I

    #@9
    .line 14875
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@b
    .line 14876
    .local v2, viewFlags:I
    const/4 v0, 0x0

    #@c
    .line 14879
    .local v0, changed:Z
    and-int/lit16 v4, v2, 0x300

    #@e
    if-eqz v4, :cond_27

    #@10
    .line 14880
    and-int/lit16 v4, v2, 0x200

    #@12
    if-eqz v4, :cond_1e

    #@14
    .line 14881
    and-int v4, v2, v5

    #@16
    if-nez v4, :cond_49

    #@18
    move v1, v3

    #@19
    .line 14883
    .local v1, offset:I
    :goto_19
    iget v4, p0, Landroid/view/View;->mVerticalScrollbarPosition:I

    #@1b
    packed-switch v4, :pswitch_data_62

    #@1e
    .line 14899
    .end local v1           #offset:I
    :cond_1e
    :goto_1e
    and-int/lit16 v4, v2, 0x100

    #@20
    if-eqz v4, :cond_27

    #@22
    .line 14900
    and-int v4, v2, v5

    #@24
    if-nez v4, :cond_5c

    #@26
    :goto_26
    add-int/2addr p4, v3

    #@27
    .line 14905
    :cond_27
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@29
    if-eq v3, p1, :cond_2e

    #@2b
    .line 14906
    const/4 v0, 0x1

    #@2c
    .line 14907
    iput p1, p0, Landroid/view/View;->mPaddingLeft:I

    #@2e
    .line 14909
    :cond_2e
    iget v3, p0, Landroid/view/View;->mPaddingTop:I

    #@30
    if-eq v3, p2, :cond_35

    #@32
    .line 14910
    const/4 v0, 0x1

    #@33
    .line 14911
    iput p2, p0, Landroid/view/View;->mPaddingTop:I

    #@35
    .line 14913
    :cond_35
    iget v3, p0, Landroid/view/View;->mPaddingRight:I

    #@37
    if-eq v3, p3, :cond_3c

    #@39
    .line 14914
    const/4 v0, 0x1

    #@3a
    .line 14915
    iput p3, p0, Landroid/view/View;->mPaddingRight:I

    #@3c
    .line 14917
    :cond_3c
    iget v3, p0, Landroid/view/View;->mPaddingBottom:I

    #@3e
    if-eq v3, p4, :cond_43

    #@40
    .line 14918
    const/4 v0, 0x1

    #@41
    .line 14919
    iput p4, p0, Landroid/view/View;->mPaddingBottom:I

    #@43
    .line 14922
    :cond_43
    if-eqz v0, :cond_48

    #@45
    .line 14923
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@48
    .line 14925
    :cond_48
    return-void

    #@49
    .line 14881
    :cond_49
    invoke-virtual {p0}, Landroid/view/View;->getVerticalScrollbarWidth()I

    #@4c
    move-result v1

    #@4d
    goto :goto_19

    #@4e
    .line 14885
    .restart local v1       #offset:I
    :pswitch_4e
    invoke-virtual {p0}, Landroid/view/View;->isLayoutRtl()Z

    #@51
    move-result v4

    #@52
    if-eqz v4, :cond_56

    #@54
    .line 14886
    add-int/2addr p1, v1

    #@55
    goto :goto_1e

    #@56
    .line 14888
    :cond_56
    add-int/2addr p3, v1

    #@57
    .line 14890
    goto :goto_1e

    #@58
    .line 14892
    :pswitch_58
    add-int/2addr p3, v1

    #@59
    .line 14893
    goto :goto_1e

    #@5a
    .line 14895
    :pswitch_5a
    add-int/2addr p1, v1

    #@5b
    goto :goto_1e

    #@5c
    .line 14900
    .end local v1           #offset:I
    :cond_5c
    invoke-virtual {p0}, Landroid/view/View;->getHorizontalScrollbarHeight()I

    #@5f
    move-result v3

    #@60
    goto :goto_26

    #@61
    .line 14883
    nop

    #@62
    :pswitch_data_62
    .packed-switch 0x0
        :pswitch_4e
        :pswitch_5a
        :pswitch_58
    .end packed-switch
.end method

.method public invalidate()V
    .registers 2

    #@0
    .prologue
    .line 10507
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/view/View;->invalidate(Z)V

    #@4
    .line 10508
    return-void
.end method

.method public invalidate(IIII)V
    .registers 14
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    const v8, 0x8000

    #@3
    const/high16 v7, -0x8000

    #@5
    .line 10470
    invoke-direct {p0}, Landroid/view/View;->skipInvalidate()Z

    #@8
    move-result v5

    #@9
    if-eqz v5, :cond_c

    #@b
    .line 10498
    :cond_b
    :goto_b
    return-void

    #@c
    .line 10473
    :cond_c
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    and-int/lit8 v5, v5, 0x30

    #@10
    const/16 v6, 0x30

    #@12
    if-eq v5, v6, :cond_1e

    #@14
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@16
    and-int/2addr v5, v8

    #@17
    if-eq v5, v8, :cond_1e

    #@19
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@1b
    and-int/2addr v5, v7

    #@1c
    if-eq v5, v7, :cond_b

    #@1e
    .line 10476
    :cond_1e
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@20
    const v6, -0x8001

    #@23
    and-int/2addr v5, v6

    #@24
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@26
    .line 10477
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@28
    or-int/2addr v5, v7

    #@29
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@2b
    .line 10478
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@2d
    const/high16 v6, 0x20

    #@2f
    or-int/2addr v5, v6

    #@30
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@32
    .line 10479
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@34
    .line 10480
    .local v1, p:Landroid/view/ViewParent;
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@36
    .line 10490
    .local v0, ai:Landroid/view/View$AttachInfo;
    if-eqz v1, :cond_b

    #@38
    if-eqz v0, :cond_b

    #@3a
    if-ge p1, p3, :cond_b

    #@3c
    if-ge p2, p4, :cond_b

    #@3e
    .line 10491
    iget v2, p0, Landroid/view/View;->mScrollX:I

    #@40
    .line 10492
    .local v2, scrollX:I
    iget v3, p0, Landroid/view/View;->mScrollY:I

    #@42
    .line 10493
    .local v3, scrollY:I
    iget-object v4, v0, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@44
    .line 10494
    .local v4, tmpr:Landroid/graphics/Rect;
    sub-int v5, p1, v2

    #@46
    sub-int v6, p2, v3

    #@48
    sub-int v7, p3, v2

    #@4a
    sub-int v8, p4, v3

    #@4c
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@4f
    .line 10495
    invoke-interface {v1, p0, v4}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@52
    goto :goto_b
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .registers 11
    .parameter "dirty"

    #@0
    .prologue
    const v8, 0x8000

    #@3
    const/high16 v7, -0x8000

    #@5
    .line 10427
    invoke-direct {p0}, Landroid/view/View;->skipInvalidate()Z

    #@8
    move-result v5

    #@9
    if-eqz v5, :cond_c

    #@b
    .line 10456
    :cond_b
    :goto_b
    return-void

    #@c
    .line 10430
    :cond_c
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    and-int/lit8 v5, v5, 0x30

    #@10
    const/16 v6, 0x30

    #@12
    if-eq v5, v6, :cond_1e

    #@14
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@16
    and-int/2addr v5, v8

    #@17
    if-eq v5, v8, :cond_1e

    #@19
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@1b
    and-int/2addr v5, v7

    #@1c
    if-eq v5, v7, :cond_b

    #@1e
    .line 10433
    :cond_1e
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@20
    const v6, -0x8001

    #@23
    and-int/2addr v5, v6

    #@24
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@26
    .line 10434
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@28
    or-int/2addr v5, v7

    #@29
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@2b
    .line 10435
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@2d
    const/high16 v6, 0x20

    #@2f
    or-int/2addr v5, v6

    #@30
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@32
    .line 10436
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@34
    .line 10437
    .local v1, p:Landroid/view/ViewParent;
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@36
    .line 10447
    .local v0, ai:Landroid/view/View$AttachInfo;
    if-eqz v1, :cond_b

    #@38
    if-eqz v0, :cond_b

    #@3a
    .line 10448
    iget v3, p0, Landroid/view/View;->mScrollX:I

    #@3c
    .line 10449
    .local v3, scrollX:I
    iget v4, p0, Landroid/view/View;->mScrollY:I

    #@3e
    .line 10450
    .local v4, scrollY:I
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@40
    .line 10451
    .local v2, r:Landroid/graphics/Rect;
    iget v5, p1, Landroid/graphics/Rect;->left:I

    #@42
    sub-int/2addr v5, v3

    #@43
    iget v6, p1, Landroid/graphics/Rect;->top:I

    #@45
    sub-int/2addr v6, v4

    #@46
    iget v7, p1, Landroid/graphics/Rect;->right:I

    #@48
    sub-int/2addr v7, v3

    #@49
    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    #@4b
    sub-int/2addr v8, v4

    #@4c
    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@4f
    .line 10453
    iget-object v5, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@51
    invoke-interface {v5, p0, v2}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@54
    goto :goto_b
.end method

.method invalidate(Z)V
    .registers 10
    .parameter "invalidateCache"

    #@0
    .prologue
    const v7, 0x8000

    #@3
    const/4 v6, 0x0

    #@4
    const/high16 v5, -0x8000

    #@6
    .line 10522
    invoke-direct {p0}, Landroid/view/View;->skipInvalidate()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_d

    #@c
    .line 10555
    :cond_c
    :goto_c
    return-void

    #@d
    .line 10525
    :cond_d
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@f
    and-int/lit8 v3, v3, 0x30

    #@11
    const/16 v4, 0x30

    #@13
    if-eq v3, v4, :cond_29

    #@15
    if-eqz p1, :cond_1c

    #@17
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@19
    and-int/2addr v3, v7

    #@1a
    if-eq v3, v7, :cond_29

    #@1c
    :cond_1c
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@1e
    and-int/2addr v3, v5

    #@1f
    if-ne v3, v5, :cond_29

    #@21
    invoke-virtual {p0}, Landroid/view/View;->isOpaque()Z

    #@24
    move-result v3

    #@25
    iget-boolean v4, p0, Landroid/view/View;->mLastIsOpaque:Z

    #@27
    if-eq v3, v4, :cond_c

    #@29
    .line 10528
    :cond_29
    invoke-virtual {p0}, Landroid/view/View;->isOpaque()Z

    #@2c
    move-result v3

    #@2d
    iput-boolean v3, p0, Landroid/view/View;->mLastIsOpaque:Z

    #@2f
    .line 10529
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@31
    and-int/lit8 v3, v3, -0x21

    #@33
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@35
    .line 10530
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@37
    const/high16 v4, 0x20

    #@39
    or-int/2addr v3, v4

    #@3a
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@3c
    .line 10531
    if-eqz p1, :cond_4b

    #@3e
    .line 10532
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@40
    or-int/2addr v3, v5

    #@41
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@43
    .line 10533
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@45
    const v4, -0x8001

    #@48
    and-int/2addr v3, v4

    #@49
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@4b
    .line 10535
    :cond_4b
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4d
    .line 10536
    .local v0, ai:Landroid/view/View$AttachInfo;
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@4f
    .line 10547
    .local v1, p:Landroid/view/ViewParent;
    if-eqz v1, :cond_c

    #@51
    if-eqz v0, :cond_c

    #@53
    .line 10548
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@55
    .line 10549
    .local v2, r:Landroid/graphics/Rect;
    iget v3, p0, Landroid/view/View;->mRight:I

    #@57
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@59
    sub-int/2addr v3, v4

    #@5a
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@5c
    iget v5, p0, Landroid/view/View;->mTop:I

    #@5e
    sub-int/2addr v4, v5

    #@5f
    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@62
    .line 10552
    invoke-interface {v1, p0, v2}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@65
    goto :goto_c
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 9
    .parameter "drawable"

    #@0
    .prologue
    .line 14373
    invoke-virtual {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_1d

    #@6
    .line 14374
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@9
    move-result-object v0

    #@a
    .line 14375
    .local v0, dirty:Landroid/graphics/Rect;
    iget v1, p0, Landroid/view/View;->mScrollX:I

    #@c
    .line 14376
    .local v1, scrollX:I
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@e
    .line 14378
    .local v2, scrollY:I
    iget v3, v0, Landroid/graphics/Rect;->left:I

    #@10
    add-int/2addr v3, v1

    #@11
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@13
    add-int/2addr v4, v2

    #@14
    iget v5, v0, Landroid/graphics/Rect;->right:I

    #@16
    add-int/2addr v5, v1

    #@17
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    #@19
    add-int/2addr v6, v2

    #@1a
    invoke-virtual {p0, v3, v4, v5, v6}, Landroid/view/View;->invalidate(IIII)V

    #@1d
    .line 14381
    .end local v0           #dirty:Landroid/graphics/Rect;
    .end local v1           #scrollX:I
    .end local v2           #scrollY:I
    :cond_1d
    return-void
.end method

.method protected invalidateParentCaches()V
    .registers 4

    #@0
    .prologue
    .line 10622
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    instance-of v0, v0, Landroid/view/View;

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 10623
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@8
    check-cast v0, Landroid/view/View;

    #@a
    iget v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@c
    const/high16 v2, -0x8000

    #@e
    or-int/2addr v1, v2

    #@f
    iput v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@11
    .line 10625
    :cond_11
    return-void
.end method

.method protected invalidateParentIfNeeded()V
    .registers 3

    #@0
    .prologue
    .line 10637
    invoke-virtual {p0}, Landroid/view/View;->isHardwareAccelerated()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@8
    instance-of v0, v0, Landroid/view/View;

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 10638
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@e
    check-cast v0, Landroid/view/View;

    #@10
    const/4 v1, 0x1

    #@11
    invoke-virtual {v0, v1}, Landroid/view/View;->invalidate(Z)V

    #@14
    .line 10640
    :cond_14
    return-void
.end method

.method invalidateViewProperty(ZZ)V
    .registers 10
    .parameter "invalidateParent"
    .parameter "forceRedraw"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 10574
    iget-object v3, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@3
    if-eqz v3, :cond_d

    #@5
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@7
    and-int/lit8 v3, v3, 0x40

    #@9
    const/16 v4, 0x40

    #@b
    if-ne v3, v4, :cond_1e

    #@d
    .line 10575
    :cond_d
    if-eqz p1, :cond_12

    #@f
    .line 10576
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@12
    .line 10578
    :cond_12
    if-eqz p2, :cond_1a

    #@14
    .line 10579
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@16
    or-int/lit8 v3, v3, 0x20

    #@18
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@1a
    .line 10581
    :cond_1a
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@1d
    .line 10595
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 10583
    :cond_1e
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@20
    .line 10584
    .local v0, ai:Landroid/view/View$AttachInfo;
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@22
    .line 10585
    .local v1, p:Landroid/view/ViewParent;
    if-eqz v1, :cond_1d

    #@24
    if-eqz v0, :cond_1d

    #@26
    .line 10586
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@28
    .line 10587
    .local v2, r:Landroid/graphics/Rect;
    iget v3, p0, Landroid/view/View;->mRight:I

    #@2a
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@2c
    sub-int/2addr v3, v4

    #@2d
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@2f
    iget v5, p0, Landroid/view/View;->mTop:I

    #@31
    sub-int/2addr v4, v5

    #@32
    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@35
    .line 10588
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@37
    instance-of v3, v3, Landroid/view/ViewGroup;

    #@39
    if-eqz v3, :cond_43

    #@3b
    .line 10589
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@3d
    check-cast v3, Landroid/view/ViewGroup;

    #@3f
    invoke-virtual {v3, p0, v2}, Landroid/view/ViewGroup;->invalidateChildFast(Landroid/view/View;Landroid/graphics/Rect;)V

    #@42
    goto :goto_1d

    #@43
    .line 10591
    :cond_43
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@45
    invoke-interface {v3, p0, v2}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@48
    goto :goto_1d
.end method

.method isAccessibilityFocused()Z
    .registers 3

    #@0
    .prologue
    .line 6599
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const/high16 v1, 0x400

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isActionableForAccessibility()Z
    .registers 2

    #@0
    .prologue
    .line 6964
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_12

    #@6
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_12

    #@c
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    :cond_12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public isActivated()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 15169
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x4000

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isClickable()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 6185
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x4000

    #@4
    const/16 v1, 0x4000

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isDirty()Z
    .registers 3

    #@0
    .prologue
    .line 9641
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x60

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isDrawingCacheEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    const v1, 0x8000

    #@3
    .line 12729
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@5
    and-int/2addr v0, v1

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isDuplicateParentStateEnabled()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x40

    #@2
    .line 12397
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isEnabled()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 5853
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isFocusable()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "focus"
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 6381
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@3
    and-int/lit8 v1, v1, 0x1

    #@5
    if-ne v0, v1, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isFocusableInTouchMode()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    const/high16 v1, 0x4

    #@2
    .line 6394
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v1, v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isFocused()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "focus"
    .end annotation

    #@0
    .prologue
    .line 5439
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isHapticFeedbackEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    const/high16 v1, 0x1000

    #@2
    .line 5974
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v1, v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isHardwareAccelerated()Z
    .registers 2

    #@0
    .prologue
    .line 13414
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mHardwareAccelerated:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isHorizontalFadingEdgeEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 11016
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x1000

    #@4
    const/16 v1, 0x1000

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isHorizontalScrollBarEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 11146
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x100

    #@4
    const/16 v1, 0x100

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isHovered()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 8267
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x1000

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isImportantForAccessibility()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 6888
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4
    const/high16 v4, 0x30

    #@6
    and-int/2addr v3, v4

    #@7
    shr-int/lit8 v0, v3, 0x14

    #@9
    .line 6890
    .local v0, mode:I
    packed-switch v0, :pswitch_data_3c

    #@c
    .line 6899
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Unknow important for accessibility mode: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    :pswitch_25
    move v1, v2

    #@26
    .line 6896
    :cond_26
    :goto_26
    :pswitch_26
    return v1

    #@27
    :pswitch_27
    invoke-virtual {p0}, Landroid/view/View;->isActionableForAccessibility()Z

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_39

    #@2d
    invoke-direct {p0}, Landroid/view/View;->hasListenersForAccessibility()Z

    #@30
    move-result v3

    #@31
    if-nez v3, :cond_39

    #@33
    invoke-virtual {p0}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@36
    move-result-object v3

    #@37
    if-eqz v3, :cond_26

    #@39
    :cond_39
    move v1, v2

    #@3a
    goto :goto_26

    #@3b
    .line 6890
    nop

    #@3c
    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_27
        :pswitch_25
        :pswitch_26
    .end packed-switch
.end method

.method public isInEditMode()Z
    .registers 2

    #@0
    .prologue
    .line 13297
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isInScrollingContainer()Z
    .registers 3

    #@0
    .prologue
    .line 8453
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@3
    move-result-object v0

    #@4
    .line 8454
    .local v0, p:Landroid/view/ViewParent;
    :goto_4
    if-eqz v0, :cond_1a

    #@6
    instance-of v1, v0, Landroid/view/ViewGroup;

    #@8
    if-eqz v1, :cond_1a

    #@a
    move-object v1, v0

    #@b
    .line 8455
    check-cast v1, Landroid/view/ViewGroup;

    #@d
    invoke-virtual {v1}, Landroid/view/ViewGroup;->shouldDelayChildPressedState()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_15

    #@13
    .line 8456
    const/4 v1, 0x1

    #@14
    .line 8460
    :goto_14
    return v1

    #@15
    .line 8458
    :cond_15
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    #@18
    move-result-object v0

    #@19
    goto :goto_4

    #@1a
    .line 8460
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_14
.end method

.method public isInTouchMode()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 7838
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 7839
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mInTouchMode:Z

    #@8
    .line 7841
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-static {}, Landroid/view/ViewRootImpl;->isInTouchMode()Z

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method public isLayoutDirectionInherited()Z
    .registers 3

    #@0
    .prologue
    .line 11984
    invoke-virtual {p0}, Landroid/view/View;->getRawLayoutDirection()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isLayoutRequested()Z
    .registers 3

    #@0
    .prologue
    .line 14205
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit16 v0, v0, 0x1000

    #@4
    const/16 v1, 0x1000

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isLayoutRtl()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 6071
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@4
    move-result v1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isLongClickable()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x20

    #@2
    .line 6212
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isOpaque()Z
    .registers 4
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    const/high16 v2, 0x180

    #@2
    const/high16 v1, 0x3f80

    #@4
    .line 10655
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    and-int/2addr v0, v2

    #@7
    if-ne v0, v2, :cond_19

    #@9
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@b
    if-eqz v0, :cond_17

    #@d
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@f
    iget v0, v0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@11
    :goto_11
    cmpl-float v0, v0, v1

    #@13
    if-ltz v0, :cond_19

    #@15
    const/4 v0, 0x1

    #@16
    :goto_16
    return v0

    #@17
    :cond_17
    move v0, v1

    #@18
    goto :goto_11

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_16
.end method

.method protected isPaddingOffsetRequired()Z
    .registers 2

    #@0
    .prologue
    .line 13319
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isPaddingRelative()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, -0x8000

    #@2
    .line 15051
    iget v0, p0, Landroid/view/View;->mUserPaddingStart:I

    #@4
    if-ne v0, v1, :cond_a

    #@6
    iget v0, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@8
    if-eq v0, v1, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method isPaddingResolved()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x2000

    #@2
    .line 12000
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isPressed()Z
    .registers 3

    #@0
    .prologue
    .line 6275
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit16 v0, v0, 0x4000

    #@4
    const/16 v1, 0x4000

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isRootNamespace()Z
    .registers 2

    #@0
    .prologue
    .line 15472
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isSaveEnabled()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x1

    #@2
    .line 6288
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isSaveFromParentEnabled()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x2000

    #@2
    .line 6352
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isScrollContainer()Z
    .registers 3

    #@0
    .prologue
    .line 5463
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x10

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isScrollbarFadingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 11230
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@6
    iget-boolean v0, v0, Landroid/view/View$ScrollabilityCache;->fadeScrollBars:Z

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isSelected()Z
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    .line 15127
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isShown()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5662
    move-object v0, p0

    #@2
    .line 5665
    .local v0, current:Landroid/view/View;
    :cond_2
    iget v3, v0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/lit8 v3, v3, 0xc

    #@6
    if-eqz v3, :cond_9

    #@8
    .line 5678
    :cond_8
    :goto_8
    return v2

    #@9
    .line 5668
    :cond_9
    iget-object v1, v0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@b
    .line 5669
    .local v1, parent:Landroid/view/ViewParent;
    if-eqz v1, :cond_8

    #@d
    .line 5672
    instance-of v3, v1, Landroid/view/View;

    #@f
    if-nez v3, :cond_13

    #@11
    .line 5673
    const/4 v2, 0x1

    #@12
    goto :goto_8

    #@13
    :cond_13
    move-object v0, v1

    #@14
    .line 5675
    check-cast v0, Landroid/view/View;

    #@16
    .line 5676
    if-nez v0, :cond_2

    #@18
    goto :goto_8
.end method

.method public isSoundEffectsEnabled()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    #@0
    .prologue
    const/high16 v1, 0x800

    #@2
    .line 5945
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v1, v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isTextAlignmentInherited()Z
    .registers 2

    #@0
    .prologue
    .line 17277
    invoke-virtual {p0}, Landroid/view/View;->getRawTextAlignment()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isTextDirectionInherited()Z
    .registers 2

    #@0
    .prologue
    .line 17064
    invoke-virtual {p0}, Landroid/view/View;->getRawTextDirection()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isVerticalFadingEdgeEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 11053
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x2000

    #@4
    const/16 v1, 0x2000

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isVerticalScrollBarEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 11176
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x200

    #@4
    const/16 v1, 0x200

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method protected isVerticalScrollBarHidden()Z
    .registers 2

    #@0
    .prologue
    .line 11641
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected isVisibleToUser()Z
    .registers 2

    #@0
    .prologue
    .line 5196
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/view/View;->isVisibleToUser(Landroid/graphics/Rect;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method protected isVisibleToUser(Landroid/graphics/Rect;)Z
    .registers 9
    .parameter "boundInView"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 5217
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    if-eqz v5, :cond_b

    #@5
    .line 5219
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    iget v5, v5, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@9
    if-eqz v5, :cond_c

    #@b
    .line 5247
    :cond_b
    :goto_b
    return v4

    #@c
    .line 5224
    :cond_c
    move-object v0, p0

    #@d
    .line 5225
    :goto_d
    instance-of v5, v0, Landroid/view/View;

    #@f
    if-eqz v5, :cond_26

    #@11
    move-object v2, v0

    #@12
    .line 5226
    check-cast v2, Landroid/view/View;

    #@14
    .line 5229
    .local v2, view:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    #@17
    move-result v5

    #@18
    const/4 v6, 0x0

    #@19
    cmpg-float v5, v5, v6

    #@1b
    if-lez v5, :cond_b

    #@1d
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@20
    move-result v5

    #@21
    if-nez v5, :cond_b

    #@23
    .line 5232
    iget-object v0, v2, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@25
    .line 5233
    .local v0, current:Landroid/view/ViewParent;
    goto :goto_d

    #@26
    .line 5235
    .end local v0           #current:Landroid/view/ViewParent;
    .end local v2           #view:Landroid/view/View;
    :cond_26
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@28
    iget-object v3, v5, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@2a
    .line 5236
    .local v3, visibleRect:Landroid/graphics/Rect;
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2c
    iget-object v1, v5, Landroid/view/View$AttachInfo;->mPoint:Landroid/graphics/Point;

    #@2e
    .line 5237
    .local v1, offset:Landroid/graphics/Point;
    invoke-virtual {p0, v3, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    #@31
    move-result v5

    #@32
    if-eqz v5, :cond_b

    #@34
    .line 5241
    if-eqz p1, :cond_44

    #@36
    .line 5242
    iget v4, v1, Landroid/graphics/Point;->x:I

    #@38
    neg-int v4, v4

    #@39
    iget v5, v1, Landroid/graphics/Point;->y:I

    #@3b
    neg-int v5, v5

    #@3c
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    #@3f
    .line 5243
    invoke-virtual {p1, v3}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    #@42
    move-result v4

    #@43
    goto :goto_b

    #@44
    .line 5245
    :cond_44
    const/4 v4, 0x1

    #@45
    goto :goto_b
.end method

.method public isWindowSplit(Landroid/graphics/Rect;)Z
    .registers 6
    .parameter "outRect"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 7757
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    if-eqz v2, :cond_15

    #@5
    .line 7759
    :try_start_5
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@7
    if-eqz v2, :cond_16

    #@9
    .line 7760
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@b
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@d
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@f
    iget-object v3, v3, Landroid/view/View$AttachInfo;->mWindow:Landroid/view/IWindow;

    #@11
    invoke-interface {v2, v3, p1}, Landroid/view/IWindowSession;->isWindowSplit(Landroid/view/IWindow;Landroid/graphics/Rect;)Z

    #@14
    move-result v1

    #@15
    .line 7769
    :cond_15
    :goto_15
    return v1

    #@16
    .line 7762
    :cond_16
    invoke-virtual {p0, p1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_19} :catch_1a

    #@19
    goto :goto_15

    #@1a
    .line 7765
    :catch_1a
    move-exception v0

    #@1b
    .line 7766
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_15
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 14666
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 14667
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@9
    .line 14669
    :cond_9
    return-void
.end method

.method public layout(IIII)V
    .registers 22
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 14230
    move-object/from16 v0, p0

    #@2
    iget v9, v0, Landroid/view/View;->mLeft:I

    #@4
    .line 14231
    .local v9, oldL:I
    move-object/from16 v0, p0

    #@6
    iget v10, v0, Landroid/view/View;->mTop:I

    #@8
    .line 14232
    .local v10, oldT:I
    move-object/from16 v0, p0

    #@a
    iget v12, v0, Landroid/view/View;->mBottom:I

    #@c
    .line 14233
    .local v12, oldB:I
    move-object/from16 v0, p0

    #@e
    iget v11, v0, Landroid/view/View;->mRight:I

    #@10
    .line 14234
    .local v11, oldR:I
    sget-boolean v1, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@12
    if-nez v1, :cond_1a

    #@14
    .line 14235
    invoke-direct/range {p0 .. p0}, Landroid/view/View;->isRtlLangAndLocaleMetaDataExist()Z

    #@17
    move-result v1

    #@18
    sput-boolean v1, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@1a
    .line 14236
    :cond_1a
    invoke-virtual/range {p0 .. p4}, Landroid/view/View;->setFrame(IIII)Z

    #@1d
    move-result v2

    #@1e
    .line 14237
    .local v2, changed:Z
    if-nez v2, :cond_2a

    #@20
    move-object/from16 v0, p0

    #@22
    iget v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@24
    and-int/lit16 v1, v1, 0x2000

    #@26
    const/16 v3, 0x2000

    #@28
    if-ne v1, v3, :cond_76

    #@2a
    :cond_2a
    move-object/from16 v1, p0

    #@2c
    move/from16 v3, p1

    #@2e
    move/from16 v4, p2

    #@30
    move/from16 v5, p3

    #@32
    move/from16 v6, p4

    #@34
    .line 14238
    invoke-virtual/range {v1 .. v6}, Landroid/view/View;->onLayout(ZIIII)V

    #@37
    .line 14239
    move-object/from16 v0, p0

    #@39
    iget v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@3b
    and-int/lit16 v1, v1, -0x2001

    #@3d
    move-object/from16 v0, p0

    #@3f
    iput v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@41
    .line 14241
    move-object/from16 v0, p0

    #@43
    iget-object v14, v0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@45
    .line 14242
    .local v14, li:Landroid/view/View$ListenerInfo;
    if-eqz v14, :cond_76

    #@47
    invoke-static {v14}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@4a
    move-result-object v1

    #@4b
    if-eqz v1, :cond_76

    #@4d
    .line 14243
    invoke-static {v14}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@54
    move-result-object v15

    #@55
    check-cast v15, Ljava/util/ArrayList;

    #@57
    .line 14245
    .local v15, listenersCopy:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View$OnLayoutChangeListener;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@5a
    move-result v16

    #@5b
    .line 14246
    .local v16, numListeners:I
    const/4 v13, 0x0

    #@5c
    .local v13, i:I
    :goto_5c
    move/from16 v0, v16

    #@5e
    if-ge v13, v0, :cond_76

    #@60
    .line 14247
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@63
    move-result-object v3

    #@64
    check-cast v3, Landroid/view/View$OnLayoutChangeListener;

    #@66
    move-object/from16 v4, p0

    #@68
    move/from16 v5, p1

    #@6a
    move/from16 v6, p2

    #@6c
    move/from16 v7, p3

    #@6e
    move/from16 v8, p4

    #@70
    invoke-interface/range {v3 .. v12}, Landroid/view/View$OnLayoutChangeListener;->onLayoutChange(Landroid/view/View;IIIIIIII)V

    #@73
    .line 14246
    add-int/lit8 v13, v13, 0x1

    #@75
    goto :goto_5c

    #@76
    .line 14251
    .end local v13           #i:I
    .end local v14           #li:Landroid/view/View$ListenerInfo;
    .end local v15           #listenersCopy:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View$OnLayoutChangeListener;>;"
    .end local v16           #numListeners:I
    :cond_76
    move-object/from16 v0, p0

    #@78
    iget v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@7a
    and-int/lit16 v1, v1, -0x1001

    #@7c
    move-object/from16 v0, p0

    #@7e
    iput v1, v0, Landroid/view/View;->mPrivateFlags:I

    #@80
    .line 14252
    return-void
.end method

.method public makeOptionalFitsSystemWindows()V
    .registers 2

    #@0
    .prologue
    const/16 v0, 0x800

    #@2
    .line 5815
    invoke-virtual {p0, v0, v0}, Landroid/view/View;->setFlags(II)V

    #@5
    .line 5816
    return-void
.end method

.method public final measure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 15756
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit16 v0, v0, 0x1000

    #@4
    const/16 v1, 0x1000

    #@6
    if-eq v0, v1, :cond_10

    #@8
    iget v0, p0, Landroid/view/View;->mOldWidthMeasureSpec:I

    #@a
    if-ne p1, v0, :cond_10

    #@c
    iget v0, p0, Landroid/view/View;->mOldHeightMeasureSpec:I

    #@e
    if-eq p2, v0, :cond_33

    #@10
    .line 15761
    :cond_10
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@12
    and-int/lit16 v0, v0, -0x801

    #@14
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@16
    .line 15763
    invoke-virtual {p0}, Landroid/view/View;->resolveRtlPropertiesIfNeeded()V

    #@19
    .line 15766
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    #@1c
    .line 15770
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@1e
    and-int/lit16 v0, v0, 0x800

    #@20
    const/16 v1, 0x800

    #@22
    if-eq v0, v1, :cond_2d

    #@24
    .line 15771
    new-instance v0, Ljava/lang/IllegalStateException;

    #@26
    const-string/jumbo v1, "onMeasure() did not set the measured dimension by calling setMeasuredDimension()"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 15776
    :cond_2d
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2f
    or-int/lit16 v0, v0, 0x2000

    #@31
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@33
    .line 15779
    :cond_33
    iput p1, p0, Landroid/view/View;->mOldWidthMeasureSpec:I

    #@35
    .line 15780
    iput p2, p0, Landroid/view/View;->mOldHeightMeasureSpec:I

    #@37
    .line 15781
    return-void
.end method

.method needGlobalAttributesUpdate(Z)V
    .registers 4
    .parameter "force"

    #@0
    .prologue
    .line 7820
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 7821
    .local v0, ai:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_19

    #@4
    iget-boolean v1, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@6
    if-nez v1, :cond_19

    #@8
    .line 7822
    if-nez p1, :cond_16

    #@a
    iget-boolean v1, v0, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@c
    if-nez v1, :cond_16

    #@e
    iget v1, v0, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@10
    if-nez v1, :cond_16

    #@12
    iget-boolean v1, v0, Landroid/view/View$AttachInfo;->mHasSystemUiListeners:Z

    #@14
    if-eqz v1, :cond_19

    #@16
    .line 7824
    :cond_16
    const/4 v1, 0x1

    #@17
    iput-boolean v1, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@19
    .line 7827
    :cond_19
    return-void
.end method

.method public notifyAccessibilityStateChanged()V
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x800

    #@2
    .line 6995
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_f

    #@e
    .line 7004
    :cond_e
    :goto_e
    return-void

    #@f
    .line 6998
    :cond_f
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@11
    and-int/2addr v0, v1

    #@12
    if-nez v0, :cond_e

    #@14
    .line 6999
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@16
    or-int/2addr v0, v1

    #@17
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@19
    .line 7000
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1b
    if-eqz v0, :cond_e

    #@1d
    .line 7001
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1f
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->childAccessibilityStateChanged(Landroid/view/View;)V

    #@22
    goto :goto_e
.end method

.method public offsetLeftAndRight(I)V
    .registers 12
    .parameter "offset"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 10109
    if-eqz p1, :cond_33

    #@4
    .line 10110
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@7
    .line 10111
    iget-object v7, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    if-eqz v7, :cond_13

    #@b
    iget-object v7, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    invoke-static {v7}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@10
    move-result v7

    #@11
    if-eqz v7, :cond_34

    #@13
    :cond_13
    move v0, v6

    #@14
    .line 10113
    .local v0, matrixIsIdentity:Z
    :goto_14
    if-eqz v0, :cond_5f

    #@16
    .line 10114
    iget-object v7, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@18
    if-eqz v7, :cond_36

    #@1a
    .line 10115
    invoke-virtual {p0, v5, v5}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@1d
    .line 10137
    :cond_1d
    :goto_1d
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@1f
    add-int/2addr v7, p1

    #@20
    iput v7, p0, Landroid/view/View;->mLeft:I

    #@22
    .line 10138
    iget v7, p0, Landroid/view/View;->mRight:I

    #@24
    add-int/2addr v7, p1

    #@25
    iput v7, p0, Landroid/view/View;->mRight:I

    #@27
    .line 10139
    iget-object v7, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@29
    if-eqz v7, :cond_63

    #@2b
    .line 10140
    iget-object v6, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2d
    invoke-virtual {v6, p1}, Landroid/view/DisplayList;->offsetLeftRight(I)V

    #@30
    .line 10141
    invoke-virtual {p0, v5, v5}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@33
    .line 10149
    .end local v0           #matrixIsIdentity:Z
    :cond_33
    :goto_33
    return-void

    #@34
    :cond_34
    move v0, v5

    #@35
    .line 10111
    goto :goto_14

    #@36
    .line 10117
    .restart local v0       #matrixIsIdentity:Z
    :cond_36
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@38
    .line 10118
    .local v3, p:Landroid/view/ViewParent;
    if-eqz v3, :cond_1d

    #@3a
    iget-object v7, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3c
    if-eqz v7, :cond_1d

    #@3e
    .line 10119
    iget-object v7, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    iget-object v4, v7, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@42
    .line 10122
    .local v4, r:Landroid/graphics/Rect;
    if-gez p1, :cond_58

    #@44
    .line 10123
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@46
    add-int v2, v7, p1

    #@48
    .line 10124
    .local v2, minLeft:I
    iget v1, p0, Landroid/view/View;->mRight:I

    #@4a
    .line 10129
    .local v1, maxRight:I
    :goto_4a
    sub-int v7, v1, v2

    #@4c
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@4e
    iget v9, p0, Landroid/view/View;->mTop:I

    #@50
    sub-int/2addr v8, v9

    #@51
    invoke-virtual {v4, v5, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@54
    .line 10130
    invoke-interface {v3, p0, v4}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@57
    goto :goto_1d

    #@58
    .line 10126
    .end local v1           #maxRight:I
    .end local v2           #minLeft:I
    :cond_58
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@5a
    .line 10127
    .restart local v2       #minLeft:I
    iget v7, p0, Landroid/view/View;->mRight:I

    #@5c
    add-int v1, v7, p1

    #@5e
    .restart local v1       #maxRight:I
    goto :goto_4a

    #@5f
    .line 10134
    .end local v1           #maxRight:I
    .end local v2           #minLeft:I
    .end local v3           #p:Landroid/view/ViewParent;
    .end local v4           #r:Landroid/graphics/Rect;
    :cond_5f
    invoke-virtual {p0, v5, v5}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@62
    goto :goto_1d

    #@63
    .line 10143
    :cond_63
    if-nez v0, :cond_68

    #@65
    .line 10144
    invoke-virtual {p0, v5, v6}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@68
    .line 10146
    :cond_68
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@6b
    goto :goto_33
.end method

.method public offsetTopAndBottom(I)V
    .registers 12
    .parameter "offset"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 10058
    if-eqz p1, :cond_33

    #@4
    .line 10059
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@7
    .line 10060
    iget-object v8, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    if-eqz v8, :cond_13

    #@b
    iget-object v8, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    invoke-static {v8}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@10
    move-result v8

    #@11
    if-eqz v8, :cond_34

    #@13
    :cond_13
    move v0, v7

    #@14
    .line 10062
    .local v0, matrixIsIdentity:Z
    :goto_14
    if-eqz v0, :cond_61

    #@16
    .line 10063
    iget-object v8, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@18
    if-eqz v8, :cond_36

    #@1a
    .line 10064
    invoke-virtual {p0, v6, v6}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@1d
    .line 10089
    :cond_1d
    :goto_1d
    iget v8, p0, Landroid/view/View;->mTop:I

    #@1f
    add-int/2addr v8, p1

    #@20
    iput v8, p0, Landroid/view/View;->mTop:I

    #@22
    .line 10090
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@24
    add-int/2addr v8, p1

    #@25
    iput v8, p0, Landroid/view/View;->mBottom:I

    #@27
    .line 10091
    iget-object v8, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@29
    if-eqz v8, :cond_65

    #@2b
    .line 10092
    iget-object v7, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2d
    invoke-virtual {v7, p1}, Landroid/view/DisplayList;->offsetTopBottom(I)V

    #@30
    .line 10093
    invoke-virtual {p0, v6, v6}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@33
    .line 10101
    .end local v0           #matrixIsIdentity:Z
    :cond_33
    :goto_33
    return-void

    #@34
    :cond_34
    move v0, v6

    #@35
    .line 10060
    goto :goto_14

    #@36
    .line 10066
    .restart local v0       #matrixIsIdentity:Z
    :cond_36
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@38
    .line 10067
    .local v3, p:Landroid/view/ViewParent;
    if-eqz v3, :cond_1d

    #@3a
    iget-object v8, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3c
    if-eqz v8, :cond_1d

    #@3e
    .line 10068
    iget-object v8, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@40
    iget-object v4, v8, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@42
    .line 10072
    .local v4, r:Landroid/graphics/Rect;
    if-gez p1, :cond_59

    #@44
    .line 10073
    iget v8, p0, Landroid/view/View;->mTop:I

    #@46
    add-int v2, v8, p1

    #@48
    .line 10074
    .local v2, minTop:I
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@4a
    .line 10075
    .local v1, maxBottom:I
    move v5, p1

    #@4b
    .line 10081
    .local v5, yLoc:I
    :goto_4b
    iget v8, p0, Landroid/view/View;->mRight:I

    #@4d
    iget v9, p0, Landroid/view/View;->mLeft:I

    #@4f
    sub-int/2addr v8, v9

    #@50
    sub-int v9, v1, v2

    #@52
    invoke-virtual {v4, v6, v5, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    #@55
    .line 10082
    invoke-interface {v3, p0, v4}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@58
    goto :goto_1d

    #@59
    .line 10077
    .end local v1           #maxBottom:I
    .end local v2           #minTop:I
    .end local v5           #yLoc:I
    :cond_59
    iget v2, p0, Landroid/view/View;->mTop:I

    #@5b
    .line 10078
    .restart local v2       #minTop:I
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@5d
    add-int v1, v8, p1

    #@5f
    .line 10079
    .restart local v1       #maxBottom:I
    const/4 v5, 0x0

    #@60
    .restart local v5       #yLoc:I
    goto :goto_4b

    #@61
    .line 10086
    .end local v1           #maxBottom:I
    .end local v2           #minTop:I
    .end local v3           #p:Landroid/view/ViewParent;
    .end local v4           #r:Landroid/graphics/Rect;
    .end local v5           #yLoc:I
    :cond_61
    invoke-virtual {p0, v6, v6}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@64
    goto :goto_1d

    #@65
    .line 10095
    :cond_65
    if-nez v0, :cond_6a

    #@67
    .line 10096
    invoke-virtual {p0, v6, v7}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@6a
    .line 10098
    :cond_6a
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@6d
    goto :goto_33
.end method

.method protected onAnimationEnd()V
    .registers 3

    #@0
    .prologue
    .line 16108
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const v1, -0x10001

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    .line 16109
    return-void
.end method

.method protected onAnimationStart()V
    .registers 3

    #@0
    .prologue
    .line 16096
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x1

    #@4
    or-int/2addr v0, v1

    #@5
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@7
    .line 16097
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 11717
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    and-int/lit16 v1, v1, 0x200

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 11718
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@8
    invoke-interface {v1, p0}, Landroid/view/ViewParent;->requestTransparentRegion(Landroid/view/View;)V

    #@b
    .line 11721
    :cond_b
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    const/high16 v2, 0x800

    #@f
    and-int/2addr v1, v2

    #@10
    if-eqz v1, :cond_1d

    #@12
    .line 11722
    invoke-direct {p0}, Landroid/view/View;->initialAwakenScrollBars()Z

    #@15
    .line 11723
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@17
    const v2, -0x8000001

    #@1a
    and-int/2addr v1, v2

    #@1b
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@1d
    .line 11726
    :cond_1d
    invoke-virtual {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@20
    .line 11728
    invoke-virtual {p0}, Landroid/view/View;->clearAccessibilityFocus()V

    #@23
    .line 11729
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_30

    #@29
    .line 11730
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@2c
    move-result-object v0

    #@2d
    .line 11731
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->focusIn(Landroid/view/View;)V

    #@30
    .line 11734
    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_30
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@32
    if-eqz v1, :cond_41

    #@34
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@36
    if-eqz v1, :cond_41

    #@38
    .line 11735
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3a
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@3c
    iget-object v2, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@3e
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl;->dequeueDisplayList(Landroid/view/DisplayList;)V

    #@41
    .line 11737
    :cond_41
    return-void
.end method

.method public onCheckIsTextEditor()Z
    .registers 2

    #@0
    .prologue
    .line 8009
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onCloseSystemDialogs(Ljava/lang/String;)V
    .registers 2
    .parameter "reason"

    #@0
    .prologue
    .line 16646
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 7796
    return-void
.end method

.method protected onCreateContextMenu(Landroid/view/ContextMenu;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 8095
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 10
    .parameter "extraSpace"

    #@0
    .prologue
    const/high16 v7, 0x40

    #@2
    const/4 v6, 0x0

    #@3
    .line 14574
    iget v5, p0, Landroid/view/View;->mViewFlags:I

    #@5
    and-int/2addr v5, v7

    #@6
    if-ne v5, v7, :cond_17

    #@8
    iget-object v5, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@a
    instance-of v5, v5, Landroid/view/View;

    #@c
    if-eqz v5, :cond_17

    #@e
    .line 14576
    iget-object v5, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@10
    check-cast v5, Landroid/view/View;

    #@12
    invoke-virtual {v5, p1}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@15
    move-result-object v0

    #@16
    .line 14631
    :cond_16
    :goto_16
    return-object v0

    #@17
    .line 14581
    :cond_17
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@19
    .line 14583
    .local v2, privateFlags:I
    const/4 v4, 0x0

    #@1a
    .line 14584
    .local v4, viewStateIndex:I
    and-int/lit16 v5, v2, 0x4000

    #@1c
    if-eqz v5, :cond_20

    #@1e
    or-int/lit8 v4, v4, 0x10

    #@20
    .line 14585
    :cond_20
    iget v5, p0, Landroid/view/View;->mViewFlags:I

    #@22
    and-int/lit8 v5, v5, 0x20

    #@24
    if-nez v5, :cond_28

    #@26
    or-int/lit8 v4, v4, 0x8

    #@28
    .line 14587
    :cond_28
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_36

    #@2e
    invoke-virtual {p0}, Landroid/view/View;->hasWindowFocus()Z

    #@31
    move-result v5

    #@32
    if-eqz v5, :cond_36

    #@34
    or-int/lit8 v4, v4, 0x4

    #@36
    .line 14589
    :cond_36
    and-int/lit8 v5, v2, 0x4

    #@38
    if-eqz v5, :cond_3c

    #@3a
    or-int/lit8 v4, v4, 0x2

    #@3c
    .line 14590
    :cond_3c
    invoke-virtual {p0}, Landroid/view/View;->hasWindowFocus()Z

    #@3f
    move-result v5

    #@40
    if-eqz v5, :cond_44

    #@42
    or-int/lit8 v4, v4, 0x1

    #@44
    .line 14591
    :cond_44
    const/high16 v5, 0x4000

    #@46
    and-int/2addr v5, v2

    #@47
    if-eqz v5, :cond_4b

    #@49
    or-int/lit8 v4, v4, 0x20

    #@4b
    .line 14592
    :cond_4b
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4d
    if-eqz v5, :cond_5d

    #@4f
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@51
    iget-boolean v5, v5, Landroid/view/View$AttachInfo;->mHardwareAccelerationRequested:Z

    #@53
    if-eqz v5, :cond_5d

    #@55
    invoke-static {}, Landroid/view/HardwareRenderer;->isAvailable()Z

    #@58
    move-result v5

    #@59
    if-eqz v5, :cond_5d

    #@5b
    .line 14597
    or-int/lit8 v4, v4, 0x40

    #@5d
    .line 14599
    :cond_5d
    const/high16 v5, 0x1000

    #@5f
    and-int/2addr v5, v2

    #@60
    if-eqz v5, :cond_64

    #@62
    or-int/lit16 v4, v4, 0x80

    #@64
    .line 14601
    :cond_64
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@66
    .line 14602
    .local v3, privateFlags2:I
    and-int/lit8 v5, v3, 0x1

    #@68
    if-eqz v5, :cond_6c

    #@6a
    or-int/lit16 v4, v4, 0x100

    #@6c
    .line 14603
    :cond_6c
    and-int/lit8 v5, v3, 0x2

    #@6e
    if-eqz v5, :cond_72

    #@70
    or-int/lit16 v4, v4, 0x200

    #@72
    .line 14605
    :cond_72
    sget-object v5, Landroid/view/View;->VIEW_STATE_SETS:[[I

    #@74
    aget-object v0, v5, v4

    #@76
    .line 14619
    .local v0, drawableState:[I
    if-eqz p1, :cond_16

    #@78
    .line 14624
    if-eqz v0, :cond_84

    #@7a
    .line 14625
    array-length v5, v0

    #@7b
    add-int/2addr v5, p1

    #@7c
    new-array v1, v5, [I

    #@7e
    .line 14626
    .local v1, fullState:[I
    array-length v5, v0

    #@7f
    invoke-static {v0, v6, v1, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@82
    :goto_82
    move-object v0, v1

    #@83
    .line 14631
    goto :goto_16

    #@84
    .line 14628
    .end local v1           #fullState:[I
    :cond_84
    new-array v1, p1, [I

    #@86
    .restart local v1       #fullState:[I
    goto :goto_82
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .registers 3
    .parameter "outAttrs"

    #@0
    .prologue
    .line 8025
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 12064
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const v1, -0x4000001

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    .line 12066
    invoke-direct {p0}, Landroid/view/View;->removeUnsetPressCallback()V

    #@b
    .line 12067
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@e
    .line 12068
    invoke-direct {p0}, Landroid/view/View;->removePerformClickCallback()V

    #@11
    .line 12069
    invoke-direct {p0}, Landroid/view/View;->removeSendViewScrolledAccessibilityEventCallback()V

    #@14
    .line 12071
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    #@17
    .line 12073
    const/4 v0, 0x0

    #@18
    invoke-virtual {p0, v0}, Landroid/view/View;->destroyLayer(Z)Z

    #@1b
    .line 12075
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1d
    if-eqz v0, :cond_3a

    #@1f
    .line 12076
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@21
    if-eqz v0, :cond_2c

    #@23
    .line 12077
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@25
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@27
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@29
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->enqueueDisplayList(Landroid/view/DisplayList;)V

    #@2c
    .line 12079
    :cond_2c
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2e
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@30
    invoke-virtual {v0, p0}, Landroid/view/ViewRootImpl;->cancelInvalidate(Landroid/view/View;)V

    #@33
    .line 12085
    :goto_33
    const/4 v0, 0x0

    #@34
    iput-object v0, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@36
    .line 12087
    invoke-virtual {p0}, Landroid/view/View;->resetAccessibilityStateChanged()V

    #@39
    .line 12088
    return-void

    #@3a
    .line 12082
    :cond_3a
    invoke-direct {p0}, Landroid/view/View;->clearDisplayList()V

    #@3d
    goto :goto_33
.end method

.method protected onDisplayHint(I)V
    .registers 2
    .parameter "hint"

    #@0
    .prologue
    .line 7662
    return-void
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 16607
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 11690
    return-void
.end method

.method protected onDrawHorizontalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .registers 7
    .parameter "canvas"
    .parameter "scrollBar"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 11660
    invoke-virtual {p2, p3, p4, p5, p6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@3
    .line 11661
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@6
    .line 11662
    return-void
.end method

.method protected final onDrawScrollBars(Landroid/graphics/Canvas;)V
    .registers 28
    .parameter "canvas"

    #@0
    .prologue
    .line 11520
    move-object/from16 v0, p0

    #@2
    iget-object v9, v0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@4
    .line 11521
    .local v9, cache:Landroid/view/View$ScrollabilityCache;
    if-eqz v9, :cond_c

    #@6
    .line 11523
    iget v0, v9, Landroid/view/View$ScrollabilityCache;->state:I

    #@8
    move/from16 v18, v0

    #@a
    .line 11525
    .local v18, state:I
    if-nez v18, :cond_d

    #@c
    .line 11632
    .end local v18           #state:I
    :cond_c
    :goto_c
    return-void

    #@d
    .line 11529
    .restart local v18       #state:I
    :cond_d
    const/4 v14, 0x0

    #@e
    .line 11531
    .local v14, invalidate:Z
    const/4 v2, 0x2

    #@f
    move/from16 v0, v18

    #@11
    if-ne v0, v2, :cond_13d

    #@13
    .line 11533
    iget-object v2, v9, Landroid/view/View$ScrollabilityCache;->interpolatorValues:[F

    #@15
    if-nez v2, :cond_1c

    #@17
    .line 11534
    const/4 v2, 0x1

    #@18
    new-array v2, v2, [F

    #@1a
    iput-object v2, v9, Landroid/view/View$ScrollabilityCache;->interpolatorValues:[F

    #@1c
    .line 11537
    :cond_1c
    iget-object v0, v9, Landroid/view/View$ScrollabilityCache;->interpolatorValues:[F

    #@1e
    move-object/from16 v19, v0

    #@20
    .line 11540
    .local v19, values:[F
    iget-object v2, v9, Landroid/view/View$ScrollabilityCache;->scrollBarInterpolator:Landroid/graphics/Interpolator;

    #@22
    move-object/from16 v0, v19

    #@24
    invoke-virtual {v2, v0}, Landroid/graphics/Interpolator;->timeToValues([F)Landroid/graphics/Interpolator$Result;

    #@27
    move-result-object v2

    #@28
    sget-object v3, Landroid/graphics/Interpolator$Result;->FREEZE_END:Landroid/graphics/Interpolator$Result;

    #@2a
    if-ne v2, v3, :cond_12f

    #@2c
    .line 11542
    const/4 v2, 0x0

    #@2d
    iput v2, v9, Landroid/view/View$ScrollabilityCache;->state:I

    #@2f
    .line 11550
    :goto_2f
    const/4 v14, 0x1

    #@30
    .line 11558
    .end local v19           #values:[F
    :goto_30
    move-object/from16 v0, p0

    #@32
    iget v0, v0, Landroid/view/View;->mViewFlags:I

    #@34
    move/from16 v22, v0

    #@36
    .line 11560
    .local v22, viewFlags:I
    move/from16 v0, v22

    #@38
    and-int/lit16 v2, v0, 0x100

    #@3a
    const/16 v3, 0x100

    #@3c
    if-ne v2, v3, :cond_146

    #@3e
    const/4 v10, 0x1

    #@3f
    .line 11562
    .local v10, drawHorizontalScrollBar:Z
    :goto_3f
    move/from16 v0, v22

    #@41
    and-int/lit16 v2, v0, 0x200

    #@43
    const/16 v3, 0x200

    #@45
    if-ne v2, v3, :cond_149

    #@47
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->isVerticalScrollBarHidden()Z

    #@4a
    move-result v2

    #@4b
    if-nez v2, :cond_149

    #@4d
    const/4 v11, 0x1

    #@4e
    .line 11566
    .local v11, drawVerticalScrollBar:Z
    :goto_4e
    if-nez v11, :cond_52

    #@50
    if-eqz v10, :cond_c

    #@52
    .line 11567
    :cond_52
    move-object/from16 v0, p0

    #@54
    iget v2, v0, Landroid/view/View;->mRight:I

    #@56
    move-object/from16 v0, p0

    #@58
    iget v3, v0, Landroid/view/View;->mLeft:I

    #@5a
    sub-int v23, v2, v3

    #@5c
    .line 11568
    .local v23, width:I
    move-object/from16 v0, p0

    #@5e
    iget v2, v0, Landroid/view/View;->mBottom:I

    #@60
    move-object/from16 v0, p0

    #@62
    iget v3, v0, Landroid/view/View;->mTop:I

    #@64
    sub-int v12, v2, v3

    #@66
    .line 11570
    .local v12, height:I
    iget-object v4, v9, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@68
    .line 11572
    .local v4, scrollBar:Landroid/widget/ScrollBarDrawable;
    move-object/from16 v0, p0

    #@6a
    iget v15, v0, Landroid/view/View;->mScrollX:I

    #@6c
    .line 11573
    .local v15, scrollX:I
    move-object/from16 v0, p0

    #@6e
    iget v0, v0, Landroid/view/View;->mScrollY:I

    #@70
    move/from16 v16, v0

    #@72
    .line 11574
    .local v16, scrollY:I
    const/high16 v2, 0x200

    #@74
    and-int v2, v2, v22

    #@76
    if-nez v2, :cond_14c

    #@78
    const/4 v13, -0x1

    #@79
    .line 11578
    .local v13, inside:I
    :goto_79
    if-eqz v10, :cond_cd

    #@7b
    .line 11579
    const/4 v2, 0x0

    #@7c
    invoke-virtual {v4, v2}, Landroid/widget/ScrollBarDrawable;->getSize(Z)I

    #@7f
    move-result v17

    #@80
    .line 11580
    .local v17, size:I
    if-gtz v17, :cond_86

    #@82
    .line 11581
    iget v0, v9, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@84
    move/from16 v17, v0

    #@86
    .line 11584
    :cond_86
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeHorizontalScrollRange()I

    #@89
    move-result v2

    #@8a
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeHorizontalScrollOffset()I

    #@8d
    move-result v3

    #@8e
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeHorizontalScrollExtent()I

    #@91
    move-result v24

    #@92
    const/16 v25, 0x0

    #@94
    move/from16 v0, v24

    #@96
    move/from16 v1, v25

    #@98
    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/widget/ScrollBarDrawable;->setParameters(IIIZ)V

    #@9b
    .line 11587
    if-eqz v11, :cond_14f

    #@9d
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getVerticalScrollbarWidth()I

    #@a0
    move-result v20

    #@a1
    .line 11589
    .local v20, verticalScrollBarGap:I
    :goto_a1
    add-int v2, v16, v12

    #@a3
    sub-int v2, v2, v17

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget v3, v0, Landroid/view/View;->mUserPaddingBottom:I

    #@a9
    and-int/2addr v3, v13

    #@aa
    sub-int v6, v2, v3

    #@ac
    .line 11590
    .local v6, top:I
    move-object/from16 v0, p0

    #@ae
    iget v2, v0, Landroid/view/View;->mPaddingLeft:I

    #@b0
    and-int/2addr v2, v13

    #@b1
    add-int v5, v15, v2

    #@b3
    .line 11591
    .local v5, left:I
    add-int v2, v15, v23

    #@b5
    move-object/from16 v0, p0

    #@b7
    iget v3, v0, Landroid/view/View;->mUserPaddingRight:I

    #@b9
    and-int/2addr v3, v13

    #@ba
    sub-int/2addr v2, v3

    #@bb
    sub-int v7, v2, v20

    #@bd
    .line 11592
    .local v7, right:I
    add-int v8, v6, v17

    #@bf
    .local v8, bottom:I
    move-object/from16 v2, p0

    #@c1
    move-object/from16 v3, p1

    #@c3
    .line 11593
    invoke-virtual/range {v2 .. v8}, Landroid/view/View;->onDrawHorizontalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    #@c6
    .line 11594
    if-eqz v14, :cond_cd

    #@c8
    .line 11595
    move-object/from16 v0, p0

    #@ca
    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->invalidate(IIII)V

    #@cd
    .line 11599
    .end local v5           #left:I
    .end local v6           #top:I
    .end local v7           #right:I
    .end local v8           #bottom:I
    .end local v17           #size:I
    .end local v20           #verticalScrollBarGap:I
    :cond_cd
    if-eqz v11, :cond_c

    #@cf
    .line 11600
    const/4 v2, 0x1

    #@d0
    invoke-virtual {v4, v2}, Landroid/widget/ScrollBarDrawable;->getSize(Z)I

    #@d3
    move-result v17

    #@d4
    .line 11601
    .restart local v17       #size:I
    if-gtz v17, :cond_da

    #@d6
    .line 11602
    iget v0, v9, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@d8
    move/from16 v17, v0

    #@da
    .line 11605
    :cond_da
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeVerticalScrollRange()I

    #@dd
    move-result v2

    #@de
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeVerticalScrollOffset()I

    #@e1
    move-result v3

    #@e2
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeVerticalScrollExtent()I

    #@e5
    move-result v24

    #@e6
    const/16 v25, 0x1

    #@e8
    move/from16 v0, v24

    #@ea
    move/from16 v1, v25

    #@ec
    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/widget/ScrollBarDrawable;->setParameters(IIIZ)V

    #@ef
    .line 11608
    move-object/from16 v0, p0

    #@f1
    iget v0, v0, Landroid/view/View;->mVerticalScrollbarPosition:I

    #@f3
    move/from16 v21, v0

    #@f5
    .line 11609
    .local v21, verticalScrollbarPosition:I
    if-nez v21, :cond_ff

    #@f7
    .line 11610
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->isLayoutRtl()Z

    #@fa
    move-result v2

    #@fb
    if-eqz v2, :cond_153

    #@fd
    const/16 v21, 0x1

    #@ff
    .line 11613
    :cond_ff
    :goto_ff
    packed-switch v21, :pswitch_data_15e

    #@102
    .line 11616
    add-int v2, v15, v23

    #@104
    sub-int v2, v2, v17

    #@106
    move-object/from16 v0, p0

    #@108
    iget v3, v0, Landroid/view/View;->mUserPaddingRight:I

    #@10a
    and-int/2addr v3, v13

    #@10b
    sub-int v5, v2, v3

    #@10d
    .line 11622
    .restart local v5       #left:I
    :goto_10d
    move-object/from16 v0, p0

    #@10f
    iget v2, v0, Landroid/view/View;->mPaddingTop:I

    #@111
    and-int/2addr v2, v13

    #@112
    add-int v6, v16, v2

    #@114
    .line 11623
    .restart local v6       #top:I
    add-int v7, v5, v17

    #@116
    .line 11624
    .restart local v7       #right:I
    add-int v2, v16, v12

    #@118
    move-object/from16 v0, p0

    #@11a
    iget v3, v0, Landroid/view/View;->mUserPaddingBottom:I

    #@11c
    and-int/2addr v3, v13

    #@11d
    sub-int v8, v2, v3

    #@11f
    .restart local v8       #bottom:I
    move-object/from16 v2, p0

    #@121
    move-object/from16 v3, p1

    #@123
    .line 11625
    invoke-virtual/range {v2 .. v8}, Landroid/view/View;->onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    #@126
    .line 11626
    if-eqz v14, :cond_c

    #@128
    .line 11627
    move-object/from16 v0, p0

    #@12a
    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->invalidate(IIII)V

    #@12d
    goto/16 :goto_c

    #@12f
    .line 11544
    .end local v4           #scrollBar:Landroid/widget/ScrollBarDrawable;
    .end local v5           #left:I
    .end local v6           #top:I
    .end local v7           #right:I
    .end local v8           #bottom:I
    .end local v10           #drawHorizontalScrollBar:Z
    .end local v11           #drawVerticalScrollBar:Z
    .end local v12           #height:I
    .end local v13           #inside:I
    .end local v15           #scrollX:I
    .end local v16           #scrollY:I
    .end local v17           #size:I
    .end local v21           #verticalScrollbarPosition:I
    .end local v22           #viewFlags:I
    .end local v23           #width:I
    .restart local v19       #values:[F
    :cond_12f
    iget-object v2, v9, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@131
    const/4 v3, 0x0

    #@132
    aget v3, v19, v3

    #@134
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    #@137
    move-result v3

    #@138
    invoke-virtual {v2, v3}, Landroid/widget/ScrollBarDrawable;->setAlpha(I)V

    #@13b
    goto/16 :goto_2f

    #@13d
    .line 11554
    .end local v19           #values:[F
    :cond_13d
    iget-object v2, v9, Landroid/view/View$ScrollabilityCache;->scrollBar:Landroid/widget/ScrollBarDrawable;

    #@13f
    const/16 v3, 0xff

    #@141
    invoke-virtual {v2, v3}, Landroid/widget/ScrollBarDrawable;->setAlpha(I)V

    #@144
    goto/16 :goto_30

    #@146
    .line 11560
    .restart local v22       #viewFlags:I
    :cond_146
    const/4 v10, 0x0

    #@147
    goto/16 :goto_3f

    #@149
    .line 11562
    .restart local v10       #drawHorizontalScrollBar:Z
    :cond_149
    const/4 v11, 0x0

    #@14a
    goto/16 :goto_4e

    #@14c
    .line 11574
    .restart local v4       #scrollBar:Landroid/widget/ScrollBarDrawable;
    .restart local v11       #drawVerticalScrollBar:Z
    .restart local v12       #height:I
    .restart local v15       #scrollX:I
    .restart local v16       #scrollY:I
    .restart local v23       #width:I
    :cond_14c
    const/4 v13, 0x0

    #@14d
    goto/16 :goto_79

    #@14f
    .line 11587
    .restart local v13       #inside:I
    .restart local v17       #size:I
    :cond_14f
    const/16 v20, 0x0

    #@151
    goto/16 :goto_a1

    #@153
    .line 11610
    .restart local v21       #verticalScrollbarPosition:I
    :cond_153
    const/16 v21, 0x2

    #@155
    goto :goto_ff

    #@156
    .line 11619
    :pswitch_156
    move-object/from16 v0, p0

    #@158
    iget v2, v0, Landroid/view/View;->mUserPaddingLeft:I

    #@15a
    and-int/2addr v2, v13

    #@15b
    add-int v5, v15, v2

    #@15d
    .restart local v5       #left:I
    goto :goto_10d

    #@15e
    .line 11613
    :pswitch_data_15e
    .packed-switch 0x1
        :pswitch_156
    .end packed-switch
.end method

.method protected onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .registers 7
    .parameter "canvas"
    .parameter "scrollBar"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 11680
    invoke-virtual {p2, p3, p4, p5, p6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@3
    .line 11681
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@6
    .line 11682
    return-void
.end method

.method public onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 7396
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x400

    #@4
    if-eqz v0, :cond_10

    #@6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getFlags()I

    #@9
    move-result v0

    #@a
    and-int/lit8 v0, v0, 0x1

    #@c
    if-eqz v0, :cond_10

    #@e
    .line 7399
    const/4 v0, 0x0

    #@f
    .line 7401
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    goto :goto_f
.end method

.method protected onFinishInflate()V
    .registers 1

    #@0
    .prologue
    .line 14356
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .registers 1

    #@0
    .prologue
    .line 7284
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .registers 7
    .parameter "gainFocus"
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 4690
    if-eqz p1, :cond_13

    #@2
    .line 4691
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_13

    #@e
    .line 4692
    const/16 v2, 0x8

    #@10
    invoke-virtual {p0, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@13
    .line 4696
    :cond_13
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@16
    move-result-object v0

    #@17
    .line 4697
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-nez p1, :cond_52

    #@19
    .line 4698
    invoke-virtual {p0}, Landroid/view/View;->isPressed()Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_23

    #@1f
    .line 4699
    const/4 v2, 0x0

    #@20
    invoke-virtual {p0, v2}, Landroid/view/View;->setPressed(Z)V

    #@23
    .line 4701
    :cond_23
    if-eqz v0, :cond_32

    #@25
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@27
    if-eqz v2, :cond_32

    #@29
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2b
    iget-boolean v2, v2, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@2d
    if-eqz v2, :cond_32

    #@2f
    .line 4703
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->focusOut(Landroid/view/View;)V

    #@32
    .line 4705
    :cond_32
    invoke-virtual {p0}, Landroid/view/View;->onFocusLost()V

    #@35
    .line 4711
    :cond_35
    :goto_35
    const/4 v2, 0x1

    #@36
    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Z)V

    #@39
    .line 4712
    iget-object v1, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@3b
    .line 4713
    .local v1, li:Landroid/view/View$ListenerInfo;
    if-eqz v1, :cond_46

    #@3d
    iget-object v2, v1, Landroid/view/View$ListenerInfo;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    #@3f
    if-eqz v2, :cond_46

    #@41
    .line 4714
    iget-object v2, v1, Landroid/view/View$ListenerInfo;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    #@43
    invoke-interface {v2, p0, p1}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    #@46
    .line 4717
    :cond_46
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@48
    if-eqz v2, :cond_51

    #@4a
    .line 4718
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4c
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mKeyDispatchState:Landroid/view/KeyEvent$DispatcherState;

    #@4e
    invoke-virtual {v2, p0}, Landroid/view/KeyEvent$DispatcherState;->reset(Ljava/lang/Object;)V

    #@51
    .line 4720
    :cond_51
    return-void

    #@52
    .line 4706
    .end local v1           #li:Landroid/view/View$ListenerInfo;
    :cond_52
    if-eqz v0, :cond_35

    #@54
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@56
    if-eqz v2, :cond_35

    #@58
    iget-object v2, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5a
    iget-boolean v2, v2, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@5c
    if-eqz v2, :cond_35

    #@5e
    .line 4708
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->focusIn(Landroid/view/View;)V

    #@61
    goto :goto_35
.end method

.method protected onFocusLost()V
    .registers 1

    #@0
    .prologue
    .line 5415
    invoke-direct {p0}, Landroid/view/View;->resetPressedState()V

    #@3
    .line 5416
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 8151
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onHoverChanged(Z)V
    .registers 2
    .parameter "hovered"

    #@0
    .prologue
    .line 8314
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 8194
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@6
    move-result v0

    #@7
    .line 8195
    .local v0, action:I
    iget-boolean v3, p0, Landroid/view/View;->mSendingHoverAccessibilityEvents:Z

    #@9
    if-nez v3, :cond_3a

    #@b
    .line 8196
    const/16 v3, 0x9

    #@d
    if-eq v0, v3, :cond_12

    #@f
    const/4 v3, 0x7

    #@10
    if-ne v0, v3, :cond_2d

    #@12
    :cond_12
    invoke-virtual {p0}, Landroid/view/View;->hasHoveredChild()Z

    #@15
    move-result v3

    #@16
    if-nez v3, :cond_2d

    #@18
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@1b
    move-result v3

    #@1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@1f
    move-result v4

    #@20
    invoke-virtual {p0, v3, v4}, Landroid/view/View;->pointInView(FF)Z

    #@23
    move-result v3

    #@24
    if-eqz v3, :cond_2d

    #@26
    .line 8200
    const/16 v3, 0x80

    #@28
    invoke-direct {p0, v3}, Landroid/view/View;->sendAccessibilityHoverEvent(I)V

    #@2b
    .line 8201
    iput-boolean v1, p0, Landroid/view/View;->mSendingHoverAccessibilityEvents:Z

    #@2d
    .line 8217
    :cond_2d
    :goto_2d
    invoke-direct {p0}, Landroid/view/View;->isHoverable()Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_70

    #@33
    .line 8218
    packed-switch v0, :pswitch_data_72

    #@36
    .line 8234
    :goto_36
    invoke-direct {p0, p1}, Landroid/view/View;->dispatchGenericMotionEventInternal(Landroid/view/MotionEvent;)Z

    #@39
    .line 8238
    :goto_39
    return v1

    #@3a
    .line 8204
    :cond_3a
    const/16 v3, 0xa

    #@3c
    if-eq v0, v3, :cond_4f

    #@3e
    const/4 v3, 0x2

    #@3f
    if-ne v0, v3, :cond_2d

    #@41
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@44
    move-result v3

    #@45
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@48
    move-result v4

    #@49
    invoke-virtual {p0, v3, v4}, Landroid/view/View;->pointInView(FF)Z

    #@4c
    move-result v3

    #@4d
    if-nez v3, :cond_2d

    #@4f
    .line 8207
    :cond_4f
    iput-boolean v2, p0, Landroid/view/View;->mSendingHoverAccessibilityEvents:Z

    #@51
    .line 8208
    const/16 v3, 0x100

    #@53
    invoke-direct {p0, v3}, Landroid/view/View;->sendAccessibilityHoverEvent(I)V

    #@56
    .line 8211
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@58
    if-eqz v3, :cond_2d

    #@5a
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5c
    iget-boolean v3, v3, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@5e
    if-nez v3, :cond_2d

    #@60
    .line 8212
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3, v5, v5}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@67
    goto :goto_2d

    #@68
    .line 8220
    :pswitch_68
    invoke-virtual {p0, v1}, Landroid/view/View;->setHovered(Z)V

    #@6b
    goto :goto_36

    #@6c
    .line 8223
    :pswitch_6c
    invoke-virtual {p0, v2}, Landroid/view/View;->setHovered(Z)V

    #@6f
    goto :goto_36

    #@70
    :cond_70
    move v1, v2

    #@71
    .line 8238
    goto :goto_39

    #@72
    .line 8218
    :pswitch_data_72
    .packed-switch 0x9
        :pswitch_68
        :pswitch_6c
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 4947
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 4948
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    #@9
    .line 4952
    :goto_9
    return-void

    #@a
    .line 4950
    :cond_a
    invoke-virtual {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)V

    #@d
    goto :goto_9
.end method

.method onInitializeAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 4960
    invoke-virtual {p1, p0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    #@3
    .line 4961
    const-class v1, Landroid/view/View;

    #@5
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 4962
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@17
    .line 4963
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    #@1a
    move-result v1

    #@1b
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    #@1e
    .line 4964
    iget-object v1, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@20
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    #@23
    .line 4966
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@26
    move-result v1

    #@27
    const/16 v2, 0x8

    #@29
    if-ne v1, v2, :cond_4d

    #@2b
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2d
    if-eqz v1, :cond_4d

    #@2f
    .line 4967
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@31
    iget-object v0, v1, Landroid/view/View$AttachInfo;->mTempArrayList:Ljava/util/ArrayList;

    #@33
    .line 4968
    .local v0, focusablesTempList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@36
    move-result-object v1

    #@37
    const/4 v2, 0x2

    #@38
    const/4 v3, 0x0

    #@39
    invoke-virtual {v1, v0, v2, v3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    #@3c
    .line 4970
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v1

    #@40
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    #@43
    .line 4971
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@46
    move-result v1

    #@47
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    #@4a
    .line 4972
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@4d
    .line 4974
    .end local v0           #focusablesTempList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_4d
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 5034
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 5035
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/view/View$AccessibilityDelegate;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@9
    .line 5039
    :goto_9
    return-void

    #@a
    .line 5037
    :cond_a
    invoke-virtual {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfoInternal(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@d
    goto :goto_9
.end method

.method onInitializeAccessibilityNodeInfoInternal(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 9
    .parameter "info"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 5092
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    iget-object v0, v5, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@5
    .line 5094
    .local v0, bounds:Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@8
    .line 5095
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    #@b
    .line 5097
    invoke-virtual {p0, v0}, Landroid/view/View;->getBoundsOnScreen(Landroid/graphics/Rect;)V

    #@e
    .line 5098
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    #@11
    .line 5100
    invoke-virtual {p0}, Landroid/view/View;->getParentForAccessibility()Landroid/view/ViewParent;

    #@14
    move-result-object v3

    #@15
    .line 5101
    .local v3, parent:Landroid/view/ViewParent;
    instance-of v5, v3, Landroid/view/View;

    #@17
    if-eqz v5, :cond_1e

    #@19
    .line 5102
    check-cast v3, Landroid/view/View;

    #@1b
    .end local v3           #parent:Landroid/view/ViewParent;
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    #@1e
    .line 5105
    :cond_1e
    iget v5, p0, Landroid/view/View;->mID:I

    #@20
    if-eq v5, v6, :cond_34

    #@22
    .line 5106
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@25
    move-result-object v4

    #@26
    .line 5107
    .local v4, rootView:Landroid/view/View;
    if-nez v4, :cond_29

    #@28
    .line 5108
    move-object v4, p0

    #@29
    .line 5110
    :cond_29
    iget v5, p0, Landroid/view/View;->mID:I

    #@2b
    invoke-direct {v4, p0, v5}, Landroid/view/View;->findLabelForView(Landroid/view/View;I)Landroid/view/View;

    #@2e
    move-result-object v1

    #@2f
    .line 5111
    .local v1, label:Landroid/view/View;
    if-eqz v1, :cond_34

    #@31
    .line 5112
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabeledBy(Landroid/view/View;)V

    #@34
    .line 5116
    .end local v1           #label:Landroid/view/View;
    .end local v4           #rootView:Landroid/view/View;
    :cond_34
    iget v5, p0, Landroid/view/View;->mLabelForId:I

    #@36
    if-eq v5, v6, :cond_4a

    #@38
    .line 5117
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@3b
    move-result-object v4

    #@3c
    .line 5118
    .restart local v4       #rootView:Landroid/view/View;
    if-nez v4, :cond_3f

    #@3e
    .line 5119
    move-object v4, p0

    #@3f
    .line 5121
    :cond_3f
    iget v5, p0, Landroid/view/View;->mLabelForId:I

    #@41
    invoke-direct {v4, p0, v5}, Landroid/view/View;->findViewInsideOutShouldExist(Landroid/view/View;I)Landroid/view/View;

    #@44
    move-result-object v2

    #@45
    .line 5122
    .local v2, labeled:Landroid/view/View;
    if-eqz v2, :cond_4a

    #@47
    .line 5123
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabelFor(Landroid/view/View;)V

    #@4a
    .line 5127
    .end local v2           #labeled:Landroid/view/View;
    .end local v4           #rootView:Landroid/view/View;
    :cond_4a
    invoke-virtual {p0}, Landroid/view/View;->isVisibleToUser()Z

    #@4d
    move-result v5

    #@4e
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    #@51
    .line 5129
    iget-object v5, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@53
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    #@5a
    .line 5130
    const-class v5, Landroid/view/View;

    #@5c
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@63
    .line 5131
    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    #@6a
    .line 5133
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    #@6d
    move-result v5

    #@6e
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    #@71
    .line 5134
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    #@74
    move-result v5

    #@75
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    #@78
    .line 5135
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    #@7b
    move-result v5

    #@7c
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocusable(Z)V

    #@7f
    .line 5136
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    #@82
    move-result v5

    #@83
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocused(Z)V

    #@86
    .line 5137
    invoke-virtual {p0}, Landroid/view/View;->isAccessibilityFocused()Z

    #@89
    move-result v5

    #@8a
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setAccessibilityFocused(Z)V

    #@8d
    .line 5138
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    #@90
    move-result v5

    #@91
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSelected(Z)V

    #@94
    .line 5139
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    #@97
    move-result v5

    #@98
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLongClickable(Z)V

    #@9b
    .line 5144
    const/4 v5, 0x4

    #@9c
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@9f
    .line 5145
    const/16 v5, 0x8

    #@a1
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@a4
    .line 5147
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    #@a7
    move-result v5

    #@a8
    if-eqz v5, :cond_b4

    #@aa
    .line 5148
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    #@ad
    move-result v5

    #@ae
    if-eqz v5, :cond_fd

    #@b0
    .line 5149
    const/4 v5, 0x2

    #@b1
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@b4
    .line 5155
    :cond_b4
    :goto_b4
    invoke-virtual {p0}, Landroid/view/View;->isAccessibilityFocused()Z

    #@b7
    move-result v5

    #@b8
    if-nez v5, :cond_102

    #@ba
    .line 5156
    const/16 v5, 0x40

    #@bc
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@bf
    .line 5161
    :goto_bf
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    #@c2
    move-result v5

    #@c3
    if-eqz v5, :cond_d0

    #@c5
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    #@c8
    move-result v5

    #@c9
    if-eqz v5, :cond_d0

    #@cb
    .line 5162
    const/16 v5, 0x10

    #@cd
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@d0
    .line 5165
    :cond_d0
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    #@d3
    move-result v5

    #@d4
    if-eqz v5, :cond_e1

    #@d6
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    #@d9
    move-result v5

    #@da
    if-eqz v5, :cond_e1

    #@dc
    .line 5166
    const/16 v5, 0x20

    #@de
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@e1
    .line 5169
    :cond_e1
    iget-object v5, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@e3
    if-eqz v5, :cond_fc

    #@e5
    iget-object v5, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@e7
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    #@ea
    move-result v5

    #@eb
    if-lez v5, :cond_fc

    #@ed
    .line 5170
    const/16 v5, 0x100

    #@ef
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@f2
    .line 5171
    const/16 v5, 0x200

    #@f4
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@f7
    .line 5172
    const/16 v5, 0xb

    #@f9
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setMovementGranularities(I)V

    #@fc
    .line 5176
    :cond_fc
    return-void

    #@fd
    .line 5151
    :cond_fd
    const/4 v5, 0x1

    #@fe
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@101
    goto :goto_b4

    #@102
    .line 5158
    :cond_102
    const/16 v5, 0x80

    #@104
    invoke-virtual {p1, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    #@107
    goto :goto_bf
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/high16 v4, 0x20

    #@2
    const/4 v1, 0x1

    #@3
    .line 7887
    const/4 v0, 0x0

    #@4
    .line 7889
    .local v0, result:Z
    sparse-switch p1, :sswitch_data_2e

    #@7
    .line 7906
    .end local v0           #result:Z
    :cond_7
    :goto_7
    return v0

    #@8
    .line 7892
    .restart local v0       #result:Z
    :sswitch_8
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@a
    and-int/lit8 v2, v2, 0x20

    #@c
    const/16 v3, 0x20

    #@e
    if-ne v2, v3, :cond_12

    #@10
    move v0, v1

    #@11
    .line 7893
    goto :goto_7

    #@12
    .line 7896
    :cond_12
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@14
    and-int/lit16 v2, v2, 0x4000

    #@16
    const/16 v3, 0x4000

    #@18
    if-eq v2, v3, :cond_1f

    #@1a
    iget v2, p0, Landroid/view/View;->mViewFlags:I

    #@1c
    and-int/2addr v2, v4

    #@1d
    if-ne v2, v4, :cond_7

    #@1f
    :cond_1f
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_7

    #@25
    .line 7899
    invoke-virtual {p0, v1}, Landroid/view/View;->setPressed(Z)V

    #@28
    .line 7900
    const/4 v2, 0x0

    #@29
    invoke-direct {p0, v2}, Landroid/view/View;->checkForLongClick(I)V

    #@2c
    move v0, v1

    #@2d
    .line 7901
    goto :goto_7

    #@2e
    .line 7889
    :sswitch_data_2e
    .sparse-switch
        0x17 -> :sswitch_8
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 7918
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    .line 7973
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 7869
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 7988
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 7935
    const/4 v0, 0x0

    #@1
    .line 7937
    .local v0, result:Z
    sparse-switch p1, :sswitch_data_2e

    #@4
    :cond_4
    :goto_4
    move v1, v0

    #@5
    .line 7956
    :goto_5
    return v1

    #@6
    .line 7940
    :sswitch_6
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@8
    and-int/lit8 v1, v1, 0x20

    #@a
    const/16 v2, 0x20

    #@c
    if-ne v1, v2, :cond_10

    #@e
    .line 7941
    const/4 v1, 0x1

    #@f
    goto :goto_5

    #@10
    .line 7943
    :cond_10
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@12
    and-int/lit16 v1, v1, 0x4000

    #@14
    const/16 v2, 0x4000

    #@16
    if-ne v1, v2, :cond_4

    #@18
    invoke-virtual {p0}, Landroid/view/View;->isPressed()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_4

    #@1e
    .line 7944
    const/4 v1, 0x0

    #@1f
    invoke-virtual {p0, v1}, Landroid/view/View;->setPressed(Z)V

    #@22
    .line 7946
    iget-boolean v1, p0, Landroid/view/View;->mHasPerformedLongPress:Z

    #@24
    if-nez v1, :cond_4

    #@26
    .line 7948
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@29
    .line 7950
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    #@2c
    move-result v0

    #@2d
    goto :goto_4

    #@2e
    .line 7937
    :sswitch_data_2e
    .sparse-switch
        0x17 -> :sswitch_6
        0x42 -> :sswitch_6
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 6
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 14268
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 15830
    sget-boolean v0, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 15831
    invoke-direct {p0}, Landroid/view/View;->isRtlLangAndLocaleMetaDataExist()Z

    #@7
    move-result v0

    #@8
    sput-boolean v0, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@a
    .line 15832
    :cond_a
    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumWidth()I

    #@d
    move-result v0

    #@e
    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    #@11
    move-result v0

    #@12
    invoke-virtual {p0}, Landroid/view/View;->getSuggestedMinimumHeight()I

    #@15
    move-result v1

    #@16
    invoke-static {v1, p2}, Landroid/view/View;->getDefaultSize(II)I

    #@19
    move-result v1

    #@1a
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setMeasuredDimension(II)V

    #@1d
    .line 15834
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .registers 5
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "clampedX"
    .parameter "clampedY"

    #@0
    .prologue
    .line 16815
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 4904
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 4905
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/view/View$AccessibilityDelegate;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    #@9
    .line 4909
    :goto_9
    return-void

    #@a
    .line 4907
    :cond_a
    invoke-virtual {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)V

    #@d
    goto :goto_9
.end method

.method onPopulateAccessibilityEventInternal(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 4918
    return-void
.end method

.method public onResolveDrawables(I)V
    .registers 2
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 14468
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 12343
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x2

    #@4
    or-int/2addr v0, v1

    #@5
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@7
    .line 12344
    sget-object v0, Landroid/view/View$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    #@9
    if-eq p1, v0, :cond_5c

    #@b
    if-eqz p1, :cond_5c

    #@d
    .line 12345
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "Wrong state class, expecting View State but received "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, " instead. This usually happens "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    const-string/jumbo v2, "when two views of different type have the same id in the same hierarchy. "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, "This view\'s id is "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@3b
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    #@3e
    move-result v3

    #@3f
    invoke-static {v2, v3}, Landroid/view/ViewDebug;->resolveId(Landroid/content/Context;I)Ljava/lang/Object;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    const-string v2, ". Make sure "

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string/jumbo v2, "other views do not use the same id."

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v0

    #@5c
    .line 12351
    :cond_5c
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .registers 2
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 11840
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 3

    #@0
    .prologue
    .line 12285
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const/high16 v1, 0x2

    #@4
    or-int/2addr v0, v1

    #@5
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@7
    .line 12286
    sget-object v0, Landroid/view/View$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    #@9
    return-object v0
.end method

.method public onScreenStateChanged(I)V
    .registers 2
    .parameter "screenState"

    #@0
    .prologue
    .line 11798
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .registers 8
    .parameter "l"
    .parameter "t"
    .parameter "oldl"
    .parameter "oldt"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 8720
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_10

    #@d
    .line 8721
    invoke-direct {p0}, Landroid/view/View;->postSendViewScrolledAccessibilityEventCallback()V

    #@10
    .line 8724
    :cond_10
    iput-boolean v2, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@12
    .line 8726
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@14
    .line 8727
    .local v0, ai:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_18

    #@16
    .line 8728
    iput-boolean v2, v0, Landroid/view/View$AttachInfo;->mViewScrollChanged:Z

    #@18
    .line 8730
    :cond_18
    return-void
.end method

.method protected onSetAlpha(I)Z
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 16123
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onSizeChanged(IIII)V
    .registers 5
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 8765
    return-void
.end method

.method public onStartTemporaryDetach()V
    .registers 3

    #@0
    .prologue
    .line 7268
    invoke-direct {p0}, Landroid/view/View;->removeUnsetPressCallback()V

    #@3
    .line 7269
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@5
    const/high16 v1, 0x400

    #@7
    or-int/2addr v0, v1

    #@8
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    .line 7270
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter "event"

    #@0
    .prologue
    const/16 v12, 0x4000

    #@2
    const/4 v11, 0x0

    #@3
    const/high16 v10, 0x20

    #@5
    const/4 v7, 0x1

    #@6
    const/4 v6, 0x0

    #@7
    .line 8323
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@9
    .line 8325
    .local v3, viewFlags:I
    and-int/lit8 v8, v3, 0x20

    #@b
    const/16 v9, 0x20

    #@d
    if-ne v8, v9, :cond_28

    #@f
    .line 8326
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@12
    move-result v8

    #@13
    if-ne v8, v7, :cond_1e

    #@15
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@17
    and-int/lit16 v8, v8, 0x4000

    #@19
    if-eqz v8, :cond_1e

    #@1b
    .line 8327
    invoke-virtual {p0, v6}, Landroid/view/View;->setPressed(Z)V

    #@1e
    .line 8331
    :cond_1e
    and-int/lit16 v8, v3, 0x4000

    #@20
    if-eq v8, v12, :cond_26

    #@22
    and-int v8, v3, v10

    #@24
    if-ne v8, v10, :cond_27

    #@26
    :cond_26
    move v6, v7

    #@27
    .line 8446
    :cond_27
    :goto_27
    return v6

    #@28
    .line 8335
    :cond_28
    iget-object v8, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@2a
    if-eqz v8, :cond_36

    #@2c
    .line 8336
    iget-object v8, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@2e
    invoke-virtual {v8, p1}, Landroid/view/TouchDelegate;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@31
    move-result v8

    #@32
    if-eqz v8, :cond_36

    #@34
    move v6, v7

    #@35
    .line 8337
    goto :goto_27

    #@36
    .line 8341
    :cond_36
    and-int/lit16 v8, v3, 0x4000

    #@38
    if-eq v8, v12, :cond_3e

    #@3a
    and-int v8, v3, v10

    #@3c
    if-ne v8, v10, :cond_27

    #@3e
    .line 8343
    :cond_3e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@41
    move-result v8

    #@42
    packed-switch v8, :pswitch_data_122

    #@45
    :cond_45
    :goto_45
    move v6, v7

    #@46
    .line 8443
    goto :goto_27

    #@47
    .line 8345
    :pswitch_47
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@49
    const/high16 v9, 0x200

    #@4b
    and-int/2addr v8, v9

    #@4c
    if-eqz v8, :cond_ad

    #@4e
    move v2, v7

    #@4f
    .line 8346
    .local v2, prepressed:Z
    :goto_4f
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@51
    and-int/lit16 v6, v6, 0x4000

    #@53
    if-nez v6, :cond_57

    #@55
    if-eqz v2, :cond_45

    #@57
    .line 8349
    :cond_57
    const/4 v0, 0x0

    #@58
    .line 8350
    .local v0, focusTaken:Z
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    #@5b
    move-result v6

    #@5c
    if-eqz v6, :cond_6e

    #@5e
    invoke-virtual {p0}, Landroid/view/View;->isFocusableInTouchMode()Z

    #@61
    move-result v6

    #@62
    if-eqz v6, :cond_6e

    #@64
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    #@67
    move-result v6

    #@68
    if-nez v6, :cond_6e

    #@6a
    .line 8351
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    #@6d
    move-result v0

    #@6e
    .line 8354
    :cond_6e
    if-eqz v2, :cond_73

    #@70
    .line 8359
    invoke-virtual {p0, v7}, Landroid/view/View;->setPressed(Z)V

    #@73
    .line 8362
    :cond_73
    iget-boolean v6, p0, Landroid/view/View;->mHasPerformedLongPress:Z

    #@75
    if-nez v6, :cond_92

    #@77
    .line 8364
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@7a
    .line 8367
    if-nez v0, :cond_92

    #@7c
    .line 8371
    iget-object v6, p0, Landroid/view/View;->mPerformClick:Landroid/view/View$PerformClick;

    #@7e
    if-nez v6, :cond_87

    #@80
    .line 8372
    new-instance v6, Landroid/view/View$PerformClick;

    #@82
    invoke-direct {v6, p0, v11}, Landroid/view/View$PerformClick;-><init>(Landroid/view/View;Landroid/view/View$1;)V

    #@85
    iput-object v6, p0, Landroid/view/View;->mPerformClick:Landroid/view/View$PerformClick;

    #@87
    .line 8374
    :cond_87
    iget-object v6, p0, Landroid/view/View;->mPerformClick:Landroid/view/View$PerformClick;

    #@89
    invoke-virtual {p0, v6}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@8c
    move-result v6

    #@8d
    if-nez v6, :cond_92

    #@8f
    .line 8375
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    #@92
    .line 8380
    :cond_92
    iget-object v6, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@94
    if-nez v6, :cond_9d

    #@96
    .line 8381
    new-instance v6, Landroid/view/View$UnsetPressedState;

    #@98
    invoke-direct {v6, p0, v11}, Landroid/view/View$UnsetPressedState;-><init>(Landroid/view/View;Landroid/view/View$1;)V

    #@9b
    iput-object v6, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@9d
    .line 8384
    :cond_9d
    if-eqz v2, :cond_af

    #@9f
    .line 8385
    iget-object v6, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@a1
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    #@a4
    move-result v8

    #@a5
    int-to-long v8, v8

    #@a6
    invoke-virtual {p0, v6, v8, v9}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    #@a9
    .line 8391
    :cond_a9
    :goto_a9
    invoke-direct {p0}, Landroid/view/View;->removeTapCallback()V

    #@ac
    goto :goto_45

    #@ad
    .end local v0           #focusTaken:Z
    .end local v2           #prepressed:Z
    :cond_ad
    move v2, v6

    #@ae
    .line 8345
    goto :goto_4f

    #@af
    .line 8387
    .restart local v0       #focusTaken:Z
    .restart local v2       #prepressed:Z
    :cond_af
    iget-object v6, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@b1
    invoke-virtual {p0, v6}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@b4
    move-result v6

    #@b5
    if-nez v6, :cond_a9

    #@b7
    .line 8389
    iget-object v6, p0, Landroid/view/View;->mUnsetPressedState:Landroid/view/View$UnsetPressedState;

    #@b9
    invoke-virtual {v6}, Landroid/view/View$UnsetPressedState;->run()V

    #@bc
    goto :goto_a9

    #@bd
    .line 8396
    .end local v0           #focusTaken:Z
    .end local v2           #prepressed:Z
    :pswitch_bd
    iput-boolean v6, p0, Landroid/view/View;->mHasPerformedLongPress:Z

    #@bf
    .line 8398
    invoke-virtual {p0, p1}, Landroid/view/View;->performButtonActionOnTouchDown(Landroid/view/MotionEvent;)Z

    #@c2
    move-result v8

    #@c3
    if-nez v8, :cond_45

    #@c5
    .line 8403
    invoke-virtual {p0}, Landroid/view/View;->isInScrollingContainer()Z

    #@c8
    move-result v1

    #@c9
    .line 8407
    .local v1, isInScrollingContainer:Z
    if-eqz v1, :cond_e9

    #@cb
    .line 8408
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@cd
    const/high16 v8, 0x200

    #@cf
    or-int/2addr v6, v8

    #@d0
    iput v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@d2
    .line 8409
    iget-object v6, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@d4
    if-nez v6, :cond_dd

    #@d6
    .line 8410
    new-instance v6, Landroid/view/View$CheckForTap;

    #@d8
    invoke-direct {v6, p0, v11}, Landroid/view/View$CheckForTap;-><init>(Landroid/view/View;Landroid/view/View$1;)V

    #@db
    iput-object v6, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@dd
    .line 8412
    :cond_dd
    iget-object v6, p0, Landroid/view/View;->mPendingCheckForTap:Landroid/view/View$CheckForTap;

    #@df
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@e2
    move-result v8

    #@e3
    int-to-long v8, v8

    #@e4
    invoke-virtual {p0, v6, v8, v9}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    #@e7
    goto/16 :goto_45

    #@e9
    .line 8415
    :cond_e9
    invoke-virtual {p0, v7}, Landroid/view/View;->setPressed(Z)V

    #@ec
    .line 8416
    invoke-direct {p0, v6}, Landroid/view/View;->checkForLongClick(I)V

    #@ef
    goto/16 :goto_45

    #@f1
    .line 8421
    .end local v1           #isInScrollingContainer:Z
    :pswitch_f1
    invoke-virtual {p0, v6}, Landroid/view/View;->setPressed(Z)V

    #@f4
    .line 8422
    invoke-direct {p0}, Landroid/view/View;->removeTapCallback()V

    #@f7
    .line 8423
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@fa
    goto/16 :goto_45

    #@fc
    .line 8427
    :pswitch_fc
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@ff
    move-result v8

    #@100
    float-to-int v4, v8

    #@101
    .line 8428
    .local v4, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@104
    move-result v8

    #@105
    float-to-int v5, v8

    #@106
    .line 8431
    .local v5, y:I
    int-to-float v8, v4

    #@107
    int-to-float v9, v5

    #@108
    iget v10, p0, Landroid/view/View;->mTouchSlop:I

    #@10a
    int-to-float v10, v10

    #@10b
    invoke-direct {p0, v8, v9, v10}, Landroid/view/View;->pointInView(FFF)Z

    #@10e
    move-result v8

    #@10f
    if-nez v8, :cond_45

    #@111
    .line 8433
    invoke-direct {p0}, Landroid/view/View;->removeTapCallback()V

    #@114
    .line 8434
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@116
    and-int/lit16 v8, v8, 0x4000

    #@118
    if-eqz v8, :cond_45

    #@11a
    .line 8436
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@11d
    .line 8438
    invoke-virtual {p0, v6}, Landroid/view/View;->setPressed(Z)V

    #@120
    goto/16 :goto_45

    #@122
    .line 8343
    :pswitch_data_122
    .packed-switch 0x0
        :pswitch_bd
        :pswitch_47
        :pswitch_fc
        :pswitch_f1
    .end packed-switch
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 8110
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 5
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 7630
    if-nez p2, :cond_9

    #@2
    .line 7631
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 7632
    invoke-direct {p0}, Landroid/view/View;->initialAwakenScrollBars()Z

    #@9
    .line 7637
    :cond_9
    :goto_9
    return-void

    #@a
    .line 7634
    :cond_a
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@c
    const/high16 v1, 0x800

    #@e
    or-int/2addr v0, v1

    #@f
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@11
    goto :goto_9
.end method

.method public onWindowFocusChanged(Z)V
    .registers 4
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 7583
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@3
    move-result-object v0

    #@4
    .line 7584
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-nez p1, :cond_28

    #@6
    .line 7585
    invoke-virtual {p0}, Landroid/view/View;->isPressed()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_10

    #@c
    .line 7586
    const/4 v1, 0x0

    #@d
    invoke-virtual {p0, v1}, Landroid/view/View;->setPressed(Z)V

    #@10
    .line 7588
    :cond_10
    if-eqz v0, :cond_1b

    #@12
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@14
    and-int/lit8 v1, v1, 0x2

    #@16
    if-eqz v1, :cond_1b

    #@18
    .line 7589
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->focusOut(Landroid/view/View;)V

    #@1b
    .line 7591
    :cond_1b
    invoke-direct {p0}, Landroid/view/View;->removeLongPressCallback()V

    #@1e
    .line 7592
    invoke-direct {p0}, Landroid/view/View;->removeTapCallback()V

    #@21
    .line 7593
    invoke-virtual {p0}, Landroid/view/View;->onFocusLost()V

    #@24
    .line 7597
    :cond_24
    :goto_24
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@27
    .line 7598
    return-void

    #@28
    .line 7594
    :cond_28
    if-eqz v0, :cond_24

    #@2a
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2c
    and-int/lit8 v1, v1, 0x2

    #@2e
    if-eqz v1, :cond_24

    #@30
    .line 7595
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->focusIn(Landroid/view/View;)V

    #@33
    goto :goto_24
.end method

.method public onWindowSystemUiVisibilityChanged(I)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 16314
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 2
    .parameter "visibility"

    #@0
    .prologue
    .line 7687
    if-nez p1, :cond_5

    #@2
    .line 7688
    invoke-direct {p0}, Landroid/view/View;->initialAwakenScrollBars()Z

    #@5
    .line 7690
    :cond_5
    return-void
.end method

.method public outputDirtyFlags(Ljava/lang/String;ZI)V
    .registers 12
    .parameter "indent"
    .parameter "clear"
    .parameter "clearMask"

    #@0
    .prologue
    .line 12740
    const-string v4, "View"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    const-string v6, "             DIRTY("

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@17
    const/high16 v7, 0x60

    #@19
    and-int/2addr v6, v7

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    const-string v6, ") DRAWN("

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@26
    and-int/lit8 v6, v6, 0x20

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const-string v6, ")"

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    const-string v6, " CACHE_VALID("

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@3a
    const v7, 0x8000

    #@3d
    and-int/2addr v6, v7

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    const-string v6, ") INVALIDATED("

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@4a
    const/high16 v7, -0x8000

    #@4c
    and-int/2addr v6, v7

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    const-string v6, ")"

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 12744
    if-eqz p2, :cond_65

    #@60
    .line 12745
    iget v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@62
    and-int/2addr v4, p3

    #@63
    iput v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@65
    .line 12747
    :cond_65
    instance-of v4, p0, Landroid/view/ViewGroup;

    #@67
    if-eqz v4, :cond_90

    #@69
    move-object v3, p0

    #@6a
    .line 12748
    check-cast v3, Landroid/view/ViewGroup;

    #@6c
    .line 12749
    .local v3, parent:Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    #@6f
    move-result v1

    #@70
    .line 12750
    .local v1, count:I
    const/4 v2, 0x0

    #@71
    .local v2, i:I
    :goto_71
    if-ge v2, v1, :cond_90

    #@73
    .line 12751
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@76
    move-result-object v0

    #@77
    .line 12752
    .local v0, child:Landroid/view/View;
    new-instance v4, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v5, "  "

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v0, v4, p2, p3}, Landroid/view/View;->outputDirtyFlags(Ljava/lang/String;ZI)V

    #@8d
    .line 12750
    add-int/lit8 v2, v2, 0x1

    #@8f
    goto :goto_71

    #@90
    .line 12755
    .end local v0           #child:Landroid/view/View;
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #parent:Landroid/view/ViewGroup;
    :cond_90
    return-void
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .registers 26
    .parameter "deltaX"
    .parameter "deltaY"
    .parameter "scrollX"
    .parameter "scrollY"
    .parameter "scrollRangeX"
    .parameter "scrollRangeY"
    .parameter "maxOverScrollX"
    .parameter "maxOverScrollY"
    .parameter "isTouchEvent"

    #@0
    .prologue
    .line 16754
    move-object/from16 v0, p0

    #@2
    iget v10, v0, Landroid/view/View;->mOverScrollMode:I

    #@4
    .line 16755
    .local v10, overScrollMode:I
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeHorizontalScrollRange()I

    #@7
    move-result v14

    #@8
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeHorizontalScrollExtent()I

    #@b
    move-result v15

    #@c
    if-le v14, v15, :cond_55

    #@e
    const/4 v2, 0x1

    #@f
    .line 16757
    .local v2, canScrollHorizontal:Z
    :goto_f
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeVerticalScrollRange()I

    #@12
    move-result v14

    #@13
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->computeVerticalScrollExtent()I

    #@16
    move-result v15

    #@17
    if-le v14, v15, :cond_57

    #@19
    const/4 v3, 0x1

    #@1a
    .line 16759
    .local v3, canScrollVertical:Z
    :goto_1a
    if-eqz v10, :cond_21

    #@1c
    const/4 v14, 0x1

    #@1d
    if-ne v10, v14, :cond_59

    #@1f
    if-eqz v2, :cond_59

    #@21
    :cond_21
    const/4 v9, 0x1

    #@22
    .line 16761
    .local v9, overScrollHorizontal:Z
    :goto_22
    if-eqz v10, :cond_29

    #@24
    const/4 v14, 0x1

    #@25
    if-ne v10, v14, :cond_5b

    #@27
    if-eqz v3, :cond_5b

    #@29
    :cond_29
    const/4 v11, 0x1

    #@2a
    .line 16764
    .local v11, overScrollVertical:Z
    :goto_2a
    add-int v7, p3, p1

    #@2c
    .line 16765
    .local v7, newScrollX:I
    if-nez v9, :cond_30

    #@2e
    .line 16766
    const/16 p7, 0x0

    #@30
    .line 16769
    :cond_30
    add-int v8, p4, p2

    #@32
    .line 16770
    .local v8, newScrollY:I
    if-nez v11, :cond_36

    #@34
    .line 16771
    const/16 p8, 0x0

    #@36
    .line 16775
    :cond_36
    move/from16 v0, p7

    #@38
    neg-int v6, v0

    #@39
    .line 16776
    .local v6, left:I
    add-int v12, p7, p5

    #@3b
    .line 16777
    .local v12, right:I
    move/from16 v0, p8

    #@3d
    neg-int v13, v0

    #@3e
    .line 16778
    .local v13, top:I
    add-int v1, p8, p6

    #@40
    .line 16780
    .local v1, bottom:I
    const/4 v4, 0x0

    #@41
    .line 16781
    .local v4, clampedX:Z
    if-le v7, v12, :cond_5d

    #@43
    .line 16782
    move v7, v12

    #@44
    .line 16783
    const/4 v4, 0x1

    #@45
    .line 16789
    :cond_45
    :goto_45
    const/4 v5, 0x0

    #@46
    .line 16790
    .local v5, clampedY:Z
    if-le v8, v1, :cond_62

    #@48
    .line 16791
    move v8, v1

    #@49
    .line 16792
    const/4 v5, 0x1

    #@4a
    .line 16798
    :cond_4a
    :goto_4a
    move-object/from16 v0, p0

    #@4c
    invoke-virtual {v0, v7, v8, v4, v5}, Landroid/view/View;->onOverScrolled(IIZZ)V

    #@4f
    .line 16800
    if-nez v4, :cond_53

    #@51
    if-eqz v5, :cond_67

    #@53
    :cond_53
    const/4 v14, 0x1

    #@54
    :goto_54
    return v14

    #@55
    .line 16755
    .end local v1           #bottom:I
    .end local v2           #canScrollHorizontal:Z
    .end local v3           #canScrollVertical:Z
    .end local v4           #clampedX:Z
    .end local v5           #clampedY:Z
    .end local v6           #left:I
    .end local v7           #newScrollX:I
    .end local v8           #newScrollY:I
    .end local v9           #overScrollHorizontal:Z
    .end local v11           #overScrollVertical:Z
    .end local v12           #right:I
    .end local v13           #top:I
    :cond_55
    const/4 v2, 0x0

    #@56
    goto :goto_f

    #@57
    .line 16757
    .restart local v2       #canScrollHorizontal:Z
    :cond_57
    const/4 v3, 0x0

    #@58
    goto :goto_1a

    #@59
    .line 16759
    .restart local v3       #canScrollVertical:Z
    :cond_59
    const/4 v9, 0x0

    #@5a
    goto :goto_22

    #@5b
    .line 16761
    .restart local v9       #overScrollHorizontal:Z
    :cond_5b
    const/4 v11, 0x0

    #@5c
    goto :goto_2a

    #@5d
    .line 16784
    .restart local v1       #bottom:I
    .restart local v4       #clampedX:Z
    .restart local v6       #left:I
    .restart local v7       #newScrollX:I
    .restart local v8       #newScrollY:I
    .restart local v11       #overScrollVertical:Z
    .restart local v12       #right:I
    .restart local v13       #top:I
    :cond_5d
    if-ge v7, v6, :cond_45

    #@5f
    .line 16785
    move v7, v6

    #@60
    .line 16786
    const/4 v4, 0x1

    #@61
    goto :goto_45

    #@62
    .line 16793
    .restart local v5       #clampedY:Z
    :cond_62
    if-ge v8, v13, :cond_4a

    #@64
    .line 16794
    move v8, v13

    #@65
    .line 16795
    const/4 v5, 0x1

    #@66
    goto :goto_4a

    #@67
    .line 16800
    :cond_67
    const/4 v14, 0x0

    #@68
    goto :goto_54
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .registers 4
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 7031
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 7032
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    #@9
    move-result v0

    #@a
    .line 7034
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->performAccessibilityActionInternal(ILandroid/os/Bundle;)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method performAccessibilityActionInternal(ILandroid/os/Bundle;)Z
    .registers 7
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 7044
    sparse-switch p1, :sswitch_data_8e

    #@5
    :cond_5
    move v1, v2

    #@6
    .line 7109
    :cond_6
    :goto_6
    return v1

    #@7
    .line 7046
    :sswitch_7
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_5

    #@d
    .line 7047
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    #@10
    goto :goto_6

    #@11
    .line 7052
    :sswitch_11
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_5

    #@17
    .line 7053
    invoke-virtual {p0}, Landroid/view/View;->performLongClick()Z

    #@1a
    goto :goto_6

    #@1b
    .line 7058
    :sswitch_1b
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_5

    #@21
    .line 7061
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, v2}, Landroid/view/ViewRootImpl;->ensureTouchMode(Z)Z

    #@28
    .line 7062
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    #@2b
    move-result v1

    #@2c
    goto :goto_6

    #@2d
    .line 7066
    :sswitch_2d
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_5

    #@33
    .line 7067
    invoke-virtual {p0}, Landroid/view/View;->clearFocus()V

    #@36
    .line 7068
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    #@39
    move-result v3

    #@3a
    if-eqz v3, :cond_6

    #@3c
    move v1, v2

    #@3d
    goto :goto_6

    #@3e
    .line 7072
    :sswitch_3e
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    #@41
    move-result v3

    #@42
    if-nez v3, :cond_5

    #@44
    .line 7073
    invoke-virtual {p0, v1}, Landroid/view/View;->setSelected(Z)V

    #@47
    .line 7074
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    #@4a
    move-result v1

    #@4b
    goto :goto_6

    #@4c
    .line 7078
    :sswitch_4c
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    #@4f
    move-result v3

    #@50
    if-eqz v3, :cond_5

    #@52
    .line 7079
    invoke-virtual {p0, v2}, Landroid/view/View;->setSelected(Z)V

    #@55
    .line 7080
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    #@58
    move-result v3

    #@59
    if-eqz v3, :cond_6

    #@5b
    move v1, v2

    #@5c
    goto :goto_6

    #@5d
    .line 7084
    :sswitch_5d
    invoke-virtual {p0}, Landroid/view/View;->isAccessibilityFocused()Z

    #@60
    move-result v1

    #@61
    if-nez v1, :cond_5

    #@63
    .line 7085
    invoke-virtual {p0}, Landroid/view/View;->requestAccessibilityFocus()Z

    #@66
    move-result v1

    #@67
    goto :goto_6

    #@68
    .line 7089
    :sswitch_68
    invoke-virtual {p0}, Landroid/view/View;->isAccessibilityFocused()Z

    #@6b
    move-result v3

    #@6c
    if-eqz v3, :cond_5

    #@6e
    .line 7090
    invoke-virtual {p0}, Landroid/view/View;->clearAccessibilityFocus()V

    #@71
    goto :goto_6

    #@72
    .line 7095
    :sswitch_72
    if-eqz p2, :cond_5

    #@74
    .line 7096
    const-string v1, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    #@76
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@79
    move-result v0

    #@7a
    .line 7098
    .local v0, granularity:I
    invoke-direct {p0, v0}, Landroid/view/View;->nextAtGranularity(I)Z

    #@7d
    move-result v1

    #@7e
    goto :goto_6

    #@7f
    .line 7102
    .end local v0           #granularity:I
    :sswitch_7f
    if-eqz p2, :cond_5

    #@81
    .line 7103
    const-string v1, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    #@83
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@86
    move-result v0

    #@87
    .line 7105
    .restart local v0       #granularity:I
    invoke-direct {p0, v0}, Landroid/view/View;->previousAtGranularity(I)Z

    #@8a
    move-result v1

    #@8b
    goto/16 :goto_6

    #@8d
    .line 7044
    nop

    #@8e
    :sswitch_data_8e
    .sparse-switch
        0x1 -> :sswitch_1b
        0x2 -> :sswitch_2d
        0x4 -> :sswitch_3e
        0x8 -> :sswitch_4c
        0x10 -> :sswitch_7
        0x20 -> :sswitch_11
        0x40 -> :sswitch_5d
        0x80 -> :sswitch_68
        0x100 -> :sswitch_72
        0x200 -> :sswitch_7f
    .end sparse-switch
.end method

.method protected performButtonActionOnTouchDown(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 4385
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x2

    #@6
    if-eqz v0, :cond_1c

    #@8
    .line 4386
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@b
    move-result v0

    #@c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@f
    move-result v1

    #@10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@13
    move-result v2

    #@14
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->showContextMenu(FFI)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    .line 4387
    const/4 v0, 0x1

    #@1b
    .line 4390
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method public performClick()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 4316
    invoke-virtual {p0, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@5
    .line 4318
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@7
    .line 4319
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_25

    #@9
    iget-object v3, v0, Landroid/view/View$ListenerInfo;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@b
    if-eqz v3, :cond_25

    #@d
    .line 4321
    instance-of v3, p0, Landroid/widget/Switch;

    #@f
    if-eqz v3, :cond_2c

    #@11
    move-object v1, p0

    #@12
    .line 4322
    check-cast v1, Landroid/widget/Switch;

    #@14
    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    #@17
    move-result v1

    #@18
    if-ne v1, v2, :cond_26

    #@1a
    .line 4323
    const/16 v1, 0x9

    #@1c
    invoke-virtual {p0, v1}, Landroid/view/View;->playSoundEffect(I)V

    #@1f
    .line 4329
    :goto_1f
    iget-object v1, v0, Landroid/view/View$ListenerInfo;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@21
    invoke-interface {v1, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    #@24
    move v1, v2

    #@25
    .line 4333
    :cond_25
    return v1

    #@26
    .line 4325
    :cond_26
    const/16 v1, 0xa

    #@28
    invoke-virtual {p0, v1}, Landroid/view/View;->playSoundEffect(I)V

    #@2b
    goto :goto_1f

    #@2c
    .line 4328
    :cond_2c
    invoke-virtual {p0, v1}, Landroid/view/View;->playSoundEffect(I)V

    #@2f
    goto :goto_1f
.end method

.method performCollectViewAttributes(Landroid/view/View$AttachInfo;I)V
    .registers 7
    .parameter "attachInfo"
    .parameter "visibility"

    #@0
    .prologue
    const/high16 v2, 0x400

    #@2
    const/4 v3, 0x1

    #@3
    .line 7807
    and-int/lit8 v1, p2, 0xc

    #@5
    if-nez v1, :cond_21

    #@7
    .line 7808
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@9
    and-int/2addr v1, v2

    #@a
    if-ne v1, v2, :cond_e

    #@c
    .line 7809
    iput-boolean v3, p1, Landroid/view/View$AttachInfo;->mKeepScreenOn:Z

    #@e
    .line 7811
    :cond_e
    iget v1, p1, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@10
    iget v2, p0, Landroid/view/View;->mSystemUiVisibility:I

    #@12
    or-int/2addr v1, v2

    #@13
    iput v1, p1, Landroid/view/View$AttachInfo;->mSystemUiVisibility:I

    #@15
    .line 7812
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@17
    .line 7813
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_21

    #@19
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$1000(Landroid/view/View$ListenerInfo;)Landroid/view/View$OnSystemUiVisibilityChangeListener;

    #@1c
    move-result-object v1

    #@1d
    if-eqz v1, :cond_21

    #@1f
    .line 7814
    iput-boolean v3, p1, Landroid/view/View$AttachInfo;->mHasSystemUiListeners:Z

    #@21
    .line 7817
    .end local v0           #li:Landroid/view/View$ListenerInfo;
    :cond_21
    return-void
.end method

.method public performHapticFeedback(I)Z
    .registers 3
    .parameter "feedbackConstant"

    #@0
    .prologue
    .line 16197
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->performHapticFeedback(II)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public performHapticFeedback(II)Z
    .registers 6
    .parameter "feedbackConstant"
    .parameter "flags"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 16210
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 16218
    :cond_5
    :goto_5
    return v0

    #@6
    .line 16214
    :cond_6
    and-int/lit8 v1, p2, 0x1

    #@8
    if-nez v1, :cond_10

    #@a
    invoke-virtual {p0}, Landroid/view/View;->isHapticFeedbackEnabled()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_5

    #@10
    .line 16218
    :cond_10
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@12
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mRootCallbacks:Landroid/view/View$AttachInfo$Callbacks;

    #@14
    and-int/lit8 v2, p2, 0x2

    #@16
    if-eqz v2, :cond_19

    #@18
    const/4 v0, 0x1

    #@19
    :cond_19
    invoke-interface {v1, p1, v0}, Landroid/view/View$AttachInfo$Callbacks;->performHapticFeedback(IZ)Z

    #@1c
    move-result v0

    #@1d
    goto :goto_5
.end method

.method public performLongClick()Z
    .registers 4

    #@0
    .prologue
    .line 4360
    const/4 v2, 0x2

    #@1
    invoke-virtual {p0, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@4
    .line 4362
    const/4 v0, 0x0

    #@5
    .line 4363
    .local v0, handled:Z
    iget-object v1, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@7
    .line 4364
    .local v1, li:Landroid/view/View$ListenerInfo;
    if-eqz v1, :cond_13

    #@9
    iget-object v2, v1, Landroid/view/View$ListenerInfo;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    #@b
    if-eqz v2, :cond_13

    #@d
    .line 4365
    iget-object v2, v1, Landroid/view/View$ListenerInfo;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    #@f
    invoke-interface {v2, p0}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    #@12
    move-result v0

    #@13
    .line 4367
    :cond_13
    if-nez v0, :cond_19

    #@15
    .line 4368
    invoke-virtual {p0}, Landroid/view/View;->showContextMenu()Z

    #@18
    move-result v0

    #@19
    .line 4370
    :cond_19
    if-eqz v0, :cond_1f

    #@1b
    .line 4371
    const/4 v2, 0x0

    #@1c
    invoke-virtual {p0, v2}, Landroid/view/View;->performHapticFeedback(I)Z

    #@1f
    .line 4373
    :cond_1f
    return v0
.end method

.method public playSoundEffect(I)V
    .registers 3
    .parameter "soundConstant"

    #@0
    .prologue
    .line 16175
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mRootCallbacks:Landroid/view/View$AttachInfo$Callbacks;

    #@8
    if-eqz v0, :cond_10

    #@a
    invoke-virtual {p0}, Landroid/view/View;->isSoundEffectsEnabled()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_11

    #@10
    .line 16179
    :cond_10
    :goto_10
    return-void

    #@11
    .line 16178
    :cond_11
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@13
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mRootCallbacks:Landroid/view/View$AttachInfo$Callbacks;

    #@15
    invoke-interface {v0, p1}, Landroid/view/View$AttachInfo$Callbacks;->playSoundEffect(I)V

    #@18
    goto :goto_10
.end method

.method final pointInView(FF)Z
    .registers 6
    .parameter "localX"
    .parameter "localY"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 9982
    cmpl-float v0, p1, v2

    #@3
    if-ltz v0, :cond_1f

    #@5
    iget v0, p0, Landroid/view/View;->mRight:I

    #@7
    iget v1, p0, Landroid/view/View;->mLeft:I

    #@9
    sub-int/2addr v0, v1

    #@a
    int-to-float v0, v0

    #@b
    cmpg-float v0, p1, v0

    #@d
    if-gez v0, :cond_1f

    #@f
    cmpl-float v0, p2, v2

    #@11
    if-ltz v0, :cond_1f

    #@13
    iget v0, p0, Landroid/view/View;->mBottom:I

    #@15
    iget v1, p0, Landroid/view/View;->mTop:I

    #@17
    sub-int/2addr v0, v1

    #@18
    int-to-float v0, v0

    #@19
    cmpg-float v0, p2, v0

    #@1b
    if-gez v0, :cond_1f

    #@1d
    const/4 v0, 0x1

    #@1e
    :goto_1e
    return v0

    #@1f
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_1e
.end method

.method public post(Ljava/lang/Runnable;)Z
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 10727
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10728
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_b

    #@4
    .line 10729
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@6
    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@9
    move-result v1

    #@a
    .line 10733
    :goto_a
    return v1

    #@b
    .line 10732
    :cond_b
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Landroid/view/ViewRootImpl$RunQueue;->post(Ljava/lang/Runnable;)V

    #@12
    .line 10733
    const/4 v1, 0x1

    #@13
    goto :goto_a
.end method

.method public postDelayed(Ljava/lang/Runnable;J)Z
    .registers 6
    .parameter "action"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 10756
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10757
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_b

    #@4
    .line 10758
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@6
    invoke-virtual {v1, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@9
    move-result v1

    #@a
    .line 10762
    :goto_a
    return v1

    #@b
    .line 10761
    :cond_b
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1, p2, p3}, Landroid/view/ViewRootImpl$RunQueue;->postDelayed(Ljava/lang/Runnable;J)V

    #@12
    .line 10762
    const/4 v1, 0x1

    #@13
    goto :goto_a
.end method

.method public postInvalidate()V
    .registers 3

    #@0
    .prologue
    .line 10849
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->postInvalidateDelayed(J)V

    #@5
    .line 10850
    return-void
.end method

.method public postInvalidate(IIII)V
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 10869
    const-wide/16 v1, 0x0

    #@2
    move-object v0, p0

    #@3
    move v3, p1

    #@4
    move v4, p2

    #@5
    move v5, p3

    #@6
    move v6, p4

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/view/View;->postInvalidateDelayed(JIIII)V

    #@a
    .line 10870
    return-void
.end method

.method public postInvalidateDelayed(J)V
    .registers 5
    .parameter "delayMilliseconds"

    #@0
    .prologue
    .line 10888
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10889
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_9

    #@4
    .line 10890
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@6
    invoke-virtual {v1, p0, p1, p2}, Landroid/view/ViewRootImpl;->dispatchInvalidateDelayed(Landroid/view/View;J)V

    #@9
    .line 10892
    :cond_9
    return-void
.end method

.method public postInvalidateDelayed(JIIII)V
    .registers 10
    .parameter "delayMilliseconds"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 10917
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10918
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_17

    #@4
    .line 10919
    invoke-static {}, Landroid/view/View$AttachInfo$InvalidateInfo;->acquire()Landroid/view/View$AttachInfo$InvalidateInfo;

    #@7
    move-result-object v1

    #@8
    .line 10920
    .local v1, info:Landroid/view/View$AttachInfo$InvalidateInfo;
    iput-object p0, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->target:Landroid/view/View;

    #@a
    .line 10921
    iput p3, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->left:I

    #@c
    .line 10922
    iput p4, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->top:I

    #@e
    .line 10923
    iput p5, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->right:I

    #@10
    .line 10924
    iput p6, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->bottom:I

    #@12
    .line 10926
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@14
    invoke-virtual {v2, v1, p1, p2}, Landroid/view/ViewRootImpl;->dispatchInvalidateRectDelayed(Landroid/view/View$AttachInfo$InvalidateInfo;J)V

    #@17
    .line 10928
    .end local v1           #info:Landroid/view/View$AttachInfo$InvalidateInfo;
    :cond_17
    return-void
.end method

.method public postInvalidateOnAnimation()V
    .registers 3

    #@0
    .prologue
    .line 10942
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10943
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_9

    #@4
    .line 10944
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@6
    invoke-virtual {v1, p0}, Landroid/view/ViewRootImpl;->dispatchInvalidateOnAnimation(Landroid/view/View;)V

    #@9
    .line 10946
    :cond_9
    return-void
.end method

.method public postInvalidateOnAnimation(IIII)V
    .registers 8
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 10966
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10967
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_17

    #@4
    .line 10968
    invoke-static {}, Landroid/view/View$AttachInfo$InvalidateInfo;->acquire()Landroid/view/View$AttachInfo$InvalidateInfo;

    #@7
    move-result-object v1

    #@8
    .line 10969
    .local v1, info:Landroid/view/View$AttachInfo$InvalidateInfo;
    iput-object p0, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->target:Landroid/view/View;

    #@a
    .line 10970
    iput p1, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->left:I

    #@c
    .line 10971
    iput p2, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->top:I

    #@e
    .line 10972
    iput p3, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->right:I

    #@10
    .line 10973
    iput p4, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->bottom:I

    #@12
    .line 10975
    iget-object v2, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@14
    invoke-virtual {v2, v1}, Landroid/view/ViewRootImpl;->dispatchInvalidateRectOnAnimation(Landroid/view/View$AttachInfo$InvalidateInfo;)V

    #@17
    .line 10977
    .end local v1           #info:Landroid/view/View$AttachInfo$InvalidateInfo;
    :cond_17
    return-void
.end method

.method public postOnAnimation(Ljava/lang/Runnable;)V
    .registers 6
    .parameter "action"

    #@0
    .prologue
    .line 10775
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10776
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_e

    #@4
    .line 10777
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@6
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@8
    const/4 v2, 0x1

    #@9
    const/4 v3, 0x0

    #@a
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@d
    .line 10783
    :goto_d
    return-void

    #@e
    .line 10781
    :cond_e
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Landroid/view/ViewRootImpl$RunQueue;->post(Ljava/lang/Runnable;)V

    #@15
    goto :goto_d
.end method

.method public postOnAnimationDelayed(Ljava/lang/Runnable;J)V
    .registers 11
    .parameter "action"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 10798
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    .line 10799
    .local v6, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v6, :cond_10

    #@4
    .line 10800
    iget-object v0, v6, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@6
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@8
    const/4 v1, 0x1

    #@9
    const/4 v3, 0x0

    #@a
    move-object v2, p1

    #@b
    move-wide v4, p2

    #@c
    invoke-virtual/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayed(ILjava/lang/Runnable;Ljava/lang/Object;J)V

    #@f
    .line 10806
    :goto_f
    return-void

    #@10
    .line 10804
    :cond_10
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/ViewRootImpl$RunQueue;->postDelayed(Ljava/lang/Runnable;J)V

    #@17
    goto :goto_f
.end method

.method protected recomputePadding()V
    .registers 5

    #@0
    .prologue
    .line 11200
    iget v0, p0, Landroid/view/View;->mUserPaddingLeft:I

    #@2
    iget v1, p0, Landroid/view/View;->mPaddingTop:I

    #@4
    iget v2, p0, Landroid/view/View;->mUserPaddingRight:I

    #@6
    iget v3, p0, Landroid/view/View;->mUserPaddingBottom:I

    #@8
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->internalSetPadding(IIII)V

    #@b
    .line 11201
    return-void
.end method

.method public refreshDrawableState()V
    .registers 3

    #@0
    .prologue
    .line 14529
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    or-int/lit16 v1, v1, 0x400

    #@4
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    .line 14530
    invoke-virtual {p0}, Landroid/view/View;->drawableStateChanged()V

    #@9
    .line 14532
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@b
    .line 14533
    .local v0, parent:Landroid/view/ViewParent;
    if-eqz v0, :cond_10

    #@d
    .line 14534
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->childDrawableStateChanged(Landroid/view/View;)V

    #@10
    .line 14536
    :cond_10
    return-void
.end method

.method public removeCallbacks(Ljava/lang/Runnable;)Z
    .registers 6
    .parameter "action"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 10824
    if-eqz p1, :cond_14

    #@3
    .line 10825
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5
    .line 10826
    .local v0, attachInfo:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_15

    #@7
    .line 10827
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@c
    .line 10828
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@e
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-virtual {v1, v3, p1, v2}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@14
    .line 10835
    .end local v0           #attachInfo:Landroid/view/View$AttachInfo;
    :cond_14
    :goto_14
    return v3

    #@15
    .line 10832
    .restart local v0       #attachInfo:Landroid/view/View$AttachInfo;
    :cond_15
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Landroid/view/ViewRootImpl$RunQueue;->removeCallbacks(Ljava/lang/Runnable;)V

    #@1c
    goto :goto_14
.end method

.method public removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 4237
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 4238
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_a

    #@4
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$100(Landroid/view/View$ListenerInfo;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 4242
    :cond_a
    :goto_a
    return-void

    #@b
    .line 4241
    :cond_b
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$100(Landroid/view/View$ListenerInfo;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@12
    goto :goto_a
.end method

.method public removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 4203
    iget-object v0, p0, Landroid/view/View;->mListenerInfo:Landroid/view/View$ListenerInfo;

    #@2
    .line 4204
    .local v0, li:Landroid/view/View$ListenerInfo;
    if-eqz v0, :cond_a

    #@4
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_b

    #@a
    .line 4208
    :cond_a
    :goto_a
    return-void

    #@b
    .line 4207
    :cond_b
    invoke-static {v0}, Landroid/view/View$ListenerInfo;->access$000(Landroid/view/View$ListenerInfo;)Ljava/util/ArrayList;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@12
    goto :goto_a
.end method

.method public requestAccessibilityFocus()Z
    .registers 6

    #@0
    .prologue
    const/high16 v4, 0x400

    #@2
    const/4 v2, 0x0

    #@3
    .line 6617
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@5
    invoke-static {v3}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@8
    move-result-object v0

    #@9
    .line 6618
    .local v0, manager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_15

    #@f
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_16

    #@15
    .line 6635
    :cond_15
    :goto_15
    return v2

    #@16
    .line 6621
    :cond_16
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@18
    and-int/lit8 v3, v3, 0xc

    #@1a
    if-nez v3, :cond_15

    #@1c
    .line 6624
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1e
    and-int/2addr v3, v4

    #@1f
    if-nez v3, :cond_15

    #@21
    .line 6625
    iget v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@23
    or-int/2addr v2, v4

    #@24
    iput v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@26
    .line 6626
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@29
    move-result-object v1

    #@2a
    .line 6627
    .local v1, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v1, :cond_30

    #@2c
    .line 6628
    const/4 v2, 0x0

    #@2d
    invoke-virtual {v1, p0, v2}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@30
    .line 6630
    :cond_30
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    #@33
    .line 6631
    const v2, 0x8000

    #@36
    invoke-virtual {p0, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@39
    .line 6632
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@3c
    .line 6633
    const/4 v2, 0x1

    #@3d
    goto :goto_15
.end method

.method public requestFitSystemWindows()V
    .registers 2

    #@0
    .prologue
    .line 5805
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 5806
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v0}, Landroid/view/ViewParent;->requestFitSystemWindows()V

    #@9
    .line 5808
    :cond_9
    return-void
.end method

.method public final requestFocus()Z
    .registers 2

    #@0
    .prologue
    .line 6719
    const/16 v0, 0x82

    #@2
    invoke-virtual {p0, v0}, Landroid/view/View;->requestFocus(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final requestFocus(I)Z
    .registers 3
    .parameter "direction"

    #@0
    .prologue
    .line 6740
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .registers 4
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    #@0
    .prologue
    .line 6773
    invoke-direct {p0, p1, p2}, Landroid/view/View;->requestFocusNoSearch(ILandroid/graphics/Rect;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public final requestFocusFromTouch()Z
    .registers 3

    #@0
    .prologue
    .line 6810
    invoke-virtual {p0}, Landroid/view/View;->isInTouchMode()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_10

    #@6
    .line 6811
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@9
    move-result-object v0

    #@a
    .line 6812
    .local v0, viewRoot:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_10

    #@c
    .line 6813
    const/4 v1, 0x0

    #@d
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->ensureTouchMode(Z)Z

    #@10
    .line 6816
    .end local v0           #viewRoot:Landroid/view/ViewRootImpl;
    :cond_10
    const/16 v1, 0x82

    #@12
    invoke-virtual {p0, v1}, Landroid/view/View;->requestFocus(I)Z

    #@15
    move-result v1

    #@16
    return v1
.end method

.method public requestLayout()V
    .registers 3

    #@0
    .prologue
    .line 15717
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    or-int/lit16 v0, v0, 0x1000

    #@4
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    .line 15718
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    const/high16 v1, -0x8000

    #@a
    or-int/2addr v0, v1

    #@b
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    .line 15720
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@f
    if-eqz v0, :cond_1e

    #@11
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@13
    invoke-interface {v0}, Landroid/view/ViewParent;->isLayoutRequested()Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_1e

    #@19
    .line 15721
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1b
    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    #@1e
    .line 15723
    :cond_1e
    return-void
.end method

.method public requestRectangleOnScreen(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "rectangle"

    #@0
    .prologue
    .line 4523
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z
    .registers 12
    .parameter "rectangle"
    .parameter "immediate"

    #@0
    .prologue
    .line 4542
    iget-object v5, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2
    if-nez v5, :cond_6

    #@4
    .line 4543
    const/4 v4, 0x0

    #@5
    .line 4578
    :cond_5
    return v4

    #@6
    .line 4546
    :cond_6
    move-object v0, p0

    #@7
    .line 4548
    .local v0, child:Landroid/view/View;
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    if-eqz v5, :cond_5d

    #@b
    iget-object v5, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@d
    iget-object v3, v5, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@f
    .line 4549
    .local v3, position:Landroid/graphics/RectF;
    :goto_f
    invoke-virtual {v3, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@12
    .line 4551
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@14
    .line 4552
    .local v1, parent:Landroid/view/ViewParent;
    const/4 v4, 0x0

    #@15
    .line 4553
    .local v4, scrolled:Z
    :goto_15
    if-eqz v1, :cond_5

    #@17
    .line 4554
    iget v5, v3, Landroid/graphics/RectF;->left:F

    #@19
    float-to-int v5, v5

    #@1a
    iget v6, v3, Landroid/graphics/RectF;->top:F

    #@1c
    float-to-int v6, v6

    #@1d
    iget v7, v3, Landroid/graphics/RectF;->right:F

    #@1f
    float-to-int v7, v7

    #@20
    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    #@22
    float-to-int v8, v8

    #@23
    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    #@26
    .line 4557
    invoke-interface {v1, v0, p1, p2}, Landroid/view/ViewParent;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    #@29
    move-result v5

    #@2a
    or-int/2addr v4, v5

    #@2b
    .line 4560
    invoke-virtual {v0}, Landroid/view/View;->hasIdentityMatrix()Z

    #@2e
    move-result v5

    #@2f
    if-nez v5, :cond_38

    #@31
    .line 4561
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@38
    .line 4564
    :cond_38
    iget v5, v0, Landroid/view/View;->mLeft:I

    #@3a
    int-to-float v5, v5

    #@3b
    iget v6, v0, Landroid/view/View;->mTop:I

    #@3d
    int-to-float v6, v6

    #@3e
    invoke-virtual {v3, v5, v6}, Landroid/graphics/RectF;->offset(FF)V

    #@41
    .line 4566
    instance-of v5, v1, Landroid/view/View;

    #@43
    if-eqz v5, :cond_5

    #@45
    move-object v2, v1

    #@46
    .line 4570
    check-cast v2, Landroid/view/View;

    #@48
    .line 4572
    .local v2, parentView:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    #@4b
    move-result v5

    #@4c
    neg-int v5, v5

    #@4d
    int-to-float v5, v5

    #@4e
    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    #@51
    move-result v6

    #@52
    neg-int v6, v6

    #@53
    int-to-float v6, v6

    #@54
    invoke-virtual {v3, v5, v6}, Landroid/graphics/RectF;->offset(FF)V

    #@57
    .line 4574
    move-object v0, v2

    #@58
    .line 4575
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@5b
    move-result-object v1

    #@5c
    .line 4576
    goto :goto_15

    #@5d
    .line 4548
    .end local v1           #parent:Landroid/view/ViewParent;
    .end local v2           #parentView:Landroid/view/View;
    .end local v3           #position:Landroid/graphics/RectF;
    .end local v4           #scrolled:Z
    :cond_5d
    new-instance v3, Landroid/graphics/RectF;

    #@5f
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    #@62
    goto :goto_f
.end method

.method public resetAccessibilityStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 7013
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const v1, -0x8000001

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    .line 7014
    return-void
.end method

.method public resetPaddingToInitialValues()V
    .registers 2

    #@0
    .prologue
    .line 15058
    invoke-direct {p0}, Landroid/view/View;->isRtlCompatibilityMode()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 15059
    iget v0, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@8
    iput v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@a
    .line 15060
    iget v0, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@c
    iput v0, p0, Landroid/view/View;->mPaddingRight:I

    #@e
    .line 15070
    :goto_e
    return-void

    #@f
    .line 15063
    :cond_f
    invoke-virtual {p0}, Landroid/view/View;->isLayoutRtl()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_2c

    #@15
    .line 15064
    iget v0, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@17
    if-ltz v0, :cond_26

    #@19
    iget v0, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@1b
    :goto_1b
    iput v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@1d
    .line 15065
    iget v0, p0, Landroid/view/View;->mUserPaddingStart:I

    #@1f
    if-ltz v0, :cond_29

    #@21
    iget v0, p0, Landroid/view/View;->mUserPaddingStart:I

    #@23
    :goto_23
    iput v0, p0, Landroid/view/View;->mPaddingRight:I

    #@25
    goto :goto_e

    #@26
    .line 15064
    :cond_26
    iget v0, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@28
    goto :goto_1b

    #@29
    .line 15065
    :cond_29
    iget v0, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@2b
    goto :goto_23

    #@2c
    .line 15067
    :cond_2c
    iget v0, p0, Landroid/view/View;->mUserPaddingStart:I

    #@2e
    if-ltz v0, :cond_3d

    #@30
    iget v0, p0, Landroid/view/View;->mUserPaddingStart:I

    #@32
    :goto_32
    iput v0, p0, Landroid/view/View;->mPaddingLeft:I

    #@34
    .line 15068
    iget v0, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@36
    if-ltz v0, :cond_40

    #@38
    iget v0, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@3a
    :goto_3a
    iput v0, p0, Landroid/view/View;->mPaddingRight:I

    #@3c
    goto :goto_e

    #@3d
    .line 15067
    :cond_3d
    iget v0, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@3f
    goto :goto_32

    #@40
    .line 15068
    :cond_40
    iget v0, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@42
    goto :goto_3a
.end method

.method protected resetResolvedDrawables()V
    .registers 3

    #@0
    .prologue
    .line 14474
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const v1, -0x40000001

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    .line 14475
    return-void
.end method

.method public resetResolvedLayoutDirection()V
    .registers 2

    #@0
    .prologue
    .line 11975
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit8 v0, v0, -0x31

    #@4
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@6
    .line 11976
    return-void
.end method

.method public resetResolvedPadding()V
    .registers 3

    #@0
    .prologue
    .line 12054
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const v1, -0x20000001

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    .line 12055
    return-void
.end method

.method public resetResolvedTextAlignment()V
    .registers 3

    #@0
    .prologue
    .line 17266
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    const v1, -0xf0001

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    .line 17268
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@a
    const/high16 v1, 0x2

    #@c
    or-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@f
    .line 17269
    return-void
.end method

.method public resetResolvedTextDirection()V
    .registers 2

    #@0
    .prologue
    .line 17053
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2
    and-int/lit16 v0, v0, -0x1e01

    #@4
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@6
    .line 17055
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    or-int/lit16 v0, v0, 0x400

    #@a
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@c
    .line 17056
    return-void
.end method

.method public resetRtlProperties()V
    .registers 1

    #@0
    .prologue
    .line 11774
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedLayoutDirection()V

    #@3
    .line 11775
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedTextDirection()V

    #@6
    .line 11776
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedTextAlignment()V

    #@9
    .line 11777
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedPadding()V

    #@c
    .line 11778
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedDrawables()V

    #@f
    .line 11779
    return-void
.end method

.method protected resolveDrawables()V
    .registers 3

    #@0
    .prologue
    .line 14446
    invoke-virtual {p0}, Landroid/view/View;->canResolveLayoutDirection()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_21

    #@6
    .line 14447
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 14448
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@13
    .line 14450
    :cond_13
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@15
    const/high16 v1, 0x4000

    #@17
    or-int/2addr v0, v1

    #@18
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1a
    .line 14451
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@1d
    move-result v0

    #@1e
    invoke-virtual {p0, v0}, Landroid/view/View;->onResolveDrawables(I)V

    #@21
    .line 14453
    :cond_21
    return-void
.end method

.method public resolveLayoutDirection()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 11912
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4
    and-int/lit8 v3, v3, -0x31

    #@6
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    .line 11914
    invoke-direct {p0}, Landroid/view/View;->hasRtlSupport()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_17

    #@e
    .line 11916
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@10
    and-int/lit8 v3, v3, 0xc

    #@12
    shr-int/lit8 v3, v3, 0x2

    #@14
    packed-switch v3, :pswitch_data_54

    #@17
    .line 11946
    :cond_17
    :goto_17
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@19
    or-int/lit8 v1, v1, 0x20

    #@1b
    iput v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1d
    move v1, v2

    #@1e
    .line 11947
    :cond_1e
    return v1

    #@1f
    .line 11921
    :pswitch_1f
    invoke-virtual {p0}, Landroid/view/View;->canResolveLayoutDirection()Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_1e

    #@25
    .line 11923
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@27
    check-cast v0, Landroid/view/View;

    #@29
    .line 11925
    .local v0, parent:Landroid/view/View;
    invoke-direct {v0}, Landroid/view/View;->isLayoutDirectionResolved()Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_1e

    #@2f
    .line 11927
    invoke-virtual {v0}, Landroid/view/View;->getLayoutDirection()I

    #@32
    move-result v1

    #@33
    if-ne v1, v2, :cond_17

    #@35
    .line 11928
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@37
    or-int/lit8 v1, v1, 0x10

    #@39
    iput v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3b
    goto :goto_17

    #@3c
    .line 11932
    .end local v0           #parent:Landroid/view/View;
    :pswitch_3c
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3e
    or-int/lit8 v1, v1, 0x10

    #@40
    iput v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@42
    goto :goto_17

    #@43
    .line 11935
    :pswitch_43
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@46
    move-result-object v1

    #@47
    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    #@4a
    move-result v1

    #@4b
    if-ne v2, v1, :cond_17

    #@4d
    .line 11937
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4f
    or-int/lit8 v1, v1, 0x10

    #@51
    iput v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@53
    goto :goto_17

    #@54
    .line 11916
    :pswitch_data_54
    .packed-switch 0x1
        :pswitch_3c
        :pswitch_1f
        :pswitch_43
    .end packed-switch
.end method

.method public resolveLayoutParams()V
    .registers 3

    #@0
    .prologue
    .line 10198
    iget-object v0, p0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 10199
    iget-object v0, p0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@6
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@9
    move-result v1

    #@a
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$LayoutParams;->resolveLayoutDirection(I)V

    #@d
    .line 10201
    :cond_d
    return-void
.end method

.method public resolvePadding()V
    .registers 6

    #@0
    .prologue
    const/high16 v2, -0x8000

    #@2
    .line 12009
    invoke-direct {p0}, Landroid/view/View;->isRtlCompatibilityMode()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_3d

    #@8
    .line 12014
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@b
    move-result v0

    #@c
    .line 12016
    .local v0, resolvedLayoutDirection:I
    iget v1, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@e
    iput v1, p0, Landroid/view/View;->mUserPaddingLeft:I

    #@10
    .line 12017
    iget v1, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@12
    iput v1, p0, Landroid/view/View;->mUserPaddingRight:I

    #@14
    .line 12019
    packed-switch v0, :pswitch_data_5a

    #@17
    .line 12030
    iget v1, p0, Landroid/view/View;->mUserPaddingStart:I

    #@19
    if-eq v1, v2, :cond_1f

    #@1b
    .line 12031
    iget v1, p0, Landroid/view/View;->mUserPaddingStart:I

    #@1d
    iput v1, p0, Landroid/view/View;->mUserPaddingLeft:I

    #@1f
    .line 12033
    :cond_1f
    iget v1, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@21
    if-eq v1, v2, :cond_27

    #@23
    .line 12034
    iget v1, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@25
    iput v1, p0, Landroid/view/View;->mUserPaddingRight:I

    #@27
    .line 12038
    :cond_27
    :goto_27
    iget v1, p0, Landroid/view/View;->mUserPaddingBottom:I

    #@29
    if-ltz v1, :cond_56

    #@2b
    iget v1, p0, Landroid/view/View;->mUserPaddingBottom:I

    #@2d
    :goto_2d
    iput v1, p0, Landroid/view/View;->mUserPaddingBottom:I

    #@2f
    .line 12040
    iget v1, p0, Landroid/view/View;->mUserPaddingLeft:I

    #@31
    iget v2, p0, Landroid/view/View;->mPaddingTop:I

    #@33
    iget v3, p0, Landroid/view/View;->mUserPaddingRight:I

    #@35
    iget v4, p0, Landroid/view/View;->mUserPaddingBottom:I

    #@37
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/view/View;->internalSetPadding(IIII)V

    #@3a
    .line 12042
    invoke-virtual {p0, v0}, Landroid/view/View;->onRtlPropertiesChanged(I)V

    #@3d
    .line 12045
    .end local v0           #resolvedLayoutDirection:I
    :cond_3d
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3f
    const/high16 v2, 0x2000

    #@41
    or-int/2addr v1, v2

    #@42
    iput v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@44
    .line 12046
    return-void

    #@45
    .line 12021
    .restart local v0       #resolvedLayoutDirection:I
    :pswitch_45
    iget v1, p0, Landroid/view/View;->mUserPaddingStart:I

    #@47
    if-eq v1, v2, :cond_4d

    #@49
    .line 12022
    iget v1, p0, Landroid/view/View;->mUserPaddingStart:I

    #@4b
    iput v1, p0, Landroid/view/View;->mUserPaddingRight:I

    #@4d
    .line 12024
    :cond_4d
    iget v1, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@4f
    if-eq v1, v2, :cond_27

    #@51
    .line 12025
    iget v1, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@53
    iput v1, p0, Landroid/view/View;->mUserPaddingLeft:I

    #@55
    goto :goto_27

    #@56
    .line 12038
    :cond_56
    iget v1, p0, Landroid/view/View;->mPaddingBottom:I

    #@58
    goto :goto_2d

    #@59
    .line 12019
    nop

    #@5a
    :pswitch_data_5a
    .packed-switch 0x1
        :pswitch_45
    .end packed-switch
.end method

.method public resolveRtlPropertiesIfNeeded()V
    .registers 2

    #@0
    .prologue
    .line 11745
    invoke-direct {p0}, Landroid/view/View;->needRtlPropertiesResolution()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 11766
    :goto_6
    return-void

    #@7
    .line 11748
    :cond_7
    invoke-direct {p0}, Landroid/view/View;->isLayoutDirectionResolved()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_13

    #@d
    .line 11749
    invoke-virtual {p0}, Landroid/view/View;->resolveLayoutDirection()Z

    #@10
    .line 11750
    invoke-virtual {p0}, Landroid/view/View;->resolveLayoutParams()V

    #@13
    .line 11753
    :cond_13
    invoke-direct {p0}, Landroid/view/View;->isTextDirectionResolved()Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_1c

    #@19
    .line 11754
    invoke-virtual {p0}, Landroid/view/View;->resolveTextDirection()Z

    #@1c
    .line 11756
    :cond_1c
    invoke-direct {p0}, Landroid/view/View;->isTextAlignmentResolved()Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_25

    #@22
    .line 11757
    invoke-virtual {p0}, Landroid/view/View;->resolveTextAlignment()Z

    #@25
    .line 11759
    :cond_25
    invoke-virtual {p0}, Landroid/view/View;->isPaddingResolved()Z

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_2e

    #@2b
    .line 11760
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@2e
    .line 11762
    :cond_2e
    invoke-direct {p0}, Landroid/view/View;->isDrawablesResolved()Z

    #@31
    move-result v0

    #@32
    if-nez v0, :cond_37

    #@34
    .line 11763
    invoke-virtual {p0}, Landroid/view/View;->resolveDrawables()V

    #@37
    .line 11765
    :cond_37
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@3a
    move-result v0

    #@3b
    invoke-virtual {p0, v0}, Landroid/view/View;->onRtlPropertiesChanged(I)V

    #@3e
    goto :goto_6
.end method

.method public resolveTextAlignment()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/high16 v6, 0x2

    #@3
    .line 17179
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@5
    const v5, -0xf0001

    #@8
    and-int/2addr v4, v5

    #@9
    iput v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@b
    .line 17181
    invoke-direct {p0}, Landroid/view/View;->hasRtlSupport()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_5f

    #@11
    .line 17183
    invoke-virtual {p0}, Landroid/view/View;->getRawTextAlignment()I

    #@14
    move-result v2

    #@15
    .line 17184
    .local v2, textAlignment:I
    packed-switch v2, :pswitch_data_66

    #@18
    .line 17231
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1a
    or-int/2addr v3, v6

    #@1b
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1d
    .line 17239
    .end local v2           #textAlignment:I
    :goto_1d
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1f
    const/high16 v4, 0x1

    #@21
    or-int/2addr v3, v4

    #@22
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    .line 17240
    const/4 v3, 0x1

    #@25
    :goto_25
    return v3

    #@26
    .line 17187
    .restart local v2       #textAlignment:I
    :pswitch_26
    invoke-direct {p0}, Landroid/view/View;->canResolveTextAlignment()Z

    #@29
    move-result v4

    #@2a
    if-nez v4, :cond_32

    #@2c
    .line 17189
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2e
    or-int/2addr v4, v6

    #@2f
    iput v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@31
    goto :goto_25

    #@32
    .line 17193
    :cond_32
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@34
    check-cast v0, Landroid/view/View;

    #@36
    .line 17196
    .local v0, parent:Landroid/view/View;
    invoke-direct {v0}, Landroid/view/View;->isTextAlignmentResolved()Z

    #@39
    move-result v4

    #@3a
    if-nez v4, :cond_42

    #@3c
    .line 17197
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3e
    or-int/2addr v4, v6

    #@3f
    iput v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@41
    goto :goto_25

    #@42
    .line 17202
    :cond_42
    invoke-virtual {v0}, Landroid/view/View;->getTextAlignment()I

    #@45
    move-result v1

    #@46
    .line 17203
    .local v1, parentResolvedTextAlignment:I
    packed-switch v1, :pswitch_data_78

    #@49
    .line 17217
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4b
    or-int/2addr v3, v6

    #@4c
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4e
    goto :goto_1d

    #@4f
    .line 17212
    :pswitch_4f
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@51
    shl-int/lit8 v4, v1, 0x11

    #@53
    or-int/2addr v3, v4

    #@54
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@56
    goto :goto_1d

    #@57
    .line 17227
    .end local v0           #parent:Landroid/view/View;
    .end local v1           #parentResolvedTextAlignment:I
    :pswitch_57
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@59
    shl-int/lit8 v4, v2, 0x11

    #@5b
    or-int/2addr v3, v4

    #@5c
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@5e
    goto :goto_1d

    #@5f
    .line 17235
    .end local v2           #textAlignment:I
    :cond_5f
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@61
    or-int/2addr v3, v6

    #@62
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@64
    goto :goto_1d

    #@65
    .line 17184
    nop

    #@66
    :pswitch_data_66
    .packed-switch 0x0
        :pswitch_26
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
    .end packed-switch

    #@78
    .line 17203
    :pswitch_data_78
    .packed-switch 0x1
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
        :pswitch_4f
    .end packed-switch
.end method

.method public resolveTextDirection()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 16970
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3
    and-int/lit16 v4, v4, -0x1e01

    #@5
    iput v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@7
    .line 16972
    invoke-direct {p0}, Landroid/view/View;->hasRtlSupport()Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_5e

    #@d
    .line 16974
    invoke-virtual {p0}, Landroid/view/View;->getRawTextDirection()I

    #@10
    move-result v2

    #@11
    .line 16975
    .local v2, textDirection:I
    packed-switch v2, :pswitch_data_66

    #@14
    .line 17018
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@16
    or-int/lit16 v3, v3, 0x400

    #@18
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1a
    .line 17026
    .end local v2           #textDirection:I
    :goto_1a
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1c
    or-int/lit16 v3, v3, 0x200

    #@1e
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@20
    .line 17027
    const/4 v3, 0x1

    #@21
    :goto_21
    return v3

    #@22
    .line 16977
    .restart local v2       #textDirection:I
    :pswitch_22
    invoke-direct {p0}, Landroid/view/View;->canResolveTextDirection()Z

    #@25
    move-result v4

    #@26
    if-nez v4, :cond_2f

    #@28
    .line 16979
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2a
    or-int/lit16 v4, v4, 0x400

    #@2c
    iput v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2e
    goto :goto_21

    #@2f
    .line 16984
    :cond_2f
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@31
    check-cast v0, Landroid/view/View;

    #@33
    .line 16986
    .local v0, parent:Landroid/view/View;
    invoke-direct {v0}, Landroid/view/View;->isTextDirectionResolved()Z

    #@36
    move-result v4

    #@37
    if-nez v4, :cond_40

    #@39
    .line 16987
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3b
    or-int/lit16 v4, v4, 0x400

    #@3d
    iput v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3f
    goto :goto_21

    #@40
    .line 16993
    :cond_40
    invoke-virtual {v0}, Landroid/view/View;->getTextDirection()I

    #@43
    move-result v1

    #@44
    .line 16994
    .local v1, parentResolvedDirection:I
    packed-switch v1, :pswitch_data_76

    #@47
    .line 17005
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@49
    or-int/lit16 v3, v3, 0x400

    #@4b
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4d
    goto :goto_1a

    #@4e
    .line 17000
    :pswitch_4e
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@50
    shl-int/lit8 v4, v1, 0xa

    #@52
    or-int/2addr v3, v4

    #@53
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@55
    goto :goto_1a

    #@56
    .line 17014
    .end local v0           #parent:Landroid/view/View;
    .end local v1           #parentResolvedDirection:I
    :pswitch_56
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@58
    shl-int/lit8 v4, v2, 0xa

    #@5a
    or-int/2addr v3, v4

    #@5b
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@5d
    goto :goto_1a

    #@5e
    .line 17022
    .end local v2           #textDirection:I
    :cond_5e
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@60
    or-int/lit16 v3, v3, 0x400

    #@62
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@64
    goto :goto_1a

    #@65
    .line 16975
    nop

    #@66
    :pswitch_data_66
    .packed-switch 0x0
        :pswitch_22
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
    .end packed-switch

    #@76
    .line 16994
    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_4e
        :pswitch_4e
        :pswitch_4e
        :pswitch_4e
        :pswitch_4e
    .end packed-switch
.end method

.method public restoreHierarchyState(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 12299
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    #@3
    .line 12300
    return-void
.end method

.method public saveHierarchyState(Landroid/util/SparseArray;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 12233
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    #@3
    .line 12234
    return-void
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .registers 13
    .parameter "who"
    .parameter "what"
    .parameter "when"

    #@0
    .prologue
    .line 14392
    invoke-virtual {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_22

    #@6
    if-eqz p2, :cond_22

    #@8
    .line 14393
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v0

    #@c
    sub-long v6, p3, v0

    #@e
    .line 14394
    .local v6, delay:J
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@10
    if-eqz v0, :cond_23

    #@12
    .line 14395
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@14
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@16
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@18
    const/4 v1, 0x1

    #@19
    invoke-static {v6, v7}, Landroid/view/Choreographer;->subtractFrameDelay(J)J

    #@1c
    move-result-wide v4

    #@1d
    move-object v2, p2

    #@1e
    move-object v3, p1

    #@1f
    invoke-virtual/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayed(ILjava/lang/Runnable;Ljava/lang/Object;J)V

    #@22
    .line 14402
    .end local v6           #delay:J
    :cond_22
    :goto_22
    return-void

    #@23
    .line 14399
    .restart local v6       #delay:J
    :cond_23
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0, p2, v6, v7}, Landroid/view/ViewRootImpl$RunQueue;->postDelayed(Ljava/lang/Runnable;J)V

    #@2a
    goto :goto_22
.end method

.method public scrollBy(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 10232
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    add-int/2addr v0, p1

    #@3
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@5
    add-int/2addr v1, p2

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->scrollTo(II)V

    #@9
    .line 10233
    return-void
.end method

.method public scrollTo(II)V
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 10211
    iget v2, p0, Landroid/view/View;->mScrollX:I

    #@2
    if-ne v2, p1, :cond_8

    #@4
    iget v2, p0, Landroid/view/View;->mScrollY:I

    #@6
    if-eq v2, p2, :cond_23

    #@8
    .line 10212
    :cond_8
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@a
    .line 10213
    .local v0, oldX:I
    iget v1, p0, Landroid/view/View;->mScrollY:I

    #@c
    .line 10214
    .local v1, oldY:I
    iput p1, p0, Landroid/view/View;->mScrollX:I

    #@e
    .line 10215
    iput p2, p0, Landroid/view/View;->mScrollY:I

    #@10
    .line 10216
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@13
    .line 10217
    iget v2, p0, Landroid/view/View;->mScrollX:I

    #@15
    iget v3, p0, Landroid/view/View;->mScrollY:I

    #@17
    invoke-virtual {p0, v2, v3, v0, v1}, Landroid/view/View;->onScrollChanged(IIII)V

    #@1a
    .line 10218
    invoke-virtual {p0}, Landroid/view/View;->awakenScrollBars()Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_23

    #@20
    .line 10219
    invoke-virtual {p0}, Landroid/view/View;->postInvalidateOnAnimation()V

    #@23
    .line 10222
    .end local v0           #oldX:I
    .end local v1           #oldY:I
    :cond_23
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .registers 3
    .parameter "eventType"

    #@0
    .prologue
    .line 4750
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 4751
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEvent(Landroid/view/View;I)V

    #@9
    .line 4755
    :goto_9
    return-void

    #@a
    .line 4753
    :cond_a
    invoke-virtual {p0, p1}, Landroid/view/View;->sendAccessibilityEventInternal(I)V

    #@d
    goto :goto_9
.end method

.method sendAccessibilityEventInternal(I)V
    .registers 3
    .parameter "eventType"

    #@0
    .prologue
    .line 4783
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_13

    #@c
    .line 4784
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {p0, v0}, Landroid/view/View;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    #@13
    .line 4786
    :cond_13
    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 4804
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 4805
    iget-object v0, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@6
    invoke-virtual {v0, p0, p1}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    #@9
    .line 4809
    :goto_9
    return-void

    #@a
    .line 4807
    :cond_a
    invoke-virtual {p0, p1}, Landroid/view/View;->sendAccessibilityEventUncheckedInternal(Landroid/view/accessibility/AccessibilityEvent;)V

    #@d
    goto :goto_9
.end method

.method sendAccessibilityEventUncheckedInternal(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 4817
    invoke-virtual {p0}, Landroid/view/View;->isShown()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 4827
    :goto_6
    return-void

    #@7
    .line 4820
    :cond_7
    invoke-virtual {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@a
    .line 4822
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@d
    move-result v0

    #@e
    const v1, 0x2a1bf

    #@11
    and-int/2addr v0, v1

    #@12
    if-eqz v0, :cond_17

    #@14
    .line 4823
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@17
    .line 4826
    :cond_17
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@1a
    move-result-object v0

    #@1b
    invoke-interface {v0, p0, p1}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    #@1e
    goto :goto_6
.end method

.method public setAccessibilityCursorPosition(I)V
    .registers 2
    .parameter "position"

    #@0
    .prologue
    .line 7194
    iput p1, p0, Landroid/view/View;->mAccessibilityCursorPosition:I

    #@2
    .line 7195
    return-void
.end method

.method public setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V
    .registers 2
    .parameter "delegate"

    #@0
    .prologue
    .line 5272
    iput-object p1, p0, Landroid/view/View;->mAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    #@2
    .line 5273
    return-void
.end method

.method public setActivated(Z)V
    .registers 7
    .parameter "activated"

    #@0
    .prologue
    const/high16 v0, 0x4000

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 15144
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    and-int/2addr v2, v0

    #@7
    if-eqz v2, :cond_21

    #@9
    move v2, v3

    #@a
    :goto_a
    if-eq v2, p1, :cond_20

    #@c
    .line 15145
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    const v4, -0x40000001

    #@11
    and-int/2addr v2, v4

    #@12
    if-eqz p1, :cond_23

    #@14
    :goto_14
    or-int/2addr v0, v2

    #@15
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@17
    .line 15146
    invoke-virtual {p0, v3}, Landroid/view/View;->invalidate(Z)V

    #@1a
    .line 15147
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@1d
    .line 15148
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchSetActivated(Z)V

    #@20
    .line 15150
    :cond_20
    return-void

    #@21
    :cond_21
    move v2, v1

    #@22
    .line 15144
    goto :goto_a

    #@23
    :cond_23
    move v0, v1

    #@24
    .line 15145
    goto :goto_14
.end method

.method public setAlpha(F)V
    .registers 5
    .parameter "alpha"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 9512
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@4
    .line 9513
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6
    iget v0, v0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@8
    cmpl-float v0, v0, p1

    #@a
    if-eqz v0, :cond_27

    #@c
    .line 9514
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@e
    iput p1, v0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@10
    .line 9515
    const/high16 v0, 0x437f

    #@12
    mul-float/2addr v0, p1

    #@13
    float-to-int v0, v0

    #@14
    invoke-virtual {p0, v0}, Landroid/view/View;->onSetAlpha(I)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_28

    #@1a
    .line 9516
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@1c
    const/high16 v1, 0x4

    #@1e
    or-int/2addr v0, v1

    #@1f
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@21
    .line 9518
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@24
    .line 9519
    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Z)V

    #@27
    .line 9528
    :cond_27
    :goto_27
    return-void

    #@28
    .line 9521
    :cond_28
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2a
    const v1, -0x40001

    #@2d
    and-int/2addr v0, v1

    #@2e
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@30
    .line 9522
    const/4 v0, 0x0

    #@31
    invoke-virtual {p0, v2, v0}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@34
    .line 9523
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@36
    if-eqz v0, :cond_27

    #@38
    .line 9524
    iget-object v0, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@3a
    invoke-virtual {v0, p1}, Landroid/view/DisplayList;->setAlpha(F)V

    #@3d
    goto :goto_27
.end method

.method setAlphaNoInvalidation(F)Z
    .registers 5
    .parameter "alpha"

    #@0
    .prologue
    .line 9541
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@3
    .line 9542
    iget-object v1, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5
    iget v1, v1, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@7
    cmpl-float v1, v1, p1

    #@9
    if-eqz v1, :cond_33

    #@b
    .line 9543
    iget-object v1, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    iput p1, v1, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@f
    .line 9544
    const/high16 v1, 0x437f

    #@11
    mul-float/2addr v1, p1

    #@12
    float-to-int v1, v1

    #@13
    invoke-virtual {p0, v1}, Landroid/view/View;->onSetAlpha(I)Z

    #@16
    move-result v0

    #@17
    .line 9545
    .local v0, subclassHandlesAlpha:Z
    if-eqz v0, :cond_22

    #@19
    .line 9546
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@1b
    const/high16 v2, 0x4

    #@1d
    or-int/2addr v1, v2

    #@1e
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@20
    .line 9547
    const/4 v1, 0x1

    #@21
    .line 9555
    .end local v0           #subclassHandlesAlpha:Z
    :goto_21
    return v1

    #@22
    .line 9549
    .restart local v0       #subclassHandlesAlpha:Z
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@24
    const v2, -0x40001

    #@27
    and-int/2addr v1, v2

    #@28
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@2a
    .line 9550
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@2c
    if-eqz v1, :cond_33

    #@2e
    .line 9551
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@30
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setAlpha(F)V

    #@33
    .line 9555
    .end local v0           #subclassHandlesAlpha:Z
    :cond_33
    const/4 v1, 0x0

    #@34
    goto :goto_21
.end method

.method public setAnimation(Landroid/view/animation/Animation;)V
    .registers 6
    .parameter "animation"

    #@0
    .prologue
    .line 16073
    iput-object p1, p0, Landroid/view/View;->mCurrentAnimation:Landroid/view/animation/Animation;

    #@2
    .line 16075
    if-eqz p1, :cond_22

    #@4
    .line 16079
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    if-eqz v0, :cond_1f

    #@8
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mScreenOn:Z

    #@c
    if-nez v0, :cond_1f

    #@e
    invoke-virtual {p1}, Landroid/view/animation/Animation;->getStartTime()J

    #@11
    move-result-wide v0

    #@12
    const-wide/16 v2, -0x1

    #@14
    cmp-long v0, v0, v2

    #@16
    if-nez v0, :cond_1f

    #@18
    .line 16081
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@1b
    move-result-wide v0

    #@1c
    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@1f
    .line 16083
    :cond_1f
    invoke-virtual {p1}, Landroid/view/animation/Animation;->reset()V

    #@22
    .line 16085
    :cond_22
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "background"

    #@0
    .prologue
    .line 14719
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3
    .line 14720
    return-void
.end method

.method public setBackgroundColor(I)V
    .registers 3
    .parameter "color"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 14677
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    instance-of v0, v0, Landroid/graphics/drawable/ColorDrawable;

    #@4
    if-eqz v0, :cond_15

    #@6
    .line 14678
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    #@e
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    #@11
    .line 14679
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@14
    .line 14683
    :goto_14
    return-void

    #@15
    .line 14681
    :cond_15
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    #@17
    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    #@1a
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@1d
    goto :goto_14
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 10
    .parameter "background"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 14727
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@6
    .line 14729
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8
    if-ne p1, v2, :cond_b

    #@a
    .line 14823
    :goto_a
    return-void

    #@b
    .line 14733
    :cond_b
    const/4 v1, 0x0

    #@c
    .line 14735
    .local v1, requestLayout:Z
    iput v4, p0, Landroid/view/View;->mBackgroundResource:I

    #@e
    .line 14741
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@10
    if-eqz v2, :cond_1c

    #@12
    .line 14742
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@14
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@17
    .line 14743
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@19
    invoke-virtual {p0, v2}, Landroid/view/View;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@1c
    .line 14746
    :cond_1c
    if-eqz p1, :cond_d0

    #@1e
    .line 14747
    sget-object v2, Landroid/view/View;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@20
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Landroid/graphics/Rect;

    #@26
    .line 14748
    .local v0, padding:Landroid/graphics/Rect;
    if-nez v0, :cond_32

    #@28
    .line 14749
    new-instance v0, Landroid/graphics/Rect;

    #@2a
    .end local v0           #padding:Landroid/graphics/Rect;
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@2d
    .line 14750
    .restart local v0       #padding:Landroid/graphics/Rect;
    sget-object v2, Landroid/view/View;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@32
    .line 14752
    :cond_32
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedDrawables()V

    #@35
    .line 14753
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@38
    move-result v2

    #@39
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@3c
    .line 14754
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_5f

    #@42
    .line 14755
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedPadding()V

    #@45
    .line 14756
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getLayoutDirection()I

    #@48
    move-result v2

    #@49
    packed-switch v2, :pswitch_data_e6

    #@4c
    .line 14764
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@4e
    iput v2, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@50
    .line 14765
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@52
    iput v2, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@54
    .line 14766
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@56
    iget v5, v0, Landroid/graphics/Rect;->top:I

    #@58
    iget v6, v0, Landroid/graphics/Rect;->right:I

    #@5a
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    #@5c
    invoke-virtual {p0, v2, v5, v6, v7}, Landroid/view/View;->internalSetPadding(IIII)V

    #@5f
    .line 14772
    :cond_5f
    :goto_5f
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@61
    if-eqz v2, :cond_7b

    #@63
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@65
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@68
    move-result v2

    #@69
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@6c
    move-result v5

    #@6d
    if-ne v2, v5, :cond_7b

    #@6f
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@71
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@74
    move-result v2

    #@75
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@78
    move-result v5

    #@79
    if-eq v2, v5, :cond_7c

    #@7b
    .line 14774
    :cond_7b
    const/4 v1, 0x1

    #@7c
    .line 14777
    :cond_7c
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@7f
    .line 14778
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@82
    move-result v2

    #@83
    if-eqz v2, :cond_8c

    #@85
    .line 14779
    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    #@88
    move-result-object v2

    #@89
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@8c
    .line 14781
    :cond_8c
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    #@8f
    move-result v2

    #@90
    if-nez v2, :cond_ce

    #@92
    move v2, v3

    #@93
    :goto_93
    invoke-virtual {p1, v2, v4}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@96
    .line 14782
    iput-object p1, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@98
    .line 14784
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@9a
    and-int/lit16 v2, v2, 0x80

    #@9c
    if-eqz v2, :cond_ab

    #@9e
    .line 14785
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@a0
    and-int/lit16 v2, v2, -0x81

    #@a2
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@a4
    .line 14786
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@a6
    or-int/lit16 v2, v2, 0x100

    #@a8
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@aa
    .line 14787
    const/4 v1, 0x1

    #@ab
    .line 14815
    .end local v0           #padding:Landroid/graphics/Rect;
    :cond_ab
    :goto_ab
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@ae
    .line 14817
    if-eqz v1, :cond_b3

    #@b0
    .line 14818
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@b3
    .line 14821
    :cond_b3
    iput-boolean v3, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@b5
    .line 14822
    invoke-virtual {p0, v3}, Landroid/view/View;->invalidate(Z)V

    #@b8
    goto/16 :goto_a

    #@ba
    .line 14758
    .restart local v0       #padding:Landroid/graphics/Rect;
    :pswitch_ba
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@bc
    iput v2, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@be
    .line 14759
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@c0
    iput v2, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@c2
    .line 14760
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@c4
    iget v5, v0, Landroid/graphics/Rect;->top:I

    #@c6
    iget v6, v0, Landroid/graphics/Rect;->left:I

    #@c8
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    #@ca
    invoke-virtual {p0, v2, v5, v6, v7}, Landroid/view/View;->internalSetPadding(IIII)V

    #@cd
    goto :goto_5f

    #@ce
    :cond_ce
    move v2, v4

    #@cf
    .line 14781
    goto :goto_93

    #@d0
    .line 14791
    .end local v0           #padding:Landroid/graphics/Rect;
    :cond_d0
    iput-object v5, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@d2
    .line 14793
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@d4
    and-int/lit16 v2, v2, 0x100

    #@d6
    if-eqz v2, :cond_e4

    #@d8
    .line 14799
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@da
    and-int/lit16 v2, v2, -0x101

    #@dc
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@de
    .line 14800
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@e0
    or-int/lit16 v2, v2, 0x80

    #@e2
    iput v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@e4
    .line 14812
    :cond_e4
    const/4 v1, 0x1

    #@e5
    goto :goto_ab

    #@e6
    .line 14756
    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_ba
    .end packed-switch
.end method

.method public setBackgroundResource(I)V
    .registers 4
    .parameter "resid"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 14694
    if-eqz p1, :cond_7

    #@2
    iget v1, p0, Landroid/view/View;->mBackgroundResource:I

    #@4
    if-ne p1, v1, :cond_7

    #@6
    .line 14705
    :goto_6
    return-void

    #@7
    .line 14698
    :cond_7
    const/4 v0, 0x0

    #@8
    .line 14699
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz p1, :cond_10

    #@a
    .line 14700
    iget-object v1, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@c
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v0

    #@10
    .line 14702
    :cond_10
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@13
    .line 14704
    iput p1, p0, Landroid/view/View;->mBackgroundResource:I

    #@15
    goto :goto_6
.end method

.method public final setBottom(I)V
    .registers 11
    .parameter "bottom"

    #@0
    .prologue
    const/high16 v8, 0x1000

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v5, 0x1

    #@4
    .line 9652
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@6
    if-eq p1, v6, :cond_74

    #@8
    .line 9653
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@b
    .line 9654
    iget-object v6, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    if-eqz v6, :cond_17

    #@f
    iget-object v6, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@11
    invoke-static {v6}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_75

    #@17
    :cond_17
    move v0, v5

    #@18
    .line 9656
    .local v0, matrixIsIdentity:Z
    :goto_18
    if-eqz v0, :cond_79

    #@1a
    .line 9657
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1c
    if-eqz v6, :cond_30

    #@1e
    .line 9659
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@20
    if-ge p1, v6, :cond_77

    #@22
    .line 9660
    iget v1, p0, Landroid/view/View;->mBottom:I

    #@24
    .line 9664
    .local v1, maxBottom:I
    :goto_24
    iget v6, p0, Landroid/view/View;->mRight:I

    #@26
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@28
    sub-int/2addr v6, v7

    #@29
    iget v7, p0, Landroid/view/View;->mTop:I

    #@2b
    sub-int v7, v1, v7

    #@2d
    invoke-virtual {p0, v4, v4, v6, v7}, Landroid/view/View;->invalidate(IIII)V

    #@30
    .line 9671
    .end local v1           #maxBottom:I
    :cond_30
    :goto_30
    iget v4, p0, Landroid/view/View;->mRight:I

    #@32
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@34
    sub-int v3, v4, v6

    #@36
    .line 9672
    .local v3, width:I
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@38
    iget v6, p0, Landroid/view/View;->mTop:I

    #@3a
    sub-int v2, v4, v6

    #@3c
    .line 9674
    .local v2, oldHeight:I
    iput p1, p0, Landroid/view/View;->mBottom:I

    #@3e
    .line 9675
    iget-object v4, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@40
    if-eqz v4, :cond_49

    #@42
    .line 9676
    iget-object v4, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@44
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@46
    invoke-virtual {v4, v6}, Landroid/view/DisplayList;->setBottom(I)V

    #@49
    .line 9679
    :cond_49
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@4b
    iget v6, p0, Landroid/view/View;->mTop:I

    #@4d
    sub-int/2addr v4, v6

    #@4e
    invoke-virtual {p0, v3, v4, v3, v2}, Landroid/view/View;->onSizeChanged(IIII)V

    #@51
    .line 9681
    if-nez v0, :cond_67

    #@53
    .line 9682
    iget v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@55
    const/high16 v6, 0x2000

    #@57
    and-int/2addr v4, v6

    #@58
    if-nez v4, :cond_5e

    #@5a
    .line 9684
    iget-object v4, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5c
    iput-boolean v5, v4, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@5e
    .line 9686
    :cond_5e
    iget v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@60
    or-int/lit8 v4, v4, 0x20

    #@62
    iput v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@64
    .line 9687
    invoke-virtual {p0, v5}, Landroid/view/View;->invalidate(Z)V

    #@67
    .line 9689
    :cond_67
    iput-boolean v5, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@69
    .line 9690
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@6c
    .line 9691
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@6e
    and-int/2addr v4, v8

    #@6f
    if-ne v4, v8, :cond_74

    #@71
    .line 9693
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@74
    .line 9696
    .end local v0           #matrixIsIdentity:Z
    .end local v2           #oldHeight:I
    .end local v3           #width:I
    :cond_74
    return-void

    #@75
    :cond_75
    move v0, v4

    #@76
    .line 9654
    goto :goto_18

    #@77
    .line 9662
    .restart local v0       #matrixIsIdentity:Z
    :cond_77
    move v1, p1

    #@78
    .restart local v1       #maxBottom:I
    goto :goto_24

    #@79
    .line 9668
    .end local v1           #maxBottom:I
    :cond_79
    invoke-virtual {p0, v5}, Landroid/view/View;->invalidate(Z)V

    #@7c
    goto :goto_30
.end method

.method public setCameraDistance(F)V
    .registers 10
    .parameter "distance"

    #@0
    .prologue
    const/high16 v7, 0x1000

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    const/4 v4, 0x0

    #@5
    .line 9104
    invoke-virtual {p0, v6, v4}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@8
    .line 9106
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@b
    .line 9107
    iget-object v2, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@d
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@10
    move-result-object v2

    #@11
    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    #@13
    int-to-float v0, v2

    #@14
    .line 9108
    .local v0, dpi:F
    iget-object v1, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@16
    .line 9109
    .local v1, info:Landroid/view/View$TransformationInfo;
    invoke-static {v1}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@19
    move-result-object v2

    #@1a
    if-nez v2, :cond_2c

    #@1c
    .line 9110
    new-instance v2, Landroid/graphics/Camera;

    #@1e
    invoke-direct {v2}, Landroid/graphics/Camera;-><init>()V

    #@21
    invoke-static {v1, v2}, Landroid/view/View$TransformationInfo;->access$1802(Landroid/view/View$TransformationInfo;Landroid/graphics/Camera;)Landroid/graphics/Camera;

    #@24
    .line 9111
    new-instance v2, Landroid/graphics/Matrix;

    #@26
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    #@29
    invoke-static {v1, v2}, Landroid/view/View$TransformationInfo;->access$1902(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    #@2c
    .line 9114
    :cond_2c
    invoke-static {v1}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@2f
    move-result-object v2

    #@30
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@33
    move-result v3

    #@34
    neg-float v3, v3

    #@35
    div-float/2addr v3, v0

    #@36
    invoke-virtual {v2, v5, v5, v3}, Landroid/graphics/Camera;->setLocation(FFF)V

    #@39
    .line 9115
    iput-boolean v6, v1, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@3b
    .line 9117
    invoke-virtual {p0, v4, v4}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@3e
    .line 9118
    iget-object v2, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@40
    if-eqz v2, :cond_4d

    #@42
    .line 9119
    iget-object v2, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@44
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@47
    move-result v3

    #@48
    neg-float v3, v3

    #@49
    div-float/2addr v3, v0

    #@4a
    invoke-virtual {v2, v3}, Landroid/view/DisplayList;->setCameraDistance(F)V

    #@4d
    .line 9121
    :cond_4d
    iget v2, p0, Landroid/view/View;->mPrivateFlags2:I

    #@4f
    and-int/2addr v2, v7

    #@50
    if-ne v2, v7, :cond_55

    #@52
    .line 9123
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@55
    .line 9125
    :cond_55
    return-void
.end method

.method public setClickable(Z)V
    .registers 4
    .parameter "clickable"

    #@0
    .prologue
    const/16 v1, 0x4000

    #@2
    .line 6200
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6201
    return-void

    #@9
    .line 6200
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "contentDescription"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 5360
    iget-object v2, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@3
    if-nez v2, :cond_8

    #@5
    .line 5361
    if-nez p1, :cond_10

    #@7
    .line 5373
    :cond_7
    :goto_7
    return-void

    #@8
    .line 5364
    :cond_8
    iget-object v2, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@a
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_7

    #@10
    .line 5367
    :cond_10
    iput-object p1, p0, Landroid/view/View;->mContentDescription:Ljava/lang/CharSequence;

    #@12
    .line 5368
    if-eqz p1, :cond_2a

    #@14
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@17
    move-result v2

    #@18
    if-lez v2, :cond_2a

    #@1a
    move v0, v1

    #@1b
    .line 5369
    .local v0, nonEmptyDesc:Z
    :goto_1b
    if-eqz v0, :cond_26

    #@1d
    invoke-virtual {p0}, Landroid/view/View;->getImportantForAccessibility()I

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_26

    #@23
    .line 5370
    invoke-virtual {p0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    #@26
    .line 5372
    :cond_26
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@29
    goto :goto_7

    #@2a
    .line 5368
    .end local v0           #nonEmptyDesc:Z
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_1b
.end method

.method public setDisabledSystemUiVisibility(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 16358
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 16359
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget v0, v0, Landroid/view/View$AttachInfo;->mDisabledSystemUiVisibility:I

    #@8
    if-eq v0, p1, :cond_17

    #@a
    .line 16360
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@c
    iput p1, v0, Landroid/view/View$AttachInfo;->mDisabledSystemUiVisibility:I

    #@e
    .line 16361
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 16362
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@14
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->recomputeViewAttributes(Landroid/view/View;)V

    #@17
    .line 16366
    :cond_17
    return-void
.end method

.method setDisplayListProperties(Landroid/view/DisplayList;)V
    .registers 16
    .parameter "displayList"

    #@0
    .prologue
    .line 13483
    if-eqz p1, :cond_e2

    #@2
    .line 13484
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@4
    iget v2, p0, Landroid/view/View;->mTop:I

    #@6
    iget v3, p0, Landroid/view/View;->mRight:I

    #@8
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@a
    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    #@d
    .line 13485
    invoke-virtual {p0}, Landroid/view/View;->hasOverlappingRendering()Z

    #@10
    move-result v0

    #@11
    invoke-virtual {p1, v0}, Landroid/view/DisplayList;->setHasOverlappingRendering(Z)V

    #@14
    .line 13486
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@16
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@18
    if-eqz v0, :cond_28

    #@1a
    .line 13487
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@1c
    check-cast v0, Landroid/view/ViewGroup;

    #@1e
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@20
    and-int/lit8 v0, v0, 0x1

    #@22
    if-eqz v0, :cond_e3

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    invoke-virtual {p1, v0}, Landroid/view/DisplayList;->setClipChildren(Z)V

    #@28
    .line 13490
    :cond_28
    const/high16 v1, 0x3f80

    #@2a
    .line 13491
    .local v1, alpha:F
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@2c
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@2e
    if-eqz v0, :cond_67

    #@30
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@32
    check-cast v0, Landroid/view/ViewGroup;

    #@34
    iget v0, v0, Landroid/view/ViewGroup;->mGroupFlags:I

    #@36
    and-int/lit16 v0, v0, 0x800

    #@38
    if-eqz v0, :cond_67

    #@3a
    .line 13493
    iget-object v11, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@3c
    check-cast v11, Landroid/view/ViewGroup;

    #@3e
    .line 13494
    .local v11, parentVG:Landroid/view/ViewGroup;
    iget-object v0, v11, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@40
    invoke-virtual {v11, p0, v0}, Landroid/view/ViewGroup;->getChildStaticTransformation(Landroid/view/View;Landroid/view/animation/Transformation;)Z

    #@43
    move-result v9

    #@44
    .line 13496
    .local v9, hasTransform:Z
    if-eqz v9, :cond_67

    #@46
    .line 13497
    iget-object v12, v11, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@48
    .line 13498
    .local v12, transform:Landroid/view/animation/Transformation;
    iget-object v0, v11, Landroid/view/ViewGroup;->mChildTransformation:Landroid/view/animation/Transformation;

    #@4a
    invoke-virtual {v0}, Landroid/view/animation/Transformation;->getTransformationType()I

    #@4d
    move-result v13

    #@4e
    .line 13499
    .local v13, transformType:I
    sget v0, Landroid/view/animation/Transformation;->TYPE_IDENTITY:I

    #@50
    if-eq v13, v0, :cond_67

    #@52
    .line 13500
    sget v0, Landroid/view/animation/Transformation;->TYPE_ALPHA:I

    #@54
    and-int/2addr v0, v13

    #@55
    if-eqz v0, :cond_5b

    #@57
    .line 13501
    invoke-virtual {v12}, Landroid/view/animation/Transformation;->getAlpha()F

    #@5a
    move-result v1

    #@5b
    .line 13503
    :cond_5b
    sget v0, Landroid/view/animation/Transformation;->TYPE_MATRIX:I

    #@5d
    and-int/2addr v0, v13

    #@5e
    if-eqz v0, :cond_67

    #@60
    .line 13504
    invoke-virtual {v12}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {p1, v0}, Landroid/view/DisplayList;->setStaticMatrix(Landroid/graphics/Matrix;)V

    #@67
    .line 13509
    .end local v9           #hasTransform:Z
    .end local v11           #parentVG:Landroid/view/ViewGroup;
    .end local v12           #transform:Landroid/view/animation/Transformation;
    .end local v13           #transformType:I
    :cond_67
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@69
    if-eqz v0, :cond_e6

    #@6b
    .line 13510
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@6d
    iget v0, v0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@6f
    mul-float/2addr v1, v0

    #@70
    .line 13511
    const/high16 v0, 0x3f80

    #@72
    cmpg-float v0, v1, v0

    #@74
    if-gez v0, :cond_82

    #@76
    .line 13512
    const/high16 v0, 0x437f

    #@78
    mul-float/2addr v0, v1

    #@79
    float-to-int v10, v0

    #@7a
    .line 13513
    .local v10, multipliedAlpha:I
    invoke-virtual {p0, v10}, Landroid/view/View;->onSetAlpha(I)Z

    #@7d
    move-result v0

    #@7e
    if-eqz v0, :cond_82

    #@80
    .line 13514
    const/high16 v1, 0x3f80

    #@82
    .line 13517
    .end local v10           #multipliedAlpha:I
    :cond_82
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@84
    iget v2, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@86
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@88
    iget v3, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@8a
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@8c
    iget v4, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@8e
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@90
    iget v5, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@92
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@94
    iget v6, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@96
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@98
    iget v7, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@9a
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9c
    iget v8, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@9e
    move-object v0, p1

    #@9f
    invoke-virtual/range {v0 .. v8}, Landroid/view/DisplayList;->setTransformationInfo(FFFFFFFF)V

    #@a2
    .line 13522
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@a4
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@a7
    move-result-object v0

    #@a8
    if-nez v0, :cond_be

    #@aa
    .line 13523
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@ac
    new-instance v2, Landroid/graphics/Camera;

    #@ae
    invoke-direct {v2}, Landroid/graphics/Camera;-><init>()V

    #@b1
    invoke-static {v0, v2}, Landroid/view/View$TransformationInfo;->access$1802(Landroid/view/View$TransformationInfo;Landroid/graphics/Camera;)Landroid/graphics/Camera;

    #@b4
    .line 13524
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@b6
    new-instance v2, Landroid/graphics/Matrix;

    #@b8
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    #@bb
    invoke-static {v0, v2}, Landroid/view/View$TransformationInfo;->access$1902(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    #@be
    .line 13526
    :cond_be
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@c0
    invoke-static {v0}, Landroid/view/View$TransformationInfo;->access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;

    #@c3
    move-result-object v0

    #@c4
    invoke-virtual {v0}, Landroid/graphics/Camera;->getLocationZ()F

    #@c7
    move-result v0

    #@c8
    invoke-virtual {p1, v0}, Landroid/view/DisplayList;->setCameraDistance(F)V

    #@cb
    .line 13527
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@cd
    const/high16 v2, 0x2000

    #@cf
    and-int/2addr v0, v2

    #@d0
    const/high16 v2, 0x2000

    #@d2
    if-ne v0, v2, :cond_e2

    #@d4
    .line 13528
    invoke-virtual {p0}, Landroid/view/View;->getPivotX()F

    #@d7
    move-result v0

    #@d8
    invoke-virtual {p1, v0}, Landroid/view/DisplayList;->setPivotX(F)V

    #@db
    .line 13529
    invoke-virtual {p0}, Landroid/view/View;->getPivotY()F

    #@de
    move-result v0

    #@df
    invoke-virtual {p1, v0}, Landroid/view/DisplayList;->setPivotY(F)V

    #@e2
    .line 13535
    .end local v1           #alpha:F
    :cond_e2
    :goto_e2
    return-void

    #@e3
    .line 13487
    :cond_e3
    const/4 v0, 0x0

    #@e4
    goto/16 :goto_25

    #@e6
    .line 13531
    .restart local v1       #alpha:F
    :cond_e6
    const/high16 v0, 0x3f80

    #@e8
    cmpg-float v0, v1, v0

    #@ea
    if-gez v0, :cond_e2

    #@ec
    .line 13532
    invoke-virtual {p1, v1}, Landroid/view/DisplayList;->setAlpha(F)V

    #@ef
    goto :goto_e2
.end method

.method public setDrawingCacheBackgroundColor(I)V
    .registers 4
    .parameter "color"

    #@0
    .prologue
    .line 13014
    iget v0, p0, Landroid/view/View;->mDrawingCacheBackgroundColor:I

    #@2
    if-eq p1, v0, :cond_e

    #@4
    .line 13015
    iput p1, p0, Landroid/view/View;->mDrawingCacheBackgroundColor:I

    #@6
    .line 13016
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    const v1, -0x8001

    #@b
    and-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    .line 13018
    :cond_e
    return-void
.end method

.method public setDrawingCacheEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const v1, 0x8000

    #@3
    const/4 v0, 0x0

    #@4
    .line 12715
    iput-boolean v0, p0, Landroid/view/View;->mCachingFailed:Z

    #@6
    .line 12716
    if-eqz p1, :cond_9

    #@8
    move v0, v1

    #@9
    :cond_9
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@c
    .line 12717
    return-void
.end method

.method public setDrawingCacheQuality(I)V
    .registers 3
    .parameter "quality"

    #@0
    .prologue
    .line 5520
    const/high16 v0, 0x18

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->setFlags(II)V

    #@5
    .line 5521
    return-void
.end method

.method public setDuplicateParentStateEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const/high16 v1, 0x40

    #@2
    .line 12384
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 12385
    return-void

    #@9
    .line 12384
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setEnabled(Z)V
    .registers 4
    .parameter "enabled"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/16 v1, 0x20

    #@2
    .line 5864
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    #@5
    move-result v0

    #@6
    if-ne p1, v0, :cond_9

    #@8
    .line 5877
    :goto_8
    return-void

    #@9
    .line 5866
    :cond_9
    if-eqz p1, :cond_17

    #@b
    const/4 v0, 0x0

    #@c
    :goto_c
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@f
    .line 5872
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@12
    .line 5876
    const/4 v0, 0x1

    #@13
    invoke-virtual {p0, v0}, Landroid/view/View;->invalidate(Z)V

    #@16
    goto :goto_8

    #@17
    :cond_17
    move v0, v1

    #@18
    .line 5866
    goto :goto_c
.end method

.method public setFadingEdgeLength(I)V
    .registers 3
    .parameter "length"

    #@0
    .prologue
    .line 3978
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@3
    .line 3979
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@5
    iput p1, v0, Landroid/view/View$ScrollabilityCache;->fadingEdgeLength:I

    #@7
    .line 3980
    return-void
.end method

.method public setFilterTouchesWhenObscured(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const/16 v1, 0x400

    #@2
    .line 6337
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6339
    return-void

    #@9
    :cond_9
    move v0, v1

    #@a
    .line 6337
    goto :goto_5
.end method

.method public setFitsSystemWindows(Z)V
    .registers 4
    .parameter "fitSystemWindows"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 5776
    if-eqz p1, :cond_8

    #@3
    move v0, v1

    #@4
    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@7
    .line 5777
    return-void

    #@8
    .line 5776
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_4
.end method

.method setFlags(II)V
    .registers 11
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const v7, -0x8001

    #@4
    const/4 v6, 0x1

    #@5
    .line 8550
    iget v1, p0, Landroid/view/View;->mViewFlags:I

    #@7
    .line 8551
    .local v1, old:I
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@9
    xor-int/lit8 v4, p2, -0x1

    #@b
    and-int/2addr v3, v4

    #@c
    and-int v4, p1, p2

    #@e
    or-int/2addr v3, v4

    #@f
    iput v3, p0, Landroid/view/View;->mViewFlags:I

    #@11
    .line 8553
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@13
    xor-int v0, v3, v1

    #@15
    .line 8554
    .local v0, changed:I
    if-nez v0, :cond_18

    #@17
    .line 8696
    :cond_17
    :goto_17
    return-void

    #@18
    .line 8557
    :cond_18
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@1a
    .line 8560
    .local v2, privateFlags:I
    and-int/lit8 v3, v0, 0x1

    #@1c
    if-eqz v3, :cond_3c

    #@1e
    and-int/lit8 v3, v2, 0x10

    #@20
    if-eqz v3, :cond_3c

    #@22
    .line 8562
    and-int/lit8 v3, v1, 0x1

    #@24
    if-ne v3, v6, :cond_170

    #@26
    and-int/lit8 v3, v2, 0x2

    #@28
    if-eqz v3, :cond_170

    #@2a
    .line 8565
    invoke-virtual {p0}, Landroid/view/View;->clearFocus()V

    #@2d
    .line 8574
    :cond_2d
    :goto_2d
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@2f
    invoke-static {v3}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@36
    move-result v3

    #@37
    if-eqz v3, :cond_3c

    #@39
    .line 8575
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@3c
    .line 8579
    :cond_3c
    and-int/lit8 v3, p1, 0xc

    #@3e
    if-nez v3, :cond_65

    #@40
    .line 8580
    and-int/lit8 v3, v0, 0xc

    #@42
    if-eqz v3, :cond_65

    #@44
    .line 8586
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@46
    or-int/lit8 v3, v3, 0x20

    #@48
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@4a
    .line 8587
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@4d
    .line 8589
    invoke-virtual {p0, v6}, Landroid/view/View;->needGlobalAttributesUpdate(Z)V

    #@50
    .line 8595
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@52
    if-eqz v3, :cond_65

    #@54
    iget v3, p0, Landroid/view/View;->mBottom:I

    #@56
    iget v4, p0, Landroid/view/View;->mTop:I

    #@58
    if-le v3, v4, :cond_65

    #@5a
    iget v3, p0, Landroid/view/View;->mRight:I

    #@5c
    iget v4, p0, Landroid/view/View;->mLeft:I

    #@5e
    if-le v3, v4, :cond_65

    #@60
    .line 8596
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@62
    invoke-interface {v3, p0}, Landroid/view/ViewParent;->focusableViewAvailable(Landroid/view/View;)V

    #@65
    .line 8602
    :cond_65
    and-int/lit8 v3, v0, 0x8

    #@67
    if-eqz v3, :cond_a1

    #@69
    .line 8603
    invoke-virtual {p0, v5}, Landroid/view/View;->needGlobalAttributesUpdate(Z)V

    #@6c
    .line 8604
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@6f
    .line 8606
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@71
    and-int/lit8 v3, v3, 0xc

    #@73
    const/16 v4, 0x8

    #@75
    if-ne v3, v4, :cond_99

    #@77
    .line 8607
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    #@7a
    move-result v3

    #@7b
    if-eqz v3, :cond_80

    #@7d
    invoke-virtual {p0}, Landroid/view/View;->clearFocus()V

    #@80
    .line 8608
    :cond_80
    invoke-virtual {p0}, Landroid/view/View;->clearAccessibilityFocus()V

    #@83
    .line 8609
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    #@86
    .line 8610
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@88
    instance-of v3, v3, Landroid/view/View;

    #@8a
    if-eqz v3, :cond_93

    #@8c
    .line 8612
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@8e
    check-cast v3, Landroid/view/View;

    #@90
    invoke-virtual {v3, v6}, Landroid/view/View;->invalidate(Z)V

    #@93
    .line 8616
    :cond_93
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@95
    or-int/lit8 v3, v3, 0x20

    #@97
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@99
    .line 8618
    :cond_99
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9b
    if-eqz v3, :cond_a1

    #@9d
    .line 8619
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9f
    iput-boolean v6, v3, Landroid/view/View$AttachInfo;->mViewVisibilityChanged:Z

    #@a1
    .line 8624
    :cond_a1
    and-int/lit8 v3, v0, 0x4

    #@a3
    if-eqz v3, :cond_cf

    #@a5
    .line 8625
    invoke-virtual {p0, v5}, Landroid/view/View;->needGlobalAttributesUpdate(Z)V

    #@a8
    .line 8630
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@aa
    or-int/lit8 v3, v3, 0x20

    #@ac
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@ae
    .line 8632
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@b0
    and-int/lit8 v3, v3, 0xc

    #@b2
    const/4 v4, 0x4

    #@b3
    if-ne v3, v4, :cond_c7

    #@b5
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    #@b8
    move-result v3

    #@b9
    if-eqz v3, :cond_c7

    #@bb
    .line 8634
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@be
    move-result-object v3

    #@bf
    if-eq v3, p0, :cond_c7

    #@c1
    .line 8635
    invoke-virtual {p0}, Landroid/view/View;->clearFocus()V

    #@c4
    .line 8636
    invoke-virtual {p0}, Landroid/view/View;->clearAccessibilityFocus()V

    #@c7
    .line 8639
    :cond_c7
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@c9
    if-eqz v3, :cond_cf

    #@cb
    .line 8640
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@cd
    iput-boolean v6, v3, Landroid/view/View$AttachInfo;->mViewVisibilityChanged:Z

    #@cf
    .line 8644
    :cond_cf
    and-int/lit8 v3, v0, 0xc

    #@d1
    if-eqz v3, :cond_f0

    #@d3
    .line 8645
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@d5
    instance-of v3, v3, Landroid/view/ViewGroup;

    #@d7
    if-eqz v3, :cond_183

    #@d9
    .line 8646
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@db
    check-cast v3, Landroid/view/ViewGroup;

    #@dd
    and-int/lit8 v4, v0, 0xc

    #@df
    and-int/lit8 v5, p1, 0xc

    #@e1
    invoke-virtual {v3, p0, v4, v5}, Landroid/view/ViewGroup;->onChildVisibilityChanged(Landroid/view/View;II)V

    #@e4
    .line 8648
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@e6
    check-cast v3, Landroid/view/View;

    #@e8
    invoke-virtual {v3, v6}, Landroid/view/View;->invalidate(Z)V

    #@eb
    .line 8652
    :cond_eb
    :goto_eb
    and-int/lit8 v3, p1, 0xc

    #@ed
    invoke-virtual {p0, p0, v3}, Landroid/view/View;->dispatchVisibilityChanged(Landroid/view/View;I)V

    #@f0
    .line 8655
    :cond_f0
    const/high16 v3, 0x2

    #@f2
    and-int/2addr v3, v0

    #@f3
    if-eqz v3, :cond_f8

    #@f5
    .line 8656
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    #@f8
    .line 8659
    :cond_f8
    const v3, 0x8000

    #@fb
    and-int/2addr v3, v0

    #@fc
    if-eqz v3, :cond_109

    #@fe
    .line 8660
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    #@101
    .line 8661
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@103
    and-int/2addr v3, v7

    #@104
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@106
    .line 8662
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@109
    .line 8665
    :cond_109
    const/high16 v3, 0x18

    #@10b
    and-int/2addr v3, v0

    #@10c
    if-eqz v3, :cond_116

    #@10e
    .line 8666
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    #@111
    .line 8667
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@113
    and-int/2addr v3, v7

    #@114
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@116
    .line 8670
    :cond_116
    and-int/lit16 v3, v0, 0x80

    #@118
    if-eqz v3, :cond_136

    #@11a
    .line 8671
    iget v3, p0, Landroid/view/View;->mViewFlags:I

    #@11c
    and-int/lit16 v3, v3, 0x80

    #@11e
    if-eqz v3, :cond_196

    #@120
    .line 8672
    iget-object v3, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@122
    if-eqz v3, :cond_18f

    #@124
    .line 8673
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@126
    and-int/lit16 v3, v3, -0x81

    #@128
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@12a
    .line 8674
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@12c
    or-int/lit16 v3, v3, 0x100

    #@12e
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@130
    .line 8681
    :goto_130
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@133
    .line 8682
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@136
    .line 8685
    :cond_136
    const/high16 v3, 0x400

    #@138
    and-int/2addr v3, v0

    #@139
    if-eqz v3, :cond_14e

    #@13b
    .line 8686
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@13d
    if-eqz v3, :cond_14e

    #@13f
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@141
    if-eqz v3, :cond_14e

    #@143
    iget-object v3, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@145
    iget-boolean v3, v3, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@147
    if-nez v3, :cond_14e

    #@149
    .line 8687
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@14b
    invoke-interface {v3, p0}, Landroid/view/ViewParent;->recomputeViewAttributes(Landroid/view/View;)V

    #@14e
    .line 8691
    :cond_14e
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@150
    invoke-static {v3}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@153
    move-result-object v3

    #@154
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@157
    move-result v3

    #@158
    if-eqz v3, :cond_17

    #@15a
    and-int/lit8 v3, v0, 0x1

    #@15c
    if-nez v3, :cond_16b

    #@15e
    and-int/lit16 v3, v0, 0x4000

    #@160
    if-nez v3, :cond_16b

    #@162
    const/high16 v3, 0x20

    #@164
    and-int/2addr v3, v0

    #@165
    if-nez v3, :cond_16b

    #@167
    and-int/lit8 v3, v0, 0x0

    #@169
    if-eqz v3, :cond_17

    #@16b
    .line 8694
    :cond_16b
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@16e
    goto/16 :goto_17

    #@170
    .line 8566
    :cond_170
    and-int/lit8 v3, v1, 0x1

    #@172
    if-nez v3, :cond_2d

    #@174
    and-int/lit8 v3, v2, 0x2

    #@176
    if-nez v3, :cond_2d

    #@178
    .line 8572
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@17a
    if-eqz v3, :cond_2d

    #@17c
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@17e
    invoke-interface {v3, p0}, Landroid/view/ViewParent;->focusableViewAvailable(Landroid/view/View;)V

    #@181
    goto/16 :goto_2d

    #@183
    .line 8649
    :cond_183
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@185
    if-eqz v3, :cond_eb

    #@187
    .line 8650
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@189
    const/4 v4, 0x0

    #@18a
    invoke-interface {v3, p0, v4}, Landroid/view/ViewParent;->invalidateChild(Landroid/view/View;Landroid/graphics/Rect;)V

    #@18d
    goto/16 :goto_eb

    #@18f
    .line 8676
    :cond_18f
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@191
    or-int/lit16 v3, v3, 0x80

    #@193
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@195
    goto :goto_130

    #@196
    .line 8679
    :cond_196
    iget v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@198
    and-int/lit16 v3, v3, -0x81

    #@19a
    iput v3, p0, Landroid/view/View;->mPrivateFlags:I

    #@19c
    goto :goto_130
.end method

.method public setFocusable(Z)V
    .registers 5
    .parameter "focusable"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 5891
    if-nez p1, :cond_9

    #@4
    .line 5892
    const/high16 v2, 0x4

    #@6
    invoke-virtual {p0, v0, v2}, Landroid/view/View;->setFlags(II)V

    #@9
    .line 5894
    :cond_9
    if-eqz p1, :cond_c

    #@b
    move v0, v1

    #@c
    :cond_c
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@f
    .line 5895
    return-void
.end method

.method public setFocusableInTouchMode(Z)V
    .registers 5
    .parameter "focusableInTouchMode"

    #@0
    .prologue
    const/high16 v1, 0x4

    #@2
    const/4 v2, 0x1

    #@3
    .line 5913
    if-eqz p1, :cond_f

    #@5
    move v0, v1

    #@6
    :goto_6
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@9
    .line 5914
    if-eqz p1, :cond_e

    #@b
    .line 5915
    invoke-virtual {p0, v2, v2}, Landroid/view/View;->setFlags(II)V

    #@e
    .line 5917
    :cond_e
    return-void

    #@f
    .line 5913
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_6
.end method

.method protected setFrame(IIII)Z
    .registers 18
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 14284
    const/4 v1, 0x0

    #@1
    .line 14291
    .local v1, changed:Z
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@3
    if-ne v8, p1, :cond_15

    #@5
    iget v8, p0, Landroid/view/View;->mRight:I

    #@7
    move/from16 v0, p3

    #@9
    if-ne v8, v0, :cond_15

    #@b
    iget v8, p0, Landroid/view/View;->mTop:I

    #@d
    if-ne v8, p2, :cond_15

    #@f
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@11
    move/from16 v0, p4

    #@13
    if-eq v8, v0, :cond_84

    #@15
    .line 14292
    :cond_15
    const/4 v1, 0x1

    #@16
    .line 14295
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@18
    and-int/lit8 v2, v8, 0x20

    #@1a
    .line 14297
    .local v2, drawn:I
    iget v8, p0, Landroid/view/View;->mRight:I

    #@1c
    iget v9, p0, Landroid/view/View;->mLeft:I

    #@1e
    sub-int v6, v8, v9

    #@20
    .line 14298
    .local v6, oldWidth:I
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@22
    iget v9, p0, Landroid/view/View;->mTop:I

    #@24
    sub-int v5, v8, v9

    #@26
    .line 14299
    .local v5, oldHeight:I
    sub-int v4, p3, p1

    #@28
    .line 14300
    .local v4, newWidth:I
    sub-int v3, p4, p2

    #@2a
    .line 14301
    .local v3, newHeight:I
    if-ne v4, v6, :cond_2e

    #@2c
    if-eq v3, v5, :cond_85

    #@2e
    :cond_2e
    const/4 v7, 0x1

    #@2f
    .line 14304
    .local v7, sizeChanged:Z
    :goto_2f
    invoke-virtual {p0, v7}, Landroid/view/View;->invalidate(Z)V

    #@32
    .line 14306
    iput p1, p0, Landroid/view/View;->mLeft:I

    #@34
    .line 14307
    iput p2, p0, Landroid/view/View;->mTop:I

    #@36
    .line 14308
    move/from16 v0, p3

    #@38
    iput v0, p0, Landroid/view/View;->mRight:I

    #@3a
    .line 14309
    move/from16 v0, p4

    #@3c
    iput v0, p0, Landroid/view/View;->mBottom:I

    #@3e
    .line 14310
    iget-object v8, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@40
    if-eqz v8, :cond_4f

    #@42
    .line 14311
    iget-object v8, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@44
    iget v9, p0, Landroid/view/View;->mLeft:I

    #@46
    iget v10, p0, Landroid/view/View;->mTop:I

    #@48
    iget v11, p0, Landroid/view/View;->mRight:I

    #@4a
    iget v12, p0, Landroid/view/View;->mBottom:I

    #@4c
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    #@4f
    .line 14314
    :cond_4f
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@51
    or-int/lit8 v8, v8, 0x10

    #@53
    iput v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@55
    .line 14317
    if-eqz v7, :cond_6a

    #@57
    .line 14318
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@59
    const/high16 v9, 0x2000

    #@5b
    and-int/2addr v8, v9

    #@5c
    if-nez v8, :cond_67

    #@5e
    .line 14320
    iget-object v8, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@60
    if-eqz v8, :cond_67

    #@62
    .line 14321
    iget-object v8, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@64
    const/4 v9, 0x1

    #@65
    iput-boolean v9, v8, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@67
    .line 14324
    :cond_67
    invoke-virtual {p0, v4, v3, v6, v5}, Landroid/view/View;->onSizeChanged(IIII)V

    #@6a
    .line 14327
    :cond_6a
    iget v8, p0, Landroid/view/View;->mViewFlags:I

    #@6c
    and-int/lit8 v8, v8, 0xc

    #@6e
    if-nez v8, :cond_7c

    #@70
    .line 14333
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@72
    or-int/lit8 v8, v8, 0x20

    #@74
    iput v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@76
    .line 14334
    invoke-virtual {p0, v7}, Landroid/view/View;->invalidate(Z)V

    #@79
    .line 14337
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@7c
    .line 14341
    :cond_7c
    iget v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@7e
    or-int/2addr v8, v2

    #@7f
    iput v8, p0, Landroid/view/View;->mPrivateFlags:I

    #@81
    .line 14343
    const/4 v8, 0x1

    #@82
    iput-boolean v8, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@84
    .line 14345
    .end local v2           #drawn:I
    .end local v3           #newHeight:I
    .end local v4           #newWidth:I
    .end local v5           #oldHeight:I
    .end local v6           #oldWidth:I
    .end local v7           #sizeChanged:Z
    :cond_84
    return v1

    #@85
    .line 14301
    .restart local v2       #drawn:I
    .restart local v3       #newHeight:I
    .restart local v4       #newWidth:I
    .restart local v5       #oldHeight:I
    .restart local v6       #oldWidth:I
    :cond_85
    const/4 v7, 0x0

    #@86
    goto :goto_2f
.end method

.method public setHapticFeedbackEnabled(Z)V
    .registers 4
    .parameter "hapticFeedbackEnabled"

    #@0
    .prologue
    const/high16 v1, 0x1000

    #@2
    .line 5961
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 5962
    return-void

    #@9
    .line 5961
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setHasTransientState(Z)V
    .registers 6
    .parameter "hasTransientState"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 6105
    if-eqz p1, :cond_3a

    #@3
    iget v1, p0, Landroid/view/View;->mTransientStateCount:I

    #@5
    add-int/lit8 v1, v1, 0x1

    #@7
    :goto_7
    iput v1, p0, Landroid/view/View;->mTransientStateCount:I

    #@9
    .line 6107
    iget v1, p0, Landroid/view/View;->mTransientStateCount:I

    #@b
    if-gez v1, :cond_16

    #@d
    .line 6108
    iput v2, p0, Landroid/view/View;->mTransientStateCount:I

    #@f
    .line 6109
    const-string v1, "View"

    #@11
    const-string v3, "hasTransientState decremented below 0: unmatched pair of setHasTransientState calls"

    #@13
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 6112
    :cond_16
    if-eqz p1, :cond_1d

    #@18
    iget v1, p0, Landroid/view/View;->mTransientStateCount:I

    #@1a
    const/4 v3, 0x1

    #@1b
    if-eq v1, v3, :cond_23

    #@1d
    :cond_1d
    if-nez p1, :cond_39

    #@1f
    iget v1, p0, Landroid/view/View;->mTransientStateCount:I

    #@21
    if-nez v1, :cond_39

    #@23
    .line 6115
    :cond_23
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@25
    const v3, -0x400001

    #@28
    and-int/2addr v3, v1

    #@29
    if-eqz p1, :cond_3f

    #@2b
    const/high16 v1, 0x40

    #@2d
    :goto_2d
    or-int/2addr v1, v3

    #@2e
    iput v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@30
    .line 6117
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@32
    if-eqz v1, :cond_39

    #@34
    .line 6119
    :try_start_34
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@36
    invoke-interface {v1, p0, p1}, Landroid/view/ViewParent;->childHasTransientStateChanged(Landroid/view/View;Z)V
    :try_end_39
    .catch Ljava/lang/AbstractMethodError; {:try_start_34 .. :try_end_39} :catch_41

    #@39
    .line 6126
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 6105
    :cond_3a
    iget v1, p0, Landroid/view/View;->mTransientStateCount:I

    #@3c
    add-int/lit8 v1, v1, -0x1

    #@3e
    goto :goto_7

    #@3f
    :cond_3f
    move v1, v2

    #@40
    .line 6115
    goto :goto_2d

    #@41
    .line 6120
    :catch_41
    move-exception v0

    #@42
    .line 6121
    .local v0, e:Ljava/lang/AbstractMethodError;
    const-string v1, "View"

    #@44
    new-instance v2, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    iget-object v3, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@4b
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    const-string v3, " does not fully implement ViewParent"

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    goto :goto_39
.end method

.method public setHorizontalFadingEdgeEnabled(Z)V
    .registers 3
    .parameter "horizontalFadingEdgeEnabled"

    #@0
    .prologue
    .line 11032
    invoke-virtual {p0}, Landroid/view/View;->isHorizontalFadingEdgeEnabled()Z

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_11

    #@6
    .line 11033
    if-eqz p1, :cond_b

    #@8
    .line 11034
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@b
    .line 11037
    :cond_b
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@d
    xor-int/lit16 v0, v0, 0x1000

    #@f
    iput v0, p0, Landroid/view/View;->mViewFlags:I

    #@11
    .line 11039
    :cond_11
    return-void
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .registers 3
    .parameter "horizontalScrollBarEnabled"

    #@0
    .prologue
    .line 11159
    invoke-virtual {p0}, Landroid/view/View;->isHorizontalScrollBarEnabled()Z

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_12

    #@6
    .line 11160
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@8
    xor-int/lit16 v0, v0, 0x100

    #@a
    iput v0, p0, Landroid/view/View;->mViewFlags:I

    #@c
    .line 11161
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@f
    .line 11162
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@12
    .line 11164
    :cond_12
    return-void
.end method

.method public setHovered(Z)V
    .registers 4
    .parameter "hovered"

    #@0
    .prologue
    const/high16 v1, 0x1000

    #@2
    .line 8286
    if-eqz p1, :cond_16

    #@4
    .line 8287
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    and-int/2addr v0, v1

    #@7
    if-nez v0, :cond_15

    #@9
    .line 8288
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@b
    or-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    .line 8289
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@11
    .line 8290
    const/4 v0, 0x1

    #@12
    invoke-virtual {p0, v0}, Landroid/view/View;->onHoverChanged(Z)V

    #@15
    .line 8299
    :cond_15
    :goto_15
    return-void

    #@16
    .line 8293
    :cond_16
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@18
    and-int/2addr v0, v1

    #@19
    if-eqz v0, :cond_15

    #@1b
    .line 8294
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@1d
    const v1, -0x10000001

    #@20
    and-int/2addr v0, v1

    #@21
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@23
    .line 8295
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@26
    .line 8296
    const/4 v0, 0x0

    #@27
    invoke-virtual {p0, v0}, Landroid/view/View;->onHoverChanged(Z)V

    #@2a
    goto :goto_15
.end method

.method public setId(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 15446
    iput p1, p0, Landroid/view/View;->mID:I

    #@3
    .line 15447
    iget v0, p0, Landroid/view/View;->mID:I

    #@5
    if-ne v0, v1, :cond_11

    #@7
    iget v0, p0, Landroid/view/View;->mLabelForId:I

    #@9
    if-eq v0, v1, :cond_11

    #@b
    .line 15448
    invoke-static {}, Landroid/view/View;->generateViewId()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/view/View;->mID:I

    #@11
    .line 15450
    :cond_11
    return-void
.end method

.method public setImportantForAccessibility(I)V
    .registers 5
    .parameter "mode"

    #@0
    .prologue
    .line 6872
    invoke-virtual {p0}, Landroid/view/View;->getImportantForAccessibility()I

    #@3
    move-result v0

    #@4
    if-eq p1, v0, :cond_1b

    #@6
    .line 6873
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    const v1, -0x300001

    #@b
    and-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@e
    .line 6874
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@10
    shl-int/lit8 v1, p1, 0x14

    #@12
    const/high16 v2, 0x30

    #@14
    and-int/2addr v1, v2

    #@15
    or-int/2addr v0, v1

    #@16
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@18
    .line 6876
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@1b
    .line 6878
    :cond_1b
    return-void
.end method

.method public setIsRootNamespace(Z)V
    .registers 3
    .parameter "isRoot"

    #@0
    .prologue
    .line 15459
    if-eqz p1, :cond_9

    #@2
    .line 15460
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@4
    or-int/lit8 v0, v0, 0x8

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    .line 15464
    :goto_8
    return-void

    #@9
    .line 15462
    :cond_9
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@b
    and-int/lit8 v0, v0, -0x9

    #@d
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@f
    goto :goto_8
.end method

.method public setKeepScreenOn(Z)V
    .registers 4
    .parameter "keepScreenOn"

    #@0
    .prologue
    const/high16 v1, 0x400

    #@2
    .line 5548
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 5549
    return-void

    #@9
    .line 5548
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setLabelFor(I)V
    .registers 4
    .parameter "id"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 5394
    iput p1, p0, Landroid/view/View;->mLabelForId:I

    #@3
    .line 5395
    iget v0, p0, Landroid/view/View;->mLabelForId:I

    #@5
    if-eq v0, v1, :cond_11

    #@7
    iget v0, p0, Landroid/view/View;->mID:I

    #@9
    if-ne v0, v1, :cond_11

    #@b
    .line 5397
    invoke-static {}, Landroid/view/View;->generateViewId()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/view/View;->mID:I

    #@11
    .line 5399
    :cond_11
    return-void
.end method

.method public setLayerPaint(Landroid/graphics/Paint;)V
    .registers 6
    .parameter "paint"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 12504
    invoke-virtual {p0}, Landroid/view/View;->getLayerType()I

    #@4
    move-result v1

    #@5
    .line 12505
    .local v1, layerType:I
    if-eqz v1, :cond_1f

    #@7
    .line 12506
    if-nez p1, :cond_20

    #@9
    new-instance v2, Landroid/graphics/Paint;

    #@b
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@e
    :goto_e
    iput-object v2, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@10
    .line 12507
    const/4 v2, 0x2

    #@11
    if-ne v1, v2, :cond_22

    #@13
    .line 12508
    invoke-virtual {p0}, Landroid/view/View;->getHardwareLayer()Landroid/view/HardwareLayer;

    #@16
    move-result-object v0

    #@17
    .line 12509
    .local v0, layer:Landroid/view/HardwareLayer;
    if-eqz v0, :cond_1c

    #@19
    .line 12510
    invoke-virtual {v0, p1}, Landroid/view/HardwareLayer;->setLayerPaint(Landroid/graphics/Paint;)V

    #@1c
    .line 12512
    :cond_1c
    invoke-virtual {p0, v3, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@1f
    .line 12517
    .end local v0           #layer:Landroid/view/HardwareLayer;
    :cond_1f
    :goto_1f
    return-void

    #@20
    :cond_20
    move-object v2, p1

    #@21
    .line 12506
    goto :goto_e

    #@22
    .line 12514
    :cond_22
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    #@25
    goto :goto_1f
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .registers 7
    .parameter "layerType"
    .parameter "paint"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v2, 0x1

    #@3
    .line 12440
    if-ltz p1, :cond_8

    #@5
    const/4 v3, 0x2

    #@6
    if-le p1, v3, :cond_10

    #@8
    .line 12441
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    const-string v2, "Layer type can only be one of: LAYER_TYPE_NONE, LAYER_TYPE_SOFTWARE or LAYER_TYPE_HARDWARE"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 12445
    :cond_10
    iget v3, p0, Landroid/view/View;->mLayerType:I

    #@12
    if-ne p1, v3, :cond_2a

    #@14
    .line 12446
    if-eqz p1, :cond_29

    #@16
    iget-object v1, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@18
    if-eq p2, v1, :cond_29

    #@1a
    .line 12447
    if-nez p2, :cond_21

    #@1c
    new-instance p2, Landroid/graphics/Paint;

    #@1e
    .end local p2
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    #@21
    :cond_21
    iput-object p2, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@23
    .line 12448
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@26
    .line 12449
    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Z)V

    #@29
    .line 12473
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 12455
    .restart local p2
    :cond_2a
    iget v3, p0, Landroid/view/View;->mLayerType:I

    #@2c
    packed-switch v3, :pswitch_data_5c

    #@2f
    .line 12466
    :goto_2f
    iput p1, p0, Landroid/view/View;->mLayerType:I

    #@31
    .line 12467
    iget v3, p0, Landroid/view/View;->mLayerType:I

    #@33
    if-nez v3, :cond_36

    #@35
    move v0, v2

    #@36
    .line 12468
    .local v0, layerDisabled:Z
    :cond_36
    if-eqz v0, :cond_4d

    #@38
    move-object p2, v1

    #@39
    .end local p2
    :cond_39
    :goto_39
    iput-object p2, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@3b
    .line 12469
    if-eqz v0, :cond_55

    #@3d
    :goto_3d
    iput-object v1, p0, Landroid/view/View;->mLocalDirtyRect:Landroid/graphics/Rect;

    #@3f
    .line 12471
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@42
    .line 12472
    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Z)V

    #@45
    goto :goto_29

    #@46
    .line 12457
    .end local v0           #layerDisabled:Z
    .restart local p2
    :pswitch_46
    invoke-virtual {p0, v0}, Landroid/view/View;->destroyLayer(Z)Z

    #@49
    .line 12460
    :pswitch_49
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    #@4c
    goto :goto_2f

    #@4d
    .line 12468
    .restart local v0       #layerDisabled:Z
    :cond_4d
    if-nez p2, :cond_39

    #@4f
    new-instance p2, Landroid/graphics/Paint;

    #@51
    .end local p2
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    #@54
    goto :goto_39

    #@55
    .line 12469
    :cond_55
    new-instance v1, Landroid/graphics/Rect;

    #@57
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@5a
    goto :goto_3d

    #@5b
    .line 12455
    nop

    #@5c
    :pswitch_data_5c
    .packed-switch 0x1
        :pswitch_49
        :pswitch_46
    .end packed-switch
.end method

.method public setLayoutDirection(I)V
    .registers 4
    .parameter "layoutDirection"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 6018
    invoke-virtual {p0}, Landroid/view/View;->getRawLayoutDirection()I

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_22

    #@6
    .line 6020
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    and-int/lit8 v0, v0, -0xd

    #@a
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@c
    .line 6021
    invoke-virtual {p0}, Landroid/view/View;->resetRtlProperties()V

    #@f
    .line 6023
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@11
    shl-int/lit8 v1, p1, 0x2

    #@13
    and-int/lit8 v1, v1, 0xc

    #@15
    or-int/2addr v0, v1

    #@16
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@18
    .line 6026
    invoke-virtual {p0}, Landroid/view/View;->resolveRtlPropertiesIfNeeded()V

    #@1b
    .line 6027
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@1e
    .line 6028
    const/4 v0, 0x1

    #@1f
    invoke-virtual {p0, v0}, Landroid/view/View;->invalidate(Z)V

    #@22
    .line 6030
    :cond_22
    return-void
.end method

.method public setLayoutInsets(Landroid/graphics/Insets;)V
    .registers 2
    .parameter "layoutInsets"

    #@0
    .prologue
    .line 15086
    iput-object p1, p0, Landroid/view/View;->mLayoutInsets:Landroid/graphics/Insets;

    #@2
    .line 15087
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 10181
    if-nez p1, :cond_a

    #@2
    .line 10182
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "Layout parameters cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 10184
    :cond_a
    iput-object p1, p0, Landroid/view/View;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    #@c
    .line 10185
    invoke-virtual {p0}, Landroid/view/View;->resolveLayoutParams()V

    #@f
    .line 10186
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@11
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@13
    if-eqz v0, :cond_1c

    #@15
    .line 10187
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@17
    check-cast v0, Landroid/view/ViewGroup;

    #@19
    invoke-virtual {v0, p0, p1}, Landroid/view/ViewGroup;->onSetLayoutParams(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1c
    .line 10189
    :cond_1c
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@1f
    .line 10190
    return-void
.end method

.method public final setLeft(I)V
    .registers 13
    .parameter "left"

    #@0
    .prologue
    const/high16 v10, 0x1000

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    .line 9716
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@6
    if-eq p1, v7, :cond_74

    #@8
    .line 9717
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@b
    .line 9718
    iget-object v7, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    if-eqz v7, :cond_17

    #@f
    iget-object v7, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@11
    invoke-static {v7}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@14
    move-result v7

    #@15
    if-eqz v7, :cond_75

    #@17
    :cond_17
    move v1, v6

    #@18
    .line 9720
    .local v1, matrixIsIdentity:Z
    :goto_18
    if-eqz v1, :cond_7b

    #@1a
    .line 9721
    iget-object v7, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1c
    if-eqz v7, :cond_32

    #@1e
    .line 9724
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@20
    if-ge p1, v7, :cond_77

    #@22
    .line 9725
    move v2, p1

    #@23
    .line 9726
    .local v2, minLeft:I
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@25
    sub-int v4, p1, v7

    #@27
    .line 9731
    .local v4, xLoc:I
    :goto_27
    iget v7, p0, Landroid/view/View;->mRight:I

    #@29
    sub-int/2addr v7, v2

    #@2a
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@2c
    iget v9, p0, Landroid/view/View;->mTop:I

    #@2e
    sub-int/2addr v8, v9

    #@2f
    invoke-virtual {p0, v4, v5, v7, v8}, Landroid/view/View;->invalidate(IIII)V

    #@32
    .line 9738
    .end local v2           #minLeft:I
    .end local v4           #xLoc:I
    :cond_32
    :goto_32
    iget v5, p0, Landroid/view/View;->mRight:I

    #@34
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@36
    sub-int v3, v5, v7

    #@38
    .line 9739
    .local v3, oldWidth:I
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@3a
    iget v7, p0, Landroid/view/View;->mTop:I

    #@3c
    sub-int v0, v5, v7

    #@3e
    .line 9741
    .local v0, height:I
    iput p1, p0, Landroid/view/View;->mLeft:I

    #@40
    .line 9742
    iget-object v5, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@42
    if-eqz v5, :cond_49

    #@44
    .line 9743
    iget-object v5, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@46
    invoke-virtual {v5, p1}, Landroid/view/DisplayList;->setLeft(I)V

    #@49
    .line 9746
    :cond_49
    iget v5, p0, Landroid/view/View;->mRight:I

    #@4b
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@4d
    sub-int/2addr v5, v7

    #@4e
    invoke-virtual {p0, v5, v0, v3, v0}, Landroid/view/View;->onSizeChanged(IIII)V

    #@51
    .line 9748
    if-nez v1, :cond_67

    #@53
    .line 9749
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@55
    const/high16 v7, 0x2000

    #@57
    and-int/2addr v5, v7

    #@58
    if-nez v5, :cond_5e

    #@5a
    .line 9751
    iget-object v5, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5c
    iput-boolean v6, v5, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@5e
    .line 9753
    :cond_5e
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@60
    or-int/lit8 v5, v5, 0x20

    #@62
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@64
    .line 9754
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@67
    .line 9756
    :cond_67
    iput-boolean v6, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@69
    .line 9757
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@6c
    .line 9758
    iget v5, p0, Landroid/view/View;->mPrivateFlags2:I

    #@6e
    and-int/2addr v5, v10

    #@6f
    if-ne v5, v10, :cond_74

    #@71
    .line 9760
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@74
    .line 9763
    .end local v0           #height:I
    .end local v1           #matrixIsIdentity:Z
    .end local v3           #oldWidth:I
    :cond_74
    return-void

    #@75
    :cond_75
    move v1, v5

    #@76
    .line 9718
    goto :goto_18

    #@77
    .line 9728
    .restart local v1       #matrixIsIdentity:Z
    :cond_77
    iget v2, p0, Landroid/view/View;->mLeft:I

    #@79
    .line 9729
    .restart local v2       #minLeft:I
    const/4 v4, 0x0

    #@7a
    .restart local v4       #xLoc:I
    goto :goto_27

    #@7b
    .line 9735
    .end local v2           #minLeft:I
    .end local v4           #xLoc:I
    :cond_7b
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@7e
    goto :goto_32
.end method

.method public setLongClickable(Z)V
    .registers 4
    .parameter "longClickable"

    #@0
    .prologue
    const/high16 v1, 0x20

    #@2
    .line 6226
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6227
    return-void

    #@9
    .line 6226
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method protected final setMeasuredDimension(II)V
    .registers 4
    .parameter "measuredWidth"
    .parameter "measuredHeight"

    #@0
    .prologue
    .line 15849
    iput p1, p0, Landroid/view/View;->mMeasuredWidth:I

    #@2
    .line 15850
    iput p2, p0, Landroid/view/View;->mMeasuredHeight:I

    #@4
    .line 15852
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@6
    or-int/lit16 v0, v0, 0x800

    #@8
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    .line 15853
    return-void
.end method

.method public setMinimumHeight(I)V
    .registers 2
    .parameter "minHeight"

    #@0
    .prologue
    .line 15992
    iput p1, p0, Landroid/view/View;->mMinHeight:I

    #@2
    .line 15993
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@5
    .line 15994
    return-void
.end method

.method public setMinimumWidth(I)V
    .registers 2
    .parameter "minWidth"

    #@0
    .prologue
    .line 16021
    iput p1, p0, Landroid/view/View;->mMinWidth:I

    #@2
    .line 16022
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@5
    .line 16024
    return-void
.end method

.method public setNextFocusDownId(I)V
    .registers 2
    .parameter "nextFocusDownId"

    #@0
    .prologue
    .line 5632
    iput p1, p0, Landroid/view/View;->mNextFocusDownId:I

    #@2
    .line 5633
    return-void
.end method

.method public setNextFocusForwardId(I)V
    .registers 2
    .parameter "nextFocusForwardId"

    #@0
    .prologue
    .line 5653
    iput p1, p0, Landroid/view/View;->mNextFocusForwardId:I

    #@2
    .line 5654
    return-void
.end method

.method public setNextFocusLeftId(I)V
    .registers 2
    .parameter "nextFocusLeftId"

    #@0
    .prologue
    .line 5569
    iput p1, p0, Landroid/view/View;->mNextFocusLeftId:I

    #@2
    .line 5570
    return-void
.end method

.method public setNextFocusRightId(I)V
    .registers 2
    .parameter "nextFocusRightId"

    #@0
    .prologue
    .line 5590
    iput p1, p0, Landroid/view/View;->mNextFocusRightId:I

    #@2
    .line 5591
    return-void
.end method

.method public setNextFocusUpId(I)V
    .registers 2
    .parameter "nextFocusUpId"

    #@0
    .prologue
    .line 5611
    iput p1, p0, Landroid/view/View;->mNextFocusUpId:I

    #@2
    .line 5612
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4263
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    .line 4264
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    #@a
    .line 4266
    :cond_a
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@d
    move-result-object v0

    #@e
    iput-object p1, v0, Landroid/view/View$ListenerInfo;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@10
    .line 4267
    return-void
.end method

.method public setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4301
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    .line 4302
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/view/View;->setLongClickable(Z)V

    #@a
    .line 4304
    :cond_a
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@d
    move-result-object v0

    #@e
    iput-object p1, v0, Landroid/view/View$ListenerInfo;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    #@10
    .line 4305
    return-void
.end method

.method public setOnDragListener(Landroid/view/View$OnDragListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4472
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Landroid/view/View$ListenerInfo;->access$602(Landroid/view/View$ListenerInfo;Landroid/view/View$OnDragListener;)Landroid/view/View$OnDragListener;

    #@7
    .line 4473
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4178
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    iput-object p1, v0, Landroid/view/View$ListenerInfo;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    #@6
    .line 4179
    return-void
.end method

.method public setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4453
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Landroid/view/View$ListenerInfo;->access$402(Landroid/view/View$ListenerInfo;Landroid/view/View$OnGenericMotionListener;)Landroid/view/View$OnGenericMotionListener;

    #@7
    .line 4454
    return-void
.end method

.method public setOnHoverListener(Landroid/view/View$OnHoverListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4461
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Landroid/view/View$ListenerInfo;->access$502(Landroid/view/View$ListenerInfo;Landroid/view/View$OnHoverListener;)Landroid/view/View$OnHoverListener;

    #@7
    .line 4462
    return-void
.end method

.method public setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4437
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Landroid/view/View$ListenerInfo;->access$202(Landroid/view/View$ListenerInfo;Landroid/view/View$OnKeyListener;)Landroid/view/View$OnKeyListener;

    #@7
    .line 4438
    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4287
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    .line 4288
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Landroid/view/View;->setLongClickable(Z)V

    #@a
    .line 4290
    :cond_a
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@d
    move-result-object v0

    #@e
    iput-object p1, v0, Landroid/view/View$ListenerInfo;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    #@10
    .line 4291
    return-void
.end method

.method public setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 16329
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Landroid/view/View$ListenerInfo;->access$1002(Landroid/view/View$ListenerInfo;Landroid/view/View$OnSystemUiVisibilityChangeListener;)Landroid/view/View$OnSystemUiVisibilityChangeListener;

    #@7
    .line 16330
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@9
    if-eqz v0, :cond_1a

    #@b
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@d
    if-eqz v0, :cond_1a

    #@f
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@11
    iget-boolean v0, v0, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@13
    if-nez v0, :cond_1a

    #@15
    .line 16331
    iget-object v0, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@17
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->recomputeViewAttributes(Landroid/view/View;)V

    #@1a
    .line 16333
    :cond_1a
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .registers 3
    .parameter "l"

    #@0
    .prologue
    .line 4445
    invoke-virtual {p0}, Landroid/view/View;->getListenerInfo()Landroid/view/View$ListenerInfo;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Landroid/view/View$ListenerInfo;->access$302(Landroid/view/View$ListenerInfo;Landroid/view/View$OnTouchListener;)Landroid/view/View$OnTouchListener;

    #@7
    .line 4446
    return-void
.end method

.method public setOverScrollMode(I)V
    .registers 5
    .parameter "overScrollMode"

    #@0
    .prologue
    .line 16841
    if-eqz p1, :cond_21

    #@2
    const/4 v0, 0x1

    #@3
    if-eq p1, v0, :cond_21

    #@5
    const/4 v0, 0x2

    #@6
    if-eq p1, v0, :cond_21

    #@8
    .line 16844
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Invalid overscroll mode "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 16846
    :cond_21
    iput p1, p0, Landroid/view/View;->mOverScrollMode:I

    #@23
    .line 16847
    return-void
.end method

.method public setPadding(IIII)V
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/high16 v0, -0x8000

    #@2
    .line 14856
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedPadding()V

    #@5
    .line 14858
    iput v0, p0, Landroid/view/View;->mUserPaddingStart:I

    #@7
    .line 14859
    iput v0, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@9
    .line 14861
    iput p1, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@b
    .line 14862
    iput p3, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@d
    .line 14864
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/View;->internalSetPadding(IIII)V

    #@10
    .line 14865
    return-void
.end method

.method public setPaddingRelative(IIII)V
    .registers 6
    .parameter "start"
    .parameter "top"
    .parameter "end"
    .parameter "bottom"

    #@0
    .prologue
    .line 14945
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedPadding()V

    #@3
    .line 14947
    iput p1, p0, Landroid/view/View;->mUserPaddingStart:I

    #@5
    .line 14948
    iput p3, p0, Landroid/view/View;->mUserPaddingEnd:I

    #@7
    .line 14950
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@a
    move-result v0

    #@b
    packed-switch v0, :pswitch_data_1e

    #@e
    .line 14958
    iput p1, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@10
    .line 14959
    iput p3, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@12
    .line 14960
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/View;->internalSetPadding(IIII)V

    #@15
    .line 14962
    :goto_15
    return-void

    #@16
    .line 14952
    :pswitch_16
    iput p3, p0, Landroid/view/View;->mUserPaddingLeftInitial:I

    #@18
    .line 14953
    iput p1, p0, Landroid/view/View;->mUserPaddingRightInitial:I

    #@1a
    .line 14954
    invoke-virtual {p0, p3, p2, p1, p4}, Landroid/view/View;->internalSetPadding(IIII)V

    #@1d
    goto :goto_15

    #@1e
    .line 14950
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_16
    .end packed-switch
.end method

.method public setPivotX(F)V
    .registers 8
    .parameter "pivotX"

    #@0
    .prologue
    const/high16 v5, 0x1000

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v3, 0x1

    #@4
    .line 9395
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9396
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@9
    const/high16 v2, 0x2000

    #@b
    or-int/2addr v1, v2

    #@c
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    .line 9397
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@10
    .line 9398
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@12
    cmpl-float v1, v1, p1

    #@14
    if-eqz v1, :cond_31

    #@16
    .line 9399
    invoke-virtual {p0, v3, v4}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9400
    iput p1, v0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@1b
    .line 9401
    iput-boolean v3, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@1d
    .line 9402
    invoke-virtual {p0, v4, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@20
    .line 9403
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 9404
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@26
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setPivotX(F)V

    #@29
    .line 9406
    :cond_29
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2b
    and-int/2addr v1, v5

    #@2c
    if-ne v1, v5, :cond_31

    #@2e
    .line 9408
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@31
    .line 9411
    :cond_31
    return-void
.end method

.method public setPivotY(F)V
    .registers 8
    .parameter "pivotY"

    #@0
    .prologue
    const/high16 v5, 0x1000

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v3, 0x1

    #@4
    .line 9445
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9446
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@9
    const/high16 v2, 0x2000

    #@b
    or-int/2addr v1, v2

    #@c
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@e
    .line 9447
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@10
    .line 9448
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@12
    cmpl-float v1, v1, p1

    #@14
    if-eqz v1, :cond_31

    #@16
    .line 9449
    invoke-virtual {p0, v3, v4}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9450
    iput p1, v0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@1b
    .line 9451
    iput-boolean v3, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@1d
    .line 9452
    invoke-virtual {p0, v4, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@20
    .line 9453
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@22
    if-eqz v1, :cond_29

    #@24
    .line 9454
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@26
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setPivotY(F)V

    #@29
    .line 9456
    :cond_29
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2b
    and-int/2addr v1, v5

    #@2c
    if-ne v1, v5, :cond_31

    #@2e
    .line 9458
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@31
    .line 9461
    :cond_31
    return-void
.end method

.method public setPressed(Z)V
    .registers 6
    .parameter "pressed"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 6239
    iget v2, p0, Landroid/view/View;->mPrivateFlags:I

    #@4
    and-int/lit16 v2, v2, 0x4000

    #@6
    const/16 v3, 0x4000

    #@8
    if-ne v2, v3, :cond_1e

    #@a
    move v2, v0

    #@b
    :goto_b
    if-eq p1, v2, :cond_20

    #@d
    .line 6241
    .local v0, needsRefresh:Z
    :goto_d
    if-eqz p1, :cond_22

    #@f
    .line 6242
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@11
    or-int/lit16 v1, v1, 0x4000

    #@13
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@15
    .line 6247
    :goto_15
    if-eqz v0, :cond_1a

    #@17
    .line 6248
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@1a
    .line 6250
    :cond_1a
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchSetPressed(Z)V

    #@1d
    .line 6251
    return-void

    #@1e
    .end local v0           #needsRefresh:Z
    :cond_1e
    move v2, v1

    #@1f
    .line 6239
    goto :goto_b

    #@20
    :cond_20
    move v0, v1

    #@21
    goto :goto_d

    #@22
    .line 6244
    .restart local v0       #needsRefresh:Z
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@24
    and-int/lit16 v1, v1, -0x4001

    #@26
    iput v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@28
    goto :goto_15
.end method

.method public final setRight(I)V
    .registers 12
    .parameter "right"

    #@0
    .prologue
    const/high16 v9, 0x1000

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v5, 0x1

    #@4
    .line 9783
    iget v6, p0, Landroid/view/View;->mRight:I

    #@6
    if-eq p1, v6, :cond_74

    #@8
    .line 9784
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@b
    .line 9785
    iget-object v6, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    if-eqz v6, :cond_17

    #@f
    iget-object v6, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@11
    invoke-static {v6}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_75

    #@17
    :cond_17
    move v1, v5

    #@18
    .line 9787
    .local v1, matrixIsIdentity:Z
    :goto_18
    if-eqz v1, :cond_79

    #@1a
    .line 9788
    iget-object v6, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1c
    if-eqz v6, :cond_30

    #@1e
    .line 9790
    iget v6, p0, Landroid/view/View;->mRight:I

    #@20
    if-ge p1, v6, :cond_77

    #@22
    .line 9791
    iget v2, p0, Landroid/view/View;->mRight:I

    #@24
    .line 9795
    .local v2, maxRight:I
    :goto_24
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@26
    sub-int v6, v2, v6

    #@28
    iget v7, p0, Landroid/view/View;->mBottom:I

    #@2a
    iget v8, p0, Landroid/view/View;->mTop:I

    #@2c
    sub-int/2addr v7, v8

    #@2d
    invoke-virtual {p0, v4, v4, v6, v7}, Landroid/view/View;->invalidate(IIII)V

    #@30
    .line 9802
    .end local v2           #maxRight:I
    :cond_30
    :goto_30
    iget v4, p0, Landroid/view/View;->mRight:I

    #@32
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@34
    sub-int v3, v4, v6

    #@36
    .line 9803
    .local v3, oldWidth:I
    iget v4, p0, Landroid/view/View;->mBottom:I

    #@38
    iget v6, p0, Landroid/view/View;->mTop:I

    #@3a
    sub-int v0, v4, v6

    #@3c
    .line 9805
    .local v0, height:I
    iput p1, p0, Landroid/view/View;->mRight:I

    #@3e
    .line 9806
    iget-object v4, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@40
    if-eqz v4, :cond_49

    #@42
    .line 9807
    iget-object v4, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@44
    iget v6, p0, Landroid/view/View;->mRight:I

    #@46
    invoke-virtual {v4, v6}, Landroid/view/DisplayList;->setRight(I)V

    #@49
    .line 9810
    :cond_49
    iget v4, p0, Landroid/view/View;->mRight:I

    #@4b
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@4d
    sub-int/2addr v4, v6

    #@4e
    invoke-virtual {p0, v4, v0, v3, v0}, Landroid/view/View;->onSizeChanged(IIII)V

    #@51
    .line 9812
    if-nez v1, :cond_67

    #@53
    .line 9813
    iget v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@55
    const/high16 v6, 0x2000

    #@57
    and-int/2addr v4, v6

    #@58
    if-nez v4, :cond_5e

    #@5a
    .line 9815
    iget-object v4, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5c
    iput-boolean v5, v4, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@5e
    .line 9817
    :cond_5e
    iget v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@60
    or-int/lit8 v4, v4, 0x20

    #@62
    iput v4, p0, Landroid/view/View;->mPrivateFlags:I

    #@64
    .line 9818
    invoke-virtual {p0, v5}, Landroid/view/View;->invalidate(Z)V

    #@67
    .line 9820
    :cond_67
    iput-boolean v5, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@69
    .line 9821
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@6c
    .line 9822
    iget v4, p0, Landroid/view/View;->mPrivateFlags2:I

    #@6e
    and-int/2addr v4, v9

    #@6f
    if-ne v4, v9, :cond_74

    #@71
    .line 9824
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@74
    .line 9827
    .end local v0           #height:I
    .end local v1           #matrixIsIdentity:Z
    .end local v3           #oldWidth:I
    :cond_74
    return-void

    #@75
    :cond_75
    move v1, v4

    #@76
    .line 9785
    goto :goto_18

    #@77
    .line 9793
    .restart local v1       #matrixIsIdentity:Z
    :cond_77
    move v2, p1

    #@78
    .restart local v2       #maxRight:I
    goto :goto_24

    #@79
    .line 9799
    .end local v2           #maxRight:I
    :cond_79
    invoke-virtual {p0, v5}, Landroid/view/View;->invalidate(Z)V

    #@7c
    goto :goto_30
.end method

.method public setRotation(F)V
    .registers 7
    .parameter "rotation"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9156
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9157
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9158
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9160
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9161
    iput p1, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@14
    .line 9162
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9163
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9164
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9165
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setRotation(F)V

    #@22
    .line 9167
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9169
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9172
    :cond_2a
    return-void
.end method

.method public setRotationX(F)V
    .registers 7
    .parameter "rotationX"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9259
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9260
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9261
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9262
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9263
    iput p1, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@14
    .line 9264
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9265
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9266
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9267
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setRotationX(F)V

    #@22
    .line 9269
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9271
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9274
    :cond_2a
    return-void
.end method

.method public setRotationY(F)V
    .registers 7
    .parameter "rotationY"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9208
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9209
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9210
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9211
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9212
    iput p1, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@14
    .line 9213
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9214
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9215
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9216
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setRotationY(F)V

    #@22
    .line 9218
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9220
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9223
    :cond_2a
    return-void
.end method

.method public setSaveEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const/high16 v1, 0x1

    #@2
    .line 6308
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6309
    return-void

    #@9
    :cond_9
    move v0, v1

    #@a
    .line 6308
    goto :goto_5
.end method

.method public setSaveFromParentEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const/high16 v1, 0x2000

    #@2
    .line 6369
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6370
    return-void

    #@9
    :cond_9
    move v0, v1

    #@a
    .line 6369
    goto :goto_5
.end method

.method public setScaleX(F)V
    .registers 7
    .parameter "scaleX"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9302
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9303
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9304
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9305
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9306
    iput p1, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@14
    .line 9307
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9308
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9309
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9310
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setScaleX(F)V

    #@22
    .line 9312
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9314
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9317
    :cond_2a
    return-void
.end method

.method public setScaleY(F)V
    .registers 7
    .parameter "scaleY"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9345
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9346
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9347
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9348
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9349
    iput p1, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@14
    .line 9350
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9351
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9352
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9353
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setScaleY(F)V

    #@22
    .line 9355
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9357
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9360
    :cond_2a
    return-void
.end method

.method public setScrollBarDefaultDelayBeforeFade(I)V
    .registers 3
    .parameter "scrollBarDefaultDelayBeforeFade"

    #@0
    .prologue
    .line 11254
    invoke-direct {p0}, Landroid/view/View;->getScrollCache()Landroid/view/View$ScrollabilityCache;

    #@3
    move-result-object v0

    #@4
    iput p1, v0, Landroid/view/View$ScrollabilityCache;->scrollBarDefaultDelayBeforeFade:I

    #@6
    .line 11255
    return-void
.end method

.method public setScrollBarFadeDuration(I)V
    .registers 3
    .parameter "scrollBarFadeDuration"

    #@0
    .prologue
    .line 11278
    invoke-direct {p0}, Landroid/view/View;->getScrollCache()Landroid/view/View$ScrollabilityCache;

    #@3
    move-result-object v0

    #@4
    iput p1, v0, Landroid/view/View$ScrollabilityCache;->scrollBarFadeDuration:I

    #@6
    .line 11279
    return-void
.end method

.method public setScrollBarSize(I)V
    .registers 3
    .parameter "scrollBarSize"

    #@0
    .prologue
    .line 11302
    invoke-direct {p0}, Landroid/view/View;->getScrollCache()Landroid/view/View$ScrollabilityCache;

    #@3
    move-result-object v0

    #@4
    iput p1, v0, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@6
    .line 11303
    return-void
.end method

.method public setScrollBarStyle(I)V
    .registers 5
    .parameter "style"

    #@0
    .prologue
    const/high16 v2, 0x300

    #@2
    .line 11325
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v2

    #@5
    if-eq p1, v0, :cond_18

    #@7
    .line 11326
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@9
    const v1, -0x3000001

    #@c
    and-int/2addr v0, v1

    #@d
    and-int v1, p1, v2

    #@f
    or-int/2addr v0, v1

    #@10
    iput v0, p0, Landroid/view/View;->mViewFlags:I

    #@12
    .line 11327
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@15
    .line 11328
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@18
    .line 11330
    :cond_18
    return-void
.end method

.method public setScrollContainer(Z)V
    .registers 4
    .parameter "isScrollContainer"

    #@0
    .prologue
    const/high16 v1, 0x10

    #@2
    .line 5476
    if-eqz p1, :cond_21

    #@4
    .line 5477
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    if-eqz v0, :cond_19

    #@8
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    and-int/2addr v0, v1

    #@b
    if-nez v0, :cond_19

    #@d
    .line 5478
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@f
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 5479
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@16
    or-int/2addr v0, v1

    #@17
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@19
    .line 5481
    :cond_19
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@1b
    const/high16 v1, 0x8

    #@1d
    or-int/2addr v0, v1

    #@1e
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@20
    .line 5488
    :goto_20
    return-void

    #@21
    .line 5483
    :cond_21
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@23
    and-int/2addr v0, v1

    #@24
    if-eqz v0, :cond_2d

    #@26
    .line 5484
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@28
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@2d
    .line 5486
    :cond_2d
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2f
    const v1, -0x180001

    #@32
    and-int/2addr v0, v1

    #@33
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@35
    goto :goto_20
.end method

.method public setScrollX(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 8794
    iget v0, p0, Landroid/view/View;->mScrollY:I

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->scrollTo(II)V

    #@5
    .line 8795
    return-void
.end method

.method public setScrollY(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 8804
    iget v0, p0, Landroid/view/View;->mScrollX:I

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/view/View;->scrollTo(II)V

    #@5
    .line 8805
    return-void
.end method

.method public setScrollbarFadingEnabled(Z)V
    .registers 4
    .parameter "fadeScrollbars"

    #@0
    .prologue
    .line 11211
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@3
    .line 11212
    iget-object v0, p0, Landroid/view/View;->mScrollCache:Landroid/view/View$ScrollabilityCache;

    #@5
    .line 11213
    .local v0, scrollabilityCache:Landroid/view/View$ScrollabilityCache;
    iput-boolean p1, v0, Landroid/view/View$ScrollabilityCache;->fadeScrollBars:Z

    #@7
    .line 11214
    if-eqz p1, :cond_d

    #@9
    .line 11215
    const/4 v1, 0x0

    #@a
    iput v1, v0, Landroid/view/View$ScrollabilityCache;->state:I

    #@c
    .line 11219
    :goto_c
    return-void

    #@d
    .line 11217
    :cond_d
    const/4 v1, 0x1

    #@e
    iput v1, v0, Landroid/view/View$ScrollabilityCache;->state:I

    #@10
    goto :goto_c
.end method

.method public setSelected(Z)V
    .registers 5
    .parameter "selected"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 15098
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@4
    and-int/lit8 v1, v1, 0x4

    #@6
    if-eqz v1, :cond_33

    #@8
    move v1, v2

    #@9
    :goto_9
    if-eq v1, p1, :cond_32

    #@b
    .line 15099
    iget v1, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    and-int/lit8 v1, v1, -0x5

    #@f
    if-eqz p1, :cond_12

    #@11
    const/4 v0, 0x4

    #@12
    :cond_12
    or-int/2addr v0, v1

    #@13
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@15
    .line 15100
    if-nez p1, :cond_1a

    #@17
    invoke-direct {p0}, Landroid/view/View;->resetPressedState()V

    #@1a
    .line 15101
    :cond_1a
    invoke-virtual {p0, v2}, Landroid/view/View;->invalidate(Z)V

    #@1d
    .line 15102
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@20
    .line 15103
    invoke-virtual {p0, p1}, Landroid/view/View;->dispatchSetSelected(Z)V

    #@23
    .line 15104
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@25
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_32

    #@2f
    .line 15105
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@32
    .line 15108
    :cond_32
    return-void

    #@33
    :cond_33
    move v1, v0

    #@34
    .line 15098
    goto :goto_9
.end method

.method public setSoundEffectsEnabled(Z)V
    .registers 4
    .parameter "soundEffectsEnabled"

    #@0
    .prologue
    const/high16 v1, 0x800

    #@2
    .line 5932
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 5933
    return-void

    #@9
    .line 5932
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setSystemUiVisibility(I)V
    .registers 6
    .parameter "visibility"

    #@0
    .prologue
    .line 16268
    const-string v1, "View"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "View.setSystemUiVisibility .. packageName : "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 16269
    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@20
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    .line 16270
    .local v0, packageName:Ljava/lang/String;
    const-string v1, "com.lge.launcher2"

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v1

    #@2a
    if-nez v1, :cond_34

    #@2c
    const-string v1, "com.lge.camera"

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_3d

    #@34
    .line 16271
    :cond_34
    const-string v1, "View"

    #@36
    const-string v2, "View.setSystemUiVisibility .. remove TRANSP FLAG"

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 16272
    and-int/lit8 p1, p1, -0x11

    #@3d
    .line 16275
    :cond_3d
    iget v1, p0, Landroid/view/View;->mSystemUiVisibility:I

    #@3f
    if-eq p1, v1, :cond_5d

    #@41
    .line 16276
    iput p1, p0, Landroid/view/View;->mSystemUiVisibility:I

    #@43
    .line 16277
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@45
    if-eqz v1, :cond_5d

    #@47
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@49
    if-eqz v1, :cond_5d

    #@4b
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@4d
    iget-boolean v1, v1, Landroid/view/View$AttachInfo;->mRecomputeGlobalAttributes:Z

    #@4f
    if-nez v1, :cond_5d

    #@51
    .line 16278
    const-string v1, "View"

    #@53
    const-string v2, "View.setSystemUiVisibility ..  : mParent.recomputeViewAttributes called"

    #@55
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 16279
    iget-object v1, p0, Landroid/view/View;->mParent:Landroid/view/ViewParent;

    #@5a
    invoke-interface {v1, p0}, Landroid/view/ViewParent;->recomputeViewAttributes(Landroid/view/View;)V

    #@5d
    .line 16282
    :cond_5d
    return-void
.end method

.method public setTag(ILjava/lang/Object;)V
    .registers 5
    .parameter "key"
    .parameter "tag"

    #@0
    .prologue
    .line 15573
    ushr-int/lit8 v0, p1, 0x18

    #@2
    const/4 v1, 0x2

    #@3
    if-ge v0, v1, :cond_d

    #@5
    .line 15574
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "The key must be an application-specific resource id."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 15578
    :cond_d
    invoke-direct {p0, p1, p2}, Landroid/view/View;->setKeyedTag(ILjava/lang/Object;)V

    #@10
    .line 15579
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .registers 8
    .parameter "tag"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 15515
    iput-object p1, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@3
    .line 15516
    iget-object v3, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@5
    if-eqz v3, :cond_3c

    #@7
    iget-object v3, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@9
    instance-of v3, v3, Ljava/lang/String;

    #@b
    if-eqz v3, :cond_3c

    #@d
    iget-object v3, p0, Landroid/view/View;->mTag:Ljava/lang/Object;

    #@f
    const-string v4, "NO_R2L"

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_3c

    #@17
    .line 15517
    sput-boolean v5, Landroid/view/View;->mRtlLangAndLocaleMetaDataExist:Z

    #@19
    .line 15519
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1b
    and-int/lit8 v3, v3, -0x3d

    #@1d
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1f
    .line 15521
    const/4 v0, 0x0

    #@20
    .line 15522
    .local v0, layoutDirection:I
    sget-object v3, Landroid/view/View;->LAYOUT_DIRECTION_FLAGS:[I

    #@22
    aget v2, v3, v5

    #@24
    .line 15524
    .local v2, value:I
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@26
    shl-int/lit8 v4, v2, 0x2

    #@28
    or-int/2addr v3, v4

    #@29
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2b
    .line 15527
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@2d
    and-int/lit16 v3, v3, -0x1c1

    #@2f
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@31
    .line 15529
    const/4 v1, 0x3

    #@32
    .line 15530
    .local v1, textDirection:I
    iget v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@34
    sget-object v4, Landroid/view/View;->PFLAG2_TEXT_DIRECTION_FLAGS:[I

    #@36
    const/4 v5, 0x3

    #@37
    aget v4, v4, v5

    #@39
    or-int/2addr v3, v4

    #@3a
    iput v3, p0, Landroid/view/View;->mPrivateFlags2:I

    #@3c
    .line 15532
    .end local v0           #layoutDirection:I
    .end local v1           #textDirection:I
    .end local v2           #value:I
    :cond_3c
    return-void
.end method

.method public setTagInternal(ILjava/lang/Object;)V
    .registers 5
    .parameter "key"
    .parameter "tag"

    #@0
    .prologue
    .line 15588
    ushr-int/lit8 v0, p1, 0x18

    #@2
    const/4 v1, 0x1

    #@3
    if-eq v0, v1, :cond_d

    #@5
    .line 15589
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "The key must be a framework-specific resource id."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 15593
    :cond_d
    invoke-direct {p0, p1, p2}, Landroid/view/View;->setKeyedTag(ILjava/lang/Object;)V

    #@10
    .line 15594
    return-void
.end method

.method public setTextAlignment(I)V
    .registers 5
    .parameter "textAlignment"

    #@0
    .prologue
    .line 17125
    invoke-virtual {p0}, Landroid/view/View;->getRawTextAlignment()I

    #@3
    move-result v0

    #@4
    if-eq p1, v0, :cond_2d

    #@6
    .line 17127
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    const v1, -0xe001

    #@b
    and-int/2addr v0, v1

    #@c
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@e
    .line 17128
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedTextAlignment()V

    #@11
    .line 17130
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@13
    shl-int/lit8 v1, p1, 0xd

    #@15
    const v2, 0xe000

    #@18
    and-int/2addr v1, v2

    #@19
    or-int/2addr v0, v1

    #@1a
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@1c
    .line 17133
    invoke-virtual {p0}, Landroid/view/View;->resolveTextAlignment()Z

    #@1f
    .line 17135
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@22
    move-result v0

    #@23
    invoke-virtual {p0, v0}, Landroid/view/View;->onRtlPropertiesChanged(I)V

    #@26
    .line 17137
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@29
    .line 17138
    const/4 v0, 0x1

    #@2a
    invoke-virtual {p0, v0}, Landroid/view/View;->invalidate(Z)V

    #@2d
    .line 17140
    :cond_2d
    return-void
.end method

.method public setTextDirection(I)V
    .registers 4
    .parameter "textDirection"

    #@0
    .prologue
    .line 16928
    invoke-virtual {p0}, Landroid/view/View;->getRawTextDirection()I

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_29

    #@6
    .line 16930
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@8
    and-int/lit16 v0, v0, -0x1c1

    #@a
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@c
    .line 16931
    invoke-virtual {p0}, Landroid/view/View;->resetResolvedTextDirection()V

    #@f
    .line 16933
    iget v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@11
    shl-int/lit8 v1, p1, 0x6

    #@13
    and-int/lit16 v1, v1, 0x1c0

    #@15
    or-int/2addr v0, v1

    #@16
    iput v0, p0, Landroid/view/View;->mPrivateFlags2:I

    #@18
    .line 16935
    invoke-virtual {p0}, Landroid/view/View;->resolveTextDirection()Z

    #@1b
    .line 16937
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    #@1e
    move-result v0

    #@1f
    invoke-virtual {p0, v0}, Landroid/view/View;->onRtlPropertiesChanged(I)V

    #@22
    .line 16939
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    #@25
    .line 16940
    const/4 v0, 0x1

    #@26
    invoke-virtual {p0, v0}, Landroid/view/View;->invalidate(Z)V

    #@29
    .line 16942
    :cond_29
    return-void
.end method

.method public final setTop(I)V
    .registers 12
    .parameter "top"

    #@0
    .prologue
    const/high16 v9, 0x1000

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x1

    #@4
    .line 9576
    iget v7, p0, Landroid/view/View;->mTop:I

    #@6
    if-eq p1, v7, :cond_76

    #@8
    .line 9577
    invoke-direct {p0}, Landroid/view/View;->updateMatrix()V

    #@b
    .line 9578
    iget-object v7, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@d
    if-eqz v7, :cond_17

    #@f
    iget-object v7, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@11
    invoke-static {v7}, Landroid/view/View$TransformationInfo;->access$1500(Landroid/view/View$TransformationInfo;)Z

    #@14
    move-result v7

    #@15
    if-eqz v7, :cond_77

    #@17
    :cond_17
    move v0, v6

    #@18
    .line 9580
    .local v0, matrixIsIdentity:Z
    :goto_18
    if-eqz v0, :cond_7d

    #@1a
    .line 9581
    iget-object v7, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1c
    if-eqz v7, :cond_32

    #@1e
    .line 9584
    iget v7, p0, Landroid/view/View;->mTop:I

    #@20
    if-ge p1, v7, :cond_79

    #@22
    .line 9585
    move v1, p1

    #@23
    .line 9586
    .local v1, minTop:I
    iget v7, p0, Landroid/view/View;->mTop:I

    #@25
    sub-int v4, p1, v7

    #@27
    .line 9591
    .local v4, yLoc:I
    :goto_27
    iget v7, p0, Landroid/view/View;->mRight:I

    #@29
    iget v8, p0, Landroid/view/View;->mLeft:I

    #@2b
    sub-int/2addr v7, v8

    #@2c
    iget v8, p0, Landroid/view/View;->mBottom:I

    #@2e
    sub-int/2addr v8, v1

    #@2f
    invoke-virtual {p0, v5, v4, v7, v8}, Landroid/view/View;->invalidate(IIII)V

    #@32
    .line 9598
    .end local v1           #minTop:I
    .end local v4           #yLoc:I
    :cond_32
    :goto_32
    iget v5, p0, Landroid/view/View;->mRight:I

    #@34
    iget v7, p0, Landroid/view/View;->mLeft:I

    #@36
    sub-int v3, v5, v7

    #@38
    .line 9599
    .local v3, width:I
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@3a
    iget v7, p0, Landroid/view/View;->mTop:I

    #@3c
    sub-int v2, v5, v7

    #@3e
    .line 9601
    .local v2, oldHeight:I
    iput p1, p0, Landroid/view/View;->mTop:I

    #@40
    .line 9602
    iget-object v5, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@42
    if-eqz v5, :cond_4b

    #@44
    .line 9603
    iget-object v5, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@46
    iget v7, p0, Landroid/view/View;->mTop:I

    #@48
    invoke-virtual {v5, v7}, Landroid/view/DisplayList;->setTop(I)V

    #@4b
    .line 9606
    :cond_4b
    iget v5, p0, Landroid/view/View;->mBottom:I

    #@4d
    iget v7, p0, Landroid/view/View;->mTop:I

    #@4f
    sub-int/2addr v5, v7

    #@50
    invoke-virtual {p0, v3, v5, v3, v2}, Landroid/view/View;->onSizeChanged(IIII)V

    #@53
    .line 9608
    if-nez v0, :cond_69

    #@55
    .line 9609
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@57
    const/high16 v7, 0x2000

    #@59
    and-int/2addr v5, v7

    #@5a
    if-nez v5, :cond_60

    #@5c
    .line 9611
    iget-object v5, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@5e
    iput-boolean v6, v5, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@60
    .line 9613
    :cond_60
    iget v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@62
    or-int/lit8 v5, v5, 0x20

    #@64
    iput v5, p0, Landroid/view/View;->mPrivateFlags:I

    #@66
    .line 9614
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@69
    .line 9616
    :cond_69
    iput-boolean v6, p0, Landroid/view/View;->mBackgroundSizeChanged:Z

    #@6b
    .line 9617
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@6e
    .line 9618
    iget v5, p0, Landroid/view/View;->mPrivateFlags2:I

    #@70
    and-int/2addr v5, v9

    #@71
    if-ne v5, v9, :cond_76

    #@73
    .line 9620
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@76
    .line 9623
    .end local v0           #matrixIsIdentity:Z
    .end local v2           #oldHeight:I
    .end local v3           #width:I
    :cond_76
    return-void

    #@77
    :cond_77
    move v0, v5

    #@78
    .line 9578
    goto :goto_18

    #@79
    .line 9588
    .restart local v0       #matrixIsIdentity:Z
    :cond_79
    iget v1, p0, Landroid/view/View;->mTop:I

    #@7b
    .line 9589
    .restart local v1       #minTop:I
    const/4 v4, 0x0

    #@7c
    .restart local v4       #yLoc:I
    goto :goto_27

    #@7d
    .line 9595
    .end local v1           #minTop:I
    .end local v4           #yLoc:I
    :cond_7d
    invoke-virtual {p0, v6}, Landroid/view/View;->invalidate(Z)V

    #@80
    goto :goto_32
.end method

.method public setTouchDelegate(Landroid/view/TouchDelegate;)V
    .registers 2
    .parameter "delegate"

    #@0
    .prologue
    .line 8533
    iput-object p1, p0, Landroid/view/View;->mTouchDelegate:Landroid/view/TouchDelegate;

    #@2
    .line 8534
    return-void
.end method

.method public setTranslationX(F)V
    .registers 7
    .parameter "translationX"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9899
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9900
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9901
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9903
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9904
    iput p1, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@14
    .line 9905
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9906
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9907
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9908
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setTranslationX(F)V

    #@22
    .line 9910
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9912
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9915
    :cond_2a
    return-void
.end method

.method public setTranslationY(F)V
    .registers 7
    .parameter "translationY"

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 9941
    invoke-virtual {p0}, Landroid/view/View;->ensureTransformationInfo()V

    #@7
    .line 9942
    iget-object v0, p0, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@9
    .line 9943
    .local v0, info:Landroid/view/View$TransformationInfo;
    iget v1, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@b
    cmpl-float v1, v1, p1

    #@d
    if-eqz v1, :cond_2a

    #@f
    .line 9944
    invoke-virtual {p0, v2, v3}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@12
    .line 9945
    iput p1, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@14
    .line 9946
    iput-boolean v2, v0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@16
    .line 9947
    invoke-virtual {p0, v3, v2}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@19
    .line 9948
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 9949
    iget-object v1, p0, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@1f
    invoke-virtual {v1, p1}, Landroid/view/DisplayList;->setTranslationY(F)V

    #@22
    .line 9951
    :cond_22
    iget v1, p0, Landroid/view/View;->mPrivateFlags2:I

    #@24
    and-int/2addr v1, v4

    #@25
    if-ne v1, v4, :cond_2a

    #@27
    .line 9953
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentIfNeeded()V

    #@2a
    .line 9956
    :cond_2a
    return-void
.end method

.method public setVerticalFadingEdgeEnabled(Z)V
    .registers 3
    .parameter "verticalFadingEdgeEnabled"

    #@0
    .prologue
    .line 11069
    invoke-virtual {p0}, Landroid/view/View;->isVerticalFadingEdgeEnabled()Z

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_11

    #@6
    .line 11070
    if-eqz p1, :cond_b

    #@8
    .line 11071
    invoke-direct {p0}, Landroid/view/View;->initScrollCache()V

    #@b
    .line 11074
    :cond_b
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@d
    xor-int/lit16 v0, v0, 0x2000

    #@f
    iput v0, p0, Landroid/view/View;->mViewFlags:I

    #@11
    .line 11076
    :cond_11
    return-void
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .registers 3
    .parameter "verticalScrollBarEnabled"

    #@0
    .prologue
    .line 11189
    invoke-virtual {p0}, Landroid/view/View;->isVerticalScrollBarEnabled()Z

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_12

    #@6
    .line 11190
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@8
    xor-int/lit16 v0, v0, 0x200

    #@a
    iput v0, p0, Landroid/view/View;->mViewFlags:I

    #@c
    .line 11191
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@f
    .line 11192
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@12
    .line 11194
    :cond_12
    return-void
.end method

.method public setVerticalScrollbarPosition(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 4149
    iget v0, p0, Landroid/view/View;->mVerticalScrollbarPosition:I

    #@2
    if-eq v0, p1, :cond_c

    #@4
    .line 4150
    iput p1, p0, Landroid/view/View;->mVerticalScrollbarPosition:I

    #@6
    .line 4151
    invoke-virtual {p0}, Landroid/view/View;->computeOpaqueFlags()V

    #@9
    .line 4152
    invoke-virtual {p0}, Landroid/view/View;->resolvePadding()V

    #@c
    .line 4154
    :cond_c
    return-void
.end method

.method public setVisibility(I)V
    .registers 5
    .parameter "visibility"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5841
    const/16 v0, 0xc

    #@3
    invoke-virtual {p0, p1, v0}, Landroid/view/View;->setFlags(II)V

    #@6
    .line 5842
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_12

    #@a
    iget-object v2, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@c
    if-nez p1, :cond_13

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@12
    .line 5843
    :cond_12
    return-void

    #@13
    :cond_13
    move v0, v1

    #@14
    .line 5842
    goto :goto_f
.end method

.method public setWillNotCacheDrawing(Z)V
    .registers 4
    .parameter "willNotCacheDrawing"

    #@0
    .prologue
    const/high16 v1, 0x2

    #@2
    .line 6162
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6163
    return-void

    #@9
    .line 6162
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setWillNotDraw(Z)V
    .registers 4
    .parameter "willNotDraw"

    #@0
    .prologue
    const/16 v1, 0x80

    #@2
    .line 6139
    if-eqz p1, :cond_9

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setFlags(II)V

    #@8
    .line 6140
    return-void

    #@9
    .line 6139
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public setX(F)V
    .registers 3
    .parameter "x"

    #@0
    .prologue
    .line 9849
    iget v0, p0, Landroid/view/View;->mLeft:I

    #@2
    int-to-float v0, v0

    #@3
    sub-float v0, p1, v0

    #@5
    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    #@8
    .line 9850
    return-void
.end method

.method public setY(F)V
    .registers 3
    .parameter "y"

    #@0
    .prologue
    .line 9872
    iget v0, p0, Landroid/view/View;->mTop:I

    #@2
    int-to-float v0, v0

    #@3
    sub-float v0, p1, v0

    #@5
    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    #@8
    .line 9873
    return-void
.end method

.method public showContextMenu()Z
    .registers 2

    #@0
    .prologue
    .line 4399
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public showContextMenu(FFI)Z
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "metaState"

    #@0
    .prologue
    .line 4413
    invoke-virtual {p0}, Landroid/view/View;->showContextMenu()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 4425
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@3
    move-result-object v0

    #@4
    .line 4426
    .local v0, parent:Landroid/view/ViewParent;
    if-nez v0, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    .line 4427
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-interface {v0, p0, p1}, Landroid/view/ViewParent;->startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public startAnimation(Landroid/view/animation/Animation;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 16042
    const-wide/16 v0, -0x1

    #@2
    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@5
    .line 16043
    invoke-virtual {p0, p1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    #@8
    .line 16044
    invoke-virtual {p0}, Landroid/view/View;->invalidateParentCaches()V

    #@b
    .line 16045
    const/4 v0, 0x1

    #@c
    invoke-virtual {p0, v0}, Landroid/view/View;->invalidate(Z)V

    #@f
    .line 16046
    return-void
.end method

.method public final startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z
    .registers 28
    .parameter "data"
    .parameter "shadowBuilder"
    .parameter "myLocalState"
    .parameter "flags"

    #@0
    .prologue
    .line 16527
    const/16 v19, 0x0

    #@2
    .line 16529
    .local v19, okay:Z
    new-instance v21, Landroid/graphics/Point;

    #@4
    invoke-direct/range {v21 .. v21}, Landroid/graphics/Point;-><init>()V

    #@7
    .line 16530
    .local v21, shadowSize:Landroid/graphics/Point;
    new-instance v22, Landroid/graphics/Point;

    #@9
    invoke-direct/range {v22 .. v22}, Landroid/graphics/Point;-><init>()V

    #@c
    .line 16531
    .local v22, shadowTouchPoint:Landroid/graphics/Point;
    move-object/from16 v0, p2

    #@e
    move-object/from16 v1, v21

    #@10
    move-object/from16 v2, v22

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/view/View$DragShadowBuilder;->onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V

    #@15
    .line 16533
    move-object/from16 v0, v21

    #@17
    iget v3, v0, Landroid/graphics/Point;->x:I

    #@19
    if-ltz v3, :cond_2d

    #@1b
    move-object/from16 v0, v21

    #@1d
    iget v3, v0, Landroid/graphics/Point;->y:I

    #@1f
    if-ltz v3, :cond_2d

    #@21
    move-object/from16 v0, v22

    #@23
    iget v3, v0, Landroid/graphics/Point;->x:I

    #@25
    if-ltz v3, :cond_2d

    #@27
    move-object/from16 v0, v22

    #@29
    iget v3, v0, Landroid/graphics/Point;->y:I

    #@2b
    if-gez v3, :cond_35

    #@2d
    .line 16535
    :cond_2d
    new-instance v3, Ljava/lang/IllegalStateException;

    #@2f
    const-string v4, "Drag shadow dimensions must not be negative"

    #@31
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@34
    throw v3

    #@35
    .line 16542
    :cond_35
    new-instance v8, Landroid/view/Surface;

    #@37
    invoke-direct {v8}, Landroid/view/Surface;-><init>()V

    #@3a
    .line 16544
    .local v8, surface:Landroid/view/Surface;
    :try_start_3a
    move-object/from16 v0, p0

    #@3c
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3e
    iget-object v3, v3, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@40
    move-object/from16 v0, p0

    #@42
    iget-object v4, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@44
    iget-object v4, v4, Landroid/view/View$AttachInfo;->mWindow:Landroid/view/IWindow;

    #@46
    move-object/from16 v0, v21

    #@48
    iget v6, v0, Landroid/graphics/Point;->x:I

    #@4a
    move-object/from16 v0, v21

    #@4c
    iget v7, v0, Landroid/graphics/Point;->y:I

    #@4e
    move/from16 v5, p4

    #@50
    invoke-interface/range {v3 .. v8}, Landroid/view/IWindowSession;->prepareDrag(Landroid/view/IWindow;IIILandroid/view/Surface;)Landroid/os/IBinder;

    #@53
    move-result-object v11

    #@54
    .line 16548
    .local v11, token:Landroid/os/IBinder;
    if-eqz v11, :cond_a6

    #@56
    .line 16549
    const/4 v3, 0x0

    #@57
    invoke-virtual {v8, v3}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_5a
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_5a} :catch_ae

    #@5a
    move-result-object v17

    #@5b
    .line 16551
    .local v17, canvas:Landroid/graphics/Canvas;
    const/4 v3, 0x0

    #@5c
    :try_start_5c
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@5e
    move-object/from16 v0, v17

    #@60
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@63
    .line 16552
    move-object/from16 v0, p2

    #@65
    move-object/from16 v1, v17

    #@67
    invoke-virtual {v0, v1}, Landroid/view/View$DragShadowBuilder;->onDrawShadow(Landroid/graphics/Canvas;)V
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_a7

    #@6a
    .line 16554
    :try_start_6a
    move-object/from16 v0, v17

    #@6c
    invoke-virtual {v8, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@6f
    .line 16557
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@72
    move-result-object v20

    #@73
    .line 16560
    .local v20, root:Landroid/view/ViewRootImpl;
    move-object/from16 v0, v20

    #@75
    move-object/from16 v1, p3

    #@77
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->setLocalDragState(Ljava/lang/Object;)V

    #@7a
    .line 16563
    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewRootImpl;->getLastTouchPoint(Landroid/graphics/Point;)V

    #@7d
    .line 16565
    move-object/from16 v0, p0

    #@7f
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@81
    iget-object v9, v3, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@83
    move-object/from16 v0, p0

    #@85
    iget-object v3, v0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@87
    iget-object v10, v3, Landroid/view/View$AttachInfo;->mWindow:Landroid/view/IWindow;

    #@89
    move-object/from16 v0, v21

    #@8b
    iget v3, v0, Landroid/graphics/Point;->x:I

    #@8d
    int-to-float v12, v3

    #@8e
    move-object/from16 v0, v21

    #@90
    iget v3, v0, Landroid/graphics/Point;->y:I

    #@92
    int-to-float v13, v3

    #@93
    move-object/from16 v0, v22

    #@95
    iget v3, v0, Landroid/graphics/Point;->x:I

    #@97
    int-to-float v14, v3

    #@98
    move-object/from16 v0, v22

    #@9a
    iget v3, v0, Landroid/graphics/Point;->y:I

    #@9c
    int-to-float v15, v3

    #@9d
    move-object/from16 v16, p1

    #@9f
    invoke-interface/range {v9 .. v16}, Landroid/view/IWindowSession;->performDrag(Landroid/view/IWindow;Landroid/os/IBinder;FFFFLandroid/content/ClipData;)Z

    #@a2
    move-result v19

    #@a3
    .line 16572
    invoke-virtual {v8}, Landroid/view/Surface;->release()V

    #@a6
    .line 16579
    .end local v11           #token:Landroid/os/IBinder;
    .end local v17           #canvas:Landroid/graphics/Canvas;
    .end local v20           #root:Landroid/view/ViewRootImpl;
    :cond_a6
    :goto_a6
    return v19

    #@a7
    .line 16554
    .restart local v11       #token:Landroid/os/IBinder;
    .restart local v17       #canvas:Landroid/graphics/Canvas;
    :catchall_a7
    move-exception v3

    #@a8
    move-object/from16 v0, v17

    #@aa
    invoke-virtual {v8, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@ad
    throw v3
    :try_end_ae
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_ae} :catch_ae

    #@ae
    .line 16574
    .end local v11           #token:Landroid/os/IBinder;
    .end local v17           #canvas:Landroid/graphics/Canvas;
    :catch_ae
    move-exception v18

    #@af
    .line 16575
    .local v18, e:Ljava/lang/Exception;
    const-string v3, "View"

    #@b1
    const-string v4, "Unable to initiate drag"

    #@b3
    move-object/from16 v0, v18

    #@b5
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b8
    .line 16576
    invoke-virtual {v8}, Landroid/view/Surface;->destroy()V

    #@bb
    goto :goto_a6
.end method

.method public toString()Ljava/lang/String;
    .registers 14

    #@0
    .prologue
    const/16 v7, 0x46

    #@2
    const/16 v9, 0x44

    #@4
    const/16 v12, 0x2c

    #@6
    const/16 v11, 0x20

    #@8
    const/16 v8, 0x2e

    #@a
    .line 3856
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    const/16 v6, 0x80

    #@e
    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    #@11
    .line 3857
    .local v2, out:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 3858
    const/16 v6, 0x7b

    #@1e
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@21
    .line 3859
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@24
    move-result v6

    #@25
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    .line 3860
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2f
    .line 3861
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@31
    and-int/lit8 v6, v6, 0xc

    #@33
    sparse-switch v6, :sswitch_data_1aa

    #@36
    .line 3865
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@39
    .line 3867
    :goto_39
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@3b
    and-int/lit8 v6, v6, 0x1

    #@3d
    const/4 v10, 0x1

    #@3e
    if-ne v6, v10, :cond_169

    #@40
    move v6, v7

    #@41
    :goto_41
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@44
    .line 3868
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@46
    and-int/lit8 v6, v6, 0x20

    #@48
    if-nez v6, :cond_16c

    #@4a
    const/16 v6, 0x45

    #@4c
    :goto_4c
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4f
    .line 3869
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@51
    and-int/lit16 v6, v6, 0x80

    #@53
    const/16 v10, 0x80

    #@55
    if-ne v6, v10, :cond_16f

    #@57
    move v6, v8

    #@58
    :goto_58
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5b
    .line 3870
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@5d
    and-int/lit16 v6, v6, 0x100

    #@5f
    if-eqz v6, :cond_172

    #@61
    const/16 v6, 0x48

    #@63
    :goto_63
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@66
    .line 3871
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@68
    and-int/lit16 v6, v6, 0x200

    #@6a
    if-eqz v6, :cond_175

    #@6c
    const/16 v6, 0x56

    #@6e
    :goto_6e
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@71
    .line 3872
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@73
    and-int/lit16 v6, v6, 0x4000

    #@75
    if-eqz v6, :cond_178

    #@77
    const/16 v6, 0x43

    #@79
    :goto_79
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7c
    .line 3873
    iget v6, p0, Landroid/view/View;->mViewFlags:I

    #@7e
    const/high16 v10, 0x20

    #@80
    and-int/2addr v6, v10

    #@81
    if-eqz v6, :cond_17b

    #@83
    const/16 v6, 0x4c

    #@85
    :goto_85
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@88
    .line 3874
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@8b
    .line 3875
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@8d
    and-int/lit8 v6, v6, 0x8

    #@8f
    if-eqz v6, :cond_17e

    #@91
    const/16 v6, 0x52

    #@93
    :goto_93
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@96
    .line 3876
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@98
    and-int/lit8 v6, v6, 0x2

    #@9a
    if-eqz v6, :cond_181

    #@9c
    :goto_9c
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@9f
    .line 3877
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@a1
    and-int/lit8 v6, v6, 0x4

    #@a3
    if-eqz v6, :cond_184

    #@a5
    const/16 v6, 0x53

    #@a7
    :goto_a7
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@aa
    .line 3878
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@ac
    const/high16 v7, 0x200

    #@ae
    and-int/2addr v6, v7

    #@af
    if-eqz v6, :cond_187

    #@b1
    .line 3879
    const/16 v6, 0x70

    #@b3
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b6
    .line 3883
    :goto_b6
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@b8
    const/high16 v7, 0x1000

    #@ba
    and-int/2addr v6, v7

    #@bb
    if-eqz v6, :cond_196

    #@bd
    const/16 v6, 0x48

    #@bf
    :goto_bf
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c2
    .line 3884
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@c4
    const/high16 v7, 0x4000

    #@c6
    and-int/2addr v6, v7

    #@c7
    if-eqz v6, :cond_199

    #@c9
    const/16 v6, 0x41

    #@cb
    :goto_cb
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ce
    .line 3885
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@d0
    const/high16 v7, -0x8000

    #@d2
    and-int/2addr v6, v7

    #@d3
    if-eqz v6, :cond_19c

    #@d5
    const/16 v6, 0x49

    #@d7
    :goto_d7
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@da
    .line 3886
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@dc
    const/high16 v7, 0x60

    #@de
    and-int/2addr v6, v7

    #@df
    if-eqz v6, :cond_19f

    #@e1
    :goto_e1
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@e4
    .line 3887
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@e7
    .line 3888
    iget v6, p0, Landroid/view/View;->mLeft:I

    #@e9
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    .line 3889
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ef
    .line 3890
    iget v6, p0, Landroid/view/View;->mTop:I

    #@f1
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    .line 3891
    const/16 v6, 0x2d

    #@f6
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@f9
    .line 3892
    iget v6, p0, Landroid/view/View;->mRight:I

    #@fb
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fe
    .line 3893
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@101
    .line 3894
    iget v6, p0, Landroid/view/View;->mBottom:I

    #@103
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    .line 3895
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    #@109
    move-result v1

    #@10a
    .line 3896
    .local v1, id:I
    const/4 v6, -0x1

    #@10b
    if-eq v1, v6, :cond_149

    #@10d
    .line 3897
    const-string v6, " #"

    #@10f
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    .line 3898
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@115
    move-result-object v6

    #@116
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    .line 3899
    iget-object v4, p0, Landroid/view/View;->mResources:Landroid/content/res/Resources;

    #@11b
    .line 3900
    .local v4, r:Landroid/content/res/Resources;
    if-eqz v1, :cond_149

    #@11d
    if-eqz v4, :cond_149

    #@11f
    .line 3903
    const/high16 v6, -0x100

    #@121
    and-int/2addr v6, v1

    #@122
    sparse-switch v6, :sswitch_data_1b8

    #@125
    .line 3911
    :try_start_125
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    #@128
    move-result-object v3

    #@129
    .line 3914
    .local v3, pkgname:Ljava/lang/String;
    :goto_129
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    #@12c
    move-result-object v5

    #@12d
    .line 3915
    .local v5, typename:Ljava/lang/String;
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    #@130
    move-result-object v0

    #@131
    .line 3916
    .local v0, entryname:Ljava/lang/String;
    const-string v6, " "

    #@133
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    .line 3917
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    .line 3918
    const-string v6, ":"

    #@13b
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    .line 3919
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    .line 3920
    const-string v6, "/"

    #@143
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    .line 3921
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_149
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_125 .. :try_end_149} :catch_1a8

    #@149
    .line 3926
    .end local v0           #entryname:Ljava/lang/String;
    .end local v3           #pkgname:Ljava/lang/String;
    .end local v4           #r:Landroid/content/res/Resources;
    .end local v5           #typename:Ljava/lang/String;
    :cond_149
    :goto_149
    const-string/jumbo v6, "}"

    #@14c
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    .line 3927
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@152
    move-result-object v6

    #@153
    return-object v6

    #@154
    .line 3862
    .end local v1           #id:I
    :sswitch_154
    const/16 v6, 0x56

    #@156
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@159
    goto/16 :goto_39

    #@15b
    .line 3863
    :sswitch_15b
    const/16 v6, 0x49

    #@15d
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@160
    goto/16 :goto_39

    #@162
    .line 3864
    :sswitch_162
    const/16 v6, 0x47

    #@164
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@167
    goto/16 :goto_39

    #@169
    :cond_169
    move v6, v8

    #@16a
    .line 3867
    goto/16 :goto_41

    #@16c
    :cond_16c
    move v6, v8

    #@16d
    .line 3868
    goto/16 :goto_4c

    #@16f
    :cond_16f
    move v6, v9

    #@170
    .line 3869
    goto/16 :goto_58

    #@172
    :cond_172
    move v6, v8

    #@173
    .line 3870
    goto/16 :goto_63

    #@175
    :cond_175
    move v6, v8

    #@176
    .line 3871
    goto/16 :goto_6e

    #@178
    :cond_178
    move v6, v8

    #@179
    .line 3872
    goto/16 :goto_79

    #@17b
    :cond_17b
    move v6, v8

    #@17c
    .line 3873
    goto/16 :goto_85

    #@17e
    :cond_17e
    move v6, v8

    #@17f
    .line 3875
    goto/16 :goto_93

    #@181
    :cond_181
    move v7, v8

    #@182
    .line 3876
    goto/16 :goto_9c

    #@184
    :cond_184
    move v6, v8

    #@185
    .line 3877
    goto/16 :goto_a7

    #@187
    .line 3881
    :cond_187
    iget v6, p0, Landroid/view/View;->mPrivateFlags:I

    #@189
    and-int/lit16 v6, v6, 0x4000

    #@18b
    if-eqz v6, :cond_194

    #@18d
    const/16 v6, 0x50

    #@18f
    :goto_18f
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@192
    goto/16 :goto_b6

    #@194
    :cond_194
    move v6, v8

    #@195
    goto :goto_18f

    #@196
    :cond_196
    move v6, v8

    #@197
    .line 3883
    goto/16 :goto_bf

    #@199
    :cond_199
    move v6, v8

    #@19a
    .line 3884
    goto/16 :goto_cb

    #@19c
    :cond_19c
    move v6, v8

    #@19d
    .line 3885
    goto/16 :goto_d7

    #@19f
    :cond_19f
    move v9, v8

    #@1a0
    .line 3886
    goto/16 :goto_e1

    #@1a2
    .line 3905
    .restart local v1       #id:I
    .restart local v4       #r:Landroid/content/res/Resources;
    :sswitch_1a2
    :try_start_1a2
    const-string v3, "app"

    #@1a4
    .line 3906
    .restart local v3       #pkgname:Ljava/lang/String;
    goto :goto_129

    #@1a5
    .line 3908
    .end local v3           #pkgname:Ljava/lang/String;
    :sswitch_1a5
    const-string v3, "android"
    :try_end_1a7
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1a2 .. :try_end_1a7} :catch_1a8

    #@1a7
    .line 3909
    .restart local v3       #pkgname:Ljava/lang/String;
    goto :goto_129

    #@1a8
    .line 3922
    .end local v3           #pkgname:Ljava/lang/String;
    :catch_1a8
    move-exception v6

    #@1a9
    goto :goto_149

    #@1aa
    .line 3861
    :sswitch_data_1aa
    .sparse-switch
        0x0 -> :sswitch_154
        0x4 -> :sswitch_15b
        0x8 -> :sswitch_162
    .end sparse-switch

    #@1b8
    .line 3903
    :sswitch_data_1b8
    .sparse-switch
        0x1000000 -> :sswitch_1a5
        0x7f000000 -> :sswitch_1a2
    .end sparse-switch
.end method

.method transformRect(Landroid/graphics/Rect;)V
    .registers 8
    .parameter "rect"

    #@0
    .prologue
    const/high16 v5, 0x3f00

    #@2
    .line 10601
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/graphics/Matrix;->isIdentity()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_2d

    #@c
    .line 10602
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@e
    iget-object v0, v1, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@10
    .line 10603
    .local v0, boundingRect:Landroid/graphics/RectF;
    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@13
    .line 10604
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@1a
    .line 10605
    iget v1, v0, Landroid/graphics/RectF;->left:F

    #@1c
    sub-float/2addr v1, v5

    #@1d
    float-to-int v1, v1

    #@1e
    iget v2, v0, Landroid/graphics/RectF;->top:F

    #@20
    sub-float/2addr v2, v5

    #@21
    float-to-int v2, v2

    #@22
    iget v3, v0, Landroid/graphics/RectF;->right:F

    #@24
    add-float/2addr v3, v5

    #@25
    float-to-int v3, v3

    #@26
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    #@28
    add-float/2addr v4, v5

    #@29
    float-to-int v4, v4

    #@2a
    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@2d
    .line 10610
    .end local v0           #boundingRect:Landroid/graphics/RectF;
    :cond_2d
    return-void
.end method

.method unFocus()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 4632
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@3
    and-int/lit8 v0, v0, 0x2

    #@5
    if-eqz v0, :cond_23

    #@7
    .line 4633
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@9
    and-int/lit8 v0, v0, -0x3

    #@b
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@d
    .line 4635
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v1, v1, v0}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    #@11
    .line 4636
    invoke-virtual {p0}, Landroid/view/View;->refreshDrawableState()V

    #@14
    .line 4638
    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@16
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_23

    #@20
    .line 4639
    invoke-virtual {p0}, Landroid/view/View;->notifyAccessibilityStateChanged()V

    #@23
    .line 4642
    :cond_23
    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "who"

    #@0
    .prologue
    .line 14431
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_11

    #@4
    if-eqz p1, :cond_11

    #@6
    .line 14432
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@a
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@c
    const/4 v1, 0x1

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0, v1, v2, p1}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@11
    .line 14435
    :cond_11
    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .registers 5
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 14411
    invoke-virtual {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_16

    #@6
    if-eqz p2, :cond_16

    #@8
    .line 14412
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 14413
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@e
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@10
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {v0, v1, p2, p1}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@16
    .line 14419
    :cond_16
    :goto_16
    return-void

    #@17
    .line 14416
    :cond_17
    invoke-static {}, Landroid/view/ViewRootImpl;->getRunQueue()Landroid/view/ViewRootImpl$RunQueue;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0, p2}, Landroid/view/ViewRootImpl$RunQueue;->removeCallbacks(Ljava/lang/Runnable;)V

    #@1e
    goto :goto_16
.end method

.method updateLocalSystemUiVisibility(II)Z
    .registers 6
    .parameter "localValue"
    .parameter "localChanges"

    #@0
    .prologue
    .line 16348
    iget v1, p0, Landroid/view/View;->mSystemUiVisibility:I

    #@2
    xor-int/lit8 v2, p2, -0x1

    #@4
    and-int/2addr v1, v2

    #@5
    and-int v2, p1, p2

    #@7
    or-int v0, v1, v2

    #@9
    .line 16349
    .local v0, val:I
    iget v1, p0, Landroid/view/View;->mSystemUiVisibility:I

    #@b
    if-eq v0, v1, :cond_12

    #@d
    .line 16350
    invoke-virtual {p0, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    #@10
    .line 16351
    const/4 v1, 0x1

    #@11
    .line 16353
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 14501
    iget-object v0, p0, Landroid/view/View;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public willNotCacheDrawing()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    const/high16 v1, 0x2

    #@2
    .line 6172
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@4
    and-int/2addr v0, v1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public willNotDraw()Z
    .registers 3
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    #@0
    .prologue
    .line 6149
    iget v0, p0, Landroid/view/View;->mViewFlags:I

    #@2
    and-int/lit16 v0, v0, 0x80

    #@4
    const/16 v1, 0x80

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
