.class public final Landroid/view/textservice/TextInfo;
.super Ljava/lang/Object;
.source "TextInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/textservice/TextInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCookie:I

.field private final mSequence:I

.field private final mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 97
    new-instance v0, Landroid/view/textservice/TextInfo$1;

    #@2
    invoke-direct {v0}, Landroid/view/textservice/TextInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/textservice/TextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/view/textservice/TextInfo;->mText:Ljava/lang/String;

    #@9
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/view/textservice/TextInfo;->mCookie:I

    #@f
    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/view/textservice/TextInfo;->mSequence:I

    #@15
    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 36
    invoke-direct {p0, p1, v0, v0}, Landroid/view/textservice/TextInfo;-><init>(Ljava/lang/String;II)V

    #@4
    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .registers 5
    .parameter "text"
    .parameter "cookie"
    .parameter "sequence"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 49
    :cond_f
    iput-object p1, p0, Landroid/view/textservice/TextInfo;->mText:Ljava/lang/String;

    #@11
    .line 50
    iput p2, p0, Landroid/view/textservice/TextInfo;->mCookie:I

    #@13
    .line 51
    iput p3, p0, Landroid/view/textservice/TextInfo;->mSequence:I

    #@15
    .line 52
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 115
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCookie()I
    .registers 2

    #@0
    .prologue
    .line 84
    iget v0, p0, Landroid/view/textservice/TextInfo;->mCookie:I

    #@2
    return v0
.end method

.method public getSequence()I
    .registers 2

    #@0
    .prologue
    .line 91
    iget v0, p0, Landroid/view/textservice/TextInfo;->mSequence:I

    #@2
    return v0
.end method

.method public getText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/view/textservice/TextInfo;->mText:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/view/textservice/TextInfo;->mText:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 69
    iget v0, p0, Landroid/view/textservice/TextInfo;->mCookie:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 70
    iget v0, p0, Landroid/view/textservice/TextInfo;->mSequence:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 71
    return-void
.end method
