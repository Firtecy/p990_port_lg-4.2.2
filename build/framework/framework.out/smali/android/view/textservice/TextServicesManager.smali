.class public final Landroid/view/textservice/TextServicesManager;
.super Ljava/lang/Object;
.source "TextServicesManager.java"


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Landroid/view/textservice/TextServicesManager;

.field private static sService:Lcom/android/internal/textservice/ITextServicesManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 65
    const-class v0, Landroid/view/textservice/TextServicesManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@5
    if-nez v1, :cond_14

    #@7
    .line 73
    const-string/jumbo v1, "textservices"

    #@a
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    .line 74
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/textservice/ITextServicesManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/textservice/ITextServicesManager;

    #@11
    move-result-object v1

    #@12
    sput-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@14
    .line 76
    .end local v0           #b:Landroid/os/IBinder;
    :cond_14
    return-void
.end method

.method public static getInstance()Landroid/view/textservice/TextServicesManager;
    .registers 2

    #@0
    .prologue
    .line 83
    const-class v1, Landroid/view/textservice/TextServicesManager;

    #@2
    monitor-enter v1

    #@3
    .line 84
    :try_start_3
    sget-object v0, Landroid/view/textservice/TextServicesManager;->sInstance:Landroid/view/textservice/TextServicesManager;

    #@5
    if-eqz v0, :cond_b

    #@7
    .line 85
    sget-object v0, Landroid/view/textservice/TextServicesManager;->sInstance:Landroid/view/textservice/TextServicesManager;

    #@9
    monitor-exit v1

    #@a
    .line 89
    :goto_a
    return-object v0

    #@b
    .line 87
    :cond_b
    new-instance v0, Landroid/view/textservice/TextServicesManager;

    #@d
    invoke-direct {v0}, Landroid/view/textservice/TextServicesManager;-><init>()V

    #@10
    sput-object v0, Landroid/view/textservice/TextServicesManager;->sInstance:Landroid/view/textservice/TextServicesManager;

    #@12
    .line 88
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_16

    #@13
    .line 89
    sget-object v0, Landroid/view/textservice/TextServicesManager;->sInstance:Landroid/view/textservice/TextServicesManager;

    #@15
    goto :goto_a

    #@16
    .line 88
    :catchall_16
    move-exception v0

    #@17
    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method


# virtual methods
.method public getCurrentSpellChecker()Landroid/view/textservice/SpellCheckerInfo;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 194
    :try_start_1
    sget-object v2, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@3
    const/4 v3, 0x0

    #@4
    invoke-interface {v2, v3}, Lcom/android/internal/textservice/ITextServicesManager;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 196
    :goto_8
    return-object v1

    #@9
    .line 195
    :catch_9
    move-exception v0

    #@a
    .line 196
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_8
.end method

.method public getCurrentSpellCheckerSubtype(Z)Landroid/view/textservice/SpellCheckerSubtype;
    .registers 7
    .parameter "allowImplicitlySelectedSubtype"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 220
    :try_start_1
    sget-object v2, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@3
    if-nez v2, :cond_e

    #@5
    .line 223
    sget-object v2, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@7
    const-string/jumbo v3, "sService is null."

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 230
    :goto_d
    return-object v1

    #@e
    .line 227
    :cond_e
    sget-object v2, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-interface {v2, v3, p1}, Lcom/android/internal/textservice/ITextServicesManager;->getCurrentSpellCheckerSubtype(Ljava/lang/String;Z)Landroid/view/textservice/SpellCheckerSubtype;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_14} :catch_16

    #@14
    move-result-object v1

    #@15
    goto :goto_d

    #@16
    .line 228
    :catch_16
    move-exception v0

    #@17
    .line 229
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "Error in getCurrentSpellCheckerSubtype: "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_d
.end method

.method public getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;
    .registers 6

    #@0
    .prologue
    .line 177
    :try_start_0
    sget-object v2, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@2
    invoke-interface {v2}, Lcom/android/internal/textservice/ITextServicesManager;->getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 184
    :goto_6
    return-object v1

    #@7
    .line 182
    :catch_7
    move-exception v0

    #@8
    .line 183
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Error in getEnabledSpellCheckers: "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 184
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public isSpellCheckerEnabled()Z
    .registers 5

    #@0
    .prologue
    .line 267
    :try_start_0
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@2
    invoke-interface {v1}, Lcom/android/internal/textservice/ITextServicesManager;->isSpellCheckerEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 270
    :goto_6
    return v1

    #@7
    .line 268
    :catch_7
    move-exception v0

    #@8
    .line 269
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Error in isSpellCheckerEnabled:"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 270
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public newSpellCheckerSession(Landroid/os/Bundle;Ljava/util/Locale;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Z)Landroid/view/textservice/SpellCheckerSession;
    .registers 22
    .parameter "bundle"
    .parameter "locale"
    .parameter "listener"
    .parameter "referToSpellCheckerLanguageSettings"

    #@0
    .prologue
    .line 108
    if-nez p3, :cond_8

    #@2
    .line 109
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v1

    #@8
    .line 111
    :cond_8
    if-nez p4, :cond_14

    #@a
    if-nez p2, :cond_14

    #@c
    .line 112
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v2, "Locale should not be null if you don\'t refer settings."

    #@10
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 116
    :cond_14
    if-eqz p4, :cond_1e

    #@16
    invoke-virtual/range {p0 .. p0}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_1e

    #@1c
    .line 117
    const/4 v12, 0x0

    #@1d
    .line 169
    :goto_1d
    return-object v12

    #@1e
    .line 122
    :cond_1e
    :try_start_1e
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@20
    const/4 v2, 0x0

    #@21
    invoke-interface {v1, v2}, Lcom/android/internal/textservice/ITextServicesManager;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_24} :catch_29

    #@24
    move-result-object v11

    #@25
    .line 126
    .local v11, sci:Landroid/view/textservice/SpellCheckerInfo;
    if-nez v11, :cond_2c

    #@27
    .line 127
    const/4 v12, 0x0

    #@28
    goto :goto_1d

    #@29
    .line 123
    .end local v11           #sci:Landroid/view/textservice/SpellCheckerInfo;
    :catch_29
    move-exception v7

    #@2a
    .line 124
    .local v7, e:Landroid/os/RemoteException;
    const/4 v12, 0x0

    #@2b
    goto :goto_1d

    #@2c
    .line 129
    .end local v7           #e:Landroid/os/RemoteException;
    .restart local v11       #sci:Landroid/view/textservice/SpellCheckerInfo;
    :cond_2c
    const/4 v14, 0x0

    #@2d
    .line 130
    .local v14, subtypeInUse:Landroid/view/textservice/SpellCheckerSubtype;
    if-eqz p4, :cond_66

    #@2f
    .line 131
    const/4 v1, 0x1

    #@30
    move-object/from16 v0, p0

    #@32
    invoke-virtual {v0, v1}, Landroid/view/textservice/TextServicesManager;->getCurrentSpellCheckerSubtype(Z)Landroid/view/textservice/SpellCheckerSubtype;

    #@35
    move-result-object v14

    #@36
    .line 132
    if-nez v14, :cond_3a

    #@38
    .line 133
    const/4 v12, 0x0

    #@39
    goto :goto_1d

    #@3a
    .line 135
    :cond_3a
    if-eqz p2, :cond_82

    #@3c
    .line 136
    invoke-virtual {v14}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@3f
    move-result-object v15

    #@40
    .line 137
    .local v15, subtypeLocale:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@43
    move-result-object v9

    #@44
    .line 138
    .local v9, inputLocale:Ljava/lang/String;
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x2

    #@49
    if-lt v1, v2, :cond_64

    #@4b
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@4e
    move-result v1

    #@4f
    const/4 v2, 0x2

    #@50
    if-lt v1, v2, :cond_64

    #@52
    const/4 v1, 0x0

    #@53
    const/4 v2, 0x2

    #@54
    invoke-virtual {v15, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    const/4 v2, 0x0

    #@59
    const/4 v3, 0x2

    #@5a
    invoke-virtual {v9, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v1

    #@62
    if-nez v1, :cond_82

    #@64
    .line 140
    :cond_64
    const/4 v12, 0x0

    #@65
    goto :goto_1d

    #@66
    .line 144
    .end local v9           #inputLocale:Ljava/lang/String;
    .end local v15           #subtypeLocale:Ljava/lang/String;
    :cond_66
    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@69
    move-result-object v10

    #@6a
    .line 145
    .local v10, localeStr:Ljava/lang/String;
    const/4 v8, 0x0

    #@6b
    .local v8, i:I
    :goto_6b
    invoke-virtual {v11}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@6e
    move-result v1

    #@6f
    if-ge v8, v1, :cond_82

    #@71
    .line 146
    invoke-virtual {v11, v8}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    #@74
    move-result-object v13

    #@75
    .line 147
    .local v13, subtype:Landroid/view/textservice/SpellCheckerSubtype;
    invoke-virtual {v13}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@78
    move-result-object v16

    #@79
    .line 148
    .local v16, tempSubtypeLocale:Ljava/lang/String;
    move-object/from16 v0, v16

    #@7b
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v1

    #@7f
    if-eqz v1, :cond_86

    #@81
    .line 149
    move-object v14, v13

    #@82
    .line 157
    .end local v8           #i:I
    .end local v10           #localeStr:Ljava/lang/String;
    .end local v13           #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    .end local v16           #tempSubtypeLocale:Ljava/lang/String;
    :cond_82
    if-nez v14, :cond_a0

    #@84
    .line 158
    const/4 v12, 0x0

    #@85
    goto :goto_1d

    #@86
    .line 151
    .restart local v8       #i:I
    .restart local v10       #localeStr:Ljava/lang/String;
    .restart local v13       #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    .restart local v16       #tempSubtypeLocale:Ljava/lang/String;
    :cond_86
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    #@89
    move-result v1

    #@8a
    const/4 v2, 0x2

    #@8b
    if-lt v1, v2, :cond_9d

    #@8d
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    #@90
    move-result v1

    #@91
    const/4 v2, 0x2

    #@92
    if-lt v1, v2, :cond_9d

    #@94
    move-object/from16 v0, v16

    #@96
    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@99
    move-result v1

    #@9a
    if-eqz v1, :cond_9d

    #@9c
    .line 153
    move-object v14, v13

    #@9d
    .line 145
    :cond_9d
    add-int/lit8 v8, v8, 0x1

    #@9f
    goto :goto_6b

    #@a0
    .line 160
    .end local v8           #i:I
    .end local v10           #localeStr:Ljava/lang/String;
    .end local v13           #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    .end local v16           #tempSubtypeLocale:Ljava/lang/String;
    :cond_a0
    new-instance v12, Landroid/view/textservice/SpellCheckerSession;

    #@a2
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@a4
    move-object/from16 v0, p3

    #@a6
    invoke-direct {v12, v11, v1, v0, v14}, Landroid/view/textservice/SpellCheckerSession;-><init>(Landroid/view/textservice/SpellCheckerInfo;Lcom/android/internal/textservice/ITextServicesManager;Landroid/view/textservice/SpellCheckerSession$SpellCheckerSessionListener;Landroid/view/textservice/SpellCheckerSubtype;)V

    #@a9
    .line 163
    .local v12, session:Landroid/view/textservice/SpellCheckerSession;
    :try_start_a9
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@ab
    invoke-virtual {v11}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {v14}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@b2
    move-result-object v3

    #@b3
    invoke-virtual {v12}, Landroid/view/textservice/SpellCheckerSession;->getTextServicesSessionListener()Lcom/android/internal/textservice/ITextServicesSessionListener;

    #@b6
    move-result-object v4

    #@b7
    invoke-virtual {v12}, Landroid/view/textservice/SpellCheckerSession;->getSpellCheckerSessionListener()Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@ba
    move-result-object v5

    #@bb
    move-object/from16 v6, p1

    #@bd
    invoke-interface/range {v1 .. v6}, Lcom/android/internal/textservice/ITextServicesManager;->getSpellCheckerService(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)V
    :try_end_c0
    .catch Landroid/os/RemoteException; {:try_start_a9 .. :try_end_c0} :catch_c2

    #@c0
    goto/16 :goto_1d

    #@c2
    .line 166
    :catch_c2
    move-exception v7

    #@c3
    .line 167
    .restart local v7       #e:Landroid/os/RemoteException;
    const/4 v12, 0x0

    #@c4
    goto/16 :goto_1d
.end method

.method public setCurrentSpellChecker(Landroid/view/textservice/SpellCheckerInfo;)V
    .registers 6
    .parameter "sci"

    #@0
    .prologue
    .line 205
    if-nez p1, :cond_24

    #@2
    .line 206
    :try_start_2
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string v2, "SpellCheckerInfo is null."

    #@6
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_a} :catch_a

    #@a
    .line 209
    :catch_a
    move-exception v0

    #@b
    .line 210
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Error in setCurrentSpellChecker: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 212
    .end local v0           #e:Landroid/os/RemoteException;
    :goto_23
    return-void

    #@24
    .line 208
    :cond_24
    :try_start_24
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@26
    const/4 v2, 0x0

    #@27
    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-interface {v1, v2, v3}, Lcom/android/internal/textservice/ITextServicesManager;->setCurrentSpellChecker(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2e} :catch_a

    #@2e
    goto :goto_23
.end method

.method public setSpellCheckerEnabled(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    .line 256
    :try_start_0
    sget-object v1, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@2
    invoke-interface {v1, p1}, Lcom/android/internal/textservice/ITextServicesManager;->setSpellCheckerEnabled(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 260
    :goto_5
    return-void

    #@6
    .line 257
    :catch_6
    move-exception v0

    #@7
    .line 258
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Error in setSpellCheckerEnabled:"

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_5
.end method

.method public setSpellCheckerSubtype(Landroid/view/textservice/SpellCheckerSubtype;)V
    .registers 7
    .parameter "subtype"

    #@0
    .prologue
    .line 240
    if-nez p1, :cond_a

    #@2
    .line 241
    const/4 v1, 0x0

    #@3
    .line 245
    .local v1, hashCode:I
    :goto_3
    :try_start_3
    sget-object v2, Landroid/view/textservice/TextServicesManager;->sService:Lcom/android/internal/textservice/ITextServicesManager;

    #@5
    const/4 v3, 0x0

    #@6
    invoke-interface {v2, v3, v1}, Lcom/android/internal/textservice/ITextServicesManager;->setCurrentSpellCheckerSubtype(Ljava/lang/String;I)V

    #@9
    .line 249
    .end local v1           #hashCode:I
    :goto_9
    return-void

    #@a
    .line 243
    :cond_a
    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerSubtype;->hashCode()I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .restart local v1       #hashCode:I
    goto :goto_3

    #@f
    .line 246
    .end local v1           #hashCode:I
    :catch_f
    move-exception v0

    #@10
    .line 247
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/view/textservice/TextServicesManager;->TAG:Ljava/lang/String;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Error in setSpellCheckerSubtype:"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_9
.end method
