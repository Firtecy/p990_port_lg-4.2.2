.class public final Landroid/view/textservice/SpellCheckerSubtype;
.super Ljava/lang/Object;
.source "SpellCheckerSubtype.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/textservice/SpellCheckerSubtype;",
            ">;"
        }
    .end annotation
.end field

.field private static final EXTRA_VALUE_KEY_VALUE_SEPARATOR:Ljava/lang/String; = "="

.field private static final EXTRA_VALUE_PAIR_SEPARATOR:Ljava/lang/String; = ","

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mExtraValueHashMapCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSubtypeExtraValue:Ljava/lang/String;

.field private final mSubtypeHashCode:I

.field private final mSubtypeLocale:Ljava/lang/String;

.field private final mSubtypeNameResId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    const-class v0, Landroid/view/textservice/SpellCheckerSubtype;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/view/textservice/SpellCheckerSubtype;->TAG:Ljava/lang/String;

    #@8
    .line 206
    new-instance v0, Landroid/view/textservice/SpellCheckerSubtype$1;

    #@a
    invoke-direct {v0}, Landroid/view/textservice/SpellCheckerSubtype$1;-><init>()V

    #@d
    sput-object v0, Landroid/view/textservice/SpellCheckerSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "nameId"
    .parameter "locale"
    .parameter "extraValue"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    iput p1, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeNameResId:I

    #@5
    .line 56
    if-eqz p2, :cond_18

    #@7
    .end local p2
    :goto_7
    iput-object p2, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@9
    .line 57
    if-eqz p3, :cond_1b

    #@b
    .end local p3
    :goto_b
    iput-object p3, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@d
    .line 58
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@f
    iget-object v1, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@11
    invoke-static {v0, v1}, Landroid/view/textservice/SpellCheckerSubtype;->hashCodeInternal(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    move-result v0

    #@15
    iput v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeHashCode:I

    #@17
    .line 59
    return-void

    #@18
    .line 56
    .restart local p2
    .restart local p3
    :cond_18
    const-string p2, ""

    #@1a
    goto :goto_7

    #@1b
    .line 57
    .end local p2
    :cond_1b
    const-string p3, ""

    #@1d
    goto :goto_b
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v1

    #@7
    iput v1, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeNameResId:I

    #@9
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 65
    .local v0, s:Ljava/lang/String;
    if-eqz v0, :cond_24

    #@f
    .end local v0           #s:Ljava/lang/String;
    :goto_f
    iput-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@11
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 67
    .restart local v0       #s:Ljava/lang/String;
    if-eqz v0, :cond_27

    #@17
    .end local v0           #s:Ljava/lang/String;
    :goto_17
    iput-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@19
    .line 68
    iget-object v1, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@1b
    iget-object v2, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@1d
    invoke-static {v1, v2}, Landroid/view/textservice/SpellCheckerSubtype;->hashCodeInternal(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    move-result v1

    #@21
    iput v1, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeHashCode:I

    #@23
    .line 69
    return-void

    #@24
    .line 65
    .restart local v0       #s:Ljava/lang/String;
    :cond_24
    const-string v0, ""

    #@26
    goto :goto_f

    #@27
    .line 67
    :cond_27
    const-string v0, ""

    #@29
    goto :goto_17
.end method

.method public static constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;
    .registers 8
    .parameter "localeStr"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 153
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_c

    #@b
    .line 165
    :cond_b
    :goto_b
    return-object v1

    #@c
    .line 155
    :cond_c
    const-string v2, "_"

    #@e
    invoke-virtual {p0, v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 158
    .local v0, localeParams:[Ljava/lang/String;
    array-length v2, v0

    #@13
    if-ne v2, v4, :cond_1d

    #@15
    .line 159
    new-instance v1, Ljava/util/Locale;

    #@17
    aget-object v2, v0, v3

    #@19
    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    #@1c
    goto :goto_b

    #@1d
    .line 160
    :cond_1d
    array-length v2, v0

    #@1e
    if-ne v2, v5, :cond_2a

    #@20
    .line 161
    new-instance v1, Ljava/util/Locale;

    #@22
    aget-object v2, v0, v3

    #@24
    aget-object v3, v0, v4

    #@26
    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_b

    #@2a
    .line 162
    :cond_2a
    array-length v2, v0

    #@2b
    if-ne v2, v6, :cond_b

    #@2d
    .line 163
    new-instance v1, Ljava/util/Locale;

    #@2f
    aget-object v2, v0, v3

    #@31
    aget-object v3, v0, v4

    #@33
    aget-object v4, v0, v5

    #@35
    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@38
    goto :goto_b
.end method

.method private getExtraValueHashMap()Ljava/util/HashMap;
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 93
    iget-object v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@4
    if-nez v4, :cond_47

    #@6
    .line 94
    new-instance v4, Ljava/util/HashMap;

    #@8
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@b
    iput-object v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@d
    .line 95
    iget-object v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@f
    const-string v5, ","

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    .line 96
    .local v3, pairs:[Ljava/lang/String;
    array-length v0, v3

    #@16
    .line 97
    .local v0, N:I
    const/4 v1, 0x0

    #@17
    .local v1, i:I
    :goto_17
    if-ge v1, v0, :cond_47

    #@19
    .line 98
    aget-object v4, v3, v1

    #@1b
    const-string v5, "="

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 99
    .local v2, pair:[Ljava/lang/String;
    array-length v4, v2

    #@22
    if-ne v4, v7, :cond_2f

    #@24
    .line 100
    iget-object v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@26
    aget-object v5, v2, v8

    #@28
    const/4 v6, 0x0

    #@29
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 97
    :cond_2c
    :goto_2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_17

    #@2f
    .line 101
    :cond_2f
    array-length v4, v2

    #@30
    if-le v4, v7, :cond_2c

    #@32
    .line 102
    array-length v4, v2

    #@33
    const/4 v5, 0x2

    #@34
    if-le v4, v5, :cond_3d

    #@36
    .line 103
    sget-object v4, Landroid/view/textservice/SpellCheckerSubtype;->TAG:Ljava/lang/String;

    #@38
    const-string v5, "ExtraValue has two or more \'=\'s"

    #@3a
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 105
    :cond_3d
    iget-object v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@3f
    aget-object v5, v2, v8

    #@41
    aget-object v6, v2, v7

    #@43
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    goto :goto_2c

    #@47
    .line 109
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #pair:[Ljava/lang/String;
    .end local v3           #pairs:[Ljava/lang/String;
    :cond_47
    iget-object v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@49
    return-object v4
.end method

.method private static hashCodeInternal(Ljava/lang/String;Ljava/lang/String;)I
    .registers 4
    .parameter "locale"
    .parameter "extraValue"

    #@0
    .prologue
    .line 220
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    aput-object p0, v0, v1

    #@6
    const/4 v1, 0x1

    #@7
    aput-object p1, v0, v1

    #@9
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public static sort(Landroid/content/Context;ILandroid/view/textservice/SpellCheckerInfo;Ljava/util/List;)Ljava/util/List;
    .registers 11
    .parameter "context"
    .parameter "flags"
    .parameter "sci"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/view/textservice/SpellCheckerInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/view/textservice/SpellCheckerSubtype;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/view/textservice/SpellCheckerSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 234
    .local p3, subtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/textservice/SpellCheckerSubtype;>;"
    if-nez p2, :cond_3

    #@2
    .line 251
    .end local p3           #subtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/textservice/SpellCheckerSubtype;>;"
    :goto_2
    return-object p3

    #@3
    .line 235
    .restart local p3       #subtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/textservice/SpellCheckerSubtype;>;"
    :cond_3
    new-instance v5, Ljava/util/HashSet;

    #@5
    invoke-direct {v5, p3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@8
    .line 237
    .local v5, subtypesSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/textservice/SpellCheckerSubtype;>;"
    new-instance v3, Ljava/util/ArrayList;

    #@a
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@d
    .line 238
    .local v3, sortedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/textservice/SpellCheckerSubtype;>;"
    invoke-virtual {p2}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@10
    move-result v0

    #@11
    .line 239
    .local v0, N:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_27

    #@14
    .line 240
    invoke-virtual {p2, v1}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    #@17
    move-result-object v4

    #@18
    .line 241
    .local v4, subtype:Landroid/view/textservice/SpellCheckerSubtype;
    invoke-virtual {v5, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_24

    #@1e
    .line 242
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 243
    invoke-virtual {v5, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@24
    .line 239
    :cond_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_12

    #@27
    .line 248
    .end local v4           #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_27
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v2

    #@2b
    .local v2, i$:Ljava/util/Iterator;
    :goto_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v6

    #@2f
    if-eqz v6, :cond_3b

    #@31
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v4

    #@35
    check-cast v4, Landroid/view/textservice/SpellCheckerSubtype;

    #@37
    .line 249
    .restart local v4       #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_2b

    #@3b
    .end local v4           #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_3b
    move-object p3, v3

    #@3c
    .line 251
    goto :goto_2
.end method


# virtual methods
.method public containsExtraValueKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 119
    invoke-direct {p0}, Landroid/view/textservice/SpellCheckerSubtype;->getExtraValueHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 196
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 139
    instance-of v2, p1, Landroid/view/textservice/SpellCheckerSubtype;

    #@3
    if-eqz v2, :cond_39

    #@5
    move-object v0, p1

    #@6
    .line 140
    check-cast v0, Landroid/view/textservice/SpellCheckerSubtype;

    #@8
    .line 141
    .local v0, subtype:Landroid/view/textservice/SpellCheckerSubtype;
    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerSubtype;->hashCode()I

    #@b
    move-result v2

    #@c
    invoke-virtual {p0}, Landroid/view/textservice/SpellCheckerSubtype;->hashCode()I

    #@f
    move-result v3

    #@10
    if-ne v2, v3, :cond_39

    #@12
    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerSubtype;->getNameResId()I

    #@15
    move-result v2

    #@16
    invoke-virtual {p0}, Landroid/view/textservice/SpellCheckerSubtype;->getNameResId()I

    #@19
    move-result v3

    #@1a
    if-ne v2, v3, :cond_39

    #@1c
    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {p0}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_39

    #@2a
    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerSubtype;->getExtraValue()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {p0}, Landroid/view/textservice/SpellCheckerSubtype;->getExtraValue()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v2

    #@36
    if-eqz v2, :cond_39

    #@38
    const/4 v1, 0x1

    #@39
    .line 146
    .end local v0           #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_39
    return v1
.end method

.method public getDisplayName(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    .registers 10
    .parameter "context"
    .parameter "packageName"
    .parameter "appInfo"

    #@0
    .prologue
    .line 180
    iget-object v3, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@2
    invoke-static {v3}, Landroid/view/textservice/SpellCheckerSubtype;->constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;

    #@5
    move-result-object v0

    #@6
    .line 181
    .local v0, locale:Ljava/util/Locale;
    if-eqz v0, :cond_11

    #@8
    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 182
    .local v1, localeStr:Ljava/lang/String;
    :goto_c
    iget v3, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeNameResId:I

    #@e
    if-nez v3, :cond_14

    #@10
    .line 190
    .end local v1           #localeStr:Ljava/lang/String;
    :cond_10
    :goto_10
    return-object v1

    #@11
    .line 181
    :cond_11
    iget-object v1, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@13
    goto :goto_c

    #@14
    .line 185
    .restart local v1       #localeStr:Ljava/lang/String;
    :cond_14
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@17
    move-result-object v3

    #@18
    iget v4, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeNameResId:I

    #@1a
    invoke-virtual {v3, p2, v4, p3}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@1d
    move-result-object v2

    #@1e
    .line 187
    .local v2, subtypeName:Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_10

    #@24
    .line 188
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    const/4 v4, 0x1

    #@29
    new-array v4, v4, [Ljava/lang/Object;

    #@2b
    const/4 v5, 0x0

    #@2c
    aput-object v1, v4, v5

    #@2e
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    goto :goto_10
.end method

.method public getExtraValue()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getExtraValueOf(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 129
    invoke-direct {p0}, Landroid/view/textservice/SpellCheckerSubtype;->getExtraValueHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/String;

    #@a
    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNameResId()I
    .registers 2

    #@0
    .prologue
    .line 75
    iget v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeNameResId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 134
    iget v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeHashCode:I

    #@2
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 201
    iget v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeNameResId:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 202
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 203
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 204
    return-void
.end method
