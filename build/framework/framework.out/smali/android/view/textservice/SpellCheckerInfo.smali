.class public final Landroid/view/textservice/SpellCheckerInfo;
.super Ljava/lang/Object;
.source "SpellCheckerInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/textservice/SpellCheckerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mId:Ljava/lang/String;

.field private final mLabel:I

.field private final mService:Landroid/content/pm/ResolveInfo;

.field private final mSettingsActivityName:Ljava/lang/String;

.field private final mSubtypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/textservice/SpellCheckerSubtype;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 44
    const-class v0, Landroid/view/textservice/SpellCheckerInfo;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/view/textservice/SpellCheckerInfo;->TAG:Ljava/lang/String;

    #@8
    .line 189
    new-instance v0, Landroid/view/textservice/SpellCheckerInfo$1;

    #@a
    invoke-direct {v0}, Landroid/view/textservice/SpellCheckerInfo$1;-><init>()V

    #@d
    sput-object v0, Landroid/view/textservice/SpellCheckerInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .registers 24
    .parameter "context"
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 58
    new-instance v18, Ljava/util/ArrayList;

    #@5
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@8
    move-object/from16 v0, v18

    #@a
    move-object/from16 v1, p0

    #@c
    iput-object v0, v1, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@e
    .line 66
    move-object/from16 v0, p2

    #@10
    move-object/from16 v1, p0

    #@12
    iput-object v0, v1, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@14
    .line 67
    move-object/from16 v0, p2

    #@16
    iget-object v14, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@18
    .line 68
    .local v14, si:Landroid/content/pm/ServiceInfo;
    new-instance v18, Landroid/content/ComponentName;

    #@1a
    iget-object v0, v14, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1c
    move-object/from16 v19, v0

    #@1e
    iget-object v0, v14, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@20
    move-object/from16 v20, v0

    #@22
    invoke-direct/range {v18 .. v20}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    invoke-virtual/range {v18 .. v18}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@28
    move-result-object v18

    #@29
    move-object/from16 v0, v18

    #@2b
    move-object/from16 v1, p0

    #@2d
    iput-object v0, v1, Landroid/view/textservice/SpellCheckerInfo;->mId:Ljava/lang/String;

    #@2f
    .line 70
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@32
    move-result-object v10

    #@33
    .line 71
    .local v10, pm:Landroid/content/pm/PackageManager;
    const/4 v7, 0x0

    #@34
    .line 72
    .local v7, label:I
    const/4 v13, 0x0

    #@35
    .line 74
    .local v13, settingsActivityComponent:Ljava/lang/String;
    const/4 v9, 0x0

    #@36
    .line 76
    .local v9, parser:Landroid/content/res/XmlResourceParser;
    :try_start_36
    const-string v18, "android.view.textservice.scs"

    #@38
    move-object/from16 v0, v18

    #@3a
    invoke-virtual {v14, v10, v0}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@3d
    move-result-object v9

    #@3e
    .line 77
    if-nez v9, :cond_87

    #@40
    .line 78
    new-instance v18, Lorg/xmlpull/v1/XmlPullParserException;

    #@42
    const-string v19, "No android.view.textservice.scs meta-data"

    #@44
    invoke-direct/range {v18 .. v19}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@47
    throw v18
    :try_end_48
    .catchall {:try_start_36 .. :try_end_48} :catchall_80
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_48} :catch_48

    #@48
    .line 124
    :catch_48
    move-exception v6

    #@49
    .line 125
    .local v6, e:Ljava/lang/Exception;
    :try_start_49
    sget-object v18, Landroid/view/textservice/SpellCheckerInfo;->TAG:Ljava/lang/String;

    #@4b
    new-instance v19, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v20, "Caught exception: "

    #@52
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v19

    #@56
    move-object/from16 v0, v19

    #@58
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v19

    #@5c
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v19

    #@60
    invoke-static/range {v18 .. v19}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 126
    new-instance v18, Lorg/xmlpull/v1/XmlPullParserException;

    #@65
    new-instance v19, Ljava/lang/StringBuilder;

    #@67
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v20, "Unable to create context for: "

    #@6c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v19

    #@70
    iget-object v0, v14, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@72
    move-object/from16 v20, v0

    #@74
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v19

    #@78
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v19

    #@7c
    invoke-direct/range {v18 .. v19}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7f
    throw v18
    :try_end_80
    .catchall {:try_start_49 .. :try_end_80} :catchall_80

    #@80
    .line 129
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_80
    move-exception v18

    #@81
    if-eqz v9, :cond_86

    #@83
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@86
    :cond_86
    throw v18

    #@87
    .line 82
    :cond_87
    :try_start_87
    iget-object v0, v14, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@89
    move-object/from16 v18, v0

    #@8b
    move-object/from16 v0, v18

    #@8d
    invoke-virtual {v10, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@90
    move-result-object v11

    #@91
    .line 83
    .local v11, res:Landroid/content/res/Resources;
    invoke-static {v9}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@94
    move-result-object v4

    #@95
    .line 86
    .local v4, attrs:Landroid/util/AttributeSet;
    :cond_95
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@98
    move-result v17

    #@99
    .local v17, type:I
    const/16 v18, 0x1

    #@9b
    move/from16 v0, v17

    #@9d
    move/from16 v1, v18

    #@9f
    if-eq v0, v1, :cond_a9

    #@a1
    const/16 v18, 0x2

    #@a3
    move/from16 v0, v17

    #@a5
    move/from16 v1, v18

    #@a7
    if-ne v0, v1, :cond_95

    #@a9
    .line 89
    :cond_a9
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@ac
    move-result-object v8

    #@ad
    .line 90
    .local v8, nodeName:Ljava/lang/String;
    const-string/jumbo v18, "spell-checker"

    #@b0
    move-object/from16 v0, v18

    #@b2
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b5
    move-result v18

    #@b6
    if-nez v18, :cond_c0

    #@b8
    .line 91
    new-instance v18, Lorg/xmlpull/v1/XmlPullParserException;

    #@ba
    const-string v19, "Meta-data does not start with spell-checker tag"

    #@bc
    invoke-direct/range {v18 .. v19}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@bf
    throw v18

    #@c0
    .line 95
    :cond_c0
    sget-object v18, Lcom/android/internal/R$styleable;->SpellChecker:[I

    #@c2
    move-object/from16 v0, v18

    #@c4
    invoke-virtual {v11, v4, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@c7
    move-result-object v12

    #@c8
    .line 97
    .local v12, sa:Landroid/content/res/TypedArray;
    const/16 v18, 0x0

    #@ca
    const/16 v19, 0x0

    #@cc
    move/from16 v0, v18

    #@ce
    move/from16 v1, v19

    #@d0
    invoke-virtual {v12, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@d3
    move-result v7

    #@d4
    .line 98
    const/16 v18, 0x1

    #@d6
    move/from16 v0, v18

    #@d8
    invoke-virtual {v12, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@db
    move-result-object v13

    #@dc
    .line 100
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->recycle()V

    #@df
    .line 102
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@e2
    move-result v5

    #@e3
    .line 105
    .local v5, depth:I
    :cond_e3
    :goto_e3
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@e6
    move-result v17

    #@e7
    const/16 v18, 0x3

    #@e9
    move/from16 v0, v17

    #@eb
    move/from16 v1, v18

    #@ed
    if-ne v0, v1, :cond_f7

    #@ef
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@f2
    move-result v18

    #@f3
    move/from16 v0, v18

    #@f5
    if-le v0, v5, :cond_15b

    #@f7
    :cond_f7
    const/16 v18, 0x1

    #@f9
    move/from16 v0, v17

    #@fb
    move/from16 v1, v18

    #@fd
    if-eq v0, v1, :cond_15b

    #@ff
    .line 106
    const/16 v18, 0x2

    #@101
    move/from16 v0, v17

    #@103
    move/from16 v1, v18

    #@105
    if-ne v0, v1, :cond_e3

    #@107
    .line 107
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@10a
    move-result-object v16

    #@10b
    .line 108
    .local v16, subtypeNodeName:Ljava/lang/String;
    const-string/jumbo v18, "subtype"

    #@10e
    move-object/from16 v0, v18

    #@110
    move-object/from16 v1, v16

    #@112
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@115
    move-result v18

    #@116
    if-nez v18, :cond_120

    #@118
    .line 109
    new-instance v18, Lorg/xmlpull/v1/XmlPullParserException;

    #@11a
    const-string v19, "Meta-data in spell-checker does not start with subtype tag"

    #@11c
    invoke-direct/range {v18 .. v19}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@11f
    throw v18

    #@120
    .line 112
    :cond_120
    sget-object v18, Lcom/android/internal/R$styleable;->SpellChecker_Subtype:[I

    #@122
    move-object/from16 v0, v18

    #@124
    invoke-virtual {v11, v4, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@127
    move-result-object v3

    #@128
    .line 114
    .local v3, a:Landroid/content/res/TypedArray;
    new-instance v15, Landroid/view/textservice/SpellCheckerSubtype;

    #@12a
    const/16 v18, 0x0

    #@12c
    const/16 v19, 0x0

    #@12e
    move/from16 v0, v18

    #@130
    move/from16 v1, v19

    #@132
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@135
    move-result v18

    #@136
    const/16 v19, 0x1

    #@138
    move/from16 v0, v19

    #@13a
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@13d
    move-result-object v19

    #@13e
    const/16 v20, 0x2

    #@140
    move/from16 v0, v20

    #@142
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@145
    move-result-object v20

    #@146
    move/from16 v0, v18

    #@148
    move-object/from16 v1, v19

    #@14a
    move-object/from16 v2, v20

    #@14c
    invoke-direct {v15, v0, v1, v2}, Landroid/view/textservice/SpellCheckerSubtype;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    #@14f
    .line 121
    .local v15, subtype:Landroid/view/textservice/SpellCheckerSubtype;
    move-object/from16 v0, p0

    #@151
    iget-object v0, v0, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@153
    move-object/from16 v18, v0

    #@155
    move-object/from16 v0, v18

    #@157
    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_15a
    .catchall {:try_start_87 .. :try_end_15a} :catchall_80
    .catch Ljava/lang/Exception; {:try_start_87 .. :try_end_15a} :catch_48

    #@15a
    goto :goto_e3

    #@15b
    .line 129
    .end local v3           #a:Landroid/content/res/TypedArray;
    .end local v15           #subtype:Landroid/view/textservice/SpellCheckerSubtype;
    .end local v16           #subtypeNodeName:Ljava/lang/String;
    :cond_15b
    if-eqz v9, :cond_160

    #@15d
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@160
    .line 131
    :cond_160
    move-object/from16 v0, p0

    #@162
    iput v7, v0, Landroid/view/textservice/SpellCheckerInfo;->mLabel:I

    #@164
    .line 132
    move-object/from16 v0, p0

    #@166
    iput-object v13, v0, Landroid/view/textservice/SpellCheckerInfo;->mSettingsActivityName:Ljava/lang/String;

    #@168
    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 139
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 58
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@a
    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mLabel:I

    #@10
    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mId:Ljava/lang/String;

    #@16
    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSettingsActivityName:Ljava/lang/String;

    #@1c
    .line 143
    sget-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@24
    iput-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@26
    .line 144
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@28
    sget-object v1, Landroid/view/textservice/SpellCheckerSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    #@2d
    .line 145
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 264
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getComponent()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 159
    new-instance v0, Landroid/content/ComponentName;

    #@2
    iget-object v1, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@4
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@6
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8
    iget-object v2, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@a
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@c
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@e
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 152
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getServiceInfo()Landroid/content/pm/ServiceInfo;
    .registers 2

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    return-object v0
.end method

.method public getSettingsActivity()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 240
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSettingsActivityName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/textservice/SpellCheckerSubtype;

    #@8
    return-object v0
.end method

.method public getSubtypeCount()I
    .registers 2

    #@0
    .prologue
    .line 247
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "pm"

    #@0
    .prologue
    .line 208
    iget v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mLabel:I

    #@2
    if-eqz v0, :cond_6

    #@4
    if-nez p1, :cond_9

    #@6
    :cond_6
    const-string v0, ""

    #@8
    .line 209
    :goto_8
    return-object v0

    #@9
    :cond_9
    invoke-virtual {p0}, Landroid/view/textservice/SpellCheckerInfo;->getPackageName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iget v1, p0, Landroid/view/textservice/SpellCheckerInfo;->mLabel:I

    #@f
    iget-object v2, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@11
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@13
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@15
    invoke-virtual {p1, v0, v1, v2}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@18
    move-result-object v0

    #@19
    goto :goto_8
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 178
    iget v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mLabel:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 179
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mId:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 180
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSettingsActivityName:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 181
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@11
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 182
    iget-object v0, p0, Landroid/view/textservice/SpellCheckerInfo;->mSubtypes:Ljava/util/ArrayList;

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@19
    .line 183
    return-void
.end method
