.class public final Landroid/view/PointerIcon;
.super Ljava/lang/Object;
.source "PointerIcon.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/PointerIcon;",
            ">;"
        }
    .end annotation
.end field

.field public static final STYLE_ARROW:I = 0x3e8

.field public static final STYLE_CUSTOM:I = -0x1

.field private static final STYLE_DEFAULT:I = 0x3e8

.field public static final STYLE_NULL:I = 0x0

.field private static final STYLE_OEM_FIRST:I = 0x2710

.field public static final STYLE_SPOT_ANCHOR:I = 0x7d2

.field public static final STYLE_SPOT_HOVER:I = 0x7d0

.field public static final STYLE_SPOT_TOUCH:I = 0x7d1

.field private static final TAG:Ljava/lang/String; = "PointerIcon"

.field private static final gNullIcon:Landroid/view/PointerIcon;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mHotSpotX:F

.field private mHotSpotY:F

.field private final mStyle:I

.field private mSystemIconResourceId:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 69
    new-instance v0, Landroid/view/PointerIcon;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    #@6
    sput-object v0, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    #@8
    .line 308
    new-instance v0, Landroid/view/PointerIcon$1;

    #@a
    invoke-direct {v0}, Landroid/view/PointerIcon$1;-><init>()V

    #@d
    sput-object v0, Landroid/view/PointerIcon;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method private constructor <init>(I)V
    .registers 2
    .parameter "style"

    #@0
    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 78
    iput p1, p0, Landroid/view/PointerIcon;->mStyle:I

    #@5
    .line 79
    return-void
.end method

.method synthetic constructor <init>(ILandroid/view/PointerIcon$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/view/PointerIcon;-><init>(I)V

    #@3
    return-void
.end method

.method static synthetic access$102(Landroid/view/PointerIcon;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput p1, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@2
    return p1
.end method

.method public static createCustomIcon(Landroid/graphics/Bitmap;FF)Landroid/view/PointerIcon;
    .registers 6
    .parameter "bitmap"
    .parameter "hotSpotX"
    .parameter "hotSpotY"

    #@0
    .prologue
    .line 162
    if-nez p0, :cond_a

    #@2
    .line 163
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "bitmap must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 165
    :cond_a
    invoke-static {p0, p1, p2}, Landroid/view/PointerIcon;->validateHotSpot(Landroid/graphics/Bitmap;FF)V

    #@d
    .line 167
    new-instance v0, Landroid/view/PointerIcon;

    #@f
    const/4 v1, -0x1

    #@10
    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    #@13
    .line 168
    .local v0, icon:Landroid/view/PointerIcon;
    iput-object p0, v0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@15
    .line 169
    iput p1, v0, Landroid/view/PointerIcon;->mHotSpotX:F

    #@17
    .line 170
    iput p2, v0, Landroid/view/PointerIcon;->mHotSpotY:F

    #@19
    .line 171
    return-object v0
.end method

.method public static getDefaultIcon(Landroid/content/Context;)Landroid/view/PointerIcon;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 101
    const/16 v0, 0x3e8

    #@2
    invoke-static {p0, v0}, Landroid/view/PointerIcon;->getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static getNullIcon()Landroid/view/PointerIcon;
    .registers 1

    #@0
    .prologue
    .line 89
    sget-object v0, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    #@2
    return-object v0
.end method

.method public static getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;
    .registers 12
    .parameter "context"
    .parameter "style"

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    const/16 v8, 0x3e8

    #@3
    .line 115
    if-nez p0, :cond_d

    #@5
    .line 116
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v5, "context must not be null"

    #@9
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v4

    #@d
    .line 119
    :cond_d
    if-nez p1, :cond_12

    #@f
    .line 120
    sget-object v4, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    #@11
    .line 145
    :goto_11
    return-object v4

    #@12
    .line 123
    :cond_12
    invoke-static {p1}, Landroid/view/PointerIcon;->getSystemIconStyleIndex(I)I

    #@15
    move-result v3

    #@16
    .line 124
    .local v3, styleIndex:I
    if-nez v3, :cond_1c

    #@18
    .line 125
    invoke-static {v8}, Landroid/view/PointerIcon;->getSystemIconStyleIndex(I)I

    #@1b
    move-result v3

    #@1c
    .line 128
    :cond_1c
    const/4 v4, 0x0

    #@1d
    sget-object v5, Lcom/android/internal/R$styleable;->Pointer:[I

    #@1f
    const v6, 0x10103f7

    #@22
    const/4 v7, 0x0

    #@23
    invoke-virtual {p0, v4, v5, v6, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@26
    move-result-object v0

    #@27
    .line 131
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@2a
    move-result v2

    #@2b
    .line 132
    .local v2, resourceId:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2e
    .line 134
    if-ne v2, v9, :cond_52

    #@30
    .line 135
    const-string v4, "PointerIcon"

    #@32
    new-instance v5, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v6, "Missing theme resources for pointer icon style "

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 136
    if-ne p1, v8, :cond_4d

    #@4a
    sget-object v4, Landroid/view/PointerIcon;->gNullIcon:Landroid/view/PointerIcon;

    #@4c
    goto :goto_11

    #@4d
    :cond_4d
    invoke-static {p0, v8}, Landroid/view/PointerIcon;->getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;

    #@50
    move-result-object v4

    #@51
    goto :goto_11

    #@52
    .line 139
    :cond_52
    new-instance v1, Landroid/view/PointerIcon;

    #@54
    invoke-direct {v1, p1}, Landroid/view/PointerIcon;-><init>(I)V

    #@57
    .line 140
    .local v1, icon:Landroid/view/PointerIcon;
    const/high16 v4, -0x100

    #@59
    and-int/2addr v4, v2

    #@5a
    const/high16 v5, 0x100

    #@5c
    if-ne v4, v5, :cond_62

    #@5e
    .line 141
    iput v2, v1, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@60
    :goto_60
    move-object v4, v1

    #@61
    .line 145
    goto :goto_11

    #@62
    .line 143
    :cond_62
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@65
    move-result-object v4

    #@66
    invoke-direct {v1, v4, v2}, Landroid/view/PointerIcon;->loadResource(Landroid/content/res/Resources;I)V

    #@69
    goto :goto_60
.end method

.method private static getSystemIconStyleIndex(I)I
    .registers 2
    .parameter "style"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 422
    sparse-switch p0, :sswitch_data_c

    #@4
    .line 432
    :goto_4
    :sswitch_4
    return v0

    #@5
    .line 426
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 428
    :sswitch_7
    const/4 v0, 0x2

    #@8
    goto :goto_4

    #@9
    .line 430
    :sswitch_9
    const/4 v0, 0x3

    #@a
    goto :goto_4

    #@b
    .line 422
    nop

    #@c
    :sswitch_data_c
    .sparse-switch
        0x3e8 -> :sswitch_4
        0x7d0 -> :sswitch_5
        0x7d1 -> :sswitch_7
        0x7d2 -> :sswitch_9
    .end sparse-switch
.end method

.method public static loadCustomIcon(Landroid/content/res/Resources;I)Landroid/view/PointerIcon;
    .registers 5
    .parameter "resources"
    .parameter "resourceId"

    #@0
    .prologue
    .line 196
    if-nez p0, :cond_b

    #@2
    .line 197
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "resources must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 200
    :cond_b
    new-instance v0, Landroid/view/PointerIcon;

    #@d
    const/4 v1, -0x1

    #@e
    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    #@11
    .line 201
    .local v0, icon:Landroid/view/PointerIcon;
    invoke-direct {v0, p0, p1}, Landroid/view/PointerIcon;->loadResource(Landroid/content/res/Resources;I)V

    #@14
    .line 202
    return-object v0
.end method

.method private loadResource(Landroid/content/res/Resources;I)V
    .registers 12
    .parameter "resources"
    .parameter "resourceId"

    #@0
    .prologue
    .line 377
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@3
    move-result-object v6

    #@4
    .line 382
    .local v6, parser:Landroid/content/res/XmlResourceParser;
    :try_start_4
    const-string/jumbo v7, "pointer-icon"

    #@7
    invoke-static {v6, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@a
    .line 384
    sget-object v7, Lcom/android/internal/R$styleable;->PointerIcon:[I

    #@c
    invoke-virtual {p1, v6, v7}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@f
    move-result-object v0

    #@10
    .line 386
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v7, 0x0

    #@11
    const/4 v8, 0x0

    #@12
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@15
    move-result v1

    #@16
    .line 387
    .local v1, bitmapRes:I
    const/4 v7, 0x1

    #@17
    const/4 v8, 0x0

    #@18
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@1b
    move-result v4

    #@1c
    .line 388
    .local v4, hotSpotX:F
    const/4 v7, 0x2

    #@1d
    const/4 v8, 0x0

    #@1e
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@21
    move-result v5

    #@22
    .line 389
    .local v5, hotSpotY:F
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_25
    .catchall {:try_start_4 .. :try_end_25} :catchall_3b
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_25} :catch_32

    #@25
    .line 393
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@28
    .line 396
    if-nez v1, :cond_40

    #@2a
    .line 397
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@2c
    const-string v8, "<pointer-icon> is missing bitmap attribute."

    #@2e
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v7

    #@32
    .line 390
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v1           #bitmapRes:I
    .end local v4           #hotSpotX:F
    .end local v5           #hotSpotY:F
    :catch_32
    move-exception v3

    #@33
    .line 391
    .local v3, ex:Ljava/lang/Exception;
    :try_start_33
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@35
    const-string v8, "Exception parsing pointer icon resource."

    #@37
    invoke-direct {v7, v8, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3a
    throw v7
    :try_end_3b
    .catchall {:try_start_33 .. :try_end_3b} :catchall_3b

    #@3b
    .line 393
    .end local v3           #ex:Ljava/lang/Exception;
    :catchall_3b
    move-exception v7

    #@3c
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->close()V

    #@3f
    throw v7

    #@40
    .line 400
    .restart local v0       #a:Landroid/content/res/TypedArray;
    .restart local v1       #bitmapRes:I
    .restart local v4       #hotSpotX:F
    .restart local v5       #hotSpotY:F
    :cond_40
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@43
    move-result-object v2

    #@44
    .line 401
    .local v2, drawable:Landroid/graphics/drawable/Drawable;
    instance-of v7, v2, Landroid/graphics/drawable/BitmapDrawable;

    #@46
    if-nez v7, :cond_50

    #@48
    .line 402
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@4a
    const-string v8, "<pointer-icon> bitmap attribute must refer to a bitmap drawable."

    #@4c
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v7

    #@50
    .line 407
    :cond_50
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    #@52
    .end local v2           #drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    #@55
    move-result-object v7

    #@56
    iput-object v7, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@58
    .line 408
    iput v4, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    #@5a
    .line 409
    iput v5, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    #@5c
    .line 410
    return-void
.end method

.method private throwIfIconIsNotLoaded()V
    .registers 3

    #@0
    .prologue
    .line 303
    invoke-virtual {p0}, Landroid/view/PointerIcon;->isLoaded()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 304
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "The icon is not loaded."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 306
    :cond_e
    return-void
.end method

.method private static validateHotSpot(Landroid/graphics/Bitmap;FF)V
    .registers 5
    .parameter "bitmap"
    .parameter "hotSpotX"
    .parameter "hotSpotY"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 413
    cmpg-float v0, p1, v1

    #@3
    if-ltz v0, :cond_e

    #@5
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@8
    move-result v0

    #@9
    int-to-float v0, v0

    #@a
    cmpl-float v0, p1, v0

    #@c
    if-ltz v0, :cond_17

    #@e
    .line 414
    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    const-string/jumbo v1, "x hotspot lies outside of the bitmap area"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 416
    :cond_17
    cmpg-float v0, p2, v1

    #@19
    if-ltz v0, :cond_24

    #@1b
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@1e
    move-result v0

    #@1f
    int-to-float v0, v0

    #@20
    cmpl-float v0, p2, v0

    #@22
    if-ltz v0, :cond_2d

    #@24
    .line 417
    :cond_24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@26
    const-string/jumbo v1, "y hotspot lies outside of the bitmap area"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 419
    :cond_2d
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 335
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "other"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 353
    if-ne p0, p1, :cond_5

    #@4
    .line 373
    :cond_4
    :goto_4
    return v1

    #@5
    .line 357
    :cond_5
    if-eqz p1, :cond_b

    #@7
    instance-of v3, p1, Landroid/view/PointerIcon;

    #@9
    if-nez v3, :cond_d

    #@b
    :cond_b
    move v1, v2

    #@c
    .line 358
    goto :goto_4

    #@d
    :cond_d
    move-object v0, p1

    #@e
    .line 361
    check-cast v0, Landroid/view/PointerIcon;

    #@10
    .line 362
    .local v0, otherIcon:Landroid/view/PointerIcon;
    iget v3, p0, Landroid/view/PointerIcon;->mStyle:I

    #@12
    iget v4, v0, Landroid/view/PointerIcon;->mStyle:I

    #@14
    if-ne v3, v4, :cond_1c

    #@16
    iget v3, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@18
    iget v4, v0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@1a
    if-eq v3, v4, :cond_1e

    #@1c
    :cond_1c
    move v1, v2

    #@1d
    .line 364
    goto :goto_4

    #@1e
    .line 367
    :cond_1e
    iget v3, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@20
    if-nez v3, :cond_4

    #@22
    iget-object v3, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@24
    iget-object v4, v0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@26
    if-ne v3, v4, :cond_38

    #@28
    iget v3, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    #@2a
    iget v4, v0, Landroid/view/PointerIcon;->mHotSpotX:F

    #@2c
    cmpl-float v3, v3, v4

    #@2e
    if-nez v3, :cond_38

    #@30
    iget v3, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    #@32
    iget v4, v0, Landroid/view/PointerIcon;->mHotSpotY:F

    #@34
    cmpl-float v3, v3, v4

    #@36
    if-eqz v3, :cond_4

    #@38
    :cond_38
    move v1, v2

    #@39
    .line 370
    goto :goto_4
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 270
    invoke-direct {p0}, Landroid/view/PointerIcon;->throwIfIconIsNotLoaded()V

    #@3
    .line 271
    iget-object v0, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@5
    return-object v0
.end method

.method public getHotSpotX()F
    .registers 2

    #@0
    .prologue
    .line 284
    invoke-direct {p0}, Landroid/view/PointerIcon;->throwIfIconIsNotLoaded()V

    #@3
    .line 285
    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    #@5
    return v0
.end method

.method public getHotSpotY()F
    .registers 2

    #@0
    .prologue
    .line 298
    invoke-direct {p0}, Landroid/view/PointerIcon;->throwIfIconIsNotLoaded()V

    #@3
    .line 299
    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    #@5
    return v0
.end method

.method public getStyle()I
    .registers 2

    #@0
    .prologue
    .line 257
    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    #@2
    return v0
.end method

.method public isLoaded()Z
    .registers 2

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    if-nez v0, :cond_8

    #@4
    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    #@6
    if-nez v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isNullIcon()Z
    .registers 2

    #@0
    .prologue
    .line 237
    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public load(Landroid/content/Context;)Landroid/view/PointerIcon;
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 217
    if-nez p1, :cond_a

    #@2
    .line 218
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "context must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 221
    :cond_a
    iget v1, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@c
    if-eqz v1, :cond_12

    #@e
    iget-object v1, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@10
    if-eqz v1, :cond_14

    #@12
    :cond_12
    move-object v0, p0

    #@13
    .line 228
    :goto_13
    return-object v0

    #@14
    .line 225
    :cond_14
    new-instance v0, Landroid/view/PointerIcon;

    #@16
    iget v1, p0, Landroid/view/PointerIcon;->mStyle:I

    #@18
    invoke-direct {v0, v1}, Landroid/view/PointerIcon;-><init>(I)V

    #@1b
    .line 226
    .local v0, result:Landroid/view/PointerIcon;
    iget v1, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@1d
    iput v1, v0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@1f
    .line 227
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@22
    move-result-object v1

    #@23
    iget v2, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@25
    invoke-direct {v0, v1, v2}, Landroid/view/PointerIcon;->loadResource(Landroid/content/res/Resources;I)V

    #@28
    goto :goto_13
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 339
    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 341
    iget v0, p0, Landroid/view/PointerIcon;->mStyle:I

    #@7
    if-eqz v0, :cond_21

    #@9
    .line 342
    iget v0, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 343
    iget v0, p0, Landroid/view/PointerIcon;->mSystemIconResourceId:I

    #@10
    if-nez v0, :cond_21

    #@12
    .line 344
    iget-object v0, p0, Landroid/view/PointerIcon;->mBitmap:Landroid/graphics/Bitmap;

    #@14
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 345
    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotX:F

    #@19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@1c
    .line 346
    iget v0, p0, Landroid/view/PointerIcon;->mHotSpotY:F

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@21
    .line 349
    :cond_21
    return-void
.end method
