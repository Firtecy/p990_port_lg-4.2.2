.class public final enum Landroid/view/ViewDebug$HierarchyTraceType;
.super Ljava/lang/Enum;
.source "ViewDebug.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewDebug;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HierarchyTraceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/view/ViewDebug$HierarchyTraceType;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum BUILD_CACHE:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum DRAW:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum INVALIDATE:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum INVALIDATE_CHILD:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum INVALIDATE_CHILD_IN_PARENT:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum ON_LAYOUT:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum ON_MEASURE:Landroid/view/ViewDebug$HierarchyTraceType;

.field public static final enum REQUEST_LAYOUT:Landroid/view/ViewDebug$HierarchyTraceType;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 310
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@7
    const-string v1, "INVALIDATE"

    #@9
    invoke-direct {v0, v1, v3}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->INVALIDATE:Landroid/view/ViewDebug$HierarchyTraceType;

    #@e
    .line 311
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@10
    const-string v1, "INVALIDATE_CHILD"

    #@12
    invoke-direct {v0, v1, v4}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->INVALIDATE_CHILD:Landroid/view/ViewDebug$HierarchyTraceType;

    #@17
    .line 312
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@19
    const-string v1, "INVALIDATE_CHILD_IN_PARENT"

    #@1b
    invoke-direct {v0, v1, v5}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->INVALIDATE_CHILD_IN_PARENT:Landroid/view/ViewDebug$HierarchyTraceType;

    #@20
    .line 313
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@22
    const-string v1, "REQUEST_LAYOUT"

    #@24
    invoke-direct {v0, v1, v6}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->REQUEST_LAYOUT:Landroid/view/ViewDebug$HierarchyTraceType;

    #@29
    .line 314
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@2b
    const-string v1, "ON_LAYOUT"

    #@2d
    invoke-direct {v0, v1, v7}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->ON_LAYOUT:Landroid/view/ViewDebug$HierarchyTraceType;

    #@32
    .line 315
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@34
    const-string v1, "ON_MEASURE"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->ON_MEASURE:Landroid/view/ViewDebug$HierarchyTraceType;

    #@3c
    .line 316
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@3e
    const-string v1, "DRAW"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->DRAW:Landroid/view/ViewDebug$HierarchyTraceType;

    #@46
    .line 317
    new-instance v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@48
    const-string v1, "BUILD_CACHE"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Landroid/view/ViewDebug$HierarchyTraceType;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->BUILD_CACHE:Landroid/view/ViewDebug$HierarchyTraceType;

    #@50
    .line 308
    const/16 v0, 0x8

    #@52
    new-array v0, v0, [Landroid/view/ViewDebug$HierarchyTraceType;

    #@54
    sget-object v1, Landroid/view/ViewDebug$HierarchyTraceType;->INVALIDATE:Landroid/view/ViewDebug$HierarchyTraceType;

    #@56
    aput-object v1, v0, v3

    #@58
    sget-object v1, Landroid/view/ViewDebug$HierarchyTraceType;->INVALIDATE_CHILD:Landroid/view/ViewDebug$HierarchyTraceType;

    #@5a
    aput-object v1, v0, v4

    #@5c
    sget-object v1, Landroid/view/ViewDebug$HierarchyTraceType;->INVALIDATE_CHILD_IN_PARENT:Landroid/view/ViewDebug$HierarchyTraceType;

    #@5e
    aput-object v1, v0, v5

    #@60
    sget-object v1, Landroid/view/ViewDebug$HierarchyTraceType;->REQUEST_LAYOUT:Landroid/view/ViewDebug$HierarchyTraceType;

    #@62
    aput-object v1, v0, v6

    #@64
    sget-object v1, Landroid/view/ViewDebug$HierarchyTraceType;->ON_LAYOUT:Landroid/view/ViewDebug$HierarchyTraceType;

    #@66
    aput-object v1, v0, v7

    #@68
    const/4 v1, 0x5

    #@69
    sget-object v2, Landroid/view/ViewDebug$HierarchyTraceType;->ON_MEASURE:Landroid/view/ViewDebug$HierarchyTraceType;

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/4 v1, 0x6

    #@6e
    sget-object v2, Landroid/view/ViewDebug$HierarchyTraceType;->DRAW:Landroid/view/ViewDebug$HierarchyTraceType;

    #@70
    aput-object v2, v0, v1

    #@72
    const/4 v1, 0x7

    #@73
    sget-object v2, Landroid/view/ViewDebug$HierarchyTraceType;->BUILD_CACHE:Landroid/view/ViewDebug$HierarchyTraceType;

    #@75
    aput-object v2, v0, v1

    #@77
    sput-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->$VALUES:[Landroid/view/ViewDebug$HierarchyTraceType;

    #@79
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 309
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/view/ViewDebug$HierarchyTraceType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 308
    const-class v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewDebug$HierarchyTraceType;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/view/ViewDebug$HierarchyTraceType;
    .registers 1

    #@0
    .prologue
    .line 308
    sget-object v0, Landroid/view/ViewDebug$HierarchyTraceType;->$VALUES:[Landroid/view/ViewDebug$HierarchyTraceType;

    #@2
    invoke-virtual {v0}, [Landroid/view/ViewDebug$HierarchyTraceType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/view/ViewDebug$HierarchyTraceType;

    #@8
    return-object v0
.end method
