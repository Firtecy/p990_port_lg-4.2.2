.class public abstract Landroid/view/IWindowManager$Stub;
.super Landroid/os/Binder;
.source "IWindowManager.java"

# interfaces
.implements Landroid/view/IWindowManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/IWindowManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.IWindowManager"

.field static final TRANSACTION_addAppToken:I = 0x10

.field static final TRANSACTION_addDisplayContentChangeListener:I = 0x47

.field static final TRANSACTION_addWindowToken:I = 0xe

.field static final TRANSACTION_clearForcedDisplayDensity:I = 0x9

.field static final TRANSACTION_clearForcedDisplaySize:I = 0x7

.field static final TRANSACTION_closeSystemDialogs:I = 0x2f

.field static final TRANSACTION_disableKeyguard:I = 0x28

.field static final TRANSACTION_dismissKeyguard:I = 0x2e

.field static final TRANSACTION_executeAppTransition:I = 0x1a

.field static final TRANSACTION_exitKeyguardSecurely:I = 0x2a

.field static final TRANSACTION_freezeRotation:I = 0x3b

.field static final TRANSACTION_freezeRotationUVS:I = 0x59

.field static final TRANSACTION_getAnimationScale:I = 0x30

.field static final TRANSACTION_getAnimationScales:I = 0x31

.field static final TRANSACTION_getAppOrientation:I = 0x13

.field static final TRANSACTION_getDsdrExtWidthHeight:I = 0x57

.field static final TRANSACTION_getDsdrStatus:I = 0x54

.field static final TRANSACTION_getFocusedWindowToken:I = 0x43

.field static final TRANSACTION_getKeycodeState:I = 0x53

.field static final TRANSACTION_getPendingAppTransition:I = 0x16

.field static final TRANSACTION_getPreferredOptionsPanelGravity:I = 0x3a

.field static final TRANSACTION_getRotation:I = 0x38

.field static final TRANSACTION_getViewBinder:I = 0x4e

.field static final TRANSACTION_getViewBinderCount:I = 0x50

.field static final TRANSACTION_getViewBinderTitle:I = 0x4f

.field static final TRANSACTION_getViewRootBinder:I = 0x4d

.field static final TRANSACTION_getVisibleStatus:I = 0x56

.field static final TRANSACTION_getVisibleWindowsForDisplay:I = 0x4a

.field static final TRANSACTION_getWindowCompatibilityScale:I = 0x44

.field static final TRANSACTION_getWindowInfo:I = 0x49

.field static final TRANSACTION_hasNavigationBar:I = 0x41

.field static final TRANSACTION_hasSystemNavBar:I = 0xa

.field static final TRANSACTION_inKeyguardRestrictedInputMode:I = 0x2d

.field static final TRANSACTION_inputMethodClientHasFocus:I = 0x5

.field static final TRANSACTION_isKeyguardLocked:I = 0x2b

.field static final TRANSACTION_isKeyguardSecure:I = 0x2c

.field static final TRANSACTION_isSafeModeEnabled:I = 0x4b

.field static final TRANSACTION_isViewServerRunning:I = 0x3

.field static final TRANSACTION_lockNow:I = 0x42

.field static final TRANSACTION_magnifyDisplay:I = 0x46

.field static final TRANSACTION_moveAppToken:I = 0x21

.field static final TRANSACTION_moveAppTokensToBottom:I = 0x23

.field static final TRANSACTION_moveAppTokensToTop:I = 0x22

.field static final TRANSACTION_moveWindowTokenToTop:I = 0x58

.field static final TRANSACTION_openSession:I = 0x4

.field static final TRANSACTION_overridePendingAppTransition:I = 0x17

.field static final TRANSACTION_overridePendingAppTransitionScaleUp:I = 0x18

.field static final TRANSACTION_overridePendingAppTransitionThumb:I = 0x19

.field static final TRANSACTION_pauseKeyDispatching:I = 0xb

.field static final TRANSACTION_prepareAppTransition:I = 0x15

.field static final TRANSACTION_reenableKeyguard:I = 0x29

.field static final TRANSACTION_registerInputMonitor:I = 0x51

.field static final TRANSACTION_removeAppToken:I = 0x20

.field static final TRANSACTION_removeDisplayContentChangeListener:I = 0x48

.field static final TRANSACTION_removeWindowToken:I = 0xf

.field static final TRANSACTION_resumeKeyDispatching:I = 0xc

.field static final TRANSACTION_screenshotApplications:I = 0x3d

.field static final TRANSACTION_screenshotApplicationsFull:I = 0x3e

.field static final TRANSACTION_sendSplitWindowFocusChanged:I = 0x5a

.field static final TRANSACTION_setAnimationScale:I = 0x32

.field static final TRANSACTION_setAnimationScales:I = 0x33

.field static final TRANSACTION_setAppGroupId:I = 0x11

.field static final TRANSACTION_setAppOrientation:I = 0x12

.field static final TRANSACTION_setAppStartingWindow:I = 0x1b

.field static final TRANSACTION_setAppVisibility:I = 0x1d

.field static final TRANSACTION_setAppWillBeHidden:I = 0x1c

.field static final TRANSACTION_setDsdrActivated:I = 0x55

.field static final TRANSACTION_setEventDispatching:I = 0xd

.field static final TRANSACTION_setFocusedApp:I = 0x14

.field static final TRANSACTION_setForcedDisplayDensity:I = 0x8

.field static final TRANSACTION_setForcedDisplaySize:I = 0x6

.field static final TRANSACTION_setInTouchMode:I = 0x34

.field static final TRANSACTION_setInputFilter:I = 0x45

.field static final TRANSACTION_setNewConfiguration:I = 0x25

.field static final TRANSACTION_setStrictModeVisualIndicatorPreference:I = 0x36

.field static final TRANSACTION_showAssistant:I = 0x4c

.field static final TRANSACTION_showStrictModeViolation:I = 0x35

.field static final TRANSACTION_startAppFreezingScreen:I = 0x1e

.field static final TRANSACTION_startFreezingScreen:I = 0x26

.field static final TRANSACTION_startViewServer:I = 0x1

.field static final TRANSACTION_statusBarVisibilityChanged:I = 0x3f

.field static final TRANSACTION_stopAppFreezingScreen:I = 0x1f

.field static final TRANSACTION_stopFreezingScreen:I = 0x27

.field static final TRANSACTION_stopViewServer:I = 0x2

.field static final TRANSACTION_thawRotation:I = 0x3c

.field static final TRANSACTION_unregisterInputMonitor:I = 0x52

.field static final TRANSACTION_updateOrientationFromAppTokens:I = 0x24

.field static final TRANSACTION_updateRotation:I = 0x37

.field static final TRANSACTION_updateSplitWindowLayout:I = 0x5b

.field static final TRANSACTION_waitForWindowDrawn:I = 0x40

.field static final TRANSACTION_watchRotation:I = 0x39


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.view.IWindowManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/IWindowManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.view.IWindowManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/IWindowManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/view/IWindowManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/view/IWindowManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/IWindowManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 20
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    sparse-switch p1, :sswitch_data_9f0

    #@3
    .line 1032
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 47
    :sswitch_8
    const-string v1, "android.view.IWindowManager"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 48
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 52
    :sswitch_11
    const-string v1, "android.view.IWindowManager"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 54
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 55
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->startViewServer(I)Z

    #@1f
    move-result v14

    #@20
    .line 56
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 57
    if-eqz v14, :cond_2d

    #@25
    const/4 v1, 0x1

    #@26
    :goto_26
    move-object/from16 v0, p3

    #@28
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 58
    const/4 v1, 0x1

    #@2c
    goto :goto_7

    #@2d
    .line 57
    :cond_2d
    const/4 v1, 0x0

    #@2e
    goto :goto_26

    #@2f
    .line 62
    .end local v2           #_arg0:I
    .end local v14           #_result:Z
    :sswitch_2f
    const-string v1, "android.view.IWindowManager"

    #@31
    move-object/from16 v0, p2

    #@33
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 63
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->stopViewServer()Z

    #@39
    move-result v14

    #@3a
    .line 64
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d
    .line 65
    if-eqz v14, :cond_47

    #@3f
    const/4 v1, 0x1

    #@40
    :goto_40
    move-object/from16 v0, p3

    #@42
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    .line 66
    const/4 v1, 0x1

    #@46
    goto :goto_7

    #@47
    .line 65
    :cond_47
    const/4 v1, 0x0

    #@48
    goto :goto_40

    #@49
    .line 70
    .end local v14           #_result:Z
    :sswitch_49
    const-string v1, "android.view.IWindowManager"

    #@4b
    move-object/from16 v0, p2

    #@4d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 71
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->isViewServerRunning()Z

    #@53
    move-result v14

    #@54
    .line 72
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@57
    .line 73
    if-eqz v14, :cond_61

    #@59
    const/4 v1, 0x1

    #@5a
    :goto_5a
    move-object/from16 v0, p3

    #@5c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    .line 74
    const/4 v1, 0x1

    #@60
    goto :goto_7

    #@61
    .line 73
    :cond_61
    const/4 v1, 0x0

    #@62
    goto :goto_5a

    #@63
    .line 78
    .end local v14           #_result:Z
    :sswitch_63
    const-string v1, "android.view.IWindowManager"

    #@65
    move-object/from16 v0, p2

    #@67
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6a
    .line 80
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@6d
    move-result-object v1

    #@6e
    invoke-static {v1}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@71
    move-result-object v2

    #@72
    .line 82
    .local v2, _arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@75
    move-result-object v1

    #@76
    invoke-static {v1}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@79
    move-result-object v3

    #@7a
    .line 83
    .local v3, _arg1:Lcom/android/internal/view/IInputContext;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->openSession(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;)Landroid/view/IWindowSession;

    #@7d
    move-result-object v14

    #@7e
    .line 84
    .local v14, _result:Landroid/view/IWindowSession;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@81
    .line 85
    if-eqz v14, :cond_8f

    #@83
    invoke-interface {v14}, Landroid/view/IWindowSession;->asBinder()Landroid/os/IBinder;

    #@86
    move-result-object v1

    #@87
    :goto_87
    move-object/from16 v0, p3

    #@89
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@8c
    .line 86
    const/4 v1, 0x1

    #@8d
    goto/16 :goto_7

    #@8f
    .line 85
    :cond_8f
    const/4 v1, 0x0

    #@90
    goto :goto_87

    #@91
    .line 90
    .end local v2           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v3           #_arg1:Lcom/android/internal/view/IInputContext;
    .end local v14           #_result:Landroid/view/IWindowSession;
    :sswitch_91
    const-string v1, "android.view.IWindowManager"

    #@93
    move-object/from16 v0, p2

    #@95
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@98
    .line 92
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9b
    move-result-object v1

    #@9c
    invoke-static {v1}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@9f
    move-result-object v2

    #@a0
    .line 93
    .restart local v2       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->inputMethodClientHasFocus(Lcom/android/internal/view/IInputMethodClient;)Z

    #@a3
    move-result v14

    #@a4
    .line 94
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a7
    .line 95
    if-eqz v14, :cond_b2

    #@a9
    const/4 v1, 0x1

    #@aa
    :goto_aa
    move-object/from16 v0, p3

    #@ac
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@af
    .line 96
    const/4 v1, 0x1

    #@b0
    goto/16 :goto_7

    #@b2
    .line 95
    :cond_b2
    const/4 v1, 0x0

    #@b3
    goto :goto_aa

    #@b4
    .line 100
    .end local v2           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v14           #_result:Z
    :sswitch_b4
    const-string v1, "android.view.IWindowManager"

    #@b6
    move-object/from16 v0, p2

    #@b8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bb
    .line 102
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@be
    move-result v2

    #@bf
    .line 104
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c2
    move-result v3

    #@c3
    .line 106
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c6
    move-result v4

    #@c7
    .line 107
    .local v4, _arg2:I
    invoke-virtual {p0, v2, v3, v4}, Landroid/view/IWindowManager$Stub;->setForcedDisplaySize(III)V

    #@ca
    .line 108
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cd
    .line 109
    const/4 v1, 0x1

    #@ce
    goto/16 :goto_7

    #@d0
    .line 113
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :sswitch_d0
    const-string v1, "android.view.IWindowManager"

    #@d2
    move-object/from16 v0, p2

    #@d4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d7
    .line 115
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@da
    move-result v2

    #@db
    .line 116
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->clearForcedDisplaySize(I)V

    #@de
    .line 117
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e1
    .line 118
    const/4 v1, 0x1

    #@e2
    goto/16 :goto_7

    #@e4
    .line 122
    .end local v2           #_arg0:I
    :sswitch_e4
    const-string v1, "android.view.IWindowManager"

    #@e6
    move-object/from16 v0, p2

    #@e8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@eb
    .line 124
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ee
    move-result v2

    #@ef
    .line 126
    .restart local v2       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f2
    move-result v3

    #@f3
    .line 127
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->setForcedDisplayDensity(II)V

    #@f6
    .line 128
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f9
    .line 129
    const/4 v1, 0x1

    #@fa
    goto/16 :goto_7

    #@fc
    .line 133
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    :sswitch_fc
    const-string v1, "android.view.IWindowManager"

    #@fe
    move-object/from16 v0, p2

    #@100
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@103
    .line 135
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v2

    #@107
    .line 136
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->clearForcedDisplayDensity(I)V

    #@10a
    .line 137
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10d
    .line 138
    const/4 v1, 0x1

    #@10e
    goto/16 :goto_7

    #@110
    .line 142
    .end local v2           #_arg0:I
    :sswitch_110
    const-string v1, "android.view.IWindowManager"

    #@112
    move-object/from16 v0, p2

    #@114
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@117
    .line 143
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->hasSystemNavBar()Z

    #@11a
    move-result v14

    #@11b
    .line 144
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@11e
    .line 145
    if-eqz v14, :cond_129

    #@120
    const/4 v1, 0x1

    #@121
    :goto_121
    move-object/from16 v0, p3

    #@123
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@126
    .line 146
    const/4 v1, 0x1

    #@127
    goto/16 :goto_7

    #@129
    .line 145
    :cond_129
    const/4 v1, 0x0

    #@12a
    goto :goto_121

    #@12b
    .line 150
    .end local v14           #_result:Z
    :sswitch_12b
    const-string v1, "android.view.IWindowManager"

    #@12d
    move-object/from16 v0, p2

    #@12f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@132
    .line 152
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@135
    move-result-object v2

    #@136
    .line 153
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->pauseKeyDispatching(Landroid/os/IBinder;)V

    #@139
    .line 154
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@13c
    .line 155
    const/4 v1, 0x1

    #@13d
    goto/16 :goto_7

    #@13f
    .line 159
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_13f
    const-string v1, "android.view.IWindowManager"

    #@141
    move-object/from16 v0, p2

    #@143
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@146
    .line 161
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@149
    move-result-object v2

    #@14a
    .line 162
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->resumeKeyDispatching(Landroid/os/IBinder;)V

    #@14d
    .line 163
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@150
    .line 164
    const/4 v1, 0x1

    #@151
    goto/16 :goto_7

    #@153
    .line 168
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_153
    const-string v1, "android.view.IWindowManager"

    #@155
    move-object/from16 v0, p2

    #@157
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15a
    .line 170
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@15d
    move-result v1

    #@15e
    if-eqz v1, :cond_16a

    #@160
    const/4 v2, 0x1

    #@161
    .line 171
    .local v2, _arg0:Z
    :goto_161
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setEventDispatching(Z)V

    #@164
    .line 172
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@167
    .line 173
    const/4 v1, 0x1

    #@168
    goto/16 :goto_7

    #@16a
    .line 170
    .end local v2           #_arg0:Z
    :cond_16a
    const/4 v2, 0x0

    #@16b
    goto :goto_161

    #@16c
    .line 177
    :sswitch_16c
    const-string v1, "android.view.IWindowManager"

    #@16e
    move-object/from16 v0, p2

    #@170
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173
    .line 179
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@176
    move-result-object v2

    #@177
    .line 181
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@17a
    move-result v3

    #@17b
    .line 182
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->addWindowToken(Landroid/os/IBinder;I)V

    #@17e
    .line 183
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@181
    .line 184
    const/4 v1, 0x1

    #@182
    goto/16 :goto_7

    #@184
    .line 188
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    :sswitch_184
    const-string v1, "android.view.IWindowManager"

    #@186
    move-object/from16 v0, p2

    #@188
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18b
    .line 190
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18e
    move-result-object v2

    #@18f
    .line 191
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->removeWindowToken(Landroid/os/IBinder;)V

    #@192
    .line 192
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@195
    .line 193
    const/4 v1, 0x1

    #@196
    goto/16 :goto_7

    #@198
    .line 197
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_198
    const-string v1, "android.view.IWindowManager"

    #@19a
    move-object/from16 v0, p2

    #@19c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19f
    .line 199
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a2
    move-result v2

    #@1a3
    .line 201
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a6
    move-result v3

    #@1a7
    .line 203
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1aa
    move-result-object v1

    #@1ab
    invoke-static {v1}, Landroid/view/IApplicationToken$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IApplicationToken;

    #@1ae
    move-result-object v4

    #@1af
    .line 205
    .local v4, _arg2:Landroid/view/IApplicationToken;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b2
    move-result v5

    #@1b3
    .line 207
    .local v5, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b6
    move-result v6

    #@1b7
    .line 209
    .local v6, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ba
    move-result v1

    #@1bb
    if-eqz v1, :cond_1cf

    #@1bd
    const/4 v7, 0x1

    #@1be
    .line 211
    .local v7, _arg5:Z
    :goto_1be
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c1
    move-result v1

    #@1c2
    if-eqz v1, :cond_1d1

    #@1c4
    const/4 v8, 0x1

    #@1c5
    .local v8, _arg6:Z
    :goto_1c5
    move-object v1, p0

    #@1c6
    .line 212
    invoke-virtual/range {v1 .. v8}, Landroid/view/IWindowManager$Stub;->addAppToken(IILandroid/view/IApplicationToken;IIZZ)V

    #@1c9
    .line 213
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1cc
    .line 214
    const/4 v1, 0x1

    #@1cd
    goto/16 :goto_7

    #@1cf
    .line 209
    .end local v7           #_arg5:Z
    .end local v8           #_arg6:Z
    :cond_1cf
    const/4 v7, 0x0

    #@1d0
    goto :goto_1be

    #@1d1
    .line 211
    .restart local v7       #_arg5:Z
    :cond_1d1
    const/4 v8, 0x0

    #@1d2
    goto :goto_1c5

    #@1d3
    .line 218
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/view/IApplicationToken;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:Z
    :sswitch_1d3
    const-string v1, "android.view.IWindowManager"

    #@1d5
    move-object/from16 v0, p2

    #@1d7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1da
    .line 220
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1dd
    move-result-object v2

    #@1de
    .line 222
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e1
    move-result v3

    #@1e2
    .line 223
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->setAppGroupId(Landroid/os/IBinder;I)V

    #@1e5
    .line 224
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e8
    .line 225
    const/4 v1, 0x1

    #@1e9
    goto/16 :goto_7

    #@1eb
    .line 229
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    :sswitch_1eb
    const-string v1, "android.view.IWindowManager"

    #@1ed
    move-object/from16 v0, p2

    #@1ef
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f2
    .line 231
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1f5
    move-result-object v1

    #@1f6
    invoke-static {v1}, Landroid/view/IApplicationToken$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IApplicationToken;

    #@1f9
    move-result-object v2

    #@1fa
    .line 233
    .local v2, _arg0:Landroid/view/IApplicationToken;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1fd
    move-result v3

    #@1fe
    .line 234
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->setAppOrientation(Landroid/view/IApplicationToken;I)V

    #@201
    .line 235
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@204
    .line 236
    const/4 v1, 0x1

    #@205
    goto/16 :goto_7

    #@207
    .line 240
    .end local v2           #_arg0:Landroid/view/IApplicationToken;
    .end local v3           #_arg1:I
    :sswitch_207
    const-string v1, "android.view.IWindowManager"

    #@209
    move-object/from16 v0, p2

    #@20b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20e
    .line 242
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@211
    move-result-object v1

    #@212
    invoke-static {v1}, Landroid/view/IApplicationToken$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IApplicationToken;

    #@215
    move-result-object v2

    #@216
    .line 243
    .restart local v2       #_arg0:Landroid/view/IApplicationToken;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getAppOrientation(Landroid/view/IApplicationToken;)I

    #@219
    move-result v14

    #@21a
    .line 244
    .local v14, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@21d
    .line 245
    move-object/from16 v0, p3

    #@21f
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@222
    .line 246
    const/4 v1, 0x1

    #@223
    goto/16 :goto_7

    #@225
    .line 250
    .end local v2           #_arg0:Landroid/view/IApplicationToken;
    .end local v14           #_result:I
    :sswitch_225
    const-string v1, "android.view.IWindowManager"

    #@227
    move-object/from16 v0, p2

    #@229
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22c
    .line 252
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@22f
    move-result-object v2

    #@230
    .line 254
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@233
    move-result v1

    #@234
    if-eqz v1, :cond_240

    #@236
    const/4 v3, 0x1

    #@237
    .line 255
    .local v3, _arg1:Z
    :goto_237
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->setFocusedApp(Landroid/os/IBinder;Z)V

    #@23a
    .line 256
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@23d
    .line 257
    const/4 v1, 0x1

    #@23e
    goto/16 :goto_7

    #@240
    .line 254
    .end local v3           #_arg1:Z
    :cond_240
    const/4 v3, 0x0

    #@241
    goto :goto_237

    #@242
    .line 261
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_242
    const-string v1, "android.view.IWindowManager"

    #@244
    move-object/from16 v0, p2

    #@246
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@249
    .line 263
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@24c
    move-result v2

    #@24d
    .line 265
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@250
    move-result v1

    #@251
    if-eqz v1, :cond_25d

    #@253
    const/4 v3, 0x1

    #@254
    .line 266
    .restart local v3       #_arg1:Z
    :goto_254
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->prepareAppTransition(IZ)V

    #@257
    .line 267
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@25a
    .line 268
    const/4 v1, 0x1

    #@25b
    goto/16 :goto_7

    #@25d
    .line 265
    .end local v3           #_arg1:Z
    :cond_25d
    const/4 v3, 0x0

    #@25e
    goto :goto_254

    #@25f
    .line 272
    .end local v2           #_arg0:I
    :sswitch_25f
    const-string v1, "android.view.IWindowManager"

    #@261
    move-object/from16 v0, p2

    #@263
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@266
    .line 273
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getPendingAppTransition()I

    #@269
    move-result v14

    #@26a
    .line 274
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@26d
    .line 275
    move-object/from16 v0, p3

    #@26f
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@272
    .line 276
    const/4 v1, 0x1

    #@273
    goto/16 :goto_7

    #@275
    .line 280
    .end local v14           #_result:I
    :sswitch_275
    const-string v1, "android.view.IWindowManager"

    #@277
    move-object/from16 v0, p2

    #@279
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27c
    .line 282
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27f
    move-result-object v2

    #@280
    .line 284
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@283
    move-result v3

    #@284
    .line 286
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@287
    move-result v4

    #@288
    .line 288
    .local v4, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@28b
    move-result-object v1

    #@28c
    invoke-static {v1}, Landroid/os/IRemoteCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IRemoteCallback;

    #@28f
    move-result-object v5

    #@290
    .line 289
    .local v5, _arg3:Landroid/os/IRemoteCallback;
    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/view/IWindowManager$Stub;->overridePendingAppTransition(Ljava/lang/String;IILandroid/os/IRemoteCallback;)V

    #@293
    .line 290
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@296
    .line 291
    const/4 v1, 0x1

    #@297
    goto/16 :goto_7

    #@299
    .line 295
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/IRemoteCallback;
    :sswitch_299
    const-string v1, "android.view.IWindowManager"

    #@29b
    move-object/from16 v0, p2

    #@29d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a0
    .line 297
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2a3
    move-result v2

    #@2a4
    .line 299
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2a7
    move-result v3

    #@2a8
    .line 301
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2ab
    move-result v4

    #@2ac
    .line 303
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2af
    move-result v5

    #@2b0
    .line 304
    .local v5, _arg3:I
    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/view/IWindowManager$Stub;->overridePendingAppTransitionScaleUp(IIII)V

    #@2b3
    .line 305
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b6
    .line 306
    const/4 v1, 0x1

    #@2b7
    goto/16 :goto_7

    #@2b9
    .line 310
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    :sswitch_2b9
    const-string v1, "android.view.IWindowManager"

    #@2bb
    move-object/from16 v0, p2

    #@2bd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c0
    .line 312
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2c3
    move-result v1

    #@2c4
    if-eqz v1, :cond_2f1

    #@2c6
    .line 313
    sget-object v1, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c8
    move-object/from16 v0, p2

    #@2ca
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2cd
    move-result-object v2

    #@2ce
    check-cast v2, Landroid/graphics/Bitmap;

    #@2d0
    .line 319
    .local v2, _arg0:Landroid/graphics/Bitmap;
    :goto_2d0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2d3
    move-result v3

    #@2d4
    .line 321
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2d7
    move-result v4

    #@2d8
    .line 323
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2db
    move-result-object v1

    #@2dc
    invoke-static {v1}, Landroid/os/IRemoteCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IRemoteCallback;

    #@2df
    move-result-object v5

    #@2e0
    .line 325
    .local v5, _arg3:Landroid/os/IRemoteCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2e3
    move-result v1

    #@2e4
    if-eqz v1, :cond_2f3

    #@2e6
    const/4 v6, 0x1

    #@2e7
    .local v6, _arg4:Z
    :goto_2e7
    move-object v1, p0

    #@2e8
    .line 326
    invoke-virtual/range {v1 .. v6}, Landroid/view/IWindowManager$Stub;->overridePendingAppTransitionThumb(Landroid/graphics/Bitmap;IILandroid/os/IRemoteCallback;Z)V

    #@2eb
    .line 327
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ee
    .line 328
    const/4 v1, 0x1

    #@2ef
    goto/16 :goto_7

    #@2f1
    .line 316
    .end local v2           #_arg0:Landroid/graphics/Bitmap;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/IRemoteCallback;
    .end local v6           #_arg4:Z
    :cond_2f1
    const/4 v2, 0x0

    #@2f2
    .restart local v2       #_arg0:Landroid/graphics/Bitmap;
    goto :goto_2d0

    #@2f3
    .line 325
    .restart local v3       #_arg1:I
    .restart local v4       #_arg2:I
    .restart local v5       #_arg3:Landroid/os/IRemoteCallback;
    :cond_2f3
    const/4 v6, 0x0

    #@2f4
    goto :goto_2e7

    #@2f5
    .line 332
    .end local v2           #_arg0:Landroid/graphics/Bitmap;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/IRemoteCallback;
    :sswitch_2f5
    const-string v1, "android.view.IWindowManager"

    #@2f7
    move-object/from16 v0, p2

    #@2f9
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2fc
    .line 333
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->executeAppTransition()V

    #@2ff
    .line 334
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@302
    .line 335
    const/4 v1, 0x1

    #@303
    goto/16 :goto_7

    #@305
    .line 339
    :sswitch_305
    const-string v1, "android.view.IWindowManager"

    #@307
    move-object/from16 v0, p2

    #@309
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30c
    .line 341
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@30f
    move-result-object v2

    #@310
    .line 343
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@313
    move-result-object v3

    #@314
    .line 345
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@317
    move-result v4

    #@318
    .line 347
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@31b
    move-result v1

    #@31c
    if-eqz v1, :cond_359

    #@31e
    .line 348
    sget-object v1, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@320
    move-object/from16 v0, p2

    #@322
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@325
    move-result-object v5

    #@326
    check-cast v5, Landroid/content/res/CompatibilityInfo;

    #@328
    .line 354
    .local v5, _arg3:Landroid/content/res/CompatibilityInfo;
    :goto_328
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@32b
    move-result v1

    #@32c
    if-eqz v1, :cond_35b

    #@32e
    .line 355
    sget-object v1, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@330
    move-object/from16 v0, p2

    #@332
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@335
    move-result-object v6

    #@336
    check-cast v6, Ljava/lang/CharSequence;

    #@338
    .line 361
    .local v6, _arg4:Ljava/lang/CharSequence;
    :goto_338
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@33b
    move-result v7

    #@33c
    .line 363
    .local v7, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@33f
    move-result v8

    #@340
    .line 365
    .local v8, _arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@343
    move-result v9

    #@344
    .line 367
    .local v9, _arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@347
    move-result-object v10

    #@348
    .line 369
    .local v10, _arg8:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34b
    move-result v1

    #@34c
    if-eqz v1, :cond_35d

    #@34e
    const/4 v11, 0x1

    #@34f
    .local v11, _arg9:Z
    :goto_34f
    move-object v1, p0

    #@350
    .line 370
    invoke-virtual/range {v1 .. v11}, Landroid/view/IWindowManager$Stub;->setAppStartingWindow(Landroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;IIILandroid/os/IBinder;Z)V

    #@353
    .line 371
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@356
    .line 372
    const/4 v1, 0x1

    #@357
    goto/16 :goto_7

    #@359
    .line 351
    .end local v5           #_arg3:Landroid/content/res/CompatibilityInfo;
    .end local v6           #_arg4:Ljava/lang/CharSequence;
    .end local v7           #_arg5:I
    .end local v8           #_arg6:I
    .end local v9           #_arg7:I
    .end local v10           #_arg8:Landroid/os/IBinder;
    .end local v11           #_arg9:Z
    :cond_359
    const/4 v5, 0x0

    #@35a
    .restart local v5       #_arg3:Landroid/content/res/CompatibilityInfo;
    goto :goto_328

    #@35b
    .line 358
    :cond_35b
    const/4 v6, 0x0

    #@35c
    .restart local v6       #_arg4:Ljava/lang/CharSequence;
    goto :goto_338

    #@35d
    .line 369
    .restart local v7       #_arg5:I
    .restart local v8       #_arg6:I
    .restart local v9       #_arg7:I
    .restart local v10       #_arg8:Landroid/os/IBinder;
    :cond_35d
    const/4 v11, 0x0

    #@35e
    goto :goto_34f

    #@35f
    .line 376
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/content/res/CompatibilityInfo;
    .end local v6           #_arg4:Ljava/lang/CharSequence;
    .end local v7           #_arg5:I
    .end local v8           #_arg6:I
    .end local v9           #_arg7:I
    .end local v10           #_arg8:Landroid/os/IBinder;
    :sswitch_35f
    const-string v1, "android.view.IWindowManager"

    #@361
    move-object/from16 v0, p2

    #@363
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@366
    .line 378
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@369
    move-result-object v2

    #@36a
    .line 379
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setAppWillBeHidden(Landroid/os/IBinder;)V

    #@36d
    .line 380
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@370
    .line 381
    const/4 v1, 0x1

    #@371
    goto/16 :goto_7

    #@373
    .line 385
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_373
    const-string v1, "android.view.IWindowManager"

    #@375
    move-object/from16 v0, p2

    #@377
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@37a
    .line 387
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@37d
    move-result-object v2

    #@37e
    .line 389
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@381
    move-result v1

    #@382
    if-eqz v1, :cond_38e

    #@384
    const/4 v3, 0x1

    #@385
    .line 390
    .local v3, _arg1:Z
    :goto_385
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->setAppVisibility(Landroid/os/IBinder;Z)V

    #@388
    .line 391
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@38b
    .line 392
    const/4 v1, 0x1

    #@38c
    goto/16 :goto_7

    #@38e
    .line 389
    .end local v3           #_arg1:Z
    :cond_38e
    const/4 v3, 0x0

    #@38f
    goto :goto_385

    #@390
    .line 396
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_390
    const-string v1, "android.view.IWindowManager"

    #@392
    move-object/from16 v0, p2

    #@394
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@397
    .line 398
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@39a
    move-result-object v2

    #@39b
    .line 400
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@39e
    move-result v3

    #@39f
    .line 401
    .local v3, _arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->startAppFreezingScreen(Landroid/os/IBinder;I)V

    #@3a2
    .line 402
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a5
    .line 403
    const/4 v1, 0x1

    #@3a6
    goto/16 :goto_7

    #@3a8
    .line 407
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    :sswitch_3a8
    const-string v1, "android.view.IWindowManager"

    #@3aa
    move-object/from16 v0, p2

    #@3ac
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3af
    .line 409
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3b2
    move-result-object v2

    #@3b3
    .line 411
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3b6
    move-result v1

    #@3b7
    if-eqz v1, :cond_3c3

    #@3b9
    const/4 v3, 0x1

    #@3ba
    .line 412
    .local v3, _arg1:Z
    :goto_3ba
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->stopAppFreezingScreen(Landroid/os/IBinder;Z)V

    #@3bd
    .line 413
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c0
    .line 414
    const/4 v1, 0x1

    #@3c1
    goto/16 :goto_7

    #@3c3
    .line 411
    .end local v3           #_arg1:Z
    :cond_3c3
    const/4 v3, 0x0

    #@3c4
    goto :goto_3ba

    #@3c5
    .line 418
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_3c5
    const-string v1, "android.view.IWindowManager"

    #@3c7
    move-object/from16 v0, p2

    #@3c9
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3cc
    .line 420
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3cf
    move-result-object v2

    #@3d0
    .line 421
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->removeAppToken(Landroid/os/IBinder;)V

    #@3d3
    .line 422
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d6
    .line 423
    const/4 v1, 0x1

    #@3d7
    goto/16 :goto_7

    #@3d9
    .line 427
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_3d9
    const-string v1, "android.view.IWindowManager"

    #@3db
    move-object/from16 v0, p2

    #@3dd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e0
    .line 429
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3e3
    move-result v2

    #@3e4
    .line 431
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3e7
    move-result-object v3

    #@3e8
    .line 432
    .local v3, _arg1:Landroid/os/IBinder;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->moveAppToken(ILandroid/os/IBinder;)V

    #@3eb
    .line 433
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3ee
    .line 434
    const/4 v1, 0x1

    #@3ef
    goto/16 :goto_7

    #@3f1
    .line 438
    .end local v2           #_arg0:I
    .end local v3           #_arg1:Landroid/os/IBinder;
    :sswitch_3f1
    const-string v1, "android.view.IWindowManager"

    #@3f3
    move-object/from16 v0, p2

    #@3f5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3f8
    .line 440
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createBinderArrayList()Ljava/util/ArrayList;

    #@3fb
    move-result-object v12

    #@3fc
    .line 441
    .local v12, _arg0:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    invoke-virtual {p0, v12}, Landroid/view/IWindowManager$Stub;->moveAppTokensToTop(Ljava/util/List;)V

    #@3ff
    .line 442
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@402
    .line 443
    const/4 v1, 0x1

    #@403
    goto/16 :goto_7

    #@405
    .line 447
    .end local v12           #_arg0:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    :sswitch_405
    const-string v1, "android.view.IWindowManager"

    #@407
    move-object/from16 v0, p2

    #@409
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40c
    .line 449
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createBinderArrayList()Ljava/util/ArrayList;

    #@40f
    move-result-object v12

    #@410
    .line 450
    .restart local v12       #_arg0:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    invoke-virtual {p0, v12}, Landroid/view/IWindowManager$Stub;->moveAppTokensToBottom(Ljava/util/List;)V

    #@413
    .line 451
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@416
    .line 452
    const/4 v1, 0x1

    #@417
    goto/16 :goto_7

    #@419
    .line 456
    .end local v12           #_arg0:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    :sswitch_419
    const-string v1, "android.view.IWindowManager"

    #@41b
    move-object/from16 v0, p2

    #@41d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@420
    .line 458
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@423
    move-result v1

    #@424
    if-eqz v1, :cond_44c

    #@426
    .line 459
    sget-object v1, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@428
    move-object/from16 v0, p2

    #@42a
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@42d
    move-result-object v2

    #@42e
    check-cast v2, Landroid/content/res/Configuration;

    #@430
    .line 465
    .local v2, _arg0:Landroid/content/res/Configuration;
    :goto_430
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@433
    move-result-object v3

    #@434
    .line 466
    .restart local v3       #_arg1:Landroid/os/IBinder;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->updateOrientationFromAppTokens(Landroid/content/res/Configuration;Landroid/os/IBinder;)Landroid/content/res/Configuration;

    #@437
    move-result-object v14

    #@438
    .line 467
    .local v14, _result:Landroid/content/res/Configuration;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@43b
    .line 468
    if-eqz v14, :cond_44e

    #@43d
    .line 469
    const/4 v1, 0x1

    #@43e
    move-object/from16 v0, p3

    #@440
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@443
    .line 470
    const/4 v1, 0x1

    #@444
    move-object/from16 v0, p3

    #@446
    invoke-virtual {v14, v0, v1}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@449
    .line 475
    :goto_449
    const/4 v1, 0x1

    #@44a
    goto/16 :goto_7

    #@44c
    .line 462
    .end local v2           #_arg0:Landroid/content/res/Configuration;
    .end local v3           #_arg1:Landroid/os/IBinder;
    .end local v14           #_result:Landroid/content/res/Configuration;
    :cond_44c
    const/4 v2, 0x0

    #@44d
    .restart local v2       #_arg0:Landroid/content/res/Configuration;
    goto :goto_430

    #@44e
    .line 473
    .restart local v3       #_arg1:Landroid/os/IBinder;
    .restart local v14       #_result:Landroid/content/res/Configuration;
    :cond_44e
    const/4 v1, 0x0

    #@44f
    move-object/from16 v0, p3

    #@451
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@454
    goto :goto_449

    #@455
    .line 479
    .end local v2           #_arg0:Landroid/content/res/Configuration;
    .end local v3           #_arg1:Landroid/os/IBinder;
    .end local v14           #_result:Landroid/content/res/Configuration;
    :sswitch_455
    const-string v1, "android.view.IWindowManager"

    #@457
    move-object/from16 v0, p2

    #@459
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45c
    .line 481
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@45f
    move-result v1

    #@460
    if-eqz v1, :cond_475

    #@462
    .line 482
    sget-object v1, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@464
    move-object/from16 v0, p2

    #@466
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@469
    move-result-object v2

    #@46a
    check-cast v2, Landroid/content/res/Configuration;

    #@46c
    .line 487
    .restart local v2       #_arg0:Landroid/content/res/Configuration;
    :goto_46c
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setNewConfiguration(Landroid/content/res/Configuration;)V

    #@46f
    .line 488
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@472
    .line 489
    const/4 v1, 0x1

    #@473
    goto/16 :goto_7

    #@475
    .line 485
    .end local v2           #_arg0:Landroid/content/res/Configuration;
    :cond_475
    const/4 v2, 0x0

    #@476
    .restart local v2       #_arg0:Landroid/content/res/Configuration;
    goto :goto_46c

    #@477
    .line 493
    .end local v2           #_arg0:Landroid/content/res/Configuration;
    :sswitch_477
    const-string v1, "android.view.IWindowManager"

    #@479
    move-object/from16 v0, p2

    #@47b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@47e
    .line 495
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@481
    move-result v2

    #@482
    .line 497
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@485
    move-result v3

    #@486
    .line 498
    .local v3, _arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->startFreezingScreen(II)V

    #@489
    .line 499
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@48c
    .line 500
    const/4 v1, 0x1

    #@48d
    goto/16 :goto_7

    #@48f
    .line 504
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    :sswitch_48f
    const-string v1, "android.view.IWindowManager"

    #@491
    move-object/from16 v0, p2

    #@493
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@496
    .line 505
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->stopFreezingScreen()V

    #@499
    .line 506
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@49c
    .line 507
    const/4 v1, 0x1

    #@49d
    goto/16 :goto_7

    #@49f
    .line 511
    :sswitch_49f
    const-string v1, "android.view.IWindowManager"

    #@4a1
    move-object/from16 v0, p2

    #@4a3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4a6
    .line 513
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4a9
    move-result-object v2

    #@4aa
    .line 515
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4ad
    move-result-object v3

    #@4ae
    .line 516
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->disableKeyguard(Landroid/os/IBinder;Ljava/lang/String;)V

    #@4b1
    .line 517
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b4
    .line 518
    const/4 v1, 0x1

    #@4b5
    goto/16 :goto_7

    #@4b7
    .line 522
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Ljava/lang/String;
    :sswitch_4b7
    const-string v1, "android.view.IWindowManager"

    #@4b9
    move-object/from16 v0, p2

    #@4bb
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4be
    .line 524
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4c1
    move-result-object v2

    #@4c2
    .line 525
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->reenableKeyguard(Landroid/os/IBinder;)V

    #@4c5
    .line 526
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c8
    .line 527
    const/4 v1, 0x1

    #@4c9
    goto/16 :goto_7

    #@4cb
    .line 531
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_4cb
    const-string v1, "android.view.IWindowManager"

    #@4cd
    move-object/from16 v0, p2

    #@4cf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d2
    .line 533
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4d5
    move-result-object v1

    #@4d6
    invoke-static {v1}, Landroid/view/IOnKeyguardExitResult$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IOnKeyguardExitResult;

    #@4d9
    move-result-object v2

    #@4da
    .line 534
    .local v2, _arg0:Landroid/view/IOnKeyguardExitResult;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->exitKeyguardSecurely(Landroid/view/IOnKeyguardExitResult;)V

    #@4dd
    .line 535
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e0
    .line 536
    const/4 v1, 0x1

    #@4e1
    goto/16 :goto_7

    #@4e3
    .line 540
    .end local v2           #_arg0:Landroid/view/IOnKeyguardExitResult;
    :sswitch_4e3
    const-string v1, "android.view.IWindowManager"

    #@4e5
    move-object/from16 v0, p2

    #@4e7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ea
    .line 541
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->isKeyguardLocked()Z

    #@4ed
    move-result v14

    #@4ee
    .line 542
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f1
    .line 543
    if-eqz v14, :cond_4fc

    #@4f3
    const/4 v1, 0x1

    #@4f4
    :goto_4f4
    move-object/from16 v0, p3

    #@4f6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4f9
    .line 544
    const/4 v1, 0x1

    #@4fa
    goto/16 :goto_7

    #@4fc
    .line 543
    :cond_4fc
    const/4 v1, 0x0

    #@4fd
    goto :goto_4f4

    #@4fe
    .line 548
    .end local v14           #_result:Z
    :sswitch_4fe
    const-string v1, "android.view.IWindowManager"

    #@500
    move-object/from16 v0, p2

    #@502
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@505
    .line 549
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->isKeyguardSecure()Z

    #@508
    move-result v14

    #@509
    .line 550
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@50c
    .line 551
    if-eqz v14, :cond_517

    #@50e
    const/4 v1, 0x1

    #@50f
    :goto_50f
    move-object/from16 v0, p3

    #@511
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@514
    .line 552
    const/4 v1, 0x1

    #@515
    goto/16 :goto_7

    #@517
    .line 551
    :cond_517
    const/4 v1, 0x0

    #@518
    goto :goto_50f

    #@519
    .line 556
    .end local v14           #_result:Z
    :sswitch_519
    const-string v1, "android.view.IWindowManager"

    #@51b
    move-object/from16 v0, p2

    #@51d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@520
    .line 557
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->inKeyguardRestrictedInputMode()Z

    #@523
    move-result v14

    #@524
    .line 558
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@527
    .line 559
    if-eqz v14, :cond_532

    #@529
    const/4 v1, 0x1

    #@52a
    :goto_52a
    move-object/from16 v0, p3

    #@52c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@52f
    .line 560
    const/4 v1, 0x1

    #@530
    goto/16 :goto_7

    #@532
    .line 559
    :cond_532
    const/4 v1, 0x0

    #@533
    goto :goto_52a

    #@534
    .line 564
    .end local v14           #_result:Z
    :sswitch_534
    const-string v1, "android.view.IWindowManager"

    #@536
    move-object/from16 v0, p2

    #@538
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53b
    .line 565
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->dismissKeyguard()V

    #@53e
    .line 566
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@541
    .line 567
    const/4 v1, 0x1

    #@542
    goto/16 :goto_7

    #@544
    .line 571
    :sswitch_544
    const-string v1, "android.view.IWindowManager"

    #@546
    move-object/from16 v0, p2

    #@548
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54b
    .line 573
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@54e
    move-result-object v2

    #@54f
    .line 574
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->closeSystemDialogs(Ljava/lang/String;)V

    #@552
    .line 575
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@555
    .line 576
    const/4 v1, 0x1

    #@556
    goto/16 :goto_7

    #@558
    .line 580
    .end local v2           #_arg0:Ljava/lang/String;
    :sswitch_558
    const-string v1, "android.view.IWindowManager"

    #@55a
    move-object/from16 v0, p2

    #@55c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55f
    .line 582
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@562
    move-result v2

    #@563
    .line 583
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getAnimationScale(I)F

    #@566
    move-result v14

    #@567
    .line 584
    .local v14, _result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@56a
    .line 585
    move-object/from16 v0, p3

    #@56c
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeFloat(F)V

    #@56f
    .line 586
    const/4 v1, 0x1

    #@570
    goto/16 :goto_7

    #@572
    .line 590
    .end local v2           #_arg0:I
    .end local v14           #_result:F
    :sswitch_572
    const-string v1, "android.view.IWindowManager"

    #@574
    move-object/from16 v0, p2

    #@576
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@579
    .line 591
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getAnimationScales()[F

    #@57c
    move-result-object v14

    #@57d
    .line 592
    .local v14, _result:[F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@580
    .line 593
    move-object/from16 v0, p3

    #@582
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeFloatArray([F)V

    #@585
    .line 594
    const/4 v1, 0x1

    #@586
    goto/16 :goto_7

    #@588
    .line 598
    .end local v14           #_result:[F
    :sswitch_588
    const-string v1, "android.view.IWindowManager"

    #@58a
    move-object/from16 v0, p2

    #@58c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58f
    .line 600
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@592
    move-result v2

    #@593
    .line 602
    .restart local v2       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@596
    move-result v3

    #@597
    .line 603
    .local v3, _arg1:F
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->setAnimationScale(IF)V

    #@59a
    .line 604
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@59d
    .line 605
    const/4 v1, 0x1

    #@59e
    goto/16 :goto_7

    #@5a0
    .line 609
    .end local v2           #_arg0:I
    .end local v3           #_arg1:F
    :sswitch_5a0
    const-string v1, "android.view.IWindowManager"

    #@5a2
    move-object/from16 v0, p2

    #@5a4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5a7
    .line 611
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createFloatArray()[F

    #@5aa
    move-result-object v2

    #@5ab
    .line 612
    .local v2, _arg0:[F
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setAnimationScales([F)V

    #@5ae
    .line 613
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5b1
    .line 614
    const/4 v1, 0x1

    #@5b2
    goto/16 :goto_7

    #@5b4
    .line 618
    .end local v2           #_arg0:[F
    :sswitch_5b4
    const-string v1, "android.view.IWindowManager"

    #@5b6
    move-object/from16 v0, p2

    #@5b8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5bb
    .line 620
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5be
    move-result v1

    #@5bf
    if-eqz v1, :cond_5cb

    #@5c1
    const/4 v2, 0x1

    #@5c2
    .line 621
    .local v2, _arg0:Z
    :goto_5c2
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setInTouchMode(Z)V

    #@5c5
    .line 622
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c8
    .line 623
    const/4 v1, 0x1

    #@5c9
    goto/16 :goto_7

    #@5cb
    .line 620
    .end local v2           #_arg0:Z
    :cond_5cb
    const/4 v2, 0x0

    #@5cc
    goto :goto_5c2

    #@5cd
    .line 627
    :sswitch_5cd
    const-string v1, "android.view.IWindowManager"

    #@5cf
    move-object/from16 v0, p2

    #@5d1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d4
    .line 629
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5d7
    move-result v1

    #@5d8
    if-eqz v1, :cond_5e4

    #@5da
    const/4 v2, 0x1

    #@5db
    .line 630
    .restart local v2       #_arg0:Z
    :goto_5db
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->showStrictModeViolation(Z)V

    #@5de
    .line 631
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e1
    .line 632
    const/4 v1, 0x1

    #@5e2
    goto/16 :goto_7

    #@5e4
    .line 629
    .end local v2           #_arg0:Z
    :cond_5e4
    const/4 v2, 0x0

    #@5e5
    goto :goto_5db

    #@5e6
    .line 636
    :sswitch_5e6
    const-string v1, "android.view.IWindowManager"

    #@5e8
    move-object/from16 v0, p2

    #@5ea
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5ed
    .line 638
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5f0
    move-result-object v2

    #@5f1
    .line 639
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setStrictModeVisualIndicatorPreference(Ljava/lang/String;)V

    #@5f4
    .line 640
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5f7
    .line 641
    const/4 v1, 0x1

    #@5f8
    goto/16 :goto_7

    #@5fa
    .line 645
    .end local v2           #_arg0:Ljava/lang/String;
    :sswitch_5fa
    const-string v1, "android.view.IWindowManager"

    #@5fc
    move-object/from16 v0, p2

    #@5fe
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@601
    .line 647
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@604
    move-result v1

    #@605
    if-eqz v1, :cond_618

    #@607
    const/4 v2, 0x1

    #@608
    .line 649
    .local v2, _arg0:Z
    :goto_608
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@60b
    move-result v1

    #@60c
    if-eqz v1, :cond_61a

    #@60e
    const/4 v3, 0x1

    #@60f
    .line 650
    .local v3, _arg1:Z
    :goto_60f
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->updateRotation(ZZ)V

    #@612
    .line 651
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@615
    .line 652
    const/4 v1, 0x1

    #@616
    goto/16 :goto_7

    #@618
    .line 647
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:Z
    :cond_618
    const/4 v2, 0x0

    #@619
    goto :goto_608

    #@61a
    .line 649
    .restart local v2       #_arg0:Z
    :cond_61a
    const/4 v3, 0x0

    #@61b
    goto :goto_60f

    #@61c
    .line 656
    .end local v2           #_arg0:Z
    :sswitch_61c
    const-string v1, "android.view.IWindowManager"

    #@61e
    move-object/from16 v0, p2

    #@620
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@623
    .line 657
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getRotation()I

    #@626
    move-result v14

    #@627
    .line 658
    .local v14, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@62a
    .line 659
    move-object/from16 v0, p3

    #@62c
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@62f
    .line 660
    const/4 v1, 0x1

    #@630
    goto/16 :goto_7

    #@632
    .line 664
    .end local v14           #_result:I
    :sswitch_632
    const-string v1, "android.view.IWindowManager"

    #@634
    move-object/from16 v0, p2

    #@636
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@639
    .line 666
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@63c
    move-result-object v1

    #@63d
    invoke-static {v1}, Landroid/view/IRotationWatcher$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IRotationWatcher;

    #@640
    move-result-object v2

    #@641
    .line 667
    .local v2, _arg0:Landroid/view/IRotationWatcher;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->watchRotation(Landroid/view/IRotationWatcher;)I

    #@644
    move-result v14

    #@645
    .line 668
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@648
    .line 669
    move-object/from16 v0, p3

    #@64a
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@64d
    .line 670
    const/4 v1, 0x1

    #@64e
    goto/16 :goto_7

    #@650
    .line 674
    .end local v2           #_arg0:Landroid/view/IRotationWatcher;
    .end local v14           #_result:I
    :sswitch_650
    const-string v1, "android.view.IWindowManager"

    #@652
    move-object/from16 v0, p2

    #@654
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@657
    .line 675
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getPreferredOptionsPanelGravity()I

    #@65a
    move-result v14

    #@65b
    .line 676
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@65e
    .line 677
    move-object/from16 v0, p3

    #@660
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@663
    .line 678
    const/4 v1, 0x1

    #@664
    goto/16 :goto_7

    #@666
    .line 682
    .end local v14           #_result:I
    :sswitch_666
    const-string v1, "android.view.IWindowManager"

    #@668
    move-object/from16 v0, p2

    #@66a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66d
    .line 684
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@670
    move-result v2

    #@671
    .line 685
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->freezeRotation(I)V

    #@674
    .line 686
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@677
    .line 687
    const/4 v1, 0x1

    #@678
    goto/16 :goto_7

    #@67a
    .line 691
    .end local v2           #_arg0:I
    :sswitch_67a
    const-string v1, "android.view.IWindowManager"

    #@67c
    move-object/from16 v0, p2

    #@67e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@681
    .line 692
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->thawRotation()V

    #@684
    .line 693
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@687
    .line 694
    const/4 v1, 0x1

    #@688
    goto/16 :goto_7

    #@68a
    .line 698
    :sswitch_68a
    const-string v1, "android.view.IWindowManager"

    #@68c
    move-object/from16 v0, p2

    #@68e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@691
    .line 700
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@694
    move-result-object v2

    #@695
    .line 702
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@698
    move-result v3

    #@699
    .line 704
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@69c
    move-result v4

    #@69d
    .line 706
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6a0
    move-result v5

    #@6a1
    .line 707
    .local v5, _arg3:I
    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/view/IWindowManager$Stub;->screenshotApplications(Landroid/os/IBinder;III)Landroid/graphics/Bitmap;

    #@6a4
    move-result-object v14

    #@6a5
    .line 708
    .local v14, _result:Landroid/graphics/Bitmap;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a8
    .line 709
    if-eqz v14, :cond_6b9

    #@6aa
    .line 710
    const/4 v1, 0x1

    #@6ab
    move-object/from16 v0, p3

    #@6ad
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6b0
    .line 711
    const/4 v1, 0x1

    #@6b1
    move-object/from16 v0, p3

    #@6b3
    invoke-virtual {v14, v0, v1}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@6b6
    .line 716
    :goto_6b6
    const/4 v1, 0x1

    #@6b7
    goto/16 :goto_7

    #@6b9
    .line 714
    :cond_6b9
    const/4 v1, 0x0

    #@6ba
    move-object/from16 v0, p3

    #@6bc
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6bf
    goto :goto_6b6

    #@6c0
    .line 720
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v14           #_result:Landroid/graphics/Bitmap;
    :sswitch_6c0
    const-string v1, "android.view.IWindowManager"

    #@6c2
    move-object/from16 v0, p2

    #@6c4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c7
    .line 722
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@6ca
    move-result-object v2

    #@6cb
    .line 724
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6ce
    move-result v3

    #@6cf
    .line 725
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->screenshotApplicationsFull(Landroid/os/IBinder;I)Landroid/graphics/Bitmap;

    #@6d2
    move-result-object v14

    #@6d3
    .line 726
    .restart local v14       #_result:Landroid/graphics/Bitmap;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6d6
    .line 727
    if-eqz v14, :cond_6e7

    #@6d8
    .line 728
    const/4 v1, 0x1

    #@6d9
    move-object/from16 v0, p3

    #@6db
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6de
    .line 729
    const/4 v1, 0x1

    #@6df
    move-object/from16 v0, p3

    #@6e1
    invoke-virtual {v14, v0, v1}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@6e4
    .line 734
    :goto_6e4
    const/4 v1, 0x1

    #@6e5
    goto/16 :goto_7

    #@6e7
    .line 732
    :cond_6e7
    const/4 v1, 0x0

    #@6e8
    move-object/from16 v0, p3

    #@6ea
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6ed
    goto :goto_6e4

    #@6ee
    .line 738
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    .end local v14           #_result:Landroid/graphics/Bitmap;
    :sswitch_6ee
    const-string v1, "android.view.IWindowManager"

    #@6f0
    move-object/from16 v0, p2

    #@6f2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6f5
    .line 740
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6f8
    move-result v2

    #@6f9
    .line 741
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->statusBarVisibilityChanged(I)V

    #@6fc
    .line 742
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6ff
    .line 743
    const/4 v1, 0x1

    #@700
    goto/16 :goto_7

    #@702
    .line 747
    .end local v2           #_arg0:I
    :sswitch_702
    const-string v1, "android.view.IWindowManager"

    #@704
    move-object/from16 v0, p2

    #@706
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@709
    .line 749
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@70c
    move-result-object v2

    #@70d
    .line 751
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@710
    move-result-object v1

    #@711
    invoke-static {v1}, Landroid/os/IRemoteCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IRemoteCallback;

    #@714
    move-result-object v3

    #@715
    .line 752
    .local v3, _arg1:Landroid/os/IRemoteCallback;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->waitForWindowDrawn(Landroid/os/IBinder;Landroid/os/IRemoteCallback;)Z

    #@718
    move-result v14

    #@719
    .line 753
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@71c
    .line 754
    if-eqz v14, :cond_727

    #@71e
    const/4 v1, 0x1

    #@71f
    :goto_71f
    move-object/from16 v0, p3

    #@721
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@724
    .line 755
    const/4 v1, 0x1

    #@725
    goto/16 :goto_7

    #@727
    .line 754
    :cond_727
    const/4 v1, 0x0

    #@728
    goto :goto_71f

    #@729
    .line 759
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Landroid/os/IRemoteCallback;
    .end local v14           #_result:Z
    :sswitch_729
    const-string v1, "android.view.IWindowManager"

    #@72b
    move-object/from16 v0, p2

    #@72d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@730
    .line 760
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->hasNavigationBar()Z

    #@733
    move-result v14

    #@734
    .line 761
    .restart local v14       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@737
    .line 762
    if-eqz v14, :cond_742

    #@739
    const/4 v1, 0x1

    #@73a
    :goto_73a
    move-object/from16 v0, p3

    #@73c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@73f
    .line 763
    const/4 v1, 0x1

    #@740
    goto/16 :goto_7

    #@742
    .line 762
    :cond_742
    const/4 v1, 0x0

    #@743
    goto :goto_73a

    #@744
    .line 767
    .end local v14           #_result:Z
    :sswitch_744
    const-string v1, "android.view.IWindowManager"

    #@746
    move-object/from16 v0, p2

    #@748
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@74b
    .line 769
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@74e
    move-result v1

    #@74f
    if-eqz v1, :cond_764

    #@751
    .line 770
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@753
    move-object/from16 v0, p2

    #@755
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@758
    move-result-object v2

    #@759
    check-cast v2, Landroid/os/Bundle;

    #@75b
    .line 775
    .local v2, _arg0:Landroid/os/Bundle;
    :goto_75b
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->lockNow(Landroid/os/Bundle;)V

    #@75e
    .line 776
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@761
    .line 777
    const/4 v1, 0x1

    #@762
    goto/16 :goto_7

    #@764
    .line 773
    .end local v2           #_arg0:Landroid/os/Bundle;
    :cond_764
    const/4 v2, 0x0

    #@765
    .restart local v2       #_arg0:Landroid/os/Bundle;
    goto :goto_75b

    #@766
    .line 781
    .end local v2           #_arg0:Landroid/os/Bundle;
    :sswitch_766
    const-string v1, "android.view.IWindowManager"

    #@768
    move-object/from16 v0, p2

    #@76a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@76d
    .line 782
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getFocusedWindowToken()Landroid/os/IBinder;

    #@770
    move-result-object v14

    #@771
    .line 783
    .local v14, _result:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@774
    .line 784
    move-object/from16 v0, p3

    #@776
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@779
    .line 785
    const/4 v1, 0x1

    #@77a
    goto/16 :goto_7

    #@77c
    .line 789
    .end local v14           #_result:Landroid/os/IBinder;
    :sswitch_77c
    const-string v1, "android.view.IWindowManager"

    #@77e
    move-object/from16 v0, p2

    #@780
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@783
    .line 791
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@786
    move-result-object v2

    #@787
    .line 792
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getWindowCompatibilityScale(Landroid/os/IBinder;)F

    #@78a
    move-result v14

    #@78b
    .line 793
    .local v14, _result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@78e
    .line 794
    move-object/from16 v0, p3

    #@790
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeFloat(F)V

    #@793
    .line 795
    const/4 v1, 0x1

    #@794
    goto/16 :goto_7

    #@796
    .line 799
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v14           #_result:F
    :sswitch_796
    const-string v1, "android.view.IWindowManager"

    #@798
    move-object/from16 v0, p2

    #@79a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@79d
    .line 801
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7a0
    move-result-object v1

    #@7a1
    invoke-static {v1}, Landroid/view/IInputFilter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IInputFilter;

    #@7a4
    move-result-object v2

    #@7a5
    .line 802
    .local v2, _arg0:Landroid/view/IInputFilter;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->setInputFilter(Landroid/view/IInputFilter;)V

    #@7a8
    .line 803
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7ab
    .line 804
    const/4 v1, 0x1

    #@7ac
    goto/16 :goto_7

    #@7ae
    .line 808
    .end local v2           #_arg0:Landroid/view/IInputFilter;
    :sswitch_7ae
    const-string v1, "android.view.IWindowManager"

    #@7b0
    move-object/from16 v0, p2

    #@7b2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7b5
    .line 810
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7b8
    move-result v2

    #@7b9
    .line 812
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@7bc
    move-result v3

    #@7bd
    .line 814
    .local v3, _arg1:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@7c0
    move-result v4

    #@7c1
    .line 816
    .local v4, _arg2:F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@7c4
    move-result v5

    #@7c5
    .line 817
    .local v5, _arg3:F
    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/view/IWindowManager$Stub;->magnifyDisplay(IFFF)V

    #@7c8
    .line 818
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7cb
    .line 819
    const/4 v1, 0x1

    #@7cc
    goto/16 :goto_7

    #@7ce
    .line 823
    .end local v2           #_arg0:I
    .end local v3           #_arg1:F
    .end local v4           #_arg2:F
    .end local v5           #_arg3:F
    :sswitch_7ce
    const-string v1, "android.view.IWindowManager"

    #@7d0
    move-object/from16 v0, p2

    #@7d2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7d5
    .line 825
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7d8
    move-result v2

    #@7d9
    .line 827
    .restart local v2       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7dc
    move-result-object v1

    #@7dd
    invoke-static {v1}, Landroid/view/IDisplayContentChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IDisplayContentChangeListener;

    #@7e0
    move-result-object v3

    #@7e1
    .line 828
    .local v3, _arg1:Landroid/view/IDisplayContentChangeListener;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->addDisplayContentChangeListener(ILandroid/view/IDisplayContentChangeListener;)V

    #@7e4
    .line 829
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e7
    .line 830
    const/4 v1, 0x1

    #@7e8
    goto/16 :goto_7

    #@7ea
    .line 834
    .end local v2           #_arg0:I
    .end local v3           #_arg1:Landroid/view/IDisplayContentChangeListener;
    :sswitch_7ea
    const-string v1, "android.view.IWindowManager"

    #@7ec
    move-object/from16 v0, p2

    #@7ee
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7f1
    .line 836
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7f4
    move-result v2

    #@7f5
    .line 838
    .restart local v2       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7f8
    move-result-object v1

    #@7f9
    invoke-static {v1}, Landroid/view/IDisplayContentChangeListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IDisplayContentChangeListener;

    #@7fc
    move-result-object v3

    #@7fd
    .line 839
    .restart local v3       #_arg1:Landroid/view/IDisplayContentChangeListener;
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->removeDisplayContentChangeListener(ILandroid/view/IDisplayContentChangeListener;)V

    #@800
    .line 840
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@803
    .line 841
    const/4 v1, 0x1

    #@804
    goto/16 :goto_7

    #@806
    .line 845
    .end local v2           #_arg0:I
    .end local v3           #_arg1:Landroid/view/IDisplayContentChangeListener;
    :sswitch_806
    const-string v1, "android.view.IWindowManager"

    #@808
    move-object/from16 v0, p2

    #@80a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80d
    .line 847
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@810
    move-result-object v2

    #@811
    .line 848
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getWindowInfo(Landroid/os/IBinder;)Landroid/view/WindowInfo;

    #@814
    move-result-object v14

    #@815
    .line 849
    .local v14, _result:Landroid/view/WindowInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@818
    .line 850
    if-eqz v14, :cond_829

    #@81a
    .line 851
    const/4 v1, 0x1

    #@81b
    move-object/from16 v0, p3

    #@81d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@820
    .line 852
    const/4 v1, 0x1

    #@821
    move-object/from16 v0, p3

    #@823
    invoke-virtual {v14, v0, v1}, Landroid/view/WindowInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@826
    .line 857
    :goto_826
    const/4 v1, 0x1

    #@827
    goto/16 :goto_7

    #@829
    .line 855
    :cond_829
    const/4 v1, 0x0

    #@82a
    move-object/from16 v0, p3

    #@82c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@82f
    goto :goto_826

    #@830
    .line 861
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v14           #_result:Landroid/view/WindowInfo;
    :sswitch_830
    const-string v1, "android.view.IWindowManager"

    #@832
    move-object/from16 v0, p2

    #@834
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@837
    .line 863
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@83a
    move-result v2

    #@83b
    .line 865
    .local v2, _arg0:I
    new-instance v13, Ljava/util/ArrayList;

    #@83d
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@840
    .line 866
    .local v13, _arg1:Ljava/util/List;,"Ljava/util/List<Landroid/view/WindowInfo;>;"
    invoke-virtual {p0, v2, v13}, Landroid/view/IWindowManager$Stub;->getVisibleWindowsForDisplay(ILjava/util/List;)V

    #@843
    .line 867
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@846
    .line 868
    move-object/from16 v0, p3

    #@848
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@84b
    .line 869
    const/4 v1, 0x1

    #@84c
    goto/16 :goto_7

    #@84e
    .line 873
    .end local v2           #_arg0:I
    .end local v13           #_arg1:Ljava/util/List;,"Ljava/util/List<Landroid/view/WindowInfo;>;"
    :sswitch_84e
    const-string v1, "android.view.IWindowManager"

    #@850
    move-object/from16 v0, p2

    #@852
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@855
    .line 874
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->isSafeModeEnabled()Z

    #@858
    move-result v14

    #@859
    .line 875
    .local v14, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@85c
    .line 876
    if-eqz v14, :cond_867

    #@85e
    const/4 v1, 0x1

    #@85f
    :goto_85f
    move-object/from16 v0, p3

    #@861
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@864
    .line 877
    const/4 v1, 0x1

    #@865
    goto/16 :goto_7

    #@867
    .line 876
    :cond_867
    const/4 v1, 0x0

    #@868
    goto :goto_85f

    #@869
    .line 881
    .end local v14           #_result:Z
    :sswitch_869
    const-string v1, "android.view.IWindowManager"

    #@86b
    move-object/from16 v0, p2

    #@86d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@870
    .line 882
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->showAssistant()V

    #@873
    .line 883
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@876
    .line 884
    const/4 v1, 0x1

    #@877
    goto/16 :goto_7

    #@879
    .line 888
    :sswitch_879
    const-string v1, "android.view.IWindowManager"

    #@87b
    move-object/from16 v0, p2

    #@87d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@880
    .line 889
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getViewRootBinder()Landroid/os/IBinder;

    #@883
    move-result-object v14

    #@884
    .line 890
    .local v14, _result:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@887
    .line 891
    move-object/from16 v0, p3

    #@889
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@88c
    .line 892
    const/4 v1, 0x1

    #@88d
    goto/16 :goto_7

    #@88f
    .line 896
    .end local v14           #_result:Landroid/os/IBinder;
    :sswitch_88f
    const-string v1, "android.view.IWindowManager"

    #@891
    move-object/from16 v0, p2

    #@893
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@896
    .line 898
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@899
    move-result v2

    #@89a
    .line 899
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getViewBinder(I)Landroid/os/IBinder;

    #@89d
    move-result-object v14

    #@89e
    .line 900
    .restart local v14       #_result:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a1
    .line 901
    move-object/from16 v0, p3

    #@8a3
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@8a6
    .line 902
    const/4 v1, 0x1

    #@8a7
    goto/16 :goto_7

    #@8a9
    .line 906
    .end local v2           #_arg0:I
    .end local v14           #_result:Landroid/os/IBinder;
    :sswitch_8a9
    const-string v1, "android.view.IWindowManager"

    #@8ab
    move-object/from16 v0, p2

    #@8ad
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8b0
    .line 908
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8b3
    move-result v2

    #@8b4
    .line 909
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getViewBinderTitle(I)Ljava/lang/String;

    #@8b7
    move-result-object v14

    #@8b8
    .line 910
    .local v14, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8bb
    .line 911
    move-object/from16 v0, p3

    #@8bd
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@8c0
    .line 912
    const/4 v1, 0x1

    #@8c1
    goto/16 :goto_7

    #@8c3
    .line 916
    .end local v2           #_arg0:I
    .end local v14           #_result:Ljava/lang/String;
    :sswitch_8c3
    const-string v1, "android.view.IWindowManager"

    #@8c5
    move-object/from16 v0, p2

    #@8c7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8ca
    .line 917
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getViewBinderCount()I

    #@8cd
    move-result v14

    #@8ce
    .line 918
    .local v14, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d1
    .line 919
    move-object/from16 v0, p3

    #@8d3
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@8d6
    .line 920
    const/4 v1, 0x1

    #@8d7
    goto/16 :goto_7

    #@8d9
    .line 924
    .end local v14           #_result:I
    :sswitch_8d9
    const-string v1, "android.view.IWindowManager"

    #@8db
    move-object/from16 v0, p2

    #@8dd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8e0
    .line 926
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8e3
    move-result-object v2

    #@8e4
    .line 927
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->registerInputMonitor(Ljava/lang/String;)Landroid/view/InputChannel;

    #@8e7
    move-result-object v14

    #@8e8
    .line 928
    .local v14, _result:Landroid/view/InputChannel;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8eb
    .line 929
    if-eqz v14, :cond_8fc

    #@8ed
    .line 930
    const/4 v1, 0x1

    #@8ee
    move-object/from16 v0, p3

    #@8f0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@8f3
    .line 931
    const/4 v1, 0x1

    #@8f4
    move-object/from16 v0, p3

    #@8f6
    invoke-virtual {v14, v0, v1}, Landroid/view/InputChannel;->writeToParcel(Landroid/os/Parcel;I)V

    #@8f9
    .line 936
    :goto_8f9
    const/4 v1, 0x1

    #@8fa
    goto/16 :goto_7

    #@8fc
    .line 934
    :cond_8fc
    const/4 v1, 0x0

    #@8fd
    move-object/from16 v0, p3

    #@8ff
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@902
    goto :goto_8f9

    #@903
    .line 940
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v14           #_result:Landroid/view/InputChannel;
    :sswitch_903
    const-string v1, "android.view.IWindowManager"

    #@905
    move-object/from16 v0, p2

    #@907
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@90a
    .line 942
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@90d
    move-result v1

    #@90e
    if-eqz v1, :cond_923

    #@910
    .line 943
    sget-object v1, Landroid/view/InputChannel;->CREATOR:Landroid/os/Parcelable$Creator;

    #@912
    move-object/from16 v0, p2

    #@914
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@917
    move-result-object v2

    #@918
    check-cast v2, Landroid/view/InputChannel;

    #@91a
    .line 948
    .local v2, _arg0:Landroid/view/InputChannel;
    :goto_91a
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->unregisterInputMonitor(Landroid/view/InputChannel;)V

    #@91d
    .line 949
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@920
    .line 950
    const/4 v1, 0x1

    #@921
    goto/16 :goto_7

    #@923
    .line 946
    .end local v2           #_arg0:Landroid/view/InputChannel;
    :cond_923
    const/4 v2, 0x0

    #@924
    .restart local v2       #_arg0:Landroid/view/InputChannel;
    goto :goto_91a

    #@925
    .line 954
    .end local v2           #_arg0:Landroid/view/InputChannel;
    :sswitch_925
    const-string v1, "android.view.IWindowManager"

    #@927
    move-object/from16 v0, p2

    #@929
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@92c
    .line 956
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@92f
    move-result v2

    #@930
    .line 957
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getKeycodeState(I)I

    #@933
    move-result v14

    #@934
    .line 958
    .local v14, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@937
    .line 959
    move-object/from16 v0, p3

    #@939
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@93c
    .line 960
    const/4 v1, 0x1

    #@93d
    goto/16 :goto_7

    #@93f
    .line 964
    .end local v2           #_arg0:I
    .end local v14           #_result:I
    :sswitch_93f
    const-string v1, "android.view.IWindowManager"

    #@941
    move-object/from16 v0, p2

    #@943
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@946
    .line 965
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getDsdrStatus()I

    #@949
    move-result v14

    #@94a
    .line 966
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@94d
    .line 967
    move-object/from16 v0, p3

    #@94f
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@952
    .line 968
    const/4 v1, 0x1

    #@953
    goto/16 :goto_7

    #@955
    .line 972
    .end local v14           #_result:I
    :sswitch_955
    const-string v1, "android.view.IWindowManager"

    #@957
    move-object/from16 v0, p2

    #@959
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95c
    .line 973
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->setDsdrActivated()V

    #@95f
    .line 974
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@962
    .line 975
    const/4 v1, 0x1

    #@963
    goto/16 :goto_7

    #@965
    .line 979
    :sswitch_965
    const-string v1, "android.view.IWindowManager"

    #@967
    move-object/from16 v0, p2

    #@969
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@96c
    .line 980
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->getVisibleStatus()I

    #@96f
    move-result v14

    #@970
    .line 981
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@973
    .line 982
    move-object/from16 v0, p3

    #@975
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@978
    .line 983
    const/4 v1, 0x1

    #@979
    goto/16 :goto_7

    #@97b
    .line 987
    .end local v14           #_result:I
    :sswitch_97b
    const-string v1, "android.view.IWindowManager"

    #@97d
    move-object/from16 v0, p2

    #@97f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@982
    .line 989
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@985
    move-result v1

    #@986
    if-eqz v1, :cond_998

    #@988
    const/4 v2, 0x1

    #@989
    .line 990
    .local v2, _arg0:Z
    :goto_989
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->getDsdrExtWidthHeight(Z)I

    #@98c
    move-result v14

    #@98d
    .line 991
    .restart local v14       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@990
    .line 992
    move-object/from16 v0, p3

    #@992
    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    #@995
    .line 993
    const/4 v1, 0x1

    #@996
    goto/16 :goto_7

    #@998
    .line 989
    .end local v2           #_arg0:Z
    .end local v14           #_result:I
    :cond_998
    const/4 v2, 0x0

    #@999
    goto :goto_989

    #@99a
    .line 997
    :sswitch_99a
    const-string v1, "android.view.IWindowManager"

    #@99c
    move-object/from16 v0, p2

    #@99e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a1
    .line 999
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9a4
    move-result-object v2

    #@9a5
    .line 1000
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->moveWindowTokenToTop(Landroid/os/IBinder;)V

    #@9a8
    .line 1001
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9ab
    .line 1002
    const/4 v1, 0x1

    #@9ac
    goto/16 :goto_7

    #@9ae
    .line 1006
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_9ae
    const-string v1, "android.view.IWindowManager"

    #@9b0
    move-object/from16 v0, p2

    #@9b2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b5
    .line 1008
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9b8
    move-result v2

    #@9b9
    .line 1009
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/view/IWindowManager$Stub;->freezeRotationUVS(I)V

    #@9bc
    .line 1010
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9bf
    .line 1011
    const/4 v1, 0x1

    #@9c0
    goto/16 :goto_7

    #@9c2
    .line 1015
    .end local v2           #_arg0:I
    :sswitch_9c2
    const-string v1, "android.view.IWindowManager"

    #@9c4
    move-object/from16 v0, p2

    #@9c6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9c9
    .line 1017
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9cc
    move-result-object v2

    #@9cd
    .line 1019
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9d0
    move-result v1

    #@9d1
    if-eqz v1, :cond_9dd

    #@9d3
    const/4 v3, 0x1

    #@9d4
    .line 1020
    .local v3, _arg1:Z
    :goto_9d4
    invoke-virtual {p0, v2, v3}, Landroid/view/IWindowManager$Stub;->sendSplitWindowFocusChanged(Landroid/os/IBinder;Z)V

    #@9d7
    .line 1021
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9da
    .line 1022
    const/4 v1, 0x1

    #@9db
    goto/16 :goto_7

    #@9dd
    .line 1019
    .end local v3           #_arg1:Z
    :cond_9dd
    const/4 v3, 0x0

    #@9de
    goto :goto_9d4

    #@9df
    .line 1026
    .end local v2           #_arg0:Landroid/os/IBinder;
    :sswitch_9df
    const-string v1, "android.view.IWindowManager"

    #@9e1
    move-object/from16 v0, p2

    #@9e3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9e6
    .line 1027
    invoke-virtual {p0}, Landroid/view/IWindowManager$Stub;->updateSplitWindowLayout()V

    #@9e9
    .line 1028
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9ec
    .line 1029
    const/4 v1, 0x1

    #@9ed
    goto/16 :goto_7

    #@9ef
    .line 43
    nop

    #@9f0
    :sswitch_data_9f0
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2f
        0x3 -> :sswitch_49
        0x4 -> :sswitch_63
        0x5 -> :sswitch_91
        0x6 -> :sswitch_b4
        0x7 -> :sswitch_d0
        0x8 -> :sswitch_e4
        0x9 -> :sswitch_fc
        0xa -> :sswitch_110
        0xb -> :sswitch_12b
        0xc -> :sswitch_13f
        0xd -> :sswitch_153
        0xe -> :sswitch_16c
        0xf -> :sswitch_184
        0x10 -> :sswitch_198
        0x11 -> :sswitch_1d3
        0x12 -> :sswitch_1eb
        0x13 -> :sswitch_207
        0x14 -> :sswitch_225
        0x15 -> :sswitch_242
        0x16 -> :sswitch_25f
        0x17 -> :sswitch_275
        0x18 -> :sswitch_299
        0x19 -> :sswitch_2b9
        0x1a -> :sswitch_2f5
        0x1b -> :sswitch_305
        0x1c -> :sswitch_35f
        0x1d -> :sswitch_373
        0x1e -> :sswitch_390
        0x1f -> :sswitch_3a8
        0x20 -> :sswitch_3c5
        0x21 -> :sswitch_3d9
        0x22 -> :sswitch_3f1
        0x23 -> :sswitch_405
        0x24 -> :sswitch_419
        0x25 -> :sswitch_455
        0x26 -> :sswitch_477
        0x27 -> :sswitch_48f
        0x28 -> :sswitch_49f
        0x29 -> :sswitch_4b7
        0x2a -> :sswitch_4cb
        0x2b -> :sswitch_4e3
        0x2c -> :sswitch_4fe
        0x2d -> :sswitch_519
        0x2e -> :sswitch_534
        0x2f -> :sswitch_544
        0x30 -> :sswitch_558
        0x31 -> :sswitch_572
        0x32 -> :sswitch_588
        0x33 -> :sswitch_5a0
        0x34 -> :sswitch_5b4
        0x35 -> :sswitch_5cd
        0x36 -> :sswitch_5e6
        0x37 -> :sswitch_5fa
        0x38 -> :sswitch_61c
        0x39 -> :sswitch_632
        0x3a -> :sswitch_650
        0x3b -> :sswitch_666
        0x3c -> :sswitch_67a
        0x3d -> :sswitch_68a
        0x3e -> :sswitch_6c0
        0x3f -> :sswitch_6ee
        0x40 -> :sswitch_702
        0x41 -> :sswitch_729
        0x42 -> :sswitch_744
        0x43 -> :sswitch_766
        0x44 -> :sswitch_77c
        0x45 -> :sswitch_796
        0x46 -> :sswitch_7ae
        0x47 -> :sswitch_7ce
        0x48 -> :sswitch_7ea
        0x49 -> :sswitch_806
        0x4a -> :sswitch_830
        0x4b -> :sswitch_84e
        0x4c -> :sswitch_869
        0x4d -> :sswitch_879
        0x4e -> :sswitch_88f
        0x4f -> :sswitch_8a9
        0x50 -> :sswitch_8c3
        0x51 -> :sswitch_8d9
        0x52 -> :sswitch_903
        0x53 -> :sswitch_925
        0x54 -> :sswitch_93f
        0x55 -> :sswitch_955
        0x56 -> :sswitch_965
        0x57 -> :sswitch_97b
        0x58 -> :sswitch_99a
        0x59 -> :sswitch_9ae
        0x5a -> :sswitch_9c2
        0x5b -> :sswitch_9df
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
