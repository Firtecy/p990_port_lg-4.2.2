.class final Landroid/view/VelocityTracker$1;
.super Ljava/lang/Object;
.source "VelocityTracker.java"

# interfaces
.implements Landroid/util/PoolableManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/VelocityTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/util/PoolableManager",
        "<",
        "Landroid/view/VelocityTracker;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public bridge synthetic newInstance()Landroid/util/Poolable;
    .registers 2

    #@0
    .prologue
    .line 36
    invoke-virtual {p0}, Landroid/view/VelocityTracker$1;->newInstance()Landroid/view/VelocityTracker;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newInstance()Landroid/view/VelocityTracker;
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 38
    new-instance v0, Landroid/view/VelocityTracker;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/view/VelocityTracker;-><init>(Ljava/lang/String;Landroid/view/VelocityTracker$1;)V

    #@6
    return-object v0
.end method

.method public bridge synthetic onAcquired(Landroid/util/Poolable;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    check-cast p1, Landroid/view/VelocityTracker;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/VelocityTracker$1;->onAcquired(Landroid/view/VelocityTracker;)V

    #@5
    return-void
.end method

.method public onAcquired(Landroid/view/VelocityTracker;)V
    .registers 2
    .parameter "element"

    #@0
    .prologue
    .line 43
    return-void
.end method

.method public bridge synthetic onReleased(Landroid/util/Poolable;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    check-cast p1, Landroid/view/VelocityTracker;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/VelocityTracker$1;->onReleased(Landroid/view/VelocityTracker;)V

    #@5
    return-void
.end method

.method public onReleased(Landroid/view/VelocityTracker;)V
    .registers 2
    .parameter "element"

    #@0
    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->clear()V

    #@3
    .line 47
    return-void
.end method
