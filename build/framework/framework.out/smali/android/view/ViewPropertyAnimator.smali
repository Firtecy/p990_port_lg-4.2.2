.class public Landroid/view/ViewPropertyAnimator;
.super Ljava/lang/Object;
.source "ViewPropertyAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ViewPropertyAnimator$AnimatorEventListener;,
        Landroid/view/ViewPropertyAnimator$NameValuesHolder;,
        Landroid/view/ViewPropertyAnimator$PropertyBundle;
    }
.end annotation


# static fields
.field private static final ALPHA:I = 0x200

.field private static final NONE:I = 0x0

.field private static final ROTATION:I = 0x10

.field private static final ROTATION_X:I = 0x20

.field private static final ROTATION_Y:I = 0x40

.field private static final SCALE_X:I = 0x4

.field private static final SCALE_Y:I = 0x8

.field private static final TRANSFORM_MASK:I = 0x1ff

.field private static final TRANSLATION_X:I = 0x1

.field private static final TRANSLATION_Y:I = 0x2

.field private static final X:I = 0x80

.field private static final Y:I = 0x100


# instance fields
.field private mAnimationStarter:Ljava/lang/Runnable;

.field private mAnimatorCleanupMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/animation/Animator;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimatorEventListener:Landroid/view/ViewPropertyAnimator$AnimatorEventListener;

.field private mAnimatorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/animation/Animator;",
            "Landroid/view/ViewPropertyAnimator$PropertyBundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimatorOnEndMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/animation/Animator;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimatorOnStartMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/animation/Animator;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimatorSetupMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/animation/Animator;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mDuration:J

.field private mDurationSet:Z

.field private mInterpolator:Landroid/animation/TimeInterpolator;

.field private mInterpolatorSet:Z

.field private mListener:Landroid/animation/Animator$AnimatorListener;

.field mPendingAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/ViewPropertyAnimator$NameValuesHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingCleanupAction:Ljava/lang/Runnable;

.field private mPendingOnEndAction:Ljava/lang/Runnable;

.field private mPendingOnStartAction:Ljava/lang/Runnable;

.field private mPendingSetupAction:Ljava/lang/Runnable;

.field private mStartDelay:J

.field private mStartDelaySet:Z

.field private final mView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 234
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 67
    iput-boolean v2, p0, Landroid/view/ViewPropertyAnimator;->mDurationSet:Z

    #@7
    .line 74
    const-wide/16 v0, 0x0

    #@9
    iput-wide v0, p0, Landroid/view/ViewPropertyAnimator;->mStartDelay:J

    #@b
    .line 80
    iput-boolean v2, p0, Landroid/view/ViewPropertyAnimator;->mStartDelaySet:Z

    #@d
    .line 93
    iput-boolean v2, p0, Landroid/view/ViewPropertyAnimator;->mInterpolatorSet:Z

    #@f
    .line 98
    iput-object v3, p0, Landroid/view/ViewPropertyAnimator;->mListener:Landroid/animation/Animator$AnimatorListener;

    #@11
    .line 105
    new-instance v0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;

    #@13
    invoke-direct {v0, p0, v3}, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;-><init>(Landroid/view/ViewPropertyAnimator;Landroid/view/ViewPropertyAnimator$1;)V

    #@16
    iput-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorEventListener:Landroid/view/ViewPropertyAnimator$AnimatorEventListener;

    #@18
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/view/ViewPropertyAnimator;->mPendingAnimations:Ljava/util/ArrayList;

    #@1f
    .line 148
    new-instance v0, Landroid/view/ViewPropertyAnimator$1;

    #@21
    invoke-direct {v0, p0}, Landroid/view/ViewPropertyAnimator$1;-><init>(Landroid/view/ViewPropertyAnimator;)V

    #@24
    iput-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimationStarter:Ljava/lang/Runnable;

    #@26
    .line 204
    new-instance v0, Ljava/util/HashMap;

    #@28
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2b
    iput-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@2d
    .line 235
    iput-object p1, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@2f
    .line 236
    invoke-virtual {p1}, Landroid/view/View;->ensureTransformationInfo()V

    #@32
    .line 237
    return-void
.end method

.method static synthetic access$100(Landroid/view/ViewPropertyAnimator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/view/ViewPropertyAnimator;->startAnimation()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorSetupMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnStartMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mListener:Landroid/animation/Animator$AnimatorListener;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnEndMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorCleanupMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/view/ViewPropertyAnimator;IF)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setValue(IF)V

    #@3
    return-void
.end method

.method private animateProperty(IF)V
    .registers 5
    .parameter "constantName"
    .parameter "toValue"

    #@0
    .prologue
    .line 779
    invoke-direct {p0, p1}, Landroid/view/ViewPropertyAnimator;->getValue(I)F

    #@3
    move-result v1

    #@4
    .line 780
    .local v1, fromValue:F
    sub-float v0, p2, v1

    #@6
    .line 781
    .local v0, deltaValue:F
    invoke-direct {p0, p1, v1, v0}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IFF)V

    #@9
    .line 782
    return-void
.end method

.method private animatePropertyBy(IF)V
    .registers 4
    .parameter "constantName"
    .parameter "byValue"

    #@0
    .prologue
    .line 793
    invoke-direct {p0, p1}, Landroid/view/ViewPropertyAnimator;->getValue(I)F

    #@3
    move-result v0

    #@4
    .line 794
    .local v0, fromValue:F
    invoke-direct {p0, p1, v0, p2}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IFF)V

    #@7
    .line 795
    return-void
.end method

.method private animatePropertyBy(IFF)V
    .registers 12
    .parameter "constantName"
    .parameter "startValue"
    .parameter "byValue"

    #@0
    .prologue
    .line 807
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@5
    move-result v6

    #@6
    if-lez v6, :cond_37

    #@8
    .line 808
    const/4 v1, 0x0

    #@9
    .line 809
    .local v1, animatorToCancel:Landroid/animation/Animator;
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@b
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@e
    move-result-object v0

    #@f
    .line 810
    .local v0, animatorSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/animation/Animator;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v3

    #@13
    .local v3, i$:Ljava/util/Iterator;
    :cond_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_32

    #@19
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v5

    #@1d
    check-cast v5, Landroid/animation/Animator;

    #@1f
    .line 811
    .local v5, runningAnim:Landroid/animation/Animator;
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@21
    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Landroid/view/ViewPropertyAnimator$PropertyBundle;

    #@27
    .line 812
    .local v2, bundle:Landroid/view/ViewPropertyAnimator$PropertyBundle;
    invoke-virtual {v2, p1}, Landroid/view/ViewPropertyAnimator$PropertyBundle;->cancel(I)Z

    #@2a
    move-result v6

    #@2b
    if-eqz v6, :cond_13

    #@2d
    .line 817
    iget v6, v2, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    #@2f
    if-nez v6, :cond_13

    #@31
    .line 819
    move-object v1, v5

    #@32
    .line 824
    .end local v2           #bundle:Landroid/view/ViewPropertyAnimator$PropertyBundle;
    .end local v5           #runningAnim:Landroid/animation/Animator;
    :cond_32
    if-eqz v1, :cond_37

    #@34
    .line 825
    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    #@37
    .line 829
    .end local v0           #animatorSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/animation/Animator;>;"
    .end local v1           #animatorToCancel:Landroid/animation/Animator;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_37
    new-instance v4, Landroid/view/ViewPropertyAnimator$NameValuesHolder;

    #@39
    invoke-direct {v4, p1, p2, p3}, Landroid/view/ViewPropertyAnimator$NameValuesHolder;-><init>(IFF)V

    #@3c
    .line 830
    .local v4, nameValuePair:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingAnimations:Ljava/util/ArrayList;

    #@3e
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@41
    .line 831
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@43
    iget-object v7, p0, Landroid/view/ViewPropertyAnimator;->mAnimationStarter:Ljava/lang/Runnable;

    #@45
    invoke-virtual {v6, v7}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@48
    .line 832
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@4a
    iget-object v7, p0, Landroid/view/ViewPropertyAnimator;->mAnimationStarter:Ljava/lang/Runnable;

    #@4c
    invoke-virtual {v6, v7}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@4f
    .line 833
    return-void
.end method

.method private getValue(I)F
    .registers 5
    .parameter "propertyConstant"

    #@0
    .prologue
    .line 897
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@2
    iget-object v0, v1, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@4
    .line 898
    .local v0, info:Landroid/view/View$TransformationInfo;
    sparse-switch p1, :sswitch_data_34

    #@7
    .line 920
    const/4 v1, 0x0

    #@8
    :goto_8
    return v1

    #@9
    .line 900
    :sswitch_9
    iget v1, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@b
    goto :goto_8

    #@c
    .line 902
    :sswitch_c
    iget v1, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@e
    goto :goto_8

    #@f
    .line 904
    :sswitch_f
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@11
    goto :goto_8

    #@12
    .line 906
    :sswitch_12
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@14
    goto :goto_8

    #@15
    .line 908
    :sswitch_15
    iget v1, v0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@17
    goto :goto_8

    #@18
    .line 910
    :sswitch_18
    iget v1, v0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@1a
    goto :goto_8

    #@1b
    .line 912
    :sswitch_1b
    iget v1, v0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@1d
    goto :goto_8

    #@1e
    .line 914
    :sswitch_1e
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@20
    iget v1, v1, Landroid/view/View;->mLeft:I

    #@22
    int-to-float v1, v1

    #@23
    iget v2, v0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@25
    add-float/2addr v1, v2

    #@26
    goto :goto_8

    #@27
    .line 916
    :sswitch_27
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@29
    iget v1, v1, Landroid/view/View;->mTop:I

    #@2b
    int-to-float v1, v1

    #@2c
    iget v2, v0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@2e
    add-float/2addr v1, v2

    #@2f
    goto :goto_8

    #@30
    .line 918
    :sswitch_30
    iget v1, v0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@32
    goto :goto_8

    #@33
    .line 898
    nop

    #@34
    :sswitch_data_34
    .sparse-switch
        0x1 -> :sswitch_9
        0x2 -> :sswitch_c
        0x4 -> :sswitch_18
        0x8 -> :sswitch_1b
        0x10 -> :sswitch_f
        0x20 -> :sswitch_12
        0x40 -> :sswitch_15
        0x80 -> :sswitch_1e
        0x100 -> :sswitch_27
        0x200 -> :sswitch_30
    .end sparse-switch
.end method

.method private setValue(IF)V
    .registers 6
    .parameter "propertyConstant"
    .parameter "value"

    #@0
    .prologue
    .line 844
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@2
    iget-object v1, v2, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@4
    .line 845
    .local v1, info:Landroid/view/View$TransformationInfo;
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@6
    iget-object v0, v2, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@8
    .line 846
    .local v0, displayList:Landroid/view/DisplayList;
    sparse-switch p1, :sswitch_data_78

    #@b
    .line 888
    :cond_b
    :goto_b
    return-void

    #@c
    .line 848
    :sswitch_c
    iput p2, v1, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@e
    .line 849
    if-eqz v0, :cond_b

    #@10
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setTranslationX(F)V

    #@13
    goto :goto_b

    #@14
    .line 852
    :sswitch_14
    iput p2, v1, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@16
    .line 853
    if-eqz v0, :cond_b

    #@18
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setTranslationY(F)V

    #@1b
    goto :goto_b

    #@1c
    .line 856
    :sswitch_1c
    iput p2, v1, Landroid/view/View$TransformationInfo;->mRotation:F

    #@1e
    .line 857
    if-eqz v0, :cond_b

    #@20
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setRotation(F)V

    #@23
    goto :goto_b

    #@24
    .line 860
    :sswitch_24
    iput p2, v1, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@26
    .line 861
    if-eqz v0, :cond_b

    #@28
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setRotationX(F)V

    #@2b
    goto :goto_b

    #@2c
    .line 864
    :sswitch_2c
    iput p2, v1, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@2e
    .line 865
    if-eqz v0, :cond_b

    #@30
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setRotationY(F)V

    #@33
    goto :goto_b

    #@34
    .line 868
    :sswitch_34
    iput p2, v1, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@36
    .line 869
    if-eqz v0, :cond_b

    #@38
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setScaleX(F)V

    #@3b
    goto :goto_b

    #@3c
    .line 872
    :sswitch_3c
    iput p2, v1, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@3e
    .line 873
    if-eqz v0, :cond_b

    #@40
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setScaleY(F)V

    #@43
    goto :goto_b

    #@44
    .line 876
    :sswitch_44
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@46
    iget v2, v2, Landroid/view/View;->mLeft:I

    #@48
    int-to-float v2, v2

    #@49
    sub-float v2, p2, v2

    #@4b
    iput v2, v1, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@4d
    .line 877
    if-eqz v0, :cond_b

    #@4f
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@51
    iget v2, v2, Landroid/view/View;->mLeft:I

    #@53
    int-to-float v2, v2

    #@54
    sub-float v2, p2, v2

    #@56
    invoke-virtual {v0, v2}, Landroid/view/DisplayList;->setTranslationX(F)V

    #@59
    goto :goto_b

    #@5a
    .line 880
    :sswitch_5a
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@5c
    iget v2, v2, Landroid/view/View;->mTop:I

    #@5e
    int-to-float v2, v2

    #@5f
    sub-float v2, p2, v2

    #@61
    iput v2, v1, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@63
    .line 881
    if-eqz v0, :cond_b

    #@65
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@67
    iget v2, v2, Landroid/view/View;->mTop:I

    #@69
    int-to-float v2, v2

    #@6a
    sub-float v2, p2, v2

    #@6c
    invoke-virtual {v0, v2}, Landroid/view/DisplayList;->setTranslationY(F)V

    #@6f
    goto :goto_b

    #@70
    .line 884
    :sswitch_70
    iput p2, v1, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@72
    .line 885
    if-eqz v0, :cond_b

    #@74
    invoke-virtual {v0, p2}, Landroid/view/DisplayList;->setAlpha(F)V

    #@77
    goto :goto_b

    #@78
    .line 846
    :sswitch_data_78
    .sparse-switch
        0x1 -> :sswitch_c
        0x2 -> :sswitch_14
        0x4 -> :sswitch_34
        0x8 -> :sswitch_3c
        0x10 -> :sswitch_1c
        0x20 -> :sswitch_24
        0x40 -> :sswitch_2c
        0x80 -> :sswitch_44
        0x100 -> :sswitch_5a
        0x200 -> :sswitch_70
    .end sparse-switch
.end method

.method private startAnimation()V
    .registers 11

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 724
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@4
    invoke-virtual {v6, v7}, Landroid/view/View;->setHasTransientState(Z)V

    #@7
    .line 725
    new-array v6, v7, [F

    #@9
    const/4 v7, 0x0

    #@a
    const/high16 v8, 0x3f80

    #@c
    aput v8, v6, v7

    #@e
    invoke-static {v6}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    #@11
    move-result-object v0

    #@12
    .line 726
    .local v0, animator:Landroid/animation/ValueAnimator;
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingAnimations:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v6}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Ljava/util/ArrayList;

    #@1a
    .line 728
    .local v2, nameValueList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewPropertyAnimator$NameValuesHolder;>;"
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingAnimations:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@1f
    .line 729
    const/4 v5, 0x0

    #@20
    .line 730
    .local v5, propertyMask:I
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v4

    #@24
    .line 731
    .local v4, propertyCount:I
    const/4 v1, 0x0

    #@25
    .local v1, i:I
    :goto_25
    if-ge v1, v4, :cond_33

    #@27
    .line 732
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v3

    #@2b
    check-cast v3, Landroid/view/ViewPropertyAnimator$NameValuesHolder;

    #@2d
    .line 733
    .local v3, nameValuesHolder:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    iget v6, v3, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mNameConstant:I

    #@2f
    or-int/2addr v5, v6

    #@30
    .line 731
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_25

    #@33
    .line 735
    .end local v3           #nameValuesHolder:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :cond_33
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@35
    new-instance v7, Landroid/view/ViewPropertyAnimator$PropertyBundle;

    #@37
    invoke-direct {v7, v5, v2}, Landroid/view/ViewPropertyAnimator$PropertyBundle;-><init>(ILjava/util/ArrayList;)V

    #@3a
    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 736
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingSetupAction:Ljava/lang/Runnable;

    #@3f
    if-eqz v6, :cond_4a

    #@41
    .line 737
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorSetupMap:Ljava/util/HashMap;

    #@43
    iget-object v7, p0, Landroid/view/ViewPropertyAnimator;->mPendingSetupAction:Ljava/lang/Runnable;

    #@45
    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 738
    iput-object v9, p0, Landroid/view/ViewPropertyAnimator;->mPendingSetupAction:Ljava/lang/Runnable;

    #@4a
    .line 740
    :cond_4a
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingCleanupAction:Ljava/lang/Runnable;

    #@4c
    if-eqz v6, :cond_57

    #@4e
    .line 741
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorCleanupMap:Ljava/util/HashMap;

    #@50
    iget-object v7, p0, Landroid/view/ViewPropertyAnimator;->mPendingCleanupAction:Ljava/lang/Runnable;

    #@52
    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    .line 742
    iput-object v9, p0, Landroid/view/ViewPropertyAnimator;->mPendingCleanupAction:Ljava/lang/Runnable;

    #@57
    .line 744
    :cond_57
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnStartAction:Ljava/lang/Runnable;

    #@59
    if-eqz v6, :cond_64

    #@5b
    .line 745
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnStartMap:Ljava/util/HashMap;

    #@5d
    iget-object v7, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnStartAction:Ljava/lang/Runnable;

    #@5f
    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    .line 746
    iput-object v9, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnStartAction:Ljava/lang/Runnable;

    #@64
    .line 748
    :cond_64
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnEndAction:Ljava/lang/Runnable;

    #@66
    if-eqz v6, :cond_71

    #@68
    .line 749
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnEndMap:Ljava/util/HashMap;

    #@6a
    iget-object v7, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnEndAction:Ljava/lang/Runnable;

    #@6c
    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6f
    .line 750
    iput-object v9, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnEndAction:Ljava/lang/Runnable;

    #@71
    .line 752
    :cond_71
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorEventListener:Landroid/view/ViewPropertyAnimator$AnimatorEventListener;

    #@73
    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@76
    .line 753
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorEventListener:Landroid/view/ViewPropertyAnimator$AnimatorEventListener;

    #@78
    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@7b
    .line 754
    iget-boolean v6, p0, Landroid/view/ViewPropertyAnimator;->mStartDelaySet:Z

    #@7d
    if-eqz v6, :cond_84

    #@7f
    .line 755
    iget-wide v6, p0, Landroid/view/ViewPropertyAnimator;->mStartDelay:J

    #@81
    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    #@84
    .line 757
    :cond_84
    iget-boolean v6, p0, Landroid/view/ViewPropertyAnimator;->mDurationSet:Z

    #@86
    if-eqz v6, :cond_8d

    #@88
    .line 758
    iget-wide v6, p0, Landroid/view/ViewPropertyAnimator;->mDuration:J

    #@8a
    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    #@8d
    .line 760
    :cond_8d
    iget-boolean v6, p0, Landroid/view/ViewPropertyAnimator;->mInterpolatorSet:Z

    #@8f
    if-eqz v6, :cond_96

    #@91
    .line 761
    iget-object v6, p0, Landroid/view/ViewPropertyAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@93
    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@96
    .line 763
    :cond_96
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    #@99
    .line 764
    return-void
.end method


# virtual methods
.method public alpha(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 608
    const/16 v0, 0x200

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 609
    return-object p0
.end method

.method public alphaBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 621
    const/16 v0, 0x200

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 622
    return-object p0
.end method

.method public cancel()V
    .registers 7

    #@0
    .prologue
    .line 353
    iget-object v4, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@5
    move-result v4

    #@6
    if-lez v4, :cond_28

    #@8
    .line 354
    iget-object v4, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorMap:Ljava/util/HashMap;

    #@a
    invoke-virtual {v4}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Ljava/util/HashMap;

    #@10
    .line 356
    .local v2, mAnimatorMapCopy:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/animation/Animator;Landroid/view/ViewPropertyAnimator$PropertyBundle;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@13
    move-result-object v0

    #@14
    .line 357
    .local v0, animatorSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/animation/Animator;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v1

    #@18
    .local v1, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_28

    #@1e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v3

    #@22
    check-cast v3, Landroid/animation/Animator;

    #@24
    .line 358
    .local v3, runningAnim:Landroid/animation/Animator;
    invoke-virtual {v3}, Landroid/animation/Animator;->cancel()V

    #@27
    goto :goto_18

    #@28
    .line 361
    .end local v0           #animatorSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/animation/Animator;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #mAnimatorMapCopy:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/animation/Animator;Landroid/view/ViewPropertyAnimator$PropertyBundle;>;"
    .end local v3           #runningAnim:Landroid/animation/Animator;
    :cond_28
    iget-object v4, p0, Landroid/view/ViewPropertyAnimator;->mPendingAnimations:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@2d
    .line 362
    iget-object v4, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@2f
    iget-object v5, p0, Landroid/view/ViewPropertyAnimator;->mAnimationStarter:Ljava/lang/Runnable;

    #@31
    invoke-virtual {v4, v5}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@34
    .line 363
    return-void
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 266
    iget-boolean v0, p0, Landroid/view/ViewPropertyAnimator;->mDurationSet:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 267
    iget-wide v0, p0, Landroid/view/ViewPropertyAnimator;->mDuration:J

    #@6
    .line 271
    :goto_6
    return-wide v0

    #@7
    :cond_7
    new-instance v0, Landroid/animation/ValueAnimator;

    #@9
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    #@c
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    #@f
    move-result-wide v0

    #@10
    goto :goto_6
.end method

.method public getStartDelay()J
    .registers 3

    #@0
    .prologue
    .line 284
    iget-boolean v0, p0, Landroid/view/ViewPropertyAnimator;->mStartDelaySet:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 285
    iget-wide v0, p0, Landroid/view/ViewPropertyAnimator;->mStartDelay:J

    #@6
    .line 289
    :goto_6
    return-wide v0

    #@7
    :cond_7
    const-wide/16 v0, 0x0

    #@9
    goto :goto_6
.end method

.method public rotation(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 426
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 427
    return-object p0
.end method

.method public rotationBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 439
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 440
    return-object p0
.end method

.method public rotationX(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 452
    const/16 v0, 0x20

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 453
    return-object p0
.end method

.method public rotationXBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 465
    const/16 v0, 0x20

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 466
    return-object p0
.end method

.method public rotationY(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 478
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 479
    return-object p0
.end method

.method public rotationYBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 491
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 492
    return-object p0
.end method

.method public scaleX(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 556
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@4
    .line 557
    return-object p0
.end method

.method public scaleXBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 569
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@4
    .line 570
    return-object p0
.end method

.method public scaleY(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 582
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 583
    return-object p0
.end method

.method public scaleYBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 595
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 596
    return-object p0
.end method

.method public setDuration(J)Landroid/view/ViewPropertyAnimator;
    .registers 6
    .parameter "duration"

    #@0
    .prologue
    .line 248
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_1f

    #@6
    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Animators cannot have negative duration: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 252
    :cond_1f
    const/4 v0, 0x1

    #@20
    iput-boolean v0, p0, Landroid/view/ViewPropertyAnimator;->mDurationSet:Z

    #@22
    .line 253
    iput-wide p1, p0, Landroid/view/ViewPropertyAnimator;->mDuration:J

    #@24
    .line 254
    return-object p0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "interpolator"

    #@0
    .prologue
    .line 320
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/view/ViewPropertyAnimator;->mInterpolatorSet:Z

    #@3
    .line 321
    iput-object p1, p0, Landroid/view/ViewPropertyAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    #@5
    .line 322
    return-object p0
.end method

.method public setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 333
    iput-object p1, p0, Landroid/view/ViewPropertyAnimator;->mListener:Landroid/animation/Animator$AnimatorListener;

    #@2
    .line 334
    return-object p0
.end method

.method public setStartDelay(J)Landroid/view/ViewPropertyAnimator;
    .registers 6
    .parameter "startDelay"

    #@0
    .prologue
    .line 302
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_1f

    #@6
    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Animators cannot have negative duration: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 306
    :cond_1f
    const/4 v0, 0x1

    #@20
    iput-boolean v0, p0, Landroid/view/ViewPropertyAnimator;->mStartDelaySet:Z

    #@22
    .line 307
    iput-wide p1, p0, Landroid/view/ViewPropertyAnimator;->mStartDelay:J

    #@24
    .line 308
    return-object p0
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    .line 345
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@2
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mAnimationStarter:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@7
    .line 346
    invoke-direct {p0}, Landroid/view/ViewPropertyAnimator;->startAnimation()V

    #@a
    .line 347
    return-void
.end method

.method public translationX(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 504
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@4
    .line 505
    return-object p0
.end method

.method public translationXBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 517
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@4
    .line 518
    return-object p0
.end method

.method public translationY(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 530
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@4
    .line 531
    return-object p0
.end method

.method public translationYBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 543
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@4
    .line 544
    return-object p0
.end method

.method public withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "runnable"

    #@0
    .prologue
    .line 711
    iput-object p1, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnEndAction:Ljava/lang/Runnable;

    #@2
    .line 712
    if-eqz p1, :cond_f

    #@4
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnEndMap:Ljava/util/HashMap;

    #@6
    if-nez v0, :cond_f

    #@8
    .line 713
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    iput-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnEndMap:Ljava/util/HashMap;

    #@f
    .line 715
    :cond_f
    return-object p0
.end method

.method public withLayer()Landroid/view/ViewPropertyAnimator;
    .registers 3

    #@0
    .prologue
    .line 647
    new-instance v1, Landroid/view/ViewPropertyAnimator$2;

    #@2
    invoke-direct {v1, p0}, Landroid/view/ViewPropertyAnimator$2;-><init>(Landroid/view/ViewPropertyAnimator;)V

    #@5
    iput-object v1, p0, Landroid/view/ViewPropertyAnimator;->mPendingSetupAction:Ljava/lang/Runnable;

    #@7
    .line 653
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mView:Landroid/view/View;

    #@9
    invoke-virtual {v1}, Landroid/view/View;->getLayerType()I

    #@c
    move-result v0

    #@d
    .line 654
    .local v0, currentLayerType:I
    new-instance v1, Landroid/view/ViewPropertyAnimator$3;

    #@f
    invoke-direct {v1, p0, v0}, Landroid/view/ViewPropertyAnimator$3;-><init>(Landroid/view/ViewPropertyAnimator;I)V

    #@12
    iput-object v1, p0, Landroid/view/ViewPropertyAnimator;->mPendingCleanupAction:Ljava/lang/Runnable;

    #@14
    .line 660
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorSetupMap:Ljava/util/HashMap;

    #@16
    if-nez v1, :cond_1f

    #@18
    .line 661
    new-instance v1, Ljava/util/HashMap;

    #@1a
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@1d
    iput-object v1, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorSetupMap:Ljava/util/HashMap;

    #@1f
    .line 663
    :cond_1f
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorCleanupMap:Ljava/util/HashMap;

    #@21
    if-nez v1, :cond_2a

    #@23
    .line 664
    new-instance v1, Ljava/util/HashMap;

    #@25
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@28
    iput-object v1, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorCleanupMap:Ljava/util/HashMap;

    #@2a
    .line 667
    :cond_2a
    return-object p0
.end method

.method public withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "runnable"

    #@0
    .prologue
    .line 682
    iput-object p1, p0, Landroid/view/ViewPropertyAnimator;->mPendingOnStartAction:Ljava/lang/Runnable;

    #@2
    .line 683
    if-eqz p1, :cond_f

    #@4
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnStartMap:Ljava/util/HashMap;

    #@6
    if-nez v0, :cond_f

    #@8
    .line 684
    new-instance v0, Ljava/util/HashMap;

    #@a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@d
    iput-object v0, p0, Landroid/view/ViewPropertyAnimator;->mAnimatorOnStartMap:Ljava/util/HashMap;

    #@f
    .line 686
    :cond_f
    return-object p0
.end method

.method public x(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 374
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 375
    return-object p0
.end method

.method public xBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 387
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 388
    return-object p0
.end method

.method public y(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 400
    const/16 v0, 0x100

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animateProperty(IF)V

    #@5
    .line 401
    return-object p0
.end method

.method public yBy(F)Landroid/view/ViewPropertyAnimator;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 413
    const/16 v0, 0x100

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/ViewPropertyAnimator;->animatePropertyBy(IF)V

    #@5
    .line 414
    return-object p0
.end method
