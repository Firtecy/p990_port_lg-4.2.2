.class public Landroid/view/ContextThemeWrapper;
.super Landroid/content/ContextWrapper;
.source "ContextThemeWrapper.java"


# instance fields
.field private mBase:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mOverrideConfiguration:Landroid/content/res/Configuration;

.field private mResources:Landroid/content/res/Resources;

.field private mTheme:Landroid/content/res/Resources$Theme;

.field private mThemeResource:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 38
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@4
    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 3
    .parameter "base"
    .parameter "themeres"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@3
    .line 43
    iput-object p1, p0, Landroid/view/ContextThemeWrapper;->mBase:Landroid/content/Context;

    #@5
    .line 44
    iput p2, p0, Landroid/view/ContextThemeWrapper;->mThemeResource:I

    #@7
    .line 45
    return-void
.end method

.method private initializeTheme()V
    .registers 5

    #@0
    .prologue
    .line 136
    iget-object v2, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@2
    if-nez v2, :cond_26

    #@4
    const/4 v0, 0x1

    #@5
    .line 137
    .local v0, first:Z
    :goto_5
    if-eqz v0, :cond_1e

    #@7
    .line 138
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    #@e
    move-result-object v2

    #@f
    iput-object v2, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@11
    .line 139
    iget-object v2, p0, Landroid/view/ContextThemeWrapper;->mBase:Landroid/content/Context;

    #@13
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@16
    move-result-object v1

    #@17
    .line 140
    .local v1, theme:Landroid/content/res/Resources$Theme;
    if-eqz v1, :cond_1e

    #@19
    .line 141
    iget-object v2, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@1b
    invoke-virtual {v2, v1}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    #@1e
    .line 144
    .end local v1           #theme:Landroid/content/res/Resources$Theme;
    :cond_1e
    iget-object v2, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@20
    iget v3, p0, Landroid/view/ContextThemeWrapper;->mThemeResource:I

    #@22
    invoke-virtual {p0, v2, v3, v0}, Landroid/view/ContextThemeWrapper;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    #@25
    .line 145
    return-void

    #@26
    .line 136
    .end local v0           #first:Z
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_5
.end method


# virtual methods
.method public applyOverrideConfiguration(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "overrideConfiguration"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 64
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "getResources() has already been called"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 66
    :cond_c
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    #@12
    const-string v1, "Override configuration has already been set"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 69
    :cond_18
    new-instance v0, Landroid/content/res/Configuration;

    #@1a
    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@1d
    iput-object v0, p0, Landroid/view/ContextThemeWrapper;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@1f
    .line 70
    return-void
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .registers 2
    .parameter "newBase"

    #@0
    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->attachBaseContext(Landroid/content/Context;)V

    #@3
    .line 49
    iput-object p1, p0, Landroid/view/ContextThemeWrapper;->mBase:Landroid/content/Context;

    #@5
    .line 50
    return-void
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 3

    #@0
    .prologue
    .line 74
    iget-object v1, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 75
    iget-object v1, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@6
    .line 83
    :goto_6
    return-object v1

    #@7
    .line 77
    :cond_7
    iget-object v1, p0, Landroid/view/ContextThemeWrapper;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@9
    if-nez v1, :cond_14

    #@b
    .line 78
    invoke-super {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@11
    .line 79
    iget-object v1, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@13
    goto :goto_6

    #@14
    .line 81
    :cond_14
    iget-object v1, p0, Landroid/view/ContextThemeWrapper;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@16
    invoke-virtual {p0, v1}, Landroid/view/ContextThemeWrapper;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    #@19
    move-result-object v0

    #@1a
    .line 82
    .local v0, resc:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v1

    #@1e
    iput-object v1, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@20
    .line 83
    iget-object v1, p0, Landroid/view/ContextThemeWrapper;->mResources:Landroid/content/res/Resources;

    #@22
    goto :goto_6
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 111
    const-string/jumbo v0, "layout_inflater"

    #@3
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_1c

    #@9
    .line 112
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mInflater:Landroid/view/LayoutInflater;

    #@b
    if-nez v0, :cond_19

    #@d
    .line 113
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mBase:Landroid/content/Context;

    #@f
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Landroid/view/ContextThemeWrapper;->mInflater:Landroid/view/LayoutInflater;

    #@19
    .line 115
    :cond_19
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mInflater:Landroid/view/LayoutInflater;

    #@1b
    .line 117
    :goto_1b
    return-object v0

    #@1c
    :cond_1c
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mBase:Landroid/content/Context;

    #@1e
    invoke-virtual {v0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    goto :goto_1b
.end method

.method public getTheme()Landroid/content/res/Resources$Theme;
    .registers 3

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 100
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@6
    .line 107
    :goto_6
    return-object v0

    #@7
    .line 103
    :cond_7
    iget v0, p0, Landroid/view/ContextThemeWrapper;->mThemeResource:I

    #@9
    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@c
    move-result-object v1

    #@d
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@f
    invoke-static {v0, v1}, Landroid/content/res/Resources;->selectDefaultTheme(II)I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/view/ContextThemeWrapper;->mThemeResource:I

    #@15
    .line 105
    invoke-direct {p0}, Landroid/view/ContextThemeWrapper;->initializeTheme()V

    #@18
    .line 107
    iget-object v0, p0, Landroid/view/ContextThemeWrapper;->mTheme:Landroid/content/res/Resources$Theme;

    #@1a
    goto :goto_6
.end method

.method public getThemeResId()I
    .registers 2

    #@0
    .prologue
    .line 95
    iget v0, p0, Landroid/view/ContextThemeWrapper;->mThemeResource:I

    #@2
    return v0
.end method

.method protected onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .registers 5
    .parameter "theme"
    .parameter "resid"
    .parameter "first"

    #@0
    .prologue
    .line 132
    const/4 v0, 0x1

    #@1
    invoke-virtual {p1, p2, v0}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    #@4
    .line 133
    return-void
.end method

.method public setTheme(I)V
    .registers 2
    .parameter "resid"

    #@0
    .prologue
    .line 88
    iput p1, p0, Landroid/view/ContextThemeWrapper;->mThemeResource:I

    #@2
    .line 89
    invoke-direct {p0}, Landroid/view/ContextThemeWrapper;->initializeTheme()V

    #@5
    .line 90
    return-void
.end method
