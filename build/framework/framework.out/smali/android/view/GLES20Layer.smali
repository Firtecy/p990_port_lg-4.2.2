.class abstract Landroid/view/GLES20Layer;
.super Landroid/view/HardwareLayer;
.source "GLES20Layer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/GLES20Layer$Finalizer;
    }
.end annotation


# instance fields
.field mFinalizer:Landroid/view/GLES20Layer$Finalizer;

.field mLayer:I


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 30
    invoke-direct {p0}, Landroid/view/HardwareLayer;-><init>()V

    #@3
    .line 31
    return-void
.end method

.method constructor <init>(IIZ)V
    .registers 4
    .parameter "width"
    .parameter "height"
    .parameter "opaque"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/view/HardwareLayer;-><init>(IIZ)V

    #@3
    .line 35
    return-void
.end method


# virtual methods
.method clearStorage()V
    .registers 2

    #@0
    .prologue
    .line 71
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    if-eqz v0, :cond_9

    #@4
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@6
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nClearLayerTexture(I)V

    #@9
    .line 72
    :cond_9
    return-void
.end method

.method copyInto(Landroid/graphics/Bitmap;)Z
    .registers 4
    .parameter "bitmap"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    iget v1, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@4
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nCopyLayer(II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method destroy()V
    .registers 2

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 63
    iget-object v0, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@6
    invoke-virtual {v0}, Landroid/view/GLES20Layer$Finalizer;->destroy()V

    #@9
    .line 64
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@c
    .line 66
    :cond_c
    const/4 v0, 0x0

    #@d
    iput v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@f
    .line 67
    return-void
.end method

.method public getLayer()I
    .registers 2

    #@0
    .prologue
    .line 43
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    return v0
.end method

.method setLayerPaint(Landroid/graphics/Paint;)V
    .registers 4
    .parameter "paint"

    #@0
    .prologue
    .line 48
    if-eqz p1, :cond_1a

    #@2
    .line 49
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@4
    iget v1, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@6
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nSetLayerPaint(II)V

    #@9
    .line 50
    iget v1, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@b
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    #@e
    move-result-object v0

    #@f
    if-eqz v0, :cond_1b

    #@11
    invoke-virtual {p1}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    #@14
    move-result-object v0

    #@15
    iget v0, v0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@17
    :goto_17
    invoke-static {v1, v0}, Landroid/view/GLES20Canvas;->nSetLayerColorFilter(II)V

    #@1a
    .line 53
    :cond_1a
    return-void

    #@1b
    .line 50
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_17
.end method
