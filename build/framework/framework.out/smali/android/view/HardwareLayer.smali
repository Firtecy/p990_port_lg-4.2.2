.class abstract Landroid/view/HardwareLayer;
.super Ljava/lang/Object;
.source "HardwareLayer.java"


# static fields
.field static final DIMENSION_UNDEFINED:I = -0x1


# instance fields
.field mDisplayList:Landroid/view/DisplayList;

.field mHeight:I

.field mOpaque:Z

.field mWidth:I


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 48
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v1, v1, v0}, Landroid/view/HardwareLayer;-><init>(IIZ)V

    #@5
    .line 49
    return-void
.end method

.method constructor <init>(IIZ)V
    .registers 4
    .parameter "width"
    .parameter "height"
    .parameter "isOpaque"

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    iput p1, p0, Landroid/view/HardwareLayer;->mWidth:I

    #@5
    .line 61
    iput p2, p0, Landroid/view/HardwareLayer;->mHeight:I

    #@7
    .line 62
    iput-boolean p3, p0, Landroid/view/HardwareLayer;->mOpaque:Z

    #@9
    .line 63
    return-void
.end method


# virtual methods
.method abstract clearStorage()V
.end method

.method abstract copyInto(Landroid/graphics/Bitmap;)Z
.end method

.method abstract destroy()V
.end method

.method abstract end(Landroid/graphics/Canvas;)V
.end method

.method abstract getCanvas()Landroid/view/HardwareCanvas;
.end method

.method getDisplayList()Landroid/view/DisplayList;
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/view/HardwareLayer;->mDisplayList:Landroid/view/DisplayList;

    #@2
    return-object v0
.end method

.method getHeight()I
    .registers 2

    #@0
    .prologue
    .line 88
    iget v0, p0, Landroid/view/HardwareLayer;->mHeight:I

    #@2
    return v0
.end method

.method getWidth()I
    .registers 2

    #@0
    .prologue
    .line 79
    iget v0, p0, Landroid/view/HardwareLayer;->mWidth:I

    #@2
    return v0
.end method

.method isOpaque()Z
    .registers 2

    #@0
    .prologue
    .line 115
    iget-boolean v0, p0, Landroid/view/HardwareLayer;->mOpaque:Z

    #@2
    return v0
.end method

.method abstract isValid()Z
.end method

.method abstract redrawLater(Landroid/view/DisplayList;Landroid/graphics/Rect;)V
.end method

.method abstract resize(II)Z
.end method

.method setDisplayList(Landroid/view/DisplayList;)V
    .registers 2
    .parameter "displayList"

    #@0
    .prologue
    .line 106
    iput-object p1, p0, Landroid/view/HardwareLayer;->mDisplayList:Landroid/view/DisplayList;

    #@2
    .line 107
    return-void
.end method

.method setLayerPaint(Landroid/graphics/Paint;)V
    .registers 2
    .parameter "paint"

    #@0
    .prologue
    .line 71
    return-void
.end method

.method abstract setOpaque(Z)V
.end method

.method abstract setTransform(Landroid/graphics/Matrix;)V
.end method

.method abstract start(Landroid/graphics/Canvas;)Landroid/view/HardwareCanvas;
.end method

.method update(IIZ)V
    .registers 4
    .parameter "width"
    .parameter "height"
    .parameter "isOpaque"

    #@0
    .prologue
    .line 187
    iput p1, p0, Landroid/view/HardwareLayer;->mWidth:I

    #@2
    .line 188
    iput p2, p0, Landroid/view/HardwareLayer;->mHeight:I

    #@4
    .line 189
    iput-boolean p3, p0, Landroid/view/HardwareLayer;->mOpaque:Z

    #@6
    .line 190
    return-void
.end method
