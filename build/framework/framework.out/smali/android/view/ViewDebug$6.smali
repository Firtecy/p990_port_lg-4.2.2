.class final Landroid/view/ViewDebug$6;
.super Ljava/lang/Object;
.source "ViewDebug.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/ViewDebug;->performViewCapture(Landroid/view/View;Z)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cache:[Landroid/graphics/Bitmap;

.field final synthetic val$captureView:Landroid/view/View;

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$skpiChildren:Z


# direct methods
.method constructor <init>([Landroid/graphics/Bitmap;Landroid/view/View;ZLjava/util/concurrent/CountDownLatch;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 730
    iput-object p1, p0, Landroid/view/ViewDebug$6;->val$cache:[Landroid/graphics/Bitmap;

    #@2
    iput-object p2, p0, Landroid/view/ViewDebug$6;->val$captureView:Landroid/view/View;

    #@4
    iput-boolean p3, p0, Landroid/view/ViewDebug$6;->val$skpiChildren:Z

    #@6
    iput-object p4, p0, Landroid/view/ViewDebug$6;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 733
    :try_start_0
    iget-object v1, p0, Landroid/view/ViewDebug$6;->val$cache:[Landroid/graphics/Bitmap;

    #@2
    const/4 v2, 0x0

    #@3
    iget-object v3, p0, Landroid/view/ViewDebug$6;->val$captureView:Landroid/view/View;

    #@5
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@7
    const/4 v5, 0x0

    #@8
    iget-boolean v6, p0, Landroid/view/ViewDebug$6;->val$skpiChildren:Z

    #@a
    invoke-virtual {v3, v4, v5, v6}, Landroid/view/View;->createSnapshot(Landroid/graphics/Bitmap$Config;IZ)Landroid/graphics/Bitmap;

    #@d
    move-result-object v3

    #@e
    aput-object v3, v1, v2
    :try_end_10
    .catchall {:try_start_0 .. :try_end_10} :catchall_24
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_10} :catch_16

    #@10
    .line 738
    iget-object v1, p0, Landroid/view/ViewDebug$6;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@12
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@15
    .line 740
    :goto_15
    return-void

    #@16
    .line 735
    :catch_16
    move-exception v0

    #@17
    .line 736
    .local v0, e:Ljava/lang/OutOfMemoryError;
    :try_start_17
    const-string v1, "View"

    #@19
    const-string v2, "Out of memory for bitmap"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e
    .catchall {:try_start_17 .. :try_end_1e} :catchall_24

    #@1e
    .line 738
    iget-object v1, p0, Landroid/view/ViewDebug$6;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@20
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@23
    goto :goto_15

    #@24
    .end local v0           #e:Ljava/lang/OutOfMemoryError;
    :catchall_24
    move-exception v1

    #@25
    iget-object v2, p0, Landroid/view/ViewDebug$6;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@27
    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@2a
    throw v1
.end method
