.class public final Landroid/view/VelocityTracker$Estimator;
.super Ljava/lang/Object;
.source "VelocityTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/VelocityTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Estimator"
.end annotation


# static fields
.field private static final MAX_DEGREE:I = 0x4


# instance fields
.field public confidence:F

.field public degree:I

.field public final xCoeff:[F

.field public final yCoeff:[F


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x5

    #@1
    .line 275
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 282
    new-array v0, v1, [F

    #@6
    iput-object v0, p0, Landroid/view/VelocityTracker$Estimator;->xCoeff:[F

    #@8
    .line 287
    new-array v0, v1, [F

    #@a
    iput-object v0, p0, Landroid/view/VelocityTracker$Estimator;->yCoeff:[F

    #@c
    return-void
.end method

.method private estimate(F[F)F
    .registers 7
    .parameter "time"
    .parameter "c"

    #@0
    .prologue
    .line 336
    const/4 v0, 0x0

    #@1
    .line 337
    .local v0, a:F
    const/high16 v2, 0x3f80

    #@3
    .line 338
    .local v2, scale:F
    const/4 v1, 0x0

    #@4
    .local v1, i:I
    :goto_4
    iget v3, p0, Landroid/view/VelocityTracker$Estimator;->degree:I

    #@6
    if-gt v1, v3, :cond_10

    #@8
    .line 339
    aget v3, p2, v1

    #@a
    mul-float/2addr v3, v2

    #@b
    add-float/2addr v0, v3

    #@c
    .line 340
    mul-float/2addr v2, p1

    #@d
    .line 338
    add-int/lit8 v1, v1, 0x1

    #@f
    goto :goto_4

    #@10
    .line 342
    :cond_10
    return v0
.end method


# virtual methods
.method public estimateX(F)F
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 305
    iget-object v0, p0, Landroid/view/VelocityTracker$Estimator;->xCoeff:[F

    #@2
    invoke-direct {p0, p1, v0}, Landroid/view/VelocityTracker$Estimator;->estimate(F[F)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public estimateY(F)F
    .registers 3
    .parameter "time"

    #@0
    .prologue
    .line 314
    iget-object v0, p0, Landroid/view/VelocityTracker$Estimator;->yCoeff:[F

    #@2
    invoke-direct {p0, p1, v0}, Landroid/view/VelocityTracker$Estimator;->estimate(F[F)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getXCoeff(I)F
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 323
    iget v0, p0, Landroid/view/VelocityTracker$Estimator;->degree:I

    #@2
    if-gt p1, v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/VelocityTracker$Estimator;->xCoeff:[F

    #@6
    aget v0, v0, p1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getYCoeff(I)F
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 332
    iget v0, p0, Landroid/view/VelocityTracker$Estimator;->degree:I

    #@2
    if-gt p1, v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/view/VelocityTracker$Estimator;->yCoeff:[F

    #@6
    aget v0, v0, p1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method
