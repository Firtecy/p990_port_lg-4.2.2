.class Landroid/view/ViewRootImpl$RunQueue$HandlerAction;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl$RunQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HandlerAction"
.end annotation


# instance fields
.field action:Ljava/lang/Runnable;

.field delay:J


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5489
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/ViewRootImpl$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 5489
    invoke-direct {p0}, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 5495
    if-ne p0, p1, :cond_5

    #@4
    .line 5499
    :cond_4
    :goto_4
    return v1

    #@5
    .line 5496
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 5498
    check-cast v0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;

    #@16
    .line 5499
    .local v0, that:Landroid/view/ViewRootImpl$RunQueue$HandlerAction;
    iget-object v3, p0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->action:Ljava/lang/Runnable;

    #@18
    if-eqz v3, :cond_26

    #@1a
    iget-object v3, p0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->action:Ljava/lang/Runnable;

    #@1c
    iget-object v4, v0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->action:Ljava/lang/Runnable;

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_4

    #@24
    :cond_24
    move v1, v2

    #@25
    goto :goto_4

    #@26
    :cond_26
    iget-object v3, v0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->action:Ljava/lang/Runnable;

    #@28
    if-nez v3, :cond_24

    #@2a
    goto :goto_4
.end method

.method public hashCode()I
    .registers 8

    #@0
    .prologue
    .line 5505
    iget-object v1, p0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->action:Ljava/lang/Runnable;

    #@2
    if-eqz v1, :cond_18

    #@4
    iget-object v1, p0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->action:Ljava/lang/Runnable;

    #@6
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    #@9
    move-result v0

    #@a
    .line 5506
    .local v0, result:I
    :goto_a
    mul-int/lit8 v1, v0, 0x1f

    #@c
    iget-wide v2, p0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->delay:J

    #@e
    iget-wide v4, p0, Landroid/view/ViewRootImpl$RunQueue$HandlerAction;->delay:J

    #@10
    const/16 v6, 0x20

    #@12
    ushr-long/2addr v4, v6

    #@13
    xor-long/2addr v2, v4

    #@14
    long-to-int v2, v2

    #@15
    add-int v0, v1, v2

    #@17
    .line 5507
    return v0

    #@18
    .line 5505
    .end local v0           #result:I
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_a
.end method
