.class Landroid/view/IDisplayContentChangeListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IDisplayContentChangeListener.java"

# interfaces
.implements Landroid/view/IDisplayContentChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IDisplayContentChangeListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    iput-object p1, p0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 109
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 116
    const-string v0, "android.view.IDisplayContentChangeListener"

    #@2
    return-object v0
.end method

.method public onRectangleOnScreenRequested(ILandroid/graphics/Rect;Z)V
    .registers 9
    .parameter "displayId"
    .parameter "rectangle"
    .parameter "immediate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 140
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 142
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "android.view.IDisplayContentChangeListener"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 143
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 144
    if-eqz p2, :cond_29

    #@10
    .line 145
    const/4 v3, 0x1

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 146
    const/4 v3, 0x0

    #@15
    invoke-virtual {p2, v0, v3}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@18
    .line 151
    :goto_18
    if-eqz p3, :cond_33

    #@1a
    :goto_1a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 152
    iget-object v1, p0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/4 v2, 0x2

    #@20
    const/4 v3, 0x0

    #@21
    const/4 v4, 0x1

    #@22
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_25
    .catchall {:try_start_6 .. :try_end_25} :catchall_2e

    #@25
    .line 155
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 157
    return-void

    #@29
    .line 149
    :cond_29
    const/4 v3, 0x0

    #@2a
    :try_start_2a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_2e

    #@2d
    goto :goto_18

    #@2e
    .line 155
    :catchall_2e
    move-exception v1

    #@2f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v1

    #@33
    :cond_33
    move v1, v2

    #@34
    .line 151
    goto :goto_1a
.end method

.method public onRotationChanged(I)V
    .registers 7
    .parameter "rotation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 172
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 174
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IDisplayContentChangeListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 175
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 176
    iget-object v1, p0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x4

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 179
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 181
    return-void

    #@18
    .line 179
    :catchall_18
    move-exception v1

    #@19
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    throw v1
.end method

.method public onWindowLayersChanged(I)V
    .registers 7
    .parameter "displayId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 160
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 162
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IDisplayContentChangeListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 163
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 164
    iget-object v1, p0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x3

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 167
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 169
    return-void

    #@18
    .line 167
    :catchall_18
    move-exception v1

    #@19
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    throw v1
.end method

.method public onWindowTransition(IILandroid/view/WindowInfo;)V
    .registers 9
    .parameter "displayId"
    .parameter "transition"
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 120
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 122
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IDisplayContentChangeListener"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 123
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 124
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 125
    if-eqz p3, :cond_25

    #@11
    .line 126
    const/4 v1, 0x1

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 127
    const/4 v1, 0x0

    #@16
    invoke-virtual {p3, v0, v1}, Landroid/view/WindowInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 132
    :goto_19
    iget-object v1, p0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v2, 0x1

    #@1c
    const/4 v3, 0x0

    #@1d
    const/4 v4, 0x1

    #@1e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_21
    .catchall {:try_start_4 .. :try_end_21} :catchall_2a

    #@21
    .line 135
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 137
    return-void

    #@25
    .line 130
    :cond_25
    const/4 v1, 0x0

    #@26
    :try_start_26
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_2a

    #@29
    goto :goto_19

    #@2a
    .line 135
    :catchall_2a
    move-exception v1

    #@2b
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v1
.end method
