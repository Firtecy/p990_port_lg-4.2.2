.class public Landroid/view/View$MeasureSpec;
.super Ljava/lang/Object;
.source "View.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MeasureSpec"
.end annotation


# static fields
.field public static final AT_MOST:I = -0x80000000

.field public static final EXACTLY:I = 0x40000000

.field private static final MODE_MASK:I = -0x40000000

.field private static final MODE_SHIFT:I = 0x1e

.field public static final UNSPECIFIED:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 17495
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getMode(I)I
    .registers 2
    .parameter "measureSpec"

    #@0
    .prologue
    .line 17545
    const/high16 v0, -0x4000

    #@2
    and-int/2addr v0, p0

    #@3
    return v0
.end method

.method public static getSize(I)I
    .registers 2
    .parameter "measureSpec"

    #@0
    .prologue
    .line 17555
    const v0, 0x3fffffff

    #@3
    and-int/2addr v0, p0

    #@4
    return v0
.end method

.method public static makeMeasureSpec(II)I
    .registers 3
    .parameter "size"
    .parameter "mode"

    #@0
    .prologue
    .line 17533
    add-int v0, p0, p1

    #@2
    return v0
.end method

.method public static toString(I)Ljava/lang/String;
    .registers 6
    .parameter "measureSpec"

    #@0
    .prologue
    .line 17566
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v0

    #@4
    .line 17567
    .local v0, mode:I
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@7
    move-result v2

    #@8
    .line 17569
    .local v2, size:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    const-string v3, "MeasureSpec: "

    #@c
    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@f
    .line 17571
    .local v1, sb:Ljava/lang/StringBuilder;
    if-nez v0, :cond_1e

    #@11
    .line 17572
    const-string v3, "UNSPECIFIED "

    #@13
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 17580
    :goto_16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    .line 17581
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    return-object v3

    #@1e
    .line 17573
    :cond_1e
    const/high16 v3, 0x4000

    #@20
    if-ne v0, v3, :cond_28

    #@22
    .line 17574
    const-string v3, "EXACTLY "

    #@24
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    goto :goto_16

    #@28
    .line 17575
    :cond_28
    const/high16 v3, -0x8000

    #@2a
    if-ne v0, v3, :cond_32

    #@2c
    .line 17576
    const-string v3, "AT_MOST "

    #@2e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    goto :goto_16

    #@32
    .line 17578
    :cond_32
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, " "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    goto :goto_16
.end method
