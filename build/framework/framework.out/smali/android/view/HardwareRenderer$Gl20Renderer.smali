.class Landroid/view/HardwareRenderer$Gl20Renderer;
.super Landroid/view/HardwareRenderer$GlRenderer;
.source "HardwareRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/HardwareRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Gl20Renderer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;
    }
.end annotation


# static fields
.field private static sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

.field private static final sPbufferLock:[Ljava/lang/Object;


# instance fields
.field private mGlCanvas:Landroid/view/GLES20Canvas;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1399
    const/4 v0, 0x0

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    sput-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbufferLock:[Ljava/lang/Object;

    #@5
    return-void
.end method

.method constructor <init>(Z)V
    .registers 3
    .parameter "translucent"

    #@0
    .prologue
    .line 1451
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/HardwareRenderer$GlRenderer;-><init>(IZ)V

    #@4
    .line 1452
    return-void
.end method

.method static synthetic access$300(Ljavax/microedition/khronos/egl/EGLContext;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1395
    invoke-static {p0}, Landroid/view/HardwareRenderer$Gl20Renderer;->usePbufferSurface(Ljavax/microedition/khronos/egl/EGLContext;)V

    #@3
    return-void
.end method

.method static synthetic access$400()Ljavax/microedition/khronos/egl/EGLSurface;
    .registers 1

    #@0
    .prologue
    .line 1395
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Ljavax/microedition/khronos/egl/EGLSurface;)Ljavax/microedition/khronos/egl/EGLSurface;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1395
    sput-object p0, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

    #@2
    return-object p0
.end method

.method static synthetic access$500(Landroid/view/View;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1395
    invoke-static {p0}, Landroid/view/HardwareRenderer$Gl20Renderer;->destroyHardwareLayer(Landroid/view/View;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/view/View;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1395
    invoke-static {p0}, Landroid/view/HardwareRenderer$Gl20Renderer;->destroyResources(Landroid/view/View;)V

    #@3
    return-void
.end method

.method static create(Z)Landroid/view/HardwareRenderer;
    .registers 2
    .parameter "translucent"

    #@0
    .prologue
    .line 1634
    invoke-static {}, Landroid/view/GLES20Canvas;->isAvailable()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 1635
    new-instance v0, Landroid/view/HardwareRenderer$Gl20Renderer;

    #@8
    invoke-direct {v0, p0}, Landroid/view/HardwareRenderer$Gl20Renderer;-><init>(Z)V

    #@b
    .line 1637
    :goto_b
    return-object v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private static destroyHardwareLayer(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    .line 1592
    const/4 v3, 0x1

    #@1
    invoke-virtual {p0, v3}, Landroid/view/View;->destroyLayer(Z)Z

    #@4
    .line 1594
    instance-of v3, p0, Landroid/view/ViewGroup;

    #@6
    if-eqz v3, :cond_1c

    #@8
    move-object v1, p0

    #@9
    .line 1595
    check-cast v1, Landroid/view/ViewGroup;

    #@b
    .line 1597
    .local v1, group:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@e
    move-result v0

    #@f
    .line 1598
    .local v0, count:I
    const/4 v2, 0x0

    #@10
    .local v2, i:I
    :goto_10
    if-ge v2, v0, :cond_1c

    #@12
    .line 1599
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v3

    #@16
    invoke-static {v3}, Landroid/view/HardwareRenderer$Gl20Renderer;->destroyHardwareLayer(Landroid/view/View;)V

    #@19
    .line 1598
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_10

    #@1c
    .line 1602
    .end local v0           #count:I
    .end local v1           #group:Landroid/view/ViewGroup;
    .end local v2           #i:I
    :cond_1c
    return-void
.end method

.method private static destroyResources(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    .line 1621
    invoke-virtual {p0}, Landroid/view/View;->destroyHardwareResources()V

    #@3
    .line 1623
    instance-of v3, p0, Landroid/view/ViewGroup;

    #@5
    if-eqz v3, :cond_1b

    #@7
    move-object v1, p0

    #@8
    .line 1624
    check-cast v1, Landroid/view/ViewGroup;

    #@a
    .line 1626
    .local v1, group:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@d
    move-result v0

    #@e
    .line 1627
    .local v0, count:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v0, :cond_1b

    #@11
    .line 1628
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@14
    move-result-object v3

    #@15
    invoke-static {v3}, Landroid/view/HardwareRenderer$Gl20Renderer;->destroyResources(Landroid/view/View;)V

    #@18
    .line 1627
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_f

    #@1b
    .line 1631
    .end local v0           #count:I
    .end local v1           #group:Landroid/view/ViewGroup;
    .end local v2           #i:I
    :cond_1b
    return-void
.end method

.method static endTrimMemory()V
    .registers 5

    #@0
    .prologue
    .line 1660
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@2
    if-eqz v0, :cond_15

    #@4
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@6
    if-eqz v0, :cond_15

    #@8
    .line 1661
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@a
    sget-object v1, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@c
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@e
    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@10
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@12
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@15
    .line 1663
    :cond_15
    return-void
.end method

.method static startTrimMemory(I)V
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 1641
    sget-object v1, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@2
    if-eqz v1, :cond_8

    #@4
    sget-object v1, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1657
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1643
    :cond_9
    sget-object v1, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglContextStorage:Ljava/lang/ThreadLocal;

    #@b
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;

    #@11
    .line 1646
    .local v0, managedContext:Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;
    if-eqz v0, :cond_8

    #@13
    .line 1649
    invoke-virtual {v0}, Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;->getContext()Ljavax/microedition/khronos/egl/EGLContext;

    #@16
    move-result-object v1

    #@17
    invoke-static {v1}, Landroid/view/HardwareRenderer$Gl20Renderer;->usePbufferSurface(Ljavax/microedition/khronos/egl/EGLContext;)V

    #@1a
    .line 1652
    const/16 v1, 0x50

    #@1c
    if-lt p0, v1, :cond_23

    #@1e
    .line 1653
    const/4 v1, 0x2

    #@1f
    invoke-static {v1}, Landroid/view/GLES20Canvas;->flushCaches(I)V

    #@22
    goto :goto_8

    #@23
    .line 1654
    :cond_23
    const/16 v1, 0x14

    #@25
    if-lt p0, v1, :cond_8

    #@27
    .line 1655
    const/4 v1, 0x1

    #@28
    invoke-static {v1}, Landroid/view/GLES20Canvas;->flushCaches(I)V

    #@2b
    goto :goto_8
.end method

.method private static usePbufferSurface(Ljavax/microedition/khronos/egl/EGLContext;)V
    .registers 6
    .parameter "eglContext"

    #@0
    .prologue
    .line 1666
    sget-object v1, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbufferLock:[Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1669
    :try_start_3
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

    #@5
    if-nez v0, :cond_19

    #@7
    .line 1670
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@9
    sget-object v2, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@b
    sget-object v3, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    #@d
    const/4 v4, 0x5

    #@e
    new-array v4, v4, [I

    #@10
    fill-array-data v4, :array_2a

    #@13
    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    #@16
    move-result-object v0

    #@17
    sput-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

    #@19
    .line 1674
    :cond_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_26

    #@1a
    .line 1675
    sget-object v0, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@1c
    sget-object v1, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@1e
    sget-object v2, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

    #@20
    sget-object v3, Landroid/view/HardwareRenderer$Gl20Renderer;->sPbuffer:Ljavax/microedition/khronos/egl/EGLSurface;

    #@22
    invoke-interface {v0, v1, v2, v3, p0}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@25
    .line 1676
    return-void

    #@26
    .line 1674
    :catchall_26
    move-exception v0

    #@27
    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    #@28
    throw v0

    #@29
    .line 1670
    nop

    #@2a
    :array_2a
    .array-data 0x4
        0x57t 0x30t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x56t 0x30t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x38t 0x30t 0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method canDraw()Z
    .registers 2

    #@0
    .prologue
    .line 1489
    invoke-super {p0}, Landroid/view/HardwareRenderer$GlRenderer;->canDraw()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    iget-object v0, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method createCanvas()Landroid/view/HardwareCanvas;
    .registers 3

    #@0
    .prologue
    .line 1456
    new-instance v0, Landroid/view/GLES20Canvas;

    #@2
    iget-boolean v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mTranslucent:Z

    #@4
    invoke-direct {v0, v1}, Landroid/view/GLES20Canvas;-><init>(Z)V

    #@7
    iput-object v0, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@9
    return-object v0
.end method

.method public createDisplayList(Ljava/lang/String;)Landroid/view/DisplayList;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 1528
    new-instance v0, Landroid/view/GLES20DisplayList;

    #@2
    invoke-direct {v0, p1}, Landroid/view/GLES20DisplayList;-><init>(Ljava/lang/String;)V

    #@5
    return-object v0
.end method

.method createHardwareLayer(IIZ)Landroid/view/HardwareLayer;
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "isOpaque"

    #@0
    .prologue
    .line 1538
    new-instance v0, Landroid/view/GLES20RenderLayer;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/view/GLES20RenderLayer;-><init>(IIZ)V

    #@5
    return-object v0
.end method

.method createHardwareLayer(Z)Landroid/view/HardwareLayer;
    .registers 3
    .parameter "isOpaque"

    #@0
    .prologue
    .line 1533
    new-instance v0, Landroid/view/GLES20TextureLayer;

    #@2
    invoke-direct {v0, p1}, Landroid/view/GLES20TextureLayer;-><init>(Z)V

    #@5
    return-object v0
.end method

.method createManagedContext(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/opengl/ManagedEGLContext;
    .registers 4
    .parameter "eglContext"

    #@0
    .prologue
    .line 1461
    new-instance v0, Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;

    #@2
    iget-object v1, p0, Landroid/view/HardwareRenderer$GlRenderer;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    #@4
    invoke-direct {v0, v1}, Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;-><init>(Ljavax/microedition/khronos/egl/EGLContext;)V

    #@7
    return-object v0
.end method

.method createSurfaceTexture(Landroid/view/HardwareLayer;)Landroid/graphics/SurfaceTexture;
    .registers 3
    .parameter "layer"

    #@0
    .prologue
    .line 1543
    check-cast p1, Landroid/view/GLES20TextureLayer;

    #@2
    .end local p1
    invoke-virtual {p1}, Landroid/view/GLES20TextureLayer;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method destroy(Z)V
    .registers 5
    .parameter "full"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1505
    :try_start_1
    invoke-super {p0, p1}, Landroid/view/HardwareRenderer$GlRenderer;->destroy(Z)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_d

    #@4
    .line 1507
    if-eqz p1, :cond_c

    #@6
    iget-object v0, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@8
    if-eqz v0, :cond_c

    #@a
    .line 1508
    iput-object v2, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@c
    .line 1511
    :cond_c
    return-void

    #@d
    .line 1507
    :catchall_d
    move-exception v0

    #@e
    if-eqz p1, :cond_16

    #@10
    iget-object v1, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@12
    if-eqz v1, :cond_16

    #@14
    .line 1508
    iput-object v2, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@16
    .line 1507
    :cond_16
    throw v0
.end method

.method destroyHardwareResources(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 1606
    if-eqz p1, :cond_a

    #@2
    .line 1607
    new-instance v0, Landroid/view/HardwareRenderer$Gl20Renderer$2;

    #@4
    invoke-direct {v0, p0, p1}, Landroid/view/HardwareRenderer$Gl20Renderer$2;-><init>(Landroid/view/HardwareRenderer$Gl20Renderer;Landroid/view/View;)V

    #@7
    invoke-virtual {p0, v0}, Landroid/view/HardwareRenderer$Gl20Renderer;->safelyRun(Ljava/lang/Runnable;)Z

    #@a
    .line 1618
    :cond_a
    return-void
.end method

.method destroyLayers(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 1577
    if-eqz p1, :cond_a

    #@2
    .line 1578
    new-instance v0, Landroid/view/HardwareRenderer$Gl20Renderer$1;

    #@4
    invoke-direct {v0, p0, p1}, Landroid/view/HardwareRenderer$Gl20Renderer$1;-><init>(Landroid/view/HardwareRenderer$Gl20Renderer;Landroid/view/View;)V

    #@7
    invoke-virtual {p0, v0}, Landroid/view/HardwareRenderer$Gl20Renderer;->safelyRun(Ljava/lang/Runnable;)Z

    #@a
    .line 1589
    :cond_a
    return-void
.end method

.method getConfig(Z)[I
    .registers 9
    .parameter "dirtyRegions"

    #@0
    .prologue
    const/16 v6, 0x3038

    #@2
    const/4 v5, 0x4

    #@3
    const/4 v1, 0x0

    #@4
    const/16 v4, 0x8

    #@6
    .line 1466
    const/16 v0, 0x13

    #@8
    new-array v2, v0, [I

    #@a
    const/16 v0, 0x3040

    #@c
    aput v0, v2, v1

    #@e
    const/4 v0, 0x1

    #@f
    aput v5, v2, v0

    #@11
    const/4 v0, 0x2

    #@12
    const/16 v3, 0x3024

    #@14
    aput v3, v2, v0

    #@16
    const/4 v0, 0x3

    #@17
    aput v4, v2, v0

    #@19
    const/16 v0, 0x3023

    #@1b
    aput v0, v2, v5

    #@1d
    const/4 v0, 0x5

    #@1e
    aput v4, v2, v0

    #@20
    const/4 v0, 0x6

    #@21
    const/16 v3, 0x3022

    #@23
    aput v3, v2, v0

    #@25
    const/4 v0, 0x7

    #@26
    aput v4, v2, v0

    #@28
    const/16 v0, 0x3021

    #@2a
    aput v0, v2, v4

    #@2c
    const/16 v0, 0x9

    #@2e
    aput v4, v2, v0

    #@30
    const/16 v0, 0xa

    #@32
    const/16 v3, 0x3025

    #@34
    aput v3, v2, v0

    #@36
    const/16 v0, 0xb

    #@38
    aput v1, v2, v0

    #@3a
    const/16 v0, 0xc

    #@3c
    const/16 v3, 0x3027

    #@3e
    aput v3, v2, v0

    #@40
    const/16 v0, 0xd

    #@42
    aput v6, v2, v0

    #@44
    const/16 v0, 0xe

    #@46
    const/16 v3, 0x3026

    #@48
    aput v3, v2, v0

    #@4a
    const/16 v3, 0xf

    #@4c
    iget-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mShowOverdraw:Z

    #@4e
    if-eqz v0, :cond_6b

    #@50
    invoke-static {}, Landroid/view/GLES20Canvas;->getStencilSize()I

    #@53
    move-result v0

    #@54
    :goto_54
    aput v0, v2, v3

    #@56
    const/16 v0, 0x10

    #@58
    const/16 v3, 0x3033

    #@5a
    aput v3, v2, v0

    #@5c
    const/16 v0, 0x11

    #@5e
    if-eqz p1, :cond_62

    #@60
    const/16 v1, 0x400

    #@62
    :cond_62
    or-int/lit8 v1, v1, 0x4

    #@64
    aput v1, v2, v0

    #@66
    const/16 v0, 0x12

    #@68
    aput v6, v2, v0

    #@6a
    return-object v2

    #@6b
    :cond_6b
    move v0, v1

    #@6c
    goto :goto_54
.end method

.method initCaches()V
    .registers 1

    #@0
    .prologue
    .line 1484
    invoke-static {}, Landroid/view/GLES20Canvas;->initCaches()V

    #@3
    .line 1485
    return-void
.end method

.method onPostDraw()V
    .registers 2

    #@0
    .prologue
    .line 1499
    iget-object v0, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@2
    invoke-virtual {v0}, Landroid/view/GLES20Canvas;->onPostDraw()V

    #@5
    .line 1500
    return-void
.end method

.method onPreDraw(Landroid/graphics/Rect;)I
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 1494
    iget-object v0, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/GLES20Canvas;->onPreDraw(Landroid/graphics/Rect;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method pushLayerUpdate(Landroid/view/HardwareLayer;)V
    .registers 3
    .parameter "layer"

    #@0
    .prologue
    .line 1523
    iget-object v0, p0, Landroid/view/HardwareRenderer$Gl20Renderer;->mGlCanvas:Landroid/view/GLES20Canvas;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/GLES20Canvas;->pushLayerUpdate(Landroid/view/HardwareLayer;)V

    #@5
    .line 1524
    return-void
.end method

.method safelyRun(Ljava/lang/Runnable;)Z
    .registers 10
    .parameter "action"

    #@0
    .prologue
    .line 1553
    const/4 v1, 0x1

    #@1
    .line 1554
    .local v1, needsContext:Z
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$Gl20Renderer;->isEnabled()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_e

    #@7
    invoke-virtual {p0}, Landroid/view/HardwareRenderer$Gl20Renderer;->checkCurrent()I

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_e

    #@d
    const/4 v1, 0x0

    #@e
    .line 1556
    :cond_e
    if-eqz v1, :cond_23

    #@10
    .line 1557
    sget-object v2, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglContextStorage:Ljava/lang/ThreadLocal;

    #@12
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;

    #@18
    .line 1559
    .local v0, managedContext:Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;
    if-nez v0, :cond_1c

    #@1a
    const/4 v2, 0x0

    #@1b
    .line 1572
    .end local v0           #managedContext:Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;
    :goto_1b
    return v2

    #@1c
    .line 1560
    .restart local v0       #managedContext:Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;
    :cond_1c
    invoke-virtual {v0}, Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;->getContext()Ljavax/microedition/khronos/egl/EGLContext;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v2}, Landroid/view/HardwareRenderer$Gl20Renderer;->usePbufferSurface(Ljavax/microedition/khronos/egl/EGLContext;)V

    #@23
    .line 1564
    .end local v0           #managedContext:Landroid/view/HardwareRenderer$Gl20Renderer$Gl20RendererEglContext;
    :cond_23
    :try_start_23
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_37

    #@26
    .line 1566
    if-eqz v1, :cond_35

    #@28
    .line 1567
    sget-object v2, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@2a
    sget-object v3, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@2c
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@2e
    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@30
    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@32
    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@35
    .line 1572
    :cond_35
    const/4 v2, 0x1

    #@36
    goto :goto_1b

    #@37
    .line 1566
    :catchall_37
    move-exception v2

    #@38
    if-eqz v1, :cond_47

    #@3a
    .line 1567
    sget-object v3, Landroid/view/HardwareRenderer$Gl20Renderer;->sEgl:Ljavax/microedition/khronos/egl/EGL10;

    #@3c
    sget-object v4, Landroid/view/HardwareRenderer$Gl20Renderer;->sEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    #@3e
    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@40
    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    #@42
    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    #@44
    invoke-interface {v3, v4, v5, v6, v7}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    #@47
    .line 1566
    :cond_47
    throw v2
.end method

.method setSurfaceTexture(Landroid/view/HardwareLayer;Landroid/graphics/SurfaceTexture;)V
    .registers 3
    .parameter "layer"
    .parameter "surfaceTexture"

    #@0
    .prologue
    .line 1548
    check-cast p1, Landroid/view/GLES20TextureLayer;

    #@2
    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/GLES20TextureLayer;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    #@5
    .line 1549
    return-void
.end method

.method setup(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1515
    invoke-super {p0, p1, p2}, Landroid/view/HardwareRenderer$GlRenderer;->setup(II)V

    #@3
    .line 1516
    iget-boolean v0, p0, Landroid/view/HardwareRenderer$GlRenderer;->mVsyncDisabled:Z

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 1517
    invoke-static {}, Landroid/view/HardwareRenderer$Gl20Renderer;->disableVsync()V

    #@a
    .line 1519
    :cond_a
    return-void
.end method
