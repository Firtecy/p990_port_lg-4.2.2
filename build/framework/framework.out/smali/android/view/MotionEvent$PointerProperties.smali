.class public final Landroid/view/MotionEvent$PointerProperties;
.super Ljava/lang/Object;
.source "MotionEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/MotionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PointerProperties"
.end annotation


# instance fields
.field public id:I

.field public toolType:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3573
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3574
    invoke-virtual {p0}, Landroid/view/MotionEvent$PointerProperties;->clear()V

    #@6
    .line 3575
    return-void
.end method

.method public constructor <init>(Landroid/view/MotionEvent$PointerProperties;)V
    .registers 2
    .parameter "other"

    #@0
    .prologue
    .line 3582
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 3583
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent$PointerProperties;->copyFrom(Landroid/view/MotionEvent$PointerProperties;)V

    #@6
    .line 3584
    return-void
.end method

.method static synthetic access$000(Landroid/view/MotionEvent$PointerProperties;Landroid/view/MotionEvent$PointerProperties;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 3569
    invoke-direct {p0, p1}, Landroid/view/MotionEvent$PointerProperties;->equals(Landroid/view/MotionEvent$PointerProperties;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static createArray(I)[Landroid/view/MotionEvent$PointerProperties;
    .registers 4
    .parameter "size"

    #@0
    .prologue
    .line 3588
    new-array v0, p0, [Landroid/view/MotionEvent$PointerProperties;

    #@2
    .line 3589
    .local v0, array:[Landroid/view/MotionEvent$PointerProperties;
    const/4 v1, 0x0

    #@3
    .local v1, i:I
    :goto_3
    if-ge v1, p0, :cond_f

    #@5
    .line 3590
    new-instance v2, Landroid/view/MotionEvent$PointerProperties;

    #@7
    invoke-direct {v2}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    #@a
    aput-object v2, v0, v1

    #@c
    .line 3589
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_3

    #@f
    .line 3592
    :cond_f
    return-object v0
.end method

.method private equals(Landroid/view/MotionEvent$PointerProperties;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 3638
    if-eqz p1, :cond_10

    #@2
    iget v0, p0, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@4
    iget v1, p1, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@6
    if-ne v0, v1, :cond_10

    #@8
    iget v0, p0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    #@a
    iget v1, p1, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    #@c
    if-ne v0, v1, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 3615
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@3
    .line 3616
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    #@6
    .line 3617
    return-void
.end method

.method public copyFrom(Landroid/view/MotionEvent$PointerProperties;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 3625
    iget v0, p1, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@2
    iput v0, p0, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@4
    .line 3626
    iget v0, p1, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    #@6
    iput v0, p0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    #@8
    .line 3627
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 3631
    instance-of v0, p1, Landroid/view/MotionEvent$PointerProperties;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 3632
    check-cast p1, Landroid/view/MotionEvent$PointerProperties;

    #@6
    .end local p1
    invoke-direct {p0, p1}, Landroid/view/MotionEvent$PointerProperties;->equals(Landroid/view/MotionEvent$PointerProperties;)Z

    #@9
    move-result v0

    #@a
    .line 3634
    :goto_a
    return v0

    #@b
    .restart local p1
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 3643
    iget v0, p0, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@2
    iget v1, p0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    #@4
    shl-int/lit8 v1, v1, 0x8

    #@6
    or-int/2addr v0, v1

    #@7
    return v0
.end method
