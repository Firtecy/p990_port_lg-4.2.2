.class public Landroid/view/ScaleGestureDetector;
.super Ljava/lang/Object;
.source "ScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;,
        Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ScaleGestureDetector"

.field private static final TOUCH_MIN_MAJOR:I = 0x30

.field private static final TOUCH_STABILIZE_TIME:J = 0x80L


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrSpan:F

.field private mCurrSpanX:F

.field private mCurrSpanY:F

.field private mCurrTime:J

.field private mFocusX:F

.field private mFocusY:F

.field private mInProgress:Z

.field private mInitialSpan:F

.field private final mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

.field private final mListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field private mMinSpan:I

.field private mPrevSpan:F

.field private mPrevSpanX:F

.field private mPrevSpanY:F

.field private mPrevTime:J

.field private mSpanSlop:I

.field private mTouchHistoryDirection:I

.field private mTouchHistoryLastAccepted:F

.field private mTouchHistoryLastAcceptedTime:J

.field private mTouchLower:F

.field private mTouchMinMajor:I

.field private mTouchUpper:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V
    .registers 6
    .parameter "context"
    .parameter "listener"

    #@0
    .prologue
    .line 162
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 158
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_38

    #@9
    new-instance v1, Landroid/view/InputEventConsistencyVerifier;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-direct {v1, p0, v2}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;I)V

    #@f
    :goto_f
    iput-object v1, p0, Landroid/view/ScaleGestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@11
    .line 163
    iput-object p1, p0, Landroid/view/ScaleGestureDetector;->mContext:Landroid/content/Context;

    #@13
    .line 164
    iput-object p2, p0, Landroid/view/ScaleGestureDetector;->mListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@15
    .line 165
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@1c
    move-result v1

    #@1d
    mul-int/lit8 v1, v1, 0x2

    #@1f
    iput v1, p0, Landroid/view/ScaleGestureDetector;->mSpanSlop:I

    #@21
    .line 167
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@24
    move-result-object v0

    #@25
    .line 168
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x105000a

    #@28
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@2b
    move-result v1

    #@2c
    iput v1, p0, Landroid/view/ScaleGestureDetector;->mTouchMinMajor:I

    #@2e
    .line 170
    const v1, 0x1050009

    #@31
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@34
    move-result v1

    #@35
    iput v1, p0, Landroid/view/ScaleGestureDetector;->mMinSpan:I

    #@37
    .line 172
    return-void

    #@38
    .line 158
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_38
    const/4 v1, 0x0

    #@39
    goto :goto_f
.end method

.method private addTouchHistory(Landroid/view/MotionEvent;)V
    .registers 26
    .parameter "ev"

    #@0
    .prologue
    .line 180
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v6

    #@4
    .line 181
    .local v6, currentTime:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@7
    move-result v5

    #@8
    .line 182
    .local v5, count:I
    move-object/from16 v0, p0

    #@a
    iget-wide v0, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAcceptedTime:J

    #@c
    move-wide/from16 v20, v0

    #@e
    sub-long v20, v6, v20

    #@10
    const-wide/16 v22, 0x80

    #@12
    cmp-long v20, v20, v22

    #@14
    if-ltz v20, :cond_c6

    #@16
    const/4 v3, 0x1

    #@17
    .line 183
    .local v3, accept:Z
    :goto_17
    const/16 v19, 0x0

    #@19
    .line 184
    .local v19, total:F
    const/16 v16, 0x0

    #@1b
    .line 185
    .local v16, sampleCount:I
    const/4 v12, 0x0

    #@1c
    .local v12, i:I
    :goto_1c
    if-ge v12, v5, :cond_df

    #@1e
    .line 186
    move-object/from16 v0, p0

    #@20
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAccepted:F

    #@22
    move/from16 v20, v0

    #@24
    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->isNaN(F)Z

    #@27
    move-result v20

    #@28
    if-nez v20, :cond_c9

    #@2a
    const/4 v10, 0x1

    #@2b
    .line 187
    .local v10, hasLastAccepted:Z
    :goto_2b
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    #@2e
    move-result v11

    #@2f
    .line 188
    .local v11, historySize:I
    add-int/lit8 v15, v11, 0x1

    #@31
    .line 189
    .local v15, pointerSampleCount:I
    const/4 v9, 0x0

    #@32
    .local v9, h:I
    :goto_32
    if-ge v9, v15, :cond_d9

    #@34
    .line 191
    if-ge v9, v11, :cond_cc

    #@36
    .line 192
    move-object/from16 v0, p1

    #@38
    invoke-virtual {v0, v12, v9}, Landroid/view/MotionEvent;->getHistoricalTouchMajor(II)F

    #@3b
    move-result v13

    #@3c
    .line 196
    .local v13, major:F
    :goto_3c
    move-object/from16 v0, p0

    #@3e
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchMinMajor:I

    #@40
    move/from16 v20, v0

    #@42
    move/from16 v0, v20

    #@44
    int-to-float v0, v0

    #@45
    move/from16 v20, v0

    #@47
    cmpg-float v20, v13, v20

    #@49
    if-gez v20, :cond_54

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchMinMajor:I

    #@4f
    move/from16 v20, v0

    #@51
    move/from16 v0, v20

    #@53
    int-to-float v13, v0

    #@54
    .line 197
    :cond_54
    add-float v19, v19, v13

    #@56
    .line 199
    move-object/from16 v0, p0

    #@58
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@5a
    move/from16 v20, v0

    #@5c
    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->isNaN(F)Z

    #@5f
    move-result v20

    #@60
    if-nez v20, :cond_6c

    #@62
    move-object/from16 v0, p0

    #@64
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@66
    move/from16 v20, v0

    #@68
    cmpl-float v20, v13, v20

    #@6a
    if-lez v20, :cond_70

    #@6c
    .line 200
    :cond_6c
    move-object/from16 v0, p0

    #@6e
    iput v13, v0, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@70
    .line 202
    :cond_70
    move-object/from16 v0, p0

    #@72
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@74
    move/from16 v20, v0

    #@76
    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->isNaN(F)Z

    #@79
    move-result v20

    #@7a
    if-nez v20, :cond_86

    #@7c
    move-object/from16 v0, p0

    #@7e
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@80
    move/from16 v20, v0

    #@82
    cmpg-float v20, v13, v20

    #@84
    if-gez v20, :cond_8a

    #@86
    .line 203
    :cond_86
    move-object/from16 v0, p0

    #@88
    iput v13, v0, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@8a
    .line 206
    :cond_8a
    if-eqz v10, :cond_c2

    #@8c
    .line 207
    move-object/from16 v0, p0

    #@8e
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAccepted:F

    #@90
    move/from16 v20, v0

    #@92
    sub-float v20, v13, v20

    #@94
    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->signum(F)F

    #@97
    move-result v20

    #@98
    move/from16 v0, v20

    #@9a
    float-to-int v8, v0

    #@9b
    .line 208
    .local v8, directionSig:I
    move-object/from16 v0, p0

    #@9d
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryDirection:I

    #@9f
    move/from16 v20, v0

    #@a1
    move/from16 v0, v20

    #@a3
    if-ne v8, v0, :cond_af

    #@a5
    if-nez v8, :cond_c2

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryDirection:I

    #@ab
    move/from16 v20, v0

    #@ad
    if-nez v20, :cond_c2

    #@af
    .line 210
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iput v8, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryDirection:I

    #@b3
    .line 211
    if-ge v9, v11, :cond_d4

    #@b5
    move-object/from16 v0, p1

    #@b7
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    #@ba
    move-result-wide v17

    #@bb
    .line 213
    .local v17, time:J
    :goto_bb
    move-wide/from16 v0, v17

    #@bd
    move-object/from16 v2, p0

    #@bf
    iput-wide v0, v2, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAcceptedTime:J

    #@c1
    .line 214
    const/4 v3, 0x0

    #@c2
    .line 189
    .end local v8           #directionSig:I
    .end local v17           #time:J
    :cond_c2
    add-int/lit8 v9, v9, 0x1

    #@c4
    goto/16 :goto_32

    #@c6
    .line 182
    .end local v3           #accept:Z
    .end local v9           #h:I
    .end local v10           #hasLastAccepted:Z
    .end local v11           #historySize:I
    .end local v12           #i:I
    .end local v13           #major:F
    .end local v15           #pointerSampleCount:I
    .end local v16           #sampleCount:I
    .end local v19           #total:F
    :cond_c6
    const/4 v3, 0x0

    #@c7
    goto/16 :goto_17

    #@c9
    .line 186
    .restart local v3       #accept:Z
    .restart local v12       #i:I
    .restart local v16       #sampleCount:I
    .restart local v19       #total:F
    :cond_c9
    const/4 v10, 0x0

    #@ca
    goto/16 :goto_2b

    #@cc
    .line 194
    .restart local v9       #h:I
    .restart local v10       #hasLastAccepted:Z
    .restart local v11       #historySize:I
    .restart local v15       #pointerSampleCount:I
    :cond_cc
    move-object/from16 v0, p1

    #@ce
    invoke-virtual {v0, v12}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    #@d1
    move-result v13

    #@d2
    .restart local v13       #major:F
    goto/16 :goto_3c

    #@d4
    .line 211
    .restart local v8       #directionSig:I
    :cond_d4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@d7
    move-result-wide v17

    #@d8
    goto :goto_bb

    #@d9
    .line 218
    .end local v8           #directionSig:I
    .end local v13           #major:F
    :cond_d9
    add-int v16, v16, v15

    #@db
    .line 185
    add-int/lit8 v12, v12, 0x1

    #@dd
    goto/16 :goto_1c

    #@df
    .line 221
    .end local v9           #h:I
    .end local v10           #hasLastAccepted:Z
    .end local v11           #historySize:I
    .end local v15           #pointerSampleCount:I
    :cond_df
    move/from16 v0, v16

    #@e1
    int-to-float v0, v0

    #@e2
    move/from16 v20, v0

    #@e4
    div-float v4, v19, v20

    #@e6
    .line 223
    .local v4, avg:F
    if-eqz v3, :cond_136

    #@e8
    .line 224
    move-object/from16 v0, p0

    #@ea
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@ec
    move/from16 v20, v0

    #@ee
    move-object/from16 v0, p0

    #@f0
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@f2
    move/from16 v21, v0

    #@f4
    add-float v20, v20, v21

    #@f6
    add-float v20, v20, v4

    #@f8
    const/high16 v21, 0x4040

    #@fa
    div-float v14, v20, v21

    #@fc
    .line 225
    .local v14, newAccepted:F
    move-object/from16 v0, p0

    #@fe
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@100
    move/from16 v20, v0

    #@102
    add-float v20, v20, v14

    #@104
    const/high16 v21, 0x4000

    #@106
    div-float v20, v20, v21

    #@108
    move/from16 v0, v20

    #@10a
    move-object/from16 v1, p0

    #@10c
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@10e
    .line 226
    move-object/from16 v0, p0

    #@110
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@112
    move/from16 v20, v0

    #@114
    add-float v20, v20, v14

    #@116
    const/high16 v21, 0x4000

    #@118
    div-float v20, v20, v21

    #@11a
    move/from16 v0, v20

    #@11c
    move-object/from16 v1, p0

    #@11e
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@120
    .line 227
    move-object/from16 v0, p0

    #@122
    iput v14, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAccepted:F

    #@124
    .line 228
    const/16 v20, 0x0

    #@126
    move/from16 v0, v20

    #@128
    move-object/from16 v1, p0

    #@12a
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mTouchHistoryDirection:I

    #@12c
    .line 229
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@12f
    move-result-wide v20

    #@130
    move-wide/from16 v0, v20

    #@132
    move-object/from16 v2, p0

    #@134
    iput-wide v0, v2, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAcceptedTime:J

    #@136
    .line 231
    .end local v14           #newAccepted:F
    :cond_136
    return-void
.end method

.method private clearTouchHistory()V
    .registers 3

    #@0
    .prologue
    const/high16 v0, 0x7fc0

    #@2
    .line 238
    iput v0, p0, Landroid/view/ScaleGestureDetector;->mTouchUpper:F

    #@4
    .line 239
    iput v0, p0, Landroid/view/ScaleGestureDetector;->mTouchLower:F

    #@6
    .line 240
    iput v0, p0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAccepted:F

    #@8
    .line 241
    const/4 v0, 0x0

    #@9
    iput v0, p0, Landroid/view/ScaleGestureDetector;->mTouchHistoryDirection:I

    #@b
    .line 242
    const-wide/16 v0, 0x0

    #@d
    iput-wide v0, p0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAcceptedTime:J

    #@f
    .line 243
    return-void
.end method


# virtual methods
.method public getCurrentSpan()F
    .registers 2

    #@0
    .prologue
    .line 414
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mCurrSpan:F

    #@2
    return v0
.end method

.method public getCurrentSpanX()F
    .registers 2

    #@0
    .prologue
    .line 424
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mCurrSpanX:F

    #@2
    return v0
.end method

.method public getCurrentSpanY()F
    .registers 2

    #@0
    .prologue
    .line 434
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mCurrSpanY:F

    #@2
    return v0
.end method

.method public getEventTime()J
    .registers 3

    #@0
    .prologue
    .line 494
    iget-wide v0, p0, Landroid/view/ScaleGestureDetector;->mCurrTime:J

    #@2
    return-wide v0
.end method

.method public getFocusX()F
    .registers 2

    #@0
    .prologue
    .line 390
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mFocusX:F

    #@2
    return v0
.end method

.method public getFocusY()F
    .registers 2

    #@0
    .prologue
    .line 404
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mFocusY:F

    #@2
    return v0
.end method

.method public getPreviousSpan()F
    .registers 2

    #@0
    .prologue
    .line 444
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mPrevSpan:F

    #@2
    return v0
.end method

.method public getPreviousSpanX()F
    .registers 2

    #@0
    .prologue
    .line 454
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mPrevSpanX:F

    #@2
    return v0
.end method

.method public getPreviousSpanY()F
    .registers 2

    #@0
    .prologue
    .line 464
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mPrevSpanY:F

    #@2
    return v0
.end method

.method public getScaleFactor()F
    .registers 3

    #@0
    .prologue
    .line 475
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mPrevSpan:F

    #@2
    const/4 v1, 0x0

    #@3
    cmpl-float v0, v0, v1

    #@5
    if-lez v0, :cond_d

    #@7
    iget v0, p0, Landroid/view/ScaleGestureDetector;->mCurrSpan:F

    #@9
    iget v1, p0, Landroid/view/ScaleGestureDetector;->mPrevSpan:F

    #@b
    div-float/2addr v0, v1

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/high16 v0, 0x3f80

    #@f
    goto :goto_c
.end method

.method public getTimeDelta()J
    .registers 5

    #@0
    .prologue
    .line 485
    iget-wide v0, p0, Landroid/view/ScaleGestureDetector;->mCurrTime:J

    #@2
    iget-wide v2, p0, Landroid/view/ScaleGestureDetector;->mPrevTime:J

    #@4
    sub-long/2addr v0, v2

    #@5
    return-wide v0
.end method

.method public isInProgress()Z
    .registers 2

    #@0
    .prologue
    .line 376
    iget-boolean v0, p0, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@2
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 29
    .parameter "event"

    #@0
    .prologue
    .line 258
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/view/ScaleGestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@4
    move-object/from16 v25, v0

    #@6
    if-eqz v25, :cond_19

    #@8
    .line 259
    move-object/from16 v0, p0

    #@a
    iget-object v0, v0, Landroid/view/ScaleGestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@c
    move-object/from16 v25, v0

    #@e
    const/16 v26, 0x0

    #@10
    move-object/from16 v0, v25

    #@12
    move-object/from16 v1, p1

    #@14
    move/from16 v2, v26

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    #@19
    .line 262
    :cond_19
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    #@1c
    move-result-wide v25

    #@1d
    move-wide/from16 v0, v25

    #@1f
    move-object/from16 v2, p0

    #@21
    iput-wide v0, v2, Landroid/view/ScaleGestureDetector;->mCurrTime:J

    #@23
    .line 264
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@26
    move-result v3

    #@27
    .line 266
    .local v3, action:I
    const/16 v25, 0x1

    #@29
    move/from16 v0, v25

    #@2b
    if-eq v3, v0, :cond_33

    #@2d
    const/16 v25, 0x3

    #@2f
    move/from16 v0, v25

    #@31
    if-ne v3, v0, :cond_66

    #@33
    :cond_33
    const/16 v19, 0x1

    #@35
    .line 268
    .local v19, streamComplete:Z
    :goto_35
    if-eqz v3, :cond_39

    #@37
    if-eqz v19, :cond_69

    #@39
    .line 272
    :cond_39
    move-object/from16 v0, p0

    #@3b
    iget-boolean v0, v0, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@3d
    move/from16 v25, v0

    #@3f
    if-eqz v25, :cond_5e

    #@41
    .line 273
    move-object/from16 v0, p0

    #@43
    iget-object v0, v0, Landroid/view/ScaleGestureDetector;->mListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@45
    move-object/from16 v25, v0

    #@47
    move-object/from16 v0, v25

    #@49
    move-object/from16 v1, p0

    #@4b
    invoke-interface {v0, v1}, Landroid/view/ScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    #@4e
    .line 274
    const/16 v25, 0x0

    #@50
    move/from16 v0, v25

    #@52
    move-object/from16 v1, p0

    #@54
    iput-boolean v0, v1, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@56
    .line 275
    const/16 v25, 0x0

    #@58
    move/from16 v0, v25

    #@5a
    move-object/from16 v1, p0

    #@5c
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mInitialSpan:F

    #@5e
    .line 278
    :cond_5e
    if-eqz v19, :cond_69

    #@60
    .line 279
    invoke-direct/range {p0 .. p0}, Landroid/view/ScaleGestureDetector;->clearTouchHistory()V

    #@63
    .line 280
    const/16 v25, 0x1

    #@65
    .line 369
    :goto_65
    return v25

    #@66
    .line 266
    .end local v19           #streamComplete:Z
    :cond_66
    const/16 v19, 0x0

    #@68
    goto :goto_35

    #@69
    .line 284
    .restart local v19       #streamComplete:Z
    :cond_69
    if-eqz v3, :cond_77

    #@6b
    const/16 v25, 0x6

    #@6d
    move/from16 v0, v25

    #@6f
    if-eq v3, v0, :cond_77

    #@71
    const/16 v25, 0x5

    #@73
    move/from16 v0, v25

    #@75
    if-ne v3, v0, :cond_95

    #@77
    :cond_77
    const/4 v4, 0x1

    #@78
    .line 287
    .local v4, configChanged:Z
    :goto_78
    const/16 v25, 0x6

    #@7a
    move/from16 v0, v25

    #@7c
    if-ne v3, v0, :cond_97

    #@7e
    const/4 v14, 0x1

    #@7f
    .line 288
    .local v14, pointerUp:Z
    :goto_7f
    if-eqz v14, :cond_99

    #@81
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@84
    move-result v15

    #@85
    .line 291
    .local v15, skipIndex:I
    :goto_85
    const/16 v20, 0x0

    #@87
    .local v20, sumX:F
    const/16 v21, 0x0

    #@89
    .line 292
    .local v21, sumY:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@8c
    move-result v5

    #@8d
    .line 293
    .local v5, count:I
    const/4 v13, 0x0

    #@8e
    .local v13, i:I
    :goto_8e
    if-ge v13, v5, :cond_ac

    #@90
    .line 294
    if-ne v15, v13, :cond_9b

    #@92
    .line 293
    :goto_92
    add-int/lit8 v13, v13, 0x1

    #@94
    goto :goto_8e

    #@95
    .line 284
    .end local v4           #configChanged:Z
    .end local v5           #count:I
    .end local v13           #i:I
    .end local v14           #pointerUp:Z
    .end local v15           #skipIndex:I
    .end local v20           #sumX:F
    .end local v21           #sumY:F
    :cond_95
    const/4 v4, 0x0

    #@96
    goto :goto_78

    #@97
    .line 287
    .restart local v4       #configChanged:Z
    :cond_97
    const/4 v14, 0x0

    #@98
    goto :goto_7f

    #@99
    .line 288
    .restart local v14       #pointerUp:Z
    :cond_99
    const/4 v15, -0x1

    #@9a
    goto :goto_85

    #@9b
    .line 295
    .restart local v5       #count:I
    .restart local v13       #i:I
    .restart local v15       #skipIndex:I
    .restart local v20       #sumX:F
    .restart local v21       #sumY:F
    :cond_9b
    move-object/from16 v0, p1

    #@9d
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getX(I)F

    #@a0
    move-result v25

    #@a1
    add-float v20, v20, v25

    #@a3
    .line 296
    move-object/from16 v0, p1

    #@a5
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getY(I)F

    #@a8
    move-result v25

    #@a9
    add-float v21, v21, v25

    #@ab
    goto :goto_92

    #@ac
    .line 298
    :cond_ac
    if-eqz v14, :cond_c7

    #@ae
    add-int/lit8 v10, v5, -0x1

    #@b0
    .line 299
    .local v10, div:I
    :goto_b0
    int-to-float v0, v10

    #@b1
    move/from16 v25, v0

    #@b3
    div-float v11, v20, v25

    #@b5
    .line 300
    .local v11, focusX:F
    int-to-float v0, v10

    #@b6
    move/from16 v25, v0

    #@b8
    div-float v12, v21, v25

    #@ba
    .line 303
    .local v12, focusY:F
    invoke-direct/range {p0 .. p1}, Landroid/view/ScaleGestureDetector;->addTouchHistory(Landroid/view/MotionEvent;)V

    #@bd
    .line 306
    const/4 v6, 0x0

    #@be
    .local v6, devSumX:F
    const/4 v7, 0x0

    #@bf
    .line 307
    .local v7, devSumY:F
    const/4 v13, 0x0

    #@c0
    :goto_c0
    if-ge v13, v5, :cond_f4

    #@c2
    .line 308
    if-ne v15, v13, :cond_c9

    #@c4
    .line 307
    :goto_c4
    add-int/lit8 v13, v13, 0x1

    #@c6
    goto :goto_c0

    #@c7
    .end local v6           #devSumX:F
    .end local v7           #devSumY:F
    .end local v10           #div:I
    .end local v11           #focusX:F
    .end local v12           #focusY:F
    :cond_c7
    move v10, v5

    #@c8
    .line 298
    goto :goto_b0

    #@c9
    .line 311
    .restart local v6       #devSumX:F
    .restart local v7       #devSumY:F
    .restart local v10       #div:I
    .restart local v11       #focusX:F
    .restart local v12       #focusY:F
    :cond_c9
    move-object/from16 v0, p0

    #@cb
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mTouchHistoryLastAccepted:F

    #@cd
    move/from16 v25, v0

    #@cf
    const/high16 v26, 0x4000

    #@d1
    div-float v22, v25, v26

    #@d3
    .line 312
    .local v22, touchSize:F
    move-object/from16 v0, p1

    #@d5
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getX(I)F

    #@d8
    move-result v25

    #@d9
    sub-float v25, v25, v11

    #@db
    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    #@de
    move-result v25

    #@df
    add-float v25, v25, v22

    #@e1
    add-float v6, v6, v25

    #@e3
    .line 313
    move-object/from16 v0, p1

    #@e5
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getY(I)F

    #@e8
    move-result v25

    #@e9
    sub-float v25, v25, v12

    #@eb
    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    #@ee
    move-result v25

    #@ef
    add-float v25, v25, v22

    #@f1
    add-float v7, v7, v25

    #@f3
    goto :goto_c4

    #@f4
    .line 315
    .end local v22           #touchSize:F
    :cond_f4
    int-to-float v0, v10

    #@f5
    move/from16 v25, v0

    #@f7
    div-float v8, v6, v25

    #@f9
    .line 316
    .local v8, devX:F
    int-to-float v0, v10

    #@fa
    move/from16 v25, v0

    #@fc
    div-float v9, v7, v25

    #@fe
    .line 321
    .local v9, devY:F
    const/high16 v25, 0x4000

    #@100
    mul-float v17, v8, v25

    #@102
    .line 322
    .local v17, spanX:F
    const/high16 v25, 0x4000

    #@104
    mul-float v18, v9, v25

    #@106
    .line 323
    .local v18, spanY:F
    mul-float v25, v17, v17

    #@108
    mul-float v26, v18, v18

    #@10a
    add-float v25, v25, v26

    #@10c
    invoke-static/range {v25 .. v25}, Landroid/util/FloatMath;->sqrt(F)F

    #@10f
    move-result v16

    #@110
    .line 328
    .local v16, span:F
    move-object/from16 v0, p0

    #@112
    iget-boolean v0, v0, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@114
    move/from16 v24, v0

    #@116
    .line 329
    .local v24, wasInProgress:Z
    move-object/from16 v0, p0

    #@118
    iput v11, v0, Landroid/view/ScaleGestureDetector;->mFocusX:F

    #@11a
    .line 330
    move-object/from16 v0, p0

    #@11c
    iput v12, v0, Landroid/view/ScaleGestureDetector;->mFocusY:F

    #@11e
    .line 331
    move-object/from16 v0, p0

    #@120
    iget-boolean v0, v0, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@122
    move/from16 v25, v0

    #@124
    if-eqz v25, :cond_152

    #@126
    move-object/from16 v0, p0

    #@128
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mMinSpan:I

    #@12a
    move/from16 v25, v0

    #@12c
    move/from16 v0, v25

    #@12e
    int-to-float v0, v0

    #@12f
    move/from16 v25, v0

    #@131
    cmpg-float v25, v16, v25

    #@133
    if-ltz v25, :cond_137

    #@135
    if-eqz v4, :cond_152

    #@137
    .line 332
    :cond_137
    move-object/from16 v0, p0

    #@139
    iget-object v0, v0, Landroid/view/ScaleGestureDetector;->mListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@13b
    move-object/from16 v25, v0

    #@13d
    move-object/from16 v0, v25

    #@13f
    move-object/from16 v1, p0

    #@141
    invoke-interface {v0, v1}, Landroid/view/ScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    #@144
    .line 333
    const/16 v25, 0x0

    #@146
    move/from16 v0, v25

    #@148
    move-object/from16 v1, p0

    #@14a
    iput-boolean v0, v1, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@14c
    .line 334
    move/from16 v0, v16

    #@14e
    move-object/from16 v1, p0

    #@150
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mInitialSpan:F

    #@152
    .line 336
    :cond_152
    if-eqz v4, :cond_17e

    #@154
    .line 337
    move/from16 v0, v17

    #@156
    move-object/from16 v1, p0

    #@158
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpanX:F

    #@15a
    move/from16 v0, v17

    #@15c
    move-object/from16 v1, p0

    #@15e
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpanX:F

    #@160
    .line 338
    move/from16 v0, v18

    #@162
    move-object/from16 v1, p0

    #@164
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpanY:F

    #@166
    move/from16 v0, v18

    #@168
    move-object/from16 v1, p0

    #@16a
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpanY:F

    #@16c
    .line 339
    move/from16 v0, v16

    #@16e
    move-object/from16 v1, p0

    #@170
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpan:F

    #@172
    move/from16 v0, v16

    #@174
    move-object/from16 v1, p0

    #@176
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpan:F

    #@178
    move/from16 v0, v16

    #@17a
    move-object/from16 v1, p0

    #@17c
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mInitialSpan:F

    #@17e
    .line 341
    :cond_17e
    move-object/from16 v0, p0

    #@180
    iget-boolean v0, v0, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@182
    move/from16 v25, v0

    #@184
    if-nez v25, :cond_1f6

    #@186
    move-object/from16 v0, p0

    #@188
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mMinSpan:I

    #@18a
    move/from16 v25, v0

    #@18c
    move/from16 v0, v25

    #@18e
    int-to-float v0, v0

    #@18f
    move/from16 v25, v0

    #@191
    cmpl-float v25, v16, v25

    #@193
    if-ltz v25, :cond_1f6

    #@195
    if-nez v24, :cond_1b2

    #@197
    move-object/from16 v0, p0

    #@199
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mInitialSpan:F

    #@19b
    move/from16 v25, v0

    #@19d
    sub-float v25, v16, v25

    #@19f
    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    #@1a2
    move-result v25

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mSpanSlop:I

    #@1a7
    move/from16 v26, v0

    #@1a9
    move/from16 v0, v26

    #@1ab
    int-to-float v0, v0

    #@1ac
    move/from16 v26, v0

    #@1ae
    cmpl-float v25, v25, v26

    #@1b0
    if-lez v25, :cond_1f6

    #@1b2
    .line 343
    :cond_1b2
    move/from16 v0, v17

    #@1b4
    move-object/from16 v1, p0

    #@1b6
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpanX:F

    #@1b8
    move/from16 v0, v17

    #@1ba
    move-object/from16 v1, p0

    #@1bc
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpanX:F

    #@1be
    .line 344
    move/from16 v0, v18

    #@1c0
    move-object/from16 v1, p0

    #@1c2
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpanY:F

    #@1c4
    move/from16 v0, v18

    #@1c6
    move-object/from16 v1, p0

    #@1c8
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpanY:F

    #@1ca
    .line 345
    move/from16 v0, v16

    #@1cc
    move-object/from16 v1, p0

    #@1ce
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpan:F

    #@1d0
    move/from16 v0, v16

    #@1d2
    move-object/from16 v1, p0

    #@1d4
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpan:F

    #@1d6
    .line 346
    move-object/from16 v0, p0

    #@1d8
    iget-wide v0, v0, Landroid/view/ScaleGestureDetector;->mCurrTime:J

    #@1da
    move-wide/from16 v25, v0

    #@1dc
    move-wide/from16 v0, v25

    #@1de
    move-object/from16 v2, p0

    #@1e0
    iput-wide v0, v2, Landroid/view/ScaleGestureDetector;->mPrevTime:J

    #@1e2
    .line 347
    move-object/from16 v0, p0

    #@1e4
    iget-object v0, v0, Landroid/view/ScaleGestureDetector;->mListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@1e6
    move-object/from16 v25, v0

    #@1e8
    move-object/from16 v0, v25

    #@1ea
    move-object/from16 v1, p0

    #@1ec
    invoke-interface {v0, v1}, Landroid/view/ScaleGestureDetector$OnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    #@1ef
    move-result v25

    #@1f0
    move/from16 v0, v25

    #@1f2
    move-object/from16 v1, p0

    #@1f4
    iput-boolean v0, v1, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@1f6
    .line 351
    :cond_1f6
    const/16 v25, 0x2

    #@1f8
    move/from16 v0, v25

    #@1fa
    if-ne v3, v0, :cond_258

    #@1fc
    .line 352
    move/from16 v0, v17

    #@1fe
    move-object/from16 v1, p0

    #@200
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpanX:F

    #@202
    .line 353
    move/from16 v0, v18

    #@204
    move-object/from16 v1, p0

    #@206
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpanY:F

    #@208
    .line 354
    move/from16 v0, v16

    #@20a
    move-object/from16 v1, p0

    #@20c
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mCurrSpan:F

    #@20e
    .line 356
    const/16 v23, 0x1

    #@210
    .line 357
    .local v23, updatePrev:Z
    move-object/from16 v0, p0

    #@212
    iget-boolean v0, v0, Landroid/view/ScaleGestureDetector;->mInProgress:Z

    #@214
    move/from16 v25, v0

    #@216
    if-eqz v25, :cond_226

    #@218
    .line 358
    move-object/from16 v0, p0

    #@21a
    iget-object v0, v0, Landroid/view/ScaleGestureDetector;->mListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    #@21c
    move-object/from16 v25, v0

    #@21e
    move-object/from16 v0, v25

    #@220
    move-object/from16 v1, p0

    #@222
    invoke-interface {v0, v1}, Landroid/view/ScaleGestureDetector$OnScaleGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    #@225
    move-result v23

    #@226
    .line 361
    :cond_226
    if-eqz v23, :cond_258

    #@228
    .line 362
    move-object/from16 v0, p0

    #@22a
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mCurrSpanX:F

    #@22c
    move/from16 v25, v0

    #@22e
    move/from16 v0, v25

    #@230
    move-object/from16 v1, p0

    #@232
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpanX:F

    #@234
    .line 363
    move-object/from16 v0, p0

    #@236
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mCurrSpanY:F

    #@238
    move/from16 v25, v0

    #@23a
    move/from16 v0, v25

    #@23c
    move-object/from16 v1, p0

    #@23e
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpanY:F

    #@240
    .line 364
    move-object/from16 v0, p0

    #@242
    iget v0, v0, Landroid/view/ScaleGestureDetector;->mCurrSpan:F

    #@244
    move/from16 v25, v0

    #@246
    move/from16 v0, v25

    #@248
    move-object/from16 v1, p0

    #@24a
    iput v0, v1, Landroid/view/ScaleGestureDetector;->mPrevSpan:F

    #@24c
    .line 365
    move-object/from16 v0, p0

    #@24e
    iget-wide v0, v0, Landroid/view/ScaleGestureDetector;->mCurrTime:J

    #@250
    move-wide/from16 v25, v0

    #@252
    move-wide/from16 v0, v25

    #@254
    move-object/from16 v2, p0

    #@256
    iput-wide v0, v2, Landroid/view/ScaleGestureDetector;->mPrevTime:J

    #@258
    .line 369
    .end local v23           #updatePrev:Z
    :cond_258
    const/16 v25, 0x1

    #@25a
    goto/16 :goto_65
.end method
