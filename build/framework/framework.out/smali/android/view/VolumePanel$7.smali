.class Landroid/view/VolumePanel$7;
.super Landroid/content/BroadcastReceiver;
.source "VolumePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/VolumePanel;->listenQuickCoverStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/VolumePanel;


# direct methods
.method constructor <init>(Landroid/view/VolumePanel;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1525
    iput-object p1, p0, Landroid/view/VolumePanel$7;->this$0:Landroid/view/VolumePanel;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1527
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1528
    .local v0, action:Ljava/lang/String;
    const/4 v2, 0x0

    #@7
    .line 1530
    .local v2, quickCoverMode:I
    const-string v5, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@9
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_54

    #@f
    .line 1531
    iget-object v5, p0, Landroid/view/VolumePanel$7;->this$0:Landroid/view/VolumePanel;

    #@11
    iget-object v5, v5, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v5

    #@17
    const-string/jumbo v6, "quick_view_enable"

    #@1a
    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1d
    move-result v5

    #@1e
    if-ne v5, v3, :cond_55

    #@20
    move v1, v3

    #@21
    .line 1533
    .local v1, isQuickCoverEnabled:Z
    :goto_21
    if-eqz v1, :cond_60

    #@23
    .line 1534
    const-string v5, "com.lge.android.intent.extra.ACCESSORY_STATE"

    #@25
    invoke-virtual {p2, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@28
    move-result v2

    #@29
    .line 1536
    invoke-static {}, Landroid/view/VolumePanel;->access$1000()Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_47

    #@2f
    const-string v5, "VolumePanel"

    #@31
    new-instance v6, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v7, "ACTION_ACCESSORY_EVENT QucikCoverMode: "

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1538
    :cond_47
    iget-object v5, p0, Landroid/view/VolumePanel$7;->this$0:Landroid/view/VolumePanel;

    #@49
    invoke-static {v5}, Landroid/view/VolumePanel;->access$300(Landroid/view/VolumePanel;)V

    #@4c
    .line 1540
    const/4 v5, 0x5

    #@4d
    if-ne v2, v5, :cond_57

    #@4f
    .line 1541
    iget-object v4, p0, Landroid/view/VolumePanel$7;->this$0:Landroid/view/VolumePanel;

    #@51
    invoke-static {v4, v3}, Landroid/view/VolumePanel;->access$1102(Landroid/view/VolumePanel;Z)Z

    #@54
    .line 1549
    .end local v1           #isQuickCoverEnabled:Z
    :cond_54
    :goto_54
    return-void

    #@55
    :cond_55
    move v1, v4

    #@56
    .line 1531
    goto :goto_21

    #@57
    .line 1542
    .restart local v1       #isQuickCoverEnabled:Z
    :cond_57
    const/4 v3, 0x6

    #@58
    if-ne v2, v3, :cond_54

    #@5a
    .line 1543
    iget-object v3, p0, Landroid/view/VolumePanel$7;->this$0:Landroid/view/VolumePanel;

    #@5c
    invoke-static {v3, v4}, Landroid/view/VolumePanel;->access$1102(Landroid/view/VolumePanel;Z)Z

    #@5f
    goto :goto_54

    #@60
    .line 1546
    :cond_60
    iget-object v3, p0, Landroid/view/VolumePanel$7;->this$0:Landroid/view/VolumePanel;

    #@62
    invoke-static {v3, v4}, Landroid/view/VolumePanel;->access$1102(Landroid/view/VolumePanel;Z)Z

    #@65
    goto :goto_54
.end method
