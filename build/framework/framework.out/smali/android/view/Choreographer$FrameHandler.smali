.class final Landroid/view/Choreographer$FrameHandler;
.super Landroid/os/Handler;
.source "Choreographer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Choreographer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FrameHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/Choreographer;


# direct methods
.method public constructor <init>(Landroid/view/Choreographer;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 656
    iput-object p1, p0, Landroid/view/Choreographer$FrameHandler;->this$0:Landroid/view/Choreographer;

    #@2
    .line 657
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 658
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 662
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_20

    #@5
    .line 673
    :goto_5
    return-void

    #@6
    .line 664
    :pswitch_6
    iget-object v0, p0, Landroid/view/Choreographer$FrameHandler;->this$0:Landroid/view/Choreographer;

    #@8
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@b
    move-result-wide v1

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->doFrame(JI)V

    #@10
    goto :goto_5

    #@11
    .line 667
    :pswitch_11
    iget-object v0, p0, Landroid/view/Choreographer$FrameHandler;->this$0:Landroid/view/Choreographer;

    #@13
    invoke-virtual {v0}, Landroid/view/Choreographer;->doScheduleVsync()V

    #@16
    goto :goto_5

    #@17
    .line 670
    :pswitch_17
    iget-object v0, p0, Landroid/view/Choreographer$FrameHandler;->this$0:Landroid/view/Choreographer;

    #@19
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@1b
    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->doScheduleCallback(I)V

    #@1e
    goto :goto_5

    #@1f
    .line 662
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_6
        :pswitch_11
        :pswitch_17
    .end packed-switch
.end method
