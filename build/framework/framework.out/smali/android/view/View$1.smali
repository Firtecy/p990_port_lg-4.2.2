.class Landroid/view/View$1;
.super Ljava/lang/Object;
.source "View.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mHandler:Ljava/lang/reflect/Method;

.field final synthetic this$0:Landroid/view/View;

.field final synthetic val$handlerName:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/view/View;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 3669
    iput-object p1, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@2
    iput-object p2, p0, Landroid/view/View$1;->val$handlerName:Ljava/lang/String;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 10
    .parameter "v"

    #@0
    .prologue
    .line 3673
    iget-object v3, p0, Landroid/view/View$1;->mHandler:Ljava/lang/reflect/Method;

    #@2
    if-nez v3, :cond_1e

    #@4
    .line 3675
    :try_start_4
    iget-object v3, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@6
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@d
    move-result-object v3

    #@e
    iget-object v4, p0, Landroid/view/View$1;->val$handlerName:Ljava/lang/String;

    #@10
    const/4 v5, 0x1

    #@11
    new-array v5, v5, [Ljava/lang/Class;

    #@13
    const/4 v6, 0x0

    #@14
    const-class v7, Landroid/view/View;

    #@16
    aput-object v7, v5, v6

    #@18
    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@1b
    move-result-object v3

    #@1c
    iput-object v3, p0, Landroid/view/View$1;->mHandler:Ljava/lang/reflect/Method;
    :try_end_1e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_1e} :catch_32

    #@1e
    .line 3690
    :cond_1e
    :try_start_1e
    iget-object v3, p0, Landroid/view/View$1;->mHandler:Ljava/lang/reflect/Method;

    #@20
    iget-object v4, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@22
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@25
    move-result-object v4

    #@26
    const/4 v5, 0x1

    #@27
    new-array v5, v5, [Ljava/lang/Object;

    #@29
    const/4 v6, 0x0

    #@2a
    iget-object v7, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@2c
    aput-object v7, v5, v6

    #@2e
    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catch Ljava/lang/IllegalAccessException; {:try_start_1e .. :try_end_31} :catch_af
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1e .. :try_end_31} :catch_b8

    #@31
    .line 3698
    return-void

    #@32
    .line 3677
    :catch_32
    move-exception v0

    #@33
    .line 3678
    .local v0, e:Ljava/lang/NoSuchMethodException;
    iget-object v3, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@35
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    #@38
    move-result v1

    #@39
    .line 3679
    .local v1, id:I
    const/4 v3, -0x1

    #@3a
    if-ne v1, v3, :cond_87

    #@3c
    const-string v2, ""

    #@3e
    .line 3682
    .local v2, idText:Ljava/lang/String;
    :goto_3e
    new-instance v3, Ljava/lang/IllegalStateException;

    #@40
    new-instance v4, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v5, "Could not find a method "

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    iget-object v5, p0, Landroid/view/View$1;->val$handlerName:Ljava/lang/String;

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    const-string v5, "(View) in the activity "

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    iget-object v5, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@59
    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, " for onClick handler"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    const-string v5, " on view "

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    iget-object v5, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@73
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v4

    #@83
    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@86
    throw v3

    #@87
    .line 3679
    .end local v2           #idText:Ljava/lang/String;
    :cond_87
    new-instance v3, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v4, " with id \'"

    #@8e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v3

    #@92
    iget-object v4, p0, Landroid/view/View$1;->this$0:Landroid/view/View;

    #@94
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    #@9f
    move-result-object v4

    #@a0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    const-string v4, "\'"

    #@a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v2

    #@ae
    goto :goto_3e

    #@af
    .line 3691
    .end local v0           #e:Ljava/lang/NoSuchMethodException;
    .end local v1           #id:I
    :catch_af
    move-exception v0

    #@b0
    .line 3692
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v3, Ljava/lang/IllegalStateException;

    #@b2
    const-string v4, "Could not execute non public method of the activity"

    #@b4
    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b7
    throw v3

    #@b8
    .line 3694
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_b8
    move-exception v0

    #@b9
    .line 3695
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    new-instance v3, Ljava/lang/IllegalStateException;

    #@bb
    const-string v4, "Could not execute method of the activity"

    #@bd
    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c0
    throw v3
.end method
