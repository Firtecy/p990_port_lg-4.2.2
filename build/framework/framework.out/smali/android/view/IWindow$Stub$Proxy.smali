.class Landroid/view/IWindow$Stub$Proxy;
.super Ljava/lang/Object;
.source "IWindow.java"

# interfaces
.implements Landroid/view/IWindow;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IWindow$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 234
    iput-object p1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 235
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public closeSystemDialogs(Ljava/lang/String;)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 380
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 382
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 383
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 384
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/16 v2, 0x8

    #@10
    const/4 v3, 0x0

    #@11
    const/4 v4, 0x1

    #@12
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_19

    #@15
    .line 387
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@18
    .line 389
    return-void

    #@19
    .line 387
    :catchall_19
    move-exception v1

    #@1a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    throw v1
.end method

.method public dispatchAppVisibility(Z)V
    .registers 7
    .parameter "visible"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 328
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 330
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.IWindow"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 331
    if-eqz p1, :cond_1b

    #@c
    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 332
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x4

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1d

    #@17
    .line 335
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 337
    return-void

    #@1b
    .line 331
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_c

    #@1d
    .line 335
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)V
    .registers 7
    .parameter "event"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 437
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 439
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 440
    if-eqz p1, :cond_20

    #@b
    .line 441
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 442
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/view/DragEvent;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 447
    :goto_13
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v2, 0xb

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x1

    #@19
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1c
    .catchall {:try_start_4 .. :try_end_1c} :catchall_25

    #@1c
    .line 450
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 452
    return-void

    #@20
    .line 445
    :cond_20
    const/4 v1, 0x0

    #@21
    :try_start_21
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_24
    .catchall {:try_start_21 .. :try_end_24} :catchall_25

    #@24
    goto :goto_13

    #@25
    .line 450
    :catchall_25
    move-exception v1

    #@26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v1
.end method

.method public dispatchGetNewSurface()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 340
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 342
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 343
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x5

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 346
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 348
    return-void

    #@15
    .line 346
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public dispatchScreenState(Z)V
    .registers 7
    .parameter "on"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 351
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 353
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.IWindow"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 354
    if-eqz p1, :cond_1b

    #@c
    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 355
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x6

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1d

    #@17
    .line 358
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 360
    return-void

    #@1b
    .line 354
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_c

    #@1d
    .line 358
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method

.method public dispatchSystemUiVisibilityChanged(IIII)V
    .registers 10
    .parameter "seq"
    .parameter "globalVisibility"
    .parameter "localValue"
    .parameter "localChanges"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 458
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 460
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 461
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 462
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 463
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 464
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 465
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v2, 0xc

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_22

    #@1e
    .line 468
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 470
    return-void

    #@22
    .line 468
    :catchall_22
    move-exception v1

    #@23
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v1
.end method

.method public dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)V
    .registers 12
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"
    .parameter "sync"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 411
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 413
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "android.view.IWindow"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 414
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    .line 415
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 416
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 417
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 418
    if-eqz p5, :cond_33

    #@19
    .line 419
    const/4 v3, 0x1

    #@1a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 420
    const/4 v3, 0x0

    #@1e
    invoke-virtual {p5, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 425
    :goto_21
    if-eqz p6, :cond_3d

    #@23
    :goto_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 426
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/16 v2, 0xa

    #@2a
    const/4 v3, 0x0

    #@2b
    const/4 v4, 0x1

    #@2c
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2f
    .catchall {:try_start_6 .. :try_end_2f} :catchall_38

    #@2f
    .line 429
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 431
    return-void

    #@33
    .line 423
    :cond_33
    const/4 v3, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_21

    #@38
    .line 429
    :catchall_38
    move-exception v1

    #@39
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    throw v1

    #@3d
    :cond_3d
    move v1, v2

    #@3e
    .line 425
    goto :goto_23
.end method

.method public dispatchWallpaperOffsets(FFFFZ)V
    .registers 11
    .parameter "x"
    .parameter "y"
    .parameter "xStep"
    .parameter "yStep"
    .parameter "sync"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 395
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 397
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.IWindow"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 398
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeFloat(F)V

    #@d
    .line 399
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeFloat(F)V

    #@10
    .line 400
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeFloat(F)V

    #@13
    .line 401
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeFloat(F)V

    #@16
    .line 402
    if-eqz p5, :cond_28

    #@18
    :goto_18
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 403
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v2, 0x9

    #@1f
    const/4 v3, 0x0

    #@20
    const/4 v4, 0x1

    #@21
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_24
    .catchall {:try_start_5 .. :try_end_24} :catchall_2a

    #@24
    .line 406
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 408
    return-void

    #@28
    .line 402
    :cond_28
    const/4 v1, 0x0

    #@29
    goto :goto_18

    #@2a
    .line 406
    :catchall_2a
    move-exception v1

    #@2b
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v1
.end method

.method public doneAnimating()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 478
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 480
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 481
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/16 v2, 0xd

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v4, 0x1

    #@f
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_16

    #@12
    .line 484
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@15
    .line 486
    return-void

    #@16
    .line 484
    :catchall_16
    move-exception v1

    #@17
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    throw v1
.end method

.method public executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .registers 9
    .parameter "command"
    .parameter "parameters"
    .parameter "descriptor"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 255
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 257
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 258
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 259
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 260
    if-eqz p3, :cond_25

    #@11
    .line 261
    const/4 v1, 0x1

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 262
    const/4 v1, 0x0

    #@16
    invoke-virtual {p3, v0, v1}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 267
    :goto_19
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v2, 0x1

    #@1c
    const/4 v3, 0x0

    #@1d
    const/4 v4, 0x1

    #@1e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_21
    .catchall {:try_start_4 .. :try_end_21} :catchall_2a

    #@21
    .line 270
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 272
    return-void

    #@25
    .line 265
    :cond_25
    const/4 v1, 0x0

    #@26
    :try_start_26
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_2a

    #@29
    goto :goto_19

    #@2a
    .line 270
    :catchall_2a
    move-exception v1

    #@2b
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 242
    const-string v0, "android.view.IWindow"

    #@2
    return-object v0
.end method

.method public moved(II)V
    .registers 8
    .parameter "newX"
    .parameter "newY"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 317
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.view.IWindow"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 318
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 319
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 320
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x3

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 323
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 325
    return-void

    #@1b
    .line 323
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method

.method public resized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V
    .registers 11
    .parameter "frame"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "reportDraw"
    .parameter "newConfig"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 275
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 277
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "android.view.IWindow"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 278
    if-eqz p1, :cond_44

    #@d
    .line 279
    const/4 v3, 0x1

    #@e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 280
    const/4 v3, 0x0

    #@12
    invoke-virtual {p1, v0, v3}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    .line 285
    :goto_15
    if-eqz p2, :cond_4e

    #@17
    .line 286
    const/4 v3, 0x1

    #@18
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 287
    const/4 v3, 0x0

    #@1c
    invoke-virtual {p2, v0, v3}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@1f
    .line 292
    :goto_1f
    if-eqz p3, :cond_53

    #@21
    .line 293
    const/4 v3, 0x1

    #@22
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 294
    const/4 v3, 0x0

    #@26
    invoke-virtual {p3, v0, v3}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@29
    .line 299
    :goto_29
    if-eqz p4, :cond_58

    #@2b
    :goto_2b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 300
    if-eqz p5, :cond_5a

    #@30
    .line 301
    const/4 v1, 0x1

    #@31
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 302
    const/4 v1, 0x0

    #@35
    invoke-virtual {p5, v0, v1}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@38
    .line 307
    :goto_38
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@3a
    const/4 v2, 0x2

    #@3b
    const/4 v3, 0x0

    #@3c
    const/4 v4, 0x1

    #@3d
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_40
    .catchall {:try_start_6 .. :try_end_40} :catchall_49

    #@40
    .line 310
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    .line 312
    return-void

    #@44
    .line 283
    :cond_44
    const/4 v3, 0x0

    #@45
    :try_start_45
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_48
    .catchall {:try_start_45 .. :try_end_48} :catchall_49

    #@48
    goto :goto_15

    #@49
    .line 310
    :catchall_49
    move-exception v1

    #@4a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4d
    throw v1

    #@4e
    .line 290
    :cond_4e
    const/4 v3, 0x0

    #@4f
    :try_start_4f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@52
    goto :goto_1f

    #@53
    .line 297
    :cond_53
    const/4 v3, 0x0

    #@54
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    goto :goto_29

    #@58
    :cond_58
    move v1, v2

    #@59
    .line 299
    goto :goto_2b

    #@5a
    .line 305
    :cond_5a
    const/4 v1, 0x0

    #@5b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_5e
    .catchall {:try_start_4f .. :try_end_5e} :catchall_49

    #@5e
    goto :goto_38
.end method

.method public windowFocusChanged(ZZ)V
    .registers 8
    .parameter "hasFocus"
    .parameter "inTouchMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 367
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 369
    .local v0, _data:Landroid/os/Parcel;
    :try_start_6
    const-string v3, "android.view.IWindow"

    #@8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@b
    .line 370
    if-eqz p1, :cond_22

    #@d
    move v3, v1

    #@e
    :goto_e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 371
    if-eqz p2, :cond_24

    #@13
    :goto_13
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 372
    iget-object v1, p0, Landroid/view/IWindow$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v2, 0x7

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_6 .. :try_end_1e} :catchall_26

    #@1e
    .line 375
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 377
    return-void

    #@22
    :cond_22
    move v3, v2

    #@23
    .line 370
    goto :goto_e

    #@24
    :cond_24
    move v1, v2

    #@25
    .line 371
    goto :goto_13

    #@26
    .line 375
    :catchall_26
    move-exception v1

    #@27
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v1
.end method
