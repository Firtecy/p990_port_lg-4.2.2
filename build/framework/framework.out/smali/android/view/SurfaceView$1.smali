.class Landroid/view/SurfaceView$1;
.super Landroid/os/Handler;
.source "SurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/SurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/SurfaceView;


# direct methods
.method constructor <init>(Landroid/view/SurfaceView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 132
    iput-object p1, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 135
    iget v2, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_52

    #@7
    .line 159
    :goto_7
    return-void

    #@8
    .line 137
    :pswitch_8
    iget-object v2, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@a
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@c
    if-eqz v3, :cond_12

    #@e
    :goto_e
    invoke-virtual {v2, v0}, Landroid/view/SurfaceView;->setKeepScreenOn(Z)V

    #@11
    goto :goto_7

    #@12
    :cond_12
    move v0, v1

    #@13
    goto :goto_e

    #@14
    .line 140
    :pswitch_14
    iget-object v0, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@16
    invoke-virtual {v0}, Landroid/view/SurfaceView;->handleGetNewSurface()V

    #@19
    goto :goto_7

    #@1a
    .line 143
    :pswitch_1a
    iget-object v0, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@1c
    invoke-static {v0, v1, v1}, Landroid/view/SurfaceView;->access$000(Landroid/view/SurfaceView;ZZ)V

    #@1f
    goto :goto_7

    #@20
    .line 148
    :pswitch_20
    iget-object v2, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@22
    iget-object v2, v2, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@24
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@27
    .line 150
    :try_start_27
    iget-object v3, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@29
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@2b
    if-eqz v2, :cond_45

    #@2d
    move v2, v0

    #@2e
    :goto_2e
    iput-boolean v2, v3, Landroid/view/SurfaceView;->mReportDrawNeeded:Z

    #@30
    .line 151
    iget-object v2, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@32
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@34
    if-eqz v3, :cond_47

    #@36
    :goto_36
    iput-boolean v0, v2, Landroid/view/SurfaceView;->mUpdateWindowNeeded:Z
    :try_end_38
    .catchall {:try_start_27 .. :try_end_38} :catchall_49

    #@38
    .line 153
    iget-object v0, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@3a
    iget-object v0, v0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@3c
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@3f
    .line 155
    iget-object v0, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@41
    invoke-static {v0, v1, v1}, Landroid/view/SurfaceView;->access$000(Landroid/view/SurfaceView;ZZ)V

    #@44
    goto :goto_7

    #@45
    :cond_45
    move v2, v1

    #@46
    .line 150
    goto :goto_2e

    #@47
    :cond_47
    move v0, v1

    #@48
    .line 151
    goto :goto_36

    #@49
    .line 153
    :catchall_49
    move-exception v0

    #@4a
    iget-object v1, p0, Landroid/view/SurfaceView$1;->this$0:Landroid/view/SurfaceView;

    #@4c
    iget-object v1, v1, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@4e
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@51
    throw v0

    #@52
    .line 135
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_8
        :pswitch_14
        :pswitch_1a
        :pswitch_20
    .end packed-switch
.end method
