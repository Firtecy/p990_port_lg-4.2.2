.class public abstract Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;
.super Ljava/lang/Object;
.source "AccessibilityIterators.java"

# interfaces
.implements Landroid/view/AccessibilityIterators$TextSegmentIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/AccessibilityIterators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AbstractTextSegmentIterator"
.end annotation


# instance fields
.field private final mSegment:[I

.field protected mText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [I

    #@6
    iput-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mSegment:[I

    #@8
    return-void
.end method


# virtual methods
.method protected getRange(II)[I
    .registers 5
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 60
    if-ltz p1, :cond_6

    #@2
    if-ltz p2, :cond_6

    #@4
    if-ne p1, p2, :cond_8

    #@6
    .line 61
    :cond_6
    const/4 v0, 0x0

    #@7
    .line 65
    :goto_7
    return-object v0

    #@8
    .line 63
    :cond_8
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mSegment:[I

    #@a
    const/4 v1, 0x0

    #@b
    aput p1, v0, v1

    #@d
    .line 64
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mSegment:[I

    #@f
    const/4 v1, 0x1

    #@10
    aput p2, v0, v1

    #@12
    .line 65
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mSegment:[I

    #@14
    goto :goto_7
.end method

.method public initialize(Ljava/lang/String;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 56
    iput-object p1, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@2
    .line 57
    return-void
.end method
