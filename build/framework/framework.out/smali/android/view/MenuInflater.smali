.class public Landroid/view/MenuInflater;
.super Ljava/lang/Object;
.source "MenuInflater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/MenuInflater$MenuState;,
        Landroid/view/MenuInflater$InflatedOnMenuItemClickListener;
    }
.end annotation


# static fields
.field private static final ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final ACTION_VIEW_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String; = "MenuInflater"

.field private static final NO_ID:I = 0x0

.field private static final XML_GROUP:Ljava/lang/String; = "group"

.field private static final XML_ITEM:Ljava/lang/String; = "item"

.field private static final XML_MENU:Ljava/lang/String; = "menu"


# instance fields
.field private final mActionProviderConstructorArguments:[Ljava/lang/Object;

.field private final mActionViewConstructorArguments:[Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mRealOwner:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 59
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/Class;

    #@3
    const/4 v1, 0x0

    #@4
    const-class v2, Landroid/content/Context;

    #@6
    aput-object v2, v0, v1

    #@8
    sput-object v0, Landroid/view/MenuInflater;->ACTION_VIEW_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    #@a
    .line 61
    sget-object v0, Landroid/view/MenuInflater;->ACTION_VIEW_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    #@c
    sput-object v0, Landroid/view/MenuInflater;->ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    #@e
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    iput-object p1, p0, Landroid/view/MenuInflater;->mContext:Landroid/content/Context;

    #@5
    .line 77
    iput-object p1, p0, Landroid/view/MenuInflater;->mRealOwner:Ljava/lang/Object;

    #@7
    .line 78
    const/4 v0, 0x1

    #@8
    new-array v0, v0, [Ljava/lang/Object;

    #@a
    const/4 v1, 0x0

    #@b
    aput-object p1, v0, v1

    #@d
    iput-object v0, p0, Landroid/view/MenuInflater;->mActionViewConstructorArguments:[Ljava/lang/Object;

    #@f
    .line 79
    iget-object v0, p0, Landroid/view/MenuInflater;->mActionViewConstructorArguments:[Ljava/lang/Object;

    #@11
    iput-object v0, p0, Landroid/view/MenuInflater;->mActionProviderConstructorArguments:[Ljava/lang/Object;

    #@13
    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 5
    .parameter "context"
    .parameter "realOwner"

    #@0
    .prologue
    .line 88
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 89
    iput-object p1, p0, Landroid/view/MenuInflater;->mContext:Landroid/content/Context;

    #@5
    .line 90
    iput-object p2, p0, Landroid/view/MenuInflater;->mRealOwner:Ljava/lang/Object;

    #@7
    .line 91
    const/4 v0, 0x1

    #@8
    new-array v0, v0, [Ljava/lang/Object;

    #@a
    const/4 v1, 0x0

    #@b
    aput-object p1, v0, v1

    #@d
    iput-object v0, p0, Landroid/view/MenuInflater;->mActionViewConstructorArguments:[Ljava/lang/Object;

    #@f
    .line 92
    iget-object v0, p0, Landroid/view/MenuInflater;->mActionViewConstructorArguments:[Ljava/lang/Object;

    #@11
    iput-object v0, p0, Landroid/view/MenuInflater;->mActionProviderConstructorArguments:[Ljava/lang/Object;

    #@13
    .line 93
    return-void
.end method

.method static synthetic access$100(Landroid/view/MenuInflater;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/view/MenuInflater;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/Class;
    .registers 1

    #@0
    .prologue
    .line 45
    sget-object v0, Landroid/view/MenuInflater;->ACTION_PROVIDER_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/view/MenuInflater;)[Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/view/MenuInflater;->mActionProviderConstructorArguments:[Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/view/MenuInflater;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/view/MenuInflater;->mRealOwner:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$500()[Ljava/lang/Class;
    .registers 1

    #@0
    .prologue
    .line 45
    sget-object v0, Landroid/view/MenuInflater;->ACTION_VIEW_CONSTRUCTOR_SIGNATURE:[Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/view/MenuInflater;)[Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/view/MenuInflater;->mActionViewConstructorArguments:[Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method private parseMenu(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    .registers 14
    .parameter "parser"
    .parameter "attrs"
    .parameter "menu"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 126
    new-instance v2, Landroid/view/MenuInflater$MenuState;

    #@2
    invoke-direct {v2, p0, p3}, Landroid/view/MenuInflater$MenuState;-><init>(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    #@5
    .line 128
    .local v2, menuState:Landroid/view/MenuInflater$MenuState;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    #@8
    move-result v0

    #@9
    .line 130
    .local v0, eventType:I
    const/4 v1, 0x0

    #@a
    .line 131
    .local v1, lookingForEndOfUnknownTag:Z
    const/4 v6, 0x0

    #@b
    .line 135
    .local v6, unknownTagName:Ljava/lang/String;
    :cond_b
    const/4 v7, 0x2

    #@c
    if-ne v0, v7, :cond_43

    #@e
    .line 136
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@11
    move-result-object v5

    #@12
    .line 137
    .local v5, tagName:Ljava/lang/String;
    const-string/jumbo v7, "menu"

    #@15
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v7

    #@19
    if-eqz v7, :cond_2a

    #@1b
    .line 139
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@1e
    move-result v0

    #@1f
    .line 148
    .end local v5           #tagName:Ljava/lang/String;
    :goto_1f
    const/4 v3, 0x0

    #@20
    .line 149
    .local v3, reachedEndOfMenu:Z
    :goto_20
    if-nez v3, :cond_d6

    #@22
    .line 150
    packed-switch v0, :pswitch_data_d8

    #@25
    .line 200
    :cond_25
    :goto_25
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@28
    move-result v0

    #@29
    goto :goto_20

    #@2a
    .line 143
    .end local v3           #reachedEndOfMenu:Z
    .restart local v5       #tagName:Ljava/lang/String;
    :cond_2a
    new-instance v7, Ljava/lang/RuntimeException;

    #@2c
    new-instance v8, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v9, "Expecting menu, got "

    #@33
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v8

    #@3f
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@42
    throw v7

    #@43
    .line 145
    .end local v5           #tagName:Ljava/lang/String;
    :cond_43
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@46
    move-result v0

    #@47
    .line 146
    const/4 v7, 0x1

    #@48
    if-ne v0, v7, :cond_b

    #@4a
    goto :goto_1f

    #@4b
    .line 152
    .restart local v3       #reachedEndOfMenu:Z
    :pswitch_4b
    if-nez v1, :cond_25

    #@4d
    .line 156
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    .line 157
    .restart local v5       #tagName:Ljava/lang/String;
    const-string v7, "group"

    #@53
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v7

    #@57
    if-eqz v7, :cond_5d

    #@59
    .line 158
    invoke-virtual {v2, p2}, Landroid/view/MenuInflater$MenuState;->readGroup(Landroid/util/AttributeSet;)V

    #@5c
    goto :goto_25

    #@5d
    .line 159
    :cond_5d
    const-string/jumbo v7, "item"

    #@60
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v7

    #@64
    if-eqz v7, :cond_6a

    #@66
    .line 160
    invoke-virtual {v2, p2}, Landroid/view/MenuInflater$MenuState;->readItem(Landroid/util/AttributeSet;)V

    #@69
    goto :goto_25

    #@6a
    .line 161
    :cond_6a
    const-string/jumbo v7, "menu"

    #@6d
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@70
    move-result v7

    #@71
    if-eqz v7, :cond_7b

    #@73
    .line 163
    invoke-virtual {v2}, Landroid/view/MenuInflater$MenuState;->addSubMenuItem()Landroid/view/SubMenu;

    #@76
    move-result-object v4

    #@77
    .line 166
    .local v4, subMenu:Landroid/view/SubMenu;
    invoke-direct {p0, p1, p2, v4}, Landroid/view/MenuInflater;->parseMenu(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V

    #@7a
    goto :goto_25

    #@7b
    .line 168
    .end local v4           #subMenu:Landroid/view/SubMenu;
    :cond_7b
    const/4 v1, 0x1

    #@7c
    .line 169
    move-object v6, v5

    #@7d
    .line 171
    goto :goto_25

    #@7e
    .line 174
    .end local v5           #tagName:Ljava/lang/String;
    :pswitch_7e
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    .line 175
    .restart local v5       #tagName:Ljava/lang/String;
    if-eqz v1, :cond_8d

    #@84
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v7

    #@88
    if-eqz v7, :cond_8d

    #@8a
    .line 176
    const/4 v1, 0x0

    #@8b
    .line 177
    const/4 v6, 0x0

    #@8c
    goto :goto_25

    #@8d
    .line 178
    :cond_8d
    const-string v7, "group"

    #@8f
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v7

    #@93
    if-eqz v7, :cond_99

    #@95
    .line 179
    invoke-virtual {v2}, Landroid/view/MenuInflater$MenuState;->resetGroup()V

    #@98
    goto :goto_25

    #@99
    .line 180
    :cond_99
    const-string/jumbo v7, "item"

    #@9c
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9f
    move-result v7

    #@a0
    if-eqz v7, :cond_c2

    #@a2
    .line 183
    invoke-virtual {v2}, Landroid/view/MenuInflater$MenuState;->hasAddedItem()Z

    #@a5
    move-result v7

    #@a6
    if-nez v7, :cond_25

    #@a8
    .line 184
    invoke-static {v2}, Landroid/view/MenuInflater$MenuState;->access$000(Landroid/view/MenuInflater$MenuState;)Landroid/view/ActionProvider;

    #@ab
    move-result-object v7

    #@ac
    if-eqz v7, :cond_bd

    #@ae
    invoke-static {v2}, Landroid/view/MenuInflater$MenuState;->access$000(Landroid/view/MenuInflater$MenuState;)Landroid/view/ActionProvider;

    #@b1
    move-result-object v7

    #@b2
    invoke-virtual {v7}, Landroid/view/ActionProvider;->hasSubMenu()Z

    #@b5
    move-result v7

    #@b6
    if-eqz v7, :cond_bd

    #@b8
    .line 186
    invoke-virtual {v2}, Landroid/view/MenuInflater$MenuState;->addSubMenuItem()Landroid/view/SubMenu;

    #@bb
    goto/16 :goto_25

    #@bd
    .line 188
    :cond_bd
    invoke-virtual {v2}, Landroid/view/MenuInflater$MenuState;->addItem()V

    #@c0
    goto/16 :goto_25

    #@c2
    .line 191
    :cond_c2
    const-string/jumbo v7, "menu"

    #@c5
    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v7

    #@c9
    if-eqz v7, :cond_25

    #@cb
    .line 192
    const/4 v3, 0x1

    #@cc
    goto/16 :goto_25

    #@ce
    .line 197
    .end local v5           #tagName:Ljava/lang/String;
    :pswitch_ce
    new-instance v7, Ljava/lang/RuntimeException;

    #@d0
    const-string v8, "Unexpected end of document"

    #@d2
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d5
    throw v7

    #@d6
    .line 202
    :cond_d6
    return-void

    #@d7
    .line 150
    nop

    #@d8
    :pswitch_data_d8
    .packed-switch 0x1
        :pswitch_ce
        :pswitch_4b
        :pswitch_7e
    .end packed-switch
.end method


# virtual methods
.method public inflate(ILandroid/view/Menu;)V
    .registers 8
    .parameter "menuRes"
    .parameter "menu"

    #@0
    .prologue
    .line 105
    const/4 v2, 0x0

    #@1
    .line 107
    .local v2, parser:Landroid/content/res/XmlResourceParser;
    :try_start_1
    iget-object v3, p0, Landroid/view/MenuInflater;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    #@a
    move-result-object v2

    #@b
    .line 108
    invoke-static {v2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@e
    move-result-object v0

    #@f
    .line 110
    .local v0, attrs:Landroid/util/AttributeSet;
    invoke-direct {p0, v2, v0, p2}, Landroid/view/MenuInflater;->parseMenu(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_21
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_12} :catch_18
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_12} :catch_28

    #@12
    .line 116
    if-eqz v2, :cond_17

    #@14
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->close()V

    #@17
    .line 118
    :cond_17
    return-void

    #@18
    .line 111
    .end local v0           #attrs:Landroid/util/AttributeSet;
    :catch_18
    move-exception v1

    #@19
    .line 112
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_19
    new-instance v3, Landroid/view/InflateException;

    #@1b
    const-string v4, "Error inflating menu XML"

    #@1d
    invoke-direct {v3, v4, v1}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@20
    throw v3
    :try_end_21
    .catchall {:try_start_19 .. :try_end_21} :catchall_21

    #@21
    .line 116
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_21
    move-exception v3

    #@22
    if-eqz v2, :cond_27

    #@24
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->close()V

    #@27
    :cond_27
    throw v3

    #@28
    .line 113
    :catch_28
    move-exception v1

    #@29
    .line 114
    .local v1, e:Ljava/io/IOException;
    :try_start_29
    new-instance v3, Landroid/view/InflateException;

    #@2b
    const-string v4, "Error inflating menu XML"

    #@2d
    invoke-direct {v3, v4, v1}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@30
    throw v3
    :try_end_31
    .catchall {:try_start_29 .. :try_end_31} :catchall_21
.end method
