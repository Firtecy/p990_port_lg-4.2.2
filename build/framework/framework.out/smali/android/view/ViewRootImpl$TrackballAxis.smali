.class final Landroid/view/ViewRootImpl$TrackballAxis;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TrackballAxis"
.end annotation


# static fields
.field static final ACCEL_MOVE_SCALING_FACTOR:F = 0.025f

.field static final FAST_MOVE_TIME:J = 0x96L

.field static final MAX_ACCELERATION:F = 20.0f


# instance fields
.field absPosition:F

.field acceleration:F

.field dir:I

.field lastMoveTime:J

.field nonAccelMovement:I

.field position:F

.field step:I


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 5208
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 5231
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@7
    .line 5232
    const-wide/16 v0, 0x0

    #@9
    iput-wide v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    #@b
    return-void
.end method


# virtual methods
.method collect(FJLjava/lang/String;)F
    .registers 13
    .parameter "off"
    .parameter "time"
    .parameter "axis"

    #@0
    .prologue
    .line 5257
    const/4 v6, 0x0

    #@1
    cmpl-float v6, p1, v6

    #@3
    if-lez v6, :cond_55

    #@5
    .line 5258
    const/high16 v6, 0x4316

    #@7
    mul-float/2addr v6, p1

    #@8
    float-to-long v3, v6

    #@9
    .line 5259
    .local v3, normTime:J
    iget v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    #@b
    if-gez v6, :cond_1b

    #@d
    .line 5261
    const/4 v6, 0x0

    #@e
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@10
    .line 5262
    const/4 v6, 0x0

    #@11
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    #@13
    .line 5263
    const/high16 v6, 0x3f80

    #@15
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@17
    .line 5264
    const-wide/16 v6, 0x0

    #@19
    iput-wide v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    #@1b
    .line 5266
    :cond_1b
    const/4 v6, 0x1

    #@1c
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    #@1e
    .line 5284
    :goto_1e
    const-wide/16 v6, 0x0

    #@20
    cmp-long v6, v3, v6

    #@22
    if-lez v6, :cond_47

    #@24
    .line 5285
    iget-wide v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    #@26
    sub-long v1, p2, v6

    #@28
    .line 5286
    .local v1, delta:J
    iput-wide p2, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    #@2a
    .line 5287
    iget v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@2c
    .line 5288
    .local v0, acc:F
    cmp-long v6, v1, v3

    #@2e
    if-gez v6, :cond_7b

    #@30
    .line 5290
    sub-long v6, v3, v1

    #@32
    long-to-float v6, v6

    #@33
    const v7, 0x3ccccccd

    #@36
    mul-float v5, v6, v7

    #@38
    .line 5291
    .local v5, scale:F
    const/high16 v6, 0x3f80

    #@3a
    cmpl-float v6, v5, v6

    #@3c
    if-lez v6, :cond_3f

    #@3e
    mul-float/2addr v0, v5

    #@3f
    .line 5295
    :cond_3f
    const/high16 v6, 0x41a0

    #@41
    cmpg-float v6, v0, v6

    #@43
    if-gez v6, :cond_78

    #@45
    .end local v0           #acc:F
    :goto_45
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@47
    .line 5306
    .end local v1           #delta:J
    .end local v5           #scale:F
    :cond_47
    :goto_47
    iget v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@49
    add-float/2addr v6, p1

    #@4a
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@4c
    .line 5307
    iget v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@4e
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    #@51
    move-result v6

    #@52
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->absPosition:F

    #@54
    return v6

    #@55
    .line 5267
    .end local v3           #normTime:J
    :cond_55
    const/4 v6, 0x0

    #@56
    cmpg-float v6, p1, v6

    #@58
    if-gez v6, :cond_75

    #@5a
    .line 5268
    neg-float v6, p1

    #@5b
    const/high16 v7, 0x4316

    #@5d
    mul-float/2addr v6, v7

    #@5e
    float-to-long v3, v6

    #@5f
    .line 5269
    .restart local v3       #normTime:J
    iget v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    #@61
    if-lez v6, :cond_71

    #@63
    .line 5271
    const/4 v6, 0x0

    #@64
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@66
    .line 5272
    const/4 v6, 0x0

    #@67
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    #@69
    .line 5273
    const/high16 v6, 0x3f80

    #@6b
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@6d
    .line 5274
    const-wide/16 v6, 0x0

    #@6f
    iput-wide v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    #@71
    .line 5276
    :cond_71
    const/4 v6, -0x1

    #@72
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    #@74
    goto :goto_1e

    #@75
    .line 5278
    .end local v3           #normTime:J
    :cond_75
    const-wide/16 v3, 0x0

    #@77
    .restart local v3       #normTime:J
    goto :goto_1e

    #@78
    .line 5295
    .restart local v0       #acc:F
    .restart local v1       #delta:J
    .restart local v5       #scale:F
    :cond_78
    const/high16 v0, 0x41a0

    #@7a
    goto :goto_45

    #@7b
    .line 5298
    .end local v5           #scale:F
    :cond_7b
    sub-long v6, v1, v3

    #@7d
    long-to-float v6, v6

    #@7e
    const v7, 0x3ccccccd

    #@81
    mul-float v5, v6, v7

    #@83
    .line 5299
    .restart local v5       #scale:F
    const/high16 v6, 0x3f80

    #@85
    cmpl-float v6, v5, v6

    #@87
    if-lez v6, :cond_8a

    #@89
    div-float/2addr v0, v5

    #@8a
    .line 5303
    :cond_8a
    const/high16 v6, 0x3f80

    #@8c
    cmpl-float v6, v0, v6

    #@8e
    if-lez v6, :cond_93

    #@90
    .end local v0           #acc:F
    :goto_90
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@92
    goto :goto_47

    #@93
    .restart local v0       #acc:F
    :cond_93
    const/high16 v0, 0x3f80

    #@95
    goto :goto_90
.end method

.method generate(F)I
    .registers 10
    .parameter "precision"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/high16 v5, 0x4000

    #@3
    const/high16 v6, 0x3f80

    #@5
    .line 5322
    const/4 v2, 0x0

    #@6
    .line 5323
    .local v2, movement:I
    const/4 v4, 0x0

    #@7
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    #@9
    .line 5325
    :goto_9
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@b
    const/4 v7, 0x0

    #@c
    cmpl-float v4, v4, v7

    #@e
    if-ltz v4, :cond_1d

    #@10
    move v1, v3

    #@11
    .line 5326
    .local v1, dir:I
    :goto_11
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    #@13
    packed-switch v4, :pswitch_data_78

    #@16
    .line 5359
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->absPosition:F

    #@18
    cmpg-float v4, v4, v6

    #@1a
    if-gez v4, :cond_51

    #@1c
    .line 5360
    :cond_1c
    return v2

    #@1d
    .line 5325
    .end local v1           #dir:I
    :cond_1d
    const/4 v1, -0x1

    #@1e
    goto :goto_11

    #@1f
    .line 5331
    .restart local v1       #dir:I
    :pswitch_1f
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->absPosition:F

    #@21
    cmpg-float v4, v4, p1

    #@23
    if-ltz v4, :cond_1c

    #@25
    .line 5334
    add-int/2addr v2, v1

    #@26
    .line 5335
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    #@28
    add-int/2addr v4, v1

    #@29
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    #@2b
    .line 5336
    iput v3, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    #@2d
    goto :goto_9

    #@2e
    .line 5342
    :pswitch_2e
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->absPosition:F

    #@30
    cmpg-float v4, v4, v5

    #@32
    if-ltz v4, :cond_1c

    #@34
    .line 5345
    add-int/2addr v2, v1

    #@35
    .line 5346
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    #@37
    add-int/2addr v4, v1

    #@38
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    #@3a
    .line 5347
    iget v7, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@3c
    if-lez v1, :cond_4f

    #@3e
    const/high16 v4, -0x4000

    #@40
    :goto_40
    add-float/2addr v4, v7

    #@41
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@43
    .line 5348
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@45
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    #@48
    move-result v4

    #@49
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->absPosition:F

    #@4b
    .line 5349
    const/4 v4, 0x2

    #@4c
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    #@4e
    goto :goto_9

    #@4f
    :cond_4f
    move v4, v5

    #@50
    .line 5347
    goto :goto_40

    #@51
    .line 5362
    :cond_51
    add-int/2addr v2, v1

    #@52
    .line 5363
    iget v7, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@54
    if-ltz v1, :cond_72

    #@56
    const/high16 v4, -0x4080

    #@58
    :goto_58
    add-float/2addr v4, v7

    #@59
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@5b
    .line 5364
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@5d
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    #@60
    move-result v4

    #@61
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->absPosition:F

    #@63
    .line 5365
    iget v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@65
    .line 5366
    .local v0, acc:F
    const v4, 0x3f8ccccd

    #@68
    mul-float/2addr v0, v4

    #@69
    .line 5367
    const/high16 v4, 0x41a0

    #@6b
    cmpg-float v4, v0, v4

    #@6d
    if-gez v4, :cond_74

    #@6f
    .end local v0           #acc:F
    :goto_6f
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@71
    goto :goto_9

    #@72
    :cond_72
    move v4, v6

    #@73
    .line 5363
    goto :goto_58

    #@74
    .line 5367
    .restart local v0       #acc:F
    :cond_74
    iget v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@76
    goto :goto_6f

    #@77
    .line 5326
    nop

    #@78
    :pswitch_data_78
    .packed-switch 0x0
        :pswitch_1f
        :pswitch_2e
    .end packed-switch
.end method

.method reset(I)V
    .registers 4
    .parameter "_step"

    #@0
    .prologue
    .line 5238
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    #@3
    .line 5239
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    #@7
    .line 5240
    const-wide/16 v0, 0x0

    #@9
    iput-wide v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    #@b
    .line 5241
    iput p1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    #@d
    .line 5242
    const/4 v0, 0x0

    #@e
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    #@10
    .line 5243
    return-void
.end method
