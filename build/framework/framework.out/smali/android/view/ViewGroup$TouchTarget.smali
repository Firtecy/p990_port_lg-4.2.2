.class final Landroid/view/ViewGroup$TouchTarget;
.super Ljava/lang/Object;
.source "ViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TouchTarget"
.end annotation


# static fields
.field public static final ALL_POINTER_IDS:I = -0x1

.field private static final MAX_RECYCLED:I = 0x20

.field private static sRecycleBin:Landroid/view/ViewGroup$TouchTarget;

.field private static final sRecycleLock:Ljava/lang/Object;

.field private static sRecycledCount:I


# instance fields
.field public child:Landroid/view/View;

.field public next:Landroid/view/ViewGroup$TouchTarget;

.field public pointerIdBits:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 6067
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/ViewGroup$TouchTarget;->sRecycleLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 6082
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 6083
    return-void
.end method

.method public static obtain(Landroid/view/View;I)Landroid/view/ViewGroup$TouchTarget;
    .registers 5
    .parameter "child"
    .parameter "pointerIdBits"

    #@0
    .prologue
    .line 6087
    sget-object v2, Landroid/view/ViewGroup$TouchTarget;->sRecycleLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 6088
    :try_start_3
    sget-object v1, Landroid/view/ViewGroup$TouchTarget;->sRecycleBin:Landroid/view/ViewGroup$TouchTarget;

    #@5
    if-nez v1, :cond_12

    #@7
    .line 6089
    new-instance v0, Landroid/view/ViewGroup$TouchTarget;

    #@9
    invoke-direct {v0}, Landroid/view/ViewGroup$TouchTarget;-><init>()V

    #@c
    .line 6096
    .local v0, target:Landroid/view/ViewGroup$TouchTarget;
    :goto_c
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_22

    #@d
    .line 6097
    iput-object p0, v0, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@f
    .line 6098
    iput p1, v0, Landroid/view/ViewGroup$TouchTarget;->pointerIdBits:I

    #@11
    .line 6099
    return-object v0

    #@12
    .line 6091
    .end local v0           #target:Landroid/view/ViewGroup$TouchTarget;
    :cond_12
    :try_start_12
    sget-object v0, Landroid/view/ViewGroup$TouchTarget;->sRecycleBin:Landroid/view/ViewGroup$TouchTarget;

    #@14
    .line 6092
    .restart local v0       #target:Landroid/view/ViewGroup$TouchTarget;
    iget-object v1, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@16
    sput-object v1, Landroid/view/ViewGroup$TouchTarget;->sRecycleBin:Landroid/view/ViewGroup$TouchTarget;

    #@18
    .line 6093
    sget v1, Landroid/view/ViewGroup$TouchTarget;->sRecycledCount:I

    #@1a
    add-int/lit8 v1, v1, -0x1

    #@1c
    sput v1, Landroid/view/ViewGroup$TouchTarget;->sRecycledCount:I

    #@1e
    .line 6094
    const/4 v1, 0x0

    #@1f
    iput-object v1, v0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@21
    goto :goto_c

    #@22
    .line 6096
    .end local v0           #target:Landroid/view/ViewGroup$TouchTarget;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_12 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method


# virtual methods
.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 6103
    sget-object v1, Landroid/view/ViewGroup$TouchTarget;->sRecycleLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 6104
    :try_start_3
    sget v0, Landroid/view/ViewGroup$TouchTarget;->sRecycledCount:I

    #@5
    const/16 v2, 0x20

    #@7
    if-ge v0, v2, :cond_1a

    #@9
    .line 6105
    sget-object v0, Landroid/view/ViewGroup$TouchTarget;->sRecycleBin:Landroid/view/ViewGroup$TouchTarget;

    #@b
    iput-object v0, p0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@d
    .line 6106
    sput-object p0, Landroid/view/ViewGroup$TouchTarget;->sRecycleBin:Landroid/view/ViewGroup$TouchTarget;

    #@f
    .line 6107
    sget v0, Landroid/view/ViewGroup$TouchTarget;->sRecycledCount:I

    #@11
    add-int/lit8 v0, v0, 0x1

    #@13
    sput v0, Landroid/view/ViewGroup$TouchTarget;->sRecycledCount:I

    #@15
    .line 6111
    :goto_15
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Landroid/view/ViewGroup$TouchTarget;->child:Landroid/view/View;

    #@18
    .line 6112
    monitor-exit v1

    #@19
    .line 6113
    return-void

    #@1a
    .line 6109
    :cond_1a
    const/4 v0, 0x0

    #@1b
    iput-object v0, p0, Landroid/view/ViewGroup$TouchTarget;->next:Landroid/view/ViewGroup$TouchTarget;

    #@1d
    goto :goto_15

    #@1e
    .line 6112
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method
