.class Landroid/view/ViewRootImpl$4;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"

# interfaces
.implements Landroid/view/SurfaceHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 5380
    iput-object p1, p0, Landroid/view/ViewRootImpl$4;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public addCallback(Landroid/view/SurfaceHolder$Callback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 5394
    return-void
.end method

.method public getSurface()Landroid/view/Surface;
    .registers 2

    #@0
    .prologue
    .line 5386
    iget-object v0, p0, Landroid/view/ViewRootImpl$4;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-static {v0}, Landroid/view/ViewRootImpl;->access$400(Landroid/view/ViewRootImpl;)Landroid/view/Surface;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSurfaceFrame()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 5425
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public isCreating()Z
    .registers 2

    #@0
    .prologue
    .line 5390
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public lockCanvas()Landroid/graphics/Canvas;
    .registers 2

    #@0
    .prologue
    .line 5415
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 5419
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 5397
    return-void
.end method

.method public setFixedSize(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 5400
    return-void
.end method

.method public setFormat(I)V
    .registers 2
    .parameter "format"

    #@0
    .prologue
    .line 5406
    return-void
.end method

.method public setKeepScreenOn(Z)V
    .registers 2
    .parameter "screenOn"

    #@0
    .prologue
    .line 5412
    return-void
.end method

.method public setSizeFromLayout()V
    .registers 1

    #@0
    .prologue
    .line 5403
    return-void
.end method

.method public setType(I)V
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 5409
    return-void
.end method

.method public unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 5423
    return-void
.end method
