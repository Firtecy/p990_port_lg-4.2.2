.class Landroid/view/WindowManagerGlobal$1;
.super Ljava/lang/Object;
.source "WindowManagerGlobal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/WindowManagerGlobal;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;Landroid/view/Display;Landroid/view/Window;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/WindowManagerGlobal;


# direct methods
.method constructor <init>(Landroid/view/WindowManagerGlobal;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 186
    iput-object p1, p0, Landroid/view/WindowManagerGlobal$1;->this$0:Landroid/view/WindowManagerGlobal;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 188
    iget-object v4, p0, Landroid/view/WindowManagerGlobal$1;->this$0:Landroid/view/WindowManagerGlobal;

    #@2
    invoke-static {v4}, Landroid/view/WindowManagerGlobal;->access$000(Landroid/view/WindowManagerGlobal;)Ljava/lang/Object;

    #@5
    move-result-object v5

    #@6
    monitor-enter v5

    #@7
    .line 189
    :try_start_7
    iget-object v4, p0, Landroid/view/WindowManagerGlobal$1;->this$0:Landroid/view/WindowManagerGlobal;

    #@9
    invoke-static {v4}, Landroid/view/WindowManagerGlobal;->access$100(Landroid/view/WindowManagerGlobal;)[Landroid/view/ViewRootImpl;

    #@c
    move-result-object v0

    #@d
    .local v0, arr$:[Landroid/view/ViewRootImpl;
    array-length v2, v0

    #@e
    .local v2, len$:I
    const/4 v1, 0x0

    #@f
    .local v1, i$:I
    :goto_f
    if-ge v1, v2, :cond_19

    #@11
    aget-object v3, v0, v1

    #@13
    .line 190
    .local v3, viewRoot:Landroid/view/ViewRootImpl;
    invoke-virtual {v3}, Landroid/view/ViewRootImpl;->loadSystemProperties()V

    #@16
    .line 189
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_f

    #@19
    .line 192
    .end local v3           #viewRoot:Landroid/view/ViewRootImpl;
    :cond_19
    monitor-exit v5

    #@1a
    .line 193
    return-void

    #@1b
    .line 192
    .end local v0           #arr$:[Landroid/view/ViewRootImpl;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_1b
    move-exception v4

    #@1c
    monitor-exit v5
    :try_end_1d
    .catchall {:try_start_7 .. :try_end_1d} :catchall_1b

    #@1d
    throw v4
.end method
