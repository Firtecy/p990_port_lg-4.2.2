.class final Landroid/view/ViewRootImpl$ViewRootHandler;
.super Landroid/os/Handler;
.source "ViewRootImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ViewRootHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2856
    iput-object p1, p0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public getMessageName(Landroid/os/Message;)Ljava/lang/String;
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 2859
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_52

    #@5
    .line 2909
    :pswitch_5
    invoke-super {p0, p1}, Landroid/os/Handler;->getMessageName(Landroid/os/Message;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    :goto_9
    return-object v0

    #@a
    .line 2861
    :pswitch_a
    const-string v0, "MSG_INVALIDATE"

    #@c
    goto :goto_9

    #@d
    .line 2863
    :pswitch_d
    const-string v0, "MSG_INVALIDATE_RECT"

    #@f
    goto :goto_9

    #@10
    .line 2865
    :pswitch_10
    const-string v0, "MSG_DIE"

    #@12
    goto :goto_9

    #@13
    .line 2867
    :pswitch_13
    const-string v0, "MSG_RESIZED"

    #@15
    goto :goto_9

    #@16
    .line 2869
    :pswitch_16
    const-string v0, "MSG_RESIZED_REPORT"

    #@18
    goto :goto_9

    #@19
    .line 2871
    :pswitch_19
    const-string v0, "MSG_WINDOW_FOCUS_CHANGED"

    #@1b
    goto :goto_9

    #@1c
    .line 2873
    :pswitch_1c
    const-string v0, "MSG_DISPATCH_KEY"

    #@1e
    goto :goto_9

    #@1f
    .line 2875
    :pswitch_1f
    const-string v0, "MSG_DISPATCH_APP_VISIBILITY"

    #@21
    goto :goto_9

    #@22
    .line 2877
    :pswitch_22
    const-string v0, "MSG_DISPATCH_GET_NEW_SURFACE"

    #@24
    goto :goto_9

    #@25
    .line 2879
    :pswitch_25
    const-string v0, "MSG_IME_FINISHED_EVENT"

    #@27
    goto :goto_9

    #@28
    .line 2881
    :pswitch_28
    const-string v0, "MSG_DISPATCH_KEY_FROM_IME"

    #@2a
    goto :goto_9

    #@2b
    .line 2883
    :pswitch_2b
    const-string v0, "MSG_FINISH_INPUT_CONNECTION"

    #@2d
    goto :goto_9

    #@2e
    .line 2885
    :pswitch_2e
    const-string v0, "MSG_CHECK_FOCUS"

    #@30
    goto :goto_9

    #@31
    .line 2887
    :pswitch_31
    const-string v0, "MSG_CLOSE_SYSTEM_DIALOGS"

    #@33
    goto :goto_9

    #@34
    .line 2889
    :pswitch_34
    const-string v0, "MSG_DISPATCH_DRAG_EVENT"

    #@36
    goto :goto_9

    #@37
    .line 2891
    :pswitch_37
    const-string v0, "MSG_DISPATCH_DRAG_LOCATION_EVENT"

    #@39
    goto :goto_9

    #@3a
    .line 2893
    :pswitch_3a
    const-string v0, "MSG_DISPATCH_SYSTEM_UI_VISIBILITY"

    #@3c
    goto :goto_9

    #@3d
    .line 2895
    :pswitch_3d
    const-string v0, "MSG_UPDATE_CONFIGURATION"

    #@3f
    goto :goto_9

    #@40
    .line 2897
    :pswitch_40
    const-string v0, "MSG_PROCESS_INPUT_EVENTS"

    #@42
    goto :goto_9

    #@43
    .line 2899
    :pswitch_43
    const-string v0, "MSG_DISPATCH_SCREEN_STATE"

    #@45
    goto :goto_9

    #@46
    .line 2901
    :pswitch_46
    const-string v0, "MSG_INVALIDATE_DISPLAY_LIST"

    #@48
    goto :goto_9

    #@49
    .line 2903
    :pswitch_49
    const-string v0, "MSG_CLEAR_ACCESSIBILITY_FOCUS_HOST"

    #@4b
    goto :goto_9

    #@4c
    .line 2905
    :pswitch_4c
    const-string v0, "MSG_DISPATCH_DONE_ANIMATING"

    #@4e
    goto :goto_9

    #@4f
    .line 2907
    :pswitch_4f
    const-string v0, "MSG_WINDOW_MOVED"

    #@51
    goto :goto_9

    #@52
    .line 2859
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_a
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_16
        :pswitch_19
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
        :pswitch_31
        :pswitch_34
        :pswitch_37
        :pswitch_3a
        :pswitch_3d
        :pswitch_40
        :pswitch_43
        :pswitch_46
        :pswitch_49
        :pswitch_4c
        :pswitch_5
        :pswitch_4f
    .end packed-switch
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 20
    .parameter "msg"

    #@0
    .prologue
    .line 2914
    move-object/from16 v0, p1

    #@2
    iget v2, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_41c

    #@7
    .line 3137
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2916
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v2, Landroid/view/View;

    #@e
    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    #@11
    goto :goto_7

    #@12
    .line 2919
    :pswitch_12
    move-object/from16 v0, p1

    #@14
    iget-object v14, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16
    check-cast v14, Landroid/view/View$AttachInfo$InvalidateInfo;

    #@18
    .line 2920
    .local v14, info:Landroid/view/View$AttachInfo$InvalidateInfo;
    iget-object v2, v14, Landroid/view/View$AttachInfo$InvalidateInfo;->target:Landroid/view/View;

    #@1a
    iget v3, v14, Landroid/view/View$AttachInfo$InvalidateInfo;->left:I

    #@1c
    iget v4, v14, Landroid/view/View$AttachInfo$InvalidateInfo;->top:I

    #@1e
    iget v5, v14, Landroid/view/View$AttachInfo$InvalidateInfo;->right:I

    #@20
    iget v6, v14, Landroid/view/View$AttachInfo$InvalidateInfo;->bottom:I

    #@22
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->invalidate(IIII)V

    #@25
    .line 2921
    invoke-virtual {v14}, Landroid/view/View$AttachInfo$InvalidateInfo;->release()V

    #@28
    goto :goto_7

    #@29
    .line 2924
    .end local v14           #info:Landroid/view/View$AttachInfo$InvalidateInfo;
    :pswitch_29
    move-object/from16 v0, p0

    #@2b
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2d
    move-object/from16 v0, p1

    #@2f
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@31
    move-object/from16 v0, p1

    #@33
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@35
    if-eqz v2, :cond_3c

    #@37
    const/4 v2, 0x1

    #@38
    :goto_38
    invoke-virtual {v3, v4, v2}, Landroid/view/ViewRootImpl;->handleImeFinishedEvent(IZ)V

    #@3b
    goto :goto_7

    #@3c
    :cond_3c
    const/4 v2, 0x0

    #@3d
    goto :goto_38

    #@3e
    .line 2927
    :pswitch_3e
    move-object/from16 v0, p0

    #@40
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@42
    const/4 v3, 0x0

    #@43
    iput-boolean v3, v2, Landroid/view/ViewRootImpl;->mProcessInputEventsScheduled:Z

    #@45
    .line 2928
    move-object/from16 v0, p0

    #@47
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@49
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->doProcessInputEvents()V

    #@4c
    goto :goto_7

    #@4d
    .line 2931
    :pswitch_4d
    move-object/from16 v0, p0

    #@4f
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@51
    move-object/from16 v0, p1

    #@53
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@55
    if-eqz v2, :cond_5c

    #@57
    const/4 v2, 0x1

    #@58
    :goto_58
    invoke-virtual {v3, v2}, Landroid/view/ViewRootImpl;->handleAppVisibility(Z)V

    #@5b
    goto :goto_7

    #@5c
    :cond_5c
    const/4 v2, 0x0

    #@5d
    goto :goto_58

    #@5e
    .line 2934
    :pswitch_5e
    move-object/from16 v0, p0

    #@60
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@62
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->handleGetNewSurface()V

    #@65
    goto :goto_7

    #@66
    .line 2938
    :pswitch_66
    move-object/from16 v0, p1

    #@68
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6a
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@6c
    .line 2939
    .local v7, args:Lcom/android/internal/os/SomeArgs;
    move-object/from16 v0, p0

    #@6e
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@70
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@72
    iget-object v3, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@74
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v2

    #@78
    if-eqz v2, :cond_9a

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@7e
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@80
    iget-object v3, v7, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@82
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v2

    #@86
    if-eqz v2, :cond_9a

    #@88
    move-object/from16 v0, p0

    #@8a
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@8c
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@8e
    iget-object v3, v7, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@90
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v2

    #@94
    if-eqz v2, :cond_9a

    #@96
    iget-object v2, v7, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@98
    if-eqz v2, :cond_7

    #@9a
    .line 2947
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    :cond_9a
    :pswitch_9a
    move-object/from16 v0, p0

    #@9c
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@9e
    iget-boolean v2, v2, Landroid/view/ViewRootImpl;->mAdded:Z

    #@a0
    if-eqz v2, :cond_7

    #@a2
    .line 2948
    move-object/from16 v0, p1

    #@a4
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a6
    check-cast v7, Lcom/android/internal/os/SomeArgs;

    #@a8
    .line 2950
    .restart local v7       #args:Lcom/android/internal/os/SomeArgs;
    iget-object v8, v7, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@aa
    check-cast v8, Landroid/content/res/Configuration;

    #@ac
    .line 2951
    .local v8, config:Landroid/content/res/Configuration;
    if-eqz v8, :cond_b6

    #@ae
    .line 2952
    move-object/from16 v0, p0

    #@b0
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@b2
    const/4 v3, 0x0

    #@b3
    invoke-virtual {v2, v8, v3}, Landroid/view/ViewRootImpl;->updateConfiguration(Landroid/content/res/Configuration;Z)V

    #@b6
    .line 2955
    :cond_b6
    move-object/from16 v0, p0

    #@b8
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@ba
    iget-object v3, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@bc
    iget-object v2, v7, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@be
    check-cast v2, Landroid/graphics/Rect;

    #@c0
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@c3
    .line 2956
    move-object/from16 v0, p0

    #@c5
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@c7
    iget-object v3, v2, Landroid/view/ViewRootImpl;->mPendingContentInsets:Landroid/graphics/Rect;

    #@c9
    iget-object v2, v7, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@cb
    check-cast v2, Landroid/graphics/Rect;

    #@cd
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@d0
    .line 2957
    move-object/from16 v0, p0

    #@d2
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@d4
    iget-object v3, v2, Landroid/view/ViewRootImpl;->mPendingVisibleInsets:Landroid/graphics/Rect;

    #@d6
    iget-object v2, v7, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@d8
    check-cast v2, Landroid/graphics/Rect;

    #@da
    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@dd
    .line 2959
    invoke-virtual {v7}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@e0
    .line 2961
    move-object/from16 v0, p1

    #@e2
    iget v2, v0, Landroid/os/Message;->what:I

    #@e4
    const/4 v3, 0x5

    #@e5
    if-ne v2, v3, :cond_ee

    #@e7
    .line 2962
    move-object/from16 v0, p0

    #@e9
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@eb
    const/4 v3, 0x1

    #@ec
    iput-boolean v3, v2, Landroid/view/ViewRootImpl;->mReportNextDraw:Z

    #@ee
    .line 2965
    :cond_ee
    move-object/from16 v0, p0

    #@f0
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@f2
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@f4
    if-eqz v2, :cond_ff

    #@f6
    .line 2966
    move-object/from16 v0, p0

    #@f8
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@fa
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@fc
    invoke-static {v2}, Landroid/view/ViewRootImpl;->access$100(Landroid/view/View;)V

    #@ff
    .line 2969
    :cond_ff
    move-object/from16 v0, p0

    #@101
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@103
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->requestLayout()V

    #@106
    goto/16 :goto_7

    #@108
    .line 2973
    .end local v7           #args:Lcom/android/internal/os/SomeArgs;
    .end local v8           #config:Landroid/content/res/Configuration;
    :pswitch_108
    move-object/from16 v0, p0

    #@10a
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@10c
    iget-boolean v2, v2, Landroid/view/ViewRootImpl;->mAdded:Z

    #@10e
    if-eqz v2, :cond_7

    #@110
    .line 2974
    move-object/from16 v0, p0

    #@112
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@114
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@116
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    #@119
    move-result v17

    #@11a
    .line 2975
    .local v17, w:I
    move-object/from16 v0, p0

    #@11c
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@11e
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@120
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    #@123
    move-result v11

    #@124
    .line 2976
    .local v11, h:I
    move-object/from16 v0, p1

    #@126
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@128
    .line 2977
    .local v15, l:I
    move-object/from16 v0, p1

    #@12a
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@12c
    move/from16 v16, v0

    #@12e
    .line 2978
    .local v16, t:I
    move-object/from16 v0, p0

    #@130
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@132
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@134
    iput v15, v2, Landroid/graphics/Rect;->left:I

    #@136
    .line 2979
    move-object/from16 v0, p0

    #@138
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@13a
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@13c
    add-int v3, v15, v17

    #@13e
    iput v3, v2, Landroid/graphics/Rect;->right:I

    #@140
    .line 2980
    move-object/from16 v0, p0

    #@142
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@144
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@146
    move/from16 v0, v16

    #@148
    iput v0, v2, Landroid/graphics/Rect;->top:I

    #@14a
    .line 2981
    move-object/from16 v0, p0

    #@14c
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@14e
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWinFrame:Landroid/graphics/Rect;

    #@150
    add-int v3, v16, v11

    #@152
    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    #@154
    .line 2983
    move-object/from16 v0, p0

    #@156
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@158
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@15a
    if-eqz v2, :cond_165

    #@15c
    .line 2984
    move-object/from16 v0, p0

    #@15e
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@160
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@162
    invoke-static {v2}, Landroid/view/ViewRootImpl;->access$100(Landroid/view/View;)V

    #@165
    .line 2986
    :cond_165
    move-object/from16 v0, p0

    #@167
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@169
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->requestLayout()V

    #@16c
    goto/16 :goto_7

    #@16e
    .line 2990
    .end local v11           #h:I
    .end local v15           #l:I
    .end local v16           #t:I
    .end local v17           #w:I
    :pswitch_16e
    move-object/from16 v0, p0

    #@170
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@172
    iget-boolean v2, v2, Landroid/view/ViewRootImpl;->mAdded:Z

    #@174
    if-eqz v2, :cond_7

    #@176
    .line 2991
    move-object/from16 v0, p1

    #@178
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@17a
    if-eqz v2, :cond_2c2

    #@17c
    const/4 v12, 0x1

    #@17d
    .line 2992
    .local v12, hasWindowFocus:Z
    :goto_17d
    move-object/from16 v0, p0

    #@17f
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@181
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@183
    iput-boolean v12, v2, Landroid/view/View$AttachInfo;->mHasWindowFocus:Z

    #@185
    .line 2994
    move-object/from16 v0, p0

    #@187
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@189
    invoke-static {v2, v12}, Landroid/view/ViewRootImpl;->access$200(Landroid/view/ViewRootImpl;Z)V

    #@18c
    .line 2996
    if-eqz v12, :cond_1f2

    #@18e
    .line 2997
    move-object/from16 v0, p1

    #@190
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@192
    if-eqz v2, :cond_2c5

    #@194
    const/4 v13, 0x1

    #@195
    .line 2998
    .local v13, inTouchMode:Z
    :goto_195
    move-object/from16 v0, p0

    #@197
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@199
    invoke-static {v2, v13}, Landroid/view/ViewRootImpl;->access$300(Landroid/view/ViewRootImpl;Z)Z

    #@19c
    .line 3000
    move-object/from16 v0, p0

    #@19e
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1a0
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1a2
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1a4
    if-eqz v2, :cond_1f2

    #@1a6
    move-object/from16 v0, p0

    #@1a8
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1aa
    invoke-static {v2}, Landroid/view/ViewRootImpl;->access$400(Landroid/view/ViewRootImpl;)Landroid/view/Surface;

    #@1ad
    move-result-object v2

    #@1ae
    if-eqz v2, :cond_1f2

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1b4
    invoke-static {v2}, Landroid/view/ViewRootImpl;->access$400(Landroid/view/ViewRootImpl;)Landroid/view/Surface;

    #@1b7
    move-result-object v2

    #@1b8
    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    #@1bb
    move-result v2

    #@1bc
    if-eqz v2, :cond_1f2

    #@1be
    .line 3002
    move-object/from16 v0, p0

    #@1c0
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1c2
    const/4 v3, 0x1

    #@1c3
    iput-boolean v3, v2, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z

    #@1c5
    .line 3004
    :try_start_1c5
    move-object/from16 v0, p0

    #@1c7
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1c9
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1cb
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1cd
    move-object/from16 v0, p0

    #@1cf
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1d1
    iget v3, v3, Landroid/view/ViewRootImpl;->mWidth:I

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v4, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1d7
    iget v4, v4, Landroid/view/ViewRootImpl;->mHeight:I

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iget-object v5, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1dd
    invoke-static {v5}, Landroid/view/ViewRootImpl;->access$500(Landroid/view/ViewRootImpl;)Landroid/view/SurfaceHolder;

    #@1e0
    move-result-object v5

    #@1e1
    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@1e4
    move-result-object v5

    #@1e5
    invoke-virtual {v2, v3, v4, v5}, Landroid/view/HardwareRenderer;->initializeIfNeeded(IILandroid/view/Surface;)Z

    #@1e8
    move-result v2

    #@1e9
    if-eqz v2, :cond_1f2

    #@1eb
    .line 3006
    move-object/from16 v0, p0

    #@1ed
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1ef
    const/4 v3, 0x1

    #@1f0
    iput-boolean v3, v2, Landroid/view/ViewRootImpl;->mFullRedrawNeeded:Z
    :try_end_1f2
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_1c5 .. :try_end_1f2} :catch_2c8

    #@1f2
    .line 3024
    .end local v13           #inTouchMode:Z
    :cond_1f2
    move-object/from16 v0, p0

    #@1f4
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@1fa
    iget-object v3, v3, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@1fc
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1fe
    invoke-static {v3}, Landroid/view/WindowManager$LayoutParams;->mayUseInputMethod(I)Z

    #@201
    move-result v3

    #@202
    iput-boolean v3, v2, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@204
    .line 3027
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@207
    move-result-object v1

    #@208
    .line 3028
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    move-object/from16 v0, p0

    #@20a
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@20c
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@20e
    if-eqz v2, :cond_239

    #@210
    .line 3029
    if-eqz v12, :cond_225

    #@212
    if-eqz v1, :cond_225

    #@214
    move-object/from16 v0, p0

    #@216
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@218
    iget-boolean v2, v2, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@21a
    if-eqz v2, :cond_225

    #@21c
    .line 3030
    move-object/from16 v0, p0

    #@21e
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@220
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@222
    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->startGettingWindowFocus(Landroid/view/View;)V

    #@225
    .line 3032
    :cond_225
    move-object/from16 v0, p0

    #@227
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@229
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@22b
    iget-object v2, v2, Landroid/view/View$AttachInfo;->mKeyDispatchState:Landroid/view/KeyEvent$DispatcherState;

    #@22d
    invoke-virtual {v2}, Landroid/view/KeyEvent$DispatcherState;->reset()V

    #@230
    .line 3033
    move-object/from16 v0, p0

    #@232
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@234
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@236
    invoke-virtual {v2, v12}, Landroid/view/View;->dispatchWindowFocusChanged(Z)V

    #@239
    .line 3038
    :cond_239
    if-eqz v12, :cond_296

    #@23b
    .line 3039
    if-eqz v1, :cond_271

    #@23d
    move-object/from16 v0, p0

    #@23f
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@241
    iget-boolean v2, v2, Landroid/view/ViewRootImpl;->mLastWasImTarget:Z

    #@243
    if-eqz v2, :cond_271

    #@245
    .line 3040
    move-object/from16 v0, p0

    #@247
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@249
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@24b
    move-object/from16 v0, p0

    #@24d
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@24f
    iget-object v3, v3, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@251
    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@254
    move-result-object v3

    #@255
    move-object/from16 v0, p0

    #@257
    iget-object v4, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@259
    iget-object v4, v4, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@25b
    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@25d
    move-object/from16 v0, p0

    #@25f
    iget-object v5, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@261
    iget-boolean v5, v5, Landroid/view/ViewRootImpl;->mHasHadWindowFocus:Z

    #@263
    if-nez v5, :cond_30b

    #@265
    const/4 v5, 0x1

    #@266
    :goto_266
    move-object/from16 v0, p0

    #@268
    iget-object v6, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@26a
    iget-object v6, v6, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@26c
    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@26e
    invoke-virtual/range {v1 .. v6}, Landroid/view/inputmethod/InputMethodManager;->onWindowFocus(Landroid/view/View;Landroid/view/View;IZI)V

    #@271
    .line 3046
    :cond_271
    move-object/from16 v0, p0

    #@273
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@275
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@277
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@279
    and-int/lit16 v3, v3, -0x101

    #@27b
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@27d
    .line 3048
    move-object/from16 v0, p0

    #@27f
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@281
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@283
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@286
    move-result-object v2

    #@287
    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    #@289
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@28b
    and-int/lit16 v3, v3, -0x101

    #@28d
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@28f
    .line 3051
    move-object/from16 v0, p0

    #@291
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@293
    const/4 v3, 0x1

    #@294
    iput-boolean v3, v2, Landroid/view/ViewRootImpl;->mHasHadWindowFocus:Z

    #@296
    .line 3054
    :cond_296
    move-object/from16 v0, p0

    #@298
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@29a
    const/4 v3, 0x0

    #@29b
    const/4 v4, 0x0

    #@29c
    invoke-virtual {v2, v3, v4}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@29f
    .line 3056
    move-object/from16 v0, p0

    #@2a1
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2a3
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2a5
    if-eqz v2, :cond_7

    #@2a7
    move-object/from16 v0, p0

    #@2a9
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2ab
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    #@2ad
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@2b0
    move-result v2

    #@2b1
    if-eqz v2, :cond_7

    #@2b3
    .line 3057
    if-eqz v12, :cond_7

    #@2b5
    .line 3058
    move-object/from16 v0, p0

    #@2b7
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2b9
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@2bb
    const/16 v3, 0x20

    #@2bd
    invoke-virtual {v2, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    #@2c0
    goto/16 :goto_7

    #@2c2
    .line 2991
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v12           #hasWindowFocus:Z
    :cond_2c2
    const/4 v12, 0x0

    #@2c3
    goto/16 :goto_17d

    #@2c5
    .line 2997
    .restart local v12       #hasWindowFocus:Z
    :cond_2c5
    const/4 v13, 0x0

    #@2c6
    goto/16 :goto_195

    #@2c8
    .line 3008
    .restart local v13       #inTouchMode:Z
    :catch_2c8
    move-exception v9

    #@2c9
    .line 3009
    .local v9, e:Landroid/view/Surface$OutOfResourcesException;
    const-string v2, "ViewRootImpl"

    #@2cb
    const-string v3, "OutOfResourcesException locking surface"

    #@2cd
    invoke-static {v2, v3, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2d0
    .line 3011
    :try_start_2d0
    move-object/from16 v0, p0

    #@2d2
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2d4
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@2d6
    move-object/from16 v0, p0

    #@2d8
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@2da
    iget-object v3, v3, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@2dc
    invoke-interface {v2, v3}, Landroid/view/IWindowSession;->outOfMemory(Landroid/view/IWindow;)Z

    #@2df
    move-result v2

    #@2e0
    if-nez v2, :cond_2f0

    #@2e2
    .line 3012
    const-string v2, "ViewRootImpl"

    #@2e4
    const-string v3, "No processes killed for memory; killing self"

    #@2e6
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2e9
    .line 3013
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@2ec
    move-result v2

    #@2ed
    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V
    :try_end_2f0
    .catch Landroid/os/RemoteException; {:try_start_2d0 .. :try_end_2f0} :catch_419

    #@2f0
    .line 3018
    :cond_2f0
    :goto_2f0
    move-object/from16 v0, p1

    #@2f2
    iget v2, v0, Landroid/os/Message;->what:I

    #@2f4
    move-object/from16 v0, p1

    #@2f6
    iget v3, v0, Landroid/os/Message;->arg1:I

    #@2f8
    move-object/from16 v0, p1

    #@2fa
    iget v4, v0, Landroid/os/Message;->arg2:I

    #@2fc
    move-object/from16 v0, p0

    #@2fe
    invoke-virtual {v0, v2, v3, v4}, Landroid/view/ViewRootImpl$ViewRootHandler;->obtainMessage(III)Landroid/os/Message;

    #@301
    move-result-object v2

    #@302
    const-wide/16 v3, 0x1f4

    #@304
    move-object/from16 v0, p0

    #@306
    invoke-virtual {v0, v2, v3, v4}, Landroid/view/ViewRootImpl$ViewRootHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@309
    goto/16 :goto_7

    #@30b
    .line 3040
    .end local v9           #e:Landroid/view/Surface$OutOfResourcesException;
    .end local v13           #inTouchMode:Z
    .restart local v1       #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_30b
    const/4 v5, 0x0

    #@30c
    goto/16 :goto_266

    #@30e
    .line 3065
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v12           #hasWindowFocus:Z
    :pswitch_30e
    move-object/from16 v0, p0

    #@310
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@312
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->doDie()V

    #@315
    goto/16 :goto_7

    #@317
    .line 3068
    :pswitch_317
    move-object/from16 v0, p1

    #@319
    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@31b
    check-cast v10, Landroid/view/KeyEvent;

    #@31d
    .line 3069
    .local v10, event:Landroid/view/KeyEvent;
    move-object/from16 v0, p0

    #@31f
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@321
    const/4 v3, 0x0

    #@322
    const/4 v4, 0x0

    #@323
    const/4 v5, 0x1

    #@324
    invoke-virtual {v2, v10, v3, v4, v5}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V

    #@327
    goto/16 :goto_7

    #@329
    .line 3075
    .end local v10           #event:Landroid/view/KeyEvent;
    :pswitch_329
    move-object/from16 v0, p1

    #@32b
    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@32d
    check-cast v10, Landroid/view/KeyEvent;

    #@32f
    .line 3076
    .restart local v10       #event:Landroid/view/KeyEvent;
    invoke-virtual {v10}, Landroid/view/KeyEvent;->getFlags()I

    #@332
    move-result v2

    #@333
    and-int/lit8 v2, v2, 0x8

    #@335
    if-eqz v2, :cond_341

    #@337
    .line 3080
    invoke-virtual {v10}, Landroid/view/KeyEvent;->getFlags()I

    #@33a
    move-result v2

    #@33b
    and-int/lit8 v2, v2, -0x9

    #@33d
    invoke-static {v10, v2}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@340
    move-result-object v10

    #@341
    .line 3082
    :cond_341
    move-object/from16 v0, p0

    #@343
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@345
    const/4 v3, 0x0

    #@346
    const/4 v4, 0x1

    #@347
    const/4 v5, 0x1

    #@348
    invoke-virtual {v2, v10, v3, v4, v5}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V

    #@34b
    goto/16 :goto_7

    #@34d
    .line 3085
    .end local v10           #event:Landroid/view/KeyEvent;
    :pswitch_34d
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@350
    move-result-object v1

    #@351
    .line 3086
    .restart local v1       #imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_7

    #@353
    .line 3087
    move-object/from16 v0, p1

    #@355
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@357
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@359
    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->reportFinishInputConnection(Landroid/view/inputmethod/InputConnection;)V

    #@35c
    goto/16 :goto_7

    #@35e
    .line 3091
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :pswitch_35e
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    #@361
    move-result-object v1

    #@362
    .line 3092
    .restart local v1       #imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_7

    #@364
    .line 3093
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@367
    goto/16 :goto_7

    #@369
    .line 3097
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :pswitch_369
    move-object/from16 v0, p0

    #@36b
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@36d
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@36f
    if-eqz v2, :cond_7

    #@371
    .line 3098
    move-object/from16 v0, p0

    #@373
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@375
    iget-object v3, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@377
    move-object/from16 v0, p1

    #@379
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@37b
    check-cast v2, Ljava/lang/String;

    #@37d
    invoke-virtual {v3, v2}, Landroid/view/View;->onCloseSystemDialogs(Ljava/lang/String;)V

    #@380
    goto/16 :goto_7

    #@382
    .line 3103
    :pswitch_382
    move-object/from16 v0, p1

    #@384
    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@386
    check-cast v10, Landroid/view/DragEvent;

    #@388
    .line 3104
    .local v10, event:Landroid/view/DragEvent;
    move-object/from16 v0, p0

    #@38a
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@38c
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mLocalDragState:Ljava/lang/Object;

    #@38e
    iput-object v2, v10, Landroid/view/DragEvent;->mLocalState:Ljava/lang/Object;

    #@390
    .line 3105
    move-object/from16 v0, p0

    #@392
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@394
    invoke-static {v2, v10}, Landroid/view/ViewRootImpl;->access$600(Landroid/view/ViewRootImpl;Landroid/view/DragEvent;)V

    #@397
    goto/16 :goto_7

    #@399
    .line 3108
    .end local v10           #event:Landroid/view/DragEvent;
    :pswitch_399
    move-object/from16 v0, p0

    #@39b
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@39d
    move-object/from16 v0, p1

    #@39f
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3a1
    check-cast v2, Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;

    #@3a3
    invoke-virtual {v3, v2}, Landroid/view/ViewRootImpl;->handleDispatchSystemUiVisibilityChanged(Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;)V

    #@3a6
    goto/16 :goto_7

    #@3a8
    .line 3111
    :pswitch_3a8
    move-object/from16 v0, p1

    #@3aa
    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3ac
    check-cast v8, Landroid/content/res/Configuration;

    #@3ae
    .line 3112
    .restart local v8       #config:Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    #@3b0
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3b2
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@3b4
    invoke-virtual {v8, v2}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@3b7
    move-result v2

    #@3b8
    if-eqz v2, :cond_3c0

    #@3ba
    .line 3113
    move-object/from16 v0, p0

    #@3bc
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3be
    iget-object v8, v2, Landroid/view/ViewRootImpl;->mLastConfiguration:Landroid/content/res/Configuration;

    #@3c0
    .line 3115
    :cond_3c0
    move-object/from16 v0, p0

    #@3c2
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3c4
    const/4 v3, 0x0

    #@3c5
    invoke-virtual {v2, v8, v3}, Landroid/view/ViewRootImpl;->updateConfiguration(Landroid/content/res/Configuration;Z)V

    #@3c8
    goto/16 :goto_7

    #@3ca
    .line 3118
    .end local v8           #config:Landroid/content/res/Configuration;
    :pswitch_3ca
    move-object/from16 v0, p0

    #@3cc
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3ce
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@3d0
    if-eqz v2, :cond_7

    #@3d2
    .line 3119
    move-object/from16 v0, p0

    #@3d4
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3d6
    move-object/from16 v0, p1

    #@3d8
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@3da
    const/4 v4, 0x1

    #@3db
    if-ne v2, v4, :cond_3e3

    #@3dd
    const/4 v2, 0x1

    #@3de
    :goto_3de
    invoke-virtual {v3, v2}, Landroid/view/ViewRootImpl;->handleScreenStateChange(Z)V

    #@3e1
    goto/16 :goto_7

    #@3e3
    :cond_3e3
    const/4 v2, 0x0

    #@3e4
    goto :goto_3de

    #@3e5
    .line 3123
    :pswitch_3e5
    move-object/from16 v0, p0

    #@3e7
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3e9
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->invalidateDisplayLists()V

    #@3ec
    goto/16 :goto_7

    #@3ee
    .line 3126
    :pswitch_3ee
    move-object/from16 v0, p0

    #@3f0
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3f2
    const/4 v3, 0x0

    #@3f3
    const/4 v4, 0x0

    #@3f4
    invoke-virtual {v2, v3, v4}, Landroid/view/ViewRootImpl;->setAccessibilityFocus(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3f7
    goto/16 :goto_7

    #@3f9
    .line 3129
    :pswitch_3f9
    move-object/from16 v0, p0

    #@3fb
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@3fd
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->handleDispatchDoneAnimating()V

    #@400
    goto/16 :goto_7

    #@402
    .line 3132
    :pswitch_402
    move-object/from16 v0, p0

    #@404
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@406
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@408
    if-eqz v2, :cond_7

    #@40a
    .line 3133
    move-object/from16 v0, p0

    #@40c
    iget-object v2, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@40e
    move-object/from16 v0, p0

    #@410
    iget-object v3, v0, Landroid/view/ViewRootImpl$ViewRootHandler;->this$0:Landroid/view/ViewRootImpl;

    #@412
    iget-object v3, v3, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@414
    invoke-virtual {v2, v3}, Landroid/view/ViewRootImpl;->invalidateWorld(Landroid/view/View;)V

    #@417
    goto/16 :goto_7

    #@419
    .line 3015
    .restart local v9       #e:Landroid/view/Surface$OutOfResourcesException;
    .restart local v12       #hasWindowFocus:Z
    .restart local v13       #inTouchMode:Z
    :catch_419
    move-exception v2

    #@41a
    goto/16 :goto_2f0

    #@41c
    .line 2914
    :pswitch_data_41c
    .packed-switch 0x1
        :pswitch_8
        :pswitch_12
        :pswitch_30e
        :pswitch_66
        :pswitch_9a
        :pswitch_16e
        :pswitch_317
        :pswitch_4d
        :pswitch_5e
        :pswitch_29
        :pswitch_329
        :pswitch_34d
        :pswitch_35e
        :pswitch_369
        :pswitch_382
        :pswitch_382
        :pswitch_399
        :pswitch_3a8
        :pswitch_3e
        :pswitch_3ca
        :pswitch_3e5
        :pswitch_3ee
        :pswitch_3f9
        :pswitch_402
        :pswitch_108
    .end packed-switch
.end method
