.class final Landroid/view/Surface$CompatibleCanvas;
.super Landroid/graphics/Canvas;
.source "Surface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Surface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CompatibleCanvas"
.end annotation


# instance fields
.field private mOrigMatrix:Landroid/graphics/Matrix;

.field final synthetic this$0:Landroid/view/Surface;


# direct methods
.method private constructor <init>(Landroid/view/Surface;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 849
    iput-object p1, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@2
    invoke-direct {p0}, Landroid/graphics/Canvas;-><init>()V

    #@5
    .line 851
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/view/Surface$CompatibleCanvas;->mOrigMatrix:Landroid/graphics/Matrix;

    #@8
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/Surface;Landroid/view/Surface$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 849
    invoke-direct {p0, p1}, Landroid/view/Surface$CompatibleCanvas;-><init>(Landroid/view/Surface;)V

    #@3
    return-void
.end method


# virtual methods
.method public getHeight()I
    .registers 4

    #@0
    .prologue
    .line 864
    invoke-super {p0}, Landroid/graphics/Canvas;->getHeight()I

    #@3
    move-result v0

    #@4
    .line 865
    .local v0, h:I
    iget-object v1, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@6
    invoke-static {v1}, Landroid/view/Surface;->access$100(Landroid/view/Surface;)Landroid/content/res/CompatibilityInfo$Translator;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_1a

    #@c
    .line 866
    int-to-float v1, v0

    #@d
    iget-object v2, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@f
    invoke-static {v2}, Landroid/view/Surface;->access$100(Landroid/view/Surface;)Landroid/content/res/CompatibilityInfo$Translator;

    #@12
    move-result-object v2

    #@13
    iget v2, v2, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@15
    mul-float/2addr v1, v2

    #@16
    const/high16 v2, 0x3f00

    #@18
    add-float/2addr v1, v2

    #@19
    float-to-int v0, v1

    #@1a
    .line 868
    :cond_1a
    return v0
.end method

.method public getMatrix(Landroid/graphics/Matrix;)V
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 886
    invoke-super {p0, p1}, Landroid/graphics/Canvas;->getMatrix(Landroid/graphics/Matrix;)V

    #@3
    .line 887
    iget-object v0, p0, Landroid/view/Surface$CompatibleCanvas;->mOrigMatrix:Landroid/graphics/Matrix;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 888
    new-instance v0, Landroid/graphics/Matrix;

    #@9
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@c
    iput-object v0, p0, Landroid/view/Surface$CompatibleCanvas;->mOrigMatrix:Landroid/graphics/Matrix;

    #@e
    .line 890
    :cond_e
    iget-object v0, p0, Landroid/view/Surface$CompatibleCanvas;->mOrigMatrix:Landroid/graphics/Matrix;

    #@10
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@13
    .line 891
    return-void
.end method

.method public getWidth()I
    .registers 4

    #@0
    .prologue
    .line 855
    invoke-super {p0}, Landroid/graphics/Canvas;->getWidth()I

    #@3
    move-result v0

    #@4
    .line 856
    .local v0, w:I
    iget-object v1, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@6
    invoke-static {v1}, Landroid/view/Surface;->access$100(Landroid/view/Surface;)Landroid/content/res/CompatibilityInfo$Translator;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_1a

    #@c
    .line 857
    int-to-float v1, v0

    #@d
    iget-object v2, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@f
    invoke-static {v2}, Landroid/view/Surface;->access$100(Landroid/view/Surface;)Landroid/content/res/CompatibilityInfo$Translator;

    #@12
    move-result-object v2

    #@13
    iget v2, v2, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@15
    mul-float/2addr v1, v2

    #@16
    const/high16 v2, 0x3f00

    #@18
    add-float/2addr v1, v2

    #@19
    float-to-int v0, v1

    #@1a
    .line 859
    :cond_1a
    return v0
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 873
    iget-object v1, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@2
    invoke-static {v1}, Landroid/view/Surface;->access$200(Landroid/view/Surface;)Landroid/graphics/Matrix;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_14

    #@8
    iget-object v1, p0, Landroid/view/Surface$CompatibleCanvas;->mOrigMatrix:Landroid/graphics/Matrix;

    #@a
    if-eqz v1, :cond_14

    #@c
    iget-object v1, p0, Landroid/view/Surface$CompatibleCanvas;->mOrigMatrix:Landroid/graphics/Matrix;

    #@e
    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_18

    #@14
    .line 876
    :cond_14
    invoke-super {p0, p1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    #@17
    .line 882
    :goto_17
    return-void

    #@18
    .line 878
    :cond_18
    new-instance v0, Landroid/graphics/Matrix;

    #@1a
    iget-object v1, p0, Landroid/view/Surface$CompatibleCanvas;->this$0:Landroid/view/Surface;

    #@1c
    invoke-static {v1}, Landroid/view/Surface;->access$200(Landroid/view/Surface;)Landroid/graphics/Matrix;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    #@23
    .line 879
    .local v0, m:Landroid/graphics/Matrix;
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    #@26
    .line 880
    invoke-super {p0, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    #@29
    goto :goto_17
.end method
