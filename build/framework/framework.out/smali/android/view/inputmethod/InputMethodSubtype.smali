.class public final Landroid/view/inputmethod/InputMethodSubtype;
.super Ljava/lang/Object;
.source "InputMethodSubtype.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation
.end field

.field private static final EXTRA_KEY_UNTRANSLATABLE_STRING_IN_SUBTYPE_NAME:Ljava/lang/String; = "UntranslatableReplacementStringInSubtypeName"

.field private static final EXTRA_VALUE_KEY_VALUE_SEPARATOR:Ljava/lang/String; = "="

.field private static final EXTRA_VALUE_PAIR_SEPARATOR:Ljava/lang/String; = ","

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private volatile mExtraValueHashMapCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsAuxiliary:Z

.field private final mOverridesImplicitlyEnabledSubtype:Z

.field private final mSubtypeExtraValue:Ljava/lang/String;

.field private final mSubtypeHashCode:I

.field private final mSubtypeIconResId:I

.field private final mSubtypeId:I

.field private final mSubtypeLocale:Ljava/lang/String;

.field private final mSubtypeMode:Ljava/lang/String;

.field private final mSubtypeNameResId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 46
    const-class v0, Landroid/view/inputmethod/InputMethodSubtype;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/view/inputmethod/InputMethodSubtype;->TAG:Ljava/lang/String;

    #@8
    .line 362
    new-instance v0, Landroid/view/inputmethod/InputMethodSubtype$1;

    #@a
    invoke-direct {v0}, Landroid/view/inputmethod/InputMethodSubtype$1;-><init>()V

    #@d
    sput-object v0, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 15
    .parameter "nameId"
    .parameter "iconId"
    .parameter "locale"
    .parameter "mode"
    .parameter "extraValue"
    .parameter "isAuxiliary"

    #@0
    .prologue
    .line 87
    const/4 v7, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    move v6, p6

    #@8
    invoke-direct/range {v0 .. v7}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@b
    .line 88
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 17
    .parameter "nameId"
    .parameter "iconId"
    .parameter "locale"
    .parameter "mode"
    .parameter "extraValue"
    .parameter "isAuxiliary"
    .parameter "overridesImplicitlyEnabledSubtype"

    #@0
    .prologue
    .line 116
    const/4 v8, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    move v6, p6

    #@8
    move/from16 v7, p7

    #@a
    invoke-direct/range {v0 .. v8}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    #@d
    .line 118
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .registers 14
    .parameter "nameId"
    .parameter "iconId"
    .parameter "locale"
    .parameter "mode"
    .parameter "extraValue"
    .parameter "isAuxiliary"
    .parameter "overridesImplicitlyEnabledSubtype"
    .parameter "id"

    #@0
    .prologue
    .line 150
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 151
    iput p1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeNameResId:I

    #@5
    .line 152
    iput p2, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeIconResId:I

    #@7
    .line 153
    if-eqz p3, :cond_1f

    #@9
    .end local p3
    :goto_9
    iput-object p3, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@b
    .line 154
    if-eqz p4, :cond_22

    #@d
    .end local p4
    :goto_d
    iput-object p4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeMode:Ljava/lang/String;

    #@f
    .line 155
    if-eqz p5, :cond_25

    #@11
    .end local p5
    :goto_11
    iput-object p5, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@13
    .line 156
    iput-boolean p6, p0, Landroid/view/inputmethod/InputMethodSubtype;->mIsAuxiliary:Z

    #@15
    .line 157
    iput-boolean p7, p0, Landroid/view/inputmethod/InputMethodSubtype;->mOverridesImplicitlyEnabledSubtype:Z

    #@17
    .line 160
    if-eqz p8, :cond_28

    #@19
    move v0, p8

    #@1a
    :goto_1a
    iput v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeHashCode:I

    #@1c
    .line 162
    iput p8, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeId:I

    #@1e
    .line 163
    return-void

    #@1f
    .line 153
    .restart local p3
    .restart local p4
    .restart local p5
    :cond_1f
    const-string p3, ""

    #@21
    goto :goto_9

    #@22
    .line 154
    .end local p3
    :cond_22
    const-string p4, ""

    #@24
    goto :goto_d

    #@25
    .line 155
    .end local p4
    :cond_25
    const-string p5, ""

    #@27
    goto :goto_11

    #@28
    .line 160
    .end local p5
    :cond_28
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@2a
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeMode:Ljava/lang/String;

    #@2c
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@2e
    iget-boolean v3, p0, Landroid/view/inputmethod/InputMethodSubtype;->mIsAuxiliary:Z

    #@30
    iget-boolean v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mOverridesImplicitlyEnabledSubtype:Z

    #@32
    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/inputmethod/InputMethodSubtype;->hashCodeInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I

    #@35
    move-result v0

    #@36
    goto :goto_1a
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 6
    .parameter "source"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 165
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v1

    #@9
    iput v1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeNameResId:I

    #@b
    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v1

    #@f
    iput v1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeIconResId:I

    #@11
    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 170
    .local v0, s:Ljava/lang/String;
    if-eqz v0, :cond_47

    #@17
    .end local v0           #s:Ljava/lang/String;
    :goto_17
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@19
    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 172
    .restart local v0       #s:Ljava/lang/String;
    if-eqz v0, :cond_4a

    #@1f
    .end local v0           #s:Ljava/lang/String;
    :goto_1f
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeMode:Ljava/lang/String;

    #@21
    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    .line 174
    .restart local v0       #s:Ljava/lang/String;
    if-eqz v0, :cond_4d

    #@27
    .end local v0           #s:Ljava/lang/String;
    :goto_27
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@29
    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v1

    #@2d
    if-ne v1, v2, :cond_50

    #@2f
    move v1, v2

    #@30
    :goto_30
    iput-boolean v1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mIsAuxiliary:Z

    #@32
    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v1

    #@36
    if-ne v1, v2, :cond_52

    #@38
    :goto_38
    iput-boolean v2, p0, Landroid/view/inputmethod/InputMethodSubtype;->mOverridesImplicitlyEnabledSubtype:Z

    #@3a
    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v1

    #@3e
    iput v1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeHashCode:I

    #@40
    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v1

    #@44
    iput v1, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeId:I

    #@46
    .line 179
    return-void

    #@47
    .line 170
    .restart local v0       #s:Ljava/lang/String;
    :cond_47
    const-string v0, ""

    #@49
    goto :goto_17

    #@4a
    .line 172
    :cond_4a
    const-string v0, ""

    #@4c
    goto :goto_1f

    #@4d
    .line 174
    :cond_4d
    const-string v0, ""

    #@4f
    goto :goto_27

    #@50
    .end local v0           #s:Ljava/lang/String;
    :cond_50
    move v1, v3

    #@51
    .line 175
    goto :goto_30

    #@52
    :cond_52
    move v2, v3

    #@53
    .line 176
    goto :goto_38
.end method

.method private static constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;
    .registers 8
    .parameter "localeStr"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 376
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v2

    #@9
    if-eqz v2, :cond_c

    #@b
    .line 388
    :cond_b
    :goto_b
    return-object v1

    #@c
    .line 378
    :cond_c
    const-string v2, "_"

    #@e
    invoke-virtual {p0, v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 381
    .local v0, localeParams:[Ljava/lang/String;
    array-length v2, v0

    #@13
    if-ne v2, v4, :cond_1d

    #@15
    .line 382
    new-instance v1, Ljava/util/Locale;

    #@17
    aget-object v2, v0, v3

    #@19
    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    #@1c
    goto :goto_b

    #@1d
    .line 383
    :cond_1d
    array-length v2, v0

    #@1e
    if-ne v2, v5, :cond_2a

    #@20
    .line 384
    new-instance v1, Ljava/util/Locale;

    #@22
    aget-object v2, v0, v3

    #@24
    aget-object v3, v0, v4

    #@26
    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_b

    #@2a
    .line 385
    :cond_2a
    array-length v2, v0

    #@2b
    if-ne v2, v6, :cond_b

    #@2d
    .line 386
    new-instance v1, Ljava/util/Locale;

    #@2f
    aget-object v2, v0, v3

    #@31
    aget-object v3, v0, v4

    #@33
    aget-object v4, v0, v5

    #@35
    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@38
    goto :goto_b
.end method

.method private getExtraValueHashMap()Ljava/util/HashMap;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 278
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@3
    if-nez v4, :cond_52

    #@5
    .line 279
    monitor-enter p0

    #@6
    .line 280
    :try_start_6
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@8
    if-nez v4, :cond_51

    #@a
    .line 281
    new-instance v4, Ljava/util/HashMap;

    #@c
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@11
    .line 282
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@13
    const-string v5, ","

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 283
    .local v3, pairs:[Ljava/lang/String;
    array-length v0, v3

    #@1a
    .line 284
    .local v0, N:I
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    if-ge v1, v0, :cond_51

    #@1d
    .line 285
    aget-object v4, v3, v1

    #@1f
    const-string v5, "="

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    .line 286
    .local v2, pair:[Ljava/lang/String;
    array-length v4, v2

    #@26
    if-ne v4, v7, :cond_34

    #@28
    .line 287
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@2a
    const/4 v5, 0x0

    #@2b
    aget-object v5, v2, v5

    #@2d
    const/4 v6, 0x0

    #@2e
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    .line 284
    :cond_31
    :goto_31
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_1b

    #@34
    .line 288
    :cond_34
    array-length v4, v2

    #@35
    if-le v4, v7, :cond_31

    #@37
    .line 289
    array-length v4, v2

    #@38
    const/4 v5, 0x2

    #@39
    if-le v4, v5, :cond_42

    #@3b
    .line 290
    sget-object v4, Landroid/view/inputmethod/InputMethodSubtype;->TAG:Ljava/lang/String;

    #@3d
    const-string v5, "ExtraValue has two or more \'=\'s"

    #@3f
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 292
    :cond_42
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@44
    const/4 v5, 0x0

    #@45
    aget-object v5, v2, v5

    #@47
    const/4 v6, 0x1

    #@48
    aget-object v6, v2, v6

    #@4a
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    goto :goto_31

    #@4e
    .line 296
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #pair:[Ljava/lang/String;
    .end local v3           #pairs:[Ljava/lang/String;
    :catchall_4e
    move-exception v4

    #@4f
    monitor-exit p0
    :try_end_50
    .catchall {:try_start_6 .. :try_end_50} :catchall_4e

    #@50
    throw v4

    #@51
    :cond_51
    :try_start_51
    monitor-exit p0
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_4e

    #@52
    .line 298
    :cond_52
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodSubtype;->mExtraValueHashMapCache:Ljava/util/HashMap;

    #@54
    return-object v4
.end method

.method private static hashCodeInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I
    .registers 8
    .parameter "locale"
    .parameter "mode"
    .parameter "extraValue"
    .parameter "isAuxiliary"
    .parameter "overridesImplicitlyEnabledSubtype"

    #@0
    .prologue
    .line 393
    const/4 v0, 0x5

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    aput-object p0, v0, v1

    #@6
    const/4 v1, 0x1

    #@7
    aput-object p1, v0, v1

    #@9
    const/4 v1, 0x2

    #@a
    aput-object p2, v0, v1

    #@c
    const/4 v1, 0x3

    #@d
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v2

    #@11
    aput-object v2, v0, v1

    #@13
    const/4 v1, 0x4

    #@14
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17
    move-result-object v2

    #@18
    aput-object v2, v0, v1

    #@1a
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public static sort(Landroid/content/Context;ILandroid/view/inputmethod/InputMethodInfo;Ljava/util/List;)Ljava/util/List;
    .registers 11
    .parameter "context"
    .parameter "flags"
    .parameter "imi"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 408
    .local p3, subtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    if-nez p2, :cond_3

    #@2
    .line 425
    .end local p3           #subtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :goto_2
    return-object p3

    #@3
    .line 409
    .restart local p3       #subtypeList:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_3
    new-instance v3, Ljava/util/HashSet;

    #@5
    invoke-direct {v3, p3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@8
    .line 411
    .local v3, inputSubtypesSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/inputmethod/InputMethodSubtype;>;"
    new-instance v4, Ljava/util/ArrayList;

    #@a
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@d
    .line 412
    .local v4, sortedList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {p2}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeCount()I

    #@10
    move-result v0

    #@11
    .line 413
    .local v0, N:I
    const/4 v1, 0x0

    #@12
    .local v1, i:I
    :goto_12
    if-ge v1, v0, :cond_27

    #@14
    .line 414
    invoke-virtual {p2, v1}, Landroid/view/inputmethod/InputMethodInfo;->getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;

    #@17
    move-result-object v5

    #@18
    .line 415
    .local v5, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_24

    #@1e
    .line 416
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 417
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@24
    .line 413
    :cond_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_12

    #@27
    .line 422
    .end local v5           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_27
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@2a
    move-result-object v2

    #@2b
    .local v2, i$:Ljava/util/Iterator;
    :goto_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2e
    move-result v6

    #@2f
    if-eqz v6, :cond_3b

    #@31
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@34
    move-result-object v5

    #@35
    check-cast v5, Landroid/view/inputmethod/InputMethodSubtype;

    #@37
    .line 423
    .restart local v5       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a
    goto :goto_2b

    #@3b
    .end local v5           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_3b
    move-object p3, v4

    #@3c
    .line 425
    goto :goto_2
.end method


# virtual methods
.method public containsExtraValueKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 308
    invoke-direct {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValueHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 346
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 328
    instance-of v3, p1, Landroid/view/inputmethod/InputMethodSubtype;

    #@4
    if-eqz v3, :cond_72

    #@6
    move-object v0, p1

    #@7
    .line 329
    check-cast v0, Landroid/view/inputmethod/InputMethodSubtype;

    #@9
    .line 330
    .local v0, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    iget v3, v0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeId:I

    #@b
    if-nez v3, :cond_11

    #@d
    iget v3, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeId:I

    #@f
    if-eqz v3, :cond_1e

    #@11
    .line 331
    :cond_11
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@14
    move-result v3

    #@15
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@18
    move-result v4

    #@19
    if-ne v3, v4, :cond_1c

    #@1b
    .line 341
    .end local v0           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_1b
    :goto_1b
    return v1

    #@1c
    .restart local v0       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_1c
    move v1, v2

    #@1d
    .line 331
    goto :goto_1b

    #@1e
    .line 333
    :cond_1e
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@21
    move-result v3

    #@22
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->hashCode()I

    #@25
    move-result v4

    #@26
    if-ne v3, v4, :cond_70

    #@28
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getNameResId()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getNameResId()I

    #@2f
    move-result v4

    #@30
    if-ne v3, v4, :cond_70

    #@32
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_70

    #@40
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getIconResId()I

    #@43
    move-result v3

    #@44
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getIconResId()I

    #@47
    move-result v4

    #@48
    if-ne v3, v4, :cond_70

    #@4a
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v3

    #@56
    if-eqz v3, :cond_70

    #@58
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValue()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValue()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v3

    #@64
    if-eqz v3, :cond_70

    #@66
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@69
    move-result v3

    #@6a
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@6d
    move-result v4

    #@6e
    if-eq v3, v4, :cond_1b

    #@70
    :cond_70
    move v1, v2

    #@71
    goto :goto_1b

    #@72
    .end local v0           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_72
    move v1, v2

    #@73
    .line 341
    goto :goto_1b
.end method

.method public getDisplayName(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    .registers 12
    .parameter "context"
    .parameter "packageName"
    .parameter "appInfo"

    #@0
    .prologue
    .line 253
    iget-object v5, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@2
    invoke-static {v5}, Landroid/view/inputmethod/InputMethodSubtype;->constructLocaleFromString(Ljava/lang/String;)Ljava/util/Locale;

    #@5
    move-result-object v1

    #@6
    .line 254
    .local v1, locale:Ljava/util/Locale;
    if-eqz v1, :cond_11

    #@8
    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 255
    .local v2, localeStr:Ljava/lang/String;
    :goto_c
    iget v5, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeNameResId:I

    #@e
    if-nez v5, :cond_14

    #@10
    .line 273
    .end local v2           #localeStr:Ljava/lang/String;
    :cond_10
    :goto_10
    return-object v2

    #@11
    .line 254
    :cond_11
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@13
    goto :goto_c

    #@14
    .line 258
    .restart local v2       #localeStr:Ljava/lang/String;
    :cond_14
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@17
    move-result-object v5

    #@18
    iget v6, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeNameResId:I

    #@1a
    invoke-virtual {v5, p2, v6, p3}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@1d
    move-result-object v4

    #@1e
    .line 260
    .local v4, subtypeName:Ljava/lang/CharSequence;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@21
    move-result v5

    #@22
    if-nez v5, :cond_10

    #@24
    .line 261
    const-string v5, "UntranslatableReplacementStringInSubtypeName"

    #@26
    invoke-virtual {p0, v5}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    #@29
    move-result v5

    #@2a
    if-eqz v5, :cond_43

    #@2c
    const-string v5, "UntranslatableReplacementStringInSubtypeName"

    #@2e
    invoke-virtual {p0, v5}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValueOf(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 266
    .local v3, replacementString:Ljava/lang/String;
    :goto_32
    :try_start_32
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    const/4 v6, 0x1

    #@37
    new-array v6, v6, [Ljava/lang/Object;

    #@39
    const/4 v7, 0x0

    #@3a
    if-eqz v3, :cond_45

    #@3c
    .end local v3           #replacementString:Ljava/lang/String;
    :goto_3c
    aput-object v3, v6, v7

    #@3e
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    goto :goto_10

    #@43
    :cond_43
    move-object v3, v2

    #@44
    .line 261
    goto :goto_32

    #@45
    .line 266
    .restart local v3       #replacementString:Ljava/lang/String;
    :cond_45
    const-string v3, ""
    :try_end_47
    .catch Ljava/util/IllegalFormatException; {:try_start_32 .. :try_end_47} :catch_48

    #@47
    goto :goto_3c

    #@48
    .line 268
    .end local v3           #replacementString:Ljava/lang/String;
    :catch_48
    move-exception v0

    #@49
    .line 269
    .local v0, e:Ljava/util/IllegalFormatException;
    sget-object v5, Landroid/view/inputmethod/InputMethodSubtype;->TAG:Ljava/lang/String;

    #@4b
    new-instance v6, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v7, "Found illegal format in subtype name("

    #@52
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    const-string v7, "): "

    #@5c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v6

    #@60
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v6

    #@64
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v6

    #@68
    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 270
    const-string v2, ""

    #@6d
    goto :goto_10
.end method

.method public getExtraValue()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getExtraValueOf(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 318
    invoke-direct {p0}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValueHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Ljava/lang/String;

    #@a
    return-object v0
.end method

.method public getIconResId()I
    .registers 2

    #@0
    .prologue
    .line 192
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeIconResId:I

    #@2
    return v0
.end method

.method public getLocale()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeMode:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNameResId()I
    .registers 2

    #@0
    .prologue
    .line 185
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeNameResId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 323
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeHashCode:I

    #@2
    return v0
.end method

.method public isAuxiliary()Z
    .registers 2

    #@0
    .prologue
    .line 228
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mIsAuxiliary:Z

    #@2
    return v0
.end method

.method public overridesImplicitlyEnabledSubtype()Z
    .registers 2

    #@0
    .prologue
    .line 238
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mOverridesImplicitlyEnabledSubtype:Z

    #@2
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "parcelableFlags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 351
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeNameResId:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 352
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeIconResId:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 353
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeLocale:Ljava/lang/String;

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 354
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeMode:Ljava/lang/String;

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 355
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeExtraValue:Ljava/lang/String;

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 356
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mIsAuxiliary:Z

    #@1d
    if-eqz v0, :cond_35

    #@1f
    move v0, v1

    #@20
    :goto_20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 357
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mOverridesImplicitlyEnabledSubtype:Z

    #@25
    if-eqz v0, :cond_37

    #@27
    :goto_27
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 358
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeHashCode:I

    #@2c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 359
    iget v0, p0, Landroid/view/inputmethod/InputMethodSubtype;->mSubtypeId:I

    #@31
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 360
    return-void

    #@35
    :cond_35
    move v0, v2

    #@36
    .line 356
    goto :goto_20

    #@37
    :cond_37
    move v1, v2

    #@38
    .line 357
    goto :goto_27
.end method
