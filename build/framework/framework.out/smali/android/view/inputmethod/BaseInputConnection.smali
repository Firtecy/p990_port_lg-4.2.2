.class public Landroid/view/inputmethod/BaseInputConnection;
.super Ljava/lang/Object;
.source "BaseInputConnection.java"

# interfaces
.implements Landroid/view/inputmethod/InputConnection;


# static fields
.field static final COMPOSING:Ljava/lang/Object; = null

.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "BaseInputConnection"


# instance fields
.field private mDefaultComposingSpans:[Ljava/lang/Object;

.field final mDummyMode:Z

.field mEditable:Landroid/text/Editable;

.field protected final mIMM:Landroid/view/inputmethod/InputMethodManager;

.field mKeyCharacterMap:Landroid/view/KeyCharacterMap;

.field final mTargetView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 56
    new-instance v0, Landroid/view/inputmethod/ComposingText;

    #@2
    invoke-direct {v0}, Landroid/view/inputmethod/ComposingText;-><init>()V

    #@5
    sput-object v0, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Z)V
    .registers 5
    .parameter "targetView"
    .parameter "fullEditor"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@6
    move-result-object v0

    #@7
    const-string v1, "input_method"

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@f
    iput-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@11
    .line 77
    iput-object p1, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@13
    .line 78
    if-nez p2, :cond_19

    #@15
    const/4 v0, 0x1

    #@16
    :goto_16
    iput-boolean v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mDummyMode:Z

    #@18
    .line 79
    return-void

    #@19
    .line 78
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_16
.end method

.method constructor <init>(Landroid/view/inputmethod/InputMethodManager;Z)V
    .registers 4
    .parameter "mgr"
    .parameter "fullEditor"

    #@0
    .prologue
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 69
    iput-object p1, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@5
    .line 70
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@8
    .line 71
    if-nez p2, :cond_e

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    iput-boolean v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mDummyMode:Z

    #@d
    .line 72
    return-void

    #@e
    .line 71
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_b
.end method

.method private ensureDefaultComposingSpans()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 609
    iget-object v3, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@3
    if-nez v3, :cond_3d

    #@5
    .line 611
    iget-object v3, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@7
    if-eqz v3, :cond_3e

    #@9
    .line 612
    iget-object v3, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@b
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@e
    move-result-object v0

    #@f
    .line 618
    .local v0, context:Landroid/content/Context;
    :goto_f
    if-eqz v0, :cond_3d

    #@11
    .line 619
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@14
    move-result-object v3

    #@15
    const/4 v4, 0x1

    #@16
    new-array v4, v4, [I

    #@18
    const v5, 0x1010230

    #@1b
    aput v5, v4, v6

    #@1d
    invoke-virtual {v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@20
    move-result-object v2

    #@21
    .line 623
    .local v2, ta:Landroid/content/res/TypedArray;
    invoke-virtual {v2, v6}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@24
    move-result-object v1

    #@25
    .line 624
    .local v1, style:Ljava/lang/CharSequence;
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@28
    .line 625
    if-eqz v1, :cond_3d

    #@2a
    instance-of v3, v1, Landroid/text/Spanned;

    #@2c
    if-eqz v3, :cond_3d

    #@2e
    move-object v3, v1

    #@2f
    .line 626
    check-cast v3, Landroid/text/Spanned;

    #@31
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    #@34
    move-result v4

    #@35
    const-class v5, Ljava/lang/Object;

    #@37
    invoke-interface {v3, v6, v4, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@3a
    move-result-object v3

    #@3b
    iput-object v3, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@3d
    .line 631
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #style:Ljava/lang/CharSequence;
    .end local v2           #ta:Landroid/content/res/TypedArray;
    :cond_3d
    return-void

    #@3e
    .line 613
    :cond_3e
    iget-object v3, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@40
    iget-object v3, v3, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@42
    if-eqz v3, :cond_4d

    #@44
    .line 614
    iget-object v3, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@46
    iget-object v3, v3, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@48
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@4b
    move-result-object v0

    #@4c
    .restart local v0       #context:Landroid/content/Context;
    goto :goto_f

    #@4d
    .line 616
    .end local v0           #context:Landroid/content/Context;
    :cond_4d
    const/4 v0, 0x0

    #@4e
    .restart local v0       #context:Landroid/content/Context;
    goto :goto_f
.end method

.method public static getComposingSpanEnd(Landroid/text/Spannable;)I
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 129
    sget-object v0, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@2
    invoke-interface {p0, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getComposingSpanStart(Landroid/text/Spannable;)I
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 125
    sget-object v0, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@2
    invoke-interface {p0, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static final removeComposingSpans(Landroid/text/Spannable;)V
    .registers 7
    .parameter "text"

    #@0
    .prologue
    .line 82
    sget-object v3, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@2
    invoke-interface {p0, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@5
    .line 83
    const/4 v3, 0x0

    #@6
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@9
    move-result v4

    #@a
    const-class v5, Ljava/lang/Object;

    #@c
    invoke-interface {p0, v3, v4, v5}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    .line 84
    .local v2, sps:[Ljava/lang/Object;
    if-eqz v2, :cond_27

    #@12
    .line 85
    array-length v3, v2

    #@13
    add-int/lit8 v0, v3, -0x1

    #@15
    .local v0, i:I
    :goto_15
    if-ltz v0, :cond_27

    #@17
    .line 86
    aget-object v1, v2, v0

    #@19
    .line 87
    .local v1, o:Ljava/lang/Object;
    invoke-interface {p0, v1}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    #@1c
    move-result v3

    #@1d
    and-int/lit16 v3, v3, 0x100

    #@1f
    if-eqz v3, :cond_24

    #@21
    .line 88
    invoke-interface {p0, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@24
    .line 85
    :cond_24
    add-int/lit8 v0, v0, -0x1

    #@26
    goto :goto_15

    #@27
    .line 92
    .end local v0           #i:I
    .end local v1           #o:Ljava/lang/Object;
    :cond_27
    return-void
.end method

.method private replaceText(Ljava/lang/CharSequence;IZ)V
    .registers 14
    .parameter "text"
    .parameter "newCursorPosition"
    .parameter "composing"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 635
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@4
    move-result-object v2

    #@5
    .line 636
    .local v2, content:Landroid/text/Editable;
    if-nez v2, :cond_8

    #@7
    .line 721
    :goto_7
    return-void

    #@8
    .line 640
    :cond_8
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->beginBatchEdit()Z

    #@b
    .line 643
    invoke-static {v2}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@e
    move-result v0

    #@f
    .line 644
    .local v0, a:I
    invoke-static {v2}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@12
    move-result v1

    #@13
    .line 648
    .local v1, b:I
    if-ge v1, v0, :cond_18

    #@15
    .line 649
    move v5, v0

    #@16
    .line 650
    .local v5, tmp:I
    move v0, v1

    #@17
    .line 651
    move v1, v5

    #@18
    .line 654
    .end local v5           #tmp:I
    :cond_18
    if-eq v0, v6, :cond_4a

    #@1a
    if-eq v1, v6, :cond_4a

    #@1c
    .line 655
    invoke-static {v2}, Landroid/view/inputmethod/BaseInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    #@1f
    .line 668
    :cond_1f
    :goto_1f
    if-eqz p3, :cond_64

    #@21
    .line 669
    const/4 v4, 0x0

    #@22
    .line 670
    .local v4, sp:Landroid/text/Spannable;
    instance-of v6, p1, Landroid/text/Spannable;

    #@24
    if-nez v6, :cond_5e

    #@26
    .line 671
    new-instance v4, Landroid/text/SpannableStringBuilder;

    #@28
    .end local v4           #sp:Landroid/text/Spannable;
    invoke-direct {v4, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@2b
    .line 672
    .restart local v4       #sp:Landroid/text/Spannable;
    move-object p1, v4

    #@2c
    .line 673
    invoke-direct {p0}, Landroid/view/inputmethod/BaseInputConnection;->ensureDefaultComposingSpans()V

    #@2f
    .line 674
    iget-object v6, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@31
    if-eqz v6, :cond_61

    #@33
    .line 675
    const/4 v3, 0x0

    #@34
    .local v3, i:I
    :goto_34
    iget-object v6, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@36
    array-length v6, v6

    #@37
    if-ge v3, v6, :cond_61

    #@39
    .line 676
    iget-object v6, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@3b
    aget-object v6, v6, v3

    #@3d
    const/4 v7, 0x0

    #@3e
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    #@41
    move-result v8

    #@42
    const/16 v9, 0x121

    #@44
    invoke-interface {v4, v6, v7, v8, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@47
    .line 675
    add-int/lit8 v3, v3, 0x1

    #@49
    goto :goto_34

    #@4a
    .line 657
    .end local v3           #i:I
    .end local v4           #sp:Landroid/text/Spannable;
    :cond_4a
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@4d
    move-result v0

    #@4e
    .line 658
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@51
    move-result v1

    #@52
    .line 659
    if-gez v0, :cond_55

    #@54
    const/4 v0, 0x0

    #@55
    .line 660
    :cond_55
    if-gez v1, :cond_58

    #@57
    const/4 v1, 0x0

    #@58
    .line 661
    :cond_58
    if-ge v1, v0, :cond_1f

    #@5a
    .line 662
    move v5, v0

    #@5b
    .line 663
    .restart local v5       #tmp:I
    move v0, v1

    #@5c
    .line 664
    move v1, v5

    #@5d
    goto :goto_1f

    #@5e
    .end local v5           #tmp:I
    .restart local v4       #sp:Landroid/text/Spannable;
    :cond_5e
    move-object v4, p1

    #@5f
    .line 681
    check-cast v4, Landroid/text/Spannable;

    #@61
    .line 683
    :cond_61
    invoke-static {v4}, Landroid/view/inputmethod/BaseInputConnection;->setComposingSpans(Landroid/text/Spannable;)V

    #@64
    .line 702
    .end local v4           #sp:Landroid/text/Spannable;
    :cond_64
    if-lez p2, :cond_80

    #@66
    .line 703
    add-int/lit8 v6, v1, -0x1

    #@68
    add-int/2addr p2, v6

    #@69
    .line 707
    :goto_69
    if-gez p2, :cond_6c

    #@6b
    const/4 p2, 0x0

    #@6c
    .line 708
    :cond_6c
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@6f
    move-result v6

    #@70
    if-le p2, v6, :cond_76

    #@72
    .line 709
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@75
    move-result p2

    #@76
    .line 710
    :cond_76
    invoke-static {v2, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@79
    .line 712
    invoke-interface {v2, v0, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    #@7c
    .line 720
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->endBatchEdit()Z

    #@7f
    goto :goto_7

    #@80
    .line 705
    :cond_80
    add-int/2addr p2, v0

    #@81
    goto :goto_69
.end method

.method private sendCurrentText()V
    .registers 12

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 570
    iget-boolean v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mDummyMode:Z

    #@5
    if-nez v1, :cond_8

    #@7
    .line 606
    :cond_7
    :goto_7
    return-void

    #@8
    .line 574
    :cond_8
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@b
    move-result-object v8

    #@c
    .line 575
    .local v8, content:Landroid/text/Editable;
    if-eqz v8, :cond_7

    #@e
    .line 576
    invoke-interface {v8}, Landroid/text/Editable;->length()I

    #@11
    move-result v6

    #@12
    .line 577
    .local v6, N:I
    if-eqz v6, :cond_7

    #@14
    .line 580
    if-ne v6, v2, :cond_3d

    #@16
    .line 583
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@18
    if-nez v1, :cond_20

    #@1a
    .line 584
    invoke-static {v4}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@1d
    move-result-object v1

    #@1e
    iput-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@20
    .line 586
    :cond_20
    new-array v7, v2, [C

    #@22
    .line 587
    .local v7, chars:[C
    invoke-interface {v8, v5, v2, v7, v5}, Landroid/text/Editable;->getChars(II[CI)V

    #@25
    .line 588
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mKeyCharacterMap:Landroid/view/KeyCharacterMap;

    #@27
    invoke-virtual {v1, v7}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    #@2a
    move-result-object v9

    #@2b
    .line 589
    .local v9, events:[Landroid/view/KeyEvent;
    if-eqz v9, :cond_3d

    #@2d
    .line 590
    const/4 v10, 0x0

    #@2e
    .local v10, i:I
    :goto_2e
    array-length v1, v9

    #@2f
    if-ge v10, v1, :cond_39

    #@31
    .line 592
    aget-object v1, v9, v10

    #@33
    invoke-virtual {p0, v1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@36
    .line 590
    add-int/lit8 v10, v10, 0x1

    #@38
    goto :goto_2e

    #@39
    .line 594
    :cond_39
    invoke-interface {v8}, Landroid/text/Editable;->clear()V

    #@3c
    goto :goto_7

    #@3d
    .line 601
    .end local v7           #chars:[C
    .end local v9           #events:[Landroid/view/KeyEvent;
    .end local v10           #i:I
    :cond_3d
    new-instance v0, Landroid/view/KeyEvent;

    #@3f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@42
    move-result-wide v1

    #@43
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-direct/range {v0 .. v5}, Landroid/view/KeyEvent;-><init>(JLjava/lang/String;II)V

    #@4a
    .line 603
    .local v0, event:Landroid/view/KeyEvent;
    invoke-virtual {p0, v0}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@4d
    .line 604
    invoke-interface {v8}, Landroid/text/Editable;->clear()V

    #@50
    goto :goto_7
.end method

.method public static setComposingSpans(Landroid/text/Spannable;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 95
    const/4 v0, 0x0

    #@1
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@4
    move-result v1

    #@5
    invoke-static {p0, v0, v1}, Landroid/view/inputmethod/BaseInputConnection;->setComposingSpans(Landroid/text/Spannable;II)V

    #@8
    .line 96
    return-void
.end method

.method public static setComposingSpans(Landroid/text/Spannable;II)V
    .registers 11
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/16 v7, 0x121

    #@2
    .line 100
    const-class v4, Ljava/lang/Object;

    #@4
    invoke-interface {p0, p1, p2, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@7
    move-result-object v3

    #@8
    .line 101
    .local v3, sps:[Ljava/lang/Object;
    if-eqz v3, :cond_35

    #@a
    .line 102
    array-length v4, v3

    #@b
    add-int/lit8 v1, v4, -0x1

    #@d
    .local v1, i:I
    :goto_d
    if-ltz v1, :cond_35

    #@f
    .line 103
    aget-object v2, v3, v1

    #@11
    .line 104
    .local v2, o:Ljava/lang/Object;
    sget-object v4, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@13
    if-ne v2, v4, :cond_1b

    #@15
    .line 105
    invoke-interface {p0, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@18
    .line 102
    :cond_18
    :goto_18
    add-int/lit8 v1, v1, -0x1

    #@1a
    goto :goto_d

    #@1b
    .line 109
    :cond_1b
    invoke-interface {p0, v2}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    #@1e
    move-result v0

    #@1f
    .line 110
    .local v0, fl:I
    and-int/lit16 v4, v0, 0x133

    #@21
    if-eq v4, v7, :cond_18

    #@23
    .line 112
    invoke-interface {p0, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@26
    move-result v4

    #@27
    invoke-interface {p0, v2}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    #@2a
    move-result v5

    #@2b
    and-int/lit8 v6, v0, -0x34

    #@2d
    or-int/lit16 v6, v6, 0x100

    #@2f
    or-int/lit8 v6, v6, 0x21

    #@31
    invoke-interface {p0, v2, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@34
    goto :goto_18

    #@35
    .line 120
    .end local v0           #fl:I
    .end local v1           #i:I
    .end local v2           #o:Ljava/lang/Object;
    :cond_35
    sget-object v4, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@37
    invoke-interface {p0, v4, p1, p2, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@3a
    .line 122
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .registers 2

    #@0
    .prologue
    .line 150
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public clearMetaKeyStates(I)Z
    .registers 4
    .parameter "states"

    #@0
    .prologue
    .line 175
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    .line 176
    .local v0, content:Landroid/text/Editable;
    if-nez v0, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    .line 178
    :goto_7
    return v1

    #@8
    .line 177
    :cond_8
    invoke-static {v0, p1}, Landroid/text/method/MetaKeyKeyListener;->clearMetaKeyState(Landroid/text/Editable;I)V

    #@b
    .line 178
    const/4 v1, 0x1

    #@c
    goto :goto_7
.end method

.method public commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)Z
    .registers 3
    .parameter "correctionInfo"

    #@0
    .prologue
    .line 192
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .registers 10
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 204
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@3
    if-eqz v4, :cond_5a

    #@5
    .line 205
    iget-object v4, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@7
    instance-of v4, v4, Landroid/widget/TextView;

    #@9
    if-eqz v4, :cond_5a

    #@b
    iget-object v4, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@d
    check-cast v4, Landroid/widget/TextView;

    #@f
    invoke-virtual {v4}, Landroid/widget/TextView;->isNoEmojiEditMode()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_5a

    #@15
    .line 206
    const/4 v2, 0x0

    #@16
    .local v2, i:I
    :goto_16
    add-int/lit8 v4, v2, 0x1

    #@18
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@1b
    move-result v6

    #@1c
    if-ge v4, v6, :cond_5a

    #@1e
    .line 207
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@21
    move-result v1

    #@22
    .line 208
    .local v1, high:C
    add-int/lit8 v4, v2, 0x1

    #@24
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    #@27
    move-result v3

    #@28
    .line 209
    .local v3, low:C
    invoke-static {v1, v3}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    #@2b
    move-result v4

    #@2c
    if-eqz v4, :cond_57

    #@2e
    .line 210
    invoke-static {v1, v3}, Ljava/lang/Character;->toCodePoint(CC)I

    #@31
    move-result v0

    #@32
    .line 212
    .local v0, codePoint:I
    invoke-static {}, Landroid/text/Layout;->getEmojiFactory()Landroid/emoji/EmojiFactory;

    #@35
    move-result-object v4

    #@36
    if-eqz v4, :cond_4c

    #@38
    invoke-static {}, Landroid/text/Layout;->getEmojiFactory()Landroid/emoji/EmojiFactory;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Landroid/emoji/EmojiFactory;->getMinimumAndroidPua()I

    #@3f
    move-result v4

    #@40
    if-lt v0, v4, :cond_4c

    #@42
    invoke-static {}, Landroid/text/Layout;->getEmojiFactory()Landroid/emoji/EmojiFactory;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Landroid/emoji/EmojiFactory;->getMaximumAndroidPua()I

    #@49
    move-result v4

    #@4a
    if-le v0, v4, :cond_57

    #@4c
    :cond_4c
    const/high16 v4, 0x2

    #@4e
    if-lt v0, v4, :cond_55

    #@50
    const v4, 0x2ffff

    #@53
    if-le v0, v4, :cond_57

    #@55
    :cond_55
    move v4, v5

    #@56
    .line 226
    .end local v0           #codePoint:I
    .end local v1           #high:C
    .end local v2           #i:I
    .end local v3           #low:C
    :goto_56
    return v4

    #@57
    .line 206
    .restart local v1       #high:C
    .restart local v2       #i:I
    .restart local v3       #low:C
    :cond_57
    add-int/lit8 v2, v2, 0x1

    #@59
    goto :goto_16

    #@5a
    .line 224
    .end local v1           #high:C
    .end local v2           #i:I
    .end local v3           #low:C
    :cond_5a
    invoke-direct {p0, p1, p2, v5}, Landroid/view/inputmethod/BaseInputConnection;->replaceText(Ljava/lang/CharSequence;IZ)V

    #@5d
    .line 225
    invoke-direct {p0}, Landroid/view/inputmethod/BaseInputConnection;->sendCurrentText()V

    #@60
    .line 226
    const/4 v4, 0x1

    #@61
    goto :goto_56
.end method

.method public deleteSurroundingText(II)Z
    .registers 15
    .parameter "beforeLength"
    .parameter "afterLength"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v11, -0x1

    #@2
    .line 238
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@5
    move-result-object v4

    #@6
    .line 239
    .local v4, content:Landroid/text/Editable;
    if-nez v4, :cond_a

    #@8
    const/4 v9, 0x0

    #@9
    .line 297
    :goto_9
    return v9

    #@a
    .line 241
    :cond_a
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->beginBatchEdit()Z

    #@d
    .line 243
    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@10
    move-result v0

    #@11
    .line 244
    .local v0, a:I
    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@14
    move-result v1

    #@15
    .line 246
    .local v1, b:I
    if-le v0, v1, :cond_1a

    #@17
    .line 247
    move v8, v0

    #@18
    .line 248
    .local v8, tmp:I
    move v0, v1

    #@19
    .line 249
    move v1, v8

    #@1a
    .line 252
    .end local v8           #tmp:I
    :cond_1a
    sget-boolean v10, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@1c
    if-eqz v10, :cond_26

    #@1e
    .line 253
    if-gez p1, :cond_60

    #@20
    .line 254
    mul-int/lit8 p1, p1, -0x1

    #@22
    .line 255
    mul-int/lit8 p2, p2, -0x1

    #@24
    .line 256
    add-int/2addr v0, p1

    #@25
    .line 257
    add-int/2addr v1, p1

    #@26
    .line 265
    :cond_26
    :goto_26
    invoke-static {v4}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    #@29
    move-result v2

    #@2a
    .line 266
    .local v2, ca:I
    invoke-static {v4}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    #@2d
    move-result v3

    #@2e
    .line 267
    .local v3, cb:I
    if-ge v3, v2, :cond_33

    #@30
    .line 268
    move v8, v2

    #@31
    .line 269
    .restart local v8       #tmp:I
    move v2, v3

    #@32
    .line 270
    move v3, v8

    #@33
    .line 272
    .end local v8           #tmp:I
    :cond_33
    if-eq v2, v11, :cond_3d

    #@35
    if-eq v3, v11, :cond_3d

    #@37
    .line 273
    if-ge v2, v0, :cond_3a

    #@39
    move v0, v2

    #@3a
    .line 274
    :cond_3a
    if-le v3, v1, :cond_3d

    #@3c
    move v1, v3

    #@3d
    .line 277
    :cond_3d
    const/4 v5, 0x0

    #@3e
    .line 279
    .local v5, deleted:I
    if-lez p1, :cond_4a

    #@40
    .line 280
    sub-int v7, v0, p1

    #@42
    .line 281
    .local v7, start:I
    if-gez v7, :cond_45

    #@44
    const/4 v7, 0x0

    #@45
    .line 282
    :cond_45
    invoke-interface {v4, v7, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@48
    .line 283
    sub-int v5, v0, v7

    #@4a
    .line 286
    .end local v7           #start:I
    :cond_4a
    if-lez p2, :cond_5c

    #@4c
    .line 287
    sub-int/2addr v1, v5

    #@4d
    .line 289
    add-int v6, v1, p2

    #@4f
    .line 290
    .local v6, end:I
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    #@52
    move-result v10

    #@53
    if-le v6, v10, :cond_59

    #@55
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    #@58
    move-result v6

    #@59
    .line 292
    :cond_59
    invoke-interface {v4, v1, v6}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@5c
    .line 295
    .end local v6           #end:I
    :cond_5c
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->endBatchEdit()Z

    #@5f
    goto :goto_9

    #@60
    .line 258
    .end local v2           #ca:I
    .end local v3           #cb:I
    .end local v5           #deleted:I
    :cond_60
    if-eq v0, v1, :cond_26

    #@62
    if-ne p1, v9, :cond_26

    #@64
    .line 259
    sub-int p1, v1, v0

    #@66
    .line 260
    move v0, v1

    #@67
    goto :goto_26
.end method

.method public endBatchEdit()Z
    .registers 2

    #@0
    .prologue
    .line 157
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public finishComposingText()Z
    .registers 3

    #@0
    .prologue
    .line 307
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    .line 308
    .local v0, content:Landroid/text/Editable;
    if-eqz v0, :cond_12

    #@6
    .line 309
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->beginBatchEdit()Z

    #@9
    .line 310
    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    #@c
    .line 311
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->endBatchEdit()Z

    #@f
    .line 312
    invoke-direct {p0}, Landroid/view/inputmethod/BaseInputConnection;->sendCurrentText()V

    #@12
    .line 314
    :cond_12
    const/4 v1, 0x1

    #@13
    return v1
.end method

.method public getCursorCapsMode(I)I
    .registers 8
    .parameter "reqModes"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 323
    iget-boolean v5, p0, Landroid/view/inputmethod/BaseInputConnection;->mDummyMode:Z

    #@3
    if-eqz v5, :cond_6

    #@5
    .line 337
    :cond_5
    :goto_5
    return v4

    #@6
    .line 325
    :cond_6
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@9
    move-result-object v2

    #@a
    .line 326
    .local v2, content:Landroid/text/Editable;
    if-eqz v2, :cond_5

    #@c
    .line 328
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@f
    move-result v0

    #@10
    .line 329
    .local v0, a:I
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@13
    move-result v1

    #@14
    .line 331
    .local v1, b:I
    if-le v0, v1, :cond_19

    #@16
    .line 332
    move v3, v0

    #@17
    .line 333
    .local v3, tmp:I
    move v0, v1

    #@18
    .line 334
    move v1, v3

    #@19
    .line 337
    .end local v3           #tmp:I
    :cond_19
    invoke-static {v2, v0, p1}, Landroid/text/TextUtils;->getCapsMode(Ljava/lang/CharSequence;II)I

    #@1c
    move-result v4

    #@1d
    goto :goto_5
.end method

.method public getEditable()Landroid/text/Editable;
    .registers 3

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mEditable:Landroid/text/Editable;

    #@2
    if-nez v0, :cond_16

    #@4
    .line 140
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    #@7
    move-result-object v0

    #@8
    const-string v1, ""

    #@a
    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mEditable:Landroid/text/Editable;

    #@10
    .line 141
    iget-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mEditable:Landroid/text/Editable;

    #@12
    const/4 v1, 0x0

    #@13
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@16
    .line 143
    :cond_16
    iget-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mEditable:Landroid/text/Editable;

    #@18
    return-object v0
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .registers 4
    .parameter "request"
    .parameter "flags"

    #@0
    .prologue
    .line 344
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getSelectedText(I)Ljava/lang/CharSequence;
    .registers 7
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 383
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@4
    move-result-object v2

    #@5
    .line 384
    .local v2, content:Landroid/text/Editable;
    if-nez v2, :cond_8

    #@7
    .line 400
    :cond_7
    :goto_7
    return-object v4

    #@8
    .line 386
    :cond_8
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@b
    move-result v0

    #@c
    .line 387
    .local v0, a:I
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@f
    move-result v1

    #@10
    .line 389
    .local v1, b:I
    if-le v0, v1, :cond_15

    #@12
    .line 390
    move v3, v0

    #@13
    .line 391
    .local v3, tmp:I
    move v0, v1

    #@14
    .line 392
    move v1, v3

    #@15
    .line 395
    .end local v3           #tmp:I
    :cond_15
    if-eq v0, v1, :cond_7

    #@17
    .line 397
    and-int/lit8 v4, p1, 0x1

    #@19
    if-eqz v4, :cond_20

    #@1b
    .line 398
    invoke-interface {v2, v0, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@1e
    move-result-object v4

    #@1f
    goto :goto_7

    #@20
    .line 400
    :cond_20
    invoke-static {v2, v0, v1}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    goto :goto_7
.end method

.method public getTextAfterCursor(II)Ljava/lang/CharSequence;
    .registers 9
    .parameter "length"
    .parameter "flags"

    #@0
    .prologue
    .line 408
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v2

    #@4
    .line 409
    .local v2, content:Landroid/text/Editable;
    if-nez v2, :cond_8

    #@6
    const/4 v4, 0x0

    #@7
    .line 433
    :goto_7
    return-object v4

    #@8
    .line 411
    :cond_8
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@b
    move-result v0

    #@c
    .line 412
    .local v0, a:I
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@f
    move-result v1

    #@10
    .line 414
    .local v1, b:I
    if-le v0, v1, :cond_15

    #@12
    .line 415
    move v3, v0

    #@13
    .line 416
    .local v3, tmp:I
    move v0, v1

    #@14
    .line 417
    move v1, v3

    #@15
    .line 421
    .end local v3           #tmp:I
    :cond_15
    if-gez v1, :cond_18

    #@17
    .line 422
    const/4 v1, 0x0

    #@18
    .line 425
    :cond_18
    add-int v4, v1, p1

    #@1a
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@1d
    move-result v5

    #@1e
    if-le v4, v5, :cond_26

    #@20
    .line 426
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@23
    move-result v4

    #@24
    sub-int p1, v4, v1

    #@26
    .line 430
    :cond_26
    and-int/lit8 v4, p2, 0x1

    #@28
    if-eqz v4, :cond_31

    #@2a
    .line 431
    add-int v4, v1, p1

    #@2c
    invoke-interface {v2, v1, v4}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@2f
    move-result-object v4

    #@30
    goto :goto_7

    #@31
    .line 433
    :cond_31
    add-int v4, v1, p1

    #@33
    invoke-static {v2, v1, v4}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    goto :goto_7
.end method

.method public getTextBeforeCursor(II)Ljava/lang/CharSequence;
    .registers 8
    .parameter "length"
    .parameter "flags"

    #@0
    .prologue
    .line 352
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v2

    #@4
    .line 353
    .local v2, content:Landroid/text/Editable;
    if-nez v2, :cond_8

    #@6
    const/4 v4, 0x0

    #@7
    .line 375
    :goto_7
    return-object v4

    #@8
    .line 355
    :cond_8
    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@b
    move-result v0

    #@c
    .line 356
    .local v0, a:I
    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@f
    move-result v1

    #@10
    .line 358
    .local v1, b:I
    if-le v0, v1, :cond_15

    #@12
    .line 359
    move v3, v0

    #@13
    .line 360
    .local v3, tmp:I
    move v0, v1

    #@14
    .line 361
    move v1, v3

    #@15
    .line 364
    .end local v3           #tmp:I
    :cond_15
    if-gtz v0, :cond_1a

    #@17
    .line 365
    const-string v4, ""

    #@19
    goto :goto_7

    #@1a
    .line 368
    :cond_1a
    if-le p1, v0, :cond_1d

    #@1c
    .line 369
    move p1, v0

    #@1d
    .line 372
    :cond_1d
    and-int/lit8 v4, p2, 0x1

    #@1f
    if-eqz v4, :cond_28

    #@21
    .line 373
    sub-int v4, v0, p1

    #@23
    invoke-interface {v2, v4, v0}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    #@26
    move-result-object v4

    #@27
    goto :goto_7

    #@28
    .line 375
    :cond_28
    sub-int v4, v0, p1

    #@2a
    invoke-static {v2, v4, v0}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    goto :goto_7
.end method

.method public performContextMenuAction(I)Z
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 458
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public performEditorAction(I)Z
    .registers 17
    .parameter "actionCode"

    #@0
    .prologue
    .line 440
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v1

    #@4
    .line 441
    .local v1, eventTime:J
    new-instance v0, Landroid/view/KeyEvent;

    #@6
    const/4 v5, 0x0

    #@7
    const/16 v6, 0x42

    #@9
    const/4 v7, 0x0

    #@a
    const/4 v8, 0x0

    #@b
    const/4 v9, -0x1

    #@c
    const/4 v10, 0x0

    #@d
    const/16 v11, 0x16

    #@f
    move-wide v3, v1

    #@10
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@13
    invoke-virtual {p0, v0}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@16
    .line 446
    new-instance v3, Landroid/view/KeyEvent;

    #@18
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1b
    move-result-wide v4

    #@1c
    const/4 v8, 0x1

    #@1d
    const/16 v9, 0x42

    #@1f
    const/4 v10, 0x0

    #@20
    const/4 v11, 0x0

    #@21
    const/4 v12, -0x1

    #@22
    const/4 v13, 0x0

    #@23
    const/16 v14, 0x16

    #@25
    move-wide v6, v1

    #@26
    invoke-direct/range {v3 .. v14}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@29
    invoke-virtual {p0, v3}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@2c
    .line 451
    const/4 v0, 0x1

    #@2d
    return v0
.end method

.method public performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 465
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected reportFinish()V
    .registers 1

    #@0
    .prologue
    .line 167
    return-void
.end method

.method public reportFullscreenMode(Z)Z
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 565
    iget-object v0, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/inputmethod/InputMethodManager;->setFullscreenMode(Z)V

    #@5
    .line 566
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 547
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v2, v1, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@4
    monitor-enter v2

    #@5
    .line 548
    :try_start_5
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@7
    if-eqz v1, :cond_27

    #@9
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mTargetView:Landroid/view/View;

    #@b
    invoke-virtual {v1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@e
    move-result-object v0

    #@f
    .line 549
    .local v0, viewRootImpl:Landroid/view/ViewRootImpl;
    :goto_f
    if-nez v0, :cond_1f

    #@11
    .line 550
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@13
    iget-object v1, v1, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@15
    if-eqz v1, :cond_1f

    #@17
    .line 551
    iget-object v1, p0, Landroid/view/inputmethod/BaseInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@19
    iget-object v1, v1, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@1b
    invoke-virtual {v1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@1e
    move-result-object v0

    #@1f
    .line 554
    :cond_1f
    if-eqz v0, :cond_24

    #@21
    .line 555
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl;->dispatchKeyFromIme(Landroid/view/KeyEvent;)V

    #@24
    .line 557
    :cond_24
    monitor-exit v2

    #@25
    .line 558
    const/4 v1, 0x0

    #@26
    return v1

    #@27
    .line 548
    .end local v0           #viewRootImpl:Landroid/view/ViewRootImpl;
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_f

    #@29
    .line 557
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_5 .. :try_end_2b} :catchall_29

    #@2b
    throw v1
.end method

.method public setComposingRegion(II)Z
    .registers 11
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/16 v7, 0x121

    #@2
    .line 480
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@5
    move-result-object v2

    #@6
    .line 481
    .local v2, content:Landroid/text/Editable;
    if-eqz v2, :cond_47

    #@8
    .line 482
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->beginBatchEdit()Z

    #@b
    .line 483
    invoke-static {v2}, Landroid/view/inputmethod/BaseInputConnection;->removeComposingSpans(Landroid/text/Spannable;)V

    #@e
    .line 484
    move v0, p1

    #@f
    .line 485
    .local v0, a:I
    move v1, p2

    #@10
    .line 486
    .local v1, b:I
    if-le v0, v1, :cond_15

    #@12
    .line 487
    move v5, v0

    #@13
    .line 488
    .local v5, tmp:I
    move v0, v1

    #@14
    .line 489
    move v1, v5

    #@15
    .line 492
    .end local v5           #tmp:I
    :cond_15
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    #@18
    move-result v4

    #@19
    .line 493
    .local v4, length:I
    if-gez v0, :cond_1c

    #@1b
    const/4 v0, 0x0

    #@1c
    .line 494
    :cond_1c
    if-gez v1, :cond_1f

    #@1e
    const/4 v1, 0x0

    #@1f
    .line 495
    :cond_1f
    if-le v0, v4, :cond_22

    #@21
    move v0, v4

    #@22
    .line 496
    :cond_22
    if-le v1, v4, :cond_25

    #@24
    move v1, v4

    #@25
    .line 498
    :cond_25
    invoke-direct {p0}, Landroid/view/inputmethod/BaseInputConnection;->ensureDefaultComposingSpans()V

    #@28
    .line 499
    iget-object v6, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@2a
    if-eqz v6, :cond_3c

    #@2c
    .line 500
    const/4 v3, 0x0

    #@2d
    .local v3, i:I
    :goto_2d
    iget-object v6, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@2f
    array-length v6, v6

    #@30
    if-ge v3, v6, :cond_3c

    #@32
    .line 501
    iget-object v6, p0, Landroid/view/inputmethod/BaseInputConnection;->mDefaultComposingSpans:[Ljava/lang/Object;

    #@34
    aget-object v6, v6, v3

    #@36
    invoke-interface {v2, v6, v0, v1, v7}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@39
    .line 500
    add-int/lit8 v3, v3, 0x1

    #@3b
    goto :goto_2d

    #@3c
    .line 506
    .end local v3           #i:I
    :cond_3c
    sget-object v6, Landroid/view/inputmethod/BaseInputConnection;->COMPOSING:Ljava/lang/Object;

    #@3e
    invoke-interface {v2, v6, v0, v1, v7}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    #@41
    .line 509
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->endBatchEdit()Z

    #@44
    .line 510
    invoke-direct {p0}, Landroid/view/inputmethod/BaseInputConnection;->sendCurrentText()V

    #@47
    .line 512
    .end local v0           #a:I
    .end local v1           #b:I
    .end local v4           #length:I
    :cond_47
    const/4 v6, 0x1

    #@48
    return v6
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .registers 4
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 475
    invoke-direct {p0, p1, p2, v0}, Landroid/view/inputmethod/BaseInputConnection;->replaceText(Ljava/lang/CharSequence;IZ)V

    #@4
    .line 476
    return v0
.end method

.method public setSelection(II)Z
    .registers 7
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 521
    invoke-virtual {p0}, Landroid/view/inputmethod/BaseInputConnection;->getEditable()Landroid/text/Editable;

    #@4
    move-result-object v0

    #@5
    .line 522
    .local v0, content:Landroid/text/Editable;
    if-nez v0, :cond_9

    #@7
    const/4 v2, 0x0

    #@8
    .line 539
    :cond_8
    :goto_8
    return v2

    #@9
    .line 523
    :cond_9
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@c
    move-result v1

    #@d
    .line 524
    .local v1, len:I
    if-gt p1, v1, :cond_8

    #@f
    if-gt p2, v1, :cond_8

    #@11
    .line 531
    if-ne p1, p2, :cond_1f

    #@13
    const/16 v3, 0x800

    #@15
    invoke-static {v0, v3}, Landroid/text/method/MetaKeyKeyListener;->getMetaState(Ljava/lang/CharSequence;I)I

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_1f

    #@1b
    .line 535
    invoke-static {v0, p1}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@1e
    goto :goto_8

    #@1f
    .line 537
    :cond_1f
    invoke-static {v0, p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@22
    goto :goto_8
.end method
