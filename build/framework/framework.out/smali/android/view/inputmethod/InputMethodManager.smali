.class public final Landroid/view/inputmethod/InputMethodManager;
.super Ljava/lang/Object;
.source "InputMethodManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/inputmethod/InputMethodManager$PendingEvent;,
        Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;,
        Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;,
        Landroid/view/inputmethod/InputMethodManager$H;
    }
.end annotation


# static fields
.field public static final CONTROL_START_INITIAL:I = 0x100

.field public static final CONTROL_WINDOW_FIRST:I = 0x4

.field public static final CONTROL_WINDOW_IS_TEXT_EDITOR:I = 0x2

.field public static final CONTROL_WINDOW_VIEW_HAS_FOCUS:I = 0x1

.field static final DEBUG:Z = false

.field public static final HIDE_IMPLICIT_ONLY:I = 0x1

.field public static final HIDE_NOT_ALWAYS:I = 0x2

.field static final INPUT_METHOD_NOT_RESPONDING_TIMEOUT:J = 0x9c4L

.field private static final MAX_PENDING_EVENT_POOL_SIZE:I = 0x4

.field static final MSG_BIND:I = 0x2

.field static final MSG_DUMP:I = 0x1

.field static final MSG_EVENT_TIMEOUT:I = 0x5

.field static final MSG_SCREEN_ON_OFF:I = 0x6

.field static final MSG_SET_ACTIVE:I = 0x4

.field static final MSG_UNBIND:I = 0x3

.field public static final RESULT_HIDDEN:I = 0x3

.field public static final RESULT_SHOWN:I = 0x2

.field public static final RESULT_UNCHANGED_HIDDEN:I = 0x1

.field public static final RESULT_UNCHANGED_SHOWN:I = 0x0

.field public static final SHOW_FORCED:I = 0x2

.field public static final SHOW_IMPLICIT:I = 0x1

.field static final TAG:Ljava/lang/String; = "InputMethodManager"

.field static mInstance:Landroid/view/inputmethod/InputMethodManager;

.field static final mInstanceSync:Ljava/lang/Object;


# instance fields
.field mActive:Z

.field mBindSequence:I

.field final mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

.field mCompletions:[Landroid/view/inputmethod/CompletionInfo;

.field mCurId:Ljava/lang/String;

.field mCurMethod:Lcom/android/internal/view/IInputMethodSession;

.field mCurRootView:Landroid/view/View;

.field mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

.field mCursorCandEnd:I

.field mCursorCandStart:I

.field mCursorRect:Landroid/graphics/Rect;

.field mCursorSelEnd:I

.field mCursorSelStart:I

.field final mDummyInputConnection:Landroid/view/inputmethod/InputConnection;

.field mFirstPendingEvent:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

.field mFullscreenMode:Z

.field final mH:Landroid/view/inputmethod/InputMethodManager$H;

.field mHasBeenInactive:Z

.field final mIInputContext:Lcom/android/internal/view/IInputContext;

.field private mIWindowManager:Landroid/view/IWindowManager;

.field final mInputMethodCallback:Lcom/android/internal/view/IInputMethodCallback;

.field final mMainLooper:Landroid/os/Looper;

.field mNextServedView:Landroid/view/View;

.field mPendingEventPool:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

.field mPendingEventPoolSize:I

.field mServedConnecting:Z

.field mServedInputConnection:Landroid/view/inputmethod/InputConnection;

.field mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

.field mServedView:Landroid/view/View;

.field final mService:Lcom/android/internal/view/IInputMethodManager;

.field mTmpCursorRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 205
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/inputmethod/InputMethodManager;->mInstanceSync:Ljava/lang/Object;

    #@7
    return-void
.end method

.method constructor <init>(Lcom/android/internal/view/IInputMethodManager;Landroid/os/Looper;)V
    .registers 5
    .parameter "service"
    .parameter "looper"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 255
    iput-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mActive:Z

    #@6
    .line 261
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z

    #@9
    .line 307
    new-instance v0, Landroid/graphics/Rect;

    #@b
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@e
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mTmpCursorRect:Landroid/graphics/Rect;

    #@10
    .line 308
    new-instance v0, Landroid/graphics/Rect;

    #@12
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@15
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorRect:Landroid/graphics/Rect;

    #@17
    .line 319
    const/4 v0, -0x1

    #@18
    iput v0, p0, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@1a
    .line 514
    new-instance v0, Landroid/view/inputmethod/InputMethodManager$1;

    #@1c
    invoke-direct {v0, p0}, Landroid/view/inputmethod/InputMethodManager$1;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    #@1f
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@21
    .line 556
    new-instance v0, Landroid/view/inputmethod/BaseInputConnection;

    #@23
    invoke-direct {v0, p0, v1}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/inputmethod/InputMethodManager;Z)V

    #@26
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mDummyInputConnection:Landroid/view/inputmethod/InputConnection;

    #@28
    .line 558
    new-instance v0, Landroid/view/inputmethod/InputMethodManager$2;

    #@2a
    invoke-direct {v0, p0}, Landroid/view/inputmethod/InputMethodManager$2;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    #@2d
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mInputMethodCallback:Lcom/android/internal/view/IInputMethodCallback;

    #@2f
    .line 571
    iput-object p1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@31
    .line 572
    iput-object p2, p0, Landroid/view/inputmethod/InputMethodManager;->mMainLooper:Landroid/os/Looper;

    #@33
    .line 573
    new-instance v0, Landroid/view/inputmethod/InputMethodManager$H;

    #@35
    invoke-direct {v0, p0, p2}, Landroid/view/inputmethod/InputMethodManager$H;-><init>(Landroid/view/inputmethod/InputMethodManager;Landroid/os/Looper;)V

    #@38
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@3a
    .line 574
    new-instance v0, Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@3c
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mDummyInputConnection:Landroid/view/inputmethod/InputConnection;

    #@3e
    invoke-direct {v0, p2, v1, p0}, Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;-><init>(Landroid/os/Looper;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/InputMethodManager;)V

    #@41
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mIInputContext:Lcom/android/internal/view/IInputContext;

    #@43
    .line 577
    sget-object v0, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@45
    if-nez v0, :cond_49

    #@47
    .line 578
    sput-object p0, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@49
    .line 581
    :cond_49
    const-string/jumbo v0, "window"

    #@4c
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@4f
    move-result-object v0

    #@50
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@53
    move-result-object v0

    #@54
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mIWindowManager:Landroid/view/IWindowManager;

    #@56
    .line 584
    return-void
.end method

.method static synthetic access$000(Landroid/view/inputmethod/InputMethodManager;ZZ)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Landroid/view/inputmethod/InputMethodManager;->checkFocusNoStartInput(ZZ)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private checkFocusNoStartInput(ZZ)Z
    .registers 9
    .parameter "forceNewFocus"
    .parameter "finishComposingText"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1311
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@4
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@6
    if-ne v3, v4, :cond_b

    #@8
    if-nez p1, :cond_b

    #@a
    .line 1347
    :goto_a
    return v1

    #@b
    .line 1315
    :cond_b
    const/4 v0, 0x0

    #@c
    .line 1316
    .local v0, ic:Landroid/view/inputmethod/InputConnection;
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@e
    monitor-enter v3

    #@f
    .line 1317
    :try_start_f
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@11
    iget-object v5, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@13
    if-ne v4, v5, :cond_1c

    #@15
    if-nez p1, :cond_1c

    #@17
    .line 1318
    monitor-exit v3

    #@18
    goto :goto_a

    #@19
    .line 1341
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v3
    :try_end_1b
    .catchall {:try_start_f .. :try_end_1b} :catchall_19

    #@1b
    throw v1

    #@1c
    .line 1326
    :cond_1c
    :try_start_1c
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@1e
    if-nez v4, :cond_28

    #@20
    .line 1327
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->finishInputLocked()V

    #@23
    .line 1331
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->closeCurrentInput()V

    #@26
    .line 1332
    monitor-exit v3

    #@27
    goto :goto_a

    #@28
    .line 1335
    :cond_28
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@2a
    .line 1337
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@2c
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@2e
    .line 1338
    const/4 v1, 0x0

    #@2f
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@31
    .line 1339
    const/4 v1, 0x0

    #@32
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@34
    .line 1340
    const/4 v1, 0x1

    #@35
    iput-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@37
    .line 1341
    monitor-exit v3
    :try_end_38
    .catchall {:try_start_1c .. :try_end_38} :catchall_19

    #@38
    .line 1343
    if-eqz p2, :cond_3f

    #@3a
    if-eqz v0, :cond_3f

    #@3c
    .line 1344
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@3f
    :cond_3f
    move v1, v2

    #@40
    .line 1347
    goto :goto_a
.end method

.method private dequeuePendingEventLocked(I)Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    .registers 6
    .parameter "seq"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1741
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mFirstPendingEvent:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@3
    .line 1742
    .local v0, p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    if-nez v0, :cond_6

    #@5
    .line 1759
    :cond_5
    :goto_5
    return-object v2

    #@6
    .line 1745
    :cond_6
    iget v3, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mSeq:I

    #@8
    if-ne v3, p1, :cond_12

    #@a
    .line 1746
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@c
    iput-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mFirstPendingEvent:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@e
    .line 1758
    :goto_e
    iput-object v2, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@10
    move-object v2, v0

    #@11
    .line 1759
    goto :goto_5

    #@12
    .line 1750
    :cond_12
    move-object v1, v0

    #@13
    .line 1751
    .local v1, prev:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    iget-object v0, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@15
    .line 1752
    if-eqz v0, :cond_5

    #@17
    .line 1755
    iget v3, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mSeq:I

    #@19
    if-ne v3, p1, :cond_12

    #@1b
    .line 1756
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@1d
    iput-object v3, v1, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@1f
    goto :goto_e
.end method

.method private enqueuePendingEventLocked(JILjava/lang/String;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V
    .registers 11
    .parameter "startTime"
    .parameter "seq"
    .parameter "inputMethodId"
    .parameter "callback"

    #@0
    .prologue
    .line 1731
    invoke-direct/range {p0 .. p5}, Landroid/view/inputmethod/InputMethodManager;->obtainPendingEventLocked(JILjava/lang/String;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@3
    move-result-object v1

    #@4
    .line 1732
    .local v1, p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mFirstPendingEvent:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@6
    iput-object v2, v1, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@8
    .line 1733
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mFirstPendingEvent:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@a
    .line 1735
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@c
    const/4 v3, 0x5

    #@d
    const/4 v4, 0x0

    #@e
    invoke-virtual {v2, v3, p3, v4, v1}, Landroid/view/inputmethod/InputMethodManager$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 1736
    .local v0, msg:Landroid/os/Message;
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Message;->setAsynchronous(Z)V

    #@16
    .line 1737
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@18
    const-wide/16 v3, 0x9c4

    #@1a
    invoke-virtual {v2, v0, v3, v4}, Landroid/view/inputmethod/InputMethodManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1d
    .line 1738
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 592
    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/view/inputmethod/InputMethodManager;->getInstance(Landroid/os/Looper;)Landroid/view/inputmethod/InputMethodManager;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getInstance(Landroid/os/Looper;)Landroid/view/inputmethod/InputMethodManager;
    .registers 5
    .parameter "mainLooper"

    #@0
    .prologue
    .line 601
    sget-object v3, Landroid/view/inputmethod/InputMethodManager;->mInstanceSync:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 602
    :try_start_3
    sget-object v2, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@5
    if-eqz v2, :cond_b

    #@7
    .line 603
    sget-object v2, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@9
    monitor-exit v3

    #@a
    .line 609
    :goto_a
    return-object v2

    #@b
    .line 605
    :cond_b
    const-string v2, "input_method"

    #@d
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v0

    #@11
    .line 606
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodManager;

    #@14
    move-result-object v1

    #@15
    .line 607
    .local v1, service:Lcom/android/internal/view/IInputMethodManager;
    new-instance v2, Landroid/view/inputmethod/InputMethodManager;

    #@17
    invoke-direct {v2, v1, p0}, Landroid/view/inputmethod/InputMethodManager;-><init>(Lcom/android/internal/view/IInputMethodManager;Landroid/os/Looper;)V

    #@1a
    sput-object v2, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@1c
    .line 608
    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_20

    #@1d
    .line 609
    sget-object v2, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@1f
    goto :goto_a

    #@20
    .line 608
    .end local v0           #b:Landroid/os/IBinder;
    .end local v1           #service:Lcom/android/internal/view/IInputMethodManager;
    :catchall_20
    move-exception v2

    #@21
    :try_start_21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method private notifyInputConnectionFinished()V
    .registers 3

    #@0
    .prologue
    .line 805
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@2
    if-eqz v1, :cond_15

    #@4
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@6
    if-eqz v1, :cond_15

    #@8
    .line 810
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@a
    invoke-virtual {v1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@d
    move-result-object v0

    #@e
    .line 811
    .local v0, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_15

    #@10
    .line 813
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@12
    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl;->dispatchFinishInputConnection(Landroid/view/inputmethod/InputConnection;)V

    #@15
    .line 816
    .end local v0           #viewRootImpl:Landroid/view/ViewRootImpl;
    :cond_15
    return-void
.end method

.method private obtainPendingEventLocked(JILjava/lang/String;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    .registers 9
    .parameter "startTime"
    .parameter "seq"
    .parameter "inputMethodId"
    .parameter "callback"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1764
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPool:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@3
    .line 1765
    .local v0, p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    if-eqz v0, :cond_1a

    #@5
    .line 1766
    iget v1, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPoolSize:I

    #@7
    add-int/lit8 v1, v1, -0x1

    #@9
    iput v1, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPoolSize:I

    #@b
    .line 1767
    iget-object v1, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@d
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPool:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@f
    .line 1768
    iput-object v2, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@11
    .line 1773
    :goto_11
    iput-wide p1, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mStartTime:J

    #@13
    .line 1774
    iput p3, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mSeq:I

    #@15
    .line 1775
    iput-object p4, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mInputMethodId:Ljava/lang/String;

    #@17
    .line 1776
    iput-object p5, v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mCallback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;

    #@19
    .line 1777
    return-object v0

    #@1a
    .line 1770
    :cond_1a
    new-instance v0, Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@1c
    .end local v0           #p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    invoke-direct {v0, v2}, Landroid/view/inputmethod/InputMethodManager$PendingEvent;-><init>(Landroid/view/inputmethod/InputMethodManager$1;)V

    #@1f
    .restart local v0       #p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    goto :goto_11
.end method

.method public static peekInstance()Landroid/view/inputmethod/InputMethodManager;
    .registers 1

    #@0
    .prologue
    .line 618
    sget-object v0, Landroid/view/inputmethod/InputMethodManager;->mInstance:Landroid/view/inputmethod/InputMethodManager;

    #@2
    return-object v0
.end method

.method private recyclePendingEventLocked(Landroid/view/inputmethod/InputMethodManager$PendingEvent;)V
    .registers 4
    .parameter "p"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1781
    iput-object v0, p1, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mInputMethodId:Ljava/lang/String;

    #@3
    .line 1782
    iput-object v0, p1, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mCallback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;

    #@5
    .line 1784
    iget v0, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPoolSize:I

    #@7
    const/4 v1, 0x4

    #@8
    if-ge v0, v1, :cond_16

    #@a
    .line 1785
    iget v0, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPoolSize:I

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    iput v0, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPoolSize:I

    #@10
    .line 1786
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPool:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@12
    iput-object v0, p1, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mNext:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@14
    .line 1787
    iput-object p1, p0, Landroid/view/inputmethod/InputMethodManager;->mPendingEventPool:Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@16
    .line 1789
    :cond_16
    return-void
.end method

.method static scheduleCheckFocusLocked(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 1294
    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@3
    move-result-object v0

    #@4
    .line 1295
    .local v0, viewRootImpl:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_9

    #@6
    .line 1296
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->dispatchCheckFocus()V

    #@9
    .line 1298
    :cond_9
    return-void
.end method

.method private showInputMethodPickerLocked()V
    .registers 5

    #@0
    .prologue
    .line 1799
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@4
    invoke-interface {v1, v2}, Lcom/android/internal/view/IInputMethodManager;->showInputMethodPickerFromClient(Lcom/android/internal/view/IInputMethodClient;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1803
    :goto_7
    return-void

    #@8
    .line 1800
    :catch_8
    move-exception v0

    #@9
    .line 1801
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "IME died: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_7
.end method


# virtual methods
.method public checkFocus()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1304
    const/4 v0, 0x1

    #@2
    invoke-direct {p0, v1, v0}, Landroid/view/inputmethod/InputMethodManager;->checkFocusNoStartInput(ZZ)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_c

    #@8
    .line 1305
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0, v1, v1, v1}, Landroid/view/inputmethod/InputMethodManager;->startInputInner(Landroid/os/IBinder;III)Z

    #@c
    .line 1307
    :cond_c
    return-void
.end method

.method clearBindingLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 757
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->clearConnectionLocked()V

    #@4
    .line 758
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@7
    .line 759
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@9
    .line 760
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@b
    .line 761
    return-void
.end method

.method clearConnectionLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 768
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@3
    .line 769
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@5
    .line 770
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 771
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@b
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;->deactivate()V

    #@e
    .line 772
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@10
    .line 774
    :cond_10
    return-void
.end method

.method closeCurrentInput()V
    .registers 5

    #@0
    .prologue
    .line 1352
    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@4
    const/4 v2, 0x2

    #@5
    const/4 v3, 0x0

    #@6
    invoke-interface {v0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager;->hideSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 1355
    :goto_9
    return-void

    #@a
    .line 1353
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public dispatchGenericMotionEvent(Landroid/content/Context;ILandroid/view/MotionEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V
    .registers 13
    .parameter "context"
    .parameter "seq"
    .parameter "motion"
    .parameter "callback"

    #@0
    .prologue
    .line 1680
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v7

    #@3
    .line 1683
    :try_start_3
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@5
    if-eqz v0, :cond_45

    #@7
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_4b

    #@9
    if-eqz v0, :cond_45

    #@b
    .line 1686
    :try_start_b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v1

    #@f
    .line 1687
    .local v1, startTime:J
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@11
    move-object v0, p0

    #@12
    move v3, p2

    #@13
    move-object v5, p4

    #@14
    invoke-direct/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->enqueuePendingEventLocked(JILjava/lang/String;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@17
    .line 1688
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@19
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mInputMethodCallback:Lcom/android/internal/view/IInputMethodCallback;

    #@1b
    invoke-interface {v0, p2, p3, v3}, Lcom/android/internal/view/IInputMethodSession;->dispatchGenericMotionEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    :try_end_1e
    .catchall {:try_start_b .. :try_end_1e} :catchall_4b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1e} :catch_20

    #@1e
    .line 1689
    :try_start_1e
    monitor-exit v7

    #@1f
    .line 1697
    .end local v1           #startTime:J
    :goto_1f
    return-void

    #@20
    .line 1690
    :catch_20
    move-exception v6

    #@21
    .line 1691
    .local v6, e:Landroid/os/RemoteException;
    const-string v0, "InputMethodManager"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "IME died: "

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v4, " dropping generic motion: "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v0, v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    .line 1694
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_45
    monitor-exit v7
    :try_end_46
    .catchall {:try_start_1e .. :try_end_46} :catchall_4b

    #@46
    .line 1696
    const/4 v0, 0x0

    #@47
    invoke-interface {p4, p2, v0}, Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;->finishedEvent(IZ)V

    #@4a
    goto :goto_1f

    #@4b
    .line 1694
    :catchall_4b
    move-exception v0

    #@4c
    :try_start_4c
    monitor-exit v7
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    throw v0
.end method

.method public dispatchKeyEvent(Landroid/content/Context;ILandroid/view/KeyEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V
    .registers 14
    .parameter "context"
    .parameter "seq"
    .parameter "key"
    .parameter "callback"

    #@0
    .prologue
    .line 1625
    const/4 v7, 0x0

    #@1
    .line 1626
    .local v7, handled:Z
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@3
    monitor-enter v8

    #@4
    .line 1629
    :try_start_4
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@6
    if-eqz v0, :cond_1a

    #@8
    .line 1630
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_1f

    #@e
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    #@11
    move-result v0

    #@12
    const/16 v3, 0x3f

    #@14
    if-ne v0, v3, :cond_1f

    #@16
    .line 1632
    invoke-direct {p0}, Landroid/view/inputmethod/InputMethodManager;->showInputMethodPickerLocked()V

    #@19
    .line 1633
    const/4 v7, 0x1

    #@1a
    .line 1646
    :cond_1a
    :goto_1a
    monitor-exit v8
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_34

    #@1b
    .line 1648
    invoke-interface {p4, p2, v7}, Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;->finishedEvent(IZ)V

    #@1e
    .line 1649
    :goto_1e
    return-void

    #@1f
    .line 1637
    :cond_1f
    :try_start_1f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@22
    move-result-wide v1

    #@23
    .line 1638
    .local v1, startTime:J
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@25
    move-object v0, p0

    #@26
    move v3, p2

    #@27
    move-object v5, p4

    #@28
    invoke-direct/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->enqueuePendingEventLocked(JILjava/lang/String;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@2b
    .line 1639
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@2d
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mInputMethodCallback:Lcom/android/internal/view/IInputMethodCallback;

    #@2f
    invoke-interface {v0, p2, p3, v3}, Lcom/android/internal/view/IInputMethodSession;->dispatchKeyEvent(ILandroid/view/KeyEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    :try_end_32
    .catchall {:try_start_1f .. :try_end_32} :catchall_34
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_32} :catch_37

    #@32
    .line 1640
    :try_start_32
    monitor-exit v8

    #@33
    goto :goto_1e

    #@34
    .line 1646
    .end local v1           #startTime:J
    :catchall_34
    move-exception v0

    #@35
    monitor-exit v8
    :try_end_36
    .catchall {:try_start_32 .. :try_end_36} :catchall_34

    #@36
    throw v0

    #@37
    .line 1641
    :catch_37
    move-exception v6

    #@38
    .line 1642
    .local v6, e:Landroid/os/RemoteException;
    :try_start_38
    const-string v0, "InputMethodManager"

    #@3a
    new-instance v3, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v4, "IME died: "

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, " dropping: "

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-static {v0, v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5c
    .catchall {:try_start_38 .. :try_end_5c} :catchall_34

    #@5c
    goto :goto_1a
.end method

.method public dispatchTrackballEvent(Landroid/content/Context;ILandroid/view/MotionEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V
    .registers 13
    .parameter "context"
    .parameter "seq"
    .parameter "motion"
    .parameter "callback"

    #@0
    .prologue
    .line 1656
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v7

    #@3
    .line 1659
    :try_start_3
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@5
    if-eqz v0, :cond_45

    #@7
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_4b

    #@9
    if-eqz v0, :cond_45

    #@b
    .line 1662
    :try_start_b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v1

    #@f
    .line 1663
    .local v1, startTime:J
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@11
    move-object v0, p0

    #@12
    move v3, p2

    #@13
    move-object v5, p4

    #@14
    invoke-direct/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->enqueuePendingEventLocked(JILjava/lang/String;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@17
    .line 1664
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@19
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mInputMethodCallback:Lcom/android/internal/view/IInputMethodCallback;

    #@1b
    invoke-interface {v0, p2, p3, v3}, Lcom/android/internal/view/IInputMethodSession;->dispatchTrackballEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    :try_end_1e
    .catchall {:try_start_b .. :try_end_1e} :catchall_4b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1e} :catch_20

    #@1e
    .line 1665
    :try_start_1e
    monitor-exit v7

    #@1f
    .line 1673
    .end local v1           #startTime:J
    :goto_1f
    return-void

    #@20
    .line 1666
    :catch_20
    move-exception v6

    #@21
    .line 1667
    .local v6, e:Landroid/os/RemoteException;
    const-string v0, "InputMethodManager"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v4, "IME died: "

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v4, " dropping trackball: "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v0, v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    .line 1670
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_45
    monitor-exit v7
    :try_end_46
    .catchall {:try_start_1e .. :try_end_46} :catchall_4b

    #@46
    .line 1672
    const/4 v0, 0x0

    #@47
    invoke-interface {p4, p2, v0}, Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;->finishedEvent(IZ)V

    #@4a
    goto :goto_1f

    #@4b
    .line 1670
    :catchall_4b
    move-exception v0

    #@4c
    :try_start_4c
    monitor-exit v7
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    throw v0
.end method

.method public displayCompletions(Landroid/view/View;[Landroid/view/inputmethod/CompletionInfo;)V
    .registers 6
    .parameter "view"
    .parameter "completions"

    #@0
    .prologue
    .line 833
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 834
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v1

    #@6
    .line 835
    :try_start_6
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eq v0, p1, :cond_18

    #@a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    if-eqz v0, :cond_16

    #@e
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10
    invoke-virtual {v0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_18

    #@16
    .line 837
    :cond_16
    monitor-exit v1

    #@17
    .line 848
    :goto_17
    return-void

    #@18
    .line 840
    :cond_18
    iput-object p2, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@1a
    .line 841
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;
    :try_end_1c
    .catchall {:try_start_6 .. :try_end_1c} :catchall_27

    #@1c
    if-eqz v0, :cond_25

    #@1e
    .line 843
    :try_start_1e
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@20
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@22
    invoke-interface {v0, v2}, Lcom/android/internal/view/IInputMethodSession;->displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    :try_end_25
    .catchall {:try_start_1e .. :try_end_25} :catchall_27
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_25} :catch_2a

    #@25
    .line 847
    :cond_25
    :goto_25
    :try_start_25
    monitor-exit v1

    #@26
    goto :goto_17

    #@27
    :catchall_27
    move-exception v0

    #@28
    monitor-exit v1
    :try_end_29
    .catchall {:try_start_25 .. :try_end_29} :catchall_27

    #@29
    throw v0

    #@2a
    .line 844
    :catch_2a
    move-exception v0

    #@2b
    goto :goto_25
.end method

.method doDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "fout"
    .parameter "args"

    #@0
    .prologue
    .line 1965
    new-instance v0, Landroid/util/PrintWriterPrinter;

    #@2
    invoke-direct {v0, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@5
    .line 1966
    .local v0, p:Landroid/util/Printer;
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Input method client state for "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, ":"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@21
    .line 1968
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "  mService="

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@39
    .line 1969
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "  mMainLooper="

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mMainLooper:Landroid/os/Looper;

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@51
    .line 1970
    new-instance v1, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v2, "  mIInputContext="

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mIInputContext:Lcom/android/internal/view/IInputContext;

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@69
    .line 1971
    new-instance v1, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v2, "  mActive="

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    iget-boolean v2, p0, Landroid/view/inputmethod/InputMethodManager;->mActive:Z

    #@76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79
    move-result-object v1

    #@7a
    const-string v2, " mHasBeenInactive="

    #@7c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    iget-boolean v2, p0, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z

    #@82
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@85
    move-result-object v1

    #@86
    const-string v2, " mBindSequence="

    #@88
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v1

    #@8c
    iget v2, p0, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@8e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v1

    #@92
    const-string v2, " mCurId="

    #@94
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v1

    #@98
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v1

    #@a2
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@a5
    .line 1975
    new-instance v1, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v2, "  mCurMethod="

    #@ac
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v1

    #@b0
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@b2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v1

    #@ba
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@bd
    .line 1976
    new-instance v1, Ljava/lang/StringBuilder;

    #@bf
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c2
    const-string v2, "  mCurRootView="

    #@c4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v1

    #@c8
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurRootView:Landroid/view/View;

    #@ca
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v1

    #@ce
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v1

    #@d2
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@d5
    .line 1977
    new-instance v1, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v2, "  mServedView="

    #@dc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v1

    #@e0
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@e2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v1

    #@e6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v1

    #@ea
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@ed
    .line 1978
    new-instance v1, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v2, "  mNextServedView="

    #@f4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v1

    #@f8
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@fa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v1

    #@fe
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v1

    #@102
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@105
    .line 1979
    new-instance v1, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v2, "  mServedConnecting="

    #@10c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v1

    #@110
    iget-boolean v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@112
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@115
    move-result-object v1

    #@116
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@119
    move-result-object v1

    #@11a
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@11d
    .line 1980
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@11f
    if-eqz v1, :cond_1b2

    #@121
    .line 1981
    const-string v1, "  mCurrentTextBoxAttribute:"

    #@123
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@126
    .line 1982
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@128
    const-string v2, "    "

    #@12a
    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/EditorInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@12d
    .line 1986
    :goto_12d
    new-instance v1, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v2, "  mServedInputConnection="

    #@134
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v1

    #@138
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@13a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v1

    #@13e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v1

    #@142
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@145
    .line 1987
    new-instance v1, Ljava/lang/StringBuilder;

    #@147
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14a
    const-string v2, "  mCompletions="

    #@14c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v1

    #@150
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@152
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v1

    #@156
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v1

    #@15a
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@15d
    .line 1988
    new-instance v1, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v2, "  mCursorRect="

    #@164
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v1

    #@168
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorRect:Landroid/graphics/Rect;

    #@16a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v1

    #@16e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@171
    move-result-object v1

    #@172
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@175
    .line 1989
    new-instance v1, Ljava/lang/StringBuilder;

    #@177
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17a
    const-string v2, "  mCursorSelStart="

    #@17c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v1

    #@180
    iget v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelStart:I

    #@182
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@185
    move-result-object v1

    #@186
    const-string v2, " mCursorSelEnd="

    #@188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v1

    #@18c
    iget v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelEnd:I

    #@18e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@191
    move-result-object v1

    #@192
    const-string v2, " mCursorCandStart="

    #@194
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v1

    #@198
    iget v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandStart:I

    #@19a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v1

    #@19e
    const-string v2, " mCursorCandEnd="

    #@1a0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v1

    #@1a4
    iget v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandEnd:I

    #@1a6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v1

    #@1aa
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ad
    move-result-object v1

    #@1ae
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1b1
    .line 1993
    return-void

    #@1b2
    .line 1984
    :cond_1b2
    const-string v1, "  mCurrentTextBoxAttribute: null"

    #@1b4
    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@1b7
    goto/16 :goto_12d
.end method

.method finishInputLocked()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 780
    iput-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurRootView:Landroid/view/View;

    #@3
    .line 781
    iput-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@5
    .line 782
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@7
    if-eqz v0, :cond_21

    #@9
    .line 785
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@b
    if-eqz v0, :cond_14

    #@d
    .line 787
    :try_start_d
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@f
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@11
    invoke-interface {v0, v1}, Lcom/android/internal/view/IInputMethodManager;->finishInput(Lcom/android/internal/view/IInputMethodClient;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_14} :catch_22

    #@14
    .line 792
    :cond_14
    :goto_14
    invoke-direct {p0}, Landroid/view/inputmethod/InputMethodManager;->notifyInputConnectionFinished()V

    #@17
    .line 794
    iput-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@19
    .line 795
    iput-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@1b
    .line 796
    const/4 v0, 0x0

    #@1c
    iput-boolean v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@1e
    .line 797
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->clearConnectionLocked()V

    #@21
    .line 799
    :cond_21
    return-void

    #@22
    .line 788
    :catch_22
    move-exception v0

    #@23
    goto :goto_14
.end method

.method finishedEvent(IZ)V
    .registers 8
    .parameter "seq"
    .parameter "handled"

    #@0
    .prologue
    .line 1701
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v3

    #@3
    .line 1702
    :try_start_3
    invoke-direct {p0, p1}, Landroid/view/inputmethod/InputMethodManager;->dequeuePendingEventLocked(I)Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@6
    move-result-object v1

    #@7
    .line 1703
    .local v1, p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    if-nez v1, :cond_b

    #@9
    .line 1704
    monitor-exit v3

    #@a
    .line 1711
    :goto_a
    return-void

    #@b
    .line 1706
    :cond_b
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@d
    const/4 v4, 0x5

    #@e
    invoke-virtual {v2, v4, v1}, Landroid/view/inputmethod/InputMethodManager$H;->removeMessages(ILjava/lang/Object;)V

    #@11
    .line 1707
    iget-object v0, v1, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mCallback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;

    #@13
    .line 1708
    .local v0, callback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;
    invoke-direct {p0, v1}, Landroid/view/inputmethod/InputMethodManager;->recyclePendingEventLocked(Landroid/view/inputmethod/InputMethodManager$PendingEvent;)V

    #@16
    .line 1709
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_1b

    #@17
    .line 1710
    invoke-interface {v0, p1, p2}, Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;->finishedEvent(IZ)V

    #@1a
    goto :goto_a

    #@1b
    .line 1709
    .end local v0           #callback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;
    .end local v1           #p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    :catchall_1b
    move-exception v2

    #@1c
    :try_start_1c
    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    #@1d
    throw v2
.end method

.method public focusIn(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 1251
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v1

    #@3
    .line 1252
    :try_start_3
    invoke-virtual {p0, p1}, Landroid/view/inputmethod/InputMethodManager;->focusInLocked(Landroid/view/View;)V

    #@6
    .line 1253
    monitor-exit v1

    #@7
    .line 1254
    return-void

    #@8
    .line 1253
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method focusInLocked(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 1259
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurRootView:Landroid/view/View;

    #@2
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    #@5
    move-result-object v1

    #@6
    if-eq v0, v1, :cond_9

    #@8
    .line 1268
    :goto_8
    return-void

    #@9
    .line 1266
    :cond_9
    iput-object p1, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@b
    .line 1267
    invoke-static {p1}, Landroid/view/inputmethod/InputMethodManager;->scheduleCheckFocusLocked(Landroid/view/View;)V

    #@e
    goto :goto_8
.end method

.method public focusOut(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 1275
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v1

    #@3
    .line 1279
    :try_start_3
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@5
    if-eq v0, p1, :cond_7

    #@7
    .line 1290
    :cond_7
    monitor-exit v1

    #@8
    .line 1291
    return-void

    #@9
    .line 1290
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public getClient()Lcom/android/internal/view/IInputMethodClient;
    .registers 2

    #@0
    .prologue
    .line 623
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@2
    return-object v0
.end method

.method public getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 6

    #@0
    .prologue
    .line 1826
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1828
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    invoke-interface {v1}, Lcom/android/internal/view/IInputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_8} :catch_b

    #@8
    move-result-object v1

    #@9
    :try_start_9
    monitor-exit v2

    #@a
    .line 1831
    :goto_a
    return-object v1

    #@b
    .line 1829
    :catch_b
    move-exception v0

    #@c
    .line 1830
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "IME died: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 1831
    const/4 v1, 0x0

    #@27
    monitor-exit v2

    #@28
    goto :goto_a

    #@29
    .line 1833
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_29

    #@2b
    throw v1
.end method

.method public getEnabledInputMethodList()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 641
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1}, Lcom/android/internal/view/IInputMethodManager;->getEnabledInputMethodList()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 642
    :catch_7
    move-exception v0

    #@8
    .line 643
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@d
    throw v1
.end method

.method public getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
    .registers 5
    .parameter "imi"
    .parameter "allowsImplicitlySelectedSubtypes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 657
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 658
    :catch_7
    move-exception v0

    #@8
    .line 659
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@d
    throw v1
.end method

.method public getInputContext()Lcom/android/internal/view/IInputContext;
    .registers 2

    #@0
    .prologue
    .line 628
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mIInputContext:Lcom/android/internal/view/IInputContext;

    #@2
    return-object v0
.end method

.method public getInputMethodList()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 633
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1}, Lcom/android/internal/view/IInputMethodManager;->getInputMethodList()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 634
    :catch_7
    move-exception v0

    #@8
    .line 635
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@d
    throw v1
.end method

.method public getLastInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 6

    #@0
    .prologue
    .line 1954
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1956
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    invoke-interface {v1}, Lcom/android/internal/view/IInputMethodManager;->getLastInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_8} :catch_b

    #@8
    move-result-object v1

    #@9
    :try_start_9
    monitor-exit v2

    #@a
    .line 1959
    :goto_a
    return-object v1

    #@b
    .line 1957
    :catch_b
    move-exception v0

    #@c
    .line 1958
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "IME died: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 1959
    const/4 v1, 0x0

    #@27
    monitor-exit v2

    #@28
    goto :goto_a

    #@29
    .line 1961
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_29

    #@2b
    throw v1
.end method

.method public getShortcutInputMethodsAndSubtypes()Ljava/util/Map;
    .registers 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 1857
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v8

    #@3
    .line 1858
    :try_start_3
    new-instance v5, Ljava/util/HashMap;

    #@5
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_66

    #@8
    .line 1862
    .local v5, ret:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    :try_start_8
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@a
    invoke-interface {v7}, Lcom/android/internal/view/IInputMethodManager;->getShortcutInputMethodsAndSubtypes()Ljava/util/List;

    #@d
    move-result-object v3

    #@e
    .line 1864
    .local v3, info:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v6, 0x0

    #@f
    .line 1865
    .local v6, subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@12
    move-result v0

    #@13
    .line 1866
    .local v0, N:I
    if-eqz v3, :cond_2f

    #@15
    if-lez v0, :cond_2f

    #@17
    .line 1867
    const/4 v2, 0x0

    #@18
    .local v2, i:I
    :goto_18
    if-ge v2, v0, :cond_2f

    #@1a
    .line 1868
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v4

    #@1e
    .line 1869
    .local v4, o:Ljava/lang/Object;
    instance-of v7, v4, Landroid/view/inputmethod/InputMethodInfo;

    #@20
    if-eqz v7, :cond_3e

    #@22
    .line 1870
    invoke-virtual {v5, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_31

    #@28
    .line 1871
    const-string v7, "InputMethodManager"

    #@2a
    const-string v9, "IMI list already contains the same InputMethod."

    #@2c
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_66
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_2f} :catch_4a

    #@2f
    .line 1884
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #info:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    .end local v4           #o:Ljava/lang/Object;
    .end local v6           #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_2f
    :goto_2f
    :try_start_2f
    monitor-exit v8
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_66

    #@30
    return-object v5

    #@31
    .line 1874
    .restart local v0       #N:I
    .restart local v2       #i:I
    .restart local v3       #info:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    .restart local v4       #o:Ljava/lang/Object;
    .restart local v6       #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_31
    :try_start_31
    new-instance v6, Ljava/util/ArrayList;

    #@33
    .end local v6           #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@36
    .line 1875
    .restart local v6       #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    check-cast v4, Landroid/view/inputmethod/InputMethodInfo;

    #@38
    .end local v4           #o:Ljava/lang/Object;
    invoke-virtual {v5, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 1867
    :cond_3b
    :goto_3b
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_18

    #@3e
    .line 1876
    .restart local v4       #o:Ljava/lang/Object;
    :cond_3e
    if-eqz v6, :cond_3b

    #@40
    instance-of v7, v4, Landroid/view/inputmethod/InputMethodSubtype;

    #@42
    if-eqz v7, :cond_3b

    #@44
    .line 1877
    check-cast v4, Landroid/view/inputmethod/InputMethodSubtype;

    #@46
    .end local v4           #o:Ljava/lang/Object;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_49
    .catchall {:try_start_31 .. :try_end_49} :catchall_66
    .catch Landroid/os/RemoteException; {:try_start_31 .. :try_end_49} :catch_4a

    #@49
    goto :goto_3b

    #@4a
    .line 1881
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #info:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Object;>;"
    .end local v6           #subtypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :catch_4a
    move-exception v1

    #@4b
    .line 1882
    .local v1, e:Landroid/os/RemoteException;
    :try_start_4b
    const-string v7, "InputMethodManager"

    #@4d
    new-instance v9, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v10, "IME died: "

    #@54
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v9

    #@58
    iget-object v10, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@5a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v9

    #@5e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v9

    #@62
    invoke-static {v7, v9, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@65
    goto :goto_2f

    #@66
    .line 1885
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v5           #ret:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/inputmethod/InputMethodInfo;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    :catchall_66
    move-exception v7

    #@67
    monitor-exit v8
    :try_end_68
    .catchall {:try_start_4b .. :try_end_68} :catchall_66

    #@68
    throw v7
.end method

.method public hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V
    .registers 5
    .parameter "token"
    .parameter "flags"

    #@0
    .prologue
    .line 1593
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->hideMySoftInput(Landroid/os/IBinder;I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1597
    return-void

    #@6
    .line 1594
    :catch_6
    move-exception v0

    #@7
    .line 1595
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    .registers 4
    .parameter "windowToken"
    .parameter "flags"

    #@0
    .prologue
    .line 993
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z
    .registers 8
    .parameter "windowToken"
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1014
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@4
    .line 1015
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@6
    monitor-enter v1

    #@7
    .line 1016
    :try_start_7
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@9
    if-eqz v2, :cond_13

    #@b
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@d
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    if-eq v2, p1, :cond_15

    #@13
    .line 1017
    :cond_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_1f

    #@14
    .line 1024
    :goto_14
    return v0

    #@15
    .line 1021
    :cond_15
    :try_start_15
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@17
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@19
    invoke-interface {v2, v3, p2, p3}, Lcom/android/internal/view/IInputMethodManager;->hideSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    :try_end_1c
    .catchall {:try_start_15 .. :try_end_1c} :catchall_1f
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1c} :catch_22

    #@1c
    move-result v0

    #@1d
    :try_start_1d
    monitor-exit v1

    #@1e
    goto :goto_14

    #@1f
    .line 1025
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_1d .. :try_end_21} :catchall_1f

    #@21
    throw v0

    #@22
    .line 1022
    :catch_22
    move-exception v2

    #@23
    .line 1024
    :try_start_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_1f

    #@24
    goto :goto_14
.end method

.method public hideStatusIcon(Landroid/os/IBinder;)V
    .registers 6
    .parameter "imeToken"

    #@0
    .prologue
    .line 673
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    invoke-interface {v1, p1, v2, v3}, Lcom/android/internal/view/IInputMethodManager;->updateStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 677
    return-void

    #@8
    .line 674
    :catch_8
    move-exception v0

    #@9
    .line 675
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@b
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@e
    throw v1
.end method

.method public isAcceptingText()Z
    .registers 2

    #@0
    .prologue
    .line 749
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 750
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isActive()Z
    .registers 3

    #@0
    .prologue
    .line 738
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 739
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v1

    #@6
    .line 740
    :try_start_6
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eqz v0, :cond_11

    #@a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@c
    if-eqz v0, :cond_11

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    monitor-exit v1

    #@10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_f

    #@13
    .line 741
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public isActive(Landroid/view/View;)Z
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 725
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 726
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v1

    #@6
    .line 727
    :try_start_6
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eq v0, p1, :cond_16

    #@a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    if-eqz v0, :cond_1d

    #@e
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10
    invoke-virtual {v0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1d

    #@16
    :cond_16
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@18
    if-eqz v0, :cond_1d

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    monitor-exit v1

    #@1c
    return v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1b

    #@1f
    .line 731
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_6 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method public isFullscreenMode()Z
    .registers 2

    #@0
    .prologue
    .line 717
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodManager;->mFullscreenMode:Z

    #@2
    return v0
.end method

.method public isWatchingCursor(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 1488
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public notifySuggestionPicked(Landroid/text/style/SuggestionSpan;Ljava/lang/String;I)V
    .registers 6
    .parameter "span"
    .parameter "originalString"
    .parameter "index"

    #@0
    .prologue
    .line 705
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/view/IInputMethodManager;->notifySuggestionPicked(Landroid/text/style/SuggestionSpan;Ljava/lang/String;I)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 709
    return-void

    #@6
    .line 706
    :catch_6
    move-exception v0

    #@7
    .line 707
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public onWindowFocus(Landroid/view/View;Landroid/view/View;IZI)V
    .registers 18
    .parameter "rootView"
    .parameter "focusedView"
    .parameter "softInputMode"
    .parameter "first"
    .parameter "windowFlags"

    #@0
    .prologue
    .line 1363
    const/4 v9, 0x0

    #@1
    .line 1364
    .local v9, forceNewFocus:Z
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@3
    monitor-enter v2

    #@4
    .line 1369
    :try_start_4
    iget-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z

    #@6
    if-eqz v1, :cond_c

    #@8
    .line 1371
    const/4 v1, 0x0

    #@9
    iput-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_54

    #@b
    .line 1372
    const/4 v9, 0x1

    #@c
    .line 1377
    :cond_c
    :try_start_c
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mIWindowManager:Landroid/view/IWindowManager;

    #@e
    if-eqz v1, :cond_50

    #@10
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mIWindowManager:Landroid/view/IWindowManager;

    #@12
    invoke-interface {v1}, Landroid/view/IWindowManager;->isKeyguardLocked()Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_50

    #@18
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mIWindowManager:Landroid/view/IWindowManager;

    #@1a
    invoke-interface {v1}, Landroid/view/IWindowManager;->isKeyguardSecure()Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_50

    #@20
    const/4 v10, 0x1

    #@21
    .line 1380
    .local v10, isScreenLocked:Z
    :goto_21
    if-eqz v10, :cond_24

    #@23
    .line 1381
    const/4 v9, 0x1

    #@24
    .line 1382
    :cond_24
    if-eqz p2, :cond_52

    #@26
    move-object v1, p2

    #@27
    :goto_27
    invoke-virtual {p0, v1}, Landroid/view/inputmethod/InputMethodManager;->focusInLocked(Landroid/view/View;)V
    :try_end_2a
    .catchall {:try_start_c .. :try_end_2a} :catchall_54
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_2a} :catch_71

    #@2a
    .line 1386
    .end local v10           #isScreenLocked:Z
    :goto_2a
    :try_start_2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_54

    #@2b
    .line 1388
    const/4 v4, 0x0

    #@2c
    .line 1389
    .local v4, controlFlags:I
    if-eqz p2, :cond_38

    #@2e
    .line 1390
    or-int/lit8 v4, v4, 0x1

    #@30
    .line 1391
    invoke-virtual {p2}, Landroid/view/View;->onCheckIsTextEditor()Z

    #@33
    move-result v1

    #@34
    if-eqz v1, :cond_38

    #@36
    .line 1392
    or-int/lit8 v4, v4, 0x2

    #@38
    .line 1395
    :cond_38
    if-eqz p4, :cond_3c

    #@3a
    .line 1396
    or-int/lit8 v4, v4, 0x4

    #@3c
    .line 1399
    :cond_3c
    const/4 v1, 0x1

    #@3d
    invoke-direct {p0, v9, v1}, Landroid/view/inputmethod/InputMethodManager;->checkFocusNoStartInput(ZZ)Z

    #@40
    move-result v1

    #@41
    if-eqz v1, :cond_57

    #@43
    .line 1404
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@46
    move-result-object v1

    #@47
    move/from16 v0, p5

    #@49
    invoke-virtual {p0, v1, v4, p3, v0}, Landroid/view/inputmethod/InputMethodManager;->startInputInner(Landroid/os/IBinder;III)Z

    #@4c
    move-result v1

    #@4d
    if-eqz v1, :cond_57

    #@4f
    .line 1420
    :goto_4f
    return-void

    #@50
    .line 1377
    .end local v4           #controlFlags:I
    :cond_50
    const/4 v10, 0x0

    #@51
    goto :goto_21

    #@52
    .restart local v10       #isScreenLocked:Z
    :cond_52
    move-object v1, p1

    #@53
    .line 1382
    goto :goto_27

    #@54
    .line 1386
    .end local v10           #isScreenLocked:Z
    :catchall_54
    move-exception v1

    #@55
    :try_start_55
    monitor-exit v2
    :try_end_56
    .catchall {:try_start_55 .. :try_end_56} :catchall_54

    #@56
    throw v1

    #@57
    .line 1412
    .restart local v4       #controlFlags:I
    :cond_57
    iget-object v11, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@59
    monitor-enter v11

    #@5a
    .line 1415
    :try_start_5a
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5c
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@5e
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@61
    move-result-object v3

    #@62
    const/4 v7, 0x0

    #@63
    const/4 v8, 0x0

    #@64
    move v5, p3

    #@65
    move/from16 v6, p5

    #@67
    invoke-interface/range {v1 .. v8}, Lcom/android/internal/view/IInputMethodManager;->windowGainedFocus(Lcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;)Lcom/android/internal/view/InputBindResult;
    :try_end_6a
    .catchall {:try_start_5a .. :try_end_6a} :catchall_6c
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_6a} :catch_6f

    #@6a
    .line 1419
    :goto_6a
    :try_start_6a
    monitor-exit v11

    #@6b
    goto :goto_4f

    #@6c
    :catchall_6c
    move-exception v1

    #@6d
    monitor-exit v11
    :try_end_6e
    .catchall {:try_start_6a .. :try_end_6e} :catchall_6c

    #@6e
    throw v1

    #@6f
    .line 1417
    :catch_6f
    move-exception v1

    #@70
    goto :goto_6a

    #@71
    .line 1383
    .end local v4           #controlFlags:I
    :catch_71
    move-exception v1

    #@72
    goto :goto_2a
.end method

.method public registerSuggestionSpansForNotification([Landroid/text/style/SuggestionSpan;)V
    .registers 4
    .parameter "spans"

    #@0
    .prologue
    .line 696
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1}, Lcom/android/internal/view/IInputMethodManager;->registerSuggestionSpansForNotification([Landroid/text/style/SuggestionSpan;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 700
    return-void

    #@6
    .line 697
    :catch_6
    move-exception v0

    #@7
    .line 698
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public reportFinishInputConnection(Landroid/view/inputmethod/InputConnection;)V
    .registers 3
    .parameter "ic"

    #@0
    .prologue
    .line 823
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@2
    if-eq v0, p1, :cond_10

    #@4
    .line 824
    invoke-interface {p1}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@7
    .line 826
    instance-of v0, p1, Landroid/view/inputmethod/BaseInputConnection;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 827
    check-cast p1, Landroid/view/inputmethod/BaseInputConnection;

    #@d
    .end local p1
    invoke-virtual {p1}, Landroid/view/inputmethod/BaseInputConnection;->reportFinish()V

    #@10
    .line 830
    :cond_10
    return-void
.end method

.method public restartInput(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1086
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@4
    .line 1087
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@6
    monitor-enter v1

    #@7
    .line 1088
    :try_start_7
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@9
    if-eq v0, p1, :cond_19

    #@b
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@d
    if-eqz v0, :cond_17

    #@f
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@11
    invoke-virtual {v0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_19

    #@17
    .line 1090
    :cond_17
    monitor-exit v1

    #@18
    .line 1097
    :goto_18
    return-void

    #@19
    .line 1093
    :cond_19
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@1c
    .line 1094
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_7 .. :try_end_1d} :catchall_22

    #@1d
    .line 1096
    const/4 v0, 0x0

    #@1e
    invoke-virtual {p0, v0, v2, v2, v2}, Landroid/view/inputmethod/InputMethodManager;->startInputInner(Landroid/os/IBinder;III)Z

    #@21
    goto :goto_18

    #@22
    .line 1094
    :catchall_22
    move-exception v0

    #@23
    :try_start_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    throw v0
.end method

.method public sendAppPrivateCommand(Landroid/view/View;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 9
    .parameter "view"
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 1530
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 1531
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v2

    #@6
    .line 1532
    :try_start_6
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eq v1, p1, :cond_16

    #@a
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    if-eqz v1, :cond_1e

    #@e
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10
    invoke-virtual {v1, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_1e

    #@16
    :cond_16
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@18
    if-eqz v1, :cond_1e

    #@1a
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@1c
    if-nez v1, :cond_20

    #@1e
    .line 1535
    :cond_1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_6 .. :try_end_1f} :catchall_27

    #@1f
    .line 1544
    :goto_1f
    return-void

    #@20
    .line 1539
    :cond_20
    :try_start_20
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@22
    invoke-interface {v1, p2, p3}, Lcom/android/internal/view/IInputMethodSession;->appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_25
    .catchall {:try_start_20 .. :try_end_25} :catchall_27
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_25} :catch_2a

    #@25
    .line 1543
    :goto_25
    :try_start_25
    monitor-exit v2

    #@26
    goto :goto_1f

    #@27
    :catchall_27
    move-exception v1

    #@28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_25 .. :try_end_29} :catchall_27

    #@29
    throw v1

    #@2a
    .line 1540
    :catch_2a
    move-exception v0

    #@2b
    .line 1541
    .local v0, e:Landroid/os/RemoteException;
    :try_start_2b
    const-string v1, "InputMethodManager"

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "IME died: "

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_45
    .catchall {:try_start_2b .. :try_end_45} :catchall_27

    #@45
    goto :goto_25
.end method

.method public setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 8
    .parameter "imiId"
    .parameter "subtypes"

    #@0
    .prologue
    .line 1944
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1946
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    invoke-interface {v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_26
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_8} :catch_a

    #@8
    .line 1950
    :goto_8
    :try_start_8
    monitor-exit v2

    #@9
    .line 1951
    return-void

    #@a
    .line 1947
    :catch_a
    move-exception v0

    #@b
    .line 1948
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "IME died: "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    goto :goto_8

    #@26
    .line 1950
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_26
    move-exception v1

    #@27
    monitor-exit v2
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_26

    #@28
    throw v1
.end method

.method public setCurrentInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)Z
    .registers 7
    .parameter "subtype"

    #@0
    .prologue
    .line 1843
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1845
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    invoke-interface {v1, p1}, Lcom/android/internal/view/IInputMethodManager;->setCurrentInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_8} :catch_b

    #@8
    move-result v1

    #@9
    :try_start_9
    monitor-exit v2

    #@a
    .line 1848
    :goto_a
    return v1

    #@b
    .line 1846
    :catch_b
    move-exception v0

    #@c
    .line 1847
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "IME died: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 1848
    const/4 v1, 0x0

    #@27
    monitor-exit v2

    #@28
    goto :goto_a

    #@29
    .line 1850
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_29

    #@2b
    throw v1
.end method

.method public setFullscreenMode(Z)V
    .registers 2
    .parameter "fullScreen"

    #@0
    .prologue
    .line 690
    iput-boolean p1, p0, Landroid/view/inputmethod/InputMethodManager;->mFullscreenMode:Z

    #@2
    .line 691
    return-void
.end method

.method public setImeWindowStatus(Landroid/os/IBinder;II)V
    .registers 6
    .parameter "imeToken"
    .parameter "vis"
    .parameter "backDisposition"

    #@0
    .prologue
    .line 682
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/view/IInputMethodManager;->setImeWindowStatus(Landroid/os/IBinder;II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 686
    return-void

    #@6
    .line 683
    :catch_6
    move-exception v0

    #@7
    .line 684
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 5
    .parameter "token"
    .parameter "id"

    #@0
    .prologue
    .line 1556
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1560
    return-void

    #@6
    .line 1557
    :catch_6
    move-exception v0

    #@7
    .line 1558
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public setInputMethodAndSubtype(Landroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 6
    .parameter "token"
    .parameter "id"
    .parameter "subtype"

    #@0
    .prologue
    .line 1573
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/view/IInputMethodManager;->setInputMethodAndSubtype(Landroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1577
    return-void

    #@6
    .line 1574
    :catch_6
    move-exception v0

    #@7
    .line 1575
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public showInputMethodAndSubtypeEnabler(Ljava/lang/String;)V
    .registers 7
    .parameter "imiId"

    #@0
    .prologue
    .line 1811
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1813
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@7
    invoke-interface {v1, v3, p1}, Lcom/android/internal/view/IInputMethodManager;->showInputMethodAndSubtypeEnablerFromClient(Lcom/android/internal/view/IInputMethodClient;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_28
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_a} :catch_c

    #@a
    .line 1817
    :goto_a
    :try_start_a
    monitor-exit v2

    #@b
    .line 1818
    return-void

    #@c
    .line 1814
    :catch_c
    move-exception v0

    #@d
    .line 1815
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "IME died: "

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_a

    #@28
    .line 1817
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_28
    move-exception v1

    #@29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_a .. :try_end_2a} :catchall_28

    #@2a
    throw v1
.end method

.method public showInputMethodPicker()V
    .registers 3

    #@0
    .prologue
    .line 1792
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v1

    #@3
    .line 1793
    :try_start_3
    invoke-direct {p0}, Landroid/view/inputmethod/InputMethodManager;->showInputMethodPickerLocked()V

    #@6
    .line 1794
    monitor-exit v1

    #@7
    .line 1795
    return-void

    #@8
    .line 1794
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public showSoftInput(Landroid/view/View;I)Z
    .registers 4
    .parameter "view"
    .parameter "flags"

    #@0
    .prologue
    .line 892
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z
    .registers 8
    .parameter "view"
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 944
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@4
    .line 945
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@6
    monitor-enter v1

    #@7
    .line 946
    :try_start_7
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@9
    if-eq v2, p1, :cond_19

    #@b
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@d
    if-eqz v2, :cond_17

    #@f
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@11
    invoke-virtual {v2, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_19

    #@17
    .line 948
    :cond_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_23

    #@18
    .line 956
    :goto_18
    return v0

    #@19
    .line 952
    :cond_19
    :try_start_19
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@1b
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@1d
    invoke-interface {v2, v3, p2, p3}, Lcom/android/internal/view/IInputMethodManager;->showSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    :try_end_20
    .catchall {:try_start_19 .. :try_end_20} :catchall_23
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_20} :catch_26

    #@20
    move-result v0

    #@21
    :try_start_21
    monitor-exit v1

    #@22
    goto :goto_18

    #@23
    .line 957
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_21 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 953
    :catch_26
    move-exception v2

    #@27
    .line 956
    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_23

    #@28
    goto :goto_18
.end method

.method public showSoftInputFromInputMethod(Landroid/os/IBinder;I)V
    .registers 5
    .parameter "token"
    .parameter "flags"

    #@0
    .prologue
    .line 1614
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->showMySoftInput(Landroid/os/IBinder;I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1618
    return-void

    #@6
    .line 1615
    :catch_6
    move-exception v0

    #@7
    .line 1616
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public showSoftInputUnchecked(ILandroid/os/ResultReceiver;)V
    .registers 5
    .parameter "flags"
    .parameter "resultReceiver"

    #@0
    .prologue
    .line 963
    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@4
    invoke-interface {v0, v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->showSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 966
    :goto_7
    return-void

    #@8
    .line 964
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public showStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V
    .registers 6
    .parameter "imeToken"
    .parameter "packageName"
    .parameter "iconId"

    #@0
    .prologue
    .line 665
    :try_start_0
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@2
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/view/IInputMethodManager;->updateStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 669
    return-void

    #@6
    .line 666
    :catch_6
    move-exception v0

    #@7
    .line 667
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@c
    throw v1
.end method

.method public startGettingWindowFocus(Landroid/view/View;)V
    .registers 4
    .parameter "rootView"

    #@0
    .prologue
    .line 1424
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v1

    #@3
    .line 1425
    :try_start_3
    iput-object p1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurRootView:Landroid/view/View;

    #@5
    .line 1426
    monitor-exit v1

    #@6
    .line 1427
    return-void

    #@7
    .line 1426
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method startInputInner(Landroid/os/IBinder;III)Z
    .registers 20
    .parameter "windowGainingFocus"
    .parameter "controlFlags"
    .parameter "softInputMode"
    .parameter "windowFlags"

    #@0
    .prologue
    .line 1102
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1103
    :try_start_3
    iget-object v13, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@5
    .line 1107
    .local v13, view:Landroid/view/View;
    if-nez v13, :cond_a

    #@7
    .line 1109
    const/4 v1, 0x0

    #@8
    monitor-exit v2

    #@9
    .line 1228
    :goto_9
    return v1

    #@a
    .line 1111
    :cond_a
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_16

    #@b
    .line 1117
    invoke-virtual {v13}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    #@e
    move-result-object v12

    #@f
    .line 1118
    .local v12, vh:Landroid/os/Handler;
    if-nez v12, :cond_19

    #@11
    .line 1124
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->closeCurrentInput()V

    #@14
    .line 1125
    const/4 v1, 0x0

    #@15
    goto :goto_9

    #@16
    .line 1111
    .end local v12           #vh:Landroid/os/Handler;
    .end local v13           #view:Landroid/view/View;
    :catchall_16
    move-exception v1

    #@17
    :try_start_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v1

    #@19
    .line 1127
    .restart local v12       #vh:Landroid/os/Handler;
    .restart local v13       #view:Landroid/view/View;
    :cond_19
    invoke-virtual {v12}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@20
    move-result-object v2

    #@21
    if-eq v1, v2, :cond_2d

    #@23
    .line 1131
    new-instance v1, Landroid/view/inputmethod/InputMethodManager$3;

    #@25
    invoke-direct {v1, p0}, Landroid/view/inputmethod/InputMethodManager$3;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    #@28
    invoke-virtual {v12, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@2b
    .line 1136
    const/4 v1, 0x0

    #@2c
    goto :goto_9

    #@2d
    .line 1142
    :cond_2d
    new-instance v7, Landroid/view/inputmethod/EditorInfo;

    #@2f
    invoke-direct {v7}, Landroid/view/inputmethod/EditorInfo;-><init>()V

    #@32
    .line 1143
    .local v7, tba:Landroid/view/inputmethod/EditorInfo;
    invoke-virtual {v13}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    iput-object v1, v7, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    #@3c
    .line 1144
    invoke-virtual {v13}, Landroid/view/View;->getId()I

    #@3f
    move-result v1

    #@40
    iput v1, v7, Landroid/view/inputmethod/EditorInfo;->fieldId:I

    #@42
    .line 1145
    invoke-virtual {v13, v7}, Landroid/view/View;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    #@45
    move-result-object v10

    #@46
    .line 1148
    .local v10, ic:Landroid/view/inputmethod/InputConnection;
    iget-object v14, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@48
    monitor-enter v14

    #@49
    .line 1151
    :try_start_49
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@4b
    if-ne v1, v13, :cond_51

    #@4d
    iget-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@4f
    if-nez v1, :cond_57

    #@51
    .line 1156
    :cond_51
    const/4 v1, 0x0

    #@52
    monitor-exit v14

    #@53
    goto :goto_9

    #@54
    .line 1226
    :catchall_54
    move-exception v1

    #@55
    monitor-exit v14
    :try_end_56
    .catchall {:try_start_49 .. :try_end_56} :catchall_54

    #@56
    throw v1

    #@57
    .line 1161
    :cond_57
    :try_start_57
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@59
    if-nez v1, :cond_61

    #@5b
    .line 1162
    move/from16 v0, p2

    #@5d
    or-int/lit16 v0, v0, 0x100

    #@5f
    move/from16 p2, v0

    #@61
    .line 1166
    :cond_61
    iput-object v7, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@63
    .line 1167
    const/4 v1, 0x0

    #@64
    iput-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@66
    .line 1169
    invoke-direct {p0}, Landroid/view/inputmethod/InputMethodManager;->notifyInputConnectionFinished()V

    #@69
    .line 1170
    iput-object v10, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnection:Landroid/view/inputmethod/InputConnection;

    #@6b
    .line 1172
    if-eqz v10, :cond_d0

    #@6d
    .line 1173
    iget v1, v7, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    #@6f
    iput v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelStart:I

    #@71
    .line 1174
    iget v1, v7, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    #@73
    iput v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelEnd:I

    #@75
    .line 1175
    const/4 v1, -0x1

    #@76
    iput v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandStart:I

    #@78
    .line 1176
    const/4 v1, -0x1

    #@79
    iput v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandEnd:I

    #@7b
    .line 1177
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorRect:Landroid/graphics/Rect;

    #@7d
    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    #@80
    .line 1178
    new-instance v8, Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@82
    invoke-virtual {v12}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@85
    move-result-object v1

    #@86
    invoke-direct {v8, v1, v10, p0}, Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;-><init>(Landroid/os/Looper;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/InputMethodManager;)V

    #@89
    .line 1182
    .local v8, servedContext:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    :goto_89
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@8b
    if-eqz v1, :cond_92

    #@8d
    .line 1183
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;

    #@8f
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;->deactivate()V

    #@92
    .line 1185
    :cond_92
    iput-object v8, p0, Landroid/view/inputmethod/InputMethodManager;->mServedInputConnectionWrapper:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    :try_end_94
    .catchall {:try_start_57 .. :try_end_94} :catchall_54

    #@94
    .line 1192
    if-eqz p1, :cond_d2

    #@96
    .line 1193
    :try_start_96
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@98
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@9a
    move-object/from16 v3, p1

    #@9c
    move/from16 v4, p2

    #@9e
    move/from16 v5, p3

    #@a0
    move/from16 v6, p4

    #@a2
    invoke-interface/range {v1 .. v8}, Lcom/android/internal/view/IInputMethodManager;->windowGainedFocus(Lcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;)Lcom/android/internal/view/InputBindResult;

    #@a5
    move-result-object v11

    #@a6
    .line 1197
    .local v11, res:Lcom/android/internal/view/InputBindResult;
    if-nez v11, :cond_ab

    #@a8
    .line 1198
    const/4 v1, 0x1

    #@a9
    iput-boolean v1, p0, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z

    #@ab
    .line 1206
    :cond_ab
    :goto_ab
    if-eqz v11, :cond_bd

    #@ad
    .line 1207
    iget-object v1, v11, Lcom/android/internal/view/InputBindResult;->id:Ljava/lang/String;

    #@af
    if-eqz v1, :cond_dd

    #@b1
    .line 1208
    iget v1, v11, Lcom/android/internal/view/InputBindResult;->sequence:I

    #@b3
    iput v1, p0, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@b5
    .line 1209
    iget-object v1, v11, Lcom/android/internal/view/InputBindResult;->method:Lcom/android/internal/view/IInputMethodSession;

    #@b7
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@b9
    .line 1210
    iget-object v1, v11, Lcom/android/internal/view/InputBindResult;->id:Ljava/lang/String;

    #@bb
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@bd
    .line 1217
    :cond_bd
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@bf
    if-eqz v1, :cond_cc

    #@c1
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;
    :try_end_c3
    .catchall {:try_start_96 .. :try_end_c3} :catchall_54
    .catch Landroid/os/RemoteException; {:try_start_96 .. :try_end_c3} :catch_e5

    #@c3
    if-eqz v1, :cond_cc

    #@c5
    .line 1219
    :try_start_c5
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@c7
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCompletions:[Landroid/view/inputmethod/CompletionInfo;

    #@c9
    invoke-interface {v1, v2}, Lcom/android/internal/view/IInputMethodSession;->displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    :try_end_cc
    .catchall {:try_start_c5 .. :try_end_cc} :catchall_54
    .catch Landroid/os/RemoteException; {:try_start_c5 .. :try_end_cc} :catch_101

    #@cc
    .line 1226
    .end local v11           #res:Lcom/android/internal/view/InputBindResult;
    :cond_cc
    :goto_cc
    :try_start_cc
    monitor-exit v14
    :try_end_cd
    .catchall {:try_start_cc .. :try_end_cd} :catchall_54

    #@cd
    .line 1228
    const/4 v1, 0x1

    #@ce
    goto/16 :goto_9

    #@d0
    .line 1180
    .end local v8           #servedContext:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    :cond_d0
    const/4 v8, 0x0

    #@d1
    .restart local v8       #servedContext:Landroid/view/inputmethod/InputMethodManager$ControlledInputConnectionWrapper;
    goto :goto_89

    #@d2
    .line 1202
    :cond_d2
    :try_start_d2
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@d4
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mClient:Lcom/android/internal/view/IInputMethodClient$Stub;

    #@d6
    move/from16 v0, p2

    #@d8
    invoke-interface {v1, v2, v8, v7, v0}, Lcom/android/internal/view/IInputMethodManager;->startInput(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@db
    move-result-object v11

    #@dc
    .restart local v11       #res:Lcom/android/internal/view/InputBindResult;
    goto :goto_ab

    #@dd
    .line 1211
    :cond_dd
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;
    :try_end_df
    .catchall {:try_start_d2 .. :try_end_df} :catchall_54
    .catch Landroid/os/RemoteException; {:try_start_d2 .. :try_end_df} :catch_e5

    #@df
    if-nez v1, :cond_bd

    #@e1
    .line 1214
    const/4 v1, 0x1

    #@e2
    :try_start_e2
    monitor-exit v14

    #@e3
    goto/16 :goto_9

    #@e5
    .line 1223
    .end local v11           #res:Lcom/android/internal/view/InputBindResult;
    :catch_e5
    move-exception v9

    #@e6
    .line 1224
    .local v9, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@e8
    new-instance v2, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v3, "IME died: "

    #@ef
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v2

    #@f3
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@f5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v2

    #@fd
    invoke-static {v1, v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_100
    .catchall {:try_start_e2 .. :try_end_100} :catchall_54

    #@100
    goto :goto_cc

    #@101
    .line 1220
    .end local v9           #e:Landroid/os/RemoteException;
    .restart local v11       #res:Lcom/android/internal/view/InputBindResult;
    :catch_101
    move-exception v1

    #@102
    goto :goto_cc
.end method

.method public switchToLastInputMethod(Landroid/os/IBinder;)Z
    .registers 7
    .parameter "imeToken"

    #@0
    .prologue
    .line 1898
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1900
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    invoke-interface {v1, p1}, Lcom/android/internal/view/IInputMethodManager;->switchToLastInputMethod(Landroid/os/IBinder;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_8} :catch_b

    #@8
    move-result v1

    #@9
    :try_start_9
    monitor-exit v2

    #@a
    .line 1903
    :goto_a
    return v1

    #@b
    .line 1901
    :catch_b
    move-exception v0

    #@c
    .line 1902
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "IME died: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 1903
    const/4 v1, 0x0

    #@27
    monitor-exit v2

    #@28
    goto :goto_a

    #@29
    .line 1905
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_29

    #@2b
    throw v1
.end method

.method public switchToNextInputMethod(Landroid/os/IBinder;Z)Z
    .registers 8
    .parameter "imeToken"
    .parameter "onlyCurrentIme"

    #@0
    .prologue
    .line 1919
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v2

    #@3
    .line 1921
    :try_start_3
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mService:Lcom/android/internal/view/IInputMethodManager;

    #@5
    invoke-interface {v1, p1, p2}, Lcom/android/internal/view/IInputMethodManager;->switchToNextInputMethod(Landroid/os/IBinder;Z)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_8} :catch_b

    #@8
    move-result v1

    #@9
    :try_start_9
    monitor-exit v2

    #@a
    .line 1924
    :goto_a
    return v1

    #@b
    .line 1922
    :catch_b
    move-exception v0

    #@c
    .line 1923
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "InputMethodManager"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "IME died: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 1924
    const/4 v1, 0x0

    #@27
    monitor-exit v2

    #@28
    goto :goto_a

    #@29
    .line 1926
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_9 .. :try_end_2b} :catchall_29

    #@2b
    throw v1
.end method

.method timeoutEvent(I)V
    .registers 12
    .parameter "seq"

    #@0
    .prologue
    .line 1715
    iget-object v5, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v5

    #@3
    .line 1716
    :try_start_3
    invoke-direct {p0, p1}, Landroid/view/inputmethod/InputMethodManager;->dequeuePendingEventLocked(I)Landroid/view/inputmethod/InputMethodManager$PendingEvent;

    #@6
    move-result-object v3

    #@7
    .line 1717
    .local v3, p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    if-nez v3, :cond_b

    #@9
    .line 1718
    monitor-exit v5

    #@a
    .line 1727
    :goto_a
    return-void

    #@b
    .line 1720
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v6

    #@f
    iget-wide v8, v3, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mStartTime:J

    #@11
    sub-long v1, v6, v8

    #@13
    .line 1721
    .local v1, delay:J
    const-string v4, "InputMethodManager"

    #@15
    new-instance v6, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v7, "Timeout waiting for IME to handle input event after "

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string/jumbo v7, "ms: "

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    iget-object v7, v3, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mInputMethodId:Ljava/lang/String;

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1723
    iget-object v0, v3, Landroid/view/inputmethod/InputMethodManager$PendingEvent;->mCallback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;

    #@3a
    .line 1724
    .local v0, callback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;
    invoke-direct {p0, v3}, Landroid/view/inputmethod/InputMethodManager;->recyclePendingEventLocked(Landroid/view/inputmethod/InputMethodManager$PendingEvent;)V

    #@3d
    .line 1725
    monitor-exit v5
    :try_end_3e
    .catchall {:try_start_3 .. :try_end_3e} :catchall_43

    #@3e
    .line 1726
    const/4 v4, 0x0

    #@3f
    invoke-interface {v0, p1, v4}, Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;->finishedEvent(IZ)V

    #@42
    goto :goto_a

    #@43
    .line 1725
    .end local v0           #callback:Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;
    .end local v1           #delay:J
    .end local v3           #p:Landroid/view/inputmethod/InputMethodManager$PendingEvent;
    :catchall_43
    move-exception v4

    #@44
    :try_start_44
    monitor-exit v5
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_43

    #@45
    throw v4
.end method

.method public toggleSoftInput(II)V
    .registers 4
    .parameter "showFlags"
    .parameter "hideFlags"

    #@0
    .prologue
    .line 1069
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1071
    :try_start_4
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@6
    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/IInputMethodSession;->toggleSoftInput(II)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1075
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1072
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public toggleSoftInputFromWindow(Landroid/os/IBinder;II)V
    .registers 6
    .parameter "windowToken"
    .parameter "showFlags"
    .parameter "hideFlags"

    #@0
    .prologue
    .line 1043
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@2
    monitor-enter v1

    #@3
    .line 1044
    :try_start_3
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@5
    if-eqz v0, :cond_f

    #@7
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@9
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    if-eq v0, p1, :cond_11

    #@f
    .line 1045
    :cond_f
    monitor-exit v1

    #@10
    .line 1054
    :goto_10
    return-void

    #@11
    .line 1047
    :cond_11
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_1c

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 1049
    :try_start_15
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@17
    invoke-interface {v0, p2, p3}, Lcom/android/internal/view/IInputMethodSession;->toggleSoftInput(II)V
    :try_end_1a
    .catchall {:try_start_15 .. :try_end_1a} :catchall_1c
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_1f

    #@1a
    .line 1053
    :cond_1a
    :goto_1a
    :try_start_1a
    monitor-exit v1

    #@1b
    goto :goto_10

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1a .. :try_end_1e} :catchall_1c

    #@1e
    throw v0

    #@1f
    .line 1050
    :catch_1f
    move-exception v0

    #@20
    goto :goto_1a
.end method

.method public updateCursor(Landroid/view/View;IIII)V
    .registers 11
    .parameter "view"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1495
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 1496
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v2

    #@6
    .line 1497
    :try_start_6
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eq v1, p1, :cond_16

    #@a
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    if-eqz v1, :cond_1e

    #@e
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10
    invoke-virtual {v1, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_1e

    #@16
    :cond_16
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@18
    if-eqz v1, :cond_1e

    #@1a
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@1c
    if-nez v1, :cond_20

    #@1e
    .line 1500
    :cond_1e
    monitor-exit v2

    #@1f
    .line 1516
    :goto_1f
    return-void

    #@20
    .line 1503
    :cond_20
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mTmpCursorRect:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/graphics/Rect;->set(IIII)V

    #@25
    .line 1504
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorRect:Landroid/graphics/Rect;

    #@27
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mTmpCursorRect:Landroid/graphics/Rect;

    #@29
    invoke-virtual {v1, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
    :try_end_2c
    .catchall {:try_start_6 .. :try_end_2c} :catchall_3f

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_3d

    #@2f
    .line 1509
    :try_start_2f
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@31
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mTmpCursorRect:Landroid/graphics/Rect;

    #@33
    invoke-interface {v1, v3}, Lcom/android/internal/view/IInputMethodSession;->updateCursor(Landroid/graphics/Rect;)V

    #@36
    .line 1510
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorRect:Landroid/graphics/Rect;

    #@38
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mTmpCursorRect:Landroid/graphics/Rect;

    #@3a
    invoke-virtual {v1, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_3d
    .catchall {:try_start_2f .. :try_end_3d} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_3d} :catch_42

    #@3d
    .line 1515
    :cond_3d
    :goto_3d
    :try_start_3d
    monitor-exit v2

    #@3e
    goto :goto_1f

    #@3f
    :catchall_3f
    move-exception v1

    #@40
    monitor-exit v2
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_3f

    #@41
    throw v1

    #@42
    .line 1511
    :catch_42
    move-exception v0

    #@43
    .line 1512
    .local v0, e:Landroid/os/RemoteException;
    :try_start_43
    const-string v1, "InputMethodManager"

    #@45
    new-instance v3, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v4, "IME died: "

    #@4c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    iget-object v4, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@52
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5d
    .catchall {:try_start_43 .. :try_end_5d} :catchall_3f

    #@5d
    goto :goto_3d
.end method

.method public updateExtractedText(Landroid/view/View;ILandroid/view/inputmethod/ExtractedText;)V
    .registers 6
    .parameter "view"
    .parameter "token"
    .parameter "text"

    #@0
    .prologue
    .line 851
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 852
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v1

    #@6
    .line 853
    :try_start_6
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eq v0, p1, :cond_18

    #@a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    if-eqz v0, :cond_16

    #@e
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10
    invoke-virtual {v0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_18

    #@16
    .line 855
    :cond_16
    monitor-exit v1

    #@17
    .line 865
    :goto_17
    return-void

    #@18
    .line 858
    :cond_18
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;
    :try_end_1a
    .catchall {:try_start_6 .. :try_end_1a} :catchall_23

    #@1a
    if-eqz v0, :cond_21

    #@1c
    .line 860
    :try_start_1c
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@1e
    invoke-interface {v0, p2, p3}, Lcom/android/internal/view/IInputMethodSession;->updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_23
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_21} :catch_26

    #@21
    .line 864
    :cond_21
    :goto_21
    :try_start_21
    monitor-exit v1

    #@22
    goto :goto_17

    #@23
    :catchall_23
    move-exception v0

    #@24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_21 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 861
    :catch_26
    move-exception v0

    #@27
    goto :goto_21
.end method

.method public updateSelection(Landroid/view/View;IIII)V
    .registers 15
    .parameter "view"
    .parameter "selStart"
    .parameter "selEnd"
    .parameter "candidatesStart"
    .parameter "candidatesEnd"

    #@0
    .prologue
    .line 1434
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 1435
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v8

    #@6
    .line 1436
    :try_start_6
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eq v0, p1, :cond_16

    #@a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    if-eqz v0, :cond_1e

    #@e
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10
    invoke-virtual {v0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1e

    #@16
    :cond_16
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@18
    if-eqz v0, :cond_1e

    #@1a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@1c
    if-nez v0, :cond_20

    #@1e
    .line 1439
    :cond_1e
    monitor-exit v8

    #@1f
    .line 1460
    :goto_1f
    return-void

    #@20
    .line 1442
    :cond_20
    iget v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelStart:I

    #@22
    if-ne v0, p2, :cond_30

    #@24
    iget v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelEnd:I

    #@26
    if-ne v0, p3, :cond_30

    #@28
    iget v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandStart:I

    #@2a
    if-ne v0, p4, :cond_30

    #@2c
    iget v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandEnd:I
    :try_end_2e
    .catchall {:try_start_6 .. :try_end_2e} :catchall_47

    #@2e
    if-eq v0, p5, :cond_45

    #@30
    .line 1449
    :cond_30
    :try_start_30
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@32
    iget v1, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelStart:I

    #@34
    iget v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelEnd:I

    #@36
    move v3, p2

    #@37
    move v4, p3

    #@38
    move v5, p4

    #@39
    move v6, p5

    #@3a
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/view/IInputMethodSession;->updateSelection(IIIIII)V

    #@3d
    .line 1451
    iput p2, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelStart:I

    #@3f
    .line 1452
    iput p3, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorSelEnd:I

    #@41
    .line 1453
    iput p4, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandStart:I

    #@43
    .line 1454
    iput p5, p0, Landroid/view/inputmethod/InputMethodManager;->mCursorCandEnd:I
    :try_end_45
    .catchall {:try_start_30 .. :try_end_45} :catchall_47
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_45} :catch_4a

    #@45
    .line 1459
    :cond_45
    :goto_45
    :try_start_45
    monitor-exit v8

    #@46
    goto :goto_1f

    #@47
    :catchall_47
    move-exception v0

    #@48
    monitor-exit v8
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_47

    #@49
    throw v0

    #@4a
    .line 1455
    :catch_4a
    move-exception v7

    #@4b
    .line 1456
    .local v7, e:Landroid/os/RemoteException;
    :try_start_4b
    const-string v0, "InputMethodManager"

    #@4d
    new-instance v1, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v2, "IME died: "

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_65
    .catchall {:try_start_4b .. :try_end_65} :catchall_47

    #@65
    goto :goto_45
.end method

.method public viewClicked(Landroid/view/View;)V
    .registers 8
    .parameter "view"

    #@0
    .prologue
    .line 1466
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@2
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mNextServedView:Landroid/view/View;

    #@4
    if-eq v2, v3, :cond_27

    #@6
    const/4 v1, 0x1

    #@7
    .line 1467
    .local v1, focusChanged:Z
    :goto_7
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@a
    .line 1468
    iget-object v3, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@c
    monitor-enter v3

    #@d
    .line 1469
    :try_start_d
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@f
    if-eq v2, p1, :cond_1d

    #@11
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@13
    if-eqz v2, :cond_25

    #@15
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@17
    invoke-virtual {v2, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_25

    #@1d
    :cond_1d
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurrentTextBoxAttribute:Landroid/view/inputmethod/EditorInfo;

    #@1f
    if-eqz v2, :cond_25

    #@21
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@23
    if-nez v2, :cond_29

    #@25
    .line 1472
    :cond_25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_d .. :try_end_26} :catchall_30

    #@26
    .line 1481
    :goto_26
    return-void

    #@27
    .line 1466
    .end local v1           #focusChanged:Z
    :cond_27
    const/4 v1, 0x0

    #@28
    goto :goto_7

    #@29
    .line 1476
    .restart local v1       #focusChanged:Z
    :cond_29
    :try_start_29
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@2b
    invoke-interface {v2, v1}, Lcom/android/internal/view/IInputMethodSession;->viewClicked(Z)V
    :try_end_2e
    .catchall {:try_start_29 .. :try_end_2e} :catchall_30
    .catch Landroid/os/RemoteException; {:try_start_29 .. :try_end_2e} :catch_33

    #@2e
    .line 1480
    :goto_2e
    :try_start_2e
    monitor-exit v3

    #@2f
    goto :goto_26

    #@30
    :catchall_30
    move-exception v2

    #@31
    monitor-exit v3
    :try_end_32
    .catchall {:try_start_2e .. :try_end_32} :catchall_30

    #@32
    throw v2

    #@33
    .line 1477
    :catch_33
    move-exception v0

    #@34
    .line 1478
    .local v0, e:Landroid/os/RemoteException;
    :try_start_34
    const-string v2, "InputMethodManager"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "IME died: "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    iget-object v5, p0, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-static {v2, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4e
    .catchall {:try_start_34 .. :try_end_4e} :catchall_30

    #@4e
    goto :goto_2e
.end method

.method public windowDismissed(Landroid/os/IBinder;)V
    .registers 4
    .parameter "appWindowToken"

    #@0
    .prologue
    .line 1237
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->checkFocus()V

    #@3
    .line 1238
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@5
    monitor-enter v1

    #@6
    .line 1239
    :try_start_6
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@8
    if-eqz v0, :cond_15

    #@a
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@f
    move-result-object v0

    #@10
    if-ne v0, p1, :cond_15

    #@12
    .line 1241
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->finishInputLocked()V

    #@15
    .line 1243
    :cond_15
    monitor-exit v1

    #@16
    .line 1244
    return-void

    #@17
    .line 1243
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_6 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method
