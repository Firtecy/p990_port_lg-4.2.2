.class public final Landroid/view/inputmethod/InputMethodInfo;
.super Ljava/lang/Object;
.source "InputMethodInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String; = "InputMethodInfo"


# instance fields
.field final mId:Ljava/lang/String;

.field private mIsAuxIme:Z

.field final mIsDefaultResId:I

.field final mService:Landroid/content/pm/ResolveInfo;

.field final mSettingsActivityName:Ljava/lang/String;

.field private final mSubtypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 408
    new-instance v0, Landroid/view/inputmethod/InputMethodInfo$1;

    #@2
    invoke-direct {v0}, Landroid/view/inputmethod/InputMethodInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/inputmethod/InputMethodInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .registers 4
    .parameter "context"
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 92
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/view/inputmethod/InputMethodInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Ljava/util/Map;)V

    #@4
    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Ljava/util/Map;)V
    .registers 32
    .parameter "context"
    .parameter "service"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 106
    .local p3, additionalSubtypesMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 79
    new-instance v3, Ljava/util/ArrayList;

    #@5
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@8
    move-object/from16 v0, p0

    #@a
    iput-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@c
    .line 107
    move-object/from16 v0, p2

    #@e
    move-object/from16 v1, p0

    #@10
    iput-object v0, v1, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@12
    .line 108
    move-object/from16 v0, p2

    #@14
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@16
    move-object/from16 v25, v0

    #@18
    .line 109
    .local v25, si:Landroid/content/pm/ServiceInfo;
    new-instance v3, Landroid/content/ComponentName;

    #@1a
    move-object/from16 v0, v25

    #@1c
    iget-object v4, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1e
    move-object/from16 v0, v25

    #@20
    iget-object v5, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@22
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    move-object/from16 v0, p0

    #@2b
    iput-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@2d
    .line 110
    const/4 v3, 0x1

    #@2e
    move-object/from16 v0, p0

    #@30
    iput-boolean v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@32
    .line 112
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@35
    move-result-object v21

    #@36
    .line 113
    .local v21, pm:Landroid/content/pm/PackageManager;
    const/16 v24, 0x0

    #@38
    .line 114
    .local v24, settingsActivityComponent:Ljava/lang/String;
    const/16 v18, 0x0

    #@3a
    .line 116
    .local v18, isDefaultResId:I
    const/16 v20, 0x0

    #@3c
    .line 118
    .local v20, parser:Landroid/content/res/XmlResourceParser;
    :try_start_3c
    const-string v3, "android.view.im"

    #@3e
    move-object/from16 v0, v25

    #@40
    move-object/from16 v1, v21

    #@42
    invoke-virtual {v0, v1, v3}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@45
    move-result-object v20

    #@46
    .line 119
    if-nez v20, :cond_75

    #@48
    .line 120
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@4a
    const-string v4, "No android.view.im meta-data"

    #@4c
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v3
    :try_end_50
    .catchall {:try_start_3c .. :try_end_50} :catchall_6e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3c .. :try_end_50} :catch_50

    #@50
    .line 183
    :catch_50
    move-exception v16

    #@51
    .line 184
    .local v16, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_51
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@53
    new-instance v4, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v5, "Unable to create context for: "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    move-object/from16 v0, v25

    #@60
    iget-object v5, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v3
    :try_end_6e
    .catchall {:try_start_51 .. :try_end_6e} :catchall_6e

    #@6e
    .line 187
    .end local v16           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_6e
    move-exception v3

    #@6f
    if-eqz v20, :cond_74

    #@71
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->close()V

    #@74
    :cond_74
    throw v3

    #@75
    .line 124
    :cond_75
    :try_start_75
    move-object/from16 v0, v25

    #@77
    iget-object v3, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@79
    move-object/from16 v0, v21

    #@7b
    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@7e
    move-result-object v22

    #@7f
    .line 126
    .local v22, res:Landroid/content/res/Resources;
    invoke-static/range {v20 .. v20}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@82
    move-result-object v14

    #@83
    .line 130
    .local v14, attrs:Landroid/util/AttributeSet;
    :cond_83
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->next()I

    #@86
    move-result v26

    #@87
    .local v26, type:I
    const/4 v3, 0x1

    #@88
    move/from16 v0, v26

    #@8a
    if-eq v0, v3, :cond_91

    #@8c
    const/4 v3, 0x2

    #@8d
    move/from16 v0, v26

    #@8f
    if-ne v0, v3, :cond_83

    #@91
    .line 133
    :cond_91
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@94
    move-result-object v19

    #@95
    .line 134
    .local v19, nodeName:Ljava/lang/String;
    const-string v3, "input-method"

    #@97
    move-object/from16 v0, v19

    #@99
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v3

    #@9d
    if-nez v3, :cond_a7

    #@9f
    .line 135
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@a1
    const-string v4, "Meta-data does not start with input-method tag"

    #@a3
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@a6
    throw v3

    #@a7
    .line 139
    :cond_a7
    sget-object v3, Lcom/android/internal/R$styleable;->InputMethod:[I

    #@a9
    move-object/from16 v0, v22

    #@ab
    invoke-virtual {v0, v14, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@ae
    move-result-object v23

    #@af
    .line 141
    .local v23, sa:Landroid/content/res/TypedArray;
    const/4 v3, 0x1

    #@b0
    move-object/from16 v0, v23

    #@b2
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@b5
    move-result-object v24

    #@b6
    .line 143
    const/4 v3, 0x0

    #@b7
    const/4 v4, 0x0

    #@b8
    move-object/from16 v0, v23

    #@ba
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@bd
    move-result v18

    #@be
    .line 145
    invoke-virtual/range {v23 .. v23}, Landroid/content/res/TypedArray;->recycle()V

    #@c1
    .line 147
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@c4
    move-result v15

    #@c5
    .line 150
    .local v15, depth:I
    :cond_c5
    :goto_c5
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->next()I

    #@c8
    move-result v26

    #@c9
    const/4 v3, 0x3

    #@ca
    move/from16 v0, v26

    #@cc
    if-ne v0, v3, :cond_d4

    #@ce
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@d1
    move-result v3

    #@d2
    if-le v3, v15, :cond_145

    #@d4
    :cond_d4
    const/4 v3, 0x1

    #@d5
    move/from16 v0, v26

    #@d7
    if-eq v0, v3, :cond_145

    #@d9
    .line 151
    const/4 v3, 0x2

    #@da
    move/from16 v0, v26

    #@dc
    if-ne v0, v3, :cond_c5

    #@de
    .line 152
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@e1
    move-result-object v19

    #@e2
    .line 153
    const-string/jumbo v3, "subtype"

    #@e5
    move-object/from16 v0, v19

    #@e7
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ea
    move-result v3

    #@eb
    if-nez v3, :cond_f5

    #@ed
    .line 154
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@ef
    const-string v4, "Meta-data in input-method does not start with subtype tag"

    #@f1
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@f4
    throw v3

    #@f5
    .line 157
    :cond_f5
    sget-object v3, Lcom/android/internal/R$styleable;->InputMethod_Subtype:[I

    #@f7
    move-object/from16 v0, v22

    #@f9
    invoke-virtual {v0, v14, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@fc
    move-result-object v12

    #@fd
    .line 159
    .local v12, a:Landroid/content/res/TypedArray;
    new-instance v2, Landroid/view/inputmethod/InputMethodSubtype;

    #@ff
    const/4 v3, 0x0

    #@100
    const/4 v4, 0x0

    #@101
    invoke-virtual {v12, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@104
    move-result v3

    #@105
    const/4 v4, 0x1

    #@106
    const/4 v5, 0x0

    #@107
    invoke-virtual {v12, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@10a
    move-result v4

    #@10b
    const/4 v5, 0x2

    #@10c
    invoke-virtual {v12, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@10f
    move-result-object v5

    #@110
    const/4 v6, 0x3

    #@111
    invoke-virtual {v12, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@114
    move-result-object v6

    #@115
    const/4 v7, 0x4

    #@116
    invoke-virtual {v12, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@119
    move-result-object v7

    #@11a
    const/4 v8, 0x5

    #@11b
    const/4 v9, 0x0

    #@11c
    invoke-virtual {v12, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@11f
    move-result v8

    #@120
    const/4 v9, 0x6

    #@121
    const/4 v10, 0x0

    #@122
    invoke-virtual {v12, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@125
    move-result v9

    #@126
    const/4 v10, 0x7

    #@127
    const/16 v27, 0x0

    #@129
    move/from16 v0, v27

    #@12b
    invoke-virtual {v12, v10, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@12e
    move-result v10

    #@12f
    invoke-direct/range {v2 .. v10}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    #@132
    .line 177
    .local v2, subtype:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->isAuxiliary()Z

    #@135
    move-result v3

    #@136
    if-nez v3, :cond_13d

    #@138
    .line 178
    const/4 v3, 0x0

    #@139
    move-object/from16 v0, p0

    #@13b
    iput-boolean v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@13d
    .line 180
    :cond_13d
    move-object/from16 v0, p0

    #@13f
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@141
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_144
    .catchall {:try_start_75 .. :try_end_144} :catchall_6e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_75 .. :try_end_144} :catch_50

    #@144
    goto :goto_c5

    #@145
    .line 187
    .end local v2           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v12           #a:Landroid/content/res/TypedArray;
    :cond_145
    if-eqz v20, :cond_14a

    #@147
    invoke-interface/range {v20 .. v20}, Landroid/content/res/XmlResourceParser;->close()V

    #@14a
    .line 190
    :cond_14a
    move-object/from16 v0, p0

    #@14c
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@14e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@151
    move-result v3

    #@152
    if-nez v3, :cond_159

    #@154
    .line 191
    const/4 v3, 0x0

    #@155
    move-object/from16 v0, p0

    #@157
    iput-boolean v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@159
    .line 194
    :cond_159
    if-eqz p3, :cond_1c4

    #@15b
    move-object/from16 v0, p0

    #@15d
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@15f
    move-object/from16 v0, p3

    #@161
    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@164
    move-result v3

    #@165
    if-eqz v3, :cond_1c4

    #@167
    .line 195
    move-object/from16 v0, p0

    #@169
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@16b
    move-object/from16 v0, p3

    #@16d
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@170
    move-result-object v13

    #@171
    check-cast v13, Ljava/util/List;

    #@173
    .line 196
    .local v13, additionalSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    #@176
    move-result v11

    #@177
    .line 197
    .local v11, N:I
    const/16 v17, 0x0

    #@179
    .local v17, i:I
    :goto_179
    move/from16 v0, v17

    #@17b
    if-ge v0, v11, :cond_1c4

    #@17d
    .line 198
    move/from16 v0, v17

    #@17f
    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@182
    move-result-object v2

    #@183
    check-cast v2, Landroid/view/inputmethod/InputMethodSubtype;

    #@185
    .line 199
    .restart local v2       #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    move-object/from16 v0, p0

    #@187
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@189
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@18c
    move-result v3

    #@18d
    if-nez v3, :cond_199

    #@18f
    .line 200
    move-object/from16 v0, p0

    #@191
    iget-object v3, v0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@193
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@196
    .line 197
    :goto_196
    add-int/lit8 v17, v17, 0x1

    #@198
    goto :goto_179

    #@199
    .line 202
    :cond_199
    const-string v3, "InputMethodInfo"

    #@19b
    new-instance v4, Ljava/lang/StringBuilder;

    #@19d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a0
    const-string v5, "Duplicated subtype definition found: "

    #@1a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v4

    #@1a6
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@1a9
    move-result-object v5

    #@1aa
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v4

    #@1ae
    const-string v5, ", "

    #@1b0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v4

    #@1b4
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    #@1b7
    move-result-object v5

    #@1b8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v4

    #@1bc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bf
    move-result-object v4

    #@1c0
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c3
    goto :goto_196

    #@1c4
    .line 207
    .end local v2           #subtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v11           #N:I
    .end local v13           #additionalSubtypes:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    .end local v17           #i:I
    :cond_1c4
    move-object/from16 v0, v24

    #@1c6
    move-object/from16 v1, p0

    #@1c8
    iput-object v0, v1, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@1ca
    .line 208
    move/from16 v0, v18

    #@1cc
    move-object/from16 v1, p0

    #@1ce
    iput v0, v1, Landroid/view/inputmethod/InputMethodInfo;->mIsDefaultResId:I

    #@1d0
    .line 209
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 211
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 79
    new-instance v1, Ljava/util/ArrayList;

    #@6
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@b
    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@11
    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@17
    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v1

    #@1b
    iput v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsDefaultResId:I

    #@1d
    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v1

    #@21
    if-ne v1, v0, :cond_37

    #@23
    :goto_23
    iput-boolean v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@25
    .line 216
    sget-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@2d
    iput-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2f
    .line 217
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@31
    sget-object v1, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@33
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    #@36
    .line 218
    return-void

    #@37
    .line 215
    :cond_37
    const/4 v0, 0x0

    #@38
    goto :goto_23
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .registers 12
    .parameter "packageName"
    .parameter "className"
    .parameter "label"
    .parameter "settingsActivity"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 224
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 79
    new-instance v3, Ljava/util/ArrayList;

    #@7
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v3, p0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@c
    .line 225
    new-instance v1, Landroid/content/pm/ResolveInfo;

    #@e
    invoke-direct {v1}, Landroid/content/pm/ResolveInfo;-><init>()V

    #@11
    .line 226
    .local v1, ri:Landroid/content/pm/ResolveInfo;
    new-instance v2, Landroid/content/pm/ServiceInfo;

    #@13
    invoke-direct {v2}, Landroid/content/pm/ServiceInfo;-><init>()V

    #@16
    .line 227
    .local v2, si:Landroid/content/pm/ServiceInfo;
    new-instance v0, Landroid/content/pm/ApplicationInfo;

    #@18
    invoke-direct {v0}, Landroid/content/pm/ApplicationInfo;-><init>()V

    #@1b
    .line 228
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    iput-object p1, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@1d
    .line 229
    iput-boolean v4, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    #@1f
    .line 230
    iput-object v0, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@21
    .line 231
    iput-boolean v4, v2, Landroid/content/pm/ComponentInfo;->enabled:Z

    #@23
    .line 232
    iput-object p1, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@25
    .line 233
    iput-object p2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@27
    .line 234
    iput-boolean v4, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    #@29
    .line 235
    iput-object p3, v2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    #@2b
    .line 236
    iput-object v2, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@2d
    .line 237
    iput-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2f
    .line 238
    new-instance v3, Landroid/content/ComponentName;

    #@31
    iget-object v4, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@33
    iget-object v5, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@35
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    iput-object v3, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@3e
    .line 239
    iput-object p4, p0, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@40
    .line 240
    iput v6, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsDefaultResId:I

    #@42
    .line 241
    iput-boolean v6, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@44
    .line 242
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 423
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string/jumbo v1, "mId="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    const-string v1, " mSettingsActivityName="

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@29
    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string/jumbo v1, "mIsDefaultResId=0x"

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    iget v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsDefaultResId:I

    #@3b
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@4a
    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, "Service:"

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@60
    .line 349
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@62
    new-instance v1, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    const-string v2, "  "

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ResolveInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@78
    .line 350
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 368
    if-ne p1, p0, :cond_5

    #@3
    const/4 v1, 0x1

    #@4
    .line 374
    :cond_4
    :goto_4
    return v1

    #@5
    .line 369
    :cond_5
    if-eqz p1, :cond_4

    #@7
    .line 371
    instance-of v2, p1, Landroid/view/inputmethod/InputMethodInfo;

    #@9
    if-eqz v2, :cond_4

    #@b
    move-object v0, p1

    #@c
    .line 373
    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    #@e
    .line 374
    .local v0, obj:Landroid/view/inputmethod/InputMethodInfo;
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@10
    iget-object v2, v0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v1

    #@16
    goto :goto_4
.end method

.method public getComponent()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 280
    new-instance v0, Landroid/content/ComponentName;

    #@2
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@4
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@6
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8
    iget-object v2, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@a
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@c
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@e
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIsDefaultResourceId()I
    .registers 2

    #@0
    .prologue
    .line 340
    iget v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsDefaultResId:I

    #@2
    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getServiceInfo()Landroid/content/pm/ServiceInfo;
    .registers 2

    #@0
    .prologue
    .line 272
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    return-object v0
.end method

.method public getServiceName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 264
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getSettingsActivity()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSubtypeAt(I)Landroid/view/inputmethod/InputMethodSubtype;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 331
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/inputmethod/InputMethodSubtype;

    #@8
    return-object v0
.end method

.method public getSubtypeCount()I
    .registers 2

    #@0
    .prologue
    .line 322
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 379
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isAuxiliaryIme()Z
    .registers 2

    #@0
    .prologue
    .line 386
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@2
    return v0
.end method

.method public loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "InputMethodInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", settings: "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string/jumbo v1, "}"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 397
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mId:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 398
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mSettingsActivityName:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 399
    iget v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsDefaultResId:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 400
    iget-boolean v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mIsAuxIme:Z

    #@11
    if-eqz v0, :cond_22

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 401
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@19
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 402
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodInfo;->mSubtypes:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@21
    .line 403
    return-void

    #@22
    .line 400
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_14
.end method
