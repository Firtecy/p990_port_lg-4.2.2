.class public Landroid/view/inputmethod/ExtractedText;
.super Ljava/lang/Object;
.source "ExtractedText.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/inputmethod/ExtractedText;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_DISABLE_BUBBLE_CUT:I = 0x10

.field public static final FLAG_DISABLE_BUBBLE_PASTE:I = 0x8

.field public static final FLAG_DISABLE_BUBBLE_POPUP:I = 0x4

.field public static final FLAG_IS_HANDLE_SHOWING:I = 0x20

.field public static final FLAG_SELECTING:I = 0x2

.field public static final FLAG_SINGLE_LINE:I = 0x1


# instance fields
.field public flags:I

.field public partialEndOffset:I

.field public partialStartOffset:I

.field public selectionEnd:I

.field public selectionStart:I

.field public startOffset:I

.field public text:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 126
    new-instance v0, Landroid/view/inputmethod/ExtractedText$1;

    #@2
    invoke-direct {v0}, Landroid/view/inputmethod/ExtractedText$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/inputmethod/ExtractedText;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 145
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    #@2
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@5
    .line 115
    iget v0, p0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 116
    iget v0, p0, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 117
    iget v0, p0, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 118
    iget v0, p0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 119
    iget v0, p0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 120
    iget v0, p0, Landroid/view/inputmethod/ExtractedText;->flags:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 121
    return-void
.end method
