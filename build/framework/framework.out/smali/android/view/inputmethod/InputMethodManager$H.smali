.class Landroid/view/inputmethod/InputMethodManager$H;
.super Landroid/os/Handler;
.source "InputMethodManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/inputmethod/InputMethodManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method constructor <init>(Landroid/view/inputmethod/InputMethodManager;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 345
    iput-object p1, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@2
    .line 346
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 347
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 351
    iget v7, p1, Landroid/os/Message;->what:I

    #@5
    packed-switch v7, :pswitch_data_168

    #@8
    .line 490
    :cond_8
    :goto_8
    return-void

    #@9
    .line 353
    :pswitch_9
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    check-cast v1, Lcom/android/internal/os/SomeArgs;

    #@d
    .line 355
    .local v1, args:Lcom/android/internal/os/SomeArgs;
    :try_start_d
    iget-object v9, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@f
    iget-object v6, v1, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@11
    check-cast v6, Ljava/io/FileDescriptor;

    #@13
    iget-object v7, v1, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@15
    check-cast v7, Ljava/io/PrintWriter;

    #@17
    iget-object v8, v1, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    #@19
    check-cast v8, [Ljava/lang/String;

    #@1b
    check-cast v8, [Ljava/lang/String;

    #@1d
    invoke-virtual {v9, v6, v7, v8}, Landroid/view/inputmethod/InputMethodManager;->doDump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_20
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_20} :catch_2f

    #@20
    .line 360
    :goto_20
    iget-object v7, v1, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@22
    monitor-enter v7

    #@23
    .line 361
    :try_start_23
    iget-object v6, v1, Lcom/android/internal/os/SomeArgs;->arg4:Ljava/lang/Object;

    #@25
    check-cast v6, Ljava/util/concurrent/CountDownLatch;

    #@27
    invoke-virtual {v6}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@2a
    .line 362
    monitor-exit v7
    :try_end_2b
    .catchall {:try_start_23 .. :try_end_2b} :catchall_4b

    #@2b
    .line 363
    invoke-virtual {v1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@2e
    goto :goto_8

    #@2f
    .line 357
    :catch_2f
    move-exception v2

    #@30
    .line 358
    .local v2, e:Ljava/lang/RuntimeException;
    iget-object v6, v1, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@32
    check-cast v6, Ljava/io/PrintWriter;

    #@34
    new-instance v7, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v8, "Exception: "

    #@3b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v6, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4a
    goto :goto_20

    #@4b
    .line 362
    .end local v2           #e:Ljava/lang/RuntimeException;
    :catchall_4b
    move-exception v6

    #@4c
    :try_start_4c
    monitor-exit v7
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    throw v6

    #@4e
    .line 367
    .end local v1           #args:Lcom/android/internal/os/SomeArgs;
    :pswitch_4e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@50
    check-cast v3, Lcom/android/internal/view/InputBindResult;

    #@52
    .line 371
    .local v3, res:Lcom/android/internal/view/InputBindResult;
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@54
    iget-object v7, v7, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@56
    monitor-enter v7

    #@57
    .line 372
    :try_start_57
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@59
    iget v8, v8, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@5b
    if-ltz v8, :cond_65

    #@5d
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@5f
    iget v8, v8, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@61
    iget v9, v3, Lcom/android/internal/view/InputBindResult;->sequence:I

    #@63
    if-eq v8, v9, :cond_93

    #@65
    .line 373
    :cond_65
    const-string v6, "InputMethodManager"

    #@67
    new-instance v8, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v9, "Ignoring onBind: cur seq="

    #@6e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    iget-object v9, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@74
    iget v9, v9, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@76
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    const-string v9, ", given seq="

    #@7c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v8

    #@80
    iget v9, v3, Lcom/android/internal/view/InputBindResult;->sequence:I

    #@82
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v8

    #@86
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v8

    #@8a
    invoke-static {v6, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 375
    monitor-exit v7

    #@8e
    goto/16 :goto_8

    #@90
    .line 381
    :catchall_90
    move-exception v6

    #@91
    monitor-exit v7
    :try_end_92
    .catchall {:try_start_57 .. :try_end_92} :catchall_90

    #@92
    throw v6

    #@93
    .line 378
    :cond_93
    :try_start_93
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@95
    iget-object v9, v3, Lcom/android/internal/view/InputBindResult;->method:Lcom/android/internal/view/IInputMethodSession;

    #@97
    iput-object v9, v8, Landroid/view/inputmethod/InputMethodManager;->mCurMethod:Lcom/android/internal/view/IInputMethodSession;

    #@99
    .line 379
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@9b
    iget-object v9, v3, Lcom/android/internal/view/InputBindResult;->id:Ljava/lang/String;

    #@9d
    iput-object v9, v8, Landroid/view/inputmethod/InputMethodManager;->mCurId:Ljava/lang/String;

    #@9f
    .line 380
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@a1
    iget v9, v3, Lcom/android/internal/view/InputBindResult;->sequence:I

    #@a3
    iput v9, v8, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@a5
    .line 381
    monitor-exit v7
    :try_end_a6
    .catchall {:try_start_93 .. :try_end_a6} :catchall_90

    #@a6
    .line 382
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@a8
    invoke-virtual {v7, v10, v6, v6, v6}, Landroid/view/inputmethod/InputMethodManager;->startInputInner(Landroid/os/IBinder;III)Z

    #@ab
    goto/16 :goto_8

    #@ad
    .line 386
    .end local v3           #res:Lcom/android/internal/view/InputBindResult;
    :pswitch_ad
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@af
    .line 390
    .local v4, sequence:I
    const/4 v5, 0x0

    #@b0
    .line 391
    .local v5, startInput:Z
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@b2
    iget-object v7, v7, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@b4
    monitor-enter v7

    #@b5
    .line 392
    :try_start_b5
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@b7
    iget v8, v8, Landroid/view/inputmethod/InputMethodManager;->mBindSequence:I

    #@b9
    if-ne v8, v4, :cond_dc

    #@bb
    .line 403
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@bd
    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodManager;->clearBindingLocked()V

    #@c0
    .line 407
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@c2
    iget-object v8, v8, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@c4
    if-eqz v8, :cond_d5

    #@c6
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@c8
    iget-object v8, v8, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@ca
    invoke-virtual {v8}, Landroid/view/View;->isFocused()Z

    #@cd
    move-result v8

    #@ce
    if-eqz v8, :cond_d5

    #@d0
    .line 408
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@d2
    const/4 v9, 0x1

    #@d3
    iput-boolean v9, v8, Landroid/view/inputmethod/InputMethodManager;->mServedConnecting:Z

    #@d5
    .line 410
    :cond_d5
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@d7
    iget-boolean v8, v8, Landroid/view/inputmethod/InputMethodManager;->mActive:Z

    #@d9
    if-eqz v8, :cond_dc

    #@db
    .line 411
    const/4 v5, 0x1

    #@dc
    .line 414
    :cond_dc
    monitor-exit v7
    :try_end_dd
    .catchall {:try_start_b5 .. :try_end_dd} :catchall_e6

    #@dd
    .line 415
    if-eqz v5, :cond_8

    #@df
    .line 416
    iget-object v7, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@e1
    invoke-virtual {v7, v10, v6, v6, v6}, Landroid/view/inputmethod/InputMethodManager;->startInputInner(Landroid/os/IBinder;III)Z

    #@e4
    goto/16 :goto_8

    #@e6
    .line 414
    :catchall_e6
    move-exception v6

    #@e7
    :try_start_e7
    monitor-exit v7
    :try_end_e8
    .catchall {:try_start_e7 .. :try_end_e8} :catchall_e6

    #@e8
    throw v6

    #@e9
    .line 421
    .end local v4           #sequence:I
    .end local v5           #startInput:Z
    :pswitch_e9
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@eb
    if-eqz v7, :cond_135

    #@ed
    .line 425
    .local v0, active:Z
    :goto_ed
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@ef
    iget-object v7, v6, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@f1
    monitor-enter v7

    #@f2
    .line 426
    :try_start_f2
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@f4
    iput-boolean v0, v6, Landroid/view/inputmethod/InputMethodManager;->mActive:Z

    #@f6
    .line 427
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@f8
    const/4 v8, 0x0

    #@f9
    iput-boolean v8, v6, Landroid/view/inputmethod/InputMethodManager;->mFullscreenMode:Z

    #@fb
    .line 428
    if-nez v0, :cond_12f

    #@fd
    .line 432
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@ff
    const/4 v8, 0x1

    #@100
    iput-boolean v8, v6, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z
    :try_end_102
    .catchall {:try_start_f2 .. :try_end_102} :catchall_132

    #@102
    .line 436
    :try_start_102
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@104
    iget-object v6, v6, Landroid/view/inputmethod/InputMethodManager;->mIInputContext:Lcom/android/internal/view/IInputContext;

    #@106
    invoke-interface {v6}, Lcom/android/internal/view/IInputContext;->finishComposingText()V
    :try_end_109
    .catchall {:try_start_102 .. :try_end_109} :catchall_132
    .catch Landroid/os/RemoteException; {:try_start_102 .. :try_end_109} :catch_165

    #@109
    .line 441
    :goto_109
    :try_start_109
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@10b
    iget-object v6, v6, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@10d
    if-eqz v6, :cond_12f

    #@10f
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@111
    iget-object v6, v6, Landroid/view/inputmethod/InputMethodManager;->mServedView:Landroid/view/View;

    #@113
    invoke-virtual {v6}, Landroid/view/View;->hasWindowFocus()Z

    #@116
    move-result v6

    #@117
    if-eqz v6, :cond_12f

    #@119
    .line 449
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@11b
    iget-object v8, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@11d
    iget-boolean v8, v8, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z

    #@11f
    const/4 v9, 0x0

    #@120
    #calls: Landroid/view/inputmethod/InputMethodManager;->checkFocusNoStartInput(ZZ)Z
    invoke-static {v6, v8, v9}, Landroid/view/inputmethod/InputMethodManager;->access$000(Landroid/view/inputmethod/InputMethodManager;ZZ)Z

    #@123
    move-result v6

    #@124
    if-eqz v6, :cond_12f

    #@126
    .line 450
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@128
    const/4 v8, 0x0

    #@129
    const/4 v9, 0x0

    #@12a
    const/4 v10, 0x0

    #@12b
    const/4 v11, 0x0

    #@12c
    invoke-virtual {v6, v8, v9, v10, v11}, Landroid/view/inputmethod/InputMethodManager;->startInputInner(Landroid/os/IBinder;III)Z

    #@12f
    .line 454
    :cond_12f
    monitor-exit v7

    #@130
    goto/16 :goto_8

    #@132
    :catchall_132
    move-exception v6

    #@133
    monitor-exit v7
    :try_end_134
    .catchall {:try_start_109 .. :try_end_134} :catchall_132

    #@134
    throw v6

    #@135
    .end local v0           #active:Z
    :cond_135
    move v0, v6

    #@136
    .line 421
    goto :goto_ed

    #@137
    .line 465
    :pswitch_137
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@139
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@13b
    invoke-virtual {v6, v7}, Landroid/view/inputmethod/InputMethodManager;->timeoutEvent(I)V

    #@13e
    goto/16 :goto_8

    #@140
    .line 470
    :pswitch_140
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@142
    if-eqz v7, :cond_161

    #@144
    .line 471
    .restart local v0       #active:Z
    :goto_144
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@146
    iget-object v7, v6, Landroid/view/inputmethod/InputMethodManager;->mH:Landroid/view/inputmethod/InputMethodManager$H;

    #@148
    monitor-enter v7

    #@149
    .line 472
    :try_start_149
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@14b
    iput-boolean v0, v6, Landroid/view/inputmethod/InputMethodManager;->mActive:Z

    #@14d
    .line 473
    if-nez v0, :cond_15b

    #@14f
    .line 477
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@151
    const/4 v8, 0x1

    #@152
    iput-boolean v8, v6, Landroid/view/inputmethod/InputMethodManager;->mHasBeenInactive:Z
    :try_end_154
    .catchall {:try_start_149 .. :try_end_154} :catchall_15e

    #@154
    .line 481
    :try_start_154
    iget-object v6, p0, Landroid/view/inputmethod/InputMethodManager$H;->this$0:Landroid/view/inputmethod/InputMethodManager;

    #@156
    iget-object v6, v6, Landroid/view/inputmethod/InputMethodManager;->mIInputContext:Lcom/android/internal/view/IInputContext;

    #@158
    invoke-interface {v6}, Lcom/android/internal/view/IInputContext;->finishComposingText()V
    :try_end_15b
    .catchall {:try_start_154 .. :try_end_15b} :catchall_15e
    .catch Landroid/os/RemoteException; {:try_start_154 .. :try_end_15b} :catch_163

    #@15b
    .line 485
    :cond_15b
    :goto_15b
    :try_start_15b
    monitor-exit v7

    #@15c
    goto/16 :goto_8

    #@15e
    :catchall_15e
    move-exception v6

    #@15f
    monitor-exit v7
    :try_end_160
    .catchall {:try_start_15b .. :try_end_160} :catchall_15e

    #@160
    throw v6

    #@161
    .end local v0           #active:Z
    :cond_161
    move v0, v6

    #@162
    .line 470
    goto :goto_144

    #@163
    .line 482
    .restart local v0       #active:Z
    :catch_163
    move-exception v6

    #@164
    goto :goto_15b

    #@165
    .line 437
    :catch_165
    move-exception v6

    #@166
    goto :goto_109

    #@167
    .line 351
    nop

    #@168
    :pswitch_data_168
    .packed-switch 0x1
        :pswitch_9
        :pswitch_4e
        :pswitch_ad
        :pswitch_e9
        :pswitch_137
        :pswitch_140
    .end packed-switch
.end method
