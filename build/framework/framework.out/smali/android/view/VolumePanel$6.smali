.class Landroid/view/VolumePanel$6;
.super Landroid/content/BroadcastReceiver;
.source "VolumePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/VolumePanel;->listenToScreenStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/VolumePanel;


# direct methods
.method constructor <init>(Landroid/view/VolumePanel;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1501
    iput-object p1, p0, Landroid/view/VolumePanel$6;->this$0:Landroid/view/VolumePanel;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1504
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1507
    .local v0, action:Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_27

    #@c
    .line 1508
    iget-object v1, p0, Landroid/view/VolumePanel$6;->this$0:Landroid/view/VolumePanel;

    #@e
    invoke-static {v1}, Landroid/view/VolumePanel;->access$900(Landroid/view/VolumePanel;)Landroid/app/Dialog;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_27

    #@18
    .line 1509
    iget-object v1, p0, Landroid/view/VolumePanel$6;->this$0:Landroid/view/VolumePanel;

    #@1a
    invoke-static {v1}, Landroid/view/VolumePanel;->access$900(Landroid/view/VolumePanel;)Landroid/app/Dialog;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    #@21
    .line 1510
    iget-object v1, p0, Landroid/view/VolumePanel$6;->this$0:Landroid/view/VolumePanel;

    #@23
    const/4 v2, -0x1

    #@24
    invoke-static {v1, v2}, Landroid/view/VolumePanel;->access$402(Landroid/view/VolumePanel;I)I

    #@27
    .line 1513
    :cond_27
    return-void
.end method
