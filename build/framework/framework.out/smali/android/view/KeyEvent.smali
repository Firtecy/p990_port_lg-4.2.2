.class public Landroid/view/KeyEvent;
.super Landroid/view/InputEvent;
.source "KeyEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/KeyEvent$DispatcherState;,
        Landroid/view/KeyEvent$Callback;
    }
.end annotation


# static fields
.field public static final ACTION_DOWN:I = 0x0

.field public static final ACTION_MULTIPLE:I = 0x2

.field public static final ACTION_UP:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/KeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field static final DEBUG:Z = false

.field public static final FLAG_CANCELED:I = 0x20

.field public static final FLAG_CANCELED_LONG_PRESS:I = 0x100

.field public static final FLAG_EDITOR_ACTION:I = 0x10

.field public static final FLAG_FALLBACK:I = 0x400

.field public static final FLAG_FROM_SYSTEM:I = 0x8

.field public static final FLAG_KEEP_TOUCH_MODE:I = 0x4

.field public static final FLAG_LONG_PRESS:I = 0x80

.field public static final FLAG_SOFT_KEYBOARD:I = 0x2

.field public static final FLAG_START_TRACKING:I = 0x40000000

.field public static final FLAG_TAINTED:I = -0x80000000

.field public static final FLAG_TRACKING:I = 0x200

.field public static final FLAG_VIRTUAL_HARD_KEY:I = 0x40

.field public static final FLAG_WOKE_HERE:I = 0x1

.field public static final KEYCODE_0:I = 0x7

.field public static final KEYCODE_1:I = 0x8

.field public static final KEYCODE_2:I = 0x9

.field public static final KEYCODE_3:I = 0xa

.field public static final KEYCODE_3D_MODE:I = 0xce

.field public static final KEYCODE_4:I = 0xb

.field public static final KEYCODE_5:I = 0xc

.field public static final KEYCODE_6:I = 0xd

.field public static final KEYCODE_7:I = 0xe

.field public static final KEYCODE_8:I = 0xf

.field public static final KEYCODE_9:I = 0x10

.field public static final KEYCODE_A:I = 0x1d

.field public static final KEYCODE_ALT_LEFT:I = 0x39

.field public static final KEYCODE_ALT_RIGHT:I = 0x3a

.field public static final KEYCODE_APOSTROPHE:I = 0x4b

.field public static final KEYCODE_APP_SWITCH:I = 0xbb

.field public static final KEYCODE_ASSIST:I = 0xdb

.field public static final KEYCODE_AT:I = 0x4d

.field public static final KEYCODE_AVR_INPUT:I = 0xb6

.field public static final KEYCODE_AVR_POWER:I = 0xb5

.field public static final KEYCODE_B:I = 0x1e

.field public static final KEYCODE_BACK:I = 0x4

.field public static final KEYCODE_BACKSLASH:I = 0x49

.field public static final KEYCODE_BOOKMARK:I = 0xae

.field public static final KEYCODE_BREAK:I = 0x79

.field public static final KEYCODE_BUTTON_1:I = 0xbc

.field public static final KEYCODE_BUTTON_10:I = 0xc5

.field public static final KEYCODE_BUTTON_11:I = 0xc6

.field public static final KEYCODE_BUTTON_12:I = 0xc7

.field public static final KEYCODE_BUTTON_13:I = 0xc8

.field public static final KEYCODE_BUTTON_14:I = 0xc9

.field public static final KEYCODE_BUTTON_15:I = 0xca

.field public static final KEYCODE_BUTTON_16:I = 0xcb

.field public static final KEYCODE_BUTTON_2:I = 0xbd

.field public static final KEYCODE_BUTTON_3:I = 0xbe

.field public static final KEYCODE_BUTTON_4:I = 0xbf

.field public static final KEYCODE_BUTTON_5:I = 0xc0

.field public static final KEYCODE_BUTTON_6:I = 0xc1

.field public static final KEYCODE_BUTTON_7:I = 0xc2

.field public static final KEYCODE_BUTTON_8:I = 0xc3

.field public static final KEYCODE_BUTTON_9:I = 0xc4

.field public static final KEYCODE_BUTTON_A:I = 0x60

.field public static final KEYCODE_BUTTON_B:I = 0x61

.field public static final KEYCODE_BUTTON_C:I = 0x62

.field public static final KEYCODE_BUTTON_L1:I = 0x66

.field public static final KEYCODE_BUTTON_L2:I = 0x68

.field public static final KEYCODE_BUTTON_MODE:I = 0x6e

.field public static final KEYCODE_BUTTON_R1:I = 0x67

.field public static final KEYCODE_BUTTON_R2:I = 0x69

.field public static final KEYCODE_BUTTON_SELECT:I = 0x6d

.field public static final KEYCODE_BUTTON_START:I = 0x6c

.field public static final KEYCODE_BUTTON_THUMBL:I = 0x6a

.field public static final KEYCODE_BUTTON_THUMBR:I = 0x6b

.field public static final KEYCODE_BUTTON_X:I = 0x63

.field public static final KEYCODE_BUTTON_Y:I = 0x64

.field public static final KEYCODE_BUTTON_Z:I = 0x65

.field public static final KEYCODE_C:I = 0x1f

.field public static final KEYCODE_CALCULATOR:I = 0xd2

.field public static final KEYCODE_CALENDAR:I = 0xd0

.field public static final KEYCODE_CALL:I = 0x5

.field public static final KEYCODE_CAMERA:I = 0x1b

.field public static final KEYCODE_CAPS_LOCK:I = 0x73

.field public static final KEYCODE_CAPTIONS:I = 0xaf

.field public static final KEYCODE_CHANNEL_DOWN:I = 0xa7

.field public static final KEYCODE_CHANNEL_UP:I = 0xa6

.field public static final KEYCODE_CLEAR:I = 0x1c

.field public static final KEYCODE_COMMA:I = 0x37

.field public static final KEYCODE_CONTACTS:I = 0xcf

.field public static final KEYCODE_CTRL_LEFT:I = 0x71

.field public static final KEYCODE_CTRL_RIGHT:I = 0x72

.field public static final KEYCODE_D:I = 0x20

.field public static final KEYCODE_DEL:I = 0x43

.field public static final KEYCODE_DPAD_CENTER:I = 0x17

.field public static final KEYCODE_DPAD_DOWN:I = 0x14

.field public static final KEYCODE_DPAD_LEFT:I = 0x15

.field public static final KEYCODE_DPAD_RIGHT:I = 0x16

.field public static final KEYCODE_DPAD_UP:I = 0x13

.field public static final KEYCODE_DVR:I = 0xad

.field public static final KEYCODE_E:I = 0x21

.field public static final KEYCODE_EDITTEXT_CURSOR_MOVE_LEFT:I = 0xe7

.field public static final KEYCODE_EDITTEXT_CURSOR_MOVE_RIGHT:I = 0xe8

.field public static final KEYCODE_EISU:I = 0xd4

.field public static final KEYCODE_ENDCALL:I = 0x6

.field public static final KEYCODE_ENTER:I = 0x42

.field public static final KEYCODE_ENVELOPE:I = 0x41

.field public static final KEYCODE_EQUALS:I = 0x46

.field public static final KEYCODE_ESCAPE:I = 0x6f

.field public static final KEYCODE_EXPLORER:I = 0x40

.field public static final KEYCODE_F:I = 0x22

.field public static final KEYCODE_F1:I = 0x83

.field public static final KEYCODE_F10:I = 0x8c

.field public static final KEYCODE_F11:I = 0x8d

.field public static final KEYCODE_F12:I = 0x8e

.field public static final KEYCODE_F2:I = 0x84

.field public static final KEYCODE_F3:I = 0x85

.field public static final KEYCODE_F4:I = 0x86

.field public static final KEYCODE_F5:I = 0x87

.field public static final KEYCODE_F6:I = 0x88

.field public static final KEYCODE_F7:I = 0x89

.field public static final KEYCODE_F8:I = 0x8a

.field public static final KEYCODE_F9:I = 0x8b

.field public static final KEYCODE_FOCUS:I = 0x50

.field public static final KEYCODE_FORWARD:I = 0x7d

.field public static final KEYCODE_FORWARD_DEL:I = 0x70

.field public static final KEYCODE_FUNCTION:I = 0x77

.field public static final KEYCODE_G:I = 0x23

.field public static final KEYCODE_GRAVE:I = 0x44

.field public static final KEYCODE_GUIDE:I = 0xac

.field public static final KEYCODE_H:I = 0x24

.field public static final KEYCODE_HEADSETHOOK:I = 0x4f

.field public static final KEYCODE_HENKAN:I = 0xd6

.field public static final KEYCODE_HOME:I = 0x3

.field public static final KEYCODE_I:I = 0x25

.field public static final KEYCODE_INFO:I = 0xa5

.field public static final KEYCODE_INSERT:I = 0x7c

.field public static final KEYCODE_J:I = 0x26

.field public static final KEYCODE_K:I = 0x27

.field public static final KEYCODE_KANA:I = 0xda

.field public static final KEYCODE_KATAKANA_HIRAGANA:I = 0xd7

.field public static final KEYCODE_KEY_QUICKCLIP:I = 0xe1

.field public static final KEYCODE_KEY_SLEEP:I = 0xe3

.field public static final KEYCODE_KEY_WAKE:I = 0xe2

.field public static final KEYCODE_L:I = 0x28

.field public static final KEYCODE_LANGUAGE_SWITCH:I = 0xcc

.field public static final KEYCODE_LEFT_BRACKET:I = 0x47

.field public static final KEYCODE_M:I = 0x29

.field public static final KEYCODE_MANNER_MODE:I = 0xcd

.field public static final KEYCODE_MEDIA_CLOSE:I = 0x80

.field public static final KEYCODE_MEDIA_EJECT:I = 0x81

.field public static final KEYCODE_MEDIA_FAST_FORWARD:I = 0x5a

.field public static final KEYCODE_MEDIA_NEXT:I = 0x57

.field public static final KEYCODE_MEDIA_PAUSE:I = 0x7f

.field public static final KEYCODE_MEDIA_PLAY:I = 0x7e

.field public static final KEYCODE_MEDIA_PLAY_PAUSE:I = 0x55

.field public static final KEYCODE_MEDIA_PREVIOUS:I = 0x58

.field public static final KEYCODE_MEDIA_RECORD:I = 0x82

.field public static final KEYCODE_MEDIA_REWIND:I = 0x59

.field public static final KEYCODE_MEDIA_STOP:I = 0x56

.field public static final KEYCODE_MENU:I = 0x52

.field public static final KEYCODE_META_LEFT:I = 0x75

.field public static final KEYCODE_META_RIGHT:I = 0x76

.field public static final KEYCODE_MINUS:I = 0x45

.field public static final KEYCODE_MOVE_END:I = 0x7b

.field public static final KEYCODE_MOVE_HOME:I = 0x7a

.field public static final KEYCODE_MUHENKAN:I = 0xd5

.field public static final KEYCODE_MUSIC:I = 0xd1

.field public static final KEYCODE_MUTE:I = 0x5b

.field public static final KEYCODE_N:I = 0x2a

.field public static final KEYCODE_NOTIFICATION:I = 0x53

.field public static final KEYCODE_NUM:I = 0x4e

.field public static final KEYCODE_NUMPAD_0:I = 0x90

.field public static final KEYCODE_NUMPAD_1:I = 0x91

.field public static final KEYCODE_NUMPAD_2:I = 0x92

.field public static final KEYCODE_NUMPAD_3:I = 0x93

.field public static final KEYCODE_NUMPAD_4:I = 0x94

.field public static final KEYCODE_NUMPAD_5:I = 0x95

.field public static final KEYCODE_NUMPAD_6:I = 0x96

.field public static final KEYCODE_NUMPAD_7:I = 0x97

.field public static final KEYCODE_NUMPAD_8:I = 0x98

.field public static final KEYCODE_NUMPAD_9:I = 0x99

.field public static final KEYCODE_NUMPAD_ADD:I = 0x9d

.field public static final KEYCODE_NUMPAD_COMMA:I = 0x9f

.field public static final KEYCODE_NUMPAD_DIVIDE:I = 0x9a

.field public static final KEYCODE_NUMPAD_DOT:I = 0x9e

.field public static final KEYCODE_NUMPAD_ENTER:I = 0xa0

.field public static final KEYCODE_NUMPAD_EQUALS:I = 0xa1

.field public static final KEYCODE_NUMPAD_LEFT_PAREN:I = 0xa2

.field public static final KEYCODE_NUMPAD_MULTIPLY:I = 0x9b

.field public static final KEYCODE_NUMPAD_RIGHT_PAREN:I = 0xa3

.field public static final KEYCODE_NUMPAD_SUBTRACT:I = 0x9c

.field public static final KEYCODE_NUM_LOCK:I = 0x8f

.field public static final KEYCODE_O:I = 0x2b

.field public static final KEYCODE_P:I = 0x2c

.field public static final KEYCODE_PAGE_DOWN:I = 0x5d

.field public static final KEYCODE_PAGE_UP:I = 0x5c

.field public static final KEYCODE_PERIOD:I = 0x38

.field public static final KEYCODE_PICTSYMBOLS:I = 0x5e

.field public static final KEYCODE_PLUS:I = 0x51

.field public static final KEYCODE_POUND:I = 0x12

.field public static final KEYCODE_POWER:I = 0x1a

.field public static final KEYCODE_PROG_BLUE:I = 0xba

.field public static final KEYCODE_PROG_GREEN:I = 0xb8

.field public static final KEYCODE_PROG_RED:I = 0xb7

.field public static final KEYCODE_PROG_YELLOW:I = 0xb9

.field public static final KEYCODE_PS_CALL:I = 0xe4

.field public static final KEYCODE_PS_CALL_END:I = 0xe6

.field public static final KEYCODE_PS_CALL_RECEIVE:I = 0xe5

.field public static final KEYCODE_PTNCLR:I = 0xdc

.field public static final KEYCODE_Q:I = 0x2d

.field public static final KEYCODE_R:I = 0x2e

.field public static final KEYCODE_RIGHT_BRACKET:I = 0x48

.field public static final KEYCODE_RO:I = 0xd9

.field public static final KEYCODE_S:I = 0x2f

.field public static final KEYCODE_SCROLL_LOCK:I = 0x74

.field public static final KEYCODE_SEARCH:I = 0x54

.field public static final KEYCODE_SEMICOLON:I = 0x4a

.field public static final KEYCODE_SETTINGS:I = 0xb0

.field public static final KEYCODE_SHIFT_LEFT:I = 0x3b

.field public static final KEYCODE_SHIFT_RIGHT:I = 0x3c

.field public static final KEYCODE_SLASH:I = 0x4c

.field public static final KEYCODE_SOFT_LEFT:I = 0x1

.field public static final KEYCODE_SOFT_RIGHT:I = 0x2

.field public static final KEYCODE_SPACE:I = 0x3e

.field public static final KEYCODE_STAR:I = 0x11

.field public static final KEYCODE_STB_INPUT:I = 0xb4

.field public static final KEYCODE_STB_POWER:I = 0xb3

.field public static final KEYCODE_SWITCH_CHARSET:I = 0x5f

.field public static final KEYCODE_SYM:I = 0x3f

.field public static final KEYCODE_SYMBOL:I = 0xdd

.field private static final KEYCODE_SYMBOLIC_NAMES:Landroid/util/SparseArray; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEYCODE_SYSRQ:I = 0x78

.field public static final KEYCODE_T:I = 0x30

.field public static final KEYCODE_TAB:I = 0x3d

.field public static final KEYCODE_TV:I = 0xaa

.field public static final KEYCODE_TV_INPUT:I = 0xb2

.field public static final KEYCODE_TV_POWER:I = 0xb1

.field public static final KEYCODE_U:I = 0x31

.field public static final KEYCODE_UNKNOWN:I = 0x0

.field public static final KEYCODE_V:I = 0x32

.field public static final KEYCODE_VIDEO_CALL:I = 0xde

.field public static final KEYCODE_VIDEO_CALL_END:I = 0xe0

.field public static final KEYCODE_VIDEO_CALL_RECEIVE:I = 0xdf

.field public static final KEYCODE_VOLUME_DOWN:I = 0x19

.field public static final KEYCODE_VOLUME_MUTE:I = 0xa4

.field public static final KEYCODE_VOLUME_UP:I = 0x18

.field public static final KEYCODE_W:I = 0x33

.field public static final KEYCODE_WINDOW:I = 0xab

.field public static final KEYCODE_X:I = 0x34

.field public static final KEYCODE_Y:I = 0x35

.field public static final KEYCODE_YEN:I = 0xd8

.field public static final KEYCODE_Z:I = 0x36

.field public static final KEYCODE_ZENKAKU_HANKAKU:I = 0xd3

.field public static final KEYCODE_ZOOM_IN:I = 0xa8

.field public static final KEYCODE_ZOOM_OUT:I = 0xa9

.field private static final LAST_KEYCODE:I = 0xe8

.field public static final MAX_KEYCODE:I = 0x54
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MAX_RECYCLED:I = 0xa

.field private static final META_ALL_MASK:I = 0x7770ff

.field public static final META_ALT_LEFT_ON:I = 0x10

.field public static final META_ALT_LOCKED:I = 0x200

.field public static final META_ALT_MASK:I = 0x32

.field public static final META_ALT_ON:I = 0x2

.field public static final META_ALT_RIGHT_ON:I = 0x20

.field public static final META_CAPS_LOCK_ON:I = 0x100000

.field public static final META_CAP_LOCKED:I = 0x100

.field public static final META_CTRL_LEFT_ON:I = 0x2000

.field public static final META_CTRL_MASK:I = 0x7000

.field public static final META_CTRL_ON:I = 0x1000

.field public static final META_CTRL_RIGHT_ON:I = 0x4000

.field public static final META_FUNCTION_ON:I = 0x8

.field private static final META_INVALID_MODIFIER_MASK:I = 0x700f00

.field private static final META_LOCK_MASK:I = 0x700000

.field public static final META_META_LEFT_ON:I = 0x20000

.field public static final META_META_MASK:I = 0x70000

.field public static final META_META_ON:I = 0x10000

.field public static final META_META_RIGHT_ON:I = 0x40000

.field private static final META_MODIFIER_MASK:I = 0x770ff

.field public static final META_NUM_LOCK_ON:I = 0x200000

.field public static final META_SCROLL_LOCK_ON:I = 0x400000

.field public static final META_SELECTING:I = 0x800

.field public static final META_SHIFT_LEFT_ON:I = 0x40

.field public static final META_SHIFT_MASK:I = 0xc1

.field public static final META_SHIFT_ON:I = 0x1

.field public static final META_SHIFT_RIGHT_ON:I = 0x80

.field private static final META_SYMBOLIC_NAMES:[Ljava/lang/String; = null

.field public static final META_SYM_LOCKED:I = 0x400

.field public static final META_SYM_ON:I = 0x4

.field private static final META_SYNTHETIC_MASK:I = 0xf00

.field static final TAG:Ljava/lang/String; = "KeyEvent"

.field private static final gRecyclerLock:Ljava/lang/Object;

.field private static gRecyclerTop:Landroid/view/KeyEvent;

.field private static gRecyclerUsed:I


# instance fields
.field private mAction:I

.field private mCharacters:Ljava/lang/String;

.field private mDeviceId:I

.field private mDownTime:J

.field private mEventTime:J

.field private mFlags:I

.field private mKeyCode:I

.field private mMetaState:I

.field private mNext:Landroid/view/KeyEvent;

.field private mRepeatCount:I

.field private mScanCode:I

.field private mSource:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 731
    new-instance v0, Landroid/util/SparseArray;

    #@2
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@5
    sput-object v0, Landroid/view/KeyEvent;->KEYCODE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@7
    .line 971
    const/16 v0, 0x20

    #@9
    new-array v0, v0, [Ljava/lang/String;

    #@b
    const/4 v1, 0x0

    #@c
    const-string v2, "META_SHIFT_ON"

    #@e
    aput-object v2, v0, v1

    #@10
    const/4 v1, 0x1

    #@11
    const-string v2, "META_ALT_ON"

    #@13
    aput-object v2, v0, v1

    #@15
    const/4 v1, 0x2

    #@16
    const-string v2, "META_SYM_ON"

    #@18
    aput-object v2, v0, v1

    #@1a
    const/4 v1, 0x3

    #@1b
    const-string v2, "META_FUNCTION_ON"

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x4

    #@20
    const-string v2, "META_ALT_LEFT_ON"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x5

    #@25
    const-string v2, "META_ALT_RIGHT_ON"

    #@27
    aput-object v2, v0, v1

    #@29
    const/4 v1, 0x6

    #@2a
    const-string v2, "META_SHIFT_LEFT_ON"

    #@2c
    aput-object v2, v0, v1

    #@2e
    const/4 v1, 0x7

    #@2f
    const-string v2, "META_SHIFT_RIGHT_ON"

    #@31
    aput-object v2, v0, v1

    #@33
    const/16 v1, 0x8

    #@35
    const-string v2, "META_CAP_LOCKED"

    #@37
    aput-object v2, v0, v1

    #@39
    const/16 v1, 0x9

    #@3b
    const-string v2, "META_ALT_LOCKED"

    #@3d
    aput-object v2, v0, v1

    #@3f
    const/16 v1, 0xa

    #@41
    const-string v2, "META_SYM_LOCKED"

    #@43
    aput-object v2, v0, v1

    #@45
    const/16 v1, 0xb

    #@47
    const-string v2, "0x00000800"

    #@49
    aput-object v2, v0, v1

    #@4b
    const/16 v1, 0xc

    #@4d
    const-string v2, "META_CTRL_ON"

    #@4f
    aput-object v2, v0, v1

    #@51
    const/16 v1, 0xd

    #@53
    const-string v2, "META_CTRL_LEFT_ON"

    #@55
    aput-object v2, v0, v1

    #@57
    const/16 v1, 0xe

    #@59
    const-string v2, "META_CTRL_RIGHT_ON"

    #@5b
    aput-object v2, v0, v1

    #@5d
    const/16 v1, 0xf

    #@5f
    const-string v2, "0x00008000"

    #@61
    aput-object v2, v0, v1

    #@63
    const/16 v1, 0x10

    #@65
    const-string v2, "META_META_ON"

    #@67
    aput-object v2, v0, v1

    #@69
    const/16 v1, 0x11

    #@6b
    const-string v2, "META_META_LEFT_ON"

    #@6d
    aput-object v2, v0, v1

    #@6f
    const/16 v1, 0x12

    #@71
    const-string v2, "META_META_RIGHT_ON"

    #@73
    aput-object v2, v0, v1

    #@75
    const/16 v1, 0x13

    #@77
    const-string v2, "0x00080000"

    #@79
    aput-object v2, v0, v1

    #@7b
    const/16 v1, 0x14

    #@7d
    const-string v2, "META_CAPS_LOCK_ON"

    #@7f
    aput-object v2, v0, v1

    #@81
    const/16 v1, 0x15

    #@83
    const-string v2, "META_NUM_LOCK_ON"

    #@85
    aput-object v2, v0, v1

    #@87
    const/16 v1, 0x16

    #@89
    const-string v2, "META_SCROLL_LOCK_ON"

    #@8b
    aput-object v2, v0, v1

    #@8d
    const/16 v1, 0x17

    #@8f
    const-string v2, "0x00800000"

    #@91
    aput-object v2, v0, v1

    #@93
    const/16 v1, 0x18

    #@95
    const-string v2, "0x01000000"

    #@97
    aput-object v2, v0, v1

    #@99
    const/16 v1, 0x19

    #@9b
    const-string v2, "0x02000000"

    #@9d
    aput-object v2, v0, v1

    #@9f
    const/16 v1, 0x1a

    #@a1
    const-string v2, "0x04000000"

    #@a3
    aput-object v2, v0, v1

    #@a5
    const/16 v1, 0x1b

    #@a7
    const-string v2, "0x08000000"

    #@a9
    aput-object v2, v0, v1

    #@ab
    const/16 v1, 0x1c

    #@ad
    const-string v2, "0x10000000"

    #@af
    aput-object v2, v0, v1

    #@b1
    const/16 v1, 0x1d

    #@b3
    const-string v2, "0x20000000"

    #@b5
    aput-object v2, v0, v1

    #@b7
    const/16 v1, 0x1e

    #@b9
    const-string v2, "0x40000000"

    #@bb
    aput-object v2, v0, v1

    #@bd
    const/16 v1, 0x1f

    #@bf
    const-string v2, "0x80000000"

    #@c1
    aput-object v2, v0, v1

    #@c3
    sput-object v0, Landroid/view/KeyEvent;->META_SYMBOLIC_NAMES:[Ljava/lang/String;

    #@c5
    .line 1362
    new-instance v0, Ljava/lang/Object;

    #@c7
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@ca
    sput-object v0, Landroid/view/KeyEvent;->gRecyclerLock:Ljava/lang/Object;

    #@cc
    .line 1438
    invoke-static {}, Landroid/view/KeyEvent;->populateKeycodeSymbolicNames()V

    #@cf
    .line 2966
    new-instance v0, Landroid/view/KeyEvent$1;

    #@d1
    invoke-direct {v0}, Landroid/view/KeyEvent$1;-><init>()V

    #@d4
    sput-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d6
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1441
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1442
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "action"
    .parameter "code"

    #@0
    .prologue
    .line 1451
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1452
    iput p1, p0, Landroid/view/KeyEvent;->mAction:I

    #@5
    .line 1453
    iput p2, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@7
    .line 1454
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@a
    .line 1455
    const/4 v0, -0x1

    #@b
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@d
    .line 1456
    return-void
.end method

.method public constructor <init>(JJIII)V
    .registers 9
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "code"
    .parameter "repeat"

    #@0
    .prologue
    .line 1472
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1473
    iput-wide p1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    .line 1474
    iput-wide p3, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@7
    .line 1475
    iput p5, p0, Landroid/view/KeyEvent;->mAction:I

    #@9
    .line 1476
    iput p6, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@b
    .line 1477
    iput p7, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@d
    .line 1478
    const/4 v0, -0x1

    #@e
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@10
    .line 1479
    return-void
.end method

.method public constructor <init>(JJIIII)V
    .registers 10
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "code"
    .parameter "repeat"
    .parameter "metaState"

    #@0
    .prologue
    .line 1496
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1497
    iput-wide p1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    .line 1498
    iput-wide p3, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@7
    .line 1499
    iput p5, p0, Landroid/view/KeyEvent;->mAction:I

    #@9
    .line 1500
    iput p6, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@b
    .line 1501
    iput p7, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@d
    .line 1502
    iput p8, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@f
    .line 1503
    const/4 v0, -0x1

    #@10
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@12
    .line 1504
    return-void
.end method

.method public constructor <init>(JJIIIIII)V
    .registers 11
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "code"
    .parameter "repeat"
    .parameter "metaState"
    .parameter "deviceId"
    .parameter "scancode"

    #@0
    .prologue
    .line 1524
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1525
    iput-wide p1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    .line 1526
    iput-wide p3, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@7
    .line 1527
    iput p5, p0, Landroid/view/KeyEvent;->mAction:I

    #@9
    .line 1528
    iput p6, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@b
    .line 1529
    iput p7, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@d
    .line 1530
    iput p8, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@f
    .line 1531
    iput p9, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@11
    .line 1532
    iput p10, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@13
    .line 1533
    return-void
.end method

.method public constructor <init>(JJIIIIIII)V
    .registers 12
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "code"
    .parameter "repeat"
    .parameter "metaState"
    .parameter "deviceId"
    .parameter "scancode"
    .parameter "flags"

    #@0
    .prologue
    .line 1554
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1555
    iput-wide p1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    .line 1556
    iput-wide p3, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@7
    .line 1557
    iput p5, p0, Landroid/view/KeyEvent;->mAction:I

    #@9
    .line 1558
    iput p6, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@b
    .line 1559
    iput p7, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@d
    .line 1560
    iput p8, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@f
    .line 1561
    iput p9, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@11
    .line 1562
    iput p10, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@13
    .line 1563
    iput p11, p0, Landroid/view/KeyEvent;->mFlags:I

    #@15
    .line 1564
    return-void
.end method

.method public constructor <init>(JJIIIIIIII)V
    .registers 13
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "code"
    .parameter "repeat"
    .parameter "metaState"
    .parameter "deviceId"
    .parameter "scancode"
    .parameter "flags"
    .parameter "source"

    #@0
    .prologue
    .line 1586
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1587
    iput-wide p1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    .line 1588
    iput-wide p3, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@7
    .line 1589
    iput p5, p0, Landroid/view/KeyEvent;->mAction:I

    #@9
    .line 1590
    iput p6, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@b
    .line 1591
    iput p7, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@d
    .line 1592
    iput p8, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@f
    .line 1593
    iput p9, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@11
    .line 1594
    iput p10, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@13
    .line 1595
    iput p11, p0, Landroid/view/KeyEvent;->mFlags:I

    #@15
    .line 1596
    iput p12, p0, Landroid/view/KeyEvent;->mSource:I

    #@17
    .line 1597
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;II)V
    .registers 8
    .parameter "time"
    .parameter "characters"
    .parameter "deviceId"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1611
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@4
    .line 1612
    iput-wide p1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@6
    .line 1613
    iput-wide p1, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@8
    .line 1614
    iput-object p3, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@a
    .line 1615
    const/4 v0, 0x2

    #@b
    iput v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@d
    .line 1616
    iput v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@f
    .line 1617
    iput v1, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@11
    .line 1618
    iput p4, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@13
    .line 1619
    iput p5, p0, Landroid/view/KeyEvent;->mFlags:I

    #@15
    .line 1620
    const/16 v0, 0x101

    #@17
    iput v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@19
    .line 1621
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 2983
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 2984
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@9
    .line 2985
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@f
    .line 2986
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@15
    .line 2987
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@1b
    .line 2988
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@21
    .line 2989
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    iput v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@27
    .line 2990
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@2d
    .line 2991
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@33
    .line 2992
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@36
    move-result-wide v0

    #@37
    iput-wide v0, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@39
    .line 2993
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@3c
    move-result-wide v0

    #@3d
    iput-wide v0, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@3f
    .line 2995
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@41
    if-eqz v0, :cond_49

    #@43
    .line 2996
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@49
    .line 2999
    :cond_49
    return-void
.end method

.method public constructor <init>(Landroid/view/KeyEvent;)V
    .registers 4
    .parameter "origEvent"

    #@0
    .prologue
    .line 1626
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1627
    iget-wide v0, p1, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    iput-wide v0, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@7
    .line 1628
    iget-wide v0, p1, Landroid/view/KeyEvent;->mEventTime:J

    #@9
    iput-wide v0, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@b
    .line 1629
    iget v0, p1, Landroid/view/KeyEvent;->mAction:I

    #@d
    iput v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@f
    .line 1630
    iget v0, p1, Landroid/view/KeyEvent;->mKeyCode:I

    #@11
    iput v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@13
    .line 1631
    iget v0, p1, Landroid/view/KeyEvent;->mRepeatCount:I

    #@15
    iput v0, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@17
    .line 1632
    iget v0, p1, Landroid/view/KeyEvent;->mMetaState:I

    #@19
    iput v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@1b
    .line 1633
    iget v0, p1, Landroid/view/KeyEvent;->mDeviceId:I

    #@1d
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@1f
    .line 1634
    iget v0, p1, Landroid/view/KeyEvent;->mSource:I

    #@21
    iput v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@23
    .line 1635
    iget v0, p1, Landroid/view/KeyEvent;->mScanCode:I

    #@25
    iput v0, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@27
    .line 1636
    iget v0, p1, Landroid/view/KeyEvent;->mFlags:I

    #@29
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2b
    .line 1637
    iget-object v0, p1, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@2d
    iput-object v0, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@2f
    .line 1638
    return-void
.end method

.method private constructor <init>(Landroid/view/KeyEvent;I)V
    .registers 5
    .parameter "origEvent"
    .parameter "action"

    #@0
    .prologue
    .line 1798
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1799
    iget-wide v0, p1, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    iput-wide v0, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@7
    .line 1800
    iget-wide v0, p1, Landroid/view/KeyEvent;->mEventTime:J

    #@9
    iput-wide v0, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@b
    .line 1801
    iput p2, p0, Landroid/view/KeyEvent;->mAction:I

    #@d
    .line 1802
    iget v0, p1, Landroid/view/KeyEvent;->mKeyCode:I

    #@f
    iput v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@11
    .line 1803
    iget v0, p1, Landroid/view/KeyEvent;->mRepeatCount:I

    #@13
    iput v0, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@15
    .line 1804
    iget v0, p1, Landroid/view/KeyEvent;->mMetaState:I

    #@17
    iput v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@19
    .line 1805
    iget v0, p1, Landroid/view/KeyEvent;->mDeviceId:I

    #@1b
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@1d
    .line 1806
    iget v0, p1, Landroid/view/KeyEvent;->mSource:I

    #@1f
    iput v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@21
    .line 1807
    iget v0, p1, Landroid/view/KeyEvent;->mScanCode:I

    #@23
    iput v0, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@25
    .line 1808
    iget v0, p1, Landroid/view/KeyEvent;->mFlags:I

    #@27
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@29
    .line 1811
    return-void
.end method

.method public constructor <init>(Landroid/view/KeyEvent;JI)V
    .registers 7
    .parameter "origEvent"
    .parameter "eventTime"
    .parameter "newRepeat"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1652
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1653
    iget-wide v0, p1, Landroid/view/KeyEvent;->mDownTime:J

    #@5
    iput-wide v0, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@7
    .line 1654
    iput-wide p2, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@9
    .line 1655
    iget v0, p1, Landroid/view/KeyEvent;->mAction:I

    #@b
    iput v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@d
    .line 1656
    iget v0, p1, Landroid/view/KeyEvent;->mKeyCode:I

    #@f
    iput v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@11
    .line 1657
    iput p4, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@13
    .line 1658
    iget v0, p1, Landroid/view/KeyEvent;->mMetaState:I

    #@15
    iput v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@17
    .line 1659
    iget v0, p1, Landroid/view/KeyEvent;->mDeviceId:I

    #@19
    iput v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@1b
    .line 1660
    iget v0, p1, Landroid/view/KeyEvent;->mSource:I

    #@1d
    iput v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@1f
    .line 1661
    iget v0, p1, Landroid/view/KeyEvent;->mScanCode:I

    #@21
    iput v0, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@23
    .line 1662
    iget v0, p1, Landroid/view/KeyEvent;->mFlags:I

    #@25
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@27
    .line 1663
    iget-object v0, p1, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@29
    iput-object v0, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@2b
    .line 1664
    return-void
.end method

.method static synthetic access$076(Landroid/view/KeyEvent;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 83
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@5
    return v0
.end method

.method public static actionToString(I)Ljava/lang/String;
    .registers 2
    .parameter "action"

    #@0
    .prologue
    .line 2876
    packed-switch p0, :pswitch_data_12

    #@3
    .line 2884
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    .line 2878
    :pswitch_8
    const-string v0, "ACTION_DOWN"

    #@a
    goto :goto_7

    #@b
    .line 2880
    :pswitch_b
    const-string v0, "ACTION_UP"

    #@d
    goto :goto_7

    #@e
    .line 2882
    :pswitch_e
    const-string v0, "ACTION_MULTIPLE"

    #@10
    goto :goto_7

    #@11
    .line 2876
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method

.method public static changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;
    .registers 3
    .parameter "event"
    .parameter "action"

    #@0
    .prologue
    .line 1821
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/view/KeyEvent;-><init>(Landroid/view/KeyEvent;I)V

    #@5
    return-object v0
.end method

.method public static changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;
    .registers 3
    .parameter "event"
    .parameter "flags"

    #@0
    .prologue
    .line 1832
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    invoke-direct {v0, p0}, Landroid/view/KeyEvent;-><init>(Landroid/view/KeyEvent;)V

    #@5
    .line 1833
    .end local p0
    .local v0, event:Landroid/view/KeyEvent;
    iput p1, v0, Landroid/view/KeyEvent;->mFlags:I

    #@7
    .line 1834
    return-object v0
.end method

.method public static changeTimeRepeat(Landroid/view/KeyEvent;JI)Landroid/view/KeyEvent;
    .registers 5
    .parameter "event"
    .parameter "eventTime"
    .parameter "newRepeat"

    #@0
    .prologue
    .line 1769
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/view/KeyEvent;-><init>(Landroid/view/KeyEvent;JI)V

    #@5
    return-object v0
.end method

.method public static changeTimeRepeat(Landroid/view/KeyEvent;JII)Landroid/view/KeyEvent;
    .registers 6
    .parameter "event"
    .parameter "eventTime"
    .parameter "newRepeat"
    .parameter "newFlags"

    #@0
    .prologue
    .line 1785
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    invoke-direct {v0, p0}, Landroid/view/KeyEvent;-><init>(Landroid/view/KeyEvent;)V

    #@5
    .line 1786
    .local v0, ret:Landroid/view/KeyEvent;
    iput-wide p1, v0, Landroid/view/KeyEvent;->mEventTime:J

    #@7
    .line 1787
    iput p3, v0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@9
    .line 1788
    iput p4, v0, Landroid/view/KeyEvent;->mFlags:I

    #@b
    .line 1789
    return-object v0
.end method

.method public static createFromParcelBody(Landroid/os/Parcel;)Landroid/view/KeyEvent;
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 2980
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    invoke-direct {v0, p0}, Landroid/view/KeyEvent;-><init>(Landroid/os/Parcel;)V

    #@5
    return-object v0
.end method

.method public static getDeadChar(II)I
    .registers 3
    .parameter "accent"
    .parameter "c"

    #@0
    .prologue
    .line 1355
    invoke-static {p0, p1}, Landroid/view/KeyCharacterMap;->getDeadChar(II)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getMaxKeyCode()I
    .registers 1

    #@0
    .prologue
    .line 1346
    const/16 v0, 0xe8

    #@2
    return v0
.end method

.method public static getModifierMetaStateMask()I
    .registers 1

    #@0
    .prologue
    .line 2047
    const v0, 0x770ff

    #@3
    return v0
.end method

.method public static final isGamepadButton(I)Z
    .registers 2
    .parameter "keyCode"

    #@0
    .prologue
    .line 1882
    sparse-switch p0, :sswitch_data_8

    #@3
    .line 1916
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 1914
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 1882
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x60 -> :sswitch_5
        0x61 -> :sswitch_5
        0x62 -> :sswitch_5
        0x63 -> :sswitch_5
        0x64 -> :sswitch_5
        0x65 -> :sswitch_5
        0x66 -> :sswitch_5
        0x67 -> :sswitch_5
        0x68 -> :sswitch_5
        0x69 -> :sswitch_5
        0x6a -> :sswitch_5
        0x6b -> :sswitch_5
        0x6c -> :sswitch_5
        0x6d -> :sswitch_5
        0x6e -> :sswitch_5
        0xbc -> :sswitch_5
        0xbd -> :sswitch_5
        0xbe -> :sswitch_5
        0xbf -> :sswitch_5
        0xc0 -> :sswitch_5
        0xc1 -> :sswitch_5
        0xc2 -> :sswitch_5
        0xc3 -> :sswitch_5
        0xc4 -> :sswitch_5
        0xc5 -> :sswitch_5
        0xc6 -> :sswitch_5
        0xc7 -> :sswitch_5
        0xc8 -> :sswitch_5
        0xc9 -> :sswitch_5
        0xca -> :sswitch_5
        0xcb -> :sswitch_5
    .end sparse-switch
.end method

.method public static isModifierKey(I)Z
    .registers 2
    .parameter "keyCode"

    #@0
    .prologue
    .line 2067
    sparse-switch p0, :sswitch_data_8

    #@3
    .line 2081
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 2079
    :sswitch_5
    const/4 v0, 0x1

    #@6
    goto :goto_4

    #@7
    .line 2067
    nop

    #@8
    :sswitch_data_8
    .sparse-switch
        0x39 -> :sswitch_5
        0x3a -> :sswitch_5
        0x3b -> :sswitch_5
        0x3c -> :sswitch_5
        0x3f -> :sswitch_5
        0x4e -> :sswitch_5
        0x71 -> :sswitch_5
        0x72 -> :sswitch_5
        0x75 -> :sswitch_5
        0x76 -> :sswitch_5
        0x77 -> :sswitch_5
    .end sparse-switch
.end method

.method public static keyCodeFromString(Ljava/lang/String;)I
    .registers 6
    .parameter "symbolicName"

    #@0
    .prologue
    .line 2912
    if-nez p0, :cond_b

    #@2
    .line 2913
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v4, "symbolicName must not be null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 2916
    :cond_b
    sget-object v3, Landroid/view/KeyEvent;->KEYCODE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@10
    move-result v0

    #@11
    .line 2917
    .local v0, count:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_24

    #@14
    .line 2918
    sget-object v3, Landroid/view/KeyEvent;->KEYCODE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_21

    #@20
    .line 2926
    .end local v2           #i:I
    :goto_20
    return v2

    #@21
    .line 2917
    .restart local v2       #i:I
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_12

    #@24
    .line 2924
    :cond_24
    const/16 v3, 0xa

    #@26
    :try_start_26
    invoke-static {p0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_29
    .catch Ljava/lang/NumberFormatException; {:try_start_26 .. :try_end_29} :catch_2b

    #@29
    move-result v2

    #@2a
    goto :goto_20

    #@2b
    .line 2925
    :catch_2b
    move-exception v1

    #@2c
    .line 2926
    .local v1, ex:Ljava/lang/NumberFormatException;
    const/4 v2, 0x0

    #@2d
    goto :goto_20
.end method

.method public static keyCodeToString(I)Ljava/lang/String;
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 2899
    sget-object v1, Landroid/view/KeyEvent;->KEYCODE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    .line 2900
    .local v0, symbolicName:Ljava/lang/String;
    if-eqz v0, :cond_b

    #@a
    .end local v0           #symbolicName:Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #symbolicName:Ljava/lang/String;
    :cond_b
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method private static metaStateFilterDirectionalModifiers(IIIII)I
    .registers 11
    .parameter "metaState"
    .parameter "modifiers"
    .parameter "basic"
    .parameter "left"
    .parameter "right"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2207
    and-int v4, p1, p2

    #@4
    if-eqz v4, :cond_4b

    #@6
    move v1, v2

    #@7
    .line 2208
    .local v1, wantBasic:Z
    :goto_7
    or-int v0, p3, p4

    #@9
    .line 2209
    .local v0, directional:I
    and-int v4, p1, v0

    #@b
    if-eqz v4, :cond_4d

    #@d
    .line 2211
    .local v2, wantLeftOrRight:Z
    :goto_d
    if-eqz v1, :cond_53

    #@f
    .line 2212
    if-eqz v2, :cond_4f

    #@11
    .line 2213
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string/jumbo v5, "modifiers must not contain "

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {p2}, Landroid/view/KeyEvent;->metaStateToString(I)Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " combined with "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {p3}, Landroid/view/KeyEvent;->metaStateToString(I)Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " or "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {p4}, Landroid/view/KeyEvent;->metaStateToString(I)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v3

    #@4b
    .end local v0           #directional:I
    .end local v1           #wantBasic:Z
    .end local v2           #wantLeftOrRight:Z
    :cond_4b
    move v1, v3

    #@4c
    .line 2207
    goto :goto_7

    #@4d
    .restart local v0       #directional:I
    .restart local v1       #wantBasic:Z
    :cond_4d
    move v2, v3

    #@4e
    .line 2209
    goto :goto_d

    #@4f
    .line 2217
    .restart local v2       #wantLeftOrRight:Z
    :cond_4f
    xor-int/lit8 v3, v0, -0x1

    #@51
    and-int/2addr p0, v3

    #@52
    .line 2221
    .end local p0
    :cond_52
    :goto_52
    return p0

    #@53
    .line 2218
    .restart local p0
    :cond_53
    if-eqz v2, :cond_52

    #@55
    .line 2219
    xor-int/lit8 v3, p2, -0x1

    #@57
    and-int/2addr p0, v3

    #@58
    goto :goto_52
.end method

.method public static metaStateHasModifiers(II)Z
    .registers 6
    .parameter "metaState"
    .parameter "modifiers"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2186
    const v1, 0x700f00

    #@4
    and-int/2addr v1, p1

    #@5
    if-eqz v1, :cond_10

    #@7
    .line 2187
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v1, "modifiers must not contain META_CAPS_LOCK_ON, META_NUM_LOCK_ON, META_SCROLL_LOCK_ON, META_CAP_LOCKED, META_ALT_LOCKED, META_SYM_LOCKED, or META_SELECTING"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 2193
    :cond_10
    invoke-static {p0}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@13
    move-result v1

    #@14
    const v2, 0x770ff

    #@17
    and-int p0, v1, v2

    #@19
    .line 2194
    const/16 v1, 0x40

    #@1b
    const/16 v2, 0x80

    #@1d
    invoke-static {p0, p1, v0, v1, v2}, Landroid/view/KeyEvent;->metaStateFilterDirectionalModifiers(IIIII)I

    #@20
    move-result p0

    #@21
    .line 2196
    const/4 v1, 0x2

    #@22
    const/16 v2, 0x10

    #@24
    const/16 v3, 0x20

    #@26
    invoke-static {p0, p1, v1, v2, v3}, Landroid/view/KeyEvent;->metaStateFilterDirectionalModifiers(IIIII)I

    #@29
    move-result p0

    #@2a
    .line 2198
    const/16 v1, 0x1000

    #@2c
    const/16 v2, 0x2000

    #@2e
    const/16 v3, 0x4000

    #@30
    invoke-static {p0, p1, v1, v2, v3}, Landroid/view/KeyEvent;->metaStateFilterDirectionalModifiers(IIIII)I

    #@33
    move-result p0

    #@34
    .line 2200
    const/high16 v1, 0x1

    #@36
    const/high16 v2, 0x2

    #@38
    const/high16 v3, 0x4

    #@3a
    invoke-static {p0, p1, v1, v2, v3}, Landroid/view/KeyEvent;->metaStateFilterDirectionalModifiers(IIIII)I

    #@3d
    move-result p0

    #@3e
    .line 2202
    if-ne p0, p1, :cond_41

    #@40
    :goto_40
    return v0

    #@41
    :cond_41
    const/4 v0, 0x0

    #@42
    goto :goto_40
.end method

.method public static metaStateHasNoModifiers(I)Z
    .registers 3
    .parameter "metaState"

    #@0
    .prologue
    .line 2150
    invoke-static {p0}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@3
    move-result v0

    #@4
    const v1, 0x770ff

    #@7
    and-int/2addr v0, v1

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static metaStateToString(I)Ljava/lang/String;
    .registers 6
    .parameter "metaState"

    #@0
    .prologue
    .line 2941
    if-nez p0, :cond_5

    #@2
    .line 2942
    const-string v2, "0"

    #@4
    .line 2963
    :cond_4
    :goto_4
    return-object v2

    #@5
    .line 2944
    :cond_5
    const/4 v3, 0x0

    #@6
    .line 2945
    .local v3, result:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@7
    .line 2946
    .local v0, i:I
    :goto_7
    if-eqz p0, :cond_2d

    #@9
    .line 2947
    and-int/lit8 v4, p0, 0x1

    #@b
    if-eqz v4, :cond_22

    #@d
    const/4 v1, 0x1

    #@e
    .line 2948
    .local v1, isSet:Z
    :goto_e
    ushr-int/lit8 p0, p0, 0x1

    #@10
    .line 2949
    if-eqz v1, :cond_1f

    #@12
    .line 2950
    sget-object v4, Landroid/view/KeyEvent;->META_SYMBOLIC_NAMES:[Ljava/lang/String;

    #@14
    aget-object v2, v4, v0

    #@16
    .line 2951
    .local v2, name:Ljava/lang/String;
    if-nez v3, :cond_24

    #@18
    .line 2952
    if-eqz p0, :cond_4

    #@1a
    .line 2955
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    .end local v3           #result:Ljava/lang/StringBuilder;
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@1f
    .line 2961
    .end local v2           #name:Ljava/lang/String;
    .restart local v3       #result:Ljava/lang/StringBuilder;
    :cond_1f
    :goto_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    .line 2962
    goto :goto_7

    #@22
    .line 2947
    .end local v1           #isSet:Z
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_e

    #@24
    .line 2957
    .restart local v1       #isSet:Z
    .restart local v2       #name:Ljava/lang/String;
    :cond_24
    const/16 v4, 0x7c

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    .line 2958
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    goto :goto_1f

    #@2d
    .line 2963
    .end local v1           #isSet:Z
    .end local v2           #name:Ljava/lang/String;
    :cond_2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    goto :goto_4
.end method

.method private native native_hasDefaultAction(I)Z
.end method

.method private native native_isSystemKey(I)Z
.end method

.method public static normalizeMetaState(I)I
    .registers 2
    .parameter "metaState"

    #@0
    .prologue
    .line 2110
    and-int/lit16 v0, p0, 0xc0

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 2111
    or-int/lit8 p0, p0, 0x1

    #@6
    .line 2113
    :cond_6
    and-int/lit8 v0, p0, 0x30

    #@8
    if-eqz v0, :cond_c

    #@a
    .line 2114
    or-int/lit8 p0, p0, 0x2

    #@c
    .line 2116
    :cond_c
    and-int/lit16 v0, p0, 0x6000

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 2117
    or-int/lit16 p0, p0, 0x1000

    #@12
    .line 2119
    :cond_12
    const/high16 v0, 0x6

    #@14
    and-int/2addr v0, p0

    #@15
    if-eqz v0, :cond_1a

    #@17
    .line 2120
    const/high16 v0, 0x1

    #@19
    or-int/2addr p0, v0

    #@1a
    .line 2122
    :cond_1a
    and-int/lit16 v0, p0, 0x100

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 2123
    const/high16 v0, 0x10

    #@20
    or-int/2addr p0, v0

    #@21
    .line 2125
    :cond_21
    and-int/lit16 v0, p0, 0x200

    #@23
    if-eqz v0, :cond_27

    #@25
    .line 2126
    or-int/lit8 p0, p0, 0x2

    #@27
    .line 2128
    :cond_27
    and-int/lit16 v0, p0, 0x400

    #@29
    if-eqz v0, :cond_2d

    #@2b
    .line 2129
    or-int/lit8 p0, p0, 0x4

    #@2d
    .line 2131
    :cond_2d
    const v0, 0x7770ff

    #@30
    and-int/2addr v0, p0

    #@31
    return v0
.end method

.method private static obtain()Landroid/view/KeyEvent;
    .registers 3

    #@0
    .prologue
    .line 1668
    sget-object v2, Landroid/view/KeyEvent;->gRecyclerLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1669
    :try_start_3
    sget-object v0, Landroid/view/KeyEvent;->gRecyclerTop:Landroid/view/KeyEvent;

    #@5
    .line 1670
    .local v0, ev:Landroid/view/KeyEvent;
    if-nez v0, :cond_e

    #@7
    .line 1671
    new-instance v0, Landroid/view/KeyEvent;

    #@9
    .end local v0           #ev:Landroid/view/KeyEvent;
    invoke-direct {v0}, Landroid/view/KeyEvent;-><init>()V

    #@c
    monitor-exit v2

    #@d
    .line 1678
    .restart local v0       #ev:Landroid/view/KeyEvent;
    :goto_d
    return-object v0

    #@e
    .line 1673
    :cond_e
    iget-object v1, v0, Landroid/view/KeyEvent;->mNext:Landroid/view/KeyEvent;

    #@10
    sput-object v1, Landroid/view/KeyEvent;->gRecyclerTop:Landroid/view/KeyEvent;

    #@12
    .line 1674
    sget v1, Landroid/view/KeyEvent;->gRecyclerUsed:I

    #@14
    add-int/lit8 v1, v1, -0x1

    #@16
    sput v1, Landroid/view/KeyEvent;->gRecyclerUsed:I

    #@18
    .line 1675
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_20

    #@19
    .line 1676
    const/4 v1, 0x0

    #@1a
    iput-object v1, v0, Landroid/view/KeyEvent;->mNext:Landroid/view/KeyEvent;

    #@1c
    .line 1677
    invoke-virtual {v0}, Landroid/view/KeyEvent;->prepareForReuse()V

    #@1f
    goto :goto_d

    #@20
    .line 1675
    :catchall_20
    move-exception v1

    #@21
    :try_start_21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method

.method public static obtain(JJIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;
    .registers 14
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "code"
    .parameter "repeat"
    .parameter "metaState"
    .parameter "deviceId"
    .parameter "scancode"
    .parameter "flags"
    .parameter "source"
    .parameter "characters"

    #@0
    .prologue
    .line 1689
    invoke-static {}, Landroid/view/KeyEvent;->obtain()Landroid/view/KeyEvent;

    #@3
    move-result-object v0

    #@4
    .line 1690
    .local v0, ev:Landroid/view/KeyEvent;
    iput-wide p0, v0, Landroid/view/KeyEvent;->mDownTime:J

    #@6
    .line 1691
    iput-wide p2, v0, Landroid/view/KeyEvent;->mEventTime:J

    #@8
    .line 1692
    iput p4, v0, Landroid/view/KeyEvent;->mAction:I

    #@a
    .line 1693
    iput p5, v0, Landroid/view/KeyEvent;->mKeyCode:I

    #@c
    .line 1694
    iput p6, v0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@e
    .line 1695
    iput p7, v0, Landroid/view/KeyEvent;->mMetaState:I

    #@10
    .line 1696
    iput p8, v0, Landroid/view/KeyEvent;->mDeviceId:I

    #@12
    .line 1697
    iput p9, v0, Landroid/view/KeyEvent;->mScanCode:I

    #@14
    .line 1698
    iput p10, v0, Landroid/view/KeyEvent;->mFlags:I

    #@16
    .line 1699
    iput p11, v0, Landroid/view/KeyEvent;->mSource:I

    #@18
    .line 1700
    iput-object p12, v0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@1a
    .line 1701
    return-object v0
.end method

.method public static obtain(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 1710
    invoke-static {}, Landroid/view/KeyEvent;->obtain()Landroid/view/KeyEvent;

    #@3
    move-result-object v0

    #@4
    .line 1711
    .local v0, ev:Landroid/view/KeyEvent;
    iget-wide v1, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@6
    iput-wide v1, v0, Landroid/view/KeyEvent;->mDownTime:J

    #@8
    .line 1712
    iget-wide v1, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@a
    iput-wide v1, v0, Landroid/view/KeyEvent;->mEventTime:J

    #@c
    .line 1713
    iget v1, p0, Landroid/view/KeyEvent;->mAction:I

    #@e
    iput v1, v0, Landroid/view/KeyEvent;->mAction:I

    #@10
    .line 1714
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@12
    iput v1, v0, Landroid/view/KeyEvent;->mKeyCode:I

    #@14
    .line 1715
    iget v1, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@16
    iput v1, v0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@18
    .line 1716
    iget v1, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@1a
    iput v1, v0, Landroid/view/KeyEvent;->mMetaState:I

    #@1c
    .line 1717
    iget v1, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@1e
    iput v1, v0, Landroid/view/KeyEvent;->mDeviceId:I

    #@20
    .line 1718
    iget v1, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@22
    iput v1, v0, Landroid/view/KeyEvent;->mScanCode:I

    #@24
    .line 1719
    iget v1, p0, Landroid/view/KeyEvent;->mFlags:I

    #@26
    iput v1, v0, Landroid/view/KeyEvent;->mFlags:I

    #@28
    .line 1720
    iget v1, p0, Landroid/view/KeyEvent;->mSource:I

    #@2a
    iput v1, v0, Landroid/view/KeyEvent;->mSource:I

    #@2c
    .line 1721
    iget-object v1, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@2e
    iput-object v1, v0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@30
    .line 1722
    return-object v0
.end method

.method private static populateKeycodeSymbolicNames()V
    .registers 3

    #@0
    .prologue
    .line 733
    sget-object v0, Landroid/view/KeyEvent;->KEYCODE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@2
    .line 734
    .local v0, names:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@3
    const-string v2, "KEYCODE_UNKNOWN"

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@8
    .line 735
    const/4 v1, 0x1

    #@9
    const-string v2, "KEYCODE_SOFT_LEFT"

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@e
    .line 736
    const/4 v1, 0x2

    #@f
    const-string v2, "KEYCODE_SOFT_RIGHT"

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@14
    .line 737
    const/4 v1, 0x3

    #@15
    const-string v2, "KEYCODE_HOME"

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1a
    .line 738
    const/4 v1, 0x4

    #@1b
    const-string v2, "KEYCODE_BACK"

    #@1d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@20
    .line 739
    const/4 v1, 0x5

    #@21
    const-string v2, "KEYCODE_CALL"

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@26
    .line 740
    const/4 v1, 0x6

    #@27
    const-string v2, "KEYCODE_ENDCALL"

    #@29
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2c
    .line 741
    const/4 v1, 0x7

    #@2d
    const-string v2, "KEYCODE_0"

    #@2f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@32
    .line 742
    const/16 v1, 0x8

    #@34
    const-string v2, "KEYCODE_1"

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@39
    .line 743
    const/16 v1, 0x9

    #@3b
    const-string v2, "KEYCODE_2"

    #@3d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@40
    .line 744
    const/16 v1, 0xa

    #@42
    const-string v2, "KEYCODE_3"

    #@44
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@47
    .line 745
    const/16 v1, 0xb

    #@49
    const-string v2, "KEYCODE_4"

    #@4b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4e
    .line 746
    const/16 v1, 0xc

    #@50
    const-string v2, "KEYCODE_5"

    #@52
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@55
    .line 747
    const/16 v1, 0xd

    #@57
    const-string v2, "KEYCODE_6"

    #@59
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5c
    .line 748
    const/16 v1, 0xe

    #@5e
    const-string v2, "KEYCODE_7"

    #@60
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@63
    .line 749
    const/16 v1, 0xf

    #@65
    const-string v2, "KEYCODE_8"

    #@67
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@6a
    .line 750
    const/16 v1, 0x10

    #@6c
    const-string v2, "KEYCODE_9"

    #@6e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@71
    .line 751
    const/16 v1, 0x11

    #@73
    const-string v2, "KEYCODE_STAR"

    #@75
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@78
    .line 752
    const/16 v1, 0x12

    #@7a
    const-string v2, "KEYCODE_POUND"

    #@7c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@7f
    .line 753
    const/16 v1, 0x13

    #@81
    const-string v2, "KEYCODE_DPAD_UP"

    #@83
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@86
    .line 754
    const/16 v1, 0x14

    #@88
    const-string v2, "KEYCODE_DPAD_DOWN"

    #@8a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@8d
    .line 755
    const/16 v1, 0x15

    #@8f
    const-string v2, "KEYCODE_DPAD_LEFT"

    #@91
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@94
    .line 756
    const/16 v1, 0x16

    #@96
    const-string v2, "KEYCODE_DPAD_RIGHT"

    #@98
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@9b
    .line 757
    const/16 v1, 0x17

    #@9d
    const-string v2, "KEYCODE_DPAD_CENTER"

    #@9f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@a2
    .line 758
    const/16 v1, 0x18

    #@a4
    const-string v2, "KEYCODE_VOLUME_UP"

    #@a6
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@a9
    .line 759
    const/16 v1, 0x19

    #@ab
    const-string v2, "KEYCODE_VOLUME_DOWN"

    #@ad
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@b0
    .line 760
    const/16 v1, 0x1a

    #@b2
    const-string v2, "KEYCODE_POWER"

    #@b4
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@b7
    .line 761
    const/16 v1, 0x1b

    #@b9
    const-string v2, "KEYCODE_CAMERA"

    #@bb
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@be
    .line 762
    const/16 v1, 0x1c

    #@c0
    const-string v2, "KEYCODE_CLEAR"

    #@c2
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@c5
    .line 763
    const/16 v1, 0x1d

    #@c7
    const-string v2, "KEYCODE_A"

    #@c9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@cc
    .line 764
    const/16 v1, 0x1e

    #@ce
    const-string v2, "KEYCODE_B"

    #@d0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@d3
    .line 765
    const/16 v1, 0x1f

    #@d5
    const-string v2, "KEYCODE_C"

    #@d7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@da
    .line 766
    const/16 v1, 0x20

    #@dc
    const-string v2, "KEYCODE_D"

    #@de
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@e1
    .line 767
    const/16 v1, 0x21

    #@e3
    const-string v2, "KEYCODE_E"

    #@e5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@e8
    .line 768
    const/16 v1, 0x22

    #@ea
    const-string v2, "KEYCODE_F"

    #@ec
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@ef
    .line 769
    const/16 v1, 0x23

    #@f1
    const-string v2, "KEYCODE_G"

    #@f3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@f6
    .line 770
    const/16 v1, 0x24

    #@f8
    const-string v2, "KEYCODE_H"

    #@fa
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@fd
    .line 771
    const/16 v1, 0x25

    #@ff
    const-string v2, "KEYCODE_I"

    #@101
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@104
    .line 772
    const/16 v1, 0x26

    #@106
    const-string v2, "KEYCODE_J"

    #@108
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@10b
    .line 773
    const/16 v1, 0x27

    #@10d
    const-string v2, "KEYCODE_K"

    #@10f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@112
    .line 774
    const/16 v1, 0x28

    #@114
    const-string v2, "KEYCODE_L"

    #@116
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@119
    .line 775
    const/16 v1, 0x29

    #@11b
    const-string v2, "KEYCODE_M"

    #@11d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@120
    .line 776
    const/16 v1, 0x2a

    #@122
    const-string v2, "KEYCODE_N"

    #@124
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@127
    .line 777
    const/16 v1, 0x2b

    #@129
    const-string v2, "KEYCODE_O"

    #@12b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@12e
    .line 778
    const/16 v1, 0x2c

    #@130
    const-string v2, "KEYCODE_P"

    #@132
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@135
    .line 779
    const/16 v1, 0x2d

    #@137
    const-string v2, "KEYCODE_Q"

    #@139
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@13c
    .line 780
    const/16 v1, 0x2e

    #@13e
    const-string v2, "KEYCODE_R"

    #@140
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@143
    .line 781
    const/16 v1, 0x2f

    #@145
    const-string v2, "KEYCODE_S"

    #@147
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@14a
    .line 782
    const/16 v1, 0x30

    #@14c
    const-string v2, "KEYCODE_T"

    #@14e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@151
    .line 783
    const/16 v1, 0x31

    #@153
    const-string v2, "KEYCODE_U"

    #@155
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@158
    .line 784
    const/16 v1, 0x32

    #@15a
    const-string v2, "KEYCODE_V"

    #@15c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@15f
    .line 785
    const/16 v1, 0x33

    #@161
    const-string v2, "KEYCODE_W"

    #@163
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@166
    .line 786
    const/16 v1, 0x34

    #@168
    const-string v2, "KEYCODE_X"

    #@16a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@16d
    .line 787
    const/16 v1, 0x35

    #@16f
    const-string v2, "KEYCODE_Y"

    #@171
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@174
    .line 788
    const/16 v1, 0x36

    #@176
    const-string v2, "KEYCODE_Z"

    #@178
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@17b
    .line 789
    const/16 v1, 0x37

    #@17d
    const-string v2, "KEYCODE_COMMA"

    #@17f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@182
    .line 790
    const/16 v1, 0x38

    #@184
    const-string v2, "KEYCODE_PERIOD"

    #@186
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@189
    .line 791
    const/16 v1, 0x39

    #@18b
    const-string v2, "KEYCODE_ALT_LEFT"

    #@18d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@190
    .line 792
    const/16 v1, 0x3a

    #@192
    const-string v2, "KEYCODE_ALT_RIGHT"

    #@194
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@197
    .line 793
    const/16 v1, 0x3b

    #@199
    const-string v2, "KEYCODE_SHIFT_LEFT"

    #@19b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@19e
    .line 794
    const/16 v1, 0x3c

    #@1a0
    const-string v2, "KEYCODE_SHIFT_RIGHT"

    #@1a2
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1a5
    .line 795
    const/16 v1, 0x3d

    #@1a7
    const-string v2, "KEYCODE_TAB"

    #@1a9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1ac
    .line 796
    const/16 v1, 0x3e

    #@1ae
    const-string v2, "KEYCODE_SPACE"

    #@1b0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1b3
    .line 797
    const/16 v1, 0x3f

    #@1b5
    const-string v2, "KEYCODE_SYM"

    #@1b7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1ba
    .line 798
    const/16 v1, 0x40

    #@1bc
    const-string v2, "KEYCODE_EXPLORER"

    #@1be
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1c1
    .line 799
    const/16 v1, 0x41

    #@1c3
    const-string v2, "KEYCODE_ENVELOPE"

    #@1c5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1c8
    .line 800
    const/16 v1, 0x42

    #@1ca
    const-string v2, "KEYCODE_ENTER"

    #@1cc
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1cf
    .line 801
    const/16 v1, 0x43

    #@1d1
    const-string v2, "KEYCODE_DEL"

    #@1d3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1d6
    .line 802
    const/16 v1, 0x44

    #@1d8
    const-string v2, "KEYCODE_GRAVE"

    #@1da
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1dd
    .line 803
    const/16 v1, 0x45

    #@1df
    const-string v2, "KEYCODE_MINUS"

    #@1e1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1e4
    .line 804
    const/16 v1, 0x46

    #@1e6
    const-string v2, "KEYCODE_EQUALS"

    #@1e8
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1eb
    .line 805
    const/16 v1, 0x47

    #@1ed
    const-string v2, "KEYCODE_LEFT_BRACKET"

    #@1ef
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1f2
    .line 806
    const/16 v1, 0x48

    #@1f4
    const-string v2, "KEYCODE_RIGHT_BRACKET"

    #@1f6
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1f9
    .line 807
    const/16 v1, 0x49

    #@1fb
    const-string v2, "KEYCODE_BACKSLASH"

    #@1fd
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@200
    .line 808
    const/16 v1, 0x4a

    #@202
    const-string v2, "KEYCODE_SEMICOLON"

    #@204
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@207
    .line 809
    const/16 v1, 0x4b

    #@209
    const-string v2, "KEYCODE_APOSTROPHE"

    #@20b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@20e
    .line 810
    const/16 v1, 0x4c

    #@210
    const-string v2, "KEYCODE_SLASH"

    #@212
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@215
    .line 811
    const/16 v1, 0x4d

    #@217
    const-string v2, "KEYCODE_AT"

    #@219
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@21c
    .line 812
    const/16 v1, 0x4e

    #@21e
    const-string v2, "KEYCODE_NUM"

    #@220
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@223
    .line 813
    const/16 v1, 0x4f

    #@225
    const-string v2, "KEYCODE_HEADSETHOOK"

    #@227
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@22a
    .line 814
    const/16 v1, 0x50

    #@22c
    const-string v2, "KEYCODE_FOCUS"

    #@22e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@231
    .line 815
    const/16 v1, 0x51

    #@233
    const-string v2, "KEYCODE_PLUS"

    #@235
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@238
    .line 816
    const/16 v1, 0x52

    #@23a
    const-string v2, "KEYCODE_MENU"

    #@23c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@23f
    .line 817
    const/16 v1, 0x53

    #@241
    const-string v2, "KEYCODE_NOTIFICATION"

    #@243
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@246
    .line 818
    const/16 v1, 0x54

    #@248
    const-string v2, "KEYCODE_SEARCH"

    #@24a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@24d
    .line 819
    const/16 v1, 0x55

    #@24f
    const-string v2, "KEYCODE_MEDIA_PLAY_PAUSE"

    #@251
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@254
    .line 820
    const/16 v1, 0x56

    #@256
    const-string v2, "KEYCODE_MEDIA_STOP"

    #@258
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@25b
    .line 821
    const/16 v1, 0x57

    #@25d
    const-string v2, "KEYCODE_MEDIA_NEXT"

    #@25f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@262
    .line 822
    const/16 v1, 0x58

    #@264
    const-string v2, "KEYCODE_MEDIA_PREVIOUS"

    #@266
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@269
    .line 823
    const/16 v1, 0x59

    #@26b
    const-string v2, "KEYCODE_MEDIA_REWIND"

    #@26d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@270
    .line 824
    const/16 v1, 0x5a

    #@272
    const-string v2, "KEYCODE_MEDIA_FAST_FORWARD"

    #@274
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@277
    .line 825
    const/16 v1, 0x5b

    #@279
    const-string v2, "KEYCODE_MUTE"

    #@27b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@27e
    .line 826
    const/16 v1, 0x5c

    #@280
    const-string v2, "KEYCODE_PAGE_UP"

    #@282
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@285
    .line 827
    const/16 v1, 0x5d

    #@287
    const-string v2, "KEYCODE_PAGE_DOWN"

    #@289
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@28c
    .line 828
    const/16 v1, 0x5e

    #@28e
    const-string v2, "KEYCODE_PICTSYMBOLS"

    #@290
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@293
    .line 829
    const/16 v1, 0x5f

    #@295
    const-string v2, "KEYCODE_SWITCH_CHARSET"

    #@297
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@29a
    .line 830
    const/16 v1, 0x60

    #@29c
    const-string v2, "KEYCODE_BUTTON_A"

    #@29e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2a1
    .line 831
    const/16 v1, 0x61

    #@2a3
    const-string v2, "KEYCODE_BUTTON_B"

    #@2a5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2a8
    .line 832
    const/16 v1, 0x62

    #@2aa
    const-string v2, "KEYCODE_BUTTON_C"

    #@2ac
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2af
    .line 833
    const/16 v1, 0x63

    #@2b1
    const-string v2, "KEYCODE_BUTTON_X"

    #@2b3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2b6
    .line 834
    const/16 v1, 0x64

    #@2b8
    const-string v2, "KEYCODE_BUTTON_Y"

    #@2ba
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2bd
    .line 835
    const/16 v1, 0x65

    #@2bf
    const-string v2, "KEYCODE_BUTTON_Z"

    #@2c1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2c4
    .line 836
    const/16 v1, 0x66

    #@2c6
    const-string v2, "KEYCODE_BUTTON_L1"

    #@2c8
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2cb
    .line 837
    const/16 v1, 0x67

    #@2cd
    const-string v2, "KEYCODE_BUTTON_R1"

    #@2cf
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2d2
    .line 838
    const/16 v1, 0x68

    #@2d4
    const-string v2, "KEYCODE_BUTTON_L2"

    #@2d6
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2d9
    .line 839
    const/16 v1, 0x69

    #@2db
    const-string v2, "KEYCODE_BUTTON_R2"

    #@2dd
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2e0
    .line 840
    const/16 v1, 0x6a

    #@2e2
    const-string v2, "KEYCODE_BUTTON_THUMBL"

    #@2e4
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2e7
    .line 841
    const/16 v1, 0x6b

    #@2e9
    const-string v2, "KEYCODE_BUTTON_THUMBR"

    #@2eb
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2ee
    .line 842
    const/16 v1, 0x6c

    #@2f0
    const-string v2, "KEYCODE_BUTTON_START"

    #@2f2
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2f5
    .line 843
    const/16 v1, 0x6d

    #@2f7
    const-string v2, "KEYCODE_BUTTON_SELECT"

    #@2f9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2fc
    .line 844
    const/16 v1, 0x6e

    #@2fe
    const-string v2, "KEYCODE_BUTTON_MODE"

    #@300
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@303
    .line 845
    const/16 v1, 0x6f

    #@305
    const-string v2, "KEYCODE_ESCAPE"

    #@307
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@30a
    .line 846
    const/16 v1, 0x70

    #@30c
    const-string v2, "KEYCODE_FORWARD_DEL"

    #@30e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@311
    .line 847
    const/16 v1, 0x71

    #@313
    const-string v2, "KEYCODE_CTRL_LEFT"

    #@315
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@318
    .line 848
    const/16 v1, 0x72

    #@31a
    const-string v2, "KEYCODE_CTRL_RIGHT"

    #@31c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@31f
    .line 849
    const/16 v1, 0x73

    #@321
    const-string v2, "KEYCODE_CAPS_LOCK"

    #@323
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@326
    .line 850
    const/16 v1, 0x74

    #@328
    const-string v2, "KEYCODE_SCROLL_LOCK"

    #@32a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@32d
    .line 851
    const/16 v1, 0x75

    #@32f
    const-string v2, "KEYCODE_META_LEFT"

    #@331
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@334
    .line 852
    const/16 v1, 0x76

    #@336
    const-string v2, "KEYCODE_META_RIGHT"

    #@338
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@33b
    .line 853
    const/16 v1, 0x77

    #@33d
    const-string v2, "KEYCODE_FUNCTION"

    #@33f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@342
    .line 854
    const/16 v1, 0x78

    #@344
    const-string v2, "KEYCODE_SYSRQ"

    #@346
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@349
    .line 855
    const/16 v1, 0x79

    #@34b
    const-string v2, "KEYCODE_BREAK"

    #@34d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@350
    .line 856
    const/16 v1, 0x7a

    #@352
    const-string v2, "KEYCODE_MOVE_HOME"

    #@354
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@357
    .line 857
    const/16 v1, 0x7b

    #@359
    const-string v2, "KEYCODE_MOVE_END"

    #@35b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@35e
    .line 858
    const/16 v1, 0x7c

    #@360
    const-string v2, "KEYCODE_INSERT"

    #@362
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@365
    .line 859
    const/16 v1, 0x7d

    #@367
    const-string v2, "KEYCODE_FORWARD"

    #@369
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@36c
    .line 860
    const/16 v1, 0x7e

    #@36e
    const-string v2, "KEYCODE_MEDIA_PLAY"

    #@370
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@373
    .line 861
    const/16 v1, 0x7f

    #@375
    const-string v2, "KEYCODE_MEDIA_PAUSE"

    #@377
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@37a
    .line 862
    const/16 v1, 0x80

    #@37c
    const-string v2, "KEYCODE_MEDIA_CLOSE"

    #@37e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@381
    .line 863
    const/16 v1, 0x81

    #@383
    const-string v2, "KEYCODE_MEDIA_EJECT"

    #@385
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@388
    .line 864
    const/16 v1, 0x82

    #@38a
    const-string v2, "KEYCODE_MEDIA_RECORD"

    #@38c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@38f
    .line 865
    const/16 v1, 0x83

    #@391
    const-string v2, "KEYCODE_F1"

    #@393
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@396
    .line 866
    const/16 v1, 0x84

    #@398
    const-string v2, "KEYCODE_F2"

    #@39a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@39d
    .line 867
    const/16 v1, 0x85

    #@39f
    const-string v2, "KEYCODE_F3"

    #@3a1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3a4
    .line 868
    const/16 v1, 0x86

    #@3a6
    const-string v2, "KEYCODE_F4"

    #@3a8
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3ab
    .line 869
    const/16 v1, 0x87

    #@3ad
    const-string v2, "KEYCODE_F5"

    #@3af
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3b2
    .line 870
    const/16 v1, 0x88

    #@3b4
    const-string v2, "KEYCODE_F6"

    #@3b6
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3b9
    .line 871
    const/16 v1, 0x89

    #@3bb
    const-string v2, "KEYCODE_F7"

    #@3bd
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3c0
    .line 872
    const/16 v1, 0x8a

    #@3c2
    const-string v2, "KEYCODE_F8"

    #@3c4
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3c7
    .line 873
    const/16 v1, 0x8b

    #@3c9
    const-string v2, "KEYCODE_F9"

    #@3cb
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3ce
    .line 874
    const/16 v1, 0x8c

    #@3d0
    const-string v2, "KEYCODE_F10"

    #@3d2
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3d5
    .line 875
    const/16 v1, 0x8d

    #@3d7
    const-string v2, "KEYCODE_F11"

    #@3d9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3dc
    .line 876
    const/16 v1, 0x8e

    #@3de
    const-string v2, "KEYCODE_F12"

    #@3e0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3e3
    .line 877
    const/16 v1, 0x8f

    #@3e5
    const-string v2, "KEYCODE_NUM_LOCK"

    #@3e7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3ea
    .line 878
    const/16 v1, 0x90

    #@3ec
    const-string v2, "KEYCODE_NUMPAD_0"

    #@3ee
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3f1
    .line 879
    const/16 v1, 0x91

    #@3f3
    const-string v2, "KEYCODE_NUMPAD_1"

    #@3f5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3f8
    .line 880
    const/16 v1, 0x92

    #@3fa
    const-string v2, "KEYCODE_NUMPAD_2"

    #@3fc
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3ff
    .line 881
    const/16 v1, 0x93

    #@401
    const-string v2, "KEYCODE_NUMPAD_3"

    #@403
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@406
    .line 882
    const/16 v1, 0x94

    #@408
    const-string v2, "KEYCODE_NUMPAD_4"

    #@40a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@40d
    .line 883
    const/16 v1, 0x95

    #@40f
    const-string v2, "KEYCODE_NUMPAD_5"

    #@411
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@414
    .line 884
    const/16 v1, 0x96

    #@416
    const-string v2, "KEYCODE_NUMPAD_6"

    #@418
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@41b
    .line 885
    const/16 v1, 0x97

    #@41d
    const-string v2, "KEYCODE_NUMPAD_7"

    #@41f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@422
    .line 886
    const/16 v1, 0x98

    #@424
    const-string v2, "KEYCODE_NUMPAD_8"

    #@426
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@429
    .line 887
    const/16 v1, 0x99

    #@42b
    const-string v2, "KEYCODE_NUMPAD_9"

    #@42d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@430
    .line 888
    const/16 v1, 0x9a

    #@432
    const-string v2, "KEYCODE_NUMPAD_DIVIDE"

    #@434
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@437
    .line 889
    const/16 v1, 0x9b

    #@439
    const-string v2, "KEYCODE_NUMPAD_MULTIPLY"

    #@43b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@43e
    .line 890
    const/16 v1, 0x9c

    #@440
    const-string v2, "KEYCODE_NUMPAD_SUBTRACT"

    #@442
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@445
    .line 891
    const/16 v1, 0x9d

    #@447
    const-string v2, "KEYCODE_NUMPAD_ADD"

    #@449
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@44c
    .line 892
    const/16 v1, 0x9e

    #@44e
    const-string v2, "KEYCODE_NUMPAD_DOT"

    #@450
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@453
    .line 893
    const/16 v1, 0x9f

    #@455
    const-string v2, "KEYCODE_NUMPAD_COMMA"

    #@457
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@45a
    .line 894
    const/16 v1, 0xa0

    #@45c
    const-string v2, "KEYCODE_NUMPAD_ENTER"

    #@45e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@461
    .line 895
    const/16 v1, 0xa1

    #@463
    const-string v2, "KEYCODE_NUMPAD_EQUALS"

    #@465
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@468
    .line 896
    const/16 v1, 0xa2

    #@46a
    const-string v2, "KEYCODE_NUMPAD_LEFT_PAREN"

    #@46c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@46f
    .line 897
    const/16 v1, 0xa3

    #@471
    const-string v2, "KEYCODE_NUMPAD_RIGHT_PAREN"

    #@473
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@476
    .line 898
    const/16 v1, 0xa4

    #@478
    const-string v2, "KEYCODE_VOLUME_MUTE"

    #@47a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@47d
    .line 899
    const/16 v1, 0xa5

    #@47f
    const-string v2, "KEYCODE_INFO"

    #@481
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@484
    .line 900
    const/16 v1, 0xa6

    #@486
    const-string v2, "KEYCODE_CHANNEL_UP"

    #@488
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@48b
    .line 901
    const/16 v1, 0xa7

    #@48d
    const-string v2, "KEYCODE_CHANNEL_DOWN"

    #@48f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@492
    .line 902
    const/16 v1, 0xa8

    #@494
    const-string v2, "KEYCODE_ZOOM_IN"

    #@496
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@499
    .line 903
    const/16 v1, 0xa9

    #@49b
    const-string v2, "KEYCODE_ZOOM_OUT"

    #@49d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4a0
    .line 904
    const/16 v1, 0xaa

    #@4a2
    const-string v2, "KEYCODE_TV"

    #@4a4
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4a7
    .line 905
    const/16 v1, 0xab

    #@4a9
    const-string v2, "KEYCODE_WINDOW"

    #@4ab
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4ae
    .line 906
    const/16 v1, 0xac

    #@4b0
    const-string v2, "KEYCODE_GUIDE"

    #@4b2
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4b5
    .line 907
    const/16 v1, 0xad

    #@4b7
    const-string v2, "KEYCODE_DVR"

    #@4b9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4bc
    .line 908
    const/16 v1, 0xae

    #@4be
    const-string v2, "KEYCODE_BOOKMARK"

    #@4c0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4c3
    .line 909
    const/16 v1, 0xaf

    #@4c5
    const-string v2, "KEYCODE_CAPTIONS"

    #@4c7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4ca
    .line 910
    const/16 v1, 0xb0

    #@4cc
    const-string v2, "KEYCODE_SETTINGS"

    #@4ce
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4d1
    .line 911
    const/16 v1, 0xb1

    #@4d3
    const-string v2, "KEYCODE_TV_POWER"

    #@4d5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4d8
    .line 912
    const/16 v1, 0xb2

    #@4da
    const-string v2, "KEYCODE_TV_INPUT"

    #@4dc
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4df
    .line 913
    const/16 v1, 0xb4

    #@4e1
    const-string v2, "KEYCODE_STB_INPUT"

    #@4e3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4e6
    .line 914
    const/16 v1, 0xb3

    #@4e8
    const-string v2, "KEYCODE_STB_POWER"

    #@4ea
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4ed
    .line 915
    const/16 v1, 0xb5

    #@4ef
    const-string v2, "KEYCODE_AVR_POWER"

    #@4f1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4f4
    .line 916
    const/16 v1, 0xb6

    #@4f6
    const-string v2, "KEYCODE_AVR_INPUT"

    #@4f8
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4fb
    .line 917
    const/16 v1, 0xb7

    #@4fd
    const-string v2, "KEYCODE_PROG_RED"

    #@4ff
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@502
    .line 918
    const/16 v1, 0xb8

    #@504
    const-string v2, "KEYCODE_PROG_GREEN"

    #@506
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@509
    .line 919
    const/16 v1, 0xb9

    #@50b
    const-string v2, "KEYCODE_PROG_YELLOW"

    #@50d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@510
    .line 920
    const/16 v1, 0xba

    #@512
    const-string v2, "KEYCODE_PROG_BLUE"

    #@514
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@517
    .line 921
    const/16 v1, 0xbb

    #@519
    const-string v2, "KEYCODE_APP_SWITCH"

    #@51b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@51e
    .line 922
    const/16 v1, 0xbc

    #@520
    const-string v2, "KEYCODE_BUTTON_1"

    #@522
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@525
    .line 923
    const/16 v1, 0xbd

    #@527
    const-string v2, "KEYCODE_BUTTON_2"

    #@529
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@52c
    .line 924
    const/16 v1, 0xbe

    #@52e
    const-string v2, "KEYCODE_BUTTON_3"

    #@530
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@533
    .line 925
    const/16 v1, 0xbf

    #@535
    const-string v2, "KEYCODE_BUTTON_4"

    #@537
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@53a
    .line 926
    const/16 v1, 0xc0

    #@53c
    const-string v2, "KEYCODE_BUTTON_5"

    #@53e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@541
    .line 927
    const/16 v1, 0xc1

    #@543
    const-string v2, "KEYCODE_BUTTON_6"

    #@545
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@548
    .line 928
    const/16 v1, 0xc2

    #@54a
    const-string v2, "KEYCODE_BUTTON_7"

    #@54c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@54f
    .line 929
    const/16 v1, 0xc3

    #@551
    const-string v2, "KEYCODE_BUTTON_8"

    #@553
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@556
    .line 930
    const/16 v1, 0xc4

    #@558
    const-string v2, "KEYCODE_BUTTON_9"

    #@55a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@55d
    .line 931
    const/16 v1, 0xc5

    #@55f
    const-string v2, "KEYCODE_BUTTON_10"

    #@561
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@564
    .line 932
    const/16 v1, 0xc6

    #@566
    const-string v2, "KEYCODE_BUTTON_11"

    #@568
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@56b
    .line 933
    const/16 v1, 0xc7

    #@56d
    const-string v2, "KEYCODE_BUTTON_12"

    #@56f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@572
    .line 934
    const/16 v1, 0xc8

    #@574
    const-string v2, "KEYCODE_BUTTON_13"

    #@576
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@579
    .line 935
    const/16 v1, 0xc9

    #@57b
    const-string v2, "KEYCODE_BUTTON_14"

    #@57d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@580
    .line 936
    const/16 v1, 0xca

    #@582
    const-string v2, "KEYCODE_BUTTON_15"

    #@584
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@587
    .line 937
    const/16 v1, 0xcb

    #@589
    const-string v2, "KEYCODE_BUTTON_16"

    #@58b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@58e
    .line 938
    const/16 v1, 0xcc

    #@590
    const-string v2, "KEYCODE_LANGUAGE_SWITCH"

    #@592
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@595
    .line 939
    const/16 v1, 0xcd

    #@597
    const-string v2, "KEYCODE_MANNER_MODE"

    #@599
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@59c
    .line 940
    const/16 v1, 0xce

    #@59e
    const-string v2, "KEYCODE_3D_MODE"

    #@5a0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5a3
    .line 941
    const/16 v1, 0xcf

    #@5a5
    const-string v2, "KEYCODE_CONTACTS"

    #@5a7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5aa
    .line 942
    const/16 v1, 0xd0

    #@5ac
    const-string v2, "KEYCODE_CALENDAR"

    #@5ae
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5b1
    .line 943
    const/16 v1, 0xd1

    #@5b3
    const-string v2, "KEYCODE_MUSIC"

    #@5b5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5b8
    .line 944
    const/16 v1, 0xd2

    #@5ba
    const-string v2, "KEYCODE_CALCULATOR"

    #@5bc
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5bf
    .line 945
    const/16 v1, 0xd3

    #@5c1
    const-string v2, "KEYCODE_ZENKAKU_HANKAKU"

    #@5c3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5c6
    .line 946
    const/16 v1, 0xd4

    #@5c8
    const-string v2, "KEYCODE_EISU"

    #@5ca
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5cd
    .line 947
    const/16 v1, 0xd5

    #@5cf
    const-string v2, "KEYCODE_MUHENKAN"

    #@5d1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5d4
    .line 948
    const/16 v1, 0xd6

    #@5d6
    const-string v2, "KEYCODE_HENKAN"

    #@5d8
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5db
    .line 949
    const/16 v1, 0xd7

    #@5dd
    const-string v2, "KEYCODE_KATAKANA_HIRAGANA"

    #@5df
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5e2
    .line 950
    const/16 v1, 0xd8

    #@5e4
    const-string v2, "KEYCODE_YEN"

    #@5e6
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5e9
    .line 951
    const/16 v1, 0xd9

    #@5eb
    const-string v2, "KEYCODE_RO"

    #@5ed
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5f0
    .line 952
    const/16 v1, 0xda

    #@5f2
    const-string v2, "KEYCODE_KANA"

    #@5f4
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5f7
    .line 953
    const/16 v1, 0xdb

    #@5f9
    const-string v2, "KEYCODE_ASSIST"

    #@5fb
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5fe
    .line 955
    const/16 v1, 0xdc

    #@600
    const-string v2, "KEYCODE_PTNCLR"

    #@602
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@605
    .line 956
    const/16 v1, 0xdd

    #@607
    const-string v2, "KEYCODE_SYMBOL"

    #@609
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@60c
    .line 957
    const/16 v1, 0xde

    #@60e
    const-string v2, "KEYCODE_VIDEO_CALL"

    #@610
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@613
    .line 958
    const/16 v1, 0xdf

    #@615
    const-string v2, "KEYCODE_VIDEO_CALL_RECEIVE"

    #@617
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@61a
    .line 959
    const/16 v1, 0xe0

    #@61c
    const-string v2, "KEYCODE_VIDEO_CALL_END"

    #@61e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@621
    .line 960
    const/16 v1, 0xe1

    #@623
    const-string v2, "KEYCODE_KEY_QUICKCLIP"

    #@625
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@628
    .line 961
    const/16 v1, 0xe2

    #@62a
    const-string v2, "KEYCODE_KEY_WAKE"

    #@62c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@62f
    .line 962
    const/16 v1, 0xe3

    #@631
    const-string v2, "KEYCODE_KEY_SLEEP"

    #@633
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@636
    .line 964
    const/16 v1, 0xe4

    #@638
    const-string v2, "KEYCODE_PS_CALL"

    #@63a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@63d
    .line 965
    const/16 v1, 0xe5

    #@63f
    const-string v2, "KEYCODE_PS_CALL_RECEIVE"

    #@641
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@644
    .line 966
    const/16 v1, 0xe6

    #@646
    const-string v2, "KEYCODE_PS_CALL_END"

    #@648
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@64b
    .line 967
    return-void
.end method


# virtual methods
.method public bridge synthetic copy()Landroid/view/InputEvent;
    .registers 2

    #@0
    .prologue
    .line 83
    invoke-virtual {p0}, Landroid/view/KeyEvent;->copy()Landroid/view/KeyEvent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public copy()Landroid/view/KeyEvent;
    .registers 2

    #@0
    .prologue
    .line 1728
    invoke-static {p0}, Landroid/view/KeyEvent;->obtain(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final dispatch(Landroid/view/KeyEvent$Callback;)Z
    .registers 3
    .parameter "receiver"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2686
    invoke-virtual {p0, p1, v0, v0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public final dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z
    .registers 10
    .parameter "receiver"
    .parameter "state"
    .parameter "target"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2702
    iget v4, p0, Landroid/view/KeyEvent;->mAction:I

    #@4
    packed-switch v4, :pswitch_data_76

    #@7
    move v3, v2

    #@8
    .line 2752
    :cond_8
    :goto_8
    return v3

    #@9
    .line 2704
    :pswitch_9
    iget v4, p0, Landroid/view/KeyEvent;->mFlags:I

    #@b
    const v5, -0x40000001

    #@e
    and-int/2addr v4, v5

    #@f
    iput v4, p0, Landroid/view/KeyEvent;->mFlags:I

    #@11
    .line 2707
    iget v4, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@13
    invoke-interface {p1, v4, p0}, Landroid/view/KeyEvent$Callback;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@16
    move-result v3

    #@17
    .line 2708
    .local v3, res:Z
    if-eqz p2, :cond_8

    #@19
    .line 2709
    if-eqz v3, :cond_2a

    #@1b
    iget v4, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@1d
    if-nez v4, :cond_2a

    #@1f
    iget v4, p0, Landroid/view/KeyEvent;->mFlags:I

    #@21
    const/high16 v5, 0x4000

    #@23
    and-int/2addr v4, v5

    #@24
    if-eqz v4, :cond_2a

    #@26
    .line 2711
    invoke-virtual {p2, p0, p3}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    #@29
    goto :goto_8

    #@2a
    .line 2712
    :cond_2a
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isLongPress()Z

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_8

    #@30
    invoke-virtual {p2, p0}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_8

    #@36
    .line 2714
    :try_start_36
    iget v4, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@38
    invoke-interface {p1, v4, p0}, Landroid/view/KeyEvent$Callback;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_8

    #@3e
    .line 2716
    invoke-virtual {p2, p0}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V
    :try_end_41
    .catch Ljava/lang/AbstractMethodError; {:try_start_36 .. :try_end_41} :catch_73

    #@41
    .line 2717
    const/4 v3, 0x1

    #@42
    goto :goto_8

    #@43
    .line 2728
    .end local v3           #res:Z
    :pswitch_43
    if-eqz p2, :cond_48

    #@45
    .line 2729
    invoke-virtual {p2, p0}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    #@48
    .line 2731
    :cond_48
    iget v4, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@4a
    invoke-interface {p1, v4, p0}, Landroid/view/KeyEvent$Callback;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@4d
    move-result v3

    #@4e
    goto :goto_8

    #@4f
    .line 2733
    :pswitch_4f
    iget v1, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@51
    .line 2734
    .local v1, count:I
    iget v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@53
    .line 2735
    .local v0, code:I
    invoke-interface {p1, v0, v1, p0}, Landroid/view/KeyEvent$Callback;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    #@56
    move-result v4

    #@57
    if-nez v4, :cond_8

    #@59
    .line 2738
    if-eqz v0, :cond_71

    #@5b
    .line 2739
    iput v2, p0, Landroid/view/KeyEvent;->mAction:I

    #@5d
    .line 2740
    iput v2, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@5f
    .line 2741
    invoke-interface {p1, v0, p0}, Landroid/view/KeyEvent$Callback;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@62
    move-result v2

    #@63
    .line 2742
    .local v2, handled:Z
    if-eqz v2, :cond_6a

    #@65
    .line 2743
    iput v3, p0, Landroid/view/KeyEvent;->mAction:I

    #@67
    .line 2744
    invoke-interface {p1, v0, p0}, Landroid/view/KeyEvent$Callback;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@6a
    .line 2746
    :cond_6a
    const/4 v4, 0x2

    #@6b
    iput v4, p0, Landroid/view/KeyEvent;->mAction:I

    #@6d
    .line 2747
    iput v1, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@6f
    move v3, v2

    #@70
    .line 2748
    goto :goto_8

    #@71
    .end local v2           #handled:Z
    :cond_71
    move v3, v2

    #@72
    .line 2750
    goto :goto_8

    #@73
    .line 2719
    .end local v0           #code:I
    .end local v1           #count:I
    .restart local v3       #res:Z
    :catch_73
    move-exception v4

    #@74
    goto :goto_8

    #@75
    .line 2702
    nop

    #@76
    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_9
        :pswitch_43
        :pswitch_4f
    .end packed-switch
.end method

.method public final getAction()I
    .registers 2

    #@0
    .prologue
    .line 2393
    iget v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@2
    return v0
.end method

.method public final getCharacters()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2451
    iget-object v0, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getDeviceId()I
    .registers 2

    #@0
    .prologue
    .line 1923
    iget v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@2
    return v0
.end method

.method public getDisplayLabel()C
    .registers 3

    #@0
    .prologue
    .line 2555
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@6
    invoke-virtual {v0, v1}, Landroid/view/KeyCharacterMap;->getDisplayLabel(I)C

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getDownTime()J
    .registers 3

    #@0
    .prologue
    .line 2489
    iget-wide v0, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@2
    return-wide v0
.end method

.method public final getEventTime()J
    .registers 3

    #@0
    .prologue
    .line 2501
    iget-wide v0, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@2
    return-wide v0
.end method

.method public final getEventTimeNano()J
    .registers 5

    #@0
    .prologue
    .line 2520
    iget-wide v0, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@2
    const-wide/32 v2, 0xf4240

    #@5
    mul-long/2addr v0, v2

    #@6
    return-wide v0
.end method

.method public final getFlags()I
    .registers 2

    #@0
    .prologue
    .line 2002
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    return v0
.end method

.method public final getKeyCharacterMap()Landroid/view/KeyCharacterMap;
    .registers 2

    #@0
    .prologue
    .line 2545
    iget v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@2
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final getKeyCode()I
    .registers 2

    #@0
    .prologue
    .line 2439
    iget v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@2
    return v0
.end method

.method public getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z
    .registers 4
    .parameter "results"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2617
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@6
    invoke-virtual {v0, v1, p1}, Landroid/view/KeyCharacterMap;->getKeyData(ILandroid/view/KeyCharacterMap$KeyData;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getKeyboardDevice()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2531
    iget v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@2
    return v0
.end method

.method public getMatch([C)C
    .registers 3
    .parameter "chars"

    #@0
    .prologue
    .line 2632
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/KeyEvent;->getMatch([CI)C

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getMatch([CI)C
    .registers 5
    .parameter "chars"
    .parameter "metaState"

    #@0
    .prologue
    .line 2645
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@6
    invoke-virtual {v0, v1, p1, p2}, Landroid/view/KeyCharacterMap;->getMatch(I[CI)C

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getMetaState()I
    .registers 2

    #@0
    .prologue
    .line 1973
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    return v0
.end method

.method public final getModifiers()I
    .registers 3

    #@0
    .prologue
    .line 1993
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    invoke-static {v0}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@5
    move-result v0

    #@6
    const v1, 0x770ff

    #@9
    and-int/2addr v0, v1

    #@a
    return v0
.end method

.method public getNumber()C
    .registers 3

    #@0
    .prologue
    .line 2669
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@6
    invoke-virtual {v0, v1}, Landroid/view/KeyCharacterMap;->getNumber(I)C

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getRepeatCount()I
    .registers 2

    #@0
    .prologue
    .line 2474
    iget v0, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@2
    return v0
.end method

.method public final getScanCode()I
    .registers 2

    #@0
    .prologue
    .line 2462
    iget v0, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@2
    return v0
.end method

.method public final getSource()I
    .registers 2

    #@0
    .prologue
    .line 1929
    iget v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@2
    return v0
.end method

.method public getUnicodeChar()I
    .registers 2

    #@0
    .prologue
    .line 2578
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    invoke-virtual {p0, v0}, Landroid/view/KeyEvent;->getUnicodeChar(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getUnicodeChar(I)I
    .registers 4
    .parameter "metaState"

    #@0
    .prologue
    .line 2602
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@6
    invoke-virtual {v0, v1, p1}, Landroid/view/KeyCharacterMap;->get(II)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final hasDefaultAction()Z
    .registers 2

    #@0
    .prologue
    .line 1874
    iget v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@2
    invoke-direct {p0, v0}, Landroid/view/KeyEvent;->native_hasDefaultAction(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final hasModifiers(I)Z
    .registers 3
    .parameter "modifiers"

    #@0
    .prologue
    .line 2271
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    invoke-static {v0, p1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final hasNoModifiers()Z
    .registers 2

    #@0
    .prologue
    .line 2240
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    invoke-static {v0}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final isAltPressed()Z
    .registers 2

    #@0
    .prologue
    .line 2284
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isCanceled()Z
    .registers 2

    #@0
    .prologue
    .line 2401
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isCapsLockOn()Z
    .registers 3

    #@0
    .prologue
    .line 2359
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    const/high16 v1, 0x10

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final isCtrlPressed()Z
    .registers 2

    #@0
    .prologue
    .line 2322
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    and-int/lit16 v0, v0, 0x1000

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isDown()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1859
    iget v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public final isFunctionPressed()Z
    .registers 2

    #@0
    .prologue
    .line 2347
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isLongPress()Z
    .registers 2

    #@0
    .prologue
    .line 2429
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    and-int/lit16 v0, v0, 0x80

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isMetaPressed()Z
    .registers 3

    #@0
    .prologue
    .line 2335
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    const/high16 v1, 0x1

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final isNumLockOn()Z
    .registers 3

    #@0
    .prologue
    .line 2371
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    const/high16 v1, 0x20

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isPrintingKey()Z
    .registers 3

    #@0
    .prologue
    .line 2678
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@6
    invoke-virtual {v0, v1}, Landroid/view/KeyCharacterMap;->isPrintingKey(I)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final isScrollLockOn()Z
    .registers 3

    #@0
    .prologue
    .line 2383
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    const/high16 v1, 0x40

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final isShiftPressed()Z
    .registers 2

    #@0
    .prologue
    .line 2297
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isSymPressed()Z
    .registers 2

    #@0
    .prologue
    .line 2309
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isSystem()Z
    .registers 2

    #@0
    .prologue
    .line 1869
    iget v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@2
    invoke-direct {p0, v0}, Landroid/view/KeyEvent;->native_isSystemKey(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final isTainted()Z
    .registers 3

    #@0
    .prologue
    .line 1840
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    const/high16 v1, -0x8000

    #@4
    and-int/2addr v0, v1

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public final isTracking()Z
    .registers 2

    #@0
    .prologue
    .line 2421
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    and-int/lit16 v0, v0, 0x200

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final recycle()V
    .registers 4

    #@0
    .prologue
    .line 1740
    invoke-super {p0}, Landroid/view/InputEvent;->recycle()V

    #@3
    .line 1741
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@6
    .line 1743
    sget-object v1, Landroid/view/KeyEvent;->gRecyclerLock:Ljava/lang/Object;

    #@8
    monitor-enter v1

    #@9
    .line 1744
    :try_start_9
    sget v0, Landroid/view/KeyEvent;->gRecyclerUsed:I

    #@b
    const/16 v2, 0xa

    #@d
    if-ge v0, v2, :cond_1b

    #@f
    .line 1745
    sget v0, Landroid/view/KeyEvent;->gRecyclerUsed:I

    #@11
    add-int/lit8 v0, v0, 0x1

    #@13
    sput v0, Landroid/view/KeyEvent;->gRecyclerUsed:I

    #@15
    .line 1746
    sget-object v0, Landroid/view/KeyEvent;->gRecyclerTop:Landroid/view/KeyEvent;

    #@17
    iput-object v0, p0, Landroid/view/KeyEvent;->mNext:Landroid/view/KeyEvent;

    #@19
    .line 1747
    sput-object p0, Landroid/view/KeyEvent;->gRecyclerTop:Landroid/view/KeyEvent;

    #@1b
    .line 1749
    :cond_1b
    monitor-exit v1

    #@1c
    .line 1750
    return-void

    #@1d
    .line 1749
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public final recycleIfNeededAfterDispatch()V
    .registers 1

    #@0
    .prologue
    .line 1756
    return-void
.end method

.method public final setSource(I)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 1935
    iput p1, p0, Landroid/view/KeyEvent;->mSource:I

    #@2
    .line 1936
    return-void
.end method

.method public final setTainted(Z)V
    .registers 4
    .parameter "tainted"

    #@0
    .prologue
    .line 1846
    if-eqz p1, :cond_a

    #@2
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@4
    const/high16 v1, -0x8000

    #@6
    or-int/2addr v0, v1

    #@7
    :goto_7
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@9
    .line 1847
    return-void

    #@a
    .line 1846
    :cond_a
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@c
    const v1, 0x7fffffff

    #@f
    and-int/2addr v0, v1

    #@10
    goto :goto_7
.end method

.method public final startTracking()V
    .registers 3

    #@0
    .prologue
    .line 2412
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@2
    const/high16 v1, 0x4000

    #@4
    or-int/2addr v0, v1

    #@5
    iput v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@7
    .line 2413
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 2849
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 2850
    .local v0, msg:Ljava/lang/StringBuilder;
    const-string v1, "KeyEvent { action="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Landroid/view/KeyEvent;->mAction:I

    #@d
    invoke-static {v2}, Landroid/view/KeyEvent;->actionToString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 2851
    const-string v1, ", keyCode="

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget v2, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@1c
    invoke-static {v2}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 2852
    const-string v1, ", scanCode="

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget v2, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    .line 2853
    iget-object v1, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@30
    if-eqz v1, :cond_43

    #@32
    .line 2854
    const-string v1, ", characters=\""

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    iget-object v2, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    const-string v2, "\""

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 2856
    :cond_43
    const-string v1, ", metaState="

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    iget v2, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@4b
    invoke-static {v2}, Landroid/view/KeyEvent;->metaStateToString(I)Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    .line 2857
    const-string v1, ", flags=0x"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    iget v2, p0, Landroid/view/KeyEvent;->mFlags:I

    #@5a
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    .line 2858
    const-string v1, ", repeatCount="

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    iget v2, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@69
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    .line 2859
    const-string v1, ", eventTime="

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    iget-wide v2, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@74
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@77
    .line 2860
    const-string v1, ", downTime="

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    iget-wide v2, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@7f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@82
    .line 2861
    const-string v1, ", deviceId="

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    iget v2, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@8a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d
    .line 2862
    const-string v1, ", source=0x"

    #@8f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    iget v2, p0, Landroid/view/KeyEvent;->mSource:I

    #@95
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    .line 2863
    const-string v1, " }"

    #@9e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    .line 2864
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v1

    #@a5
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 3002
    const/4 v0, 0x2

    #@1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4
    .line 3004
    iget v0, p0, Landroid/view/KeyEvent;->mDeviceId:I

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 3005
    iget v0, p0, Landroid/view/KeyEvent;->mSource:I

    #@b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 3006
    iget v0, p0, Landroid/view/KeyEvent;->mAction:I

    #@10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3007
    iget v0, p0, Landroid/view/KeyEvent;->mKeyCode:I

    #@15
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 3008
    iget v0, p0, Landroid/view/KeyEvent;->mRepeatCount:I

    #@1a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 3009
    iget v0, p0, Landroid/view/KeyEvent;->mMetaState:I

    #@1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 3010
    iget v0, p0, Landroid/view/KeyEvent;->mScanCode:I

    #@24
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 3011
    iget v0, p0, Landroid/view/KeyEvent;->mFlags:I

    #@29
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 3012
    iget-wide v0, p0, Landroid/view/KeyEvent;->mDownTime:J

    #@2e
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@31
    .line 3013
    iget-wide v0, p0, Landroid/view/KeyEvent;->mEventTime:J

    #@33
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@36
    .line 3015
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@38
    if-eqz v0, :cond_3f

    #@3a
    .line 3016
    iget-object v0, p0, Landroid/view/KeyEvent;->mCharacters:Ljava/lang/String;

    #@3c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3f
    .line 3019
    :cond_3f
    return-void
.end method
