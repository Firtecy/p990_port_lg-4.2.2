.class public final Landroid/view/MotionEvent;
.super Landroid/view/InputEvent;
.source "MotionEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/MotionEvent$PointerProperties;,
        Landroid/view/MotionEvent$PointerCoords;
    }
.end annotation


# static fields
.field public static final ACTION_CANCEL:I = 0x3

.field public static final ACTION_DOWN:I = 0x0

.field public static final ACTION_HOVER_ENTER:I = 0x9

.field public static final ACTION_HOVER_EXIT:I = 0xa

.field public static final ACTION_HOVER_MOVE:I = 0x7

.field public static final ACTION_MASK:I = 0xff

.field public static final ACTION_MOVE:I = 0x2

.field public static final ACTION_OUTSIDE:I = 0x4

.field public static final ACTION_POINTER_1_DOWN:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_1_UP:I = 0x6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_2_DOWN:I = 0x105
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_2_UP:I = 0x106
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_3_DOWN:I = 0x205
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_3_UP:I = 0x206
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_DOWN:I = 0x5

.field public static final ACTION_POINTER_ID_MASK:I = 0xff00
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_ID_SHIFT:I = 0x8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_POINTER_INDEX_MASK:I = 0xff00

.field public static final ACTION_POINTER_INDEX_SHIFT:I = 0x8

.field public static final ACTION_POINTER_UP:I = 0x6

.field public static final ACTION_SCROLL:I = 0x8

.field public static final ACTION_UP:I = 0x1

.field public static final AXIS_BRAKE:I = 0x17

.field public static final AXIS_DISTANCE:I = 0x18

.field public static final AXIS_GAS:I = 0x16

.field public static final AXIS_GENERIC_1:I = 0x20

.field public static final AXIS_GENERIC_10:I = 0x29

.field public static final AXIS_GENERIC_11:I = 0x2a

.field public static final AXIS_GENERIC_12:I = 0x2b

.field public static final AXIS_GENERIC_13:I = 0x2c

.field public static final AXIS_GENERIC_14:I = 0x2d

.field public static final AXIS_GENERIC_15:I = 0x2e

.field public static final AXIS_GENERIC_16:I = 0x2f

.field public static final AXIS_GENERIC_2:I = 0x21

.field public static final AXIS_GENERIC_3:I = 0x22

.field public static final AXIS_GENERIC_4:I = 0x23

.field public static final AXIS_GENERIC_5:I = 0x24

.field public static final AXIS_GENERIC_6:I = 0x25

.field public static final AXIS_GENERIC_7:I = 0x26

.field public static final AXIS_GENERIC_8:I = 0x27

.field public static final AXIS_GENERIC_9:I = 0x28

.field public static final AXIS_HAT_X:I = 0xf

.field public static final AXIS_HAT_Y:I = 0x10

.field public static final AXIS_HSCROLL:I = 0xa

.field public static final AXIS_LTRIGGER:I = 0x11

.field public static final AXIS_ORIENTATION:I = 0x8

.field public static final AXIS_PRESSURE:I = 0x2

.field public static final AXIS_RTRIGGER:I = 0x12

.field public static final AXIS_RUDDER:I = 0x14

.field public static final AXIS_RX:I = 0xc

.field public static final AXIS_RY:I = 0xd

.field public static final AXIS_RZ:I = 0xe

.field public static final AXIS_SIZE:I = 0x3

.field private static final AXIS_SYMBOLIC_NAMES:Landroid/util/SparseArray; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final AXIS_THROTTLE:I = 0x13

.field public static final AXIS_TILT:I = 0x19

.field public static final AXIS_TOOL_MAJOR:I = 0x6

.field public static final AXIS_TOOL_MINOR:I = 0x7

.field public static final AXIS_TOUCH_MAJOR:I = 0x4

.field public static final AXIS_TOUCH_MINOR:I = 0x5

.field public static final AXIS_VSCROLL:I = 0x9

.field public static final AXIS_WHEEL:I = 0x15

.field public static final AXIS_X:I = 0x0

.field public static final AXIS_Y:I = 0x1

.field public static final AXIS_Z:I = 0xb

.field public static final BUTTON_BACK:I = 0x8

.field public static final BUTTON_FORWARD:I = 0x10

.field public static final BUTTON_PRIMARY:I = 0x1

.field public static final BUTTON_SECONDARY:I = 0x2

.field private static final BUTTON_SYMBOLIC_NAMES:[Ljava/lang/String; = null

.field public static final BUTTON_TERTIARY:I = 0x4

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDGE_BOTTOM:I = 0x2

.field public static final EDGE_LEFT:I = 0x4

.field public static final EDGE_RIGHT:I = 0x8

.field public static final EDGE_TOP:I = 0x1

.field public static final FLAG_EVENT_IS_SPLITED:I = 0x2

.field public static final FLAG_TAINTED:I = -0x80000000

.field public static final FLAG_WINDOW_IS_OBSCURED:I = 0x1

.field private static final HISTORY_CURRENT:I = -0x80000000

.field public static final INVALID_POINTER_ID:I = -0x1

.field private static final MAX_RECYCLED:I = 0xa

.field private static final NS_PER_MS:J = 0xf4240L

.field public static final TOOL_TYPE_ERASER:I = 0x4

.field public static final TOOL_TYPE_FINGER:I = 0x1

.field public static final TOOL_TYPE_MOUSE:I = 0x3

.field public static final TOOL_TYPE_PALM:I = 0x5

.field public static final TOOL_TYPE_PALM_COVER:I = 0x6

.field public static final TOOL_TYPE_STYLUS:I = 0x2

.field private static final TOOL_TYPE_SYMBOLIC_NAMES:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final TOOL_TYPE_UNKNOWN:I

.field private static final gRecyclerLock:Ljava/lang/Object;

.field private static gRecyclerTop:Landroid/view/MotionEvent;

.field private static gRecyclerUsed:I

.field private static final gSharedTempLock:Ljava/lang/Object;

.field private static gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

.field private static gSharedTempPointerIndexMap:[I

.field private static gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

.field private static hasEverPalmPressed:Z


# instance fields
.field private mNativePtr:I

.field private mNext:Landroid/view/MotionEvent;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 1109
    new-instance v1, Landroid/util/SparseArray;

    #@7
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@a
    sput-object v1, Landroid/view/MotionEvent;->AXIS_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@c
    .line 1111
    sget-object v0, Landroid/view/MotionEvent;->AXIS_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@e
    .line 1112
    .local v0, names:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/String;>;"
    const-string v1, "AXIS_X"

    #@10
    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@13
    .line 1113
    const-string v1, "AXIS_Y"

    #@15
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@18
    .line 1114
    const-string v1, "AXIS_PRESSURE"

    #@1a
    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1d
    .line 1115
    const-string v1, "AXIS_SIZE"

    #@1f
    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@22
    .line 1116
    const-string v1, "AXIS_TOUCH_MAJOR"

    #@24
    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@27
    .line 1117
    const/4 v1, 0x5

    #@28
    const-string v2, "AXIS_TOUCH_MINOR"

    #@2a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2d
    .line 1118
    const/4 v1, 0x6

    #@2e
    const-string v2, "AXIS_TOOL_MAJOR"

    #@30
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@33
    .line 1119
    const/4 v1, 0x7

    #@34
    const-string v2, "AXIS_TOOL_MINOR"

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@39
    .line 1120
    const/16 v1, 0x8

    #@3b
    const-string v2, "AXIS_ORIENTATION"

    #@3d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@40
    .line 1121
    const/16 v1, 0x9

    #@42
    const-string v2, "AXIS_VSCROLL"

    #@44
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@47
    .line 1122
    const/16 v1, 0xa

    #@49
    const-string v2, "AXIS_HSCROLL"

    #@4b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4e
    .line 1123
    const/16 v1, 0xb

    #@50
    const-string v2, "AXIS_Z"

    #@52
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@55
    .line 1124
    const/16 v1, 0xc

    #@57
    const-string v2, "AXIS_RX"

    #@59
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@5c
    .line 1125
    const/16 v1, 0xd

    #@5e
    const-string v2, "AXIS_RY"

    #@60
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@63
    .line 1126
    const/16 v1, 0xe

    #@65
    const-string v2, "AXIS_RZ"

    #@67
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@6a
    .line 1127
    const/16 v1, 0xf

    #@6c
    const-string v2, "AXIS_HAT_X"

    #@6e
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@71
    .line 1128
    const/16 v1, 0x10

    #@73
    const-string v2, "AXIS_HAT_Y"

    #@75
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@78
    .line 1129
    const/16 v1, 0x11

    #@7a
    const-string v2, "AXIS_LTRIGGER"

    #@7c
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@7f
    .line 1130
    const/16 v1, 0x12

    #@81
    const-string v2, "AXIS_RTRIGGER"

    #@83
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@86
    .line 1131
    const/16 v1, 0x13

    #@88
    const-string v2, "AXIS_THROTTLE"

    #@8a
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@8d
    .line 1132
    const/16 v1, 0x14

    #@8f
    const-string v2, "AXIS_RUDDER"

    #@91
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@94
    .line 1133
    const/16 v1, 0x15

    #@96
    const-string v2, "AXIS_WHEEL"

    #@98
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@9b
    .line 1134
    const/16 v1, 0x16

    #@9d
    const-string v2, "AXIS_GAS"

    #@9f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@a2
    .line 1135
    const/16 v1, 0x17

    #@a4
    const-string v2, "AXIS_BRAKE"

    #@a6
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@a9
    .line 1136
    const/16 v1, 0x18

    #@ab
    const-string v2, "AXIS_DISTANCE"

    #@ad
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@b0
    .line 1137
    const/16 v1, 0x19

    #@b2
    const-string v2, "AXIS_TILT"

    #@b4
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@b7
    .line 1138
    const/16 v1, 0x20

    #@b9
    const-string v2, "AXIS_GENERIC_1"

    #@bb
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@be
    .line 1139
    const/16 v1, 0x21

    #@c0
    const-string v2, "AXIS_GENERIC_2"

    #@c2
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@c5
    .line 1140
    const/16 v1, 0x22

    #@c7
    const-string v2, "AXIS_GENERIC_3"

    #@c9
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@cc
    .line 1141
    const/16 v1, 0x23

    #@ce
    const-string v2, "AXIS_GENERIC_4"

    #@d0
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@d3
    .line 1142
    const/16 v1, 0x24

    #@d5
    const-string v2, "AXIS_GENERIC_5"

    #@d7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@da
    .line 1143
    const/16 v1, 0x25

    #@dc
    const-string v2, "AXIS_GENERIC_6"

    #@de
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@e1
    .line 1144
    const/16 v1, 0x26

    #@e3
    const-string v2, "AXIS_GENERIC_7"

    #@e5
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@e8
    .line 1145
    const/16 v1, 0x27

    #@ea
    const-string v2, "AXIS_GENERIC_8"

    #@ec
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@ef
    .line 1146
    const/16 v1, 0x28

    #@f1
    const-string v2, "AXIS_GENERIC_9"

    #@f3
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@f6
    .line 1147
    const/16 v1, 0x29

    #@f8
    const-string v2, "AXIS_GENERIC_10"

    #@fa
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@fd
    .line 1148
    const/16 v1, 0x2a

    #@ff
    const-string v2, "AXIS_GENERIC_11"

    #@101
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@104
    .line 1149
    const/16 v1, 0x2b

    #@106
    const-string v2, "AXIS_GENERIC_12"

    #@108
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@10b
    .line 1150
    const/16 v1, 0x2c

    #@10d
    const-string v2, "AXIS_GENERIC_13"

    #@10f
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@112
    .line 1151
    const/16 v1, 0x2d

    #@114
    const-string v2, "AXIS_GENERIC_14"

    #@116
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@119
    .line 1152
    const/16 v1, 0x2e

    #@11b
    const-string v2, "AXIS_GENERIC_15"

    #@11d
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@120
    .line 1153
    const/16 v1, 0x2f

    #@122
    const-string v2, "AXIS_GENERIC_16"

    #@124
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@127
    .line 1207
    const/16 v1, 0x20

    #@129
    new-array v1, v1, [Ljava/lang/String;

    #@12b
    const-string v2, "BUTTON_PRIMARY"

    #@12d
    aput-object v2, v1, v4

    #@12f
    const-string v2, "BUTTON_SECONDARY"

    #@131
    aput-object v2, v1, v5

    #@133
    const-string v2, "BUTTON_TERTIARY"

    #@135
    aput-object v2, v1, v6

    #@137
    const-string v2, "BUTTON_BACK"

    #@139
    aput-object v2, v1, v7

    #@13b
    const-string v2, "BUTTON_FORWARD"

    #@13d
    aput-object v2, v1, v8

    #@13f
    const/4 v2, 0x5

    #@140
    const-string v3, "0x00000020"

    #@142
    aput-object v3, v1, v2

    #@144
    const/4 v2, 0x6

    #@145
    const-string v3, "0x00000040"

    #@147
    aput-object v3, v1, v2

    #@149
    const/4 v2, 0x7

    #@14a
    const-string v3, "0x00000080"

    #@14c
    aput-object v3, v1, v2

    #@14e
    const/16 v2, 0x8

    #@150
    const-string v3, "0x00000100"

    #@152
    aput-object v3, v1, v2

    #@154
    const/16 v2, 0x9

    #@156
    const-string v3, "0x00000200"

    #@158
    aput-object v3, v1, v2

    #@15a
    const/16 v2, 0xa

    #@15c
    const-string v3, "0x00000400"

    #@15e
    aput-object v3, v1, v2

    #@160
    const/16 v2, 0xb

    #@162
    const-string v3, "0x00000800"

    #@164
    aput-object v3, v1, v2

    #@166
    const/16 v2, 0xc

    #@168
    const-string v3, "0x00001000"

    #@16a
    aput-object v3, v1, v2

    #@16c
    const/16 v2, 0xd

    #@16e
    const-string v3, "0x00002000"

    #@170
    aput-object v3, v1, v2

    #@172
    const/16 v2, 0xe

    #@174
    const-string v3, "0x00004000"

    #@176
    aput-object v3, v1, v2

    #@178
    const/16 v2, 0xf

    #@17a
    const-string v3, "0x00008000"

    #@17c
    aput-object v3, v1, v2

    #@17e
    const/16 v2, 0x10

    #@180
    const-string v3, "0x00010000"

    #@182
    aput-object v3, v1, v2

    #@184
    const/16 v2, 0x11

    #@186
    const-string v3, "0x00020000"

    #@188
    aput-object v3, v1, v2

    #@18a
    const/16 v2, 0x12

    #@18c
    const-string v3, "0x00040000"

    #@18e
    aput-object v3, v1, v2

    #@190
    const/16 v2, 0x13

    #@192
    const-string v3, "0x00080000"

    #@194
    aput-object v3, v1, v2

    #@196
    const/16 v2, 0x14

    #@198
    const-string v3, "0x00100000"

    #@19a
    aput-object v3, v1, v2

    #@19c
    const/16 v2, 0x15

    #@19e
    const-string v3, "0x00200000"

    #@1a0
    aput-object v3, v1, v2

    #@1a2
    const/16 v2, 0x16

    #@1a4
    const-string v3, "0x00400000"

    #@1a6
    aput-object v3, v1, v2

    #@1a8
    const/16 v2, 0x17

    #@1aa
    const-string v3, "0x00800000"

    #@1ac
    aput-object v3, v1, v2

    #@1ae
    const/16 v2, 0x18

    #@1b0
    const-string v3, "0x01000000"

    #@1b2
    aput-object v3, v1, v2

    #@1b4
    const/16 v2, 0x19

    #@1b6
    const-string v3, "0x02000000"

    #@1b8
    aput-object v3, v1, v2

    #@1ba
    const/16 v2, 0x1a

    #@1bc
    const-string v3, "0x04000000"

    #@1be
    aput-object v3, v1, v2

    #@1c0
    const/16 v2, 0x1b

    #@1c2
    const-string v3, "0x08000000"

    #@1c4
    aput-object v3, v1, v2

    #@1c6
    const/16 v2, 0x1c

    #@1c8
    const-string v3, "0x10000000"

    #@1ca
    aput-object v3, v1, v2

    #@1cc
    const/16 v2, 0x1d

    #@1ce
    const-string v3, "0x20000000"

    #@1d0
    aput-object v3, v1, v2

    #@1d2
    const/16 v2, 0x1e

    #@1d4
    const-string v3, "0x40000000"

    #@1d6
    aput-object v3, v1, v2

    #@1d8
    const/16 v2, 0x1f

    #@1da
    const-string v3, "0x80000000"

    #@1dc
    aput-object v3, v1, v2

    #@1de
    sput-object v1, Landroid/view/MotionEvent;->BUTTON_SYMBOLIC_NAMES:[Ljava/lang/String;

    #@1e0
    .line 1301
    new-instance v1, Landroid/util/SparseArray;

    #@1e2
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@1e5
    sput-object v1, Landroid/view/MotionEvent;->TOOL_TYPE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@1e7
    .line 1303
    sget-object v0, Landroid/view/MotionEvent;->TOOL_TYPE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@1e9
    .line 1304
    const-string v1, "TOOL_TYPE_UNKNOWN"

    #@1eb
    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1ee
    .line 1305
    const-string v1, "TOOL_TYPE_FINGER"

    #@1f0
    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1f3
    .line 1306
    const-string v1, "TOOL_TYPE_STYLUS"

    #@1f5
    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1f8
    .line 1307
    const-string v1, "TOOL_TYPE_MOUSE"

    #@1fa
    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1fd
    .line 1308
    const-string v1, "TOOL_TYPE_ERASER"

    #@1ff
    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@202
    .line 1309
    const/4 v1, 0x5

    #@203
    const-string v2, "TOOL_TYPE_PALM"

    #@205
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@208
    .line 1310
    const/4 v1, 0x6

    #@209
    const-string v2, "TOOL_TYPE_PALM_COVER"

    #@20b
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@20e
    .line 1317
    new-instance v1, Ljava/lang/Object;

    #@210
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@213
    sput-object v1, Landroid/view/MotionEvent;->gRecyclerLock:Ljava/lang/Object;

    #@215
    .line 1323
    new-instance v1, Ljava/lang/Object;

    #@217
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@21a
    sput-object v1, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@21c
    .line 3209
    new-instance v1, Landroid/view/MotionEvent$1;

    #@21e
    invoke-direct {v1}, Landroid/view/MotionEvent$1;-><init>()V

    #@221
    sput-object v1, Landroid/view/MotionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@223
    .line 3235
    sput-boolean v4, Landroid/view/MotionEvent;->hasEverPalmPressed:Z

    #@225
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1403
    invoke-direct {p0}, Landroid/view/InputEvent;-><init>()V

    #@3
    .line 1404
    return-void
.end method

.method public static actionToString(I)Ljava/lang/String;
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 3090
    packed-switch p0, :pswitch_data_62

    #@3
    .line 3110
    :pswitch_3
    const v1, 0xff00

    #@6
    and-int/2addr v1, p0

    #@7
    shr-int/lit8 v0, v1, 0x8

    #@9
    .line 3111
    .local v0, index:I
    and-int/lit16 v1, p0, 0xff

    #@b
    packed-switch v1, :pswitch_data_7c

    #@e
    .line 3117
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .end local v0           #index:I
    :goto_12
    return-object v1

    #@13
    .line 3092
    :pswitch_13
    const-string v1, "ACTION_DOWN"

    #@15
    goto :goto_12

    #@16
    .line 3094
    :pswitch_16
    const-string v1, "ACTION_UP"

    #@18
    goto :goto_12

    #@19
    .line 3096
    :pswitch_19
    const-string v1, "ACTION_CANCEL"

    #@1b
    goto :goto_12

    #@1c
    .line 3098
    :pswitch_1c
    const-string v1, "ACTION_OUTSIDE"

    #@1e
    goto :goto_12

    #@1f
    .line 3100
    :pswitch_1f
    const-string v1, "ACTION_MOVE"

    #@21
    goto :goto_12

    #@22
    .line 3102
    :pswitch_22
    const-string v1, "ACTION_HOVER_MOVE"

    #@24
    goto :goto_12

    #@25
    .line 3104
    :pswitch_25
    const-string v1, "ACTION_SCROLL"

    #@27
    goto :goto_12

    #@28
    .line 3106
    :pswitch_28
    const-string v1, "ACTION_HOVER_ENTER"

    #@2a
    goto :goto_12

    #@2b
    .line 3108
    :pswitch_2b
    const-string v1, "ACTION_HOVER_EXIT"

    #@2d
    goto :goto_12

    #@2e
    .line 3113
    .restart local v0       #index:I
    :pswitch_2e
    new-instance v1, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v2, "ACTION_POINTER_DOWN("

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, ")"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    goto :goto_12

    #@48
    .line 3115
    :pswitch_48
    new-instance v1, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v2, "ACTION_POINTER_UP("

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    const-string v2, ")"

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    goto :goto_12

    #@62
    .line 3090
    :pswitch_data_62
    .packed-switch 0x0
        :pswitch_13
        :pswitch_16
        :pswitch_1f
        :pswitch_19
        :pswitch_1c
        :pswitch_3
        :pswitch_3
        :pswitch_22
        :pswitch_25
        :pswitch_28
        :pswitch_2b
    .end packed-switch

    #@7c
    .line 3111
    :pswitch_data_7c
    .packed-switch 0x5
        :pswitch_2e
        :pswitch_48
    .end packed-switch
.end method

.method public static axisFromString(Ljava/lang/String;)I
    .registers 6
    .parameter "symbolicName"

    #@0
    .prologue
    .line 3142
    if-nez p0, :cond_b

    #@2
    .line 3143
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v4, "symbolicName must not be null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 3146
    :cond_b
    sget-object v3, Landroid/view/MotionEvent;->AXIS_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@10
    move-result v0

    #@11
    .line 3147
    .local v0, count:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_24

    #@14
    .line 3148
    sget-object v3, Landroid/view/MotionEvent;->AXIS_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_21

    #@20
    .line 3156
    .end local v2           #i:I
    :goto_20
    return v2

    #@21
    .line 3147
    .restart local v2       #i:I
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_12

    #@24
    .line 3154
    :cond_24
    const/16 v3, 0xa

    #@26
    :try_start_26
    invoke-static {p0, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_29
    .catch Ljava/lang/NumberFormatException; {:try_start_26 .. :try_end_29} :catch_2b

    #@29
    move-result v2

    #@2a
    goto :goto_20

    #@2b
    .line 3155
    :catch_2b
    move-exception v1

    #@2c
    .line 3156
    .local v1, ex:Ljava/lang/NumberFormatException;
    const/4 v2, -0x1

    #@2d
    goto :goto_20
.end method

.method public static axisToString(I)Ljava/lang/String;
    .registers 3
    .parameter "axis"

    #@0
    .prologue
    .line 3129
    sget-object v1, Landroid/view/MotionEvent;->AXIS_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    .line 3130
    .local v0, symbolicName:Ljava/lang/String;
    if-eqz v0, :cond_b

    #@a
    .end local v0           #symbolicName:Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #symbolicName:Ljava/lang/String;
    :cond_b
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method public static buttonStateToString(I)Ljava/lang/String;
    .registers 6
    .parameter "buttonState"

    #@0
    .prologue
    .line 3171
    if-nez p0, :cond_5

    #@2
    .line 3172
    const-string v2, "0"

    #@4
    .line 3193
    :cond_4
    :goto_4
    return-object v2

    #@5
    .line 3174
    :cond_5
    const/4 v3, 0x0

    #@6
    .line 3175
    .local v3, result:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@7
    .line 3176
    .local v0, i:I
    :goto_7
    if-eqz p0, :cond_2d

    #@9
    .line 3177
    and-int/lit8 v4, p0, 0x1

    #@b
    if-eqz v4, :cond_22

    #@d
    const/4 v1, 0x1

    #@e
    .line 3178
    .local v1, isSet:Z
    :goto_e
    ushr-int/lit8 p0, p0, 0x1

    #@10
    .line 3179
    if-eqz v1, :cond_1f

    #@12
    .line 3180
    sget-object v4, Landroid/view/MotionEvent;->BUTTON_SYMBOLIC_NAMES:[Ljava/lang/String;

    #@14
    aget-object v2, v4, v0

    #@16
    .line 3181
    .local v2, name:Ljava/lang/String;
    if-nez v3, :cond_24

    #@18
    .line 3182
    if-eqz p0, :cond_4

    #@1a
    .line 3185
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c
    .end local v3           #result:Ljava/lang/StringBuilder;
    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@1f
    .line 3191
    .end local v2           #name:Ljava/lang/String;
    .restart local v3       #result:Ljava/lang/StringBuilder;
    :cond_1f
    :goto_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    .line 3192
    goto :goto_7

    #@22
    .line 3177
    .end local v1           #isSet:Z
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_e

    #@24
    .line 3187
    .restart local v1       #isSet:Z
    .restart local v2       #name:Ljava/lang/String;
    :cond_24
    const/16 v4, 0x7c

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    .line 3188
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    goto :goto_1f

    #@2d
    .line 3193
    .end local v1           #isSet:Z
    .end local v2           #name:Ljava/lang/String;
    :cond_2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    goto :goto_4
.end method

.method public static checkPalmTouchAndMakeCancelMotionEvent(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 6
    .parameter "in"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v3, 0x1

    #@3
    .line 3239
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionMasked()I

    #@6
    move-result v1

    #@7
    if-eq v1, v3, :cond_11

    #@9
    sget-boolean v1, Landroid/view/MotionEvent;->hasEverPalmPressed:Z

    #@b
    if-eqz v1, :cond_11

    #@d
    .line 3240
    invoke-virtual {p0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@10
    .line 3258
    :cond_10
    :goto_10
    return-object p0

    #@11
    .line 3242
    :cond_11
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionMasked()I

    #@14
    move-result v1

    #@15
    if-ne v1, v3, :cond_1a

    #@17
    .line 3243
    sput-boolean v2, Landroid/view/MotionEvent;->hasEverPalmPressed:Z

    #@19
    goto :goto_10

    #@1a
    .line 3245
    :cond_1a
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionMasked()I

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_22

    #@20
    .line 3246
    sput-boolean v2, Landroid/view/MotionEvent;->hasEverPalmPressed:Z

    #@22
    .line 3249
    :cond_22
    const/4 v0, 0x0

    #@23
    .local v0, i:I
    :goto_23
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    #@26
    move-result v1

    #@27
    if-ge v0, v1, :cond_10

    #@29
    .line 3250
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    #@2c
    move-result v1

    #@2d
    const/4 v2, 0x5

    #@2e
    if-eq v1, v2, :cond_37

    #@30
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    #@33
    move-result v1

    #@34
    const/4 v2, 0x6

    #@35
    if-ne v1, v2, :cond_3d

    #@37
    .line 3252
    :cond_37
    sput-boolean v3, Landroid/view/MotionEvent;->hasEverPalmPressed:Z

    #@39
    .line 3253
    invoke-virtual {p0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    #@3c
    goto :goto_10

    #@3d
    .line 3249
    :cond_3d
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_23
.end method

.method private static final clamp(FFF)F
    .registers 4
    .parameter "value"
    .parameter "low"
    .parameter "high"

    #@0
    .prologue
    .line 2913
    cmpg-float v0, p0, p1

    #@2
    if-gez v0, :cond_5

    #@4
    .line 2918
    .end local p1
    :goto_4
    return p1

    #@5
    .line 2915
    .restart local p1
    :cond_5
    cmpl-float v0, p0, p2

    #@7
    if-lez v0, :cond_b

    #@9
    move p1, p2

    #@a
    .line 2916
    goto :goto_4

    #@b
    :cond_b
    move p1, p0

    #@c
    .line 2918
    goto :goto_4
.end method

.method public static createFromParcelBody(Landroid/os/Parcel;)Landroid/view/MotionEvent;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 3223
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@3
    move-result-object v0

    #@4
    .line 3224
    .local v0, ev:Landroid/view/MotionEvent;
    iget v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@6
    invoke-static {v1, p0}, Landroid/view/MotionEvent;->nativeReadFromParcel(ILandroid/os/Parcel;)I

    #@9
    move-result v1

    #@a
    iput v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@c
    .line 3225
    return-object v0
.end method

.method private static final ensureSharedTempPointerCapacity(I)V
    .registers 3
    .parameter "desiredCapacity"

    #@0
    .prologue
    .line 1329
    sget-object v1, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@2
    if-eqz v1, :cond_9

    #@4
    sget-object v1, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@6
    array-length v1, v1

    #@7
    if-ge v1, p0, :cond_28

    #@9
    .line 1331
    :cond_9
    sget-object v1, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@b
    if-eqz v1, :cond_15

    #@d
    sget-object v1, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@f
    array-length v0, v1

    #@10
    .line 1332
    .local v0, capacity:I
    :goto_10
    if-ge v0, p0, :cond_18

    #@12
    .line 1333
    mul-int/lit8 v0, v0, 0x2

    #@14
    goto :goto_10

    #@15
    .line 1331
    .end local v0           #capacity:I
    :cond_15
    const/16 v0, 0x8

    #@17
    goto :goto_10

    #@18
    .line 1335
    .restart local v0       #capacity:I
    :cond_18
    invoke-static {v0}, Landroid/view/MotionEvent$PointerCoords;->createArray(I)[Landroid/view/MotionEvent$PointerCoords;

    #@1b
    move-result-object v1

    #@1c
    sput-object v1, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@1e
    .line 1336
    invoke-static {v0}, Landroid/view/MotionEvent$PointerProperties;->createArray(I)[Landroid/view/MotionEvent$PointerProperties;

    #@21
    move-result-object v1

    #@22
    sput-object v1, Landroid/view/MotionEvent;->gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@24
    .line 1337
    new-array v1, v0, [I

    #@26
    sput-object v1, Landroid/view/MotionEvent;->gSharedTempPointerIndexMap:[I

    #@28
    .line 1339
    .end local v0           #capacity:I
    :cond_28
    return-void
.end method

.method private static native nativeAddBatch(IJ[Landroid/view/MotionEvent$PointerCoords;I)V
.end method

.method private static native nativeCopy(IIZ)I
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeFindPointerIndex(II)I
.end method

.method private static native nativeGetAction(I)I
.end method

.method private static native nativeGetAxisValue(IIII)F
.end method

.method private static native nativeGetButtonState(I)I
.end method

.method private static native nativeGetDeviceId(I)I
.end method

.method private static native nativeGetDownTimeNanos(I)J
.end method

.method private static native nativeGetEdgeFlags(I)I
.end method

.method private static native nativeGetEventTimeNanos(II)J
.end method

.method private static native nativeGetFlags(I)I
.end method

.method private static native nativeGetHistorySize(I)I
.end method

.method private static native nativeGetMetaState(I)I
.end method

.method private static native nativeGetPointerCoords(IIILandroid/view/MotionEvent$PointerCoords;)V
.end method

.method private static native nativeGetPointerCount(I)I
.end method

.method private static native nativeGetPointerId(II)I
.end method

.method private static native nativeGetPointerProperties(IILandroid/view/MotionEvent$PointerProperties;)V
.end method

.method private static native nativeGetRawAxisValue(IIII)F
.end method

.method private static native nativeGetSource(I)I
.end method

.method private static native nativeGetToolType(II)I
.end method

.method private static native nativeGetXOffset(I)F
.end method

.method private static native nativeGetXPrecision(I)F
.end method

.method private static native nativeGetYOffset(I)F
.end method

.method private static native nativeGetYPrecision(I)F
.end method

.method private static native nativeInitialize(IIIIIIIIFFFFJJI[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;)I
.end method

.method private static native nativeIsTouchEvent(I)Z
.end method

.method private static native nativeOffsetLocation(IFF)V
.end method

.method private static native nativeReadFromParcel(ILandroid/os/Parcel;)I
.end method

.method private static native nativeScale(IF)V
.end method

.method private static native nativeSetAction(II)V
.end method

.method private static native nativeSetDownTimeNanos(IJ)V
.end method

.method private static native nativeSetEdgeFlags(II)V
.end method

.method private static native nativeSetFlags(II)V
.end method

.method private static native nativeSetPointerId(III)V
.end method

.method private static native nativeSetSource(II)I
.end method

.method private static native nativeTransform(ILandroid/graphics/Matrix;)V
.end method

.method private static native nativeWriteToParcel(ILandroid/os/Parcel;)V
.end method

.method private static obtain()Landroid/view/MotionEvent;
    .registers 3

    #@0
    .prologue
    .line 1420
    sget-object v2, Landroid/view/MotionEvent;->gRecyclerLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1421
    :try_start_3
    sget-object v0, Landroid/view/MotionEvent;->gRecyclerTop:Landroid/view/MotionEvent;

    #@5
    .line 1422
    .local v0, ev:Landroid/view/MotionEvent;
    if-nez v0, :cond_e

    #@7
    .line 1423
    new-instance v0, Landroid/view/MotionEvent;

    #@9
    .end local v0           #ev:Landroid/view/MotionEvent;
    invoke-direct {v0}, Landroid/view/MotionEvent;-><init>()V

    #@c
    monitor-exit v2

    #@d
    .line 1430
    .restart local v0       #ev:Landroid/view/MotionEvent;
    :goto_d
    return-object v0

    #@e
    .line 1425
    :cond_e
    iget-object v1, v0, Landroid/view/MotionEvent;->mNext:Landroid/view/MotionEvent;

    #@10
    sput-object v1, Landroid/view/MotionEvent;->gRecyclerTop:Landroid/view/MotionEvent;

    #@12
    .line 1426
    sget v1, Landroid/view/MotionEvent;->gRecyclerUsed:I

    #@14
    add-int/lit8 v1, v1, -0x1

    #@16
    sput v1, Landroid/view/MotionEvent;->gRecyclerUsed:I

    #@18
    .line 1427
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_20

    #@19
    .line 1428
    const/4 v1, 0x0

    #@1a
    iput-object v1, v0, Landroid/view/MotionEvent;->mNext:Landroid/view/MotionEvent;

    #@1c
    .line 1429
    invoke-virtual {v0}, Landroid/view/MotionEvent;->prepareForReuse()V

    #@1f
    goto :goto_d

    #@20
    .line 1427
    :catchall_20
    move-exception v1

    #@21
    :try_start_21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method

.method public static obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;
    .registers 36
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "pressure"
    .parameter "size"
    .parameter "metaState"
    .parameter "xPrecision"
    .parameter "yPrecision"
    .parameter "deviceId"
    .parameter "edgeFlags"

    #@0
    .prologue
    .line 1554
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@3
    move-result-object v20

    #@4
    .line 1555
    .local v20, ev:Landroid/view/MotionEvent;
    sget-object v21, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@6
    monitor-enter v21

    #@7
    .line 1556
    const/4 v1, 0x1

    #@8
    :try_start_8
    invoke-static {v1}, Landroid/view/MotionEvent;->ensureSharedTempPointerCapacity(I)V

    #@b
    .line 1557
    sget-object v18, Landroid/view/MotionEvent;->gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@d
    .line 1558
    .local v18, pp:[Landroid/view/MotionEvent$PointerProperties;
    const/4 v1, 0x0

    #@e
    aget-object v1, v18, v1

    #@10
    invoke-virtual {v1}, Landroid/view/MotionEvent$PointerProperties;->clear()V

    #@13
    .line 1559
    const/4 v1, 0x0

    #@14
    aget-object v1, v18, v1

    #@16
    const/4 v2, 0x0

    #@17
    iput v2, v1, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@19
    .line 1561
    sget-object v19, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@1b
    .line 1562
    .local v19, pc:[Landroid/view/MotionEvent$PointerCoords;
    const/4 v1, 0x0

    #@1c
    aget-object v1, v19, v1

    #@1e
    invoke-virtual {v1}, Landroid/view/MotionEvent$PointerCoords;->clear()V

    #@21
    .line 1563
    const/4 v1, 0x0

    #@22
    aget-object v1, v19, v1

    #@24
    move/from16 v0, p5

    #@26
    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@28
    .line 1564
    const/4 v1, 0x0

    #@29
    aget-object v1, v19, v1

    #@2b
    move/from16 v0, p6

    #@2d
    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@2f
    .line 1565
    const/4 v1, 0x0

    #@30
    aget-object v1, v19, v1

    #@32
    move/from16 v0, p7

    #@34
    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@36
    .line 1566
    const/4 v1, 0x0

    #@37
    aget-object v1, v19, v1

    #@39
    move/from16 v0, p8

    #@3b
    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@3d
    .line 1568
    move-object/from16 v0, v20

    #@3f
    iget v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@41
    const/4 v3, 0x0

    #@42
    const/4 v5, 0x0

    #@43
    const/4 v8, 0x0

    #@44
    const/4 v9, 0x0

    #@45
    const/4 v10, 0x0

    #@46
    const-wide/32 v6, 0xf4240

    #@49
    mul-long v13, p0, v6

    #@4b
    const-wide/32 v6, 0xf4240

    #@4e
    mul-long v15, p2, v6

    #@50
    const/16 v17, 0x1

    #@52
    move/from16 v2, p12

    #@54
    move/from16 v4, p4

    #@56
    move/from16 v6, p13

    #@58
    move/from16 v7, p9

    #@5a
    move/from16 v11, p10

    #@5c
    move/from16 v12, p11

    #@5e
    invoke-static/range {v1 .. v19}, Landroid/view/MotionEvent;->nativeInitialize(IIIIIIIIFFFFJJI[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;)I

    #@61
    move-result v1

    #@62
    move-object/from16 v0, v20

    #@64
    iput v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@66
    .line 1573
    monitor-exit v21

    #@67
    return-object v20

    #@68
    .line 1574
    .end local v18           #pp:[Landroid/view/MotionEvent$PointerProperties;
    .end local v19           #pc:[Landroid/view/MotionEvent$PointerCoords;
    :catchall_68
    move-exception v1

    #@69
    monitor-exit v21
    :try_end_6a
    .catchall {:try_start_8 .. :try_end_6a} :catchall_68

    #@6a
    throw v1
.end method

.method public static obtain(JJIFFI)Landroid/view/MotionEvent;
    .registers 22
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "metaState"

    #@0
    .prologue
    .line 1635
    const/high16 v7, 0x3f80

    #@2
    const/high16 v8, 0x3f80

    #@4
    const/high16 v10, 0x3f80

    #@6
    const/high16 v11, 0x3f80

    #@8
    const/4 v12, 0x0

    #@9
    const/4 v13, 0x0

    #@a
    move-wide v0, p0

    #@b
    move-wide/from16 v2, p2

    #@d
    move/from16 v4, p4

    #@f
    move/from16 v5, p5

    #@11
    move/from16 v6, p6

    #@13
    move/from16 v9, p7

    #@15
    invoke-static/range {v0 .. v13}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    #@18
    move-result-object v0

    #@19
    return-object v0
.end method

.method public static obtain(JJIIFFFFIFFII)Landroid/view/MotionEvent;
    .registers 29
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "pointerCount"
    .parameter "x"
    .parameter "y"
    .parameter "pressure"
    .parameter "size"
    .parameter "metaState"
    .parameter "xPrecision"
    .parameter "yPrecision"
    .parameter "deviceId"
    .parameter "edgeFlags"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1614
    move-wide v0, p0

    #@1
    move-wide/from16 v2, p2

    #@3
    move/from16 v4, p4

    #@5
    move/from16 v5, p6

    #@7
    move/from16 v6, p7

    #@9
    move/from16 v7, p8

    #@b
    move/from16 v8, p9

    #@d
    move/from16 v9, p10

    #@f
    move/from16 v10, p11

    #@11
    move/from16 v11, p12

    #@13
    move/from16 v12, p13

    #@15
    move/from16 v13, p14

    #@17
    invoke-static/range {v0 .. v13}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public static obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;
    .registers 35
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "pointerCount"
    .parameter "pointerIds"
    .parameter "pointerCoords"
    .parameter "metaState"
    .parameter "xPrecision"
    .parameter "yPrecision"
    .parameter "deviceId"
    .parameter "edgeFlags"
    .parameter "source"
    .parameter "flags"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1509
    sget-object v19, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@2
    monitor-enter v19

    #@3
    .line 1510
    :try_start_3
    invoke-static/range {p5 .. p5}, Landroid/view/MotionEvent;->ensureSharedTempPointerCapacity(I)V

    #@6
    .line 1511
    sget-object v8, Landroid/view/MotionEvent;->gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@8
    .line 1512
    .local v8, pp:[Landroid/view/MotionEvent$PointerProperties;
    const/16 v18, 0x0

    #@a
    .local v18, i:I
    :goto_a
    move/from16 v0, v18

    #@c
    move/from16 v1, p5

    #@e
    if-ge v0, v1, :cond_1e

    #@10
    .line 1513
    aget-object v2, v8, v18

    #@12
    invoke-virtual {v2}, Landroid/view/MotionEvent$PointerProperties;->clear()V

    #@15
    .line 1514
    aget-object v2, v8, v18

    #@17
    aget v3, p6, v18

    #@19
    iput v3, v2, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@1b
    .line 1512
    add-int/lit8 v18, v18, 0x1

    #@1d
    goto :goto_a

    #@1e
    .line 1516
    :cond_1e
    const/4 v11, 0x0

    #@1f
    move-wide/from16 v2, p0

    #@21
    move-wide/from16 v4, p2

    #@23
    move/from16 v6, p4

    #@25
    move/from16 v7, p5

    #@27
    move-object/from16 v9, p7

    #@29
    move/from16 v10, p8

    #@2b
    move/from16 v12, p9

    #@2d
    move/from16 v13, p10

    #@2f
    move/from16 v14, p11

    #@31
    move/from16 v15, p12

    #@33
    move/from16 v16, p13

    #@35
    move/from16 v17, p14

    #@37
    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    #@3a
    move-result-object v2

    #@3b
    monitor-exit v19

    #@3c
    return-object v2

    #@3d
    .line 1519
    .end local v8           #pp:[Landroid/view/MotionEvent$PointerProperties;
    .end local v18           #i:I
    :catchall_3d
    move-exception v2

    #@3e
    monitor-exit v19
    :try_end_3f
    .catchall {:try_start_3 .. :try_end_3f} :catchall_3d

    #@3f
    throw v2
.end method

.method public static obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;
    .registers 37
    .parameter "downTime"
    .parameter "eventTime"
    .parameter "action"
    .parameter "pointerCount"
    .parameter "pointerProperties"
    .parameter "pointerCoords"
    .parameter "metaState"
    .parameter "buttonState"
    .parameter "xPrecision"
    .parameter "yPrecision"
    .parameter "deviceId"
    .parameter "edgeFlags"
    .parameter "source"
    .parameter "flags"

    #@0
    .prologue
    .line 1466
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@3
    move-result-object v20

    #@4
    .line 1467
    .local v20, ev:Landroid/view/MotionEvent;
    move-object/from16 v0, v20

    #@6
    iget v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@8
    const/4 v9, 0x0

    #@9
    const/4 v10, 0x0

    #@a
    const-wide/32 v2, 0xf4240

    #@d
    mul-long v13, p0, v2

    #@f
    const-wide/32 v2, 0xf4240

    #@12
    mul-long v15, p2, v2

    #@14
    move/from16 v2, p12

    #@16
    move/from16 v3, p14

    #@18
    move/from16 v4, p4

    #@1a
    move/from16 v5, p15

    #@1c
    move/from16 v6, p13

    #@1e
    move/from16 v7, p8

    #@20
    move/from16 v8, p9

    #@22
    move/from16 v11, p10

    #@24
    move/from16 v12, p11

    #@26
    move/from16 v17, p5

    #@28
    move-object/from16 v18, p6

    #@2a
    move-object/from16 v19, p7

    #@2c
    invoke-static/range {v1 .. v19}, Landroid/view/MotionEvent;->nativeInitialize(IIIIIIIIFFFFJJI[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;)I

    #@2f
    move-result v1

    #@30
    move-object/from16 v0, v20

    #@32
    iput v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@34
    .line 1472
    return-object v20
.end method

.method public static obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 5
    .parameter "other"

    #@0
    .prologue
    .line 1643
    if-nez p0, :cond_b

    #@2
    .line 1644
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "other motion event must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1647
    :cond_b
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@e
    move-result-object v0

    #@f
    .line 1648
    .local v0, ev:Landroid/view/MotionEvent;
    iget v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@11
    iget v2, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@13
    const/4 v3, 0x1

    #@14
    invoke-static {v1, v2, v3}, Landroid/view/MotionEvent;->nativeCopy(IIZ)I

    #@17
    move-result v1

    #@18
    iput v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@1a
    .line 1649
    return-object v0
.end method

.method public static obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .registers 5
    .parameter "other"

    #@0
    .prologue
    .line 1657
    if-nez p0, :cond_b

    #@2
    .line 1658
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "other motion event must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1661
    :cond_b
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@e
    move-result-object v0

    #@f
    .line 1662
    .local v0, ev:Landroid/view/MotionEvent;
    iget v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@11
    iget v2, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@13
    const/4 v3, 0x0

    #@14
    invoke-static {v1, v2, v3}, Landroid/view/MotionEvent;->nativeCopy(IIZ)I

    #@17
    move-result v1

    #@18
    iput v1, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@1a
    .line 1663
    return-object v0
.end method

.method public static toolTypeToString(I)Ljava/lang/String;
    .registers 3
    .parameter "toolType"

    #@0
    .prologue
    .line 3205
    sget-object v1, Landroid/view/MotionEvent;->TOOL_TYPE_SYMBOLIC_NAMES:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    .line 3206
    .local v0, symbolicName:Ljava/lang/String;
    if-eqz v0, :cond_b

    #@a
    .end local v0           #symbolicName:Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #symbolicName:Ljava/lang/String;
    :cond_b
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method


# virtual methods
.method public final addBatch(JFFFFI)V
    .registers 13
    .parameter "eventTime"
    .parameter "x"
    .parameter "y"
    .parameter "pressure"
    .parameter "size"
    .parameter "metaState"

    #@0
    .prologue
    .line 2806
    sget-object v2, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 2807
    const/4 v1, 0x1

    #@4
    :try_start_4
    invoke-static {v1}, Landroid/view/MotionEvent;->ensureSharedTempPointerCapacity(I)V

    #@7
    .line 2808
    sget-object v0, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@9
    .line 2809
    .local v0, pc:[Landroid/view/MotionEvent$PointerCoords;
    const/4 v1, 0x0

    #@a
    aget-object v1, v0, v1

    #@c
    invoke-virtual {v1}, Landroid/view/MotionEvent$PointerCoords;->clear()V

    #@f
    .line 2810
    const/4 v1, 0x0

    #@10
    aget-object v1, v0, v1

    #@12
    iput p3, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@14
    .line 2811
    const/4 v1, 0x0

    #@15
    aget-object v1, v0, v1

    #@17
    iput p4, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@19
    .line 2812
    const/4 v1, 0x0

    #@1a
    aget-object v1, v0, v1

    #@1c
    iput p5, v1, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@1e
    .line 2813
    const/4 v1, 0x0

    #@1f
    aget-object v1, v0, v1

    #@21
    iput p6, v1, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@23
    .line 2815
    iget v1, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@25
    const-wide/32 v3, 0xf4240

    #@28
    mul-long/2addr v3, p1

    #@29
    invoke-static {v1, v3, v4, v0, p7}, Landroid/view/MotionEvent;->nativeAddBatch(IJ[Landroid/view/MotionEvent$PointerCoords;I)V

    #@2c
    .line 2816
    monitor-exit v2

    #@2d
    .line 2817
    return-void

    #@2e
    .line 2816
    .end local v0           #pc:[Landroid/view/MotionEvent$PointerCoords;
    :catchall_2e
    move-exception v1

    #@2f
    monitor-exit v2
    :try_end_30
    .catchall {:try_start_4 .. :try_end_30} :catchall_2e

    #@30
    throw v1
.end method

.method public final addBatch(J[Landroid/view/MotionEvent$PointerCoords;I)V
    .registers 8
    .parameter "eventTime"
    .parameter "pointerCoords"
    .parameter "metaState"

    #@0
    .prologue
    .line 2831
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const-wide/32 v1, 0xf4240

    #@5
    mul-long/2addr v1, p1

    #@6
    invoke-static {v0, v1, v2, p3, p4}, Landroid/view/MotionEvent;->nativeAddBatch(IJ[Landroid/view/MotionEvent$PointerCoords;I)V

    #@9
    .line 2832
    return-void
.end method

.method public final addBatch(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "event"

    #@0
    .prologue
    .line 2847
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@5
    move-result v0

    #@6
    .line 2848
    .local v0, action:I
    const/4 v11, 0x2

    #@7
    if-eq v0, v11, :cond_e

    #@9
    const/4 v11, 0x7

    #@a
    if-eq v0, v11, :cond_e

    #@c
    .line 2849
    const/4 v11, 0x0

    #@d
    .line 2892
    :goto_d
    return v11

    #@e
    .line 2851
    :cond_e
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@10
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@13
    move-result v11

    #@14
    if-eq v0, v11, :cond_18

    #@16
    .line 2852
    const/4 v11, 0x0

    #@17
    goto :goto_d

    #@18
    .line 2855
    :cond_18
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@1a
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetDeviceId(I)I

    #@1d
    move-result v11

    #@1e
    iget v12, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@20
    invoke-static {v12}, Landroid/view/MotionEvent;->nativeGetDeviceId(I)I

    #@23
    move-result v12

    #@24
    if-ne v11, v12, :cond_42

    #@26
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@28
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetSource(I)I

    #@2b
    move-result v11

    #@2c
    iget v12, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@2e
    invoke-static {v12}, Landroid/view/MotionEvent;->nativeGetSource(I)I

    #@31
    move-result v12

    #@32
    if-ne v11, v12, :cond_42

    #@34
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@36
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetFlags(I)I

    #@39
    move-result v11

    #@3a
    iget v12, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@3c
    invoke-static {v12}, Landroid/view/MotionEvent;->nativeGetFlags(I)I

    #@3f
    move-result v12

    #@40
    if-eq v11, v12, :cond_44

    #@42
    .line 2858
    :cond_42
    const/4 v11, 0x0

    #@43
    goto :goto_d

    #@44
    .line 2861
    :cond_44
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@46
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@49
    move-result v9

    #@4a
    .line 2862
    .local v9, pointerCount:I
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@4c
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@4f
    move-result v11

    #@50
    if-eq v9, v11, :cond_54

    #@52
    .line 2863
    const/4 v11, 0x0

    #@53
    goto :goto_d

    #@54
    .line 2866
    :cond_54
    sget-object v12, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@56
    monitor-enter v12

    #@57
    .line 2867
    const/4 v11, 0x2

    #@58
    :try_start_58
    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    #@5b
    move-result v11

    #@5c
    invoke-static {v11}, Landroid/view/MotionEvent;->ensureSharedTempPointerCapacity(I)V

    #@5f
    .line 2868
    sget-object v10, Landroid/view/MotionEvent;->gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@61
    .line 2869
    .local v10, pp:[Landroid/view/MotionEvent$PointerProperties;
    sget-object v8, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@63
    .line 2871
    .local v8, pc:[Landroid/view/MotionEvent$PointerCoords;
    const/4 v6, 0x0

    #@64
    .local v6, i:I
    :goto_64
    if-ge v6, v9, :cond_8b

    #@66
    .line 2872
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@68
    const/4 v13, 0x0

    #@69
    aget-object v13, v10, v13

    #@6b
    invoke-static {v11, v6, v13}, Landroid/view/MotionEvent;->nativeGetPointerProperties(IILandroid/view/MotionEvent$PointerProperties;)V

    #@6e
    .line 2873
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@70
    const/4 v13, 0x1

    #@71
    aget-object v13, v10, v13

    #@73
    invoke-static {v11, v6, v13}, Landroid/view/MotionEvent;->nativeGetPointerProperties(IILandroid/view/MotionEvent$PointerProperties;)V

    #@76
    .line 2874
    const/4 v11, 0x0

    #@77
    aget-object v11, v10, v11

    #@79
    const/4 v13, 0x1

    #@7a
    aget-object v13, v10, v13

    #@7c
    invoke-static {v11, v13}, Landroid/view/MotionEvent$PointerProperties;->access$000(Landroid/view/MotionEvent$PointerProperties;Landroid/view/MotionEvent$PointerProperties;)Z

    #@7f
    move-result v11

    #@80
    if-nez v11, :cond_88

    #@82
    .line 2875
    const/4 v11, 0x0

    #@83
    monitor-exit v12

    #@84
    goto :goto_d

    #@85
    .line 2891
    .end local v6           #i:I
    .end local v8           #pc:[Landroid/view/MotionEvent$PointerCoords;
    .end local v10           #pp:[Landroid/view/MotionEvent$PointerProperties;
    :catchall_85
    move-exception v11

    #@86
    monitor-exit v12
    :try_end_87
    .catchall {:try_start_58 .. :try_end_87} :catchall_85

    #@87
    throw v11

    #@88
    .line 2871
    .restart local v6       #i:I
    .restart local v8       #pc:[Landroid/view/MotionEvent$PointerCoords;
    .restart local v10       #pp:[Landroid/view/MotionEvent$PointerProperties;
    :cond_88
    add-int/lit8 v6, v6, 0x1

    #@8a
    goto :goto_64

    #@8b
    .line 2879
    :cond_8b
    :try_start_8b
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@8d
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetMetaState(I)I

    #@90
    move-result v7

    #@91
    .line 2880
    .local v7, metaState:I
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@93
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetHistorySize(I)I

    #@96
    move-result v5

    #@97
    .line 2881
    .local v5, historySize:I
    const/4 v3, 0x0

    #@98
    .local v3, h:I
    :goto_98
    if-gt v3, v5, :cond_bb

    #@9a
    .line 2882
    if-ne v3, v5, :cond_ab

    #@9c
    const/high16 v4, -0x8000

    #@9e
    .line 2884
    .local v4, historyPos:I
    :goto_9e
    const/4 v6, 0x0

    #@9f
    :goto_9f
    if-ge v6, v9, :cond_ad

    #@a1
    .line 2885
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@a3
    aget-object v13, v8, v6

    #@a5
    invoke-static {v11, v6, v4, v13}, Landroid/view/MotionEvent;->nativeGetPointerCoords(IIILandroid/view/MotionEvent$PointerCoords;)V

    #@a8
    .line 2884
    add-int/lit8 v6, v6, 0x1

    #@aa
    goto :goto_9f

    #@ab
    .end local v4           #historyPos:I
    :cond_ab
    move v4, v3

    #@ac
    .line 2882
    goto :goto_9e

    #@ad
    .line 2888
    .restart local v4       #historyPos:I
    :cond_ad
    iget v11, p1, Landroid/view/MotionEvent;->mNativePtr:I

    #@af
    invoke-static {v11, v4}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@b2
    move-result-wide v1

    #@b3
    .line 2889
    .local v1, eventTimeNanos:J
    iget v11, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@b5
    invoke-static {v11, v1, v2, v8, v7}, Landroid/view/MotionEvent;->nativeAddBatch(IJ[Landroid/view/MotionEvent$PointerCoords;I)V

    #@b8
    .line 2881
    add-int/lit8 v3, v3, 0x1

    #@ba
    goto :goto_98

    #@bb
    .line 2891
    .end local v1           #eventTimeNanos:J
    .end local v4           #historyPos:I
    :cond_bb
    monitor-exit v12
    :try_end_bc
    .catchall {:try_start_8b .. :try_end_bc} :catchall_85

    #@bc
    .line 2892
    const/4 v11, 0x1

    #@bd
    goto/16 :goto_d
.end method

.method public final clampNoHistory(FFFF)Landroid/view/MotionEvent;
    .registers 29
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 2926
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@3
    move-result-object v21

    #@4
    .line 2927
    .local v21, ev:Landroid/view/MotionEvent;
    sget-object v23, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@6
    monitor-enter v23

    #@7
    .line 2928
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget v2, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@b
    invoke-static {v2}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@e
    move-result v18

    #@f
    .line 2930
    .local v18, pointerCount:I
    invoke-static/range {v18 .. v18}, Landroid/view/MotionEvent;->ensureSharedTempPointerCapacity(I)V

    #@12
    .line 2931
    sget-object v19, Landroid/view/MotionEvent;->gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@14
    .line 2932
    .local v19, pp:[Landroid/view/MotionEvent$PointerProperties;
    sget-object v20, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@16
    .line 2934
    .local v20, pc:[Landroid/view/MotionEvent$PointerCoords;
    const/16 v22, 0x0

    #@18
    .local v22, i:I
    :goto_18
    move/from16 v0, v22

    #@1a
    move/from16 v1, v18

    #@1c
    if-ge v0, v1, :cond_59

    #@1e
    .line 2935
    move-object/from16 v0, p0

    #@20
    iget v2, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@22
    aget-object v3, v19, v22

    #@24
    move/from16 v0, v22

    #@26
    invoke-static {v2, v0, v3}, Landroid/view/MotionEvent;->nativeGetPointerProperties(IILandroid/view/MotionEvent$PointerProperties;)V

    #@29
    .line 2936
    move-object/from16 v0, p0

    #@2b
    iget v2, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2d
    const/high16 v3, -0x8000

    #@2f
    aget-object v4, v20, v22

    #@31
    move/from16 v0, v22

    #@33
    invoke-static {v2, v0, v3, v4}, Landroid/view/MotionEvent;->nativeGetPointerCoords(IIILandroid/view/MotionEvent$PointerCoords;)V

    #@36
    .line 2937
    aget-object v2, v20, v22

    #@38
    aget-object v3, v20, v22

    #@3a
    iget v3, v3, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@3c
    move/from16 v0, p1

    #@3e
    move/from16 v1, p3

    #@40
    invoke-static {v3, v0, v1}, Landroid/view/MotionEvent;->clamp(FFF)F

    #@43
    move-result v3

    #@44
    iput v3, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@46
    .line 2938
    aget-object v2, v20, v22

    #@48
    aget-object v3, v20, v22

    #@4a
    iget v3, v3, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@4c
    move/from16 v0, p2

    #@4e
    move/from16 v1, p4

    #@50
    invoke-static {v3, v0, v1}, Landroid/view/MotionEvent;->clamp(FFF)F

    #@53
    move-result v3

    #@54
    iput v3, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@56
    .line 2934
    add-int/lit8 v22, v22, 0x1

    #@58
    goto :goto_18

    #@59
    .line 2940
    :cond_59
    move-object/from16 v0, v21

    #@5b
    iget v2, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@5d
    move-object/from16 v0, p0

    #@5f
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@61
    invoke-static {v3}, Landroid/view/MotionEvent;->nativeGetDeviceId(I)I

    #@64
    move-result v3

    #@65
    move-object/from16 v0, p0

    #@67
    iget v4, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@69
    invoke-static {v4}, Landroid/view/MotionEvent;->nativeGetSource(I)I

    #@6c
    move-result v4

    #@6d
    move-object/from16 v0, p0

    #@6f
    iget v5, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@71
    invoke-static {v5}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@74
    move-result v5

    #@75
    move-object/from16 v0, p0

    #@77
    iget v6, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@79
    invoke-static {v6}, Landroid/view/MotionEvent;->nativeGetFlags(I)I

    #@7c
    move-result v6

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget v7, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@81
    invoke-static {v7}, Landroid/view/MotionEvent;->nativeGetEdgeFlags(I)I

    #@84
    move-result v7

    #@85
    move-object/from16 v0, p0

    #@87
    iget v8, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@89
    invoke-static {v8}, Landroid/view/MotionEvent;->nativeGetMetaState(I)I

    #@8c
    move-result v8

    #@8d
    move-object/from16 v0, p0

    #@8f
    iget v9, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@91
    invoke-static {v9}, Landroid/view/MotionEvent;->nativeGetButtonState(I)I

    #@94
    move-result v9

    #@95
    move-object/from16 v0, p0

    #@97
    iget v10, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@99
    invoke-static {v10}, Landroid/view/MotionEvent;->nativeGetXOffset(I)F

    #@9c
    move-result v10

    #@9d
    move-object/from16 v0, p0

    #@9f
    iget v11, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@a1
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetYOffset(I)F

    #@a4
    move-result v11

    #@a5
    move-object/from16 v0, p0

    #@a7
    iget v12, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@a9
    invoke-static {v12}, Landroid/view/MotionEvent;->nativeGetXPrecision(I)F

    #@ac
    move-result v12

    #@ad
    move-object/from16 v0, p0

    #@af
    iget v13, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@b1
    invoke-static {v13}, Landroid/view/MotionEvent;->nativeGetYPrecision(I)F

    #@b4
    move-result v13

    #@b5
    move-object/from16 v0, p0

    #@b7
    iget v14, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@b9
    invoke-static {v14}, Landroid/view/MotionEvent;->nativeGetDownTimeNanos(I)J

    #@bc
    move-result-wide v14

    #@bd
    move-object/from16 v0, p0

    #@bf
    iget v0, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@c1
    move/from16 v16, v0

    #@c3
    const/high16 v17, -0x8000

    #@c5
    invoke-static/range {v16 .. v17}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@c8
    move-result-wide v16

    #@c9
    invoke-static/range {v2 .. v20}, Landroid/view/MotionEvent;->nativeInitialize(IIIIIIIIFFFFJJI[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;)I

    #@cc
    move-result v2

    #@cd
    move-object/from16 v0, v21

    #@cf
    iput v2, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@d1
    .line 2950
    monitor-exit v23

    #@d2
    return-object v21

    #@d3
    .line 2951
    .end local v18           #pointerCount:I
    .end local v19           #pp:[Landroid/view/MotionEvent$PointerProperties;
    .end local v20           #pc:[Landroid/view/MotionEvent$PointerCoords;
    .end local v22           #i:I
    :catchall_d3
    move-exception v2

    #@d4
    monitor-exit v23
    :try_end_d5
    .catchall {:try_start_7 .. :try_end_d5} :catchall_d3

    #@d5
    throw v2
.end method

.method public bridge synthetic copy()Landroid/view/InputEvent;
    .registers 2

    #@0
    .prologue
    .line 168
    invoke-virtual {p0}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public copy()Landroid/view/MotionEvent;
    .registers 2

    #@0
    .prologue
    .line 1669
    invoke-static {p0}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 1409
    :try_start_0
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1410
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@6
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeDispose(I)V

    #@9
    .line 1411
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/view/MotionEvent;->mNativePtr:I
    :try_end_c
    .catchall {:try_start_0 .. :try_end_c} :catchall_10

    #@c
    .line 1414
    :cond_c
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@f
    .line 1416
    return-void

    #@10
    .line 1414
    :catchall_10
    move-exception v0

    #@11
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@14
    throw v0
.end method

.method public final findPointerIndex(I)I
    .registers 3
    .parameter "pointerId"

    #@0
    .prologue
    .line 2007
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeFindPointerIndex(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getAction()I
    .registers 2

    #@0
    .prologue
    .line 1732
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getActionIndex()I
    .registers 3

    #@0
    .prologue
    .line 1755
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@5
    move-result v0

    #@6
    const v1, 0xff00

    #@9
    and-int/2addr v0, v1

    #@a
    shr-int/lit8 v0, v0, 0x8

    #@c
    return v0
.end method

.method public final getActionMasked()I
    .registers 2

    #@0
    .prologue
    .line 1741
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@5
    move-result v0

    #@6
    and-int/lit16 v0, v0, 0xff

    #@8
    return v0
.end method

.method public final getAxisValue(I)F
    .registers 5
    .parameter "axis"

    #@0
    .prologue
    .line 1945
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x0

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, p1, v1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getAxisValue(II)F
    .registers 5
    .parameter "axis"
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2167
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/high16 v1, -0x8000

    #@4
    invoke-static {v0, p1, p2, v1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getButtonState()I
    .registers 2

    #@0
    .prologue
    .line 2225
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetButtonState(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getDeviceId()I
    .registers 2

    #@0
    .prologue
    .line 1709
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetDeviceId(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getDownTime()J
    .registers 5

    #@0
    .prologue
    .line 1801
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetDownTimeNanos(I)J

    #@5
    move-result-wide v0

    #@6
    const-wide/32 v2, 0xf4240

    #@9
    div-long/2addr v0, v2

    #@a
    return-wide v0
.end method

.method public final getEdgeFlags()I
    .registers 2

    #@0
    .prologue
    .line 2733
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetEdgeFlags(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getEventTime()J
    .registers 5

    #@0
    .prologue
    .line 1823
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/high16 v1, -0x8000

    #@4
    invoke-static {v0, v1}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@7
    move-result-wide v0

    #@8
    const-wide/32 v2, 0xf4240

    #@b
    div-long/2addr v0, v2

    #@c
    return-wide v0
.end method

.method public final getEventTimeNano()J
    .registers 3

    #@0
    .prologue
    .line 1842
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/high16 v1, -0x8000

    #@4
    invoke-static {v0, v1}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public final getFlags()I
    .registers 2

    #@0
    .prologue
    .line 1779
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetFlags(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getHistoricalAxisValue(II)F
    .registers 5
    .parameter "axis"
    .parameter "pos"

    #@0
    .prologue
    .line 2515
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, p1, v1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalAxisValue(III)F
    .registers 5
    .parameter "axis"
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2696
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getHistoricalEventTime(I)J
    .registers 6
    .parameter "pos"

    #@0
    .prologue
    .line 2337
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@5
    move-result-wide v0

    #@6
    const-wide/32 v2, 0xf4240

    #@9
    div-long/2addr v0, v2

    #@a
    return-wide v0
.end method

.method public final getHistoricalEventTimeNano(I)J
    .registers 4
    .parameter "pos"

    #@0
    .prologue
    .line 2363
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public final getHistoricalOrientation(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2498
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/16 v1, 0x8

    #@4
    const/4 v2, 0x0

    #@5
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getHistoricalOrientation(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2677
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/16 v1, 0x8

    #@4
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalPointerCoords(IILandroid/view/MotionEvent$PointerCoords;)V
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"
    .parameter "outPointerCoords"

    #@0
    .prologue
    .line 2717
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/view/MotionEvent;->nativeGetPointerCoords(IIILandroid/view/MotionEvent$PointerCoords;)V

    #@5
    .line 2718
    return-void
.end method

.method public final getHistoricalPressure(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2408
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalPressure(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2569
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x2

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalSize(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2423
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalSize(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2587
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x3

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalToolMajor(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2468
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x6

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalToolMajor(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2641
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x6

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalToolMinor(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2483
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x7

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalToolMinor(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2659
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x7

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalTouchMajor(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2438
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x4

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalTouchMajor(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2605
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x4

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalTouchMinor(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2453
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x5

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalTouchMinor(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2623
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x5

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalX(I)F
    .registers 4
    .parameter "pos"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2378
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@3
    invoke-static {v0, v1, v1, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalX(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2533
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistoricalY(I)F
    .registers 5
    .parameter "pos"

    #@0
    .prologue
    .line 2393
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-static {v0, v1, v2, p1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final getHistoricalY(II)F
    .registers 5
    .parameter "pointerIndex"
    .parameter "pos"

    #@0
    .prologue
    .line 2551
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1, p1, p2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getHistorySize()I
    .registers 2

    #@0
    .prologue
    .line 2317
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetHistorySize(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getMetaState()I
    .registers 2

    #@0
    .prologue
    .line 2210
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetMetaState(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getOrientation()F
    .registers 5

    #@0
    .prologue
    .line 1932
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/16 v1, 0x8

    #@4
    const/4 v2, 0x0

    #@5
    const/high16 v3, -0x8000

    #@7
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public final getOrientation(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2151
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/16 v1, 0x8

    #@4
    const/high16 v2, -0x8000

    #@6
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V
    .registers 5
    .parameter "pointerIndex"
    .parameter "outPointerCoords"

    #@0
    .prologue
    .line 2181
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/high16 v1, -0x8000

    #@4
    invoke-static {v0, p1, v1, p2}, Landroid/view/MotionEvent;->nativeGetPointerCoords(IIILandroid/view/MotionEvent$PointerCoords;)V

    #@7
    .line 2182
    return-void
.end method

.method public final getPointerCount()I
    .registers 2

    #@0
    .prologue
    .line 1953
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getPointerId(I)I
    .registers 3
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 1965
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeGetPointerId(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getPointerIdBits()I
    .registers 6

    #@0
    .prologue
    .line 2959
    const/4 v1, 0x0

    #@1
    .line 2960
    .local v1, idBits:I
    iget v3, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@3
    invoke-static {v3}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@6
    move-result v2

    #@7
    .line 2961
    .local v2, pointerCount:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v2, :cond_16

    #@a
    .line 2962
    const/4 v3, 0x1

    #@b
    iget v4, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@d
    invoke-static {v4, v0}, Landroid/view/MotionEvent;->nativeGetPointerId(II)I

    #@10
    move-result v4

    #@11
    shl-int/2addr v3, v4

    #@12
    or-int/2addr v1, v3

    #@13
    .line 2961
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_8

    #@16
    .line 2964
    :cond_16
    return v1
.end method

.method public final getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V
    .registers 4
    .parameter "pointerIndex"
    .parameter "outPointerProperties"

    #@0
    .prologue
    .line 2196
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/view/MotionEvent;->nativeGetPointerProperties(IILandroid/view/MotionEvent$PointerProperties;)V

    #@5
    .line 2197
    return-void
.end method

.method public final getPressure()F
    .registers 5

    #@0
    .prologue
    .line 1872
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getPressure(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2054
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x2

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getRawX()F
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2238
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@3
    const/high16 v1, -0x8000

    #@5
    invoke-static {v0, v2, v2, v1}, Landroid/view/MotionEvent;->nativeGetRawAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getRawX(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2266
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x0

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetRawAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getRawY()F
    .registers 5

    #@0
    .prologue
    .line 2251
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetRawAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getRawY(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2280
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x1

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetRawAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getSize()F
    .registers 5

    #@0
    .prologue
    .line 1882
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getSize(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2072
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x3

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getSource()I
    .registers 2

    #@0
    .prologue
    .line 1715
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetSource(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getToolMajor()F
    .registers 5

    #@0
    .prologue
    .line 1912
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x6

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getToolMajor(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2116
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x6

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getToolMinor()F
    .registers 5

    #@0
    .prologue
    .line 1922
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x7

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getToolMinor(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2132
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x7

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getToolType(I)I
    .registers 3
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 1995
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeGetToolType(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getTouchMajor()F
    .registers 5

    #@0
    .prologue
    .line 1892
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x4

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getTouchMajor(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2086
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x4

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getTouchMinor()F
    .registers 5

    #@0
    .prologue
    .line 1902
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x5

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getTouchMinor(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2100
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x5

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getX()F
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1852
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@3
    const/high16 v1, -0x8000

    #@5
    invoke-static {v0, v2, v2, v1}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getX(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2022
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x0

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getXPrecision()F
    .registers 2

    #@0
    .prologue
    .line 2293
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetXPrecision(I)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getY()F
    .registers 5

    #@0
    .prologue
    .line 1862
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    const/high16 v3, -0x8000

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final getY(I)F
    .registers 5
    .parameter "pointerIndex"

    #@0
    .prologue
    .line 2037
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const/4 v1, 0x1

    #@3
    const/high16 v2, -0x8000

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public final getYPrecision()F
    .registers 2

    #@0
    .prologue
    .line 2305
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeGetYPrecision(I)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final isTainted()Z
    .registers 3

    #@0
    .prologue
    .line 1785
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getFlags()I

    #@3
    move-result v0

    #@4
    .line 1786
    .local v0, flags:I
    const/high16 v1, -0x8000

    #@6
    and-int/2addr v1, v0

    #@7
    if-eqz v1, :cond_b

    #@9
    const/4 v1, 0x1

    #@a
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public final isTouchEvent()Z
    .registers 2

    #@0
    .prologue
    .line 1770
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/view/MotionEvent;->nativeIsTouchEvent(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final isWithinBoundsNoHistory(FFFF)Z
    .registers 13
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/high16 v7, -0x8000

    #@4
    .line 2901
    iget v6, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@6
    invoke-static {v6}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@9
    move-result v1

    #@a
    .line 2902
    .local v1, pointerCount:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v1, :cond_2d

    #@d
    .line 2903
    iget v6, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@f
    invoke-static {v6, v4, v0, v7}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@12
    move-result v2

    #@13
    .line 2904
    .local v2, x:F
    iget v6, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@15
    invoke-static {v6, v5, v0, v7}, Landroid/view/MotionEvent;->nativeGetAxisValue(IIII)F

    #@18
    move-result v3

    #@19
    .line 2905
    .local v3, y:F
    cmpg-float v6, v2, p1

    #@1b
    if-ltz v6, :cond_29

    #@1d
    cmpl-float v6, v2, p3

    #@1f
    if-gtz v6, :cond_29

    #@21
    cmpg-float v6, v3, p2

    #@23
    if-ltz v6, :cond_29

    #@25
    cmpl-float v6, v3, p4

    #@27
    if-lez v6, :cond_2a

    #@29
    .line 2909
    .end local v2           #x:F
    .end local v3           #y:F
    :cond_29
    :goto_29
    return v4

    #@2a
    .line 2902
    .restart local v2       #x:F
    .restart local v3       #y:F
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_b

    #@2d
    .end local v2           #x:F
    .end local v3           #y:F
    :cond_2d
    move v4, v5

    #@2e
    .line 2909
    goto :goto_29
.end method

.method public final offsetLocation(FF)V
    .registers 5
    .parameter "deltaX"
    .parameter "deltaY"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2759
    cmpl-float v0, p1, v1

    #@3
    if-nez v0, :cond_9

    #@5
    cmpl-float v0, p2, v1

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 2760
    :cond_9
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@b
    invoke-static {v0, p1, p2}, Landroid/view/MotionEvent;->nativeOffsetLocation(IFF)V

    #@e
    .line 2762
    :cond_e
    return-void
.end method

.method public final recycle()V
    .registers 4

    #@0
    .prologue
    .line 1678
    invoke-super {p0}, Landroid/view/InputEvent;->recycle()V

    #@3
    .line 1680
    sget-object v1, Landroid/view/MotionEvent;->gRecyclerLock:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 1681
    :try_start_6
    sget v0, Landroid/view/MotionEvent;->gRecyclerUsed:I

    #@8
    const/16 v2, 0xa

    #@a
    if-ge v0, v2, :cond_18

    #@c
    .line 1682
    sget v0, Landroid/view/MotionEvent;->gRecyclerUsed:I

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    sput v0, Landroid/view/MotionEvent;->gRecyclerUsed:I

    #@12
    .line 1683
    sget-object v0, Landroid/view/MotionEvent;->gRecyclerTop:Landroid/view/MotionEvent;

    #@14
    iput-object v0, p0, Landroid/view/MotionEvent;->mNext:Landroid/view/MotionEvent;

    #@16
    .line 1684
    sput-object p0, Landroid/view/MotionEvent;->gRecyclerTop:Landroid/view/MotionEvent;

    #@18
    .line 1686
    :cond_18
    monitor-exit v1

    #@19
    .line 1687
    return-void

    #@1a
    .line 1686
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_6 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public final scale(F)V
    .registers 3
    .parameter "scale"

    #@0
    .prologue
    .line 1701
    const/high16 v0, 0x3f80

    #@2
    cmpl-float v0, p1, v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 1702
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@8
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeScale(IF)V

    #@b
    .line 1704
    :cond_b
    return-void
.end method

.method public final setAction(I)V
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 2750
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeSetAction(II)V

    #@5
    .line 2751
    return-void
.end method

.method public final setDownTime(J)V
    .registers 6
    .parameter "downTime"

    #@0
    .prologue
    .line 1811
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    const-wide/32 v1, 0xf4240

    #@5
    mul-long/2addr v1, p1

    #@6
    invoke-static {v0, v1, v2}, Landroid/view/MotionEvent;->nativeSetDownTimeNanos(IJ)V

    #@9
    .line 1812
    return-void
.end method

.method public final setEdgeFlags(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 2743
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeSetEdgeFlags(II)V

    #@5
    .line 2744
    return-void
.end method

.method public final setLocation(FF)V
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 2772
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v0

    #@4
    .line 2773
    .local v0, oldX:F
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    #@7
    move-result v1

    #@8
    .line 2774
    .local v1, oldY:F
    sub-float v2, p1, v0

    #@a
    sub-float v3, p2, v1

    #@c
    invoke-virtual {p0, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    #@f
    .line 2775
    return-void
.end method

.method public final setPointerId(II)V
    .registers 4
    .parameter "pointerIndex"
    .parameter "pointerID"

    #@0
    .prologue
    .line 1974
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/view/MotionEvent;->nativeSetPointerId(III)V

    #@5
    .line 1975
    return-void
.end method

.method public final setSource(I)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 1721
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeSetSource(II)I

    #@5
    .line 1722
    return-void
.end method

.method public final setTainted(Z)V
    .registers 5
    .parameter "tainted"

    #@0
    .prologue
    .line 1792
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getFlags()I

    #@3
    move-result v0

    #@4
    .line 1793
    .local v0, flags:I
    iget v2, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@6
    if-eqz p1, :cond_f

    #@8
    const/high16 v1, -0x8000

    #@a
    or-int/2addr v1, v0

    #@b
    :goto_b
    invoke-static {v2, v1}, Landroid/view/MotionEvent;->nativeSetFlags(II)V

    #@e
    .line 1794
    return-void

    #@f
    .line 1793
    :cond_f
    const v1, 0x7fffffff

    #@12
    and-int/2addr v1, v0

    #@13
    goto :goto_b
.end method

.method public final split(I)Landroid/view/MotionEvent;
    .registers 38
    .parameter "idBits"

    #@0
    .prologue
    .line 2972
    invoke-static {}, Landroid/view/MotionEvent;->obtain()Landroid/view/MotionEvent;

    #@3
    move-result-object v22

    #@4
    .line 2973
    .local v22, ev:Landroid/view/MotionEvent;
    sget-object v35, Landroid/view/MotionEvent;->gSharedTempLock:Ljava/lang/Object;

    #@6
    monitor-enter v35

    #@7
    .line 2974
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@b
    invoke-static {v3}, Landroid/view/MotionEvent;->nativeGetPointerCount(I)I

    #@e
    move-result v34

    #@f
    .line 2975
    .local v34, oldPointerCount:I
    invoke-static/range {v34 .. v34}, Landroid/view/MotionEvent;->ensureSharedTempPointerCapacity(I)V

    #@12
    .line 2976
    sget-object v20, Landroid/view/MotionEvent;->gSharedTempPointerProperties:[Landroid/view/MotionEvent$PointerProperties;

    #@14
    .line 2977
    .local v20, pp:[Landroid/view/MotionEvent$PointerProperties;
    sget-object v21, Landroid/view/MotionEvent;->gSharedTempPointerCoords:[Landroid/view/MotionEvent$PointerCoords;

    #@16
    .line 2978
    .local v21, pc:[Landroid/view/MotionEvent$PointerCoords;
    sget-object v28, Landroid/view/MotionEvent;->gSharedTempPointerIndexMap:[I

    #@18
    .line 2980
    .local v28, map:[I
    move-object/from16 v0, p0

    #@1a
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@1c
    invoke-static {v3}, Landroid/view/MotionEvent;->nativeGetAction(I)I

    #@1f
    move-result v31

    #@20
    .line 2981
    .local v31, oldAction:I
    move/from16 v0, v31

    #@22
    and-int/lit16 v0, v0, 0xff

    #@24
    move/from16 v32, v0

    #@26
    .line 2982
    .local v32, oldActionMasked:I
    const v3, 0xff00

    #@29
    and-int v3, v3, v31

    #@2b
    shr-int/lit8 v33, v3, 0x8

    #@2d
    .line 2984
    .local v33, oldActionPointerIndex:I
    const/16 v29, -0x1

    #@2f
    .line 2985
    .local v29, newActionPointerIndex:I
    const/16 v19, 0x0

    #@31
    .line 2986
    .local v19, newPointerCount:I
    const/16 v30, 0x0

    #@33
    .line 2987
    .local v30, newIdBits:I
    const/16 v26, 0x0

    #@35
    .local v26, i:I
    :goto_35
    move/from16 v0, v26

    #@37
    move/from16 v1, v34

    #@39
    if-ge v0, v1, :cond_62

    #@3b
    .line 2988
    move-object/from16 v0, p0

    #@3d
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@3f
    aget-object v4, v20, v19

    #@41
    move/from16 v0, v26

    #@43
    invoke-static {v3, v0, v4}, Landroid/view/MotionEvent;->nativeGetPointerProperties(IILandroid/view/MotionEvent$PointerProperties;)V

    #@46
    .line 2989
    const/4 v3, 0x1

    #@47
    aget-object v4, v20, v19

    #@49
    iget v4, v4, Landroid/view/MotionEvent$PointerProperties;->id:I

    #@4b
    shl-int v27, v3, v4

    #@4d
    .line 2990
    .local v27, idBit:I
    and-int v3, v27, p1

    #@4f
    if-eqz v3, :cond_5f

    #@51
    .line 2991
    move/from16 v0, v26

    #@53
    move/from16 v1, v33

    #@55
    if-ne v0, v1, :cond_59

    #@57
    .line 2992
    move/from16 v29, v19

    #@59
    .line 2994
    :cond_59
    aput v26, v28, v19

    #@5b
    .line 2995
    add-int/lit8 v19, v19, 0x1

    #@5d
    .line 2996
    or-int v30, v30, v27

    #@5f
    .line 2987
    :cond_5f
    add-int/lit8 v26, v26, 0x1

    #@61
    goto :goto_35

    #@62
    .line 3000
    .end local v27           #idBit:I
    :cond_62
    if-nez v19, :cond_68

    #@64
    .line 3003
    const/16 v22, 0x0

    #@66
    monitor-exit v35

    #@67
    .line 3048
    .end local v22           #ev:Landroid/view/MotionEvent;
    :goto_67
    return-object v22

    #@68
    .line 3007
    .restart local v22       #ev:Landroid/view/MotionEvent;
    :cond_68
    const/4 v3, 0x5

    #@69
    move/from16 v0, v32

    #@6b
    if-eq v0, v3, :cond_72

    #@6d
    const/4 v3, 0x6

    #@6e
    move/from16 v0, v32

    #@70
    if-ne v0, v3, :cond_b8

    #@72
    .line 3008
    :cond_72
    if-gez v29, :cond_a5

    #@74
    .line 3010
    const/4 v6, 0x2

    #@75
    .line 3025
    .local v6, newAction:I
    :goto_75
    move-object/from16 v0, p0

    #@77
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@79
    invoke-static {v3}, Landroid/view/MotionEvent;->nativeGetHistorySize(I)I

    #@7c
    move-result v25

    #@7d
    .line 3026
    .local v25, historySize:I
    const/16 v23, 0x0

    #@7f
    .local v23, h:I
    :goto_7f
    move/from16 v0, v23

    #@81
    move/from16 v1, v25

    #@83
    if-gt v0, v1, :cond_142

    #@85
    .line 3027
    move/from16 v0, v23

    #@87
    move/from16 v1, v25

    #@89
    if-ne v0, v1, :cond_bb

    #@8b
    const/high16 v24, -0x8000

    #@8d
    .line 3029
    .local v24, historyPos:I
    :goto_8d
    const/16 v26, 0x0

    #@8f
    :goto_8f
    move/from16 v0, v26

    #@91
    move/from16 v1, v19

    #@93
    if-ge v0, v1, :cond_be

    #@95
    .line 3030
    move-object/from16 v0, p0

    #@97
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@99
    aget v4, v28, v26

    #@9b
    aget-object v5, v21, v26

    #@9d
    move/from16 v0, v24

    #@9f
    invoke-static {v3, v4, v0, v5}, Landroid/view/MotionEvent;->nativeGetPointerCoords(IIILandroid/view/MotionEvent$PointerCoords;)V

    #@a2
    .line 3029
    add-int/lit8 v26, v26, 0x1

    #@a4
    goto :goto_8f

    #@a5
    .line 3011
    .end local v6           #newAction:I
    .end local v23           #h:I
    .end local v24           #historyPos:I
    .end local v25           #historySize:I
    :cond_a5
    const/4 v3, 0x1

    #@a6
    move/from16 v0, v19

    #@a8
    if-ne v0, v3, :cond_b3

    #@aa
    .line 3013
    const/4 v3, 0x5

    #@ab
    move/from16 v0, v32

    #@ad
    if-ne v0, v3, :cond_b1

    #@af
    const/4 v6, 0x0

    #@b0
    .restart local v6       #newAction:I
    :goto_b0
    goto :goto_75

    #@b1
    .end local v6           #newAction:I
    :cond_b1
    const/4 v6, 0x1

    #@b2
    goto :goto_b0

    #@b3
    .line 3017
    :cond_b3
    shl-int/lit8 v3, v29, 0x8

    #@b5
    or-int v6, v32, v3

    #@b7
    .restart local v6       #newAction:I
    goto :goto_75

    #@b8
    .line 3022
    .end local v6           #newAction:I
    :cond_b8
    move/from16 v6, v31

    #@ba
    .restart local v6       #newAction:I
    goto :goto_75

    #@bb
    .restart local v23       #h:I
    .restart local v25       #historySize:I
    :cond_bb
    move/from16 v24, v23

    #@bd
    .line 3027
    goto :goto_8d

    #@be
    .line 3033
    .restart local v24       #historyPos:I
    :cond_be
    move-object/from16 v0, p0

    #@c0
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@c2
    move/from16 v0, v24

    #@c4
    invoke-static {v3, v0}, Landroid/view/MotionEvent;->nativeGetEventTimeNanos(II)J

    #@c7
    move-result-wide v17

    #@c8
    .line 3034
    .local v17, eventTimeNanos:J
    if-nez v23, :cond_132

    #@ca
    .line 3035
    move-object/from16 v0, v22

    #@cc
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@ce
    move-object/from16 v0, p0

    #@d0
    iget v4, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@d2
    invoke-static {v4}, Landroid/view/MotionEvent;->nativeGetDeviceId(I)I

    #@d5
    move-result v4

    #@d6
    move-object/from16 v0, p0

    #@d8
    iget v5, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@da
    invoke-static {v5}, Landroid/view/MotionEvent;->nativeGetSource(I)I

    #@dd
    move-result v5

    #@de
    move-object/from16 v0, p0

    #@e0
    iget v7, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@e2
    invoke-static {v7}, Landroid/view/MotionEvent;->nativeGetFlags(I)I

    #@e5
    move-result v7

    #@e6
    move-object/from16 v0, p0

    #@e8
    iget v8, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@ea
    invoke-static {v8}, Landroid/view/MotionEvent;->nativeGetEdgeFlags(I)I

    #@ed
    move-result v8

    #@ee
    move-object/from16 v0, p0

    #@f0
    iget v9, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@f2
    invoke-static {v9}, Landroid/view/MotionEvent;->nativeGetMetaState(I)I

    #@f5
    move-result v9

    #@f6
    move-object/from16 v0, p0

    #@f8
    iget v10, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@fa
    invoke-static {v10}, Landroid/view/MotionEvent;->nativeGetButtonState(I)I

    #@fd
    move-result v10

    #@fe
    move-object/from16 v0, p0

    #@100
    iget v11, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@102
    invoke-static {v11}, Landroid/view/MotionEvent;->nativeGetXOffset(I)F

    #@105
    move-result v11

    #@106
    move-object/from16 v0, p0

    #@108
    iget v12, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@10a
    invoke-static {v12}, Landroid/view/MotionEvent;->nativeGetYOffset(I)F

    #@10d
    move-result v12

    #@10e
    move-object/from16 v0, p0

    #@110
    iget v13, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@112
    invoke-static {v13}, Landroid/view/MotionEvent;->nativeGetXPrecision(I)F

    #@115
    move-result v13

    #@116
    move-object/from16 v0, p0

    #@118
    iget v14, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@11a
    invoke-static {v14}, Landroid/view/MotionEvent;->nativeGetYPrecision(I)F

    #@11d
    move-result v14

    #@11e
    move-object/from16 v0, p0

    #@120
    iget v15, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@122
    invoke-static {v15}, Landroid/view/MotionEvent;->nativeGetDownTimeNanos(I)J

    #@125
    move-result-wide v15

    #@126
    invoke-static/range {v3 .. v21}, Landroid/view/MotionEvent;->nativeInitialize(IIIIIIIIFFFFJJI[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;)I

    #@129
    move-result v3

    #@12a
    move-object/from16 v0, v22

    #@12c
    iput v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@12e
    .line 3026
    :goto_12e
    add-int/lit8 v23, v23, 0x1

    #@130
    goto/16 :goto_7f

    #@132
    .line 3045
    :cond_132
    move-object/from16 v0, v22

    #@134
    iget v3, v0, Landroid/view/MotionEvent;->mNativePtr:I

    #@136
    const/4 v4, 0x0

    #@137
    move-wide/from16 v0, v17

    #@139
    move-object/from16 v2, v21

    #@13b
    invoke-static {v3, v0, v1, v2, v4}, Landroid/view/MotionEvent;->nativeAddBatch(IJ[Landroid/view/MotionEvent$PointerCoords;I)V

    #@13e
    goto :goto_12e

    #@13f
    .line 3049
    .end local v6           #newAction:I
    .end local v17           #eventTimeNanos:J
    .end local v19           #newPointerCount:I
    .end local v20           #pp:[Landroid/view/MotionEvent$PointerProperties;
    .end local v21           #pc:[Landroid/view/MotionEvent$PointerCoords;
    .end local v22           #ev:Landroid/view/MotionEvent;
    .end local v23           #h:I
    .end local v24           #historyPos:I
    .end local v25           #historySize:I
    .end local v26           #i:I
    .end local v28           #map:[I
    .end local v29           #newActionPointerIndex:I
    .end local v30           #newIdBits:I
    .end local v31           #oldAction:I
    .end local v32           #oldActionMasked:I
    .end local v33           #oldActionPointerIndex:I
    .end local v34           #oldPointerCount:I
    :catchall_13f
    move-exception v3

    #@140
    monitor-exit v35
    :try_end_141
    .catchall {:try_start_7 .. :try_end_141} :catchall_13f

    #@141
    throw v3

    #@142
    .line 3048
    .restart local v6       #newAction:I
    .restart local v19       #newPointerCount:I
    .restart local v20       #pp:[Landroid/view/MotionEvent$PointerProperties;
    .restart local v21       #pc:[Landroid/view/MotionEvent$PointerCoords;
    .restart local v22       #ev:Landroid/view/MotionEvent;
    .restart local v23       #h:I
    .restart local v25       #historySize:I
    .restart local v26       #i:I
    .restart local v28       #map:[I
    .restart local v29       #newActionPointerIndex:I
    .restart local v30       #newIdBits:I
    .restart local v31       #oldAction:I
    .restart local v32       #oldActionMasked:I
    .restart local v33       #oldActionPointerIndex:I
    .restart local v34       #oldPointerCount:I
    :cond_142
    :try_start_142
    monitor-exit v35
    :try_end_143
    .catchall {:try_start_142 .. :try_end_143} :catchall_13f

    #@143
    goto/16 :goto_67
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 3054
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 3055
    .local v1, msg:Ljava/lang/StringBuilder;
    const-string v3, "MotionEvent { action="

    #@7
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getAction()I

    #@e
    move-result v4

    #@f
    invoke-static {v4}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 3057
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    #@19
    move-result v2

    #@1a
    .line 3058
    .local v2, pointerCount:I
    const/4 v0, 0x0

    #@1b
    .local v0, i:I
    :goto_1b
    if-ge v0, v2, :cond_80

    #@1d
    .line 3059
    const-string v3, ", id["

    #@1f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "]="

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@30
    move-result v4

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    .line 3060
    const-string v3, ", x["

    #@36
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    const-string v4, "]="

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@47
    move-result v4

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4b
    .line 3061
    const-string v3, ", y["

    #@4d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    const-string v4, "]="

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@5e
    move-result v4

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@62
    .line 3062
    const-string v3, ", toolType["

    #@64
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    const-string v4, "]="

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    #@75
    move-result v4

    #@76
    invoke-static {v4}, Landroid/view/MotionEvent;->toolTypeToString(I)Ljava/lang/String;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 3058
    add-int/lit8 v0, v0, 0x1

    #@7f
    goto :goto_1b

    #@80
    .line 3066
    :cond_80
    const-string v3, ", buttonState="

    #@82
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getButtonState()I

    #@89
    move-result v4

    #@8a
    invoke-static {v4}, Landroid/view/MotionEvent;->buttonStateToString(I)Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 3067
    const-string v3, ", metaState="

    #@93
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getMetaState()I

    #@9a
    move-result v4

    #@9b
    invoke-static {v4}, Landroid/view/KeyEvent;->metaStateToString(I)Ljava/lang/String;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    .line 3068
    const-string v3, ", flags=0x"

    #@a4
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v3

    #@a8
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getFlags()I

    #@ab
    move-result v4

    #@ac
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@af
    move-result-object v4

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 3069
    const-string v3, ", edgeFlags=0x"

    #@b5
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v3

    #@b9
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getEdgeFlags()I

    #@bc
    move-result v4

    #@bd
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    .line 3070
    const-string v3, ", pointerCount="

    #@c6
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cd
    .line 3071
    const-string v3, ", historySize="

    #@cf
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v3

    #@d3
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getHistorySize()I

    #@d6
    move-result v4

    #@d7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@da
    .line 3072
    const-string v3, ", eventTime="

    #@dc
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v3

    #@e0
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getEventTime()J

    #@e3
    move-result-wide v4

    #@e4
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e7
    .line 3073
    const-string v3, ", downTime="

    #@e9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getDownTime()J

    #@f0
    move-result-wide v4

    #@f1
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@f4
    .line 3074
    const-string v3, ", deviceId="

    #@f6
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v3

    #@fa
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getDeviceId()I

    #@fd
    move-result v4

    #@fe
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    .line 3075
    const-string v3, ", source=0x"

    #@103
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v3

    #@107
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getSource()I

    #@10a
    move-result v4

    #@10b
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10e
    move-result-object v4

    #@10f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    .line 3076
    const-string v3, " }"

    #@114
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    .line 3077
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a
    move-result-object v3

    #@11b
    return-object v3
.end method

.method public final transform(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 2783
    if-nez p1, :cond_b

    #@2
    .line 2784
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "matrix must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 2787
    :cond_b
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@d
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeTransform(ILandroid/graphics/Matrix;)V

    #@10
    .line 2788
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 3229
    const/4 v0, 0x1

    #@1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4
    .line 3230
    iget v0, p0, Landroid/view/MotionEvent;->mNativePtr:I

    #@6
    invoke-static {v0, p1}, Landroid/view/MotionEvent;->nativeWriteToParcel(ILandroid/os/Parcel;)V

    #@9
    .line 3231
    return-void
.end method
