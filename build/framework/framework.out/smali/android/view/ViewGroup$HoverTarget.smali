.class final Landroid/view/ViewGroup$HoverTarget;
.super Ljava/lang/Object;
.source "ViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "HoverTarget"
.end annotation


# static fields
.field private static final MAX_RECYCLED:I = 0x20

.field private static sRecycleBin:Landroid/view/ViewGroup$HoverTarget;

.field private static final sRecycleLock:Ljava/lang/Object;

.field private static sRecycledCount:I


# instance fields
.field public child:Landroid/view/View;

.field public next:Landroid/view/ViewGroup$HoverTarget;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 6119
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/ViewGroup$HoverTarget;->sRecycleLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 6129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 6130
    return-void
.end method

.method public static obtain(Landroid/view/View;)Landroid/view/ViewGroup$HoverTarget;
    .registers 4
    .parameter "child"

    #@0
    .prologue
    .line 6134
    sget-object v2, Landroid/view/ViewGroup$HoverTarget;->sRecycleLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 6135
    :try_start_3
    sget-object v1, Landroid/view/ViewGroup$HoverTarget;->sRecycleBin:Landroid/view/ViewGroup$HoverTarget;

    #@5
    if-nez v1, :cond_10

    #@7
    .line 6136
    new-instance v0, Landroid/view/ViewGroup$HoverTarget;

    #@9
    invoke-direct {v0}, Landroid/view/ViewGroup$HoverTarget;-><init>()V

    #@c
    .line 6143
    .local v0, target:Landroid/view/ViewGroup$HoverTarget;
    :goto_c
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_20

    #@d
    .line 6144
    iput-object p0, v0, Landroid/view/ViewGroup$HoverTarget;->child:Landroid/view/View;

    #@f
    .line 6145
    return-object v0

    #@10
    .line 6138
    .end local v0           #target:Landroid/view/ViewGroup$HoverTarget;
    :cond_10
    :try_start_10
    sget-object v0, Landroid/view/ViewGroup$HoverTarget;->sRecycleBin:Landroid/view/ViewGroup$HoverTarget;

    #@12
    .line 6139
    .restart local v0       #target:Landroid/view/ViewGroup$HoverTarget;
    iget-object v1, v0, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@14
    sput-object v1, Landroid/view/ViewGroup$HoverTarget;->sRecycleBin:Landroid/view/ViewGroup$HoverTarget;

    #@16
    .line 6140
    sget v1, Landroid/view/ViewGroup$HoverTarget;->sRecycledCount:I

    #@18
    add-int/lit8 v1, v1, -0x1

    #@1a
    sput v1, Landroid/view/ViewGroup$HoverTarget;->sRecycledCount:I

    #@1c
    .line 6141
    const/4 v1, 0x0

    #@1d
    iput-object v1, v0, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@1f
    goto :goto_c

    #@20
    .line 6143
    .end local v0           #target:Landroid/view/ViewGroup$HoverTarget;
    :catchall_20
    move-exception v1

    #@21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_10 .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method


# virtual methods
.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 6149
    sget-object v1, Landroid/view/ViewGroup$HoverTarget;->sRecycleLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 6150
    :try_start_3
    sget v0, Landroid/view/ViewGroup$HoverTarget;->sRecycledCount:I

    #@5
    const/16 v2, 0x20

    #@7
    if-ge v0, v2, :cond_1a

    #@9
    .line 6151
    sget-object v0, Landroid/view/ViewGroup$HoverTarget;->sRecycleBin:Landroid/view/ViewGroup$HoverTarget;

    #@b
    iput-object v0, p0, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@d
    .line 6152
    sput-object p0, Landroid/view/ViewGroup$HoverTarget;->sRecycleBin:Landroid/view/ViewGroup$HoverTarget;

    #@f
    .line 6153
    sget v0, Landroid/view/ViewGroup$HoverTarget;->sRecycledCount:I

    #@11
    add-int/lit8 v0, v0, 0x1

    #@13
    sput v0, Landroid/view/ViewGroup$HoverTarget;->sRecycledCount:I

    #@15
    .line 6157
    :goto_15
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Landroid/view/ViewGroup$HoverTarget;->child:Landroid/view/View;

    #@18
    .line 6158
    monitor-exit v1

    #@19
    .line 6159
    return-void

    #@1a
    .line 6155
    :cond_1a
    const/4 v0, 0x0

    #@1b
    iput-object v0, p0, Landroid/view/ViewGroup$HoverTarget;->next:Landroid/view/ViewGroup$HoverTarget;

    #@1d
    goto :goto_15

    #@1e
    .line 6158
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method
