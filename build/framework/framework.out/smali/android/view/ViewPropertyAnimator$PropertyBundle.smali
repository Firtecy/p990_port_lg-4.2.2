.class Landroid/view/ViewPropertyAnimator$PropertyBundle;
.super Ljava/lang/Object;
.source "ViewPropertyAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewPropertyAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PropertyBundle"
.end annotation


# instance fields
.field mNameValuesHolder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/ViewPropertyAnimator$NameValuesHolder;",
            ">;"
        }
    .end annotation
.end field

.field mPropertyMask:I


# direct methods
.method constructor <init>(ILjava/util/ArrayList;)V
    .registers 3
    .parameter "propertyMask"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/ViewPropertyAnimator$NameValuesHolder;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 164
    .local p2, nameValuesHolder:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewPropertyAnimator$NameValuesHolder;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 165
    iput p1, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    #@5
    .line 166
    iput-object p2, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    #@7
    .line 167
    return-void
.end method


# virtual methods
.method cancel(I)Z
    .registers 7
    .parameter "propertyConstant"

    #@0
    .prologue
    .line 180
    iget v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    #@2
    and-int/2addr v3, p1

    #@3
    if-eqz v3, :cond_2f

    #@5
    iget-object v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    #@7
    if-eqz v3, :cond_2f

    #@9
    .line 181
    iget-object v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v0

    #@f
    .line 182
    .local v0, count:I
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    if-ge v1, v0, :cond_2f

    #@12
    .line 183
    iget-object v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Landroid/view/ViewPropertyAnimator$NameValuesHolder;

    #@1a
    .line 184
    .local v2, nameValuesHolder:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    iget v3, v2, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mNameConstant:I

    #@1c
    if-ne v3, p1, :cond_2c

    #@1e
    .line 185
    iget-object v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@23
    .line 186
    iget v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    #@25
    xor-int/lit8 v4, p1, -0x1

    #@27
    and-int/2addr v3, v4

    #@28
    iput v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    #@2a
    .line 187
    const/4 v3, 0x1

    #@2b
    .line 191
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #nameValuesHolder:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :goto_2b
    return v3

    #@2c
    .line 182
    .restart local v0       #count:I
    .restart local v1       #i:I
    .restart local v2       #nameValuesHolder:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_10

    #@2f
    .line 191
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #nameValuesHolder:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :cond_2f
    const/4 v3, 0x0

    #@30
    goto :goto_2b
.end method
