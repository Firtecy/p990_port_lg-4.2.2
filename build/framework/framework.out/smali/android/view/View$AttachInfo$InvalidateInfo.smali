.class Landroid/view/View$AttachInfo$InvalidateInfo;
.super Ljava/lang/Object;
.source "View.java"

# interfaces
.implements Landroid/util/Poolable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/View$AttachInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InvalidateInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/util/Poolable",
        "<",
        "Landroid/view/View$AttachInfo$InvalidateInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final POOL_LIMIT:I = 0xa

.field private static final sPool:Landroid/util/Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pool",
            "<",
            "Landroid/view/View$AttachInfo$InvalidateInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field bottom:I

.field left:I

.field private mIsPooled:Z

.field private mNext:Landroid/view/View$AttachInfo$InvalidateInfo;

.field right:I

.field target:Landroid/view/View;

.field top:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 17897
    new-instance v0, Landroid/view/View$AttachInfo$InvalidateInfo$1;

    #@2
    invoke-direct {v0}, Landroid/view/View$AttachInfo$InvalidateInfo$1;-><init>()V

    #@5
    const/16 v1, 0xa

    #@7
    invoke-static {v0, v1}, Landroid/util/Pools;->finitePool(Landroid/util/PoolableManager;I)Landroid/util/Pool;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/util/Pools;->synchronizedPool(Landroid/util/Pool;)Landroid/util/Pool;

    #@e
    move-result-object v0

    #@f
    sput-object v0, Landroid/view/View$AttachInfo$InvalidateInfo;->sPool:Landroid/util/Pool;

    #@11
    return-void
.end method

.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 17895
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static acquire()Landroid/view/View$AttachInfo$InvalidateInfo;
    .registers 1

    #@0
    .prologue
    .line 17931
    sget-object v0, Landroid/view/View$AttachInfo$InvalidateInfo;->sPool:Landroid/util/Pool;

    #@2
    invoke-interface {v0}, Landroid/util/Pool;->acquire()Landroid/util/Poolable;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/View$AttachInfo$InvalidateInfo;

    #@8
    return-object v0
.end method


# virtual methods
.method public getNextPoolable()Landroid/view/View$AttachInfo$InvalidateInfo;
    .registers 2

    #@0
    .prologue
    .line 17927
    iget-object v0, p0, Landroid/view/View$AttachInfo$InvalidateInfo;->mNext:Landroid/view/View$AttachInfo$InvalidateInfo;

    #@2
    return-object v0
.end method

.method public bridge synthetic getNextPoolable()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 17895
    invoke-virtual {p0}, Landroid/view/View$AttachInfo$InvalidateInfo;->getNextPoolable()Landroid/view/View$AttachInfo$InvalidateInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public isPooled()Z
    .registers 2

    #@0
    .prologue
    .line 17939
    iget-boolean v0, p0, Landroid/view/View$AttachInfo$InvalidateInfo;->mIsPooled:Z

    #@2
    return v0
.end method

.method release()V
    .registers 2

    #@0
    .prologue
    .line 17935
    sget-object v0, Landroid/view/View$AttachInfo$InvalidateInfo;->sPool:Landroid/util/Pool;

    #@2
    invoke-interface {v0, p0}, Landroid/util/Pool;->release(Landroid/util/Poolable;)V

    #@5
    .line 17936
    return-void
.end method

.method public setNextPoolable(Landroid/view/View$AttachInfo$InvalidateInfo;)V
    .registers 2
    .parameter "element"

    #@0
    .prologue
    .line 17923
    iput-object p1, p0, Landroid/view/View$AttachInfo$InvalidateInfo;->mNext:Landroid/view/View$AttachInfo$InvalidateInfo;

    #@2
    .line 17924
    return-void
.end method

.method public bridge synthetic setNextPoolable(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17895
    check-cast p1, Landroid/view/View$AttachInfo$InvalidateInfo;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/View$AttachInfo$InvalidateInfo;->setNextPoolable(Landroid/view/View$AttachInfo$InvalidateInfo;)V

    #@5
    return-void
.end method

.method public setPooled(Z)V
    .registers 2
    .parameter "isPooled"

    #@0
    .prologue
    .line 17943
    iput-boolean p1, p0, Landroid/view/View$AttachInfo$InvalidateInfo;->mIsPooled:Z

    #@2
    .line 17944
    return-void
.end method
