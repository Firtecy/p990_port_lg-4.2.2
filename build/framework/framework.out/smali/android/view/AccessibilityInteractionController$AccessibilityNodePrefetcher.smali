.class Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;
.super Ljava/lang/Object;
.source "AccessibilityInteractionController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/AccessibilityInteractionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccessibilityNodePrefetcher"
.end annotation


# static fields
.field private static final MAX_ACCESSIBILITY_NODE_INFO_BATCH_SIZE:I = 0x32


# instance fields
.field private final mTempViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/view/AccessibilityInteractionController;


# direct methods
.method private constructor <init>(Landroid/view/AccessibilityInteractionController;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 613
    iput-object p1, p0, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->this$0:Landroid/view/AccessibilityInteractionController;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 617
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->mTempViewList:Ljava/util/ArrayList;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/AccessibilityInteractionController;Landroid/view/AccessibilityInteractionController$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 613
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;-><init>(Landroid/view/AccessibilityInteractionController;)V

    #@3
    return-void
.end method

.method private prefetchDescendantsOfRealNode(Landroid/view/View;Ljava/util/List;)V
    .registers 16
    .parameter "root"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    const/16 v12, 0x32

    #@2
    .line 706
    instance-of v11, p1, Landroid/view/ViewGroup;

    #@4
    if-nez v11, :cond_7

    #@6
    .line 755
    :cond_6
    :goto_6
    return-void

    #@7
    .line 709
    :cond_7
    new-instance v1, Ljava/util/HashMap;

    #@9
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@c
    .line 711
    .local v1, addedChildren:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    iget-object v4, p0, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->mTempViewList:Ljava/util/ArrayList;

    #@e
    .line 712
    .local v4, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@11
    .line 714
    :try_start_11
    invoke-virtual {p1, v4}, Landroid/view/View;->addChildrenForAccessibility(Ljava/util/ArrayList;)V

    #@14
    .line 715
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v3

    #@18
    .line 716
    .local v3, childCount:I
    const/4 v6, 0x0

    #@19
    .local v6, i:I
    :goto_19
    if-ge v6, v3, :cond_5c

    #@1b
    .line 717
    invoke-interface {p2}, Ljava/util/List;->size()I
    :try_end_1e
    .catchall {:try_start_11 .. :try_end_1e} :catchall_57

    #@1e
    move-result v11

    #@1f
    if-lt v11, v12, :cond_25

    #@21
    .line 740
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@24
    goto :goto_6

    #@25
    .line 720
    :cond_25
    :try_start_25
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Landroid/view/View;

    #@2b
    .line 721
    .local v2, child:Landroid/view/View;
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->this$0:Landroid/view/AccessibilityInteractionController;

    #@2d
    invoke-static {v11, v2}, Landroid/view/AccessibilityInteractionController;->access$100(Landroid/view/AccessibilityInteractionController;Landroid/view/View;)Z

    #@30
    move-result v11

    #@31
    if-eqz v11, :cond_46

    #@33
    .line 722
    invoke-virtual {v2}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@36
    move-result-object v9

    #@37
    .line 723
    .local v9, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-nez v9, :cond_49

    #@39
    .line 724
    invoke-virtual {v2}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3c
    move-result-object v8

    #@3d
    .line 725
    .local v8, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v8, :cond_46

    #@3f
    .line 726
    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@42
    .line 727
    const/4 v11, 0x0

    #@43
    invoke-virtual {v1, v2, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    .line 716
    .end local v8           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v9           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_46
    :goto_46
    add-int/lit8 v6, v6, 0x1

    #@48
    goto :goto_19

    #@49
    .line 730
    .restart local v9       #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_49
    const/4 v11, -0x1

    #@4a
    invoke-virtual {v9, v11}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@4d
    move-result-object v8

    #@4e
    .line 732
    .restart local v8       #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v8, :cond_46

    #@50
    .line 733
    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@53
    .line 734
    invoke-virtual {v1, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_56
    .catchall {:try_start_25 .. :try_end_56} :catchall_57

    #@56
    goto :goto_46

    #@57
    .line 740
    .end local v2           #child:Landroid/view/View;
    .end local v3           #childCount:I
    .end local v6           #i:I
    .end local v8           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v9           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :catchall_57
    move-exception v11

    #@58
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@5b
    throw v11

    #@5c
    .restart local v3       #childCount:I
    .restart local v6       #i:I
    :cond_5c
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@5f
    .line 742
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@62
    move-result v11

    #@63
    if-ge v11, v12, :cond_6

    #@65
    .line 743
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@68
    move-result-object v11

    #@69
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@6c
    move-result-object v7

    #@6d
    .local v7, i$:Ljava/util/Iterator;
    :goto_6d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@70
    move-result v11

    #@71
    if-eqz v11, :cond_6

    #@73
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@76
    move-result-object v5

    #@77
    check-cast v5, Ljava/util/Map$Entry;

    #@79
    .line 744
    .local v5, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@7c
    move-result-object v0

    #@7d
    check-cast v0, Landroid/view/View;

    #@7f
    .line 745
    .local v0, addedChild:Landroid/view/View;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@82
    move-result-object v10

    #@83
    check-cast v10, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@85
    .line 746
    .local v10, virtualRoot:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v10, :cond_8b

    #@87
    .line 747
    invoke-direct {p0, v0, p2}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchDescendantsOfRealNode(Landroid/view/View;Ljava/util/List;)V

    #@8a
    goto :goto_6d

    #@8b
    .line 749
    :cond_8b
    invoke-virtual {v0}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@8e
    move-result-object v9

    #@8f
    .line 751
    .restart local v9       #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    invoke-direct {p0, v10, v9, p2}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchDescendantsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V

    #@92
    goto :goto_6d
.end method

.method private prefetchDescendantsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V
    .registers 14
    .parameter "root"
    .parameter "provider"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            "Landroid/view/accessibility/AccessibilityNodeProvider;",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    const/16 v9, 0x32

    #@2
    .line 822
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@5
    move-result-object v5

    #@6
    .line 823
    .local v5, childNodeIds:Landroid/util/SparseLongArray;
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@9
    move-result v7

    #@a
    .line 824
    .local v7, initialOutInfosSize:I
    invoke-virtual {v5}, Landroid/util/SparseLongArray;->size()I

    #@d
    move-result v2

    #@e
    .line 825
    .local v2, childCount:I
    const/4 v6, 0x0

    #@f
    .local v6, i:I
    :goto_f
    if-ge v6, v2, :cond_2c

    #@11
    .line 826
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@14
    move-result v8

    #@15
    if-lt v8, v9, :cond_18

    #@17
    .line 843
    :cond_17
    return-void

    #@18
    .line 829
    :cond_18
    invoke-virtual {v5, v6}, Landroid/util/SparseLongArray;->get(I)J

    #@1b
    move-result-wide v3

    #@1c
    .line 830
    .local v3, childNodeId:J
    invoke-static {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@1f
    move-result v8

    #@20
    invoke-virtual {p2, v8}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@23
    move-result-object v1

    #@24
    .line 832
    .local v1, child:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v1, :cond_29

    #@26
    .line 833
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@29
    .line 825
    :cond_29
    add-int/lit8 v6, v6, 0x1

    #@2b
    goto :goto_f

    #@2c
    .line 836
    .end local v1           #child:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v3           #childNodeId:J
    :cond_2c
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@2f
    move-result v8

    #@30
    if-ge v8, v9, :cond_17

    #@32
    .line 837
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@35
    move-result v8

    #@36
    sub-int v0, v8, v7

    #@38
    .line 838
    .local v0, addedChildCount:I
    const/4 v6, 0x0

    #@39
    :goto_39
    if-ge v6, v0, :cond_17

    #@3b
    .line 839
    add-int v8, v7, v6

    #@3d
    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@40
    move-result-object v1

    #@41
    check-cast v1, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@43
    .line 840
    .restart local v1       #child:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {p0, v1, p2, p3}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchDescendantsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V

    #@46
    .line 838
    add-int/lit8 v6, v6, 0x1

    #@48
    goto :goto_39
.end method

.method private prefetchPredecessorsOfRealNode(Landroid/view/View;Ljava/util/List;)V
    .registers 8
    .parameter "view"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 655
    .local p2, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-virtual {p1}, Landroid/view/View;->getParentForAccessibility()Landroid/view/ViewParent;

    #@3
    move-result-object v1

    #@4
    .line 657
    .local v1, parent:Landroid/view/ViewParent;
    :goto_4
    instance-of v3, v1, Landroid/view/View;

    #@6
    if-eqz v3, :cond_21

    #@8
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@b
    move-result v3

    #@c
    const/16 v4, 0x32

    #@e
    if-ge v3, v4, :cond_21

    #@10
    move-object v2, v1

    #@11
    .line 658
    check-cast v2, Landroid/view/View;

    #@13
    .line 659
    .local v2, parentView:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@16
    move-result-object v0

    #@17
    .line 660
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v0, :cond_1c

    #@19
    .line 661
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1c
    .line 663
    :cond_1c
    invoke-interface {v1}, Landroid/view/ViewParent;->getParentForAccessibility()Landroid/view/ViewParent;

    #@1f
    move-result-object v1

    #@20
    .line 664
    goto :goto_4

    #@21
    .line 665
    .end local v0           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v2           #parentView:Landroid/view/View;
    :cond_21
    return-void
.end method

.method private prefetchPredecessorsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V
    .registers 13
    .parameter "root"
    .parameter "providerHost"
    .parameter "provider"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            "Landroid/view/View;",
            "Landroid/view/accessibility/AccessibilityNodeProvider;",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p4, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    const/4 v7, -0x1

    #@1
    .line 760
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@4
    move-result-wide v2

    #@5
    .line 761
    .local v2, parentNodeId:J
    invoke-static {v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@8
    move-result v0

    #@9
    .line 762
    .local v0, accessibilityViewId:I
    :goto_9
    if-eq v0, v7, :cond_13

    #@b
    .line 763
    invoke-interface {p4}, Ljava/util/List;->size()I

    #@e
    move-result v5

    #@f
    const/16 v6, 0x32

    #@11
    if-lt v5, v6, :cond_14

    #@13
    .line 783
    :cond_13
    :goto_13
    return-void

    #@14
    .line 766
    :cond_14
    invoke-static {v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@17
    move-result v4

    #@18
    .line 768
    .local v4, virtualDescendantId:I
    if-ne v4, v7, :cond_20

    #@1a
    invoke-virtual {p2}, Landroid/view/View;->getAccessibilityViewId()I

    #@1d
    move-result v5

    #@1e
    if-ne v0, v5, :cond_32

    #@20
    .line 770
    :cond_20
    invoke-virtual {p3, v4}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@23
    move-result-object v1

    #@24
    .line 772
    .local v1, parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v1, :cond_29

    #@26
    .line 773
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@29
    .line 775
    :cond_29
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@2c
    move-result-wide v2

    #@2d
    .line 776
    invoke-static {v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@30
    move-result v0

    #@31
    .line 778
    goto :goto_9

    #@32
    .line 779
    .end local v1           #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_32
    invoke-direct {p0, p2, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchPredecessorsOfRealNode(Landroid/view/View;Ljava/util/List;)V

    #@35
    goto :goto_13
.end method

.method private prefetchSiblingsOfRealNode(Landroid/view/View;Ljava/util/List;)V
    .registers 13
    .parameter "current"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 669
    .local p2, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-virtual {p1}, Landroid/view/View;->getParentForAccessibility()Landroid/view/ViewParent;

    #@3
    move-result-object v5

    #@4
    .line 670
    .local v5, parent:Landroid/view/ViewParent;
    instance-of v8, v5, Landroid/view/ViewGroup;

    #@6
    if-eqz v8, :cond_25

    #@8
    move-object v6, v5

    #@9
    .line 671
    check-cast v6, Landroid/view/ViewGroup;

    #@b
    .line 672
    .local v6, parentGroup:Landroid/view/ViewGroup;
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->mTempViewList:Ljava/util/ArrayList;

    #@d
    .line 673
    .local v2, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@10
    .line 675
    :try_start_10
    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->addChildrenForAccessibility(Ljava/util/ArrayList;)V

    #@13
    .line 676
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v1

    #@17
    .line 677
    .local v1, childCount:I
    const/4 v3, 0x0

    #@18
    .local v3, i:I
    :goto_18
    if-ge v3, v1, :cond_22

    #@1a
    .line 678
    invoke-interface {p2}, Ljava/util/List;->size()I
    :try_end_1d
    .catchall {:try_start_10 .. :try_end_1d} :catchall_57

    #@1d
    move-result v8

    #@1e
    const/16 v9, 0x32

    #@20
    if-lt v8, v9, :cond_26

    #@22
    .line 699
    :cond_22
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@25
    .line 702
    .end local v1           #childCount:I
    .end local v2           #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3           #i:I
    .end local v6           #parentGroup:Landroid/view/ViewGroup;
    :cond_25
    return-void

    #@26
    .line 681
    .restart local v1       #childCount:I
    .restart local v2       #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .restart local v3       #i:I
    .restart local v6       #parentGroup:Landroid/view/ViewGroup;
    :cond_26
    :try_start_26
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Landroid/view/View;

    #@2c
    .line 682
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getAccessibilityViewId()I

    #@2f
    move-result v8

    #@30
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@33
    move-result v9

    #@34
    if-eq v8, v9, :cond_4e

    #@36
    iget-object v8, p0, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->this$0:Landroid/view/AccessibilityInteractionController;

    #@38
    invoke-static {v8, v0}, Landroid/view/AccessibilityInteractionController;->access$100(Landroid/view/AccessibilityInteractionController;Landroid/view/View;)Z

    #@3b
    move-result v8

    #@3c
    if-eqz v8, :cond_4e

    #@3e
    .line 684
    const/4 v4, 0x0

    #@3f
    .line 685
    .local v4, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v0}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@42
    move-result-object v7

    #@43
    .line 687
    .local v7, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-nez v7, :cond_51

    #@45
    .line 688
    invoke-virtual {v0}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@48
    move-result-object v4

    #@49
    .line 693
    :goto_49
    if-eqz v4, :cond_4e

    #@4b
    .line 694
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@4e
    .line 677
    .end local v4           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v7           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_4e
    add-int/lit8 v3, v3, 0x1

    #@50
    goto :goto_18

    #@51
    .line 690
    .restart local v4       #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v7       #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_51
    const/4 v8, -0x1

    #@52
    invoke-virtual {v7, v8}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_55
    .catchall {:try_start_26 .. :try_end_55} :catchall_57

    #@55
    move-result-object v4

    #@56
    goto :goto_49

    #@57
    .line 699
    .end local v0           #child:Landroid/view/View;
    .end local v1           #childCount:I
    .end local v3           #i:I
    .end local v4           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v7           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :catchall_57
    move-exception v8

    #@58
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@5b
    throw v8
.end method

.method private prefetchSiblingsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V
    .registers 22
    .parameter "current"
    .parameter "providerHost"
    .parameter "provider"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            "Landroid/view/View;",
            "Landroid/view/accessibility/AccessibilityNodeProvider;",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 787
    .local p4, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@3
    move-result-wide v12

    #@4
    .line 788
    .local v12, parentNodeId:J
    invoke-static {v12, v13}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@7
    move-result v11

    #@8
    .line 790
    .local v11, parentAccessibilityViewId:I
    invoke-static {v12, v13}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@b
    move-result v14

    #@c
    .line 792
    .local v14, parentVirtualDescendantId:I
    const/4 v15, -0x1

    #@d
    if-ne v14, v15, :cond_15

    #@f
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getAccessibilityViewId()I

    #@12
    move-result v15

    #@13
    if-ne v11, v15, :cond_53

    #@15
    .line 794
    :cond_15
    move-object/from16 v0, p3

    #@17
    invoke-virtual {v0, v14}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1a
    move-result-object v10

    #@1b
    .line 796
    .local v10, parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v10, :cond_32

    #@1d
    .line 797
    invoke-virtual {v10}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@20
    move-result-object v7

    #@21
    .line 798
    .local v7, childNodeIds:Landroid/util/SparseLongArray;
    invoke-virtual {v7}, Landroid/util/SparseLongArray;->size()I

    #@24
    move-result v4

    #@25
    .line 799
    .local v4, childCount:I
    const/4 v9, 0x0

    #@26
    .local v9, i:I
    :goto_26
    if-ge v9, v4, :cond_32

    #@28
    .line 800
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    #@2b
    move-result v15

    #@2c
    const/16 v16, 0x32

    #@2e
    move/from16 v0, v16

    #@30
    if-lt v15, v0, :cond_33

    #@32
    .line 818
    .end local v4           #childCount:I
    .end local v7           #childNodeIds:Landroid/util/SparseLongArray;
    .end local v9           #i:I
    .end local v10           #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_32
    :goto_32
    return-void

    #@33
    .line 803
    .restart local v4       #childCount:I
    .restart local v7       #childNodeIds:Landroid/util/SparseLongArray;
    .restart local v9       #i:I
    .restart local v10       #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_33
    invoke-virtual {v7, v9}, Landroid/util/SparseLongArray;->get(I)J

    #@36
    move-result-wide v5

    #@37
    .line 804
    .local v5, childNodeId:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@3a
    move-result-wide v15

    #@3b
    cmp-long v15, v5, v15

    #@3d
    if-eqz v15, :cond_50

    #@3f
    .line 805
    invoke-static {v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@42
    move-result v8

    #@43
    .line 807
    .local v8, childVirtualDescendantId:I
    move-object/from16 v0, p3

    #@45
    invoke-virtual {v0, v8}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@48
    move-result-object v3

    #@49
    .line 809
    .local v3, child:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v3, :cond_50

    #@4b
    .line 810
    move-object/from16 v0, p4

    #@4d
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@50
    .line 799
    .end local v3           #child:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v8           #childVirtualDescendantId:I
    :cond_50
    add-int/lit8 v9, v9, 0x1

    #@52
    goto :goto_26

    #@53
    .line 816
    .end local v4           #childCount:I
    .end local v5           #childNodeId:J
    .end local v7           #childNodeIds:Landroid/util/SparseLongArray;
    .end local v9           #i:I
    .end local v10           #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_53
    move-object/from16 v0, p0

    #@55
    move-object/from16 v1, p2

    #@57
    move-object/from16 v2, p4

    #@59
    invoke-direct {v0, v1, v2}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchSiblingsOfRealNode(Landroid/view/View;Ljava/util/List;)V

    #@5c
    goto :goto_32
.end method


# virtual methods
.method public prefetchAccessibilityNodeInfos(Landroid/view/View;IILjava/util/List;)V
    .registers 8
    .parameter "view"
    .parameter "virtualViewId"
    .parameter "prefetchFlags"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "II",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 621
    .local p4, outInfos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@3
    move-result-object v0

    #@4
    .line 622
    .local v0, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-nez v0, :cond_25

    #@6
    .line 623
    invoke-virtual {p1}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@9
    move-result-object v1

    #@a
    .line 624
    .local v1, root:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v1, :cond_24

    #@c
    .line 625
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@f
    .line 626
    and-int/lit8 v2, p3, 0x1

    #@11
    if-eqz v2, :cond_16

    #@13
    .line 627
    invoke-direct {p0, p1, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchPredecessorsOfRealNode(Landroid/view/View;Ljava/util/List;)V

    #@16
    .line 629
    :cond_16
    and-int/lit8 v2, p3, 0x2

    #@18
    if-eqz v2, :cond_1d

    #@1a
    .line 630
    invoke-direct {p0, p1, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchSiblingsOfRealNode(Landroid/view/View;Ljava/util/List;)V

    #@1d
    .line 632
    :cond_1d
    and-int/lit8 v2, p3, 0x4

    #@1f
    if-eqz v2, :cond_24

    #@21
    .line 633
    invoke-direct {p0, p1, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchDescendantsOfRealNode(Landroid/view/View;Ljava/util/List;)V

    #@24
    .line 651
    :cond_24
    :goto_24
    return-void

    #@25
    .line 637
    .end local v1           #root:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_25
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityNodeProvider;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@28
    move-result-object v1

    #@29
    .line 638
    .restart local v1       #root:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v1, :cond_24

    #@2b
    .line 639
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2e
    .line 640
    and-int/lit8 v2, p3, 0x1

    #@30
    if-eqz v2, :cond_35

    #@32
    .line 641
    invoke-direct {p0, v1, p1, v0, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchPredecessorsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V

    #@35
    .line 643
    :cond_35
    and-int/lit8 v2, p3, 0x2

    #@37
    if-eqz v2, :cond_3c

    #@39
    .line 644
    invoke-direct {p0, v1, p1, v0, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchSiblingsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V

    #@3c
    .line 646
    :cond_3c
    and-int/lit8 v2, p3, 0x4

    #@3e
    if-eqz v2, :cond_24

    #@40
    .line 647
    invoke-direct {p0, v1, v0, p4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchDescendantsOfVirtualNode(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/view/accessibility/AccessibilityNodeProvider;Ljava/util/List;)V

    #@43
    goto :goto_24
.end method
