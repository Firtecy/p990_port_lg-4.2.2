.class public Landroid/view/TextureView;
.super Landroid/view/View;
.source "TextureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/TextureView$SurfaceTextureListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TextureView"


# instance fields
.field private mCanvas:Landroid/graphics/Canvas;

.field private mLayer:Landroid/view/HardwareLayer;

.field private mListener:Landroid/view/TextureView$SurfaceTextureListener;

.field private final mLock:[Ljava/lang/Object;

.field private final mMatrix:Landroid/graphics/Matrix;

.field private mMatrixChanged:Z

.field private mNativeWindow:I

.field private final mNativeWindowLock:[Ljava/lang/Object;

.field private mOpaque:Z

.field private mSaveCount:I

.field private mSurface:Landroid/graphics/SurfaceTexture;

.field private mUpdateLayer:Z

.field private mUpdateListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

.field private mUpdateSurface:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 137
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@4
    .line 112
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/view/TextureView;->mOpaque:Z

    #@7
    .line 114
    new-instance v0, Landroid/graphics/Matrix;

    #@9
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@c
    iput-object v0, p0, Landroid/view/TextureView;->mMatrix:Landroid/graphics/Matrix;

    #@e
    .line 117
    new-array v0, v1, [Ljava/lang/Object;

    #@10
    iput-object v0, p0, Landroid/view/TextureView;->mLock:[Ljava/lang/Object;

    #@12
    .line 126
    new-array v0, v1, [Ljava/lang/Object;

    #@14
    iput-object v0, p0, Landroid/view/TextureView;->mNativeWindowLock:[Ljava/lang/Object;

    #@16
    .line 138
    invoke-direct {p0}, Landroid/view/TextureView;->init()V

    #@19
    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 149
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 112
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/view/TextureView;->mOpaque:Z

    #@7
    .line 114
    new-instance v0, Landroid/graphics/Matrix;

    #@9
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@c
    iput-object v0, p0, Landroid/view/TextureView;->mMatrix:Landroid/graphics/Matrix;

    #@e
    .line 117
    new-array v0, v1, [Ljava/lang/Object;

    #@10
    iput-object v0, p0, Landroid/view/TextureView;->mLock:[Ljava/lang/Object;

    #@12
    .line 126
    new-array v0, v1, [Ljava/lang/Object;

    #@14
    iput-object v0, p0, Landroid/view/TextureView;->mNativeWindowLock:[Ljava/lang/Object;

    #@16
    .line 150
    invoke-direct {p0}, Landroid/view/TextureView;->init()V

    #@19
    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 165
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 112
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/view/TextureView;->mOpaque:Z

    #@7
    .line 114
    new-instance v0, Landroid/graphics/Matrix;

    #@9
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@c
    iput-object v0, p0, Landroid/view/TextureView;->mMatrix:Landroid/graphics/Matrix;

    #@e
    .line 117
    new-array v0, v1, [Ljava/lang/Object;

    #@10
    iput-object v0, p0, Landroid/view/TextureView;->mLock:[Ljava/lang/Object;

    #@12
    .line 126
    new-array v0, v1, [Ljava/lang/Object;

    #@14
    iput-object v0, p0, Landroid/view/TextureView;->mNativeWindowLock:[Ljava/lang/Object;

    #@16
    .line 166
    invoke-direct {p0}, Landroid/view/TextureView;->init()V

    #@19
    .line 167
    return-void
.end method

.method static synthetic access$000(Landroid/view/TextureView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 105
    invoke-direct {p0}, Landroid/view/TextureView;->destroySurface()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/view/TextureView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 105
    invoke-direct {p0}, Landroid/view/TextureView;->updateLayer()V

    #@3
    return-void
.end method

.method private applyTransformMatrix()V
    .registers 3

    #@0
    .prologue
    .line 498
    iget-boolean v0, p0, Landroid/view/TextureView;->mMatrixChanged:Z

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 499
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@a
    iget-object v1, p0, Landroid/view/TextureView;->mMatrix:Landroid/graphics/Matrix;

    #@c
    invoke-virtual {v0, v1}, Landroid/view/HardwareLayer;->setTransform(Landroid/graphics/Matrix;)V

    #@f
    .line 500
    const/4 v0, 0x0

    #@10
    iput-boolean v0, p0, Landroid/view/TextureView;->mMatrixChanged:Z

    #@12
    .line 502
    :cond_12
    return-void
.end method

.method private applyUpdate()V
    .registers 5

    #@0
    .prologue
    .line 434
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 451
    :cond_4
    :goto_4
    return-void

    #@5
    .line 438
    :cond_5
    iget-object v1, p0, Landroid/view/TextureView;->mLock:[Ljava/lang/Object;

    #@7
    monitor-enter v1

    #@8
    .line 439
    :try_start_8
    iget-boolean v0, p0, Landroid/view/TextureView;->mUpdateLayer:Z

    #@a
    if-eqz v0, :cond_2b

    #@c
    .line 440
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/view/TextureView;->mUpdateLayer:Z

    #@f
    .line 444
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_8 .. :try_end_10} :catchall_2d

    #@10
    .line 446
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@12
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@15
    move-result v1

    #@16
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@19
    move-result v2

    #@1a
    iget-boolean v3, p0, Landroid/view/TextureView;->mOpaque:Z

    #@1c
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/HardwareLayer;->update(IIZ)V

    #@1f
    .line 448
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@21
    if-eqz v0, :cond_4

    #@23
    .line 449
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@25
    iget-object v1, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@27
    invoke-interface {v0, v1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V

    #@2a
    goto :goto_4

    #@2b
    .line 442
    :cond_2b
    :try_start_2b
    monitor-exit v1

    #@2c
    goto :goto_4

    #@2d
    .line 444
    :catchall_2d
    move-exception v0

    #@2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2b .. :try_end_2f} :catchall_2d

    #@2f
    throw v0
.end method

.method private destroySurface()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 225
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@3
    if-eqz v1, :cond_33

    #@5
    .line 226
    iget-object v1, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@7
    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->detachFromGLContext()V

    #@a
    .line 229
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@c
    invoke-virtual {v1}, Landroid/view/HardwareLayer;->clearStorage()V

    #@f
    .line 231
    const/4 v0, 0x1

    #@10
    .line 232
    .local v0, shouldRelease:Z
    iget-object v1, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@12
    if-eqz v1, :cond_1c

    #@14
    .line 233
    iget-object v1, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@16
    iget-object v2, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@18
    invoke-interface {v1, v2}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    #@1b
    move-result v0

    #@1c
    .line 236
    :cond_1c
    iget-object v2, p0, Landroid/view/TextureView;->mNativeWindowLock:[Ljava/lang/Object;

    #@1e
    monitor-enter v2

    #@1f
    .line 237
    :try_start_1f
    invoke-direct {p0}, Landroid/view/TextureView;->nDestroyNativeWindow()V

    #@22
    .line 238
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_1f .. :try_end_23} :catchall_34

    #@23
    .line 240
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@25
    invoke-virtual {v1}, Landroid/view/HardwareLayer;->destroy()V

    #@28
    .line 241
    if-eqz v0, :cond_2f

    #@2a
    iget-object v1, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@2c
    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->release()V

    #@2f
    .line 242
    :cond_2f
    iput-object v3, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@31
    .line 243
    iput-object v3, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@33
    .line 245
    .end local v0           #shouldRelease:Z
    :cond_33
    return-void

    #@34
    .line 238
    .restart local v0       #shouldRelease:Z
    :catchall_34
    move-exception v1

    #@35
    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v1
.end method

.method private init()V
    .registers 2

    #@0
    .prologue
    .line 170
    new-instance v0, Landroid/graphics/Paint;

    #@2
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@5
    iput-object v0, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@7
    .line 171
    return-void
.end method

.method private native nCreateNativeWindow(Landroid/graphics/SurfaceTexture;)V
.end method

.method private native nDestroyNativeWindow()V
.end method

.method private static native nLockCanvas(ILandroid/graphics/Canvas;Landroid/graphics/Rect;)V
.end method

.method private static native nSetDefaultBufferSize(Landroid/graphics/SurfaceTexture;II)V
.end method

.method private static native nUnlockCanvasAndPost(ILandroid/graphics/Canvas;)V
.end method

.method private updateLayer()V
    .registers 3

    #@0
    .prologue
    .line 421
    iget-object v1, p0, Landroid/view/TextureView;->mLock:[Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 422
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Landroid/view/TextureView;->mUpdateLayer:Z

    #@6
    .line 423
    monitor-exit v1

    #@7
    .line 424
    return-void

    #@8
    .line 423
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private updateLayerAndInvalidate()V
    .registers 3

    #@0
    .prologue
    .line 427
    iget-object v1, p0, Landroid/view/TextureView;->mLock:[Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 428
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Landroid/view/TextureView;->mUpdateLayer:Z

    #@6
    .line 429
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_b

    #@7
    .line 430
    invoke-virtual {p0}, Landroid/view/TextureView;->invalidate()V

    #@a
    .line 431
    return-void

    #@b
    .line 429
    :catchall_b
    move-exception v0

    #@c
    :try_start_c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_c .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method


# virtual methods
.method public buildLayer()V
    .registers 1

    #@0
    .prologue
    .line 286
    return-void
.end method

.method protected destroyHardwareResources()V
    .registers 2

    #@0
    .prologue
    .line 335
    invoke-super {p0}, Landroid/view/View;->destroyHardwareResources()V

    #@3
    .line 336
    invoke-direct {p0}, Landroid/view/TextureView;->destroySurface()V

    #@6
    .line 337
    invoke-virtual {p0}, Landroid/view/TextureView;->invalidateParentCaches()V

    #@9
    .line 338
    const/4 v0, 0x1

    #@a
    invoke-virtual {p0, v0}, Landroid/view/TextureView;->invalidate(Z)V

    #@d
    .line 339
    return-void
.end method

.method destroyLayer(Z)Z
    .registers 3
    .parameter "valid"

    #@0
    .prologue
    .line 327
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 297
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const v1, -0x600001

    #@5
    and-int/2addr v0, v1

    #@6
    or-int/lit8 v0, v0, 0x20

    #@8
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    .line 299
    invoke-direct {p0}, Landroid/view/TextureView;->applyUpdate()V

    #@d
    .line 300
    invoke-direct {p0}, Landroid/view/TextureView;->applyTransformMatrix()V

    #@10
    .line 301
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 3

    #@0
    .prologue
    .line 525
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/view/TextureView;->getBitmap(II)Landroid/graphics/Bitmap;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getBitmap(II)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 552
    invoke-virtual {p0}, Landroid/view/TextureView;->isAvailable()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1d

    #@6
    if-lez p1, :cond_1d

    #@8
    if-lez p2, :cond_1d

    #@a
    .line 553
    invoke-virtual {p0}, Landroid/view/TextureView;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@11
    move-result-object v0

    #@12
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@14
    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {p0, v0}, Landroid/view/TextureView;->getBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@1b
    move-result-object v0

    #@1c
    .line 556
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method public getBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "bitmap"

    #@0
    .prologue
    .line 583
    if-eqz p1, :cond_42

    #@2
    invoke-virtual {p0}, Landroid/view/TextureView;->isAvailable()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_42

    #@8
    .line 584
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@a
    .line 585
    .local v0, info:Landroid/view/View$AttachInfo;
    if-eqz v0, :cond_28

    #@c
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@e
    if-eqz v1, :cond_28

    #@10
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@12
    invoke-virtual {v1}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_28

    #@18
    .line 587
    iget-object v1, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1a
    invoke-virtual {v1}, Landroid/view/HardwareRenderer;->validate()Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_28

    #@20
    .line 588
    new-instance v1, Ljava/lang/IllegalStateException;

    #@22
    const-string v2, "Could not acquire hardware rendering context"

    #@24
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 592
    :cond_28
    invoke-direct {p0}, Landroid/view/TextureView;->applyUpdate()V

    #@2b
    .line 593
    invoke-direct {p0}, Landroid/view/TextureView;->applyTransformMatrix()V

    #@2e
    .line 599
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@30
    if-nez v1, :cond_39

    #@32
    iget-boolean v1, p0, Landroid/view/TextureView;->mUpdateSurface:Z

    #@34
    if-eqz v1, :cond_39

    #@36
    .line 600
    invoke-virtual {p0}, Landroid/view/TextureView;->getHardwareLayer()Landroid/view/HardwareLayer;

    #@39
    .line 603
    :cond_39
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@3b
    if-eqz v1, :cond_42

    #@3d
    .line 604
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@3f
    invoke-virtual {v1, p1}, Landroid/view/HardwareLayer;->copyInto(Landroid/graphics/Bitmap;)Z

    #@42
    .line 607
    .end local v0           #info:Landroid/view/View$AttachInfo;
    :cond_42
    return-object p1
.end method

.method getHardwareLayer()Landroid/view/HardwareLayer;
    .registers 5

    #@0
    .prologue
    .line 344
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@2
    const v1, 0x8020

    #@5
    or-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@8
    .line 345
    iget v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@a
    const v1, -0x600001

    #@d
    and-int/2addr v0, v1

    #@e
    iput v0, p0, Landroid/view/View;->mPrivateFlags:I

    #@10
    .line 347
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@12
    if-nez v0, :cond_7a

    #@14
    .line 348
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@16
    if-eqz v0, :cond_1e

    #@18
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@1a
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@1c
    if-nez v0, :cond_20

    #@1e
    .line 349
    :cond_1e
    const/4 v0, 0x0

    #@1f
    .line 400
    :goto_1f
    return-object v0

    #@20
    .line 352
    :cond_20
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@22
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@24
    iget-boolean v1, p0, Landroid/view/TextureView;->mOpaque:Z

    #@26
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->createHardwareLayer(Z)Landroid/view/HardwareLayer;

    #@29
    move-result-object v0

    #@2a
    iput-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@2c
    .line 353
    iget-boolean v0, p0, Landroid/view/TextureView;->mUpdateSurface:Z

    #@2e
    if-nez v0, :cond_3c

    #@30
    .line 355
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@32
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@34
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@36
    invoke-virtual {v0, v1}, Landroid/view/HardwareRenderer;->createSurfaceTexture(Landroid/view/HardwareLayer;)Landroid/graphics/SurfaceTexture;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@3c
    .line 357
    :cond_3c
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@3e
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@41
    move-result v1

    #@42
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@45
    move-result v2

    #@46
    invoke-static {v0, v1, v2}, Landroid/view/TextureView;->nSetDefaultBufferSize(Landroid/graphics/SurfaceTexture;II)V

    #@49
    .line 358
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@4b
    invoke-direct {p0, v0}, Landroid/view/TextureView;->nCreateNativeWindow(Landroid/graphics/SurfaceTexture;)V

    #@4e
    .line 360
    new-instance v0, Landroid/view/TextureView$2;

    #@50
    invoke-direct {v0, p0}, Landroid/view/TextureView$2;-><init>(Landroid/view/TextureView;)V

    #@53
    iput-object v0, p0, Landroid/view/TextureView;->mUpdateListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@55
    .line 374
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@57
    iget-object v1, p0, Landroid/view/TextureView;->mUpdateListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@59
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@5c
    .line 376
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@5e
    if-eqz v0, :cond_73

    #@60
    iget-boolean v0, p0, Landroid/view/TextureView;->mUpdateSurface:Z

    #@62
    if-nez v0, :cond_73

    #@64
    .line 377
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@66
    iget-object v1, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@68
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@6b
    move-result v2

    #@6c
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@6f
    move-result v3

    #@70
    invoke-interface {v0, v1, v2, v3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    #@73
    .line 379
    :cond_73
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@75
    iget-object v1, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@77
    invoke-virtual {v0, v1}, Landroid/view/HardwareLayer;->setLayerPaint(Landroid/graphics/Paint;)V

    #@7a
    .line 382
    :cond_7a
    iget-boolean v0, p0, Landroid/view/TextureView;->mUpdateSurface:Z

    #@7c
    if-eqz v0, :cond_9f

    #@7e
    .line 386
    const/4 v0, 0x0

    #@7f
    iput-boolean v0, p0, Landroid/view/TextureView;->mUpdateSurface:Z

    #@81
    .line 390
    invoke-direct {p0}, Landroid/view/TextureView;->updateLayer()V

    #@84
    .line 391
    const/4 v0, 0x1

    #@85
    iput-boolean v0, p0, Landroid/view/TextureView;->mMatrixChanged:Z

    #@87
    .line 393
    iget-object v0, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@89
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8b
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@8d
    iget-object v2, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@8f
    invoke-virtual {v0, v1, v2}, Landroid/view/HardwareRenderer;->setSurfaceTexture(Landroid/view/HardwareLayer;Landroid/graphics/SurfaceTexture;)V

    #@92
    .line 394
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@94
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@97
    move-result v1

    #@98
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@9b
    move-result v2

    #@9c
    invoke-static {v0, v1, v2}, Landroid/view/TextureView;->nSetDefaultBufferSize(Landroid/graphics/SurfaceTexture;II)V

    #@9f
    .line 397
    :cond_9f
    invoke-direct {p0}, Landroid/view/TextureView;->applyUpdate()V

    #@a2
    .line 398
    invoke-direct {p0}, Landroid/view/TextureView;->applyTransformMatrix()V

    #@a5
    .line 400
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@a7
    goto/16 :goto_1f
.end method

.method public getLayerType()I
    .registers 2

    #@0
    .prologue
    .line 273
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .registers 2

    #@0
    .prologue
    .line 704
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@2
    return-object v0
.end method

.method public getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;
    .registers 2

    #@0
    .prologue
    .line 742
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@2
    return-object v0
.end method

.method public getTransform(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .registers 3
    .parameter "transform"

    #@0
    .prologue
    .line 488
    if-nez p1, :cond_7

    #@2
    .line 489
    new-instance p1, Landroid/graphics/Matrix;

    #@4
    .end local p1
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    #@7
    .line 492
    .restart local p1
    :cond_7
    iget-object v0, p0, Landroid/view/TextureView;->mMatrix:Landroid/graphics/Matrix;

    #@9
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@c
    .line 494
    return-object p1
.end method

.method hasStaticLayer()Z
    .registers 2

    #@0
    .prologue
    .line 278
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 616
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isOpaque()Z
    .registers 2

    #@0
    .prologue
    .line 178
    iget-boolean v0, p0, Landroid/view/TextureView;->mOpaque:Z

    #@2
    return v0
.end method

.method public lockCanvas()Landroid/graphics/Canvas;
    .registers 2

    #@0
    .prologue
    .line 643
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/view/TextureView;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 5
    .parameter "dirty"

    #@0
    .prologue
    .line 660
    invoke-virtual {p0}, Landroid/view/TextureView;->isAvailable()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 671
    :goto_7
    return-object v0

    #@8
    .line 662
    :cond_8
    iget-object v0, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@a
    if-nez v0, :cond_13

    #@c
    .line 663
    new-instance v0, Landroid/graphics/Canvas;

    #@e
    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    #@11
    iput-object v0, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@13
    .line 666
    :cond_13
    iget-object v1, p0, Landroid/view/TextureView;->mNativeWindowLock:[Ljava/lang/Object;

    #@15
    monitor-enter v1

    #@16
    .line 667
    :try_start_16
    iget v0, p0, Landroid/view/TextureView;->mNativeWindow:I

    #@18
    iget-object v2, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@1a
    invoke-static {v0, v2, p1}, Landroid/view/TextureView;->nLockCanvas(ILandroid/graphics/Canvas;Landroid/graphics/Rect;)V

    #@1d
    .line 668
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_16 .. :try_end_1e} :catchall_29

    #@1e
    .line 669
    iget-object v0, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@20
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    #@23
    move-result v0

    #@24
    iput v0, p0, Landroid/view/TextureView;->mSaveCount:I

    #@26
    .line 671
    iget-object v0, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@28
    goto :goto_7

    #@29
    .line 668
    :catchall_29
    move-exception v0

    #@2a
    :try_start_2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 199
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@3
    .line 201
    invoke-virtual {p0}, Landroid/view/TextureView;->isHardwareAccelerated()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_10

    #@9
    .line 202
    const-string v0, "TextureView"

    #@b
    const-string v1, "A TextureView or a subclass can only be used with hardware acceleration enabled."

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 205
    :cond_10
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 5

    #@0
    .prologue
    .line 209
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@3
    .line 210
    iget-object v1, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@5
    if-eqz v1, :cond_38

    #@7
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9
    if-eqz v1, :cond_38

    #@b
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@d
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@f
    if-eqz v1, :cond_38

    #@11
    .line 211
    iget-object v1, p0, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@13
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@15
    new-instance v2, Landroid/view/TextureView$1;

    #@17
    invoke-direct {v2, p0}, Landroid/view/TextureView$1;-><init>(Landroid/view/TextureView;)V

    #@1a
    invoke-virtual {v1, v2}, Landroid/view/HardwareRenderer;->safelyRun(Ljava/lang/Runnable;)Z

    #@1d
    move-result v0

    #@1e
    .line 218
    .local v0, success:Z
    if-nez v0, :cond_38

    #@20
    .line 219
    const-string v1, "TextureView"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "TextureView was not able to destroy its surface: "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 222
    .end local v0           #success:Z
    :cond_38
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 311
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 9
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    .line 315
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    #@3
    .line 316
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@5
    if-eqz v0, :cond_2a

    #@7
    .line 317
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@9
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@c
    move-result v1

    #@d
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@10
    move-result v2

    #@11
    invoke-static {v0, v1, v2}, Landroid/view/TextureView;->nSetDefaultBufferSize(Landroid/graphics/SurfaceTexture;II)V

    #@14
    .line 318
    invoke-direct {p0}, Landroid/view/TextureView;->updateLayer()V

    #@17
    .line 319
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@19
    if-eqz v0, :cond_2a

    #@1b
    .line 320
    iget-object v0, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@1d
    iget-object v1, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@1f
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    #@22
    move-result v2

    #@23
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    #@26
    move-result v3

    #@27
    invoke-interface {v0, v1, v2, v3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V

    #@2a
    .line 323
    :cond_2a
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 5
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 405
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 407
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 411
    if-nez p2, :cond_14

    #@9
    .line 412
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@b
    iget-object v1, p0, Landroid/view/TextureView;->mUpdateListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@d
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@10
    .line 413
    invoke-direct {p0}, Landroid/view/TextureView;->updateLayerAndInvalidate()V

    #@13
    .line 418
    :cond_13
    :goto_13
    return-void

    #@14
    .line 415
    :cond_14
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    #@1a
    goto :goto_13
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .registers 4
    .parameter "layerType"
    .parameter "paint"

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@2
    if-eq p2, v0, :cond_9

    #@4
    .line 263
    iput-object p2, p0, Landroid/view/View;->mLayerPaint:Landroid/graphics/Paint;

    #@6
    .line 264
    invoke-virtual {p0}, Landroid/view/TextureView;->invalidate()V

    #@9
    .line 266
    :cond_9
    return-void
.end method

.method public setOpaque(Z)V
    .registers 3
    .parameter "opaque"

    #@0
    .prologue
    .line 189
    iget-boolean v0, p0, Landroid/view/TextureView;->mOpaque:Z

    #@2
    if-eq p1, v0, :cond_d

    #@4
    .line 190
    iput-boolean p1, p0, Landroid/view/TextureView;->mOpaque:Z

    #@6
    .line 191
    iget-object v0, p0, Landroid/view/TextureView;->mLayer:Landroid/view/HardwareLayer;

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 192
    invoke-direct {p0}, Landroid/view/TextureView;->updateLayerAndInvalidate()V

    #@d
    .line 195
    :cond_d
    return-void
.end method

.method public setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .registers 4
    .parameter "surfaceTexture"

    #@0
    .prologue
    .line 723
    if-nez p1, :cond_b

    #@2
    .line 724
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "surfaceTexture must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 726
    :cond_b
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 727
    iget-object v0, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@11
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    #@14
    .line 729
    :cond_14
    iput-object p1, p0, Landroid/view/TextureView;->mSurface:Landroid/graphics/SurfaceTexture;

    #@16
    .line 730
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Landroid/view/TextureView;->mUpdateSurface:Z

    #@19
    .line 731
    invoke-virtual {p0}, Landroid/view/TextureView;->invalidateParentIfNeeded()V

    #@1c
    .line 732
    return-void
.end method

.method public setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 753
    iput-object p1, p0, Landroid/view/TextureView;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    #@2
    .line 754
    return-void
.end method

.method public setTransform(Landroid/graphics/Matrix;)V
    .registers 3
    .parameter "transform"

    #@0
    .prologue
    .line 471
    iget-object v0, p0, Landroid/view/TextureView;->mMatrix:Landroid/graphics/Matrix;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@5
    .line 472
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/view/TextureView;->mMatrixChanged:Z

    #@8
    .line 473
    invoke-virtual {p0}, Landroid/view/TextureView;->invalidateParentIfNeeded()V

    #@b
    .line 474
    return-void
.end method

.method public unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 686
    iget-object v0, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@2
    if-eqz v0, :cond_1b

    #@4
    iget-object v0, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@6
    if-ne p1, v0, :cond_1b

    #@8
    .line 687
    iget v0, p0, Landroid/view/TextureView;->mSaveCount:I

    #@a
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@d
    .line 688
    const/4 v0, 0x0

    #@e
    iput v0, p0, Landroid/view/TextureView;->mSaveCount:I

    #@10
    .line 690
    iget-object v1, p0, Landroid/view/TextureView;->mNativeWindowLock:[Ljava/lang/Object;

    #@12
    monitor-enter v1

    #@13
    .line 691
    :try_start_13
    iget v0, p0, Landroid/view/TextureView;->mNativeWindow:I

    #@15
    iget-object v2, p0, Landroid/view/TextureView;->mCanvas:Landroid/graphics/Canvas;

    #@17
    invoke-static {v0, v2}, Landroid/view/TextureView;->nUnlockCanvasAndPost(ILandroid/graphics/Canvas;)V

    #@1a
    .line 692
    monitor-exit v1

    #@1b
    .line 694
    :cond_1b
    return-void

    #@1c
    .line 692
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_13 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method
