.class final Landroid/view/Choreographer$1;
.super Ljava/lang/ThreadLocal;
.source "Choreographer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Choreographer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ThreadLocal",
        "<",
        "Landroid/view/Choreographer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/ThreadLocal;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method protected initialValue()Landroid/view/Choreographer;
    .registers 4

    #@0
    .prologue
    .line 91
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    .line 92
    .local v0, looper:Landroid/os/Looper;
    if-nez v0, :cond_e

    #@6
    .line 93
    new-instance v1, Ljava/lang/IllegalStateException;

    #@8
    const-string v2, "The current thread must have a looper!"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 95
    :cond_e
    new-instance v1, Landroid/view/Choreographer;

    #@10
    const/4 v2, 0x0

    #@11
    invoke-direct {v1, v0, v2}, Landroid/view/Choreographer;-><init>(Landroid/os/Looper;Landroid/view/Choreographer$1;)V

    #@14
    return-object v1
.end method

.method protected bridge synthetic initialValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 88
    invoke-virtual {p0}, Landroid/view/Choreographer$1;->initialValue()Landroid/view/Choreographer;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
