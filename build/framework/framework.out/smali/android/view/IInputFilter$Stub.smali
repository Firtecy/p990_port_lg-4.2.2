.class public abstract Landroid/view/IInputFilter$Stub;
.super Landroid/os/Binder;
.source "IInputFilter.java"

# interfaces
.implements Landroid/view/IInputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IInputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/IInputFilter$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.IInputFilter"

.field static final TRANSACTION_filterInputEvent:I = 0x3

.field static final TRANSACTION_install:I = 0x1

.field static final TRANSACTION_uninstall:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.view.IInputFilter"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/IInputFilter$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/IInputFilter;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.view.IInputFilter"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/IInputFilter;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/view/IInputFilter;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/view/IInputFilter$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/IInputFilter$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 44
    sparse-switch p1, :sswitch_data_46

    #@4
    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 48
    :sswitch_9
    const-string v3, "android.view.IInputFilter"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 53
    :sswitch_f
    const-string v3, "android.view.IInputFilter"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v3

    #@18
    invoke-static {v3}, Landroid/view/IInputFilterHost$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IInputFilterHost;

    #@1b
    move-result-object v0

    #@1c
    .line 56
    .local v0, _arg0:Landroid/view/IInputFilterHost;
    invoke-virtual {p0, v0}, Landroid/view/IInputFilter$Stub;->install(Landroid/view/IInputFilterHost;)V

    #@1f
    goto :goto_8

    #@20
    .line 61
    .end local v0           #_arg0:Landroid/view/IInputFilterHost;
    :sswitch_20
    const-string v3, "android.view.IInputFilter"

    #@22
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 62
    invoke-virtual {p0}, Landroid/view/IInputFilter$Stub;->uninstall()V

    #@28
    goto :goto_8

    #@29
    .line 67
    :sswitch_29
    const-string v3, "android.view.IInputFilter"

    #@2b
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_44

    #@34
    .line 70
    sget-object v3, Landroid/view/InputEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@36
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@39
    move-result-object v0

    #@3a
    check-cast v0, Landroid/view/InputEvent;

    #@3c
    .line 76
    .local v0, _arg0:Landroid/view/InputEvent;
    :goto_3c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v1

    #@40
    .line 77
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/view/IInputFilter$Stub;->filterInputEvent(Landroid/view/InputEvent;I)V

    #@43
    goto :goto_8

    #@44
    .line 73
    .end local v0           #_arg0:Landroid/view/InputEvent;
    .end local v1           #_arg1:I
    :cond_44
    const/4 v0, 0x0

    #@45
    .restart local v0       #_arg0:Landroid/view/InputEvent;
    goto :goto_3c

    #@46
    .line 44
    :sswitch_data_46
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_20
        0x3 -> :sswitch_29
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
