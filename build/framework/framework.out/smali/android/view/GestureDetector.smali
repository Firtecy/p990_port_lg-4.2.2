.class public Landroid/view/GestureDetector;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/GestureDetector$GestureHandler;,
        Landroid/view/GestureDetector$SimpleOnGestureListener;,
        Landroid/view/GestureDetector$OnDoubleTapListener;,
        Landroid/view/GestureDetector$OnGestureListener;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final DOUBLE_TAP_TIMEOUT:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final LONGPRESS_TIMEOUT:I = 0x0

.field private static final LONG_PRESS:I = 0x2

.field private static final SHOW_PRESS:I = 0x1

.field private static final TAP:I = 0x3

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private mDoubleTapSlopSquare:I

.field private mDoubleTapTouchSlopSquare:I

.field private mDownFocusX:F

.field private mDownFocusY:F

.field private final mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private final mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

.field private mIsDoubleTapping:Z

.field private mIsLongpressEnabled:Z

.field private mLastFocusX:F

.field private mLastFocusY:F

.field private final mListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mMaximumFlingVelocity:I

.field private mMinimumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 202
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@3
    move-result v0

    #@4
    sput v0, Landroid/view/GestureDetector;->LONGPRESS_TIMEOUT:I

    #@6
    .line 203
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    #@9
    move-result v0

    #@a
    sput v0, Landroid/view/GestureDetector;->TAP_TIMEOUT:I

    #@c
    .line 204
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    #@f
    move-result v0

    #@10
    sput v0, Landroid/view/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    #@12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 4
    .parameter "context"
    .parameter "listener"

    #@0
    .prologue
    .line 331
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    #@4
    .line 332
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 346
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 244
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_29

    #@9
    new-instance v0, Landroid/view/InputEventConsistencyVerifier;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-direct {v0, p0, v1}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;I)V

    #@f
    :goto_f
    iput-object v0, p0, Landroid/view/GestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@11
    .line 347
    if-eqz p3, :cond_2b

    #@13
    .line 348
    new-instance v0, Landroid/view/GestureDetector$GestureHandler;

    #@15
    invoke-direct {v0, p0, p3}, Landroid/view/GestureDetector$GestureHandler;-><init>(Landroid/view/GestureDetector;Landroid/os/Handler;)V

    #@18
    iput-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@1a
    .line 352
    :goto_1a
    iput-object p2, p0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@1c
    .line 353
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    #@1e
    if-eqz v0, :cond_25

    #@20
    .line 354
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    #@22
    .end local p2
    invoke-virtual {p0, p2}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    #@25
    .line 356
    :cond_25
    invoke-direct {p0, p1}, Landroid/view/GestureDetector;->init(Landroid/content/Context;)V

    #@28
    .line 357
    return-void

    #@29
    .line 244
    .restart local p2
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_f

    #@2b
    .line 350
    :cond_2b
    new-instance v0, Landroid/view/GestureDetector$GestureHandler;

    #@2d
    invoke-direct {v0, p0}, Landroid/view/GestureDetector$GestureHandler;-><init>(Landroid/view/GestureDetector;)V

    #@30
    iput-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@32
    goto :goto_1a
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V
    .registers 5
    .parameter "context"
    .parameter "listener"
    .parameter "handler"
    .parameter "unused"

    #@0
    .prologue
    .line 373
    invoke-direct {p0, p1, p2, p3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    #@3
    .line 374
    return-void
.end method

.method public constructor <init>(Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 3
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 316
    invoke-direct {p0, v0, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    #@4
    .line 317
    return-void
.end method

.method public constructor <init>(Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .registers 4
    .parameter "listener"
    .parameter "handler"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 298
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    #@4
    .line 299
    return-void
.end method

.method static synthetic access$000(Landroid/view/GestureDetector;)Landroid/view/MotionEvent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/view/GestureDetector;)Landroid/view/GestureDetector$OnGestureListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/view/GestureDetector;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Landroid/view/GestureDetector;->dispatchLongPress()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/view/GestureDetector;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/view/GestureDetector;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-boolean v0, p0, Landroid/view/GestureDetector;->mStillDown:Z

    #@2
    return v0
.end method

.method private cancel()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 631
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@3
    const/4 v1, 0x1

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 632
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x2

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 633
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@f
    const/4 v1, 0x3

    #@10
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@13
    .line 634
    iget-object v0, p0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@15
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    #@18
    .line 635
    const/4 v0, 0x0

    #@19
    iput-object v0, p0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@1b
    .line 636
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mIsDoubleTapping:Z

    #@1d
    .line 637
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mStillDown:Z

    #@1f
    .line 638
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mAlwaysInTapRegion:Z

    #@21
    .line 639
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    #@23
    .line 640
    iget-boolean v0, p0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@25
    if-eqz v0, :cond_29

    #@27
    .line 641
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@29
    .line 643
    :cond_29
    return-void
.end method

.method private cancelTaps()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 646
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@3
    const/4 v1, 0x1

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@7
    .line 647
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x2

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 648
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@f
    const/4 v1, 0x3

    #@10
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@13
    .line 649
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mIsDoubleTapping:Z

    #@15
    .line 650
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mAlwaysInTapRegion:Z

    #@17
    .line 651
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    #@19
    .line 652
    iget-boolean v0, p0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@1b
    if-eqz v0, :cond_1f

    #@1d
    .line 653
    iput-boolean v2, p0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@1f
    .line 655
    :cond_1f
    return-void
.end method

.method private dispatchLongPress()V
    .registers 3

    #@0
    .prologue
    .line 673
    iget-object v0, p0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 674
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@9
    .line 675
    iget-object v0, p0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@b
    iget-object v1, p0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@d
    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    #@10
    .line 676
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 377
    iget-object v4, p0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@2
    if-nez v4, :cond_c

    #@4
    .line 378
    new-instance v4, Ljava/lang/NullPointerException;

    #@6
    const-string v5, "OnGestureListener must not be null"

    #@8
    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@b
    throw v4

    #@c
    .line 380
    :cond_c
    const/4 v4, 0x1

    #@d
    iput-boolean v4, p0, Landroid/view/GestureDetector;->mIsLongpressEnabled:Z

    #@f
    .line 384
    if-nez p1, :cond_33

    #@11
    .line 386
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    #@14
    move-result v3

    #@15
    .line 387
    .local v3, touchSlop:I
    move v2, v3

    #@16
    .line 388
    .local v2, doubleTapTouchSlop:I
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapSlop()I

    #@19
    move-result v1

    #@1a
    .line 390
    .local v1, doubleTapSlop:I
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    #@1d
    move-result v4

    #@1e
    iput v4, p0, Landroid/view/GestureDetector;->mMinimumFlingVelocity:I

    #@20
    .line 391
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    #@23
    move-result v4

    #@24
    iput v4, p0, Landroid/view/GestureDetector;->mMaximumFlingVelocity:I

    #@26
    .line 400
    :goto_26
    mul-int v4, v3, v3

    #@28
    iput v4, p0, Landroid/view/GestureDetector;->mTouchSlopSquare:I

    #@2a
    .line 401
    mul-int v4, v2, v2

    #@2c
    iput v4, p0, Landroid/view/GestureDetector;->mDoubleTapTouchSlopSquare:I

    #@2e
    .line 402
    mul-int v4, v1, v1

    #@30
    iput v4, p0, Landroid/view/GestureDetector;->mDoubleTapSlopSquare:I

    #@32
    .line 403
    return-void

    #@33
    .line 393
    .end local v1           #doubleTapSlop:I
    .end local v2           #doubleTapTouchSlop:I
    .end local v3           #touchSlop:I
    :cond_33
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@36
    move-result-object v0

    #@37
    .line 394
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@3a
    move-result v3

    #@3b
    .line 395
    .restart local v3       #touchSlop:I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapTouchSlop()I

    #@3e
    move-result v2

    #@3f
    .line 396
    .restart local v2       #doubleTapTouchSlop:I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    #@42
    move-result v1

    #@43
    .line 397
    .restart local v1       #doubleTapSlop:I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@46
    move-result v4

    #@47
    iput v4, p0, Landroid/view/GestureDetector;->mMinimumFlingVelocity:I

    #@49
    .line 398
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@4c
    move-result v4

    #@4d
    iput v4, p0, Landroid/view/GestureDetector;->mMaximumFlingVelocity:I

    #@4f
    goto :goto_26
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "firstDown"
    .parameter "firstUp"
    .parameter "secondDown"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 659
    iget-boolean v3, p0, Landroid/view/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    #@3
    if-nez v3, :cond_6

    #@5
    .line 669
    :cond_5
    :goto_5
    return v2

    #@6
    .line 663
    :cond_6
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    #@9
    move-result-wide v3

    #@a
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    #@d
    move-result-wide v5

    #@e
    sub-long/2addr v3, v5

    #@f
    sget v5, Landroid/view/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    #@11
    int-to-long v5, v5

    #@12
    cmp-long v3, v3, v5

    #@14
    if-gtz v3, :cond_5

    #@16
    .line 667
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@19
    move-result v3

    #@1a
    float-to-int v3, v3

    #@1b
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    #@1e
    move-result v4

    #@1f
    float-to-int v4, v4

    #@20
    sub-int v0, v3, v4

    #@22
    .line 668
    .local v0, deltaX:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@25
    move-result v3

    #@26
    float-to-int v3, v3

    #@27
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    #@2a
    move-result v4

    #@2b
    float-to-int v4, v4

    #@2c
    sub-int v1, v3, v4

    #@2e
    .line 669
    .local v1, deltaY:I
    mul-int v3, v0, v0

    #@30
    mul-int v4, v1, v1

    #@32
    add-int/2addr v3, v4

    #@33
    iget v4, p0, Landroid/view/GestureDetector;->mDoubleTapSlopSquare:I

    #@35
    if-ge v3, v4, :cond_5

    #@37
    const/4 v2, 0x1

    #@38
    goto :goto_5
.end method


# virtual methods
.method public isLongpressEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 433
    iget-boolean v0, p0, Landroid/view/GestureDetector;->mIsLongpressEnabled:Z

    #@2
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 43
    .parameter "ev"

    #@0
    .prologue
    .line 445
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/view/GestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@4
    move-object/from16 v35, v0

    #@6
    if-eqz v35, :cond_19

    #@8
    .line 446
    move-object/from16 v0, p0

    #@a
    iget-object v0, v0, Landroid/view/GestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@c
    move-object/from16 v35, v0

    #@e
    const/16 v36, 0x0

    #@10
    move-object/from16 v0, v35

    #@12
    move-object/from16 v1, p1

    #@14
    move/from16 v2, v36

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    #@19
    .line 449
    :cond_19
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@1c
    move-result v5

    #@1d
    .line 451
    .local v5, action:I
    move-object/from16 v0, p0

    #@1f
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@21
    move-object/from16 v35, v0

    #@23
    if-nez v35, :cond_2f

    #@25
    .line 452
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@28
    move-result-object v35

    #@29
    move-object/from16 v0, v35

    #@2b
    move-object/from16 v1, p0

    #@2d
    iput-object v0, v1, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@2f
    .line 454
    :cond_2f
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@33
    move-object/from16 v35, v0

    #@35
    move-object/from16 v0, v35

    #@37
    move-object/from16 v1, p1

    #@39
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@3c
    .line 456
    and-int/lit16 v0, v5, 0xff

    #@3e
    move/from16 v35, v0

    #@40
    const/16 v36, 0x6

    #@42
    move/from16 v0, v35

    #@44
    move/from16 v1, v36

    #@46
    if-ne v0, v1, :cond_67

    #@48
    const/16 v21, 0x1

    #@4a
    .line 458
    .local v21, pointerUp:Z
    :goto_4a
    if-eqz v21, :cond_6a

    #@4c
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@4f
    move-result v24

    #@50
    .line 461
    .local v24, skipIndex:I
    :goto_50
    const/16 v25, 0x0

    #@52
    .local v25, sumX:F
    const/16 v26, 0x0

    #@54
    .line 462
    .local v26, sumY:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@57
    move-result v6

    #@58
    .line 463
    .local v6, count:I
    const/16 v17, 0x0

    #@5a
    .local v17, i:I
    :goto_5a
    move/from16 v0, v17

    #@5c
    if-ge v0, v6, :cond_82

    #@5e
    .line 464
    move/from16 v0, v24

    #@60
    move/from16 v1, v17

    #@62
    if-ne v0, v1, :cond_6d

    #@64
    .line 463
    :goto_64
    add-int/lit8 v17, v17, 0x1

    #@66
    goto :goto_5a

    #@67
    .line 456
    .end local v6           #count:I
    .end local v17           #i:I
    .end local v21           #pointerUp:Z
    .end local v24           #skipIndex:I
    .end local v25           #sumX:F
    .end local v26           #sumY:F
    :cond_67
    const/16 v21, 0x0

    #@69
    goto :goto_4a

    #@6a
    .line 458
    .restart local v21       #pointerUp:Z
    :cond_6a
    const/16 v24, -0x1

    #@6c
    goto :goto_50

    #@6d
    .line 465
    .restart local v6       #count:I
    .restart local v17       #i:I
    .restart local v24       #skipIndex:I
    .restart local v25       #sumX:F
    .restart local v26       #sumY:F
    :cond_6d
    move-object/from16 v0, p1

    #@6f
    move/from16 v1, v17

    #@71
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    #@74
    move-result v35

    #@75
    add-float v25, v25, v35

    #@77
    .line 466
    move-object/from16 v0, p1

    #@79
    move/from16 v1, v17

    #@7b
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    #@7e
    move-result v35

    #@7f
    add-float v26, v26, v35

    #@81
    goto :goto_64

    #@82
    .line 468
    :cond_82
    if-eqz v21, :cond_b5

    #@84
    add-int/lit8 v11, v6, -0x1

    #@86
    .line 469
    .local v11, div:I
    :goto_86
    int-to-float v0, v11

    #@87
    move/from16 v35, v0

    #@89
    div-float v13, v25, v35

    #@8b
    .line 470
    .local v13, focusX:F
    int-to-float v0, v11

    #@8c
    move/from16 v35, v0

    #@8e
    div-float v14, v26, v35

    #@90
    .line 472
    .local v14, focusY:F
    const/16 v16, 0x0

    #@92
    .line 474
    .local v16, handled:Z
    and-int/lit16 v0, v5, 0xff

    #@94
    move/from16 v35, v0

    #@96
    packed-switch v35, :pswitch_data_4bc

    #@99
    .line 624
    :cond_99
    :goto_99
    :pswitch_99
    if-nez v16, :cond_b4

    #@9b
    move-object/from16 v0, p0

    #@9d
    iget-object v0, v0, Landroid/view/GestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@9f
    move-object/from16 v35, v0

    #@a1
    if-eqz v35, :cond_b4

    #@a3
    .line 625
    move-object/from16 v0, p0

    #@a5
    iget-object v0, v0, Landroid/view/GestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@a7
    move-object/from16 v35, v0

    #@a9
    const/16 v36, 0x0

    #@ab
    move-object/from16 v0, v35

    #@ad
    move-object/from16 v1, p1

    #@af
    move/from16 v2, v36

    #@b1
    invoke-virtual {v0, v1, v2}, Landroid/view/InputEventConsistencyVerifier;->onUnhandledEvent(Landroid/view/InputEvent;I)V

    #@b4
    .line 627
    :cond_b4
    return v16

    #@b5
    .end local v11           #div:I
    .end local v13           #focusX:F
    .end local v14           #focusY:F
    .end local v16           #handled:Z
    :cond_b5
    move v11, v6

    #@b6
    .line 468
    goto :goto_86

    #@b7
    .line 476
    .restart local v11       #div:I
    .restart local v13       #focusX:F
    .restart local v14       #focusY:F
    .restart local v16       #handled:Z
    :pswitch_b7
    move-object/from16 v0, p0

    #@b9
    iput v13, v0, Landroid/view/GestureDetector;->mLastFocusX:F

    #@bb
    move-object/from16 v0, p0

    #@bd
    iput v13, v0, Landroid/view/GestureDetector;->mDownFocusX:F

    #@bf
    .line 477
    move-object/from16 v0, p0

    #@c1
    iput v14, v0, Landroid/view/GestureDetector;->mLastFocusY:F

    #@c3
    move-object/from16 v0, p0

    #@c5
    iput v14, v0, Landroid/view/GestureDetector;->mDownFocusY:F

    #@c7
    .line 479
    invoke-direct/range {p0 .. p0}, Landroid/view/GestureDetector;->cancelTaps()V

    #@ca
    goto :goto_99

    #@cb
    .line 483
    :pswitch_cb
    move-object/from16 v0, p0

    #@cd
    iput v13, v0, Landroid/view/GestureDetector;->mLastFocusX:F

    #@cf
    move-object/from16 v0, p0

    #@d1
    iput v13, v0, Landroid/view/GestureDetector;->mDownFocusX:F

    #@d3
    .line 484
    move-object/from16 v0, p0

    #@d5
    iput v14, v0, Landroid/view/GestureDetector;->mLastFocusY:F

    #@d7
    move-object/from16 v0, p0

    #@d9
    iput v14, v0, Landroid/view/GestureDetector;->mDownFocusY:F

    #@db
    .line 488
    move-object/from16 v0, p0

    #@dd
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@df
    move-object/from16 v35, v0

    #@e1
    const/16 v36, 0x3e8

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget v0, v0, Landroid/view/GestureDetector;->mMaximumFlingVelocity:I

    #@e7
    move/from16 v37, v0

    #@e9
    move/from16 v0, v37

    #@eb
    int-to-float v0, v0

    #@ec
    move/from16 v37, v0

    #@ee
    invoke-virtual/range {v35 .. v37}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@f1
    .line 489
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@f4
    move-result v27

    #@f5
    .line 490
    .local v27, upIndex:I
    move-object/from16 v0, p1

    #@f7
    move/from16 v1, v27

    #@f9
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@fc
    move-result v18

    #@fd
    .line 491
    .local v18, id1:I
    move-object/from16 v0, p0

    #@ff
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@101
    move-object/from16 v35, v0

    #@103
    move-object/from16 v0, v35

    #@105
    move/from16 v1, v18

    #@107
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@10a
    move-result v32

    #@10b
    .line 492
    .local v32, x1:F
    move-object/from16 v0, p0

    #@10d
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@10f
    move-object/from16 v35, v0

    #@111
    move-object/from16 v0, v35

    #@113
    move/from16 v1, v18

    #@115
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@118
    move-result v34

    #@119
    .line 493
    .local v34, y1:F
    const/16 v17, 0x0

    #@11b
    :goto_11b
    move/from16 v0, v17

    #@11d
    if-ge v0, v6, :cond_99

    #@11f
    .line 494
    move/from16 v0, v17

    #@121
    move/from16 v1, v27

    #@123
    if-ne v0, v1, :cond_128

    #@125
    .line 493
    :cond_125
    add-int/lit8 v17, v17, 0x1

    #@127
    goto :goto_11b

    #@128
    .line 496
    :cond_128
    move-object/from16 v0, p1

    #@12a
    move/from16 v1, v17

    #@12c
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@12f
    move-result v19

    #@130
    .line 497
    .local v19, id2:I
    move-object/from16 v0, p0

    #@132
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@134
    move-object/from16 v35, v0

    #@136
    move-object/from16 v0, v35

    #@138
    move/from16 v1, v19

    #@13a
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@13d
    move-result v35

    #@13e
    mul-float v31, v32, v35

    #@140
    .line 498
    .local v31, x:F
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@144
    move-object/from16 v35, v0

    #@146
    move-object/from16 v0, v35

    #@148
    move/from16 v1, v19

    #@14a
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@14d
    move-result v35

    #@14e
    mul-float v33, v34, v35

    #@150
    .line 500
    .local v33, y:F
    add-float v12, v31, v33

    #@152
    .line 501
    .local v12, dot:F
    const/16 v35, 0x0

    #@154
    cmpg-float v35, v12, v35

    #@156
    if-gez v35, :cond_125

    #@158
    .line 502
    move-object/from16 v0, p0

    #@15a
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@15c
    move-object/from16 v35, v0

    #@15e
    invoke-virtual/range {v35 .. v35}, Landroid/view/VelocityTracker;->clear()V

    #@161
    goto/16 :goto_99

    #@163
    .line 509
    .end local v12           #dot:F
    .end local v18           #id1:I
    .end local v19           #id2:I
    .end local v27           #upIndex:I
    .end local v31           #x:F
    .end local v32           #x1:F
    .end local v33           #y:F
    .end local v34           #y1:F
    :pswitch_163
    move-object/from16 v0, p0

    #@165
    iget-object v0, v0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@167
    move-object/from16 v35, v0

    #@169
    if-eqz v35, :cond_1da

    #@16b
    .line 510
    move-object/from16 v0, p0

    #@16d
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@16f
    move-object/from16 v35, v0

    #@171
    const/16 v36, 0x3

    #@173
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->hasMessages(I)Z

    #@176
    move-result v15

    #@177
    .line 511
    .local v15, hadTapMessage:Z
    if-eqz v15, :cond_184

    #@179
    move-object/from16 v0, p0

    #@17b
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@17d
    move-object/from16 v35, v0

    #@17f
    const/16 v36, 0x3

    #@181
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@184
    .line 512
    :cond_184
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@188
    move-object/from16 v35, v0

    #@18a
    if-eqz v35, :cond_28f

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Landroid/view/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    #@190
    move-object/from16 v35, v0

    #@192
    if-eqz v35, :cond_28f

    #@194
    if-eqz v15, :cond_28f

    #@196
    move-object/from16 v0, p0

    #@198
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@19a
    move-object/from16 v35, v0

    #@19c
    move-object/from16 v0, p0

    #@19e
    iget-object v0, v0, Landroid/view/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    #@1a0
    move-object/from16 v36, v0

    #@1a2
    move-object/from16 v0, p0

    #@1a4
    move-object/from16 v1, v35

    #@1a6
    move-object/from16 v2, v36

    #@1a8
    move-object/from16 v3, p1

    #@1aa
    invoke-direct {v0, v1, v2, v3}, Landroid/view/GestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    #@1ad
    move-result v35

    #@1ae
    if-eqz v35, :cond_28f

    #@1b0
    .line 515
    const/16 v35, 0x1

    #@1b2
    move/from16 v0, v35

    #@1b4
    move-object/from16 v1, p0

    #@1b6
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mIsDoubleTapping:Z

    #@1b8
    .line 517
    move-object/from16 v0, p0

    #@1ba
    iget-object v0, v0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@1bc
    move-object/from16 v35, v0

    #@1be
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@1c2
    move-object/from16 v36, v0

    #@1c4
    invoke-interface/range {v35 .. v36}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    #@1c7
    move-result v35

    #@1c8
    or-int v16, v16, v35

    #@1ca
    .line 519
    move-object/from16 v0, p0

    #@1cc
    iget-object v0, v0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@1ce
    move-object/from16 v35, v0

    #@1d0
    move-object/from16 v0, v35

    #@1d2
    move-object/from16 v1, p1

    #@1d4
    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    #@1d7
    move-result v35

    #@1d8
    or-int v16, v16, v35

    #@1da
    .line 526
    .end local v15           #hadTapMessage:Z
    :cond_1da
    :goto_1da
    move-object/from16 v0, p0

    #@1dc
    iput v13, v0, Landroid/view/GestureDetector;->mLastFocusX:F

    #@1de
    move-object/from16 v0, p0

    #@1e0
    iput v13, v0, Landroid/view/GestureDetector;->mDownFocusX:F

    #@1e2
    .line 527
    move-object/from16 v0, p0

    #@1e4
    iput v14, v0, Landroid/view/GestureDetector;->mLastFocusY:F

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iput v14, v0, Landroid/view/GestureDetector;->mDownFocusY:F

    #@1ea
    .line 528
    move-object/from16 v0, p0

    #@1ec
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@1ee
    move-object/from16 v35, v0

    #@1f0
    if-eqz v35, :cond_1fb

    #@1f2
    .line 529
    move-object/from16 v0, p0

    #@1f4
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@1f6
    move-object/from16 v35, v0

    #@1f8
    invoke-virtual/range {v35 .. v35}, Landroid/view/MotionEvent;->recycle()V

    #@1fb
    .line 531
    :cond_1fb
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@1fe
    move-result-object v35

    #@1ff
    move-object/from16 v0, v35

    #@201
    move-object/from16 v1, p0

    #@203
    iput-object v0, v1, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@205
    .line 532
    const/16 v35, 0x1

    #@207
    move/from16 v0, v35

    #@209
    move-object/from16 v1, p0

    #@20b
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mAlwaysInTapRegion:Z

    #@20d
    .line 533
    const/16 v35, 0x1

    #@20f
    move/from16 v0, v35

    #@211
    move-object/from16 v1, p0

    #@213
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    #@215
    .line 534
    const/16 v35, 0x1

    #@217
    move/from16 v0, v35

    #@219
    move-object/from16 v1, p0

    #@21b
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mStillDown:Z

    #@21d
    .line 535
    const/16 v35, 0x0

    #@21f
    move/from16 v0, v35

    #@221
    move-object/from16 v1, p0

    #@223
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mInLongPress:Z

    #@225
    .line 537
    move-object/from16 v0, p0

    #@227
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mIsLongpressEnabled:Z

    #@229
    move/from16 v35, v0

    #@22b
    if-eqz v35, :cond_25f

    #@22d
    .line 538
    move-object/from16 v0, p0

    #@22f
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@231
    move-object/from16 v35, v0

    #@233
    const/16 v36, 0x2

    #@235
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@238
    .line 539
    move-object/from16 v0, p0

    #@23a
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@23c
    move-object/from16 v35, v0

    #@23e
    const/16 v36, 0x2

    #@240
    move-object/from16 v0, p0

    #@242
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@244
    move-object/from16 v37, v0

    #@246
    invoke-virtual/range {v37 .. v37}, Landroid/view/MotionEvent;->getDownTime()J

    #@249
    move-result-wide v37

    #@24a
    sget v39, Landroid/view/GestureDetector;->TAP_TIMEOUT:I

    #@24c
    move/from16 v0, v39

    #@24e
    int-to-long v0, v0

    #@24f
    move-wide/from16 v39, v0

    #@251
    add-long v37, v37, v39

    #@253
    sget v39, Landroid/view/GestureDetector;->LONGPRESS_TIMEOUT:I

    #@255
    move/from16 v0, v39

    #@257
    int-to-long v0, v0

    #@258
    move-wide/from16 v39, v0

    #@25a
    add-long v37, v37, v39

    #@25c
    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    #@25f
    .line 542
    :cond_25f
    move-object/from16 v0, p0

    #@261
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@263
    move-object/from16 v35, v0

    #@265
    const/16 v36, 0x1

    #@267
    move-object/from16 v0, p0

    #@269
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@26b
    move-object/from16 v37, v0

    #@26d
    invoke-virtual/range {v37 .. v37}, Landroid/view/MotionEvent;->getDownTime()J

    #@270
    move-result-wide v37

    #@271
    sget v39, Landroid/view/GestureDetector;->TAP_TIMEOUT:I

    #@273
    move/from16 v0, v39

    #@275
    int-to-long v0, v0

    #@276
    move-wide/from16 v39, v0

    #@278
    add-long v37, v37, v39

    #@27a
    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    #@27d
    .line 543
    move-object/from16 v0, p0

    #@27f
    iget-object v0, v0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@281
    move-object/from16 v35, v0

    #@283
    move-object/from16 v0, v35

    #@285
    move-object/from16 v1, p1

    #@287
    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    #@28a
    move-result v35

    #@28b
    or-int v16, v16, v35

    #@28d
    .line 544
    goto/16 :goto_99

    #@28f
    .line 522
    .restart local v15       #hadTapMessage:Z
    :cond_28f
    move-object/from16 v0, p0

    #@291
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@293
    move-object/from16 v35, v0

    #@295
    const/16 v36, 0x3

    #@297
    sget v37, Landroid/view/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    #@299
    move/from16 v0, v37

    #@29b
    int-to-long v0, v0

    #@29c
    move-wide/from16 v37, v0

    #@29e
    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2a1
    goto/16 :goto_1da

    #@2a3
    .line 547
    .end local v15           #hadTapMessage:Z
    :pswitch_2a3
    move-object/from16 v0, p0

    #@2a5
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@2a7
    move/from16 v35, v0

    #@2a9
    if-nez v35, :cond_99

    #@2ab
    .line 550
    move-object/from16 v0, p0

    #@2ad
    iget v0, v0, Landroid/view/GestureDetector;->mLastFocusX:F

    #@2af
    move/from16 v35, v0

    #@2b1
    sub-float v22, v35, v13

    #@2b3
    .line 551
    .local v22, scrollX:F
    move-object/from16 v0, p0

    #@2b5
    iget v0, v0, Landroid/view/GestureDetector;->mLastFocusY:F

    #@2b7
    move/from16 v35, v0

    #@2b9
    sub-float v23, v35, v14

    #@2bb
    .line 552
    .local v23, scrollY:F
    move-object/from16 v0, p0

    #@2bd
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mIsDoubleTapping:Z

    #@2bf
    move/from16 v35, v0

    #@2c1
    if-eqz v35, :cond_2d5

    #@2c3
    .line 554
    move-object/from16 v0, p0

    #@2c5
    iget-object v0, v0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@2c7
    move-object/from16 v35, v0

    #@2c9
    move-object/from16 v0, v35

    #@2cb
    move-object/from16 v1, p1

    #@2cd
    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    #@2d0
    move-result v35

    #@2d1
    or-int v16, v16, v35

    #@2d3
    goto/16 :goto_99

    #@2d5
    .line 555
    :cond_2d5
    move-object/from16 v0, p0

    #@2d7
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mAlwaysInTapRegion:Z

    #@2d9
    move/from16 v35, v0

    #@2db
    if-eqz v35, :cond_362

    #@2dd
    .line 556
    move-object/from16 v0, p0

    #@2df
    iget v0, v0, Landroid/view/GestureDetector;->mDownFocusX:F

    #@2e1
    move/from16 v35, v0

    #@2e3
    sub-float v35, v13, v35

    #@2e5
    move/from16 v0, v35

    #@2e7
    float-to-int v8, v0

    #@2e8
    .line 557
    .local v8, deltaX:I
    move-object/from16 v0, p0

    #@2ea
    iget v0, v0, Landroid/view/GestureDetector;->mDownFocusY:F

    #@2ec
    move/from16 v35, v0

    #@2ee
    sub-float v35, v14, v35

    #@2f0
    move/from16 v0, v35

    #@2f2
    float-to-int v9, v0

    #@2f3
    .line 558
    .local v9, deltaY:I
    mul-int v35, v8, v8

    #@2f5
    mul-int v36, v9, v9

    #@2f7
    add-int v10, v35, v36

    #@2f9
    .line 559
    .local v10, distance:I
    move-object/from16 v0, p0

    #@2fb
    iget v0, v0, Landroid/view/GestureDetector;->mTouchSlopSquare:I

    #@2fd
    move/from16 v35, v0

    #@2ff
    move/from16 v0, v35

    #@301
    if-le v10, v0, :cond_34e

    #@303
    .line 560
    move-object/from16 v0, p0

    #@305
    iget-object v0, v0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@307
    move-object/from16 v35, v0

    #@309
    move-object/from16 v0, p0

    #@30b
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@30d
    move-object/from16 v36, v0

    #@30f
    move-object/from16 v0, v35

    #@311
    move-object/from16 v1, v36

    #@313
    move-object/from16 v2, p1

    #@315
    move/from16 v3, v22

    #@317
    move/from16 v4, v23

    #@319
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    #@31c
    move-result v16

    #@31d
    .line 561
    move-object/from16 v0, p0

    #@31f
    iput v13, v0, Landroid/view/GestureDetector;->mLastFocusX:F

    #@321
    .line 562
    move-object/from16 v0, p0

    #@323
    iput v14, v0, Landroid/view/GestureDetector;->mLastFocusY:F

    #@325
    .line 563
    const/16 v35, 0x0

    #@327
    move/from16 v0, v35

    #@329
    move-object/from16 v1, p0

    #@32b
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mAlwaysInTapRegion:Z

    #@32d
    .line 564
    move-object/from16 v0, p0

    #@32f
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@331
    move-object/from16 v35, v0

    #@333
    const/16 v36, 0x3

    #@335
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@338
    .line 565
    move-object/from16 v0, p0

    #@33a
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@33c
    move-object/from16 v35, v0

    #@33e
    const/16 v36, 0x1

    #@340
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@343
    .line 566
    move-object/from16 v0, p0

    #@345
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@347
    move-object/from16 v35, v0

    #@349
    const/16 v36, 0x2

    #@34b
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@34e
    .line 568
    :cond_34e
    move-object/from16 v0, p0

    #@350
    iget v0, v0, Landroid/view/GestureDetector;->mDoubleTapTouchSlopSquare:I

    #@352
    move/from16 v35, v0

    #@354
    move/from16 v0, v35

    #@356
    if-le v10, v0, :cond_99

    #@358
    .line 569
    const/16 v35, 0x0

    #@35a
    move/from16 v0, v35

    #@35c
    move-object/from16 v1, p0

    #@35e
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    #@360
    goto/16 :goto_99

    #@362
    .line 571
    .end local v8           #deltaX:I
    .end local v9           #deltaY:I
    .end local v10           #distance:I
    :cond_362
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    #@365
    move-result v35

    #@366
    const/high16 v36, 0x3f80

    #@368
    cmpl-float v35, v35, v36

    #@36a
    if-gez v35, :cond_376

    #@36c
    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    #@36f
    move-result v35

    #@370
    const/high16 v36, 0x3f80

    #@372
    cmpl-float v35, v35, v36

    #@374
    if-ltz v35, :cond_99

    #@376
    .line 572
    :cond_376
    move-object/from16 v0, p0

    #@378
    iget-object v0, v0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@37a
    move-object/from16 v35, v0

    #@37c
    move-object/from16 v0, p0

    #@37e
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@380
    move-object/from16 v36, v0

    #@382
    move-object/from16 v0, v35

    #@384
    move-object/from16 v1, v36

    #@386
    move-object/from16 v2, p1

    #@388
    move/from16 v3, v22

    #@38a
    move/from16 v4, v23

    #@38c
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    #@38f
    move-result v16

    #@390
    .line 573
    move-object/from16 v0, p0

    #@392
    iput v13, v0, Landroid/view/GestureDetector;->mLastFocusX:F

    #@394
    .line 574
    move-object/from16 v0, p0

    #@396
    iput v14, v0, Landroid/view/GestureDetector;->mLastFocusY:F

    #@398
    goto/16 :goto_99

    #@39a
    .line 579
    .end local v22           #scrollX:F
    .end local v23           #scrollY:F
    :pswitch_39a
    const/16 v35, 0x0

    #@39c
    move/from16 v0, v35

    #@39e
    move-object/from16 v1, p0

    #@3a0
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mStillDown:Z

    #@3a2
    .line 580
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@3a5
    move-result-object v7

    #@3a6
    .line 581
    .local v7, currentUpEvent:Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    #@3a8
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mIsDoubleTapping:Z

    #@3aa
    move/from16 v35, v0

    #@3ac
    if-eqz v35, :cond_40c

    #@3ae
    .line 583
    move-object/from16 v0, p0

    #@3b0
    iget-object v0, v0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@3b2
    move-object/from16 v35, v0

    #@3b4
    move-object/from16 v0, v35

    #@3b6
    move-object/from16 v1, p1

    #@3b8
    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    #@3bb
    move-result v35

    #@3bc
    or-int v16, v16, v35

    #@3be
    .line 603
    :cond_3be
    :goto_3be
    move-object/from16 v0, p0

    #@3c0
    iget-object v0, v0, Landroid/view/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    #@3c2
    move-object/from16 v35, v0

    #@3c4
    if-eqz v35, :cond_3cf

    #@3c6
    .line 604
    move-object/from16 v0, p0

    #@3c8
    iget-object v0, v0, Landroid/view/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    #@3ca
    move-object/from16 v35, v0

    #@3cc
    invoke-virtual/range {v35 .. v35}, Landroid/view/MotionEvent;->recycle()V

    #@3cf
    .line 607
    :cond_3cf
    move-object/from16 v0, p0

    #@3d1
    iput-object v7, v0, Landroid/view/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    #@3d3
    .line 608
    move-object/from16 v0, p0

    #@3d5
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@3d7
    move-object/from16 v35, v0

    #@3d9
    if-eqz v35, :cond_3ec

    #@3db
    .line 611
    move-object/from16 v0, p0

    #@3dd
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@3df
    move-object/from16 v35, v0

    #@3e1
    invoke-virtual/range {v35 .. v35}, Landroid/view/VelocityTracker;->recycle()V

    #@3e4
    .line 612
    const/16 v35, 0x0

    #@3e6
    move-object/from16 v0, v35

    #@3e8
    move-object/from16 v1, p0

    #@3ea
    iput-object v0, v1, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@3ec
    .line 614
    :cond_3ec
    const/16 v35, 0x0

    #@3ee
    move/from16 v0, v35

    #@3f0
    move-object/from16 v1, p0

    #@3f2
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mIsDoubleTapping:Z

    #@3f4
    .line 615
    move-object/from16 v0, p0

    #@3f6
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@3f8
    move-object/from16 v35, v0

    #@3fa
    const/16 v36, 0x1

    #@3fc
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@3ff
    .line 616
    move-object/from16 v0, p0

    #@401
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@403
    move-object/from16 v35, v0

    #@405
    const/16 v36, 0x2

    #@407
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@40a
    goto/16 :goto_99

    #@40c
    .line 584
    :cond_40c
    move-object/from16 v0, p0

    #@40e
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mInLongPress:Z

    #@410
    move/from16 v35, v0

    #@412
    if-eqz v35, :cond_428

    #@414
    .line 585
    move-object/from16 v0, p0

    #@416
    iget-object v0, v0, Landroid/view/GestureDetector;->mHandler:Landroid/os/Handler;

    #@418
    move-object/from16 v35, v0

    #@41a
    const/16 v36, 0x3

    #@41c
    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    #@41f
    .line 586
    const/16 v35, 0x0

    #@421
    move/from16 v0, v35

    #@423
    move-object/from16 v1, p0

    #@425
    iput-boolean v0, v1, Landroid/view/GestureDetector;->mInLongPress:Z

    #@427
    goto :goto_3be

    #@428
    .line 587
    :cond_428
    move-object/from16 v0, p0

    #@42a
    iget-boolean v0, v0, Landroid/view/GestureDetector;->mAlwaysInTapRegion:Z

    #@42c
    move/from16 v35, v0

    #@42e
    if-eqz v35, :cond_43f

    #@430
    .line 588
    move-object/from16 v0, p0

    #@432
    iget-object v0, v0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@434
    move-object/from16 v35, v0

    #@436
    move-object/from16 v0, v35

    #@438
    move-object/from16 v1, p1

    #@43a
    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    #@43d
    move-result v16

    #@43e
    goto :goto_3be

    #@43f
    .line 592
    :cond_43f
    move-object/from16 v0, p0

    #@441
    iget-object v0, v0, Landroid/view/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@443
    move-object/from16 v28, v0

    #@445
    .line 593
    .local v28, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v35, 0x0

    #@447
    move-object/from16 v0, p1

    #@449
    move/from16 v1, v35

    #@44b
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@44e
    move-result v20

    #@44f
    .line 594
    .local v20, pointerId:I
    const/16 v35, 0x3e8

    #@451
    move-object/from16 v0, p0

    #@453
    iget v0, v0, Landroid/view/GestureDetector;->mMaximumFlingVelocity:I

    #@455
    move/from16 v36, v0

    #@457
    move/from16 v0, v36

    #@459
    int-to-float v0, v0

    #@45a
    move/from16 v36, v0

    #@45c
    move-object/from16 v0, v28

    #@45e
    move/from16 v1, v35

    #@460
    move/from16 v2, v36

    #@462
    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@465
    .line 595
    move-object/from16 v0, v28

    #@467
    move/from16 v1, v20

    #@469
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@46c
    move-result v30

    #@46d
    .line 596
    .local v30, velocityY:F
    move-object/from16 v0, v28

    #@46f
    move/from16 v1, v20

    #@471
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@474
    move-result v29

    #@475
    .line 598
    .local v29, velocityX:F
    invoke-static/range {v30 .. v30}, Ljava/lang/Math;->abs(F)F

    #@478
    move-result v35

    #@479
    move-object/from16 v0, p0

    #@47b
    iget v0, v0, Landroid/view/GestureDetector;->mMinimumFlingVelocity:I

    #@47d
    move/from16 v36, v0

    #@47f
    move/from16 v0, v36

    #@481
    int-to-float v0, v0

    #@482
    move/from16 v36, v0

    #@484
    cmpl-float v35, v35, v36

    #@486
    if-gtz v35, :cond_49b

    #@488
    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(F)F

    #@48b
    move-result v35

    #@48c
    move-object/from16 v0, p0

    #@48e
    iget v0, v0, Landroid/view/GestureDetector;->mMinimumFlingVelocity:I

    #@490
    move/from16 v36, v0

    #@492
    move/from16 v0, v36

    #@494
    int-to-float v0, v0

    #@495
    move/from16 v36, v0

    #@497
    cmpl-float v35, v35, v36

    #@499
    if-lez v35, :cond_3be

    #@49b
    .line 600
    :cond_49b
    move-object/from16 v0, p0

    #@49d
    iget-object v0, v0, Landroid/view/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    #@49f
    move-object/from16 v35, v0

    #@4a1
    move-object/from16 v0, p0

    #@4a3
    iget-object v0, v0, Landroid/view/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    #@4a5
    move-object/from16 v36, v0

    #@4a7
    move-object/from16 v0, v35

    #@4a9
    move-object/from16 v1, v36

    #@4ab
    move-object/from16 v2, p1

    #@4ad
    move/from16 v3, v29

    #@4af
    move/from16 v4, v30

    #@4b1
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    #@4b4
    move-result v16

    #@4b5
    goto/16 :goto_3be

    #@4b7
    .line 620
    .end local v7           #currentUpEvent:Landroid/view/MotionEvent;
    .end local v20           #pointerId:I
    .end local v28           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v29           #velocityX:F
    .end local v30           #velocityY:F
    :pswitch_4b7
    invoke-direct/range {p0 .. p0}, Landroid/view/GestureDetector;->cancel()V

    #@4ba
    goto/16 :goto_99

    #@4bc
    .line 474
    :pswitch_data_4bc
    .packed-switch 0x0
        :pswitch_163
        :pswitch_39a
        :pswitch_2a3
        :pswitch_4b7
        :pswitch_99
        :pswitch_b7
        :pswitch_cb
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .registers 2
    .parameter "isLongpressEnabled"

    #@0
    .prologue
    .line 426
    iput-boolean p1, p0, Landroid/view/GestureDetector;->mIsLongpressEnabled:Z

    #@2
    .line 427
    return-void
.end method

.method public setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .registers 2
    .parameter "onDoubleTapListener"

    #@0
    .prologue
    .line 413
    iput-object p1, p0, Landroid/view/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    #@2
    .line 414
    return-void
.end method
