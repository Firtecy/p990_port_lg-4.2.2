.class final Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "InvalidateOnAnimationRunnable"
.end annotation


# instance fields
.field private mPosted:Z

.field private mTempViewRects:[Landroid/view/View$AttachInfo$InvalidateInfo;

.field private mTempViews:[Landroid/view/View;

.field private mViewRects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View$AttachInfo$InvalidateInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 4600
    iput-object p1, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4602
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@c
    .line 4603
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@13
    return-void
.end method

.method private postIfNeededLocked()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 4676
    iget-boolean v0, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mPosted:Z

    #@3
    if-nez v0, :cond_f

    #@5
    .line 4677
    iget-object v0, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->this$0:Landroid/view/ViewRootImpl;

    #@7
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v2, p0, v1}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@d
    .line 4678
    iput-boolean v2, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mPosted:Z

    #@f
    .line 4680
    :cond_f
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 4609
    monitor-enter p0

    #@1
    .line 4610
    :try_start_1
    iget-object v0, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6
    .line 4611
    invoke-direct {p0}, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->postIfNeededLocked()V

    #@9
    .line 4612
    monitor-exit p0

    #@a
    .line 4613
    return-void

    #@b
    .line 4612
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public addViewRect(Landroid/view/View$AttachInfo$InvalidateInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 4616
    monitor-enter p0

    #@1
    .line 4617
    :try_start_1
    iget-object v0, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6
    .line 4618
    invoke-direct {p0}, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->postIfNeededLocked()V

    #@9
    .line 4619
    monitor-exit p0

    #@a
    .line 4620
    return-void

    #@b
    .line 4619
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .registers 8
    .parameter "view"

    #@0
    .prologue
    .line 4623
    monitor-enter p0

    #@1
    .line 4624
    :try_start_1
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@6
    .line 4626
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v0

    #@c
    .local v0, i:I
    move v1, v0

    #@d
    .end local v0           #i:I
    .local v1, i:I
    :goto_d
    add-int/lit8 v0, v1, -0x1

    #@f
    .end local v1           #i:I
    .restart local v0       #i:I
    if-lez v1, :cond_27

    #@11
    .line 4627
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Landroid/view/View$AttachInfo$InvalidateInfo;

    #@19
    .line 4628
    .local v2, info:Landroid/view/View$AttachInfo$InvalidateInfo;
    iget-object v3, v2, Landroid/view/View$AttachInfo$InvalidateInfo;->target:Landroid/view/View;

    #@1b
    if-ne v3, p1, :cond_25

    #@1d
    .line 4629
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@22
    .line 4630
    invoke-virtual {v2}, Landroid/view/View$AttachInfo$InvalidateInfo;->release()V

    #@25
    :cond_25
    move v1, v0

    #@26
    .line 4632
    .end local v0           #i:I
    .restart local v1       #i:I
    goto :goto_d

    #@27
    .line 4634
    .end local v1           #i:I
    .end local v2           #info:Landroid/view/View$AttachInfo$InvalidateInfo;
    .restart local v0       #i:I
    :cond_27
    iget-boolean v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mPosted:Z

    #@29
    if-eqz v3, :cond_47

    #@2b
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_47

    #@33
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_47

    #@3b
    .line 4635
    iget-object v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->this$0:Landroid/view/ViewRootImpl;

    #@3d
    iget-object v3, v3, Landroid/view/ViewRootImpl;->mChoreographer:Landroid/view/Choreographer;

    #@3f
    const/4 v4, 0x1

    #@40
    const/4 v5, 0x0

    #@41
    invoke-virtual {v3, v4, p0, v5}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    #@44
    .line 4636
    const/4 v3, 0x0

    #@45
    iput-boolean v3, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mPosted:Z

    #@47
    .line 4638
    :cond_47
    monitor-exit p0

    #@48
    .line 4639
    return-void

    #@49
    .line 4638
    .end local v0           #i:I
    :catchall_49
    move-exception v3

    #@4a
    monitor-exit p0
    :try_end_4b
    .catchall {:try_start_1 .. :try_end_4b} :catchall_49

    #@4b
    throw v3
.end method

.method public run()V
    .registers 10

    #@0
    .prologue
    .line 4645
    monitor-enter p0

    #@1
    .line 4646
    const/4 v4, 0x0

    #@2
    :try_start_2
    iput-boolean v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mPosted:Z

    #@4
    .line 4648
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    .line 4649
    .local v2, viewCount:I
    if-eqz v2, :cond_21

    #@c
    .line 4650
    iget-object v5, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@e
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViews:[Landroid/view/View;

    #@10
    if-eqz v4, :cond_51

    #@12
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViews:[Landroid/view/View;

    #@14
    :goto_14
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, [Landroid/view/View;

    #@1a
    iput-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViews:[Landroid/view/View;

    #@1c
    .line 4652
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViews:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@21
    .line 4655
    :cond_21
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v3

    #@27
    .line 4656
    .local v3, viewRectCount:I
    if-eqz v3, :cond_3e

    #@29
    .line 4657
    iget-object v5, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@2b
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViewRects:[Landroid/view/View$AttachInfo$InvalidateInfo;

    #@2d
    if-eqz v4, :cond_54

    #@2f
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViewRects:[Landroid/view/View$AttachInfo$InvalidateInfo;

    #@31
    :goto_31
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@34
    move-result-object v4

    #@35
    check-cast v4, [Landroid/view/View$AttachInfo$InvalidateInfo;

    #@37
    iput-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViewRects:[Landroid/view/View$AttachInfo$InvalidateInfo;

    #@39
    .line 4659
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mViewRects:Ljava/util/ArrayList;

    #@3b
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@3e
    .line 4661
    :cond_3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_2 .. :try_end_3f} :catchall_57

    #@3f
    .line 4663
    const/4 v0, 0x0

    #@40
    .local v0, i:I
    :goto_40
    if-ge v0, v2, :cond_5a

    #@42
    .line 4664
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViews:[Landroid/view/View;

    #@44
    aget-object v4, v4, v0

    #@46
    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    #@49
    .line 4665
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViews:[Landroid/view/View;

    #@4b
    const/4 v5, 0x0

    #@4c
    aput-object v5, v4, v0

    #@4e
    .line 4663
    add-int/lit8 v0, v0, 0x1

    #@50
    goto :goto_40

    #@51
    .line 4650
    .end local v0           #i:I
    .end local v3           #viewRectCount:I
    :cond_51
    :try_start_51
    new-array v4, v2, [Landroid/view/View;

    #@53
    goto :goto_14

    #@54
    .line 4657
    .restart local v3       #viewRectCount:I
    :cond_54
    new-array v4, v3, [Landroid/view/View$AttachInfo$InvalidateInfo;

    #@56
    goto :goto_31

    #@57
    .line 4661
    .end local v2           #viewCount:I
    .end local v3           #viewRectCount:I
    :catchall_57
    move-exception v4

    #@58
    monitor-exit p0
    :try_end_59
    .catchall {:try_start_51 .. :try_end_59} :catchall_57

    #@59
    throw v4

    #@5a
    .line 4668
    .restart local v0       #i:I
    .restart local v2       #viewCount:I
    .restart local v3       #viewRectCount:I
    :cond_5a
    const/4 v0, 0x0

    #@5b
    :goto_5b
    if-ge v0, v3, :cond_74

    #@5d
    .line 4669
    iget-object v4, p0, Landroid/view/ViewRootImpl$InvalidateOnAnimationRunnable;->mTempViewRects:[Landroid/view/View$AttachInfo$InvalidateInfo;

    #@5f
    aget-object v1, v4, v0

    #@61
    .line 4670
    .local v1, info:Landroid/view/View$AttachInfo$InvalidateInfo;
    iget-object v4, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->target:Landroid/view/View;

    #@63
    iget v5, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->left:I

    #@65
    iget v6, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->top:I

    #@67
    iget v7, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->right:I

    #@69
    iget v8, v1, Landroid/view/View$AttachInfo$InvalidateInfo;->bottom:I

    #@6b
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->invalidate(IIII)V

    #@6e
    .line 4671
    invoke-virtual {v1}, Landroid/view/View$AttachInfo$InvalidateInfo;->release()V

    #@71
    .line 4668
    add-int/lit8 v0, v0, 0x1

    #@73
    goto :goto_5b

    #@74
    .line 4673
    .end local v1           #info:Landroid/view/View$AttachInfo$InvalidateInfo;
    :cond_74
    return-void
.end method
