.class public Landroid/view/KeyEvent$DispatcherState;
.super Ljava/lang/Object;
.source "KeyEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/KeyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DispatcherState"
.end annotation


# instance fields
.field mActiveLongPresses:Landroid/util/SparseIntArray;

.field mDownKeyCode:I

.field mDownTarget:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 2759
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2762
    new-instance v0, Landroid/util/SparseIntArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/KeyEvent$DispatcherState;->mActiveLongPresses:Landroid/util/SparseIntArray;

    #@a
    return-void
.end method


# virtual methods
.method public handleUpEvent(Landroid/view/KeyEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 2830
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@3
    move-result v1

    #@4
    .line 2832
    .local v1, keyCode:I
    iget-object v2, p0, Landroid/view/KeyEvent$DispatcherState;->mActiveLongPresses:Landroid/util/SparseIntArray;

    #@6
    invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    #@9
    move-result v0

    #@a
    .line 2833
    .local v0, index:I
    if-ltz v0, :cond_16

    #@c
    .line 2835
    const/16 v2, 0x120

    #@e
    invoke-static {p1, v2}, Landroid/view/KeyEvent;->access$076(Landroid/view/KeyEvent;I)I

    #@11
    .line 2836
    iget-object v2, p0, Landroid/view/KeyEvent$DispatcherState;->mActiveLongPresses:Landroid/util/SparseIntArray;

    #@13
    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    #@16
    .line 2838
    :cond_16
    iget v2, p0, Landroid/view/KeyEvent$DispatcherState;->mDownKeyCode:I

    #@18
    if-ne v2, v1, :cond_25

    #@1a
    .line 2840
    const/16 v2, 0x200

    #@1c
    invoke-static {p1, v2}, Landroid/view/KeyEvent;->access$076(Landroid/view/KeyEvent;I)I

    #@1f
    .line 2841
    const/4 v2, 0x0

    #@20
    iput v2, p0, Landroid/view/KeyEvent$DispatcherState;->mDownKeyCode:I

    #@22
    .line 2842
    const/4 v2, 0x0

    #@23
    iput-object v2, p0, Landroid/view/KeyEvent$DispatcherState;->mDownTarget:Ljava/lang/Object;

    #@25
    .line 2844
    :cond_25
    return-void
.end method

.method public isTracking(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 2810
    iget v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownKeyCode:I

    #@2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5
    move-result v1

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public performedLongPress(Landroid/view/KeyEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 2820
    iget-object v0, p0, Landroid/view/KeyEvent$DispatcherState;->mActiveLongPresses:Landroid/util/SparseIntArray;

    #@2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5
    move-result v1

    #@6
    const/4 v2, 0x1

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    #@a
    .line 2821
    return-void
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 2769
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownKeyCode:I

    #@3
    .line 2770
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownTarget:Ljava/lang/Object;

    #@6
    .line 2771
    iget-object v0, p0, Landroid/view/KeyEvent$DispatcherState;->mActiveLongPresses:Landroid/util/SparseIntArray;

    #@8
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    #@b
    .line 2772
    return-void
.end method

.method public reset(Ljava/lang/Object;)V
    .registers 3
    .parameter "target"

    #@0
    .prologue
    .line 2778
    iget-object v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownTarget:Ljava/lang/Object;

    #@2
    if-ne v0, p1, :cond_a

    #@4
    .line 2780
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownKeyCode:I

    #@7
    .line 2781
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownTarget:Ljava/lang/Object;

    #@a
    .line 2783
    :cond_a
    return-void
.end method

.method public startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V
    .registers 5
    .parameter "event"
    .parameter "target"

    #@0
    .prologue
    .line 2796
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 2797
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Can only start tracking on a down event"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 2801
    :cond_e
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/view/KeyEvent$DispatcherState;->mDownKeyCode:I

    #@14
    .line 2802
    iput-object p2, p0, Landroid/view/KeyEvent$DispatcherState;->mDownTarget:Ljava/lang/Object;

    #@16
    .line 2803
    return-void
.end method
