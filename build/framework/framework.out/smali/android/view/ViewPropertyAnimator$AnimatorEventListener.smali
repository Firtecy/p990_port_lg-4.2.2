.class Landroid/view/ViewPropertyAnimator$AnimatorEventListener;
.super Ljava/lang/Object;
.source "ViewPropertyAnimator.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewPropertyAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimatorEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewPropertyAnimator;


# direct methods
.method private constructor <init>(Landroid/view/ViewPropertyAnimator;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 929
    iput-object p1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/ViewPropertyAnimator;Landroid/view/ViewPropertyAnimator$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 929
    invoke-direct {p0, p1}, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;-><init>(Landroid/view/ViewPropertyAnimator;)V

    #@3
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter "animation"

    #@0
    .prologue
    .line 954
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@2
    invoke-static {v0}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_11

    #@8
    .line 955
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@a
    invoke-static {v0}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    #@11
    .line 957
    :cond_11
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@13
    invoke-static {v0}, Landroid/view/ViewPropertyAnimator;->access$600(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@16
    move-result-object v0

    #@17
    if-eqz v0, :cond_22

    #@19
    .line 958
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@1b
    invoke-static {v0}, Landroid/view/ViewPropertyAnimator;->access$600(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 960
    :cond_22
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 5
    .parameter "animation"

    #@0
    .prologue
    .line 971
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@2
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, v2}, Landroid/view/View;->setHasTransientState(Z)V

    #@a
    .line 972
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@c
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@f
    move-result-object v1

    #@10
    if-eqz v1, :cond_1b

    #@12
    .line 973
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@14
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@17
    move-result-object v1

    #@18
    invoke-interface {v1, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    #@1b
    .line 975
    :cond_1b
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@1d
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$600(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@20
    move-result-object v1

    #@21
    if-eqz v1, :cond_3d

    #@23
    .line 976
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@25
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$600(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Ljava/lang/Runnable;

    #@2f
    .line 977
    .local v0, r:Ljava/lang/Runnable;
    if-eqz v0, :cond_34

    #@31
    .line 978
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@34
    .line 980
    :cond_34
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@36
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$600(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 982
    .end local v0           #r:Ljava/lang/Runnable;
    :cond_3d
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@3f
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$700(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@42
    move-result-object v1

    #@43
    if-eqz v1, :cond_5f

    #@45
    .line 983
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@47
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$700(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4e
    move-result-object v0

    #@4f
    check-cast v0, Ljava/lang/Runnable;

    #@51
    .line 984
    .restart local v0       #r:Ljava/lang/Runnable;
    if-eqz v0, :cond_56

    #@53
    .line 985
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@56
    .line 987
    :cond_56
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@58
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$700(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@5b
    move-result-object v1

    #@5c
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5f
    .line 989
    .end local v0           #r:Ljava/lang/Runnable;
    :cond_5f
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@61
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$800(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    .line 990
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 3
    .parameter "animation"

    #@0
    .prologue
    .line 964
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@2
    invoke-static {v0}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_11

    #@8
    .line 965
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@a
    invoke-static {v0}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    #@11
    .line 967
    :cond_11
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 933
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@2
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$300(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_22

    #@8
    .line 934
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@a
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$300(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Ljava/lang/Runnable;

    #@14
    .line 935
    .local v0, r:Ljava/lang/Runnable;
    if-eqz v0, :cond_19

    #@16
    .line 936
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@19
    .line 938
    :cond_19
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@1b
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$300(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 940
    .end local v0           #r:Ljava/lang/Runnable;
    :cond_22
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@24
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$400(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@27
    move-result-object v1

    #@28
    if-eqz v1, :cond_44

    #@2a
    .line 941
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@2c
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$400(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Ljava/lang/Runnable;

    #@36
    .line 942
    .restart local v0       #r:Ljava/lang/Runnable;
    if-eqz v0, :cond_3b

    #@38
    .line 943
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@3b
    .line 945
    :cond_3b
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@3d
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$400(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@44
    .line 947
    .end local v0           #r:Ljava/lang/Runnable;
    :cond_44
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@46
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@49
    move-result-object v1

    #@4a
    if-eqz v1, :cond_55

    #@4c
    .line 948
    iget-object v1, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@4e
    invoke-static {v1}, Landroid/view/ViewPropertyAnimator;->access$500(Landroid/view/ViewPropertyAnimator;)Landroid/animation/Animator$AnimatorListener;

    #@51
    move-result-object v1

    #@52
    invoke-interface {v1, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    #@55
    .line 950
    :cond_55
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 16
    .parameter "animation"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    .line 1002
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@4
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$800(Landroid/view/ViewPropertyAnimator;)Ljava/util/HashMap;

    #@7
    move-result-object v12

    #@8
    invoke-virtual {v12, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v4

    #@c
    check-cast v4, Landroid/view/ViewPropertyAnimator$PropertyBundle;

    #@e
    .line 1003
    .local v4, propertyBundle:Landroid/view/ViewPropertyAnimator$PropertyBundle;
    if-nez v4, :cond_11

    #@10
    .line 1049
    :goto_10
    return-void

    #@11
    .line 1007
    :cond_11
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@13
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@16
    move-result-object v12

    #@17
    iget-object v12, v12, Landroid/view/View;->mDisplayList:Landroid/view/DisplayList;

    #@19
    if-eqz v12, :cond_66

    #@1b
    move v6, v10

    #@1c
    .line 1014
    .local v6, useDisplayListProperties:Z
    :goto_1c
    const/4 v0, 0x0

    #@1d
    .line 1015
    .local v0, alphaHandled:Z
    if-nez v6, :cond_28

    #@1f
    .line 1016
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@21
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@24
    move-result-object v12

    #@25
    invoke-virtual {v12}, Landroid/view/View;->invalidateParentCaches()V

    #@28
    .line 1018
    :cond_28
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    #@2b
    move-result v2

    #@2c
    .line 1019
    .local v2, fraction:F
    iget v5, v4, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    #@2e
    .line 1020
    .local v5, propertyMask:I
    and-int/lit16 v12, v5, 0x1ff

    #@30
    if-eqz v12, :cond_3b

    #@32
    .line 1021
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@34
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@37
    move-result-object v12

    #@38
    invoke-virtual {v12, v11, v11}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@3b
    .line 1023
    :cond_3b
    iget-object v8, v4, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    #@3d
    .line 1024
    .local v8, valueList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewPropertyAnimator$NameValuesHolder;>;"
    if-eqz v8, :cond_70

    #@3f
    .line 1025
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@42
    move-result v1

    #@43
    .line 1026
    .local v1, count:I
    const/4 v3, 0x0

    #@44
    .local v3, i:I
    :goto_44
    if-ge v3, v1, :cond_70

    #@46
    .line 1027
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@49
    move-result-object v9

    #@4a
    check-cast v9, Landroid/view/ViewPropertyAnimator$NameValuesHolder;

    #@4c
    .line 1028
    .local v9, values:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    iget v12, v9, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mFromValue:F

    #@4e
    iget v13, v9, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mDeltaValue:F

    #@50
    mul-float/2addr v13, v2

    #@51
    add-float v7, v12, v13

    #@53
    .line 1029
    .local v7, value:F
    iget v12, v9, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mNameConstant:I

    #@55
    const/16 v13, 0x200

    #@57
    if-ne v12, v13, :cond_68

    #@59
    .line 1030
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@5b
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@5e
    move-result-object v12

    #@5f
    invoke-virtual {v12, v7}, Landroid/view/View;->setAlphaNoInvalidation(F)Z

    #@62
    move-result v0

    #@63
    .line 1026
    :goto_63
    add-int/lit8 v3, v3, 0x1

    #@65
    goto :goto_44

    #@66
    .end local v0           #alphaHandled:Z
    .end local v1           #count:I
    .end local v2           #fraction:F
    .end local v3           #i:I
    .end local v5           #propertyMask:I
    .end local v6           #useDisplayListProperties:Z
    .end local v7           #value:F
    .end local v8           #valueList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewPropertyAnimator$NameValuesHolder;>;"
    .end local v9           #values:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :cond_66
    move v6, v11

    #@67
    .line 1007
    goto :goto_1c

    #@68
    .line 1032
    .restart local v0       #alphaHandled:Z
    .restart local v1       #count:I
    .restart local v2       #fraction:F
    .restart local v3       #i:I
    .restart local v5       #propertyMask:I
    .restart local v6       #useDisplayListProperties:Z
    .restart local v7       #value:F
    .restart local v8       #valueList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/ViewPropertyAnimator$NameValuesHolder;>;"
    .restart local v9       #values:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :cond_68
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@6a
    iget v13, v9, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mNameConstant:I

    #@6c
    invoke-static {v12, v13, v7}, Landroid/view/ViewPropertyAnimator;->access$900(Landroid/view/ViewPropertyAnimator;IF)V

    #@6f
    goto :goto_63

    #@70
    .line 1036
    .end local v1           #count:I
    .end local v3           #i:I
    .end local v7           #value:F
    .end local v9           #values:Landroid/view/ViewPropertyAnimator$NameValuesHolder;
    :cond_70
    and-int/lit16 v12, v5, 0x1ff

    #@72
    if-eqz v12, :cond_8c

    #@74
    .line 1037
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@76
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@79
    move-result-object v12

    #@7a
    iget-object v12, v12, Landroid/view/View;->mTransformationInfo:Landroid/view/View$TransformationInfo;

    #@7c
    iput-boolean v10, v12, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@7e
    .line 1038
    if-nez v6, :cond_8c

    #@80
    .line 1039
    iget-object v12, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@82
    invoke-static {v12}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@85
    move-result-object v12

    #@86
    iget v13, v12, Landroid/view/View;->mPrivateFlags:I

    #@88
    or-int/lit8 v13, v13, 0x20

    #@8a
    iput v13, v12, Landroid/view/View;->mPrivateFlags:I

    #@8c
    .line 1044
    :cond_8c
    if-eqz v0, :cond_99

    #@8e
    .line 1045
    iget-object v11, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@90
    invoke-static {v11}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@93
    move-result-object v11

    #@94
    invoke-virtual {v11, v10}, Landroid/view/View;->invalidate(Z)V

    #@97
    goto/16 :goto_10

    #@99
    .line 1047
    :cond_99
    iget-object v10, p0, Landroid/view/ViewPropertyAnimator$AnimatorEventListener;->this$0:Landroid/view/ViewPropertyAnimator;

    #@9b
    invoke-static {v10}, Landroid/view/ViewPropertyAnimator;->access$200(Landroid/view/ViewPropertyAnimator;)Landroid/view/View;

    #@9e
    move-result-object v10

    #@9f
    invoke-virtual {v10, v11, v11}, Landroid/view/View;->invalidateViewProperty(ZZ)V

    #@a2
    goto/16 :goto_10
.end method
