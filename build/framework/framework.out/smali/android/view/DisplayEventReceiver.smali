.class public abstract Landroid/view/DisplayEventReceiver;
.super Ljava/lang/Object;
.source "DisplayEventReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DisplayEventReceiver"


# instance fields
.field private final mCloseGuard:Ldalvik/system/CloseGuard;

.field private mMessageQueue:Landroid/os/MessageQueue;

.field private mReceiverPtr:I


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .registers 4
    .parameter "looper"

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/view/DisplayEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 56
    if-nez p1, :cond_14

    #@b
    .line 57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string/jumbo v1, "looper must not be null"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 60
    :cond_14
    invoke-virtual {p1}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Landroid/view/DisplayEventReceiver;->mMessageQueue:Landroid/os/MessageQueue;

    #@1a
    .line 61
    iget-object v0, p0, Landroid/view/DisplayEventReceiver;->mMessageQueue:Landroid/os/MessageQueue;

    #@1c
    invoke-static {p0, v0}, Landroid/view/DisplayEventReceiver;->nativeInit(Landroid/view/DisplayEventReceiver;Landroid/os/MessageQueue;)I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Landroid/view/DisplayEventReceiver;->mReceiverPtr:I

    #@22
    .line 63
    iget-object v0, p0, Landroid/view/DisplayEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@24
    const-string v1, "dispose"

    #@26
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@29
    .line 64
    return-void
.end method

.method private dispatchHotplug(JIZ)V
    .registers 5
    .parameter "timestampNanos"
    .parameter "builtInDisplayId"
    .parameter "connected"

    #@0
    .prologue
    .line 145
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/DisplayEventReceiver;->onHotplug(JIZ)V

    #@3
    .line 146
    return-void
.end method

.method private dispatchVsync(JII)V
    .registers 5
    .parameter "timestampNanos"
    .parameter "builtInDisplayId"
    .parameter "frame"

    #@0
    .prologue
    .line 139
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/DisplayEventReceiver;->onVsync(JII)V

    #@3
    .line 140
    return-void
.end method

.method private dispose(Z)V
    .registers 3
    .parameter "finalized"

    #@0
    .prologue
    .line 83
    iget-object v0, p0, Landroid/view/DisplayEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 84
    if-eqz p1, :cond_b

    #@6
    .line 85
    iget-object v0, p0, Landroid/view/DisplayEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@8
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    #@b
    .line 87
    :cond_b
    iget-object v0, p0, Landroid/view/DisplayEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@d
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@10
    .line 90
    :cond_10
    iget v0, p0, Landroid/view/DisplayEventReceiver;->mReceiverPtr:I

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 91
    iget v0, p0, Landroid/view/DisplayEventReceiver;->mReceiverPtr:I

    #@16
    invoke-static {v0}, Landroid/view/DisplayEventReceiver;->nativeDispose(I)V

    #@19
    .line 92
    const/4 v0, 0x0

    #@1a
    iput v0, p0, Landroid/view/DisplayEventReceiver;->mReceiverPtr:I

    #@1c
    .line 94
    :cond_1c
    const/4 v0, 0x0

    #@1d
    iput-object v0, p0, Landroid/view/DisplayEventReceiver;->mMessageQueue:Landroid/os/MessageQueue;

    #@1f
    .line 95
    return-void
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeInit(Landroid/view/DisplayEventReceiver;Landroid/os/MessageQueue;)I
.end method

.method private static native nativeScheduleVsync(I)V
.end method


# virtual methods
.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/view/DisplayEventReceiver;->dispose(Z)V

    #@4
    .line 80
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    const/4 v0, 0x1

    #@1
    :try_start_1
    invoke-direct {p0, v0}, Landroid/view/DisplayEventReceiver;->dispose(Z)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_8

    #@4
    .line 71
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@7
    .line 73
    return-void

    #@8
    .line 71
    :catchall_8
    move-exception v0

    #@9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    throw v0
.end method

.method public onHotplug(JIZ)V
    .registers 5
    .parameter "timestampNanos"
    .parameter "builtInDisplayId"
    .parameter "connected"

    #@0
    .prologue
    .line 121
    return-void
.end method

.method public onVsync(JII)V
    .registers 5
    .parameter "timestampNanos"
    .parameter "builtInDisplayId"
    .parameter "frame"

    #@0
    .prologue
    .line 109
    return-void
.end method

.method public scheduleVsync()V
    .registers 3

    #@0
    .prologue
    .line 128
    iget v0, p0, Landroid/view/DisplayEventReceiver;->mReceiverPtr:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 129
    const-string v0, "DisplayEventReceiver"

    #@6
    const-string v1, "Attempted to schedule a vertical sync pulse but the display event receiver has already been disposed."

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 134
    :goto_b
    return-void

    #@c
    .line 132
    :cond_c
    iget v0, p0, Landroid/view/DisplayEventReceiver;->mReceiverPtr:I

    #@e
    invoke-static {v0}, Landroid/view/DisplayEventReceiver;->nativeScheduleVsync(I)V

    #@11
    goto :goto_b
.end method
