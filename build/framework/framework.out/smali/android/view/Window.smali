.class public abstract Landroid/view/Window;
.super Ljava/lang/Object;
.source "Window.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/Window$Callback;
    }
.end annotation


# static fields
.field protected static final DEFAULT_FEATURES:I = 0x41

.field public static final FEATURE_ACTION_BAR:I = 0x8

.field public static final FEATURE_ACTION_BAR_OVERLAY:I = 0x9

.field public static final FEATURE_ACTION_MODE_OVERLAY:I = 0xa

.field public static final FEATURE_CONTEXT_MENU:I = 0x6

.field public static final FEATURE_CUSTOM_TITLE:I = 0x7

.field public static final FEATURE_INDETERMINATE_PROGRESS:I = 0x5

.field public static final FEATURE_LEFT_ICON:I = 0x3

.field public static final FEATURE_MAX:I = 0xa

.field public static final FEATURE_NO_TITLE:I = 0x1

.field public static final FEATURE_OPTIONS_PANEL:I = 0x0

.field public static final FEATURE_PROGRESS:I = 0x2

.field public static final FEATURE_RIGHT_ICON:I = 0x4

.field public static final ID_ANDROID_CONTENT:I = 0x1020002

.field public static final PROGRESS_END:I = 0x2710

.field public static final PROGRESS_INDETERMINATE_OFF:I = -0x4

.field public static final PROGRESS_INDETERMINATE_ON:I = -0x3

.field public static final PROGRESS_SECONDARY_END:I = 0x7530

.field public static final PROGRESS_SECONDARY_START:I = 0x4e20

.field public static final PROGRESS_START:I = 0x0

.field public static final PROGRESS_VISIBILITY_OFF:I = -0x2

.field public static final PROGRESS_VISIBILITY_ON:I = -0x1

.field private static final PROPERTY_HARDWARE_UI:Ljava/lang/String; = "persist.sys.ui.hw"


# instance fields
.field private mActiveChild:Landroid/view/Window;

.field private mAppName:Ljava/lang/String;

.field private mAppToken:Landroid/os/IBinder;

.field private mCallback:Landroid/view/Window$Callback;

.field private mCloseOnTouchOutside:Z

.field private mContainer:Landroid/view/Window;

.field private final mContext:Landroid/content/Context;

.field private mDefaultWindowFormat:I

.field private mDestroyed:Z

.field private mFeatures:I

.field private mForcedWindowFlags:I

.field private mHardwareAccelerated:Z

.field private mHasChildren:Z

.field private mHasSoftInputMode:Z

.field private mHaveDimAmount:Z

.field private mHaveWindowFormat:Z

.field private mIsActive:Z

.field private mLocalFeatures:I

.field private mSetCloseOnTouchOutside:Z

.field private final mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowStyle:Landroid/content/res/TypedArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/16 v0, 0x41

    #@2
    const/4 v1, 0x0

    #@3
    .line 390
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 138
    iput-boolean v1, p0, Landroid/view/Window;->mIsActive:Z

    #@8
    .line 139
    iput-boolean v1, p0, Landroid/view/Window;->mHasChildren:Z

    #@a
    .line 140
    iput-boolean v1, p0, Landroid/view/Window;->mCloseOnTouchOutside:Z

    #@c
    .line 141
    iput-boolean v1, p0, Landroid/view/Window;->mSetCloseOnTouchOutside:Z

    #@e
    .line 142
    iput v1, p0, Landroid/view/Window;->mForcedWindowFlags:I

    #@10
    .line 144
    iput v0, p0, Landroid/view/Window;->mFeatures:I

    #@12
    .line 145
    iput v0, p0, Landroid/view/Window;->mLocalFeatures:I

    #@14
    .line 147
    iput-boolean v1, p0, Landroid/view/Window;->mHaveWindowFormat:Z

    #@16
    .line 148
    iput-boolean v1, p0, Landroid/view/Window;->mHaveDimAmount:Z

    #@18
    .line 149
    const/4 v0, -0x1

    #@19
    iput v0, p0, Landroid/view/Window;->mDefaultWindowFormat:I

    #@1b
    .line 151
    iput-boolean v1, p0, Landroid/view/Window;->mHasSoftInputMode:Z

    #@1d
    .line 156
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@1f
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@22
    iput-object v0, p0, Landroid/view/Window;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@24
    .line 391
    iput-object p1, p0, Landroid/view/Window;->mContext:Landroid/content/Context;

    #@26
    .line 392
    return-void
.end method

.method private isOutOfBounds(Landroid/content/Context;Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "context"
    .parameter "event"

    #@0
    .prologue
    .line 840
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v4

    #@4
    float-to-int v2, v4

    #@5
    .line 841
    .local v2, x:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    #@8
    move-result v4

    #@9
    float-to-int v3, v4

    #@a
    .line 842
    .local v3, y:I
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    #@11
    move-result v1

    #@12
    .line 843
    .local v1, slop:I
    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@15
    move-result-object v0

    #@16
    .line 844
    .local v0, decorView:Landroid/view/View;
    neg-int v4, v1

    #@17
    if-lt v2, v4, :cond_2a

    #@19
    neg-int v4, v1

    #@1a
    if-lt v3, v4, :cond_2a

    #@1c
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    #@1f
    move-result v4

    #@20
    add-int/2addr v4, v1

    #@21
    if-gt v2, v4, :cond_2a

    #@23
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    #@26
    move-result v4

    #@27
    add-int/2addr v4, v1

    #@28
    if-le v3, v4, :cond_2c

    #@2a
    :cond_2a
    const/4 v4, 0x1

    #@2b
    :goto_2b
    return v4

    #@2c
    :cond_2c
    const/4 v4, 0x0

    #@2d
    goto :goto_2b
.end method


# virtual methods
.method public abstract addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public addFlags(I)V
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 709
    invoke-virtual {p0, p1, p1}, Landroid/view/Window;->setFlags(II)V

    #@3
    .line 710
    return-void
.end method

.method adjustLayoutParamsForSubWindow(Landroid/view/WindowManager$LayoutParams;)V
    .registers 8
    .parameter "wp"

    #@0
    .prologue
    const/16 v5, 0x3e8

    #@2
    .line 490
    invoke-virtual {p1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    .line 491
    .local v0, curTitle:Ljava/lang/CharSequence;
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@8
    if-lt v3, v5, :cond_91

    #@a
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@c
    const/16 v4, 0x7cf

    #@e
    if-gt v3, v4, :cond_91

    #@10
    .line 493
    iget-object v3, p1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@12
    if-nez v3, :cond_20

    #@14
    .line 494
    invoke-virtual {p0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    #@17
    move-result-object v1

    #@18
    .line 495
    .local v1, decor:Landroid/view/View;
    if-eqz v1, :cond_20

    #@1a
    .line 496
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@1d
    move-result-object v3

    #@1e
    iput-object v3, p1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@20
    .line 499
    .end local v1           #decor:Landroid/view/View;
    :cond_20
    if-eqz v0, :cond_28

    #@22
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_50

    #@28
    .line 501
    :cond_28
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2a
    const/16 v4, 0x3e9

    #@2c
    if-ne v3, v4, :cond_68

    #@2e
    .line 502
    const-string v2, "Media"

    #@30
    .line 514
    .local v2, title:Ljava/lang/String;
    :goto_30
    iget-object v3, p0, Landroid/view/Window;->mAppName:Ljava/lang/String;

    #@32
    if-eqz v3, :cond_4d

    #@34
    .line 515
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, ":"

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    iget-object v4, p0, Landroid/view/Window;->mAppName:Ljava/lang/String;

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    .line 517
    :cond_4d
    invoke-virtual {p1, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@50
    .line 528
    .end local v2           #title:Ljava/lang/String;
    :cond_50
    :goto_50
    iget-object v3, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@52
    if-nez v3, :cond_5c

    #@54
    .line 529
    iget-object v3, p0, Landroid/view/Window;->mContext:Landroid/content/Context;

    #@56
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    iput-object v3, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@5c
    .line 531
    :cond_5c
    iget-boolean v3, p0, Landroid/view/Window;->mHardwareAccelerated:Z

    #@5e
    if-eqz v3, :cond_67

    #@60
    .line 532
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@62
    const/high16 v4, 0x100

    #@64
    or-int/2addr v3, v4

    #@65
    iput v3, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@67
    .line 534
    :cond_67
    return-void

    #@68
    .line 503
    :cond_68
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@6a
    const/16 v4, 0x3ec

    #@6c
    if-ne v3, v4, :cond_71

    #@6e
    .line 504
    const-string v2, "MediaOvr"

    #@70
    .restart local v2       #title:Ljava/lang/String;
    goto :goto_30

    #@71
    .line 505
    .end local v2           #title:Ljava/lang/String;
    :cond_71
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@73
    if-ne v3, v5, :cond_78

    #@75
    .line 506
    const-string v2, "Panel"

    #@77
    .restart local v2       #title:Ljava/lang/String;
    goto :goto_30

    #@78
    .line 507
    .end local v2           #title:Ljava/lang/String;
    :cond_78
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@7a
    const/16 v4, 0x3ea

    #@7c
    if-ne v3, v4, :cond_81

    #@7e
    .line 508
    const-string v2, "SubPanel"

    #@80
    .restart local v2       #title:Ljava/lang/String;
    goto :goto_30

    #@81
    .line 509
    .end local v2           #title:Ljava/lang/String;
    :cond_81
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@83
    const/16 v4, 0x3eb

    #@85
    if-ne v3, v4, :cond_8a

    #@87
    .line 510
    const-string v2, "AtchDlg"

    #@89
    .restart local v2       #title:Ljava/lang/String;
    goto :goto_30

    #@8a
    .line 512
    .end local v2           #title:Ljava/lang/String;
    :cond_8a
    iget v3, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@8c
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    .restart local v2       #title:Ljava/lang/String;
    goto :goto_30

    #@91
    .line 520
    .end local v2           #title:Ljava/lang/String;
    :cond_91
    iget-object v3, p1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@93
    if-nez v3, :cond_9d

    #@95
    .line 521
    iget-object v3, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@97
    if-nez v3, :cond_af

    #@99
    iget-object v3, p0, Landroid/view/Window;->mAppToken:Landroid/os/IBinder;

    #@9b
    :goto_9b
    iput-object v3, p1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@9d
    .line 523
    :cond_9d
    if-eqz v0, :cond_a5

    #@9f
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@a2
    move-result v3

    #@a3
    if-nez v3, :cond_50

    #@a5
    :cond_a5
    iget-object v3, p0, Landroid/view/Window;->mAppName:Ljava/lang/String;

    #@a7
    if-eqz v3, :cond_50

    #@a9
    .line 525
    iget-object v3, p0, Landroid/view/Window;->mAppName:Ljava/lang/String;

    #@ab
    invoke-virtual {p1, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@ae
    goto :goto_50

    #@af
    .line 521
    :cond_af
    iget-object v3, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@b1
    iget-object v3, v3, Landroid/view/Window;->mAppToken:Landroid/os/IBinder;

    #@b3
    goto :goto_9b
.end method

.method public abstract alwaysReadCloseOnTouchAttr()V
.end method

.method public clearFlags(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 720
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/view/Window;->setFlags(II)V

    #@4
    .line 721
    return-void
.end method

.method public abstract closeAllPanels()V
.end method

.method public abstract closePanel(I)V
.end method

.method public final destroy()V
    .registers 2

    #@0
    .prologue
    .line 451
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/view/Window;->mDestroyed:Z

    #@3
    .line 452
    return-void
.end method

.method public findViewById(I)Landroid/view/View;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 900
    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getAppTokenOfWindow()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 1263
    iget-object v0, p0, Landroid/view/Window;->mAppToken:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public final getAttributes()Landroid/view/WindowManager$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 795
    iget-object v0, p0, Landroid/view/Window;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@2
    return-object v0
.end method

.method public final getCallback()Landroid/view/Window$Callback;
    .registers 2

    #@0
    .prologue
    .line 560
    iget-object v0, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@2
    return-object v0
.end method

.method public final getContainer()Landroid/view/Window;
    .registers 2

    #@0
    .prologue
    .line 442
    iget-object v0, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@2
    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 401
    iget-object v0, p0, Landroid/view/Window;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public abstract getCurrentFocus()Landroid/view/View;
.end method

.method public abstract getDecorView()Landroid/view/View;
.end method

.method protected final getFeatures()I
    .registers 2

    #@0
    .prologue
    .line 1174
    iget v0, p0, Landroid/view/Window;->mFeatures:I

    #@2
    return v0
.end method

.method protected final getForcedWindowFlags()I
    .registers 2

    #@0
    .prologue
    .line 803
    iget v0, p0, Landroid/view/Window;->mForcedWindowFlags:I

    #@2
    return v0
.end method

.method public abstract getLayoutInflater()Landroid/view/LayoutInflater;
.end method

.method protected final getLocalFeatures()I
    .registers 2

    #@0
    .prologue
    .line 1196
    iget v0, p0, Landroid/view/Window;->mLocalFeatures:I

    #@2
    return v0
.end method

.method public abstract getVolumeControlStream()I
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .registers 2

    #@0
    .prologue
    .line 543
    iget-object v0, p0, Landroid/view/Window;->mWindowManager:Landroid/view/WindowManager;

    #@2
    return-object v0
.end method

.method public final getWindowStyle()Landroid/content/res/TypedArray;
    .registers 3

    #@0
    .prologue
    .line 409
    monitor-enter p0

    #@1
    .line 410
    :try_start_1
    iget-object v0, p0, Landroid/view/Window;->mWindowStyle:Landroid/content/res/TypedArray;

    #@3
    if-nez v0, :cond_f

    #@5
    .line 411
    iget-object v0, p0, Landroid/view/Window;->mContext:Landroid/content/Context;

    #@7
    sget-object v1, Lcom/android/internal/R$styleable;->Window:[I

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/view/Window;->mWindowStyle:Landroid/content/res/TypedArray;

    #@f
    .line 414
    :cond_f
    iget-object v0, p0, Landroid/view/Window;->mWindowStyle:Landroid/content/res/TypedArray;

    #@11
    monitor-exit p0

    #@12
    return-object v0

    #@13
    .line 415
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public final hasChildren()Z
    .registers 2

    #@0
    .prologue
    .line 446
    iget-boolean v0, p0, Landroid/view/Window;->mHasChildren:Z

    #@2
    return v0
.end method

.method public hasFeature(I)Z
    .registers 5
    .parameter "feature"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1184
    invoke-virtual {p0}, Landroid/view/Window;->getFeatures()I

    #@4
    move-result v1

    #@5
    shl-int v2, v0, p1

    #@7
    and-int/2addr v1, v2

    #@8
    if-eqz v1, :cond_b

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method protected final hasSoftInputMode()Z
    .registers 2

    #@0
    .prologue
    .line 810
    iget-boolean v0, p0, Landroid/view/Window;->mHasSoftInputMode:Z

    #@2
    return v0
.end method

.method protected haveDimAmount()Z
    .registers 2

    #@0
    .prologue
    .line 1222
    iget-boolean v0, p0, Landroid/view/Window;->mHaveDimAmount:Z

    #@2
    return v0
.end method

.method public abstract invalidatePanelMenu(I)V
.end method

.method public final isActive()Z
    .registers 2

    #@0
    .prologue
    .line 888
    iget-boolean v0, p0, Landroid/view/Window;->mIsActive:Z

    #@2
    return v0
.end method

.method public final isDestroyed()Z
    .registers 2

    #@0
    .prologue
    .line 456
    iget-boolean v0, p0, Landroid/view/Window;->mDestroyed:Z

    #@2
    return v0
.end method

.method public abstract isFloating()Z
.end method

.method public abstract isShortcutKey(ILandroid/view/KeyEvent;)Z
.end method

.method public final makeActive()V
    .registers 3

    #@0
    .prologue
    .line 876
    iget-object v0, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 877
    iget-object v0, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@6
    iget-object v0, v0, Landroid/view/Window;->mActiveChild:Landroid/view/Window;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 878
    iget-object v0, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@c
    iget-object v0, v0, Landroid/view/Window;->mActiveChild:Landroid/view/Window;

    #@e
    const/4 v1, 0x0

    #@f
    iput-boolean v1, v0, Landroid/view/Window;->mIsActive:Z

    #@11
    .line 880
    :cond_11
    iget-object v0, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@13
    iput-object p0, v0, Landroid/view/Window;->mActiveChild:Landroid/view/Window;

    #@15
    .line 882
    :cond_15
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Landroid/view/Window;->mIsActive:Z

    #@18
    .line 883
    invoke-virtual {p0}, Landroid/view/Window;->onActive()V

    #@1b
    .line 884
    return-void
.end method

.method protected abstract onActive()V
.end method

.method public abstract onConfigurationChanged(Landroid/content/res/Configuration;)V
.end method

.method public abstract openPanel(ILandroid/view/KeyEvent;)V
.end method

.method public abstract peekDecorView()Landroid/view/View;
.end method

.method public abstract performContextMenuIdentifierAction(II)Z
.end method

.method public abstract performPanelIdentifierAction(III)Z
.end method

.method public abstract performPanelShortcut(IILandroid/view/KeyEvent;I)Z
.end method

.method protected removeFeature(I)V
    .registers 5
    .parameter "featureId"

    #@0
    .prologue
    .line 870
    const/4 v1, 0x1

    #@1
    shl-int v0, v1, p1

    #@3
    .line 871
    .local v0, flag:I
    iget v1, p0, Landroid/view/Window;->mFeatures:I

    #@5
    xor-int/lit8 v2, v0, -0x1

    #@7
    and-int/2addr v1, v2

    #@8
    iput v1, p0, Landroid/view/Window;->mFeatures:I

    #@a
    .line 872
    iget v1, p0, Landroid/view/Window;->mLocalFeatures:I

    #@c
    iget-object v2, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@e
    if-eqz v2, :cond_17

    #@10
    iget-object v2, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@12
    iget v2, v2, Landroid/view/Window;->mFeatures:I

    #@14
    xor-int/lit8 v2, v2, -0x1

    #@16
    and-int/2addr v0, v2

    #@17
    .end local v0           #flag:I
    :cond_17
    xor-int/lit8 v2, v0, -0x1

    #@19
    and-int/2addr v1, v2

    #@1a
    iput v1, p0, Landroid/view/Window;->mLocalFeatures:I

    #@1c
    .line 873
    return-void
.end method

.method public requestFeature(I)Z
    .registers 6
    .parameter "featureId"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 860
    shl-int v0, v2, p1

    #@3
    .line 861
    .local v0, flag:I
    iget v1, p0, Landroid/view/Window;->mFeatures:I

    #@5
    or-int/2addr v1, v0

    #@6
    iput v1, p0, Landroid/view/Window;->mFeatures:I

    #@8
    .line 862
    iget v3, p0, Landroid/view/Window;->mLocalFeatures:I

    #@a
    iget-object v1, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@c
    if-eqz v1, :cond_1f

    #@e
    iget-object v1, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@10
    iget v1, v1, Landroid/view/Window;->mFeatures:I

    #@12
    xor-int/lit8 v1, v1, -0x1

    #@14
    and-int/2addr v1, v0

    #@15
    :goto_15
    or-int/2addr v1, v3

    #@16
    iput v1, p0, Landroid/view/Window;->mLocalFeatures:I

    #@18
    .line 863
    iget v1, p0, Landroid/view/Window;->mFeatures:I

    #@1a
    and-int/2addr v1, v0

    #@1b
    if-eqz v1, :cond_21

    #@1d
    move v1, v2

    #@1e
    :goto_1e
    return v1

    #@1f
    :cond_1f
    move v1, v0

    #@20
    .line 862
    goto :goto_15

    #@21
    .line 863
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_1e
.end method

.method public abstract restoreHierarchyState(Landroid/os/Bundle;)V
.end method

.method public abstract saveHierarchyState()Landroid/os/Bundle;
.end method

.method public setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    .registers 4
    .parameter "a"

    #@0
    .prologue
    .line 782
    iget-object v0, p0, Landroid/view/Window;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    #@5
    .line 783
    iget-object v0, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 784
    iget-object v0, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@b
    iget-object v1, p0, Landroid/view/Window;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@d
    invoke-interface {v0, v1}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@10
    .line 786
    :cond_10
    return-void
.end method

.method public abstract setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public setBackgroundDrawableResource(I)V
    .registers 3
    .parameter "resid"

    #@0
    .prologue
    .line 1011
    iget-object v0, p0, Landroid/view/Window;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@d
    .line 1012
    return-void
.end method

.method public setCallback(Landroid/view/Window$Callback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 553
    iput-object p1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@2
    .line 554
    return-void
.end method

.method public abstract setChildDrawable(ILandroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setChildInt(II)V
.end method

.method public setCloseOnTouchOutside(Z)V
    .registers 3
    .parameter "close"

    #@0
    .prologue
    .line 815
    iput-boolean p1, p0, Landroid/view/Window;->mCloseOnTouchOutside:Z

    #@2
    .line 816
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/view/Window;->mSetCloseOnTouchOutside:Z

    #@5
    .line 817
    return-void
.end method

.method public setCloseOnTouchOutsideIfNotSet(Z)V
    .registers 3
    .parameter "close"

    #@0
    .prologue
    .line 821
    iget-boolean v0, p0, Landroid/view/Window;->mSetCloseOnTouchOutside:Z

    #@2
    if-nez v0, :cond_9

    #@4
    .line 822
    iput-boolean p1, p0, Landroid/view/Window;->mCloseOnTouchOutside:Z

    #@6
    .line 823
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/view/Window;->mSetCloseOnTouchOutside:Z

    #@9
    .line 825
    :cond_9
    return-void
.end method

.method public setContainer(Landroid/view/Window;)V
    .registers 3
    .parameter "container"

    #@0
    .prologue
    .line 426
    iput-object p1, p0, Landroid/view/Window;->mContainer:Landroid/view/Window;

    #@2
    .line 427
    if-eqz p1, :cond_13

    #@4
    .line 429
    iget v0, p0, Landroid/view/Window;->mFeatures:I

    #@6
    or-int/lit8 v0, v0, 0x2

    #@8
    iput v0, p0, Landroid/view/Window;->mFeatures:I

    #@a
    .line 430
    iget v0, p0, Landroid/view/Window;->mLocalFeatures:I

    #@c
    or-int/lit8 v0, v0, 0x2

    #@e
    iput v0, p0, Landroid/view/Window;->mLocalFeatures:I

    #@10
    .line 431
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p1, Landroid/view/Window;->mHasChildren:Z

    #@13
    .line 433
    :cond_13
    return-void
.end method

.method public abstract setContentView(I)V
.end method

.method public abstract setContentView(Landroid/view/View;)V
.end method

.method public abstract setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method protected setDefaultWindowFormat(I)V
    .registers 4
    .parameter "format"

    #@0
    .prologue
    .line 1210
    iput p1, p0, Landroid/view/Window;->mDefaultWindowFormat:I

    #@2
    .line 1211
    iget-boolean v1, p0, Landroid/view/Window;->mHaveWindowFormat:Z

    #@4
    if-nez v1, :cond_15

    #@6
    .line 1212
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@9
    move-result-object v0

    #@a
    .line 1213
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@c
    .line 1214
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@e
    if-eqz v1, :cond_15

    #@10
    .line 1215
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@12
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@15
    .line 1218
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_15
    return-void
.end method

.method public setDimAmount(F)V
    .registers 4
    .parameter "amount"

    #@0
    .prologue
    .line 763
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 764
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@6
    .line 765
    const/4 v1, 0x1

    #@7
    iput-boolean v1, p0, Landroid/view/Window;->mHaveDimAmount:Z

    #@9
    .line 766
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@b
    if-eqz v1, :cond_12

    #@d
    .line 767
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@f
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@12
    .line 769
    :cond_12
    return-void
.end method

.method public abstract setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setFeatureDrawableAlpha(II)V
.end method

.method public abstract setFeatureDrawableResource(II)V
.end method

.method public abstract setFeatureDrawableUri(ILandroid/net/Uri;)V
.end method

.method public abstract setFeatureInt(II)V
.end method

.method public setFlags(II)V
    .registers 6
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 743
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 744
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@6
    xor-int/lit8 v2, p2, -0x1

    #@8
    and-int/2addr v1, v2

    #@9
    and-int v2, p1, p2

    #@b
    or-int/2addr v1, v2

    #@c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@e
    .line 745
    const/high16 v1, 0x800

    #@10
    and-int/2addr v1, p2

    #@11
    if-eqz v1, :cond_19

    #@13
    .line 746
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@15
    or-int/lit8 v1, v1, 0x8

    #@17
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@19
    .line 748
    :cond_19
    iget v1, p0, Landroid/view/Window;->mForcedWindowFlags:I

    #@1b
    or-int/2addr v1, p2

    #@1c
    iput v1, p0, Landroid/view/Window;->mForcedWindowFlags:I

    #@1e
    .line 749
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@20
    if-eqz v1, :cond_27

    #@22
    .line 750
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@24
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@27
    .line 752
    :cond_27
    return-void
.end method

.method public setFormat(I)V
    .registers 4
    .parameter "format"

    #@0
    .prologue
    .line 653
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 654
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_15

    #@6
    .line 655
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@8
    .line 656
    const/4 v1, 0x1

    #@9
    iput-boolean v1, p0, Landroid/view/Window;->mHaveWindowFormat:Z

    #@b
    .line 661
    :goto_b
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@d
    if-eqz v1, :cond_14

    #@f
    .line 662
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@11
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@14
    .line 664
    :cond_14
    return-void

    #@15
    .line 658
    :cond_15
    iget v1, p0, Landroid/view/Window;->mDefaultWindowFormat:I

    #@17
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@19
    .line 659
    const/4 v1, 0x0

    #@1a
    iput-boolean v1, p0, Landroid/view/Window;->mHaveWindowFormat:Z

    #@1c
    goto :goto_b
.end method

.method public setGravity(I)V
    .registers 4
    .parameter "gravity"

    #@0
    .prologue
    .line 620
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 621
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@6
    .line 622
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 623
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@c
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@f
    .line 625
    :cond_f
    return-void
.end method

.method public setLayout(II)V
    .registers 5
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 600
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 601
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@6
    .line 602
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@8
    .line 603
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 604
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@e
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@11
    .line 606
    :cond_11
    return-void
.end method

.method public setSoftInputMode(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 689
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 690
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_15

    #@6
    .line 691
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@8
    .line 692
    const/4 v1, 0x1

    #@9
    iput-boolean v1, p0, Landroid/view/Window;->mHasSoftInputMode:Z

    #@b
    .line 696
    :goto_b
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@d
    if-eqz v1, :cond_14

    #@f
    .line 697
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@11
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@14
    .line 699
    :cond_14
    return-void

    #@15
    .line 694
    :cond_15
    const/4 v1, 0x0

    #@16
    iput-boolean v1, p0, Landroid/view/Window;->mHasSoftInputMode:Z

    #@18
    goto :goto_b
.end method

.method public abstract setTitle(Ljava/lang/CharSequence;)V
.end method

.method public abstract setTitleColor(I)V
.end method

.method public setType(I)V
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 634
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 635
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@6
    .line 636
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 637
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@c
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@f
    .line 639
    :cond_f
    return-void
.end method

.method public setUiOptions(I)V
    .registers 2
    .parameter "uiOptions"

    #@0
    .prologue
    .line 1250
    return-void
.end method

.method public setUiOptions(II)V
    .registers 3
    .parameter "uiOptions"
    .parameter "mask"

    #@0
    .prologue
    .line 1258
    return-void
.end method

.method public abstract setVolumeControlStream(I)V
.end method

.method public setWindowAnimations(I)V
    .registers 4
    .parameter "resId"

    #@0
    .prologue
    .line 674
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 675
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@6
    .line 676
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 677
    iget-object v1, p0, Landroid/view/Window;->mCallback:Landroid/view/Window$Callback;

    #@c
    invoke-interface {v1, v0}, Landroid/view/Window$Callback;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    #@f
    .line 679
    :cond_f
    return-void
.end method

.method public setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 5
    .parameter "wm"
    .parameter "appToken"
    .parameter "appName"

    #@0
    .prologue
    .line 467
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;Z)V

    #@4
    .line 468
    return-void
.end method

.method public setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;Z)V
    .registers 7
    .parameter "wm"
    .parameter "appToken"
    .parameter "appName"
    .parameter "hardwareAccelerated"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 479
    iput-object p2, p0, Landroid/view/Window;->mAppToken:Landroid/os/IBinder;

    #@3
    .line 480
    iput-object p3, p0, Landroid/view/Window;->mAppName:Ljava/lang/String;

    #@5
    .line 481
    if-nez p4, :cond_10

    #@7
    const-string/jumbo v1, "persist.sys.ui.hw"

    #@a
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :cond_11
    iput-boolean v0, p0, Landroid/view/Window;->mHardwareAccelerated:Z

    #@13
    .line 483
    if-nez p1, :cond_20

    #@15
    .line 484
    iget-object v0, p0, Landroid/view/Window;->mContext:Landroid/content/Context;

    #@17
    const-string/jumbo v1, "window"

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1d
    move-result-object p1

    #@1e
    .end local p1
    check-cast p1, Landroid/view/WindowManager;

    #@20
    .line 486
    .restart local p1
    :cond_20
    check-cast p1, Landroid/view/WindowManagerImpl;

    #@22
    .end local p1
    invoke-virtual {p1, p0}, Landroid/view/WindowManagerImpl;->createLocalWindowManager(Landroid/view/Window;)Landroid/view/WindowManagerImpl;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Landroid/view/Window;->mWindowManager:Landroid/view/WindowManager;

    #@28
    .line 487
    return-void
.end method

.method public shouldCloseOnTouch(Landroid/content/Context;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "context"
    .parameter "event"

    #@0
    .prologue
    .line 832
    iget-boolean v0, p0, Landroid/view/Window;->mCloseOnTouchOutside:Z

    #@2
    if-eqz v0, :cond_18

    #@4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_18

    #@a
    invoke-direct {p0, p1, p2}, Landroid/view/Window;->isOutOfBounds(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_18

    #@10
    invoke-virtual {p0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    #@13
    move-result-object v0

    #@14
    if-eqz v0, :cond_18

    #@16
    .line 834
    const/4 v0, 0x1

    #@17
    .line 836
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method public abstract superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
.end method

.method public abstract superDispatchKeyEvent(Landroid/view/KeyEvent;)Z
.end method

.method public abstract superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
.end method

.method public abstract superDispatchTouchEvent(Landroid/view/MotionEvent;)Z
.end method

.method public abstract superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z
.end method

.method public abstract takeInputQueue(Landroid/view/InputQueue$Callback;)V
.end method

.method public abstract takeKeyEvents(Z)V
.end method

.method public abstract takeSurface(Landroid/view/SurfaceHolder$Callback2;)V
.end method

.method public abstract togglePanel(ILandroid/view/KeyEvent;)V
.end method
