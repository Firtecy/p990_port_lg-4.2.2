.class public Landroid/view/DragEvent;
.super Ljava/lang/Object;
.source "DragEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACTION_DRAG_ENDED:I = 0x4

.field public static final ACTION_DRAG_ENTERED:I = 0x5

.field public static final ACTION_DRAG_EXITED:I = 0x6

.field public static final ACTION_DRAG_LOCATION:I = 0x2

.field public static final ACTION_DRAG_STARTED:I = 0x1

.field public static final ACTION_DROP:I = 0x3

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/DragEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_RECYCLED:I = 0xa

.field private static final TRACK_RECYCLED_LOCATION:Z

.field private static final gRecyclerLock:Ljava/lang/Object;

.field private static gRecyclerTop:Landroid/view/DragEvent;

.field private static gRecyclerUsed:I


# instance fields
.field mAction:I

.field mClipData:Landroid/content/ClipData;

.field mClipDescription:Landroid/content/ClipDescription;

.field mDragResult:Z

.field mLocalState:Ljava/lang/Object;

.field private mNext:Landroid/view/DragEvent;

.field private mRecycled:Z

.field private mRecycledLocation:Ljava/lang/RuntimeException;

.field mX:F

.field mY:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 139
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/DragEvent;->gRecyclerLock:Ljava/lang/Object;

    #@7
    .line 140
    const/4 v0, 0x0

    #@8
    sput v0, Landroid/view/DragEvent;->gRecyclerUsed:I

    #@a
    .line 141
    const/4 v0, 0x0

    #@b
    sput-object v0, Landroid/view/DragEvent;->gRecyclerTop:Landroid/view/DragEvent;

    #@d
    .line 485
    new-instance v0, Landroid/view/DragEvent$1;

    #@f
    invoke-direct {v0}, Landroid/view/DragEvent$1;-><init>()V

    #@12
    sput-object v0, Landroid/view/DragEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 252
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 253
    return-void
.end method

.method private init(IFFLandroid/content/ClipDescription;Landroid/content/ClipData;Ljava/lang/Object;Z)V
    .registers 8
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "description"
    .parameter "data"
    .parameter "localState"
    .parameter "result"

    #@0
    .prologue
    .line 257
    iput p1, p0, Landroid/view/DragEvent;->mAction:I

    #@2
    .line 258
    iput p2, p0, Landroid/view/DragEvent;->mX:F

    #@4
    .line 259
    iput p3, p0, Landroid/view/DragEvent;->mY:F

    #@6
    .line 260
    iput-object p4, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@8
    .line 261
    iput-object p5, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@a
    .line 262
    iput-object p6, p0, Landroid/view/DragEvent;->mLocalState:Ljava/lang/Object;

    #@c
    .line 263
    iput-boolean p7, p0, Landroid/view/DragEvent;->mDragResult:Z

    #@e
    .line 264
    return-void
.end method

.method static obtain()Landroid/view/DragEvent;
    .registers 7

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    move v2, v1

    #@4
    move-object v4, v3

    #@5
    move-object v5, v3

    #@6
    move v6, v0

    #@7
    .line 267
    invoke-static/range {v0 .. v6}, Landroid/view/DragEvent;->obtain(IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static obtain(IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;
    .registers 17
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "localState"
    .parameter "description"
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 274
    sget-object v9, Landroid/view/DragEvent;->gRecyclerLock:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 275
    :try_start_3
    sget-object v1, Landroid/view/DragEvent;->gRecyclerTop:Landroid/view/DragEvent;

    #@5
    if-nez v1, :cond_1a

    #@7
    .line 276
    new-instance v0, Landroid/view/DragEvent;

    #@9
    invoke-direct {v0}, Landroid/view/DragEvent;-><init>()V

    #@c
    .local v0, ev:Landroid/view/DragEvent;
    move v1, p0

    #@d
    move v2, p1

    #@e
    move v3, p2

    #@f
    move-object v4, p4

    #@10
    move-object v5, p5

    #@11
    move-object v6, p3

    #@12
    move/from16 v7, p6

    #@14
    .line 277
    invoke-direct/range {v0 .. v7}, Landroid/view/DragEvent;->init(IFFLandroid/content/ClipDescription;Landroid/content/ClipData;Ljava/lang/Object;Z)V

    #@17
    .line 278
    monitor-exit v9

    #@18
    move-object v8, v0

    #@19
    .line 290
    .end local v0           #ev:Landroid/view/DragEvent;
    .local v8, ev:Ljava/lang/Object;
    :goto_19
    return-object v8

    #@1a
    .line 280
    .end local v8           #ev:Ljava/lang/Object;
    :cond_1a
    sget-object v0, Landroid/view/DragEvent;->gRecyclerTop:Landroid/view/DragEvent;

    #@1c
    .line 281
    .restart local v0       #ev:Landroid/view/DragEvent;
    iget-object v1, v0, Landroid/view/DragEvent;->mNext:Landroid/view/DragEvent;

    #@1e
    sput-object v1, Landroid/view/DragEvent;->gRecyclerTop:Landroid/view/DragEvent;

    #@20
    .line 282
    sget v1, Landroid/view/DragEvent;->gRecyclerUsed:I

    #@22
    add-int/lit8 v1, v1, -0x1

    #@24
    sput v1, Landroid/view/DragEvent;->gRecyclerUsed:I

    #@26
    .line 283
    monitor-exit v9
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_3d

    #@27
    .line 284
    const/4 v1, 0x0

    #@28
    iput-object v1, v0, Landroid/view/DragEvent;->mRecycledLocation:Ljava/lang/RuntimeException;

    #@2a
    .line 285
    const/4 v1, 0x0

    #@2b
    iput-boolean v1, v0, Landroid/view/DragEvent;->mRecycled:Z

    #@2d
    .line 286
    const/4 v1, 0x0

    #@2e
    iput-object v1, v0, Landroid/view/DragEvent;->mNext:Landroid/view/DragEvent;

    #@30
    move v1, p0

    #@31
    move v2, p1

    #@32
    move v3, p2

    #@33
    move-object v4, p4

    #@34
    move-object v5, p5

    #@35
    move-object v6, p3

    #@36
    move/from16 v7, p6

    #@38
    .line 288
    invoke-direct/range {v0 .. v7}, Landroid/view/DragEvent;->init(IFFLandroid/content/ClipDescription;Landroid/content/ClipData;Ljava/lang/Object;Z)V

    #@3b
    move-object v8, v0

    #@3c
    .line 290
    .restart local v8       #ev:Ljava/lang/Object;
    goto :goto_19

    #@3d
    .line 283
    .end local v0           #ev:Landroid/view/DragEvent;
    .end local v8           #ev:Ljava/lang/Object;
    :catchall_3d
    move-exception v1

    #@3e
    :try_start_3e
    monitor-exit v9
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v1
.end method

.method public static obtain(Landroid/view/DragEvent;)Landroid/view/DragEvent;
    .registers 8
    .parameter "source"

    #@0
    .prologue
    .line 295
    iget v0, p0, Landroid/view/DragEvent;->mAction:I

    #@2
    iget v1, p0, Landroid/view/DragEvent;->mX:F

    #@4
    iget v2, p0, Landroid/view/DragEvent;->mY:F

    #@6
    iget-object v3, p0, Landroid/view/DragEvent;->mLocalState:Ljava/lang/Object;

    #@8
    iget-object v4, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@a
    iget-object v5, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@c
    iget-boolean v6, p0, Landroid/view/DragEvent;->mDragResult:Z

    #@e
    invoke-static/range {v0 .. v6}, Landroid/view/DragEvent;->obtain(IFFLjava/lang/Object;Landroid/content/ClipDescription;Landroid/content/ClipData;Z)Landroid/view/DragEvent;

    #@11
    move-result-object v0

    #@12
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 455
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAction()I
    .registers 2

    #@0
    .prologue
    .line 313
    iget v0, p0, Landroid/view/DragEvent;->mAction:I

    #@2
    return v0
.end method

.method public getClipData()Landroid/content/ClipData;
    .registers 2

    #@0
    .prologue
    .line 343
    iget-object v0, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@2
    return-object v0
.end method

.method public getClipDescription()Landroid/content/ClipDescription;
    .registers 2

    #@0
    .prologue
    .line 357
    iget-object v0, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@2
    return-object v0
.end method

.method public getLocalState()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 371
    iget-object v0, p0, Landroid/view/DragEvent;->mLocalState:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getResult()Z
    .registers 2

    #@0
    .prologue
    .line 398
    iget-boolean v0, p0, Landroid/view/DragEvent;->mDragResult:Z

    #@2
    return v0
.end method

.method public getX()F
    .registers 2

    #@0
    .prologue
    .line 322
    iget v0, p0, Landroid/view/DragEvent;->mX:F

    #@2
    return v0
.end method

.method public getY()F
    .registers 2

    #@0
    .prologue
    .line 332
    iget v0, p0, Landroid/view/DragEvent;->mY:F

    #@2
    return v0
.end method

.method public final recycle()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 415
    iget-boolean v0, p0, Landroid/view/DragEvent;->mRecycled:Z

    #@3
    if-eqz v0, :cond_22

    #@5
    .line 416
    new-instance v0, Ljava/lang/RuntimeException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {p0}, Landroid/view/DragEvent;->toString()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, " recycled twice!"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 418
    :cond_22
    const/4 v0, 0x1

    #@23
    iput-boolean v0, p0, Landroid/view/DragEvent;->mRecycled:Z

    #@25
    .line 421
    iput-object v1, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@27
    .line 422
    iput-object v1, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@29
    .line 423
    iput-object v1, p0, Landroid/view/DragEvent;->mLocalState:Ljava/lang/Object;

    #@2b
    .line 425
    sget-object v1, Landroid/view/DragEvent;->gRecyclerLock:Ljava/lang/Object;

    #@2d
    monitor-enter v1

    #@2e
    .line 426
    :try_start_2e
    sget v0, Landroid/view/DragEvent;->gRecyclerUsed:I

    #@30
    const/16 v2, 0xa

    #@32
    if-ge v0, v2, :cond_40

    #@34
    .line 427
    sget v0, Landroid/view/DragEvent;->gRecyclerUsed:I

    #@36
    add-int/lit8 v0, v0, 0x1

    #@38
    sput v0, Landroid/view/DragEvent;->gRecyclerUsed:I

    #@3a
    .line 428
    sget-object v0, Landroid/view/DragEvent;->gRecyclerTop:Landroid/view/DragEvent;

    #@3c
    iput-object v0, p0, Landroid/view/DragEvent;->mNext:Landroid/view/DragEvent;

    #@3e
    .line 429
    sput-object p0, Landroid/view/DragEvent;->gRecyclerTop:Landroid/view/DragEvent;

    #@40
    .line 431
    :cond_40
    monitor-exit v1

    #@41
    .line 432
    return-void

    #@42
    .line 431
    :catchall_42
    move-exception v0

    #@43
    monitor-exit v1
    :try_end_44
    .catchall {:try_start_2e .. :try_end_44} :catchall_42

    #@44
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 441
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DragEvent{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " action="

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    iget v1, p0, Landroid/view/DragEvent;->mAction:I

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, " @ ("

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    iget v1, p0, Landroid/view/DragEvent;->mX:F

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, ", "

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    iget v1, p0, Landroid/view/DragEvent;->mY:F

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, ") desc="

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    iget-object v1, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, " data="

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget-object v1, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, " local="

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    iget-object v1, p0, Landroid/view/DragEvent;->mLocalState:Ljava/lang/Object;

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    const-string v1, " result="

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    iget-boolean v1, p0, Landroid/view/DragEvent;->mDragResult:Z

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    const-string/jumbo v1, "}"

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 464
    iget v0, p0, Landroid/view/DragEvent;->mAction:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 465
    iget v0, p0, Landroid/view/DragEvent;->mX:F

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@c
    .line 466
    iget v0, p0, Landroid/view/DragEvent;->mY:F

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@11
    .line 467
    iget-boolean v0, p0, Landroid/view/DragEvent;->mDragResult:Z

    #@13
    if-eqz v0, :cond_28

    #@15
    move v0, v1

    #@16
    :goto_16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 468
    iget-object v0, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@1b
    if-nez v0, :cond_2a

    #@1d
    .line 469
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 474
    :goto_20
    iget-object v0, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@22
    if-nez v0, :cond_33

    #@24
    .line 475
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 480
    :goto_27
    return-void

    #@28
    :cond_28
    move v0, v2

    #@29
    .line 467
    goto :goto_16

    #@2a
    .line 471
    :cond_2a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 472
    iget-object v0, p0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@2f
    invoke-virtual {v0, p1, p2}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@32
    goto :goto_20

    #@33
    .line 477
    :cond_33
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@36
    .line 478
    iget-object v0, p0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@38
    invoke-virtual {v0, p1, p2}, Landroid/content/ClipDescription;->writeToParcel(Landroid/os/Parcel;I)V

    #@3b
    goto :goto_27
.end method
