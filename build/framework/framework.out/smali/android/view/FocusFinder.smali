.class public Landroid/view/FocusFinder;
.super Ljava/lang/Object;
.source "FocusFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/FocusFinder$SequentialFocusComparator;
    }
.end annotation


# static fields
.field private static final tlFocusFinder:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/view/FocusFinder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mBestCandidateRect:Landroid/graphics/Rect;

.field final mFocusedRect:Landroid/graphics/Rect;

.field final mOtherRect:Landroid/graphics/Rect;

.field final mSequentialFocusComparator:Landroid/view/FocusFinder$SequentialFocusComparator;

.field private final mTempList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    new-instance v0, Landroid/view/FocusFinder$1;

    #@2
    invoke-direct {v0}, Landroid/view/FocusFinder$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/FocusFinder;->tlFocusFinder:Ljava/lang/ThreadLocal;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    #@a
    .line 47
    new-instance v0, Landroid/graphics/Rect;

    #@c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v0, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    #@11
    .line 48
    new-instance v0, Landroid/graphics/Rect;

    #@13
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@16
    iput-object v0, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@18
    .line 49
    new-instance v0, Landroid/view/FocusFinder$SequentialFocusComparator;

    #@1a
    const/4 v1, 0x0

    #@1b
    invoke-direct {v0, v1}, Landroid/view/FocusFinder$SequentialFocusComparator;-><init>(Landroid/view/FocusFinder$1;)V

    #@1e
    iput-object v0, p0, Landroid/view/FocusFinder;->mSequentialFocusComparator:Landroid/view/FocusFinder$SequentialFocusComparator;

    #@20
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    #@22
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@25
    iput-object v0, p0, Landroid/view/FocusFinder;->mTempList:Ljava/util/ArrayList;

    #@27
    .line 54
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/FocusFinder$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-direct {p0}, Landroid/view/FocusFinder;-><init>()V

    #@3
    return-void
.end method

.method private findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;
    .registers 13
    .parameter "root"
    .parameter "focused"
    .parameter "focusedRect"
    .parameter "direction"

    #@0
    .prologue
    .line 82
    const/4 v6, 0x0

    #@1
    .line 83
    .local v6, next:Landroid/view/View;
    if-eqz p2, :cond_7

    #@3
    .line 84
    invoke-direct {p0, p1, p2, p4}, Landroid/view/FocusFinder;->findNextUserSpecifiedFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    #@6
    move-result-object v6

    #@7
    .line 86
    :cond_7
    if-eqz v6, :cond_b

    #@9
    move-object v7, v6

    #@a
    .line 99
    .end local v6           #next:Landroid/view/View;
    .local v7, next:Landroid/view/View;
    :goto_a
    return-object v7

    #@b
    .line 89
    .end local v7           #next:Landroid/view/View;
    .restart local v6       #next:Landroid/view/View;
    :cond_b
    iget-object v5, p0, Landroid/view/FocusFinder;->mTempList:Ljava/util/ArrayList;

    #@d
    .line 91
    .local v5, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :try_start_d
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@10
    .line 92
    invoke-virtual {p1, v5, p4}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;I)V

    #@13
    .line 93
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_22

    #@19
    move-object v0, p0

    #@1a
    move-object v1, p1

    #@1b
    move-object v2, p2

    #@1c
    move-object v3, p3

    #@1d
    move v4, p4

    #@1e
    .line 94
    invoke-direct/range {v0 .. v5}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;ILjava/util/ArrayList;)Landroid/view/View;
    :try_end_21
    .catchall {:try_start_d .. :try_end_21} :catchall_27

    #@21
    move-result-object v6

    #@22
    .line 97
    :cond_22
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@25
    move-object v7, v6

    #@26
    .line 99
    .end local v6           #next:Landroid/view/View;
    .restart local v7       #next:Landroid/view/View;
    goto :goto_a

    #@27
    .line 97
    .end local v7           #next:Landroid/view/View;
    .restart local v6       #next:Landroid/view/View;
    :catchall_27
    move-exception v0

    #@28
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@2b
    throw v0
.end method

.method private findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;ILjava/util/ArrayList;)Landroid/view/View;
    .registers 12
    .parameter "root"
    .parameter "focused"
    .parameter "focusedRect"
    .parameter "direction"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 115
    .local p5, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz p2, :cond_28

    #@2
    .line 116
    if-nez p3, :cond_6

    #@4
    .line 117
    iget-object p3, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    #@6
    .line 120
    :cond_6
    invoke-virtual {p2, p3}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@9
    .line 121
    invoke-virtual {p1, p2, p3}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@c
    .line 160
    :cond_c
    :goto_c
    sparse-switch p4, :sswitch_data_56

    #@f
    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "Unknown direction: "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 123
    :cond_28
    if-nez p3, :cond_c

    #@2a
    .line 124
    iget-object p3, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    #@2c
    .line 126
    sparse-switch p4, :sswitch_data_70

    #@2f
    goto :goto_c

    #@30
    .line 152
    :sswitch_30
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    #@33
    goto :goto_c

    #@34
    .line 129
    :sswitch_34
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    #@37
    goto :goto_c

    #@38
    .line 137
    :sswitch_38
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    #@3b
    goto :goto_c

    #@3c
    .line 144
    :sswitch_3c
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    #@3f
    goto :goto_c

    #@40
    :sswitch_40
    move-object v0, p0

    #@41
    move-object v1, p5

    #@42
    move-object v2, p1

    #@43
    move-object v3, p2

    #@44
    move-object v4, p3

    #@45
    move v5, p4

    #@46
    .line 163
    invoke-direct/range {v0 .. v5}, Landroid/view/FocusFinder;->findNextFocusInRelativeDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    #@49
    move-result-object v0

    #@4a
    .line 169
    :goto_4a
    return-object v0

    #@4b
    :sswitch_4b
    move-object v0, p0

    #@4c
    move-object v1, p5

    #@4d
    move-object v2, p1

    #@4e
    move-object v3, p2

    #@4f
    move-object v4, p3

    #@50
    move v5, p4

    #@51
    invoke-virtual/range {v0 .. v5}, Landroid/view/FocusFinder;->findNextFocusInAbsoluteDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    #@54
    move-result-object v0

    #@55
    goto :goto_4a

    #@56
    .line 160
    :sswitch_data_56
    .sparse-switch
        0x1 -> :sswitch_40
        0x2 -> :sswitch_40
        0x11 -> :sswitch_4b
        0x21 -> :sswitch_4b
        0x42 -> :sswitch_4b
        0x82 -> :sswitch_4b
    .end sparse-switch

    #@70
    .line 126
    :sswitch_data_70
    .sparse-switch
        0x1 -> :sswitch_30
        0x2 -> :sswitch_38
        0x11 -> :sswitch_3c
        0x21 -> :sswitch_3c
        0x42 -> :sswitch_34
        0x82 -> :sswitch_34
    .end sparse-switch
.end method

.method private findNextFocusInRelativeDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;
    .registers 9
    .parameter
    .parameter "root"
    .parameter "focused"
    .parameter "focusedRect"
    .parameter "direction"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            "I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 180
    .local p1, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :try_start_0
    iget-object v1, p0, Landroid/view/FocusFinder;->mSequentialFocusComparator:Landroid/view/FocusFinder$SequentialFocusComparator;

    #@2
    invoke-virtual {v1, p2}, Landroid/view/FocusFinder$SequentialFocusComparator;->setRoot(Landroid/view/ViewGroup;)V

    #@5
    .line 181
    iget-object v1, p0, Landroid/view/FocusFinder;->mSequentialFocusComparator:Landroid/view/FocusFinder$SequentialFocusComparator;

    #@7
    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_a
    .catchall {:try_start_0 .. :try_end_a} :catchall_1f

    #@a
    .line 183
    iget-object v1, p0, Landroid/view/FocusFinder;->mSequentialFocusComparator:Landroid/view/FocusFinder$SequentialFocusComparator;

    #@c
    invoke-virtual {v1}, Landroid/view/FocusFinder$SequentialFocusComparator;->recycle()V

    #@f
    .line 186
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v0

    #@13
    .line 187
    .local v0, count:I
    packed-switch p5, :pswitch_data_30

    #@16
    .line 193
    add-int/lit8 v1, v0, -0x1

    #@18
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/view/View;

    #@1e
    :goto_1e
    return-object v1

    #@1f
    .line 183
    .end local v0           #count:I
    :catchall_1f
    move-exception v1

    #@20
    iget-object v2, p0, Landroid/view/FocusFinder;->mSequentialFocusComparator:Landroid/view/FocusFinder$SequentialFocusComparator;

    #@22
    invoke-virtual {v2}, Landroid/view/FocusFinder$SequentialFocusComparator;->recycle()V

    #@25
    throw v1

    #@26
    .line 189
    .restart local v0       #count:I
    :pswitch_26
    invoke-static {p2, p3, p1, v0}, Landroid/view/FocusFinder;->getForwardFocusable(Landroid/view/ViewGroup;Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;

    #@29
    move-result-object v1

    #@2a
    goto :goto_1e

    #@2b
    .line 191
    :pswitch_2b
    invoke-static {p2, p3, p1, v0}, Landroid/view/FocusFinder;->getBackwardFocusable(Landroid/view/ViewGroup;Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;

    #@2e
    move-result-object v1

    #@2f
    goto :goto_1e

    #@30
    .line 187
    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_26
    .end packed-switch
.end method

.method private findNextUserSpecifiedFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .registers 6
    .parameter "root"
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 104
    invoke-virtual {p2, p1, p3}, Landroid/view/View;->findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 105
    .local v0, userSetNextFocus:Landroid/view/View;
    if-eqz v0, :cond_19

    #@6
    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_19

    #@c
    invoke-virtual {v0}, Landroid/view/View;->isInTouchMode()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_18

    #@12
    invoke-virtual {v0}, Landroid/view/View;->isFocusableInTouchMode()Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_19

    #@18
    .line 110
    .end local v0           #userSetNextFocus:Landroid/view/View;
    :cond_18
    :goto_18
    return-object v0

    #@19
    .restart local v0       #userSetNextFocus:Landroid/view/View;
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_18
.end method

.method private static getBackwardFocusable(Landroid/view/ViewGroup;Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;
    .registers 5
    .parameter "root"
    .parameter "focused"
    .parameter
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 275
    .local p2, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->getPreviousFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static getForwardFocusable(Landroid/view/ViewGroup;Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;
    .registers 5
    .parameter "root"
    .parameter "focused"
    .parameter
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 252
    .local p2, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->getNextFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getInstance()Landroid/view/FocusFinder;
    .registers 1

    #@0
    .prologue
    .line 43
    sget-object v0, Landroid/view/FocusFinder;->tlFocusFinder:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/FocusFinder;

    #@8
    return-object v0
.end method

.method private static getNextFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;
    .registers 5
    .parameter "focused"
    .parameter
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 259
    .local p1, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz p0, :cond_15

    #@2
    .line 260
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->lastIndexOf(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    .line 261
    .local v0, position:I
    if-ltz v0, :cond_15

    #@8
    add-int/lit8 v1, v0, 0x1

    #@a
    if-ge v1, p2, :cond_15

    #@c
    .line 262
    add-int/lit8 v1, v0, 0x1

    #@e
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/view/View;

    #@14
    .line 268
    .end local v0           #position:I
    :goto_14
    return-object v1

    #@15
    .line 265
    :cond_15
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    #@18
    move-result v1

    #@19
    if-nez v1, :cond_23

    #@1b
    .line 266
    const/4 v1, 0x0

    #@1c
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/view/View;

    #@22
    goto :goto_14

    #@23
    .line 268
    :cond_23
    const/4 v1, 0x0

    #@24
    goto :goto_14
.end method

.method private static getPreviousFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;
    .registers 5
    .parameter "focused"
    .parameter
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .line 282
    .local p1, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz p0, :cond_11

    #@2
    .line 283
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    .line 284
    .local v0, position:I
    if-lez v0, :cond_11

    #@8
    .line 285
    add-int/lit8 v1, v0, -0x1

    #@a
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/view/View;

    #@10
    .line 291
    .end local v0           #position:I
    :goto_10
    return-object v1

    #@11
    .line 288
    :cond_11
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_20

    #@17
    .line 289
    add-int/lit8 v1, p2, -0x1

    #@19
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/view/View;

    #@1f
    goto :goto_10

    #@20
    .line 291
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_10
.end method

.method private isTouchCandidate(IILandroid/graphics/Rect;I)Z
    .registers 8
    .parameter "x"
    .parameter "y"
    .parameter "destRect"
    .parameter "direction"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 612
    sparse-switch p4, :sswitch_data_46

    #@5
    .line 622
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 614
    :sswitch_d
    iget v2, p3, Landroid/graphics/Rect;->left:I

    #@f
    if-gt v2, p1, :cond_1a

    #@11
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@13
    if-gt v2, p2, :cond_1a

    #@15
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@17
    if-gt p2, v2, :cond_1a

    #@19
    .line 620
    :cond_19
    :goto_19
    return v0

    #@1a
    :cond_1a
    move v0, v1

    #@1b
    .line 614
    goto :goto_19

    #@1c
    .line 616
    :sswitch_1c
    iget v2, p3, Landroid/graphics/Rect;->left:I

    #@1e
    if-lt v2, p1, :cond_28

    #@20
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@22
    if-gt v2, p2, :cond_28

    #@24
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@26
    if-le p2, v2, :cond_19

    #@28
    :cond_28
    move v0, v1

    #@29
    goto :goto_19

    #@2a
    .line 618
    :sswitch_2a
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@2c
    if-gt v2, p2, :cond_36

    #@2e
    iget v2, p3, Landroid/graphics/Rect;->left:I

    #@30
    if-gt v2, p1, :cond_36

    #@32
    iget v2, p3, Landroid/graphics/Rect;->right:I

    #@34
    if-le p1, v2, :cond_19

    #@36
    :cond_36
    move v0, v1

    #@37
    goto :goto_19

    #@38
    .line 620
    :sswitch_38
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@3a
    if-lt v2, p2, :cond_44

    #@3c
    iget v2, p3, Landroid/graphics/Rect;->left:I

    #@3e
    if-gt v2, p1, :cond_44

    #@40
    iget v2, p3, Landroid/graphics/Rect;->right:I

    #@42
    if-le p1, v2, :cond_19

    #@44
    :cond_44
    move v0, v1

    #@45
    goto :goto_19

    #@46
    .line 612
    :sswitch_data_46
    .sparse-switch
        0x11 -> :sswitch_d
        0x21 -> :sswitch_2a
        0x42 -> :sswitch_1c
        0x82 -> :sswitch_38
    .end sparse-switch
.end method

.method static majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .registers 5
    .parameter "direction"
    .parameter "source"
    .parameter "dest"

    #@0
    .prologue
    .line 457
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2}, Landroid/view/FocusFinder;->majorAxisDistanceRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@4
    move-result v1

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method static majorAxisDistanceRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .registers 5
    .parameter "direction"
    .parameter "source"
    .parameter "dest"

    #@0
    .prologue
    .line 461
    sparse-switch p0, :sswitch_data_24

    #@3
    .line 471
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 463
    :sswitch_b
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@d
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@f
    sub-int/2addr v0, v1

    #@10
    .line 469
    :goto_10
    return v0

    #@11
    .line 465
    :sswitch_11
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@13
    iget v1, p1, Landroid/graphics/Rect;->right:I

    #@15
    sub-int/2addr v0, v1

    #@16
    goto :goto_10

    #@17
    .line 467
    :sswitch_17
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@19
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@1b
    sub-int/2addr v0, v1

    #@1c
    goto :goto_10

    #@1d
    .line 469
    :sswitch_1d
    iget v0, p2, Landroid/graphics/Rect;->top:I

    #@1f
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    #@21
    sub-int/2addr v0, v1

    #@22
    goto :goto_10

    #@23
    .line 461
    nop

    #@24
    :sswitch_data_24
    .sparse-switch
        0x11 -> :sswitch_b
        0x21 -> :sswitch_17
        0x42 -> :sswitch_11
        0x82 -> :sswitch_1d
    .end sparse-switch
.end method

.method static majorAxisDistanceToFarEdge(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .registers 5
    .parameter "direction"
    .parameter "source"
    .parameter "dest"

    #@0
    .prologue
    .line 482
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, p2}, Landroid/view/FocusFinder;->majorAxisDistanceToFarEdgeRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@4
    move-result v1

    #@5
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method static majorAxisDistanceToFarEdgeRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .registers 5
    .parameter "direction"
    .parameter "source"
    .parameter "dest"

    #@0
    .prologue
    .line 486
    sparse-switch p0, :sswitch_data_24

    #@3
    .line 496
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 488
    :sswitch_b
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@d
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@f
    sub-int/2addr v0, v1

    #@10
    .line 494
    :goto_10
    return v0

    #@11
    .line 490
    :sswitch_11
    iget v0, p2, Landroid/graphics/Rect;->right:I

    #@13
    iget v1, p1, Landroid/graphics/Rect;->right:I

    #@15
    sub-int/2addr v0, v1

    #@16
    goto :goto_10

    #@17
    .line 492
    :sswitch_17
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@19
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@1b
    sub-int/2addr v0, v1

    #@1c
    goto :goto_10

    #@1d
    .line 494
    :sswitch_1d
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    #@1f
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    #@21
    sub-int/2addr v0, v1

    #@22
    goto :goto_10

    #@23
    .line 486
    nop

    #@24
    :sswitch_data_24
    .sparse-switch
        0x11 -> :sswitch_b
        0x21 -> :sswitch_17
        0x42 -> :sswitch_11
        0x82 -> :sswitch_1d
    .end sparse-switch
.end method

.method static minorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .registers 6
    .parameter "direction"
    .parameter "source"
    .parameter "dest"

    #@0
    .prologue
    .line 509
    sparse-switch p0, :sswitch_data_3c

    #@3
    .line 523
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 513
    :sswitch_b
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@d
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@10
    move-result v1

    #@11
    div-int/lit8 v1, v1, 0x2

    #@13
    add-int/2addr v0, v1

    #@14
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@16
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    #@19
    move-result v2

    #@1a
    div-int/lit8 v2, v2, 0x2

    #@1c
    add-int/2addr v1, v2

    #@1d
    sub-int/2addr v0, v1

    #@1e
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@21
    move-result v0

    #@22
    .line 519
    :goto_22
    return v0

    #@23
    :sswitch_23
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@25
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@28
    move-result v1

    #@29
    div-int/lit8 v1, v1, 0x2

    #@2b
    add-int/2addr v0, v1

    #@2c
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@2e
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    #@31
    move-result v2

    #@32
    div-int/lit8 v2, v2, 0x2

    #@34
    add-int/2addr v1, v2

    #@35
    sub-int/2addr v0, v1

    #@36
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    #@39
    move-result v0

    #@3a
    goto :goto_22

    #@3b
    .line 509
    nop

    #@3c
    :sswitch_data_3c
    .sparse-switch
        0x11 -> :sswitch_b
        0x21 -> :sswitch_23
        0x42 -> :sswitch_b
        0x82 -> :sswitch_23
    .end sparse-switch
.end method

.method private setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .registers 7
    .parameter "root"
    .parameter "focusedRect"

    #@0
    .prologue
    .line 197
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollY()I

    #@3
    move-result v2

    #@4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    #@7
    move-result v3

    #@8
    add-int v0, v2, v3

    #@a
    .line 198
    .local v0, rootBottom:I
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollX()I

    #@d
    move-result v2

    #@e
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    #@11
    move-result v3

    #@12
    add-int v1, v2, v3

    #@14
    .line 199
    .local v1, rootRight:I
    invoke-virtual {p2, v1, v0, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@17
    .line 200
    return-void
.end method

.method private setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .registers 5
    .parameter "root"
    .parameter "focusedRect"

    #@0
    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollY()I

    #@3
    move-result v1

    #@4
    .line 204
    .local v1, rootTop:I
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollX()I

    #@7
    move-result v0

    #@8
    .line 205
    .local v0, rootLeft:I
    invoke-virtual {p2, v0, v1, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    #@b
    .line 206
    return-void
.end method


# virtual methods
.method beamBeats(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 11
    .parameter "direction"
    .parameter "source"
    .parameter "rect1"
    .parameter "rect2"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 344
    invoke-virtual {p0, p1, p2, p3}, Landroid/view/FocusFinder;->beamsOverlap(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    .line 345
    .local v0, rect1InSrcBeam:Z
    invoke-virtual {p0, p1, p2, p4}, Landroid/view/FocusFinder;->beamsOverlap(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@9
    move-result v1

    #@a
    .line 348
    .local v1, rect2InSrcBeam:Z
    if-nez v1, :cond_e

    #@c
    if-nez v0, :cond_10

    #@e
    :cond_e
    move v2, v3

    #@f
    .line 371
    :cond_f
    :goto_f
    return v2

    #@10
    .line 358
    :cond_10
    invoke-virtual {p0, p1, p2, p4}, Landroid/view/FocusFinder;->isToDirectionOf(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_f

    #@16
    .line 363
    const/16 v4, 0x11

    #@18
    if-eq p1, v4, :cond_f

    #@1a
    const/16 v4, 0x42

    #@1c
    if-eq p1, v4, :cond_f

    #@1e
    .line 371
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@21
    move-result v4

    #@22
    invoke-static {p1, p2, p4}, Landroid/view/FocusFinder;->majorAxisDistanceToFarEdge(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@25
    move-result v5

    #@26
    if-lt v4, v5, :cond_f

    #@28
    move v2, v3

    #@29
    goto :goto_f
.end method

.method beamsOverlap(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 8
    .parameter "direction"
    .parameter "rect1"
    .parameter "rect2"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 421
    sparse-switch p1, :sswitch_data_2a

    #@5
    .line 429
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 424
    :sswitch_d
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    #@f
    iget v3, p2, Landroid/graphics/Rect;->top:I

    #@11
    if-lt v2, v3, :cond_1a

    #@13
    iget v2, p3, Landroid/graphics/Rect;->top:I

    #@15
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    #@17
    if-gt v2, v3, :cond_1a

    #@19
    .line 427
    :cond_19
    :goto_19
    return v0

    #@1a
    :cond_1a
    move v0, v1

    #@1b
    .line 424
    goto :goto_19

    #@1c
    .line 427
    :sswitch_1c
    iget v2, p3, Landroid/graphics/Rect;->right:I

    #@1e
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@20
    if-lt v2, v3, :cond_28

    #@22
    iget v2, p3, Landroid/graphics/Rect;->left:I

    #@24
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@26
    if-le v2, v3, :cond_19

    #@28
    :cond_28
    move v0, v1

    #@29
    goto :goto_19

    #@2a
    .line 421
    :sswitch_data_2a
    .sparse-switch
        0x11 -> :sswitch_d
        0x21 -> :sswitch_1c
        0x42 -> :sswitch_d
        0x82 -> :sswitch_1c
    .end sparse-switch
.end method

.method public findNearestTouchable(Landroid/view/ViewGroup;III[I)Landroid/view/View;
    .registers 21
    .parameter "root"
    .parameter "x"
    .parameter "y"
    .parameter "direction"
    .parameter "deltas"

    #@0
    .prologue
    .line 539
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getTouchables()Ljava/util/ArrayList;

    #@3
    move-result-object v12

    #@4
    .line 540
    .local v12, touchables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const v8, 0x7fffffff

    #@7
    .line 541
    .local v8, minDistance:I
    const/4 v3, 0x0

    #@8
    .line 543
    .local v3, closest:Landroid/view/View;
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v9

    #@c
    .line 545
    .local v9, numTouchables:I
    move-object/from16 v0, p1

    #@e
    iget-object v13, v0, Landroid/view/View;->mContext:Landroid/content/Context;

    #@10
    invoke-static {v13}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@13
    move-result-object v13

    #@14
    invoke-virtual {v13}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    #@17
    move-result v6

    #@18
    .line 547
    .local v6, edgeSlop:I
    new-instance v4, Landroid/graphics/Rect;

    #@1a
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@1d
    .line 548
    .local v4, closestBounds:Landroid/graphics/Rect;
    iget-object v11, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    #@1f
    .line 550
    .local v11, touchableBounds:Landroid/graphics/Rect;
    const/4 v7, 0x0

    #@20
    .local v7, i:I
    :goto_20
    if-ge v7, v9, :cond_88

    #@22
    .line 551
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v10

    #@26
    check-cast v10, Landroid/view/View;

    #@28
    .line 554
    .local v10, touchable:Landroid/view/View;
    invoke-virtual {v10, v11}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@2b
    .line 556
    const/4 v13, 0x1

    #@2c
    const/4 v14, 0x1

    #@2d
    move-object/from16 v0, p1

    #@2f
    invoke-virtual {v0, v10, v11, v13, v14}, Landroid/view/ViewGroup;->offsetRectBetweenParentAndChild(Landroid/view/View;Landroid/graphics/Rect;ZZ)V

    #@32
    .line 558
    move/from16 v0, p2

    #@34
    move/from16 v1, p3

    #@36
    move/from16 v2, p4

    #@38
    invoke-direct {p0, v0, v1, v11, v2}, Landroid/view/FocusFinder;->isTouchCandidate(IILandroid/graphics/Rect;I)Z

    #@3b
    move-result v13

    #@3c
    if-nez v13, :cond_41

    #@3e
    .line 550
    :cond_3e
    :goto_3e
    add-int/lit8 v7, v7, 0x1

    #@40
    goto :goto_20

    #@41
    .line 562
    :cond_41
    const v5, 0x7fffffff

    #@44
    .line 564
    .local v5, distance:I
    sparse-switch p4, :sswitch_data_8a

    #@47
    .line 579
    :goto_47
    if-ge v5, v6, :cond_3e

    #@49
    .line 581
    if-eqz v3, :cond_59

    #@4b
    invoke-virtual {v4, v11}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@4e
    move-result v13

    #@4f
    if-nez v13, :cond_59

    #@51
    invoke-virtual {v11, v4}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@54
    move-result v13

    #@55
    if-nez v13, :cond_3e

    #@57
    if-ge v5, v8, :cond_3e

    #@59
    .line 584
    :cond_59
    move v8, v5

    #@5a
    .line 585
    move-object v3, v10

    #@5b
    .line 586
    invoke-virtual {v4, v11}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5e
    .line 587
    sparse-switch p4, :sswitch_data_9c

    #@61
    goto :goto_3e

    #@62
    .line 589
    :sswitch_62
    const/4 v13, 0x0

    #@63
    neg-int v14, v5

    #@64
    aput v14, p5, v13

    #@66
    goto :goto_3e

    #@67
    .line 566
    :sswitch_67
    iget v13, v11, Landroid/graphics/Rect;->right:I

    #@69
    sub-int v13, p2, v13

    #@6b
    add-int/lit8 v5, v13, 0x1

    #@6d
    .line 567
    goto :goto_47

    #@6e
    .line 569
    :sswitch_6e
    iget v5, v11, Landroid/graphics/Rect;->left:I

    #@70
    .line 570
    goto :goto_47

    #@71
    .line 572
    :sswitch_71
    iget v13, v11, Landroid/graphics/Rect;->bottom:I

    #@73
    sub-int v13, p3, v13

    #@75
    add-int/lit8 v5, v13, 0x1

    #@77
    .line 573
    goto :goto_47

    #@78
    .line 575
    :sswitch_78
    iget v5, v11, Landroid/graphics/Rect;->top:I

    #@7a
    goto :goto_47

    #@7b
    .line 592
    :sswitch_7b
    const/4 v13, 0x0

    #@7c
    aput v5, p5, v13

    #@7e
    goto :goto_3e

    #@7f
    .line 595
    :sswitch_7f
    const/4 v13, 0x1

    #@80
    neg-int v14, v5

    #@81
    aput v14, p5, v13

    #@83
    goto :goto_3e

    #@84
    .line 598
    :sswitch_84
    const/4 v13, 0x1

    #@85
    aput v5, p5, v13

    #@87
    goto :goto_3e

    #@88
    .line 604
    .end local v5           #distance:I
    .end local v10           #touchable:Landroid/view/View;
    :cond_88
    return-object v3

    #@89
    .line 564
    nop

    #@8a
    :sswitch_data_8a
    .sparse-switch
        0x11 -> :sswitch_67
        0x21 -> :sswitch_71
        0x42 -> :sswitch_6e
        0x82 -> :sswitch_78
    .end sparse-switch

    #@9c
    .line 587
    :sswitch_data_9c
    .sparse-switch
        0x11 -> :sswitch_62
        0x21 -> :sswitch_7f
        0x42 -> :sswitch_7b
        0x82 -> :sswitch_84
    .end sparse-switch
.end method

.method public final findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .registers 5
    .parameter "root"
    .parameter "focused"
    .parameter "direction"

    #@0
    .prologue
    .line 65
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0, p3}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;
    .registers 6
    .parameter "root"
    .parameter "focusedRect"
    .parameter "direction"

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    #@2
    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5
    .line 78
    const/4 v0, 0x0

    #@6
    iget-object v1, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    #@8
    invoke-direct {p0, p1, v0, v1, p3}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method findNextFocusInAbsoluteDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;
    .registers 13
    .parameter
    .parameter "root"
    .parameter "focused"
    .parameter "focusedRect"
    .parameter "direction"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            "I)",
            "Landroid/view/View;"
        }
    .end annotation

    #@0
    .prologue
    .local p1, focusables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v6, 0x0

    #@1
    .line 212
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@3
    invoke-virtual {v4, p4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@6
    .line 213
    sparse-switch p5, :sswitch_data_6e

    #@9
    .line 227
    :goto_9
    const/4 v0, 0x0

    #@a
    .line 229
    .local v0, closest:Landroid/view/View;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v3

    #@e
    .line 230
    .local v3, numFocusables:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v3, :cond_6d

    #@11
    .line 231
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/view/View;

    #@17
    .line 234
    .local v1, focusable:Landroid/view/View;
    if-eq v1, p3, :cond_1b

    #@19
    if-ne v1, p2, :cond_50

    #@1b
    .line 230
    :cond_1b
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_f

    #@1e
    .line 215
    .end local v0           #closest:Landroid/view/View;
    .end local v1           #focusable:Landroid/view/View;
    .end local v2           #i:I
    .end local v3           #numFocusables:I
    :sswitch_1e
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@20
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    #@23
    move-result v5

    #@24
    add-int/lit8 v5, v5, 0x1

    #@26
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    #@29
    goto :goto_9

    #@2a
    .line 218
    :sswitch_2a
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@2c
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    #@2f
    move-result v5

    #@30
    add-int/lit8 v5, v5, 0x1

    #@32
    neg-int v5, v5

    #@33
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    #@36
    goto :goto_9

    #@37
    .line 221
    :sswitch_37
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@39
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    #@3c
    move-result v5

    #@3d
    add-int/lit8 v5, v5, 0x1

    #@3f
    invoke-virtual {v4, v6, v5}, Landroid/graphics/Rect;->offset(II)V

    #@42
    goto :goto_9

    #@43
    .line 224
    :sswitch_43
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@45
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    #@48
    move-result v5

    #@49
    add-int/lit8 v5, v5, 0x1

    #@4b
    neg-int v5, v5

    #@4c
    invoke-virtual {v4, v6, v5}, Landroid/graphics/Rect;->offset(II)V

    #@4f
    goto :goto_9

    #@50
    .line 237
    .restart local v0       #closest:Landroid/view/View;
    .restart local v1       #focusable:Landroid/view/View;
    .restart local v2       #i:I
    .restart local v3       #numFocusables:I
    :cond_50
    iget-object v4, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    #@52
    invoke-virtual {v1, v4}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    #@55
    .line 238
    iget-object v4, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    #@57
    invoke-virtual {p2, v1, v4}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@5a
    .line 240
    iget-object v4, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    #@5c
    iget-object v5, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@5e
    invoke-virtual {p0, p5, p4, v4, v5}, Landroid/view/FocusFinder;->isBetterCandidate(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@61
    move-result v4

    #@62
    if-eqz v4, :cond_1b

    #@64
    .line 241
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    #@66
    iget-object v5, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    #@68
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@6b
    .line 242
    move-object v0, v1

    #@6c
    goto :goto_1b

    #@6d
    .line 245
    .end local v1           #focusable:Landroid/view/View;
    :cond_6d
    return-object v0

    #@6e
    .line 213
    :sswitch_data_6e
    .sparse-switch
        0x11 -> :sswitch_1e
        0x21 -> :sswitch_37
        0x42 -> :sswitch_2a
        0x82 -> :sswitch_43
    .end sparse-switch
.end method

.method getWeightedDistanceFor(II)I
    .registers 5
    .parameter "majorAxisDistance"
    .parameter "minorAxisDistance"

    #@0
    .prologue
    .line 381
    mul-int/lit8 v0, p1, 0xd

    #@2
    mul-int/2addr v0, p1

    #@3
    mul-int v1, p2, p2

    #@5
    add-int/2addr v0, v1

    #@6
    return v0
.end method

.method isBetterCandidate(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 10
    .parameter "direction"
    .parameter "source"
    .parameter "rect1"
    .parameter "rect2"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 308
    invoke-virtual {p0, p2, p3, p1}, Landroid/view/FocusFinder;->isCandidate(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_a

    #@8
    move v0, v1

    #@9
    .line 329
    :cond_9
    :goto_9
    return v0

    #@a
    .line 314
    :cond_a
    invoke-virtual {p0, p2, p4, p1}, Landroid/view/FocusFinder;->isCandidate(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_9

    #@10
    .line 319
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/FocusFinder;->beamBeats(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_9

    #@16
    .line 324
    invoke-virtual {p0, p1, p2, p4, p3}, Landroid/view/FocusFinder;->beamBeats(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_1e

    #@1c
    move v0, v1

    #@1d
    .line 325
    goto :goto_9

    #@1e
    .line 329
    :cond_1e
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@21
    move-result v2

    #@22
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->minorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@25
    move-result v3

    #@26
    invoke-virtual {p0, v2, v3}, Landroid/view/FocusFinder;->getWeightedDistanceFor(II)I

    #@29
    move-result v2

    #@2a
    invoke-static {p1, p2, p4}, Landroid/view/FocusFinder;->majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@2d
    move-result v3

    #@2e
    invoke-static {p1, p2, p4}, Landroid/view/FocusFinder;->minorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    #@31
    move-result v4

    #@32
    invoke-virtual {p0, v3, v4}, Landroid/view/FocusFinder;->getWeightedDistanceFor(II)I

    #@35
    move-result v3

    #@36
    if-lt v2, v3, :cond_9

    #@38
    move v0, v1

    #@39
    goto :goto_9
.end method

.method isCandidate(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Z
    .registers 8
    .parameter "srcRect"
    .parameter "destRect"
    .parameter "direction"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 394
    sparse-switch p3, :sswitch_data_5e

    #@5
    .line 408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 396
    :sswitch_d
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@f
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@11
    if-gt v2, v3, :cond_19

    #@13
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@15
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@17
    if-lt v2, v3, :cond_20

    #@19
    :cond_19
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@1b
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@1d
    if-le v2, v3, :cond_20

    #@1f
    .line 405
    :cond_1f
    :goto_1f
    return v0

    #@20
    :cond_20
    move v0, v1

    #@21
    .line 396
    goto :goto_1f

    #@22
    .line 399
    :sswitch_22
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@24
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@26
    if-lt v2, v3, :cond_2e

    #@28
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@2a
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@2c
    if-gt v2, v3, :cond_34

    #@2e
    :cond_2e
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@30
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@32
    if-lt v2, v3, :cond_1f

    #@34
    :cond_34
    move v0, v1

    #@35
    goto :goto_1f

    #@36
    .line 402
    :sswitch_36
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    #@38
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    #@3a
    if-gt v2, v3, :cond_42

    #@3c
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@3e
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    #@40
    if-lt v2, v3, :cond_48

    #@42
    :cond_42
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@44
    iget v3, p2, Landroid/graphics/Rect;->top:I

    #@46
    if-gt v2, v3, :cond_1f

    #@48
    :cond_48
    move v0, v1

    #@49
    goto :goto_1f

    #@4a
    .line 405
    :sswitch_4a
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@4c
    iget v3, p2, Landroid/graphics/Rect;->top:I

    #@4e
    if-lt v2, v3, :cond_56

    #@50
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    #@52
    iget v3, p2, Landroid/graphics/Rect;->top:I

    #@54
    if-gt v2, v3, :cond_5c

    #@56
    :cond_56
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    #@58
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    #@5a
    if-lt v2, v3, :cond_1f

    #@5c
    :cond_5c
    move v0, v1

    #@5d
    goto :goto_1f

    #@5e
    .line 394
    :sswitch_data_5e
    .sparse-switch
        0x11 -> :sswitch_d
        0x21 -> :sswitch_36
        0x42 -> :sswitch_22
        0x82 -> :sswitch_4a
    .end sparse-switch
.end method

.method isToDirectionOf(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 8
    .parameter "direction"
    .parameter "src"
    .parameter "dest"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 437
    sparse-switch p1, :sswitch_data_2e

    #@5
    .line 447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 439
    :sswitch_d
    iget v2, p2, Landroid/graphics/Rect;->left:I

    #@f
    iget v3, p3, Landroid/graphics/Rect;->right:I

    #@11
    if-lt v2, v3, :cond_14

    #@13
    .line 445
    :cond_13
    :goto_13
    return v0

    #@14
    :cond_14
    move v0, v1

    #@15
    .line 439
    goto :goto_13

    #@16
    .line 441
    :sswitch_16
    iget v2, p2, Landroid/graphics/Rect;->right:I

    #@18
    iget v3, p3, Landroid/graphics/Rect;->left:I

    #@1a
    if-le v2, v3, :cond_13

    #@1c
    move v0, v1

    #@1d
    goto :goto_13

    #@1e
    .line 443
    :sswitch_1e
    iget v2, p2, Landroid/graphics/Rect;->top:I

    #@20
    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    #@22
    if-ge v2, v3, :cond_13

    #@24
    move v0, v1

    #@25
    goto :goto_13

    #@26
    .line 445
    :sswitch_26
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    #@28
    iget v3, p3, Landroid/graphics/Rect;->top:I

    #@2a
    if-le v2, v3, :cond_13

    #@2c
    move v0, v1

    #@2d
    goto :goto_13

    #@2e
    .line 437
    :sswitch_data_2e
    .sparse-switch
        0x11 -> :sswitch_d
        0x21 -> :sswitch_1e
        0x42 -> :sswitch_16
        0x82 -> :sswitch_26
    .end sparse-switch
.end method
