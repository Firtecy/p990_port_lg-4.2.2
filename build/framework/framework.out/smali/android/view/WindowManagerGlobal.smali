.class public final Landroid/view/WindowManagerGlobal;
.super Ljava/lang/Object;
.source "WindowManagerGlobal.java"


# static fields
.field public static final ADD_APP_EXITING:I = -0x4

.field public static final ADD_BAD_APP_TOKEN:I = -0x1

.field public static final ADD_BAD_SUBWINDOW_TOKEN:I = -0x2

.field public static final ADD_DUPLICATE_ADD:I = -0x5

.field public static final ADD_FLAG_APP_VISIBLE:I = 0x2

.field public static final ADD_FLAG_IN_TOUCH_MODE:I = 0x1

.field public static final ADD_INVALID_DISPLAY:I = -0x9

.field public static final ADD_MULTIPLE_SINGLETON:I = -0x7

.field public static final ADD_NOT_APP_TOKEN:I = -0x3

.field public static final ADD_OKAY:I = 0x0

.field public static final ADD_PERMISSION_DENIED:I = -0x8

.field public static final ADD_STARTING_NOT_NEEDED:I = -0x6

.field public static final RELAYOUT_DEFER_SURFACE_DESTROY:I = 0x2

.field public static final RELAYOUT_INSETS_PENDING:I = 0x1

.field public static final RELAYOUT_RES_ANIMATING:I = 0x8

.field public static final RELAYOUT_RES_FIRST_TIME:I = 0x2

.field public static final RELAYOUT_RES_IN_TOUCH_MODE:I = 0x1

.field public static final RELAYOUT_RES_SURFACE_CHANGED:I = 0x4

.field private static final TAG:Ljava/lang/String; = "WindowManager"

.field private static sDefaultWindowManager:Landroid/view/WindowManagerGlobal;

.field private static sWindowManagerService:Landroid/view/IWindowManager;

.field private static sWindowSession:Landroid/view/IWindowSession;


# instance fields
.field private final mLock:Ljava/lang/Object;

.field private mNeedsEglTerminate:Z

.field private mParams:[Landroid/view/WindowManager$LayoutParams;

.field private mRoots:[Landroid/view/ViewRootImpl;

.field private mSystemPropertyUpdater:Ljava/lang/Runnable;

.field private mViews:[Landroid/view/View;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 108
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@a
    .line 118
    return-void
.end method

.method static synthetic access$000(Landroid/view/WindowManagerGlobal;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/view/WindowManagerGlobal;)[Landroid/view/ViewRootImpl;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@2
    return-object v0
.end method

.method private findViewLocked(Landroid/view/View;Z)I
    .registers 7
    .parameter "view"
    .parameter "required"

    #@0
    .prologue
    .line 374
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@2
    if-eqz v2, :cond_14

    #@4
    .line 375
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@6
    array-length v0, v2

    #@7
    .line 376
    .local v0, count:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_14

    #@a
    .line 377
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@c
    aget-object v2, v2, v1

    #@e
    if-ne v2, p1, :cond_11

    #@10
    .line 385
    .end local v0           #count:I
    .end local v1           #i:I
    :goto_10
    return v1

    #@11
    .line 376
    .restart local v0       #count:I
    .restart local v1       #i:I
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_8

    #@14
    .line 382
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_14
    if-eqz p2, :cond_1e

    #@16
    .line 383
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v3, "View not attached to window manager"

    #@1a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v2

    #@1e
    .line 385
    :cond_1e
    const/4 v1, -0x1

    #@1f
    goto :goto_10
.end method

.method public static getInstance()Landroid/view/WindowManagerGlobal;
    .registers 2

    #@0
    .prologue
    .line 121
    const-class v1, Landroid/view/WindowManagerGlobal;

    #@2
    monitor-enter v1

    #@3
    .line 122
    :try_start_3
    sget-object v0, Landroid/view/WindowManagerGlobal;->sDefaultWindowManager:Landroid/view/WindowManagerGlobal;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 123
    new-instance v0, Landroid/view/WindowManagerGlobal;

    #@9
    invoke-direct {v0}, Landroid/view/WindowManagerGlobal;-><init>()V

    #@c
    sput-object v0, Landroid/view/WindowManagerGlobal;->sDefaultWindowManager:Landroid/view/WindowManagerGlobal;

    #@e
    .line 125
    :cond_e
    sget-object v0, Landroid/view/WindowManagerGlobal;->sDefaultWindowManager:Landroid/view/WindowManagerGlobal;

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 126
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public static getWindowManagerService()Landroid/view/IWindowManager;
    .registers 2

    #@0
    .prologue
    .line 130
    const-class v1, Landroid/view/WindowManagerGlobal;

    #@2
    monitor-enter v1

    #@3
    .line 131
    :try_start_3
    sget-object v0, Landroid/view/WindowManagerGlobal;->sWindowManagerService:Landroid/view/IWindowManager;

    #@5
    if-nez v0, :cond_14

    #@7
    .line 132
    const-string/jumbo v0, "window"

    #@a
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Landroid/view/WindowManagerGlobal;->sWindowManagerService:Landroid/view/IWindowManager;

    #@14
    .line 135
    :cond_14
    sget-object v0, Landroid/view/WindowManagerGlobal;->sWindowManagerService:Landroid/view/IWindowManager;

    #@16
    monitor-exit v1

    #@17
    return-object v0

    #@18
    .line 136
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method private static getWindowName(Landroid/view/ViewRootImpl;)Ljava/lang/String;
    .registers 3
    .parameter "root"

    #@0
    .prologue
    .line 490
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    iget-object v1, p0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@7
    invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "/"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const/16 v1, 0x40

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@2a
    move-result v1

    #@2b
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    return-object v0
.end method

.method public static getWindowSession(Landroid/os/Looper;)Landroid/view/IWindowSession;
    .registers 8
    .parameter "mainLooper"

    #@0
    .prologue
    .line 140
    const-class v5, Landroid/view/WindowManagerGlobal;

    #@2
    monitor-enter v5

    #@3
    .line 141
    :try_start_3
    sget-object v4, Landroid/view/WindowManagerGlobal;->sWindowSession:Landroid/view/IWindowSession;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_32

    #@5
    if-nez v4, :cond_25

    #@7
    .line 143
    :try_start_7
    invoke-static {p0}, Landroid/view/inputmethod/InputMethodManager;->getInstance(Landroid/os/Looper;)Landroid/view/inputmethod/InputMethodManager;

    #@a
    move-result-object v2

    #@b
    .line 144
    .local v2, imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    #@e
    move-result-object v3

    #@f
    .line 145
    .local v3, windowManager:Landroid/view/IWindowManager;
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodManager;->getClient()Lcom/android/internal/view/IInputMethodClient;

    #@12
    move-result-object v4

    #@13
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodManager;->getInputContext()Lcom/android/internal/view/IInputContext;

    #@16
    move-result-object v6

    #@17
    invoke-interface {v3, v4, v6}, Landroid/view/IWindowManager;->openSession(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;)Landroid/view/IWindowSession;

    #@1a
    move-result-object v4

    #@1b
    sput-object v4, Landroid/view/WindowManagerGlobal;->sWindowSession:Landroid/view/IWindowSession;

    #@1d
    .line 147
    const/4 v4, 0x2

    #@1e
    invoke-interface {v3, v4}, Landroid/view/IWindowManager;->getAnimationScale(I)F

    #@21
    move-result v0

    #@22
    .line 148
    .local v0, animatorScale:F
    invoke-static {v0}, Landroid/animation/ValueAnimator;->setDurationScale(F)V
    :try_end_25
    .catchall {:try_start_7 .. :try_end_25} :catchall_32
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_25} :catch_29

    #@25
    .line 153
    .end local v0           #animatorScale:F
    .end local v2           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v3           #windowManager:Landroid/view/IWindowManager;
    :cond_25
    :goto_25
    :try_start_25
    sget-object v4, Landroid/view/WindowManagerGlobal;->sWindowSession:Landroid/view/IWindowSession;

    #@27
    monitor-exit v5

    #@28
    return-object v4

    #@29
    .line 149
    :catch_29
    move-exception v1

    #@2a
    .line 150
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@2c
    const-string v6, "Failed to open window session"

    #@2e
    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31
    goto :goto_25

    #@32
    .line 154
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_32
    move-exception v4

    #@33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_25 .. :try_end_34} :catchall_32

    #@34
    throw v4
.end method

.method public static peekWindowSession()Landroid/view/IWindowSession;
    .registers 2

    #@0
    .prologue
    .line 158
    const-class v1, Landroid/view/WindowManagerGlobal;

    #@2
    monitor-enter v1

    #@3
    .line 159
    :try_start_3
    sget-object v0, Landroid/view/WindowManagerGlobal;->sWindowSession:Landroid/view/IWindowSession;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 160
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private static removeItem([Ljava/lang/Object;[Ljava/lang/Object;I)V
    .registers 5
    .parameter "dst"
    .parameter "src"
    .parameter "index"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 363
    array-length v0, p0

    #@2
    if-lez v0, :cond_15

    #@4
    .line 364
    if-lez p2, :cond_9

    #@6
    .line 365
    invoke-static {p1, v1, p0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@9
    .line 367
    :cond_9
    array-length v0, p0

    #@a
    if-ge p2, v0, :cond_15

    #@c
    .line 368
    add-int/lit8 v0, p2, 0x1

    #@e
    array-length v1, p1

    #@f
    sub-int/2addr v1, p2

    #@10
    add-int/lit8 v1, v1, -0x1

    #@12
    invoke-static {p1, v0, p0, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@15
    .line 371
    :cond_15
    return-void
.end method

.method private removeViewLocked(IZ)Landroid/view/View;
    .registers 11
    .parameter "index"
    .parameter "immediate"

    #@0
    .prologue
    .line 327
    iget-object v7, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@2
    aget-object v2, v7, p1

    #@4
    .line 328
    .local v2, root:Landroid/view/ViewRootImpl;
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->getView()Landroid/view/View;

    #@7
    move-result-object v6

    #@8
    .line 330
    .local v6, view:Landroid/view/View;
    if-eqz v6, :cond_1f

    #@a
    .line 331
    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@d
    move-result-object v7

    #@e
    invoke-static {v7}, Landroid/view/inputmethod/InputMethodManager;->getInstance(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    #@11
    move-result-object v1

    #@12
    .line 332
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_1f

    #@14
    .line 333
    iget-object v7, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@16
    aget-object v7, v7, p1

    #@18
    invoke-virtual {v7}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@1b
    move-result-object v7

    #@1c
    invoke-virtual {v1, v7}, Landroid/view/inputmethod/InputMethodManager;->windowDismissed(Landroid/os/IBinder;)V

    #@1f
    .line 336
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_1f
    invoke-virtual {v2, p2}, Landroid/view/ViewRootImpl;->die(Z)V

    #@22
    .line 338
    iget-object v7, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@24
    array-length v0, v7

    #@25
    .line 341
    .local v0, count:I
    add-int/lit8 v7, v0, -0x1

    #@27
    new-array v5, v7, [Landroid/view/View;

    #@29
    .line 342
    .local v5, tmpViews:[Landroid/view/View;
    iget-object v7, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@2b
    invoke-static {v5, v7, p1}, Landroid/view/WindowManagerGlobal;->removeItem([Ljava/lang/Object;[Ljava/lang/Object;I)V

    #@2e
    .line 343
    iput-object v5, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@30
    .line 345
    add-int/lit8 v7, v0, -0x1

    #@32
    new-array v4, v7, [Landroid/view/ViewRootImpl;

    #@34
    .line 346
    .local v4, tmpRoots:[Landroid/view/ViewRootImpl;
    iget-object v7, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@36
    invoke-static {v4, v7, p1}, Landroid/view/WindowManagerGlobal;->removeItem([Ljava/lang/Object;[Ljava/lang/Object;I)V

    #@39
    .line 347
    iput-object v4, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@3b
    .line 349
    add-int/lit8 v7, v0, -0x1

    #@3d
    new-array v3, v7, [Landroid/view/WindowManager$LayoutParams;

    #@3f
    .line 351
    .local v3, tmpParams:[Landroid/view/WindowManager$LayoutParams;
    iget-object v7, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@41
    invoke-static {v3, v7, p1}, Landroid/view/WindowManagerGlobal;->removeItem([Ljava/lang/Object;[Ljava/lang/Object;I)V

    #@44
    .line 352
    iput-object v3, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@46
    .line 354
    if-eqz v6, :cond_4c

    #@48
    .line 355
    const/4 v7, 0x0

    #@49
    invoke-virtual {v6, v7}, Landroid/view/View;->assignParent(Landroid/view/ViewParent;)V

    #@4c
    .line 359
    :cond_4c
    return-object v6
.end method


# virtual methods
.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;Landroid/view/Display;Landroid/view/Window;)V
    .registers 19
    .parameter "view"
    .parameter "params"
    .parameter "display"
    .parameter "parentWindow"

    #@0
    .prologue
    .line 165
    if-nez p1, :cond_b

    #@2
    .line 166
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v10, "view must not be null"

    #@7
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v9

    #@b
    .line 168
    :cond_b
    if-nez p3, :cond_15

    #@d
    .line 169
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v10, "display must not be null"

    #@11
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v9

    #@15
    .line 171
    :cond_15
    move-object/from16 v0, p2

    #@17
    instance-of v9, v0, Landroid/view/WindowManager$LayoutParams;

    #@19
    if-nez v9, :cond_23

    #@1b
    .line 172
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v10, "Params must be WindowManager.LayoutParams"

    #@1f
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v9

    #@23
    :cond_23
    move-object/from16 v8, p2

    #@25
    .line 175
    check-cast v8, Landroid/view/WindowManager$LayoutParams;

    #@27
    .line 176
    .local v8, wparams:Landroid/view/WindowManager$LayoutParams;
    if-eqz p4, :cond_2e

    #@29
    .line 177
    move-object/from16 v0, p4

    #@2b
    invoke-virtual {v0, v8}, Landroid/view/Window;->adjustLayoutParamsForSubWindow(Landroid/view/WindowManager$LayoutParams;)V

    #@2e
    .line 181
    :cond_2e
    const/4 v6, 0x0

    #@2f
    .line 183
    .local v6, panelParentView:Landroid/view/View;
    iget-object v10, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@31
    monitor-enter v10

    #@32
    .line 185
    :try_start_32
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mSystemPropertyUpdater:Ljava/lang/Runnable;

    #@34
    if-nez v9, :cond_42

    #@36
    .line 186
    new-instance v9, Landroid/view/WindowManagerGlobal$1;

    #@38
    invoke-direct {v9, p0}, Landroid/view/WindowManagerGlobal$1;-><init>(Landroid/view/WindowManagerGlobal;)V

    #@3b
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mSystemPropertyUpdater:Ljava/lang/Runnable;

    #@3d
    .line 195
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mSystemPropertyUpdater:Ljava/lang/Runnable;

    #@3f
    invoke-static {v9}, Landroid/os/SystemProperties;->addChangeCallback(Ljava/lang/Runnable;)V

    #@42
    .line 198
    :cond_42
    const/4 v9, 0x0

    #@43
    invoke-direct {p0, p1, v9}, Landroid/view/WindowManagerGlobal;->findViewLocked(Landroid/view/View;Z)I

    #@46
    move-result v4

    #@47
    .line 199
    .local v4, index:I
    if-ltz v4, :cond_6b

    #@49
    .line 200
    new-instance v9, Ljava/lang/IllegalStateException;

    #@4b
    new-instance v11, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v12, "View "

    #@52
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v11

    #@56
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v11

    #@5a
    const-string v12, " has already been added to the window manager."

    #@5c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v11

    #@60
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v11

    #@64
    invoke-direct {v9, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@67
    throw v9

    #@68
    .line 242
    .end local v4           #index:I
    :catchall_68
    move-exception v9

    #@69
    monitor-exit v10
    :try_end_6a
    .catchall {:try_start_32 .. :try_end_6a} :catchall_68

    #@6a
    throw v9

    #@6b
    .line 206
    .restart local v4       #index:I
    :cond_6b
    :try_start_6b
    iget v9, v8, Landroid/view/WindowManager$LayoutParams;->type:I

    #@6d
    const/16 v11, 0x3e8

    #@6f
    if-lt v9, v11, :cond_98

    #@71
    iget v9, v8, Landroid/view/WindowManager$LayoutParams;->type:I

    #@73
    const/16 v11, 0x7cf

    #@75
    if-gt v9, v11, :cond_98

    #@77
    .line 208
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@79
    if-eqz v9, :cond_96

    #@7b
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@7d
    array-length v1, v9

    #@7e
    .line 209
    .local v1, count:I
    :goto_7e
    const/4 v3, 0x0

    #@7f
    .local v3, i:I
    :goto_7f
    if-ge v3, v1, :cond_98

    #@81
    .line 210
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@83
    aget-object v9, v9, v3

    #@85
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mWindow:Landroid/view/ViewRootImpl$W;

    #@87
    invoke-virtual {v9}, Landroid/view/ViewRootImpl$W;->asBinder()Landroid/os/IBinder;

    #@8a
    move-result-object v9

    #@8b
    iget-object v11, v8, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@8d
    if-ne v9, v11, :cond_93

    #@8f
    .line 211
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@91
    aget-object v6, v9, v3

    #@93
    .line 209
    :cond_93
    add-int/lit8 v3, v3, 0x1

    #@95
    goto :goto_7f

    #@96
    .line 208
    .end local v1           #count:I
    .end local v3           #i:I
    :cond_96
    const/4 v1, 0x0

    #@97
    goto :goto_7e

    #@98
    .line 216
    :cond_98
    new-instance v7, Landroid/view/ViewRootImpl;

    #@9a
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@9d
    move-result-object v9

    #@9e
    move-object/from16 v0, p3

    #@a0
    invoke-direct {v7, v9, v0}, Landroid/view/ViewRootImpl;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    #@a3
    .line 218
    .local v7, root:Landroid/view/ViewRootImpl;
    invoke-virtual {p1, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@a6
    .line 220
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@a8
    if-nez v9, :cond_cd

    #@aa
    .line 221
    const/4 v4, 0x1

    #@ab
    .line 222
    const/4 v9, 0x1

    #@ac
    new-array v9, v9, [Landroid/view/View;

    #@ae
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@b0
    .line 223
    const/4 v9, 0x1

    #@b1
    new-array v9, v9, [Landroid/view/ViewRootImpl;

    #@b3
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@b5
    .line 224
    const/4 v9, 0x1

    #@b6
    new-array v9, v9, [Landroid/view/WindowManager$LayoutParams;

    #@b8
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@ba
    .line 237
    :goto_ba
    add-int/lit8 v4, v4, -0x1

    #@bc
    .line 239
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@be
    aput-object p1, v9, v4

    #@c0
    .line 240
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@c2
    aput-object v7, v9, v4

    #@c4
    .line 241
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@c6
    aput-object v8, v9, v4

    #@c8
    .line 242
    monitor-exit v10
    :try_end_c9
    .catchall {:try_start_6b .. :try_end_c9} :catchall_68

    #@c9
    .line 246
    :try_start_c9
    invoke-virtual {v7, p1, v8, v6}, Landroid/view/ViewRootImpl;->setView(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/View;)V
    :try_end_cc
    .catch Ljava/lang/RuntimeException; {:try_start_c9 .. :try_end_cc} :catch_100

    #@cc
    .line 257
    return-void

    #@cd
    .line 226
    :cond_cd
    :try_start_cd
    iget-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@cf
    array-length v9, v9

    #@d0
    add-int/lit8 v4, v9, 0x1

    #@d2
    .line 227
    iget-object v5, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@d4
    .line 228
    .local v5, old:[Ljava/lang/Object;
    new-array v9, v4, [Landroid/view/View;

    #@d6
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@d8
    .line 229
    const/4 v9, 0x0

    #@d9
    iget-object v11, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@db
    const/4 v12, 0x0

    #@dc
    add-int/lit8 v13, v4, -0x1

    #@de
    invoke-static {v5, v9, v11, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e1
    .line 230
    iget-object v5, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@e3
    .line 231
    new-array v9, v4, [Landroid/view/ViewRootImpl;

    #@e5
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@e7
    .line 232
    const/4 v9, 0x0

    #@e8
    iget-object v11, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@ea
    const/4 v12, 0x0

    #@eb
    add-int/lit8 v13, v4, -0x1

    #@ed
    invoke-static {v5, v9, v11, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@f0
    .line 233
    iget-object v5, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@f2
    .line 234
    new-array v9, v4, [Landroid/view/WindowManager$LayoutParams;

    #@f4
    iput-object v9, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@f6
    .line 235
    const/4 v9, 0x0

    #@f7
    iget-object v11, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@f9
    const/4 v12, 0x0

    #@fa
    add-int/lit8 v13, v4, -0x1

    #@fc
    invoke-static {v5, v9, v11, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_ff
    .catchall {:try_start_cd .. :try_end_ff} :catchall_68

    #@ff
    goto :goto_ba

    #@100
    .line 247
    .end local v5           #old:[Ljava/lang/Object;
    :catch_100
    move-exception v2

    #@101
    .line 249
    .local v2, e:Ljava/lang/RuntimeException;
    iget-object v10, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@103
    monitor-enter v10

    #@104
    .line 250
    const/4 v9, 0x0

    #@105
    :try_start_105
    invoke-direct {p0, p1, v9}, Landroid/view/WindowManagerGlobal;->findViewLocked(Landroid/view/View;Z)I

    #@108
    move-result v4

    #@109
    .line 251
    if-ltz v4, :cond_10f

    #@10b
    .line 252
    const/4 v9, 0x1

    #@10c
    invoke-direct {p0, v4, v9}, Landroid/view/WindowManagerGlobal;->removeViewLocked(IZ)Landroid/view/View;

    #@10f
    .line 254
    :cond_10f
    monitor-exit v10
    :try_end_110
    .catchall {:try_start_105 .. :try_end_110} :catchall_111

    #@110
    .line 255
    throw v2

    #@111
    .line 254
    :catchall_111
    move-exception v9

    #@112
    :try_start_112
    monitor-exit v10
    :try_end_113
    .catchall {:try_start_112 .. :try_end_113} :catchall_111

    #@113
    throw v9
.end method

.method public closeAll(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "token"
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 297
    iget-object v5, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 298
    :try_start_3
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@5
    if-nez v4, :cond_9

    #@7
    .line 299
    monitor-exit v5

    #@8
    .line 324
    :goto_8
    return-void

    #@9
    .line 301
    :cond_9
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@b
    array-length v0, v4

    #@c
    .line 303
    .local v0, count:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_6e

    #@f
    .line 306
    if-eqz p1, :cond_19

    #@11
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@13
    aget-object v4, v4, v1

    #@15
    iget-object v4, v4, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@17
    if-ne v4, p1, :cond_6b

    #@19
    .line 307
    :cond_19
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@1b
    aget-object v3, v4, v1

    #@1d
    .line 310
    .local v3, root:Landroid/view/ViewRootImpl;
    if-eqz p2, :cond_63

    #@1f
    .line 311
    new-instance v2, Landroid/view/WindowLeaked;

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    const-string v6, " "

    #@2c
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v6, " has leaked window "

    #@36
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v3}, Landroid/view/ViewRootImpl;->getView()Landroid/view/View;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v6, " that was originally added here"

    #@44
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-direct {v2, v4}, Landroid/view/WindowLeaked;-><init>(Ljava/lang/String;)V

    #@4f
    .line 314
    .local v2, leak:Landroid/view/WindowLeaked;
    invoke-virtual {v3}, Landroid/view/ViewRootImpl;->getLocation()Landroid/view/WindowLeaked;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Landroid/view/WindowLeaked;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v2, v4}, Landroid/view/WindowLeaked;->setStackTrace([Ljava/lang/StackTraceElement;)V

    #@5a
    .line 315
    const-string v4, "WindowManager"

    #@5c
    invoke-virtual {v2}, Landroid/view/WindowLeaked;->getMessage()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    invoke-static {v4, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63
    .line 318
    .end local v2           #leak:Landroid/view/WindowLeaked;
    :cond_63
    const/4 v4, 0x0

    #@64
    invoke-direct {p0, v1, v4}, Landroid/view/WindowManagerGlobal;->removeViewLocked(IZ)Landroid/view/View;

    #@67
    .line 319
    add-int/lit8 v1, v1, -0x1

    #@69
    .line 320
    add-int/lit8 v0, v0, -0x1

    #@6b
    .line 303
    .end local v3           #root:Landroid/view/ViewRootImpl;
    :cond_6b
    add-int/lit8 v1, v1, 0x1

    #@6d
    goto :goto_d

    #@6e
    .line 323
    :cond_6e
    monitor-exit v5

    #@6f
    goto :goto_8

    #@70
    .end local v0           #count:I
    .end local v1           #i:I
    :catchall_70
    move-exception v4

    #@71
    monitor-exit v5
    :try_end_72
    .catchall {:try_start_3 .. :try_end_72} :catchall_70

    #@72
    throw v4
.end method

.method public dumpGfxInfo(Ljava/io/FileDescriptor;)V
    .registers 19
    .parameter "fd"

    #@0
    .prologue
    .line 434
    new-instance v3, Ljava/io/FileOutputStream;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@7
    .line 435
    .local v3, fout:Ljava/io/FileOutputStream;
    new-instance v7, Ljava/io/PrintWriter;

    #@9
    invoke-direct {v7, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    #@c
    .line 437
    .local v7, pw:Ljava/io/PrintWriter;
    :try_start_c
    move-object/from16 v0, p0

    #@e
    iget-object v12, v0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v12
    :try_end_11
    .catchall {:try_start_c .. :try_end_11} :catchall_f1

    #@11
    .line 438
    :try_start_11
    move-object/from16 v0, p0

    #@13
    iget-object v11, v0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@15
    if-eqz v11, :cond_e9

    #@17
    .line 439
    move-object/from16 v0, p0

    #@19
    iget-object v11, v0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@1b
    array-length v1, v11

    #@1c
    .line 441
    .local v1, count:I
    const-string v11, "Profile data in ms:"

    #@1e
    invoke-virtual {v7, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@21
    .line 443
    const/4 v4, 0x0

    #@22
    .local v4, i:I
    :goto_22
    if-ge v4, v1, :cond_49

    #@24
    .line 444
    move-object/from16 v0, p0

    #@26
    iget-object v11, v0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@28
    aget-object v9, v11, v4

    #@2a
    .line 445
    .local v9, root:Landroid/view/ViewRootImpl;
    invoke-static {v9}, Landroid/view/WindowManagerGlobal;->getWindowName(Landroid/view/ViewRootImpl;)Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    .line 446
    .local v6, name:Ljava/lang/String;
    const-string v11, "\n\t%s"

    #@30
    const/4 v13, 0x1

    #@31
    new-array v13, v13, [Ljava/lang/Object;

    #@33
    const/4 v14, 0x0

    #@34
    aput-object v6, v13, v14

    #@36
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@39
    .line 448
    invoke-virtual {v9}, Landroid/view/ViewRootImpl;->getView()Landroid/view/View;

    #@3c
    move-result-object v11

    #@3d
    iget-object v11, v11, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3f
    iget-object v8, v11, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@41
    .line 450
    .local v8, renderer:Landroid/view/HardwareRenderer;
    if-eqz v8, :cond_46

    #@43
    .line 451
    invoke-virtual {v8, v7}, Landroid/view/HardwareRenderer;->dumpGfxInfo(Ljava/io/PrintWriter;)V

    #@46
    .line 443
    :cond_46
    add-int/lit8 v4, v4, 0x1

    #@48
    goto :goto_22

    #@49
    .line 455
    .end local v6           #name:Ljava/lang/String;
    .end local v8           #renderer:Landroid/view/HardwareRenderer;
    .end local v9           #root:Landroid/view/ViewRootImpl;
    :cond_49
    const-string v11, "\nView hierarchy:\n"

    #@4b
    invoke-virtual {v7, v11}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4e
    .line 457
    const/4 v10, 0x0

    #@4f
    .line 458
    .local v10, viewsCount:I
    const/4 v2, 0x0

    #@50
    .line 459
    .local v2, displayListsSize:I
    const/4 v11, 0x2

    #@51
    new-array v5, v11, [I

    #@53
    .line 461
    .local v5, info:[I
    const/4 v4, 0x0

    #@54
    :goto_54
    if-ge v4, v1, :cond_b7

    #@56
    .line 462
    move-object/from16 v0, p0

    #@58
    iget-object v11, v0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@5a
    aget-object v9, v11, v4

    #@5c
    .line 463
    .restart local v9       #root:Landroid/view/ViewRootImpl;
    invoke-virtual {v9, v5}, Landroid/view/ViewRootImpl;->dumpGfxInfo([I)V

    #@5f
    .line 465
    invoke-static {v9}, Landroid/view/WindowManagerGlobal;->getWindowName(Landroid/view/ViewRootImpl;)Ljava/lang/String;

    #@62
    move-result-object v6

    #@63
    .line 466
    .restart local v6       #name:Ljava/lang/String;
    const-string v11, "  %s\n  %d views, %.2f kB of display lists"

    #@65
    const/4 v13, 0x3

    #@66
    new-array v13, v13, [Ljava/lang/Object;

    #@68
    const/4 v14, 0x0

    #@69
    aput-object v6, v13, v14

    #@6b
    const/4 v14, 0x1

    #@6c
    const/4 v15, 0x0

    #@6d
    aget v15, v5, v15

    #@6f
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@72
    move-result-object v15

    #@73
    aput-object v15, v13, v14

    #@75
    const/4 v14, 0x2

    #@76
    const/4 v15, 0x1

    #@77
    aget v15, v5, v15

    #@79
    int-to-float v15, v15

    #@7a
    const/high16 v16, 0x4480

    #@7c
    div-float v15, v15, v16

    #@7e
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@81
    move-result-object v15

    #@82
    aput-object v15, v13, v14

    #@84
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@87
    .line 468
    invoke-virtual {v9}, Landroid/view/ViewRootImpl;->getView()Landroid/view/View;

    #@8a
    move-result-object v11

    #@8b
    iget-object v11, v11, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@8d
    iget-object v8, v11, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@8f
    .line 470
    .restart local v8       #renderer:Landroid/view/HardwareRenderer;
    if-eqz v8, :cond_a4

    #@91
    .line 471
    const-string v11, ", %d frames rendered"

    #@93
    const/4 v13, 0x1

    #@94
    new-array v13, v13, [Ljava/lang/Object;

    #@96
    const/4 v14, 0x0

    #@97
    invoke-virtual {v8}, Landroid/view/HardwareRenderer;->getFrameCount()J

    #@9a
    move-result-wide v15

    #@9b
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9e
    move-result-object v15

    #@9f
    aput-object v15, v13, v14

    #@a1
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@a4
    .line 473
    :cond_a4
    const-string v11, "\n\n"

    #@a6
    const/4 v13, 0x0

    #@a7
    new-array v13, v13, [Ljava/lang/Object;

    #@a9
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@ac
    .line 475
    const/4 v11, 0x0

    #@ad
    aget v11, v5, v11

    #@af
    add-int/2addr v10, v11

    #@b0
    .line 476
    const/4 v11, 0x1

    #@b1
    aget v11, v5, v11

    #@b3
    add-int/2addr v2, v11

    #@b4
    .line 461
    add-int/lit8 v4, v4, 0x1

    #@b6
    goto :goto_54

    #@b7
    .line 479
    .end local v6           #name:Ljava/lang/String;
    .end local v8           #renderer:Landroid/view/HardwareRenderer;
    .end local v9           #root:Landroid/view/ViewRootImpl;
    :cond_b7
    const-string v11, "\nTotal ViewRootImpl: %d\n"

    #@b9
    const/4 v13, 0x1

    #@ba
    new-array v13, v13, [Ljava/lang/Object;

    #@bc
    const/4 v14, 0x0

    #@bd
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c0
    move-result-object v15

    #@c1
    aput-object v15, v13, v14

    #@c3
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@c6
    .line 480
    const-string v11, "Total Views:        %d\n"

    #@c8
    const/4 v13, 0x1

    #@c9
    new-array v13, v13, [Ljava/lang/Object;

    #@cb
    const/4 v14, 0x0

    #@cc
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cf
    move-result-object v15

    #@d0
    aput-object v15, v13, v14

    #@d2
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@d5
    .line 481
    const-string v11, "Total DisplayList:  %.2f kB\n\n"

    #@d7
    const/4 v13, 0x1

    #@d8
    new-array v13, v13, [Ljava/lang/Object;

    #@da
    const/4 v14, 0x0

    #@db
    int-to-float v15, v2

    #@dc
    const/high16 v16, 0x4480

    #@de
    div-float v15, v15, v16

    #@e0
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@e3
    move-result-object v15

    #@e4
    aput-object v15, v13, v14

    #@e6
    invoke-virtual {v7, v11, v13}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@e9
    .line 483
    .end local v1           #count:I
    .end local v2           #displayListsSize:I
    .end local v4           #i:I
    .end local v5           #info:[I
    .end local v10           #viewsCount:I
    :cond_e9
    monitor-exit v12
    :try_end_ea
    .catchall {:try_start_11 .. :try_end_ea} :catchall_ee

    #@ea
    .line 485
    invoke-virtual {v7}, Ljava/io/PrintWriter;->flush()V

    #@ed
    .line 487
    return-void

    #@ee
    .line 483
    :catchall_ee
    move-exception v11

    #@ef
    :try_start_ef
    monitor-exit v12
    :try_end_f0
    .catchall {:try_start_ef .. :try_end_f0} :catchall_ee

    #@f0
    :try_start_f0
    throw v11
    :try_end_f1
    .catchall {:try_start_f0 .. :try_end_f1} :catchall_f1

    #@f1
    .line 485
    :catchall_f1
    move-exception v11

    #@f2
    invoke-virtual {v7}, Ljava/io/PrintWriter;->flush()V

    #@f5
    throw v11
.end method

.method public endTrimMemory()V
    .registers 2

    #@0
    .prologue
    .line 415
    invoke-static {}, Landroid/view/HardwareRenderer;->endTrimMemory()V

    #@3
    .line 417
    iget-boolean v0, p0, Landroid/view/WindowManagerGlobal;->mNeedsEglTerminate:Z

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 418
    invoke-static {}, Landroid/opengl/ManagedEGLContext;->doTerminate()Z

    #@a
    .line 419
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/view/WindowManagerGlobal;->mNeedsEglTerminate:Z

    #@d
    .line 421
    :cond_d
    return-void
.end method

.method public removeView(Landroid/view/View;Z)V
    .registers 9
    .parameter "view"
    .parameter "immediate"

    #@0
    .prologue
    .line 280
    if-nez p1, :cond_b

    #@2
    .line 281
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "view must not be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 284
    :cond_b
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@d
    monitor-enter v3

    #@e
    .line 285
    const/4 v2, 0x1

    #@f
    :try_start_f
    invoke-direct {p0, p1, v2}, Landroid/view/WindowManagerGlobal;->findViewLocked(Landroid/view/View;Z)I

    #@12
    move-result v1

    #@13
    .line 286
    .local v1, index:I
    invoke-direct {p0, v1, p2}, Landroid/view/WindowManagerGlobal;->removeViewLocked(IZ)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    .line 287
    .local v0, curView:Landroid/view/View;
    if-ne v0, p1, :cond_1b

    #@19
    .line 288
    monitor-exit v3

    #@1a
    return-void

    #@1b
    .line 291
    :cond_1b
    new-instance v2, Ljava/lang/IllegalStateException;

    #@1d
    new-instance v4, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v5, "Calling with view "

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    const-string v5, " but the ViewAncestor is attached to "

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v2

    #@3e
    .line 293
    .end local v0           #curView:Landroid/view/View;
    .end local v1           #index:I
    :catchall_3e
    move-exception v2

    #@3f
    monitor-exit v3
    :try_end_40
    .catchall {:try_start_f .. :try_end_40} :catchall_3e

    #@40
    throw v2
.end method

.method public reportNewConfiguration(Landroid/content/res/Configuration;)V
    .registers 8
    .parameter "config"

    #@0
    .prologue
    .line 509
    iget-object v5, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 510
    :try_start_3
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@5
    if-eqz v4, :cond_1d

    #@7
    .line 511
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@9
    array-length v1, v4

    #@a
    .line 512
    .local v1, count:I
    new-instance v0, Landroid/content/res/Configuration;

    #@c
    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_1f

    #@f
    .line 513
    .end local p1
    .local v0, config:Landroid/content/res/Configuration;
    const/4 v2, 0x0

    #@10
    .local v2, i:I
    :goto_10
    if-ge v2, v1, :cond_1c

    #@12
    .line 514
    :try_start_12
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@14
    aget-object v3, v4, v2

    #@16
    .line 515
    .local v3, root:Landroid/view/ViewRootImpl;
    invoke-virtual {v3, v0}, Landroid/view/ViewRootImpl;->requestUpdateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_22

    #@19
    .line 513
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_10

    #@1c
    .end local v3           #root:Landroid/view/ViewRootImpl;
    :cond_1c
    move-object p1, v0

    #@1d
    .line 518
    .end local v0           #config:Landroid/content/res/Configuration;
    .end local v1           #count:I
    .end local v2           #i:I
    .restart local p1
    :cond_1d
    :try_start_1d
    monitor-exit v5

    #@1e
    .line 519
    return-void

    #@1f
    .line 518
    :catchall_1f
    move-exception v4

    #@20
    :goto_20
    monitor-exit v5
    :try_end_21
    .catchall {:try_start_1d .. :try_end_21} :catchall_1f

    #@21
    throw v4

    #@22
    .end local p1
    .restart local v0       #config:Landroid/content/res/Configuration;
    .restart local v1       #count:I
    .restart local v2       #i:I
    :catchall_22
    move-exception v4

    #@23
    move-object p1, v0

    #@24
    .end local v0           #config:Landroid/content/res/Configuration;
    .restart local p1
    goto :goto_20
.end method

.method public setStoppedState(Landroid/os/IBinder;Z)V
    .registers 8
    .parameter "token"
    .parameter "stopped"

    #@0
    .prologue
    .line 495
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 496
    :try_start_3
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@5
    if-eqz v3, :cond_21

    #@7
    .line 497
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@9
    array-length v0, v3

    #@a
    .line 498
    .local v0, count:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_21

    #@d
    .line 499
    if-eqz p1, :cond_17

    #@f
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@11
    aget-object v3, v3, v1

    #@13
    iget-object v3, v3, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@15
    if-ne v3, p1, :cond_1e

    #@17
    .line 500
    :cond_17
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@19
    aget-object v2, v3, v1

    #@1b
    .line 501
    .local v2, root:Landroid/view/ViewRootImpl;
    invoke-virtual {v2, p2}, Landroid/view/ViewRootImpl;->setStopped(Z)V

    #@1e
    .line 498
    .end local v2           #root:Landroid/view/ViewRootImpl;
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    #@20
    goto :goto_b

    #@21
    .line 505
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_21
    monitor-exit v4

    #@22
    .line 506
    return-void

    #@23
    .line 505
    :catchall_23
    move-exception v3

    #@24
    monitor-exit v4
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v3
.end method

.method public startTrimMemory(I)V
    .registers 7
    .parameter "level"

    #@0
    .prologue
    const/16 v4, 0x50

    #@2
    .line 389
    invoke-static {}, Landroid/view/HardwareRenderer;->isAvailable()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_1c

    #@8
    .line 392
    if-ge p1, v4, :cond_14

    #@a
    const/16 v2, 0x3c

    #@c
    if-lt p1, v2, :cond_38

    #@e
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_38

    #@14
    .line 397
    :cond_14
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@16
    monitor-enter v3

    #@17
    .line 398
    :try_start_17
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@19
    if-nez v2, :cond_1d

    #@1b
    monitor-exit v3

    #@1c
    .line 412
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 399
    :cond_1d
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@1f
    array-length v0, v2

    #@20
    .line 400
    .local v0, count:I
    const/4 v1, 0x0

    #@21
    .local v1, i:I
    :goto_21
    if-ge v1, v0, :cond_2d

    #@23
    .line 401
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@25
    aget-object v2, v2, v1

    #@27
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->terminateHardwareResources()V

    #@2a
    .line 400
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_21

    #@2d
    .line 403
    :cond_2d
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_17 .. :try_end_2e} :catchall_35

    #@2e
    .line 405
    const/4 v2, 0x1

    #@2f
    iput-boolean v2, p0, Landroid/view/WindowManagerGlobal;->mNeedsEglTerminate:Z

    #@31
    .line 406
    invoke-static {v4}, Landroid/view/HardwareRenderer;->startTrimMemory(I)V

    #@34
    goto :goto_1c

    #@35
    .line 403
    .end local v0           #count:I
    .end local v1           #i:I
    :catchall_35
    move-exception v2

    #@36
    :try_start_36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 410
    :cond_38
    invoke-static {p1}, Landroid/view/HardwareRenderer;->startTrimMemory(I)V

    #@3b
    goto :goto_1c
.end method

.method public trimLocalMemory()V
    .registers 5

    #@0
    .prologue
    .line 424
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 425
    :try_start_3
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@5
    if-nez v2, :cond_9

    #@7
    monitor-exit v3

    #@8
    .line 431
    :goto_8
    return-void

    #@9
    .line 426
    :cond_9
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mViews:[Landroid/view/View;

    #@b
    array-length v0, v2

    #@c
    .line 427
    .local v0, count:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_19

    #@f
    .line 428
    iget-object v2, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@11
    aget-object v2, v2, v1

    #@13
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->destroyHardwareLayers()V

    #@16
    .line 427
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_d

    #@19
    .line 430
    :cond_19
    monitor-exit v3

    #@1a
    goto :goto_8

    #@1b
    .end local v0           #count:I
    .end local v1           #i:I
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit v3
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v2
.end method

.method public updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 8
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 260
    if-nez p1, :cond_b

    #@2
    .line 261
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v4, "view must not be null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 263
    :cond_b
    instance-of v3, p2, Landroid/view/WindowManager$LayoutParams;

    #@d
    if-nez v3, :cond_17

    #@f
    .line 264
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@11
    const-string v4, "Params must be WindowManager.LayoutParams"

    #@13
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v3

    #@17
    :cond_17
    move-object v2, p2

    #@18
    .line 267
    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    #@1a
    .line 269
    .local v2, wparams:Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@1d
    .line 271
    iget-object v4, p0, Landroid/view/WindowManagerGlobal;->mLock:Ljava/lang/Object;

    #@1f
    monitor-enter v4

    #@20
    .line 272
    const/4 v3, 0x1

    #@21
    :try_start_21
    invoke-direct {p0, p1, v3}, Landroid/view/WindowManagerGlobal;->findViewLocked(Landroid/view/View;Z)I

    #@24
    move-result v0

    #@25
    .line 273
    .local v0, index:I
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mRoots:[Landroid/view/ViewRootImpl;

    #@27
    aget-object v1, v3, v0

    #@29
    .line 274
    .local v1, root:Landroid/view/ViewRootImpl;
    iget-object v3, p0, Landroid/view/WindowManagerGlobal;->mParams:[Landroid/view/WindowManager$LayoutParams;

    #@2b
    aput-object v2, v3, v0

    #@2d
    .line 275
    const/4 v3, 0x0

    #@2e
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewRootImpl;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;Z)V

    #@31
    .line 276
    monitor-exit v4

    #@32
    .line 277
    return-void

    #@33
    .line 276
    .end local v0           #index:I
    .end local v1           #root:Landroid/view/ViewRootImpl;
    :catchall_33
    move-exception v3

    #@34
    monitor-exit v4
    :try_end_35
    .catchall {:try_start_21 .. :try_end_35} :catchall_33

    #@35
    throw v3
.end method
