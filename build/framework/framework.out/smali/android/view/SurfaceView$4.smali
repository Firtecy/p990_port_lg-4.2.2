.class Landroid/view/SurfaceView$4;
.super Ljava/lang/Object;
.source "SurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/SurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SurfaceHolder"


# instance fields
.field final synthetic this$0:Landroid/view/SurfaceView;


# direct methods
.method constructor <init>(Landroid/view/SurfaceView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 853
    iput-object p1, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method private final internalLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 12
    .parameter "dirty"

    #@0
    .prologue
    .line 926
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget-object v6, v6, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@4
    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@7
    .line 931
    const/4 v0, 0x0

    #@8
    .line 932
    .local v0, c:Landroid/graphics/Canvas;
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@a
    iget-boolean v6, v6, Landroid/view/SurfaceView;->mDrawingStopped:Z

    #@c
    if-nez v6, :cond_3c

    #@e
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@10
    iget-object v6, v6, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@12
    if-eqz v6, :cond_3c

    #@14
    .line 933
    if-nez p1, :cond_34

    #@16
    .line 934
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@18
    iget-object v6, v6, Landroid/view/SurfaceView;->mTmpDirty:Landroid/graphics/Rect;

    #@1a
    if-nez v6, :cond_25

    #@1c
    .line 935
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@1e
    new-instance v7, Landroid/graphics/Rect;

    #@20
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@23
    iput-object v7, v6, Landroid/view/SurfaceView;->mTmpDirty:Landroid/graphics/Rect;

    #@25
    .line 937
    :cond_25
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@27
    iget-object v6, v6, Landroid/view/SurfaceView;->mTmpDirty:Landroid/graphics/Rect;

    #@29
    iget-object v7, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2b
    iget-object v7, v7, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@2d
    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@30
    .line 938
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@32
    iget-object p1, v6, Landroid/view/SurfaceView;->mTmpDirty:Landroid/graphics/Rect;

    #@34
    .line 942
    :cond_34
    :try_start_34
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@36
    iget-object v6, v6, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@38
    invoke-virtual {v6, p1}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_3b} :catch_47

    #@3b
    move-result-object v0

    #@3c
    .line 949
    :cond_3c
    :goto_3c
    if-eqz v0, :cond_50

    #@3e
    .line 950
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@40
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@43
    move-result-wide v7

    #@44
    iput-wide v7, v6, Landroid/view/SurfaceView;->mLastLockTime:J

    #@46
    .line 969
    .end local v0           #c:Landroid/graphics/Canvas;
    :goto_46
    return-object v0

    #@47
    .line 943
    .restart local v0       #c:Landroid/graphics/Canvas;
    :catch_47
    move-exception v1

    #@48
    .line 944
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "SurfaceHolder"

    #@4a
    const-string v7, "Exception locking surface"

    #@4c
    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4f
    goto :goto_3c

    #@50
    .line 957
    .end local v1           #e:Ljava/lang/Exception;
    :cond_50
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@53
    move-result-wide v4

    #@54
    .line 958
    .local v4, now:J
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@56
    iget-wide v6, v6, Landroid/view/SurfaceView;->mLastLockTime:J

    #@58
    const-wide/16 v8, 0x64

    #@5a
    add-long v2, v6, v8

    #@5c
    .line 959
    .local v2, nextTime:J
    cmp-long v6, v2, v4

    #@5e
    if-lez v6, :cond_69

    #@60
    .line 961
    sub-long v6, v2, v4

    #@62
    :try_start_62
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_65
    .catch Ljava/lang/InterruptedException; {:try_start_62 .. :try_end_65} :catch_76

    #@65
    .line 964
    :goto_65
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@68
    move-result-wide v4

    #@69
    .line 966
    :cond_69
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@6b
    iput-wide v4, v6, Landroid/view/SurfaceView;->mLastLockTime:J

    #@6d
    .line 967
    iget-object v6, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@6f
    iget-object v6, v6, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@71
    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@74
    .line 969
    const/4 v0, 0x0

    #@75
    goto :goto_46

    #@76
    .line 962
    :catch_76
    move-exception v6

    #@77
    goto :goto_65
.end method


# virtual methods
.method public addCallback(Landroid/view/SurfaceHolder$Callback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 862
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget-object v1, v0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@4
    monitor-enter v1

    #@5
    .line 865
    :try_start_5
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@7
    iget-object v0, v0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_16

    #@f
    .line 866
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@11
    iget-object v0, v0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@16
    .line 868
    :cond_16
    monitor-exit v1

    #@17
    .line 869
    return-void

    #@18
    .line 868
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public getSurface()Landroid/view/Surface;
    .registers 2

    #@0
    .prologue
    .line 984
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget-object v0, v0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@4
    return-object v0
.end method

.method public getSurfaceFrame()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 988
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget-object v0, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@4
    return-object v0
.end method

.method public isCreating()Z
    .registers 2

    #@0
    .prologue
    .line 858
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget-boolean v0, v0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@4
    return v0
.end method

.method public lockCanvas()Landroid/graphics/Canvas;
    .registers 2

    #@0
    .prologue
    .line 918
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/view/SurfaceView$4;->internalLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 922
    invoke-direct {p0, p1}, Landroid/view/SurfaceView$4;->internalLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 872
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget-object v1, v0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@4
    monitor-enter v1

    #@5
    .line 873
    :try_start_5
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@7
    iget-object v0, v0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@c
    .line 874
    monitor-exit v1

    #@d
    .line 875
    return-void

    #@e
    .line 874
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public setFixedSize(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 878
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@2
    iget v0, v0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@4
    if-ne v0, p1, :cond_c

    #@6
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@8
    iget v0, v0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@a
    if-eq v0, p2, :cond_19

    #@c
    .line 879
    :cond_c
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@e
    iput p1, v0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@10
    .line 880
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@12
    iput p2, v0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@14
    .line 881
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@16
    invoke-virtual {v0}, Landroid/view/SurfaceView;->requestLayout()V

    #@19
    .line 883
    :cond_19
    return-void
.end method

.method public setFormat(I)V
    .registers 4
    .parameter "format"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 896
    const/4 v0, -0x1

    #@2
    if-ne p1, v0, :cond_5

    #@4
    .line 897
    const/4 p1, 0x4

    #@5
    .line 899
    :cond_5
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@7
    iput p1, v0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@9
    .line 900
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@b
    iget-object v0, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 901
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@11
    invoke-static {v0, v1, v1}, Landroid/view/SurfaceView;->access$000(Landroid/view/SurfaceView;ZZ)V

    #@14
    .line 903
    :cond_14
    return-void
.end method

.method public setKeepScreenOn(Z)V
    .registers 5
    .parameter "screenOn"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 912
    iget-object v2, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@3
    iget-object v2, v2, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@5
    invoke-virtual {v2, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 913
    .local v0, msg:Landroid/os/Message;
    if-eqz p1, :cond_15

    #@b
    :goto_b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 914
    iget-object v1, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@f
    iget-object v1, v1, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@11
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@14
    .line 915
    return-void

    #@15
    .line 913
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_b
.end method

.method public setSizeFromLayout()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 886
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@3
    iget v0, v0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@5
    if-ne v0, v2, :cond_d

    #@7
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@9
    iget v0, v0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@b
    if-eq v0, v2, :cond_1a

    #@d
    .line 887
    :cond_d
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@f
    iget-object v1, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@11
    iput v2, v1, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@13
    iput v2, v0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@15
    .line 888
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@17
    invoke-virtual {v0}, Landroid/view/SurfaceView;->requestLayout()V

    #@1a
    .line 890
    :cond_1a
    return-void
.end method

.method public setType(I)V
    .registers 2
    .parameter "type"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 909
    return-void
.end method

.method public unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 975
    if-eqz p1, :cond_9

    #@2
    .line 976
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@4
    iget-object v0, v0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@6
    invoke-virtual {v0, p1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@9
    .line 980
    :cond_9
    iget-object v0, p0, Landroid/view/SurfaceView$4;->this$0:Landroid/view/SurfaceView;

    #@b
    iget-object v0, v0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@d
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@10
    .line 981
    return-void
.end method
