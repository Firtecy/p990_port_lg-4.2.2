.class Landroid/view/View$AttachInfo;
.super Ljava/lang/Object;
.source "View.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AttachInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/View$AttachInfo$InvalidateInfo;,
        Landroid/view/View$AttachInfo$Callbacks;
    }
.end annotation


# instance fields
.field mAccessibilityFocusDrawable:Landroid/graphics/drawable/Drawable;

.field mAccessibilityWindowId:I

.field mApplicationScale:F

.field mCanvas:Landroid/graphics/Canvas;

.field final mContentInsets:Landroid/graphics/Rect;

.field mDebugLayout:Z

.field mDisabledSystemUiVisibility:I

.field final mDisplay:Landroid/view/Display;

.field mDrawingTime:J

.field mForceReportNewAttributes:Z

.field final mGivenInternalInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

.field mGlobalSystemUiVisibility:I

.field final mHandler:Landroid/os/Handler;

.field mHardwareAccelerated:Z

.field mHardwareAccelerationRequested:Z

.field mHardwareCanvas:Landroid/view/HardwareCanvas;

.field mHardwareRenderer:Landroid/view/HardwareRenderer;

.field mHasSystemUiListeners:Z

.field mHasWindowFocus:Z

.field mIgnoreDirtyState:Z

.field mInTouchMode:Z

.field mIncludeNotImportantViews:Z

.field final mInvalidateChildLocation:[I

.field mKeepScreenOn:Z

.field final mKeyDispatchState:Landroid/view/KeyEvent$DispatcherState;

.field mPanelParentWindowToken:Landroid/os/IBinder;

.field final mPoint:Landroid/graphics/Point;

.field mRecomputeGlobalAttributes:Z

.field final mRootCallbacks:Landroid/view/View$AttachInfo$Callbacks;

.field mRootView:Landroid/view/View;

.field mScalingRequired:Z

.field mScreenOn:Z

.field final mScrollContainers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final mSession:Landroid/view/IWindowSession;

.field mSetIgnoreDirtyState:Z

.field mSurface:Landroid/view/Surface;

.field mSystemUiVisibility:I

.field final mTempArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final mTmpInvalRect:Landroid/graphics/Rect;

.field final mTmpMatrix:Landroid/graphics/Matrix;

.field final mTmpTransformLocation:[F

.field final mTmpTransformRect:Landroid/graphics/RectF;

.field final mTmpTransformation:Landroid/view/animation/Transformation;

.field final mTransparentLocation:[I

.field final mTreeObserver:Landroid/view/ViewTreeObserver;

.field mTurnOffWindowResizeAnim:Z

.field mUse32BitDrawingCache:Z

.field final mViewRootImpl:Landroid/view/ViewRootImpl;

.field mViewScrollChanged:Z

.field mViewVisibilityChanged:Z

.field final mVisibleInsets:Landroid/graphics/Rect;

.field final mWindow:Landroid/view/IWindow;

.field mWindowLeft:I

.field final mWindowToken:Landroid/os/IBinder;

.field mWindowTop:I

.field mWindowVisibility:I


# direct methods
.method constructor <init>(Landroid/view/IWindowSession;Landroid/view/IWindow;Landroid/view/Display;Landroid/view/ViewRootImpl;Landroid/os/Handler;Landroid/view/View$AttachInfo$Callbacks;)V
    .registers 10
    .parameter "session"
    .parameter "window"
    .parameter "display"
    .parameter "viewRootImpl"
    .parameter "handler"
    .parameter "effectPlayer"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x2

    #@2
    .line 18215
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 18008
    new-instance v0, Landroid/graphics/Rect;

    #@7
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@a
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mContentInsets:Landroid/graphics/Rect;

    #@c
    .line 18015
    new-instance v0, Landroid/graphics/Rect;

    #@e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@11
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mVisibleInsets:Landroid/graphics/Rect;

    #@13
    .line 18024
    new-instance v0, Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@15
    invoke-direct {v0}, Landroid/view/ViewTreeObserver$InternalInsetsInfo;-><init>()V

    #@18
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mGivenInternalInsets:Landroid/view/ViewTreeObserver$InternalInsetsInfo;

    #@1a
    .line 18032
    new-instance v0, Ljava/util/ArrayList;

    #@1c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mScrollContainers:Ljava/util/ArrayList;

    #@21
    .line 18034
    new-instance v0, Landroid/view/KeyEvent$DispatcherState;

    #@23
    invoke-direct {v0}, Landroid/view/KeyEvent$DispatcherState;-><init>()V

    #@26
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mKeyDispatchState:Landroid/view/KeyEvent$DispatcherState;

    #@28
    .line 18061
    iput-boolean v2, p0, Landroid/view/View$AttachInfo;->mSetIgnoreDirtyState:Z

    #@2a
    .line 18119
    new-array v0, v1, [I

    #@2c
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTransparentLocation:[I

    #@2e
    .line 18125
    new-array v0, v1, [I

    #@30
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mInvalidateChildLocation:[I

    #@32
    .line 18132
    new-array v0, v1, [F

    #@34
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTmpTransformLocation:[F

    #@36
    .line 18138
    new-instance v0, Landroid/view/ViewTreeObserver;

    #@38
    invoke-direct {v0}, Landroid/view/ViewTreeObserver;-><init>()V

    #@3b
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@3d
    .line 18160
    new-instance v0, Landroid/graphics/Rect;

    #@3f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@42
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTmpInvalRect:Landroid/graphics/Rect;

    #@44
    .line 18165
    new-instance v0, Landroid/graphics/RectF;

    #@46
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@49
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTmpTransformRect:Landroid/graphics/RectF;

    #@4b
    .line 18170
    new-instance v0, Landroid/graphics/Matrix;

    #@4d
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@50
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTmpMatrix:Landroid/graphics/Matrix;

    #@52
    .line 18175
    new-instance v0, Landroid/view/animation/Transformation;

    #@54
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@57
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTmpTransformation:Landroid/view/animation/Transformation;

    #@59
    .line 18180
    new-instance v0, Ljava/util/ArrayList;

    #@5b
    const/16 v1, 0x18

    #@5d
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@60
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mTempArrayList:Ljava/util/ArrayList;

    #@62
    .line 18185
    const/4 v0, -0x1

    #@63
    iput v0, p0, Landroid/view/View$AttachInfo;->mAccessibilityWindowId:I

    #@65
    .line 18201
    const-string v0, "debug.layout"

    #@67
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6a
    move-result v0

    #@6b
    iput-boolean v0, p0, Landroid/view/View$AttachInfo;->mDebugLayout:Z

    #@6d
    .line 18206
    new-instance v0, Landroid/graphics/Point;

    #@6f
    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    #@72
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mPoint:Landroid/graphics/Point;

    #@74
    .line 18216
    iput-object p1, p0, Landroid/view/View$AttachInfo;->mSession:Landroid/view/IWindowSession;

    #@76
    .line 18217
    iput-object p2, p0, Landroid/view/View$AttachInfo;->mWindow:Landroid/view/IWindow;

    #@78
    .line 18218
    invoke-interface {p2}, Landroid/view/IWindow;->asBinder()Landroid/os/IBinder;

    #@7b
    move-result-object v0

    #@7c
    iput-object v0, p0, Landroid/view/View$AttachInfo;->mWindowToken:Landroid/os/IBinder;

    #@7e
    .line 18219
    iput-object p3, p0, Landroid/view/View$AttachInfo;->mDisplay:Landroid/view/Display;

    #@80
    .line 18220
    iput-object p4, p0, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@82
    .line 18221
    iput-object p5, p0, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@84
    .line 18222
    iput-object p6, p0, Landroid/view/View$AttachInfo;->mRootCallbacks:Landroid/view/View$AttachInfo$Callbacks;

    #@86
    .line 18223
    return-void
.end method
