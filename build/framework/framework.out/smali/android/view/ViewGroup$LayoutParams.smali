.class public Landroid/view/ViewGroup$LayoutParams;
.super Ljava/lang/Object;
.source "ViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# static fields
.field public static final FILL_PARENT:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MATCH_PARENT:I = -0x1

.field public static final WRAP_CONTENT:I = -0x2


# instance fields
.field public height:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x1
                to = "MATCH_PARENT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x2
                to = "WRAP_CONTENT"
            .end subannotation
        }
    .end annotation
.end field

.field public layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

.field public width:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x1
                to = "MATCH_PARENT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = -0x2
                to = "WRAP_CONTENT"
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5643
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 5644
    return-void
.end method

.method public constructor <init>(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 5624
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 5625
    iput p1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@5
    .line 5626
    iput p2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@7
    .line 5627
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "c"
    .parameter "attrs"

    #@0
    .prologue
    .line 5605
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 5606
    sget-object v1, Lcom/android/internal/R$styleable;->ViewGroup_Layout:[I

    #@5
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v0

    #@9
    .line 5607
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@a
    const/4 v2, 0x1

    #@b
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;->setBaseAttributes(Landroid/content/res/TypedArray;II)V

    #@e
    .line 5610
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@11
    .line 5611
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 5634
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 5635
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@5
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@7
    .line 5636
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@9
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@b
    .line 5637
    return-void
.end method

.method protected static sizeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 5705
    const/4 v0, -0x2

    #@1
    if-ne p0, v0, :cond_7

    #@3
    .line 5706
    const-string/jumbo v0, "wrap-content"

    #@6
    .line 5711
    :goto_6
    return-object v0

    #@7
    .line 5708
    :cond_7
    const/4 v0, -0x1

    #@8
    if-ne p0, v0, :cond_e

    #@a
    .line 5709
    const-string/jumbo v0, "match-parent"

    #@d
    goto :goto_6

    #@e
    .line 5711
    :cond_e
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    goto :goto_6
.end method


# virtual methods
.method public debug(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "output"

    #@0
    .prologue
    .line 5681
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "ViewGroup.LayoutParams={ width="

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@11
    invoke-static {v1}, Landroid/view/ViewGroup$LayoutParams;->sizeToString(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", height="

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@21
    invoke-static {v1}, Landroid/view/ViewGroup$LayoutParams;->sizeToString(I)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " }"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method

.method public onDebugDraw(Landroid/view/View;Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "view"
    .parameter "canvas"

    #@0
    .prologue
    .line 5694
    return-void
.end method

.method public resolveLayoutDirection(I)V
    .registers 2
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 5669
    return-void
.end method

.method protected setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .registers 5
    .parameter "a"
    .parameter "widthAttr"
    .parameter "heightAttr"

    #@0
    .prologue
    .line 5654
    const-string/jumbo v0, "layout_width"

    #@3
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@9
    .line 5655
    const-string/jumbo v0, "layout_height"

    #@c
    invoke-virtual {p1, p3, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@12
    .line 5656
    return-void
.end method
