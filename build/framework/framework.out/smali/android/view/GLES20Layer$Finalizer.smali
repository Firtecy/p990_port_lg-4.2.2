.class Landroid/view/GLES20Layer$Finalizer;
.super Ljava/lang/Object;
.source "GLES20Layer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/GLES20Layer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Finalizer"
.end annotation


# instance fields
.field private mLayerId:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "layerId"

    #@0
    .prologue
    .line 77
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 78
    iput p1, p0, Landroid/view/GLES20Layer$Finalizer;->mLayerId:I

    #@5
    .line 79
    return-void
.end method


# virtual methods
.method destroy()V
    .registers 2

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/view/GLES20Layer$Finalizer;->mLayerId:I

    #@2
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nDestroyLayer(I)V

    #@5
    .line 94
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/view/GLES20Layer$Finalizer;->mLayerId:I

    #@8
    .line 95
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 84
    :try_start_0
    iget v0, p0, Landroid/view/GLES20Layer$Finalizer;->mLayerId:I

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 85
    iget v0, p0, Landroid/view/GLES20Layer$Finalizer;->mLayerId:I

    #@6
    invoke-static {v0}, Landroid/view/GLES20Canvas;->nDestroyLayerDeferred(I)V
    :try_end_9
    .catchall {:try_start_0 .. :try_end_9} :catchall_d

    #@9
    .line 88
    :cond_9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    .line 90
    return-void

    #@d
    .line 88
    :catchall_d
    move-exception v0

    #@e
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@11
    throw v0
.end method
