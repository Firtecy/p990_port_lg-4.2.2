.class Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;
.super Ljava/lang/Object;
.source "HardwareRenderer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/HardwareRenderer$GlRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FunctorsRunnable"
.end annotation


# instance fields
.field attachInfo:Landroid/view/View$AttachInfo;

.field final synthetic this$0:Landroid/view/HardwareRenderer$GlRenderer;


# direct methods
.method constructor <init>(Landroid/view/HardwareRenderer$GlRenderer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1119
    iput-object p1, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->this$0:Landroid/view/HardwareRenderer$GlRenderer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 1124
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->attachInfo:Landroid/view/View$AttachInfo;

    #@2
    iget-object v0, v3, Landroid/view/View$AttachInfo;->mHardwareRenderer:Landroid/view/HardwareRenderer;

    #@4
    .line 1125
    .local v0, renderer:Landroid/view/HardwareRenderer;
    if-eqz v0, :cond_10

    #@6
    invoke-virtual {v0}, Landroid/view/HardwareRenderer;->isEnabled()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_10

    #@c
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->this$0:Landroid/view/HardwareRenderer$GlRenderer;

    #@e
    if-eq v0, v3, :cond_11

    #@10
    .line 1134
    :cond_10
    :goto_10
    return-void

    #@11
    .line 1129
    :cond_11
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->this$0:Landroid/view/HardwareRenderer$GlRenderer;

    #@13
    invoke-virtual {v3}, Landroid/view/HardwareRenderer$GlRenderer;->checkCurrent()I

    #@16
    move-result v2

    #@17
    .line 1130
    .local v2, surfaceState:I
    if-eqz v2, :cond_10

    #@19
    .line 1131
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->this$0:Landroid/view/HardwareRenderer$GlRenderer;

    #@1b
    iget-object v3, v3, Landroid/view/HardwareRenderer$GlRenderer;->mCanvas:Landroid/view/HardwareCanvas;

    #@1d
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->this$0:Landroid/view/HardwareRenderer$GlRenderer;

    #@1f
    invoke-static {v4}, Landroid/view/HardwareRenderer$GlRenderer;->access$000(Landroid/view/HardwareRenderer$GlRenderer;)Landroid/graphics/Rect;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v3, v4}, Landroid/view/HardwareCanvas;->invokeFunctors(Landroid/graphics/Rect;)I

    #@26
    move-result v1

    #@27
    .line 1132
    .local v1, status:I
    iget-object v3, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->this$0:Landroid/view/HardwareRenderer$GlRenderer;

    #@29
    iget-object v4, p0, Landroid/view/HardwareRenderer$GlRenderer$FunctorsRunnable;->attachInfo:Landroid/view/View$AttachInfo;

    #@2b
    invoke-static {v3, v4, v1}, Landroid/view/HardwareRenderer$GlRenderer;->access$100(Landroid/view/HardwareRenderer$GlRenderer;Landroid/view/View$AttachInfo;I)V

    #@2e
    goto :goto_10
.end method
