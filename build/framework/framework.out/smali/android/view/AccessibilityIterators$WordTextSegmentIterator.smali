.class Landroid/view/AccessibilityIterators$WordTextSegmentIterator;
.super Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;
.source "AccessibilityIterators.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/AccessibilityIterators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WordTextSegmentIterator"
.end annotation


# static fields
.field private static sInstance:Landroid/view/AccessibilityIterators$WordTextSegmentIterator;


# direct methods
.method private constructor <init>(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 178
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;-><init>(Ljava/util/Locale;Landroid/view/AccessibilityIterators$1;)V

    #@4
    .line 179
    return-void
.end method

.method public static getInstance(Ljava/util/Locale;)Landroid/view/AccessibilityIterators$WordTextSegmentIterator;
    .registers 2
    .parameter "locale"

    #@0
    .prologue
    .line 171
    sget-object v0, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$WordTextSegmentIterator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 172
    new-instance v0, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;

    #@6
    invoke-direct {v0, p0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;-><init>(Ljava/util/Locale;)V

    #@9
    sput-object v0, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$WordTextSegmentIterator;

    #@b
    .line 174
    :cond_b
    sget-object v0, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$WordTextSegmentIterator;

    #@d
    return-object v0
.end method

.method private isEndBoundary(I)Z
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 244
    if-lez p1, :cond_1a

    #@2
    add-int/lit8 v0, p1, -0x1

    #@4
    invoke-direct {p0, v0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isLetterOrDigit(I)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1a

    #@a
    iget-object v0, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@c
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@f
    move-result v0

    #@10
    if-eq p1, v0, :cond_18

    #@12
    invoke-direct {p0, p1}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isLetterOrDigit(I)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method private isLetterOrDigit(I)Z
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 249
    if-ltz p1, :cond_15

    #@2
    iget-object v1, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-ge p1, v1, :cond_15

    #@a
    .line 250
    iget-object v1, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/String;->codePointAt(I)I

    #@f
    move-result v0

    #@10
    .line 251
    .local v0, codePoint:I
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    #@13
    move-result v1

    #@14
    .line 253
    .end local v0           #codePoint:I
    :goto_14
    return v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method private isStartBoundary(I)Z
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 239
    invoke-direct {p0, p1}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isLetterOrDigit(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_12

    #@6
    if-eqz p1, :cond_10

    #@8
    add-int/lit8 v0, p1, -0x1

    #@a
    invoke-direct {p0, v0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isLetterOrDigit(I)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method


# virtual methods
.method public following(I)[I
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 188
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 189
    .local v2, textLegth:I
    if-gtz v2, :cond_b

    #@a
    .line 209
    :cond_a
    :goto_a
    return-object v3

    #@b
    .line 192
    :cond_b
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@d
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@10
    move-result v4

    #@11
    if-ge p1, v4, :cond_a

    #@13
    .line 195
    move v1, p1

    #@14
    .line 196
    .local v1, start:I
    if-gez v1, :cond_17

    #@16
    .line 197
    const/4 v1, 0x0

    #@17
    .line 199
    :cond_17
    invoke-direct {p0, v1}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isLetterOrDigit(I)Z

    #@1a
    move-result v4

    #@1b
    if-nez v4, :cond_2c

    #@1d
    invoke-direct {p0, v1}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isStartBoundary(I)Z

    #@20
    move-result v4

    #@21
    if-nez v4, :cond_2c

    #@23
    .line 200
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@25
    invoke-virtual {v4, v1}, Ljava/text/BreakIterator;->following(I)I

    #@28
    move-result v1

    #@29
    .line 201
    if-ne v1, v5, :cond_17

    #@2b
    goto :goto_a

    #@2c
    .line 205
    :cond_2c
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@2e
    invoke-virtual {v4, v1}, Ljava/text/BreakIterator;->following(I)I

    #@31
    move-result v0

    #@32
    .line 206
    .local v0, end:I
    if-eq v0, v5, :cond_a

    #@34
    invoke-direct {p0, v0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isEndBoundary(I)Z

    #@37
    move-result v4

    #@38
    if-eqz v4, :cond_a

    #@3a
    .line 209
    invoke-virtual {p0, v1, v0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->getRange(II)[I

    #@3d
    move-result-object v3

    #@3e
    goto :goto_a
.end method

.method protected onLocaleChanged(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 183
    invoke-static {p1}, Ljava/text/BreakIterator;->getWordInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@6
    .line 184
    return-void
.end method

.method public preceding(I)[I
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 214
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 215
    .local v2, textLegth:I
    if-gtz v2, :cond_b

    #@a
    .line 235
    :cond_a
    :goto_a
    return-object v3

    #@b
    .line 218
    :cond_b
    if-lez p1, :cond_a

    #@d
    .line 221
    move v0, p1

    #@e
    .line 222
    .local v0, end:I
    if-le v0, v2, :cond_11

    #@10
    .line 223
    move v0, v2

    #@11
    .line 225
    :cond_11
    if-lez v0, :cond_2a

    #@13
    add-int/lit8 v4, v0, -0x1

    #@15
    invoke-direct {p0, v4}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isLetterOrDigit(I)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_2a

    #@1b
    invoke-direct {p0, v0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isEndBoundary(I)Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_2a

    #@21
    .line 226
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@23
    invoke-virtual {v4, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@26
    move-result v0

    #@27
    .line 227
    if-ne v0, v5, :cond_11

    #@29
    goto :goto_a

    #@2a
    .line 231
    :cond_2a
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@2c
    invoke-virtual {v4, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@2f
    move-result v1

    #@30
    .line 232
    .local v1, start:I
    if-eq v1, v5, :cond_a

    #@32
    invoke-direct {p0, v1}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->isStartBoundary(I)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_a

    #@38
    .line 235
    invoke-virtual {p0, v1, v0}, Landroid/view/AccessibilityIterators$WordTextSegmentIterator;->getRange(II)[I

    #@3b
    move-result-object v3

    #@3c
    goto :goto_a
.end method
