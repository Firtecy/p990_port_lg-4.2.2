.class public final Landroid/view/KeyCharacterMap$FallbackAction;
.super Ljava/lang/Object;
.source "KeyCharacterMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/KeyCharacterMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FallbackAction"
.end annotation


# static fields
.field private static final MAX_RECYCLED:I = 0xa

.field private static sRecycleBin:Landroid/view/KeyCharacterMap$FallbackAction;

.field private static final sRecycleLock:Ljava/lang/Object;

.field private static sRecycledCount:I


# instance fields
.field public keyCode:I

.field public metaState:I

.field private next:Landroid/view/KeyCharacterMap$FallbackAction;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 731
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 740
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 741
    return-void
.end method

.method public static obtain()Landroid/view/KeyCharacterMap$FallbackAction;
    .registers 3

    #@0
    .prologue
    .line 745
    sget-object v2, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 746
    :try_start_3
    sget-object v1, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleBin:Landroid/view/KeyCharacterMap$FallbackAction;

    #@5
    if-nez v1, :cond_e

    #@7
    .line 747
    new-instance v0, Landroid/view/KeyCharacterMap$FallbackAction;

    #@9
    invoke-direct {v0}, Landroid/view/KeyCharacterMap$FallbackAction;-><init>()V

    #@c
    .line 754
    .local v0, target:Landroid/view/KeyCharacterMap$FallbackAction;
    :goto_c
    monitor-exit v2

    #@d
    .line 755
    return-object v0

    #@e
    .line 749
    .end local v0           #target:Landroid/view/KeyCharacterMap$FallbackAction;
    :cond_e
    sget-object v0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleBin:Landroid/view/KeyCharacterMap$FallbackAction;

    #@10
    .line 750
    .restart local v0       #target:Landroid/view/KeyCharacterMap$FallbackAction;
    iget-object v1, v0, Landroid/view/KeyCharacterMap$FallbackAction;->next:Landroid/view/KeyCharacterMap$FallbackAction;

    #@12
    sput-object v1, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleBin:Landroid/view/KeyCharacterMap$FallbackAction;

    #@14
    .line 751
    sget v1, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycledCount:I

    #@16
    add-int/lit8 v1, v1, -0x1

    #@18
    sput v1, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycledCount:I

    #@1a
    .line 752
    const/4 v1, 0x0

    #@1b
    iput-object v1, v0, Landroid/view/KeyCharacterMap$FallbackAction;->next:Landroid/view/KeyCharacterMap$FallbackAction;

    #@1d
    goto :goto_c

    #@1e
    .line 754
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit v2
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v1
.end method


# virtual methods
.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 759
    sget-object v1, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 760
    :try_start_3
    sget v0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycledCount:I

    #@5
    const/16 v2, 0xa

    #@7
    if-ge v0, v2, :cond_17

    #@9
    .line 761
    sget-object v0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleBin:Landroid/view/KeyCharacterMap$FallbackAction;

    #@b
    iput-object v0, p0, Landroid/view/KeyCharacterMap$FallbackAction;->next:Landroid/view/KeyCharacterMap$FallbackAction;

    #@d
    .line 762
    sput-object p0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycleBin:Landroid/view/KeyCharacterMap$FallbackAction;

    #@f
    .line 763
    sget v0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycledCount:I

    #@11
    add-int/lit8 v0, v0, 0x1

    #@13
    sput v0, Landroid/view/KeyCharacterMap$FallbackAction;->sRecycledCount:I

    #@15
    .line 767
    :goto_15
    monitor-exit v1

    #@16
    .line 768
    return-void

    #@17
    .line 765
    :cond_17
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Landroid/view/KeyCharacterMap$FallbackAction;->next:Landroid/view/KeyCharacterMap$FallbackAction;

    #@1a
    goto :goto_15

    #@1b
    .line 767
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method
