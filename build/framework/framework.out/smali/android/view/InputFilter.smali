.class public abstract Landroid/view/InputFilter;
.super Landroid/view/IInputFilter$Stub;
.source "InputFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/InputFilter$H;
    }
.end annotation


# static fields
.field private static final MSG_INPUT_EVENT:I = 0x3

.field private static final MSG_INSTALL:I = 0x1

.field private static final MSG_UNINSTALL:I = 0x2


# instance fields
.field private final mH:Landroid/view/InputFilter$H;

.field private mHost:Landroid/view/IInputFilterHost;

.field private final mInboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

.field private final mOutboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .registers 6
    .parameter "looper"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 126
    invoke-direct {p0}, Landroid/view/IInputFilter$Stub;-><init>()V

    #@5
    .line 106
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_2b

    #@b
    new-instance v0, Landroid/view/InputEventConsistencyVerifier;

    #@d
    const-string v2, "InputFilter#InboundInputEventConsistencyVerifier"

    #@f
    invoke-direct {v0, p0, v3, v2}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    #@12
    :goto_12
    iput-object v0, p0, Landroid/view/InputFilter;->mInboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@14
    .line 111
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_21

    #@1a
    new-instance v1, Landroid/view/InputEventConsistencyVerifier;

    #@1c
    const-string v0, "InputFilter#OutboundInputEventConsistencyVerifier"

    #@1e
    invoke-direct {v1, p0, v3, v0}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    #@21
    :cond_21
    iput-object v1, p0, Landroid/view/InputFilter;->mOutboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@23
    .line 127
    new-instance v0, Landroid/view/InputFilter$H;

    #@25
    invoke-direct {v0, p0, p1}, Landroid/view/InputFilter$H;-><init>(Landroid/view/InputFilter;Landroid/os/Looper;)V

    #@28
    iput-object v0, p0, Landroid/view/InputFilter;->mH:Landroid/view/InputFilter$H;

    #@2a
    .line 128
    return-void

    #@2b
    :cond_2b
    move-object v0, v1

    #@2c
    .line 106
    goto :goto_12
.end method

.method static synthetic access$002(Landroid/view/InputFilter;Landroid/view/IInputFilterHost;)Landroid/view/IInputFilterHost;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 100
    iput-object p1, p0, Landroid/view/InputFilter;->mHost:Landroid/view/IInputFilterHost;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Landroid/view/InputFilter;)Landroid/view/InputEventConsistencyVerifier;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/view/InputFilter;->mInboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/view/InputFilter;)Landroid/view/InputEventConsistencyVerifier;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/view/InputFilter;->mOutboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@2
    return-object v0
.end method


# virtual methods
.method public final filterInputEvent(Landroid/view/InputEvent;I)V
    .registers 6
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/view/InputFilter;->mH:Landroid/view/InputFilter$H;

    #@2
    const/4 v1, 0x3

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {v0, v1, p2, v2, p1}, Landroid/view/InputFilter$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@b
    .line 157
    return-void
.end method

.method public final install(Landroid/view/IInputFilterHost;)V
    .registers 4
    .parameter "host"

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/view/InputFilter;->mH:Landroid/view/InputFilter$H;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1, p1}, Landroid/view/InputFilter$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 138
    return-void
.end method

.method public onInputEvent(Landroid/view/InputEvent;I)V
    .registers 3
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 196
    invoke-virtual {p0, p1, p2}, Landroid/view/InputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V

    #@3
    .line 197
    return-void
.end method

.method public onInstalled()V
    .registers 1

    #@0
    .prologue
    .line 207
    return-void
.end method

.method public onUninstalled()V
    .registers 1

    #@0
    .prologue
    .line 217
    return-void
.end method

.method public sendInputEvent(Landroid/view/InputEvent;I)V
    .registers 5
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 166
    if-nez p1, :cond_a

    #@2
    .line 167
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "event must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 169
    :cond_a
    iget-object v0, p0, Landroid/view/InputFilter;->mHost:Landroid/view/IInputFilterHost;

    #@c
    if-nez v0, :cond_16

    #@e
    .line 170
    new-instance v0, Ljava/lang/IllegalStateException;

    #@10
    const-string v1, "Cannot send input event because the input filter is not installed."

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 173
    :cond_16
    iget-object v0, p0, Landroid/view/InputFilter;->mOutboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@18
    if-eqz v0, :cond_20

    #@1a
    .line 174
    iget-object v0, p0, Landroid/view/InputFilter;->mOutboundInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-virtual {v0, p1, v1}, Landroid/view/InputEventConsistencyVerifier;->onInputEvent(Landroid/view/InputEvent;I)V

    #@20
    .line 177
    :cond_20
    :try_start_20
    iget-object v0, p0, Landroid/view/InputFilter;->mHost:Landroid/view/IInputFilterHost;

    #@22
    invoke-interface {v0, p1, p2}, Landroid/view/IInputFilterHost;->sendInputEvent(Landroid/view/InputEvent;I)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_25} :catch_26

    #@25
    .line 181
    :goto_25
    return-void

    #@26
    .line 178
    :catch_26
    move-exception v0

    #@27
    goto :goto_25
.end method

.method public final uninstall()V
    .registers 3

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Landroid/view/InputFilter;->mH:Landroid/view/InputFilter$H;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Landroid/view/InputFilter$H;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 146
    return-void
.end method
