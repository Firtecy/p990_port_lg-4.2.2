.class public abstract Landroid/view/LayoutInflater;
.super Ljava/lang/Object;
.source "LayoutInflater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/LayoutInflater$BlinkLayout;,
        Landroid/view/LayoutInflater$FactoryMerger;,
        Landroid/view/LayoutInflater$Factory2;,
        Landroid/view/LayoutInflater$Factory;,
        Landroid/view/LayoutInflater$Filter;
    }
.end annotation


# static fields
.field private static final TAG_1995:Ljava/lang/String; = "blink"

.field private static final TAG_INCLUDE:Ljava/lang/String; = "include"

.field private static final TAG_MERGE:Ljava/lang/String; = "merge"

.field private static final TAG_REQUEST_FOCUS:Ljava/lang/String; = "requestFocus"

.field static final mConstructorSignature:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final sConstructorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Constructor",
            "<+",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final DEBUG:Z

.field final mConstructorArgs:[Ljava/lang/Object;

.field protected final mContext:Landroid/content/Context;

.field private mFactory:Landroid/view/LayoutInflater$Factory;

.field private mFactory2:Landroid/view/LayoutInflater$Factory2;

.field private mFactorySet:Z

.field private mFilter:Landroid/view/LayoutInflater$Filter;

.field private mFilterMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mPrivateFactory:Landroid/view/LayoutInflater$Factory2;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 80
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/Class;

    #@3
    const/4 v1, 0x0

    #@4
    const-class v2, Landroid/content/Context;

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-class v2, Landroid/util/AttributeSet;

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Landroid/view/LayoutInflater;->mConstructorSignature:[Ljava/lang/Class;

    #@f
    .line 83
    new-instance v0, Ljava/util/HashMap;

    #@11
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@14
    sput-object v0, Landroid/view/LayoutInflater;->sConstructorMap:Ljava/util/HashMap;

    #@16
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 186
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/view/LayoutInflater;->DEBUG:Z

    #@6
    .line 78
    const/4 v0, 0x2

    #@7
    new-array v0, v0, [Ljava/lang/Object;

    #@9
    iput-object v0, p0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@b
    .line 187
    iput-object p1, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@d
    .line 188
    return-void
.end method

.method protected constructor <init>(Landroid/view/LayoutInflater;Landroid/content/Context;)V
    .registers 4
    .parameter "original"
    .parameter "newContext"

    #@0
    .prologue
    .line 198
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/view/LayoutInflater;->DEBUG:Z

    #@6
    .line 78
    const/4 v0, 0x2

    #@7
    new-array v0, v0, [Ljava/lang/Object;

    #@9
    iput-object v0, p0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@b
    .line 199
    iput-object p2, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@d
    .line 200
    iget-object v0, p1, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@f
    iput-object v0, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@11
    .line 201
    iget-object v0, p1, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@13
    iput-object v0, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@15
    .line 202
    iget-object v0, p1, Landroid/view/LayoutInflater;->mPrivateFactory:Landroid/view/LayoutInflater$Factory2;

    #@17
    iput-object v0, p0, Landroid/view/LayoutInflater;->mPrivateFactory:Landroid/view/LayoutInflater$Factory2;

    #@19
    .line 203
    iget-object v0, p1, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@1b
    iput-object v0, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@1d
    .line 204
    return-void
.end method

.method private failNotAllowed(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "name"
    .parameter "prefix"
    .parameter "attrs"

    #@0
    .prologue
    .line 625
    new-instance v0, Landroid/view/InflateException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ": Class not allowed to be inflated "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    if-eqz p2, :cond_28

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object p1

    #@28
    .end local p1
    :cond_28
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@33
    throw v0
.end method

.method public static from(Landroid/content/Context;)Landroid/view/LayoutInflater;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 210
    const-string/jumbo v1, "layout_inflater"

    #@3
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/view/LayoutInflater;

    #@9
    .line 212
    .local v0, LayoutInflater:Landroid/view/LayoutInflater;
    if-nez v0, :cond_13

    #@b
    .line 213
    new-instance v1, Ljava/lang/AssertionError;

    #@d
    const-string v2, "LayoutInflater not found."

    #@f
    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@12
    throw v1

    #@13
    .line 215
    :cond_13
    return-object v0
.end method

.method private parseInclude(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;)V
    .registers 27
    .parameter "parser"
    .parameter "parent"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 773
    move-object/from16 v0, p2

    #@2
    instance-of v0, v0, Landroid/view/ViewGroup;

    #@4
    move/from16 v19, v0

    #@6
    if-eqz v19, :cond_181

    #@8
    .line 774
    const/16 v19, 0x0

    #@a
    const-string/jumbo v20, "layout"

    #@d
    const/16 v21, 0x0

    #@f
    move-object/from16 v0, p3

    #@11
    move-object/from16 v1, v19

    #@13
    move-object/from16 v2, v20

    #@15
    move/from16 v3, v21

    #@17
    invoke-interface {v0, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    #@1a
    move-result v13

    #@1b
    .line 775
    .local v13, layout:I
    if-nez v13, :cond_59

    #@1d
    .line 776
    const/16 v19, 0x0

    #@1f
    const-string/jumbo v20, "layout"

    #@22
    move-object/from16 v0, p3

    #@24
    move-object/from16 v1, v19

    #@26
    move-object/from16 v2, v20

    #@28
    invoke-interface {v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v16

    #@2c
    .line 777
    .local v16, value:Ljava/lang/String;
    if-nez v16, :cond_36

    #@2e
    .line 778
    new-instance v19, Landroid/view/InflateException;

    #@30
    const-string v20, "You must specifiy a layout in the include tag: <include layout=\"@layout/layoutID\" />"

    #@32
    invoke-direct/range {v19 .. v20}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@35
    throw v19

    #@36
    .line 781
    :cond_36
    new-instance v19, Landroid/view/InflateException;

    #@38
    new-instance v20, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v21, "You must specifiy a valid layout reference. The layout ID "

    #@3f
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v20

    #@43
    move-object/from16 v0, v20

    #@45
    move-object/from16 v1, v16

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v20

    #@4b
    const-string v21, " is not valid."

    #@4d
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v20

    #@51
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v20

    #@55
    invoke-direct/range {v19 .. v20}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@58
    throw v19

    #@59
    .line 785
    .end local v16           #value:Ljava/lang/String;
    :cond_59
    invoke-virtual/range {p0 .. p0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    #@5c
    move-result-object v19

    #@5d
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@60
    move-result-object v19

    #@61
    move-object/from16 v0, v19

    #@63
    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    #@66
    move-result-object v8

    #@67
    .line 789
    .local v8, childParser:Landroid/content/res/XmlResourceParser;
    :try_start_67
    invoke-static {v8}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@6a
    move-result-object v6

    #@6b
    .line 792
    .local v6, childAttrs:Landroid/util/AttributeSet;
    :cond_6b
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->next()I

    #@6e
    move-result v15

    #@6f
    .local v15, type:I
    const/16 v19, 0x2

    #@71
    move/from16 v0, v19

    #@73
    if-eq v15, v0, :cond_7b

    #@75
    const/16 v19, 0x1

    #@77
    move/from16 v0, v19

    #@79
    if-ne v15, v0, :cond_6b

    #@7b
    .line 796
    :cond_7b
    const/16 v19, 0x2

    #@7d
    move/from16 v0, v19

    #@7f
    if-eq v15, v0, :cond_a3

    #@81
    .line 797
    new-instance v19, Landroid/view/InflateException;

    #@83
    new-instance v20, Ljava/lang/StringBuilder;

    #@85
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    #@8b
    move-result-object v21

    #@8c
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v20

    #@90
    const-string v21, ": No start tag found!"

    #@92
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v20

    #@96
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v20

    #@9a
    invoke-direct/range {v19 .. v20}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@9d
    throw v19
    :try_end_9e
    .catchall {:try_start_67 .. :try_end_9e} :catchall_9e

    #@9e
    .line 860
    .end local v6           #childAttrs:Landroid/util/AttributeSet;
    .end local v15           #type:I
    :catchall_9e
    move-exception v19

    #@9f
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->close()V

    #@a2
    throw v19

    #@a3
    .line 801
    .restart local v6       #childAttrs:Landroid/util/AttributeSet;
    .restart local v15       #type:I
    :cond_a3
    :try_start_a3
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@a6
    move-result-object v7

    #@a7
    .line 803
    .local v7, childName:Ljava/lang/String;
    const-string/jumbo v19, "merge"

    #@aa
    move-object/from16 v0, v19

    #@ac
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@af
    move-result v19

    #@b0
    if-eqz v19, :cond_dd

    #@b2
    .line 805
    const/16 v19, 0x0

    #@b4
    move-object/from16 v0, p0

    #@b6
    move-object/from16 v1, p2

    #@b8
    move/from16 v2, v19

    #@ba
    invoke-virtual {v0, v8, v1, v6, v2}, Landroid/view/LayoutInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V
    :try_end_bd
    .catchall {:try_start_a3 .. :try_end_bd} :catchall_9e

    #@bd
    .line 860
    :goto_bd
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->close()V

    #@c0
    .line 867
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@c3
    move-result v9

    #@c4
    .line 869
    .local v9, currentDepth:I
    :cond_c4
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@c7
    move-result v15

    #@c8
    const/16 v19, 0x3

    #@ca
    move/from16 v0, v19

    #@cc
    if-ne v15, v0, :cond_d6

    #@ce
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@d1
    move-result v19

    #@d2
    move/from16 v0, v19

    #@d4
    if-le v0, v9, :cond_dc

    #@d6
    :cond_d6
    const/16 v19, 0x1

    #@d8
    move/from16 v0, v19

    #@da
    if-ne v15, v0, :cond_c4

    #@dc
    .line 872
    :cond_dc
    return-void

    #@dd
    .line 807
    .end local v9           #currentDepth:I
    :cond_dd
    :try_start_dd
    move-object/from16 v0, p0

    #@df
    move-object/from16 v1, p2

    #@e1
    invoke-virtual {v0, v1, v7, v6}, Landroid/view/LayoutInflater;->createViewFromTag(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    #@e4
    move-result-object v17

    #@e5
    .line 808
    .local v17, view:Landroid/view/View;
    move-object/from16 v0, p2

    #@e7
    check-cast v0, Landroid/view/ViewGroup;

    #@e9
    move-object v11, v0
    :try_end_ea
    .catchall {:try_start_dd .. :try_end_ea} :catchall_9e

    #@ea
    .line 818
    .local v11, group:Landroid/view/ViewGroup;
    const/4 v14, 0x0

    #@eb
    .line 820
    .local v14, params:Landroid/view/ViewGroup$LayoutParams;
    :try_start_eb
    move-object/from16 v0, p3

    #@ed
    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    :try_end_f0
    .catchall {:try_start_eb .. :try_end_f0} :catchall_15a
    .catch Ljava/lang/RuntimeException; {:try_start_eb .. :try_end_f0} :catch_14d

    #@f0
    move-result-object v14

    #@f1
    .line 824
    if-eqz v14, :cond_f8

    #@f3
    .line 825
    :try_start_f3
    move-object/from16 v0, v17

    #@f5
    invoke-virtual {v0, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@f8
    .line 830
    :cond_f8
    :goto_f8
    const/16 v19, 0x1

    #@fa
    move-object/from16 v0, p0

    #@fc
    move-object/from16 v1, v17

    #@fe
    move/from16 v2, v19

    #@100
    invoke-virtual {v0, v8, v1, v6, v2}, Landroid/view/LayoutInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V

    #@103
    .line 834
    move-object/from16 v0, p0

    #@105
    iget-object v0, v0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@107
    move-object/from16 v19, v0

    #@109
    sget-object v20, Lcom/android/internal/R$styleable;->View:[I

    #@10b
    const/16 v21, 0x0

    #@10d
    const/16 v22, 0x0

    #@10f
    move-object/from16 v0, v19

    #@111
    move-object/from16 v1, p3

    #@113
    move-object/from16 v2, v20

    #@115
    move/from16 v3, v21

    #@117
    move/from16 v4, v22

    #@119
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@11c
    move-result-object v5

    #@11d
    .line 836
    .local v5, a:Landroid/content/res/TypedArray;
    const/16 v19, 0x8

    #@11f
    const/16 v20, -0x1

    #@121
    move/from16 v0, v19

    #@123
    move/from16 v1, v20

    #@125
    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@128
    move-result v12

    #@129
    .line 838
    .local v12, id:I
    const/16 v19, 0x14

    #@12b
    const/16 v20, -0x1

    #@12d
    move/from16 v0, v19

    #@12f
    move/from16 v1, v20

    #@131
    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@134
    move-result v18

    #@135
    .line 839
    .local v18, visibility:I
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    #@138
    .line 841
    const/16 v19, -0x1

    #@13a
    move/from16 v0, v19

    #@13c
    if-eq v12, v0, :cond_143

    #@13e
    .line 842
    move-object/from16 v0, v17

    #@140
    invoke-virtual {v0, v12}, Landroid/view/View;->setId(I)V

    #@143
    .line 845
    :cond_143
    packed-switch v18, :pswitch_data_18a

    #@146
    .line 857
    :goto_146
    move-object/from16 v0, v17

    #@148
    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_14b
    .catchall {:try_start_f3 .. :try_end_14b} :catchall_9e

    #@14b
    goto/16 :goto_bd

    #@14d
    .line 821
    .end local v5           #a:Landroid/content/res/TypedArray;
    .end local v12           #id:I
    .end local v18           #visibility:I
    :catch_14d
    move-exception v10

    #@14e
    .line 822
    .local v10, e:Ljava/lang/RuntimeException;
    :try_start_14e
    invoke-virtual {v11, v6}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    :try_end_151
    .catchall {:try_start_14e .. :try_end_151} :catchall_15a

    #@151
    move-result-object v14

    #@152
    .line 824
    if-eqz v14, :cond_f8

    #@154
    .line 825
    :try_start_154
    move-object/from16 v0, v17

    #@156
    invoke-virtual {v0, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@159
    goto :goto_f8

    #@15a
    .line 824
    .end local v10           #e:Ljava/lang/RuntimeException;
    :catchall_15a
    move-exception v19

    #@15b
    if-eqz v14, :cond_162

    #@15d
    .line 825
    move-object/from16 v0, v17

    #@15f
    invoke-virtual {v0, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@162
    .line 824
    :cond_162
    throw v19

    #@163
    .line 847
    .restart local v5       #a:Landroid/content/res/TypedArray;
    .restart local v12       #id:I
    .restart local v18       #visibility:I
    :pswitch_163
    const/16 v19, 0x0

    #@165
    move-object/from16 v0, v17

    #@167
    move/from16 v1, v19

    #@169
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@16c
    goto :goto_146

    #@16d
    .line 850
    :pswitch_16d
    const/16 v19, 0x4

    #@16f
    move-object/from16 v0, v17

    #@171
    move/from16 v1, v19

    #@173
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@176
    goto :goto_146

    #@177
    .line 853
    :pswitch_177
    const/16 v19, 0x8

    #@179
    move-object/from16 v0, v17

    #@17b
    move/from16 v1, v19

    #@17d
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_180
    .catchall {:try_start_154 .. :try_end_180} :catchall_9e

    #@180
    goto :goto_146

    #@181
    .line 864
    .end local v5           #a:Landroid/content/res/TypedArray;
    .end local v6           #childAttrs:Landroid/util/AttributeSet;
    .end local v7           #childName:Ljava/lang/String;
    .end local v8           #childParser:Landroid/content/res/XmlResourceParser;
    .end local v11           #group:Landroid/view/ViewGroup;
    .end local v12           #id:I
    .end local v13           #layout:I
    .end local v14           #params:Landroid/view/ViewGroup$LayoutParams;
    .end local v15           #type:I
    .end local v17           #view:Landroid/view/View;
    .end local v18           #visibility:I
    :cond_181
    new-instance v19, Landroid/view/InflateException;

    #@183
    const-string v20, "<include /> can only be used inside of a ViewGroup"

    #@185
    invoke-direct/range {v19 .. v20}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@188
    throw v19

    #@189
    .line 845
    nop

    #@18a
    :pswitch_data_18a
    .packed-switch 0x0
        :pswitch_163
        :pswitch_16d
        :pswitch_177
    .end packed-switch
.end method

.method private parseRequestFocus(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;)V
    .registers 6
    .parameter "parser"
    .parameter "parent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 760
    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    #@3
    .line 761
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@6
    move-result v0

    #@7
    .line 763
    .local v0, currentDepth:I
    :cond_7
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@a
    move-result v1

    #@b
    .local v1, type:I
    const/4 v2, 0x3

    #@c
    if-ne v1, v2, :cond_14

    #@e
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@11
    move-result v2

    #@12
    if-le v2, v0, :cond_17

    #@14
    :cond_14
    const/4 v2, 0x1

    #@15
    if-ne v1, v2, :cond_7

    #@17
    .line 766
    :cond_17
    return-void
.end method


# virtual methods
.method public abstract cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;
.end method

.method public final createView(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 16
    .parameter "name"
    .parameter "prefix"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Landroid/view/InflateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 546
    sget-object v10, Landroid/view/LayoutInflater;->sConstructorMap:Ljava/util/HashMap;

    #@3
    invoke-virtual {v10, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v5

    #@7
    check-cast v5, Ljava/lang/reflect/Constructor;

    #@9
    .line 547
    .local v5, constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<+Landroid/view/View;>;"
    const/4 v4, 0x0

    #@a
    .line 550
    .local v4, clazz:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/view/View;>;"
    if-nez v5, :cond_64

    #@c
    .line 552
    :try_start_c
    iget-object v10, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v10}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v11

    #@12
    if-eqz p2, :cond_62

    #@14
    new-instance v10, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v10

    #@21
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v10

    #@25
    :goto_25
    invoke-virtual {v11, v10}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@28
    move-result-object v10

    #@29
    const-class v11, Landroid/view/View;

    #@2b
    invoke-virtual {v10, v11}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    #@2e
    move-result-object v4

    #@2f
    .line 555
    iget-object v10, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@31
    if-eqz v10, :cond_40

    #@33
    if-eqz v4, :cond_40

    #@35
    .line 556
    iget-object v10, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@37
    invoke-interface {v10, v4}, Landroid/view/LayoutInflater$Filter;->onLoadClass(Ljava/lang/Class;)Z

    #@3a
    move-result v1

    #@3b
    .line 557
    .local v1, allowed:Z
    if-nez v1, :cond_40

    #@3d
    .line 558
    invoke-direct {p0, p1, p2, p3}, Landroid/view/LayoutInflater;->failNotAllowed(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)V

    #@40
    .line 561
    .end local v1           #allowed:Z
    :cond_40
    sget-object v10, Landroid/view/LayoutInflater;->mConstructorSignature:[Ljava/lang/Class;

    #@42
    invoke-virtual {v4, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@45
    move-result-object v5

    #@46
    .line 562
    sget-object v10, Landroid/view/LayoutInflater;->sConstructorMap:Ljava/util/HashMap;

    #@48
    invoke-virtual {v10, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    .line 584
    :cond_4b
    :goto_4b
    iget-object v3, p0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@4d
    .line 585
    .local v3, args:[Ljava/lang/Object;
    const/4 v10, 0x1

    #@4e
    aput-object p3, v3, v10

    #@50
    .line 587
    invoke-virtual {v5, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    move-result-object v8

    #@54
    check-cast v8, Landroid/view/View;

    #@56
    .line 588
    .local v8, view:Landroid/view/View;
    instance-of v10, v8, Landroid/view/ViewStub;

    #@58
    if-eqz v10, :cond_61

    #@5a
    .line 590
    move-object v0, v8

    #@5b
    check-cast v0, Landroid/view/ViewStub;

    #@5d
    move-object v9, v0

    #@5e
    .line 591
    .local v9, viewStub:Landroid/view/ViewStub;
    invoke-virtual {v9, p0}, Landroid/view/ViewStub;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    #@61
    .line 593
    .end local v9           #viewStub:Landroid/view/ViewStub;
    :cond_61
    return-object v8

    #@62
    .end local v3           #args:[Ljava/lang/Object;
    .end local v8           #view:Landroid/view/View;
    :cond_62
    move-object v10, p1

    #@63
    .line 552
    goto :goto_25

    #@64
    .line 565
    :cond_64
    iget-object v10, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@66
    if-eqz v10, :cond_4b

    #@68
    .line 567
    iget-object v10, p0, Landroid/view/LayoutInflater;->mFilterMap:Ljava/util/HashMap;

    #@6a
    invoke-virtual {v10, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6d
    move-result-object v2

    #@6e
    check-cast v2, Ljava/lang/Boolean;

    #@70
    .line 568
    .local v2, allowedState:Ljava/lang/Boolean;
    if-nez v2, :cond_ea

    #@72
    .line 570
    iget-object v10, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@74
    invoke-virtual {v10}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@77
    move-result-object v11

    #@78
    if-eqz p2, :cond_e6

    #@7a
    new-instance v10, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v10

    #@83
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v10

    #@87
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v10

    #@8b
    :goto_8b
    invoke-virtual {v11, v10}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@8e
    move-result-object v10

    #@8f
    const-class v11, Landroid/view/View;

    #@91
    invoke-virtual {v10, v11}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    #@94
    move-result-object v4

    #@95
    .line 573
    if-eqz v4, :cond_e8

    #@97
    iget-object v10, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@99
    invoke-interface {v10, v4}, Landroid/view/LayoutInflater$Filter;->onLoadClass(Ljava/lang/Class;)Z

    #@9c
    move-result v10

    #@9d
    if-eqz v10, :cond_e8

    #@9f
    .line 574
    .restart local v1       #allowed:Z
    :goto_9f
    iget-object v10, p0, Landroid/view/LayoutInflater;->mFilterMap:Ljava/util/HashMap;

    #@a1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@a4
    move-result-object v11

    #@a5
    invoke-virtual {v10, p1, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a8
    .line 575
    if-nez v1, :cond_4b

    #@aa
    .line 576
    invoke-direct {p0, p1, p2, p3}, Landroid/view/LayoutInflater;->failNotAllowed(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)V
    :try_end_ad
    .catch Ljava/lang/NoSuchMethodException; {:try_start_c .. :try_end_ad} :catch_ae
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_ad} :catch_f7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_c .. :try_end_ad} :catch_12f
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_ad} :catch_131

    #@ad
    goto :goto_4b

    #@ae
    .line 595
    .end local v1           #allowed:Z
    .end local v2           #allowedState:Ljava/lang/Boolean;
    :catch_ae
    move-exception v6

    #@af
    .line 596
    .local v6, e:Ljava/lang/NoSuchMethodException;
    new-instance v7, Landroid/view/InflateException;

    #@b1
    new-instance v10, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@b9
    move-result-object v11

    #@ba
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v10

    #@be
    const-string v11, ": Error inflating class "

    #@c0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v10

    #@c4
    if-eqz p2, :cond_d7

    #@c6
    new-instance v11, Ljava/lang/StringBuilder;

    #@c8
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@cb
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v11

    #@cf
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v11

    #@d3
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object p1

    #@d7
    .end local p1
    :cond_d7
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v10

    #@db
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v10

    #@df
    invoke-direct {v7, v10}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@e2
    .line 599
    .local v7, ie:Landroid/view/InflateException;
    invoke-virtual {v7, v6}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@e5
    .line 600
    throw v7

    #@e6
    .end local v6           #e:Ljava/lang/NoSuchMethodException;
    .end local v7           #ie:Landroid/view/InflateException;
    .restart local v2       #allowedState:Ljava/lang/Boolean;
    .restart local p1
    :cond_e6
    move-object v10, p1

    #@e7
    .line 570
    goto :goto_8b

    #@e8
    .line 573
    :cond_e8
    const/4 v1, 0x0

    #@e9
    goto :goto_9f

    #@ea
    .line 578
    :cond_ea
    :try_start_ea
    sget-object v10, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    #@ec
    invoke-virtual {v2, v10}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v10

    #@f0
    if-eqz v10, :cond_4b

    #@f2
    .line 579
    invoke-direct {p0, p1, p2, p3}, Landroid/view/LayoutInflater;->failNotAllowed(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)V
    :try_end_f5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_ea .. :try_end_f5} :catch_ae
    .catch Ljava/lang/ClassCastException; {:try_start_ea .. :try_end_f5} :catch_f7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_ea .. :try_end_f5} :catch_12f
    .catch Ljava/lang/Exception; {:try_start_ea .. :try_end_f5} :catch_131

    #@f5
    goto/16 :goto_4b

    #@f7
    .line 602
    .end local v2           #allowedState:Ljava/lang/Boolean;
    :catch_f7
    move-exception v6

    #@f8
    .line 604
    .local v6, e:Ljava/lang/ClassCastException;
    new-instance v7, Landroid/view/InflateException;

    #@fa
    new-instance v10, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@102
    move-result-object v11

    #@103
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v10

    #@107
    const-string v11, ": Class is not a View "

    #@109
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v10

    #@10d
    if-eqz p2, :cond_120

    #@10f
    new-instance v11, Ljava/lang/StringBuilder;

    #@111
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v11

    #@118
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v11

    #@11c
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object p1

    #@120
    .end local p1
    :cond_120
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v10

    #@124
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v10

    #@128
    invoke-direct {v7, v10}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@12b
    .line 607
    .restart local v7       #ie:Landroid/view/InflateException;
    invoke-virtual {v7, v6}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@12e
    .line 608
    throw v7

    #@12f
    .line 609
    .end local v6           #e:Ljava/lang/ClassCastException;
    .end local v7           #ie:Landroid/view/InflateException;
    .restart local p1
    :catch_12f
    move-exception v6

    #@130
    .line 611
    .local v6, e:Ljava/lang/ClassNotFoundException;
    throw v6

    #@131
    .line 612
    .end local v6           #e:Ljava/lang/ClassNotFoundException;
    :catch_131
    move-exception v6

    #@132
    .line 613
    .local v6, e:Ljava/lang/Exception;
    new-instance v7, Landroid/view/InflateException;

    #@134
    new-instance v10, Ljava/lang/StringBuilder;

    #@136
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@13c
    move-result-object v11

    #@13d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v10

    #@141
    const-string v11, ": Error inflating class "

    #@143
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v11

    #@147
    if-nez v4, :cond_15a

    #@149
    const-string v10, "<unknown>"

    #@14b
    :goto_14b
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v10

    #@14f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@152
    move-result-object v10

    #@153
    invoke-direct {v7, v10}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@156
    .line 616
    .restart local v7       #ie:Landroid/view/InflateException;
    invoke-virtual {v7, v6}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@159
    .line 617
    throw v7

    #@15a
    .line 613
    .end local v7           #ie:Landroid/view/InflateException;
    :cond_15a
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@15d
    move-result-object v10

    #@15e
    goto :goto_14b
.end method

.method createViewFromTag(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 9
    .parameter "parent"
    .parameter "name"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 667
    const-string/jumbo v3, "view"

    #@4
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_10

    #@a
    .line 668
    const-string v3, "class"

    #@c
    invoke-interface {p3, v4, v3}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object p2

    #@10
    .line 675
    :cond_10
    :try_start_10
    iget-object v3, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@12
    if-eqz v3, :cond_3a

    #@14
    iget-object v3, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@16
    iget-object v4, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@18
    invoke-interface {v3, p1, p2, v4, p3}, Landroid/view/LayoutInflater$Factory2;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    #@1b
    move-result-object v2

    #@1c
    .line 679
    .local v2, view:Landroid/view/View;
    :goto_1c
    if-nez v2, :cond_2a

    #@1e
    iget-object v3, p0, Landroid/view/LayoutInflater;->mPrivateFactory:Landroid/view/LayoutInflater$Factory2;

    #@20
    if-eqz v3, :cond_2a

    #@22
    .line 680
    iget-object v3, p0, Landroid/view/LayoutInflater;->mPrivateFactory:Landroid/view/LayoutInflater$Factory2;

    #@24
    iget-object v4, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@26
    invoke-interface {v3, p1, p2, v4, p3}, Landroid/view/LayoutInflater$Factory2;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    #@29
    move-result-object v2

    #@2a
    .line 683
    :cond_2a
    if-nez v2, :cond_39

    #@2c
    .line 684
    const/4 v3, -0x1

    #@2d
    const/16 v4, 0x2e

    #@2f
    invoke-virtual {p2, v4}, Ljava/lang/String;->indexOf(I)I

    #@32
    move-result v4

    #@33
    if-ne v3, v4, :cond_49

    #@35
    .line 685
    invoke-virtual {p0, p1, p2, p3}, Landroid/view/LayoutInflater;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    #@38
    move-result-object v2

    #@39
    .line 692
    :cond_39
    :goto_39
    return-object v2

    #@3a
    .line 676
    .end local v2           #view:Landroid/view/View;
    :cond_3a
    iget-object v3, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@3c
    if-eqz v3, :cond_47

    #@3e
    iget-object v3, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@40
    iget-object v4, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@42
    invoke-interface {v3, p2, v4, p3}, Landroid/view/LayoutInflater$Factory;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    #@45
    move-result-object v2

    #@46
    .restart local v2       #view:Landroid/view/View;
    goto :goto_1c

    #@47
    .line 677
    .end local v2           #view:Landroid/view/View;
    :cond_47
    const/4 v2, 0x0

    #@48
    .restart local v2       #view:Landroid/view/View;
    goto :goto_1c

    #@49
    .line 687
    :cond_49
    const/4 v3, 0x0

    #@4a
    invoke-virtual {p0, p2, v3, p3}, Landroid/view/LayoutInflater;->createView(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    :try_end_4d
    .catch Landroid/view/InflateException; {:try_start_10 .. :try_end_4d} :catch_4f
    .catch Ljava/lang/ClassNotFoundException; {:try_start_10 .. :try_end_4d} :catch_51
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_4d} :catch_76

    #@4d
    move-result-object v2

    #@4e
    goto :goto_39

    #@4f
    .line 694
    .end local v2           #view:Landroid/view/View;
    :catch_4f
    move-exception v0

    #@50
    .line 695
    .local v0, e:Landroid/view/InflateException;
    throw v0

    #@51
    .line 697
    .end local v0           #e:Landroid/view/InflateException;
    :catch_51
    move-exception v0

    #@52
    .line 698
    .local v0, e:Ljava/lang/ClassNotFoundException;
    new-instance v1, Landroid/view/InflateException;

    #@54
    new-instance v3, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    const-string v4, ": Error inflating class "

    #@63
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-direct {v1, v3}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@72
    .line 700
    .local v1, ie:Landroid/view/InflateException;
    invoke-virtual {v1, v0}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@75
    .line 701
    throw v1

    #@76
    .line 703
    .end local v0           #e:Ljava/lang/ClassNotFoundException;
    .end local v1           #ie:Landroid/view/InflateException;
    :catch_76
    move-exception v0

    #@77
    .line 704
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Landroid/view/InflateException;

    #@79
    new-instance v3, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    const-string v4, ": Error inflating class "

    #@88
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v3

    #@94
    invoke-direct {v1, v3}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@97
    .line 706
    .restart local v1       #ie:Landroid/view/InflateException;
    invoke-virtual {v1, v0}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@9a
    .line 707
    throw v1
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public final getFactory()Landroid/view/LayoutInflater$Factory;
    .registers 2

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@2
    return-object v0
.end method

.method public final getFactory2()Landroid/view/LayoutInflater$Factory2;
    .registers 2

    #@0
    .prologue
    .line 257
    iget-object v0, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@2
    return-object v0
.end method

.method public getFilter()Landroid/view/LayoutInflater$Filter;
    .registers 2

    #@0
    .prologue
    .line 321
    iget-object v0, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@2
    return-object v0
.end method

.method public inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 4
    .parameter "resource"
    .parameter "root"

    #@0
    .prologue
    .line 352
    if-eqz p2, :cond_8

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    return-object v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method public inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    .registers 6
    .parameter "resource"
    .parameter "root"
    .parameter "attachToRoot"

    #@0
    .prologue
    .line 394
    invoke-virtual {p0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    #@b
    move-result-object v0

    #@c
    .line 396
    .local v0, parser:Landroid/content/res/XmlResourceParser;
    :try_start_c
    invoke-virtual {p0, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_14

    #@f
    move-result-object v1

    #@10
    .line 398
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    #@13
    .line 396
    return-object v1

    #@14
    .line 398
    :catchall_14
    move-exception v1

    #@15
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    #@18
    throw v1
.end method

.method public inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 4
    .parameter "parser"
    .parameter "root"

    #@0
    .prologue
    .line 372
    if-eqz p2, :cond_8

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/LayoutInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;Z)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    return-object v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method public inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .registers 21
    .parameter "parser"
    .parameter "root"
    .parameter "attachToRoot"

    #@0
    .prologue
    .line 425
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@4
    monitor-enter v13

    #@5
    .line 426
    :try_start_5
    invoke-static/range {p1 .. p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@8
    move-result-object v3

    #@9
    .line 427
    .local v3, attrs:Landroid/util/AttributeSet;
    move-object/from16 v0, p0

    #@b
    iget-object v12, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@d
    const/4 v14, 0x0

    #@e
    aget-object v6, v12, v14

    #@10
    check-cast v6, Landroid/content/Context;

    #@12
    .line 428
    .local v6, lastContext:Landroid/content/Context;
    move-object/from16 v0, p0

    #@14
    iget-object v12, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@16
    const/4 v14, 0x0

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v15, v0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@1b
    aput-object v15, v12, v14
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_69

    #@1d
    .line 429
    move-object/from16 v9, p2

    #@1f
    .line 435
    .local v9, result:Landroid/view/View;
    :cond_1f
    :try_start_1f
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@22
    move-result v11

    #@23
    .local v11, type:I
    const/4 v12, 0x2

    #@24
    if-eq v11, v12, :cond_29

    #@26
    const/4 v12, 0x1

    #@27
    if-ne v11, v12, :cond_1f

    #@29
    .line 439
    :cond_29
    const/4 v12, 0x2

    #@2a
    if-eq v11, v12, :cond_6c

    #@2c
    .line 440
    new-instance v12, Landroid/view/InflateException;

    #@2e
    new-instance v14, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@36
    move-result-object v15

    #@37
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v14

    #@3b
    const-string v15, ": No start tag found!"

    #@3d
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v14

    #@41
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v14

    #@45
    invoke-direct {v12, v14}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@48
    throw v12
    :try_end_49
    .catchall {:try_start_1f .. :try_end_49} :catchall_57
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1f .. :try_end_49} :catch_49
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_49} :catch_85

    #@49
    .line 507
    .end local v11           #type:I
    :catch_49
    move-exception v4

    #@4a
    .line 508
    .local v4, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_4a
    new-instance v5, Landroid/view/InflateException;

    #@4c
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    #@4f
    move-result-object v12

    #@50
    invoke-direct {v5, v12}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@53
    .line 509
    .local v5, ex:Landroid/view/InflateException;
    invoke-virtual {v5, v4}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@56
    .line 510
    throw v5
    :try_end_57
    .catchall {:try_start_4a .. :try_end_57} :catchall_57

    #@57
    .line 519
    .end local v4           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .end local v5           #ex:Landroid/view/InflateException;
    :catchall_57
    move-exception v12

    #@58
    :try_start_58
    move-object/from16 v0, p0

    #@5a
    iget-object v14, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@5c
    const/4 v15, 0x0

    #@5d
    aput-object v6, v14, v15

    #@5f
    .line 520
    move-object/from16 v0, p0

    #@61
    iget-object v14, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@63
    const/4 v15, 0x1

    #@64
    const/16 v16, 0x0

    #@66
    aput-object v16, v14, v15

    #@68
    .line 519
    throw v12

    #@69
    .line 524
    .end local v3           #attrs:Landroid/util/AttributeSet;
    .end local v6           #lastContext:Landroid/content/Context;
    .end local v9           #result:Landroid/view/View;
    :catchall_69
    move-exception v12

    #@6a
    monitor-exit v13
    :try_end_6b
    .catchall {:try_start_58 .. :try_end_6b} :catchall_69

    #@6b
    throw v12

    #@6c
    .line 444
    .restart local v3       #attrs:Landroid/util/AttributeSet;
    .restart local v6       #lastContext:Landroid/content/Context;
    .restart local v9       #result:Landroid/view/View;
    .restart local v11       #type:I
    :cond_6c
    :try_start_6c
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@6f
    move-result-object v7

    #@70
    .line 453
    .local v7, name:Ljava/lang/String;
    const-string/jumbo v12, "merge"

    #@73
    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v12

    #@77
    if-eqz v12, :cond_c9

    #@79
    .line 454
    if-eqz p2, :cond_7d

    #@7b
    if-nez p3, :cond_ae

    #@7d
    .line 455
    :cond_7d
    new-instance v12, Landroid/view/InflateException;

    #@7f
    const-string v14, "<merge /> can be used only with a valid ViewGroup root and attachToRoot=true"

    #@81
    invoke-direct {v12, v14}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@84
    throw v12
    :try_end_85
    .catchall {:try_start_6c .. :try_end_85} :catchall_57
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6c .. :try_end_85} :catch_49
    .catch Ljava/io/IOException; {:try_start_6c .. :try_end_85} :catch_85

    #@85
    .line 511
    .end local v7           #name:Ljava/lang/String;
    .end local v11           #type:I
    :catch_85
    move-exception v4

    #@86
    .line 512
    .local v4, e:Ljava/io/IOException;
    :try_start_86
    new-instance v5, Landroid/view/InflateException;

    #@88
    new-instance v12, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@90
    move-result-object v14

    #@91
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v12

    #@95
    const-string v14, ": "

    #@97
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v12

    #@9b
    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@9e
    move-result-object v14

    #@9f
    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v12

    #@a3
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v12

    #@a7
    invoke-direct {v5, v12}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@aa
    .line 515
    .restart local v5       #ex:Landroid/view/InflateException;
    invoke-virtual {v5, v4}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@ad
    .line 516
    throw v5
    :try_end_ae
    .catchall {:try_start_86 .. :try_end_ae} :catchall_57

    #@ae
    .line 459
    .end local v4           #e:Ljava/io/IOException;
    .end local v5           #ex:Landroid/view/InflateException;
    .restart local v7       #name:Ljava/lang/String;
    .restart local v11       #type:I
    :cond_ae
    const/4 v12, 0x0

    #@af
    :try_start_af
    move-object/from16 v0, p0

    #@b1
    move-object/from16 v1, p1

    #@b3
    move-object/from16 v2, p2

    #@b5
    invoke-virtual {v0, v1, v2, v3, v12}, Landroid/view/LayoutInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V
    :try_end_b8
    .catchall {:try_start_af .. :try_end_b8} :catchall_57
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_af .. :try_end_b8} :catch_49
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_b8} :catch_85

    #@b8
    .line 519
    :cond_b8
    :goto_b8
    :try_start_b8
    move-object/from16 v0, p0

    #@ba
    iget-object v12, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@bc
    const/4 v14, 0x0

    #@bd
    aput-object v6, v12, v14

    #@bf
    .line 520
    move-object/from16 v0, p0

    #@c1
    iget-object v12, v0, Landroid/view/LayoutInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@c3
    const/4 v14, 0x1

    #@c4
    const/4 v15, 0x0

    #@c5
    aput-object v15, v12, v14

    #@c7
    .line 523
    monitor-exit v13
    :try_end_c8
    .catchall {:try_start_b8 .. :try_end_c8} :catchall_69

    #@c8
    return-object v9

    #@c9
    .line 463
    :cond_c9
    :try_start_c9
    const-string v12, "blink"

    #@cb
    invoke-virtual {v12, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v12

    #@cf
    if-eqz v12, :cond_ff

    #@d1
    .line 464
    new-instance v10, Landroid/view/LayoutInflater$BlinkLayout;

    #@d3
    move-object/from16 v0, p0

    #@d5
    iget-object v12, v0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@d7
    invoke-direct {v10, v12, v3}, Landroid/view/LayoutInflater$BlinkLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@da
    .line 469
    .local v10, temp:Landroid/view/View;
    :goto_da
    const/4 v8, 0x0

    #@db
    .line 471
    .local v8, params:Landroid/view/ViewGroup$LayoutParams;
    if-eqz p2, :cond_e8

    #@dd
    .line 477
    move-object/from16 v0, p2

    #@df
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    #@e2
    move-result-object v8

    #@e3
    .line 478
    if-nez p3, :cond_e8

    #@e5
    .line 481
    invoke-virtual {v10, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@e8
    .line 489
    :cond_e8
    const/4 v12, 0x1

    #@e9
    move-object/from16 v0, p0

    #@eb
    move-object/from16 v1, p1

    #@ed
    invoke-virtual {v0, v1, v10, v3, v12}, Landroid/view/LayoutInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V

    #@f0
    .line 496
    if-eqz p2, :cond_f9

    #@f2
    if-eqz p3, :cond_f9

    #@f4
    .line 497
    move-object/from16 v0, p2

    #@f6
    invoke-virtual {v0, v10, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@f9
    .line 502
    :cond_f9
    if-eqz p2, :cond_fd

    #@fb
    if-nez p3, :cond_b8

    #@fd
    .line 503
    :cond_fd
    move-object v9, v10

    #@fe
    goto :goto_b8

    #@ff
    .line 466
    .end local v8           #params:Landroid/view/ViewGroup$LayoutParams;
    .end local v10           #temp:Landroid/view/View;
    :cond_ff
    move-object/from16 v0, p0

    #@101
    move-object/from16 v1, p2

    #@103
    invoke-virtual {v0, v1, v7, v3}, Landroid/view/LayoutInflater;->createViewFromTag(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    :try_end_106
    .catchall {:try_start_c9 .. :try_end_106} :catchall_57
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c9 .. :try_end_106} :catch_49
    .catch Ljava/io/IOException; {:try_start_c9 .. :try_end_106} :catch_85

    #@106
    move-result-object v10

    #@107
    .restart local v10       #temp:Landroid/view/View;
    goto :goto_da
.end method

.method protected onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 5
    .parameter "parent"
    .parameter "name"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 660
    invoke-virtual {p0, p2, p3}, Landroid/view/LayoutInflater;->onCreateView(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected onCreateView(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 4
    .parameter "name"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 643
    const-string v0, "android.view."

    #@2
    invoke-virtual {p0, p1, v0, p2}, Landroid/view/LayoutInflater;->createView(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V
    .registers 13
    .parameter "parser"
    .parameter "parent"
    .parameter "attrs"
    .parameter "finishInflate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 718
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@4
    move-result v0

    #@5
    .line 722
    .local v0, depth:I
    :cond_5
    :goto_5
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@8
    move-result v3

    #@9
    .local v3, type:I
    const/4 v6, 0x3

    #@a
    if-ne v3, v6, :cond_12

    #@c
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@f
    move-result v6

    #@10
    if-le v6, v0, :cond_82

    #@12
    :cond_12
    if-eq v3, v7, :cond_82

    #@14
    .line 724
    const/4 v6, 0x2

    #@15
    if-ne v3, v6, :cond_5

    #@17
    .line 728
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 730
    .local v1, name:Ljava/lang/String;
    const-string/jumbo v6, "requestFocus"

    #@1e
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v6

    #@22
    if-eqz v6, :cond_28

    #@24
    .line 731
    invoke-direct {p0, p1, p2}, Landroid/view/LayoutInflater;->parseRequestFocus(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;)V

    #@27
    goto :goto_5

    #@28
    .line 732
    :cond_28
    const-string v6, "include"

    #@2a
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v6

    #@2e
    if-eqz v6, :cond_42

    #@30
    .line 733
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@33
    move-result v6

    #@34
    if-nez v6, :cond_3e

    #@36
    .line 734
    new-instance v6, Landroid/view/InflateException;

    #@38
    const-string v7, "<include /> cannot be the root element"

    #@3a
    invoke-direct {v6, v7}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v6

    #@3e
    .line 736
    :cond_3e
    invoke-direct {p0, p1, p2, p3}, Landroid/view/LayoutInflater;->parseInclude(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;)V

    #@41
    goto :goto_5

    #@42
    .line 737
    :cond_42
    const-string/jumbo v6, "merge"

    #@45
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v6

    #@49
    if-eqz v6, :cond_53

    #@4b
    .line 738
    new-instance v6, Landroid/view/InflateException;

    #@4d
    const-string v7, "<merge /> must be the root element"

    #@4f
    invoke-direct {v6, v7}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@52
    throw v6

    #@53
    .line 739
    :cond_53
    const-string v6, "blink"

    #@55
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v6

    #@59
    if-eqz v6, :cond_70

    #@5b
    .line 740
    new-instance v4, Landroid/view/LayoutInflater$BlinkLayout;

    #@5d
    iget-object v6, p0, Landroid/view/LayoutInflater;->mContext:Landroid/content/Context;

    #@5f
    invoke-direct {v4, v6, p3}, Landroid/view/LayoutInflater$BlinkLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@62
    .local v4, view:Landroid/view/View;
    move-object v5, p2

    #@63
    .line 741
    check-cast v5, Landroid/view/ViewGroup;

    #@65
    .line 742
    .local v5, viewGroup:Landroid/view/ViewGroup;
    invoke-virtual {v5, p3}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    #@68
    move-result-object v2

    #@69
    .line 743
    .local v2, params:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, p1, v4, p3, v7}, Landroid/view/LayoutInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V

    #@6c
    .line 744
    invoke-virtual {v5, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@6f
    goto :goto_5

    #@70
    .line 746
    .end local v2           #params:Landroid/view/ViewGroup$LayoutParams;
    .end local v4           #view:Landroid/view/View;
    .end local v5           #viewGroup:Landroid/view/ViewGroup;
    :cond_70
    invoke-virtual {p0, p2, v1, p3}, Landroid/view/LayoutInflater;->createViewFromTag(Landroid/view/View;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    #@73
    move-result-object v4

    #@74
    .restart local v4       #view:Landroid/view/View;
    move-object v5, p2

    #@75
    .line 747
    check-cast v5, Landroid/view/ViewGroup;

    #@77
    .line 748
    .restart local v5       #viewGroup:Landroid/view/ViewGroup;
    invoke-virtual {v5, p3}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    #@7a
    move-result-object v2

    #@7b
    .line 749
    .restart local v2       #params:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, p1, v4, p3, v7}, Landroid/view/LayoutInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V

    #@7e
    .line 750
    invoke-virtual {v5, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@81
    goto :goto_5

    #@82
    .line 754
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #params:Landroid/view/ViewGroup$LayoutParams;
    .end local v4           #view:Landroid/view/View;
    .end local v5           #viewGroup:Landroid/view/ViewGroup;
    :cond_82
    if-eqz p4, :cond_87

    #@84
    invoke-virtual {p2}, Landroid/view/View;->onFinishInflate()V

    #@87
    .line 755
    :cond_87
    return-void
.end method

.method public setFactory(Landroid/view/LayoutInflater$Factory;)V
    .registers 6
    .parameter "factory"

    #@0
    .prologue
    .line 276
    iget-boolean v0, p0, Landroid/view/LayoutInflater;->mFactorySet:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 277
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "A factory has already been set on this LayoutInflater"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 279
    :cond_c
    if-nez p1, :cond_16

    #@e
    .line 280
    new-instance v0, Ljava/lang/NullPointerException;

    #@10
    const-string v1, "Given factory can not be null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 282
    :cond_16
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Landroid/view/LayoutInflater;->mFactorySet:Z

    #@19
    .line 283
    iget-object v0, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@1b
    if-nez v0, :cond_20

    #@1d
    .line 284
    iput-object p1, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@1f
    .line 288
    :goto_1f
    return-void

    #@20
    .line 286
    :cond_20
    new-instance v0, Landroid/view/LayoutInflater$FactoryMerger;

    #@22
    const/4 v1, 0x0

    #@23
    iget-object v2, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@25
    iget-object v3, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@27
    invoke-direct {v0, p1, v1, v2, v3}, Landroid/view/LayoutInflater$FactoryMerger;-><init>(Landroid/view/LayoutInflater$Factory;Landroid/view/LayoutInflater$Factory2;Landroid/view/LayoutInflater$Factory;Landroid/view/LayoutInflater$Factory2;)V

    #@2a
    iput-object v0, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@2c
    goto :goto_1f
.end method

.method public setFactory2(Landroid/view/LayoutInflater$Factory2;)V
    .registers 5
    .parameter "factory"

    #@0
    .prologue
    .line 295
    iget-boolean v0, p0, Landroid/view/LayoutInflater;->mFactorySet:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 296
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "A factory has already been set on this LayoutInflater"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 298
    :cond_c
    if-nez p1, :cond_16

    #@e
    .line 299
    new-instance v0, Ljava/lang/NullPointerException;

    #@10
    const-string v1, "Given factory can not be null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 301
    :cond_16
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Landroid/view/LayoutInflater;->mFactorySet:Z

    #@19
    .line 302
    iget-object v0, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@1b
    if-nez v0, :cond_22

    #@1d
    .line 303
    iput-object p1, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@1f
    iput-object p1, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@21
    .line 307
    :goto_21
    return-void

    #@22
    .line 305
    :cond_22
    new-instance v0, Landroid/view/LayoutInflater$FactoryMerger;

    #@24
    iget-object v1, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@26
    iget-object v2, p0, Landroid/view/LayoutInflater;->mFactory2:Landroid/view/LayoutInflater$Factory2;

    #@28
    invoke-direct {v0, p1, p1, v1, v2}, Landroid/view/LayoutInflater$FactoryMerger;-><init>(Landroid/view/LayoutInflater$Factory;Landroid/view/LayoutInflater$Factory2;Landroid/view/LayoutInflater$Factory;Landroid/view/LayoutInflater$Factory2;)V

    #@2b
    iput-object v0, p0, Landroid/view/LayoutInflater;->mFactory:Landroid/view/LayoutInflater$Factory;

    #@2d
    goto :goto_21
.end method

.method public setFilter(Landroid/view/LayoutInflater$Filter;)V
    .registers 3
    .parameter "filter"

    #@0
    .prologue
    .line 334
    iput-object p1, p0, Landroid/view/LayoutInflater;->mFilter:Landroid/view/LayoutInflater$Filter;

    #@2
    .line 335
    if-eqz p1, :cond_b

    #@4
    .line 336
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/LayoutInflater;->mFilterMap:Ljava/util/HashMap;

    #@b
    .line 338
    :cond_b
    return-void
.end method

.method public setPrivateFactory(Landroid/view/LayoutInflater$Factory2;)V
    .registers 2
    .parameter "factory"

    #@0
    .prologue
    .line 313
    iput-object p1, p0, Landroid/view/LayoutInflater;->mPrivateFactory:Landroid/view/LayoutInflater$Factory2;

    #@2
    .line 314
    return-void
.end method
