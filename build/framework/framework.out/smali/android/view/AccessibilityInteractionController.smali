.class final Landroid/view/AccessibilityInteractionController;
.super Ljava/lang/Object;
.source "AccessibilityInteractionController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/AccessibilityInteractionController$1;,
        Landroid/view/AccessibilityInteractionController$PrivateHandler;,
        Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mMyLooperThreadId:J

.field private final mMyProcessId:I

.field private final mPrefetcher:Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;

.field private mTempAccessibilityNodeInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mViewRootImpl:Landroid/view/ViewRootImpl;


# direct methods
.method public constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 5
    .parameter "viewRootImpl"

    #@0
    .prologue
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    new-instance v1, Ljava/util/ArrayList;

    #@5
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v1, p0, Landroid/view/AccessibilityInteractionController;->mTempAccessibilityNodeInfoList:Ljava/util/ArrayList;

    #@a
    .line 63
    new-instance v1, Ljava/util/ArrayList;

    #@c
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v1, p0, Landroid/view/AccessibilityInteractionController;->mTempArrayList:Ljava/util/ArrayList;

    #@11
    .line 65
    new-instance v1, Landroid/graphics/Rect;

    #@13
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@16
    iput-object v1, p0, Landroid/view/AccessibilityInteractionController;->mTempRect:Landroid/graphics/Rect;

    #@18
    .line 68
    iget-object v1, p1, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    #@1a
    invoke-virtual {v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->getLooper()Landroid/os/Looper;

    #@1d
    move-result-object v0

    #@1e
    .line 69
    .local v0, looper:Landroid/os/Looper;
    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    #@25
    move-result-wide v1

    #@26
    iput-wide v1, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@28
    .line 70
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@2b
    move-result v1

    #@2c
    iput v1, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@2e
    .line 71
    new-instance v1, Landroid/view/AccessibilityInteractionController$PrivateHandler;

    #@30
    invoke-direct {v1, p0, v0}, Landroid/view/AccessibilityInteractionController$PrivateHandler;-><init>(Landroid/view/AccessibilityInteractionController;Landroid/os/Looper;)V

    #@33
    iput-object v1, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@35
    .line 72
    iput-object p1, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@37
    .line 73
    new-instance v1, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;

    #@39
    const/4 v2, 0x0

    #@3a
    invoke-direct {v1, p0, v2}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;-><init>(Landroid/view/AccessibilityInteractionController;Landroid/view/AccessibilityInteractionController$1;)V

    #@3d
    iput-object v1, p0, Landroid/view/AccessibilityInteractionController;->mPrefetcher:Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;

    #@3f
    .line 74
    return-void
.end method

.method static synthetic access$100(Landroid/view/AccessibilityInteractionController;Landroid/view/View;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Landroid/view/AccessibilityInteractionController;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->findAccessibilityNodeInfoByAccessibilityIdUiThread(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/view/AccessibilityInteractionController;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->perfromAccessibilityActionUiThread(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/view/AccessibilityInteractionController;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->findAccessibilityNodeInfoByViewIdUiThread(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/view/AccessibilityInteractionController;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->findAccessibilityNodeInfosByTextUiThread(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/view/AccessibilityInteractionController;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->findFocusUiThread(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/view/AccessibilityInteractionController;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/view/AccessibilityInteractionController;->focusSearchUiThread(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method private applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 590
    if-nez p1, :cond_3

    #@2
    .line 605
    :cond_2
    :goto_2
    return-void

    #@3
    .line 593
    :cond_3
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@5
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    iget v0, v2, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@9
    .line 594
    .local v0, applicationScale:F
    const/high16 v2, 0x3f80

    #@b
    cmpl-float v2, v0, v2

    #@d
    if-eqz v2, :cond_2

    #@f
    .line 595
    iget-object v1, p0, Landroid/view/AccessibilityInteractionController;->mTempRect:Landroid/graphics/Rect;

    #@11
    .line 597
    .local v1, bounds:Landroid/graphics/Rect;
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInParent(Landroid/graphics/Rect;)V

    #@14
    .line 598
    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->scale(F)V

    #@17
    .line 599
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    #@1a
    .line 601
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    #@1d
    .line 602
    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->scale(F)V

    #@20
    .line 603
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    #@23
    goto :goto_2
.end method

.method private applyApplicationScaleIfNeeded(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 576
    .local p1, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    if-nez p1, :cond_3

    #@2
    .line 587
    :cond_2
    return-void

    #@3
    .line 579
    :cond_3
    iget-object v4, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@5
    iget-object v4, v4, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@7
    iget v0, v4, Landroid/view/View$AttachInfo;->mApplicationScale:F

    #@9
    .line 580
    .local v0, applicationScale:F
    const/high16 v4, 0x3f80

    #@b
    cmpl-float v4, v0, v4

    #@d
    if-eqz v4, :cond_2

    #@f
    .line 581
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@12
    move-result v3

    #@13
    .line 582
    .local v3, infoCount:I
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    if-ge v1, v3, :cond_2

    #@16
    .line 583
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1c
    .line 584
    .local v2, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {p0, v2}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@1f
    .line 582
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_14
.end method

.method private findAccessibilityNodeInfoByAccessibilityIdUiThread(Landroid/os/Message;)V
    .registers 13
    .parameter "message"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 114
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@3
    .line 116
    .local v3, flags:I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5
    check-cast v1, Lcom/android/internal/os/SomeArgs;

    #@7
    .line 117
    .local v1, args:Lcom/android/internal/os/SomeArgs;
    iget v0, v1, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@9
    .line 118
    .local v0, accessibilityViewId:I
    iget v7, v1, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@b
    .line 119
    .local v7, virtualDescendantId:I
    iget v5, v1, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@d
    .line 120
    .local v5, interactionId:I
    iget-object v2, v1, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@f
    check-cast v2, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@11
    .line 123
    .local v2, callback:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual {v1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@14
    .line 125
    iget-object v4, p0, Landroid/view/AccessibilityInteractionController;->mTempAccessibilityNodeInfoList:Ljava/util/ArrayList;

    #@16
    .line 126
    .local v4, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->clear()V

    #@19
    .line 128
    :try_start_19
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@1b
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1d
    if-eqz v9, :cond_25

    #@1f
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@21
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;
    :try_end_23
    .catchall {:try_start_19 .. :try_end_23} :catchall_6e

    #@23
    if-nez v9, :cond_36

    #@25
    .line 144
    :cond_25
    :try_start_25
    iget-object v8, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@27
    iget-object v8, v8, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@29
    const/4 v9, 0x0

    #@2a
    iput-boolean v9, v8, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@2c
    .line 145
    invoke-direct {p0, v4}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Ljava/util/List;)V

    #@2f
    .line 146
    invoke-interface {v2, v4, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V

    #@32
    .line 147
    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_35} :catch_67

    #@35
    .line 152
    :goto_35
    return-void

    #@36
    .line 131
    :cond_36
    :try_start_36
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@38
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@3a
    and-int/lit8 v10, v3, 0x8

    #@3c
    if-eqz v10, :cond_3f

    #@3e
    const/4 v8, 0x1

    #@3f
    :cond_3f
    iput-boolean v8, v9, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@41
    .line 133
    const/4 v6, 0x0

    #@42
    .line 134
    .local v6, root:Landroid/view/View;
    const/4 v8, -0x1

    #@43
    if-ne v0, v8, :cond_69

    #@45
    .line 135
    iget-object v8, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@47
    iget-object v6, v8, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@49
    .line 139
    :goto_49
    if-eqz v6, :cond_56

    #@4b
    invoke-direct {p0, v6}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@4e
    move-result v8

    #@4f
    if-eqz v8, :cond_56

    #@51
    .line 140
    iget-object v8, p0, Landroid/view/AccessibilityInteractionController;->mPrefetcher:Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;

    #@53
    invoke-virtual {v8, v6, v7, v3, v4}, Landroid/view/AccessibilityInteractionController$AccessibilityNodePrefetcher;->prefetchAccessibilityNodeInfos(Landroid/view/View;IILjava/util/List;)V
    :try_end_56
    .catchall {:try_start_36 .. :try_end_56} :catchall_6e

    #@56
    .line 144
    :cond_56
    :try_start_56
    iget-object v8, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@58
    iget-object v8, v8, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5a
    const/4 v9, 0x0

    #@5b
    iput-boolean v9, v8, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@5d
    .line 145
    invoke-direct {p0, v4}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Ljava/util/List;)V

    #@60
    .line 146
    invoke-interface {v2, v4, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V

    #@63
    .line 147
    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_66
    .catch Landroid/os/RemoteException; {:try_start_56 .. :try_end_66} :catch_67

    #@66
    goto :goto_35

    #@67
    .line 148
    .end local v6           #root:Landroid/view/View;
    :catch_67
    move-exception v8

    #@68
    goto :goto_35

    #@69
    .line 137
    .restart local v6       #root:Landroid/view/View;
    :cond_69
    :try_start_69
    invoke-direct {p0, v0}, Landroid/view/AccessibilityInteractionController;->findViewByAccessibilityId(I)Landroid/view/View;
    :try_end_6c
    .catchall {:try_start_69 .. :try_end_6c} :catchall_6e

    #@6c
    move-result-object v6

    #@6d
    goto :goto_49

    #@6e
    .line 143
    .end local v6           #root:Landroid/view/View;
    :catchall_6e
    move-exception v8

    #@6f
    .line 144
    :try_start_6f
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@71
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@73
    const/4 v10, 0x0

    #@74
    iput-boolean v10, v9, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@76
    .line 145
    invoke-direct {p0, v4}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Ljava/util/List;)V

    #@79
    .line 146
    invoke-interface {v2, v4, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V

    #@7c
    .line 147
    invoke-interface {v4}, Ljava/util/List;->clear()V
    :try_end_7f
    .catch Landroid/os/RemoteException; {:try_start_6f .. :try_end_7f} :catch_80

    #@7f
    .line 143
    :goto_7f
    throw v8

    #@80
    .line 148
    :catch_80
    move-exception v9

    #@81
    goto :goto_7f
.end method

.method private findAccessibilityNodeInfoByViewIdUiThread(Landroid/os/Message;)V
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 182
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@3
    .line 183
    .local v3, flags:I
    iget v0, p1, Landroid/os/Message;->arg2:I

    #@5
    .line 185
    .local v0, accessibilityViewId:I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7
    check-cast v1, Lcom/android/internal/os/SomeArgs;

    #@9
    .line 186
    .local v1, args:Lcom/android/internal/os/SomeArgs;
    iget v8, v1, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@b
    .line 187
    .local v8, viewId:I
    iget v5, v1, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@d
    .line 188
    .local v5, interactionId:I
    iget-object v2, v1, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@f
    check-cast v2, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@11
    .line 191
    .local v2, callback:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual {v1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@14
    .line 193
    const/4 v4, 0x0

    #@15
    .line 195
    .local v4, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_start_15
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@17
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@19
    if-eqz v10, :cond_21

    #@1b
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@1d
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;
    :try_end_1f
    .catchall {:try_start_15 .. :try_end_1f} :catchall_69

    #@1f
    if-nez v10, :cond_2f

    #@21
    .line 214
    :cond_21
    :try_start_21
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@23
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@25
    const/4 v10, 0x0

    #@26
    iput-boolean v10, v9, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@28
    .line 215
    invoke-direct {p0, v4}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@2b
    .line 216
    invoke-interface {v2, v4, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2e} :catch_62

    #@2e
    .line 221
    :goto_2e
    return-void

    #@2f
    .line 198
    :cond_2f
    :try_start_2f
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@31
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@33
    and-int/lit8 v11, v3, 0x8

    #@35
    if-eqz v11, :cond_38

    #@37
    const/4 v9, 0x1

    #@38
    :cond_38
    iput-boolean v9, v10, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@3a
    .line 200
    const/4 v6, 0x0

    #@3b
    .line 201
    .local v6, root:Landroid/view/View;
    const/4 v9, -0x1

    #@3c
    if-eq v0, v9, :cond_64

    #@3e
    .line 202
    invoke-direct {p0, v0}, Landroid/view/AccessibilityInteractionController;->findViewByAccessibilityId(I)Landroid/view/View;

    #@41
    move-result-object v6

    #@42
    .line 206
    :goto_42
    if-eqz v6, :cond_54

    #@44
    .line 207
    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@47
    move-result-object v7

    #@48
    .line 208
    .local v7, target:Landroid/view/View;
    if-eqz v7, :cond_54

    #@4a
    invoke-direct {p0, v7}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@4d
    move-result v9

    #@4e
    if-eqz v9, :cond_54

    #@50
    .line 209
    invoke-virtual {v7}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_53
    .catchall {:try_start_2f .. :try_end_53} :catchall_69

    #@53
    move-result-object v4

    #@54
    .line 214
    .end local v7           #target:Landroid/view/View;
    :cond_54
    :try_start_54
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@56
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@58
    const/4 v10, 0x0

    #@59
    iput-boolean v10, v9, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@5b
    .line 215
    invoke-direct {p0, v4}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@5e
    .line 216
    invoke-interface {v2, v4, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_61
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_61} :catch_62

    #@61
    goto :goto_2e

    #@62
    .line 217
    .end local v6           #root:Landroid/view/View;
    :catch_62
    move-exception v9

    #@63
    goto :goto_2e

    #@64
    .line 204
    .restart local v6       #root:Landroid/view/View;
    :cond_64
    :try_start_64
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@66
    iget-object v6, v9, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;
    :try_end_68
    .catchall {:try_start_64 .. :try_end_68} :catchall_69

    #@68
    goto :goto_42

    #@69
    .line 213
    .end local v6           #root:Landroid/view/View;
    :catchall_69
    move-exception v9

    #@6a
    .line 214
    :try_start_6a
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@6c
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6e
    const/4 v11, 0x0

    #@6f
    iput-boolean v11, v10, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@71
    .line 215
    invoke-direct {p0, v4}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@74
    .line 216
    invoke-interface {v2, v4, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_77
    .catch Landroid/os/RemoteException; {:try_start_6a .. :try_end_77} :catch_78

    #@77
    .line 213
    :goto_77
    throw v9

    #@78
    .line 217
    :catch_78
    move-exception v10

    #@79
    goto :goto_77
.end method

.method private findAccessibilityNodeInfosByTextUiThread(Landroid/os/Message;)V
    .registers 22
    .parameter "message"

    #@0
    .prologue
    .line 252
    move-object/from16 v0, p1

    #@2
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@4
    .line 254
    .local v5, flags:I
    move-object/from16 v0, p1

    #@6
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v3, Lcom/android/internal/os/SomeArgs;

    #@a
    .line 255
    .local v3, args:Lcom/android/internal/os/SomeArgs;
    iget-object v14, v3, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@c
    check-cast v14, Ljava/lang/String;

    #@e
    .line 256
    .local v14, text:Ljava/lang/String;
    iget-object v4, v3, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@10
    check-cast v4, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@12
    .line 258
    .local v4, callback:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    iget v2, v3, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@14
    .line 259
    .local v2, accessibilityViewId:I
    iget v0, v3, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@16
    move/from16 v16, v0

    #@18
    .line 260
    .local v16, virtualDescendantId:I
    iget v11, v3, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@1a
    .line 261
    .local v11, interactionId:I
    invoke-virtual {v3}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@1d
    .line 263
    const/4 v9, 0x0

    #@1e
    .line 265
    .local v9, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    :try_start_1e
    move-object/from16 v0, p0

    #@20
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@22
    move-object/from16 v17, v0

    #@24
    move-object/from16 v0, v17

    #@26
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@28
    move-object/from16 v17, v0

    #@2a
    if-eqz v17, :cond_3a

    #@2c
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@30
    move-object/from16 v17, v0

    #@32
    move-object/from16 v0, v17

    #@34
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@36
    move-object/from16 v17, v0
    :try_end_38
    .catchall {:try_start_1e .. :try_end_38} :catchall_117

    #@38
    if-nez v17, :cond_57

    #@3a
    .line 312
    :cond_3a
    :try_start_3a
    move-object/from16 v0, p0

    #@3c
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@3e
    move-object/from16 v17, v0

    #@40
    move-object/from16 v0, v17

    #@42
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@44
    move-object/from16 v17, v0

    #@46
    const/16 v18, 0x0

    #@48
    move/from16 v0, v18

    #@4a
    move-object/from16 v1, v17

    #@4c
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@4e
    .line 313
    move-object/from16 v0, p0

    #@50
    invoke-direct {v0, v9}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Ljava/util/List;)V

    #@53
    .line 314
    invoke-interface {v4, v9, v11}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V
    :try_end_56
    .catch Landroid/os/RemoteException; {:try_start_3a .. :try_end_56} :catch_af

    #@56
    .line 319
    :goto_56
    return-void

    #@57
    .line 268
    :cond_57
    :try_start_57
    move-object/from16 v0, p0

    #@59
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@5b
    move-object/from16 v17, v0

    #@5d
    move-object/from16 v0, v17

    #@5f
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@61
    move-object/from16 v18, v0

    #@63
    and-int/lit8 v17, v5, 0x8

    #@65
    if-eqz v17, :cond_b1

    #@67
    const/16 v17, 0x1

    #@69
    :goto_69
    move/from16 v0, v17

    #@6b
    move-object/from16 v1, v18

    #@6d
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@6f
    .line 270
    const/4 v13, 0x0

    #@70
    .line 271
    .local v13, root:Landroid/view/View;
    const/16 v17, -0x1

    #@72
    move/from16 v0, v17

    #@74
    if-eq v2, v0, :cond_b4

    #@76
    .line 272
    move-object/from16 v0, p0

    #@78
    invoke-direct {v0, v2}, Landroid/view/AccessibilityInteractionController;->findViewByAccessibilityId(I)Landroid/view/View;

    #@7b
    move-result-object v13

    #@7c
    .line 276
    :goto_7c
    if-eqz v13, :cond_92

    #@7e
    move-object/from16 v0, p0

    #@80
    invoke-direct {v0, v13}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@83
    move-result v17

    #@84
    if-eqz v17, :cond_92

    #@86
    .line 277
    invoke-virtual {v13}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@89
    move-result-object v12

    #@8a
    .line 278
    .local v12, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v12, :cond_bf

    #@8c
    .line 279
    move/from16 v0, v16

    #@8e
    invoke-virtual {v12, v14, v0}, Landroid/view/accessibility/AccessibilityNodeProvider;->findAccessibilityNodeInfosByText(Ljava/lang/String;I)Ljava/util/List;
    :try_end_91
    .catchall {:try_start_57 .. :try_end_91} :catchall_117

    #@91
    move-result-object v9

    #@92
    .line 312
    .end local v12           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_92
    :try_start_92
    move-object/from16 v0, p0

    #@94
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@96
    move-object/from16 v17, v0

    #@98
    move-object/from16 v0, v17

    #@9a
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@9c
    move-object/from16 v17, v0

    #@9e
    const/16 v18, 0x0

    #@a0
    move/from16 v0, v18

    #@a2
    move-object/from16 v1, v17

    #@a4
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@a6
    .line 313
    move-object/from16 v0, p0

    #@a8
    invoke-direct {v0, v9}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Ljava/util/List;)V

    #@ab
    .line 314
    invoke-interface {v4, v9, v11}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V
    :try_end_ae
    .catch Landroid/os/RemoteException; {:try_start_92 .. :try_end_ae} :catch_af

    #@ae
    goto :goto_56

    #@af
    .line 315
    .end local v13           #root:Landroid/view/View;
    :catch_af
    move-exception v17

    #@b0
    goto :goto_56

    #@b1
    .line 268
    :cond_b1
    const/16 v17, 0x0

    #@b3
    goto :goto_69

    #@b4
    .line 274
    .restart local v13       #root:Landroid/view/View;
    :cond_b4
    :try_start_b4
    move-object/from16 v0, p0

    #@b6
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@b8
    move-object/from16 v17, v0

    #@ba
    move-object/from16 v0, v17

    #@bc
    iget-object v13, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@be
    goto :goto_7c

    #@bf
    .line 281
    .restart local v12       #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_bf
    const/16 v17, -0x1

    #@c1
    move/from16 v0, v16

    #@c3
    move/from16 v1, v17

    #@c5
    if-ne v0, v1, :cond_92

    #@c7
    .line 282
    move-object/from16 v0, p0

    #@c9
    iget-object v7, v0, Landroid/view/AccessibilityInteractionController;->mTempArrayList:Ljava/util/ArrayList;

    #@cb
    .line 283
    .local v7, foundViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@ce
    .line 284
    const/16 v17, 0x7

    #@d0
    move/from16 v0, v17

    #@d2
    invoke-virtual {v13, v7, v14, v0}, Landroid/view/View;->findViewsWithText(Ljava/util/ArrayList;Ljava/lang/CharSequence;I)V

    #@d5
    .line 287
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    #@d8
    move-result v17

    #@d9
    if-nez v17, :cond_92

    #@db
    .line 288
    move-object/from16 v0, p0

    #@dd
    iget-object v9, v0, Landroid/view/AccessibilityInteractionController;->mTempAccessibilityNodeInfoList:Ljava/util/ArrayList;

    #@df
    .line 289
    invoke-interface {v9}, Ljava/util/List;->clear()V

    #@e2
    .line 290
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@e5
    move-result v15

    #@e6
    .line 291
    .local v15, viewCount:I
    const/4 v8, 0x0

    #@e7
    .local v8, i:I
    :goto_e7
    if-ge v8, v15, :cond_92

    #@e9
    .line 292
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ec
    move-result-object v6

    #@ed
    check-cast v6, Landroid/view/View;

    #@ef
    .line 293
    .local v6, foundView:Landroid/view/View;
    move-object/from16 v0, p0

    #@f1
    invoke-direct {v0, v6}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@f4
    move-result v17

    #@f5
    if-eqz v17, :cond_10a

    #@f7
    .line 294
    invoke-virtual {v6}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@fa
    move-result-object v12

    #@fb
    .line 295
    if-eqz v12, :cond_10d

    #@fd
    .line 296
    const/16 v17, -0x1

    #@ff
    move/from16 v0, v17

    #@101
    invoke-virtual {v12, v14, v0}, Landroid/view/accessibility/AccessibilityNodeProvider;->findAccessibilityNodeInfosByText(Ljava/lang/String;I)Ljava/util/List;

    #@104
    move-result-object v10

    #@105
    .line 299
    .local v10, infosFromProvider:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    if-eqz v10, :cond_10a

    #@107
    .line 300
    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@10a
    .line 291
    .end local v10           #infosFromProvider:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    :cond_10a
    :goto_10a
    add-int/lit8 v8, v8, 0x1

    #@10c
    goto :goto_e7

    #@10d
    .line 303
    :cond_10d
    invoke-virtual {v6}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@110
    move-result-object v17

    #@111
    move-object/from16 v0, v17

    #@113
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_116
    .catchall {:try_start_b4 .. :try_end_116} :catchall_117

    #@116
    goto :goto_10a

    #@117
    .line 311
    .end local v6           #foundView:Landroid/view/View;
    .end local v7           #foundViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v8           #i:I
    .end local v12           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    .end local v13           #root:Landroid/view/View;
    .end local v15           #viewCount:I
    :catchall_117
    move-exception v17

    #@118
    .line 312
    :try_start_118
    move-object/from16 v0, p0

    #@11a
    iget-object v0, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@11c
    move-object/from16 v18, v0

    #@11e
    move-object/from16 v0, v18

    #@120
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@122
    move-object/from16 v18, v0

    #@124
    const/16 v19, 0x0

    #@126
    move/from16 v0, v19

    #@128
    move-object/from16 v1, v18

    #@12a
    iput-boolean v0, v1, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@12c
    .line 313
    move-object/from16 v0, p0

    #@12e
    invoke-direct {v0, v9}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Ljava/util/List;)V

    #@131
    .line 314
    invoke-interface {v4, v9, v11}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V
    :try_end_134
    .catch Landroid/os/RemoteException; {:try_start_118 .. :try_end_134} :catch_135

    #@134
    .line 311
    :goto_134
    throw v17

    #@135
    .line 315
    :catch_135
    move-exception v18

    #@136
    goto :goto_134
.end method

.method private findFocusUiThread(Landroid/os/Message;)V
    .registers 18
    .parameter "message"

    #@0
    .prologue
    .line 350
    move-object/from16 v0, p1

    #@2
    iget v4, v0, Landroid/os/Message;->arg1:I

    #@4
    .line 351
    .local v4, flags:I
    move-object/from16 v0, p1

    #@6
    iget v5, v0, Landroid/os/Message;->arg2:I

    #@8
    .line 353
    .local v5, focusType:I
    move-object/from16 v0, p1

    #@a
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v2, Lcom/android/internal/os/SomeArgs;

    #@e
    .line 354
    .local v2, args:Lcom/android/internal/os/SomeArgs;
    iget v8, v2, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@10
    .line 355
    .local v8, interactionId:I
    iget v1, v2, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@12
    .line 356
    .local v1, accessibilityViewId:I
    iget v12, v2, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@14
    .line 357
    .local v12, virtualDescendantId:I
    iget-object v3, v2, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@16
    check-cast v3, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@18
    .line 360
    .local v3, callback:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual {v2}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@1b
    .line 362
    const/4 v6, 0x0

    #@1c
    .line 364
    .local v6, focused:Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_start_1c
    move-object/from16 v0, p0

    #@1e
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@20
    iget-object v13, v13, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@22
    if-eqz v13, :cond_2c

    #@24
    move-object/from16 v0, p0

    #@26
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@28
    iget-object v13, v13, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;
    :try_end_2a
    .catchall {:try_start_1c .. :try_end_2a} :catchall_7b

    #@2a
    if-nez v13, :cond_3e

    #@2c
    .line 409
    :cond_2c
    :try_start_2c
    move-object/from16 v0, p0

    #@2e
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@30
    iget-object v13, v13, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@32
    const/4 v14, 0x0

    #@33
    iput-boolean v14, v13, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@35
    .line 410
    move-object/from16 v0, p0

    #@37
    invoke-direct {v0, v6}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3a
    .line 411
    invoke-interface {v3, v6, v8}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_3d
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_3d} :catch_b7

    #@3d
    .line 416
    :goto_3d
    return-void

    #@3e
    .line 367
    :cond_3e
    :try_start_3e
    move-object/from16 v0, p0

    #@40
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@42
    iget-object v14, v13, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@44
    and-int/lit8 v13, v4, 0x8

    #@46
    if-eqz v13, :cond_8e

    #@48
    const/4 v13, 0x1

    #@49
    :goto_49
    iput-boolean v13, v14, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@4b
    .line 369
    const/4 v10, 0x0

    #@4c
    .line 370
    .local v10, root:Landroid/view/View;
    const/4 v13, -0x1

    #@4d
    if-eq v1, v13, :cond_90

    #@4f
    .line 371
    move-object/from16 v0, p0

    #@51
    invoke-direct {v0, v1}, Landroid/view/AccessibilityInteractionController;->findViewByAccessibilityId(I)Landroid/view/View;

    #@54
    move-result-object v10

    #@55
    .line 375
    :goto_55
    if-eqz v10, :cond_a5

    #@57
    move-object/from16 v0, p0

    #@59
    invoke-direct {v0, v10}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@5c
    move-result v13

    #@5d
    if-eqz v13, :cond_a5

    #@5f
    .line 376
    packed-switch v5, :pswitch_data_f0

    #@62
    .line 404
    new-instance v13, Ljava/lang/IllegalArgumentException;

    #@64
    new-instance v14, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v15, "Unknown focus type: "

    #@6b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v14

    #@6f
    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v14

    #@73
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v14

    #@77
    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@7a
    throw v13
    :try_end_7b
    .catchall {:try_start_3e .. :try_end_7b} :catchall_7b

    #@7b
    .line 408
    .end local v10           #root:Landroid/view/View;
    :catchall_7b
    move-exception v13

    #@7c
    .line 409
    :try_start_7c
    move-object/from16 v0, p0

    #@7e
    iget-object v14, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@80
    iget-object v14, v14, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@82
    const/4 v15, 0x0

    #@83
    iput-boolean v15, v14, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@85
    .line 410
    move-object/from16 v0, p0

    #@87
    invoke-direct {v0, v6}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@8a
    .line 411
    invoke-interface {v3, v6, v8}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_8d
    .catch Landroid/os/RemoteException; {:try_start_7c .. :try_end_8d} :catch_ed

    #@8d
    .line 408
    :goto_8d
    throw v13

    #@8e
    .line 367
    :cond_8e
    const/4 v13, 0x0

    #@8f
    goto :goto_49

    #@90
    .line 373
    .restart local v10       #root:Landroid/view/View;
    :cond_90
    :try_start_90
    move-object/from16 v0, p0

    #@92
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@94
    iget-object v10, v13, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@96
    goto :goto_55

    #@97
    .line 378
    :pswitch_97
    move-object/from16 v0, p0

    #@99
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@9b
    iget-object v7, v13, Landroid/view/ViewRootImpl;->mAccessibilityFocusedHost:Landroid/view/View;

    #@9d
    .line 381
    .local v7, host:Landroid/view/View;
    if-eqz v7, :cond_a5

    #@9f
    invoke-static {v7, v10}, Landroid/view/ViewRootImpl;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z
    :try_end_a2
    .catchall {:try_start_90 .. :try_end_a2} :catchall_7b

    #@a2
    move-result v13

    #@a3
    if-nez v13, :cond_b9

    #@a5
    .line 409
    .end local v7           #host:Landroid/view/View;
    :cond_a5
    :goto_a5
    :try_start_a5
    move-object/from16 v0, p0

    #@a7
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@a9
    iget-object v13, v13, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@ab
    const/4 v14, 0x0

    #@ac
    iput-boolean v14, v13, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@ae
    .line 410
    move-object/from16 v0, p0

    #@b0
    invoke-direct {v0, v6}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@b3
    .line 411
    invoke-interface {v3, v6, v8}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_b6
    .catch Landroid/os/RemoteException; {:try_start_a5 .. :try_end_b6} :catch_b7

    #@b6
    goto :goto_3d

    #@b7
    .line 412
    .end local v10           #root:Landroid/view/View;
    :catch_b7
    move-exception v13

    #@b8
    goto :goto_3d

    #@b9
    .line 386
    .restart local v7       #host:Landroid/view/View;
    .restart local v10       #root:Landroid/view/View;
    :cond_b9
    :try_start_b9
    invoke-virtual {v7}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@bc
    move-result-object v9

    #@bd
    .line 387
    .local v9, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v9, :cond_d2

    #@bf
    .line 388
    move-object/from16 v0, p0

    #@c1
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@c3
    iget-object v13, v13, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@c5
    if-eqz v13, :cond_a5

    #@c7
    .line 389
    move-object/from16 v0, p0

    #@c9
    iget-object v13, v0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@cb
    iget-object v13, v13, Landroid/view/ViewRootImpl;->mAccessibilityFocusedVirtualView:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@cd
    invoke-static {v13}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@d0
    move-result-object v6

    #@d1
    goto :goto_a5

    #@d2
    .line 392
    :cond_d2
    const/4 v13, -0x1

    #@d3
    if-ne v12, v13, :cond_a5

    #@d5
    .line 393
    invoke-virtual {v7}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@d8
    move-result-object v6

    #@d9
    goto :goto_a5

    #@da
    .line 398
    .end local v7           #host:Landroid/view/View;
    .end local v9           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :pswitch_da
    invoke-virtual {v10}, Landroid/view/View;->findFocus()Landroid/view/View;

    #@dd
    move-result-object v11

    #@de
    .line 399
    .local v11, target:Landroid/view/View;
    if-eqz v11, :cond_a5

    #@e0
    move-object/from16 v0, p0

    #@e2
    invoke-direct {v0, v11}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@e5
    move-result v13

    #@e6
    if-eqz v13, :cond_a5

    #@e8
    .line 400
    invoke-virtual {v11}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_eb
    .catchall {:try_start_b9 .. :try_end_eb} :catchall_7b

    #@eb
    move-result-object v6

    #@ec
    goto :goto_a5

    #@ed
    .line 412
    .end local v10           #root:Landroid/view/View;
    .end local v11           #target:Landroid/view/View;
    :catch_ed
    move-exception v14

    #@ee
    goto :goto_8d

    #@ef
    .line 376
    nop

    #@f0
    :pswitch_data_f0
    .packed-switch 0x1
        :pswitch_da
        :pswitch_97
    .end packed-switch
.end method

.method private findViewByAccessibilityId(I)Landroid/view/View;
    .registers 6
    .parameter "accessibilityId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 564
    iget-object v3, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@3
    iget-object v1, v3, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@5
    .line 565
    .local v1, root:Landroid/view/View;
    if-nez v1, :cond_9

    #@7
    move-object v0, v2

    #@8
    .line 572
    :cond_8
    :goto_8
    return-object v0

    #@9
    .line 568
    :cond_9
    invoke-virtual {v1, p1}, Landroid/view/View;->findViewByAccessibilityId(I)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    .line 569
    .local v0, foundView:Landroid/view/View;
    if-eqz v0, :cond_8

    #@f
    invoke-direct {p0, v0}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_8

    #@15
    move-object v0, v2

    #@16
    .line 570
    goto :goto_8
.end method

.method private focusSearchUiThread(Landroid/os/Message;)V
    .registers 14
    .parameter "message"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 446
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@3
    .line 447
    .local v4, flags:I
    iget v0, p1, Landroid/os/Message;->arg2:I

    #@5
    .line 449
    .local v0, accessibilityViewId:I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7
    check-cast v1, Lcom/android/internal/os/SomeArgs;

    #@9
    .line 450
    .local v1, args:Lcom/android/internal/os/SomeArgs;
    iget v3, v1, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@b
    .line 451
    .local v3, direction:I
    iget v5, v1, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@d
    .line 452
    .local v5, interactionId:I
    iget-object v2, v1, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@f
    check-cast v2, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@11
    .line 455
    .local v2, callback:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual {v1}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@14
    .line 457
    const/4 v6, 0x0

    #@15
    .line 459
    .local v6, next:Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_start_15
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@17
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@19
    if-eqz v10, :cond_21

    #@1b
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@1d
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;
    :try_end_1f
    .catchall {:try_start_15 .. :try_end_1f} :catchall_69

    #@1f
    if-nez v10, :cond_2f

    #@21
    .line 478
    :cond_21
    :try_start_21
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@23
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@25
    const/4 v10, 0x0

    #@26
    iput-boolean v10, v9, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@28
    .line 479
    invoke-direct {p0, v6}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@2b
    .line 480
    invoke-interface {v2, v6, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2e} :catch_62

    #@2e
    .line 485
    :goto_2e
    return-void

    #@2f
    .line 462
    :cond_2f
    :try_start_2f
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@31
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@33
    and-int/lit8 v11, v4, 0x8

    #@35
    if-eqz v11, :cond_38

    #@37
    const/4 v9, 0x1

    #@38
    :cond_38
    iput-boolean v9, v10, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@3a
    .line 464
    const/4 v8, 0x0

    #@3b
    .line 465
    .local v8, root:Landroid/view/View;
    const/4 v9, -0x1

    #@3c
    if-eq v0, v9, :cond_64

    #@3e
    .line 466
    invoke-direct {p0, v0}, Landroid/view/AccessibilityInteractionController;->findViewByAccessibilityId(I)Landroid/view/View;

    #@41
    move-result-object v8

    #@42
    .line 470
    :goto_42
    if-eqz v8, :cond_54

    #@44
    invoke-direct {p0, v8}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@47
    move-result v9

    #@48
    if-eqz v9, :cond_54

    #@4a
    .line 471
    invoke-virtual {v8, v3}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    #@4d
    move-result-object v7

    #@4e
    .line 472
    .local v7, nextView:Landroid/view/View;
    if-eqz v7, :cond_54

    #@50
    .line 473
    invoke-virtual {v7}, Landroid/view/View;->createAccessibilityNodeInfo()Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_53
    .catchall {:try_start_2f .. :try_end_53} :catchall_69

    #@53
    move-result-object v6

    #@54
    .line 478
    .end local v7           #nextView:Landroid/view/View;
    :cond_54
    :try_start_54
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@56
    iget-object v9, v9, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@58
    const/4 v10, 0x0

    #@59
    iput-boolean v10, v9, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@5b
    .line 479
    invoke-direct {p0, v6}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@5e
    .line 480
    invoke-interface {v2, v6, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_61
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_61} :catch_62

    #@61
    goto :goto_2e

    #@62
    .line 481
    .end local v8           #root:Landroid/view/View;
    :catch_62
    move-exception v9

    #@63
    goto :goto_2e

    #@64
    .line 468
    .restart local v8       #root:Landroid/view/View;
    :cond_64
    :try_start_64
    iget-object v9, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@66
    iget-object v8, v9, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;
    :try_end_68
    .catchall {:try_start_64 .. :try_end_68} :catchall_69

    #@68
    goto :goto_42

    #@69
    .line 477
    .end local v8           #root:Landroid/view/View;
    :catchall_69
    move-exception v9

    #@6a
    .line 478
    :try_start_6a
    iget-object v10, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@6c
    iget-object v10, v10, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6e
    const/4 v11, 0x0

    #@6f
    iput-boolean v11, v10, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@71
    .line 479
    invoke-direct {p0, v6}, Landroid/view/AccessibilityInteractionController;->applyApplicationScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@74
    .line 480
    invoke-interface {v2, v6, v5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    :try_end_77
    .catch Landroid/os/RemoteException; {:try_start_6a .. :try_end_77} :catch_78

    #@77
    .line 477
    :goto_77
    throw v9

    #@78
    .line 481
    :catch_78
    move-exception v10

    #@79
    goto :goto_77
.end method

.method private isShown(Landroid/view/View;)Z
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 81
    iget-object v0, p1, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p1, Landroid/view/View;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@6
    iget v0, v0, Landroid/view/View$AttachInfo;->mWindowVisibility:I

    #@8
    if-nez v0, :cond_12

    #@a
    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private perfromAccessibilityActionUiThread(Landroid/os/Message;)V
    .registers 16
    .parameter "message"

    #@0
    .prologue
    .line 518
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@2
    .line 519
    .local v5, flags:I
    iget v0, p1, Landroid/os/Message;->arg2:I

    #@4
    .line 521
    .local v0, accessibilityViewId:I
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6
    check-cast v2, Lcom/android/internal/os/SomeArgs;

    #@8
    .line 522
    .local v2, args:Lcom/android/internal/os/SomeArgs;
    iget v10, v2, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@a
    .line 523
    .local v10, virtualDescendantId:I
    iget v1, v2, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@c
    .line 524
    .local v1, action:I
    iget v6, v2, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@e
    .line 525
    .local v6, interactionId:I
    iget-object v4, v2, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@10
    check-cast v4, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@12
    .line 527
    .local v4, callback:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    iget-object v3, v2, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@14
    check-cast v3, Landroid/os/Bundle;

    #@16
    .line 529
    .local v3, arguments:Landroid/os/Bundle;
    invoke-virtual {v2}, Lcom/android/internal/os/SomeArgs;->recycle()V

    #@19
    .line 531
    const/4 v8, 0x0

    #@1a
    .line 533
    .local v8, succeeded:Z
    :try_start_1a
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@1c
    iget-object v11, v11, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@1e
    if-eqz v11, :cond_26

    #@20
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@22
    iget-object v11, v11, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;
    :try_end_24
    .catchall {:try_start_1a .. :try_end_24} :catchall_72

    #@24
    if-nez v11, :cond_31

    #@26
    .line 555
    :cond_26
    :try_start_26
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@28
    iget-object v11, v11, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@2a
    const/4 v12, 0x0

    #@2b
    iput-boolean v12, v11, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@2d
    .line 556
    invoke-interface {v4, v8, v6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setPerformAccessibilityActionResult(ZI)V
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_30} :catch_61

    #@30
    .line 561
    :goto_30
    return-void

    #@31
    .line 536
    :cond_31
    :try_start_31
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@33
    iget-object v12, v11, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@35
    and-int/lit8 v11, v5, 0x8

    #@37
    if-eqz v11, :cond_63

    #@39
    const/4 v11, 0x1

    #@3a
    :goto_3a
    iput-boolean v11, v12, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@3c
    .line 538
    const/4 v9, 0x0

    #@3d
    .line 539
    .local v9, target:Landroid/view/View;
    const/4 v11, -0x1

    #@3e
    if-eq v0, v11, :cond_65

    #@40
    .line 540
    invoke-direct {p0, v0}, Landroid/view/AccessibilityInteractionController;->findViewByAccessibilityId(I)Landroid/view/View;

    #@43
    move-result-object v9

    #@44
    .line 544
    :goto_44
    if-eqz v9, :cond_56

    #@46
    invoke-direct {p0, v9}, Landroid/view/AccessibilityInteractionController;->isShown(Landroid/view/View;)Z

    #@49
    move-result v11

    #@4a
    if-eqz v11, :cond_56

    #@4c
    .line 545
    invoke-virtual {v9}, Landroid/view/View;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    #@4f
    move-result-object v7

    #@50
    .line 546
    .local v7, provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    if-eqz v7, :cond_6a

    #@52
    .line 547
    invoke-virtual {v7, v10, v1, v3}, Landroid/view/accessibility/AccessibilityNodeProvider;->performAction(IILandroid/os/Bundle;)Z
    :try_end_55
    .catchall {:try_start_31 .. :try_end_55} :catchall_72

    #@55
    move-result v8

    #@56
    .line 555
    .end local v7           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_56
    :goto_56
    :try_start_56
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@58
    iget-object v11, v11, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@5a
    const/4 v12, 0x0

    #@5b
    iput-boolean v12, v11, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@5d
    .line 556
    invoke-interface {v4, v8, v6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setPerformAccessibilityActionResult(ZI)V
    :try_end_60
    .catch Landroid/os/RemoteException; {:try_start_56 .. :try_end_60} :catch_61

    #@60
    goto :goto_30

    #@61
    .line 557
    .end local v9           #target:Landroid/view/View;
    :catch_61
    move-exception v11

    #@62
    goto :goto_30

    #@63
    .line 536
    :cond_63
    const/4 v11, 0x0

    #@64
    goto :goto_3a

    #@65
    .line 542
    .restart local v9       #target:Landroid/view/View;
    :cond_65
    :try_start_65
    iget-object v11, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@67
    iget-object v9, v11, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@69
    goto :goto_44

    #@6a
    .line 549
    .restart local v7       #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    :cond_6a
    const/4 v11, -0x1

    #@6b
    if-ne v10, v11, :cond_56

    #@6d
    .line 550
    invoke-virtual {v9, v1, v3}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z
    :try_end_70
    .catchall {:try_start_65 .. :try_end_70} :catchall_72

    #@70
    move-result v8

    #@71
    goto :goto_56

    #@72
    .line 554
    .end local v7           #provider:Landroid/view/accessibility/AccessibilityNodeProvider;
    .end local v9           #target:Landroid/view/View;
    :catchall_72
    move-exception v11

    #@73
    .line 555
    :try_start_73
    iget-object v12, p0, Landroid/view/AccessibilityInteractionController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    #@75
    iget-object v12, v12, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@77
    const/4 v13, 0x0

    #@78
    iput-boolean v13, v12, Landroid/view/View$AttachInfo;->mIncludeNotImportantViews:Z

    #@7a
    .line 556
    invoke-interface {v4, v8, v6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->setPerformAccessibilityActionResult(ZI)V
    :try_end_7d
    .catch Landroid/os/RemoteException; {:try_start_73 .. :try_end_7d} :catch_7e

    #@7d
    .line 554
    :goto_7d
    throw v11

    #@7e
    .line 557
    :catch_7e
    move-exception v12

    #@7f
    goto :goto_7d
.end method


# virtual methods
.method public findAccessibilityNodeInfoByAccessibilityIdClientThread(JILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 13
    .parameter "accessibilityNodeId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 90
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 91
    .local v1, message:Landroid/os/Message;
    const/4 v2, 0x2

    #@7
    iput v2, v1, Landroid/os/Message;->what:I

    #@9
    .line 92
    iput p5, v1, Landroid/os/Message;->arg1:I

    #@b
    .line 94
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@e
    move-result-object v0

    #@f
    .line 95
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@12
    move-result v2

    #@13
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@15
    .line 96
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@18
    move-result v2

    #@19
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@1b
    .line 97
    iput p3, v0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@1d
    .line 98
    iput-object p4, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@1f
    .line 99
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    .line 105
    iget v2, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@23
    if-ne p6, v2, :cond_33

    #@25
    iget-wide v2, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@27
    cmp-long v2, p7, v2

    #@29
    if-nez v2, :cond_33

    #@2b
    .line 106
    invoke-static {p7, p8}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->setSameThreadMessage(Landroid/os/Message;)V

    #@32
    .line 111
    :goto_32
    return-void

    #@33
    .line 109
    :cond_33
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@35
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@38
    goto :goto_32
.end method

.method public findAccessibilityNodeInfoByViewIdClientThread(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 14
    .parameter "accessibilityNodeId"
    .parameter "viewId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 157
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 158
    .local v1, message:Landroid/os/Message;
    const/4 v2, 0x3

    #@7
    iput v2, v1, Landroid/os/Message;->what:I

    #@9
    .line 159
    iput p6, v1, Landroid/os/Message;->arg1:I

    #@b
    .line 160
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@e
    move-result v2

    #@f
    iput v2, v1, Landroid/os/Message;->arg2:I

    #@11
    .line 162
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@14
    move-result-object v0

    #@15
    .line 163
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    iput p3, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@17
    .line 164
    iput p4, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@19
    .line 165
    iput-object p5, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@1b
    .line 167
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    .line 173
    iget v2, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@1f
    if-ne p7, v2, :cond_2f

    #@21
    iget-wide v2, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@23
    cmp-long v2, p8, v2

    #@25
    if-nez v2, :cond_2f

    #@27
    .line 174
    invoke-static {p8, p9}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->setSameThreadMessage(Landroid/os/Message;)V

    #@2e
    .line 179
    :goto_2e
    return-void

    #@2f
    .line 177
    :cond_2f
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@31
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@34
    goto :goto_2e
.end method

.method public findAccessibilityNodeInfosByTextClientThread(JLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 14
    .parameter "accessibilityNodeId"
    .parameter "text"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 226
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 227
    .local v1, message:Landroid/os/Message;
    const/4 v2, 0x4

    #@7
    iput v2, v1, Landroid/os/Message;->what:I

    #@9
    .line 228
    iput p6, v1, Landroid/os/Message;->arg1:I

    #@b
    .line 230
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@e
    move-result-object v0

    #@f
    .line 231
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    iput-object p3, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@11
    .line 232
    iput-object p5, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@13
    .line 233
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@16
    move-result v2

    #@17
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@19
    .line 234
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@1c
    move-result v2

    #@1d
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@1f
    .line 235
    iput p4, v0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@21
    .line 237
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23
    .line 243
    iget v2, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@25
    if-ne p7, v2, :cond_35

    #@27
    iget-wide v2, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@29
    cmp-long v2, p8, v2

    #@2b
    if-nez v2, :cond_35

    #@2d
    .line 244
    invoke-static {p8, p9}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->setSameThreadMessage(Landroid/os/Message;)V

    #@34
    .line 249
    :goto_34
    return-void

    #@35
    .line 247
    :cond_35
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@37
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3a
    goto :goto_34
.end method

.method public findFocusClientThread(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 14
    .parameter "accessibilityNodeId"
    .parameter "focusType"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 324
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 325
    .local v1, message:Landroid/os/Message;
    const/4 v2, 0x5

    #@7
    iput v2, v1, Landroid/os/Message;->what:I

    #@9
    .line 326
    iput p6, v1, Landroid/os/Message;->arg1:I

    #@b
    .line 327
    iput p3, v1, Landroid/os/Message;->arg2:I

    #@d
    .line 329
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@10
    move-result-object v0

    #@11
    .line 330
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    iput p4, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@13
    .line 331
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@16
    move-result v2

    #@17
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@19
    .line 332
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@1c
    move-result v2

    #@1d
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@1f
    .line 333
    iput-object p5, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@21
    .line 335
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23
    .line 341
    iget v2, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@25
    if-ne p7, v2, :cond_35

    #@27
    iget-wide v2, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@29
    cmp-long v2, p8, v2

    #@2b
    if-nez v2, :cond_35

    #@2d
    .line 342
    invoke-static {p8, p9}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->setSameThreadMessage(Landroid/os/Message;)V

    #@34
    .line 347
    :goto_34
    return-void

    #@35
    .line 345
    :cond_35
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@37
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3a
    goto :goto_34
.end method

.method public focusSearchClientThread(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 14
    .parameter "accessibilityNodeId"
    .parameter "direction"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 421
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 422
    .local v1, message:Landroid/os/Message;
    const/4 v2, 0x6

    #@7
    iput v2, v1, Landroid/os/Message;->what:I

    #@9
    .line 423
    iput p6, v1, Landroid/os/Message;->arg1:I

    #@b
    .line 424
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@e
    move-result v2

    #@f
    iput v2, v1, Landroid/os/Message;->arg2:I

    #@11
    .line 426
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@14
    move-result-object v0

    #@15
    .line 427
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    iput p3, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@17
    .line 428
    iput p4, v0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@19
    .line 429
    iput-object p5, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@1b
    .line 431
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    .line 437
    iget v2, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@1f
    if-ne p7, v2, :cond_2f

    #@21
    iget-wide v2, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@23
    cmp-long v2, p8, v2

    #@25
    if-nez v2, :cond_2f

    #@27
    .line 438
    invoke-static {p8, p9}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->setSameThreadMessage(Landroid/os/Message;)V

    #@2e
    .line 443
    :goto_2e
    return-void

    #@2f
    .line 441
    :cond_2f
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@31
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@34
    goto :goto_2e
.end method

.method public performAccessibilityActionClientThread(JILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 15
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interogatingPid"
    .parameter "interrogatingTid"

    #@0
    .prologue
    .line 491
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@2
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 492
    .local v1, message:Landroid/os/Message;
    const/4 v2, 0x1

    #@7
    iput v2, v1, Landroid/os/Message;->what:I

    #@9
    .line 493
    iput p7, v1, Landroid/os/Message;->arg1:I

    #@b
    .line 494
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@e
    move-result v2

    #@f
    iput v2, v1, Landroid/os/Message;->arg2:I

    #@11
    .line 496
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    #@14
    move-result-object v0

    #@15
    .line 497
    .local v0, args:Lcom/android/internal/os/SomeArgs;
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@18
    move-result v2

    #@19
    iput v2, v0, Lcom/android/internal/os/SomeArgs;->argi1:I

    #@1b
    .line 498
    iput p3, v0, Lcom/android/internal/os/SomeArgs;->argi2:I

    #@1d
    .line 499
    iput p5, v0, Lcom/android/internal/os/SomeArgs;->argi3:I

    #@1f
    .line 500
    iput-object p6, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    #@21
    .line 501
    iput-object p4, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    #@23
    .line 503
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@25
    .line 509
    iget v2, p0, Landroid/view/AccessibilityInteractionController;->mMyProcessId:I

    #@27
    if-ne p8, v2, :cond_37

    #@29
    iget-wide v2, p0, Landroid/view/AccessibilityInteractionController;->mMyLooperThreadId:J

    #@2b
    cmp-long v2, p9, v2

    #@2d
    if-nez v2, :cond_37

    #@2f
    .line 510
    invoke-static {p9, p10}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->setSameThreadMessage(Landroid/os/Message;)V

    #@36
    .line 515
    :goto_36
    return-void

    #@37
    .line 513
    :cond_37
    iget-object v2, p0, Landroid/view/AccessibilityInteractionController;->mHandler:Landroid/os/Handler;

    #@39
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3c
    goto :goto_36
.end method
