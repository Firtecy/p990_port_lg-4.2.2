.class final Landroid/view/PointerIcon$1;
.super Ljava/lang/Object;
.source "PointerIcon.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/PointerIcon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/view/PointerIcon;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 309
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/view/PointerIcon;
    .registers 9
    .parameter "in"

    #@0
    .prologue
    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v4

    #@4
    .line 312
    .local v4, style:I
    if-nez v4, :cond_b

    #@6
    .line 313
    invoke-static {}, Landroid/view/PointerIcon;->getNullIcon()Landroid/view/PointerIcon;

    #@9
    move-result-object v3

    #@a
    .line 326
    :goto_a
    return-object v3

    #@b
    .line 316
    :cond_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v5

    #@f
    .line 317
    .local v5, systemIconResourceId:I
    if-eqz v5, :cond_1b

    #@11
    .line 318
    new-instance v3, Landroid/view/PointerIcon;

    #@13
    const/4 v6, 0x0

    #@14
    invoke-direct {v3, v4, v6}, Landroid/view/PointerIcon;-><init>(ILandroid/view/PointerIcon$1;)V

    #@17
    .line 319
    .local v3, icon:Landroid/view/PointerIcon;
    invoke-static {v3, v5}, Landroid/view/PointerIcon;->access$102(Landroid/view/PointerIcon;I)I

    #@1a
    goto :goto_a

    #@1b
    .line 323
    .end local v3           #icon:Landroid/view/PointerIcon;
    :cond_1b
    sget-object v6, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v6, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/graphics/Bitmap;

    #@23
    .line 324
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@26
    move-result v1

    #@27
    .line 325
    .local v1, hotSpotX:F
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@2a
    move-result v2

    #@2b
    .line 326
    .local v2, hotSpotY:F
    invoke-static {v0, v1, v2}, Landroid/view/PointerIcon;->createCustomIcon(Landroid/graphics/Bitmap;FF)Landroid/view/PointerIcon;

    #@2e
    move-result-object v3

    #@2f
    goto :goto_a
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 309
    invoke-virtual {p0, p1}, Landroid/view/PointerIcon$1;->createFromParcel(Landroid/os/Parcel;)Landroid/view/PointerIcon;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/view/PointerIcon;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 330
    new-array v0, p1, [Landroid/view/PointerIcon;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 309
    invoke-virtual {p0, p1}, Landroid/view/PointerIcon$1;->newArray(I)[Landroid/view/PointerIcon;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
