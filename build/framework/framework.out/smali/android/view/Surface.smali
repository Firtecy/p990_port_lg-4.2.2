.class public Landroid/view/Surface;
.super Ljava/lang/Object;
.source "Surface.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/Surface$CompatibleCanvas;,
        Landroid/view/Surface$PhysicalDisplayInfo;,
        Landroid/view/Surface$OutOfResourcesException;
    }
.end annotation


# static fields
.field public static final BUILT_IN_DISPLAY_ID_HDMI:I = 0x1

.field public static final BUILT_IN_DISPLAY_ID_MAIN:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/Surface;",
            ">;"
        }
    .end annotation
.end field

.field public static final FX_SURFACE_BLUR:I = 0x10000
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FX_SURFACE_DIM:I = 0x20000

.field public static final FX_SURFACE_MASK:I = 0xf0000

.field public static final FX_SURFACE_NORMAL:I = 0x0

.field public static final FX_SURFACE_SCREENSHOT:I = 0x30000

#the value of this static final field might be set in the static constructor
.field private static final HEADLESS:Z = false

.field public static final HIDDEN:I = 0x4

.field public static final NON_PREMULTIPLIED:I = 0x100

.field public static final OPAQUE:I = 0x400

.field public static final PROTECTED_APP:I = 0x800

.field public static final ROTATION_0:I = 0x0

.field public static final ROTATION_180:I = 0x2

.field public static final ROTATION_270:I = 0x3

.field public static final ROTATION_90:I = 0x1

.field public static final SECURE:I = 0x80

.field public static final SURFACE_HIDDEN:I = 0x1

.field public static final SWIVEL_ROTATION_0:I = 0x4

.field public static final SWIVEL_ROTATION_90:I = 0x5

.field private static final TAG:Ljava/lang/String; = "Surface"


# instance fields
.field private final mCanvas:Landroid/graphics/Canvas;

.field private mCanvasSaveCount:I

.field private final mCloseGuard:Ldalvik/system/CloseGuard;

.field private mCompatibilityTranslator:Landroid/content/res/CompatibilityInfo$Translator;

.field private mCompatibleMatrix:Landroid/graphics/Matrix;

.field private mGenerationId:I

.field private mHeight:I

.field private mName:Ljava/lang/String;

.field private mNativeSurface:I

.field private mNativeSurfaceControl:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 40
    const-string v0, "1"

    #@2
    const-string/jumbo v1, "ro.config.headless"

    #@5
    const-string v2, "0"

    #@7
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    sput-boolean v0, Landroid/view/Surface;->HEADLESS:Z

    #@11
    .line 43
    new-instance v0, Landroid/view/Surface$1;

    #@13
    invoke-direct {v0}, Landroid/view/Surface$1;-><init>()V

    #@16
    sput-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 292
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 212
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 223
    new-instance v0, Landroid/view/Surface$CompatibleCanvas;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-direct {v0, p0, v1}, Landroid/view/Surface$CompatibleCanvas;-><init>(Landroid/view/Surface;Landroid/view/Surface$1;)V

    #@f
    iput-object v0, p0, Landroid/view/Surface;->mCanvas:Landroid/graphics/Canvas;

    #@11
    .line 293
    invoke-static {}, Landroid/view/Surface;->checkHeadless()V

    #@14
    .line 295
    iget-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@16
    const-string/jumbo v1, "release"

    #@19
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@1c
    .line 296
    return-void
.end method

.method public constructor <init>(Landroid/graphics/SurfaceTexture;)V
    .registers 5
    .parameter "surfaceTexture"

    #@0
    .prologue
    .line 359
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 212
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v1

    #@7
    iput-object v1, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 223
    new-instance v1, Landroid/view/Surface$CompatibleCanvas;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-direct {v1, p0, v2}, Landroid/view/Surface$CompatibleCanvas;-><init>(Landroid/view/Surface;Landroid/view/Surface$1;)V

    #@f
    iput-object v1, p0, Landroid/view/Surface;->mCanvas:Landroid/graphics/Canvas;

    #@11
    .line 360
    if-nez p1, :cond_1c

    #@13
    .line 361
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@15
    const-string/jumbo v2, "surfaceTexture must not be null"

    #@18
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1

    #@1c
    .line 364
    :cond_1c
    invoke-static {}, Landroid/view/Surface;->checkHeadless()V

    #@1f
    .line 366
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Landroid/view/Surface;->mName:Ljava/lang/String;

    #@25
    .line 368
    :try_start_25
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeCreateFromSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_28
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_25 .. :try_end_28} :catch_31

    #@28
    .line 374
    iget-object v1, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@2a
    const-string/jumbo v2, "release"

    #@2d
    invoke-virtual {v1, v2}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@30
    .line 375
    return-void

    #@31
    .line 369
    :catch_31
    move-exception v0

    #@32
    .line 371
    .local v0, ex:Landroid/view/Surface$OutOfResourcesException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@34
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@37
    throw v1
.end method

.method public constructor <init>(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V
    .registers 10
    .parameter "session"
    .parameter "name"
    .parameter "w"
    .parameter "h"
    .parameter "format"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    #@0
    .prologue
    .line 322
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 212
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 223
    new-instance v0, Landroid/view/Surface$CompatibleCanvas;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-direct {v0, p0, v1}, Landroid/view/Surface$CompatibleCanvas;-><init>(Landroid/view/Surface;Landroid/view/Surface$1;)V

    #@f
    iput-object v0, p0, Landroid/view/Surface;->mCanvas:Landroid/graphics/Canvas;

    #@11
    .line 323
    if-nez p1, :cond_1c

    #@13
    .line 324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string/jumbo v1, "session must not be null"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 326
    :cond_1c
    if-nez p2, :cond_27

    #@1e
    .line 327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@20
    const-string/jumbo v1, "name must not be null"

    #@23
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 330
    :cond_27
    and-int/lit8 v0, p6, 0x4

    #@29
    if-nez v0, :cond_48

    #@2b
    .line 331
    const-string v0, "Surface"

    #@2d
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "Surfaces should always be created with the HIDDEN flag set to ensure that they are not made visible prematurely before all of the surface\'s properties have been configured.  Set the other properties and make the surface visible within a transaction.  New surface name: "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    new-instance v2, Ljava/lang/Throwable;

    #@42
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@45
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    .line 339
    :cond_48
    invoke-static {}, Landroid/view/Surface;->checkHeadless()V

    #@4b
    .line 341
    iput-object p2, p0, Landroid/view/Surface;->mName:Ljava/lang/String;

    #@4d
    .line 342
    iput p3, p0, Landroid/view/Surface;->mWidth:I

    #@4f
    .line 343
    iput p4, p0, Landroid/view/Surface;->mHeight:I

    #@51
    .line 344
    invoke-direct/range {p0 .. p6}, Landroid/view/Surface;->nativeCreate(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V

    #@54
    .line 346
    iget-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@56
    const-string/jumbo v1, "release"

    #@59
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@5c
    .line 347
    return-void
.end method

.method static synthetic access$100(Landroid/view/Surface;)Landroid/content/res/CompatibilityInfo$Translator;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Landroid/view/Surface;->mCompatibilityTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/view/Surface;)Landroid/graphics/Matrix;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Landroid/view/Surface;->mCompatibleMatrix:Landroid/graphics/Matrix;

    #@2
    return-object v0
.end method

.method public static blankDisplay(Landroid/os/IBinder;)V
    .registers 3
    .parameter "displayToken"

    #@0
    .prologue
    .line 675
    if-nez p0, :cond_a

    #@2
    .line 676
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "displayToken must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 678
    :cond_a
    invoke-static {p0}, Landroid/view/Surface;->nativeBlankDisplay(Landroid/os/IBinder;)V

    #@d
    .line 679
    return-void
.end method

.method private static checkHeadless()V
    .registers 2

    #@0
    .prologue
    .line 757
    sget-boolean v0, Landroid/view/Surface;->HEADLESS:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 758
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@6
    const-string v1, "Device is headless"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 760
    :cond_c
    return-void
.end method

.method public static closeTransaction()V
    .registers 0

    #@0
    .prologue
    .line 538
    invoke-static {}, Landroid/view/Surface;->nativeCloseTransaction()V

    #@3
    .line 539
    return-void
.end method

.method public static createDisplay(Ljava/lang/String;Z)Landroid/os/IBinder;
    .registers 4
    .parameter "name"
    .parameter "secure"

    #@0
    .prologue
    .line 625
    if-nez p0, :cond_b

    #@2
    .line 626
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "name must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 628
    :cond_b
    invoke-static {p0, p1}, Landroid/view/Surface;->nativeCreateDisplay(Ljava/lang/String;Z)Landroid/os/IBinder;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public static getBuiltInDisplay(I)Landroid/os/IBinder;
    .registers 2
    .parameter "builtInDisplayId"

    #@0
    .prologue
    .line 620
    invoke-static {p0}, Landroid/view/Surface;->nativeGetBuiltInDisplay(I)Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getDisplayInfo(Landroid/os/IBinder;Landroid/view/Surface$PhysicalDisplayInfo;)Z
    .registers 4
    .parameter "displayToken"
    .parameter "outInfo"

    #@0
    .prologue
    .line 664
    if-nez p0, :cond_a

    #@2
    .line 665
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "displayToken must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 667
    :cond_a
    if-nez p1, :cond_15

    #@c
    .line 668
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v1, "outInfo must not be null"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 670
    :cond_15
    invoke-static {p0, p1}, Landroid/view/Surface;->nativeGetDisplayInfo(Landroid/os/IBinder;Landroid/view/Surface$PhysicalDisplayInfo;)Z

    #@18
    move-result v0

    #@19
    return v0
.end method

.method private static native nativeBlankDisplay(Landroid/os/IBinder;)V
.end method

.method private static native nativeCloseTransaction()V
.end method

.method private native nativeCopyFrom(Landroid/view/Surface;)V
.end method

.method private native nativeCreate(Landroid/view/SurfaceSession;Ljava/lang/String;IIII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation
.end method

.method private static native nativeCreateDisplay(Ljava/lang/String;Z)Landroid/os/IBinder;
.end method

.method private native nativeCreateFromSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation
.end method

.method private native nativeDestroy()V
.end method

.method private static native nativeGetBuiltInDisplay(I)Landroid/os/IBinder;
.end method

.method private static native nativeGetDisplayInfo(Landroid/os/IBinder;Landroid/view/Surface$PhysicalDisplayInfo;)Z
.end method

.method private native nativeGetIdentity()I
.end method

.method private native nativeIsConsumerRunningBehind()Z
.end method

.method private native nativeIsValid()Z
.end method

.method private native nativeLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
.end method

.method private static native nativeOpenTransaction()V
.end method

.method private native nativeReadFromParcel(Landroid/os/Parcel;)V
.end method

.method private native nativeRelease()V
.end method

.method private static native nativeScreenshot(Landroid/os/IBinder;IIIIZ)Landroid/graphics/Bitmap;
.end method

.method private native nativeSetAlpha(F)V
.end method

.method private static native nativeSetAnimationTransaction()V
.end method

.method private static native nativeSetDisplayLayerStack(Landroid/os/IBinder;I)V
.end method

.method private static native nativeSetDisplayProjection(Landroid/os/IBinder;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
.end method

.method private static native nativeSetDisplaySurface(Landroid/os/IBinder;Landroid/view/Surface;)V
.end method

.method private native nativeSetFlags(II)V
.end method

.method private native nativeSetLayer(I)V
.end method

.method private native nativeSetLayerStack(I)V
.end method

.method private native nativeSetMatrix(FFFF)V
.end method

.method private native nativeSetPosition(FF)V
.end method

.method private native nativeSetSize(II)V
.end method

.method private native nativeSetTransparentRegionHint(Landroid/graphics/Region;)V
.end method

.method private native nativeSetWindowCrop(Landroid/graphics/Rect;)V
.end method

.method private native nativeTransferFrom(Landroid/view/Surface;)V
.end method

.method private static native nativeUnblankDisplay(Landroid/os/IBinder;)V
.end method

.method private native nativeUnlockCanvasAndPost(Landroid/graphics/Canvas;)V
.end method

.method private native nativeWriteToParcel(Landroid/os/Parcel;)V
.end method

.method public static openTransaction()V
    .registers 0

    #@0
    .prologue
    .line 533
    invoke-static {}, Landroid/view/Surface;->nativeOpenTransaction()V

    #@3
    .line 534
    return-void
.end method

.method public static screenshot(II)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 500
    invoke-static {v3}, Landroid/view/Surface;->getBuiltInDisplay(I)Landroid/os/IBinder;

    #@4
    move-result-object v0

    #@5
    .line 501
    .local v0, displayToken:Landroid/os/IBinder;
    const/4 v5, 0x1

    #@6
    move v1, p0

    #@7
    move v2, p1

    #@8
    move v4, v3

    #@9
    invoke-static/range {v0 .. v5}, Landroid/view/Surface;->nativeScreenshot(Landroid/os/IBinder;IIIIZ)Landroid/graphics/Bitmap;

    #@c
    move-result-object v1

    #@d
    return-object v1
.end method

.method public static screenshot(IIII)Landroid/graphics/Bitmap;
    .registers 10
    .parameter "width"
    .parameter "height"
    .parameter "minLayer"
    .parameter "maxLayer"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 522
    invoke-static {v5}, Landroid/view/Surface;->getBuiltInDisplay(I)Landroid/os/IBinder;

    #@4
    move-result-object v0

    #@5
    .local v0, displayToken:Landroid/os/IBinder;
    move v1, p0

    #@6
    move v2, p1

    #@7
    move v3, p2

    #@8
    move v4, p3

    #@9
    .line 523
    invoke-static/range {v0 .. v5}, Landroid/view/Surface;->nativeScreenshot(Landroid/os/IBinder;IIIIZ)Landroid/graphics/Bitmap;

    #@c
    move-result-object v1

    #@d
    return-object v1
.end method

.method public static setAnimationTransaction()V
    .registers 0

    #@0
    .prologue
    .line 543
    invoke-static {}, Landroid/view/Surface;->nativeSetAnimationTransaction()V

    #@3
    .line 544
    return-void
.end method

.method public static setDisplayLayerStack(Landroid/os/IBinder;I)V
    .registers 4
    .parameter "displayToken"
    .parameter "layerStack"

    #@0
    .prologue
    .line 641
    if-nez p0, :cond_a

    #@2
    .line 642
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "displayToken must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 644
    :cond_a
    invoke-static {p0, p1}, Landroid/view/Surface;->nativeSetDisplayLayerStack(Landroid/os/IBinder;I)V

    #@d
    .line 645
    return-void
.end method

.method public static setDisplayProjection(Landroid/os/IBinder;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 6
    .parameter "displayToken"
    .parameter "orientation"
    .parameter "layerStackRect"
    .parameter "displayRect"

    #@0
    .prologue
    .line 650
    if-nez p0, :cond_a

    #@2
    .line 651
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "displayToken must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 653
    :cond_a
    if-nez p2, :cond_15

    #@c
    .line 654
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v1, "layerStackRect must not be null"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 656
    :cond_15
    if-nez p3, :cond_1f

    #@17
    .line 657
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v1, "displayRect must not be null"

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 659
    :cond_1f
    invoke-static {p0, p1, p2, p3}, Landroid/view/Surface;->nativeSetDisplayProjection(Landroid/os/IBinder;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@22
    .line 660
    return-void
.end method

.method public static setDisplaySurface(Landroid/os/IBinder;Landroid/view/Surface;)V
    .registers 4
    .parameter "displayToken"
    .parameter "surface"

    #@0
    .prologue
    .line 633
    if-nez p0, :cond_a

    #@2
    .line 634
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "displayToken must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 636
    :cond_a
    invoke-static {p0, p1}, Landroid/view/Surface;->nativeSetDisplaySurface(Landroid/os/IBinder;Landroid/view/Surface;)V

    #@d
    .line 637
    return-void
.end method

.method public static unblankDisplay(Landroid/os/IBinder;)V
    .registers 3
    .parameter "displayToken"

    #@0
    .prologue
    .line 683
    if-nez p0, :cond_a

    #@2
    .line 684
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "displayToken must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 686
    :cond_a
    invoke-static {p0}, Landroid/view/Surface;->nativeUnblankDisplay(Landroid/os/IBinder;)V

    #@d
    .line 687
    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/view/Surface;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 699
    if-nez p1, :cond_b

    #@2
    .line 700
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "other must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 702
    :cond_b
    if-eq p1, p0, :cond_10

    #@d
    .line 703
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeCopyFrom(Landroid/view/Surface;)V

    #@10
    .line 705
    :cond_10
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 726
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 406
    invoke-direct {p0}, Landroid/view/Surface;->nativeDestroy()V

    #@3
    .line 407
    iget-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@5
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@8
    .line 408
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 380
    :try_start_0
    iget-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 381
    iget-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@6
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    #@9
    .line 383
    :cond_9
    invoke-direct {p0}, Landroid/view/Surface;->nativeRelease()V
    :try_end_c
    .catchall {:try_start_0 .. :try_end_c} :catchall_10

    #@c
    .line 385
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@f
    .line 387
    return-void

    #@10
    .line 385
    :catchall_10
    move-exception v0

    #@11
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@14
    throw v0
.end method

.method public getGenerationId()I
    .registers 2

    #@0
    .prologue
    .line 428
    iget v0, p0, Landroid/view/Surface;->mGenerationId:I

    #@2
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 575
    iget v0, p0, Landroid/view/Surface;->mHeight:I

    #@2
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 570
    iget v0, p0, Landroid/view/Surface;->mWidth:I

    #@2
    return v0
.end method

.method public hide()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 580
    invoke-direct {p0, v0, v0}, Landroid/view/Surface;->nativeSetFlags(II)V

    #@4
    .line 581
    return-void
.end method

.method public isConsumerRunningBehind()Z
    .registers 2

    #@0
    .prologue
    .line 438
    invoke-direct {p0}, Landroid/view/Surface;->nativeIsConsumerRunningBehind()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isValid()Z
    .registers 2

    #@0
    .prologue
    .line 417
    invoke-direct {p0}, Landroid/view/Surface;->nativeIsValid()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 3
    .parameter "dirty"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 458
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 730
    if-nez p1, :cond_b

    #@2
    .line 731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "source must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 734
    :cond_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/view/Surface;->mName:Ljava/lang/String;

    #@11
    .line 735
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeReadFromParcel(Landroid/os/Parcel;)V

    #@14
    .line 736
    return-void
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 395
    invoke-direct {p0}, Landroid/view/Surface;->nativeRelease()V

    #@3
    .line 396
    iget-object v0, p0, Landroid/view/Surface;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@5
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@8
    .line 397
    return-void
.end method

.method public setAlpha(F)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 595
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeSetAlpha(F)V

    #@3
    .line 596
    return-void
.end method

.method setCompatibilityTranslator(Landroid/content/res/CompatibilityInfo$Translator;)V
    .registers 4
    .parameter "translator"

    #@0
    .prologue
    .line 484
    if-eqz p1, :cond_12

    #@2
    .line 485
    iget v0, p1, Landroid/content/res/CompatibilityInfo$Translator;->applicationScale:F

    #@4
    .line 486
    .local v0, appScale:F
    new-instance v1, Landroid/graphics/Matrix;

    #@6
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    #@9
    iput-object v1, p0, Landroid/view/Surface;->mCompatibleMatrix:Landroid/graphics/Matrix;

    #@b
    .line 487
    iget-object v1, p0, Landroid/view/Surface;->mCompatibleMatrix:Landroid/graphics/Matrix;

    #@d
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    #@10
    .line 488
    iput-object p1, p0, Landroid/view/Surface;->mCompatibilityTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@12
    .line 490
    .end local v0           #appScale:F
    :cond_12
    return-void
.end method

.method public setFlags(II)V
    .registers 3
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 605
    invoke-direct {p0, p1, p2}, Landroid/view/Surface;->nativeSetFlags(II)V

    #@3
    .line 606
    return-void
.end method

.method public setLayer(I)V
    .registers 2
    .parameter "zorder"

    #@0
    .prologue
    .line 548
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeSetLayer(I)V

    #@3
    .line 549
    return-void
.end method

.method public setLayerStack(I)V
    .registers 2
    .parameter "layerStack"

    #@0
    .prologue
    .line 615
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeSetLayerStack(I)V

    #@3
    .line 616
    return-void
.end method

.method public setMatrix(FFFF)V
    .registers 5
    .parameter "dsdx"
    .parameter "dtdx"
    .parameter "dsdy"
    .parameter "dtdy"

    #@0
    .prologue
    .line 600
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/Surface;->nativeSetMatrix(FFFF)V

    #@3
    .line 601
    return-void
.end method

.method public setPosition(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 558
    invoke-direct {p0, p1, p2}, Landroid/view/Surface;->nativeSetPosition(FF)V

    #@3
    .line 559
    return-void
.end method

.method public setPosition(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 553
    int-to-float v0, p1

    #@1
    int-to-float v1, p2

    #@2
    invoke-direct {p0, v0, v1}, Landroid/view/Surface;->nativeSetPosition(FF)V

    #@5
    .line 554
    return-void
.end method

.method public setSize(II)V
    .registers 3
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 563
    iput p1, p0, Landroid/view/Surface;->mWidth:I

    #@2
    .line 564
    iput p2, p0, Landroid/view/Surface;->mHeight:I

    #@4
    .line 565
    invoke-direct {p0, p1, p2}, Landroid/view/Surface;->nativeSetSize(II)V

    #@7
    .line 566
    return-void
.end method

.method public setTransparentRegionHint(Landroid/graphics/Region;)V
    .registers 2
    .parameter "region"

    #@0
    .prologue
    .line 590
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeSetTransparentRegionHint(Landroid/graphics/Region;)V

    #@3
    .line 591
    return-void
.end method

.method public setWindowCrop(Landroid/graphics/Rect;)V
    .registers 2
    .parameter "crop"

    #@0
    .prologue
    .line 610
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeSetWindowCrop(Landroid/graphics/Rect;)V

    #@3
    .line 611
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    .line 585
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, v0, v1}, Landroid/view/Surface;->nativeSetFlags(II)V

    #@5
    .line 586
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 753
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Surface(name="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/view/Surface;->mName:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", identity="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-direct {p0}, Landroid/view/Surface;->nativeGetIdentity()I

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, ")"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    return-object v0
.end method

.method public transferFrom(Landroid/view/Surface;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 716
    if-nez p1, :cond_b

    #@2
    .line 717
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "other must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 719
    :cond_b
    if-eq p1, p0, :cond_10

    #@d
    .line 720
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeTransferFrom(Landroid/view/Surface;)V

    #@10
    .line 722
    :cond_10
    return-void
.end method

.method public unlockCanvas(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 476
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 468
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeUnlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@3
    .line 469
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 740
    if-nez p1, :cond_a

    #@2
    .line 741
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "dest must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 744
    :cond_a
    iget-object v0, p0, Landroid/view/Surface;->mName:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 745
    invoke-direct {p0, p1}, Landroid/view/Surface;->nativeWriteToParcel(Landroid/os/Parcel;)V

    #@12
    .line 746
    and-int/lit8 v0, p2, 0x1

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 747
    invoke-virtual {p0}, Landroid/view/Surface;->release()V

    #@19
    .line 749
    :cond_19
    return-void
.end method
