.class public final Landroid/view/VelocityTracker;
.super Ljava/lang/Object;
.source "VelocityTracker.java"

# interfaces
.implements Landroid/util/Poolable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/VelocityTracker$Estimator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/util/Poolable",
        "<",
        "Landroid/view/VelocityTracker;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTIVE_POINTER_ID:I = -0x1

.field private static final sPool:Landroid/util/Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pool",
            "<",
            "Landroid/view/VelocityTracker;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIsPooled:Z

.field private mNext:Landroid/view/VelocityTracker;

.field private mPtr:I

.field private final mStrategy:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 35
    new-instance v0, Landroid/view/VelocityTracker$1;

    #@2
    invoke-direct {v0}, Landroid/view/VelocityTracker$1;-><init>()V

    #@5
    const/4 v1, 0x2

    #@6
    invoke-static {v0, v1}, Landroid/util/Pools;->finitePool(Landroid/util/PoolableManager;I)Landroid/util/Pool;

    #@9
    move-result-object v0

    #@a
    invoke-static {v0}, Landroid/util/Pools;->synchronizedPool(Landroid/util/Pool;)Landroid/util/Pool;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/view/VelocityTracker;->sPool:Landroid/util/Pool;

    #@10
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "strategy"

    #@0
    .prologue
    .line 133
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 134
    invoke-static {p1}, Landroid/view/VelocityTracker;->nativeInitialize(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@9
    .line 135
    iput-object p1, p0, Landroid/view/VelocityTracker;->mStrategy:Ljava/lang/String;

    #@b
    .line 136
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/view/VelocityTracker$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/view/VelocityTracker;-><init>(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private static native nativeAddMovement(ILandroid/view/MotionEvent;)V
.end method

.method private static native nativeClear(I)V
.end method

.method private static native nativeComputeCurrentVelocity(IIF)V
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeGetEstimator(IILandroid/view/VelocityTracker$Estimator;)Z
.end method

.method private static native nativeGetXVelocity(II)F
.end method

.method private static native nativeGetYVelocity(II)F
.end method

.method private static native nativeInitialize(Ljava/lang/String;)I
.end method

.method public static obtain()Landroid/view/VelocityTracker;
    .registers 1

    #@0
    .prologue
    .line 76
    sget-object v0, Landroid/view/VelocityTracker;->sPool:Landroid/util/Pool;

    #@2
    invoke-interface {v0}, Landroid/util/Pool;->acquire()Landroid/util/Poolable;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/VelocityTracker;

    #@8
    return-object v0
.end method

.method public static obtain(Ljava/lang/String;)Landroid/view/VelocityTracker;
    .registers 2
    .parameter "strategy"

    #@0
    .prologue
    .line 89
    if-nez p0, :cond_7

    #@2
    .line 90
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@5
    move-result-object v0

    #@6
    .line 92
    :goto_6
    return-object v0

    #@7
    :cond_7
    new-instance v0, Landroid/view/VelocityTracker;

    #@9
    invoke-direct {v0, p0}, Landroid/view/VelocityTracker;-><init>(Ljava/lang/String;)V

    #@c
    goto :goto_6
.end method


# virtual methods
.method public addMovement(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 167
    if-nez p1, :cond_a

    #@2
    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "event must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 170
    :cond_a
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@c
    invoke-static {v0, p1}, Landroid/view/VelocityTracker;->nativeAddMovement(ILandroid/view/MotionEvent;)V

    #@f
    .line 171
    return-void
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 154
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    invoke-static {v0}, Landroid/view/VelocityTracker;->nativeClear(I)V

    #@5
    .line 155
    return-void
.end method

.method public computeCurrentVelocity(I)V
    .registers 4
    .parameter "units"

    #@0
    .prologue
    .line 180
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    const v1, 0x7f7fffff

    #@5
    invoke-static {v0, p1, v1}, Landroid/view/VelocityTracker;->nativeComputeCurrentVelocity(IIF)V

    #@8
    .line 181
    return-void
.end method

.method public computeCurrentVelocity(IF)V
    .registers 4
    .parameter "units"
    .parameter "maxVelocity"

    #@0
    .prologue
    .line 197
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/view/VelocityTracker;->nativeComputeCurrentVelocity(IIF)V

    #@5
    .line 198
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 141
    :try_start_0
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 142
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@6
    invoke-static {v0}, Landroid/view/VelocityTracker;->nativeDispose(I)V

    #@9
    .line 143
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/view/VelocityTracker;->mPtr:I
    :try_end_c
    .catchall {:try_start_0 .. :try_end_c} :catchall_10

    #@c
    .line 146
    :cond_c
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@f
    .line 148
    return-void

    #@10
    .line 146
    :catchall_10
    move-exception v0

    #@11
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@14
    throw v0
.end method

.method public getEstimator(ILandroid/view/VelocityTracker$Estimator;)Z
    .registers 5
    .parameter "id"
    .parameter "outEstimator"

    #@0
    .prologue
    .line 257
    if-nez p2, :cond_b

    #@2
    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "outEstimator must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 260
    :cond_b
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@d
    invoke-static {v0, p1, p2}, Landroid/view/VelocityTracker;->nativeGetEstimator(IILandroid/view/VelocityTracker$Estimator;)Z

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public getNextPoolable()Landroid/view/VelocityTracker;
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/view/VelocityTracker;->mNext:Landroid/view/VelocityTracker;

    #@2
    return-object v0
.end method

.method public bridge synthetic getNextPoolable()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-virtual {p0}, Landroid/view/VelocityTracker;->getNextPoolable()Landroid/view/VelocityTracker;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getXVelocity()F
    .registers 3

    #@0
    .prologue
    .line 207
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    const/4 v1, -0x1

    #@3
    invoke-static {v0, v1}, Landroid/view/VelocityTracker;->nativeGetXVelocity(II)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getXVelocity(I)F
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 228
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/VelocityTracker;->nativeGetXVelocity(II)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getYVelocity()F
    .registers 3

    #@0
    .prologue
    .line 217
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    const/4 v1, -0x1

    #@3
    invoke-static {v0, v1}, Landroid/view/VelocityTracker;->nativeGetYVelocity(II)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getYVelocity(I)F
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 239
    iget v0, p0, Landroid/view/VelocityTracker;->mPtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/VelocityTracker;->nativeGetYVelocity(II)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPooled()Z
    .registers 2

    #@0
    .prologue
    .line 123
    iget-boolean v0, p0, Landroid/view/VelocityTracker;->mIsPooled:Z

    #@2
    return v0
.end method

.method public recycle()V
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/view/VelocityTracker;->mStrategy:Ljava/lang/String;

    #@2
    if-nez v0, :cond_9

    #@4
    .line 101
    sget-object v0, Landroid/view/VelocityTracker;->sPool:Landroid/util/Pool;

    #@6
    invoke-interface {v0, p0}, Landroid/util/Pool;->release(Landroid/util/Poolable;)V

    #@9
    .line 103
    :cond_9
    return-void
.end method

.method public setNextPoolable(Landroid/view/VelocityTracker;)V
    .registers 2
    .parameter "element"

    #@0
    .prologue
    .line 109
    iput-object p1, p0, Landroid/view/VelocityTracker;->mNext:Landroid/view/VelocityTracker;

    #@2
    .line 110
    return-void
.end method

.method public bridge synthetic setNextPoolable(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    check-cast p1, Landroid/view/VelocityTracker;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/VelocityTracker;->setNextPoolable(Landroid/view/VelocityTracker;)V

    #@5
    return-void
.end method

.method public setPooled(Z)V
    .registers 2
    .parameter "isPooled"

    #@0
    .prologue
    .line 130
    iput-boolean p1, p0, Landroid/view/VelocityTracker;->mIsPooled:Z

    #@2
    .line 131
    return-void
.end method
