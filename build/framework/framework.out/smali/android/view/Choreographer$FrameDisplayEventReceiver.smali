.class final Landroid/view/Choreographer$FrameDisplayEventReceiver;
.super Landroid/view/DisplayEventReceiver;
.source "Choreographer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Choreographer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FrameDisplayEventReceiver"
.end annotation


# instance fields
.field private mFrame:I

.field private mHavePendingVsync:Z

.field private mTimestampNanos:J

.field final synthetic this$0:Landroid/view/Choreographer;


# direct methods
.method public constructor <init>(Landroid/view/Choreographer;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 682
    iput-object p1, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->this$0:Landroid/view/Choreographer;

    #@2
    .line 683
    invoke-direct {p0, p2}, Landroid/view/DisplayEventReceiver;-><init>(Landroid/os/Looper;)V

    #@5
    .line 684
    return-void
.end method


# virtual methods
.method public onVsync(JII)V
    .registers 13
    .parameter "timestampNanos"
    .parameter "builtInDisplayId"
    .parameter "frame"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 696
    if-eqz p3, :cond_e

    #@3
    .line 697
    const-string v3, "Choreographer"

    #@5
    const-string v4, "Received vsync from secondary display, but we don\'t support this case yet.  Choreographer needs a way to explicitly request vsync for a specific display to ensure it doesn\'t lose track of its scheduled vsync."

    #@7
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 701
    invoke-virtual {p0}, Landroid/view/Choreographer$FrameDisplayEventReceiver;->scheduleVsync()V

    #@d
    .line 730
    :goto_d
    return-void

    #@e
    .line 710
    :cond_e
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@11
    move-result-wide v1

    #@12
    .line 711
    .local v1, now:J
    cmp-long v3, p1, v1

    #@14
    if-lez v3, :cond_43

    #@16
    .line 712
    const-string v3, "Choreographer"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Frame time is "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    sub-long v5, p1, v1

    #@25
    long-to-float v5, v5

    #@26
    const v6, 0x358637bd

    #@29
    mul-float/2addr v5, v6

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, " ms in the future!  Check that graphics HAL is generating vsync "

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string/jumbo v5, "timestamps using the correct timebase."

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 715
    move-wide p1, v1

    #@43
    .line 718
    :cond_43
    iget-boolean v3, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mHavePendingVsync:Z

    #@45
    if-eqz v3, :cond_6e

    #@47
    .line 719
    const-string v3, "Choreographer"

    #@49
    const-string v4, "Already have a pending vsync event.  There should only be one at a time."

    #@4b
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 725
    :goto_4e
    iput-wide p1, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mTimestampNanos:J

    #@50
    .line 726
    iput p4, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mFrame:I

    #@52
    .line 727
    iget-object v3, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->this$0:Landroid/view/Choreographer;

    #@54
    invoke-static {v3}, Landroid/view/Choreographer;->access$300(Landroid/view/Choreographer;)Landroid/view/Choreographer$FrameHandler;

    #@57
    move-result-object v3

    #@58
    invoke-static {v3, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    #@5b
    move-result-object v0

    #@5c
    .line 728
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v7}, Landroid/os/Message;->setAsynchronous(Z)V

    #@5f
    .line 729
    iget-object v3, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->this$0:Landroid/view/Choreographer;

    #@61
    invoke-static {v3}, Landroid/view/Choreographer;->access$300(Landroid/view/Choreographer;)Landroid/view/Choreographer$FrameHandler;

    #@64
    move-result-object v3

    #@65
    const-wide/32 v4, 0xf4240

    #@68
    div-long v4, p1, v4

    #@6a
    invoke-virtual {v3, v0, v4, v5}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@6d
    goto :goto_d

    #@6e
    .line 722
    .end local v0           #msg:Landroid/os/Message;
    :cond_6e
    iput-boolean v7, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mHavePendingVsync:Z

    #@70
    goto :goto_4e
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 734
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mHavePendingVsync:Z

    #@3
    .line 735
    iget-object v0, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->this$0:Landroid/view/Choreographer;

    #@5
    iget-wide v1, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mTimestampNanos:J

    #@7
    iget v3, p0, Landroid/view/Choreographer$FrameDisplayEventReceiver;->mFrame:I

    #@9
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/Choreographer;->doFrame(JI)V

    #@c
    .line 736
    return-void
.end method
