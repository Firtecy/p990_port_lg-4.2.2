.class Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;
.super Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;
.source "AccessibilityIterators.java"

# interfaces
.implements Landroid/content/ComponentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/AccessibilityIterators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CharacterTextSegmentIterator"
.end annotation


# static fields
.field private static sInstance:Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;


# instance fields
.field protected mImpl:Ljava/text/BreakIterator;

.field private mLocale:Ljava/util/Locale;


# direct methods
.method private constructor <init>(Ljava/util/Locale;)V
    .registers 2
    .parameter "locale"

    #@0
    .prologue
    .line 84
    invoke-direct {p0}, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;-><init>()V

    #@3
    .line 85
    iput-object p1, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mLocale:Ljava/util/Locale;

    #@5
    .line 86
    invoke-virtual {p0, p1}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->onLocaleChanged(Ljava/util/Locale;)V

    #@8
    .line 87
    invoke-static {p0}, Landroid/view/ViewRootImpl;->addConfigCallback(Landroid/content/ComponentCallbacks;)V

    #@b
    .line 88
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Locale;Landroid/view/AccessibilityIterators$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;-><init>(Ljava/util/Locale;)V

    #@3
    return-void
.end method

.method public static getInstance(Ljava/util/Locale;)Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;
    .registers 2
    .parameter "locale"

    #@0
    .prologue
    .line 78
    sget-object v0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 79
    new-instance v0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;

    #@6
    invoke-direct {v0, p0}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;-><init>(Ljava/util/Locale;)V

    #@9
    sput-object v0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;

    #@b
    .line 81
    :cond_b
    sget-object v0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->sInstance:Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;

    #@d
    return-object v0
.end method


# virtual methods
.method public following(I)[I
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 98
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 99
    .local v2, textLegth:I
    if-gtz v2, :cond_b

    #@a
    .line 119
    :cond_a
    :goto_a
    return-object v3

    #@b
    .line 102
    :cond_b
    if-ge p1, v2, :cond_a

    #@d
    .line 105
    move v1, p1

    #@e
    .line 106
    .local v1, start:I
    if-gez v1, :cond_11

    #@10
    .line 107
    const/4 v1, 0x0

    #@11
    .line 109
    :cond_11
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@13
    invoke-virtual {v4, v1}, Ljava/text/BreakIterator;->isBoundary(I)Z

    #@16
    move-result v4

    #@17
    if-nez v4, :cond_22

    #@19
    .line 110
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@1b
    invoke-virtual {v4, v1}, Ljava/text/BreakIterator;->following(I)I

    #@1e
    move-result v1

    #@1f
    .line 111
    if-ne v1, v5, :cond_11

    #@21
    goto :goto_a

    #@22
    .line 115
    :cond_22
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@24
    invoke-virtual {v4, v1}, Ljava/text/BreakIterator;->following(I)I

    #@27
    move-result v0

    #@28
    .line 116
    .local v0, end:I
    if-eq v0, v5, :cond_a

    #@2a
    .line 119
    invoke-virtual {p0, v1, v0}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->getRange(II)[I

    #@2d
    move-result-object v3

    #@2e
    goto :goto_a
.end method

.method public initialize(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->initialize(Ljava/lang/String;)V

    #@3
    .line 93
    iget-object v0, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@5
    invoke-virtual {v0, p1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    #@8
    .line 94
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 150
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@2
    .line 151
    .local v0, locale:Ljava/util/Locale;
    iget-object v1, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mLocale:Ljava/util/Locale;

    #@4
    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_f

    #@a
    .line 152
    iput-object v0, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mLocale:Ljava/util/Locale;

    #@c
    .line 153
    invoke-virtual {p0, v0}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->onLocaleChanged(Ljava/util/Locale;)V

    #@f
    .line 155
    :cond_f
    return-void
.end method

.method protected onLocaleChanged(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 163
    invoke-static {p1}, Ljava/text/BreakIterator;->getCharacterInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@6
    .line 164
    return-void
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 160
    return-void
.end method

.method public preceding(I)[I
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 124
    iget-object v4, p0, Landroid/view/AccessibilityIterators$AbstractTextSegmentIterator;->mText:Ljava/lang/String;

    #@4
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 125
    .local v2, textLegth:I
    if-gtz v2, :cond_b

    #@a
    .line 145
    :cond_a
    :goto_a
    return-object v3

    #@b
    .line 128
    :cond_b
    if-lez p1, :cond_a

    #@d
    .line 131
    move v0, p1

    #@e
    .line 132
    .local v0, end:I
    if-le v0, v2, :cond_11

    #@10
    .line 133
    move v0, v2

    #@11
    .line 135
    :cond_11
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@13
    invoke-virtual {v4, v0}, Ljava/text/BreakIterator;->isBoundary(I)Z

    #@16
    move-result v4

    #@17
    if-nez v4, :cond_22

    #@19
    .line 136
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@1b
    invoke-virtual {v4, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@1e
    move-result v0

    #@1f
    .line 137
    if-ne v0, v5, :cond_11

    #@21
    goto :goto_a

    #@22
    .line 141
    :cond_22
    iget-object v4, p0, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->mImpl:Ljava/text/BreakIterator;

    #@24
    invoke-virtual {v4, v0}, Ljava/text/BreakIterator;->preceding(I)I

    #@27
    move-result v1

    #@28
    .line 142
    .local v1, start:I
    if-eq v1, v5, :cond_a

    #@2a
    .line 145
    invoke-virtual {p0, v1, v0}, Landroid/view/AccessibilityIterators$CharacterTextSegmentIterator;->getRange(II)[I

    #@2d
    move-result-object v3

    #@2e
    goto :goto_a
.end method
