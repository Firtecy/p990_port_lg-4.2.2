.class Landroid/view/GLES20RenderLayer;
.super Landroid/view/GLES20Layer;
.source "GLES20RenderLayer.java"


# instance fields
.field private final mCanvas:Landroid/view/GLES20Canvas;

.field private mLayerHeight:I

.field private mLayerWidth:I


# direct methods
.method constructor <init>(IIZ)V
    .registers 9
    .parameter "width"
    .parameter "height"
    .parameter "isOpaque"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/view/GLES20Layer;-><init>(IIZ)V

    #@6
    .line 37
    const/4 v3, 0x2

    #@7
    new-array v0, v3, [I

    #@9
    .line 38
    .local v0, layerInfo:[I
    invoke-static {p1, p2, p3, v0}, Landroid/view/GLES20Canvas;->nCreateLayer(IIZ[I)I

    #@c
    move-result v3

    #@d
    iput v3, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@f
    .line 39
    iget v3, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@11
    if-eqz v3, :cond_32

    #@13
    .line 40
    aget v3, v0, v2

    #@15
    iput v3, p0, Landroid/view/GLES20RenderLayer;->mLayerWidth:I

    #@17
    .line 41
    aget v3, v0, v1

    #@19
    iput v3, p0, Landroid/view/GLES20RenderLayer;->mLayerHeight:I

    #@1b
    .line 43
    new-instance v3, Landroid/view/GLES20Canvas;

    #@1d
    iget v4, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@1f
    if-nez p3, :cond_30

    #@21
    :goto_21
    invoke-direct {v3, v4, v1}, Landroid/view/GLES20Canvas;-><init>(IZ)V

    #@24
    iput-object v3, p0, Landroid/view/GLES20RenderLayer;->mCanvas:Landroid/view/GLES20Canvas;

    #@26
    .line 44
    new-instance v1, Landroid/view/GLES20Layer$Finalizer;

    #@28
    iget v2, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2a
    invoke-direct {v1, v2}, Landroid/view/GLES20Layer$Finalizer;-><init>(I)V

    #@2d
    iput-object v1, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@2f
    .line 49
    :goto_2f
    return-void

    #@30
    :cond_30
    move v1, v2

    #@31
    .line 43
    goto :goto_21

    #@32
    .line 46
    :cond_32
    iput-object v4, p0, Landroid/view/GLES20RenderLayer;->mCanvas:Landroid/view/GLES20Canvas;

    #@34
    .line 47
    iput-object v4, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@36
    goto :goto_2f
.end method


# virtual methods
.method end(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "currentCanvas"

    #@0
    .prologue
    .line 92
    instance-of v0, p1, Landroid/view/GLES20Canvas;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 93
    check-cast p1, Landroid/view/GLES20Canvas;

    #@6
    .end local p1
    invoke-virtual {p1}, Landroid/view/GLES20Canvas;->resume()V

    #@9
    .line 95
    :cond_9
    return-void
.end method

.method getCanvas()Landroid/view/HardwareCanvas;
    .registers 2

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Landroid/view/GLES20RenderLayer;->mCanvas:Landroid/view/GLES20Canvas;

    #@2
    return-object v0
.end method

.method isValid()Z
    .registers 2

    #@0
    .prologue
    .line 53
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    if-eqz v0, :cond_e

    #@4
    iget v0, p0, Landroid/view/GLES20RenderLayer;->mLayerWidth:I

    #@6
    if-lez v0, :cond_e

    #@8
    iget v0, p0, Landroid/view/GLES20RenderLayer;->mLayerHeight:I

    #@a
    if-lez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method redrawLater(Landroid/view/DisplayList;Landroid/graphics/Rect;)V
    .registers 10
    .parameter "displayList"
    .parameter "dirtyRect"

    #@0
    .prologue
    .line 114
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    iget-object v1, p0, Landroid/view/GLES20RenderLayer;->mCanvas:Landroid/view/GLES20Canvas;

    #@4
    invoke-virtual {v1}, Landroid/view/GLES20Canvas;->getRenderer()I

    #@7
    move-result v1

    #@8
    check-cast p1, Landroid/view/GLES20DisplayList;

    #@a
    .end local p1
    invoke-virtual {p1}, Landroid/view/GLES20DisplayList;->getNativeDisplayList()I

    #@d
    move-result v2

    #@e
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@10
    iget v4, p2, Landroid/graphics/Rect;->top:I

    #@12
    iget v5, p2, Landroid/graphics/Rect;->right:I

    #@14
    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    #@16
    invoke-static/range {v0 .. v6}, Landroid/view/GLES20Canvas;->nUpdateRenderLayer(IIIIIII)V

    #@19
    .line 117
    return-void
.end method

.method resize(II)Z
    .registers 6
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 58
    invoke-virtual {p0}, Landroid/view/GLES20RenderLayer;->isValid()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_b

    #@7
    if-lez p1, :cond_b

    #@9
    if-gtz p2, :cond_c

    #@b
    .line 76
    :cond_b
    :goto_b
    return v1

    #@c
    .line 60
    :cond_c
    iput p1, p0, Landroid/view/HardwareLayer;->mWidth:I

    #@e
    .line 61
    iput p2, p0, Landroid/view/HardwareLayer;->mHeight:I

    #@10
    .line 63
    iget v2, p0, Landroid/view/GLES20RenderLayer;->mLayerWidth:I

    #@12
    if-ne p1, v2, :cond_18

    #@14
    iget v2, p0, Landroid/view/GLES20RenderLayer;->mLayerHeight:I

    #@16
    if-eq p2, v2, :cond_2c

    #@18
    .line 64
    :cond_18
    const/4 v2, 0x2

    #@19
    new-array v0, v2, [I

    #@1b
    .line 66
    .local v0, layerInfo:[I
    iget v2, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@1d
    invoke-static {v2, p1, p2, v0}, Landroid/view/GLES20Canvas;->nResizeLayer(III[I)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_31

    #@23
    .line 67
    aget v1, v0, v1

    #@25
    iput v1, p0, Landroid/view/GLES20RenderLayer;->mLayerWidth:I

    #@27
    .line 68
    const/4 v1, 0x1

    #@28
    aget v1, v0, v1

    #@2a
    iput v1, p0, Landroid/view/GLES20RenderLayer;->mLayerHeight:I

    #@2c
    .line 76
    .end local v0           #layerInfo:[I
    :cond_2c
    :goto_2c
    invoke-virtual {p0}, Landroid/view/GLES20RenderLayer;->isValid()Z

    #@2f
    move-result v1

    #@30
    goto :goto_b

    #@31
    .line 71
    .restart local v0       #layerInfo:[I
    :cond_31
    iput v1, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@33
    .line 72
    iput v1, p0, Landroid/view/GLES20RenderLayer;->mLayerWidth:I

    #@35
    .line 73
    iput v1, p0, Landroid/view/GLES20RenderLayer;->mLayerHeight:I

    #@37
    goto :goto_2c
.end method

.method setOpaque(Z)V
    .registers 3
    .parameter "isOpaque"

    #@0
    .prologue
    .line 81
    iput-boolean p1, p0, Landroid/view/HardwareLayer;->mOpaque:Z

    #@2
    .line 82
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@4
    invoke-static {v0, p1}, Landroid/view/GLES20Canvas;->nSetOpaqueLayer(IZ)V

    #@7
    .line 83
    return-void
.end method

.method setTransform(Landroid/graphics/Matrix;)V
    .registers 2
    .parameter "matrix"

    #@0
    .prologue
    .line 110
    return-void
.end method

.method start(Landroid/graphics/Canvas;)Landroid/view/HardwareCanvas;
    .registers 3
    .parameter "currentCanvas"

    #@0
    .prologue
    .line 99
    instance-of v0, p1, Landroid/view/GLES20Canvas;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 100
    check-cast p1, Landroid/view/GLES20Canvas;

    #@6
    .end local p1
    invoke-virtual {p1}, Landroid/view/GLES20Canvas;->interrupt()V

    #@9
    .line 102
    :cond_9
    invoke-virtual {p0}, Landroid/view/GLES20RenderLayer;->getCanvas()Landroid/view/HardwareCanvas;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method
