.class Landroid/view/SurfaceView$MyWindow;
.super Lcom/android/internal/view/BaseIWindow;
.source "SurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/SurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyWindow"
.end annotation


# instance fields
.field mCurHeight:I

.field mCurWidth:I

.field private final mSurfaceView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/SurfaceView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/SurfaceView;)V
    .registers 3
    .parameter "surfaceView"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 787
    invoke-direct {p0}, Lcom/android/internal/view/BaseIWindow;-><init>()V

    #@4
    .line 849
    iput v0, p0, Landroid/view/SurfaceView$MyWindow;->mCurWidth:I

    #@6
    .line 850
    iput v0, p0, Landroid/view/SurfaceView$MyWindow;->mCurHeight:I

    #@8
    .line 788
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@a
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@d
    iput-object v0, p0, Landroid/view/SurfaceView$MyWindow;->mSurfaceView:Ljava/lang/ref/WeakReference;

    #@f
    .line 789
    return-void
.end method


# virtual methods
.method public dispatchAppVisibility(Z)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 832
    return-void
.end method

.method public dispatchGetNewSurface()V
    .registers 5

    #@0
    .prologue
    .line 835
    iget-object v2, p0, Landroid/view/SurfaceView$MyWindow;->mSurfaceView:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/view/SurfaceView;

    #@8
    .line 836
    .local v1, surfaceView:Landroid/view/SurfaceView;
    if-eqz v1, :cond_16

    #@a
    .line 837
    iget-object v2, v1, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@c
    const/4 v3, 0x2

    #@d
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    .line 838
    .local v0, msg:Landroid/os/Message;
    iget-object v2, v1, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@13
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@16
    .line 840
    .end local v0           #msg:Landroid/os/Message;
    :cond_16
    return-void
.end method

.method public executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .registers 4
    .parameter "command"
    .parameter "parameters"
    .parameter "out"

    #@0
    .prologue
    .line 847
    return-void
.end method

.method public resized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V
    .registers 13
    .parameter "frame"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "reportDraw"
    .parameter "newConfig"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 794
    iget-object v5, p0, Landroid/view/SurfaceView$MyWindow;->mSurfaceView:Ljava/lang/ref/WeakReference;

    #@4
    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/view/SurfaceView;

    #@a
    .line 795
    .local v2, surfaceView:Landroid/view/SurfaceView;
    if-eqz v2, :cond_2a

    #@c
    .line 797
    invoke-virtual {v2}, Landroid/view/SurfaceView;->isLockScreen()Z

    #@f
    move-result v5

    #@10
    if-nez v5, :cond_54

    #@12
    .line 801
    iget-object v3, v2, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@14
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@17
    .line 803
    if-eqz p4, :cond_2b

    #@19
    .line 804
    const/4 v3, 0x1

    #@1a
    :try_start_1a
    iput-boolean v3, v2, Landroid/view/SurfaceView;->mUpdateWindowNeeded:Z

    #@1c
    .line 805
    const/4 v3, 0x1

    #@1d
    iput-boolean v3, v2, Landroid/view/SurfaceView;->mReportDrawNeeded:Z

    #@1f
    .line 806
    iget-object v3, v2, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@21
    const/4 v4, 0x3

    #@22
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_25
    .catchall {:try_start_1a .. :try_end_25} :catchall_4d

    #@25
    .line 813
    :cond_25
    :goto_25
    iget-object v3, v2, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@27
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@2a
    .line 828
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 807
    :cond_2b
    :try_start_2b
    iget-object v3, v2, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@2d
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    #@30
    move-result v3

    #@31
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@34
    move-result v4

    #@35
    if-ne v3, v4, :cond_43

    #@37
    iget-object v3, v2, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@39
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    #@3c
    move-result v3

    #@3d
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@40
    move-result v4

    #@41
    if-eq v3, v4, :cond_25

    #@43
    .line 809
    :cond_43
    const/4 v3, 0x1

    #@44
    iput-boolean v3, v2, Landroid/view/SurfaceView;->mUpdateWindowNeeded:Z

    #@46
    .line 810
    iget-object v3, v2, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@48
    const/4 v4, 0x3

    #@49
    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_4c
    .catchall {:try_start_2b .. :try_end_4c} :catchall_4d

    #@4c
    goto :goto_25

    #@4d
    .line 813
    :catchall_4d
    move-exception v3

    #@4e
    iget-object v4, v2, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@50
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@53
    throw v3

    #@54
    .line 817
    :cond_54
    if-nez p4, :cond_6e

    #@56
    iget-object v5, v2, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@58
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    #@5b
    move-result v5

    #@5c
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@5f
    move-result v6

    #@60
    if-ne v5, v6, :cond_6e

    #@62
    iget-object v5, v2, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@64
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    #@67
    move-result v5

    #@68
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@6b
    move-result v6

    #@6c
    if-eq v5, v6, :cond_89

    #@6e
    :cond_6e
    move v1, v4

    #@6f
    .line 818
    .local v1, needToUpdate:Z
    :goto_6f
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@72
    move-result-object v0

    #@73
    .line 819
    .local v0, msg:Landroid/os/Message;
    const/4 v5, 0x4

    #@74
    iput v5, v0, Landroid/os/Message;->what:I

    #@76
    .line 820
    if-eqz p4, :cond_8b

    #@78
    move v5, v4

    #@79
    :goto_79
    iput v5, v0, Landroid/os/Message;->arg1:I

    #@7b
    .line 821
    if-eqz v1, :cond_8d

    #@7d
    :goto_7d
    iput v4, v0, Landroid/os/Message;->arg2:I

    #@7f
    .line 822
    if-eqz v1, :cond_2a

    #@81
    .line 823
    iget-object v3, v2, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@83
    const-wide/16 v4, 0x0

    #@85
    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@88
    goto :goto_2a

    #@89
    .end local v0           #msg:Landroid/os/Message;
    .end local v1           #needToUpdate:Z
    :cond_89
    move v1, v3

    #@8a
    .line 817
    goto :goto_6f

    #@8b
    .restart local v0       #msg:Landroid/os/Message;
    .restart local v1       #needToUpdate:Z
    :cond_8b
    move v5, v3

    #@8c
    .line 820
    goto :goto_79

    #@8d
    :cond_8d
    move v4, v3

    #@8e
    .line 821
    goto :goto_7d
.end method

.method public windowFocusChanged(ZZ)V
    .registers 6
    .parameter "hasFocus"
    .parameter "touchEnabled"

    #@0
    .prologue
    .line 843
    const-string v0, "SurfaceView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Unexpected focus in surface: focus="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", touchEnabled="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 844
    return-void
.end method
