.class public final Landroid/view/DisplayInfo;
.super Ljava/lang/Object;
.source "DisplayInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/DisplayInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public address:Ljava/lang/String;

.field public appHeight:I

.field public appWidth:I

.field public flags:I

.field public largestNominalAppHeight:I

.field public largestNominalAppWidth:I

.field public layerStack:I

.field public logicalDensityDpi:I

.field public logicalHeight:I

.field public logicalWidth:I

.field public name:Ljava/lang/String;

.field public physicalXDpi:F

.field public physicalYDpi:F

.field public refreshRate:F

.field public rotation:I

.field public smallestNominalAppHeight:I

.field public smallestNominalAppWidth:I

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 156
    new-instance v0, Landroid/view/DisplayInfo$1;

    #@2
    invoke-direct {v0}, Landroid/view/DisplayInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/DisplayInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 168
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 169
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 175
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 176
    invoke-virtual {p0, p1}, Landroid/view/DisplayInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 177
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/view/DisplayInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/view/DisplayInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/view/DisplayInfo;)V
    .registers 2
    .parameter "other"

    #@0
    .prologue
    .line 171
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 172
    invoke-virtual {p0, p1}, Landroid/view/DisplayInfo;->copyFrom(Landroid/view/DisplayInfo;)V

    #@6
    .line 173
    return-void
.end method

.method private static flagsToString(I)Ljava/lang/String;
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 337
    .local v0, result:Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x2

    #@7
    if-eqz v1, :cond_e

    #@9
    .line 338
    const-string v1, ", FLAG_SECURE"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 340
    :cond_e
    and-int/lit8 v1, p0, 0x1

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 341
    const-string v1, ", FLAG_SUPPORTS_PROTECTED_BUFFERS"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 343
    :cond_17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    return-object v1
.end method

.method private getMetricsWithSize(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;II)V
    .registers 8
    .parameter "outMetrics"
    .parameter "cih"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 300
    iget v1, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@2
    iput v1, p1, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@4
    iput v1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@6
    .line 301
    iput p3, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@8
    iput p3, p1, Landroid/util/DisplayMetrics;->noncompatWidthPixels:I

    #@a
    .line 302
    iput p4, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@c
    iput p4, p1, Landroid/util/DisplayMetrics;->noncompatHeightPixels:I

    #@e
    .line 304
    iget v1, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@10
    int-to-float v1, v1

    #@11
    const v2, 0x3bcccccd

    #@14
    mul-float/2addr v1, v2

    #@15
    iput v1, p1, Landroid/util/DisplayMetrics;->noncompatDensity:F

    #@17
    iput v1, p1, Landroid/util/DisplayMetrics;->density:F

    #@19
    .line 306
    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    #@1b
    iput v1, p1, Landroid/util/DisplayMetrics;->noncompatScaledDensity:F

    #@1d
    iput v1, p1, Landroid/util/DisplayMetrics;->scaledDensity:F

    #@1f
    .line 307
    iget v1, p0, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@21
    iput v1, p1, Landroid/util/DisplayMetrics;->noncompatXdpi:F

    #@23
    iput v1, p1, Landroid/util/DisplayMetrics;->xdpi:F

    #@25
    .line 308
    iget v1, p0, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@27
    iput v1, p1, Landroid/util/DisplayMetrics;->noncompatYdpi:F

    #@29
    iput v1, p1, Landroid/util/DisplayMetrics;->ydpi:F

    #@2b
    .line 310
    if-eqz p2, :cond_36

    #@2d
    .line 311
    invoke-virtual {p2}, Landroid/view/CompatibilityInfoHolder;->getIfNeeded()Landroid/content/res/CompatibilityInfo;

    #@30
    move-result-object v0

    #@31
    .line 312
    .local v0, ci:Landroid/content/res/CompatibilityInfo;
    if-eqz v0, :cond_36

    #@33
    .line 313
    invoke-virtual {v0, p1}, Landroid/content/res/CompatibilityInfo;->applyToDisplayMetrics(Landroid/util/DisplayMetrics;)V

    #@36
    .line 316
    .end local v0           #ci:Landroid/content/res/CompatibilityInfo;
    :cond_36
    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/view/DisplayInfo;)V
    .registers 3
    .parameter "other"

    #@0
    .prologue
    .line 212
    iget v0, p1, Landroid/view/DisplayInfo;->layerStack:I

    #@2
    iput v0, p0, Landroid/view/DisplayInfo;->layerStack:I

    #@4
    .line 213
    iget v0, p1, Landroid/view/DisplayInfo;->flags:I

    #@6
    iput v0, p0, Landroid/view/DisplayInfo;->flags:I

    #@8
    .line 214
    iget v0, p1, Landroid/view/DisplayInfo;->type:I

    #@a
    iput v0, p0, Landroid/view/DisplayInfo;->type:I

    #@c
    .line 215
    iget-object v0, p1, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@e
    iput-object v0, p0, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@10
    .line 216
    iget-object v0, p1, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@12
    iput-object v0, p0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@14
    .line 217
    iget v0, p1, Landroid/view/DisplayInfo;->appWidth:I

    #@16
    iput v0, p0, Landroid/view/DisplayInfo;->appWidth:I

    #@18
    .line 218
    iget v0, p1, Landroid/view/DisplayInfo;->appHeight:I

    #@1a
    iput v0, p0, Landroid/view/DisplayInfo;->appHeight:I

    #@1c
    .line 219
    iget v0, p1, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@1e
    iput v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@20
    .line 220
    iget v0, p1, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@22
    iput v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@24
    .line 221
    iget v0, p1, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@26
    iput v0, p0, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@28
    .line 222
    iget v0, p1, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@2a
    iput v0, p0, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@2c
    .line 223
    iget v0, p1, Landroid/view/DisplayInfo;->logicalWidth:I

    #@2e
    iput v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@30
    .line 224
    iget v0, p1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@32
    iput v0, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@34
    .line 225
    iget v0, p1, Landroid/view/DisplayInfo;->rotation:I

    #@36
    iput v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@38
    .line 226
    iget v0, p1, Landroid/view/DisplayInfo;->refreshRate:F

    #@3a
    iput v0, p0, Landroid/view/DisplayInfo;->refreshRate:F

    #@3c
    .line 227
    iget v0, p1, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@3e
    iput v0, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@40
    .line 228
    iget v0, p1, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@42
    iput v0, p0, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@44
    .line 229
    iget v0, p1, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@46
    iput v0, p0, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@48
    .line 230
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 277
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Landroid/view/DisplayInfo;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 185
    if-eqz p1, :cond_7e

    #@2
    iget v0, p0, Landroid/view/DisplayInfo;->layerStack:I

    #@4
    iget v1, p1, Landroid/view/DisplayInfo;->layerStack:I

    #@6
    if-ne v0, v1, :cond_7e

    #@8
    iget v0, p0, Landroid/view/DisplayInfo;->flags:I

    #@a
    iget v1, p1, Landroid/view/DisplayInfo;->flags:I

    #@c
    if-ne v0, v1, :cond_7e

    #@e
    iget v0, p0, Landroid/view/DisplayInfo;->type:I

    #@10
    iget v1, p1, Landroid/view/DisplayInfo;->type:I

    #@12
    if-ne v0, v1, :cond_7e

    #@14
    iget-object v0, p0, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@16
    iget-object v1, p1, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@18
    invoke-static {v0, v1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_7e

    #@1e
    iget-object v0, p0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@20
    iget-object v1, p1, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@22
    invoke-static {v0, v1}, Llibcore/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_7e

    #@28
    iget v0, p0, Landroid/view/DisplayInfo;->appWidth:I

    #@2a
    iget v1, p1, Landroid/view/DisplayInfo;->appWidth:I

    #@2c
    if-ne v0, v1, :cond_7e

    #@2e
    iget v0, p0, Landroid/view/DisplayInfo;->appHeight:I

    #@30
    iget v1, p1, Landroid/view/DisplayInfo;->appHeight:I

    #@32
    if-ne v0, v1, :cond_7e

    #@34
    iget v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@36
    iget v1, p1, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@38
    if-ne v0, v1, :cond_7e

    #@3a
    iget v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@3c
    iget v1, p1, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@3e
    if-ne v0, v1, :cond_7e

    #@40
    iget v0, p0, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@42
    iget v1, p1, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@44
    if-ne v0, v1, :cond_7e

    #@46
    iget v0, p0, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@48
    iget v1, p1, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@4a
    if-ne v0, v1, :cond_7e

    #@4c
    iget v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@4e
    iget v1, p1, Landroid/view/DisplayInfo;->logicalWidth:I

    #@50
    if-ne v0, v1, :cond_7e

    #@52
    iget v0, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@54
    iget v1, p1, Landroid/view/DisplayInfo;->logicalHeight:I

    #@56
    if-ne v0, v1, :cond_7e

    #@58
    iget v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@5a
    iget v1, p1, Landroid/view/DisplayInfo;->rotation:I

    #@5c
    if-ne v0, v1, :cond_7e

    #@5e
    iget v0, p0, Landroid/view/DisplayInfo;->refreshRate:F

    #@60
    iget v1, p1, Landroid/view/DisplayInfo;->refreshRate:F

    #@62
    cmpl-float v0, v0, v1

    #@64
    if-nez v0, :cond_7e

    #@66
    iget v0, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@68
    iget v1, p1, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@6a
    if-ne v0, v1, :cond_7e

    #@6c
    iget v0, p0, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@6e
    iget v1, p1, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@70
    cmpl-float v0, v0, v1

    #@72
    if-nez v0, :cond_7e

    #@74
    iget v0, p0, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@76
    iget v1, p1, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@78
    cmpl-float v0, v0, v1

    #@7a
    if-nez v0, :cond_7e

    #@7c
    const/4 v0, 0x1

    #@7d
    :goto_7d
    return v0

    #@7e
    :cond_7e
    const/4 v0, 0x0

    #@7f
    goto :goto_7d
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 181
    instance-of v0, p1, Landroid/view/DisplayInfo;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Landroid/view/DisplayInfo;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/DisplayInfo;->equals(Landroid/view/DisplayInfo;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getAppMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V
    .registers 5
    .parameter "outMetrics"
    .parameter "cih"

    #@0
    .prologue
    .line 281
    iget v0, p0, Landroid/view/DisplayInfo;->appWidth:I

    #@2
    iget v1, p0, Landroid/view/DisplayInfo;->appHeight:I

    #@4
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/view/DisplayInfo;->getMetricsWithSize(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;II)V

    #@7
    .line 282
    return-void
.end method

.method public getLogicalMetrics(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;)V
    .registers 5
    .parameter "outMetrics"
    .parameter "cih"

    #@0
    .prologue
    .line 285
    iget v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@2
    iget v1, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@4
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/view/DisplayInfo;->getMetricsWithSize(Landroid/util/DisplayMetrics;Landroid/view/CompatibilityInfoHolder;II)V

    #@7
    .line 286
    return-void
.end method

.method public getNaturalHeight()I
    .registers 3

    #@0
    .prologue
    .line 294
    iget v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@2
    if-eqz v0, :cond_9

    #@4
    iget v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@6
    const/4 v1, 0x2

    #@7
    if-ne v0, v1, :cond_c

    #@9
    :cond_9
    iget v0, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    iget v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@e
    goto :goto_b
.end method

.method public getNaturalWidth()I
    .registers 3

    #@0
    .prologue
    .line 289
    iget v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@2
    if-eqz v0, :cond_9

    #@4
    iget v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@6
    const/4 v1, 0x2

    #@7
    if-ne v0, v1, :cond_c

    #@9
    :cond_9
    iget v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    iget v0, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@e
    goto :goto_b
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 208
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/view/DisplayInfo;->layerStack:I

    #@6
    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/view/DisplayInfo;->flags:I

    #@c
    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/view/DisplayInfo;->type:I

    #@12
    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@18
    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@1e
    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/view/DisplayInfo;->appWidth:I

    #@24
    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/view/DisplayInfo;->appHeight:I

    #@2a
    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@30
    .line 241
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v0

    #@34
    iput v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@36
    .line 242
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v0

    #@3a
    iput v0, p0, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@3c
    .line 243
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v0

    #@40
    iput v0, p0, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@42
    .line 244
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v0

    #@46
    iput v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@48
    .line 245
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v0

    #@4c
    iput v0, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@4e
    .line 246
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v0

    #@52
    iput v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@54
    .line 247
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@57
    move-result v0

    #@58
    iput v0, p0, Landroid/view/DisplayInfo;->refreshRate:F

    #@5a
    .line 248
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v0

    #@5e
    iput v0, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@60
    .line 249
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@63
    move-result v0

    #@64
    iput v0, p0, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@66
    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@69
    move-result v0

    #@6a
    iput v0, p0, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@6c
    .line 251
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DisplayInfo{\""

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "\", app "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/view/DisplayInfo;->appWidth:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " x "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/view/DisplayInfo;->appHeight:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", real "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, " x "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ", largest app "

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, " x "

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget v1, p0, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, ", smallest app "

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    iget v1, p0, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    const-string v1, " x "

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    iget v1, p0, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    const-string v1, ", "

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    iget v1, p0, Landroid/view/DisplayInfo;->refreshRate:F

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    const-string v1, " fps"

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    const-string v1, ", rotation "

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    iget v1, p0, Landroid/view/DisplayInfo;->rotation:I

    #@8b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    const-string v1, ", density "

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v0

    #@95
    iget v1, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v0

    #@9b
    const-string v1, ", "

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v0

    #@a1
    iget v1, p0, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    const-string v1, " x "

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v0

    #@ad
    iget v1, p0, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@af
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v0

    #@b3
    const-string v1, " dpi"

    #@b5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v0

    #@b9
    const-string v1, ", layerStack "

    #@bb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v0

    #@bf
    iget v1, p0, Landroid/view/DisplayInfo;->layerStack:I

    #@c1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v0

    #@c5
    const-string v1, ", type "

    #@c7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v0

    #@cb
    iget v1, p0, Landroid/view/DisplayInfo;->type:I

    #@cd
    invoke-static {v1}, Landroid/view/Display;->typeToString(I)Ljava/lang/String;

    #@d0
    move-result-object v1

    #@d1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v0

    #@d5
    const-string v1, ", address "

    #@d7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v0

    #@db
    iget-object v1, p0, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@dd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v0

    #@e1
    iget v1, p0, Landroid/view/DisplayInfo;->flags:I

    #@e3
    invoke-static {v1}, Landroid/view/DisplayInfo;->flagsToString(I)Ljava/lang/String;

    #@e6
    move-result-object v1

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    const-string/jumbo v1, "}"

    #@ee
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v0

    #@f2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v0

    #@f6
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 255
    iget v0, p0, Landroid/view/DisplayInfo;->layerStack:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 256
    iget v0, p0, Landroid/view/DisplayInfo;->flags:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 257
    iget v0, p0, Landroid/view/DisplayInfo;->type:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 258
    iget-object v0, p0, Landroid/view/DisplayInfo;->address:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 259
    iget-object v0, p0, Landroid/view/DisplayInfo;->name:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 260
    iget v0, p0, Landroid/view/DisplayInfo;->appWidth:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 261
    iget v0, p0, Landroid/view/DisplayInfo;->appHeight:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 262
    iget v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppWidth:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 263
    iget v0, p0, Landroid/view/DisplayInfo;->smallestNominalAppHeight:I

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 264
    iget v0, p0, Landroid/view/DisplayInfo;->largestNominalAppWidth:I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 265
    iget v0, p0, Landroid/view/DisplayInfo;->largestNominalAppHeight:I

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 266
    iget v0, p0, Landroid/view/DisplayInfo;->logicalWidth:I

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 267
    iget v0, p0, Landroid/view/DisplayInfo;->logicalHeight:I

    #@3e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 268
    iget v0, p0, Landroid/view/DisplayInfo;->rotation:I

    #@43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@46
    .line 269
    iget v0, p0, Landroid/view/DisplayInfo;->refreshRate:F

    #@48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@4b
    .line 270
    iget v0, p0, Landroid/view/DisplayInfo;->logicalDensityDpi:I

    #@4d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    .line 271
    iget v0, p0, Landroid/view/DisplayInfo;->physicalXDpi:F

    #@52
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@55
    .line 272
    iget v0, p0, Landroid/view/DisplayInfo;->physicalYDpi:F

    #@57
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@5a
    .line 273
    return-void
.end method
