.class Landroid/view/GLES20TextureLayer;
.super Landroid/view/GLES20Layer;
.source "GLES20TextureLayer.java"


# instance fields
.field private mSurface:Landroid/graphics/SurfaceTexture;

.field private mTexture:I


# direct methods
.method constructor <init>(Z)V
    .registers 5
    .parameter "isOpaque"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/view/GLES20Layer;-><init>()V

    #@3
    .line 34
    const/4 v1, 0x2

    #@4
    new-array v0, v1, [I

    #@6
    .line 35
    .local v0, layerInfo:[I
    invoke-static {p1, v0}, Landroid/view/GLES20Canvas;->nCreateTextureLayer(Z[I)I

    #@9
    move-result v1

    #@a
    iput v1, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@c
    .line 37
    iget v1, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@e
    if-eqz v1, :cond_1f

    #@10
    .line 38
    const/4 v1, 0x0

    #@11
    aget v1, v0, v1

    #@13
    iput v1, p0, Landroid/view/GLES20TextureLayer;->mTexture:I

    #@15
    .line 39
    new-instance v1, Landroid/view/GLES20Layer$Finalizer;

    #@17
    iget v2, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@19
    invoke-direct {v1, v2}, Landroid/view/GLES20Layer$Finalizer;-><init>(I)V

    #@1c
    iput-object v1, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@1e
    .line 43
    :goto_1e
    return-void

    #@1f
    .line 41
    :cond_1f
    const/4 v1, 0x0

    #@20
    iput-object v1, p0, Landroid/view/GLES20Layer;->mFinalizer:Landroid/view/GLES20Layer$Finalizer;

    #@22
    goto :goto_1e
.end method


# virtual methods
.method end(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "currentCanvas"

    #@0
    .prologue
    .line 67
    return-void
.end method

.method getCanvas()Landroid/view/HardwareCanvas;
    .registers 2

    #@0
    .prologue
    .line 57
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .registers 4

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 71
    new-instance v0, Landroid/graphics/SurfaceTexture;

    #@6
    iget v1, p0, Landroid/view/GLES20TextureLayer;->mTexture:I

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v0, v1, v2}, Landroid/graphics/SurfaceTexture;-><init>(IZ)V

    #@c
    iput-object v0, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@e
    .line 73
    :cond_e
    iget-object v0, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@10
    return-object v0
.end method

.method isValid()Z
    .registers 2

    #@0
    .prologue
    .line 47
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    if-eqz v0, :cond_a

    #@4
    iget v0, p0, Landroid/view/GLES20TextureLayer;->mTexture:I

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method redrawLater(Landroid/view/DisplayList;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "displayList"
    .parameter "dirtyRect"

    #@0
    .prologue
    .line 102
    return-void
.end method

.method resize(II)Z
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/view/GLES20TextureLayer;->isValid()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method setOpaque(Z)V
    .registers 4
    .parameter "isOpaque"

    #@0
    .prologue
    .line 92
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Use update(int, int, boolean) instead"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .registers 4
    .parameter "surfaceTexture"

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 78
    iget-object v0, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@6
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    #@9
    .line 80
    :cond_9
    iput-object p1, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@b
    .line 81
    iget-object v0, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@d
    iget v1, p0, Landroid/view/GLES20TextureLayer;->mTexture:I

    #@f
    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->attachToGLContext(I)V

    #@12
    .line 82
    return-void
.end method

.method setTransform(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 97
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->nSetTextureLayerTransform(II)V

    #@7
    .line 98
    return-void
.end method

.method start(Landroid/graphics/Canvas;)Landroid/view/HardwareCanvas;
    .registers 3
    .parameter "currentCanvas"

    #@0
    .prologue
    .line 62
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method update(IIZ)V
    .registers 6
    .parameter "width"
    .parameter "height"
    .parameter "isOpaque"

    #@0
    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3}, Landroid/view/GLES20Layer;->update(IIZ)V

    #@3
    .line 87
    iget v0, p0, Landroid/view/GLES20Layer;->mLayer:I

    #@5
    iget-object v1, p0, Landroid/view/GLES20TextureLayer;->mSurface:Landroid/graphics/SurfaceTexture;

    #@7
    invoke-static {v0, p1, p2, p3, v1}, Landroid/view/GLES20Canvas;->nUpdateTextureLayer(IIIZLandroid/graphics/SurfaceTexture;)V

    #@a
    .line 88
    return-void
.end method
