.class public abstract Landroid/view/InputEventReceiver;
.super Ljava/lang/Object;
.source "InputEventReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/InputEventReceiver$Factory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "InputEventReceiver"


# instance fields
.field private final mCloseGuard:Ldalvik/system/CloseGuard;

.field private mInputChannel:Landroid/view/InputChannel;

.field private mMessageQueue:Landroid/os/MessageQueue;

.field private mReceiverPtr:I

.field private final mSeqMap:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Landroid/view/InputChannel;Landroid/os/Looper;)V
    .registers 5
    .parameter "inputChannel"
    .parameter "looper"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/view/InputEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 43
    new-instance v0, Landroid/util/SparseIntArray;

    #@b
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@e
    iput-object v0, p0, Landroid/view/InputEventReceiver;->mSeqMap:Landroid/util/SparseIntArray;

    #@10
    .line 59
    if-nez p1, :cond_1a

    #@12
    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@14
    const-string v1, "inputChannel must not be null"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 62
    :cond_1a
    if-nez p2, :cond_25

    #@1c
    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1e
    const-string/jumbo v1, "looper must not be null"

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 66
    :cond_25
    iput-object p1, p0, Landroid/view/InputEventReceiver;->mInputChannel:Landroid/view/InputChannel;

    #@27
    .line 67
    invoke-virtual {p2}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/view/InputEventReceiver;->mMessageQueue:Landroid/os/MessageQueue;

    #@2d
    .line 68
    iget-object v0, p0, Landroid/view/InputEventReceiver;->mMessageQueue:Landroid/os/MessageQueue;

    #@2f
    invoke-static {p0, p1, v0}, Landroid/view/InputEventReceiver;->nativeInit(Landroid/view/InputEventReceiver;Landroid/view/InputChannel;Landroid/os/MessageQueue;)I

    #@32
    move-result v0

    #@33
    iput v0, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@35
    .line 70
    iget-object v0, p0, Landroid/view/InputEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@37
    const-string v1, "dispose"

    #@39
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@3c
    .line 71
    return-void
.end method

.method private dispatchBatchedInputEventPending()V
    .registers 1

    #@0
    .prologue
    .line 185
    invoke-virtual {p0}, Landroid/view/InputEventReceiver;->onBatchedInputEventPending()V

    #@3
    .line 186
    return-void
.end method

.method private dispatchInputEvent(ILandroid/view/InputEvent;)V
    .registers 5
    .parameter "seq"
    .parameter "event"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Landroid/view/InputEventReceiver;->mSeqMap:Landroid/util/SparseIntArray;

    #@2
    invoke-virtual {p2}, Landroid/view/InputEvent;->getSequenceNumber()I

    #@5
    move-result v1

    #@6
    invoke-virtual {v0, v1, p1}, Landroid/util/SparseIntArray;->put(II)V

    #@9
    .line 179
    invoke-virtual {p0, p2}, Landroid/view/InputEventReceiver;->onInputEvent(Landroid/view/InputEvent;)V

    #@c
    .line 180
    return-void
.end method

.method private dispose(Z)V
    .registers 4
    .parameter "finalized"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 90
    iget-object v0, p0, Landroid/view/InputEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 91
    if-eqz p1, :cond_c

    #@7
    .line 92
    iget-object v0, p0, Landroid/view/InputEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@9
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    #@c
    .line 94
    :cond_c
    iget-object v0, p0, Landroid/view/InputEventReceiver;->mCloseGuard:Ldalvik/system/CloseGuard;

    #@e
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@11
    .line 97
    :cond_11
    iget v0, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@13
    if-eqz v0, :cond_1d

    #@15
    .line 98
    iget v0, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@17
    invoke-static {v0}, Landroid/view/InputEventReceiver;->nativeDispose(I)V

    #@1a
    .line 99
    const/4 v0, 0x0

    #@1b
    iput v0, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@1d
    .line 101
    :cond_1d
    iput-object v1, p0, Landroid/view/InputEventReceiver;->mInputChannel:Landroid/view/InputChannel;

    #@1f
    .line 102
    iput-object v1, p0, Landroid/view/InputEventReceiver;->mMessageQueue:Landroid/os/MessageQueue;

    #@21
    .line 103
    return-void
.end method

.method private static native nativeConsumeBatchedInputEvents(IJ)V
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeFinishInputEvent(IIZ)V
.end method

.method private static native nativeInit(Landroid/view/InputEventReceiver;Landroid/view/InputChannel;Landroid/os/MessageQueue;)I
.end method


# virtual methods
.method public final consumeBatchedInputEvents(J)V
    .registers 5
    .parameter "frameTimeNanos"

    #@0
    .prologue
    .line 167
    iget v0, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@2
    if-nez v0, :cond_c

    #@4
    .line 168
    const-string v0, "InputEventReceiver"

    #@6
    const-string v1, "Attempted to consume batched input events but the input event receiver has already been disposed."

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 173
    :goto_b
    return-void

    #@c
    .line 171
    :cond_c
    iget v0, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@e
    invoke-static {v0, p1, p2}, Landroid/view/InputEventReceiver;->nativeConsumeBatchedInputEvents(IJ)V

    #@11
    goto :goto_b
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 86
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/view/InputEventReceiver;->dispose(Z)V

    #@4
    .line 87
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    const/4 v0, 0x1

    #@1
    :try_start_1
    invoke-direct {p0, v0}, Landroid/view/InputEventReceiver;->dispose(Z)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_8

    #@4
    .line 78
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@7
    .line 80
    return-void

    #@8
    .line 78
    :catchall_8
    move-exception v0

    #@9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    throw v0
.end method

.method public final finishInputEvent(Landroid/view/InputEvent;Z)V
    .registers 7
    .parameter "event"
    .parameter "handled"

    #@0
    .prologue
    .line 137
    if-nez p1, :cond_a

    #@2
    .line 138
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v3, "event must not be null"

    #@6
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v2

    #@a
    .line 140
    :cond_a
    iget v2, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@c
    if-nez v2, :cond_19

    #@e
    .line 141
    const-string v2, "InputEventReceiver"

    #@10
    const-string v3, "Attempted to finish an input event but the input event receiver has already been disposed."

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 153
    :goto_15
    invoke-virtual {p1}, Landroid/view/InputEvent;->recycleIfNeededAfterDispatch()V

    #@18
    .line 154
    return-void

    #@19
    .line 144
    :cond_19
    iget-object v2, p0, Landroid/view/InputEventReceiver;->mSeqMap:Landroid/util/SparseIntArray;

    #@1b
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSequenceNumber()I

    #@1e
    move-result v3

    #@1f
    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    #@22
    move-result v0

    #@23
    .line 145
    .local v0, index:I
    if-gez v0, :cond_2d

    #@25
    .line 146
    const-string v2, "InputEventReceiver"

    #@27
    const-string v3, "Attempted to finish an input event that is not in progress."

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_15

    #@2d
    .line 148
    :cond_2d
    iget-object v2, p0, Landroid/view/InputEventReceiver;->mSeqMap:Landroid/util/SparseIntArray;

    #@2f
    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    #@32
    move-result v1

    #@33
    .line 149
    .local v1, seq:I
    iget-object v2, p0, Landroid/view/InputEventReceiver;->mSeqMap:Landroid/util/SparseIntArray;

    #@35
    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    #@38
    .line 150
    iget v2, p0, Landroid/view/InputEventReceiver;->mReceiverPtr:I

    #@3a
    invoke-static {v2, v1, p2}, Landroid/view/InputEventReceiver;->nativeFinishInputEvent(IIZ)V

    #@3d
    goto :goto_15
.end method

.method public onBatchedInputEventPending()V
    .registers 3

    #@0
    .prologue
    .line 126
    const-wide/16 v0, -0x1

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/view/InputEventReceiver;->consumeBatchedInputEvents(J)V

    #@5
    .line 127
    return-void
.end method

.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 114
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/InputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@4
    .line 115
    return-void
.end method
