.class final Landroid/view/Choreographer$CallbackQueue;
.super Ljava/lang/Object;
.source "Choreographer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Choreographer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CallbackQueue"
.end annotation


# instance fields
.field private mHead:Landroid/view/Choreographer$CallbackRecord;

.field final synthetic this$0:Landroid/view/Choreographer;


# direct methods
.method private constructor <init>(Landroid/view/Choreographer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 754
    iput-object p1, p0, Landroid/view/Choreographer$CallbackQueue;->this$0:Landroid/view/Choreographer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/Choreographer;Landroid/view/Choreographer$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 754
    invoke-direct {p0, p1}, Landroid/view/Choreographer$CallbackQueue;-><init>(Landroid/view/Choreographer;)V

    #@3
    return-void
.end method


# virtual methods
.method public addCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter "dueTime"
    .parameter "action"
    .parameter "token"

    #@0
    .prologue
    .line 782
    iget-object v2, p0, Landroid/view/Choreographer$CallbackQueue;->this$0:Landroid/view/Choreographer;

    #@2
    invoke-static {v2, p1, p2, p3, p4}, Landroid/view/Choreographer;->access$500(Landroid/view/Choreographer;JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;

    #@5
    move-result-object v0

    #@6
    .line 783
    .local v0, callback:Landroid/view/Choreographer$CallbackRecord;
    iget-object v1, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@8
    .line 784
    .local v1, entry:Landroid/view/Choreographer$CallbackRecord;
    if-nez v1, :cond_d

    #@a
    .line 785
    iput-object v0, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@c
    .line 801
    :goto_c
    return-void

    #@d
    .line 788
    :cond_d
    iget-wide v2, v1, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    #@f
    cmp-long v2, p1, v2

    #@11
    if-gez v2, :cond_1a

    #@13
    .line 789
    iput-object v1, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@15
    .line 790
    iput-object v0, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@17
    goto :goto_c

    #@18
    .line 798
    :cond_18
    iget-object v1, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@1a
    .line 793
    :cond_1a
    iget-object v2, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@1c
    if-eqz v2, :cond_2a

    #@1e
    .line 794
    iget-object v2, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@20
    iget-wide v2, v2, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    #@22
    cmp-long v2, p1, v2

    #@24
    if-gez v2, :cond_18

    #@26
    .line 795
    iget-object v2, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@28
    iput-object v2, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@2a
    .line 800
    :cond_2a
    iput-object v0, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@2c
    goto :goto_c
.end method

.method public extractDueCallbacksLocked(J)Landroid/view/Choreographer$CallbackRecord;
    .registers 9
    .parameter "now"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 762
    iget-object v0, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@3
    .line 763
    .local v0, callbacks:Landroid/view/Choreographer$CallbackRecord;
    if-eqz v0, :cond_b

    #@5
    iget-wide v4, v0, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    #@7
    cmp-long v4, v4, p1

    #@9
    if-lez v4, :cond_d

    #@b
    :cond_b
    move-object v0, v3

    #@c
    .line 778
    .end local v0           #callbacks:Landroid/view/Choreographer$CallbackRecord;
    :goto_c
    return-object v0

    #@d
    .line 767
    .restart local v0       #callbacks:Landroid/view/Choreographer$CallbackRecord;
    :cond_d
    move-object v1, v0

    #@e
    .line 768
    .local v1, last:Landroid/view/Choreographer$CallbackRecord;
    iget-object v2, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@10
    .line 769
    .local v2, next:Landroid/view/Choreographer$CallbackRecord;
    :goto_10
    if-eqz v2, :cond_1a

    #@12
    .line 770
    iget-wide v4, v2, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    #@14
    cmp-long v4, v4, p1

    #@16
    if-lez v4, :cond_1d

    #@18
    .line 771
    iput-object v3, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@1a
    .line 777
    :cond_1a
    iput-object v2, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@1c
    goto :goto_c

    #@1d
    .line 774
    :cond_1d
    move-object v1, v2

    #@1e
    .line 775
    iget-object v2, v2, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@20
    goto :goto_10
.end method

.method public hasDueCallbacksLocked(J)Z
    .registers 5
    .parameter "now"

    #@0
    .prologue
    .line 758
    iget-object v0, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@6
    iget-wide v0, v0, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    #@8
    cmp-long v0, v0, p1

    #@a
    if-gtz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public removeCallbacksLocked(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter "action"
    .parameter "token"

    #@0
    .prologue
    .line 804
    const/4 v2, 0x0

    #@1
    .line 805
    .local v2, predecessor:Landroid/view/Choreographer$CallbackRecord;
    iget-object v0, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@3
    .local v0, callback:Landroid/view/Choreographer$CallbackRecord;
    :goto_3
    if-eqz v0, :cond_23

    #@5
    .line 806
    iget-object v1, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@7
    .line 807
    .local v1, next:Landroid/view/Choreographer$CallbackRecord;
    if-eqz p1, :cond_d

    #@9
    iget-object v3, v0, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    #@b
    if-ne v3, p1, :cond_21

    #@d
    :cond_d
    if-eqz p2, :cond_13

    #@f
    iget-object v3, v0, Landroid/view/Choreographer$CallbackRecord;->token:Ljava/lang/Object;

    #@11
    if-ne v3, p2, :cond_21

    #@13
    .line 809
    :cond_13
    if-eqz v2, :cond_1e

    #@15
    .line 810
    iput-object v1, v2, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@17
    .line 814
    :goto_17
    iget-object v3, p0, Landroid/view/Choreographer$CallbackQueue;->this$0:Landroid/view/Choreographer;

    #@19
    invoke-static {v3, v0}, Landroid/view/Choreographer;->access$600(Landroid/view/Choreographer;Landroid/view/Choreographer$CallbackRecord;)V

    #@1c
    .line 818
    :goto_1c
    move-object v0, v1

    #@1d
    .line 819
    goto :goto_3

    #@1e
    .line 812
    :cond_1e
    iput-object v1, p0, Landroid/view/Choreographer$CallbackQueue;->mHead:Landroid/view/Choreographer$CallbackRecord;

    #@20
    goto :goto_17

    #@21
    .line 816
    :cond_21
    move-object v2, v0

    #@22
    goto :goto_1c

    #@23
    .line 820
    .end local v1           #next:Landroid/view/Choreographer$CallbackRecord;
    :cond_23
    return-void
.end method
