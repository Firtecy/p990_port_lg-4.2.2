.class Landroid/view/ViewGroup$3;
.super Ljava/lang/Object;
.source "ViewGroup.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 5081
    iput-object p1, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .registers 7
    .parameter "transition"
    .parameter "container"
    .parameter "view"
    .parameter "transitionType"

    #@0
    .prologue
    .line 5095
    iget-object v0, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@2
    invoke-static {v0}, Landroid/view/ViewGroup;->access$300(Landroid/view/ViewGroup;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_19

    #@8
    invoke-virtual {p1}, Landroid/animation/LayoutTransition;->isChangingLayout()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_19

    #@e
    .line 5096
    iget-object v0, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@10
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    #@13
    .line 5097
    iget-object v0, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@15
    const/4 v1, 0x0

    #@16
    invoke-static {v0, v1}, Landroid/view/ViewGroup;->access$302(Landroid/view/ViewGroup;Z)Z

    #@19
    .line 5099
    :cond_19
    const/4 v0, 0x3

    #@1a
    if-ne p4, v0, :cond_29

    #@1c
    iget-object v0, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@1e
    invoke-static {v0}, Landroid/view/ViewGroup;->access$400(Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    #@21
    move-result-object v0

    #@22
    if-eqz v0, :cond_29

    #@24
    .line 5100
    iget-object v0, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@26
    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    #@29
    .line 5102
    :cond_29
    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .registers 6
    .parameter "transition"
    .parameter "container"
    .parameter "view"
    .parameter "transitionType"

    #@0
    .prologue
    .line 5087
    const/4 v0, 0x3

    #@1
    if-ne p4, v0, :cond_8

    #@3
    .line 5088
    iget-object v0, p0, Landroid/view/ViewGroup$3;->this$0:Landroid/view/ViewGroup;

    #@5
    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    #@8
    .line 5090
    :cond_8
    return-void
.end method
