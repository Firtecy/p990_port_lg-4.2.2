.class public abstract Landroid/view/WindowOrientationListener;
.super Ljava/lang/Object;
.source "WindowOrientationListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/WindowOrientationListener$SensorEventListenerImpl;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final ISG2:Z = false

#the value of this static final field might be set in the static constructor
.field private static final LOG:Z = false

.field private static final TAG:Ljava/lang/String; = "WindowOrientationListener"

.field public static final TYPE_SENSOR_LGE_SCREEN_ORIENTATION:I = 0x1fa2643

.field private static final USE_GRAVITY_SENSOR:Z


# instance fields
.field mCurrentRotation:I

.field private mEnabled:Z

.field private mRate:I

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorEventListener:Landroid/view/WindowOrientationListener$SensorEventListenerImpl;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 45
    const-string v0, "debug.orientation.log"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Landroid/view/WindowOrientationListener;->LOG:Z

    #@9
    .line 49
    const-string/jumbo v0, "ro.build.product"

    #@c
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "g2"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@15
    move-result v0

    #@16
    sput-boolean v0, Landroid/view/WindowOrientationListener;->ISG2:Z

    #@18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, p1, v0}, Landroid/view/WindowOrientationListener;-><init>(Landroid/content/Context;I)V

    #@4
    .line 73
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "rate"

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 59
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/view/WindowOrientationListener;->mCurrentRotation:I

    #@6
    .line 86
    const-string/jumbo v0, "sensor"

    #@9
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/hardware/SensorManager;

    #@f
    iput-object v0, p0, Landroid/view/WindowOrientationListener;->mSensorManager:Landroid/hardware/SensorManager;

    #@11
    .line 87
    iput p2, p0, Landroid/view/WindowOrientationListener;->mRate:I

    #@13
    .line 95
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensorManager:Landroid/hardware/SensorManager;

    #@15
    const v1, 0x1fa2643

    #@18
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/view/WindowOrientationListener;->mSensor:Landroid/hardware/Sensor;

    #@1e
    .line 98
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensor:Landroid/hardware/Sensor;

    #@20
    if-eqz v0, :cond_29

    #@22
    .line 100
    new-instance v0, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;

    #@24
    invoke-direct {v0, p0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;-><init>(Landroid/view/WindowOrientationListener;)V

    #@27
    iput-object v0, p0, Landroid/view/WindowOrientationListener;->mSensorEventListener:Landroid/view/WindowOrientationListener$SensorEventListenerImpl;

    #@29
    .line 102
    :cond_29
    return-void
.end method

.method static synthetic access$000()Z
    .registers 1

    #@0
    .prologue
    .line 43
    sget-boolean v0, Landroid/view/WindowOrientationListener;->ISG2:Z

    #@2
    return v0
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 43
    sget-boolean v0, Landroid/view/WindowOrientationListener;->LOG:Z

    #@2
    return v0
.end method


# virtual methods
.method public canDetectOrientation()Z
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensor:Landroid/hardware/Sensor;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public disable()V
    .registers 3

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensor:Landroid/hardware/Sensor;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 126
    const-string v0, "WindowOrientationListener"

    #@6
    const-string v1, "Cannot detect sensors. Invalid disable"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 136
    :cond_b
    :goto_b
    return-void

    #@c
    .line 129
    :cond_c
    iget-boolean v0, p0, Landroid/view/WindowOrientationListener;->mEnabled:Z

    #@e
    const/4 v1, 0x1

    #@f
    if-ne v0, v1, :cond_b

    #@11
    .line 130
    sget-boolean v0, Landroid/view/WindowOrientationListener;->LOG:Z

    #@13
    if-eqz v0, :cond_1c

    #@15
    .line 131
    const-string v0, "WindowOrientationListener"

    #@17
    const-string v1, "WindowOrientationListener disabled"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 133
    :cond_1c
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensorManager:Landroid/hardware/SensorManager;

    #@1e
    iget-object v1, p0, Landroid/view/WindowOrientationListener;->mSensorEventListener:Landroid/view/WindowOrientationListener$SensorEventListenerImpl;

    #@20
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@23
    .line 134
    const/4 v0, 0x0

    #@24
    iput-boolean v0, p0, Landroid/view/WindowOrientationListener;->mEnabled:Z

    #@26
    goto :goto_b
.end method

.method public enable()V
    .registers 5

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensor:Landroid/hardware/Sensor;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 109
    const-string v0, "WindowOrientationListener"

    #@6
    const-string v1, "Cannot detect sensors. Not enabled"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 119
    :cond_b
    :goto_b
    return-void

    #@c
    .line 112
    :cond_c
    iget-boolean v0, p0, Landroid/view/WindowOrientationListener;->mEnabled:Z

    #@e
    if-nez v0, :cond_b

    #@10
    .line 113
    sget-boolean v0, Landroid/view/WindowOrientationListener;->LOG:Z

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 114
    const-string v0, "WindowOrientationListener"

    #@16
    const-string v1, "WindowOrientationListener enabled"

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 116
    :cond_1b
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensorManager:Landroid/hardware/SensorManager;

    #@1d
    iget-object v1, p0, Landroid/view/WindowOrientationListener;->mSensorEventListener:Landroid/view/WindowOrientationListener$SensorEventListenerImpl;

    #@1f
    iget-object v2, p0, Landroid/view/WindowOrientationListener;->mSensor:Landroid/hardware/Sensor;

    #@21
    iget v3, p0, Landroid/view/WindowOrientationListener;->mRate:I

    #@23
    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    #@26
    .line 117
    const/4 v0, 0x1

    #@27
    iput-boolean v0, p0, Landroid/view/WindowOrientationListener;->mEnabled:Z

    #@29
    goto :goto_b
.end method

.method public getProposedRotation()I
    .registers 2

    #@0
    .prologue
    .line 156
    iget-boolean v0, p0, Landroid/view/WindowOrientationListener;->mEnabled:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 157
    iget-object v0, p0, Landroid/view/WindowOrientationListener;->mSensorEventListener:Landroid/view/WindowOrientationListener$SensorEventListenerImpl;

    #@6
    invoke-virtual {v0}, Landroid/view/WindowOrientationListener$SensorEventListenerImpl;->getProposedRotation()I

    #@9
    move-result v0

    #@a
    .line 159
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public abstract onProposedRotationChanged(I)V
.end method

.method public setCurrentRotation(I)V
    .registers 2
    .parameter "rotation"

    #@0
    .prologue
    .line 144
    iput p1, p0, Landroid/view/WindowOrientationListener;->mCurrentRotation:I

    #@2
    .line 145
    return-void
.end method
