.class public Landroid/view/SurfaceView;
.super Landroid/view/View;
.source "SurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/SurfaceView$MyWindow;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field static final GET_NEW_SURFACE_MSG:I = 0x2

.field static final KEEP_SCREEN_ON_MSG:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SurfaceView"

.field static final UPDATE_WINDOW_MSG:I = 0x3

.field static final UPDATE_WINDOW_MSG2:I = 0x4


# instance fields
.field final mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/SurfaceHolder$Callback;",
            ">;"
        }
    .end annotation
.end field

.field final mConfiguration:Landroid/content/res/Configuration;

.field final mContentInsets:Landroid/graphics/Rect;

.field private final mDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field mDrawingStopped:Z

.field private mDsdrDlg:Landroid/app/Dialog;

.field mFormat:I

.field private mGlobalListenersAdded:Z

.field final mHandler:Landroid/os/Handler;

.field mHaveFrame:Z

.field mHeight:I

.field mIsCreating:Z

.field private mIsLockScreen:Z

.field mLastLockTime:J

.field mLastSurfaceHeight:I

.field mLastSurfaceWidth:I

.field final mLayout:Landroid/view/WindowManager$LayoutParams;

.field mLeft:I

.field final mLocation:[I

.field final mNewSurface:Landroid/view/Surface;

.field mReportDrawNeeded:Z

.field mRequestedFormat:I

.field mRequestedHeight:I

.field mRequestedVisible:Z

.field mRequestedWidth:I

.field final mScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field mSession:Landroid/view/IWindowSession;

.field final mSurface:Landroid/view/Surface;

.field mSurfaceCreated:Z

.field final mSurfaceFrame:Landroid/graphics/Rect;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field final mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

.field mTmpDirty:Landroid/graphics/Rect;

.field mTop:I

.field private mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

.field mUpdateWindowNeeded:Z

.field mViewVisibility:Z

.field mVisible:Z

.field final mVisibleInsets:Landroid/graphics/Rect;

.field mWidth:I

.field final mWinFrame:Landroid/graphics/Rect;

.field mWindow:Landroid/view/SurfaceView$MyWindow;

.field mWindowType:I

.field mWindowVisibility:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 213
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@5
    .line 100
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@8
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@f
    .line 105
    const/4 v0, 0x2

    #@10
    new-array v0, v0, [I

    #@12
    iput-object v0, p0, Landroid/view/SurfaceView;->mLocation:[I

    #@14
    .line 107
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@16
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@19
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@1b
    .line 108
    new-instance v0, Landroid/view/Surface;

    #@1d
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@20
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@22
    .line 109
    new-instance v0, Landroid/view/Surface;

    #@24
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@27
    iput-object v0, p0, Landroid/view/SurfaceView;->mNewSurface:Landroid/view/Surface;

    #@29
    .line 110
    const/4 v0, 0x1

    #@2a
    iput-boolean v0, p0, Landroid/view/SurfaceView;->mDrawingStopped:Z

    #@2c
    .line 112
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@2e
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@31
    iput-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@33
    .line 116
    new-instance v0, Landroid/graphics/Rect;

    #@35
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@38
    iput-object v0, p0, Landroid/view/SurfaceView;->mVisibleInsets:Landroid/graphics/Rect;

    #@3a
    .line 117
    new-instance v0, Landroid/graphics/Rect;

    #@3c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@3f
    iput-object v0, p0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@41
    .line 118
    new-instance v0, Landroid/graphics/Rect;

    #@43
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@46
    iput-object v0, p0, Landroid/view/SurfaceView;->mContentInsets:Landroid/graphics/Rect;

    #@48
    .line 119
    new-instance v0, Landroid/content/res/Configuration;

    #@4a
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@4d
    iput-object v0, p0, Landroid/view/SurfaceView;->mConfiguration:Landroid/content/res/Configuration;

    #@4f
    .line 128
    const/16 v0, 0x3e9

    #@51
    iput v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@53
    .line 130
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@55
    .line 132
    new-instance v0, Landroid/view/SurfaceView$1;

    #@57
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$1;-><init>(Landroid/view/SurfaceView;)V

    #@5a
    iput-object v0, p0, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@5c
    .line 162
    new-instance v0, Landroid/view/SurfaceView$2;

    #@5e
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$2;-><init>(Landroid/view/SurfaceView;)V

    #@61
    iput-object v0, p0, Landroid/view/SurfaceView;->mScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@63
    .line 169
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@65
    .line 170
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mWindowVisibility:Z

    #@67
    .line 171
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@69
    .line 172
    iput v2, p0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@6b
    .line 173
    iput v2, p0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@6d
    .line 177
    const/4 v0, 0x4

    #@6e
    iput v0, p0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@70
    .line 179
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mHaveFrame:Z

    #@72
    .line 180
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@74
    .line 181
    const-wide/16 v0, 0x0

    #@76
    iput-wide v0, p0, Landroid/view/SurfaceView;->mLastLockTime:J

    #@78
    .line 183
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mVisible:Z

    #@7a
    .line 184
    iput v2, p0, Landroid/view/SurfaceView;->mLeft:I

    #@7c
    .line 185
    iput v2, p0, Landroid/view/SurfaceView;->mTop:I

    #@7e
    .line 186
    iput v2, p0, Landroid/view/SurfaceView;->mWidth:I

    #@80
    .line 187
    iput v2, p0, Landroid/view/SurfaceView;->mHeight:I

    #@82
    .line 188
    iput v2, p0, Landroid/view/SurfaceView;->mFormat:I

    #@84
    .line 189
    new-instance v0, Landroid/graphics/Rect;

    #@86
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@89
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@8b
    .line 191
    iput v2, p0, Landroid/view/SurfaceView;->mLastSurfaceWidth:I

    #@8d
    iput v2, p0, Landroid/view/SurfaceView;->mLastSurfaceHeight:I

    #@8f
    .line 196
    new-instance v0, Landroid/view/SurfaceView$3;

    #@91
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$3;-><init>(Landroid/view/SurfaceView;)V

    #@94
    iput-object v0, p0, Landroid/view/SurfaceView;->mDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    #@96
    .line 209
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mIsLockScreen:Z

    #@98
    .line 853
    new-instance v0, Landroid/view/SurfaceView$4;

    #@9a
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$4;-><init>(Landroid/view/SurfaceView;)V

    #@9d
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@9f
    .line 214
    invoke-direct {p0}, Landroid/view/SurfaceView;->init()V

    #@a2
    .line 215
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 218
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 100
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@8
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@f
    .line 105
    const/4 v0, 0x2

    #@10
    new-array v0, v0, [I

    #@12
    iput-object v0, p0, Landroid/view/SurfaceView;->mLocation:[I

    #@14
    .line 107
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@16
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@19
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@1b
    .line 108
    new-instance v0, Landroid/view/Surface;

    #@1d
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@20
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@22
    .line 109
    new-instance v0, Landroid/view/Surface;

    #@24
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@27
    iput-object v0, p0, Landroid/view/SurfaceView;->mNewSurface:Landroid/view/Surface;

    #@29
    .line 110
    const/4 v0, 0x1

    #@2a
    iput-boolean v0, p0, Landroid/view/SurfaceView;->mDrawingStopped:Z

    #@2c
    .line 112
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@2e
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@31
    iput-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@33
    .line 116
    new-instance v0, Landroid/graphics/Rect;

    #@35
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@38
    iput-object v0, p0, Landroid/view/SurfaceView;->mVisibleInsets:Landroid/graphics/Rect;

    #@3a
    .line 117
    new-instance v0, Landroid/graphics/Rect;

    #@3c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@3f
    iput-object v0, p0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@41
    .line 118
    new-instance v0, Landroid/graphics/Rect;

    #@43
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@46
    iput-object v0, p0, Landroid/view/SurfaceView;->mContentInsets:Landroid/graphics/Rect;

    #@48
    .line 119
    new-instance v0, Landroid/content/res/Configuration;

    #@4a
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@4d
    iput-object v0, p0, Landroid/view/SurfaceView;->mConfiguration:Landroid/content/res/Configuration;

    #@4f
    .line 128
    const/16 v0, 0x3e9

    #@51
    iput v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@53
    .line 130
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@55
    .line 132
    new-instance v0, Landroid/view/SurfaceView$1;

    #@57
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$1;-><init>(Landroid/view/SurfaceView;)V

    #@5a
    iput-object v0, p0, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@5c
    .line 162
    new-instance v0, Landroid/view/SurfaceView$2;

    #@5e
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$2;-><init>(Landroid/view/SurfaceView;)V

    #@61
    iput-object v0, p0, Landroid/view/SurfaceView;->mScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@63
    .line 169
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@65
    .line 170
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mWindowVisibility:Z

    #@67
    .line 171
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@69
    .line 172
    iput v2, p0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@6b
    .line 173
    iput v2, p0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@6d
    .line 177
    const/4 v0, 0x4

    #@6e
    iput v0, p0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@70
    .line 179
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mHaveFrame:Z

    #@72
    .line 180
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@74
    .line 181
    const-wide/16 v0, 0x0

    #@76
    iput-wide v0, p0, Landroid/view/SurfaceView;->mLastLockTime:J

    #@78
    .line 183
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mVisible:Z

    #@7a
    .line 184
    iput v2, p0, Landroid/view/SurfaceView;->mLeft:I

    #@7c
    .line 185
    iput v2, p0, Landroid/view/SurfaceView;->mTop:I

    #@7e
    .line 186
    iput v2, p0, Landroid/view/SurfaceView;->mWidth:I

    #@80
    .line 187
    iput v2, p0, Landroid/view/SurfaceView;->mHeight:I

    #@82
    .line 188
    iput v2, p0, Landroid/view/SurfaceView;->mFormat:I

    #@84
    .line 189
    new-instance v0, Landroid/graphics/Rect;

    #@86
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@89
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@8b
    .line 191
    iput v2, p0, Landroid/view/SurfaceView;->mLastSurfaceWidth:I

    #@8d
    iput v2, p0, Landroid/view/SurfaceView;->mLastSurfaceHeight:I

    #@8f
    .line 196
    new-instance v0, Landroid/view/SurfaceView$3;

    #@91
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$3;-><init>(Landroid/view/SurfaceView;)V

    #@94
    iput-object v0, p0, Landroid/view/SurfaceView;->mDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    #@96
    .line 209
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mIsLockScreen:Z

    #@98
    .line 853
    new-instance v0, Landroid/view/SurfaceView$4;

    #@9a
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$4;-><init>(Landroid/view/SurfaceView;)V

    #@9d
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@9f
    .line 219
    invoke-direct {p0}, Landroid/view/SurfaceView;->init()V

    #@a2
    .line 220
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 223
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 100
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@8
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    #@a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v0, p0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@f
    .line 105
    const/4 v0, 0x2

    #@10
    new-array v0, v0, [I

    #@12
    iput-object v0, p0, Landroid/view/SurfaceView;->mLocation:[I

    #@14
    .line 107
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@16
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@19
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@1b
    .line 108
    new-instance v0, Landroid/view/Surface;

    #@1d
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@20
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@22
    .line 109
    new-instance v0, Landroid/view/Surface;

    #@24
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@27
    iput-object v0, p0, Landroid/view/SurfaceView;->mNewSurface:Landroid/view/Surface;

    #@29
    .line 110
    const/4 v0, 0x1

    #@2a
    iput-boolean v0, p0, Landroid/view/SurfaceView;->mDrawingStopped:Z

    #@2c
    .line 112
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@2e
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@31
    iput-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@33
    .line 116
    new-instance v0, Landroid/graphics/Rect;

    #@35
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@38
    iput-object v0, p0, Landroid/view/SurfaceView;->mVisibleInsets:Landroid/graphics/Rect;

    #@3a
    .line 117
    new-instance v0, Landroid/graphics/Rect;

    #@3c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@3f
    iput-object v0, p0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@41
    .line 118
    new-instance v0, Landroid/graphics/Rect;

    #@43
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@46
    iput-object v0, p0, Landroid/view/SurfaceView;->mContentInsets:Landroid/graphics/Rect;

    #@48
    .line 119
    new-instance v0, Landroid/content/res/Configuration;

    #@4a
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@4d
    iput-object v0, p0, Landroid/view/SurfaceView;->mConfiguration:Landroid/content/res/Configuration;

    #@4f
    .line 128
    const/16 v0, 0x3e9

    #@51
    iput v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@53
    .line 130
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@55
    .line 132
    new-instance v0, Landroid/view/SurfaceView$1;

    #@57
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$1;-><init>(Landroid/view/SurfaceView;)V

    #@5a
    iput-object v0, p0, Landroid/view/SurfaceView;->mHandler:Landroid/os/Handler;

    #@5c
    .line 162
    new-instance v0, Landroid/view/SurfaceView$2;

    #@5e
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$2;-><init>(Landroid/view/SurfaceView;)V

    #@61
    iput-object v0, p0, Landroid/view/SurfaceView;->mScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@63
    .line 169
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@65
    .line 170
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mWindowVisibility:Z

    #@67
    .line 171
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@69
    .line 172
    iput v2, p0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@6b
    .line 173
    iput v2, p0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@6d
    .line 177
    const/4 v0, 0x4

    #@6e
    iput v0, p0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@70
    .line 179
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mHaveFrame:Z

    #@72
    .line 180
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@74
    .line 181
    const-wide/16 v0, 0x0

    #@76
    iput-wide v0, p0, Landroid/view/SurfaceView;->mLastLockTime:J

    #@78
    .line 183
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mVisible:Z

    #@7a
    .line 184
    iput v2, p0, Landroid/view/SurfaceView;->mLeft:I

    #@7c
    .line 185
    iput v2, p0, Landroid/view/SurfaceView;->mTop:I

    #@7e
    .line 186
    iput v2, p0, Landroid/view/SurfaceView;->mWidth:I

    #@80
    .line 187
    iput v2, p0, Landroid/view/SurfaceView;->mHeight:I

    #@82
    .line 188
    iput v2, p0, Landroid/view/SurfaceView;->mFormat:I

    #@84
    .line 189
    new-instance v0, Landroid/graphics/Rect;

    #@86
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@89
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@8b
    .line 191
    iput v2, p0, Landroid/view/SurfaceView;->mLastSurfaceWidth:I

    #@8d
    iput v2, p0, Landroid/view/SurfaceView;->mLastSurfaceHeight:I

    #@8f
    .line 196
    new-instance v0, Landroid/view/SurfaceView$3;

    #@91
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$3;-><init>(Landroid/view/SurfaceView;)V

    #@94
    iput-object v0, p0, Landroid/view/SurfaceView;->mDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    #@96
    .line 209
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mIsLockScreen:Z

    #@98
    .line 853
    new-instance v0, Landroid/view/SurfaceView$4;

    #@9a
    invoke-direct {v0, p0}, Landroid/view/SurfaceView$4;-><init>(Landroid/view/SurfaceView;)V

    #@9d
    iput-object v0, p0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@9f
    .line 224
    invoke-direct {p0}, Landroid/view/SurfaceView;->init()V

    #@a2
    .line 225
    return-void
.end method

.method static synthetic access$000(Landroid/view/SurfaceView;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@3
    return-void
.end method

.method private getSurfaceCallbacks()[Landroid/view/SurfaceHolder$Callback;
    .registers 4

    #@0
    .prologue
    .line 749
    iget-object v2, p0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v2

    #@3
    .line 750
    :try_start_3
    iget-object v1, p0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    new-array v0, v1, [Landroid/view/SurfaceHolder$Callback;

    #@b
    .line 751
    .local v0, callbacks:[Landroid/view/SurfaceHolder$Callback;
    iget-object v1, p0, Landroid/view/SurfaceView;->mCallbacks:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@10
    .line 752
    monitor-exit v2

    #@11
    .line 753
    return-object v0

    #@12
    .line 752
    .end local v0           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method private init()V
    .registers 2

    #@0
    .prologue
    .line 228
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/view/SurfaceView;->setWillNotDraw(Z)V

    #@4
    .line 229
    return-void
.end method

.method private updateWindow(ZZ)V
    .registers 47
    .parameter "force"
    .parameter "redrawNeeded"

    #@0
    .prologue
    .line 469
    move-object/from16 v0, p0

    #@2
    iget-boolean v3, v0, Landroid/view/SurfaceView;->mHaveFrame:Z

    #@4
    if-nez v3, :cond_7

    #@6
    .line 745
    :cond_6
    :goto_6
    return-void

    #@7
    .line 472
    :cond_7
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@a
    move-result-object v41

    #@b
    .line 473
    .local v41, viewRoot:Landroid/view/ViewRootImpl;
    const/16 v29, 0x0

    #@d
    .line 474
    .local v29, isParentExternal:Z
    if-eqz v41, :cond_23

    #@f
    .line 475
    move-object/from16 v0, v41

    #@11
    iget-object v3, v0, Landroid/view/ViewRootImpl;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@13
    move-object/from16 v0, p0

    #@15
    iput-object v3, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@17
    .line 477
    move-object/from16 v0, v41

    #@19
    iget-object v3, v0, Landroid/view/ViewRootImpl;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    #@1b
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@1d
    and-int/lit8 v3, v3, 0x1

    #@1f
    if-eqz v3, :cond_43f

    #@21
    const/16 v29, 0x1

    #@23
    .line 481
    :cond_23
    :goto_23
    move-object/from16 v0, p0

    #@25
    iget-object v3, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@27
    if-eqz v3, :cond_34

    #@29
    .line 482
    move-object/from16 v0, p0

    #@2b
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v4, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@31
    invoke-virtual {v3, v4}, Landroid/view/Surface;->setCompatibilityTranslator(Landroid/content/res/CompatibilityInfo$Translator;)V

    #@34
    .line 487
    :cond_34
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getDisplay()Landroid/view/Display;

    #@37
    move-result-object v25

    #@38
    .line 488
    .local v25, ext_display:Landroid/view/Display;
    const/16 v28, 0x0

    #@3a
    .line 489
    .local v28, isExternal:Z
    const/16 v37, 0x0

    #@3c
    .line 490
    .local v37, skipResizing:Z
    const/16 v24, 0x0

    #@3e
    .line 491
    .local v24, dsdrExternalWidth:I
    const/16 v23, 0x0

    #@40
    .line 493
    .local v23, dsdrExternalHeight:I
    if-eqz v25, :cond_62

    #@42
    .line 494
    move-object/from16 v0, p0

    #@44
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@46
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@48
    and-int/lit8 v3, v3, 0x1

    #@4a
    if-eqz v3, :cond_443

    #@4c
    const/16 v28, 0x1

    #@4e
    .line 495
    :goto_4e
    move-object/from16 v0, p0

    #@50
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@52
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@54
    and-int/lit8 v3, v3, 0x2

    #@56
    if-eqz v3, :cond_447

    #@58
    const/16 v37, 0x1

    #@5a
    .line 496
    :goto_5a
    invoke-virtual/range {v25 .. v25}, Landroid/view/Display;->getDsdrExternalWidth()I

    #@5d
    move-result v24

    #@5e
    .line 497
    invoke-virtual/range {v25 .. v25}, Landroid/view/Display;->getDsdrExternalHeight()I

    #@61
    move-result v23

    #@62
    .line 501
    :cond_62
    move-object/from16 v0, p0

    #@64
    iget v0, v0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@66
    move/from16 v32, v0

    #@68
    .line 502
    .local v32, myWidth:I
    if-gtz v32, :cond_6e

    #@6a
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getWidth()I

    #@6d
    move-result v32

    #@6e
    .line 503
    :cond_6e
    move-object/from16 v0, p0

    #@70
    iget v0, v0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@72
    move/from16 v31, v0

    #@74
    .line 504
    .local v31, myHeight:I
    if-gtz v31, :cond_7a

    #@76
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getHeight()I

    #@79
    move-result v31

    #@7a
    .line 506
    :cond_7a
    move-object/from16 v0, p0

    #@7c
    iget-object v3, v0, Landroid/view/SurfaceView;->mLocation:[I

    #@7e
    move-object/from16 v0, p0

    #@80
    invoke-virtual {v0, v3}, Landroid/view/SurfaceView;->getLocationInWindow([I)V

    #@83
    .line 507
    move-object/from16 v0, p0

    #@85
    iget-object v3, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@87
    if-nez v3, :cond_44b

    #@89
    const/16 v20, 0x1

    #@8b
    .line 508
    .local v20, creating:Z
    :goto_8b
    move-object/from16 v0, p0

    #@8d
    iget v3, v0, Landroid/view/SurfaceView;->mFormat:I

    #@8f
    move-object/from16 v0, p0

    #@91
    iget v4, v0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@93
    if-eq v3, v4, :cond_44f

    #@95
    const/16 v26, 0x1

    #@97
    .line 509
    .local v26, formatChanged:Z
    :goto_97
    move-object/from16 v0, p0

    #@99
    iget v3, v0, Landroid/view/SurfaceView;->mWidth:I

    #@9b
    move/from16 v0, v32

    #@9d
    if-ne v3, v0, :cond_a7

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget v3, v0, Landroid/view/SurfaceView;->mHeight:I

    #@a3
    move/from16 v0, v31

    #@a5
    if-eq v3, v0, :cond_453

    #@a7
    :cond_a7
    const/16 v36, 0x1

    #@a9
    .line 510
    .local v36, sizeChanged:Z
    :goto_a9
    move-object/from16 v0, p0

    #@ab
    iget-boolean v3, v0, Landroid/view/SurfaceView;->mVisible:Z

    #@ad
    move-object/from16 v0, p0

    #@af
    iget-boolean v4, v0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@b1
    if-eq v3, v4, :cond_457

    #@b3
    const/16 v43, 0x1

    #@b5
    .line 512
    .local v43, visibleChanged:Z
    :goto_b5
    if-nez p1, :cond_e7

    #@b7
    if-nez v20, :cond_e7

    #@b9
    if-nez v26, :cond_e7

    #@bb
    if-nez v36, :cond_e7

    #@bd
    if-nez v43, :cond_e7

    #@bf
    move-object/from16 v0, p0

    #@c1
    iget v3, v0, Landroid/view/SurfaceView;->mLeft:I

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget-object v4, v0, Landroid/view/SurfaceView;->mLocation:[I

    #@c7
    const/4 v5, 0x0

    #@c8
    aget v4, v4, v5

    #@ca
    if-ne v3, v4, :cond_e7

    #@cc
    move-object/from16 v0, p0

    #@ce
    iget v3, v0, Landroid/view/SurfaceView;->mTop:I

    #@d0
    move-object/from16 v0, p0

    #@d2
    iget-object v4, v0, Landroid/view/SurfaceView;->mLocation:[I

    #@d4
    const/4 v5, 0x1

    #@d5
    aget v4, v4, v5

    #@d7
    if-ne v3, v4, :cond_e7

    #@d9
    move-object/from16 v0, p0

    #@db
    iget-boolean v3, v0, Landroid/view/SurfaceView;->mUpdateWindowNeeded:Z

    #@dd
    if-nez v3, :cond_e7

    #@df
    move-object/from16 v0, p0

    #@e1
    iget-boolean v3, v0, Landroid/view/SurfaceView;->mReportDrawNeeded:Z

    #@e3
    if-nez v3, :cond_e7

    #@e5
    if-eqz p2, :cond_6

    #@e7
    .line 523
    :cond_e7
    :try_start_e7
    move-object/from16 v0, p0

    #@e9
    iget-boolean v0, v0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@eb
    move/from16 v42, v0

    #@ed
    move/from16 v0, v42

    #@ef
    move-object/from16 v1, p0

    #@f1
    iput-boolean v0, v1, Landroid/view/SurfaceView;->mVisible:Z

    #@f3
    .line 524
    .local v42, visible:Z
    move-object/from16 v0, p0

    #@f5
    iget-object v3, v0, Landroid/view/SurfaceView;->mLocation:[I

    #@f7
    const/4 v4, 0x0

    #@f8
    aget v3, v3, v4

    #@fa
    move-object/from16 v0, p0

    #@fc
    iput v3, v0, Landroid/view/SurfaceView;->mLeft:I

    #@fe
    .line 525
    move-object/from16 v0, p0

    #@100
    iget-object v3, v0, Landroid/view/SurfaceView;->mLocation:[I

    #@102
    const/4 v4, 0x1

    #@103
    aget v3, v3, v4

    #@105
    move-object/from16 v0, p0

    #@107
    iput v3, v0, Landroid/view/SurfaceView;->mTop:I

    #@109
    .line 526
    move/from16 v0, v32

    #@10b
    move-object/from16 v1, p0

    #@10d
    iput v0, v1, Landroid/view/SurfaceView;->mWidth:I

    #@10f
    .line 527
    move/from16 v0, v31

    #@111
    move-object/from16 v1, p0

    #@113
    iput v0, v1, Landroid/view/SurfaceView;->mHeight:I

    #@115
    .line 528
    move-object/from16 v0, p0

    #@117
    iget v3, v0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@119
    move-object/from16 v0, p0

    #@11b
    iput v3, v0, Landroid/view/SurfaceView;->mFormat:I

    #@11d
    .line 533
    move-object/from16 v0, p0

    #@11f
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@121
    move-object/from16 v0, p0

    #@123
    iget v4, v0, Landroid/view/SurfaceView;->mLeft:I

    #@125
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    #@127
    .line 534
    move-object/from16 v0, p0

    #@129
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@12b
    move-object/from16 v0, p0

    #@12d
    iget v4, v0, Landroid/view/SurfaceView;->mTop:I

    #@12f
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    #@131
    .line 535
    move-object/from16 v0, p0

    #@133
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@135
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getWidth()I

    #@138
    move-result v4

    #@139
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    #@13b
    .line 536
    move-object/from16 v0, p0

    #@13d
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@13f
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getHeight()I

    #@142
    move-result v4

    #@143
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    #@145
    .line 540
    if-nez v37, :cond_1b3

    #@147
    if-lez v24, :cond_1b3

    #@149
    if-lez v23, :cond_1b3

    #@14b
    .line 541
    if-eqz v28, :cond_1b3

    #@14d
    .line 542
    move-object/from16 v0, p0

    #@14f
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@151
    move/from16 v0, v24

    #@153
    move-object/from16 v1, p0

    #@155
    iput v0, v1, Landroid/view/SurfaceView;->mWidth:I

    #@157
    move/from16 v0, v24

    #@159
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    #@15b
    move/from16 v32, v24

    #@15d
    .line 543
    move-object/from16 v0, p0

    #@15f
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@161
    move/from16 v0, v23

    #@163
    move-object/from16 v1, p0

    #@165
    iput v0, v1, Landroid/view/SurfaceView;->mHeight:I

    #@167
    move/from16 v0, v23

    #@169
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    #@16b
    move/from16 v31, v23

    #@16d
    .line 544
    move-object/from16 v0, p0

    #@16f
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@171
    const/4 v4, 0x0

    #@172
    move-object/from16 v0, p0

    #@174
    iput v4, v0, Landroid/view/SurfaceView;->mLeft:I

    #@176
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    #@178
    .line 545
    move-object/from16 v0, p0

    #@17a
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@17c
    const/4 v4, 0x0

    #@17d
    move-object/from16 v0, p0

    #@17f
    iput v4, v0, Landroid/view/SurfaceView;->mTop:I

    #@181
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    #@183
    .line 546
    const-string v3, "SurfaceView"

    #@185
    new-instance v4, Ljava/lang/StringBuilder;

    #@187
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18a
    const-string v5, "[DSDR][SV.java] updateWindow w,h=("

    #@18c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v4

    #@190
    move-object/from16 v0, p0

    #@192
    iget v5, v0, Landroid/view/SurfaceView;->mWidth:I

    #@194
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@197
    move-result-object v4

    #@198
    const-string v5, ","

    #@19a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v4

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iget v5, v0, Landroid/view/SurfaceView;->mHeight:I

    #@1a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v4

    #@1a6
    const-string v5, ")"

    #@1a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v4

    #@1ac
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1af
    move-result-object v4

    #@1b0
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b3
    .line 551
    :cond_1b3
    move-object/from16 v0, p0

    #@1b5
    iget-object v3, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@1b7
    if-eqz v3, :cond_1c4

    #@1b9
    .line 552
    move-object/from16 v0, p0

    #@1bb
    iget-object v3, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@1bd
    move-object/from16 v0, p0

    #@1bf
    iget-object v4, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@1c1
    invoke-virtual {v3, v4}, Landroid/content/res/CompatibilityInfo$Translator;->translateLayoutParamsInAppWindowToScreen(Landroid/view/WindowManager$LayoutParams;)V

    #@1c4
    .line 555
    :cond_1c4
    move-object/from16 v0, p0

    #@1c6
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iget v4, v0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@1cc
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->format:I

    #@1ce
    .line 556
    move-object/from16 v0, p0

    #@1d0
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@1d2
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1d4
    or-int/lit16 v4, v4, 0x4218

    #@1d6
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1d8
    .line 563
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getContext()Landroid/content/Context;

    #@1db
    move-result-object v3

    #@1dc
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1df
    move-result-object v3

    #@1e0
    invoke-virtual {v3}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@1e3
    move-result-object v3

    #@1e4
    invoke-virtual {v3}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@1e7
    move-result v3

    #@1e8
    if-nez v3, :cond_211

    #@1ea
    .line 564
    move-object/from16 v0, p0

    #@1ec
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@1ee
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1f0
    const/high16 v5, 0x2000

    #@1f2
    or-int/2addr v4, v5

    #@1f3
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1f5
    .line 566
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getContext()Landroid/content/Context;

    #@1f8
    move-result-object v3

    #@1f9
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1fc
    move-result-object v3

    #@1fd
    invoke-virtual {v3}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@200
    move-result-object v3

    #@201
    invoke-virtual {v3}, Landroid/content/res/CompatibilityInfo;->requiresWvgaAspect()Z

    #@204
    move-result v3

    #@205
    if-eqz v3, :cond_211

    #@207
    .line 567
    move-object/from16 v0, p0

    #@209
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@20b
    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@20d
    or-int/lit16 v4, v4, 0x1000

    #@20f
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@211
    .line 572
    :cond_211
    move-object/from16 v0, p0

    #@213
    iget-object v3, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@215
    if-nez v3, :cond_25d

    #@217
    .line 573
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getDisplay()Landroid/view/Display;

    #@21a
    move-result-object v21

    #@21b
    .line 574
    .local v21, display:Landroid/view/Display;
    new-instance v3, Landroid/view/SurfaceView$MyWindow;

    #@21d
    move-object/from16 v0, p0

    #@21f
    invoke-direct {v3, v0}, Landroid/view/SurfaceView$MyWindow;-><init>(Landroid/view/SurfaceView;)V

    #@222
    move-object/from16 v0, p0

    #@224
    iput-object v3, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@226
    .line 575
    move-object/from16 v0, p0

    #@228
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@22a
    move-object/from16 v0, p0

    #@22c
    iget v4, v0, Landroid/view/SurfaceView;->mWindowType:I

    #@22e
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@230
    .line 576
    move-object/from16 v0, p0

    #@232
    iget-object v3, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@234
    const v4, 0x800033

    #@237
    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@239
    .line 577
    move-object/from16 v0, p0

    #@23b
    iget-object v3, v0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@23d
    move-object/from16 v0, p0

    #@23f
    iget-object v4, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@241
    move-object/from16 v0, p0

    #@243
    iget-object v5, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@245
    iget v5, v5, Landroid/view/SurfaceView$MyWindow;->mSeq:I

    #@247
    move-object/from16 v0, p0

    #@249
    iget-object v6, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@24b
    move-object/from16 v0, p0

    #@24d
    iget-boolean v7, v0, Landroid/view/SurfaceView;->mVisible:Z

    #@24f
    if-eqz v7, :cond_45b

    #@251
    const/4 v7, 0x0

    #@252
    :goto_252
    invoke-virtual/range {v21 .. v21}, Landroid/view/Display;->getDisplayId()I

    #@255
    move-result v8

    #@256
    move-object/from16 v0, p0

    #@258
    iget-object v9, v0, Landroid/view/SurfaceView;->mContentInsets:Landroid/graphics/Rect;

    #@25a
    invoke-interface/range {v3 .. v9}, Landroid/view/IWindowSession;->addToDisplayWithoutInputChannel(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;)I

    #@25d
    .line 586
    .end local v21           #display:Landroid/view/Display;
    :cond_25d
    move-object/from16 v0, p0

    #@25f
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@261
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_264
    .catch Landroid/os/RemoteException; {:try_start_e7 .. :try_end_264} :catch_485

    #@264
    .line 588
    const/4 v3, 0x0

    #@265
    :try_start_265
    move-object/from16 v0, p0

    #@267
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mUpdateWindowNeeded:Z

    #@269
    .line 589
    move-object/from16 v0, p0

    #@26b
    iget-boolean v0, v0, Landroid/view/SurfaceView;->mReportDrawNeeded:Z

    #@26d
    move/from16 v35, v0

    #@26f
    .line 590
    .local v35, reportDrawNeeded:Z
    const/4 v3, 0x0

    #@270
    move-object/from16 v0, p0

    #@272
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mReportDrawNeeded:Z

    #@274
    .line 591
    if-nez v42, :cond_45f

    #@276
    const/4 v3, 0x1

    #@277
    :goto_277
    move-object/from16 v0, p0

    #@279
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mDrawingStopped:Z

    #@27b
    .line 595
    move-object/from16 v0, p0

    #@27d
    iget-object v3, v0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@27f
    move-object/from16 v0, p0

    #@281
    iget-object v4, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@283
    move-object/from16 v0, p0

    #@285
    iget-object v5, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@287
    iget v5, v5, Landroid/view/SurfaceView$MyWindow;->mSeq:I

    #@289
    move-object/from16 v0, p0

    #@28b
    iget-object v6, v0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@28d
    move-object/from16 v0, p0

    #@28f
    iget v7, v0, Landroid/view/SurfaceView;->mWidth:I

    #@291
    move-object/from16 v0, p0

    #@293
    iget v8, v0, Landroid/view/SurfaceView;->mHeight:I

    #@295
    if-eqz v42, :cond_462

    #@297
    const/4 v9, 0x0

    #@298
    :goto_298
    const/4 v10, 0x2

    #@299
    move-object/from16 v0, p0

    #@29b
    iget-object v11, v0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@29d
    move-object/from16 v0, p0

    #@29f
    iget-object v12, v0, Landroid/view/SurfaceView;->mContentInsets:Landroid/graphics/Rect;

    #@2a1
    move-object/from16 v0, p0

    #@2a3
    iget-object v13, v0, Landroid/view/SurfaceView;->mVisibleInsets:Landroid/graphics/Rect;

    #@2a5
    move-object/from16 v0, p0

    #@2a7
    iget-object v14, v0, Landroid/view/SurfaceView;->mConfiguration:Landroid/content/res/Configuration;

    #@2a9
    move-object/from16 v0, p0

    #@2ab
    iget-object v15, v0, Landroid/view/SurfaceView;->mNewSurface:Landroid/view/Surface;

    #@2ad
    invoke-interface/range {v3 .. v15}, Landroid/view/IWindowSession;->relayout(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I

    #@2b0
    move-result v34

    #@2b1
    .line 601
    .local v34, relayoutResult:I
    and-int/lit8 v3, v34, 0x2

    #@2b3
    if-eqz v3, :cond_2ba

    #@2b5
    .line 602
    const/4 v3, 0x1

    #@2b6
    move-object/from16 v0, p0

    #@2b8
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mReportDrawNeeded:Z

    #@2ba
    .line 609
    :cond_2ba
    if-eqz v25, :cond_375

    #@2bc
    invoke-virtual/range {v25 .. v25}, Landroid/view/Display;->getDsdrStatus()I

    #@2bf
    move-result v3

    #@2c0
    const/4 v4, 0x1

    #@2c1
    if-le v3, v4, :cond_375

    #@2c3
    if-eqz v28, :cond_375

    #@2c5
    and-int/lit8 v3, v34, 0x4

    #@2c7
    if-lez v3, :cond_375

    #@2c9
    if-eqz v42, :cond_375

    #@2cb
    .line 611
    const-string v3, "SurfaceView"

    #@2cd
    new-instance v4, Ljava/lang/StringBuilder;

    #@2cf
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d2
    const-string v5, "[DSDR][SurfaceView.java]updateWindow() : mDsdrDlg = "

    #@2d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d7
    move-result-object v4

    #@2d8
    move-object/from16 v0, p0

    #@2da
    iget-object v5, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@2dc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2df
    move-result-object v4

    #@2e0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e3
    move-result-object v4

    #@2e4
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e7
    .catchall {:try_start_265 .. :try_end_2e7} :catchall_47c

    #@2e7
    .line 612
    if-nez v29, :cond_466

    #@2e9
    .line 614
    :try_start_2e9
    move-object/from16 v0, p0

    #@2eb
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@2ed
    if-nez v3, :cond_367

    #@2ef
    .line 615
    new-instance v3, Landroid/app/Dialog;

    #@2f1
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getContext()Landroid/content/Context;

    #@2f4
    move-result-object v4

    #@2f5
    invoke-direct {v3, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    #@2f8
    move-object/from16 v0, p0

    #@2fa
    iput-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@2fc
    .line 616
    move-object/from16 v0, p0

    #@2fe
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@300
    const/4 v4, 0x1

    #@301
    invoke-virtual {v3, v4}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    #@304
    .line 618
    move-object/from16 v0, p0

    #@306
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@308
    new-instance v4, Landroid/view/View;

    #@30a
    invoke-virtual/range {p0 .. p0}, Landroid/view/SurfaceView;->getContext()Landroid/content/Context;

    #@30d
    move-result-object v5

    #@30e
    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@311
    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    #@314
    .line 619
    move-object/from16 v0, p0

    #@316
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@318
    const/4 v4, 0x0

    #@319
    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    #@31c
    .line 620
    move-object/from16 v0, p0

    #@31e
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@320
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@323
    move-result-object v3

    #@324
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@327
    move-result-object v22

    #@328
    .line 621
    .local v22, dlgLp:Landroid/view/WindowManager$LayoutParams;
    const/4 v3, 0x0

    #@329
    move-object/from16 v0, v22

    #@32b
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@32d
    .line 622
    const/4 v3, 0x0

    #@32e
    move-object/from16 v0, v22

    #@330
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@332
    .line 623
    const/4 v3, -0x1

    #@333
    move-object/from16 v0, v22

    #@335
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    #@337
    .line 624
    const/4 v3, -0x1

    #@338
    move-object/from16 v0, v22

    #@33a
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    #@33c
    .line 625
    const/4 v3, 0x0

    #@33d
    move-object/from16 v0, v22

    #@33f
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@341
    .line 626
    move-object/from16 v0, v22

    #@343
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@345
    and-int/lit8 v3, v3, -0x3

    #@347
    move-object/from16 v0, v22

    #@349
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@34b
    .line 627
    move-object/from16 v0, v22

    #@34d
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@34f
    or-int/lit8 v3, v3, 0x8

    #@351
    move-object/from16 v0, v22

    #@353
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@355
    .line 629
    const/4 v3, 0x1

    #@356
    move-object/from16 v0, v22

    #@358
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@35a
    .line 630
    move-object/from16 v0, p0

    #@35c
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@35e
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@361
    move-result-object v3

    #@362
    move-object/from16 v0, v22

    #@364
    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@367
    .line 632
    .end local v22           #dlgLp:Landroid/view/WindowManager$LayoutParams;
    :cond_367
    const-string v3, "SurfaceView"

    #@369
    const-string v4, "[DSDR][SurfaceView.java]updateWindow() : Added External Window for External SurfaceView"

    #@36b
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@36e
    .line 633
    move-object/from16 v0, p0

    #@370
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@372
    invoke-virtual {v3}, Landroid/app/Dialog;->show()V
    :try_end_375
    .catchall {:try_start_2e9 .. :try_end_375} :catchall_47c
    .catch Ljava/lang/Exception; {:try_start_2e9 .. :try_end_375} :catch_5b2

    #@375
    .line 641
    :cond_375
    :goto_375
    if-eqz v43, :cond_392

    #@377
    .line 642
    if-nez v42, :cond_392

    #@379
    :try_start_379
    move-object/from16 v0, p0

    #@37b
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@37d
    if-eqz v3, :cond_392

    #@37f
    .line 643
    move-object/from16 v0, p0

    #@381
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@383
    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    #@386
    .line 644
    const/4 v3, 0x0

    #@387
    move-object/from16 v0, p0

    #@389
    iput-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@38b
    .line 645
    const-string v3, "SurfaceView"

    #@38d
    const-string v4, "[DSDR][SurfaceView.java]updateWindow() : mDsdrDlg.dismiss"

    #@38f
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@392
    .line 653
    :cond_392
    move-object/from16 v0, p0

    #@394
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@396
    const/4 v4, 0x0

    #@397
    iput v4, v3, Landroid/graphics/Rect;->left:I

    #@399
    .line 654
    move-object/from16 v0, p0

    #@39b
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@39d
    const/4 v4, 0x0

    #@39e
    iput v4, v3, Landroid/graphics/Rect;->top:I

    #@3a0
    .line 655
    move-object/from16 v0, p0

    #@3a2
    iget-object v3, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@3a4
    if-nez v3, :cond_488

    #@3a6
    .line 656
    move-object/from16 v0, p0

    #@3a8
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@3aa
    move-object/from16 v0, p0

    #@3ac
    iget-object v4, v0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@3ae
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    #@3b1
    move-result v4

    #@3b2
    iput v4, v3, Landroid/graphics/Rect;->right:I

    #@3b4
    .line 657
    move-object/from16 v0, p0

    #@3b6
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@3b8
    move-object/from16 v0, p0

    #@3ba
    iget-object v4, v0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@3bc
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    #@3bf
    move-result v4

    #@3c0
    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    #@3c2
    .line 664
    :goto_3c2
    move-object/from16 v0, p0

    #@3c4
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@3c6
    iget v0, v3, Landroid/graphics/Rect;->right:I

    #@3c8
    move/from16 v40, v0

    #@3ca
    .line 665
    .local v40, surfaceWidth:I
    move-object/from16 v0, p0

    #@3cc
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@3ce
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    #@3d0
    move/from16 v39, v0

    #@3d2
    .line 666
    .local v39, surfaceHeight:I
    move-object/from16 v0, p0

    #@3d4
    iget v3, v0, Landroid/view/SurfaceView;->mLastSurfaceWidth:I

    #@3d6
    move/from16 v0, v40

    #@3d8
    if-ne v3, v0, :cond_3e2

    #@3da
    move-object/from16 v0, p0

    #@3dc
    iget v3, v0, Landroid/view/SurfaceView;->mLastSurfaceHeight:I

    #@3de
    move/from16 v0, v39

    #@3e0
    if-eq v3, v0, :cond_4bc

    #@3e2
    :cond_3e2
    const/16 v33, 0x1

    #@3e4
    .line 668
    .local v33, realSizeChanged:Z
    :goto_3e4
    move/from16 v0, v40

    #@3e6
    move-object/from16 v1, p0

    #@3e8
    iput v0, v1, Landroid/view/SurfaceView;->mLastSurfaceWidth:I

    #@3ea
    .line 669
    move/from16 v0, v39

    #@3ec
    move-object/from16 v1, p0

    #@3ee
    iput v0, v1, Landroid/view/SurfaceView;->mLastSurfaceHeight:I
    :try_end_3f0
    .catchall {:try_start_379 .. :try_end_3f0} :catchall_47c

    #@3f0
    .line 671
    :try_start_3f0
    move-object/from16 v0, p0

    #@3f2
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@3f4
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_3f7
    .catch Landroid/os/RemoteException; {:try_start_3f0 .. :try_end_3f7} :catch_485

    #@3f7
    .line 675
    or-int v3, v20, v35

    #@3f9
    or-int p2, p2, v3

    #@3fb
    .line 677
    const/16 v19, 0x0

    #@3fd
    .line 679
    .local v19, callbacks:[Landroid/view/SurfaceHolder$Callback;
    and-int/lit8 v3, v34, 0x4

    #@3ff
    if-eqz v3, :cond_4c0

    #@401
    const/16 v38, 0x1

    #@403
    .line 681
    .local v38, surfaceChanged:Z
    :goto_403
    :try_start_403
    move-object/from16 v0, p0

    #@405
    iget-boolean v3, v0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@407
    if-eqz v3, :cond_4c4

    #@409
    if-nez v38, :cond_40f

    #@40b
    if-nez v42, :cond_4c4

    #@40d
    if-eqz v43, :cond_4c4

    #@40f
    .line 682
    :cond_40f
    const/4 v3, 0x0

    #@410
    move-object/from16 v0, p0

    #@412
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@414
    .line 683
    move-object/from16 v0, p0

    #@416
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@418
    invoke-virtual {v3}, Landroid/view/Surface;->isValid()Z

    #@41b
    move-result v3

    #@41c
    if-eqz v3, :cond_4c4

    #@41e
    .line 685
    invoke-direct/range {p0 .. p0}, Landroid/view/SurfaceView;->getSurfaceCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@421
    move-result-object v19

    #@422
    .line 686
    move-object/from16 v17, v19

    #@424
    .local v17, arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v17

    #@426
    array-length v0, v0

    #@427
    move/from16 v30, v0

    #@429
    .local v30, len$:I
    const/16 v27, 0x0

    #@42b
    .local v27, i$:I
    :goto_42b
    move/from16 v0, v27

    #@42d
    move/from16 v1, v30

    #@42f
    if-ge v0, v1, :cond_4c4

    #@431
    aget-object v18, v17, v27

    #@433
    .line 687
    .local v18, c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@435
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@437
    move-object/from16 v0, v18

    #@439
    invoke-interface {v0, v3}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    :try_end_43c
    .catchall {:try_start_403 .. :try_end_43c} :catchall_574

    #@43c
    .line 686
    add-int/lit8 v27, v27, 0x1

    #@43e
    goto :goto_42b

    #@43f
    .line 477
    .end local v17           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v18           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v19           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v20           #creating:Z
    .end local v23           #dsdrExternalHeight:I
    .end local v24           #dsdrExternalWidth:I
    .end local v25           #ext_display:Landroid/view/Display;
    .end local v26           #formatChanged:Z
    .end local v27           #i$:I
    .end local v28           #isExternal:Z
    .end local v30           #len$:I
    .end local v31           #myHeight:I
    .end local v32           #myWidth:I
    .end local v33           #realSizeChanged:Z
    .end local v34           #relayoutResult:I
    .end local v35           #reportDrawNeeded:Z
    .end local v36           #sizeChanged:Z
    .end local v37           #skipResizing:Z
    .end local v38           #surfaceChanged:Z
    .end local v39           #surfaceHeight:I
    .end local v40           #surfaceWidth:I
    .end local v42           #visible:Z
    .end local v43           #visibleChanged:Z
    :cond_43f
    const/16 v29, 0x0

    #@441
    goto/16 :goto_23

    #@443
    .line 494
    .restart local v23       #dsdrExternalHeight:I
    .restart local v24       #dsdrExternalWidth:I
    .restart local v25       #ext_display:Landroid/view/Display;
    .restart local v28       #isExternal:Z
    .restart local v37       #skipResizing:Z
    :cond_443
    const/16 v28, 0x0

    #@445
    goto/16 :goto_4e

    #@447
    .line 495
    :cond_447
    const/16 v37, 0x0

    #@449
    goto/16 :goto_5a

    #@44b
    .line 507
    .restart local v31       #myHeight:I
    .restart local v32       #myWidth:I
    :cond_44b
    const/16 v20, 0x0

    #@44d
    goto/16 :goto_8b

    #@44f
    .line 508
    .restart local v20       #creating:Z
    :cond_44f
    const/16 v26, 0x0

    #@451
    goto/16 :goto_97

    #@453
    .line 509
    .restart local v26       #formatChanged:Z
    :cond_453
    const/16 v36, 0x0

    #@455
    goto/16 :goto_a9

    #@457
    .line 510
    .restart local v36       #sizeChanged:Z
    :cond_457
    const/16 v43, 0x0

    #@459
    goto/16 :goto_b5

    #@45b
    .line 577
    .restart local v21       #display:Landroid/view/Display;
    .restart local v42       #visible:Z
    .restart local v43       #visibleChanged:Z
    :cond_45b
    const/16 v7, 0x8

    #@45d
    goto/16 :goto_252

    #@45f
    .line 591
    .end local v21           #display:Landroid/view/Display;
    .restart local v35       #reportDrawNeeded:Z
    :cond_45f
    const/4 v3, 0x0

    #@460
    goto/16 :goto_277

    #@462
    .line 595
    :cond_462
    const/16 v9, 0x8

    #@464
    goto/16 :goto_298

    #@466
    .line 636
    .restart local v34       #relayoutResult:I
    :cond_466
    :try_start_466
    move-object/from16 v0, p0

    #@468
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@46a
    if-eqz v3, :cond_375

    #@46c
    .line 637
    move-object/from16 v0, p0

    #@46e
    iget-object v3, v0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@470
    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    #@473
    .line 638
    const-string v3, "SurfaceView"

    #@475
    const-string v4, "[DSDR][SurfaceView.java]updateWindow() : mDsdrDlg.dismiss because parent window is already external"

    #@477
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_47a
    .catchall {:try_start_466 .. :try_end_47a} :catchall_47c

    #@47a
    goto/16 :goto_375

    #@47c
    .line 671
    .end local v34           #relayoutResult:I
    .end local v35           #reportDrawNeeded:Z
    :catchall_47c
    move-exception v3

    #@47d
    :try_start_47d
    move-object/from16 v0, p0

    #@47f
    iget-object v4, v0, Landroid/view/SurfaceView;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@481
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@484
    throw v3
    :try_end_485
    .catch Landroid/os/RemoteException; {:try_start_47d .. :try_end_485} :catch_485

    #@485
    .line 738
    .end local v42           #visible:Z
    :catch_485
    move-exception v3

    #@486
    goto/16 :goto_6

    #@488
    .line 659
    .restart local v34       #relayoutResult:I
    .restart local v35       #reportDrawNeeded:Z
    .restart local v42       #visible:Z
    :cond_488
    :try_start_488
    move-object/from16 v0, p0

    #@48a
    iget-object v3, v0, Landroid/view/SurfaceView;->mTranslator:Landroid/content/res/CompatibilityInfo$Translator;

    #@48c
    iget v0, v3, Landroid/content/res/CompatibilityInfo$Translator;->applicationInvertedScale:F

    #@48e
    move/from16 v16, v0

    #@490
    .line 660
    .local v16, appInvertedScale:F
    move-object/from16 v0, p0

    #@492
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@494
    move-object/from16 v0, p0

    #@496
    iget-object v4, v0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@498
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    #@49b
    move-result v4

    #@49c
    int-to-float v4, v4

    #@49d
    mul-float v4, v4, v16

    #@49f
    const/high16 v5, 0x3f00

    #@4a1
    add-float/2addr v4, v5

    #@4a2
    float-to-int v4, v4

    #@4a3
    iput v4, v3, Landroid/graphics/Rect;->right:I

    #@4a5
    .line 661
    move-object/from16 v0, p0

    #@4a7
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceFrame:Landroid/graphics/Rect;

    #@4a9
    move-object/from16 v0, p0

    #@4ab
    iget-object v4, v0, Landroid/view/SurfaceView;->mWinFrame:Landroid/graphics/Rect;

    #@4ad
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    #@4b0
    move-result v4

    #@4b1
    int-to-float v4, v4

    #@4b2
    mul-float v4, v4, v16

    #@4b4
    const/high16 v5, 0x3f00

    #@4b6
    add-float/2addr v4, v5

    #@4b7
    float-to-int v4, v4

    #@4b8
    iput v4, v3, Landroid/graphics/Rect;->bottom:I
    :try_end_4ba
    .catchall {:try_start_488 .. :try_end_4ba} :catchall_47c

    #@4ba
    goto/16 :goto_3c2

    #@4bc
    .line 666
    .end local v16           #appInvertedScale:F
    .restart local v39       #surfaceHeight:I
    .restart local v40       #surfaceWidth:I
    :cond_4bc
    const/16 v33, 0x0

    #@4be
    goto/16 :goto_3e4

    #@4c0
    .line 679
    .restart local v19       #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .restart local v33       #realSizeChanged:Z
    :cond_4c0
    const/16 v38, 0x0

    #@4c2
    goto/16 :goto_403

    #@4c4
    .line 692
    .restart local v38       #surfaceChanged:Z
    :cond_4c4
    :try_start_4c4
    move-object/from16 v0, p0

    #@4c6
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@4c8
    move-object/from16 v0, p0

    #@4ca
    iget-object v4, v0, Landroid/view/SurfaceView;->mNewSurface:Landroid/view/Surface;

    #@4cc
    invoke-virtual {v3, v4}, Landroid/view/Surface;->transferFrom(Landroid/view/Surface;)V

    #@4cf
    .line 694
    if-eqz v42, :cond_593

    #@4d1
    move-object/from16 v0, p0

    #@4d3
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurface:Landroid/view/Surface;

    #@4d5
    invoke-virtual {v3}, Landroid/view/Surface;->isValid()Z

    #@4d8
    move-result v3

    #@4d9
    if-eqz v3, :cond_593

    #@4db
    .line 695
    move-object/from16 v0, p0

    #@4dd
    iget-boolean v3, v0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@4df
    if-nez v3, :cond_512

    #@4e1
    if-nez v38, :cond_4e5

    #@4e3
    if-eqz v43, :cond_512

    #@4e5
    .line 696
    :cond_4e5
    const/4 v3, 0x1

    #@4e6
    move-object/from16 v0, p0

    #@4e8
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mSurfaceCreated:Z

    #@4ea
    .line 697
    const/4 v3, 0x1

    #@4eb
    move-object/from16 v0, p0

    #@4ed
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@4ef
    .line 699
    if-nez v19, :cond_4f5

    #@4f1
    .line 700
    invoke-direct/range {p0 .. p0}, Landroid/view/SurfaceView;->getSurfaceCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@4f4
    move-result-object v19

    #@4f5
    .line 702
    :cond_4f5
    move-object/from16 v17, v19

    #@4f7
    .restart local v17       #arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v17

    #@4f9
    array-length v0, v0

    #@4fa
    move/from16 v30, v0

    #@4fc
    .restart local v30       #len$:I
    const/16 v27, 0x0

    #@4fe
    .restart local v27       #i$:I
    :goto_4fe
    move/from16 v0, v27

    #@500
    move/from16 v1, v30

    #@502
    if-ge v0, v1, :cond_512

    #@504
    aget-object v18, v17, v27

    #@506
    .line 703
    .restart local v18       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@508
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@50a
    move-object/from16 v0, v18

    #@50c
    invoke-interface {v0, v3}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    #@50f
    .line 702
    add-int/lit8 v27, v27, 0x1

    #@511
    goto :goto_4fe

    #@512
    .line 706
    .end local v17           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v18           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v27           #i$:I
    .end local v30           #len$:I
    :cond_512
    if-nez v20, :cond_51c

    #@514
    if-nez v26, :cond_51c

    #@516
    if-nez v36, :cond_51c

    #@518
    if-nez v43, :cond_51c

    #@51a
    if-eqz v33, :cond_547

    #@51c
    .line 710
    :cond_51c
    if-nez v19, :cond_522

    #@51e
    .line 711
    invoke-direct/range {p0 .. p0}, Landroid/view/SurfaceView;->getSurfaceCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@521
    move-result-object v19

    #@522
    .line 713
    :cond_522
    move-object/from16 v17, v19

    #@524
    .restart local v17       #arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v17

    #@526
    array-length v0, v0

    #@527
    move/from16 v30, v0

    #@529
    .restart local v30       #len$:I
    const/16 v27, 0x0

    #@52b
    .restart local v27       #i$:I
    :goto_52b
    move/from16 v0, v27

    #@52d
    move/from16 v1, v30

    #@52f
    if-ge v0, v1, :cond_547

    #@531
    aget-object v18, v17, v27

    #@533
    .line 714
    .restart local v18       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@535
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@537
    move-object/from16 v0, p0

    #@539
    iget v4, v0, Landroid/view/SurfaceView;->mFormat:I

    #@53b
    move-object/from16 v0, v18

    #@53d
    move/from16 v1, v32

    #@53f
    move/from16 v2, v31

    #@541
    invoke-interface {v0, v3, v4, v1, v2}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    #@544
    .line 713
    add-int/lit8 v27, v27, 0x1

    #@546
    goto :goto_52b

    #@547
    .line 717
    .end local v17           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v18           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v27           #i$:I
    .end local v30           #len$:I
    :cond_547
    if-eqz p2, :cond_593

    #@549
    .line 719
    if-nez v19, :cond_54f

    #@54b
    .line 720
    invoke-direct/range {p0 .. p0}, Landroid/view/SurfaceView;->getSurfaceCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@54e
    move-result-object v19

    #@54f
    .line 722
    :cond_54f
    move-object/from16 v17, v19

    #@551
    .restart local v17       #arr$:[Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v17

    #@553
    array-length v0, v0

    #@554
    move/from16 v30, v0

    #@556
    .restart local v30       #len$:I
    const/16 v27, 0x0

    #@558
    .restart local v27       #i$:I
    :goto_558
    move/from16 v0, v27

    #@55a
    move/from16 v1, v30

    #@55c
    if-ge v0, v1, :cond_593

    #@55e
    aget-object v18, v17, v27

    #@560
    .line 723
    .restart local v18       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v18

    #@562
    instance-of v3, v0, Landroid/view/SurfaceHolder$Callback2;

    #@564
    if-eqz v3, :cond_571

    #@566
    .line 724
    check-cast v18, Landroid/view/SurfaceHolder$Callback2;

    #@568
    .end local v18           #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@56a
    iget-object v3, v0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@56c
    move-object/from16 v0, v18

    #@56e
    invoke-interface {v0, v3}, Landroid/view/SurfaceHolder$Callback2;->surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V
    :try_end_571
    .catchall {:try_start_4c4 .. :try_end_571} :catchall_574

    #@571
    .line 722
    :cond_571
    add-int/lit8 v27, v27, 0x1

    #@573
    goto :goto_558

    #@574
    .line 731
    .end local v17           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v27           #i$:I
    .end local v30           #len$:I
    :catchall_574
    move-exception v3

    #@575
    const/4 v4, 0x0

    #@576
    :try_start_576
    move-object/from16 v0, p0

    #@578
    iput-boolean v4, v0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@57a
    .line 732
    if-eqz p2, :cond_587

    #@57c
    .line 734
    move-object/from16 v0, p0

    #@57e
    iget-object v4, v0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@580
    move-object/from16 v0, p0

    #@582
    iget-object v5, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@584
    invoke-interface {v4, v5}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V

    #@587
    .line 736
    :cond_587
    move-object/from16 v0, p0

    #@589
    iget-object v4, v0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@58b
    move-object/from16 v0, p0

    #@58d
    iget-object v5, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@58f
    invoke-interface {v4, v5}, Landroid/view/IWindowSession;->performDeferredDestroy(Landroid/view/IWindow;)V

    #@592
    .line 731
    throw v3

    #@593
    :cond_593
    const/4 v3, 0x0

    #@594
    move-object/from16 v0, p0

    #@596
    iput-boolean v3, v0, Landroid/view/SurfaceView;->mIsCreating:Z

    #@598
    .line 732
    if-eqz p2, :cond_5a5

    #@59a
    .line 734
    move-object/from16 v0, p0

    #@59c
    iget-object v3, v0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@59e
    move-object/from16 v0, p0

    #@5a0
    iget-object v4, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@5a2
    invoke-interface {v3, v4}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V

    #@5a5
    .line 736
    :cond_5a5
    move-object/from16 v0, p0

    #@5a7
    iget-object v3, v0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@5a9
    move-object/from16 v0, p0

    #@5ab
    iget-object v4, v0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@5ad
    invoke-interface {v3, v4}, Landroid/view/IWindowSession;->performDeferredDestroy(Landroid/view/IWindow;)V
    :try_end_5b0
    .catch Landroid/os/RemoteException; {:try_start_576 .. :try_end_5b0} :catch_485

    #@5b0
    goto/16 :goto_6

    #@5b2
    .line 634
    .end local v19           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v33           #realSizeChanged:Z
    .end local v38           #surfaceChanged:Z
    .end local v39           #surfaceHeight:I
    .end local v40           #surfaceWidth:I
    :catch_5b2
    move-exception v3

    #@5b3
    goto/16 :goto_375
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 379
    iget v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@2
    const/16 v1, 0x3e8

    #@4
    if-eq v0, v1, :cond_14

    #@6
    .line 381
    iget v0, p0, Landroid/view/SurfaceView;->mPrivateFlags:I

    #@8
    and-int/lit16 v0, v0, 0x80

    #@a
    const/16 v1, 0x80

    #@c
    if-ne v0, v1, :cond_14

    #@e
    .line 383
    const/4 v0, 0x0

    #@f
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@11
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@14
    .line 386
    :cond_14
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@17
    .line 387
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    #@0
    .prologue
    .line 367
    iget v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@2
    const/16 v1, 0x3e8

    #@4
    if-eq v0, v1, :cond_12

    #@6
    .line 369
    iget v0, p0, Landroid/view/SurfaceView;->mPrivateFlags:I

    #@8
    and-int/lit16 v0, v0, 0x80

    #@a
    if-nez v0, :cond_12

    #@c
    .line 371
    const/4 v0, 0x0

    #@d
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@f
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    #@12
    .line 374
    :cond_12
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    #@15
    .line 375
    return-void
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .registers 11
    .parameter "region"

    #@0
    .prologue
    .line 340
    iget v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@2
    const/16 v3, 0x3e8

    #@4
    if-ne v0, v3, :cond_b

    #@6
    .line 341
    invoke-super {p0, p1}, Landroid/view/View;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    #@9
    move-result v7

    #@a
    .line 362
    :cond_a
    :goto_a
    return v7

    #@b
    .line 344
    :cond_b
    const/4 v7, 0x1

    #@c
    .line 345
    .local v7, opaque:Z
    iget v0, p0, Landroid/view/SurfaceView;->mPrivateFlags:I

    #@e
    and-int/lit16 v0, v0, 0x80

    #@10
    if-nez v0, :cond_20

    #@12
    .line 347
    invoke-super {p0, p1}, Landroid/view/View;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    #@15
    move-result v7

    #@16
    .line 359
    :cond_16
    :goto_16
    iget v0, p0, Landroid/view/SurfaceView;->mRequestedFormat:I

    #@18
    invoke-static {v0}, Landroid/graphics/PixelFormat;->formatHasAlpha(I)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_a

    #@1e
    .line 360
    const/4 v7, 0x0

    #@1f
    goto :goto_a

    #@20
    .line 348
    :cond_20
    if-eqz p1, :cond_16

    #@22
    .line 349
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getWidth()I

    #@25
    move-result v8

    #@26
    .line 350
    .local v8, w:I
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getHeight()I

    #@29
    move-result v6

    #@2a
    .line 351
    .local v6, h:I
    if-lez v8, :cond_16

    #@2c
    if-lez v6, :cond_16

    #@2e
    .line 352
    iget-object v0, p0, Landroid/view/SurfaceView;->mLocation:[I

    #@30
    invoke-virtual {p0, v0}, Landroid/view/SurfaceView;->getLocationInWindow([I)V

    #@33
    .line 354
    iget-object v0, p0, Landroid/view/SurfaceView;->mLocation:[I

    #@35
    const/4 v3, 0x0

    #@36
    aget v1, v0, v3

    #@38
    .line 355
    .local v1, l:I
    iget-object v0, p0, Landroid/view/SurfaceView;->mLocation:[I

    #@3a
    const/4 v3, 0x1

    #@3b
    aget v2, v0, v3

    #@3d
    .line 356
    .local v2, t:I
    add-int v3, v1, v8

    #@3f
    add-int v4, v2, v6

    #@41
    sget-object v5, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@43
    move-object v0, p1

    #@44
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@47
    goto :goto_16
.end method

.method public getHolder()Landroid/view/SurfaceHolder;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/view/SurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    #@2
    return-object v0
.end method

.method handleGetNewSurface()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 757
    invoke-direct {p0, v0, v0}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@4
    .line 758
    return-void
.end method

.method public isFixedSize()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 768
    iget v0, p0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@3
    if-ne v0, v1, :cond_9

    #@5
    iget v0, p0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@7
    if-eq v0, v1, :cond_b

    #@9
    :cond_9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isLockScreen()Z
    .registers 2

    #@0
    .prologue
    .line 1009
    iget-boolean v0, p0, Landroid/view/SurfaceView;->mIsLockScreen:Z

    #@2
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 243
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@4
    .line 244
    iget-object v1, p0, Landroid/view/SurfaceView;->mParent:Landroid/view/ViewParent;

    #@6
    invoke-interface {v1, p0}, Landroid/view/ViewParent;->requestTransparentRegion(Landroid/view/View;)V

    #@9
    .line 245
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getWindowSession()Landroid/view/IWindowSession;

    #@c
    move-result-object v1

    #@d
    iput-object v1, p0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@f
    .line 246
    iget-object v1, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@11
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getWindowToken()Landroid/os/IBinder;

    #@14
    move-result-object v3

    #@15
    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@17
    .line 247
    iget-object v1, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@19
    const-string v3, "SurfaceView"

    #@1b
    invoke-virtual {v1, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@1e
    .line 248
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getVisibility()I

    #@21
    move-result v1

    #@22
    if-nez v1, :cond_3c

    #@24
    move v1, v2

    #@25
    :goto_25
    iput-boolean v1, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@27
    .line 250
    iget-boolean v1, p0, Landroid/view/SurfaceView;->mGlobalListenersAdded:Z

    #@29
    if-nez v1, :cond_3b

    #@2b
    .line 251
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@2e
    move-result-object v0

    #@2f
    .line 252
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/view/SurfaceView;->mScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@31
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@34
    .line 253
    iget-object v1, p0, Landroid/view/SurfaceView;->mDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    #@36
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@39
    .line 254
    iput-boolean v2, p0, Landroid/view/SurfaceView;->mGlobalListenersAdded:Z

    #@3b
    .line 256
    .end local v0           #observer:Landroid/view/ViewTreeObserver;
    :cond_3b
    return-void

    #@3c
    .line 248
    :cond_3c
    const/4 v1, 0x0

    #@3d
    goto :goto_25
.end method

.method protected onDetachedFromWindow()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 288
    iget-object v1, p0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@4
    if-eqz v1, :cond_14

    #@6
    .line 289
    const-string v1, "SurfaceView"

    #@8
    const-string v2, "[DSDR][SV.java] onDetachedFromWindow() : mDsdrDlg.dismiss"

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 290
    iget-object v1, p0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@f
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    #@12
    .line 291
    iput-object v4, p0, Landroid/view/SurfaceView;->mDsdrDlg:Landroid/app/Dialog;

    #@14
    .line 295
    :cond_14
    iget-boolean v1, p0, Landroid/view/SurfaceView;->mGlobalListenersAdded:Z

    #@16
    if-eqz v1, :cond_28

    #@18
    .line 296
    invoke-virtual {p0}, Landroid/view/SurfaceView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@1b
    move-result-object v0

    #@1c
    .line 297
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/view/SurfaceView;->mScrollChangedListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    #@21
    .line 298
    iget-object v1, p0, Landroid/view/SurfaceView;->mDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    #@23
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    #@26
    .line 299
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mGlobalListenersAdded:Z

    #@28
    .line 302
    .end local v0           #observer:Landroid/view/ViewTreeObserver;
    :cond_28
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@2a
    .line 303
    invoke-direct {p0, v3, v3}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@2d
    .line 304
    iput-boolean v3, p0, Landroid/view/SurfaceView;->mHaveFrame:Z

    #@2f
    .line 305
    iget-object v1, p0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@31
    if-eqz v1, :cond_3c

    #@33
    .line 307
    :try_start_33
    iget-object v1, p0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@35
    iget-object v2, p0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@37
    invoke-interface {v1, v2}, Landroid/view/IWindowSession;->remove(Landroid/view/IWindow;)V
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_3a} :catch_46

    #@3a
    .line 311
    :goto_3a
    iput-object v4, p0, Landroid/view/SurfaceView;->mWindow:Landroid/view/SurfaceView$MyWindow;

    #@3c
    .line 313
    :cond_3c
    iput-object v4, p0, Landroid/view/SurfaceView;->mSession:Landroid/view/IWindowSession;

    #@3e
    .line 314
    iget-object v1, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@40
    iput-object v4, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@42
    .line 316
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@45
    .line 317
    return-void

    #@46
    .line 308
    :catch_46
    move-exception v1

    #@47
    goto :goto_3a
.end method

.method protected onMeasure(II)V
    .registers 7
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 321
    iget v2, p0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@3
    if-ltz v2, :cond_19

    #@5
    iget v2, p0, Landroid/view/SurfaceView;->mRequestedWidth:I

    #@7
    invoke-static {v2, p1, v3}, Landroid/view/SurfaceView;->resolveSizeAndState(III)I

    #@a
    move-result v1

    #@b
    .line 324
    .local v1, width:I
    :goto_b
    iget v2, p0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@d
    if-ltz v2, :cond_1e

    #@f
    iget v2, p0, Landroid/view/SurfaceView;->mRequestedHeight:I

    #@11
    invoke-static {v2, p2, v3}, Landroid/view/SurfaceView;->resolveSizeAndState(III)I

    #@14
    move-result v0

    #@15
    .line 327
    .local v0, height:I
    :goto_15
    invoke-virtual {p0, v1, v0}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    #@18
    .line 328
    return-void

    #@19
    .line 321
    .end local v0           #height:I
    .end local v1           #width:I
    :cond_19
    invoke-static {v3, p1}, Landroid/view/SurfaceView;->getDefaultSize(II)I

    #@1c
    move-result v1

    #@1d
    goto :goto_b

    #@1e
    .line 324
    .restart local v1       #width:I
    :cond_1e
    invoke-static {v3, p2}, Landroid/view/SurfaceView;->getDefaultSize(II)I

    #@21
    move-result v0

    #@22
    goto :goto_15
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 5
    .parameter "visibility"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 260
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    #@5
    .line 261
    if-nez p1, :cond_18

    #@7
    move v0, v1

    #@8
    :goto_8
    iput-boolean v0, p0, Landroid/view/SurfaceView;->mWindowVisibility:Z

    #@a
    .line 262
    iget-boolean v0, p0, Landroid/view/SurfaceView;->mWindowVisibility:Z

    #@c
    if-eqz v0, :cond_1a

    #@e
    iget-boolean v0, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@10
    if-eqz v0, :cond_1a

    #@12
    :goto_12
    iput-boolean v1, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@14
    .line 263
    invoke-direct {p0, v2, v2}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@17
    .line 264
    return-void

    #@18
    :cond_18
    move v0, v2

    #@19
    .line 261
    goto :goto_8

    #@1a
    :cond_1a
    move v1, v2

    #@1b
    .line 262
    goto :goto_12
.end method

.method public setExtUsage(I)V
    .registers 4
    .parameter "extUsage"

    #@0
    .prologue
    .line 778
    iget-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@2
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@4
    .line 779
    const/4 v0, 0x1

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {p0, v0, v1}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@9
    .line 781
    return-void
.end method

.method public setExtendFlags(I)V
    .registers 3
    .parameter "extFlags"

    #@0
    .prologue
    .line 463
    iget-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@2
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@4
    .line 464
    return-void
.end method

.method protected setFrame(IIII)Z
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 333
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->setFrame(IIII)Z

    #@4
    move-result v0

    #@5
    .line 334
    .local v0, result:Z
    invoke-direct {p0, v1, v1}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@8
    .line 335
    return v0
.end method

.method public setLockScreenFlag()V
    .registers 2

    #@0
    .prologue
    .line 999
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/view/SurfaceView;->mIsLockScreen:Z

    #@3
    .line 1000
    return-void
.end method

.method public setSecure(Z)V
    .registers 4
    .parameter "isSecure"

    #@0
    .prologue
    .line 443
    if-eqz p1, :cond_b

    #@2
    .line 444
    iget-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@4
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@6
    or-int/lit16 v1, v1, 0x2000

    #@8
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@a
    .line 448
    :goto_a
    return-void

    #@b
    .line 446
    :cond_b
    iget-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@d
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@f
    and-int/lit16 v1, v1, -0x2001

    #@11
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@13
    goto :goto_a
.end method

.method public setVisibility(I)V
    .registers 6
    .parameter "visibility"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 268
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    #@5
    .line 269
    if-nez p1, :cond_20

    #@7
    move v1, v2

    #@8
    :goto_8
    iput-boolean v1, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@a
    .line 270
    iget-boolean v1, p0, Landroid/view/SurfaceView;->mWindowVisibility:Z

    #@c
    if-eqz v1, :cond_22

    #@e
    iget-boolean v1, p0, Landroid/view/SurfaceView;->mViewVisibility:Z

    #@10
    if-eqz v1, :cond_22

    #@12
    move v0, v2

    #@13
    .line 271
    .local v0, newRequestedVisible:Z
    :goto_13
    iget-boolean v1, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@15
    if-eq v0, v1, :cond_1a

    #@17
    .line 278
    invoke-virtual {p0}, Landroid/view/SurfaceView;->requestLayout()V

    #@1a
    .line 280
    :cond_1a
    iput-boolean v0, p0, Landroid/view/SurfaceView;->mRequestedVisible:Z

    #@1c
    .line 281
    invoke-direct {p0, v3, v3}, Landroid/view/SurfaceView;->updateWindow(ZZ)V

    #@1f
    .line 282
    return-void

    #@20
    .end local v0           #newRequestedVisible:Z
    :cond_20
    move v1, v3

    #@21
    .line 269
    goto :goto_8

    #@22
    :cond_22
    move v0, v3

    #@23
    .line 270
    goto :goto_13
.end method

.method public setWindowType(I)V
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 456
    iput p1, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@2
    .line 457
    return-void
.end method

.method public setZOrderMediaOverlay(Z)V
    .registers 3
    .parameter "isMediaOverlay"

    #@0
    .prologue
    .line 401
    if-eqz p1, :cond_7

    #@2
    const/16 v0, 0x3ec

    #@4
    :goto_4
    iput v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@6
    .line 404
    return-void

    #@7
    .line 401
    :cond_7
    const/16 v0, 0x3e9

    #@9
    goto :goto_4
.end method

.method public setZOrderOnTop(Z)V
    .registers 5
    .parameter "onTop"

    #@0
    .prologue
    .line 420
    if-eqz p1, :cond_10

    #@2
    .line 421
    const/16 v0, 0x3e8

    #@4
    iput v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@6
    .line 423
    iget-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@8
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@a
    const/high16 v2, 0x2

    #@c
    or-int/2addr v1, v2

    #@d
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@f
    .line 428
    :goto_f
    return-void

    #@10
    .line 425
    :cond_10
    const/16 v0, 0x3e9

    #@12
    iput v0, p0, Landroid/view/SurfaceView;->mWindowType:I

    #@14
    .line 426
    iget-object v0, p0, Landroid/view/SurfaceView;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@16
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@18
    const v2, -0x20001

    #@1b
    and-int/2addr v1, v2

    #@1c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1e
    goto :goto_f
.end method
