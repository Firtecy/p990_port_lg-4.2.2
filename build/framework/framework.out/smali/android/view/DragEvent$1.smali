.class final Landroid/view/DragEvent$1;
.super Ljava/lang/Object;
.source "DragEvent.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/DragEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/view/DragEvent;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 486
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/view/DragEvent;
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 488
    invoke-static {}, Landroid/view/DragEvent;->obtain()Landroid/view/DragEvent;

    #@3
    move-result-object v0

    #@4
    .line 489
    .local v0, event:Landroid/view/DragEvent;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v1

    #@8
    iput v1, v0, Landroid/view/DragEvent;->mAction:I

    #@a
    .line 490
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@d
    move-result v1

    #@e
    iput v1, v0, Landroid/view/DragEvent;->mX:F

    #@10
    .line 491
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@13
    move-result v1

    #@14
    iput v1, v0, Landroid/view/DragEvent;->mY:F

    #@16
    .line 492
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_40

    #@1c
    const/4 v1, 0x1

    #@1d
    :goto_1d
    iput-boolean v1, v0, Landroid/view/DragEvent;->mDragResult:Z

    #@1f
    .line 493
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_2f

    #@25
    .line 494
    sget-object v1, Landroid/content/ClipData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2a
    move-result-object v1

    #@2b
    check-cast v1, Landroid/content/ClipData;

    #@2d
    iput-object v1, v0, Landroid/view/DragEvent;->mClipData:Landroid/content/ClipData;

    #@2f
    .line 496
    :cond_2f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_3f

    #@35
    .line 497
    sget-object v1, Landroid/content/ClipDescription;->CREATOR:Landroid/os/Parcelable$Creator;

    #@37
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3a
    move-result-object v1

    #@3b
    check-cast v1, Landroid/content/ClipDescription;

    #@3d
    iput-object v1, v0, Landroid/view/DragEvent;->mClipDescription:Landroid/content/ClipDescription;

    #@3f
    .line 499
    :cond_3f
    return-object v0

    #@40
    .line 492
    :cond_40
    const/4 v1, 0x0

    #@41
    goto :goto_1d
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 486
    invoke-virtual {p0, p1}, Landroid/view/DragEvent$1;->createFromParcel(Landroid/os/Parcel;)Landroid/view/DragEvent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/view/DragEvent;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 503
    new-array v0, p1, [Landroid/view/DragEvent;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 486
    invoke-virtual {p0, p1}, Landroid/view/DragEvent$1;->newArray(I)[Landroid/view/DragEvent;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
