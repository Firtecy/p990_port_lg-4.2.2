.class public Landroid/view/KeyCharacterMap;
.super Ljava/lang/Object;
.source "KeyCharacterMap.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/KeyCharacterMap$FallbackAction;,
        Landroid/view/KeyCharacterMap$UnavailableException;,
        Landroid/view/KeyCharacterMap$KeyData;
    }
.end annotation


# static fields
.field private static final ACCENT_ACUTE:I = 0xb4

.field private static final ACCENT_BREVE:I = 0x2d8

.field private static final ACCENT_CARON:I = 0x2c7

.field private static final ACCENT_CEDILLA:I = 0xb8

.field private static final ACCENT_CIRCUMFLEX:I = 0x2c6

.field private static final ACCENT_CIRCUMFLEX_LEGACY:I = 0x5e

.field private static final ACCENT_COMMA_ABOVE:I = 0x1fbd

.field private static final ACCENT_COMMA_ABOVE_RIGHT:I = 0x2bc

.field private static final ACCENT_DOT_ABOVE:I = 0x2d9

.field private static final ACCENT_DOT_BELOW:I = 0x2e

.field private static final ACCENT_DOUBLE_ACUTE:I = 0x2dd

.field private static final ACCENT_GRAVE:I = 0x2cb

.field private static final ACCENT_GRAVE_LEGACY:I = 0x60

.field private static final ACCENT_HOOK_ABOVE:I = 0x2c0

.field private static final ACCENT_HORN:I = 0x27

.field private static final ACCENT_MACRON:I = 0xaf

.field private static final ACCENT_MACRON_BELOW:I = 0x2cd

.field private static final ACCENT_OGONEK:I = 0x2db

.field private static final ACCENT_REVERSED_COMMA_ABOVE:I = 0x2bd

.field private static final ACCENT_RING_ABOVE:I = 0x2da

.field private static final ACCENT_STROKE:I = 0x2d

.field private static final ACCENT_TILDE:I = 0x2dc

.field private static final ACCENT_TILDE_LEGACY:I = 0x7e

.field private static final ACCENT_TURNED_COMMA_ABOVE:I = 0x2bb

.field private static final ACCENT_UMLAUT:I = 0xa8

.field private static final ACCENT_VERTICAL_LINE_ABOVE:I = 0x2c8

.field private static final ACCENT_VERTICAL_LINE_BELOW:I = 0x2cc

.field public static final ALPHA:I = 0x3

.field public static final BUILT_IN_KEYBOARD:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COMBINING_ACCENT:I = -0x80000000

.field public static final COMBINING_ACCENT_MASK:I = 0x7fffffff

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/KeyCharacterMap;",
            ">;"
        }
    .end annotation
.end field

.field public static final FULL:I = 0x4

.field public static final HEX_INPUT:C = '\uef00'

.field public static final MODIFIER_BEHAVIOR_CHORDED:I = 0x0

.field public static final MODIFIER_BEHAVIOR_CHORDED_OR_TOGGLED:I = 0x1

.field public static final NUMERIC:I = 0x1

.field public static final PICKER_DIALOG_INPUT:C = '\uef01'

.field public static final PREDICTIVE:I = 0x2

.field public static final SPECIAL_FUNCTION:I = 0x5

.field public static final VIRTUAL_KEYBOARD:I = -0x1

.field private static final sAccentToCombining:Landroid/util/SparseIntArray;

.field private static final sCombiningToAccent:Landroid/util/SparseIntArray;

.field private static final sDeadKeyBuilder:Ljava/lang/StringBuilder;

.field private static final sDeadKeyCache:Landroid/util/SparseIntArray;


# instance fields
.field private mPtr:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/16 v6, 0x302

    #@2
    const/16 v5, 0x300

    #@4
    const/16 v4, 0x2cb

    #@6
    const/16 v2, 0xb4

    #@8
    const/16 v3, 0x2d

    #@a
    .line 186
    new-instance v0, Landroid/util/SparseIntArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@f
    sput-object v0, Landroid/view/KeyCharacterMap;->sCombiningToAccent:Landroid/util/SparseIntArray;

    #@11
    .line 187
    new-instance v0, Landroid/util/SparseIntArray;

    #@13
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@16
    sput-object v0, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@18
    .line 189
    invoke-static {v5, v4}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@1b
    .line 190
    const/16 v0, 0x301

    #@1d
    invoke-static {v0, v2}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@20
    .line 191
    const/16 v0, 0x2c6

    #@22
    invoke-static {v6, v0}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@25
    .line 192
    const/16 v0, 0x303

    #@27
    const/16 v1, 0x2dc

    #@29
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@2c
    .line 193
    const/16 v0, 0x304

    #@2e
    const/16 v1, 0xaf

    #@30
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@33
    .line 194
    const/16 v0, 0x306

    #@35
    const/16 v1, 0x2d8

    #@37
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@3a
    .line 195
    const/16 v0, 0x307

    #@3c
    const/16 v1, 0x2d9

    #@3e
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@41
    .line 196
    const/16 v0, 0x308

    #@43
    const/16 v1, 0xa8

    #@45
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@48
    .line 197
    const/16 v0, 0x309

    #@4a
    const/16 v1, 0x2c0

    #@4c
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@4f
    .line 198
    const/16 v0, 0x30a

    #@51
    const/16 v1, 0x2da

    #@53
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@56
    .line 199
    const/16 v0, 0x30b

    #@58
    const/16 v1, 0x2dd

    #@5a
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@5d
    .line 200
    const/16 v0, 0x30c

    #@5f
    const/16 v1, 0x2c7

    #@61
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@64
    .line 201
    const/16 v0, 0x30d

    #@66
    const/16 v1, 0x2c8

    #@68
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@6b
    .line 206
    const/16 v0, 0x312

    #@6d
    const/16 v1, 0x2bb

    #@6f
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@72
    .line 207
    const/16 v0, 0x313

    #@74
    const/16 v1, 0x1fbd

    #@76
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@79
    .line 208
    const/16 v0, 0x314

    #@7b
    const/16 v1, 0x2bd

    #@7d
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@80
    .line 209
    const/16 v0, 0x315

    #@82
    const/16 v1, 0x2bc

    #@84
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@87
    .line 210
    const/16 v0, 0x31b

    #@89
    const/16 v1, 0x27

    #@8b
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@8e
    .line 211
    const/16 v0, 0x323

    #@90
    const/16 v1, 0x2e

    #@92
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@95
    .line 213
    const/16 v0, 0x327

    #@97
    const/16 v1, 0xb8

    #@99
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@9c
    .line 214
    const/16 v0, 0x328

    #@9e
    const/16 v1, 0x2db

    #@a0
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@a3
    .line 215
    const/16 v0, 0x329

    #@a5
    const/16 v1, 0x2cc

    #@a7
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@aa
    .line 216
    const/16 v0, 0x331

    #@ac
    const/16 v1, 0x2cd

    #@ae
    invoke-static {v0, v1}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@b1
    .line 217
    const/16 v0, 0x335

    #@b3
    invoke-static {v0, v3}, Landroid/view/KeyCharacterMap;->addCombining(II)V

    #@b6
    .line 223
    sget-object v0, Landroid/view/KeyCharacterMap;->sCombiningToAccent:Landroid/util/SparseIntArray;

    #@b8
    const/16 v1, 0x340

    #@ba
    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->append(II)V

    #@bd
    .line 224
    sget-object v0, Landroid/view/KeyCharacterMap;->sCombiningToAccent:Landroid/util/SparseIntArray;

    #@bf
    const/16 v1, 0x341

    #@c1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    #@c4
    .line 225
    sget-object v0, Landroid/view/KeyCharacterMap;->sCombiningToAccent:Landroid/util/SparseIntArray;

    #@c6
    const/16 v1, 0x343

    #@c8
    const/16 v2, 0x1fbd

    #@ca
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    #@cd
    .line 228
    sget-object v0, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@cf
    const/16 v1, 0x60

    #@d1
    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->append(II)V

    #@d4
    .line 229
    sget-object v0, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@d6
    const/16 v1, 0x5e

    #@d8
    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->append(II)V

    #@db
    .line 230
    sget-object v0, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@dd
    const/16 v1, 0x7e

    #@df
    const/16 v2, 0x303

    #@e1
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    #@e4
    .line 243
    new-instance v0, Landroid/util/SparseIntArray;

    #@e6
    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    #@e9
    sput-object v0, Landroid/view/KeyCharacterMap;->sDeadKeyCache:Landroid/util/SparseIntArray;

    #@eb
    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    #@ed
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@f0
    sput-object v0, Landroid/view/KeyCharacterMap;->sDeadKeyBuilder:Ljava/lang/StringBuilder;

    #@f2
    .line 248
    const/16 v0, 0x44

    #@f4
    const/16 v1, 0x110

    #@f6
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@f9
    .line 249
    const/16 v0, 0x47

    #@fb
    const/16 v1, 0x1e4

    #@fd
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@100
    .line 250
    const/16 v0, 0x48

    #@102
    const/16 v1, 0x126

    #@104
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@107
    .line 251
    const/16 v0, 0x49

    #@109
    const/16 v1, 0x197

    #@10b
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@10e
    .line 252
    const/16 v0, 0x4c

    #@110
    const/16 v1, 0x141

    #@112
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@115
    .line 253
    const/16 v0, 0x4f

    #@117
    const/16 v1, 0xd8

    #@119
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@11c
    .line 254
    const/16 v0, 0x54

    #@11e
    const/16 v1, 0x166

    #@120
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@123
    .line 255
    const/16 v0, 0x64

    #@125
    const/16 v1, 0x111

    #@127
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@12a
    .line 256
    const/16 v0, 0x67

    #@12c
    const/16 v1, 0x1e5

    #@12e
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@131
    .line 257
    const/16 v0, 0x68

    #@133
    const/16 v1, 0x127

    #@135
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@138
    .line 258
    const/16 v0, 0x69

    #@13a
    const/16 v1, 0x268

    #@13c
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@13f
    .line 259
    const/16 v0, 0x6c

    #@141
    const/16 v1, 0x142

    #@143
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@146
    .line 260
    const/16 v0, 0x6f

    #@148
    const/16 v1, 0xf8

    #@14a
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@14d
    .line 261
    const/16 v0, 0x74

    #@14f
    const/16 v1, 0x167

    #@151
    invoke-static {v3, v0, v1}, Landroid/view/KeyCharacterMap;->addDeadKey(III)V

    #@154
    .line 273
    new-instance v0, Landroid/view/KeyCharacterMap$1;

    #@156
    invoke-direct {v0}, Landroid/view/KeyCharacterMap$1;-><init>()V

    #@159
    sput-object v0, Landroid/view/KeyCharacterMap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15b
    return-void
.end method

.method private constructor <init>(I)V
    .registers 2
    .parameter "ptr"

    #@0
    .prologue
    .line 309
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 310
    iput p1, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@5
    .line 311
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    #@0
    .prologue
    .line 298
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 299
    if-nez p1, :cond_e

    #@5
    .line 300
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "parcel must not be null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 302
    :cond_e
    invoke-static {p1}, Landroid/view/KeyCharacterMap;->nativeReadFromParcel(Landroid/os/Parcel;)I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@14
    .line 303
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@16
    if-nez v0, :cond_20

    #@18
    .line 304
    new-instance v0, Ljava/lang/RuntimeException;

    #@1a
    const-string v1, "Could not read KeyCharacterMap from parcel."

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 306
    :cond_20
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/view/KeyCharacterMap$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/view/KeyCharacterMap;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private static addCombining(II)V
    .registers 3
    .parameter "combining"
    .parameter "accent"

    #@0
    .prologue
    .line 234
    sget-object v0, Landroid/view/KeyCharacterMap;->sCombiningToAccent:Landroid/util/SparseIntArray;

    #@2
    invoke-virtual {v0, p0, p1}, Landroid/util/SparseIntArray;->append(II)V

    #@5
    .line 235
    sget-object v0, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@7
    invoke-virtual {v0, p1, p0}, Landroid/util/SparseIntArray;->append(II)V

    #@a
    .line 236
    return-void
.end method

.method private static addDeadKey(III)V
    .registers 7
    .parameter "accent"
    .parameter "c"
    .parameter "result"

    #@0
    .prologue
    .line 265
    sget-object v2, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@2
    invoke-virtual {v2, p0}, Landroid/util/SparseIntArray;->get(I)I

    #@5
    move-result v1

    #@6
    .line 266
    .local v1, combining:I
    if-nez v1, :cond_10

    #@8
    .line 267
    new-instance v2, Ljava/lang/IllegalStateException;

    #@a
    const-string v3, "Invalid dead key declaration."

    #@c
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2

    #@10
    .line 269
    :cond_10
    shl-int/lit8 v2, v1, 0x10

    #@12
    or-int v0, v2, p1

    #@14
    .line 270
    .local v0, combination:I
    sget-object v2, Landroid/view/KeyCharacterMap;->sDeadKeyCache:Landroid/util/SparseIntArray;

    #@16
    invoke-virtual {v2, v0, p2}, Landroid/util/SparseIntArray;->put(II)V

    #@19
    .line 271
    return-void
.end method

.method public static deviceHasKey(I)Z
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 685
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@4
    move-result-object v0

    #@5
    const/4 v1, 0x1

    #@6
    new-array v1, v1, [I

    #@8
    aput p0, v1, v2

    #@a
    invoke-virtual {v0, v1}, Landroid/hardware/input/InputManager;->deviceHasKeys([I)[Z

    #@d
    move-result-object v0

    #@e
    aget-boolean v0, v0, v2

    #@10
    return v0
.end method

.method public static deviceHasKeys([I)[Z
    .registers 2
    .parameter "keyCodes"

    #@0
    .prologue
    .line 699
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/hardware/input/InputManager;->deviceHasKeys([I)[Z

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getDeadChar(II)I
    .registers 11
    .parameter "accent"
    .parameter "c"

    #@0
    .prologue
    const/4 v8, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 484
    sget-object v5, Landroid/view/KeyCharacterMap;->sAccentToCombining:Landroid/util/SparseIntArray;

    #@4
    invoke-virtual {v5, p0}, Landroid/util/SparseIntArray;->get(I)I

    #@7
    move-result v2

    #@8
    .line 485
    .local v2, combining:I
    if-nez v2, :cond_c

    #@a
    move v1, v4

    #@b
    .line 502
    :goto_b
    return v1

    #@c
    .line 489
    :cond_c
    shl-int/lit8 v5, v2, 0x10

    #@e
    or-int v0, v5, p1

    #@10
    .line 491
    .local v0, combination:I
    sget-object v5, Landroid/view/KeyCharacterMap;->sDeadKeyCache:Landroid/util/SparseIntArray;

    #@12
    monitor-enter v5

    #@13
    .line 492
    :try_start_13
    sget-object v6, Landroid/view/KeyCharacterMap;->sDeadKeyCache:Landroid/util/SparseIntArray;

    #@15
    const/4 v7, -0x1

    #@16
    invoke-virtual {v6, v0, v7}, Landroid/util/SparseIntArray;->get(II)I

    #@19
    move-result v1

    #@1a
    .line 493
    .local v1, combined:I
    if-ne v1, v8, :cond_47

    #@1c
    .line 494
    sget-object v6, Landroid/view/KeyCharacterMap;->sDeadKeyBuilder:Ljava/lang/StringBuilder;

    #@1e
    const/4 v7, 0x0

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@22
    .line 495
    sget-object v6, Landroid/view/KeyCharacterMap;->sDeadKeyBuilder:Ljava/lang/StringBuilder;

    #@24
    int-to-char v7, p1

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@28
    .line 496
    sget-object v6, Landroid/view/KeyCharacterMap;->sDeadKeyBuilder:Ljava/lang/StringBuilder;

    #@2a
    int-to-char v7, v2

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    .line 497
    sget-object v6, Landroid/view/KeyCharacterMap;->sDeadKeyBuilder:Ljava/lang/StringBuilder;

    #@30
    sget-object v7, Ljava/text/Normalizer$Form;->NFC:Ljava/text/Normalizer$Form;

    #@32
    invoke-static {v6, v7}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    .line 498
    .local v3, result:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@39
    move-result v6

    #@3a
    const/4 v7, 0x1

    #@3b
    if-ne v6, v7, :cond_4c

    #@3d
    const/4 v4, 0x0

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    #@41
    move-result v1

    #@42
    .line 499
    :goto_42
    sget-object v4, Landroid/view/KeyCharacterMap;->sDeadKeyCache:Landroid/util/SparseIntArray;

    #@44
    invoke-virtual {v4, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    #@47
    .line 501
    .end local v3           #result:Ljava/lang/String;
    :cond_47
    monitor-exit v5

    #@48
    goto :goto_b

    #@49
    .end local v1           #combined:I
    :catchall_49
    move-exception v4

    #@4a
    monitor-exit v5
    :try_end_4b
    .catchall {:try_start_13 .. :try_end_4b} :catchall_49

    #@4b
    throw v4

    #@4c
    .restart local v1       #combined:I
    .restart local v3       #result:Ljava/lang/String;
    :cond_4c
    move v1, v4

    #@4d
    .line 498
    goto :goto_42
.end method

.method public static load(I)Landroid/view/KeyCharacterMap;
    .registers 6
    .parameter "deviceId"

    #@0
    .prologue
    .line 331
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@3
    move-result-object v0

    #@4
    .line 332
    .local v0, im:Landroid/hardware/input/InputManager;
    invoke-virtual {v0, p0}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    #@7
    move-result-object v1

    #@8
    .line 333
    .local v1, inputDevice:Landroid/view/InputDevice;
    if-nez v1, :cond_2a

    #@a
    .line 334
    const/4 v2, -0x1

    #@b
    invoke-virtual {v0, v2}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    #@e
    move-result-object v1

    #@f
    .line 335
    if-nez v1, :cond_2a

    #@11
    .line 336
    new-instance v2, Landroid/view/KeyCharacterMap$UnavailableException;

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Could not load key character map for device "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-direct {v2, v3}, Landroid/view/KeyCharacterMap$UnavailableException;-><init>(Ljava/lang/String;)V

    #@29
    throw v2

    #@2a
    .line 340
    :cond_2a
    invoke-virtual {v1}, Landroid/view/InputDevice;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@2d
    move-result-object v2

    #@2e
    return-object v2
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeGetCharacter(III)C
.end method

.method private static native nativeGetDisplayLabel(II)C
.end method

.method private static native nativeGetEvents(I[C)[Landroid/view/KeyEvent;
.end method

.method private static native nativeGetFallbackAction(IIILandroid/view/KeyCharacterMap$FallbackAction;)Z
.end method

.method private static native nativeGetKeyboardType(I)I
.end method

.method private static native nativeGetMatch(II[CI)C
.end method

.method private static native nativeGetNumber(II)C
.end method

.method private static native nativeReadFromParcel(Landroid/os/Parcel;)I
.end method

.method private static native nativeWriteToParcel(ILandroid/os/Parcel;)V
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 712
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 316
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@6
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->nativeDispose(I)V

    #@9
    .line 317
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@c
    .line 319
    :cond_c
    return-void
.end method

.method public get(II)I
    .registers 6
    .parameter "keyCode"
    .parameter "metaState"

    #@0
    .prologue
    .line 365
    invoke-static {p2}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@3
    move-result p2

    #@4
    .line 366
    iget v2, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@6
    invoke-static {v2, p1, p2}, Landroid/view/KeyCharacterMap;->nativeGetCharacter(III)C

    #@9
    move-result v0

    #@a
    .line 368
    .local v0, ch:C
    sget-object v2, Landroid/view/KeyCharacterMap;->sCombiningToAccent:Landroid/util/SparseIntArray;

    #@c
    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->get(I)I

    #@f
    move-result v1

    #@10
    .line 369
    .local v1, map:I
    if-eqz v1, :cond_16

    #@12
    .line 370
    const/high16 v2, -0x8000

    #@14
    or-int v0, v1, v2

    #@16
    .line 372
    .end local v0           #ch:C
    :cond_16
    return v0
.end method

.method public getDisplayLabel(I)C
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 472
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/KeyCharacterMap;->nativeGetDisplayLabel(II)C

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getEvents([C)[Landroid/view/KeyEvent;
    .registers 4
    .parameter "chars"

    #@0
    .prologue
    .line 592
    if-nez p1, :cond_a

    #@2
    .line 593
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "chars must not be null."

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 595
    :cond_a
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@c
    invoke-static {v0, p1}, Landroid/view/KeyCharacterMap;->nativeGetEvents(I[C)[Landroid/view/KeyEvent;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public getFallbackAction(II)Landroid/view/KeyCharacterMap$FallbackAction;
    .registers 5
    .parameter "keyCode"
    .parameter "metaState"

    #@0
    .prologue
    .line 394
    invoke-static {}, Landroid/view/KeyCharacterMap$FallbackAction;->obtain()Landroid/view/KeyCharacterMap$FallbackAction;

    #@3
    move-result-object v0

    #@4
    .line 395
    .local v0, action:Landroid/view/KeyCharacterMap$FallbackAction;
    invoke-static {p2}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@7
    move-result p2

    #@8
    .line 396
    iget v1, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@a
    invoke-static {v1, p1, p2, v0}, Landroid/view/KeyCharacterMap;->nativeGetFallbackAction(IIILandroid/view/KeyCharacterMap$FallbackAction;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_19

    #@10
    .line 397
    iget v1, v0, Landroid/view/KeyCharacterMap$FallbackAction;->metaState:I

    #@12
    invoke-static {v1}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@15
    move-result v1

    #@16
    iput v1, v0, Landroid/view/KeyCharacterMap$FallbackAction;->metaState:I

    #@18
    .line 401
    .end local v0           #action:Landroid/view/KeyCharacterMap$FallbackAction;
    :goto_18
    return-object v0

    #@19
    .line 400
    .restart local v0       #action:Landroid/view/KeyCharacterMap$FallbackAction;
    :cond_19
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap$FallbackAction;->recycle()V

    #@1c
    .line 401
    const/4 v0, 0x0

    #@1d
    goto :goto_18
.end method

.method public getKeyData(ILandroid/view/KeyCharacterMap$KeyData;)Z
    .registers 10
    .parameter "keyCode"
    .parameter "results"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 550
    iget-object v3, p2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@6
    array-length v3, v3

    #@7
    const/4 v4, 0x4

    #@8
    if-ge v3, v4, :cond_13

    #@a
    .line 551
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@c
    const-string/jumbo v2, "results.meta.length must be >= 4"

    #@f
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@12
    throw v1

    #@13
    .line 555
    :cond_13
    iget v3, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@15
    invoke-static {v3, p1}, Landroid/view/KeyCharacterMap;->nativeGetDisplayLabel(II)C

    #@18
    move-result v0

    #@19
    .line 556
    .local v0, displayLabel:C
    if-nez v0, :cond_1c

    #@1b
    .line 567
    :goto_1b
    return v1

    #@1c
    .line 560
    :cond_1c
    iput-char v0, p2, Landroid/view/KeyCharacterMap$KeyData;->displayLabel:C

    #@1e
    .line 561
    iget v3, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@20
    invoke-static {v3, p1}, Landroid/view/KeyCharacterMap;->nativeGetNumber(II)C

    #@23
    move-result v3

    #@24
    iput-char v3, p2, Landroid/view/KeyCharacterMap$KeyData;->number:C

    #@26
    .line 562
    iget-object v3, p2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@28
    iget v4, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@2a
    invoke-static {v4, p1, v1}, Landroid/view/KeyCharacterMap;->nativeGetCharacter(III)C

    #@2d
    move-result v4

    #@2e
    aput-char v4, v3, v1

    #@30
    .line 563
    iget-object v1, p2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@32
    iget v3, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@34
    invoke-static {v3, p1, v2}, Landroid/view/KeyCharacterMap;->nativeGetCharacter(III)C

    #@37
    move-result v3

    #@38
    aput-char v3, v1, v2

    #@3a
    .line 564
    iget-object v1, p2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@3c
    iget v3, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@3e
    invoke-static {v3, p1, v5}, Landroid/view/KeyCharacterMap;->nativeGetCharacter(III)C

    #@41
    move-result v3

    #@42
    aput-char v3, v1, v5

    #@44
    .line 565
    iget-object v1, p2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@46
    iget v3, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@48
    invoke-static {v3, p1, v6}, Landroid/view/KeyCharacterMap;->nativeGetCharacter(III)C

    #@4b
    move-result v3

    #@4c
    aput-char v3, v1, v6

    #@4e
    move v1, v2

    #@4f
    .line 567
    goto :goto_1b
.end method

.method public getKeyboardType()I
    .registers 2

    #@0
    .prologue
    .line 632
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@2
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->nativeGetKeyboardType(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMatch(I[C)C
    .registers 4
    .parameter "keyCode"
    .parameter "chars"

    #@0
    .prologue
    .line 442
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/view/KeyCharacterMap;->getMatch(I[CI)C

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public getMatch(I[CI)C
    .registers 6
    .parameter "keyCode"
    .parameter "chars"
    .parameter "metaState"

    #@0
    .prologue
    .line 456
    if-nez p2, :cond_a

    #@2
    .line 457
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "chars must not be null."

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 460
    :cond_a
    invoke-static {p3}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@d
    move-result p3

    #@e
    .line 461
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@10
    invoke-static {v0, p1, p2, p3}, Landroid/view/KeyCharacterMap;->nativeGetMatch(II[CI)C

    #@13
    move-result v0

    #@14
    return v0
.end method

.method public getModifierBehavior()I
    .registers 2

    #@0
    .prologue
    .line 668
    invoke-virtual {p0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_c

    #@7
    .line 673
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    .line 671
    :pswitch_9
    const/4 v0, 0x0

    #@a
    goto :goto_8

    #@b
    .line 668
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x4
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method public getNumber(I)C
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    .line 426
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@2
    invoke-static {v0, p1}, Landroid/view/KeyCharacterMap;->nativeGetNumber(II)C

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPrintingKey(I)Z
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 605
    iget v1, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@2
    invoke-static {v1, p1}, Landroid/view/KeyCharacterMap;->nativeGetDisplayLabel(II)C

    #@5
    move-result v1

    #@6
    invoke-static {v1}, Ljava/lang/Character;->getType(C)I

    #@9
    move-result v0

    #@a
    .line 607
    .local v0, type:I
    packed-switch v0, :pswitch_data_12

    #@d
    .line 616
    const/4 v1, 0x1

    #@e
    :goto_e
    return v1

    #@f
    .line 614
    :pswitch_f
    const/4 v1, 0x0

    #@10
    goto :goto_e

    #@11
    .line 607
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0xc
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 704
    if-nez p1, :cond_b

    #@2
    .line 705
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "parcel must not be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 707
    :cond_b
    iget v0, p0, Landroid/view/KeyCharacterMap;->mPtr:I

    #@d
    invoke-static {v0, p1}, Landroid/view/KeyCharacterMap;->nativeWriteToParcel(ILandroid/os/Parcel;)V

    #@10
    .line 708
    return-void
.end method
