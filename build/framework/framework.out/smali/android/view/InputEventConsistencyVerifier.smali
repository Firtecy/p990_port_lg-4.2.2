.class public final Landroid/view/InputEventConsistencyVerifier;
.super Ljava/lang/Object;
.source "InputEventConsistencyVerifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/InputEventConsistencyVerifier$KeyState;
    }
.end annotation


# static fields
.field private static final EVENT_TYPE_GENERIC_MOTION:Ljava/lang/String; = "GenericMotionEvent"

.field private static final EVENT_TYPE_KEY:Ljava/lang/String; = "KeyEvent"

.field private static final EVENT_TYPE_TOUCH:Ljava/lang/String; = "TouchEvent"

.field private static final EVENT_TYPE_TRACKBALL:Ljava/lang/String; = "TrackballEvent"

.field public static final FLAG_RAW_DEVICE_INPUT:I = 0x1

#the value of this static final field might be set in the static constructor
.field private static final IS_ENG_BUILD:Z = false

.field private static final RECENT_EVENTS_TO_LOG:I = 0x5


# instance fields
.field private final mCaller:Ljava/lang/Object;

.field private mCurrentEvent:Landroid/view/InputEvent;

.field private mCurrentEventType:Ljava/lang/String;

.field private final mFlags:I

.field private mHoverEntered:Z

.field private mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

.field private mLastEventSeq:I

.field private mLastEventType:Ljava/lang/String;

.field private mLastNestingLevel:I

.field private final mLogTag:Ljava/lang/String;

.field private mMostRecentEventIndex:I

.field private mRecentEvents:[Landroid/view/InputEvent;

.field private mRecentEventsUnhandled:[Z

.field private mTouchEventStreamDeviceId:I

.field private mTouchEventStreamIsTainted:Z

.field private mTouchEventStreamPointers:I

.field private mTouchEventStreamSource:I

.field private mTouchEventStreamUnhandled:Z

.field private mTrackballDown:Z

.field private mTrackballUnhandled:Z

.field private mViolationMessage:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 33
    const-string v0, "eng"

    #@2
    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    sput-boolean v0, Landroid/view/InputEventConsistencyVerifier;->IS_ENG_BUILD:Z

    #@a
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;I)V
    .registers 4
    .parameter "caller"
    .parameter "flags"

    #@0
    .prologue
    .line 116
    const-class v0, Landroid/view/InputEventConsistencyVerifier;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-direct {p0, p1, p2, v0}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    #@9
    .line 117
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;ILjava/lang/String;)V
    .registers 5
    .parameter "caller"
    .parameter "flags"
    .parameter "logTag"

    #@0
    .prologue
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 87
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamDeviceId:I

    #@6
    .line 126
    iput-object p1, p0, Landroid/view/InputEventConsistencyVerifier;->mCaller:Ljava/lang/Object;

    #@8
    .line 127
    iput p2, p0, Landroid/view/InputEventConsistencyVerifier;->mFlags:I

    #@a
    .line 128
    if-eqz p3, :cond_f

    #@c
    .end local p3
    :goto_c
    iput-object p3, p0, Landroid/view/InputEventConsistencyVerifier;->mLogTag:Ljava/lang/String;

    #@e
    .line 129
    return-void

    #@f
    .line 128
    .restart local p3
    :cond_f
    const-string p3, "InputEventConsistencyVerifier"

    #@11
    goto :goto_c
.end method

.method private addKeyState(III)V
    .registers 6
    .parameter "deviceId"
    .parameter "source"
    .parameter "keyCode"

    #@0
    .prologue
    .line 691
    invoke-static {p1, p2, p3}, Landroid/view/InputEventConsistencyVerifier$KeyState;->obtain(III)Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@3
    move-result-object v0

    #@4
    .line 692
    .local v0, state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    iget-object v1, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@6
    iput-object v1, v0, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@8
    .line 693
    iput-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@a
    .line 694
    return-void
.end method

.method private static appendEvent(Ljava/lang/StringBuilder;ILandroid/view/InputEvent;Z)V
    .registers 7
    .parameter "message"
    .parameter "index"
    .parameter "event"
    .parameter "unhandled"

    #@0
    .prologue
    .line 648
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3
    move-result-object v0

    #@4
    const-string v1, ": sent at "

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p2}, Landroid/view/InputEvent;->getEventTimeNano()J

    #@d
    move-result-wide v1

    #@e
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11
    .line 649
    const-string v0, ", "

    #@13
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 650
    if-eqz p3, :cond_1d

    #@18
    .line 651
    const-string v0, "(unhandled) "

    #@1a
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 653
    :cond_1d
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    .line 654
    return-void
.end method

.method private ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 567
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    #@3
    move-result v0

    #@4
    .line 568
    .local v0, historySize:I
    if-eqz v0, :cond_2e

    #@6
    .line 569
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "History size is "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " but it should always be 0 for "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@1e
    move-result v2

    #@1f
    invoke-static {v2}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-direct {p0, v1}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@2e
    .line 572
    :cond_2e
    return-void
.end method

.method private ensureMetaStateIsNormalized(I)V
    .registers 7
    .parameter "metaState"

    #@0
    .prologue
    .line 551
    invoke-static {p1}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    #@3
    move-result v0

    #@4
    .line 552
    .local v0, normalizedMetaState:I
    if-eq v0, p1, :cond_20

    #@6
    .line 553
    const-string v1, "Metastate not normalized.  Was 0x%08x but expected 0x%08x."

    #@8
    const/4 v2, 0x2

    #@9
    new-array v2, v2, [Ljava/lang/Object;

    #@b
    const/4 v3, 0x0

    #@c
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v4

    #@10
    aput-object v4, v2, v3

    #@12
    const/4 v3, 0x1

    #@13
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v4

    #@17
    aput-object v4, v2, v3

    #@19
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {p0, v1}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@20
    .line 556
    :cond_20
    return-void
.end method

.method private ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 559
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@3
    move-result v0

    #@4
    .line 560
    .local v0, pointerCount:I
    const/4 v1, 0x1

    #@5
    if-eq v0, v1, :cond_2f

    #@7
    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Pointer count is "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, " but it should always be 1 for "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@1f
    move-result v2

    #@20
    invoke-static {v2}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-direct {p0, v1}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@2f
    .line 564
    :cond_2f
    return-void
.end method

.method private findKeyState(IIIZ)Landroid/view/InputEventConsistencyVerifier$KeyState;
    .registers 9
    .parameter "deviceId"
    .parameter "source"
    .parameter "keyCode"
    .parameter "remove"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 669
    const/4 v0, 0x0

    #@2
    .line 670
    .local v0, last:Landroid/view/InputEventConsistencyVerifier$KeyState;
    iget-object v1, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@4
    .line 671
    .local v1, state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :goto_4
    if-eqz v1, :cond_26

    #@6
    .line 672
    iget v3, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->deviceId:I

    #@8
    if-ne v3, p1, :cond_22

    #@a
    iget v3, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->source:I

    #@c
    if-ne v3, p2, :cond_22

    #@e
    iget v3, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->keyCode:I

    #@10
    if-ne v3, p3, :cond_22

    #@12
    .line 674
    if-eqz p4, :cond_1c

    #@14
    .line 675
    if-eqz v0, :cond_1d

    #@16
    .line 676
    iget-object v3, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@18
    iput-object v3, v0, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@1a
    .line 680
    :goto_1a
    iput-object v2, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@1c
    .line 687
    .end local v1           #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :cond_1c
    :goto_1c
    return-object v1

    #@1d
    .line 678
    .restart local v1       #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :cond_1d
    iget-object v3, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@1f
    iput-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@21
    goto :goto_1a

    #@22
    .line 684
    :cond_22
    move-object v0, v1

    #@23
    .line 685
    iget-object v1, v1, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@25
    goto :goto_4

    #@26
    :cond_26
    move-object v1, v2

    #@27
    .line 687
    goto :goto_1c
.end method

.method private finishEvent()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x5

    #@2
    const/4 v6, 0x0

    #@3
    .line 598
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@5
    if-eqz v3, :cond_63

    #@7
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_63

    #@f
    .line 599
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEvent:Landroid/view/InputEvent;

    #@11
    invoke-virtual {v3}, Landroid/view/InputEvent;->isTainted()Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_5e

    #@17
    .line 601
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@19
    const-string v4, "\n  in "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    iget-object v4, p0, Landroid/view/InputEventConsistencyVerifier;->mCaller:Ljava/lang/Object;

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    .line 602
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@26
    const-string v4, "\n  "

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 603
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@2d
    iget-object v4, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEvent:Landroid/view/InputEvent;

    #@2f
    invoke-static {v3, v6, v4, v6}, Landroid/view/InputEventConsistencyVerifier;->appendEvent(Ljava/lang/StringBuilder;ILandroid/view/InputEvent;Z)V

    #@32
    .line 605
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@34
    if-eqz v3, :cond_4d

    #@36
    .line 606
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@38
    const-string v4, "\n  -- recent events --"

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 607
    const/4 v1, 0x0

    #@3e
    .local v1, i:I
    :goto_3e
    if-ge v1, v7, :cond_4d

    #@40
    .line 608
    iget v3, p0, Landroid/view/InputEventConsistencyVerifier;->mMostRecentEventIndex:I

    #@42
    add-int/lit8 v3, v3, 0x5

    #@44
    sub-int/2addr v3, v1

    #@45
    rem-int/lit8 v2, v3, 0x5

    #@47
    .line 610
    .local v2, index:I
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@49
    aget-object v0, v3, v2

    #@4b
    .line 611
    .local v0, event:Landroid/view/InputEvent;
    if-nez v0, :cond_97

    #@4d
    .line 619
    .end local v0           #event:Landroid/view/InputEvent;
    .end local v1           #i:I
    .end local v2           #index:I
    :cond_4d
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mLogTag:Ljava/lang/String;

    #@4f
    iget-object v4, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 623
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEvent:Landroid/view/InputEvent;

    #@5a
    const/4 v4, 0x1

    #@5b
    invoke-virtual {v3, v4}, Landroid/view/InputEvent;->setTainted(Z)V

    #@5e
    .line 625
    :cond_5e
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@60
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    #@63
    .line 629
    :cond_63
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@65
    if-nez v3, :cond_6f

    #@67
    .line 630
    new-array v3, v7, [Landroid/view/InputEvent;

    #@69
    iput-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@6b
    .line 631
    new-array v3, v7, [Z

    #@6d
    iput-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEventsUnhandled:[Z

    #@6f
    .line 633
    :cond_6f
    iget v3, p0, Landroid/view/InputEventConsistencyVerifier;->mMostRecentEventIndex:I

    #@71
    add-int/lit8 v3, v3, 0x1

    #@73
    rem-int/lit8 v2, v3, 0x5

    #@75
    .line 634
    .restart local v2       #index:I
    iput v2, p0, Landroid/view/InputEventConsistencyVerifier;->mMostRecentEventIndex:I

    #@77
    .line 635
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@79
    aget-object v3, v3, v2

    #@7b
    if-eqz v3, :cond_84

    #@7d
    .line 636
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@7f
    aget-object v3, v3, v2

    #@81
    invoke-virtual {v3}, Landroid/view/InputEvent;->recycle()V

    #@84
    .line 638
    :cond_84
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEvents:[Landroid/view/InputEvent;

    #@86
    iget-object v4, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEvent:Landroid/view/InputEvent;

    #@88
    invoke-virtual {v4}, Landroid/view/InputEvent;->copy()Landroid/view/InputEvent;

    #@8b
    move-result-object v4

    #@8c
    aput-object v4, v3, v2

    #@8e
    .line 639
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEventsUnhandled:[Z

    #@90
    aput-boolean v6, v3, v2

    #@92
    .line 642
    iput-object v8, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEvent:Landroid/view/InputEvent;

    #@94
    .line 643
    iput-object v8, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEventType:Ljava/lang/String;

    #@96
    .line 644
    return-void

    #@97
    .line 614
    .restart local v0       #event:Landroid/view/InputEvent;
    .restart local v1       #i:I
    :cond_97
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@99
    const-string v4, "\n  "

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    .line 615
    iget-object v3, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@a0
    add-int/lit8 v4, v1, 0x1

    #@a2
    iget-object v5, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEventsUnhandled:[Z

    #@a4
    aget-boolean v5, v5, v2

    #@a6
    invoke-static {v3, v4, v0, v5}, Landroid/view/InputEventConsistencyVerifier;->appendEvent(Ljava/lang/StringBuilder;ILandroid/view/InputEvent;Z)V

    #@a9
    .line 607
    add-int/lit8 v1, v1, 0x1

    #@ab
    goto :goto_3e
.end method

.method public static isInstrumentationEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 136
    sget-boolean v0, Landroid/view/InputEventConsistencyVerifier;->IS_ENG_BUILD:Z

    #@2
    return v0
.end method

.method private problem(Ljava/lang/String;)V
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 657
    iget-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 658
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@b
    .line 660
    :cond_b
    iget-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_26

    #@13
    .line 661
    iget-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@15
    iget-object v1, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEventType:Ljava/lang/String;

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    const-string v1, ": "

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 665
    :goto_20
    iget-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 666
    return-void

    #@26
    .line 663
    :cond_26
    iget-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mViolationMessage:Ljava/lang/StringBuilder;

    #@28
    const-string v1, "\n  "

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    goto :goto_20
.end method

.method private startEvent(Landroid/view/InputEvent;ILjava/lang/String;)Z
    .registers 7
    .parameter "event"
    .parameter "nestingLevel"
    .parameter "eventType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 576
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSequenceNumber()I

    #@4
    move-result v0

    #@5
    .line 577
    .local v0, seq:I
    iget v2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventSeq:I

    #@7
    if-ne v0, v2, :cond_12

    #@9
    iget v2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastNestingLevel:I

    #@b
    if-ge p2, v2, :cond_12

    #@d
    iget-object v2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventType:Ljava/lang/String;

    #@f
    if-ne p3, v2, :cond_12

    #@11
    .line 594
    :goto_11
    return v1

    #@12
    .line 582
    :cond_12
    if-lez p2, :cond_20

    #@14
    .line 583
    iput v0, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventSeq:I

    #@16
    .line 584
    iput-object p3, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventType:Ljava/lang/String;

    #@18
    .line 585
    iput p2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastNestingLevel:I

    #@1a
    .line 592
    :goto_1a
    iput-object p1, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEvent:Landroid/view/InputEvent;

    #@1c
    .line 593
    iput-object p3, p0, Landroid/view/InputEventConsistencyVerifier;->mCurrentEventType:Ljava/lang/String;

    #@1e
    .line 594
    const/4 v1, 0x1

    #@1f
    goto :goto_11

    #@20
    .line 587
    :cond_20
    const/4 v2, -0x1

    #@21
    iput v2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventSeq:I

    #@23
    .line 588
    const/4 v2, 0x0

    #@24
    iput-object v2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventType:Ljava/lang/String;

    #@26
    .line 589
    iput v1, p0, Landroid/view/InputEventConsistencyVerifier;->mLastNestingLevel:I

    #@28
    goto :goto_1a
.end method


# virtual methods
.method public onGenericMotionEvent(Landroid/view/MotionEvent;I)V
    .registers 6
    .parameter "event"
    .parameter "nestingLevel"

    #@0
    .prologue
    .line 460
    const-string v2, "GenericMotionEvent"

    #@2
    invoke-direct {p0, p1, p2, v2}, Landroid/view/InputEventConsistencyVerifier;->startEvent(Landroid/view/InputEvent;ILjava/lang/String;)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 506
    :goto_8
    return-void

    #@9
    .line 465
    :cond_9
    :try_start_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@c
    move-result v2

    #@d
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->ensureMetaStateIsNormalized(I)V

    #@10
    .line 467
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@13
    move-result v0

    #@14
    .line 468
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@17
    move-result v1

    #@18
    .line 469
    .local v1, source:I
    and-int/lit8 v2, v1, 0x2

    #@1a
    if-eqz v2, :cond_4f

    #@1c
    .line 470
    packed-switch v0, :pswitch_data_60

    #@1f
    .line 490
    const-string v2, "Invalid action for generic pointer event."

    #@21
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_9 .. :try_end_24} :catchall_2f

    #@24
    .line 504
    :cond_24
    :goto_24
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@27
    goto :goto_8

    #@28
    .line 472
    :pswitch_28
    :try_start_28
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@2b
    .line 473
    const/4 v2, 0x1

    #@2c
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mHoverEntered:Z
    :try_end_2e
    .catchall {:try_start_28 .. :try_end_2e} :catchall_2f

    #@2e
    goto :goto_24

    #@2f
    .line 504
    .end local v0           #action:I
    .end local v1           #source:I
    :catchall_2f
    move-exception v2

    #@30
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@33
    throw v2

    #@34
    .line 476
    .restart local v0       #action:I
    .restart local v1       #source:I
    :pswitch_34
    :try_start_34
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@37
    goto :goto_24

    #@38
    .line 479
    :pswitch_38
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@3b
    .line 480
    iget-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mHoverEntered:Z

    #@3d
    if-nez v2, :cond_44

    #@3f
    .line 481
    const-string v2, "ACTION_HOVER_EXIT without prior ACTION_HOVER_ENTER"

    #@41
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@44
    .line 483
    :cond_44
    const/4 v2, 0x0

    #@45
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mHoverEntered:Z

    #@47
    goto :goto_24

    #@48
    .line 486
    :pswitch_48
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@4b
    .line 487
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@4e
    goto :goto_24

    #@4f
    .line 493
    :cond_4f
    and-int/lit8 v2, v1, 0x10

    #@51
    if-eqz v2, :cond_24

    #@53
    .line 494
    packed-switch v0, :pswitch_data_6c

    #@56
    .line 499
    const-string v2, "Invalid action for generic joystick event."

    #@58
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@5b
    goto :goto_24

    #@5c
    .line 496
    :pswitch_5c
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V
    :try_end_5f
    .catchall {:try_start_34 .. :try_end_5f} :catchall_2f

    #@5f
    goto :goto_24

    #@60
    .line 470
    :pswitch_data_60
    .packed-switch 0x7
        :pswitch_34
        :pswitch_48
        :pswitch_28
        :pswitch_38
    .end packed-switch

    #@6c
    .line 494
    :pswitch_data_6c
    .packed-switch 0x2
        :pswitch_5c
    .end packed-switch
.end method

.method public onInputEvent(Landroid/view/InputEvent;I)V
    .registers 6
    .parameter "event"
    .parameter "nestingLevel"

    #@0
    .prologue
    .line 169
    instance-of v2, p1, Landroid/view/KeyEvent;

    #@2
    if-eqz v2, :cond_b

    #@4
    move-object v0, p1

    #@5
    .line 170
    check-cast v0, Landroid/view/KeyEvent;

    #@7
    .line 171
    .local v0, keyEvent:Landroid/view/KeyEvent;
    invoke-virtual {p0, v0, p2}, Landroid/view/InputEventConsistencyVerifier;->onKeyEvent(Landroid/view/KeyEvent;I)V

    #@a
    .line 182
    .end local v0           #keyEvent:Landroid/view/KeyEvent;
    :goto_a
    return-void

    #@b
    :cond_b
    move-object v1, p1

    #@c
    .line 173
    check-cast v1, Landroid/view/MotionEvent;

    #@e
    .line 174
    .local v1, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_18

    #@14
    .line 175
    invoke-virtual {p0, v1, p2}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    #@17
    goto :goto_a

    #@18
    .line 176
    :cond_18
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getSource()I

    #@1b
    move-result v2

    #@1c
    and-int/lit8 v2, v2, 0x4

    #@1e
    if-eqz v2, :cond_24

    #@20
    .line 177
    invoke-virtual {p0, v1, p2}, Landroid/view/InputEventConsistencyVerifier;->onTrackballEvent(Landroid/view/MotionEvent;I)V

    #@23
    goto :goto_a

    #@24
    .line 179
    :cond_24
    invoke-virtual {p0, v1, p2}, Landroid/view/InputEventConsistencyVerifier;->onGenericMotionEvent(Landroid/view/MotionEvent;I)V

    #@27
    goto :goto_a
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;I)V
    .registers 10
    .parameter "event"
    .parameter "nestingLevel"

    #@0
    .prologue
    .line 194
    const-string v5, "KeyEvent"

    #@2
    invoke-direct {p0, p1, p2, v5}, Landroid/view/InputEventConsistencyVerifier;->startEvent(Landroid/view/InputEvent;ILjava/lang/String;)Z

    #@5
    move-result v5

    #@6
    if-nez v5, :cond_9

    #@8
    .line 244
    :goto_8
    return-void

    #@9
    .line 199
    :cond_9
    :try_start_9
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    #@c
    move-result v5

    #@d
    invoke-direct {p0, v5}, Landroid/view/InputEventConsistencyVerifier;->ensureMetaStateIsNormalized(I)V

    #@10
    .line 201
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@13
    move-result v0

    #@14
    .line 202
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I

    #@17
    move-result v1

    #@18
    .line 203
    .local v1, deviceId:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getSource()I

    #@1b
    move-result v3

    #@1c
    .line 204
    .local v3, source:I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@1f
    move-result v2

    #@20
    .line 205
    .local v2, keyCode:I
    packed-switch v0, :pswitch_data_82

    #@23
    .line 237
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v6, "Invalid action "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-static {v0}, Landroid/view/KeyEvent;->actionToString(I)Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    const-string v6, " for key event."

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-direct {p0, v5}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V
    :try_end_43
    .catchall {:try_start_9 .. :try_end_43} :catchall_56

    #@43
    .line 242
    :cond_43
    :goto_43
    :pswitch_43
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@46
    goto :goto_8

    #@47
    .line 207
    :pswitch_47
    const/4 v5, 0x0

    #@48
    :try_start_48
    invoke-direct {p0, v1, v3, v2, v5}, Landroid/view/InputEventConsistencyVerifier;->findKeyState(IIIZ)Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@4b
    move-result-object v4

    #@4c
    .line 208
    .local v4, state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    if-eqz v4, :cond_6d

    #@4e
    .line 213
    iget-boolean v5, v4, Landroid/view/InputEventConsistencyVerifier$KeyState;->unhandled:Z

    #@50
    if-eqz v5, :cond_5b

    #@52
    .line 214
    const/4 v5, 0x0

    #@53
    iput-boolean v5, v4, Landroid/view/InputEventConsistencyVerifier$KeyState;->unhandled:Z
    :try_end_55
    .catchall {:try_start_48 .. :try_end_55} :catchall_56

    #@55
    goto :goto_43

    #@56
    .line 242
    .end local v0           #action:I
    .end local v1           #deviceId:I
    .end local v2           #keyCode:I
    .end local v3           #source:I
    .end local v4           #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :catchall_56
    move-exception v5

    #@57
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@5a
    throw v5

    #@5b
    .line 215
    .restart local v0       #action:I
    .restart local v1       #deviceId:I
    .restart local v2       #keyCode:I
    .restart local v3       #source:I
    .restart local v4       #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :cond_5b
    :try_start_5b
    iget v5, p0, Landroid/view/InputEventConsistencyVerifier;->mFlags:I

    #@5d
    and-int/lit8 v5, v5, 0x1

    #@5f
    if-nez v5, :cond_43

    #@61
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@64
    move-result v5

    #@65
    if-nez v5, :cond_43

    #@67
    .line 217
    const-string v5, "ACTION_DOWN but key is already down and this event is not a key repeat."

    #@69
    invoke-direct {p0, v5}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@6c
    goto :goto_43

    #@6d
    .line 221
    :cond_6d
    invoke-direct {p0, v1, v3, v2}, Landroid/view/InputEventConsistencyVerifier;->addKeyState(III)V

    #@70
    goto :goto_43

    #@71
    .line 226
    .end local v4           #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :pswitch_71
    const/4 v5, 0x1

    #@72
    invoke-direct {p0, v1, v3, v2, v5}, Landroid/view/InputEventConsistencyVerifier;->findKeyState(IIIZ)Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@75
    move-result-object v4

    #@76
    .line 227
    .restart local v4       #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    if-nez v4, :cond_7e

    #@78
    .line 228
    const-string v5, "ACTION_UP but key was not down."

    #@7a
    invoke-direct {p0, v5}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@7d
    goto :goto_43

    #@7e
    .line 230
    :cond_7e
    invoke-virtual {v4}, Landroid/view/InputEventConsistencyVerifier$KeyState;->recycle()V
    :try_end_81
    .catchall {:try_start_5b .. :try_end_81} :catchall_56

    #@81
    goto :goto_43

    #@82
    .line 205
    :pswitch_data_82
    .packed-switch 0x0
        :pswitch_47
        :pswitch_71
        :pswitch_43
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;I)V
    .registers 16
    .parameter "event"
    .parameter "nestingLevel"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    .line 319
    const-string v12, "TouchEvent"

    #@4
    invoke-direct {p0, p1, p2, v12}, Landroid/view/InputEventConsistencyVerifier;->startEvent(Landroid/view/InputEvent;ILjava/lang/String;)Z

    #@7
    move-result v12

    #@8
    if-nez v12, :cond_b

    #@a
    .line 448
    :goto_a
    return-void

    #@b
    .line 323
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@e
    move-result v0

    #@f
    .line 324
    .local v0, action:I
    if-eqz v0, :cond_17

    #@11
    const/4 v12, 0x3

    #@12
    if-eq v0, v12, :cond_17

    #@14
    const/4 v12, 0x4

    #@15
    if-ne v0, v12, :cond_e8

    #@17
    :cond_17
    move v7, v11

    #@18
    .line 326
    .local v7, newStream:Z
    :goto_18
    if-eqz v7, :cond_28

    #@1a
    iget-boolean v12, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@1c
    if-nez v12, :cond_22

    #@1e
    iget-boolean v12, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamUnhandled:Z

    #@20
    if-eqz v12, :cond_28

    #@22
    .line 327
    :cond_22
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@24
    .line 328
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamUnhandled:Z

    #@26
    .line 329
    iput v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@28
    .line 331
    :cond_28
    iget-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@2a
    if-eqz v10, :cond_2f

    #@2c
    .line 332
    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->setTainted(Z)V

    #@2f
    .line 336
    :cond_2f
    :try_start_2f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@32
    move-result v10

    #@33
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->ensureMetaStateIsNormalized(I)V

    #@36
    .line 338
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    #@39
    move-result v3

    #@3a
    .line 339
    .local v3, deviceId:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@3d
    move-result v9

    #@3e
    .line 341
    .local v9, source:I
    if-nez v7, :cond_8d

    #@40
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamDeviceId:I

    #@42
    const/4 v12, -0x1

    #@43
    if-eq v10, v12, :cond_8d

    #@45
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamDeviceId:I

    #@47
    if-ne v10, v3, :cond_4d

    #@49
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamSource:I

    #@4b
    if-eq v10, v9, :cond_8d

    #@4d
    .line 344
    :cond_4d
    new-instance v10, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v12, "Touch event stream contains events from multiple sources: previous device id "

    #@54
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v10

    #@58
    iget v12, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamDeviceId:I

    #@5a
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v10

    #@5e
    const-string v12, ", previous source "

    #@60
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v10

    #@64
    iget v12, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamSource:I

    #@66
    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@69
    move-result-object v12

    #@6a
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v10

    #@6e
    const-string v12, ", new device id "

    #@70
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v10

    #@74
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v10

    #@78
    const-string v12, ", new source "

    #@7a
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v10

    #@7e
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@81
    move-result-object v12

    #@82
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v10

    #@86
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v10

    #@8a
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@8d
    .line 350
    :cond_8d
    iput v3, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamDeviceId:I

    #@8f
    .line 351
    iput v9, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamSource:I

    #@91
    .line 353
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@94
    move-result v8

    #@95
    .line 354
    .local v8, pointerCount:I
    and-int/lit8 v10, v9, 0x2

    #@97
    if-eqz v10, :cond_21f

    #@99
    .line 355
    packed-switch v0, :pswitch_data_226

    #@9c
    .line 395
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@9f
    move-result v2

    #@a0
    .line 396
    .local v2, actionMasked:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@a3
    move-result v1

    #@a4
    .line 397
    .local v1, actionIndex:I
    const/4 v10, 0x5

    #@a5
    if-ne v2, v10, :cond_195

    #@a7
    .line 398
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@a9
    if-nez v10, :cond_b3

    #@ab
    .line 399
    const-string v10, "ACTION_POINTER_DOWN but no other pointers were down."

    #@ad
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@b0
    .line 400
    const/4 v10, 0x1

    #@b1
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@b3
    .line 402
    :cond_b3
    if-ltz v1, :cond_b7

    #@b5
    if-lt v1, v8, :cond_162

    #@b7
    .line 403
    :cond_b7
    new-instance v10, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v11, "ACTION_POINTER_DOWN index is "

    #@be
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v10

    #@c2
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v10

    #@c6
    const-string v11, " but the pointer count is "

    #@c8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v10

    #@cc
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v10

    #@d0
    const-string v11, "."

    #@d2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v10

    #@d6
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v10

    #@da
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@dd
    .line 405
    const/4 v10, 0x1

    #@de
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@e0
    .line 417
    :goto_e0
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V
    :try_end_e3
    .catchall {:try_start_2f .. :try_end_e3} :catchall_104

    #@e3
    .line 446
    .end local v1           #actionIndex:I
    .end local v2           #actionMasked:I
    :cond_e3
    :goto_e3
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@e6
    goto/16 :goto_a

    #@e8
    .end local v3           #deviceId:I
    .end local v7           #newStream:Z
    .end local v8           #pointerCount:I
    .end local v9           #source:I
    :cond_e8
    move v7, v10

    #@e9
    .line 324
    goto/16 :goto_18

    #@eb
    .line 357
    .restart local v3       #deviceId:I
    .restart local v7       #newStream:Z
    .restart local v8       #pointerCount:I
    .restart local v9       #source:I
    :pswitch_eb
    :try_start_eb
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@ed
    if-eqz v10, :cond_f4

    #@ef
    .line 358
    const-string v10, "ACTION_DOWN but pointers are already down.  Probably missing ACTION_UP from previous gesture."

    #@f1
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@f4
    .line 361
    :cond_f4
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@f7
    .line 362
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@fa
    .line 363
    const/4 v10, 0x0

    #@fb
    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@fe
    move-result v10

    #@ff
    shl-int v10, v11, v10

    #@101
    iput v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I
    :try_end_103
    .catchall {:try_start_eb .. :try_end_103} :catchall_104

    #@103
    goto :goto_e3

    #@104
    .line 446
    .end local v3           #deviceId:I
    .end local v8           #pointerCount:I
    .end local v9           #source:I
    :catchall_104
    move-exception v10

    #@105
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@108
    throw v10

    #@109
    .line 366
    .restart local v3       #deviceId:I
    .restart local v8       #pointerCount:I
    .restart local v9       #source:I
    :pswitch_109
    :try_start_109
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@10c
    .line 367
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@10f
    .line 368
    const/4 v10, 0x0

    #@110
    iput v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@112
    .line 369
    const/4 v10, 0x0

    #@113
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@115
    goto :goto_e3

    #@116
    .line 372
    :pswitch_116
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@118
    invoke-static {v10}, Ljava/lang/Integer;->bitCount(I)I

    #@11b
    move-result v4

    #@11c
    .line 374
    .local v4, expectedPointerCount:I
    if-eq v8, v4, :cond_e3

    #@11e
    .line 375
    new-instance v10, Ljava/lang/StringBuilder;

    #@120
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@123
    const-string v11, "ACTION_MOVE contained "

    #@125
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v10

    #@129
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v10

    #@12d
    const-string v11, " pointers but there are currently "

    #@12f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v10

    #@133
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@136
    move-result-object v10

    #@137
    const-string v11, " pointers down."

    #@139
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v10

    #@13d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@140
    move-result-object v10

    #@141
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@144
    .line 378
    const/4 v10, 0x1

    #@145
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@147
    goto :goto_e3

    #@148
    .line 383
    .end local v4           #expectedPointerCount:I
    :pswitch_148
    const/4 v10, 0x0

    #@149
    iput v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@14b
    .line 384
    const/4 v10, 0x0

    #@14c
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@14e
    goto :goto_e3

    #@14f
    .line 387
    :pswitch_14f
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@151
    if-eqz v10, :cond_158

    #@153
    .line 388
    const-string v10, "ACTION_OUTSIDE but pointers are still down."

    #@155
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@158
    .line 390
    :cond_158
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@15b
    .line 391
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@15e
    .line 392
    const/4 v10, 0x0

    #@15f
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@161
    goto :goto_e3

    #@162
    .line 407
    .restart local v1       #actionIndex:I
    .restart local v2       #actionMasked:I
    :cond_162
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@165
    move-result v5

    #@166
    .line 408
    .local v5, id:I
    shl-int v6, v11, v5

    #@168
    .line 409
    .local v6, idBit:I
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@16a
    and-int/2addr v10, v6

    #@16b
    if-eqz v10, :cond_18e

    #@16d
    .line 410
    new-instance v10, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    const-string v11, "ACTION_POINTER_DOWN specified pointer id "

    #@174
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v10

    #@178
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v10

    #@17c
    const-string v11, " which is already down."

    #@17e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v10

    #@182
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@185
    move-result-object v10

    #@186
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@189
    .line 412
    const/4 v10, 0x1

    #@18a
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@18c
    goto/16 :goto_e0

    #@18e
    .line 414
    :cond_18e
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@190
    or-int/2addr v10, v6

    #@191
    iput v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@193
    goto/16 :goto_e0

    #@195
    .line 418
    .end local v5           #id:I
    .end local v6           #idBit:I
    :cond_195
    const/4 v10, 0x6

    #@196
    if-ne v2, v10, :cond_1fd

    #@198
    .line 419
    if-ltz v1, :cond_19c

    #@19a
    if-lt v1, v8, :cond_1ca

    #@19c
    .line 420
    :cond_19c
    new-instance v10, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v11, "ACTION_POINTER_UP index is "

    #@1a3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v10

    #@1a7
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v10

    #@1ab
    const-string v11, " but the pointer count is "

    #@1ad
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v10

    #@1b1
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v10

    #@1b5
    const-string v11, "."

    #@1b7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v10

    #@1bb
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1be
    move-result-object v10

    #@1bf
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@1c2
    .line 422
    const/4 v10, 0x1

    #@1c3
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@1c5
    .line 434
    :goto_1c5
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@1c8
    goto/16 :goto_e3

    #@1ca
    .line 424
    :cond_1ca
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1cd
    move-result v5

    #@1ce
    .line 425
    .restart local v5       #id:I
    shl-int v6, v11, v5

    #@1d0
    .line 426
    .restart local v6       #idBit:I
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@1d2
    and-int/2addr v10, v6

    #@1d3
    if-nez v10, :cond_1f5

    #@1d5
    .line 427
    new-instance v10, Ljava/lang/StringBuilder;

    #@1d7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1da
    const-string v11, "ACTION_POINTER_UP specified pointer id "

    #@1dc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v10

    #@1e0
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v10

    #@1e4
    const-string v11, " which is not currently down."

    #@1e6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v10

    #@1ea
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ed
    move-result-object v10

    #@1ee
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@1f1
    .line 429
    const/4 v10, 0x1

    #@1f2
    iput-boolean v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@1f4
    goto :goto_1c5

    #@1f5
    .line 431
    :cond_1f5
    iget v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@1f7
    xor-int/lit8 v11, v6, -0x1

    #@1f9
    and-int/2addr v10, v11

    #@1fa
    iput v10, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@1fc
    goto :goto_1c5

    #@1fd
    .line 436
    .end local v5           #id:I
    .end local v6           #idBit:I
    :cond_1fd
    new-instance v10, Ljava/lang/StringBuilder;

    #@1ff
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@202
    const-string v11, "Invalid action "

    #@204
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@207
    move-result-object v10

    #@208
    invoke-static {v0}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    #@20b
    move-result-object v11

    #@20c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v10

    #@210
    const-string v11, " for touch event."

    #@212
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@215
    move-result-object v10

    #@216
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@219
    move-result-object v10

    #@21a
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@21d
    goto/16 :goto_e3

    #@21f
    .line 443
    .end local v1           #actionIndex:I
    .end local v2           #actionMasked:I
    :cond_21f
    const-string v10, "Source was not SOURCE_CLASS_POINTER."

    #@221
    invoke-direct {p0, v10}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V
    :try_end_224
    .catchall {:try_start_109 .. :try_end_224} :catchall_104

    #@224
    goto/16 :goto_e3

    #@226
    .line 355
    :pswitch_data_226
    .packed-switch 0x0
        :pswitch_eb
        :pswitch_109
        :pswitch_116
        :pswitch_148
        :pswitch_14f
    .end packed-switch
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;I)V
    .registers 8
    .parameter "event"
    .parameter "nestingLevel"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 256
    const-string v2, "TrackballEvent"

    #@3
    invoke-direct {p0, p1, p2, v2}, Landroid/view/InputEventConsistencyVerifier;->startEvent(Landroid/view/InputEvent;ILjava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 307
    :goto_9
    return-void

    #@a
    .line 261
    :cond_a
    :try_start_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    #@d
    move-result v2

    #@e
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->ensureMetaStateIsNormalized(I)V

    #@11
    .line 263
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@14
    move-result v0

    #@15
    .line 264
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@18
    move-result v1

    #@19
    .line 265
    .local v1, source:I
    and-int/lit8 v2, v1, 0x4

    #@1b
    if-eqz v2, :cond_a2

    #@1d
    .line 266
    packed-switch v0, :pswitch_data_a8

    #@20
    .line 291
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "Invalid action "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v0}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, " for trackball event."

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@40
    .line 296
    :goto_40
    iget-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@42
    if-eqz v2, :cond_90

    #@44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    #@47
    move-result v2

    #@48
    cmpg-float v2, v2, v4

    #@4a
    if-gtz v2, :cond_90

    #@4c
    .line 297
    const-string v2, "Trackball is down but pressure is not greater than 0."

    #@4e
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V
    :try_end_51
    .catchall {:try_start_a .. :try_end_51} :catchall_69

    #@51
    .line 305
    :cond_51
    :goto_51
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@54
    goto :goto_9

    #@55
    .line 268
    :pswitch_55
    :try_start_55
    iget-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@57
    if-eqz v2, :cond_6e

    #@59
    iget-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballUnhandled:Z

    #@5b
    if-nez v2, :cond_6e

    #@5d
    .line 269
    const-string v2, "ACTION_DOWN but trackball is already down."

    #@5f
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@62
    .line 274
    :goto_62
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@65
    .line 275
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V
    :try_end_68
    .catchall {:try_start_55 .. :try_end_68} :catchall_69

    #@68
    goto :goto_40

    #@69
    .line 305
    .end local v0           #action:I
    .end local v1           #source:I
    :catchall_69
    move-exception v2

    #@6a
    invoke-direct {p0}, Landroid/view/InputEventConsistencyVerifier;->finishEvent()V

    #@6d
    throw v2

    #@6e
    .line 271
    .restart local v0       #action:I
    .restart local v1       #source:I
    :cond_6e
    const/4 v2, 0x1

    #@6f
    :try_start_6f
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@71
    .line 272
    const/4 v2, 0x0

    #@72
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballUnhandled:Z

    #@74
    goto :goto_62

    #@75
    .line 278
    :pswitch_75
    iget-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@77
    if-nez v2, :cond_85

    #@79
    .line 279
    const-string v2, "ACTION_UP but trackball is not down."

    #@7b
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@7e
    .line 284
    :goto_7e
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensureHistorySizeIsZeroForThisAction(Landroid/view/MotionEvent;)V

    #@81
    .line 285
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@84
    goto :goto_40

    #@85
    .line 281
    :cond_85
    const/4 v2, 0x0

    #@86
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@88
    .line 282
    const/4 v2, 0x0

    #@89
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballUnhandled:Z

    #@8b
    goto :goto_7e

    #@8c
    .line 288
    :pswitch_8c
    invoke-direct {p0, p1}, Landroid/view/InputEventConsistencyVerifier;->ensurePointerCountIsOneForThisAction(Landroid/view/MotionEvent;)V

    #@8f
    goto :goto_40

    #@90
    .line 298
    :cond_90
    iget-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@92
    if-nez v2, :cond_51

    #@94
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    #@97
    move-result v2

    #@98
    cmpl-float v2, v2, v4

    #@9a
    if-eqz v2, :cond_51

    #@9c
    .line 299
    const-string v2, "Trackball is up but pressure is not equal to 0."

    #@9e
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V

    #@a1
    goto :goto_51

    #@a2
    .line 302
    :cond_a2
    const-string v2, "Source was not SOURCE_CLASS_TRACKBALL."

    #@a4
    invoke-direct {p0, v2}, Landroid/view/InputEventConsistencyVerifier;->problem(Ljava/lang/String;)V
    :try_end_a7
    .catchall {:try_start_6f .. :try_end_a7} :catchall_69

    #@a7
    goto :goto_51

    #@a8
    .line 266
    :pswitch_data_a8
    .packed-switch 0x0
        :pswitch_55
        :pswitch_75
        :pswitch_8c
    .end packed-switch
.end method

.method public onUnhandledEvent(Landroid/view/InputEvent;I)V
    .registers 12
    .parameter "event"
    .parameter "nestingLevel"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 521
    iget v6, p0, Landroid/view/InputEventConsistencyVerifier;->mLastNestingLevel:I

    #@3
    if-eq p2, v6, :cond_6

    #@5
    .line 548
    :cond_5
    :goto_5
    return-void

    #@6
    .line 525
    :cond_6
    iget-object v6, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEventsUnhandled:[Z

    #@8
    if-eqz v6, :cond_10

    #@a
    .line 526
    iget-object v6, p0, Landroid/view/InputEventConsistencyVerifier;->mRecentEventsUnhandled:[Z

    #@c
    iget v7, p0, Landroid/view/InputEventConsistencyVerifier;->mMostRecentEventIndex:I

    #@e
    aput-boolean v8, v6, v7

    #@10
    .line 529
    :cond_10
    instance-of v6, p1, Landroid/view/KeyEvent;

    #@12
    if-eqz v6, :cond_2d

    #@14
    move-object v2, p1

    #@15
    .line 530
    check-cast v2, Landroid/view/KeyEvent;

    #@17
    .line 531
    .local v2, keyEvent:Landroid/view/KeyEvent;
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getDeviceId()I

    #@1a
    move-result v0

    #@1b
    .line 532
    .local v0, deviceId:I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getSource()I

    #@1e
    move-result v4

    #@1f
    .line 533
    .local v4, source:I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@22
    move-result v1

    #@23
    .line 534
    .local v1, keyCode:I
    const/4 v6, 0x0

    #@24
    invoke-direct {p0, v0, v4, v1, v6}, Landroid/view/InputEventConsistencyVerifier;->findKeyState(IIIZ)Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@27
    move-result-object v5

    #@28
    .line 535
    .local v5, state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    if-eqz v5, :cond_5

    #@2a
    .line 536
    iput-boolean v8, v5, Landroid/view/InputEventConsistencyVerifier$KeyState;->unhandled:Z

    #@2c
    goto :goto_5

    #@2d
    .end local v0           #deviceId:I
    .end local v1           #keyCode:I
    .end local v2           #keyEvent:Landroid/view/KeyEvent;
    .end local v4           #source:I
    .end local v5           #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :cond_2d
    move-object v3, p1

    #@2e
    .line 539
    check-cast v3, Landroid/view/MotionEvent;

    #@30
    .line 540
    .local v3, motionEvent:Landroid/view/MotionEvent;
    invoke-virtual {v3}, Landroid/view/MotionEvent;->isTouchEvent()Z

    #@33
    move-result v6

    #@34
    if-eqz v6, :cond_39

    #@36
    .line 541
    iput-boolean v8, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamUnhandled:Z

    #@38
    goto :goto_5

    #@39
    .line 542
    :cond_39
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getSource()I

    #@3c
    move-result v6

    #@3d
    and-int/lit8 v6, v6, 0x4

    #@3f
    if-eqz v6, :cond_5

    #@41
    .line 543
    iget-boolean v6, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@43
    if-eqz v6, :cond_5

    #@45
    .line 544
    iput-boolean v8, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballUnhandled:Z

    #@47
    goto :goto_5
.end method

.method public reset()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 143
    const/4 v1, -0x1

    #@2
    iput v1, p0, Landroid/view/InputEventConsistencyVerifier;->mLastEventSeq:I

    #@4
    .line 144
    iput v2, p0, Landroid/view/InputEventConsistencyVerifier;->mLastNestingLevel:I

    #@6
    .line 145
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballDown:Z

    #@8
    .line 146
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTrackballUnhandled:Z

    #@a
    .line 147
    iput v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamPointers:I

    #@c
    .line 148
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamIsTainted:Z

    #@e
    .line 149
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mTouchEventStreamUnhandled:Z

    #@10
    .line 150
    iput-boolean v2, p0, Landroid/view/InputEventConsistencyVerifier;->mHoverEntered:Z

    #@12
    .line 152
    :goto_12
    iget-object v1, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@14
    if-eqz v1, :cond_20

    #@16
    .line 153
    iget-object v0, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@18
    .line 154
    .local v0, state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    iget-object v1, v0, Landroid/view/InputEventConsistencyVerifier$KeyState;->next:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@1a
    iput-object v1, p0, Landroid/view/InputEventConsistencyVerifier;->mKeyStateList:Landroid/view/InputEventConsistencyVerifier$KeyState;

    #@1c
    .line 155
    invoke-virtual {v0}, Landroid/view/InputEventConsistencyVerifier$KeyState;->recycle()V

    #@1f
    goto :goto_12

    #@20
    .line 157
    .end local v0           #state:Landroid/view/InputEventConsistencyVerifier$KeyState;
    :cond_20
    return-void
.end method
