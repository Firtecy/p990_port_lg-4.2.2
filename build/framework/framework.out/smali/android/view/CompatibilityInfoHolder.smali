.class public Landroid/view/CompatibilityInfoHolder;
.super Ljava/lang/Object;
.source "CompatibilityInfoHolder.java"


# instance fields
.field private volatile mCompatInfo:Landroid/content/res/CompatibilityInfo;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 22
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 23
    sget-object v0, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@5
    iput-object v0, p0, Landroid/view/CompatibilityInfoHolder;->mCompatInfo:Landroid/content/res/CompatibilityInfo;

    #@7
    return-void
.end method


# virtual methods
.method public get()Landroid/content/res/CompatibilityInfo;
    .registers 2

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/view/CompatibilityInfoHolder;->mCompatInfo:Landroid/content/res/CompatibilityInfo;

    #@2
    return-object v0
.end method

.method public getIfNeeded()Landroid/content/res/CompatibilityInfo;
    .registers 3

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Landroid/view/CompatibilityInfoHolder;->mCompatInfo:Landroid/content/res/CompatibilityInfo;

    #@2
    .line 44
    .local v0, ci:Landroid/content/res/CompatibilityInfo;
    if-eqz v0, :cond_8

    #@4
    sget-object v1, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@6
    if-ne v0, v1, :cond_9

    #@8
    .line 45
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 47
    .end local v0           #ci:Landroid/content/res/CompatibilityInfo;
    :cond_9
    return-object v0
.end method

.method public set(Landroid/content/res/CompatibilityInfo;)V
    .registers 3
    .parameter "compatInfo"

    #@0
    .prologue
    .line 26
    if-eqz p1, :cond_11

    #@2
    invoke-virtual {p1}, Landroid/content/res/CompatibilityInfo;->isScalingRequired()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_e

    #@8
    invoke-virtual {p1}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_11

    #@e
    .line 28
    :cond_e
    iput-object p1, p0, Landroid/view/CompatibilityInfoHolder;->mCompatInfo:Landroid/content/res/CompatibilityInfo;

    #@10
    .line 36
    :goto_10
    return-void

    #@11
    .line 30
    :cond_11
    if-eqz p1, :cond_1c

    #@13
    invoke-virtual {p1}, Landroid/content/res/CompatibilityInfo;->needsSecondaryDpi()Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_1c

    #@19
    .line 31
    iput-object p1, p0, Landroid/view/CompatibilityInfoHolder;->mCompatInfo:Landroid/content/res/CompatibilityInfo;

    #@1b
    goto :goto_10

    #@1c
    .line 34
    :cond_1c
    sget-object v0, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@1e
    iput-object v0, p0, Landroid/view/CompatibilityInfoHolder;->mCompatInfo:Landroid/content/res/CompatibilityInfo;

    #@20
    goto :goto_10
.end method
