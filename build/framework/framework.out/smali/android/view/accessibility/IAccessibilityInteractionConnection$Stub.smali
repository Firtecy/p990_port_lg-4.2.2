.class public abstract Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;
.super Landroid/os/Binder;
.source "IAccessibilityInteractionConnection.java"

# interfaces
.implements Landroid/view/accessibility/IAccessibilityInteractionConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/accessibility/IAccessibilityInteractionConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.accessibility.IAccessibilityInteractionConnection"

.field static final TRANSACTION_findAccessibilityNodeInfoByAccessibilityId:I = 0x1

.field static final TRANSACTION_findAccessibilityNodeInfoByViewId:I = 0x2

.field static final TRANSACTION_findAccessibilityNodeInfosByText:I = 0x3

.field static final TRANSACTION_findFocus:I = 0x4

.field static final TRANSACTION_focusSearch:I = 0x5

.field static final TRANSACTION_performAccessibilityAction:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnection;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 36
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 44
    sparse-switch p1, :sswitch_data_166

    #@3
    .line 177
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 48
    :sswitch_8
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 49
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 53
    :sswitch_11
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 55
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@1b
    move-result-wide v2

    #@1c
    .line 57
    .local v2, _arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v4

    #@20
    .line 59
    .local v4, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@23
    move-result-object v1

    #@24
    invoke-static {v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@27
    move-result-object v5

    #@28
    .line 61
    .local v5, _arg2:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v6

    #@2c
    .line 63
    .local v6, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v7

    #@30
    .line 65
    .local v7, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@33
    move-result-wide v8

    #@34
    .local v8, _arg5:J
    move-object/from16 v1, p0

    #@36
    .line 66
    invoke-virtual/range {v1 .. v9}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->findAccessibilityNodeInfoByAccessibilityId(JILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@39
    .line 67
    const/4 v1, 0x1

    #@3a
    goto :goto_7

    #@3b
    .line 71
    .end local v2           #_arg0:J
    .end local v4           #_arg1:I
    .end local v5           #_arg2:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v6           #_arg3:I
    .end local v7           #_arg4:I
    .end local v8           #_arg5:J
    :sswitch_3b
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@3d
    move-object/from16 v0, p2

    #@3f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42
    .line 73
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@45
    move-result-wide v2

    #@46
    .line 75
    .restart local v2       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v4

    #@4a
    .line 77
    .restart local v4       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v5

    #@4e
    .line 79
    .local v5, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@51
    move-result-object v1

    #@52
    invoke-static {v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@55
    move-result-object v6

    #@56
    .line 81
    .local v6, _arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@59
    move-result v7

    #@5a
    .line 83
    .restart local v7       #_arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5d
    move-result v8

    #@5e
    .line 85
    .local v8, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@61
    move-result-wide v18

    #@62
    .local v18, _arg6:J
    move-object/from16 v10, p0

    #@64
    move-wide v11, v2

    #@65
    move v13, v4

    #@66
    move v14, v5

    #@67
    move-object v15, v6

    #@68
    move/from16 v16, v7

    #@6a
    move/from16 v17, v8

    #@6c
    .line 86
    invoke-virtual/range {v10 .. v19}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->findAccessibilityNodeInfoByViewId(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@6f
    .line 87
    const/4 v1, 0x1

    #@70
    goto :goto_7

    #@71
    .line 91
    .end local v2           #_arg0:J
    .end local v4           #_arg1:I
    .end local v5           #_arg2:I
    .end local v6           #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v7           #_arg4:I
    .end local v8           #_arg5:I
    .end local v18           #_arg6:J
    :sswitch_71
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@73
    move-object/from16 v0, p2

    #@75
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@78
    .line 93
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@7b
    move-result-wide v2

    #@7c
    .line 95
    .restart local v2       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7f
    move-result-object v4

    #@80
    .line 97
    .local v4, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v5

    #@84
    .line 99
    .restart local v5       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@87
    move-result-object v1

    #@88
    invoke-static {v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@8b
    move-result-object v6

    #@8c
    .line 101
    .restart local v6       #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8f
    move-result v7

    #@90
    .line 103
    .restart local v7       #_arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@93
    move-result v8

    #@94
    .line 105
    .restart local v8       #_arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@97
    move-result-wide v18

    #@98
    .restart local v18       #_arg6:J
    move-object/from16 v10, p0

    #@9a
    move-wide v11, v2

    #@9b
    move-object v13, v4

    #@9c
    move v14, v5

    #@9d
    move-object v15, v6

    #@9e
    move/from16 v16, v7

    #@a0
    move/from16 v17, v8

    #@a2
    .line 106
    invoke-virtual/range {v10 .. v19}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->findAccessibilityNodeInfosByText(JLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@a5
    .line 107
    const/4 v1, 0x1

    #@a6
    goto/16 :goto_7

    #@a8
    .line 111
    .end local v2           #_arg0:J
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:I
    .end local v6           #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v7           #_arg4:I
    .end local v8           #_arg5:I
    .end local v18           #_arg6:J
    :sswitch_a8
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@aa
    move-object/from16 v0, p2

    #@ac
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@af
    .line 113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@b2
    move-result-wide v2

    #@b3
    .line 115
    .restart local v2       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b6
    move-result v4

    #@b7
    .line 117
    .local v4, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ba
    move-result v5

    #@bb
    .line 119
    .restart local v5       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@be
    move-result-object v1

    #@bf
    invoke-static {v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@c2
    move-result-object v6

    #@c3
    .line 121
    .restart local v6       #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c6
    move-result v7

    #@c7
    .line 123
    .restart local v7       #_arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ca
    move-result v8

    #@cb
    .line 125
    .restart local v8       #_arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@ce
    move-result-wide v18

    #@cf
    .restart local v18       #_arg6:J
    move-object/from16 v10, p0

    #@d1
    move-wide v11, v2

    #@d2
    move v13, v4

    #@d3
    move v14, v5

    #@d4
    move-object v15, v6

    #@d5
    move/from16 v16, v7

    #@d7
    move/from16 v17, v8

    #@d9
    .line 126
    invoke-virtual/range {v10 .. v19}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->findFocus(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@dc
    .line 127
    const/4 v1, 0x1

    #@dd
    goto/16 :goto_7

    #@df
    .line 131
    .end local v2           #_arg0:J
    .end local v4           #_arg1:I
    .end local v5           #_arg2:I
    .end local v6           #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v7           #_arg4:I
    .end local v8           #_arg5:I
    .end local v18           #_arg6:J
    :sswitch_df
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@e1
    move-object/from16 v0, p2

    #@e3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e6
    .line 133
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@e9
    move-result-wide v2

    #@ea
    .line 135
    .restart local v2       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ed
    move-result v4

    #@ee
    .line 137
    .restart local v4       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v5

    #@f2
    .line 139
    .restart local v5       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@f5
    move-result-object v1

    #@f6
    invoke-static {v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@f9
    move-result-object v6

    #@fa
    .line 141
    .restart local v6       #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@fd
    move-result v7

    #@fe
    .line 143
    .restart local v7       #_arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@101
    move-result v8

    #@102
    .line 145
    .restart local v8       #_arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@105
    move-result-wide v18

    #@106
    .restart local v18       #_arg6:J
    move-object/from16 v10, p0

    #@108
    move-wide v11, v2

    #@109
    move v13, v4

    #@10a
    move v14, v5

    #@10b
    move-object v15, v6

    #@10c
    move/from16 v16, v7

    #@10e
    move/from16 v17, v8

    #@110
    .line 146
    invoke-virtual/range {v10 .. v19}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->focusSearch(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@113
    .line 147
    const/4 v1, 0x1

    #@114
    goto/16 :goto_7

    #@116
    .line 151
    .end local v2           #_arg0:J
    .end local v4           #_arg1:I
    .end local v5           #_arg2:I
    .end local v6           #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v7           #_arg4:I
    .end local v8           #_arg5:I
    .end local v18           #_arg6:J
    :sswitch_116
    const-string v1, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@118
    move-object/from16 v0, p2

    #@11a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11d
    .line 153
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@120
    move-result-wide v2

    #@121
    .line 155
    .restart local v2       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@124
    move-result v4

    #@125
    .line 157
    .restart local v4       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@128
    move-result v1

    #@129
    if-eqz v1, :cond_163

    #@12b
    .line 158
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12d
    move-object/from16 v0, p2

    #@12f
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@132
    move-result-object v5

    #@133
    check-cast v5, Landroid/os/Bundle;

    #@135
    .line 164
    .local v5, _arg2:Landroid/os/Bundle;
    :goto_135
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@138
    move-result v6

    #@139
    .line 166
    .local v6, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@13c
    move-result-object v1

    #@13d
    invoke-static {v1}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@140
    move-result-object v7

    #@141
    .line 168
    .local v7, _arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@144
    move-result v8

    #@145
    .line 170
    .restart local v8       #_arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@148
    move-result v18

    #@149
    .line 172
    .local v18, _arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@14c
    move-result-wide v29

    #@14d
    .local v29, _arg7:J
    move-object/from16 v20, p0

    #@14f
    move-wide/from16 v21, v2

    #@151
    move/from16 v23, v4

    #@153
    move-object/from16 v24, v5

    #@155
    move/from16 v25, v6

    #@157
    move-object/from16 v26, v7

    #@159
    move/from16 v27, v8

    #@15b
    move/from16 v28, v18

    #@15d
    .line 173
    invoke-virtual/range {v20 .. v30}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->performAccessibilityAction(JILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V

    #@160
    .line 174
    const/4 v1, 0x1

    #@161
    goto/16 :goto_7

    #@163
    .line 161
    .end local v5           #_arg2:Landroid/os/Bundle;
    .end local v6           #_arg3:I
    .end local v7           #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v8           #_arg5:I
    .end local v18           #_arg6:I
    .end local v29           #_arg7:J
    :cond_163
    const/4 v5, 0x0

    #@164
    .restart local v5       #_arg2:Landroid/os/Bundle;
    goto :goto_135

    #@165
    .line 44
    nop

    #@166
    :sswitch_data_166
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_3b
        0x3 -> :sswitch_71
        0x4 -> :sswitch_a8
        0x5 -> :sswitch_df
        0x6 -> :sswitch_116
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
