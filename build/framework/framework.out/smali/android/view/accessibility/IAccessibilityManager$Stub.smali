.class public abstract Landroid/view/accessibility/IAccessibilityManager$Stub;
.super Landroid/os/Binder;
.source "IAccessibilityManager.java"

# interfaces
.implements Landroid/view/accessibility/IAccessibilityManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/accessibility/IAccessibilityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/accessibility/IAccessibilityManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.accessibility.IAccessibilityManager"

.field static final TRANSACTION_addAccessibilityInteractionConnection:I = 0x6

.field static final TRANSACTION_addClient:I = 0x1

.field static final TRANSACTION_getEnabledAccessibilityServiceList:I = 0x4

.field static final TRANSACTION_getInstalledAccessibilityServiceList:I = 0x3

.field static final TRANSACTION_interrupt:I = 0x5

.field static final TRANSACTION_registerUiTestAutomationService:I = 0x8

.field static final TRANSACTION_removeAccessibilityInteractionConnection:I = 0x7

.field static final TRANSACTION_sendAccessibilityEvent:I = 0x2

.field static final TRANSACTION_setColorConvert:I = 0xb

.field static final TRANSACTION_temporaryEnableAccessibilityStateUntilKeyguardRemoved:I = 0xa

.field static final TRANSACTION_unregisterUiTestAutomationService:I = 0x9


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.view.accessibility.IAccessibilityManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.view.accessibility.IAccessibilityManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/accessibility/IAccessibilityManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/view/accessibility/IAccessibilityManager;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/view/accessibility/IAccessibilityManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/accessibility/IAccessibilityManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    .line 44
    sparse-switch p1, :sswitch_data_14e

    #@5
    .line 192
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v7

    #@9
    :goto_9
    return v7

    #@a
    .line 48
    :sswitch_a
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 53
    :sswitch_10
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@12
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18
    move-result-object v6

    #@19
    invoke-static {v6}, Landroid/view/accessibility/IAccessibilityManagerClient$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManagerClient;

    #@1c
    move-result-object v0

    #@1d
    .line 57
    .local v0, _arg0:Landroid/view/accessibility/IAccessibilityManagerClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v1

    #@21
    .line 58
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/view/accessibility/IAccessibilityManager$Stub;->addClient(Landroid/view/accessibility/IAccessibilityManagerClient;I)I

    #@24
    move-result v4

    #@25
    .line 59
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@28
    .line 60
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    goto :goto_9

    #@2c
    .line 65
    .end local v0           #_arg0:Landroid/view/accessibility/IAccessibilityManagerClient;
    .end local v1           #_arg1:I
    .end local v4           #_result:I
    :sswitch_2c
    const-string v8, "android.view.accessibility.IAccessibilityManager"

    #@2e
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v8

    #@35
    if-eqz v8, :cond_51

    #@37
    .line 68
    sget-object v8, Landroid/view/accessibility/AccessibilityEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@39
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    #@3f
    .line 74
    .local v0, _arg0:Landroid/view/accessibility/AccessibilityEvent;
    :goto_3f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v1

    #@43
    .line 75
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/view/accessibility/IAccessibilityManager$Stub;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;I)Z

    #@46
    move-result v4

    #@47
    .line 76
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a
    .line 77
    if-eqz v4, :cond_4d

    #@4c
    move v6, v7

    #@4d
    :cond_4d
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    goto :goto_9

    #@51
    .line 71
    .end local v0           #_arg0:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #_arg1:I
    .end local v4           #_result:Z
    :cond_51
    const/4 v0, 0x0

    #@52
    .restart local v0       #_arg0:Landroid/view/accessibility/AccessibilityEvent;
    goto :goto_3f

    #@53
    .line 82
    .end local v0           #_arg0:Landroid/view/accessibility/AccessibilityEvent;
    :sswitch_53
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@55
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@58
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v0

    #@5c
    .line 85
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->getInstalledAccessibilityServiceList(I)Ljava/util/List;

    #@5f
    move-result-object v5

    #@60
    .line 86
    .local v5, _result:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@63
    .line 87
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@66
    goto :goto_9

    #@67
    .line 92
    .end local v0           #_arg0:I
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    :sswitch_67
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@69
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v0

    #@70
    .line 96
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v1

    #@74
    .line 97
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/view/accessibility/IAccessibilityManager$Stub;->getEnabledAccessibilityServiceList(II)Ljava/util/List;

    #@77
    move-result-object v5

    #@78
    .line 98
    .restart local v5       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7b
    .line 99
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@7e
    goto :goto_9

    #@7f
    .line 104
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v5           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    :sswitch_7f
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@81
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v0

    #@88
    .line 107
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->interrupt(I)V

    #@8b
    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8e
    goto/16 :goto_9

    #@90
    .line 113
    .end local v0           #_arg0:I
    :sswitch_90
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@92
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95
    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@98
    move-result-object v6

    #@99
    invoke-static {v6}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@9c
    move-result-object v0

    #@9d
    .line 117
    .local v0, _arg0:Landroid/view/IWindow;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a0
    move-result-object v6

    #@a1
    invoke-static {v6}, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnection;

    #@a4
    move-result-object v1

    #@a5
    .line 119
    .local v1, _arg1:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v2

    #@a9
    .line 120
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/accessibility/IAccessibilityManager$Stub;->addAccessibilityInteractionConnection(Landroid/view/IWindow;Landroid/view/accessibility/IAccessibilityInteractionConnection;I)I

    #@ac
    move-result v4

    #@ad
    .line 121
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b0
    .line 122
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@b3
    goto/16 :goto_9

    #@b5
    .line 127
    .end local v0           #_arg0:Landroid/view/IWindow;
    .end local v1           #_arg1:Landroid/view/accessibility/IAccessibilityInteractionConnection;
    .end local v2           #_arg2:I
    .end local v4           #_result:I
    :sswitch_b5
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@b7
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ba
    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@bd
    move-result-object v6

    #@be
    invoke-static {v6}, Landroid/view/IWindow$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindow;

    #@c1
    move-result-object v0

    #@c2
    .line 130
    .restart local v0       #_arg0:Landroid/view/IWindow;
    invoke-virtual {p0, v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->removeAccessibilityInteractionConnection(Landroid/view/IWindow;)V

    #@c5
    .line 131
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c8
    goto/16 :goto_9

    #@ca
    .line 136
    .end local v0           #_arg0:Landroid/view/IWindow;
    :sswitch_ca
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@cc
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cf
    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@d2
    move-result-object v6

    #@d3
    invoke-static {v6}, Landroid/accessibilityservice/IAccessibilityServiceClient$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@d6
    move-result-object v0

    #@d7
    .line 140
    .local v0, _arg0:Landroid/accessibilityservice/IAccessibilityServiceClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@da
    move-result v6

    #@db
    if-eqz v6, :cond_ed

    #@dd
    .line 141
    sget-object v6, Landroid/accessibilityservice/AccessibilityServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@df
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e2
    move-result-object v1

    #@e3
    check-cast v1, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@e5
    .line 146
    .local v1, _arg1:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :goto_e5
    invoke-virtual {p0, v0, v1}, Landroid/view/accessibility/IAccessibilityManager$Stub;->registerUiTestAutomationService(Landroid/accessibilityservice/IAccessibilityServiceClient;Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    #@e8
    .line 147
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@eb
    goto/16 :goto_9

    #@ed
    .line 144
    .end local v1           #_arg1:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_ed
    const/4 v1, 0x0

    #@ee
    .restart local v1       #_arg1:Landroid/accessibilityservice/AccessibilityServiceInfo;
    goto :goto_e5

    #@ef
    .line 152
    .end local v0           #_arg0:Landroid/accessibilityservice/IAccessibilityServiceClient;
    .end local v1           #_arg1:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :sswitch_ef
    const-string v6, "android.view.accessibility.IAccessibilityManager"

    #@f1
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f4
    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@f7
    move-result-object v6

    #@f8
    invoke-static {v6}, Landroid/accessibilityservice/IAccessibilityServiceClient$Stub;->asInterface(Landroid/os/IBinder;)Landroid/accessibilityservice/IAccessibilityServiceClient;

    #@fb
    move-result-object v0

    #@fc
    .line 155
    .restart local v0       #_arg0:Landroid/accessibilityservice/IAccessibilityServiceClient;
    invoke-virtual {p0, v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->unregisterUiTestAutomationService(Landroid/accessibilityservice/IAccessibilityServiceClient;)V

    #@ff
    .line 156
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@102
    goto/16 :goto_9

    #@104
    .line 161
    .end local v0           #_arg0:Landroid/accessibilityservice/IAccessibilityServiceClient;
    :sswitch_104
    const-string v8, "android.view.accessibility.IAccessibilityManager"

    #@106
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@109
    .line 163
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10c
    move-result v8

    #@10d
    if-eqz v8, :cond_126

    #@10f
    .line 164
    sget-object v8, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@111
    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@114
    move-result-object v0

    #@115
    check-cast v0, Landroid/content/ComponentName;

    #@117
    .line 170
    .local v0, _arg0:Landroid/content/ComponentName;
    :goto_117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11a
    move-result v8

    #@11b
    if-eqz v8, :cond_128

    #@11d
    move v1, v7

    #@11e
    .line 171
    .local v1, _arg1:Z
    :goto_11e
    invoke-virtual {p0, v0, v1}, Landroid/view/accessibility/IAccessibilityManager$Stub;->temporaryEnableAccessibilityStateUntilKeyguardRemoved(Landroid/content/ComponentName;Z)V

    #@121
    .line 172
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@124
    goto/16 :goto_9

    #@126
    .line 167
    .end local v0           #_arg0:Landroid/content/ComponentName;
    .end local v1           #_arg1:Z
    :cond_126
    const/4 v0, 0x0

    #@127
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_117

    #@128
    :cond_128
    move v1, v6

    #@129
    .line 170
    goto :goto_11e

    #@12a
    .line 177
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :sswitch_12a
    const-string v8, "android.view.accessibility.IAccessibilityManager"

    #@12c
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12f
    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@132
    move-result v0

    #@133
    .line 181
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@136
    move-result v1

    #@137
    .line 183
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@13a
    move-result v2

    #@13b
    .line 185
    .local v2, _arg2:F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@13e
    move-result v3

    #@13f
    .line 186
    .local v3, _arg3:F
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/accessibility/IAccessibilityManager$Stub;->setColorConvert(IIFF)Z

    #@142
    move-result v4

    #@143
    .line 187
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@146
    .line 188
    if-eqz v4, :cond_149

    #@148
    move v6, v7

    #@149
    :cond_149
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@14c
    goto/16 :goto_9

    #@14e
    .line 44
    :sswitch_data_14e
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_53
        0x4 -> :sswitch_67
        0x5 -> :sswitch_7f
        0x6 -> :sswitch_90
        0x7 -> :sswitch_b5
        0x8 -> :sswitch_ca
        0x9 -> :sswitch_ef
        0xa -> :sswitch_104
        0xb -> :sswitch_12a
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
