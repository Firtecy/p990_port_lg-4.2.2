.class public final Landroid/view/accessibility/AccessibilityEvent;
.super Landroid/view/accessibility/AccessibilityRecord;
.source "AccessibilityEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = false

.field public static final INVALID_POSITION:I = -0x1

.field private static final MAX_POOL_SIZE:I = 0xa

.field public static final MAX_TEXT_LENGTH:I = 0x1f4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPES_ALL_MASK:I = -0x1

.field public static final TYPE_ANNOUNCEMENT:I = 0x4000

.field public static final TYPE_GESTURE_DETECTION_END:I = 0x80000

.field public static final TYPE_GESTURE_DETECTION_START:I = 0x40000

.field public static final TYPE_NOTIFICATION_STATE_CHANGED:I = 0x40

.field public static final TYPE_TOUCH_EXPLORATION_GESTURE_END:I = 0x400

.field public static final TYPE_TOUCH_EXPLORATION_GESTURE_START:I = 0x200

.field public static final TYPE_TOUCH_INTERACTION_END:I = 0x200000

.field public static final TYPE_TOUCH_INTERACTION_START:I = 0x100000

.field public static final TYPE_VIEW_ACCESSIBILITY_FOCUSED:I = 0x8000

.field public static final TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED:I = 0x10000

.field public static final TYPE_VIEW_CLICKED:I = 0x1

.field public static final TYPE_VIEW_FOCUSED:I = 0x8

.field public static final TYPE_VIEW_HOVER_ENTER:I = 0x80

.field public static final TYPE_VIEW_HOVER_EXIT:I = 0x100

.field public static final TYPE_VIEW_LONG_CLICKED:I = 0x2

.field public static final TYPE_VIEW_SCROLLED:I = 0x1000

.field public static final TYPE_VIEW_SELECTED:I = 0x4

.field public static final TYPE_VIEW_TEXT_CHANGED:I = 0x10

.field public static final TYPE_VIEW_TEXT_SELECTION_CHANGED:I = 0x2000

.field public static final TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY:I = 0x20000

.field public static final TYPE_WINDOW_CONTENT_CHANGED:I = 0x800

.field public static final TYPE_WINDOW_STATE_CHANGED:I = 0x20

.field private static sPool:Landroid/view/accessibility/AccessibilityEvent;

.field private static final sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field mAction:I

.field private mEventTime:J

.field private mEventType:I

.field private mIsInPool:Z

.field mMovementGranularity:I

.field private mNext:Landroid/view/accessibility/AccessibilityEvent;

.field private mPackageName:Ljava/lang/CharSequence;

.field private final mRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/accessibility/AccessibilityRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 689
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/accessibility/AccessibilityEvent;->sPoolLock:Ljava/lang/Object;

    #@7
    .line 1193
    new-instance v0, Landroid/view/accessibility/AccessibilityEvent$1;

    #@9
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityEvent$1;-><init>()V

    #@c
    sput-object v0, Landroid/view/accessibility/AccessibilityEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 706
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityRecord;-><init>()V

    #@3
    .line 701
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@a
    .line 707
    return-void
.end method

.method public static eventTypeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "eventType"

    #@0
    .prologue
    .line 1140
    sparse-switch p0, :sswitch_data_48

    #@3
    .line 1186
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 1142
    :sswitch_5
    const-string v0, "TYPE_VIEW_CLICKED"

    #@7
    goto :goto_4

    #@8
    .line 1144
    :sswitch_8
    const-string v0, "TYPE_VIEW_LONG_CLICKED"

    #@a
    goto :goto_4

    #@b
    .line 1146
    :sswitch_b
    const-string v0, "TYPE_VIEW_SELECTED"

    #@d
    goto :goto_4

    #@e
    .line 1148
    :sswitch_e
    const-string v0, "TYPE_VIEW_FOCUSED"

    #@10
    goto :goto_4

    #@11
    .line 1150
    :sswitch_11
    const-string v0, "TYPE_VIEW_TEXT_CHANGED"

    #@13
    goto :goto_4

    #@14
    .line 1152
    :sswitch_14
    const-string v0, "TYPE_WINDOW_STATE_CHANGED"

    #@16
    goto :goto_4

    #@17
    .line 1154
    :sswitch_17
    const-string v0, "TYPE_VIEW_HOVER_ENTER"

    #@19
    goto :goto_4

    #@1a
    .line 1156
    :sswitch_1a
    const-string v0, "TYPE_VIEW_HOVER_EXIT"

    #@1c
    goto :goto_4

    #@1d
    .line 1158
    :sswitch_1d
    const-string v0, "TYPE_NOTIFICATION_STATE_CHANGED"

    #@1f
    goto :goto_4

    #@20
    .line 1160
    :sswitch_20
    const-string v0, "TYPE_TOUCH_EXPLORATION_GESTURE_START"

    #@22
    goto :goto_4

    #@23
    .line 1162
    :sswitch_23
    const-string v0, "TYPE_TOUCH_EXPLORATION_GESTURE_END"

    #@25
    goto :goto_4

    #@26
    .line 1164
    :sswitch_26
    const-string v0, "TYPE_WINDOW_CONTENT_CHANGED"

    #@28
    goto :goto_4

    #@29
    .line 1166
    :sswitch_29
    const-string v0, "TYPE_VIEW_TEXT_SELECTION_CHANGED"

    #@2b
    goto :goto_4

    #@2c
    .line 1168
    :sswitch_2c
    const-string v0, "TYPE_VIEW_SCROLLED"

    #@2e
    goto :goto_4

    #@2f
    .line 1170
    :sswitch_2f
    const-string v0, "TYPE_ANNOUNCEMENT"

    #@31
    goto :goto_4

    #@32
    .line 1172
    :sswitch_32
    const-string v0, "TYPE_VIEW_ACCESSIBILITY_FOCUSED"

    #@34
    goto :goto_4

    #@35
    .line 1174
    :sswitch_35
    const-string v0, "TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED"

    #@37
    goto :goto_4

    #@38
    .line 1176
    :sswitch_38
    const-string v0, "TYPE_CURRENT_AT_GRANULARITY_MOVEMENT_CHANGED"

    #@3a
    goto :goto_4

    #@3b
    .line 1178
    :sswitch_3b
    const-string v0, "TYPE_GESTURE_DETECTION_START"

    #@3d
    goto :goto_4

    #@3e
    .line 1180
    :sswitch_3e
    const-string v0, "TYPE_GESTURE_DETECTION_END"

    #@40
    goto :goto_4

    #@41
    .line 1182
    :sswitch_41
    const-string v0, "TYPE_TOUCH_INTERACTION_START"

    #@43
    goto :goto_4

    #@44
    .line 1184
    :sswitch_44
    const-string v0, "TYPE_TOUCH_INTERACTION_END"

    #@46
    goto :goto_4

    #@47
    .line 1140
    nop

    #@48
    :sswitch_data_48
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_8
        0x4 -> :sswitch_b
        0x8 -> :sswitch_e
        0x10 -> :sswitch_11
        0x20 -> :sswitch_14
        0x40 -> :sswitch_1d
        0x80 -> :sswitch_17
        0x100 -> :sswitch_1a
        0x200 -> :sswitch_20
        0x400 -> :sswitch_23
        0x800 -> :sswitch_26
        0x1000 -> :sswitch_2c
        0x2000 -> :sswitch_29
        0x4000 -> :sswitch_2f
        0x8000 -> :sswitch_32
        0x10000 -> :sswitch_35
        0x20000 -> :sswitch_38
        0x40000 -> :sswitch_3b
        0x80000 -> :sswitch_3e
        0x100000 -> :sswitch_41
        0x200000 -> :sswitch_44
    .end sparse-switch
.end method

.method public static obtain()Landroid/view/accessibility/AccessibilityEvent;
    .registers 3

    #@0
    .prologue
    .line 919
    sget-object v2, Landroid/view/accessibility/AccessibilityEvent;->sPoolLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 920
    :try_start_3
    sget-object v1, Landroid/view/accessibility/AccessibilityEvent;->sPool:Landroid/view/accessibility/AccessibilityEvent;

    #@5
    if-eqz v1, :cond_1d

    #@7
    .line 921
    sget-object v0, Landroid/view/accessibility/AccessibilityEvent;->sPool:Landroid/view/accessibility/AccessibilityEvent;

    #@9
    .line 922
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    sget-object v1, Landroid/view/accessibility/AccessibilityEvent;->sPool:Landroid/view/accessibility/AccessibilityEvent;

    #@b
    iget-object v1, v1, Landroid/view/accessibility/AccessibilityEvent;->mNext:Landroid/view/accessibility/AccessibilityEvent;

    #@d
    sput-object v1, Landroid/view/accessibility/AccessibilityEvent;->sPool:Landroid/view/accessibility/AccessibilityEvent;

    #@f
    .line 923
    sget v1, Landroid/view/accessibility/AccessibilityEvent;->sPoolSize:I

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    sput v1, Landroid/view/accessibility/AccessibilityEvent;->sPoolSize:I

    #@15
    .line 924
    const/4 v1, 0x0

    #@16
    iput-object v1, v0, Landroid/view/accessibility/AccessibilityEvent;->mNext:Landroid/view/accessibility/AccessibilityEvent;

    #@18
    .line 925
    const/4 v1, 0x0

    #@19
    iput-boolean v1, v0, Landroid/view/accessibility/AccessibilityEvent;->mIsInPool:Z

    #@1b
    .line 926
    monitor-exit v2

    #@1c
    .line 928
    .end local v0           #event:Landroid/view/accessibility/AccessibilityEvent;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    new-instance v0, Landroid/view/accessibility/AccessibilityEvent;

    #@1f
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityEvent;-><init>()V

    #@22
    monitor-exit v2

    #@23
    goto :goto_1c

    #@24
    .line 929
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public static obtain(I)Landroid/view/accessibility/AccessibilityEvent;
    .registers 2
    .parameter "eventType"

    #@0
    .prologue
    .line 885
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    #@3
    move-result-object v0

    #@4
    .line 886
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    #@7
    .line 887
    return-object v0
.end method

.method public static obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 899
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    #@3
    move-result-object v0

    #@4
    .line 900
    .local v0, eventClone:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityEvent;->init(Landroid/view/accessibility/AccessibilityEvent;)V

    #@7
    .line 902
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v4

    #@d
    .line 903
    .local v4, recordCount:I
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v4, :cond_24

    #@10
    .line 904
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/view/accessibility/AccessibilityRecord;

    #@18
    .line 905
    .local v2, record:Landroid/view/accessibility/AccessibilityRecord;
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityRecord;->obtain(Landroid/view/accessibility/AccessibilityRecord;)Landroid/view/accessibility/AccessibilityRecord;

    #@1b
    move-result-object v3

    #@1c
    .line 906
    .local v3, recordClone:Landroid/view/accessibility/AccessibilityRecord;
    iget-object v5, v0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 903
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_e

    #@24
    .line 909
    .end local v2           #record:Landroid/view/accessibility/AccessibilityRecord;
    .end local v3           #recordClone:Landroid/view/accessibility/AccessibilityRecord;
    :cond_24
    return-object v0
.end method

.method private readAccessibilityRecordFromParcel(Landroid/view/accessibility/AccessibilityRecord;Landroid/os/Parcel;)V
    .registers 7
    .parameter "record"
    .parameter "parcel"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 1008
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5
    move-result v0

    #@6
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@8
    .line 1009
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v0

    #@c
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@e
    .line 1010
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v0

    #@12
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@14
    .line 1011
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@1a
    .line 1012
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v0

    #@1e
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@20
    .line 1013
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v0

    #@24
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@26
    .line 1014
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v0

    #@2a
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@2c
    .line 1015
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v0

    #@30
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@32
    .line 1016
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v0

    #@36
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@38
    .line 1017
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v0

    #@3c
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@3e
    .line 1018
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v0

    #@42
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@44
    .line 1019
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@46
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@49
    move-result-object v0

    #@4a
    check-cast v0, Ljava/lang/CharSequence;

    #@4c
    iput-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@4e
    .line 1020
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@50
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@53
    move-result-object v0

    #@54
    check-cast v0, Ljava/lang/CharSequence;

    #@56
    iput-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@58
    .line 1021
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@5a
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5d
    move-result-object v0

    #@5e
    check-cast v0, Ljava/lang/CharSequence;

    #@60
    iput-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@62
    .line 1022
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@65
    move-result-object v0

    #@66
    iput-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@68
    .line 1023
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@6a
    invoke-virtual {p2, v0, v2}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    #@6d
    .line 1024
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v0

    #@71
    iput v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@73
    .line 1025
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@76
    move-result-wide v2

    #@77
    iput-wide v2, p1, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@79
    .line 1026
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7c
    move-result v0

    #@7d
    if-ne v0, v1, :cond_83

    #@7f
    move v0, v1

    #@80
    :goto_80
    iput-boolean v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@82
    .line 1027
    return-void

    #@83
    .line 1026
    :cond_83
    const/4 v0, 0x0

    #@84
    goto :goto_80
.end method

.method private writeAccessibilityRecordToParcel(Landroid/view/accessibility/AccessibilityRecord;Landroid/os/Parcel;I)V
    .registers 6
    .parameter "record"
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 1059
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 1060
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 1061
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 1062
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1063
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@16
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 1064
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@1b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 1065
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@20
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 1066
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@25
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 1067
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@2a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 1068
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@2f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 1069
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@34
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 1070
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@39
    invoke-static {v0, p2, p3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@3c
    .line 1071
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@3e
    invoke-static {v0, p2, p3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@41
    .line 1072
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@43
    invoke-static {v0, p2, p3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@46
    .line 1073
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@48
    invoke-virtual {p2, v0, p3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@4b
    .line 1074
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@4d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    #@50
    .line 1075
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@52
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@55
    .line 1076
    iget-wide v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@57
    invoke-virtual {p2, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@5a
    .line 1077
    iget-boolean v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@5c
    if-eqz v0, :cond_63

    #@5e
    const/4 v0, 0x1

    #@5f
    :goto_5f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@62
    .line 1078
    return-void

    #@63
    .line 1077
    :cond_63
    const/4 v0, 0x0

    #@64
    goto :goto_5f
.end method


# virtual methods
.method public appendRecord(Landroid/view/accessibility/AccessibilityRecord;)V
    .registers 3
    .parameter "record"

    #@0
    .prologue
    .line 758
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->enforceNotSealed()V

    #@3
    .line 759
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8
    .line 760
    return-void
.end method

.method protected clear()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 963
    invoke-super {p0}, Landroid/view/accessibility/AccessibilityRecord;->clear()V

    #@4
    .line 964
    iput v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@6
    .line 965
    iput v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@8
    .line 966
    iput v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@a
    .line 967
    const/4 v1, 0x0

    #@b
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@d
    .line 968
    const-wide/16 v1, 0x0

    #@f
    iput-wide v1, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@11
    .line 969
    :goto_11
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_25

    #@19
    .line 970
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/view/accessibility/AccessibilityRecord;

    #@21
    .line 971
    .local v0, record:Landroid/view/accessibility/AccessibilityRecord;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityRecord;->recycle()V

    #@24
    goto :goto_11

    #@25
    .line 973
    .end local v0           #record:Landroid/view/accessibility/AccessibilityRecord;
    :cond_25
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 1084
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAction()I
    .registers 2

    #@0
    .prologue
    .line 874
    iget v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@2
    return v0
.end method

.method public getEventTime()J
    .registers 3

    #@0
    .prologue
    .line 799
    iget-wide v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@2
    return-wide v0
.end method

.method public getEventType()I
    .registers 2

    #@0
    .prologue
    .line 778
    iget v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@2
    return v0
.end method

.method public getMovementGranularity()I
    .registers 2

    #@0
    .prologue
    .line 853
    iget v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@2
    return v0
.end method

.method public getPackageName()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 820
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getRecord(I)Landroid/view/accessibility/AccessibilityRecord;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 769
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/accessibility/AccessibilityRecord;

    #@8
    return-object v0
.end method

.method public getRecordCount()I
    .registers 2

    #@0
    .prologue
    .line 747
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method init(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 715
    invoke-super {p0, p1}, Landroid/view/accessibility/AccessibilityRecord;->init(Landroid/view/accessibility/AccessibilityRecord;)V

    #@3
    .line 716
    iget v0, p1, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@5
    iput v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@7
    .line 717
    iget v0, p1, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@9
    iput v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@b
    .line 718
    iget v0, p1, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@d
    iput v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@f
    .line 719
    iget-wide v0, p1, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@11
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@13
    .line 720
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@15
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@17
    .line 721
    return-void
.end method

.method public initFromParcel(Landroid/os/Parcel;)V
    .registers 7
    .parameter "parcel"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 981
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v4

    #@5
    if-ne v4, v3, :cond_4e

    #@7
    :goto_7
    iput-boolean v3, p0, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@9
    .line 982
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v3

    #@d
    iput v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@f
    .line 983
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v3

    #@13
    iput v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@15
    .line 984
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v3

    #@19
    iput v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@1b
    .line 985
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v3, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Ljava/lang/CharSequence;

    #@23
    iput-object v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@25
    .line 986
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@28
    move-result-wide v3

    #@29
    iput-wide v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@2b
    .line 987
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2e
    move-result v3

    #@2f
    iput v3, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@31
    .line 988
    invoke-direct {p0, p0, p1}, Landroid/view/accessibility/AccessibilityEvent;->readAccessibilityRecordFromParcel(Landroid/view/accessibility/AccessibilityRecord;Landroid/os/Parcel;)V

    #@34
    .line 991
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v2

    #@38
    .line 992
    .local v2, recordCount:I
    const/4 v0, 0x0

    #@39
    .local v0, i:I
    :goto_39
    if-ge v0, v2, :cond_50

    #@3b
    .line 993
    invoke-static {}, Landroid/view/accessibility/AccessibilityRecord;->obtain()Landroid/view/accessibility/AccessibilityRecord;

    #@3e
    move-result-object v1

    #@3f
    .line 994
    .local v1, record:Landroid/view/accessibility/AccessibilityRecord;
    invoke-direct {p0, v1, p1}, Landroid/view/accessibility/AccessibilityEvent;->readAccessibilityRecordFromParcel(Landroid/view/accessibility/AccessibilityRecord;Landroid/os/Parcel;)V

    #@42
    .line 995
    iget v3, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@44
    iput v3, v1, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@46
    .line 996
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@48
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4b
    .line 992
    add-int/lit8 v0, v0, 0x1

    #@4d
    goto :goto_39

    #@4e
    .line 981
    .end local v0           #i:I
    .end local v1           #record:Landroid/view/accessibility/AccessibilityRecord;
    .end local v2           #recordCount:I
    :cond_4e
    const/4 v3, 0x0

    #@4f
    goto :goto_7

    #@50
    .line 998
    .restart local v0       #i:I
    .restart local v2       #recordCount:I
    :cond_50
    return-void
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 942
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mIsInPool:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 943
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Event already recycled!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 945
    :cond_c
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->clear()V

    #@f
    .line 946
    sget-object v1, Landroid/view/accessibility/AccessibilityEvent;->sPoolLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 947
    :try_start_12
    sget v0, Landroid/view/accessibility/AccessibilityEvent;->sPoolSize:I

    #@14
    const/16 v2, 0xa

    #@16
    if-gt v0, v2, :cond_27

    #@18
    .line 948
    sget-object v0, Landroid/view/accessibility/AccessibilityEvent;->sPool:Landroid/view/accessibility/AccessibilityEvent;

    #@1a
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mNext:Landroid/view/accessibility/AccessibilityEvent;

    #@1c
    .line 949
    sput-object p0, Landroid/view/accessibility/AccessibilityEvent;->sPool:Landroid/view/accessibility/AccessibilityEvent;

    #@1e
    .line 950
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Landroid/view/accessibility/AccessibilityEvent;->mIsInPool:Z

    #@21
    .line 951
    sget v0, Landroid/view/accessibility/AccessibilityEvent;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Landroid/view/accessibility/AccessibilityEvent;->sPoolSize:I

    #@27
    .line 953
    :cond_27
    monitor-exit v1

    #@28
    .line 954
    return-void

    #@29
    .line 953
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method public setAction(I)V
    .registers 2
    .parameter "action"

    #@0
    .prologue
    .line 864
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->enforceNotSealed()V

    #@3
    .line 865
    iput p1, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@5
    .line 866
    return-void
.end method

.method public setEventTime(J)V
    .registers 3
    .parameter "eventTime"

    #@0
    .prologue
    .line 810
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->enforceNotSealed()V

    #@3
    .line 811
    iput-wide p1, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@5
    .line 812
    return-void
.end method

.method public setEventType(I)V
    .registers 2
    .parameter "eventType"

    #@0
    .prologue
    .line 789
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->enforceNotSealed()V

    #@3
    .line 790
    iput p1, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@5
    .line 791
    return-void
.end method

.method public setMovementGranularity(I)V
    .registers 2
    .parameter "granularity"

    #@0
    .prologue
    .line 843
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->enforceNotSealed()V

    #@3
    .line 844
    iput p1, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@5
    .line 845
    return-void
.end method

.method public setPackageName(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 831
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->enforceNotSealed()V

    #@3
    .line 832
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@5
    .line 833
    return-void
.end method

.method public setSealed(Z)V
    .registers 6
    .parameter "sealed"

    #@0
    .prologue
    .line 732
    invoke-super {p0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setSealed(Z)V

    #@3
    .line 733
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@5
    .line 734
    .local v3, records:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityRecord;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@8
    move-result v2

    #@9
    .line 735
    .local v2, recordCount:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v2, :cond_18

    #@c
    .line 736
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/view/accessibility/AccessibilityRecord;

    #@12
    .line 737
    .local v1, record:Landroid/view/accessibility/AccessibilityRecord;
    invoke-virtual {v1, p1}, Landroid/view/accessibility/AccessibilityRecord;->setSealed(Z)V

    #@15
    .line 735
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_a

    #@18
    .line 739
    .end local v1           #record:Landroid/view/accessibility/AccessibilityRecord;
    :cond_18
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1089
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1090
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "EventType: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget v2, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@d
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->eventTypeToString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 1091
    const-string v1, "; EventTime: "

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget-wide v2, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@1c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    .line 1092
    const-string v1, "; PackageName: "

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2a
    .line 1093
    const-string v1, "; MovementGranularity: "

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    iget v2, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    .line 1094
    const-string v1, "; Action: "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    iget v2, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    .line 1095
    invoke-super {p0}, Landroid/view/accessibility/AccessibilityRecord;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    .line 1127
    const-string v1, "; recordCount: "

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getRecordCount()I

    #@50
    move-result v2

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    .line 1129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1033
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->isSealed()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_44

    #@7
    const/4 v3, 0x1

    #@8
    :goto_8
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 1034
    iget v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventType:I

    #@d
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 1035
    iget v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mMovementGranularity:I

    #@12
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 1036
    iget v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mAction:I

    #@17
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 1037
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mPackageName:Ljava/lang/CharSequence;

    #@1c
    invoke-static {v3, p1, v4}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@1f
    .line 1038
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mEventTime:J

    #@21
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@24
    .line 1039
    iget v3, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@26
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 1040
    invoke-direct {p0, p0, p1, p2}, Landroid/view/accessibility/AccessibilityEvent;->writeAccessibilityRecordToParcel(Landroid/view/accessibility/AccessibilityRecord;Landroid/os/Parcel;I)V

    #@2c
    .line 1043
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getRecordCount()I

    #@2f
    move-result v2

    #@30
    .line 1044
    .local v2, recordCount:I
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 1045
    const/4 v0, 0x0

    #@34
    .local v0, i:I
    :goto_34
    if-ge v0, v2, :cond_46

    #@36
    .line 1046
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityEvent;->mRecords:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v1

    #@3c
    check-cast v1, Landroid/view/accessibility/AccessibilityRecord;

    #@3e
    .line 1047
    .local v1, record:Landroid/view/accessibility/AccessibilityRecord;
    invoke-direct {p0, v1, p1, p2}, Landroid/view/accessibility/AccessibilityEvent;->writeAccessibilityRecordToParcel(Landroid/view/accessibility/AccessibilityRecord;Landroid/os/Parcel;I)V

    #@41
    .line 1045
    add-int/lit8 v0, v0, 0x1

    #@43
    goto :goto_34

    #@44
    .end local v0           #i:I
    .end local v1           #record:Landroid/view/accessibility/AccessibilityRecord;
    .end local v2           #recordCount:I
    :cond_44
    move v3, v4

    #@45
    .line 1033
    goto :goto_8

    #@46
    .line 1049
    .restart local v0       #i:I
    .restart local v2       #recordCount:I
    :cond_46
    return-void
.end method
