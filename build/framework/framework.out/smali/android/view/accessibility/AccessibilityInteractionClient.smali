.class public final Landroid/view/accessibility/AccessibilityInteractionClient;
.super Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;
.source "AccessibilityInteractionClient.java"


# static fields
.field private static final CHECK_INTEGRITY:Z = true

.field private static final DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "AccessibilityInteractionClient"

.field public static final NO_ID:I = -0x1

.field private static final TIMEOUT_INTERACTION_MILLIS:J = 0x1388L

.field private static final sAccessibilityNodeInfoCache:Landroid/view/accessibility/AccessibilityNodeInfoCache;

.field private static final sClients:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/view/accessibility/AccessibilityInteractionClient;",
            ">;"
        }
    .end annotation
.end field

.field private static final sConnectionCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/accessibilityservice/IAccessibilityServiceConnection;",
            ">;"
        }
    .end annotation
.end field

.field private static final sStaticLock:Ljava/lang/Object;


# instance fields
.field private mFindAccessibilityNodeInfoResult:Landroid/view/accessibility/AccessibilityNodeInfo;

.field private mFindAccessibilityNodeInfosResult:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mInstanceLock:Ljava/lang/Object;

.field private volatile mInteractionId:I

.field private final mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mPerformAccessibilityActionResult:Z

.field private mSameThreadMessage:Landroid/os/Message;

.field private final mTempBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 86
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sStaticLock:Ljava/lang/Object;

    #@7
    .line 88
    new-instance v0, Landroid/util/LongSparseArray;

    #@9
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@c
    sput-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sClients:Landroid/util/LongSparseArray;

    #@e
    .line 108
    new-instance v0, Landroid/util/SparseArray;

    #@10
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@13
    sput-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@15
    .line 113
    new-instance v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@17
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityNodeInfoCache;-><init>()V

    #@1a
    sput-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sAccessibilityNodeInfoCache:Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@1c
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 143
    invoke-direct {p0}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;-><init>()V

    #@3
    .line 91
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@a
    .line 93
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@11
    .line 95
    const/4 v0, -0x1

    #@12
    iput v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@14
    .line 105
    new-instance v0, Landroid/graphics/Rect;

    #@16
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@19
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mTempBounds:Landroid/graphics/Rect;

    #@1b
    .line 145
    return-void
.end method

.method private applyCompatibilityScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;F)V
    .registers 5
    .parameter "info"
    .parameter "scale"

    #@0
    .prologue
    .line 613
    const/high16 v1, 0x3f80

    #@2
    cmpl-float v1, p2, v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 624
    :goto_6
    return-void

    #@7
    .line 616
    :cond_7
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mTempBounds:Landroid/graphics/Rect;

    #@9
    .line 617
    .local v0, bounds:Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInParent(Landroid/graphics/Rect;)V

    #@c
    .line 618
    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->scale(F)V

    #@f
    .line 619
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    #@12
    .line 621
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    #@15
    .line 622
    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->scale(F)V

    #@18
    .line 623
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    #@1b
    goto :goto_6
.end method

.method private checkFindAccessibilityNodeInfoResultIntegrity(Ljava/util/List;)V
    .registers 21
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 716
    .local p1, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@3
    move-result v15

    #@4
    if-nez v15, :cond_7

    #@6
    .line 760
    :cond_6
    :goto_6
    return-void

    #@7
    .line 720
    :cond_7
    const/4 v15, 0x0

    #@8
    move-object/from16 v0, p1

    #@a
    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v13

    #@e
    check-cast v13, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@10
    .line 721
    .local v13, root:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@13
    move-result v11

    #@14
    .line 722
    .local v11, infoCount:I
    const/4 v10, 0x1

    #@15
    .local v10, i:I
    :goto_15
    if-ge v10, v11, :cond_35

    #@17
    .line 723
    move v12, v10

    #@18
    .local v12, j:I
    :goto_18
    if-ge v12, v11, :cond_2f

    #@1a
    .line 724
    move-object/from16 v0, p1

    #@1c
    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@22
    .line 725
    .local v1, candidate:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v13}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@25
    move-result-wide v15

    #@26
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@29
    move-result-wide v17

    #@2a
    cmp-long v15, v15, v17

    #@2c
    if-nez v15, :cond_32

    #@2e
    .line 726
    move-object v13, v1

    #@2f
    .line 722
    .end local v1           #candidate:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_2f
    add-int/lit8 v10, v10, 0x1

    #@31
    goto :goto_15

    #@32
    .line 723
    .restart local v1       #candidate:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_32
    add-int/lit8 v12, v12, 0x1

    #@34
    goto :goto_18

    #@35
    .line 731
    .end local v1           #candidate:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v12           #j:I
    :cond_35
    if-nez v13, :cond_3e

    #@37
    .line 732
    const-string v15, "AccessibilityInteractionClient"

    #@39
    const-string v16, "No root."

    #@3b
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 735
    :cond_3e
    new-instance v14, Ljava/util/HashSet;

    #@40
    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    #@43
    .line 736
    .local v14, seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    new-instance v9, Ljava/util/LinkedList;

    #@45
    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    #@48
    .line 737
    .local v9, fringe:Ljava/util/Queue;,"Ljava/util/Queue<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-interface {v9, v13}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    #@4b
    .line 738
    :cond_4b
    invoke-interface {v9}, Ljava/util/Queue;->isEmpty()Z

    #@4e
    move-result v15

    #@4f
    if-nez v15, :cond_90

    #@51
    .line 739
    invoke-interface {v9}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    #@54
    move-result-object v7

    #@55
    check-cast v7, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@57
    .line 740
    .local v7, current:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v14, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5a
    move-result v15

    #@5b
    if-nez v15, :cond_65

    #@5d
    .line 741
    const-string v15, "AccessibilityInteractionClient"

    #@5f
    const-string v16, "Duplicate node."

    #@61
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_6

    #@65
    .line 744
    :cond_65
    invoke-virtual {v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@68
    move-result-object v6

    #@69
    .line 745
    .local v6, childIds:Landroid/util/SparseLongArray;
    invoke-virtual {v6}, Landroid/util/SparseLongArray;->size()I

    #@6c
    move-result v3

    #@6d
    .line 746
    .local v3, childCount:I
    const/4 v10, 0x0

    #@6e
    :goto_6e
    if-ge v10, v3, :cond_4b

    #@70
    .line 747
    invoke-virtual {v6, v10}, Landroid/util/SparseLongArray;->valueAt(I)J

    #@73
    move-result-wide v4

    #@74
    .line 748
    .local v4, childId:J
    const/4 v12, 0x0

    #@75
    .restart local v12       #j:I
    :goto_75
    if-ge v12, v11, :cond_8d

    #@77
    .line 749
    move-object/from16 v0, p1

    #@79
    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@7c
    move-result-object v2

    #@7d
    check-cast v2, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@7f
    .line 750
    .local v2, child:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@82
    move-result-wide v15

    #@83
    cmp-long v15, v15, v4

    #@85
    if-nez v15, :cond_8a

    #@87
    .line 751
    invoke-interface {v9, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    #@8a
    .line 748
    :cond_8a
    add-int/lit8 v12, v12, 0x1

    #@8c
    goto :goto_75

    #@8d
    .line 746
    .end local v2           #child:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_8d
    add-int/lit8 v10, v10, 0x1

    #@8f
    goto :goto_6e

    #@90
    .line 756
    .end local v3           #childCount:I
    .end local v4           #childId:J
    .end local v6           #childIds:Landroid/util/SparseLongArray;
    .end local v7           #current:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v12           #j:I
    :cond_90
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@93
    move-result v15

    #@94
    invoke-virtual {v14}, Ljava/util/HashSet;->size()I

    #@97
    move-result v16

    #@98
    sub-int v8, v15, v16

    #@9a
    .line 757
    .local v8, disconnectedCount:I
    if-lez v8, :cond_6

    #@9c
    .line 758
    const-string v15, "AccessibilityInteractionClient"

    #@9e
    new-instance v16, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    move-object/from16 v0, v16

    #@a5
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v16

    #@a9
    const-string v17, " Disconnected nodes."

    #@ab
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v16

    #@af
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v16

    #@b3
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto/16 :goto_6
.end method

.method private clearResultLocked()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 566
    const/4 v0, -0x1

    #@2
    iput v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@4
    .line 567
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfoResult:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@6
    .line 568
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfosResult:Ljava/util/List;

    #@8
    .line 569
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mPerformAccessibilityActionResult:Z

    #@b
    .line 570
    return-void
.end method

.method private finalizeAndCacheAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;IF)V
    .registers 5
    .parameter "info"
    .parameter "connectionId"
    .parameter "windowScale"

    #@0
    .prologue
    .line 635
    if-eqz p1, :cond_11

    #@2
    .line 636
    invoke-direct {p0, p1, p3}, Landroid/view/accessibility/AccessibilityInteractionClient;->applyCompatibilityScaleIfNeeded(Landroid/view/accessibility/AccessibilityNodeInfo;F)V

    #@5
    .line 637
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setConnectionId(I)V

    #@8
    .line 638
    const/4 v0, 0x1

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSealed(Z)V

    #@c
    .line 639
    sget-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sAccessibilityNodeInfoCache:Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@e
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->add(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@11
    .line 641
    :cond_11
    return-void
.end method

.method private finalizeAndCacheAccessibilityNodeInfos(Ljava/util/List;IF)V
    .registers 7
    .parameter
    .parameter "connectionId"
    .parameter "windowScale"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;IF)V"
        }
    .end annotation

    #@0
    .prologue
    .line 652
    .local p1, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    if-eqz p1, :cond_15

    #@2
    .line 653
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@5
    move-result v2

    #@6
    .line 654
    .local v2, infosCount:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v2, :cond_15

    #@9
    .line 655
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@f
    .line 656
    .local v1, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {p0, v1, p2, p3}, Landroid/view/accessibility/AccessibilityInteractionClient;->finalizeAndCacheAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;IF)V

    #@12
    .line 654
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_7

    #@15
    .line 659
    .end local v0           #i:I
    .end local v1           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v2           #infosCount:I
    :cond_15
    return-void
.end method

.method private getFindAccessibilityNodeInfoResultAndClear(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 6
    .parameter "interactionId"

    #@0
    .prologue
    .line 462
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 463
    :try_start_3
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->waitForResultTimedLocked(I)Z

    #@6
    move-result v1

    #@7
    .line 464
    .local v1, success:Z
    if-eqz v1, :cond_10

    #@9
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfoResult:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@b
    .line 465
    .local v0, result:Landroid/view/accessibility/AccessibilityNodeInfo;
    :goto_b
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityInteractionClient;->clearResultLocked()V

    #@e
    .line 466
    monitor-exit v3

    #@f
    return-object v0

    #@10
    .line 464
    .end local v0           #result:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_b

    #@12
    .line 467
    .end local v1           #success:Z
    :catchall_12
    move-exception v2

    #@13
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v2
.end method

.method private getFindAccessibilityNodeInfosResultAndClear(I)Ljava/util/List;
    .registers 6
    .parameter "interactionId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 492
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 493
    :try_start_3
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->waitForResultTimedLocked(I)Z

    #@6
    move-result v1

    #@7
    .line 494
    .local v1, success:Z
    const/4 v0, 0x0

    #@8
    .line 495
    .local v0, result:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    if-eqz v1, :cond_18

    #@a
    .line 496
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfosResult:Ljava/util/List;

    #@c
    .line 500
    :goto_c
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityInteractionClient;->clearResultLocked()V

    #@f
    .line 501
    sget-boolean v2, Landroid/os/Build;->IS_DEBUGGABLE:Z

    #@11
    if-eqz v2, :cond_16

    #@13
    .line 502
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityInteractionClient;->checkFindAccessibilityNodeInfoResultIntegrity(Ljava/util/List;)V

    #@16
    .line 504
    :cond_16
    monitor-exit v3

    #@17
    return-object v0

    #@18
    .line 498
    :cond_18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@1b
    move-result-object v0

    #@1c
    goto :goto_c

    #@1d
    .line 505
    .end local v0           #result:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .end local v1           #success:Z
    :catchall_1d
    move-exception v2

    #@1e
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v2
.end method

.method public static getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;
    .registers 3

    #@0
    .prologue
    .line 120
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    #@7
    move-result-wide v0

    #@8
    .line 121
    .local v0, threadId:J
    invoke-static {v0, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;

    #@b
    move-result-object v2

    #@c
    return-object v2
.end method

.method public static getInstanceForThread(J)Landroid/view/accessibility/AccessibilityInteractionClient;
    .registers 5
    .parameter "threadId"

    #@0
    .prologue
    .line 133
    sget-object v2, Landroid/view/accessibility/AccessibilityInteractionClient;->sStaticLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 134
    :try_start_3
    sget-object v1, Landroid/view/accessibility/AccessibilityInteractionClient;->sClients:Landroid/util/LongSparseArray;

    #@5
    invoke-virtual {v1, p0, p1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/accessibility/AccessibilityInteractionClient;

    #@b
    .line 135
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    if-nez v0, :cond_17

    #@d
    .line 136
    new-instance v0, Landroid/view/accessibility/AccessibilityInteractionClient;

    #@f
    .end local v0           #client:Landroid/view/accessibility/AccessibilityInteractionClient;
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityInteractionClient;-><init>()V

    #@12
    .line 137
    .restart local v0       #client:Landroid/view/accessibility/AccessibilityInteractionClient;
    sget-object v1, Landroid/view/accessibility/AccessibilityInteractionClient;->sClients:Landroid/util/LongSparseArray;

    #@14
    invoke-virtual {v1, p0, p1, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@17
    .line 139
    :cond_17
    monitor-exit v2

    #@18
    return-object v0

    #@19
    .line 140
    .end local v0           #client:Landroid/view/accessibility/AccessibilityInteractionClient;
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method private getPerformAccessibilityActionResultAndClear(I)Z
    .registers 6
    .parameter "interactionId"

    #@0
    .prologue
    .line 541
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 542
    :try_start_3
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->waitForResultTimedLocked(I)Z

    #@6
    move-result v1

    #@7
    .line 543
    .local v1, success:Z
    if-eqz v1, :cond_10

    #@9
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mPerformAccessibilityActionResult:Z

    #@b
    .line 544
    .local v0, result:Z
    :goto_b
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityInteractionClient;->clearResultLocked()V

    #@e
    .line 545
    monitor-exit v3

    #@f
    return v0

    #@10
    .line 543
    .end local v0           #result:Z
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_b

    #@12
    .line 546
    .end local v1           #success:Z
    :catchall_12
    move-exception v2

    #@13
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v2
.end method

.method private getSameProcessMessageAndClear()Landroid/os/Message;
    .registers 4

    #@0
    .prologue
    .line 668
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 669
    :try_start_3
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mSameThreadMessage:Landroid/os/Message;

    #@5
    .line 670
    .local v0, result:Landroid/os/Message;
    const/4 v1, 0x0

    #@6
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mSameThreadMessage:Landroid/os/Message;

    #@8
    .line 671
    monitor-exit v2

    #@9
    return-object v0

    #@a
    .line 672
    .end local v0           #result:Landroid/os/Message;
    :catchall_a
    move-exception v1

    #@b
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v1
.end method

.method private waitForResultTimedLocked(I)Z
    .registers 12
    .parameter "interactionId"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 579
    const-wide/16 v5, 0x1388

    #@3
    .line 580
    .local v5, waitTimeMillis:J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v3

    #@7
    .line 583
    .local v3, startTimeMillis:J
    :goto_7
    :try_start_7
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityInteractionClient;->getSameProcessMessageAndClear()Landroid/os/Message;

    #@a
    move-result-object v2

    #@b
    .line 584
    .local v2, sameProcessMessage:Landroid/os/Message;
    if-eqz v2, :cond_14

    #@d
    .line 585
    invoke-virtual {v2}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8, v2}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@14
    .line 588
    :cond_14
    iget v8, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@16
    if-ne v8, p1, :cond_1a

    #@18
    .line 589
    const/4 v7, 0x1

    #@19
    .line 597
    :cond_19
    return v7

    #@1a
    .line 591
    :cond_1a
    iget v8, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@1c
    if-gt v8, p1, :cond_19

    #@1e
    .line 594
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@21
    move-result-wide v8

    #@22
    sub-long v0, v8, v3

    #@24
    .line 595
    .local v0, elapsedTimeMillis:J
    const-wide/16 v8, 0x1388

    #@26
    sub-long v5, v8, v0

    #@28
    .line 596
    const-wide/16 v8, 0x0

    #@2a
    cmp-long v8, v5, v8

    #@2c
    if-lez v8, :cond_19

    #@2e
    .line 599
    iget-object v8, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@30
    invoke-virtual {v8, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_33
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_33} :catch_34

    #@33
    goto :goto_7

    #@34
    .line 600
    .end local v0           #elapsedTimeMillis:J
    .end local v2           #sameProcessMessage:Landroid/os/Message;
    :catch_34
    move-exception v8

    #@35
    goto :goto_7
.end method


# virtual methods
.method public addConnection(ILandroid/accessibilityservice/IAccessibilityServiceConnection;)V
    .registers 5
    .parameter "connectionId"
    .parameter "connection"

    #@0
    .prologue
    .line 694
    sget-object v1, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 695
    :try_start_3
    sget-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@8
    .line 696
    monitor-exit v1

    #@9
    .line 697
    return-void

    #@a
    .line 696
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public clearCache()V
    .registers 2

    #@0
    .prologue
    .line 448
    sget-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sAccessibilityNodeInfoCache:Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@2
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clear()V

    #@5
    .line 449
    return-void
.end method

.method public findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 20
    .parameter "connectionId"
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "prefetchFlags"

    #@0
    .prologue
    .line 189
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@3
    move-result-object v2

    #@4
    .line 190
    .local v2, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v2, :cond_48

    #@6
    .line 191
    sget-object v3, Landroid/view/accessibility/AccessibilityInteractionClient;->sAccessibilityNodeInfoCache:Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@8
    move-wide/from16 v0, p3

    #@a
    invoke-virtual {v3, v0, v1}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->get(J)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@d
    move-result-object v11

    #@e
    .line 193
    .local v11, cachedInfo:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v11, :cond_11

    #@10
    .line 220
    .end local v2           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v11           #cachedInfo:Landroid/view/accessibility/AccessibilityNodeInfo;
    :goto_10
    return-object v11

    #@11
    .line 196
    .restart local v2       #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .restart local v11       #cachedInfo:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_11
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@13
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@16
    move-result v6

    #@17
    .line 197
    .local v6, interactionId:I
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    #@1e
    move-result-wide v9

    #@1f
    move/from16 v3, p2

    #@21
    move-wide/from16 v4, p3

    #@23
    move-object v7, p0

    #@24
    move/from16 v8, p5

    #@26
    invoke-interface/range {v2 .. v10}, Landroid/accessibilityservice/IAccessibilityServiceConnection;->findAccessibilityNodeInfoByAccessibilityId(IJILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IJ)F

    #@29
    move-result v13

    #@2a
    .line 201
    .local v13, windowScale:F
    const/4 v3, 0x0

    #@2b
    cmpl-float v3, v13, v3

    #@2d
    if-lez v3, :cond_48

    #@2f
    .line 202
    invoke-direct {p0, v6}, Landroid/view/accessibility/AccessibilityInteractionClient;->getFindAccessibilityNodeInfosResultAndClear(I)Ljava/util/List;

    #@32
    move-result-object v12

    #@33
    .line 204
    .local v12, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-direct {p0, v12, p1, v13}, Landroid/view/accessibility/AccessibilityInteractionClient;->finalizeAndCacheAccessibilityNodeInfos(Ljava/util/List;IF)V

    #@36
    .line 205
    if-eqz v12, :cond_48

    #@38
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    #@3b
    move-result v3

    #@3c
    if-nez v3, :cond_48

    #@3e
    .line 206
    const/4 v3, 0x0

    #@3f
    invoke-interface {v12, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v3

    #@43
    check-cast v3, Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_45
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_45} :catch_47

    #@45
    move-object v11, v3

    #@46
    goto :goto_10

    #@47
    .line 214
    .end local v2           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v6           #interactionId:I
    .end local v11           #cachedInfo:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v12           #infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .end local v13           #windowScale:F
    :catch_47
    move-exception v3

    #@48
    .line 220
    :cond_48
    const/4 v11, 0x0

    #@49
    goto :goto_10
.end method

.method public findAccessibilityNodeInfoByViewId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 17
    .parameter "connectionId"
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "viewId"

    #@0
    .prologue
    .line 242
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@3
    move-result-object v0

    #@4
    .line 243
    .local v0, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v0, :cond_2b

    #@6
    .line 244
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@b
    move-result v5

    #@c
    .line 245
    .local v5, interactionId:I
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    #@13
    move-result-wide v7

    #@14
    move v1, p2

    #@15
    move-wide v2, p3

    #@16
    move/from16 v4, p5

    #@18
    move-object v6, p0

    #@19
    invoke-interface/range {v0 .. v8}, Landroid/accessibilityservice/IAccessibilityServiceConnection;->findAccessibilityNodeInfoByViewId(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@1c
    move-result v10

    #@1d
    .line 250
    .local v10, windowScale:F
    const/4 v1, 0x0

    #@1e
    cmpl-float v1, v10, v1

    #@20
    if-lez v1, :cond_2b

    #@22
    .line 251
    invoke-direct {p0, v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->getFindAccessibilityNodeInfoResultAndClear(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@25
    move-result-object v9

    #@26
    .line 253
    .local v9, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {p0, v9, p1, v10}, Landroid/view/accessibility/AccessibilityInteractionClient;->finalizeAndCacheAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;IF)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_29} :catch_2a

    #@29
    .line 267
    .end local v0           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v5           #interactionId:I
    .end local v9           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v10           #windowScale:F
    :goto_29
    return-object v9

    #@2a
    .line 261
    :catch_2a
    move-exception v1

    #@2b
    .line 267
    :cond_2b
    const/4 v9, 0x0

    #@2c
    goto :goto_29
.end method

.method public findAccessibilityNodeInfosByText(IIJLjava/lang/String;)Ljava/util/List;
    .registers 17
    .parameter "connectionId"
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 290
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@3
    move-result-object v0

    #@4
    .line 291
    .local v0, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v0, :cond_2b

    #@6
    .line 292
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@b
    move-result v5

    #@c
    .line 293
    .local v5, interactionId:I
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    #@13
    move-result-wide v7

    #@14
    move v1, p2

    #@15
    move-wide v2, p3

    #@16
    move-object/from16 v4, p5

    #@18
    move-object v6, p0

    #@19
    invoke-interface/range {v0 .. v8}, Landroid/accessibilityservice/IAccessibilityServiceConnection;->findAccessibilityNodeInfosByText(IJLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@1c
    move-result v10

    #@1d
    .line 297
    .local v10, windowScale:F
    const/4 v1, 0x0

    #@1e
    cmpl-float v1, v10, v1

    #@20
    if-lez v1, :cond_2b

    #@22
    .line 298
    invoke-direct {p0, v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->getFindAccessibilityNodeInfosResultAndClear(I)Ljava/util/List;

    #@25
    move-result-object v9

    #@26
    .line 300
    .local v9, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    invoke-direct {p0, v9, p1, v10}, Landroid/view/accessibility/AccessibilityInteractionClient;->finalizeAndCacheAccessibilityNodeInfos(Ljava/util/List;IF)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_29} :catch_2a

    #@29
    .line 314
    .end local v0           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v5           #interactionId:I
    .end local v9           #infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .end local v10           #windowScale:F
    :goto_29
    return-object v9

    #@2a
    .line 308
    :catch_2a
    move-exception v1

    #@2b
    .line 314
    :cond_2b
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@2e
    move-result-object v9

    #@2f
    goto :goto_29
.end method

.method public findFocus(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 17
    .parameter "connectionId"
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "focusType"

    #@0
    .prologue
    .line 336
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@3
    move-result-object v0

    #@4
    .line 337
    .local v0, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v0, :cond_2b

    #@6
    .line 338
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@b
    move-result v5

    #@c
    .line 339
    .local v5, interactionId:I
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    #@13
    move-result-wide v7

    #@14
    move v1, p2

    #@15
    move-wide v2, p3

    #@16
    move/from16 v4, p5

    #@18
    move-object v6, p0

    #@19
    invoke-interface/range {v0 .. v8}, Landroid/accessibilityservice/IAccessibilityServiceConnection;->findFocus(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@1c
    move-result v10

    #@1d
    .line 343
    .local v10, windowScale:F
    const/4 v1, 0x0

    #@1e
    cmpl-float v1, v10, v1

    #@20
    if-lez v1, :cond_2b

    #@22
    .line 344
    invoke-direct {p0, v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->getFindAccessibilityNodeInfoResultAndClear(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@25
    move-result-object v9

    #@26
    .line 346
    .local v9, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {p0, v9, p1, v10}, Landroid/view/accessibility/AccessibilityInteractionClient;->finalizeAndCacheAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;IF)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_29} :catch_2a

    #@29
    .line 359
    .end local v0           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v5           #interactionId:I
    .end local v9           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v10           #windowScale:F
    :goto_29
    return-object v9

    #@2a
    .line 354
    :catch_2a
    move-exception v1

    #@2b
    .line 359
    :cond_2b
    const/4 v9, 0x0

    #@2c
    goto :goto_29
.end method

.method public focusSearch(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 17
    .parameter "connectionId"
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "direction"

    #@0
    .prologue
    .line 381
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@3
    move-result-object v0

    #@4
    .line 382
    .local v0, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v0, :cond_2b

    #@6
    .line 383
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@b
    move-result v5

    #@c
    .line 384
    .local v5, interactionId:I
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    #@13
    move-result-wide v7

    #@14
    move v1, p2

    #@15
    move-wide v2, p3

    #@16
    move/from16 v4, p5

    #@18
    move-object v6, p0

    #@19
    invoke-interface/range {v0 .. v8}, Landroid/accessibilityservice/IAccessibilityServiceConnection;->focusSearch(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@1c
    move-result v10

    #@1d
    .line 388
    .local v10, windowScale:F
    const/4 v1, 0x0

    #@1e
    cmpl-float v1, v10, v1

    #@20
    if-lez v1, :cond_2b

    #@22
    .line 389
    invoke-direct {p0, v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->getFindAccessibilityNodeInfoResultAndClear(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@25
    move-result-object v9

    #@26
    .line 391
    .local v9, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {p0, v9, p1, v10}, Landroid/view/accessibility/AccessibilityInteractionClient;->finalizeAndCacheAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;IF)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_29} :catch_2a

    #@29
    .line 404
    .end local v0           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v5           #interactionId:I
    .end local v9           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v10           #windowScale:F
    :goto_29
    return-object v9

    #@2a
    .line 399
    :catch_2a
    move-exception v1

    #@2b
    .line 404
    :cond_2b
    const/4 v9, 0x0

    #@2c
    goto :goto_29
.end method

.method public getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .registers 4
    .parameter "connectionId"

    #@0
    .prologue
    .line 682
    sget-object v1, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 683
    :try_start_3
    sget-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@b
    monitor-exit v1

    #@c
    return-object v0

    #@d
    .line 684
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getRootInActiveWindow(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 8
    .parameter "connectionId"

    #@0
    .prologue
    .line 167
    const/4 v2, -0x1

    #@1
    sget-wide v3, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@3
    const/4 v5, 0x4

    #@4
    move-object v0, p0

    #@5
    move v1, p1

    #@6
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 452
    sget-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sAccessibilityNodeInfoCache:Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@5
    .line 453
    return-void
.end method

.method public performAccessibilityAction(IIJILandroid/os/Bundle;)Z
    .registers 18
    .parameter "connectionId"
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 425
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityInteractionClient;->getConnection(I)Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@3
    move-result-object v0

    #@4
    .line 426
    .local v0, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v0, :cond_27

    #@6
    .line 427
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionIdCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@b
    move-result v6

    #@c
    .line 428
    .local v6, interactionId:I
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    #@13
    move-result-wide v8

    #@14
    move v1, p2

    #@15
    move-wide v2, p3

    #@16
    move/from16 v4, p5

    #@18
    move-object/from16 v5, p6

    #@1a
    move-object v7, p0

    #@1b
    invoke-interface/range {v0 .. v9}, Landroid/accessibilityservice/IAccessibilityServiceConnection;->performAccessibilityAction(IJILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)Z

    #@1e
    move-result v10

    #@1f
    .line 431
    .local v10, success:Z
    if-eqz v10, :cond_27

    #@21
    .line 432
    invoke-direct {p0, v6}, Landroid/view/accessibility/AccessibilityInteractionClient;->getPerformAccessibilityActionResultAndClear(I)Z
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_24} :catch_26

    #@24
    move-result v1

    #@25
    .line 444
    .end local v0           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v6           #interactionId:I
    .end local v10           #success:Z
    :goto_25
    return v1

    #@26
    .line 439
    :catch_26
    move-exception v1

    #@27
    .line 444
    :cond_27
    const/4 v1, 0x0

    #@28
    goto :goto_25
.end method

.method public removeConnection(I)V
    .registers 4
    .parameter "connectionId"

    #@0
    .prologue
    .line 705
    sget-object v1, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 706
    :try_start_3
    sget-object v0, Landroid/view/accessibility/AccessibilityInteractionClient;->sConnectionCache:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@8
    .line 707
    monitor-exit v1

    #@9
    .line 708
    return-void

    #@a
    .line 707
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public setFindAccessibilityNodeInfoResult(Landroid/view/accessibility/AccessibilityNodeInfo;I)V
    .registers 5
    .parameter "info"
    .parameter "interactionId"

    #@0
    .prologue
    .line 475
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 476
    :try_start_3
    iget v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@5
    if-le p2, v0, :cond_b

    #@7
    .line 477
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfoResult:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@9
    .line 478
    iput p2, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@b
    .line 480
    :cond_b
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@d
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@10
    .line 481
    monitor-exit v1

    #@11
    .line 482
    return-void

    #@12
    .line 481
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public setFindAccessibilityNodeInfosResult(Ljava/util/List;I)V
    .registers 7
    .parameter
    .parameter "interactionId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 513
    .local p1, infos:Ljava/util/List;,"Ljava/util/List<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 514
    :try_start_3
    iget v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@5
    if-le p2, v1, :cond_1f

    #@7
    .line 515
    if-eqz p1, :cond_2e

    #@9
    .line 518
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@c
    move-result v1

    #@d
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@10
    move-result v3

    #@11
    if-eq v1, v3, :cond_26

    #@13
    const/4 v0, 0x1

    #@14
    .line 519
    .local v0, isIpcCall:Z
    :goto_14
    if-nez v0, :cond_28

    #@16
    .line 520
    new-instance v1, Ljava/util/ArrayList;

    #@18
    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@1b
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfosResult:Ljava/util/List;

    #@1d
    .line 528
    .end local v0           #isIpcCall:Z
    :goto_1d
    iput p2, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@1f
    .line 530
    :cond_1f
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@21
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    #@24
    .line 531
    monitor-exit v2

    #@25
    .line 532
    return-void

    #@26
    .line 518
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_14

    #@28
    .line 523
    .restart local v0       #isIpcCall:Z
    :cond_28
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfosResult:Ljava/util/List;

    #@2a
    goto :goto_1d

    #@2b
    .line 531
    .end local v0           #isIpcCall:Z
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2b

    #@2d
    throw v1

    #@2e
    .line 526
    :cond_2e
    :try_start_2e
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@31
    move-result-object v1

    #@32
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mFindAccessibilityNodeInfosResult:Ljava/util/List;
    :try_end_34
    .catchall {:try_start_2e .. :try_end_34} :catchall_2b

    #@34
    goto :goto_1d
.end method

.method public setPerformAccessibilityActionResult(ZI)V
    .registers 5
    .parameter "succeeded"
    .parameter "interactionId"

    #@0
    .prologue
    .line 553
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 554
    :try_start_3
    iget v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@5
    if-le p2, v0, :cond_b

    #@7
    .line 555
    iput-boolean p1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mPerformAccessibilityActionResult:Z

    #@9
    .line 556
    iput p2, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInteractionId:I

    #@b
    .line 558
    :cond_b
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@d
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@10
    .line 559
    monitor-exit v1

    #@11
    .line 560
    return-void

    #@12
    .line 559
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public setSameThreadMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 154
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 155
    :try_start_3
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mSameThreadMessage:Landroid/os/Message;

    #@5
    .line 156
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityInteractionClient;->mInstanceLock:Ljava/lang/Object;

    #@7
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@a
    .line 157
    monitor-exit v1

    #@b
    .line 158
    return-void

    #@c
    .line 157
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method
