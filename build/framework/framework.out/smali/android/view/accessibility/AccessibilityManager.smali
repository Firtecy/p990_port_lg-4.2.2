.class public final Landroid/view/accessibility/AccessibilityManager;
.super Ljava/lang/Object;
.source "AccessibilityManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/accessibility/AccessibilityManager$MyHandler;,
        Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DO_SET_STATE:I = 0xa

.field private static final LOG_TAG:Ljava/lang/String; = "AccessibilityManager"

.field public static final STATE_FLAG_ACCESSIBILITY_ENABLED:I = 0x1

.field public static final STATE_FLAG_TOUCH_EXPLORATION_ENABLED:I = 0x2

.field private static sInstance:Landroid/view/accessibility/AccessibilityManager;

.field static final sInstanceSync:Ljava/lang/Object;


# instance fields
.field final mAccessibilityStateChangeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field final mClient:Landroid/view/accessibility/IAccessibilityManagerClient$Stub;

.field final mHandler:Landroid/os/Handler;

.field mIsEnabled:Z

.field mIsTouchExplorationEnabled:Z

.field final mService:Landroid/view/accessibility/IAccessibilityManager;

.field final mUserId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 75
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/accessibility/AccessibilityManager;->sInstanceSync:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/accessibility/IAccessibilityManager;I)V
    .registers 8
    .parameter "context"
    .parameter "service"
    .parameter "userId"

    #@0
    .prologue
    .line 194
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 91
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@5
    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@8
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mAccessibilityStateChangeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@a
    .line 110
    new-instance v2, Landroid/view/accessibility/AccessibilityManager$1;

    #@c
    invoke-direct {v2, p0}, Landroid/view/accessibility/AccessibilityManager$1;-><init>(Landroid/view/accessibility/AccessibilityManager;)V

    #@f
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mClient:Landroid/view/accessibility/IAccessibilityManagerClient$Stub;

    #@11
    .line 195
    new-instance v2, Landroid/view/accessibility/AccessibilityManager$MyHandler;

    #@13
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@16
    move-result-object v3

    #@17
    invoke-direct {v2, p0, v3}, Landroid/view/accessibility/AccessibilityManager$MyHandler;-><init>(Landroid/view/accessibility/AccessibilityManager;Landroid/os/Looper;)V

    #@1a
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mHandler:Landroid/os/Handler;

    #@1c
    .line 196
    iput-object p2, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@1e
    .line 197
    iput p3, p0, Landroid/view/accessibility/AccessibilityManager;->mUserId:I

    #@20
    .line 200
    :try_start_20
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@22
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityManager;->mClient:Landroid/view/accessibility/IAccessibilityManagerClient$Stub;

    #@24
    invoke-interface {v2, v3, p3}, Landroid/view/accessibility/IAccessibilityManager;->addClient(Landroid/view/accessibility/IAccessibilityManagerClient;I)I

    #@27
    move-result v1

    #@28
    .line 201
    .local v1, stateFlags:I
    invoke-direct {p0, v1}, Landroid/view/accessibility/AccessibilityManager;->setState(I)V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_2b} :catch_2c

    #@2b
    .line 205
    .end local v1           #stateFlags:I
    :goto_2b
    return-void

    #@2c
    .line 202
    :catch_2c
    move-exception v0

    #@2d
    .line 203
    .local v0, re:Landroid/os/RemoteException;
    const-string v2, "AccessibilityManager"

    #@2f
    const-string v3, "AccessibilityManagerService is dead"

    #@31
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    goto :goto_2b
.end method

.method static synthetic access$000(Landroid/view/accessibility/AccessibilityManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 64
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityManager;->setState(I)V

    #@3
    return-void
.end method

.method public static createAsSharedAcrossUsers(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 149
    sget-object v1, Landroid/view/accessibility/AccessibilityManager;->sInstanceSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 150
    :try_start_3
    sget-object v0, Landroid/view/accessibility/AccessibilityManager;->sInstance:Landroid/view/accessibility/AccessibilityManager;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 151
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v2, "AccessibilityManager already created."

    #@b
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 154
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 153
    :cond_12
    const/4 v0, -0x2

    #@13
    :try_start_13
    invoke-static {p0, v0}, Landroid/view/accessibility/AccessibilityManager;->createSingletonInstance(Landroid/content/Context;I)V

    #@16
    .line 154
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_13 .. :try_end_17} :catchall_f

    #@17
    .line 155
    return-void
.end method

.method private static createSingletonInstance(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "userId"

    #@0
    .prologue
    .line 180
    const-string v2, "accessibility"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    .line 181
    .local v0, iBinder:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/view/accessibility/IAccessibilityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;

    #@9
    move-result-object v1

    #@a
    .line 182
    .local v1, service:Landroid/view/accessibility/IAccessibilityManager;
    new-instance v2, Landroid/view/accessibility/AccessibilityManager;

    #@c
    invoke-direct {v2, p0, v1, p1}, Landroid/view/accessibility/AccessibilityManager;-><init>(Landroid/content/Context;Landroid/view/accessibility/IAccessibilityManager;I)V

    #@f
    sput-object v2, Landroid/view/accessibility/AccessibilityManager;->sInstance:Landroid/view/accessibility/AccessibilityManager;

    #@11
    .line 183
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 165
    sget-object v1, Landroid/view/accessibility/AccessibilityManager;->sInstanceSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 166
    :try_start_3
    sget-object v0, Landroid/view/accessibility/AccessibilityManager;->sInstance:Landroid/view/accessibility/AccessibilityManager;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 167
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@a
    move-result v0

    #@b
    invoke-static {p0, v0}, Landroid/view/accessibility/AccessibilityManager;->createSingletonInstance(Landroid/content/Context;I)V

    #@e
    .line 169
    :cond_e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_12

    #@f
    .line 170
    sget-object v0, Landroid/view/accessibility/AccessibilityManager;->sInstance:Landroid/view/accessibility/AccessibilityManager;

    #@11
    return-object v0

    #@12
    .line 169
    :catchall_12
    move-exception v0

    #@13
    :try_start_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method private notifyAccessibilityStateChanged()V
    .registers 5

    #@0
    .prologue
    .line 412
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mAccessibilityStateChangeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 413
    .local v1, listenerCount:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_19

    #@9
    .line 414
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mAccessibilityStateChangeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    #@11
    iget-boolean v3, p0, Landroid/view/accessibility/AccessibilityManager;->mIsEnabled:Z

    #@13
    invoke-interface {v2, v3}, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;->onAccessibilityStateChanged(Z)V

    #@16
    .line 413
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_7

    #@19
    .line 416
    :cond_19
    return-void
.end method

.method private setAccessibilityState(Z)V
    .registers 4
    .parameter "isEnabled"

    #@0
    .prologue
    .line 400
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mHandler:Landroid/os/Handler;

    #@2
    monitor-enter v1

    #@3
    .line 401
    :try_start_3
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityManager;->mIsEnabled:Z

    #@5
    if-eq p1, v0, :cond_c

    #@7
    .line 402
    iput-boolean p1, p0, Landroid/view/accessibility/AccessibilityManager;->mIsEnabled:Z

    #@9
    .line 403
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityManager;->notifyAccessibilityStateChanged()V

    #@c
    .line 405
    :cond_c
    monitor-exit v1

    #@d
    .line 406
    return-void

    #@e
    .line 405
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method private setState(I)V
    .registers 6
    .parameter "stateFlags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 389
    and-int/lit8 v3, p1, 0x1

    #@4
    if-eqz v3, :cond_11

    #@6
    move v0, v1

    #@7
    .line 390
    .local v0, accessibilityEnabled:Z
    :goto_7
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityManager;->setAccessibilityState(Z)V

    #@a
    .line 391
    and-int/lit8 v3, p1, 0x2

    #@c
    if-eqz v3, :cond_13

    #@e
    :goto_e
    iput-boolean v1, p0, Landroid/view/accessibility/AccessibilityManager;->mIsTouchExplorationEnabled:Z

    #@10
    .line 392
    return-void

    #@11
    .end local v0           #accessibilityEnabled:Z
    :cond_11
    move v0, v2

    #@12
    .line 389
    goto :goto_7

    #@13
    .restart local v0       #accessibilityEnabled:Z
    :cond_13
    move v1, v2

    #@14
    .line 391
    goto :goto_e
.end method


# virtual methods
.method public addAccessibilityInteractionConnection(Landroid/view/IWindow;Landroid/view/accessibility/IAccessibilityInteractionConnection;)I
    .registers 6
    .parameter "windowToken"
    .parameter "connection"

    #@0
    .prologue
    .line 428
    :try_start_0
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@2
    iget v2, p0, Landroid/view/accessibility/AccessibilityManager;->mUserId:I

    #@4
    invoke-interface {v1, p1, p2, v2}, Landroid/view/accessibility/IAccessibilityManager;->addAccessibilityInteractionConnection(Landroid/view/IWindow;Landroid/view/accessibility/IAccessibilityInteractionConnection;I)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 432
    :goto_8
    return v1

    #@9
    .line 429
    :catch_9
    move-exception v0

    #@a
    .line 430
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "AccessibilityManager"

    #@c
    const-string v2, "Error while adding an accessibility interaction connection. "

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 432
    const/4 v1, -0x1

    #@12
    goto :goto_8
.end method

.method public addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 369
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityManager;->mAccessibilityStateChangeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getAccessibilityServiceList()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ServiceInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 305
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    #@3
    move-result-object v3

    #@4
    .line 306
    .local v3, infos:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    new-instance v4, Ljava/util/ArrayList;

    #@6
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@9
    .line 307
    .local v4, services:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ServiceInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@c
    move-result v2

    #@d
    .line 308
    .local v2, infoCount:I
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    if-ge v0, v2, :cond_22

    #@10
    .line 309
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@16
    .line 310
    .local v1, info:Landroid/accessibilityservice/AccessibilityServiceInfo;
    invoke-virtual {v1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    #@19
    move-result-object v5

    #@1a
    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@1c
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1f
    .line 308
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_e

    #@22
    .line 312
    .end local v1           #info:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_22
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@25
    move-result-object v5

    #@26
    return-object v5
.end method

.method public getClient()Landroid/view/accessibility/IAccessibilityManagerClient;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityManager;->mClient:Landroid/view/accessibility/IAccessibilityManagerClient$Stub;

    #@2
    invoke-virtual {v0}, Landroid/view/accessibility/IAccessibilityManagerClient$Stub;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/accessibility/IAccessibilityManagerClient;

    #@8
    return-object v0
.end method

.method public getEnabledAccessibilityServiceList(I)Ljava/util/List;
    .registers 6
    .parameter "feedbackTypeFlags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 348
    const/4 v1, 0x0

    #@1
    .line 350
    .local v1, services:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    :try_start_1
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@3
    iget v3, p0, Landroid/view/accessibility/AccessibilityManager;->mUserId:I

    #@5
    invoke-interface {v2, p1, v3}, Landroid/view/accessibility/IAccessibilityManager;->getEnabledAccessibilityServiceList(II)Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_e

    #@8
    move-result-object v1

    #@9
    .line 357
    :goto_9
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@c
    move-result-object v2

    #@d
    return-object v2

    #@e
    .line 354
    :catch_e
    move-exception v0

    #@f
    .line 355
    .local v0, re:Landroid/os/RemoteException;
    const-string v2, "AccessibilityManager"

    #@11
    const-string v3, "Error while obtaining the installed AccessibilityServices. "

    #@13
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_9
.end method

.method public getInstalledAccessibilityServiceList()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 321
    const/4 v1, 0x0

    #@1
    .line 323
    .local v1, services:Ljava/util/List;,"Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    :try_start_1
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@3
    iget v3, p0, Landroid/view/accessibility/AccessibilityManager;->mUserId:I

    #@5
    invoke-interface {v2, v3}, Landroid/view/accessibility/IAccessibilityManager;->getInstalledAccessibilityServiceList(I)Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_8} :catch_e

    #@8
    move-result-object v1

    #@9
    .line 330
    :goto_9
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@c
    move-result-object v2

    #@d
    return-object v2

    #@e
    .line 327
    :catch_e
    move-exception v0

    #@f
    .line 328
    .local v0, re:Landroid/os/RemoteException;
    const-string v2, "AccessibilityManager"

    #@11
    const-string v3, "Error while obtaining the installed AccessibilityServices. "

    #@13
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_9
.end method

.method public interrupt()V
    .registers 4

    #@0
    .prologue
    .line 283
    iget-boolean v1, p0, Landroid/view/accessibility/AccessibilityManager;->mIsEnabled:Z

    #@2
    if-nez v1, :cond_c

    #@4
    .line 284
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string v2, "Accessibility off. Did you forget to check that?"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 287
    :cond_c
    :try_start_c
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@e
    iget v2, p0, Landroid/view/accessibility/AccessibilityManager;->mUserId:I

    #@10
    invoke-interface {v1, v2}, Landroid/view/accessibility/IAccessibilityManager;->interrupt(I)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_13} :catch_14

    #@13
    .line 294
    :goto_13
    return-void

    #@14
    .line 291
    :catch_14
    move-exception v0

    #@15
    .line 292
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "AccessibilityManager"

    #@17
    const-string v2, "Error while requesting interrupt from all services. "

    #@19
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_13
.end method

.method public isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 213
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mHandler:Landroid/os/Handler;

    #@2
    monitor-enter v1

    #@3
    .line 214
    :try_start_3
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityManager;->mIsEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 215
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public isTouchExplorationEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 224
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mHandler:Landroid/os/Handler;

    #@2
    monitor-enter v1

    #@3
    .line 225
    :try_start_3
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityManager;->mIsTouchExplorationEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 226
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public removeAccessibilityInteractionConnection(Landroid/view/IWindow;)V
    .registers 5
    .parameter "windowToken"

    #@0
    .prologue
    .line 443
    :try_start_0
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@2
    invoke-interface {v1, p1}, Landroid/view/accessibility/IAccessibilityManager;->removeAccessibilityInteractionConnection(Landroid/view/IWindow;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 447
    :goto_5
    return-void

    #@6
    .line 444
    :catch_6
    move-exception v0

    #@7
    .line 445
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "AccessibilityManager"

    #@9
    const-string v2, "Error while removing an accessibility interaction connection. "

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 380
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityManager;->mAccessibilityStateChangeListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    .line 255
    iget-boolean v4, p0, Landroid/view/accessibility/AccessibilityManager;->mIsEnabled:Z

    #@2
    if-nez v4, :cond_c

    #@4
    .line 256
    new-instance v4, Ljava/lang/IllegalStateException;

    #@6
    const-string v5, "Accessibility off. Did you forget to check that?"

    #@8
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v4

    #@c
    .line 258
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 260
    .local v0, doRecycle:Z
    :try_start_d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@10
    move-result-wide v4

    #@11
    invoke-virtual {p1, v4, v5}, Landroid/view/accessibility/AccessibilityEvent;->setEventTime(J)V

    #@14
    .line 264
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@17
    move-result-wide v1

    #@18
    .line 265
    .local v1, identityToken:J
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@1a
    iget v5, p0, Landroid/view/accessibility/AccessibilityManager;->mUserId:I

    #@1c
    invoke-interface {v4, p1, v5}, Landroid/view/accessibility/IAccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;I)Z

    #@1f
    move-result v0

    #@20
    .line 266
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_23
    .catchall {:try_start_d .. :try_end_23} :catchall_4e
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_23} :catch_29

    #@23
    .line 273
    if-eqz v0, :cond_28

    #@25
    .line 274
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@28
    .line 277
    .end local v1           #identityToken:J
    :cond_28
    :goto_28
    return-void

    #@29
    .line 270
    :catch_29
    move-exception v3

    #@2a
    .line 271
    .local v3, re:Landroid/os/RemoteException;
    :try_start_2a
    const-string v4, "AccessibilityManager"

    #@2c
    new-instance v5, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v6, "Error during sending "

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    const-string v6, " "

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_48
    .catchall {:try_start_2a .. :try_end_48} :catchall_4e

    #@48
    .line 273
    if-eqz v0, :cond_28

    #@4a
    .line 274
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@4d
    goto :goto_28

    #@4e
    .line 273
    .end local v3           #re:Landroid/os/RemoteException;
    :catchall_4e
    move-exception v4

    #@4f
    if-eqz v0, :cond_54

    #@51
    .line 274
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@54
    :cond_54
    throw v4
.end method

.method public setColorConvert(IIFF)Z
    .registers 8
    .parameter "hue"
    .parameter "intensity"
    .parameter "sat"
    .parameter "contrast"

    #@0
    .prologue
    .line 465
    :try_start_0
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityManager;->mService:Landroid/view/accessibility/IAccessibilityManager;

    #@2
    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/accessibility/IAccessibilityManager;->setColorConvert(IIFF)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 469
    :goto_6
    return v1

    #@7
    .line 466
    :catch_7
    move-exception v0

    #@8
    .line 467
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "AccessibilityManager"

    #@a
    const-string v2, "Error while converting color. "

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 469
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method
