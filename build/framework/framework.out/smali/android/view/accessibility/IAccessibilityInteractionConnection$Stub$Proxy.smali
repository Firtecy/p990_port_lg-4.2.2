.class Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;
.super Ljava/lang/Object;
.source "IAccessibilityInteractionConnection.java"

# interfaces
.implements Landroid/view/accessibility/IAccessibilityInteractionConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 183
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 184
    iput-object p1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 185
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public findAccessibilityNodeInfoByAccessibilityId(JILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 14
    .parameter "accessibilityNodeId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 196
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 198
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 199
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@d
    .line 200
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 201
    if-eqz p4, :cond_16

    #@12
    invoke-interface {p4}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@15
    move-result-object v1

    #@16
    :cond_16
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@19
    .line 202
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 203
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 204
    invoke-virtual {v0, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@22
    .line 205
    iget-object v1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/4 v2, 0x1

    #@25
    const/4 v3, 0x0

    #@26
    const/4 v4, 0x1

    #@27
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2a
    .catchall {:try_start_5 .. :try_end_2a} :catchall_2e

    #@2a
    .line 208
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 210
    return-void

    #@2e
    .line 208
    :catchall_2e
    move-exception v1

    #@2f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v1
.end method

.method public findAccessibilityNodeInfoByViewId(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 15
    .parameter "accessibilityNodeId"
    .parameter "viewId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 213
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 215
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 216
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@d
    .line 217
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 218
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 219
    if-eqz p5, :cond_19

    #@15
    invoke-interface {p5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@18
    move-result-object v1

    #@19
    :cond_19
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1c
    .line 220
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 221
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 222
    invoke-virtual {v0, p8, p9}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 223
    iget-object v1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v2, 0x2

    #@28
    const/4 v3, 0x0

    #@29
    const/4 v4, 0x1

    #@2a
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2d
    .catchall {:try_start_5 .. :try_end_2d} :catchall_31

    #@2d
    .line 226
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 228
    return-void

    #@31
    .line 226
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1
.end method

.method public findAccessibilityNodeInfosByText(JLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 15
    .parameter "accessibilityNodeId"
    .parameter "text"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 233
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 234
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@d
    .line 235
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 236
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 237
    if-eqz p5, :cond_19

    #@15
    invoke-interface {p5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@18
    move-result-object v1

    #@19
    :cond_19
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1c
    .line 238
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 239
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 240
    invoke-virtual {v0, p8, p9}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 241
    iget-object v1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v2, 0x3

    #@28
    const/4 v3, 0x0

    #@29
    const/4 v4, 0x1

    #@2a
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2d
    .catchall {:try_start_5 .. :try_end_2d} :catchall_31

    #@2d
    .line 244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 246
    return-void

    #@31
    .line 244
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1
.end method

.method public findFocus(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 15
    .parameter "accessibilityNodeId"
    .parameter "focusType"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 251
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 252
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@d
    .line 253
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 254
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 255
    if-eqz p5, :cond_19

    #@15
    invoke-interface {p5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@18
    move-result-object v1

    #@19
    :cond_19
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1c
    .line 256
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 257
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 258
    invoke-virtual {v0, p8, p9}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 259
    iget-object v1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v2, 0x4

    #@28
    const/4 v3, 0x0

    #@29
    const/4 v4, 0x1

    #@2a
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2d
    .catchall {:try_start_5 .. :try_end_2d} :catchall_31

    #@2d
    .line 262
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 264
    return-void

    #@31
    .line 262
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1
.end method

.method public focusSearch(JIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 15
    .parameter "accessibilityNodeId"
    .parameter "direction"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 267
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 269
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 270
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@d
    .line 271
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 272
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 273
    if-eqz p5, :cond_19

    #@15
    invoke-interface {p5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@18
    move-result-object v1

    #@19
    :cond_19
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1c
    .line 274
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 275
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 276
    invoke-virtual {v0, p8, p9}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 277
    iget-object v1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v2, 0x5

    #@28
    const/4 v3, 0x0

    #@29
    const/4 v4, 0x1

    #@2a
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2d
    .catchall {:try_start_5 .. :try_end_2d} :catchall_31

    #@2d
    .line 280
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 282
    return-void

    #@31
    .line 280
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 192
    const-string v0, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@2
    return-object v0
.end method

.method public performAccessibilityAction(JILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IIJ)V
    .registers 16
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "interrogatingPid"
    .parameter "interrogatingTid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 285
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 287
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.view.accessibility.IAccessibilityInteractionConnection"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 288
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    #@d
    .line 289
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 290
    if-eqz p4, :cond_3b

    #@12
    .line 291
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 292
    const/4 v2, 0x0

    #@17
    invoke-virtual {p4, v0, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 297
    :goto_1a
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 298
    if-eqz p6, :cond_23

    #@1f
    invoke-interface {p6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@22
    move-result-object v1

    #@23
    :cond_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@26
    .line 299
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 300
    invoke-virtual {v0, p8}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 301
    invoke-virtual {v0, p9, p10}, Landroid/os/Parcel;->writeLong(J)V

    #@2f
    .line 302
    iget-object v1, p0, Landroid/view/accessibility/IAccessibilityInteractionConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@31
    const/4 v2, 0x6

    #@32
    const/4 v3, 0x0

    #@33
    const/4 v4, 0x1

    #@34
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_37
    .catchall {:try_start_5 .. :try_end_37} :catchall_40

    #@37
    .line 305
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 307
    return-void

    #@3b
    .line 295
    :cond_3b
    const/4 v2, 0x0

    #@3c
    :try_start_3c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_40

    #@3f
    goto :goto_1a

    #@40
    .line 305
    :catchall_40
    move-exception v1

    #@41
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v1
.end method
