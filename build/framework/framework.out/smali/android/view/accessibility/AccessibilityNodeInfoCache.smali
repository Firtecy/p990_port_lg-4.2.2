.class public Landroid/view/accessibility/AccessibilityNodeInfoCache;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoCache.java"


# static fields
.field private static final CHECK_INTEGRITY:Z = true

.field private static final DEBUG:Z = false

.field private static final ENABLED:Z = true

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mCacheImpl:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mWindowId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    const-class v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->LOG_TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@a
    .line 54
    new-instance v0, Landroid/util/LongSparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@11
    .line 58
    return-void
.end method

.method private checkIntegrity()V
    .registers 24

    #@0
    .prologue
    .line 268
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@4
    move-object/from16 v20, v0

    #@6
    monitor-enter v20

    #@7
    .line 270
    :try_start_7
    move-object/from16 v0, p0

    #@9
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@b
    move-object/from16 v19, v0

    #@d
    invoke-virtual/range {v19 .. v19}, Landroid/util/LongSparseArray;->size()I

    #@10
    move-result v19

    #@11
    if-gtz v19, :cond_15

    #@13
    .line 271
    monitor-exit v20

    #@14
    .line 342
    :goto_14
    return-void

    #@15
    .line 276
    :cond_15
    move-object/from16 v0, p0

    #@17
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@19
    move-object/from16 v19, v0

    #@1b
    const/16 v21, 0x0

    #@1d
    move-object/from16 v0, v19

    #@1f
    move/from16 v1, v21

    #@21
    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@24
    move-result-object v16

    #@25
    check-cast v16, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@27
    .line 277
    .local v16, root:Landroid/view/accessibility/AccessibilityNodeInfo;
    move-object/from16 v15, v16

    #@29
    .line 278
    .local v15, parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    :goto_29
    if-eqz v15, :cond_42

    #@2b
    .line 279
    move-object/from16 v16, v15

    #@2d
    .line 280
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@31
    move-object/from16 v19, v0

    #@33
    invoke-virtual {v15}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@36
    move-result-wide v21

    #@37
    move-object/from16 v0, v19

    #@39
    move-wide/from16 v1, v21

    #@3b
    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@3e
    move-result-object v15

    #@3f
    .end local v15           #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    check-cast v15, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@41
    .restart local v15       #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    goto :goto_29

    #@42
    .line 284
    :cond_42
    invoke-virtual/range {v16 .. v16}, Landroid/view/accessibility/AccessibilityNodeInfo;->getWindowId()I

    #@45
    move-result v18

    #@46
    .line 285
    .local v18, windowId:I
    const/4 v3, 0x0

    #@47
    .line 286
    .local v3, accessFocus:Landroid/view/accessibility/AccessibilityNodeInfo;
    const/4 v14, 0x0

    #@48
    .line 287
    .local v14, inputFocus:Landroid/view/accessibility/AccessibilityNodeInfo;
    new-instance v17, Ljava/util/HashSet;

    #@4a
    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    #@4d
    .line 288
    .local v17, seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    new-instance v11, Ljava/util/LinkedList;

    #@4f
    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    #@52
    .line 289
    .local v11, fringe:Ljava/util/Queue;,"Ljava/util/Queue<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    move-object/from16 v0, v16

    #@54
    invoke-interface {v11, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    #@57
    .line 291
    :cond_57
    invoke-interface {v11}, Ljava/util/Queue;->isEmpty()Z

    #@5a
    move-result v19

    #@5b
    if-nez v19, :cond_103

    #@5d
    .line 292
    invoke-interface {v11}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    #@60
    move-result-object v10

    #@61
    check-cast v10, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@63
    .line 294
    .local v10, current:Landroid/view/accessibility/AccessibilityNodeInfo;
    move-object/from16 v0, v17

    #@65
    invoke-virtual {v0, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@68
    move-result v19

    #@69
    if-nez v19, :cond_8e

    #@6b
    .line 295
    sget-object v19, Landroid/view/accessibility/AccessibilityNodeInfoCache;->LOG_TAG:Ljava/lang/String;

    #@6d
    new-instance v21, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v22, "Duplicate node: "

    #@74
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v21

    #@78
    move-object/from16 v0, v21

    #@7a
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v21

    #@7e
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v21

    #@82
    move-object/from16 v0, v19

    #@84
    move-object/from16 v1, v21

    #@86
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 296
    monitor-exit v20

    #@8a
    goto :goto_14

    #@8b
    .line 341
    .end local v3           #accessFocus:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v10           #current:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v11           #fringe:Ljava/util/Queue;,"Ljava/util/Queue<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .end local v14           #inputFocus:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v15           #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v16           #root:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v17           #seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .end local v18           #windowId:I
    :catchall_8b
    move-exception v19

    #@8c
    monitor-exit v20
    :try_end_8d
    .catchall {:try_start_7 .. :try_end_8d} :catchall_8b

    #@8d
    throw v19

    #@8e
    .line 300
    .restart local v3       #accessFocus:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v10       #current:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v11       #fringe:Ljava/util/Queue;,"Ljava/util/Queue<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .restart local v14       #inputFocus:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v15       #parent:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v16       #root:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v17       #seen:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/view/accessibility/AccessibilityNodeInfo;>;"
    .restart local v18       #windowId:I
    :cond_8e
    :try_start_8e
    invoke-virtual {v10}, Landroid/view/accessibility/AccessibilityNodeInfo;->isAccessibilityFocused()Z

    #@91
    move-result v19

    #@92
    if-eqz v19, :cond_b4

    #@94
    .line 301
    if-eqz v3, :cond_ff

    #@96
    .line 302
    sget-object v19, Landroid/view/accessibility/AccessibilityNodeInfoCache;->LOG_TAG:Ljava/lang/String;

    #@98
    new-instance v21, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v22, "Duplicate accessibility focus:"

    #@9f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v21

    #@a3
    move-object/from16 v0, v21

    #@a5
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v21

    #@a9
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v21

    #@ad
    move-object/from16 v0, v19

    #@af
    move-object/from16 v1, v21

    #@b1
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 309
    :cond_b4
    :goto_b4
    invoke-virtual {v10}, Landroid/view/accessibility/AccessibilityNodeInfo;->isFocused()Z

    #@b7
    move-result v19

    #@b8
    if-eqz v19, :cond_da

    #@ba
    .line 310
    if-eqz v14, :cond_101

    #@bc
    .line 311
    sget-object v19, Landroid/view/accessibility/AccessibilityNodeInfoCache;->LOG_TAG:Ljava/lang/String;

    #@be
    new-instance v21, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v22, "Duplicate input focus: "

    #@c5
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v21

    #@c9
    move-object/from16 v0, v21

    #@cb
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v21

    #@cf
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v21

    #@d3
    move-object/from16 v0, v19

    #@d5
    move-object/from16 v1, v21

    #@d7
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 317
    :cond_da
    :goto_da
    invoke-virtual {v10}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@dd
    move-result-object v9

    #@de
    .line 318
    .local v9, childIds:Landroid/util/SparseLongArray;
    invoke-virtual {v9}, Landroid/util/SparseLongArray;->size()I

    #@e1
    move-result v6

    #@e2
    .line 319
    .local v6, childCount:I
    const/4 v12, 0x0

    #@e3
    .local v12, i:I
    :goto_e3
    if-ge v12, v6, :cond_57

    #@e5
    .line 320
    invoke-virtual {v9, v12}, Landroid/util/SparseLongArray;->valueAt(I)J

    #@e8
    move-result-wide v7

    #@e9
    .line 321
    .local v7, childId:J
    move-object/from16 v0, p0

    #@eb
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@ed
    move-object/from16 v19, v0

    #@ef
    move-object/from16 v0, v19

    #@f1
    invoke-virtual {v0, v7, v8}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@f4
    move-result-object v5

    #@f5
    check-cast v5, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@f7
    .line 322
    .local v5, child:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v5, :cond_fc

    #@f9
    .line 323
    invoke-interface {v11, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    #@fc
    .line 319
    :cond_fc
    add-int/lit8 v12, v12, 0x1

    #@fe
    goto :goto_e3

    #@ff
    .line 304
    .end local v5           #child:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v6           #childCount:I
    .end local v7           #childId:J
    .end local v9           #childIds:Landroid/util/SparseLongArray;
    .end local v12           #i:I
    :cond_ff
    move-object v3, v10

    #@100
    goto :goto_b4

    #@101
    .line 313
    :cond_101
    move-object v14, v10

    #@102
    goto :goto_da

    #@103
    .line 329
    .end local v10           #current:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_103
    move-object/from16 v0, p0

    #@105
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@107
    move-object/from16 v19, v0

    #@109
    invoke-virtual/range {v19 .. v19}, Landroid/util/LongSparseArray;->size()I

    #@10c
    move-result v4

    #@10d
    .line 330
    .local v4, cacheSize:I
    const/4 v12, 0x0

    #@10e
    .restart local v12       #i:I
    :goto_10e
    if-ge v12, v4, :cond_179

    #@110
    .line 331
    move-object/from16 v0, p0

    #@112
    iget-object v0, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@114
    move-object/from16 v19, v0

    #@116
    move-object/from16 v0, v19

    #@118
    invoke-virtual {v0, v12}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@11b
    move-result-object v13

    #@11c
    check-cast v13, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@11e
    .line 332
    .local v13, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    move-object/from16 v0, v17

    #@120
    invoke-virtual {v0, v13}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@123
    move-result v19

    #@124
    if-nez v19, :cond_13b

    #@126
    .line 333
    invoke-virtual {v13}, Landroid/view/accessibility/AccessibilityNodeInfo;->getWindowId()I

    #@129
    move-result v19

    #@12a
    move/from16 v0, v19

    #@12c
    move/from16 v1, v18

    #@12e
    if-ne v0, v1, :cond_13e

    #@130
    .line 334
    sget-object v19, Landroid/view/accessibility/AccessibilityNodeInfoCache;->LOG_TAG:Ljava/lang/String;

    #@132
    const-string v21, "Disconneced node: "

    #@134
    move-object/from16 v0, v19

    #@136
    move-object/from16 v1, v21

    #@138
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13b
    .line 330
    :cond_13b
    :goto_13b
    add-int/lit8 v12, v12, 0x1

    #@13d
    goto :goto_10e

    #@13e
    .line 336
    :cond_13e
    sget-object v19, Landroid/view/accessibility/AccessibilityNodeInfoCache;->LOG_TAG:Ljava/lang/String;

    #@140
    new-instance v21, Ljava/lang/StringBuilder;

    #@142
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v22, "Node from: "

    #@147
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v21

    #@14b
    invoke-virtual {v13}, Landroid/view/accessibility/AccessibilityNodeInfo;->getWindowId()I

    #@14e
    move-result v22

    #@14f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@152
    move-result-object v21

    #@153
    const-string v22, " not from:"

    #@155
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v21

    #@159
    move-object/from16 v0, v21

    #@15b
    move/from16 v1, v18

    #@15d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@160
    move-result-object v21

    #@161
    const-string v22, " "

    #@163
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v21

    #@167
    move-object/from16 v0, v21

    #@169
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v21

    #@16d
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@170
    move-result-object v21

    #@171
    move-object/from16 v0, v19

    #@173
    move-object/from16 v1, v21

    #@175
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@178
    goto :goto_13b

    #@179
    .line 341
    .end local v13           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_179
    monitor-exit v20
    :try_end_17a
    .catchall {:try_start_8e .. :try_end_17a} :catchall_8b

    #@17a
    goto/16 :goto_14
.end method

.method private clearSubTreeLocked(J)V
    .registers 10
    .parameter "rootNodeId"

    #@0
    .prologue
    .line 214
    iget-object v6, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@2
    invoke-virtual {v6, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@5
    move-result-object v4

    #@6
    check-cast v4, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@8
    .line 215
    .local v4, current:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-nez v4, :cond_b

    #@a
    .line 225
    :cond_a
    return-void

    #@b
    .line 218
    :cond_b
    iget-object v6, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@d
    invoke-virtual {v6, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    #@10
    .line 219
    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@13
    move-result-object v3

    #@14
    .line 220
    .local v3, childNodeIds:Landroid/util/SparseLongArray;
    invoke-virtual {v3}, Landroid/util/SparseLongArray;->size()I

    #@17
    move-result v0

    #@18
    .line 221
    .local v0, childCount:I
    const/4 v5, 0x0

    #@19
    .local v5, i:I
    :goto_19
    if-ge v5, v0, :cond_a

    #@1b
    .line 222
    invoke-virtual {v3, v5}, Landroid/util/SparseLongArray;->valueAt(I)J

    #@1e
    move-result-wide v1

    #@1f
    .line 223
    .local v1, childNodeId:J
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@22
    .line 221
    add-int/lit8 v5, v5, 0x1

    #@24
    goto :goto_19
.end method

.method private clearSubtreeWithOldAccessibilityFocusLocked(J)V
    .registers 9
    .parameter "currentAccessibilityFocusId"

    #@0
    .prologue
    .line 250
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@2
    invoke-virtual {v5}, Landroid/util/LongSparseArray;->size()I

    #@5
    move-result v0

    #@6
    .line 251
    .local v0, cacheSize:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_22

    #@9
    .line 252
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@b
    invoke-virtual {v5, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@11
    .line 253
    .local v2, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@14
    move-result-wide v3

    #@15
    .line 254
    .local v3, infoSourceId:J
    cmp-long v5, v3, p1

    #@17
    if-eqz v5, :cond_23

    #@19
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->isAccessibilityFocused()Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_23

    #@1f
    .line 255
    invoke-direct {p0, v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@22
    .line 259
    .end local v2           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v3           #infoSourceId:J
    :cond_22
    return-void

    #@23
    .line 251
    .restart local v2       #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v3       #infoSourceId:J
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_7
.end method

.method private clearSubtreeWithOldInputFocusLocked(J)V
    .registers 9
    .parameter "currentInputFocusId"

    #@0
    .prologue
    .line 233
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@2
    invoke-virtual {v5}, Landroid/util/LongSparseArray;->size()I

    #@5
    move-result v0

    #@6
    .line 234
    .local v0, cacheSize:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_22

    #@9
    .line 235
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@b
    invoke-virtual {v5, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@11
    .line 236
    .local v2, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@14
    move-result-wide v3

    #@15
    .line 237
    .local v3, infoSourceId:J
    cmp-long v5, v3, p1

    #@17
    if-eqz v5, :cond_23

    #@19
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->isFocused()Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_23

    #@1f
    .line 238
    invoke-direct {p0, v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@22
    .line 242
    .end local v2           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v3           #infoSourceId:J
    :cond_22
    return-void

    #@23
    .line 234
    .restart local v2       #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v3       #infoSourceId:J
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_7
.end method


# virtual methods
.method public add(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 19
    .parameter "info"

    #@0
    .prologue
    .line 149
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v14

    #@5
    .line 154
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getSourceNodeId()J

    #@8
    move-result-wide v11

    #@9
    .line 155
    .local v11, sourceId:J
    move-object/from16 v0, p0

    #@b
    iget-object v13, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@d
    invoke-virtual {v13, v11, v12}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@10
    move-result-object v8

    #@11
    check-cast v8, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@13
    .line 156
    .local v8, oldInfo:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v8, :cond_47

    #@15
    .line 161
    invoke-virtual {v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@18
    move-result-object v7

    #@19
    .line 162
    .local v7, oldChildrenIds:Landroid/util/SparseLongArray;
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildNodeIds()Landroid/util/SparseLongArray;

    #@1c
    move-result-object v3

    #@1d
    .line 163
    .local v3, newChildrenIds:Landroid/util/SparseLongArray;
    invoke-virtual {v7}, Landroid/util/SparseLongArray;->size()I

    #@20
    move-result v4

    #@21
    .line 164
    .local v4, oldChildCount:I
    const/4 v2, 0x0

    #@22
    .local v2, i:I
    :goto_22
    if-ge v2, v4, :cond_36

    #@24
    .line 165
    invoke-virtual {v7, v2}, Landroid/util/SparseLongArray;->valueAt(I)J

    #@27
    move-result-wide v5

    #@28
    .line 166
    .local v5, oldChildId:J
    invoke-virtual {v3, v5, v6}, Landroid/util/SparseLongArray;->indexOfValue(J)I

    #@2b
    move-result v13

    #@2c
    if-gez v13, :cond_33

    #@2e
    .line 167
    move-object/from16 v0, p0

    #@30
    invoke-direct {v0, v5, v6}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@33
    .line 164
    :cond_33
    add-int/lit8 v2, v2, 0x1

    #@35
    goto :goto_22

    #@36
    .line 174
    .end local v5           #oldChildId:J
    :cond_36
    invoke-virtual {v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@39
    move-result-wide v9

    #@3a
    .line 175
    .local v9, oldParentId:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParentNodeId()J

    #@3d
    move-result-wide v15

    #@3e
    cmp-long v13, v15, v9

    #@40
    if-eqz v13, :cond_47

    #@42
    .line 176
    move-object/from16 v0, p0

    #@44
    invoke-direct {v0, v9, v10}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@47
    .line 182
    .end local v2           #i:I
    .end local v3           #newChildrenIds:Landroid/util/SparseLongArray;
    .end local v4           #oldChildCount:I
    .end local v7           #oldChildrenIds:Landroid/util/SparseLongArray;
    .end local v9           #oldParentId:J
    :cond_47
    invoke-static/range {p1 .. p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@4a
    move-result-object v1

    #@4b
    .line 183
    .local v1, clone:Landroid/view/accessibility/AccessibilityNodeInfo;
    move-object/from16 v0, p0

    #@4d
    iget-object v13, v0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@4f
    invoke-virtual {v13, v11, v12, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    #@52
    .line 184
    monitor-exit v14

    #@53
    .line 186
    return-void

    #@54
    .line 184
    .end local v1           #clone:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v8           #oldInfo:Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v11           #sourceId:J
    :catchall_54
    move-exception v13

    #@55
    monitor-exit v14
    :try_end_56
    .catchall {:try_start_5 .. :try_end_56} :catchall_54

    #@56
    throw v13
.end method

.method public clear()V
    .registers 6

    #@0
    .prologue
    .line 193
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 198
    :try_start_3
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@5
    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    #@8
    move-result v2

    #@9
    .line 199
    .local v2, nodeCount:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v2, :cond_1a

    #@c
    .line 200
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@e
    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@14
    .line 201
    .local v1, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    #@17
    .line 199
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_a

    #@1a
    .line 203
    .end local v1           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_1a
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@1c
    invoke-virtual {v3}, Landroid/util/LongSparseArray;->clear()V

    #@1f
    .line 204
    monitor-exit v4

    #@20
    .line 206
    return-void

    #@21
    .line 204
    .end local v0           #i:I
    .end local v2           #nodeCount:I
    :catchall_21
    move-exception v3

    #@22
    monitor-exit v4
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v3
.end method

.method public get(J)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 6
    .parameter "accessibilityNodeId"

    #@0
    .prologue
    .line 125
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 126
    :try_start_3
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mCacheImpl:Landroid/util/LongSparseArray;

    #@5
    invoke-virtual {v1, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@b
    .line 127
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    if-eqz v0, :cond_11

    #@d
    .line 130
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@10
    move-result-object v0

    #@11
    .line 135
    :cond_11
    monitor-exit v2

    #@12
    return-object v0

    #@13
    .line 136
    .end local v0           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 10
    .parameter "event"

    #@0
    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@3
    move-result v2

    #@4
    .line 69
    .local v2, eventType:I
    sparse-switch v2, :sswitch_data_54

    #@7
    .line 111
    :cond_7
    :goto_7
    sget-boolean v6, Landroid/os/Build;->IS_DEBUGGABLE:Z

    #@9
    if-eqz v6, :cond_e

    #@b
    .line 112
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->checkIntegrity()V

    #@e
    .line 115
    :cond_e
    return-void

    #@f
    .line 72
    :sswitch_f
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getWindowId()I

    #@12
    move-result v6

    #@13
    iput v6, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mWindowId:I

    #@15
    .line 73
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clear()V

    #@18
    goto :goto_7

    #@19
    .line 77
    :sswitch_19
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getWindowId()I

    #@1c
    move-result v5

    #@1d
    .line 78
    .local v5, windowId:I
    iget v6, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mWindowId:I

    #@1f
    if-eq v6, v5, :cond_7

    #@21
    .line 80
    iput v5, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mWindowId:I

    #@23
    .line 81
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clear()V

    #@26
    goto :goto_7

    #@27
    .line 92
    .end local v5           #windowId:I
    :sswitch_27
    iget-object v7, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@29
    monitor-enter v7

    #@2a
    .line 93
    :try_start_2a
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSourceNodeId()J

    #@2d
    move-result-wide v3

    #@2e
    .line 94
    .local v3, sourceId:J
    invoke-direct {p0, v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@31
    .line 95
    const/16 v6, 0x8

    #@33
    if-ne v2, v6, :cond_38

    #@35
    .line 96
    invoke-direct {p0, v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubtreeWithOldInputFocusLocked(J)V

    #@38
    .line 98
    :cond_38
    const v6, 0x8000

    #@3b
    if-ne v2, v6, :cond_40

    #@3d
    .line 99
    invoke-direct {p0, v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubtreeWithOldAccessibilityFocusLocked(J)V

    #@40
    .line 101
    :cond_40
    monitor-exit v7

    #@41
    goto :goto_7

    #@42
    .end local v3           #sourceId:J
    :catchall_42
    move-exception v6

    #@43
    monitor-exit v7
    :try_end_44
    .catchall {:try_start_2a .. :try_end_44} :catchall_42

    #@44
    throw v6

    #@45
    .line 105
    :sswitch_45
    iget-object v7, p0, Landroid/view/accessibility/AccessibilityNodeInfoCache;->mLock:Ljava/lang/Object;

    #@47
    monitor-enter v7

    #@48
    .line 106
    :try_start_48
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getSourceNodeId()J

    #@4b
    move-result-wide v0

    #@4c
    .line 107
    .local v0, accessibilityNodeId:J
    invoke-direct {p0, v0, v1}, Landroid/view/accessibility/AccessibilityNodeInfoCache;->clearSubTreeLocked(J)V

    #@4f
    .line 108
    monitor-exit v7

    #@50
    goto :goto_7

    #@51
    .end local v0           #accessibilityNodeId:J
    :catchall_51
    move-exception v6

    #@52
    monitor-exit v7
    :try_end_53
    .catchall {:try_start_48 .. :try_end_53} :catchall_51

    #@53
    throw v6

    #@54
    .line 69
    :sswitch_data_54
    .sparse-switch
        0x4 -> :sswitch_27
        0x8 -> :sswitch_27
        0x10 -> :sswitch_27
        0x20 -> :sswitch_f
        0x80 -> :sswitch_19
        0x100 -> :sswitch_19
        0x800 -> :sswitch_45
        0x1000 -> :sswitch_45
        0x2000 -> :sswitch_27
        0x8000 -> :sswitch_27
    .end sparse-switch
.end method
