.class public Landroid/view/accessibility/AccessibilityRecord;
.super Ljava/lang/Object;
.source "AccessibilityRecord.java"


# static fields
.field private static final GET_SOURCE_PREFETCH_FLAGS:I = 0x7

.field private static final MAX_POOL_SIZE:I = 0xa

.field private static final PROPERTY_CHECKED:I = 0x1

.field private static final PROPERTY_ENABLED:I = 0x2

.field private static final PROPERTY_FULL_SCREEN:I = 0x80

.field private static final PROPERTY_IMPORTANT_FOR_ACCESSIBILITY:I = 0x200

.field private static final PROPERTY_PASSWORD:I = 0x4

.field private static final PROPERTY_SCROLLABLE:I = 0x100

.field private static final UNDEFINED:I = -0x1

.field private static sPool:Landroid/view/accessibility/AccessibilityRecord;

.field private static final sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field mAddedCount:I

.field mBeforeText:Ljava/lang/CharSequence;

.field mBooleanProperties:I

.field mClassName:Ljava/lang/CharSequence;

.field mConnectionId:I

.field mContentDescription:Ljava/lang/CharSequence;

.field mCurrentItemIndex:I

.field mFromIndex:I

.field private mIsInPool:Z

.field mItemCount:I

.field mMaxScrollX:I

.field mMaxScrollY:I

.field private mNext:Landroid/view/accessibility/AccessibilityRecord;

.field mParcelableData:Landroid/os/Parcelable;

.field mRemovedCount:I

.field mScrollX:I

.field mScrollY:I

.field mSealed:Z

.field mSourceNodeId:J

.field mSourceWindowId:I

.field final mText:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field mToIndex:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 74
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/view/accessibility/AccessibilityRecord;->sPoolLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 108
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 81
    const/16 v0, 0x200

    #@6
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@8
    .line 82
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@a
    .line 83
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@c
    .line 84
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@e
    .line 85
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@10
    .line 86
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@12
    .line 87
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@14
    .line 88
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@16
    .line 89
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@18
    .line 91
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@1a
    .line 92
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@1c
    .line 93
    invoke-static {v2, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@1f
    move-result-wide v0

    #@20
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@22
    .line 94
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@24
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    #@26
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@29
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@2b
    .line 103
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@2d
    .line 109
    return-void
.end method

.method private getBooleanProperty(I)Z
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 689
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-ne v0, p1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public static obtain()Landroid/view/accessibility/AccessibilityRecord;
    .registers 3

    #@0
    .prologue
    .line 726
    sget-object v2, Landroid/view/accessibility/AccessibilityRecord;->sPoolLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 727
    :try_start_3
    sget-object v1, Landroid/view/accessibility/AccessibilityRecord;->sPool:Landroid/view/accessibility/AccessibilityRecord;

    #@5
    if-eqz v1, :cond_1d

    #@7
    .line 728
    sget-object v0, Landroid/view/accessibility/AccessibilityRecord;->sPool:Landroid/view/accessibility/AccessibilityRecord;

    #@9
    .line 729
    .local v0, record:Landroid/view/accessibility/AccessibilityRecord;
    sget-object v1, Landroid/view/accessibility/AccessibilityRecord;->sPool:Landroid/view/accessibility/AccessibilityRecord;

    #@b
    iget-object v1, v1, Landroid/view/accessibility/AccessibilityRecord;->mNext:Landroid/view/accessibility/AccessibilityRecord;

    #@d
    sput-object v1, Landroid/view/accessibility/AccessibilityRecord;->sPool:Landroid/view/accessibility/AccessibilityRecord;

    #@f
    .line 730
    sget v1, Landroid/view/accessibility/AccessibilityRecord;->sPoolSize:I

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    sput v1, Landroid/view/accessibility/AccessibilityRecord;->sPoolSize:I

    #@15
    .line 731
    const/4 v1, 0x0

    #@16
    iput-object v1, v0, Landroid/view/accessibility/AccessibilityRecord;->mNext:Landroid/view/accessibility/AccessibilityRecord;

    #@18
    .line 732
    const/4 v1, 0x0

    #@19
    iput-boolean v1, v0, Landroid/view/accessibility/AccessibilityRecord;->mIsInPool:Z

    #@1b
    .line 733
    monitor-exit v2

    #@1c
    .line 735
    .end local v0           #record:Landroid/view/accessibility/AccessibilityRecord;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    new-instance v0, Landroid/view/accessibility/AccessibilityRecord;

    #@1f
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityRecord;-><init>()V

    #@22
    monitor-exit v2

    #@23
    goto :goto_1c

    #@24
    .line 736
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public static obtain(Landroid/view/accessibility/AccessibilityRecord;)Landroid/view/accessibility/AccessibilityRecord;
    .registers 2
    .parameter "record"

    #@0
    .prologue
    .line 714
    invoke-static {}, Landroid/view/accessibility/AccessibilityRecord;->obtain()Landroid/view/accessibility/AccessibilityRecord;

    #@3
    move-result-object v0

    #@4
    .line 715
    .local v0, clone:Landroid/view/accessibility/AccessibilityRecord;
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityRecord;->init(Landroid/view/accessibility/AccessibilityRecord;)V

    #@7
    .line 716
    return-object v0
.end method

.method private setBooleanProperty(IZ)V
    .registers 5
    .parameter "property"
    .parameter "value"

    #@0
    .prologue
    .line 699
    if-eqz p2, :cond_8

    #@2
    .line 700
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@4
    or-int/2addr v0, p1

    #@5
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@7
    .line 704
    :goto_7
    return-void

    #@8
    .line 702
    :cond_8
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@a
    xor-int/lit8 v1, p1, -0x1

    #@c
    and-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@f
    goto :goto_7
.end method


# virtual methods
.method clear()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 793
    const/4 v0, 0x0

    #@3
    iput-boolean v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@5
    .line 794
    const/16 v0, 0x200

    #@7
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@9
    .line 795
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@b
    .line 796
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@d
    .line 797
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@f
    .line 798
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@11
    .line 799
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@13
    .line 800
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@15
    .line 801
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@17
    .line 802
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@19
    .line 803
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@1b
    .line 804
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@1d
    .line 805
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@1f
    .line 806
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@21
    .line 807
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@23
    .line 808
    iput-object v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@25
    .line 809
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@27
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@2a
    .line 810
    invoke-static {v2, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@2d
    move-result-wide v0

    #@2e
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@30
    .line 811
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@32
    .line 812
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@34
    .line 813
    return-void
.end method

.method enforceNotSealed()V
    .registers 3

    #@0
    .prologue
    .line 676
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->isSealed()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 677
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Cannot perform this action on a sealed instance."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 680
    :cond_e
    return-void
.end method

.method enforceSealed()V
    .registers 3

    #@0
    .prologue
    .line 664
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->isSealed()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 665
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Cannot perform this action on a not sealed instance."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 668
    :cond_e
    return-void
.end method

.method public getAddedCount()I
    .registers 2

    #@0
    .prologue
    .line 484
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@2
    return v0
.end method

.method public getBeforeText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 557
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getClassName()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 526
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 578
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getCurrentItemIndex()I
    .registers 2

    #@0
    .prologue
    .line 339
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@2
    return v0
.end method

.method public getFromIndex()I
    .registers 2

    #@0
    .prologue
    .line 363
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@2
    return v0
.end method

.method public getItemCount()I
    .registers 2

    #@0
    .prologue
    .line 318
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@2
    return v0
.end method

.method public getMaxScrollX()I
    .registers 2

    #@0
    .prologue
    .line 446
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@2
    return v0
.end method

.method public getMaxScrollY()I
    .registers 2

    #@0
    .prologue
    .line 465
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@2
    return v0
.end method

.method public getParcelableData()Landroid/os/Parcelable;
    .registers 2

    #@0
    .prologue
    .line 599
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@2
    return-object v0
.end method

.method public getRemovedCount()I
    .registers 2

    #@0
    .prologue
    .line 505
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@2
    return v0
.end method

.method public getScrollX()I
    .registers 2

    #@0
    .prologue
    .line 408
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@2
    return v0
.end method

.method public getScrollY()I
    .registers 2

    #@0
    .prologue
    .line 427
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@2
    return v0
.end method

.method public getSource()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 7

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 160
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceSealed()V

    #@4
    .line 161
    iget v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@6
    if-eq v1, v3, :cond_14

    #@8
    iget v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@a
    if-eq v1, v3, :cond_14

    #@c
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@e
    invoke-static {v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@11
    move-result v1

    #@12
    if-ne v1, v3, :cond_16

    #@14
    .line 163
    :cond_14
    const/4 v1, 0x0

    #@15
    .line 166
    :goto_15
    return-object v1

    #@16
    .line 165
    :cond_16
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@19
    move-result-object v0

    #@1a
    .line 166
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@1c
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@1e
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@20
    const/4 v5, 0x7

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@24
    move-result-object v1

    #@25
    goto :goto_15
.end method

.method public getSourceNodeId()J
    .registers 3

    #@0
    .prologue
    .line 622
    iget-wide v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@2
    return-wide v0
.end method

.method public getText()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 548
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@2
    return-object v0
.end method

.method public getToIndex()I
    .registers 2

    #@0
    .prologue
    .line 388
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@2
    return v0
.end method

.method public getWindowId()I
    .registers 2

    #@0
    .prologue
    .line 187
    iget v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@2
    return v0
.end method

.method init(Landroid/view/accessibility/AccessibilityRecord;)V
    .registers 4
    .parameter "record"

    #@0
    .prologue
    .line 767
    iget-boolean v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@2
    iput-boolean v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@4
    .line 768
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@6
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBooleanProperties:I

    #@8
    .line 769
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@a
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@c
    .line 770
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@e
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@10
    .line 771
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@12
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@14
    .line 772
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@16
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@18
    .line 773
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@1a
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@1c
    .line 774
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@1e
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@20
    .line 775
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@22
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@24
    .line 776
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@26
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@28
    .line 777
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@2a
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@2c
    .line 778
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@2e
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@30
    .line 779
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@32
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@34
    .line 780
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@36
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@38
    .line 781
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@3a
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@3c
    .line 782
    iget-object v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@3e
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@40
    .line 783
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@42
    iget-object v1, p1, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@44
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@47
    .line 784
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@49
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@4b
    .line 785
    iget-wide v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@4d
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@4f
    .line 786
    iget v0, p1, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@51
    iput v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@53
    .line 787
    return-void
.end method

.method public isChecked()Z
    .registers 2

    #@0
    .prologue
    .line 196
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 217
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isFullScreen()Z
    .registers 2

    #@0
    .prologue
    .line 259
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isImportantForAccessibility()Z
    .registers 2

    #@0
    .prologue
    .line 309
    const/16 v0, 0x200

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPassword()Z
    .registers 2

    #@0
    .prologue
    .line 238
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isScrollable()Z
    .registers 2

    #@0
    .prologue
    .line 280
    const/16 v0, 0x100

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method isSealed()Z
    .registers 2

    #@0
    .prologue
    .line 655
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@2
    return v0
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 747
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mIsInPool:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 748
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Record already recycled!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 750
    :cond_c
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->clear()V

    #@f
    .line 751
    sget-object v1, Landroid/view/accessibility/AccessibilityRecord;->sPoolLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 752
    :try_start_12
    sget v0, Landroid/view/accessibility/AccessibilityRecord;->sPoolSize:I

    #@14
    const/16 v2, 0xa

    #@16
    if-gt v0, v2, :cond_27

    #@18
    .line 753
    sget-object v0, Landroid/view/accessibility/AccessibilityRecord;->sPool:Landroid/view/accessibility/AccessibilityRecord;

    #@1a
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mNext:Landroid/view/accessibility/AccessibilityRecord;

    #@1c
    .line 754
    sput-object p0, Landroid/view/accessibility/AccessibilityRecord;->sPool:Landroid/view/accessibility/AccessibilityRecord;

    #@1e
    .line 755
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Landroid/view/accessibility/AccessibilityRecord;->mIsInPool:Z

    #@21
    .line 756
    sget v0, Landroid/view/accessibility/AccessibilityRecord;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Landroid/view/accessibility/AccessibilityRecord;->sPoolSize:I

    #@27
    .line 758
    :cond_27
    monitor-exit v1

    #@28
    .line 759
    return-void

    #@29
    .line 758
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method public setAddedCount(I)V
    .registers 2
    .parameter "addedCount"

    #@0
    .prologue
    .line 495
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 496
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@5
    .line 497
    return-void
.end method

.method public setBeforeText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "beforeText"

    #@0
    .prologue
    .line 568
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 569
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@5
    .line 570
    return-void
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "isChecked"

    #@0
    .prologue
    .line 207
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 208
    const/4 v0, 0x1

    #@4
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setBooleanProperty(IZ)V

    #@7
    .line 209
    return-void
.end method

.method public setClassName(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "className"

    #@0
    .prologue
    .line 537
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 538
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@5
    .line 539
    return-void
.end method

.method public setConnectionId(I)V
    .registers 2
    .parameter "connectionId"

    #@0
    .prologue
    .line 634
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 635
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mConnectionId:I

    #@5
    .line 636
    return-void
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "contentDescription"

    #@0
    .prologue
    .line 589
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 590
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@5
    .line 591
    return-void
.end method

.method public setCurrentItemIndex(I)V
    .registers 2
    .parameter "currentItemIndex"

    #@0
    .prologue
    .line 350
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 351
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@5
    .line 352
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "isEnabled"

    #@0
    .prologue
    .line 228
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 229
    const/4 v0, 0x2

    #@4
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setBooleanProperty(IZ)V

    #@7
    .line 230
    return-void
.end method

.method public setFromIndex(I)V
    .registers 2
    .parameter "fromIndex"

    #@0
    .prologue
    .line 377
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 378
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@5
    .line 379
    return-void
.end method

.method public setFullScreen(Z)V
    .registers 3
    .parameter "isFullScreen"

    #@0
    .prologue
    .line 270
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 271
    const/16 v0, 0x80

    #@5
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setBooleanProperty(IZ)V

    #@8
    .line 272
    return-void
.end method

.method public setItemCount(I)V
    .registers 2
    .parameter "itemCount"

    #@0
    .prologue
    .line 329
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 330
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@5
    .line 331
    return-void
.end method

.method public setMaxScrollX(I)V
    .registers 2
    .parameter "maxScrollX"

    #@0
    .prologue
    .line 455
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 456
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@5
    .line 457
    return-void
.end method

.method public setMaxScrollY(I)V
    .registers 2
    .parameter "maxScrollY"

    #@0
    .prologue
    .line 474
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 475
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@5
    .line 476
    return-void
.end method

.method public setParcelableData(Landroid/os/Parcelable;)V
    .registers 2
    .parameter "parcelableData"

    #@0
    .prologue
    .line 610
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 611
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@5
    .line 612
    return-void
.end method

.method public setPassword(Z)V
    .registers 3
    .parameter "isPassword"

    #@0
    .prologue
    .line 249
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 250
    const/4 v0, 0x4

    #@4
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setBooleanProperty(IZ)V

    #@7
    .line 251
    return-void
.end method

.method public setRemovedCount(I)V
    .registers 2
    .parameter "removedCount"

    #@0
    .prologue
    .line 516
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 517
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@5
    .line 518
    return-void
.end method

.method public setScrollX(I)V
    .registers 2
    .parameter "scrollX"

    #@0
    .prologue
    .line 417
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 418
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@5
    .line 419
    return-void
.end method

.method public setScrollY(I)V
    .registers 2
    .parameter "scrollY"

    #@0
    .prologue
    .line 436
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 437
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@5
    .line 438
    return-void
.end method

.method public setScrollable(Z)V
    .registers 3
    .parameter "scrollable"

    #@0
    .prologue
    .line 291
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 292
    const/16 v0, 0x100

    #@5
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setBooleanProperty(IZ)V

    #@8
    .line 293
    return-void
.end method

.method public setSealed(Z)V
    .registers 2
    .parameter "sealed"

    #@0
    .prologue
    .line 646
    iput-boolean p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mSealed:Z

    #@2
    .line 647
    return-void
.end method

.method public setSource(Landroid/view/View;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 119
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/accessibility/AccessibilityRecord;->setSource(Landroid/view/View;I)V

    #@4
    .line 120
    return-void
.end method

.method public setSource(Landroid/view/View;I)V
    .registers 7
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 137
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@4
    .line 139
    if-ne p2, v3, :cond_28

    #@6
    .line 140
    if-eqz p1, :cond_26

    #@8
    invoke-virtual {p1}, Landroid/view/View;->isImportantForAccessibility()Z

    #@b
    move-result v0

    #@c
    .line 144
    .local v0, important:Z
    :goto_c
    const/16 v2, 0x200

    #@e
    invoke-direct {p0, v2, v0}, Landroid/view/accessibility/AccessibilityRecord;->setBooleanProperty(IZ)V

    #@11
    .line 145
    if-eqz p1, :cond_2a

    #@13
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityWindowId()I

    #@16
    move-result v2

    #@17
    :goto_17
    iput v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@19
    .line 146
    if-eqz p1, :cond_2c

    #@1b
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@1e
    move-result v1

    #@1f
    .line 147
    .local v1, rootViewId:I
    :goto_1f
    invoke-static {v1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@22
    move-result-wide v2

    #@23
    iput-wide v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceNodeId:J

    #@25
    .line 148
    return-void

    #@26
    .line 140
    .end local v0           #important:Z
    .end local v1           #rootViewId:I
    :cond_26
    const/4 v0, 0x1

    #@27
    goto :goto_c

    #@28
    .line 142
    :cond_28
    const/4 v0, 0x1

    #@29
    .restart local v0       #important:Z
    goto :goto_c

    #@2a
    :cond_2a
    move v2, v3

    #@2b
    .line 145
    goto :goto_17

    #@2c
    :cond_2c
    move v1, v3

    #@2d
    .line 146
    goto :goto_1f
.end method

.method public setToIndex(I)V
    .registers 2
    .parameter "toIndex"

    #@0
    .prologue
    .line 398
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityRecord;->enforceNotSealed()V

    #@3
    .line 399
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@5
    .line 400
    return-void
.end method

.method public setWindowId(I)V
    .registers 2
    .parameter "windowId"

    #@0
    .prologue
    .line 178
    iput p1, p0, Landroid/view/accessibility/AccessibilityRecord;->mSourceWindowId:I

    #@2
    .line 179
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 818
    .local v0, builder:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, " [ ClassName: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mClassName:Ljava/lang/CharSequence;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 819
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v2, "; Text: "

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mText:Ljava/util/List;

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 820
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "; ContentDescription: "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mContentDescription:Ljava/lang/CharSequence;

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    .line 821
    new-instance v1, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v2, "; ItemCount: "

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mItemCount:I

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    .line 822
    new-instance v1, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v2, "; CurrentItemIndex: "

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mCurrentItemIndex:I

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 823
    new-instance v1, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v2, "; IsEnabled: "

    #@84
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    const/4 v2, 0x2

    #@89
    invoke-direct {p0, v2}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@8c
    move-result v2

    #@8d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    .line 824
    new-instance v1, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v2, "; IsPassword: "

    #@9f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    const/4 v2, 0x4

    #@a4
    invoke-direct {p0, v2}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@a7
    move-result v2

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v1

    #@b0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 825
    new-instance v1, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v2, "; IsChecked: "

    #@ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    const/4 v2, 0x1

    #@bf
    invoke-direct {p0, v2}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@c2
    move-result v2

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v1

    #@c7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v1

    #@cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    .line 826
    new-instance v1, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v2, "; IsFullScreen: "

    #@d5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v1

    #@d9
    const/16 v2, 0x80

    #@db
    invoke-direct {p0, v2}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@de
    move-result v2

    #@df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v1

    #@e3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v1

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    .line 827
    new-instance v1, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v2, "; Scrollable: "

    #@f1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v1

    #@f5
    const/16 v2, 0x100

    #@f7
    invoke-direct {p0, v2}, Landroid/view/accessibility/AccessibilityRecord;->getBooleanProperty(I)Z

    #@fa
    move-result v2

    #@fb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v1

    #@103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    .line 828
    new-instance v1, Ljava/lang/StringBuilder;

    #@108
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v2, "; BeforeText: "

    #@10d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v1

    #@111
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mBeforeText:Ljava/lang/CharSequence;

    #@113
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v1

    #@117
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a
    move-result-object v1

    #@11b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    .line 829
    new-instance v1, Ljava/lang/StringBuilder;

    #@120
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@123
    const-string v2, "; FromIndex: "

    #@125
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v1

    #@129
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mFromIndex:I

    #@12b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v1

    #@12f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@132
    move-result-object v1

    #@133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    .line 830
    new-instance v1, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v2, "; ToIndex: "

    #@13d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v1

    #@141
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mToIndex:I

    #@143
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@146
    move-result-object v1

    #@147
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14a
    move-result-object v1

    #@14b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    .line 831
    new-instance v1, Ljava/lang/StringBuilder;

    #@150
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@153
    const-string v2, "; ScrollX: "

    #@155
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v1

    #@159
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollX:I

    #@15b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v1

    #@15f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@162
    move-result-object v1

    #@163
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    .line 832
    new-instance v1, Ljava/lang/StringBuilder;

    #@168
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    const-string v2, "; ScrollY: "

    #@16d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v1

    #@171
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mScrollY:I

    #@173
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@176
    move-result-object v1

    #@177
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17a
    move-result-object v1

    #@17b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    .line 833
    new-instance v1, Ljava/lang/StringBuilder;

    #@180
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@183
    const-string v2, "; MaxScrollX: "

    #@185
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v1

    #@189
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollX:I

    #@18b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v1

    #@18f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@192
    move-result-object v1

    #@193
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    .line 834
    new-instance v1, Ljava/lang/StringBuilder;

    #@198
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19b
    const-string v2, "; MaxScrollY: "

    #@19d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v1

    #@1a1
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mMaxScrollY:I

    #@1a3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v1

    #@1a7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1aa
    move-result-object v1

    #@1ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    .line 835
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b3
    const-string v2, "; AddedCount: "

    #@1b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v1

    #@1b9
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mAddedCount:I

    #@1bb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v1

    #@1bf
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c2
    move-result-object v1

    #@1c3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    .line 836
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1cb
    const-string v2, "; RemovedCount: "

    #@1cd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v1

    #@1d1
    iget v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mRemovedCount:I

    #@1d3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v1

    #@1d7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1da
    move-result-object v1

    #@1db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    .line 837
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v2, "; ParcelableData: "

    #@1e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v1

    #@1e9
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityRecord;->mParcelableData:Landroid/os/Parcelable;

    #@1eb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v1

    #@1ef
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f2
    move-result-object v1

    #@1f3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    .line 838
    const-string v1, " ]"

    #@1f8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fb
    .line 839
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fe
    move-result-object v1

    #@1ff
    return-object v1
.end method
