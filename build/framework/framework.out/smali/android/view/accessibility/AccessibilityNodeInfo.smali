.class public Landroid/view/accessibility/AccessibilityNodeInfo;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACTION_ACCESSIBILITY_FOCUS:I = 0x40

.field public static final ACTION_ARGUMENT_HTML_ELEMENT_STRING:Ljava/lang/String; = "ACTION_ARGUMENT_HTML_ELEMENT_STRING"

.field public static final ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT:Ljava/lang/String; = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

.field public static final ACTION_CLEAR_ACCESSIBILITY_FOCUS:I = 0x80

.field public static final ACTION_CLEAR_FOCUS:I = 0x2

.field public static final ACTION_CLEAR_SELECTION:I = 0x8

.field public static final ACTION_CLICK:I = 0x10

.field public static final ACTION_FOCUS:I = 0x1

.field public static final ACTION_LONG_CLICK:I = 0x20

.field public static final ACTION_NEXT_AT_MOVEMENT_GRANULARITY:I = 0x100

.field public static final ACTION_NEXT_HTML_ELEMENT:I = 0x400

.field public static final ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY:I = 0x200

.field public static final ACTION_PREVIOUS_HTML_ELEMENT:I = 0x800

.field public static final ACTION_SCROLL_BACKWARD:I = 0x2000

.field public static final ACTION_SCROLL_FORWARD:I = 0x1000

.field public static final ACTION_SELECT:I = 0x4

.field public static final ACTIVE_WINDOW_ID:I = -0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = false

.field public static final FLAG_PREFETCH_DESCENDANTS:I = 0x4

.field public static final FLAG_PREFETCH_PREDECESSORS:I = 0x1

.field public static final FLAG_PREFETCH_SIBLINGS:I = 0x2

.field public static final FOCUS_ACCESSIBILITY:I = 0x2

.field public static final FOCUS_INPUT:I = 0x1

.field public static final INCLUDE_NOT_IMPORTANT_VIEWS:I = 0x8

.field private static final MAX_POOL_SIZE:I = 0x32

.field public static final MOVEMENT_GRANULARITY_CHARACTER:I = 0x1

.field public static final MOVEMENT_GRANULARITY_LINE:I = 0x4

.field public static final MOVEMENT_GRANULARITY_PAGE:I = 0x10

.field public static final MOVEMENT_GRANULARITY_PARAGRAPH:I = 0x8

.field public static final MOVEMENT_GRANULARITY_WORD:I = 0x2

.field private static final PROPERTY_ACCESSIBILITY_FOCUSED:I = 0x400

.field private static final PROPERTY_CHECKABLE:I = 0x1

.field private static final PROPERTY_CHECKED:I = 0x2

.field private static final PROPERTY_CLICKABLE:I = 0x20

.field private static final PROPERTY_ENABLED:I = 0x80

.field private static final PROPERTY_FOCUSABLE:I = 0x4

.field private static final PROPERTY_FOCUSED:I = 0x8

.field private static final PROPERTY_LONG_CLICKABLE:I = 0x40

.field private static final PROPERTY_PASSWORD:I = 0x100

.field private static final PROPERTY_SCROLLABLE:I = 0x200

.field private static final PROPERTY_SELECTED:I = 0x10

.field private static final PROPERTY_VISIBLE_TO_USER:I = 0x800

#the value of this static final field might be set in the static constructor
.field public static final ROOT_NODE_ID:J = 0x0L

.field public static final UNDEFINED:I = -0x1

.field private static final VIRTUAL_DESCENDANT_ID_MASK:J = -0x100000000L

.field private static final VIRTUAL_DESCENDANT_ID_SHIFT:I = 0x20

.field private static sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

.field private static final sPoolLock:Ljava/lang/Object;

.field private static sPoolSize:I


# instance fields
.field private mActions:I

.field private mBooleanProperties:I

.field private final mBoundsInParent:Landroid/graphics/Rect;

.field private final mBoundsInScreen:Landroid/graphics/Rect;

.field private final mChildNodeIds:Landroid/util/SparseLongArray;

.field private mClassName:Ljava/lang/CharSequence;

.field private mConnectionId:I

.field private mContentDescription:Ljava/lang/CharSequence;

.field private mIsInPool:Z

.field private mLabelForId:J

.field private mLabeledById:J

.field private mMovementGranularities:I

.field private mNext:Landroid/view/accessibility/AccessibilityNodeInfo;

.field private mPackageName:Ljava/lang/CharSequence;

.field private mParentNodeId:J

.field private mSealed:Z

.field private mSourceNodeId:J

.field private mText:Ljava/lang/CharSequence;

.field private mWindowId:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 65
    invoke-static {v0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@4
    move-result-wide v0

    #@5
    sput-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@7
    .line 357
    new-instance v0, Ljava/lang/Object;

    #@9
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@c
    sput-object v0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolLock:Ljava/lang/Object;

    #@e
    .line 1883
    new-instance v0, Landroid/view/accessibility/AccessibilityNodeInfo$1;

    #@10
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$1;-><init>()V

    #@13
    sput-object v0, Landroid/view/accessibility/AccessibilityNodeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 390
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 365
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@6
    .line 366
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@8
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@a
    .line 367
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@c
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@e
    .line 368
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@10
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@12
    .line 369
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@14
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@16
    .line 372
    new-instance v0, Landroid/graphics/Rect;

    #@18
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@1d
    .line 373
    new-instance v0, Landroid/graphics/Rect;

    #@1f
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@22
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@24
    .line 380
    new-instance v0, Landroid/util/SparseLongArray;

    #@26
    invoke-direct {v0}, Landroid/util/SparseLongArray;-><init>()V

    #@29
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@2b
    .line 385
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@2d
    .line 392
    return-void
.end method

.method static synthetic access$000(Landroid/view/accessibility/AccessibilityNodeInfo;Landroid/os/Parcel;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->initFromParcel(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private canPerformRequestOverConnection(J)Z
    .registers 5
    .parameter "accessibilityNodeId"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1778
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@3
    if-eq v0, v1, :cond_11

    #@5
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@8
    move-result v0

    #@9
    if-eq v0, v1, :cond_11

    #@b
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@d
    if-eq v0, v1, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method private clear()V
    .registers 6

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 1694
    iput-boolean v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSealed:Z

    #@5
    .line 1695
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@7
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@9
    .line 1696
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@b
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@d
    .line 1697
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@f
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@11
    .line 1698
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@13
    iput-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@15
    .line 1699
    iput v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@17
    .line 1700
    iput v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@19
    .line 1701
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@1b
    .line 1702
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@1d
    invoke-virtual {v0}, Landroid/util/SparseLongArray;->clear()V

    #@20
    .line 1703
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@25
    .line 1704
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@27
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@2a
    .line 1705
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@2c
    .line 1706
    iput-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@2e
    .line 1707
    iput-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@30
    .line 1708
    iput-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@32
    .line 1709
    iput-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@34
    .line 1710
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@36
    .line 1711
    return-void
.end method

.method private enforceValidFocusDirection(I)V
    .registers 5
    .parameter "direction"

    #@0
    .prologue
    .line 1446
    sparse-switch p1, :sswitch_data_1e

    #@3
    .line 1455
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown direction: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1453
    :sswitch_1c
    return-void

    #@1d
    .line 1446
    nop

    #@1e
    :sswitch_data_1e
    .sparse-switch
        0x1 -> :sswitch_1c
        0x2 -> :sswitch_1c
        0x11 -> :sswitch_1c
        0x21 -> :sswitch_1c
        0x42 -> :sswitch_1c
        0x82 -> :sswitch_1c
    .end sparse-switch
.end method

.method private enforceValidFocusType(I)V
    .registers 5
    .parameter "focusType"

    #@0
    .prologue
    .line 1460
    packed-switch p1, :pswitch_data_1e

    #@3
    .line 1465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown focus type: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1463
    :pswitch_1c
    return-void

    #@1d
    .line 1460
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1c
    .end packed-switch
.end method

.method public static getAccessibilityViewId(J)I
    .registers 3
    .parameter "accessibilityNodeId"

    #@0
    .prologue
    .line 323
    long-to-int v0, p0

    #@1
    return v0
.end method

.method private static getActionSymbolicName(I)Ljava/lang/String;
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 1720
    sparse-switch p0, :sswitch_data_46

    #@3
    .line 1750
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown action: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1722
    :sswitch_1c
    const-string v0, "ACTION_FOCUS"

    #@1e
    .line 1748
    :goto_1e
    return-object v0

    #@1f
    .line 1724
    :sswitch_1f
    const-string v0, "ACTION_CLEAR_FOCUS"

    #@21
    goto :goto_1e

    #@22
    .line 1726
    :sswitch_22
    const-string v0, "ACTION_SELECT"

    #@24
    goto :goto_1e

    #@25
    .line 1728
    :sswitch_25
    const-string v0, "ACTION_CLEAR_SELECTION"

    #@27
    goto :goto_1e

    #@28
    .line 1730
    :sswitch_28
    const-string v0, "ACTION_CLICK"

    #@2a
    goto :goto_1e

    #@2b
    .line 1732
    :sswitch_2b
    const-string v0, "ACTION_LONG_CLICK"

    #@2d
    goto :goto_1e

    #@2e
    .line 1734
    :sswitch_2e
    const-string v0, "ACTION_ACCESSIBILITY_FOCUS"

    #@30
    goto :goto_1e

    #@31
    .line 1736
    :sswitch_31
    const-string v0, "ACTION_CLEAR_ACCESSIBILITY_FOCUS"

    #@33
    goto :goto_1e

    #@34
    .line 1738
    :sswitch_34
    const-string v0, "ACTION_NEXT_AT_MOVEMENT_GRANULARITY"

    #@36
    goto :goto_1e

    #@37
    .line 1740
    :sswitch_37
    const-string v0, "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY"

    #@39
    goto :goto_1e

    #@3a
    .line 1742
    :sswitch_3a
    const-string v0, "ACTION_NEXT_HTML_ELEMENT"

    #@3c
    goto :goto_1e

    #@3d
    .line 1744
    :sswitch_3d
    const-string v0, "ACTION_PREVIOUS_HTML_ELEMENT"

    #@3f
    goto :goto_1e

    #@40
    .line 1746
    :sswitch_40
    const-string v0, "ACTION_SCROLL_FORWARD"

    #@42
    goto :goto_1e

    #@43
    .line 1748
    :sswitch_43
    const-string v0, "ACTION_SCROLL_BACKWARD"

    #@45
    goto :goto_1e

    #@46
    .line 1720
    :sswitch_data_46
    .sparse-switch
        0x1 -> :sswitch_1c
        0x2 -> :sswitch_1f
        0x4 -> :sswitch_22
        0x8 -> :sswitch_25
        0x10 -> :sswitch_28
        0x20 -> :sswitch_2b
        0x40 -> :sswitch_2e
        0x80 -> :sswitch_31
        0x100 -> :sswitch_34
        0x200 -> :sswitch_37
        0x400 -> :sswitch_3a
        0x800 -> :sswitch_3d
        0x1000 -> :sswitch_40
        0x2000 -> :sswitch_43
    .end sparse-switch
.end method

.method private getBooleanProperty(I)Z
    .registers 3
    .parameter "property"

    #@0
    .prologue
    .line 1358
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-eqz v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method private static getMovementGranularitySymbolicName(I)Ljava/lang/String;
    .registers 4
    .parameter "granularity"

    #@0
    .prologue
    .line 1761
    sparse-switch p0, :sswitch_data_2c

    #@3
    .line 1773
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown movement granularity: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1763
    :sswitch_1c
    const-string v0, "MOVEMENT_GRANULARITY_CHARACTER"

    #@1e
    .line 1771
    :goto_1e
    return-object v0

    #@1f
    .line 1765
    :sswitch_1f
    const-string v0, "MOVEMENT_GRANULARITY_WORD"

    #@21
    goto :goto_1e

    #@22
    .line 1767
    :sswitch_22
    const-string v0, "MOVEMENT_GRANULARITY_LINE"

    #@24
    goto :goto_1e

    #@25
    .line 1769
    :sswitch_25
    const-string v0, "MOVEMENT_GRANULARITY_PARAGRAPH"

    #@27
    goto :goto_1e

    #@28
    .line 1771
    :sswitch_28
    const-string v0, "MOVEMENT_GRANULARITY_PAGE"

    #@2a
    goto :goto_1e

    #@2b
    .line 1761
    nop

    #@2c
    :sswitch_data_2c
    .sparse-switch
        0x1 -> :sswitch_1c
        0x2 -> :sswitch_1f
        0x4 -> :sswitch_22
        0x8 -> :sswitch_25
        0x10 -> :sswitch_28
    .end sparse-switch
.end method

.method public static getVirtualDescendantId(J)I
    .registers 5
    .parameter "accessibilityNodeId"

    #@0
    .prologue
    .line 336
    const-wide v0, -0x100000000L

    #@5
    and-long/2addr v0, p0

    #@6
    const/16 v2, 0x20

    #@8
    shr-long/2addr v0, v2

    #@9
    long-to-int v0, v0

    #@a
    return v0
.end method

.method private init(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 7
    .parameter "other"

    #@0
    .prologue
    .line 1625
    iget-boolean v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mSealed:Z

    #@2
    iput-boolean v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSealed:Z

    #@4
    .line 1626
    iget-wide v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@6
    iput-wide v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@8
    .line 1627
    iget-wide v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@a
    iput-wide v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@c
    .line 1628
    iget-wide v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@e
    iput-wide v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@10
    .line 1629
    iget-wide v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@12
    iput-wide v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@14
    .line 1630
    iget v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@16
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@18
    .line 1631
    iget v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@1a
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@1c
    .line 1632
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@1e
    iget-object v3, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@20
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@23
    .line 1633
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@25
    iget-object v3, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@27
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@2a
    .line 1634
    iget-object v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@2c
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@2e
    .line 1635
    iget-object v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@30
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@32
    .line 1636
    iget-object v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@34
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@36
    .line 1637
    iget-object v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@38
    iput-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@3a
    .line 1638
    iget v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@3c
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@3e
    .line 1639
    iget v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@40
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@42
    .line 1640
    iget v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@44
    iput v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@46
    .line 1641
    iget-object v2, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@48
    invoke-virtual {v2}, Landroid/util/SparseLongArray;->size()I

    #@4b
    move-result v1

    #@4c
    .line 1642
    .local v1, otherChildIdCount:I
    const/4 v0, 0x0

    #@4d
    .local v0, i:I
    :goto_4d
    if-ge v0, v1, :cond_5d

    #@4f
    .line 1643
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@51
    iget-object v3, p1, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@53
    invoke-virtual {v3, v0}, Landroid/util/SparseLongArray;->valueAt(I)J

    #@56
    move-result-wide v3

    #@57
    invoke-virtual {v2, v0, v3, v4}, Landroid/util/SparseLongArray;->put(IJ)V

    #@5a
    .line 1642
    add-int/lit8 v0, v0, 0x1

    #@5c
    goto :goto_4d

    #@5d
    .line 1645
    :cond_5d
    return-void
.end method

.method private initFromParcel(Landroid/os/Parcel;)V
    .registers 9
    .parameter "parcel"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1653
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v6

    #@5
    if-ne v6, v5, :cond_40

    #@7
    :goto_7
    iput-boolean v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSealed:Z

    #@9
    .line 1654
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@c
    move-result-wide v5

    #@d
    iput-wide v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@f
    .line 1655
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v5

    #@13
    iput v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@15
    .line 1656
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@18
    move-result-wide v5

    #@19
    iput-wide v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@1b
    .line 1657
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1e
    move-result-wide v5

    #@1f
    iput-wide v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@21
    .line 1658
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@24
    move-result-wide v5

    #@25
    iput-wide v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@27
    .line 1659
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v5

    #@2b
    iput v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@2d
    .line 1661
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@2f
    .line 1662
    .local v2, childIds:Landroid/util/SparseLongArray;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v3

    #@33
    .line 1663
    .local v3, childrenSize:I
    const/4 v4, 0x0

    #@34
    .local v4, i:I
    :goto_34
    if-ge v4, v3, :cond_42

    #@36
    .line 1664
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@39
    move-result-wide v0

    #@3a
    .line 1665
    .local v0, childId:J
    invoke-virtual {v2, v4, v0, v1}, Landroid/util/SparseLongArray;->put(IJ)V

    #@3d
    .line 1663
    add-int/lit8 v4, v4, 0x1

    #@3f
    goto :goto_34

    #@40
    .line 1653
    .end local v0           #childId:J
    .end local v2           #childIds:Landroid/util/SparseLongArray;
    .end local v3           #childrenSize:I
    .end local v4           #i:I
    :cond_40
    const/4 v5, 0x0

    #@41
    goto :goto_7

    #@42
    .line 1668
    .restart local v2       #childIds:Landroid/util/SparseLongArray;
    .restart local v3       #childrenSize:I
    .restart local v4       #i:I
    :cond_42
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v6

    #@48
    iput v6, v5, Landroid/graphics/Rect;->top:I

    #@4a
    .line 1669
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@4c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v6

    #@50
    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    #@52
    .line 1670
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v6

    #@58
    iput v6, v5, Landroid/graphics/Rect;->left:I

    #@5a
    .line 1671
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@5c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v6

    #@60
    iput v6, v5, Landroid/graphics/Rect;->right:I

    #@62
    .line 1673
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v6

    #@68
    iput v6, v5, Landroid/graphics/Rect;->top:I

    #@6a
    .line 1674
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@6c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v6

    #@70
    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    #@72
    .line 1675
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v6

    #@78
    iput v6, v5, Landroid/graphics/Rect;->left:I

    #@7a
    .line 1676
    iget-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@7c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v6

    #@80
    iput v6, v5, Landroid/graphics/Rect;->right:I

    #@82
    .line 1678
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@85
    move-result v5

    #@86
    iput v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@88
    .line 1680
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8b
    move-result v5

    #@8c
    iput v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@8e
    .line 1682
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@91
    move-result v5

    #@92
    iput v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@94
    .line 1684
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequence()Ljava/lang/CharSequence;

    #@97
    move-result-object v5

    #@98
    iput-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@9a
    .line 1685
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequence()Ljava/lang/CharSequence;

    #@9d
    move-result-object v5

    #@9e
    iput-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@a0
    .line 1686
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequence()Ljava/lang/CharSequence;

    #@a3
    move-result-object v5

    #@a4
    iput-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@a6
    .line 1687
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequence()Ljava/lang/CharSequence;

    #@a9
    move-result-object v5

    #@aa
    iput-object v5, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@ac
    .line 1688
    return-void
.end method

.method public static makeNodeId(II)J
    .registers 6
    .parameter "accessibilityViewId"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    .line 352
    int-to-long v0, p1

    #@1
    const/16 v2, 0x20

    #@3
    shl-long/2addr v0, v2

    #@4
    int-to-long v2, p0

    #@5
    or-long/2addr v0, v2

    #@6
    return-wide v0
.end method

.method public static obtain()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 3

    #@0
    .prologue
    .line 1520
    sget-object v2, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1521
    :try_start_3
    sget-object v1, Landroid/view/accessibility/AccessibilityNodeInfo;->sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@5
    if-eqz v1, :cond_1d

    #@7
    .line 1522
    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@9
    .line 1523
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    sget-object v1, Landroid/view/accessibility/AccessibilityNodeInfo;->sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@b
    iget-object v1, v1, Landroid/view/accessibility/AccessibilityNodeInfo;->mNext:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@d
    sput-object v1, Landroid/view/accessibility/AccessibilityNodeInfo;->sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@f
    .line 1524
    sget v1, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolSize:I

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    sput v1, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolSize:I

    #@15
    .line 1525
    const/4 v1, 0x0

    #@16
    iput-object v1, v0, Landroid/view/accessibility/AccessibilityNodeInfo;->mNext:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@18
    .line 1526
    const/4 v1, 0x0

    #@19
    iput-boolean v1, v0, Landroid/view/accessibility/AccessibilityNodeInfo;->mIsInPool:Z

    #@1b
    .line 1527
    monitor-exit v2

    #@1c
    .line 1529
    .end local v0           #info:Landroid/view/accessibility/AccessibilityNodeInfo;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    new-instance v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1f
    invoke-direct {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;-><init>()V

    #@22
    monitor-exit v2

    #@23
    goto :goto_1c

    #@24
    .line 1530
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method

.method public static obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 1493
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3
    move-result-object v0

    #@4
    .line 1494
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;)V

    #@7
    .line 1495
    return-object v0
.end method

.method public static obtain(Landroid/view/View;I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 3
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    .line 1509
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3
    move-result-object v0

    #@4
    .line 1510
    .local v0, info:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-virtual {v0, p0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    #@7
    .line 1511
    return-object v0
.end method

.method public static obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 1542
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    #@3
    move-result-object v0

    #@4
    .line 1543
    .local v0, infoClone:Landroid/view/accessibility/AccessibilityNodeInfo;
    invoke-direct {v0, p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->init(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@7
    .line 1544
    return-object v0
.end method

.method private setBooleanProperty(IZ)V
    .registers 5
    .parameter "property"
    .parameter "value"

    #@0
    .prologue
    .line 1370
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1371
    if-eqz p2, :cond_b

    #@5
    .line 1372
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@7
    or-int/2addr v0, p1

    #@8
    iput v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@a
    .line 1376
    :goto_a
    return-void

    #@b
    .line 1374
    :cond_b
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@d
    xor-int/lit8 v1, p1, -0x1

    #@f
    and-int/2addr v0, v1

    #@10
    iput v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@12
    goto :goto_a
.end method


# virtual methods
.method public addAction(I)V
    .registers 3
    .parameter "action"

    #@0
    .prologue
    .line 608
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 609
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@5
    or-int/2addr v0, p1

    #@6
    iput v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@8
    .line 610
    return-void
.end method

.method public addChild(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 545
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    #@4
    .line 546
    return-void
.end method

.method public addChild(Landroid/view/View;I)V
    .registers 8
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    .line 563
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 564
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@5
    invoke-virtual {v4}, Landroid/util/SparseLongArray;->size()I

    #@8
    move-result v2

    #@9
    .line 565
    .local v2, index:I
    if-eqz p1, :cond_19

    #@b
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@e
    move-result v3

    #@f
    .line 567
    .local v3, rootAccessibilityViewId:I
    :goto_f
    invoke-static {v3, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@12
    move-result-wide v0

    #@13
    .line 568
    .local v0, childNodeId:J
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@15
    invoke-virtual {v4, v2, v0, v1}, Landroid/util/SparseLongArray;->put(IJ)V

    #@18
    .line 569
    return-void

    #@19
    .line 565
    .end local v0           #childNodeId:J
    .end local v3           #rootAccessibilityViewId:I
    :cond_19
    const/4 v3, -0x1

    #@1a
    goto :goto_f
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 1395
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected enforceNotSealed()V
    .registers 3

    #@0
    .prologue
    .line 1477
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isSealed()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 1478
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Cannot perform this action on a sealed instance."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1481
    :cond_e
    return-void
.end method

.method protected enforceSealed()V
    .registers 3

    #@0
    .prologue
    .line 1439
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isSealed()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 1440
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Cannot perform this action on a not sealed instance."

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1443
    :cond_e
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1785
    if-ne p0, p1, :cond_5

    #@4
    .line 1801
    :cond_4
    :goto_4
    return v1

    #@5
    .line 1788
    :cond_5
    if-nez p1, :cond_9

    #@7
    move v1, v2

    #@8
    .line 1789
    goto :goto_4

    #@9
    .line 1791
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@10
    move-result-object v4

    #@11
    if-eq v3, v4, :cond_15

    #@13
    move v1, v2

    #@14
    .line 1792
    goto :goto_4

    #@15
    :cond_15
    move-object v0, p1

    #@16
    .line 1794
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    #@18
    .line 1795
    .local v0, other:Landroid/view/accessibility/AccessibilityNodeInfo;
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@1a
    iget-wide v5, v0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@1c
    cmp-long v3, v3, v5

    #@1e
    if-eqz v3, :cond_22

    #@20
    move v1, v2

    #@21
    .line 1796
    goto :goto_4

    #@22
    .line 1798
    :cond_22
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@24
    iget v4, v0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@26
    if-eq v3, v4, :cond_4

    #@28
    move v1, v2

    #@29
    .line 1799
    goto :goto_4
.end method

.method public findAccessibilityNodeInfosByText(Ljava/lang/String;)Ljava/util/List;
    .registers 8
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 698
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 699
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_10

    #@b
    .line 700
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@e
    move-result-object v1

    #@f
    .line 703
    :goto_f
    return-object v1

    #@10
    .line 702
    :cond_10
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@13
    move-result-object v0

    #@14
    .line 703
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@16
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@18
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@1a
    move-object v5, p1

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfosByText(IIJLjava/lang/String;)Ljava/util/List;

    #@1e
    move-result-object v1

    #@1f
    goto :goto_f
.end method

.method public findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 8
    .parameter "focus"

    #@0
    .prologue
    .line 447
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 448
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceValidFocusType(I)V

    #@6
    .line 449
    iget-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@8
    invoke-direct {p0, v0, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_10

    #@e
    .line 450
    const/4 v0, 0x0

    #@f
    .line 452
    :goto_f
    return-object v0

    #@10
    :cond_10
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@13
    move-result-object v0

    #@14
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@16
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@18
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@1a
    move v5, p1

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findFocus(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_f
.end method

.method public focusSearch(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 8
    .parameter "direction"

    #@0
    .prologue
    .line 471
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 472
    invoke-direct {p0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceValidFocusDirection(I)V

    #@6
    .line 473
    iget-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@8
    invoke-direct {p0, v0, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_10

    #@e
    .line 474
    const/4 v0, 0x0

    #@f
    .line 476
    :goto_f
    return-object v0

    #@10
    :cond_10
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@13
    move-result-object v0

    #@14
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@16
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@18
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@1a
    move v5, p1

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->focusSearch(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_f
.end method

.method public getActions()I
    .registers 2

    #@0
    .prologue
    .line 592
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@2
    return v0
.end method

.method public getBoundsInParent(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "outBounds"

    #@0
    .prologue
    .line 784
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@6
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@8
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@a
    iget v2, v2, Landroid/graphics/Rect;->right:I

    #@c
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@e
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@10
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@13
    .line 786
    return-void
.end method

.method public getBoundsInScreen(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "outBounds"

    #@0
    .prologue
    .line 811
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@2
    iget v0, v0, Landroid/graphics/Rect;->left:I

    #@4
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@6
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@8
    iget-object v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@a
    iget v2, v2, Landroid/graphics/Rect;->right:I

    #@c
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@e
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@10
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@13
    .line 813
    return-void
.end method

.method public getChild(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 8
    .parameter "index"

    #@0
    .prologue
    .line 522
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 523
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_d

    #@b
    .line 524
    const/4 v1, 0x0

    #@c
    .line 528
    :goto_c
    return-object v1

    #@d
    .line 526
    :cond_d
    iget-object v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@f
    invoke-virtual {v1, p1}, Landroid/util/SparseLongArray;->get(I)J

    #@12
    move-result-wide v3

    #@13
    .line 527
    .local v3, childId:J
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@16
    move-result-object v0

    #@17
    .line 528
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@19
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@1b
    const/4 v5, 0x4

    #@1c
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1f
    move-result-object v1

    #@20
    goto :goto_c
.end method

.method public getChildCount()I
    .registers 2

    #@0
    .prologue
    .line 504
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@2
    invoke-virtual {v0}, Landroid/util/SparseLongArray;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getChildNodeIds()Landroid/util/SparseLongArray;
    .registers 2

    #@0
    .prologue
    .line 495
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@2
    return-object v0
.end method

.method public getClassName()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1165
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1217
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getLabelFor()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 7

    #@0
    .prologue
    .line 1285
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 1286
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_d

    #@b
    .line 1287
    const/4 v1, 0x0

    #@c
    .line 1290
    :goto_c
    return-object v1

    #@d
    .line 1289
    :cond_d
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@10
    move-result-object v0

    #@11
    .line 1290
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@13
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@15
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@17
    const/4 v5, 0x6

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1b
    move-result-object v1

    #@1c
    goto :goto_c
.end method

.method public getLabeledBy()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 7

    #@0
    .prologue
    .line 1342
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 1343
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_d

    #@b
    .line 1344
    const/4 v1, 0x0

    #@c
    .line 1347
    :goto_c
    return-object v1

    #@d
    .line 1346
    :cond_d
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@10
    move-result-object v0

    #@11
    .line 1347
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@13
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@15
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@17
    const/4 v5, 0x6

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1b
    move-result-object v1

    #@1c
    goto :goto_c
.end method

.method public getMovementGranularities()I
    .registers 2

    #@0
    .prologue
    .line 635
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@2
    return v0
.end method

.method public getPackageName()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1139
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getParent()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 7

    #@0
    .prologue
    .line 718
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 719
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_d

    #@b
    .line 720
    const/4 v1, 0x0

    #@c
    .line 723
    :goto_c
    return-object v1

    #@d
    .line 722
    :cond_d
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@10
    move-result-object v0

    #@11
    .line 723
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@13
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@15
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@17
    const/4 v5, 0x6

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1b
    move-result-object v1

    #@1c
    goto :goto_c
.end method

.method public getParentNodeId()J
    .registers 3

    #@0
    .prologue
    .line 733
    iget-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@2
    return-wide v0
.end method

.method public getSourceNodeId()J
    .registers 3

    #@0
    .prologue
    .line 1406
    iget-wide v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@2
    return-wide v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1191
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getWindowId()I
    .registers 2

    #@0
    .prologue
    .line 486
    iget v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 6

    #@0
    .prologue
    .line 1806
    const/16 v0, 0x1f

    #@2
    .line 1807
    .local v0, prime:I
    const/4 v1, 0x1

    #@3
    .line 1808
    .local v1, result:I
    iget-wide v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@5
    invoke-static {v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->getAccessibilityViewId(J)I

    #@8
    move-result v2

    #@9
    add-int/lit8 v1, v2, 0x1f

    #@b
    .line 1809
    mul-int/lit8 v2, v1, 0x1f

    #@d
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@f
    invoke-static {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->getVirtualDescendantId(J)I

    #@12
    move-result v3

    #@13
    add-int v1, v2, v3

    #@15
    .line 1810
    mul-int/lit8 v2, v1, 0x1f

    #@17
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@19
    add-int v1, v2, v3

    #@1b
    .line 1811
    return v1
.end method

.method public isAccessibilityFocused()Z
    .registers 2

    #@0
    .prologue
    .line 963
    const/16 v0, 0x400

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isCheckable()Z
    .registers 2

    #@0
    .prologue
    .line 838
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isChecked()Z
    .registers 2

    #@0
    .prologue
    .line 863
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isClickable()Z
    .registers 2

    #@0
    .prologue
    .line 1013
    const/16 v0, 0x20

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1063
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isFocusable()Z
    .registers 2

    #@0
    .prologue
    .line 888
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public isFocused()Z
    .registers 2

    #@0
    .prologue
    .line 913
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isLongClickable()Z
    .registers 2

    #@0
    .prologue
    .line 1038
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isPassword()Z
    .registers 2

    #@0
    .prologue
    .line 1088
    const/16 v0, 0x100

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isScrollable()Z
    .registers 2

    #@0
    .prologue
    .line 1113
    const/16 v0, 0x200

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isSealed()Z
    .registers 2

    #@0
    .prologue
    .line 1428
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSealed:Z

    #@2
    return v0
.end method

.method public isSelected()Z
    .registers 2

    #@0
    .prologue
    .line 988
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isVisibleToUser()Z
    .registers 2

    #@0
    .prologue
    .line 938
    const/16 v0, 0x800

    #@2
    invoke-direct {p0, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBooleanProperty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public performAction(I)Z
    .registers 9
    .parameter "action"

    #@0
    .prologue
    .line 651
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 652
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_d

    #@b
    .line 653
    const/4 v1, 0x0

    #@c
    .line 656
    :goto_c
    return v1

    #@d
    .line 655
    :cond_d
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@10
    move-result-object v0

    #@11
    .line 656
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@13
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@15
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@17
    const/4 v6, 0x0

    #@18
    move v5, p1

    #@19
    invoke-virtual/range {v0 .. v6}, Landroid/view/accessibility/AccessibilityInteractionClient;->performAccessibilityAction(IIJILandroid/os/Bundle;)Z

    #@1c
    move-result v1

    #@1d
    goto :goto_c
.end method

.method public performAction(ILandroid/os/Bundle;)Z
    .registers 10
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 674
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceSealed()V

    #@3
    .line 675
    iget-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@5
    invoke-direct {p0, v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->canPerformRequestOverConnection(J)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_d

    #@b
    .line 676
    const/4 v1, 0x0

    #@c
    .line 679
    :goto_c
    return v1

    #@d
    .line 678
    :cond_d
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@10
    move-result-object v0

    #@11
    .line 679
    .local v0, client:Landroid/view/accessibility/AccessibilityInteractionClient;
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@13
    iget v2, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@15
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@17
    move v5, p1

    #@18
    move-object v6, p2

    #@19
    invoke-virtual/range {v0 .. v6}, Landroid/view/accessibility/AccessibilityInteractionClient;->performAccessibilityAction(IIJILandroid/os/Bundle;)Z

    #@1c
    move-result v1

    #@1d
    goto :goto_c
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 1555
    iget-boolean v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mIsInPool:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1556
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Info already recycled!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1558
    :cond_c
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->clear()V

    #@f
    .line 1559
    sget-object v1, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 1560
    :try_start_12
    sget v0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolSize:I

    #@14
    const/16 v2, 0x32

    #@16
    if-gt v0, v2, :cond_27

    #@18
    .line 1561
    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1a
    iput-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mNext:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1c
    .line 1562
    sput-object p0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPool:Landroid/view/accessibility/AccessibilityNodeInfo;

    #@1e
    .line 1563
    const/4 v0, 0x1

    #@1f
    iput-boolean v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mIsInPool:Z

    #@21
    .line 1564
    sget v0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolSize:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Landroid/view/accessibility/AccessibilityNodeInfo;->sPoolSize:I

    #@27
    .line 1566
    :cond_27
    monitor-exit v1

    #@28
    .line 1567
    return-void

    #@29
    .line 1566
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v0
.end method

.method public setAccessibilityFocused(Z)V
    .registers 3
    .parameter "focused"

    #@0
    .prologue
    .line 979
    const/16 v0, 0x400

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 980
    return-void
.end method

.method public setBoundsInParent(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "bounds"

    #@0
    .prologue
    .line 801
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 802
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@5
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@7
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@9
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@b
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@10
    .line 803
    return-void
.end method

.method public setBoundsInScreen(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "bounds"

    #@0
    .prologue
    .line 828
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 829
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@5
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@7
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@9
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@b
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@10
    .line 830
    return-void
.end method

.method public setCheckable(Z)V
    .registers 3
    .parameter "checkable"

    #@0
    .prologue
    .line 854
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@4
    .line 855
    return-void
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "checked"

    #@0
    .prologue
    .line 879
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@4
    .line 880
    return-void
.end method

.method public setClassName(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "className"

    #@0
    .prologue
    .line 1181
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1182
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@5
    .line 1183
    return-void
.end method

.method public setClickable(Z)V
    .registers 3
    .parameter "clickable"

    #@0
    .prologue
    .line 1029
    const/16 v0, 0x20

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 1030
    return-void
.end method

.method public setConnectionId(I)V
    .registers 2
    .parameter "connectionId"

    #@0
    .prologue
    .line 1387
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1388
    iput p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@5
    .line 1389
    return-void
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "contentDescription"

    #@0
    .prologue
    .line 1233
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1234
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@5
    .line 1235
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1079
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 1080
    return-void
.end method

.method public setFocusable(Z)V
    .registers 3
    .parameter "focusable"

    #@0
    .prologue
    .line 904
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@4
    .line 905
    return-void
.end method

.method public setFocused(Z)V
    .registers 3
    .parameter "focused"

    #@0
    .prologue
    .line 929
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 930
    return-void
.end method

.method public setLabelFor(Landroid/view/View;)V
    .registers 3
    .parameter "labeled"

    #@0
    .prologue
    .line 1244
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabelFor(Landroid/view/View;I)V

    #@4
    .line 1245
    return-void
.end method

.method public setLabelFor(Landroid/view/View;I)V
    .registers 6
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    .line 1267
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1268
    if-eqz p1, :cond_10

    #@5
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@8
    move-result v0

    #@9
    .line 1270
    .local v0, rootAccessibilityViewId:I
    :goto_9
    invoke-static {v0, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@c
    move-result-wide v1

    #@d
    iput-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@f
    .line 1271
    return-void

    #@10
    .line 1268
    .end local v0           #rootAccessibilityViewId:I
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_9
.end method

.method public setLabeledBy(Landroid/view/View;)V
    .registers 3
    .parameter "label"

    #@0
    .prologue
    .line 1301
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabeledBy(Landroid/view/View;I)V

    #@4
    .line 1302
    return-void
.end method

.method public setLabeledBy(Landroid/view/View;I)V
    .registers 6
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    .line 1324
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1325
    if-eqz p1, :cond_10

    #@5
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@8
    move-result v0

    #@9
    .line 1327
    .local v0, rootAccessibilityViewId:I
    :goto_9
    invoke-static {v0, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@c
    move-result-wide v1

    #@d
    iput-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@f
    .line 1328
    return-void

    #@10
    .line 1325
    .end local v0           #rootAccessibilityViewId:I
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_9
.end method

.method public setLongClickable(Z)V
    .registers 3
    .parameter "longClickable"

    #@0
    .prologue
    .line 1054
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 1055
    return-void
.end method

.method public setMovementGranularities(I)V
    .registers 2
    .parameter "granularities"

    #@0
    .prologue
    .line 625
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 626
    iput p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@5
    .line 627
    return-void
.end method

.method public setPackageName(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 1155
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1156
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@5
    .line 1157
    return-void
.end method

.method public setParent(Landroid/view/View;)V
    .registers 3
    .parameter "parent"

    #@0
    .prologue
    .line 749
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;I)V

    #@4
    .line 750
    return-void
.end method

.method public setParent(Landroid/view/View;I)V
    .registers 6
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    .line 772
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 773
    if-eqz p1, :cond_10

    #@5
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@8
    move-result v0

    #@9
    .line 775
    .local v0, rootAccessibilityViewId:I
    :goto_9
    invoke-static {v0, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@c
    move-result-wide v1

    #@d
    iput-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@f
    .line 776
    return-void

    #@10
    .line 773
    .end local v0           #rootAccessibilityViewId:I
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_9
.end method

.method public setPassword(Z)V
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 1104
    const/16 v0, 0x100

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 1105
    return-void
.end method

.method public setScrollable(Z)V
    .registers 3
    .parameter "scrollable"

    #@0
    .prologue
    .line 1129
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1130
    const/16 v0, 0x200

    #@5
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@8
    .line 1131
    return-void
.end method

.method public setSealed(Z)V
    .registers 2
    .parameter "sealed"

    #@0
    .prologue
    .line 1417
    iput-boolean p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSealed:Z

    #@2
    .line 1418
    return-void
.end method

.method public setSelected(Z)V
    .registers 3
    .parameter "selected"

    #@0
    .prologue
    .line 1004
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 1005
    return-void
.end method

.method public setSource(Landroid/view/View;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 405
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    #@4
    .line 406
    return-void
.end method

.method public setSource(Landroid/view/View;I)V
    .registers 6
    .parameter "root"
    .parameter "virtualDescendantId"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 428
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@4
    .line 429
    if-eqz p1, :cond_19

    #@6
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityWindowId()I

    #@9
    move-result v1

    #@a
    :goto_a
    iput v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@c
    .line 430
    if-eqz p1, :cond_1b

    #@e
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityViewId()I

    #@11
    move-result v0

    #@12
    .line 432
    .local v0, rootAccessibilityViewId:I
    :goto_12
    invoke-static {v0, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->makeNodeId(II)J

    #@15
    move-result-wide v1

    #@16
    iput-wide v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@18
    .line 433
    return-void

    #@19
    .end local v0           #rootAccessibilityViewId:I
    :cond_19
    move v1, v2

    #@1a
    .line 429
    goto :goto_a

    #@1b
    :cond_1b
    move v0, v2

    #@1c
    .line 430
    goto :goto_12
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 1207
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->enforceNotSealed()V

    #@3
    .line 1208
    iput-object p1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@5
    .line 1209
    return-void
.end method

.method public setVisibleToUser(Z)V
    .registers 3
    .parameter "visibleToUser"

    #@0
    .prologue
    .line 954
    const/16 v0, 0x800

    #@2
    invoke-direct {p0, v0, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBooleanProperty(IZ)V

    #@5
    .line 955
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1816
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1817
    .local v2, builder:Ljava/lang/StringBuilder;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 1847
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "; boundsInParent: "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 1848
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "; boundsInScreen: "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    .line 1850
    const-string v3, "; packageName: "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@47
    .line 1851
    const-string v3, "; className: "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@52
    .line 1852
    const-string v3, "; text: "

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@5d
    .line 1853
    const-string v3, "; contentDescription: "

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    iget-object v4, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@68
    .line 1855
    const-string v3, "; checkable: "

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isCheckable()Z

    #@71
    move-result v4

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@75
    .line 1856
    const-string v3, "; checked: "

    #@77
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isChecked()Z

    #@7e
    move-result v4

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@82
    .line 1857
    const-string v3, "; focusable: "

    #@84
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isFocusable()Z

    #@8b
    move-result v4

    #@8c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8f
    .line 1858
    const-string v3, "; focused: "

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isFocused()Z

    #@98
    move-result v4

    #@99
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9c
    .line 1859
    const-string v3, "; selected: "

    #@9e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isSelected()Z

    #@a5
    move-result v4

    #@a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a9
    .line 1860
    const-string v3, "; clickable: "

    #@ab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isClickable()Z

    #@b2
    move-result v4

    #@b3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b6
    .line 1861
    const-string v3, "; longClickable: "

    #@b8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v3

    #@bc
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isLongClickable()Z

    #@bf
    move-result v4

    #@c0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c3
    .line 1862
    const-string v3, "; enabled: "

    #@c5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v3

    #@c9
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isEnabled()Z

    #@cc
    move-result v4

    #@cd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d0
    .line 1863
    const-string v3, "; password: "

    #@d2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v3

    #@d6
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isPassword()Z

    #@d9
    move-result v4

    #@da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@dd
    .line 1864
    new-instance v3, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v4, "; scrollable: "

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isScrollable()Z

    #@eb
    move-result v4

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v3

    #@f0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v3

    #@f4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    .line 1866
    const-string v3, "; ["

    #@f9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    .line 1867
    iget v1, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@fe
    .local v1, actionBits:I
    :cond_fe
    :goto_fe
    if-eqz v1, :cond_119

    #@100
    .line 1868
    const/4 v3, 0x1

    #@101
    invoke-static {v1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@104
    move-result v4

    #@105
    shl-int v0, v3, v4

    #@107
    .line 1869
    .local v0, action:I
    xor-int/lit8 v3, v0, -0x1

    #@109
    and-int/2addr v1, v3

    #@10a
    .line 1870
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->getActionSymbolicName(I)Ljava/lang/String;

    #@10d
    move-result-object v3

    #@10e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    .line 1871
    if-eqz v1, :cond_fe

    #@113
    .line 1872
    const-string v3, ", "

    #@115
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    goto :goto_fe

    #@119
    .line 1875
    .end local v0           #action:I
    :cond_119
    const-string v3, "]"

    #@11b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    .line 1877
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v3

    #@122
    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 1577
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->isSealed()Z

    #@3
    move-result v3

    #@4
    if-eqz v3, :cond_3e

    #@6
    const/4 v3, 0x1

    #@7
    :goto_7
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 1578
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mSourceNodeId:J

    #@c
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@f
    .line 1579
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mWindowId:I

    #@11
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1580
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mParentNodeId:J

    #@16
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@19
    .line 1581
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabelForId:J

    #@1b
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 1582
    iget-wide v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mLabeledById:J

    #@20
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 1583
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mConnectionId:I

    #@25
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 1585
    iget-object v0, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mChildNodeIds:Landroid/util/SparseLongArray;

    #@2a
    .line 1586
    .local v0, childIds:Landroid/util/SparseLongArray;
    invoke-virtual {v0}, Landroid/util/SparseLongArray;->size()I

    #@2d
    move-result v1

    #@2e
    .line 1587
    .local v1, childIdsSize:I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 1588
    const/4 v2, 0x0

    #@32
    .local v2, i:I
    :goto_32
    if-ge v2, v1, :cond_40

    #@34
    .line 1589
    invoke-virtual {v0, v2}, Landroid/util/SparseLongArray;->valueAt(I)J

    #@37
    move-result-wide v3

    #@38
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@3b
    .line 1588
    add-int/lit8 v2, v2, 0x1

    #@3d
    goto :goto_32

    #@3e
    .line 1577
    .end local v0           #childIds:Landroid/util/SparseLongArray;
    .end local v1           #childIdsSize:I
    .end local v2           #i:I
    :cond_3e
    const/4 v3, 0x0

    #@3f
    goto :goto_7

    #@40
    .line 1592
    .restart local v0       #childIds:Landroid/util/SparseLongArray;
    .restart local v1       #childIdsSize:I
    .restart local v2       #i:I
    :cond_40
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@42
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@44
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    .line 1593
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@49
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@4b
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4e
    .line 1594
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@50
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@52
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@55
    .line 1595
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInParent:Landroid/graphics/Rect;

    #@57
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@59
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5c
    .line 1597
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@5e
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@60
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@63
    .line 1598
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@65
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@67
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    .line 1599
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@6c
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@6e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@71
    .line 1600
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBoundsInScreen:Landroid/graphics/Rect;

    #@73
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@75
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@78
    .line 1602
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mActions:I

    #@7a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7d
    .line 1604
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mMovementGranularities:I

    #@7f
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@82
    .line 1606
    iget v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mBooleanProperties:I

    #@84
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@87
    .line 1608
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mPackageName:Ljava/lang/CharSequence;

    #@89
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeCharSequence(Ljava/lang/CharSequence;)V

    #@8c
    .line 1609
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mClassName:Ljava/lang/CharSequence;

    #@8e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeCharSequence(Ljava/lang/CharSequence;)V

    #@91
    .line 1610
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mText:Ljava/lang/CharSequence;

    #@93
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeCharSequence(Ljava/lang/CharSequence;)V

    #@96
    .line 1611
    iget-object v3, p0, Landroid/view/accessibility/AccessibilityNodeInfo;->mContentDescription:Ljava/lang/CharSequence;

    #@98
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeCharSequence(Ljava/lang/CharSequence;)V

    #@9b
    .line 1615
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    #@9e
    .line 1616
    return-void
.end method
