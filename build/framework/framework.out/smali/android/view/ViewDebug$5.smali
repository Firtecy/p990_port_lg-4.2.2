.class final Landroid/view/ViewDebug$5;
.super Ljava/lang/Object;
.source "ViewDebug.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/ViewDebug;->profileViewOperation(Landroid/view/View;Landroid/view/ViewDebug$ViewOperation;)J
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$duration:[J

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$operation:Landroid/view/ViewDebug$ViewOperation;


# direct methods
.method constructor <init>(Landroid/view/ViewDebug$ViewOperation;[JLjava/util/concurrent/CountDownLatch;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 596
    iput-object p1, p0, Landroid/view/ViewDebug$5;->val$operation:Landroid/view/ViewDebug$ViewOperation;

    #@2
    iput-object p2, p0, Landroid/view/ViewDebug$5;->val$duration:[J

    #@4
    iput-object p3, p0, Landroid/view/ViewDebug$5;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 599
    :try_start_0
    iget-object v3, p0, Landroid/view/ViewDebug$5;->val$operation:Landroid/view/ViewDebug$ViewOperation;

    #@2
    invoke-interface {v3}, Landroid/view/ViewDebug$ViewOperation;->pre()[Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 600
    .local v0, data:[Ljava/lang/Object;,"[TT;"
    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    #@9
    move-result-wide v1

    #@a
    .line 602
    .local v1, start:J
    iget-object v3, p0, Landroid/view/ViewDebug$5;->val$operation:Landroid/view/ViewDebug$ViewOperation;

    #@c
    invoke-interface {v3, v0}, Landroid/view/ViewDebug$ViewOperation;->run([Ljava/lang/Object;)V

    #@f
    .line 603
    iget-object v3, p0, Landroid/view/ViewDebug$5;->val$duration:[J

    #@11
    const/4 v4, 0x0

    #@12
    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    #@15
    move-result-wide v5

    #@16
    sub-long/2addr v5, v1

    #@17
    aput-wide v5, v3, v4

    #@19
    .line 605
    iget-object v3, p0, Landroid/view/ViewDebug$5;->val$operation:Landroid/view/ViewDebug$ViewOperation;

    #@1b
    invoke-interface {v3, v0}, Landroid/view/ViewDebug$ViewOperation;->post([Ljava/lang/Object;)V
    :try_end_1e
    .catchall {:try_start_0 .. :try_end_1e} :catchall_24

    #@1e
    .line 607
    iget-object v3, p0, Landroid/view/ViewDebug$5;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@20
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@23
    .line 609
    return-void

    #@24
    .line 607
    .end local v0           #data:[Ljava/lang/Object;,"[TT;"
    .end local v1           #start:J
    :catchall_24
    move-exception v3

    #@25
    iget-object v4, p0, Landroid/view/ViewDebug$5;->val$latch:Ljava/util/concurrent/CountDownLatch;

    #@27
    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@2a
    throw v3
.end method
