.class public Landroid/view/VolumePanel;
.super Landroid/os/Handler;
.source "VolumePanel.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/VolumePanel$WarningDialogReceiver;,
        Landroid/view/VolumePanel$StreamControl;,
        Landroid/view/VolumePanel$StreamResources;
    }
.end annotation


# static fields
.field private static final BEEP_DURATION:I = 0x96

.field public static final ENFORCE_SAFE_SOUND_DELAY:I = 0x32

.field private static final FREE_DELAY:I = 0x2710

.field private static LOGD:Z = false

.field private static final MAX_VOLUME:I = 0x64

.field private static final MSG_DISMISS_SAFE_VOLUME_WARNING:I = 0xd

.field private static final MSG_DISPLAY_QUIETMODE_WARNING:I = 0xc

.field private static final MSG_DISPLAY_SAFE_VOLUME_WARNING:I = 0xb

.field private static final MSG_FREE_RESOURCES:I = 0x1

.field private static final MSG_MUTE_CHANGED:I = 0x7

.field private static final MSG_PLAY_SOUND:I = 0x2

.field private static final MSG_REMOTE_VOLUME_CHANGED:I = 0x8

.field private static final MSG_REMOTE_VOLUME_UPDATE_IF_SHOWN:I = 0x9

.field private static final MSG_RINGER_MODE_CHANGED:I = 0x6

.field private static final MSG_SET_ENFORCE_SAFE_VOLUME:I = 0xe

.field private static final MSG_SLIDER_VISIBILITY_CHANGED:I = 0xa

.field private static final MSG_STOP_SOUNDS:I = 0x3

.field private static final MSG_TIMEOUT:I = 0x5

.field private static final MSG_VIBRATE:I = 0x4

.field private static final MSG_VOLUME_CHANGED:I = 0x0

.field public static final PLAY_SOUND_DELAY:I = 0x32

.field private static final STREAMS:[Landroid/view/VolumePanel$StreamResources; = null

.field private static final STREAMS_EXPANDED:[Landroid/view/VolumePanel$StreamResources; = null

.field private static final STREAM_MASTER:I = -0x64

.field private static final TAG:Ljava/lang/String; = "VolumePanel"

.field private static final TIMEOUT_DELAY:I = 0xbb8

.field public static final VIBRATE_DELAY:I = 0x32

.field private static final VIBRATE_DURATION:I = 0xc8

.field private static sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

.field private static sConfirmSafeVolumeLock:Ljava/lang/Object;


# instance fields
.field private mActiveStreamType:I

.field private mAudioManager:Landroid/media/AudioManager;

.field protected mAudioService:Landroid/media/AudioService;

.field protected mContext:Landroid/content/Context;

.field private final mDialog:Landroid/app/Dialog;

.field private final mDivider:Landroid/view/View;

.field private mIsQuickCoverClose:Z

.field private mIsSoundException:Z

.field private final mMoreButton:Landroid/widget/ImageView;

.field private final mPanel:Landroid/view/ViewGroup;

.field private final mPlayMasterStreamTones:Z

.field private mRingIsSilent:Z

.field private mShowCombinedVolumes:Z

.field private final mSliderGroup:Landroid/view/ViewGroup;

.field private mStreamControls:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/VolumePanel$StreamControl;",
            ">;"
        }
    .end annotation
.end field

.field private mToneGenerators:[Landroid/media/ToneGenerator;

.field private mVibrator:Landroid/os/Vibrator;

.field private final mView:Landroid/view/View;

.field private mVoiceCapable:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x1

    #@5
    .line 83
    sput-boolean v3, Landroid/view/VolumePanel;->LOGD:Z

    #@7
    .line 267
    const/16 v0, 0xb

    #@9
    new-array v0, v0, [Landroid/view/VolumePanel$StreamResources;

    #@b
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->BluetoothSCOStream:Landroid/view/VolumePanel$StreamResources;

    #@d
    aput-object v1, v0, v4

    #@f
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->RingerStream:Landroid/view/VolumePanel$StreamResources;

    #@11
    aput-object v1, v0, v3

    #@13
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->VoiceStream:Landroid/view/VolumePanel$StreamResources;

    #@15
    aput-object v1, v0, v5

    #@17
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->MediaStream:Landroid/view/VolumePanel$StreamResources;

    #@19
    aput-object v1, v0, v6

    #@1b
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->NotificationStream:Landroid/view/VolumePanel$StreamResources;

    #@1d
    aput-object v1, v0, v7

    #@1f
    const/4 v1, 0x5

    #@20
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->AlarmStream:Landroid/view/VolumePanel$StreamResources;

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x6

    #@25
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->FMStream:Landroid/view/VolumePanel$StreamResources;

    #@27
    aput-object v2, v0, v1

    #@29
    const/4 v1, 0x7

    #@2a
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->MasterStream:Landroid/view/VolumePanel$StreamResources;

    #@2c
    aput-object v2, v0, v1

    #@2e
    const/16 v1, 0x8

    #@30
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->RemoteStream:Landroid/view/VolumePanel$StreamResources;

    #@32
    aput-object v2, v0, v1

    #@34
    const/16 v1, 0x9

    #@36
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->DMBStream:Landroid/view/VolumePanel$StreamResources;

    #@38
    aput-object v2, v0, v1

    #@3a
    const/16 v1, 0xa

    #@3c
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->SYSTEMStream:Landroid/view/VolumePanel$StreamResources;

    #@3e
    aput-object v2, v0, v1

    #@40
    sput-object v0, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@42
    .line 284
    new-array v0, v7, [Landroid/view/VolumePanel$StreamResources;

    #@44
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->RingerStream:Landroid/view/VolumePanel$StreamResources;

    #@46
    aput-object v1, v0, v4

    #@48
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->NotificationStream:Landroid/view/VolumePanel$StreamResources;

    #@4a
    aput-object v1, v0, v3

    #@4c
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->SYSTEMStream:Landroid/view/VolumePanel$StreamResources;

    #@4e
    aput-object v1, v0, v5

    #@50
    sget-object v1, Landroid/view/VolumePanel$StreamResources;->MediaStream:Landroid/view/VolumePanel$StreamResources;

    #@52
    aput-object v1, v0, v6

    #@54
    sput-object v0, Landroid/view/VolumePanel;->STREAMS_EXPANDED:[Landroid/view/VolumePanel$StreamResources;

    #@56
    .line 311
    new-instance v0, Ljava/lang/Object;

    #@58
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5b
    sput-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    #@5d
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioService;)V
    .registers 15
    .parameter "context"
    .parameter "volumeService"

    #@0
    .prologue
    .line 342
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 142
    const/4 v9, 0x0

    #@4
    iput-boolean v9, p0, Landroid/view/VolumePanel;->mIsSoundException:Z

    #@6
    .line 160
    const/4 v9, -0x1

    #@7
    iput v9, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@9
    .line 170
    const/4 v9, 0x0

    #@a
    iput-boolean v9, p0, Landroid/view/VolumePanel;->mIsQuickCoverClose:Z

    #@c
    .line 343
    iput-object p1, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@e
    .line 344
    const-string v9, "audio"

    #@10
    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v9

    #@14
    check-cast v9, Landroid/media/AudioManager;

    #@16
    iput-object v9, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@18
    .line 345
    iput-object p2, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@1a
    .line 348
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v9

    #@1e
    const v10, 0x1110010

    #@21
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@24
    move-result v6

    #@25
    .line 350
    .local v6, useMasterVolume:Z
    if-eqz v6, :cond_3f

    #@27
    .line 351
    const/4 v0, 0x0

    #@28
    .local v0, i:I
    :goto_28
    sget-object v9, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@2a
    array-length v9, v9

    #@2b
    if-ge v0, v9, :cond_3f

    #@2d
    .line 352
    sget-object v9, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@2f
    aget-object v5, v9, v0

    #@31
    .line 353
    .local v5, streamRes:Landroid/view/VolumePanel$StreamResources;
    iget v9, v5, Landroid/view/VolumePanel$StreamResources;->streamType:I

    #@33
    const/16 v10, -0x64

    #@35
    if-ne v9, v10, :cond_3d

    #@37
    const/4 v9, 0x1

    #@38
    :goto_38
    iput-boolean v9, v5, Landroid/view/VolumePanel$StreamResources;->show:Z

    #@3a
    .line 351
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_28

    #@3d
    .line 353
    :cond_3d
    const/4 v9, 0x0

    #@3e
    goto :goto_38

    #@3f
    .line 357
    .end local v0           #i:I
    .end local v5           #streamRes:Landroid/view/VolumePanel$StreamResources;
    :cond_3f
    const-string/jumbo v9, "layout_inflater"

    #@42
    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@45
    move-result-object v1

    #@46
    check-cast v1, Landroid/view/LayoutInflater;

    #@48
    .line 359
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v9, 0x10900e6

    #@4b
    const/4 v10, 0x0

    #@4c
    invoke-virtual {v1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@4f
    move-result-object v7

    #@50
    iput-object v7, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@52
    .line 360
    .local v7, view:Landroid/view/View;
    iget-object v9, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@54
    new-instance v10, Landroid/view/VolumePanel$1;

    #@56
    invoke-direct {v10, p0}, Landroid/view/VolumePanel$1;-><init>(Landroid/view/VolumePanel;)V

    #@59
    invoke-virtual {v9, v10}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@5c
    .line 366
    iget-object v9, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@5e
    const v10, 0x10203b7

    #@61
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@64
    move-result-object v9

    #@65
    check-cast v9, Landroid/view/ViewGroup;

    #@67
    iput-object v9, p0, Landroid/view/VolumePanel;->mPanel:Landroid/view/ViewGroup;

    #@69
    .line 367
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_RESOURCE:Z

    #@6b
    if-eqz v9, :cond_75

    #@6d
    .line 368
    iget-object v9, p0, Landroid/view/VolumePanel;->mPanel:Landroid/view/ViewGroup;

    #@6f
    const v10, 0x20201ba

    #@72
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    #@75
    .line 371
    :cond_75
    iget-object v9, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@77
    const v10, 0x10203b8

    #@7a
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@7d
    move-result-object v9

    #@7e
    check-cast v9, Landroid/view/ViewGroup;

    #@80
    iput-object v9, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@82
    .line 372
    iget-object v9, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@84
    const v10, 0x102032d

    #@87
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@8a
    move-result-object v9

    #@8b
    check-cast v9, Landroid/widget/ImageView;

    #@8d
    iput-object v9, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@8f
    .line 373
    iget-object v9, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@91
    const v10, 0x2020369

    #@94
    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    #@97
    .line 374
    iget-object v9, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@99
    iget-object v10, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@9b
    const v11, 0x20902ff

    #@9e
    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v10

    #@a2
    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@a5
    .line 375
    iget-object v9, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@a7
    const v10, 0x10203b9

    #@aa
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@ad
    move-result-object v9

    #@ae
    check-cast v9, Landroid/widget/ImageView;

    #@b0
    iput-object v9, p0, Landroid/view/VolumePanel;->mDivider:Landroid/view/View;

    #@b2
    .line 377
    new-instance v9, Landroid/view/VolumePanel$2;

    #@b4
    const v10, 0x10302fe

    #@b7
    invoke-direct {v9, p0, p1, v10}, Landroid/view/VolumePanel$2;-><init>(Landroid/view/VolumePanel;Landroid/content/Context;I)V

    #@ba
    iput-object v9, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@bc
    .line 386
    iget-object v9, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@be
    const-string v10, "Volume control"

    #@c0
    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    #@c3
    .line 387
    iget-object v9, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@c5
    iget-object v10, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@c7
    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    #@ca
    .line 388
    iget-object v9, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@cc
    new-instance v10, Landroid/view/VolumePanel$3;

    #@ce
    invoke-direct {v10, p0}, Landroid/view/VolumePanel$3;-><init>(Landroid/view/VolumePanel;)V

    #@d1
    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@d4
    .line 400
    iget-object v9, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@d6
    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@d9
    move-result-object v8

    #@da
    .line 401
    .local v8, window:Landroid/view/Window;
    const/16 v9, 0x30

    #@dc
    invoke-virtual {v8, v9}, Landroid/view/Window;->setGravity(I)V

    #@df
    .line 402
    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@e2
    move-result-object v2

    #@e3
    .line 403
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    const/4 v9, 0x0

    #@e4
    iput-object v9, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@e6
    .line 405
    iget-object v9, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@e8
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@eb
    move-result-object v9

    #@ec
    const v10, 0x105004f

    #@ef
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    #@f2
    move-result v9

    #@f3
    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    #@f5
    .line 407
    const/16 v9, 0x7e4

    #@f7
    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@f9
    .line 408
    const/4 v9, -0x2

    #@fa
    iput v9, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@fc
    .line 409
    const/4 v9, -0x2

    #@fd
    iput v9, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@ff
    .line 410
    iget v9, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@101
    or-int/lit8 v9, v9, 0x20

    #@103
    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@105
    .line 411
    invoke-virtual {v8, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@108
    .line 412
    const v9, 0x40028

    #@10b
    invoke-virtual {v8, v9}, Landroid/view/Window;->addFlags(I)V

    #@10e
    .line 415
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@111
    move-result v9

    #@112
    new-array v9, v9, [Landroid/media/ToneGenerator;

    #@114
    iput-object v9, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@116
    .line 416
    const-string/jumbo v9, "vibrator"

    #@119
    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11c
    move-result-object v9

    #@11d
    check-cast v9, Landroid/os/Vibrator;

    #@11f
    iput-object v9, p0, Landroid/view/VolumePanel;->mVibrator:Landroid/os/Vibrator;

    #@121
    .line 418
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@124
    move-result-object v9

    #@125
    const v10, 0x1110030

    #@128
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@12b
    move-result v9

    #@12c
    iput-boolean v9, p0, Landroid/view/VolumePanel;->mVoiceCapable:Z

    #@12e
    .line 420
    const/4 v9, 0x1

    #@12f
    iput-boolean v9, p0, Landroid/view/VolumePanel;->mShowCombinedVolumes:Z

    #@131
    .line 422
    iget-boolean v9, p0, Landroid/view/VolumePanel;->mShowCombinedVolumes:Z

    #@133
    if-nez v9, :cond_176

    #@135
    .line 423
    iget-object v9, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@137
    const/16 v10, 0x8

    #@139
    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    #@13c
    .line 424
    iget-object v9, p0, Landroid/view/VolumePanel;->mDivider:Landroid/view/View;

    #@13e
    const/16 v10, 0x8

    #@140
    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    #@143
    .line 429
    :goto_143
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@146
    move-result-object v9

    #@147
    const v10, 0x1110010

    #@14a
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@14d
    move-result v4

    #@14e
    .line 431
    .local v4, masterVolumeOnly:Z
    iget-object v9, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@150
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@153
    move-result-object v9

    #@154
    const v10, 0x1110011

    #@157
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@15a
    move-result v3

    #@15b
    .line 434
    .local v3, masterVolumeKeySounds:Z
    invoke-direct {p0}, Landroid/view/VolumePanel;->listenToScreenStatus()V

    #@15e
    .line 438
    invoke-direct {p0}, Landroid/view/VolumePanel;->listenQuickCoverStatus()V

    #@161
    .line 441
    if-eqz v4, :cond_17c

    #@163
    if-eqz v3, :cond_17c

    #@165
    const/4 v9, 0x1

    #@166
    :goto_166
    iput-boolean v9, p0, Landroid/view/VolumePanel;->mPlayMasterStreamTones:Z

    #@168
    .line 443
    invoke-direct {p0}, Landroid/view/VolumePanel;->listenToRingerMode()V

    #@16b
    .line 446
    const-string/jumbo v9, "ro.lge.audio_soundexception"

    #@16e
    const/4 v10, 0x0

    #@16f
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@172
    move-result v9

    #@173
    iput-boolean v9, p0, Landroid/view/VolumePanel;->mIsSoundException:Z

    #@175
    .line 448
    return-void

    #@176
    .line 426
    .end local v3           #masterVolumeKeySounds:Z
    .end local v4           #masterVolumeOnly:Z
    :cond_176
    iget-object v9, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@178
    invoke-virtual {v9, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@17b
    goto :goto_143

    #@17c
    .line 441
    .restart local v3       #masterVolumeKeySounds:Z
    .restart local v4       #masterVolumeOnly:Z
    :cond_17c
    const/4 v9, 0x0

    #@17d
    goto :goto_166
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 80
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1000()Z
    .registers 1

    #@0
    .prologue
    .line 80
    sget-boolean v0, Landroid/view/VolumePanel;->LOGD:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    sput-object p0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@2
    return-object p0
.end method

.method static synthetic access$1102(Landroid/view/VolumePanel;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    iput-boolean p1, p0, Landroid/view/VolumePanel;->mIsQuickCoverClose:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/view/VolumePanel;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Landroid/view/VolumePanel;->resetTimeout()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/view/VolumePanel;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Landroid/view/VolumePanel;->forceTimeout()V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/view/VolumePanel;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget v0, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/view/VolumePanel;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    iput p1, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@2
    return p1
.end method

.method static synthetic access$500(Landroid/view/VolumePanel;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget-boolean v0, p0, Landroid/view/VolumePanel;->mShowCombinedVolumes:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/view/VolumePanel;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/view/VolumePanel;->collapse(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/view/VolumePanel;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/view/VolumePanel;)Landroid/app/Dialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@2
    return-object v0
.end method

.method private addOtherVolumes()V
    .registers 6

    #@0
    .prologue
    .line 575
    iget-boolean v3, p0, Landroid/view/VolumePanel;->mShowCombinedVolumes:Z

    #@2
    if-nez v3, :cond_5

    #@4
    .line 591
    :cond_4
    return-void

    #@5
    .line 578
    :cond_5
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    sget-object v3, Landroid/view/VolumePanel;->STREAMS_EXPANDED:[Landroid/view/VolumePanel$StreamResources;

    #@8
    array-length v3, v3

    #@9
    if-ge v0, v3, :cond_4

    #@b
    .line 580
    sget-object v3, Landroid/view/VolumePanel;->STREAMS_EXPANDED:[Landroid/view/VolumePanel$StreamResources;

    #@d
    aget-object v3, v3, v0

    #@f
    iget v2, v3, Landroid/view/VolumePanel$StreamResources;->streamType:I

    #@11
    .line 581
    .local v2, streamType:I
    iget v3, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@13
    if-eq v2, v3, :cond_27

    #@15
    const/4 v3, 0x2

    #@16
    if-ne v2, v3, :cond_1f

    #@18
    iget v3, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@1a
    if-eqz v3, :cond_27

    #@1c
    const/4 v3, 0x6

    #@1d
    if-eq v2, v3, :cond_27

    #@1f
    :cond_1f
    const/4 v3, 0x5

    #@20
    if-ne v2, v3, :cond_2a

    #@22
    iget v3, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@24
    const/4 v4, 0x4

    #@25
    if-ne v3, v4, :cond_2a

    #@27
    .line 578
    :cond_27
    :goto_27
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_6

    #@2a
    .line 587
    :cond_2a
    iget-object v3, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@2c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v1

    #@34
    check-cast v1, Landroid/view/VolumePanel$StreamControl;

    #@36
    .line 588
    .local v1, sc:Landroid/view/VolumePanel$StreamControl;
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@38
    iget-object v4, v1, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@3a
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@3d
    .line 589
    invoke-direct {p0, v1}, Landroid/view/VolumePanel;->updateSlider(Landroid/view/VolumePanel$StreamControl;)V

    #@40
    goto :goto_27
.end method

.method private collapse(I)V
    .registers 8
    .parameter "streamType"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v4, 0x0

    #@3
    .line 652
    if-eqz p1, :cond_f

    #@5
    const/4 v2, 0x6

    #@6
    if-eq p1, v2, :cond_f

    #@8
    iget-boolean v2, p0, Landroid/view/VolumePanel;->mIsQuickCoverClose:Z

    #@a
    if-eqz v2, :cond_3c

    #@c
    const/4 v2, 0x3

    #@d
    if-ne p1, v2, :cond_3c

    #@f
    .line 654
    :cond_f
    iget-object v2, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@11
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    #@14
    .line 656
    iget-boolean v2, p0, Landroid/view/VolumePanel;->mIsQuickCoverClose:Z

    #@16
    if-eqz v2, :cond_35

    #@18
    .line 657
    iget-object v2, p0, Landroid/view/VolumePanel;->mDivider:Landroid/view/View;

    #@1a
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    #@1d
    .line 668
    :goto_1d
    invoke-direct {p0, v4}, Landroid/view/VolumePanel;->showDescription(Z)V

    #@20
    .line 670
    iget-object v2, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@22
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    #@25
    move-result v0

    #@26
    .line 671
    .local v0, count:I
    const/4 v1, 0x1

    #@27
    .local v1, i:I
    :goto_27
    if-ge v1, v0, :cond_47

    #@29
    .line 672
    iget-object v2, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@2b
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    #@32
    .line 671
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_27

    #@35
    .line 659
    .end local v0           #count:I
    .end local v1           #i:I
    :cond_35
    iget-object v2, p0, Landroid/view/VolumePanel;->mDivider:Landroid/view/View;

    #@37
    const/4 v3, 0x4

    #@38
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@3b
    goto :goto_1d

    #@3c
    .line 662
    :cond_3c
    iget-object v2, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@3e
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@41
    .line 663
    iget-object v2, p0, Landroid/view/VolumePanel;->mDivider:Landroid/view/View;

    #@43
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    #@46
    goto :goto_1d

    #@47
    .line 674
    .restart local v0       #count:I
    .restart local v1       #i:I
    :cond_47
    return-void
.end method

.method private createSliders()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/16 v9, 0x8

    #@3
    .line 507
    iget-object v7, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@5
    const-string/jumbo v8, "layout_inflater"

    #@8
    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/view/LayoutInflater;

    #@e
    .line 509
    .local v1, inflater:Landroid/view/LayoutInflater;
    new-instance v7, Ljava/util/HashMap;

    #@10
    sget-object v8, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@12
    array-length v8, v8

    #@13
    invoke-direct {v7, v8}, Ljava/util/HashMap;-><init>(I)V

    #@16
    iput-object v7, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@18
    .line 510
    iget-object v7, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v3

    #@1e
    .line 511
    .local v3, res:Landroid/content/res/Resources;
    const/4 v0, 0x0

    #@1f
    .local v0, i:I
    :goto_1f
    sget-object v7, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@21
    array-length v7, v7

    #@22
    if-ge v0, v7, :cond_d6

    #@24
    .line 512
    sget-object v7, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@26
    aget-object v5, v7, v0

    #@28
    .line 513
    .local v5, streamRes:Landroid/view/VolumePanel$StreamResources;
    iget v6, v5, Landroid/view/VolumePanel$StreamResources;->streamType:I

    #@2a
    .line 519
    .local v6, streamType:I
    new-instance v4, Landroid/view/VolumePanel$StreamControl;

    #@2c
    invoke-direct {v4, p0, v10}, Landroid/view/VolumePanel$StreamControl;-><init>(Landroid/view/VolumePanel;Landroid/view/VolumePanel$1;)V

    #@2f
    .line 520
    .local v4, sc:Landroid/view/VolumePanel$StreamControl;
    iput v6, v4, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@31
    .line 521
    const v7, 0x10900e7

    #@34
    invoke-virtual {v1, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@37
    move-result-object v7

    #@38
    check-cast v7, Landroid/view/ViewGroup;

    #@3a
    iput-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@3c
    .line 522
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@3e
    invoke-virtual {v7, v4}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    #@41
    .line 523
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@43
    const v8, 0x10203bb

    #@46
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@49
    move-result-object v7

    #@4a
    check-cast v7, Landroid/widget/ImageView;

    #@4c
    iput-object v7, v4, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@4e
    .line 524
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@50
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    #@53
    .line 525
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@55
    iget v8, v5, Landroid/view/VolumePanel$StreamResources;->descRes:I

    #@57
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v8

    #@5b
    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@5e
    .line 526
    iget v7, v5, Landroid/view/VolumePanel$StreamResources;->iconRes:I

    #@60
    iput v7, v4, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@62
    .line 527
    iget v7, v5, Landroid/view/VolumePanel$StreamResources;->iconMuteRes:I

    #@64
    iput v7, v4, Landroid/view/VolumePanel$StreamControl;->iconMuteRes:I

    #@66
    .line 528
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@68
    iget v8, v4, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@6a
    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    #@6d
    .line 529
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@6f
    const v8, 0x102035a

    #@72
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@75
    move-result-object v7

    #@76
    check-cast v7, Landroid/widget/SeekBar;

    #@78
    iput-object v7, v4, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@7a
    .line 530
    const/4 v7, 0x6

    #@7b
    if-eq v6, v7, :cond_7f

    #@7d
    if-nez v6, :cond_d4

    #@7f
    :cond_7f
    const/4 v2, 0x1

    #@80
    .line 532
    .local v2, plusOne:I
    :goto_80
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@82
    invoke-direct {p0, v6}, Landroid/view/VolumePanel;->getStreamMaxVolume(I)I

    #@85
    move-result v8

    #@86
    add-int/2addr v8, v2

    #@87
    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setMax(I)V

    #@8a
    .line 533
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@8c
    invoke-virtual {v7, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    #@8f
    .line 534
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@91
    invoke-virtual {v7, v4}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    #@94
    .line 536
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@96
    if-eqz v7, :cond_c7

    #@98
    .line 537
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@9a
    const v8, 0x10203ba

    #@9d
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@a0
    move-result-object v7

    #@a1
    check-cast v7, Landroid/widget/TextView;

    #@a3
    iput-object v7, v4, Landroid/view/VolumePanel$StreamControl;->description:Landroid/widget/TextView;

    #@a5
    .line 538
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->description:Landroid/widget/TextView;

    #@a7
    iget v8, v5, Landroid/view/VolumePanel$StreamResources;->descRes:I

    #@a9
    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b0
    .line 539
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->description:Landroid/widget/TextView;

    #@b2
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    #@b5
    .line 541
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@b7
    const v8, 0x10203bc

    #@ba
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    #@bd
    move-result-object v7

    #@be
    check-cast v7, Landroid/widget/ImageView;

    #@c0
    iput-object v7, v4, Landroid/view/VolumePanel$StreamControl;->hDivider:Landroid/widget/ImageView;

    #@c2
    .line 542
    iget-object v7, v4, Landroid/view/VolumePanel$StreamControl;->hDivider:Landroid/widget/ImageView;

    #@c4
    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    #@c7
    .line 546
    :cond_c7
    iget-object v7, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@c9
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cc
    move-result-object v8

    #@cd
    invoke-virtual {v7, v8, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d0
    .line 511
    add-int/lit8 v0, v0, 0x1

    #@d2
    goto/16 :goto_1f

    #@d4
    .line 530
    .end local v2           #plusOne:I
    :cond_d4
    const/4 v2, 0x0

    #@d5
    goto :goto_80

    #@d6
    .line 548
    .end local v4           #sc:Landroid/view/VolumePanel$StreamControl;
    .end local v5           #streamRes:Landroid/view/VolumePanel$StreamResources;
    .end local v6           #streamType:I
    :cond_d6
    return-void
.end method

.method private expand()V
    .registers 5

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    .line 633
    iget-object v2, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@4
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    #@7
    move-result v0

    #@8
    .line 635
    .local v0, count:I
    iget-object v2, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@a
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@d
    .line 636
    iget-object v2, p0, Landroid/view/VolumePanel;->mDivider:Landroid/view/View;

    #@f
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@12
    .line 638
    const/4 v2, 0x1

    #@13
    invoke-direct {p0, v2}, Landroid/view/VolumePanel;->showDescription(Z)V

    #@16
    .line 641
    const/4 v1, 0x0

    #@17
    .local v1, i:I
    :goto_17
    if-ge v1, v0, :cond_26

    #@19
    .line 642
    iget-object v2, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@1b
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@1e
    move-result-object v2

    #@1f
    const/4 v3, 0x0

    #@20
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@23
    .line 641
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_17

    #@26
    .line 648
    :cond_26
    return-void
.end method

.method private forceTimeout()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x5

    #@1
    .line 1404
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@4
    .line 1405
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->sendMessage(Landroid/os/Message;)Z

    #@b
    .line 1406
    return-void
.end method

.method private getOrCreateToneGenerator(I)Landroid/media/ToneGenerator;
    .registers 6
    .parameter "streamType"

    #@0
    .prologue
    .line 1207
    const/16 v1, -0x64

    #@2
    if-ne p1, v1, :cond_9

    #@4
    .line 1211
    iget-boolean v1, p0, Landroid/view/VolumePanel;->mPlayMasterStreamTones:Z

    #@6
    if-eqz v1, :cond_21

    #@8
    .line 1212
    const/4 p1, 0x1

    #@9
    .line 1217
    :cond_9
    monitor-enter p0

    #@a
    .line 1218
    :try_start_a
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@c
    aget-object v1, v1, p1
    :try_end_e
    .catchall {:try_start_a .. :try_end_e} :catchall_41

    #@e
    if-nez v1, :cond_1b

    #@10
    .line 1220
    :try_start_10
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@12
    new-instance v2, Landroid/media/ToneGenerator;

    #@14
    const/16 v3, 0x64

    #@16
    invoke-direct {v2, p1, v3}, Landroid/media/ToneGenerator;-><init>(II)V

    #@19
    aput-object v2, v1, p1
    :try_end_1b
    .catchall {:try_start_10 .. :try_end_1b} :catchall_41
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_1b} :catch_23

    #@1b
    .line 1228
    :cond_1b
    :goto_1b
    :try_start_1b
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@1d
    aget-object v1, v1, p1

    #@1f
    monitor-exit p0

    #@20
    :goto_20
    return-object v1

    #@21
    .line 1214
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_20

    #@23
    .line 1221
    :catch_23
    move-exception v0

    #@24
    .line 1222
    .local v0, e:Ljava/lang/RuntimeException;
    sget-boolean v1, Landroid/view/VolumePanel;->LOGD:Z

    #@26
    if-eqz v1, :cond_1b

    #@28
    .line 1223
    const-string v1, "VolumePanel"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "ToneGenerator constructor failed with RuntimeException: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_1b

    #@41
    .line 1229
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catchall_41
    move-exception v1

    #@42
    monitor-exit p0
    :try_end_43
    .catchall {:try_start_1b .. :try_end_43} :catchall_41

    #@43
    throw v1
.end method

.method private getStreamMaxVolume(I)I
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 477
    const/16 v0, -0x64

    #@2
    if-ne p1, v0, :cond_b

    #@4
    .line 478
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@6
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterMaxVolume()I

    #@9
    move-result v0

    #@a
    .line 482
    :goto_a
    return v0

    #@b
    .line 479
    :cond_b
    const/16 v0, -0xc8

    #@d
    if-ne p1, v0, :cond_16

    #@f
    .line 480
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@11
    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamMaxVolume()I

    #@14
    move-result v0

    #@15
    goto :goto_a

    #@16
    .line 482
    :cond_16
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@18
    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    #@1b
    move-result v0

    #@1c
    goto :goto_a
.end method

.method private getStreamVolume(I)I
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 487
    const/16 v0, -0x64

    #@2
    if-ne p1, v0, :cond_b

    #@4
    .line 488
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@6
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterVolume()I

    #@9
    move-result v0

    #@a
    .line 492
    :goto_a
    return v0

    #@b
    .line 489
    :cond_b
    const/16 v0, -0xc8

    #@d
    if-ne p1, v0, :cond_16

    #@f
    .line 490
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@11
    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamVolume()I

    #@14
    move-result v0

    #@15
    goto :goto_a

    #@16
    .line 492
    :cond_16
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@18
    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@1b
    move-result v0

    #@1c
    goto :goto_a
.end method

.method private isExpanded()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 623
    iget-object v2, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@3
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    #@6
    move-result v0

    #@7
    .line 625
    .local v0, count:I
    if-le v0, v1, :cond_18

    #@9
    iget-object v2, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@b
    add-int/lit8 v3, v0, -0x1

    #@d
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_18

    #@17
    .line 628
    :goto_17
    return v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method

.method private isMuted(I)Z
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 467
    const/16 v0, -0x64

    #@2
    if-ne p1, v0, :cond_b

    #@4
    .line 468
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@6
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMasterMute()Z

    #@9
    move-result v0

    #@a
    .line 472
    :goto_a
    return v0

    #@b
    .line 469
    :cond_b
    const/16 v0, -0xc8

    #@d
    if-ne p1, v0, :cond_1b

    #@f
    .line 470
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@11
    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamVolume()I

    #@14
    move-result v0

    #@15
    if-gtz v0, :cond_19

    #@17
    const/4 v0, 0x1

    #@18
    goto :goto_a

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_a

    #@1b
    .line 472
    :cond_1b
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@1d
    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    #@20
    move-result v0

    #@21
    goto :goto_a
.end method

.method private listenQuickCoverStatus()V
    .registers 4

    #@0
    .prologue
    .line 1523
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 1524
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 1525
    iget-object v1, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@c
    new-instance v2, Landroid/view/VolumePanel$7;

    #@e
    invoke-direct {v2, p0}, Landroid/view/VolumePanel$7;-><init>(Landroid/view/VolumePanel;)V

    #@11
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@14
    .line 1551
    return-void
.end method

.method private listenToRingerMode()V
    .registers 4

    #@0
    .prologue
    .line 451
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 452
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 453
    iget-object v1, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@c
    new-instance v2, Landroid/view/VolumePanel$4;

    #@e
    invoke-direct {v2, p0}, Landroid/view/VolumePanel$4;-><init>(Landroid/view/VolumePanel;)V

    #@11
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@14
    .line 464
    return-void
.end method

.method private listenToScreenStatus()V
    .registers 4

    #@0
    .prologue
    .line 1499
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 1500
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 1501
    iget-object v1, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@c
    new-instance v2, Landroid/view/VolumePanel$6;

    #@e
    invoke-direct {v2, p0}, Landroid/view/VolumePanel$6;-><init>(Landroid/view/VolumePanel;)V

    #@11
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@14
    .line 1515
    return-void
.end method

.method private reorderSliders(I)V
    .registers 6
    .parameter "activeStreamType"

    #@0
    .prologue
    .line 552
    invoke-direct {p0}, Landroid/view/VolumePanel;->isExpanded()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_f

    #@6
    .line 553
    const-string v1, "VolumePanel"

    #@8
    const-string/jumbo v2, "reorderSliders returnm, isExpanded() true"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 572
    :goto_e
    return-void

    #@f
    .line 558
    :cond_f
    iget-object v1, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@11
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    #@14
    .line 560
    iget-object v1, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@16
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@20
    .line 561
    .local v0, active:Landroid/view/VolumePanel$StreamControl;
    if-nez v0, :cond_41

    #@22
    .line 562
    const-string v1, "VolumePanel"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Missing stream type! - "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 563
    const/4 v1, -0x1

    #@3b
    iput v1, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@3d
    .line 571
    :goto_3d
    invoke-direct {p0}, Landroid/view/VolumePanel;->addOtherVolumes()V

    #@40
    goto :goto_e

    #@41
    .line 565
    :cond_41
    iget-object v1, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@43
    iget-object v2, v0, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@45
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@48
    .line 566
    iput p1, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@4a
    .line 567
    iget-object v1, v0, Landroid/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    #@4c
    const/4 v2, 0x0

    #@4d
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@50
    .line 568
    invoke-direct {p0, v0}, Landroid/view/VolumePanel;->updateSlider(Landroid/view/VolumePanel$StreamControl;)V

    #@53
    goto :goto_3d
.end method

.method private resetTimeout()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x5

    #@1
    .line 1399
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@4
    .line 1400
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    const-wide/16 v1, 0xbb8

    #@a
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@d
    .line 1401
    return-void
.end method

.method private setMusicIcon(III)V
    .registers 7
    .parameter "resId"
    .parameter "resMuteId"
    .parameter "streamType"

    #@0
    .prologue
    .line 1288
    iget-object v1, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@2
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@c
    .line 1289
    .local v0, sc:Landroid/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_2b

    #@e
    .line 1290
    iput p1, v0, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@10
    .line 1291
    iput p2, v0, Landroid/view/VolumePanel$StreamControl;->iconMuteRes:I

    #@12
    .line 1293
    iget v1, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@14
    invoke-direct {p0, v1}, Landroid/view/VolumePanel;->isMuted(I)Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_24

    #@1a
    iget-object v1, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@1c
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@1e
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@21
    move-result v1

    #@22
    if-nez v1, :cond_2c

    #@24
    .line 1294
    :cond_24
    iget-object v1, v0, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@26
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->iconMuteRes:I

    #@28
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    #@2b
    .line 1300
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 1297
    :cond_2c
    iget-object v1, v0, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@2e
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@30
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    #@33
    goto :goto_2b
.end method

.method private setStreamVolume(III)V
    .registers 5
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    #@0
    .prologue
    .line 497
    const/16 v0, -0x64

    #@2
    if-ne p1, v0, :cond_a

    #@4
    .line 498
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@6
    invoke-virtual {v0, p2, p3}, Landroid/media/AudioManager;->setMasterVolume(II)V

    #@9
    .line 504
    :goto_9
    return-void

    #@a
    .line 499
    :cond_a
    const/16 v0, -0xc8

    #@c
    if-ne p1, v0, :cond_14

    #@e
    .line 500
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@10
    invoke-virtual {v0, p2}, Landroid/media/AudioService;->setRemoteStreamVolume(I)V

    #@13
    goto :goto_9

    #@14
    .line 502
    :cond_14
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@16
    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@19
    goto :goto_9
.end method

.method private showDescription(Z)V
    .registers 8
    .parameter "show"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v4, 0x0

    #@3
    .line 686
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@5
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    #@8
    move-result v0

    #@9
    .line 688
    .local v0, count:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_36

    #@c
    .line 689
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@e
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/view/VolumePanel$StreamControl;

    #@18
    .line 691
    .local v2, sc:Landroid/view/VolumePanel$StreamControl;
    if-eqz p1, :cond_2b

    #@1a
    .line 692
    iget-object v3, v2, Landroid/view/VolumePanel$StreamControl;->description:Landroid/widget/TextView;

    #@1c
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    #@1f
    .line 693
    add-int/lit8 v3, v0, -0x1

    #@21
    if-ge v1, v3, :cond_28

    #@23
    .line 694
    iget-object v3, v2, Landroid/view/VolumePanel$StreamControl;->hDivider:Landroid/widget/ImageView;

    #@25
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@28
    .line 688
    :cond_28
    :goto_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_a

    #@2b
    .line 697
    :cond_2b
    iget-object v3, v2, Landroid/view/VolumePanel$StreamControl;->description:Landroid/widget/TextView;

    #@2d
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    #@30
    .line 698
    iget-object v3, v2, Landroid/view/VolumePanel$StreamControl;->hDivider:Landroid/widget/ImageView;

    #@32
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    #@35
    goto :goto_28

    #@36
    .line 701
    .end local v2           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_36
    return-void
.end method

.method private updateMusicIcon()V
    .registers 12

    #@0
    .prologue
    const v10, 0x202024a

    #@3
    const v9, 0x2020249

    #@6
    const v8, 0x2020238

    #@9
    const v7, 0x2020237

    #@c
    const/4 v6, 0x3

    #@d
    .line 1234
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@f
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    #@12
    move-result v0

    #@13
    .line 1235
    .local v0, count:I
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    if-ge v1, v0, :cond_8e

    #@16
    .line 1236
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@18
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Landroid/view/VolumePanel$StreamControl;

    #@22
    .line 1238
    .local v2, sc:Landroid/view/VolumePanel$StreamControl;
    iget v3, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@24
    sparse-switch v3, :sswitch_data_90

    #@27
    .line 1235
    :cond_27
    :goto_27
    add-int/lit8 v1, v1, 0x1

    #@29
    goto :goto_14

    #@2a
    .line 1240
    :sswitch_2a
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@2c
    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    #@2f
    move-result v3

    #@30
    and-int/lit16 v3, v3, 0x380

    #@32
    if-eqz v3, :cond_3a

    #@34
    .line 1244
    iget v3, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@36
    invoke-direct {p0, v7, v8, v3}, Landroid/view/VolumePanel;->setMusicIcon(III)V

    #@39
    goto :goto_27

    #@3a
    .line 1245
    :cond_3a
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@3c
    invoke-virtual {v3}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_4a

    #@42
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@44
    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerOnForMedia()Z

    #@47
    move-result v3

    #@48
    if-eqz v3, :cond_27

    #@4a
    .line 1249
    :cond_4a
    iget v3, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@4c
    invoke-direct {p0, v9, v10, v3}, Landroid/view/VolumePanel;->setMusicIcon(III)V

    #@4f
    goto :goto_27

    #@50
    .line 1256
    :sswitch_50
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@52
    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    #@55
    move-result v3

    #@56
    and-int/lit16 v3, v3, 0x380

    #@58
    if-eqz v3, :cond_60

    #@5a
    .line 1260
    iget v3, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@5c
    invoke-direct {p0, v7, v8, v3}, Landroid/view/VolumePanel;->setMusicIcon(III)V

    #@5f
    goto :goto_27

    #@60
    .line 1261
    :cond_60
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@62
    invoke-virtual {v3}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    #@65
    move-result v3

    #@66
    if-eqz v3, :cond_7c

    #@68
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@6a
    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerOnForMedia()Z

    #@6d
    move-result v3

    #@6e
    if-nez v3, :cond_7c

    #@70
    .line 1262
    const v3, 0x202023a

    #@73
    const v4, 0x202023c

    #@76
    iget v5, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@78
    invoke-direct {p0, v3, v4, v5}, Landroid/view/VolumePanel;->setMusicIcon(III)V

    #@7b
    goto :goto_27

    #@7c
    .line 1265
    :cond_7c
    const v3, 0x202023e

    #@7f
    const v4, 0x2020240

    #@82
    iget v5, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@84
    invoke-direct {p0, v3, v4, v5}, Landroid/view/VolumePanel;->setMusicIcon(III)V

    #@87
    goto :goto_27

    #@88
    .line 1271
    :sswitch_88
    iget v3, v2, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@8a
    invoke-direct {p0, v9, v10, v3}, Landroid/view/VolumePanel;->setMusicIcon(III)V

    #@8d
    goto :goto_27

    #@8e
    .line 1279
    .end local v2           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_8e
    return-void

    #@8f
    .line 1238
    nop

    #@90
    :sswitch_data_90
    .sparse-switch
        0x3 -> :sswitch_50
        0xa -> :sswitch_88
        0xb -> :sswitch_2a
    .end sparse-switch
.end method

.method private updateSlider(Landroid/view/VolumePanel$StreamControl;)V
    .registers 8
    .parameter "sc"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 595
    iget-object v4, p1, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@4
    iget v5, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@6
    invoke-direct {p0, v5}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@9
    move-result v5

    #@a
    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    #@d
    .line 596
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@f
    invoke-direct {p0, v4}, Landroid/view/VolumePanel;->isMuted(I)Z

    #@12
    move-result v1

    #@13
    .line 598
    .local v1, muted:Z
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@15
    const/4 v5, 0x3

    #@16
    if-ne v4, v5, :cond_54

    #@18
    .line 599
    if-nez v1, :cond_24

    #@1a
    iget-object v4, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@1c
    iget v5, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@1e
    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@21
    move-result v4

    #@22
    if-nez v4, :cond_4f

    #@24
    :cond_24
    move v0, v3

    #@25
    .line 600
    .local v0, music_muted:Z
    :goto_25
    iget-object v5, p1, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@27
    if-eqz v0, :cond_51

    #@29
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->iconMuteRes:I

    #@2b
    :goto_2b
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    #@2e
    .line 605
    .end local v0           #music_muted:Z
    :goto_2e
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@30
    const/4 v5, 0x2

    #@31
    if-ne v4, v5, :cond_43

    #@33
    iget-object v4, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@35
    invoke-virtual {v4}, Landroid/media/AudioManager;->getRingerMode()I

    #@38
    move-result v4

    #@39
    if-ne v4, v3, :cond_43

    #@3b
    .line 607
    iget-object v4, p1, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@3d
    const v5, 0x2020246

    #@40
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    #@43
    .line 609
    :cond_43
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@45
    const/16 v5, -0xc8

    #@47
    if-ne v4, v5, :cond_61

    #@49
    .line 612
    iget-object v2, p1, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@4b
    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@4e
    .line 618
    :goto_4e
    return-void

    #@4f
    :cond_4f
    move v0, v2

    #@50
    .line 599
    goto :goto_25

    #@51
    .line 600
    .restart local v0       #music_muted:Z
    :cond_51
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@53
    goto :goto_2b

    #@54
    .line 602
    .end local v0           #music_muted:Z
    :cond_54
    iget-object v5, p1, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@56
    if-eqz v1, :cond_5e

    #@58
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->iconMuteRes:I

    #@5a
    :goto_5a
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    #@5d
    goto :goto_2e

    #@5e
    :cond_5e
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@60
    goto :goto_5a

    #@61
    .line 613
    :cond_61
    iget v4, p1, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@63
    iget-object v5, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@65
    invoke-virtual {v5}, Landroid/media/AudioManager;->getMasterStreamType()I

    #@68
    move-result v5

    #@69
    if-eq v4, v5, :cond_73

    #@6b
    if-eqz v1, :cond_73

    #@6d
    .line 614
    iget-object v3, p1, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@6f
    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@72
    goto :goto_4e

    #@73
    .line 616
    :cond_73
    iget-object v2, p1, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@75
    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@78
    goto :goto_4e
.end method

.method private updateStates()V
    .registers 5

    #@0
    .prologue
    .line 677
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@2
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    #@5
    move-result v0

    #@6
    .line 678
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1b

    #@9
    .line 679
    iget-object v3, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@b
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/view/VolumePanel$StreamControl;

    #@15
    .line 680
    .local v2, sc:Landroid/view/VolumePanel$StreamControl;
    invoke-direct {p0, v2}, Landroid/view/VolumePanel;->updateSlider(Landroid/view/VolumePanel$StreamControl;)V

    #@18
    .line 678
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_7

    #@1b
    .line 682
    .end local v2           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_1b
    return-void
.end method


# virtual methods
.method public destoryVolumePanel()V
    .registers 3

    #@0
    .prologue
    .line 1580
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onFreeResources()V

    #@3
    .line 1582
    iget-object v0, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 1583
    iget-object v0, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    #@d
    .line 1584
    iget-object v0, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@f
    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    #@12
    .line 1587
    :cond_12
    iget-object v0, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 1588
    iget-object v0, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@18
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    #@1b
    .line 1591
    :cond_1b
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 1592
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@21
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    #@24
    .line 1594
    :cond_24
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1316
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_70

    #@5
    .line 1396
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1319
    :pswitch_6
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@8
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@a
    invoke-virtual {p0, v0, v1}, Landroid/view/VolumePanel;->onVolumeChanged(II)V

    #@d
    goto :goto_5

    #@e
    .line 1324
    :pswitch_e
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@10
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@12
    invoke-virtual {p0, v0, v1}, Landroid/view/VolumePanel;->onMuteChanged(II)V

    #@15
    goto :goto_5

    #@16
    .line 1329
    :pswitch_16
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onFreeResources()V

    #@19
    goto :goto_5

    #@1a
    .line 1334
    :pswitch_1a
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onStopSounds()V

    #@1d
    goto :goto_5

    #@1e
    .line 1339
    :pswitch_1e
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@20
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@22
    invoke-virtual {p0, v0, v1}, Landroid/view/VolumePanel;->onPlaySound(II)V

    #@25
    goto :goto_5

    #@26
    .line 1344
    :pswitch_26
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onVibrate()V

    #@29
    goto :goto_5

    #@2a
    .line 1349
    :pswitch_2a
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@2c
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_5

    #@32
    .line 1350
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@34
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    #@37
    .line 1351
    const/4 v0, -0x1

    #@38
    iput v0, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@3a
    goto :goto_5

    #@3b
    .line 1356
    :pswitch_3b
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@3d
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_5

    #@43
    .line 1357
    invoke-direct {p0}, Landroid/view/VolumePanel;->updateStates()V

    #@46
    goto :goto_5

    #@47
    .line 1363
    :pswitch_47
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@49
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@4b
    invoke-virtual {p0, v0, v1}, Landroid/view/VolumePanel;->onRemoteVolumeChanged(II)V

    #@4e
    goto :goto_5

    #@4f
    .line 1368
    :pswitch_4f
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onRemoteVolumeUpdateIfShown()V

    #@52
    goto :goto_5

    #@53
    .line 1372
    :pswitch_53
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@55
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@57
    invoke-virtual {p0, v0, v1}, Landroid/view/VolumePanel;->onSliderVisibilityChanged(II)V

    #@5a
    goto :goto_5

    #@5b
    .line 1376
    :pswitch_5b
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onDisplaySafeVolumeWarning()V

    #@5e
    goto :goto_5

    #@5f
    .line 1381
    :pswitch_5f
    invoke-direct {p0}, Landroid/view/VolumePanel;->forceTimeout()V

    #@62
    .line 1382
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onDisplayQuietModeWarning()V

    #@65
    goto :goto_5

    #@66
    .line 1388
    :pswitch_66
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@68
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->onDismissSafeVolumeWarning(I)V

    #@6b
    goto :goto_5

    #@6c
    .line 1392
    :pswitch_6c
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onSetEnforceSafeVolume()V

    #@6f
    goto :goto_5

    #@70
    .line 1316
    :pswitch_data_70
    .packed-switch 0x0
        :pswitch_6
        :pswitch_16
        :pswitch_1e
        :pswitch_1a
        :pswitch_26
        :pswitch_2a
        :pswitch_3b
        :pswitch_e
        :pswitch_47
        :pswitch_4f
        :pswitch_53
        :pswitch_5b
        :pswitch_5f
        :pswitch_66
        :pswitch_6c
    .end packed-switch
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 1517
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@2
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 1488
    iget-object v0, p0, Landroid/view/VolumePanel;->mMoreButton:Landroid/widget/ImageView;

    #@2
    if-ne p1, v0, :cond_7

    #@4
    .line 1489
    invoke-direct {p0}, Landroid/view/VolumePanel;->expand()V

    #@7
    .line 1491
    :cond_7
    invoke-direct {p0}, Landroid/view/VolumePanel;->resetTimeout()V

    #@a
    .line 1492
    return-void
.end method

.method protected onDismissSafeVolumeWarning(I)V
    .registers 9
    .parameter "direction"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1179
    sget-object v4, Landroid/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    #@3
    monitor-enter v4

    #@4
    .line 1180
    :try_start_4
    sget-object v5, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@6
    if-eqz v5, :cond_43

    #@8
    sget-object v5, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@a
    invoke-virtual {v5}, Landroid/app/AlertDialog;->isShowing()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_43

    #@10
    .line 1181
    iget-object v5, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@12
    const-string/jumbo v6, "power"

    #@15
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/os/PowerManager;

    #@1b
    .line 1182
    .local v2, pm:Landroid/os/PowerManager;
    iget-object v5, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@1d
    const-string/jumbo v6, "keyguard"

    #@20
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/app/KeyguardManager;

    #@26
    .line 1183
    .local v1, km:Landroid/app/KeyguardManager;
    if-eqz v1, :cond_45

    #@28
    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    #@2b
    move-result v5

    #@2c
    if-eqz v5, :cond_45

    #@2e
    move v0, v3

    #@2f
    .line 1184
    .local v0, isLocked:Z
    :goto_2f
    if-eqz v0, :cond_47

    #@31
    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    #@34
    move-result v5

    #@35
    if-nez v5, :cond_47

    #@37
    if-ne p1, v3, :cond_47

    #@39
    .line 1185
    iget-object v3, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@3b
    invoke-virtual {v3}, Landroid/media/AudioService;->disableSafeMediaVolume()V

    #@3e
    .line 1186
    sget-object v3, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@40
    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    #@43
    .line 1191
    .end local v0           #isLocked:Z
    .end local v1           #km:Landroid/app/KeyguardManager;
    .end local v2           #pm:Landroid/os/PowerManager;
    :cond_43
    :goto_43
    monitor-exit v4

    #@44
    .line 1192
    return-void

    #@45
    .line 1183
    .restart local v1       #km:Landroid/app/KeyguardManager;
    .restart local v2       #pm:Landroid/os/PowerManager;
    :cond_45
    const/4 v0, 0x0

    #@46
    goto :goto_2f

    #@47
    .line 1187
    .restart local v0       #isLocked:Z
    :cond_47
    const/4 v3, -0x1

    #@48
    if-ne p1, v3, :cond_43

    #@4a
    .line 1188
    sget-object v3, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@4c
    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    #@4f
    goto :goto_43

    #@50
    .line 1191
    .end local v0           #isLocked:Z
    .end local v1           #km:Landroid/app/KeyguardManager;
    .end local v2           #pm:Landroid/os/PowerManager;
    :catchall_50
    move-exception v3

    #@51
    monitor-exit v4
    :try_end_52
    .catchall {:try_start_4 .. :try_end_52} :catchall_50

    #@52
    throw v3
.end method

.method protected onDisplayQuietModeWarning()V
    .registers 7

    #@0
    .prologue
    .line 1561
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@5
    .line 1562
    .local v1, in:Landroid/content/Intent;
    const-string v3, "com.lge.settings.QUIET_MODE_ASK_SOUNDPROFILE"

    #@7
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    .line 1563
    const/high16 v3, 0x3000

    #@c
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@f
    .line 1565
    iget-object v3, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@11
    const-string/jumbo v4, "statusbar"

    #@14
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Landroid/app/StatusBarManager;

    #@1a
    .line 1566
    .local v2, sbm:Landroid/app/StatusBarManager;
    if-eqz v2, :cond_1f

    #@1c
    .line 1567
    invoke-virtual {v2}, Landroid/app/StatusBarManager;->collapsePanels()V

    #@1f
    .line 1571
    :cond_1f
    :try_start_1f
    iget-object v3, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@21
    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_24
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1f .. :try_end_24} :catch_25

    #@24
    .line 1575
    :goto_24
    return-void

    #@25
    .line 1572
    :catch_25
    move-exception v0

    #@26
    .line 1573
    .local v0, ex:Landroid/content/ActivityNotFoundException;
    const-string v3, "TAG"

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "QmodeAlertActivity "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_24
.end method

.method protected onDisplaySafeVolumeWarning()V
    .registers 6

    #@0
    .prologue
    .line 1149
    sget-object v2, Landroid/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 1150
    :try_start_3
    sget-object v1, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@5
    if-eqz v1, :cond_9

    #@7
    .line 1151
    monitor-exit v2

    #@8
    .line 1175
    :goto_8
    return-void

    #@9
    .line 1153
    :cond_9
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@b
    iget-object v3, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@d
    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@10
    const v3, 0x20902a9

    #@13
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@16
    move-result-object v1

    #@17
    const v3, 0x1040013

    #@1a
    new-instance v4, Landroid/view/VolumePanel$5;

    #@1c
    invoke-direct {v4, p0}, Landroid/view/VolumePanel$5;-><init>(Landroid/view/VolumePanel;)V

    #@1f
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@22
    move-result-object v1

    #@23
    const v3, 0x1040009

    #@26
    const/4 v4, 0x0

    #@27
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@2a
    move-result-object v1

    #@2b
    const v3, 0x1010355

    #@2e
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@35
    move-result-object v1

    #@36
    sput-object v1, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@38
    .line 1167
    new-instance v0, Landroid/view/VolumePanel$WarningDialogReceiver;

    #@3a
    iget-object v1, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@3c
    sget-object v3, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@3e
    invoke-direct {v0, v1, v3}, Landroid/view/VolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;)V

    #@41
    .line 1170
    .local v0, warning:Landroid/view/VolumePanel$WarningDialogReceiver;
    sget-object v1, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@43
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@46
    .line 1171
    sget-object v1, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@48
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@4b
    move-result-object v1

    #@4c
    const/16 v3, 0x7d9

    #@4e
    invoke-virtual {v1, v3}, Landroid/view/Window;->setType(I)V

    #@51
    .line 1173
    sget-object v1, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@53
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@56
    .line 1174
    monitor-exit v2

    #@57
    goto :goto_8

    #@58
    .end local v0           #warning:Landroid/view/VolumePanel$WarningDialogReceiver;
    :catchall_58
    move-exception v1

    #@59
    monitor-exit v2
    :try_end_5a
    .catchall {:try_start_3 .. :try_end_5a} :catchall_58

    #@5a
    throw v1
.end method

.method protected onFreeResources()V
    .registers 4

    #@0
    .prologue
    .line 1304
    monitor-enter p0

    #@1
    .line 1305
    :try_start_1
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@3
    array-length v1, v1

    #@4
    add-int/lit8 v0, v1, -0x1

    #@6
    .local v0, i:I
    :goto_6
    if-ltz v0, :cond_1d

    #@8
    .line 1306
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@a
    aget-object v1, v1, v0

    #@c
    if-eqz v1, :cond_15

    #@e
    .line 1307
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@10
    aget-object v1, v1, v0

    #@12
    invoke-virtual {v1}, Landroid/media/ToneGenerator;->release()V

    #@15
    .line 1309
    :cond_15
    iget-object v1, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@17
    const/4 v2, 0x0

    #@18
    aput-object v2, v1, v0

    #@1a
    .line 1305
    add-int/lit8 v0, v0, -0x1

    #@1c
    goto :goto_6

    #@1d
    .line 1311
    :cond_1d
    monitor-exit p0

    #@1e
    .line 1312
    return-void

    #@1f
    .line 1311
    .end local v0           #i:I
    :catchall_1f
    move-exception v1

    #@20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method protected onMuteChanged(II)V
    .registers 7
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    .line 849
    sget-boolean v1, Landroid/view/VolumePanel;->LOGD:Z

    #@2
    if-eqz v1, :cond_2d

    #@4
    const-string v1, "VolumePanel"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string/jumbo v3, "onMuteChanged(streamType: "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", flags: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ")"

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 851
    :cond_2d
    iget-object v1, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@2f
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@39
    .line 852
    .local v0, sc:Landroid/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_4a

    #@3b
    .line 853
    iget-object v2, v0, Landroid/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    #@3d
    iget v1, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@3f
    invoke-direct {p0, v1}, Landroid/view/VolumePanel;->isMuted(I)Z

    #@42
    move-result v1

    #@43
    if-eqz v1, :cond_4e

    #@45
    iget v1, v0, Landroid/view/VolumePanel$StreamControl;->iconMuteRes:I

    #@47
    :goto_47
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@4a
    .line 856
    :cond_4a
    invoke-virtual {p0, p1, p2}, Landroid/view/VolumePanel;->onVolumeChanged(II)V

    #@4d
    .line 857
    return-void

    #@4e
    .line 853
    :cond_4e
    iget v1, v0, Landroid/view/VolumePanel$StreamControl;->iconRes:I

    #@50
    goto :goto_47
.end method

.method protected onPlaySound(II)V
    .registers 7
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 1042
    invoke-virtual {p0, v2}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_d

    #@7
    .line 1043
    invoke-virtual {p0, v2}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@a
    .line 1045
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onStopSounds()V

    #@d
    .line 1048
    :cond_d
    monitor-enter p0

    #@e
    .line 1049
    :try_start_e
    invoke-direct {p0, p1}, Landroid/view/VolumePanel;->getOrCreateToneGenerator(I)Landroid/media/ToneGenerator;

    #@11
    move-result-object v0

    #@12
    .line 1051
    .local v0, toneGen:Landroid/media/ToneGenerator;
    if-eqz v0, :cond_2c

    #@14
    iget-object v1, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@16
    invoke-virtual {v1}, Landroid/media/AudioService;->getRingerMode()I

    #@19
    move-result v1

    #@1a
    const/4 v2, 0x2

    #@1b
    if-ne v1, v2, :cond_2c

    #@1d
    .line 1053
    const/16 v1, 0x18

    #@1f
    invoke-virtual {v0, v1}, Landroid/media/ToneGenerator;->startTone(I)Z

    #@22
    .line 1054
    const/4 v1, 0x3

    #@23
    invoke-virtual {p0, v1}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@26
    move-result-object v1

    #@27
    const-wide/16 v2, 0x96

    #@29
    invoke-virtual {p0, v1, v2, v3}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2c
    .line 1056
    :cond_2c
    monitor-exit p0

    #@2d
    .line 1057
    return-void

    #@2e
    .line 1056
    .end local v0           #toneGen:Landroid/media/ToneGenerator;
    :catchall_2e
    move-exception v1

    #@2f
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_e .. :try_end_30} :catchall_2e

    #@30
    throw v1
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .registers 11
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1410
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    .line 1411
    .local v1, tag:Ljava/lang/Object;
    if-eqz p3, :cond_57

    #@8
    instance-of v2, v1, Landroid/view/VolumePanel$StreamControl;

    #@a
    if-eqz v2, :cond_57

    #@c
    move-object v0, v1

    #@d
    .line 1412
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@f
    .line 1415
    .local v0, sc:Landroid/view/VolumePanel$StreamControl;
    sget-boolean v2, Landroid/view/VolumePanel;->LOGD:Z

    #@11
    if-eqz v2, :cond_48

    #@13
    const-string v2, "VolumePanel"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string/jumbo v4, "onProgressChanged() START streamType = "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    iget v4, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ", getStreamVolume = "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    iget v4, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@2f
    invoke-direct {p0, v4}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@32
    move-result v4

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, ", set progress="

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1421
    :cond_48
    if-nez p2, :cond_5b

    #@4a
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@4c
    invoke-direct {p0, v2}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@4f
    move-result v2

    #@50
    if-eq v2, p2, :cond_5b

    #@52
    .line 1422
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@54
    invoke-direct {p0, v2, p2, v6}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@57
    .line 1437
    .end local v0           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_57
    :goto_57
    invoke-direct {p0}, Landroid/view/VolumePanel;->resetTimeout()V

    #@5a
    .line 1438
    return-void

    #@5b
    .line 1423
    .restart local v0       #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_5b
    if-lez p2, :cond_77

    #@5d
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@5f
    invoke-direct {p0, v2}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@62
    move-result v2

    #@63
    if-nez v2, :cond_77

    #@65
    .line 1424
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@67
    const/4 v3, 0x2

    #@68
    if-ne v2, v3, :cond_71

    #@6a
    .line 1425
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@6c
    const/4 v3, 0x3

    #@6d
    invoke-direct {p0, v2, p2, v3}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@70
    goto :goto_57

    #@71
    .line 1427
    :cond_71
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@73
    invoke-direct {p0, v2, p2, v6}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@76
    goto :goto_57

    #@77
    .line 1429
    :cond_77
    if-lez p2, :cond_85

    #@79
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@7b
    if-nez v2, :cond_85

    #@7d
    .line 1430
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@7f
    add-int/lit8 v3, p2, -0x1

    #@81
    invoke-direct {p0, v2, v3, v5}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@84
    goto :goto_57

    #@85
    .line 1432
    :cond_85
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@87
    invoke-direct {p0, v2, p2, v5}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@8a
    goto :goto_57
.end method

.method protected onRemoteVolumeChanged(II)V
    .registers 9
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x2

    #@2
    const/16 v3, -0xc8

    #@4
    .line 1086
    sget-boolean v0, Landroid/view/VolumePanel;->LOGD:Z

    #@6
    if-eqz v0, :cond_31

    #@8
    const-string v0, "VolumePanel"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string/jumbo v2, "onRemoteVolumeChanged(stream:"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, ", flags: "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, ")"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 1088
    :cond_31
    and-int/lit8 v0, p2, 0x1

    #@33
    if-nez v0, :cond_3d

    #@35
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@37
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_82

    #@3d
    .line 1089
    :cond_3d
    monitor-enter p0

    #@3e
    .line 1090
    :try_start_3e
    iget v0, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@40
    if-eq v0, v3, :cond_47

    #@42
    .line 1091
    const/16 v0, -0xc8

    #@44
    invoke-direct {p0, v0}, Landroid/view/VolumePanel;->reorderSliders(I)V

    #@47
    .line 1093
    :cond_47
    const/16 v0, -0xc8

    #@49
    invoke-virtual {p0, v0, p2}, Landroid/view/VolumePanel;->onShowVolumeChanged(II)V

    #@4c
    .line 1094
    monitor-exit p0
    :try_end_4d
    .catchall {:try_start_3e .. :try_end_4d} :catchall_7f

    #@4d
    .line 1099
    :cond_4d
    :goto_4d
    and-int/lit8 v0, p2, 0x4

    #@4f
    if-eqz v0, :cond_61

    #@51
    iget-boolean v0, p0, Landroid/view/VolumePanel;->mRingIsSilent:Z

    #@53
    if-nez v0, :cond_61

    #@55
    .line 1100
    invoke-virtual {p0, v4}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@58
    .line 1101
    invoke-virtual {p0, v4, p1, p2}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@5b
    move-result-object v0

    #@5c
    const-wide/16 v1, 0x32

    #@5e
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@61
    .line 1104
    :cond_61
    and-int/lit8 v0, p2, 0x8

    #@63
    if-eqz v0, :cond_6f

    #@65
    .line 1105
    invoke-virtual {p0, v4}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@68
    .line 1106
    const/4 v0, 0x4

    #@69
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@6c
    .line 1107
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onStopSounds()V

    #@6f
    .line 1110
    :cond_6f
    invoke-virtual {p0, v5}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@72
    .line 1111
    invoke-virtual {p0, v5}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@75
    move-result-object v0

    #@76
    const-wide/16 v1, 0x2710

    #@78
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@7b
    .line 1113
    invoke-direct {p0}, Landroid/view/VolumePanel;->resetTimeout()V

    #@7e
    .line 1114
    return-void

    #@7f
    .line 1094
    :catchall_7f
    move-exception v0

    #@80
    :try_start_80
    monitor-exit p0
    :try_end_81
    .catchall {:try_start_80 .. :try_end_81} :catchall_7f

    #@81
    throw v0

    #@82
    .line 1096
    :cond_82
    sget-boolean v0, Landroid/view/VolumePanel;->LOGD:Z

    #@84
    if-eqz v0, :cond_4d

    #@86
    const-string v0, "VolumePanel"

    #@88
    const-string/jumbo v1, "not calling onShowVolumeChanged(), no FLAG_SHOW_UI or no UI"

    #@8b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    goto :goto_4d
.end method

.method protected onRemoteVolumeUpdateIfShown()V
    .registers 4

    #@0
    .prologue
    const/16 v2, -0xc8

    #@2
    .line 1117
    sget-boolean v0, Landroid/view/VolumePanel;->LOGD:Z

    #@4
    if-eqz v0, :cond_e

    #@6
    const-string v0, "VolumePanel"

    #@8
    const-string/jumbo v1, "onRemoteVolumeUpdateIfShown()"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1118
    :cond_e
    iget-object v0, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@10
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_22

    #@16
    iget v0, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@18
    if-ne v0, v2, :cond_22

    #@1a
    iget-object v0, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@1c
    if-eqz v0, :cond_22

    #@1e
    .line 1121
    const/4 v0, 0x0

    #@1f
    invoke-virtual {p0, v2, v0}, Landroid/view/VolumePanel;->onShowVolumeChanged(II)V

    #@22
    .line 1123
    :cond_22
    return-void
.end method

.method protected onSetEnforceSafeVolume()V
    .registers 5

    #@0
    .prologue
    .line 1195
    sget-object v1, Landroid/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1196
    :try_start_3
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@5
    if-eqz v0, :cond_16

    #@7
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@9
    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 1197
    const/4 v0, 0x3

    #@10
    const/16 v2, 0xa

    #@12
    const/4 v3, 0x1

    #@13
    invoke-direct {p0, v0, v2, v3}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@16
    .line 1199
    :cond_16
    monitor-exit v1

    #@17
    .line 1200
    return-void

    #@18
    .line 1199
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method protected onShowVolumeChanged(II)V
    .registers 13
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    .line 860
    invoke-direct {p0, p1}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@3
    move-result v1

    #@4
    .line 862
    .local v1, index:I
    const/4 v7, 0x0

    #@5
    iput-boolean v7, p0, Landroid/view/VolumePanel;->mRingIsSilent:Z

    #@7
    .line 864
    sget-boolean v7, Landroid/view/VolumePanel;->LOGD:Z

    #@9
    if-eqz v7, :cond_38

    #@b
    .line 865
    const-string v7, "VolumePanel"

    #@d
    new-instance v8, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string/jumbo v9, "onShowVolumeChanged(streamType: "

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    const-string v9, ", flags: "

    #@1f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v8

    #@27
    const-string v9, "), index: "

    #@29
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v8

    #@35
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 871
    :cond_38
    invoke-direct {p0, p1}, Landroid/view/VolumePanel;->getStreamMaxVolume(I)I

    #@3b
    move-result v3

    #@3c
    .line 873
    .local v3, max:I
    sparse-switch p1, :sswitch_data_172

    #@3f
    .line 974
    :cond_3f
    :goto_3f
    :sswitch_3f
    invoke-direct {p0}, Landroid/view/VolumePanel;->updateMusicIcon()V

    #@42
    .line 977
    iget-object v7, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v8

    #@48
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    move-result-object v5

    #@4c
    check-cast v5, Landroid/view/VolumePanel$StreamControl;

    #@4e
    .line 978
    .local v5, sc:Landroid/view/VolumePanel$StreamControl;
    if-eqz v5, :cond_7e

    #@50
    .line 979
    iget-object v7, v5, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@52
    invoke-virtual {v7}, Landroid/widget/SeekBar;->getMax()I

    #@55
    move-result v7

    #@56
    if-eq v7, v3, :cond_5d

    #@58
    .line 980
    iget-object v7, v5, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@5a
    invoke-virtual {v7, v3}, Landroid/widget/SeekBar;->setMax(I)V

    #@5d
    .line 983
    :cond_5d
    iget-object v7, v5, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@5f
    invoke-virtual {v7, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    #@62
    .line 984
    and-int/lit8 v7, p2, 0x20

    #@64
    if-nez v7, :cond_78

    #@66
    iget-object v7, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@68
    invoke-virtual {v7}, Landroid/media/AudioManager;->getMasterStreamType()I

    #@6b
    move-result v7

    #@6c
    if-eq p1, v7, :cond_146

    #@6e
    const/16 v7, -0xc8

    #@70
    if-eq p1, v7, :cond_146

    #@72
    invoke-direct {p0, p1}, Landroid/view/VolumePanel;->isMuted(I)Z

    #@75
    move-result v7

    #@76
    if-eqz v7, :cond_146

    #@78
    .line 988
    :cond_78
    iget-object v7, v5, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@7a
    const/4 v8, 0x0

    #@7b
    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@7e
    .line 994
    :cond_7e
    :goto_7e
    iget-object v7, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@80
    invoke-virtual {v7}, Landroid/app/Dialog;->isShowing()Z

    #@83
    move-result v7

    #@84
    if-nez v7, :cond_ca

    #@86
    .line 995
    const/16 v7, -0xc8

    #@88
    if-ne p1, v7, :cond_14e

    #@8a
    const/4 v6, -0x1

    #@8b
    .line 998
    .local v6, stream:I
    :goto_8b
    iget-boolean v7, p0, Landroid/view/VolumePanel;->mIsQuickCoverClose:Z

    #@8d
    if-eqz v7, :cond_151

    #@8f
    if-eqz p1, :cond_97

    #@91
    const/4 v7, 0x6

    #@92
    if-eq p1, v7, :cond_97

    #@94
    const/4 v7, 0x3

    #@95
    if-ne p1, v7, :cond_151

    #@97
    .line 1000
    :cond_97
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@99
    const/16 v7, 0x21c

    #@9b
    const/4 v8, -0x2

    #@9c
    const/high16 v9, 0x3f80

    #@9e
    invoke-direct {v0, v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@a1
    .line 1001
    .local v0, SliderGroupParam:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@a3
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@a6
    .line 1002
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    #@a8
    const/4 v7, -0x2

    #@a9
    const/4 v8, -0x2

    #@aa
    invoke-direct {v2, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@ad
    .line 1003
    .local v2, layoutParam:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@af
    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@b2
    .line 1013
    :goto_b2
    iget-object v7, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@b4
    invoke-virtual {v7, v6}, Landroid/media/AudioManager;->forceVolumeControlStream(I)V

    #@b7
    .line 1014
    iget-object v7, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@b9
    iget-object v8, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@bb
    invoke-virtual {v7, v8}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    #@be
    .line 1018
    and-int/lit16 v7, p2, 0x100

    #@c0
    if-eqz v7, :cond_16d

    #@c2
    .line 1019
    invoke-direct {p0}, Landroid/view/VolumePanel;->expand()V

    #@c5
    .line 1025
    :goto_c5
    iget-object v7, p0, Landroid/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    #@c7
    invoke-virtual {v7}, Landroid/app/Dialog;->show()V

    #@ca
    .line 1029
    .end local v0           #SliderGroupParam:Landroid/widget/LinearLayout$LayoutParams;
    .end local v2           #layoutParam:Landroid/widget/LinearLayout$LayoutParams;
    .end local v6           #stream:I
    :cond_ca
    const/16 v7, -0xc8

    #@cc
    if-eq p1, v7, :cond_f4

    #@ce
    and-int/lit8 v7, p2, 0x10

    #@d0
    if-eqz v7, :cond_f4

    #@d2
    iget-object v7, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@d4
    invoke-virtual {v7, p1}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    #@d7
    move-result v7

    #@d8
    if-nez v7, :cond_e1

    #@da
    iget-boolean v7, p0, Landroid/view/VolumePanel;->mIsSoundException:Z

    #@dc
    if-eqz v7, :cond_f4

    #@de
    const/4 v7, 0x2

    #@df
    if-ne p1, v7, :cond_f4

    #@e1
    :cond_e1
    iget-object v7, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@e3
    invoke-virtual {v7}, Landroid/media/AudioManager;->getRingerMode()I

    #@e6
    move-result v7

    #@e7
    const/4 v8, 0x1

    #@e8
    if-ne v7, v8, :cond_f4

    #@ea
    .line 1036
    const/4 v7, 0x4

    #@eb
    invoke-virtual {p0, v7}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@ee
    move-result-object v7

    #@ef
    const-wide/16 v8, 0x32

    #@f1
    invoke-virtual {p0, v7, v8, v9}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@f4
    .line 1038
    :cond_f4
    return-void

    #@f5
    .line 877
    .end local v5           #sc:Landroid/view/VolumePanel$StreamControl;
    :sswitch_f5
    iget-object v7, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@f7
    const/4 v8, 0x1

    #@f8
    invoke-static {v7, v8}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    #@fb
    move-result-object v4

    #@fc
    .line 879
    .local v4, ringuri:Landroid/net/Uri;
    if-nez v4, :cond_3f

    #@fe
    .line 880
    const/4 v7, 0x1

    #@ff
    iput-boolean v7, p0, Landroid/view/VolumePanel;->mRingIsSilent:Z

    #@101
    goto/16 :goto_3f

    #@103
    .line 933
    .end local v4           #ringuri:Landroid/net/Uri;
    :sswitch_103
    add-int/lit8 v1, v1, 0x1

    #@105
    .line 934
    add-int/lit8 v3, v3, 0x1

    #@107
    .line 935
    goto/16 :goto_3f

    #@109
    .line 943
    :sswitch_109
    iget-object v7, p0, Landroid/view/VolumePanel;->mContext:Landroid/content/Context;

    #@10b
    const/4 v8, 0x2

    #@10c
    invoke-static {v7, v8}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    #@10f
    move-result-object v4

    #@110
    .line 945
    .restart local v4       #ringuri:Landroid/net/Uri;
    if-nez v4, :cond_3f

    #@112
    .line 946
    const/4 v7, 0x1

    #@113
    iput-boolean v7, p0, Landroid/view/VolumePanel;->mRingIsSilent:Z

    #@115
    goto/16 :goto_3f

    #@117
    .line 957
    .end local v4           #ringuri:Landroid/net/Uri;
    :sswitch_117
    add-int/lit8 v1, v1, 0x1

    #@119
    .line 958
    add-int/lit8 v3, v3, 0x1

    #@11b
    .line 959
    goto/16 :goto_3f

    #@11d
    .line 963
    :sswitch_11d
    sget-boolean v7, Landroid/view/VolumePanel;->LOGD:Z

    #@11f
    if-eqz v7, :cond_3f

    #@121
    const-string v7, "VolumePanel"

    #@123
    new-instance v8, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string/jumbo v9, "showing remote volume "

    #@12b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v8

    #@12f
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v8

    #@133
    const-string v9, " over "

    #@135
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v8

    #@139
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v8

    #@13d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@140
    move-result-object v8

    #@141
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@144
    goto/16 :goto_3f

    #@146
    .line 990
    .restart local v5       #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_146
    iget-object v7, v5, Landroid/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/SeekBar;

    #@148
    const/4 v8, 0x1

    #@149
    invoke-virtual {v7, v8}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@14c
    goto/16 :goto_7e

    #@14e
    :cond_14e
    move v6, p1

    #@14f
    .line 995
    goto/16 :goto_8b

    #@151
    .line 1005
    .restart local v6       #stream:I
    :cond_151
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    #@153
    const/4 v7, 0x0

    #@154
    const/4 v8, -0x2

    #@155
    const/high16 v9, 0x3f80

    #@157
    invoke-direct {v0, v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@15a
    .line 1006
    .restart local v0       #SliderGroupParam:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Landroid/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    #@15c
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@15f
    .line 1007
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    #@161
    const/4 v7, -0x1

    #@162
    const/4 v8, -0x2

    #@163
    invoke-direct {v2, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@166
    .line 1008
    .restart local v2       #layoutParam:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Landroid/view/VolumePanel;->mView:Landroid/view/View;

    #@168
    invoke-virtual {v7, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@16b
    goto/16 :goto_b2

    #@16d
    .line 1021
    :cond_16d
    invoke-direct {p0, v6}, Landroid/view/VolumePanel;->collapse(I)V

    #@170
    goto/16 :goto_c5

    #@172
    .line 873
    :sswitch_data_172
    .sparse-switch
        -0xc8 -> :sswitch_11d
        0x0 -> :sswitch_103
        0x2 -> :sswitch_f5
        0x3 -> :sswitch_3f
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_109
        0x6 -> :sswitch_117
        0xa -> :sswitch_3f
        0xb -> :sswitch_3f
    .end sparse-switch
.end method

.method protected declared-synchronized onSliderVisibilityChanged(II)V
    .registers 9
    .parameter "streamType"
    .parameter "visible"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1134
    monitor-enter p0

    #@2
    :try_start_2
    sget-boolean v3, Landroid/view/VolumePanel;->LOGD:Z

    #@4
    if-eqz v3, :cond_2f

    #@6
    const-string v3, "VolumePanel"

    #@8
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v5, "onSliderVisibilityChanged(stream="

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    const-string v5, ", visi="

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    const-string v5, ")"

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 1135
    :cond_2f
    if-ne p2, v1, :cond_4d

    #@31
    .line 1136
    .local v1, isVisible:Z
    :goto_31
    sget-object v3, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@33
    array-length v3, v3

    #@34
    add-int/lit8 v0, v3, -0x1

    #@36
    .local v0, i:I
    :goto_36
    if-ltz v0, :cond_4b

    #@38
    .line 1137
    sget-object v3, Landroid/view/VolumePanel;->STREAMS:[Landroid/view/VolumePanel$StreamResources;

    #@3a
    aget-object v2, v3, v0

    #@3c
    .line 1138
    .local v2, streamRes:Landroid/view/VolumePanel$StreamResources;
    iget v3, v2, Landroid/view/VolumePanel$StreamResources;->streamType:I

    #@3e
    if-ne v3, p1, :cond_4f

    #@40
    .line 1139
    iput-boolean v1, v2, Landroid/view/VolumePanel$StreamResources;->show:Z

    #@42
    .line 1140
    if-nez v1, :cond_4b

    #@44
    iget v3, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@46
    if-ne v3, p1, :cond_4b

    #@48
    .line 1141
    const/4 v3, -0x1

    #@49
    iput v3, p0, Landroid/view/VolumePanel;->mActiveStreamType:I
    :try_end_4b
    .catchall {:try_start_2 .. :try_end_4b} :catchall_52

    #@4b
    .line 1146
    .end local v2           #streamRes:Landroid/view/VolumePanel$StreamResources;
    :cond_4b
    monitor-exit p0

    #@4c
    return-void

    #@4d
    .line 1135
    .end local v0           #i:I
    .end local v1           #isVisible:Z
    :cond_4d
    const/4 v1, 0x0

    #@4e
    goto :goto_31

    #@4f
    .line 1136
    .restart local v0       #i:I
    .restart local v1       #isVisible:Z
    .restart local v2       #streamRes:Landroid/view/VolumePanel$StreamResources;
    :cond_4f
    add-int/lit8 v0, v0, -0x1

    #@51
    goto :goto_36

    #@52
    .line 1134
    .end local v0           #i:I
    .end local v1           #isVisible:Z
    .end local v2           #streamRes:Landroid/view/VolumePanel$StreamResources;
    :catchall_52
    move-exception v3

    #@53
    monitor-exit p0

    #@54
    throw v3
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2
    .parameter "seekBar"

    #@0
    .prologue
    .line 1441
    return-void
.end method

.method protected onStopSounds()V
    .registers 5

    #@0
    .prologue
    .line 1061
    monitor-enter p0

    #@1
    .line 1062
    :try_start_1
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    #@4
    move-result v1

    #@5
    .line 1063
    .local v1, numStreamTypes:I
    add-int/lit8 v0, v1, -0x1

    #@7
    .local v0, i:I
    :goto_7
    if-ltz v0, :cond_15

    #@9
    .line 1064
    iget-object v3, p0, Landroid/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    #@b
    aget-object v2, v3, v0

    #@d
    .line 1065
    .local v2, toneGen:Landroid/media/ToneGenerator;
    if-eqz v2, :cond_12

    #@f
    .line 1066
    invoke-virtual {v2}, Landroid/media/ToneGenerator;->stopTone()V

    #@12
    .line 1063
    :cond_12
    add-int/lit8 v0, v0, -0x1

    #@14
    goto :goto_7

    #@15
    .line 1069
    .end local v2           #toneGen:Landroid/media/ToneGenerator;
    :cond_15
    monitor-exit p0

    #@16
    .line 1070
    return-void

    #@17
    .line 1069
    .end local v0           #i:I
    .end local v1           #numStreamTypes:I
    :catchall_17
    move-exception v3

    #@18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_17

    #@19
    throw v3
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 12
    .parameter "seekBar"

    #@0
    .prologue
    const-wide/16 v8, 0x32

    #@2
    const/16 v7, 0xa

    #@4
    const/4 v6, 0x1

    #@5
    const/16 v5, -0xc8

    #@7
    .line 1444
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    .line 1445
    .local v1, tag:Ljava/lang/Object;
    instance-of v2, v1, Landroid/view/VolumePanel$StreamControl;

    #@d
    if-eqz v2, :cond_1d

    #@f
    move-object v0, v1

    #@10
    .line 1446
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@12
    .line 1451
    .local v0, sc:Landroid/view/VolumePanel$StreamControl;
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@14
    if-ne v2, v5, :cond_1d

    #@16
    .line 1452
    invoke-direct {p0, v5}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@19
    move-result v2

    #@1a
    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    #@1d
    .line 1456
    .end local v0           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_1d
    instance-of v2, v1, Landroid/view/VolumePanel$StreamControl;

    #@1f
    if-eqz v2, :cond_63

    #@21
    move-object v0, v1

    #@22
    .line 1457
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@24
    .line 1458
    .restart local v0       #sc:Landroid/view/VolumePanel$StreamControl;
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@26
    if-eqz v2, :cond_2d

    #@28
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@2a
    const/4 v3, 0x6

    #@2b
    if-ne v2, v3, :cond_63

    #@2d
    :cond_2d
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@2f
    invoke-direct {p0, v2}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@32
    move-result v2

    #@33
    if-ge v2, v6, :cond_63

    #@35
    .line 1460
    sget-boolean v2, Landroid/view/VolumePanel;->LOGD:Z

    #@37
    if-eqz v2, :cond_58

    #@39
    const-string v2, "VolumePanel"

    #@3b
    new-instance v3, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string/jumbo v4, "onStopTrackingTouch(getStreamVolume = "

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    iget v4, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@49
    invoke-direct {p0, v4}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@4c
    move-result v4

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 1461
    :cond_58
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@5a
    iget v3, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@5c
    invoke-direct {p0, v3}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@5f
    move-result v3

    #@60
    invoke-direct {p0, v2, v3, v6}, Landroid/view/VolumePanel;->setStreamVolume(III)V

    #@63
    .line 1464
    .end local v0           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_63
    instance-of v2, v1, Landroid/view/VolumePanel$StreamControl;

    #@65
    if-eqz v2, :cond_91

    #@67
    move-object v0, v1

    #@68
    .line 1465
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@6a
    .line 1467
    .restart local v0       #sc:Landroid/view/VolumePanel$StreamControl;
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@6c
    if-eq v2, v5, :cond_91

    #@6e
    iget-object v2, p0, Landroid/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    #@70
    iget v3, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@72
    invoke-virtual {v2, v3}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    #@75
    move-result v2

    #@76
    if-nez v2, :cond_81

    #@78
    iget-boolean v2, p0, Landroid/view/VolumePanel;->mIsSoundException:Z

    #@7a
    if-eqz v2, :cond_91

    #@7c
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@7e
    const/4 v3, 0x2

    #@7f
    if-ne v2, v3, :cond_91

    #@81
    :cond_81
    iget-object v2, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@83
    invoke-virtual {v2}, Landroid/media/AudioManager;->getRingerMode()I

    #@86
    move-result v2

    #@87
    if-ne v2, v6, :cond_91

    #@89
    .line 1471
    const/4 v2, 0x4

    #@8a
    invoke-virtual {p0, v2}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@8d
    move-result-object v2

    #@8e
    invoke-virtual {p0, v2, v8, v9}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@91
    .line 1477
    .end local v0           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_91
    instance-of v2, v1, Landroid/view/VolumePanel$StreamControl;

    #@93
    if-eqz v2, :cond_b4

    #@95
    move-object v0, v1

    #@96
    .line 1478
    check-cast v0, Landroid/view/VolumePanel$StreamControl;

    #@98
    .line 1479
    .restart local v0       #sc:Landroid/view/VolumePanel$StreamControl;
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@9a
    const/4 v3, 0x3

    #@9b
    if-ne v2, v3, :cond_b4

    #@9d
    iget v2, v0, Landroid/view/VolumePanel$StreamControl;->streamType:I

    #@9f
    invoke-direct {p0, v2}, Landroid/view/VolumePanel;->getStreamVolume(I)I

    #@a2
    move-result v2

    #@a3
    if-ge v2, v7, :cond_ab

    #@a5
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    #@a8
    move-result v2

    #@a9
    if-lt v2, v7, :cond_b4

    #@ab
    .line 1481
    :cond_ab
    const/16 v2, 0xe

    #@ad
    invoke-virtual {p0, v2}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {p0, v2, v8, v9}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@b4
    .line 1485
    .end local v0           #sc:Landroid/view/VolumePanel$StreamControl;
    :cond_b4
    return-void
.end method

.method protected onVibrate()V
    .registers 4

    #@0
    .prologue
    .line 1075
    iget-object v0, p0, Landroid/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    #@2
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x1

    #@7
    if-eq v0, v1, :cond_a

    #@9
    .line 1080
    :goto_9
    return-void

    #@a
    .line 1079
    :cond_a
    iget-object v0, p0, Landroid/view/VolumePanel;->mVibrator:Landroid/os/Vibrator;

    #@c
    const-wide/16 v1, 0xc8

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    #@11
    goto :goto_9
.end method

.method protected onVolumeChanged(II)V
    .registers 9
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x2

    #@3
    .line 799
    sget-boolean v0, Landroid/view/VolumePanel;->LOGD:Z

    #@5
    if-eqz v0, :cond_30

    #@7
    const-string v0, "VolumePanel"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v2, "onVolumeChanged(streamType: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ", flags: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ")"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 807
    :cond_30
    invoke-direct {p0}, Landroid/view/VolumePanel;->isExpanded()Z

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_43

    #@36
    .line 808
    if-ne p1, v5, :cond_8f

    #@38
    .line 809
    iget-object v0, p0, Landroid/view/VolumePanel;->mPanel:Landroid/view/ViewGroup;

    #@3a
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3d
    move-result-object v0

    #@3e
    const/16 v1, 0xcc

    #@40
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@43
    .line 816
    :cond_43
    :goto_43
    and-int/lit8 v0, p2, 0x1

    #@45
    if-nez v0, :cond_4d

    #@47
    iget-boolean v0, p0, Landroid/view/VolumePanel;->mIsQuickCoverClose:Z

    #@49
    if-eqz v0, :cond_59

    #@4b
    if-ne p1, v5, :cond_59

    #@4d
    .line 818
    :cond_4d
    monitor-enter p0

    #@4e
    .line 819
    :try_start_4e
    iget v0, p0, Landroid/view/VolumePanel;->mActiveStreamType:I

    #@50
    if-eq v0, p1, :cond_55

    #@52
    .line 820
    invoke-direct {p0, p1}, Landroid/view/VolumePanel;->reorderSliders(I)V

    #@55
    .line 822
    :cond_55
    invoke-virtual {p0, p1, p2}, Landroid/view/VolumePanel;->onShowVolumeChanged(II)V

    #@58
    .line 823
    monitor-exit p0
    :try_end_59
    .catchall {:try_start_4e .. :try_end_59} :catchall_9b

    #@59
    .line 826
    :cond_59
    and-int/lit8 v0, p2, 0x4

    #@5b
    if-eqz v0, :cond_6d

    #@5d
    iget-boolean v0, p0, Landroid/view/VolumePanel;->mRingIsSilent:Z

    #@5f
    if-nez v0, :cond_6d

    #@61
    .line 827
    invoke-virtual {p0, v3}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@64
    .line 828
    invoke-virtual {p0, v3, p1, p2}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@67
    move-result-object v0

    #@68
    const-wide/16 v1, 0x32

    #@6a
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@6d
    .line 831
    :cond_6d
    and-int/lit8 v0, p2, 0x8

    #@6f
    if-eqz v0, :cond_7b

    #@71
    .line 832
    invoke-virtual {p0, v3}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@74
    .line 833
    const/4 v0, 0x4

    #@75
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@78
    .line 834
    invoke-virtual {p0}, Landroid/view/VolumePanel;->onStopSounds()V

    #@7b
    .line 837
    :cond_7b
    invoke-virtual {p0, v4}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@7e
    .line 838
    invoke-virtual {p0, v4}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@81
    move-result-object v0

    #@82
    const-wide/16 v1, 0x2710

    #@84
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@87
    .line 841
    and-int/lit8 v0, p2, 0x1

    #@89
    if-eqz v0, :cond_8e

    #@8b
    .line 843
    invoke-direct {p0}, Landroid/view/VolumePanel;->resetTimeout()V

    #@8e
    .line 845
    :cond_8e
    return-void

    #@8f
    .line 811
    :cond_8f
    iget-object v0, p0, Landroid/view/VolumePanel;->mPanel:Landroid/view/ViewGroup;

    #@91
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    #@94
    move-result-object v0

    #@95
    const/16 v1, 0xff

    #@97
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@9a
    goto :goto_43

    #@9b
    .line 823
    :catchall_9b
    move-exception v0

    #@9c
    :try_start_9c
    monitor-exit p0
    :try_end_9d
    .catchall {:try_start_9c .. :try_end_9d} :catchall_9b

    #@9d
    throw v0
.end method

.method public postDismissSafeVolumeWarning(I)V
    .registers 4
    .parameter "directon"

    #@0
    .prologue
    const/16 v1, 0xd

    #@2
    .line 787
    invoke-virtual {p0, v1}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 789
    :goto_8
    return-void

    #@9
    .line 788
    :cond_9
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, v1, p1, v0}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    goto :goto_8
.end method

.method public postDisplayQuietModeWarning()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xc

    #@2
    const/4 v1, 0x0

    #@3
    .line 1556
    invoke-virtual {p0, v2}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 1558
    :goto_9
    return-void

    #@a
    .line 1557
    :cond_a
    invoke-virtual {p0, v2, v1, v1}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    goto :goto_9
.end method

.method public postDisplaySafeVolumeWarning()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xb

    #@2
    const/4 v1, 0x0

    #@3
    .line 781
    invoke-virtual {p0, v2}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 783
    :goto_9
    return-void

    #@a
    .line 782
    :cond_a
    invoke-virtual {p0, v2, v1, v1}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    goto :goto_9
.end method

.method public postHasNewRemotePlaybackInfo()V
    .registers 3

    #@0
    .prologue
    const/16 v1, 0x9

    #@2
    .line 755
    invoke-virtual {p0, v1}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 759
    :goto_8
    return-void

    #@9
    .line 758
    :cond_9
    invoke-virtual {p0, v1}, Landroid/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@10
    goto :goto_8
.end method

.method public postMasterMuteChanged(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 777
    const/16 v0, -0x64

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/view/VolumePanel;->postMuteChanged(II)V

    #@5
    .line 778
    return-void
.end method

.method public postMasterVolumeChanged(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 762
    const/16 v0, -0x64

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/view/VolumePanel;->postVolumeChanged(II)V

    #@5
    .line 763
    return-void
.end method

.method public postMuteChanged(II)V
    .registers 4
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    .line 766
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_8

    #@7
    .line 774
    :goto_7
    return-void

    #@8
    .line 767
    :cond_8
    monitor-enter p0

    #@9
    .line 768
    :try_start_9
    iget-object v0, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@b
    if-nez v0, :cond_10

    #@d
    .line 769
    invoke-direct {p0}, Landroid/view/VolumePanel;->createSliders()V

    #@10
    .line 771
    :cond_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_9 .. :try_end_11} :catchall_1e

    #@11
    .line 772
    const/4 v0, 0x1

    #@12
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@15
    .line 773
    const/4 v0, 0x7

    #@16
    invoke-virtual {p0, v0, p1, p2}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1d
    goto :goto_7

    #@1e
    .line 771
    :catchall_1e
    move-exception v0

    #@1f
    :try_start_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    #@20
    throw v0
.end method

.method public postRemoteSliderVisibility(Z)V
    .registers 5
    .parameter "visible"

    #@0
    .prologue
    .line 739
    const/16 v1, 0xa

    #@2
    const/16 v2, -0xc8

    #@4
    if-eqz p1, :cond_f

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-virtual {p0, v1, v2, v0}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@e
    .line 741
    return-void

    #@f
    .line 739
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_7
.end method

.method public postRemoteVolumeChanged(II)V
    .registers 5
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 723
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@4
    if-eqz v0, :cond_17

    #@6
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@8
    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_17

    #@e
    .line 724
    const-string v0, "VolumePanel"

    #@10
    const-string/jumbo v1, "postRemoteVolumeChanged return, sConfirmSafeVolumeDialog showing"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 736
    :cond_16
    :goto_16
    return-void

    #@17
    .line 728
    :cond_17
    invoke-virtual {p0, v1}, Landroid/view/VolumePanel;->hasMessages(I)Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_16

    #@1d
    .line 729
    monitor-enter p0

    #@1e
    .line 730
    :try_start_1e
    iget-object v0, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@20
    if-nez v0, :cond_25

    #@22
    .line 731
    invoke-direct {p0}, Landroid/view/VolumePanel;->createSliders()V

    #@25
    .line 733
    :cond_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_1e .. :try_end_26} :catchall_32

    #@26
    .line 734
    const/4 v0, 0x1

    #@27
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@2a
    .line 735
    invoke-virtual {p0, v1, p1, p2}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@31
    goto :goto_16

    #@32
    .line 733
    :catchall_32
    move-exception v0

    #@33
    :try_start_33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method

.method public postVolumeChanged(II)V
    .registers 5
    .parameter "streamType"
    .parameter "flags"

    #@0
    .prologue
    .line 705
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@2
    if-eqz v0, :cond_15

    #@4
    sget-object v0, Landroid/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_15

    #@c
    .line 706
    const-string v0, "VolumePanel"

    #@e
    const-string/jumbo v1, "postVolumeChanged return, sConfirmSafeVolumeDialog showing"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 720
    :goto_14
    return-void

    #@15
    .line 713
    :cond_15
    monitor-enter p0

    #@16
    .line 714
    :try_start_16
    iget-object v0, p0, Landroid/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    #@18
    if-nez v0, :cond_1d

    #@1a
    .line 715
    invoke-direct {p0}, Landroid/view/VolumePanel;->createSliders()V

    #@1d
    .line 717
    :cond_1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_16 .. :try_end_1e} :catchall_2b

    #@1e
    .line 718
    const/4 v0, 0x1

    #@1f
    invoke-virtual {p0, v0}, Landroid/view/VolumePanel;->removeMessages(I)V

    #@22
    .line 719
    const/4 v0, 0x0

    #@23
    invoke-virtual {p0, v0, p1, p2}, Landroid/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@2a
    goto :goto_14

    #@2b
    .line 717
    :catchall_2b
    move-exception v0

    #@2c
    :try_start_2c
    monitor-exit p0
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v0
.end method
