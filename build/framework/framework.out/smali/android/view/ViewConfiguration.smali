.class public Landroid/view/ViewConfiguration;
.super Ljava/lang/Object;
.source "ViewConfiguration.java"


# static fields
.field private static final DEFAULT_LONG_PRESS_TIMEOUT:I = 0x1f4

.field private static final DOUBLE_TAP_SLOP:I = 0x64

.field private static final DOUBLE_TAP_TIMEOUT:I = 0x12c

.field private static final DOUBLE_TAP_TOUCH_SLOP:I = 0x8

.field private static final EDGE_SLOP:I = 0xc

.field private static final FADING_EDGE_LENGTH:I = 0xc

.field private static final GLOBAL_ACTIONS_KEY_TIMEOUT:I = 0x1f4

.field private static final HOVER_TAP_SLOP:I = 0x14

.field private static final HOVER_TAP_TIMEOUT:I = 0x96

.field private static final JUMP_TAP_TIMEOUT:I = 0x1f4

.field private static final KEY_REPEAT_DELAY:I = 0x32

.field private static final MAXIMUM_DRAWING_CACHE_SIZE:I = 0x177000
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MAXIMUM_FLING_VELOCITY:I = 0x1f40

.field private static final MINIMUM_FLING_VELOCITY:I = 0x32

.field private static final OVERFLING_DISTANCE:I = 0x6

.field private static final OVERSCROLL_DISTANCE:I = 0x0

.field private static final PAGING_TOUCH_SLOP:I = 0x10

.field private static final PRESSED_STATE_DURATION:I = 0x40

.field private static final SCROLL_BAR_DEFAULT_DELAY:I = 0x12c

.field private static final SCROLL_BAR_FADE_DURATION:I = 0xfa

.field private static final SCROLL_BAR_SIZE:I = 0xa

.field private static final SCROLL_FRICTION:F = 0.015f

.field private static final SEND_RECURRING_ACCESSIBILITY_EVENTS_INTERVAL_MILLIS:J = 0x64L

.field private static final TAP_TIMEOUT:I = 0xb4

.field private static final TOUCH_SLOP:I = 0x8

.field private static final WINDOW_TOUCH_SLOP:I = 0x10

.field private static final ZOOM_CONTROLS_TIMEOUT:I = 0xbb8

.field static final sConfigurations:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/ViewConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDoubleTapSlop:I

.field private final mDoubleTapTouchSlop:I

.field private final mEdgeSlop:I

.field private final mFadingEdgeLength:I

.field private final mFadingMarqueeEnabled:Z

.field private final mMaximumDrawingCacheSize:I

.field private final mMaximumFlingVelocity:I

.field private final mMinimumFlingVelocity:I

.field private final mOverflingDistance:I

.field private final mOverscrollDistance:I

.field private final mPagingTouchSlop:I

.field private final mScrollbarSize:I

.field private final mTouchSlop:I

.field private final mWindowTouchSlop:I

.field private sHasPermanentMenuKey:Z

.field private sHasPermanentMenuKeySet:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 226
    new-instance v0, Landroid/util/SparseArray;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    #@6
    sput-object v0, Landroid/view/ViewConfiguration;->sConfigurations:Landroid/util/SparseArray;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/16 v2, 0x10

    #@2
    const/16 v0, 0xc

    #@4
    const/16 v1, 0x8

    #@6
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 234
    iput v0, p0, Landroid/view/ViewConfiguration;->mEdgeSlop:I

    #@b
    .line 235
    iput v0, p0, Landroid/view/ViewConfiguration;->mFadingEdgeLength:I

    #@d
    .line 236
    const/16 v0, 0x32

    #@f
    iput v0, p0, Landroid/view/ViewConfiguration;->mMinimumFlingVelocity:I

    #@11
    .line 237
    const/16 v0, 0x1f40

    #@13
    iput v0, p0, Landroid/view/ViewConfiguration;->mMaximumFlingVelocity:I

    #@15
    .line 238
    const/16 v0, 0xa

    #@17
    iput v0, p0, Landroid/view/ViewConfiguration;->mScrollbarSize:I

    #@19
    .line 239
    iput v1, p0, Landroid/view/ViewConfiguration;->mTouchSlop:I

    #@1b
    .line 240
    iput v1, p0, Landroid/view/ViewConfiguration;->mDoubleTapTouchSlop:I

    #@1d
    .line 241
    iput v2, p0, Landroid/view/ViewConfiguration;->mPagingTouchSlop:I

    #@1f
    .line 242
    const/16 v0, 0x64

    #@21
    iput v0, p0, Landroid/view/ViewConfiguration;->mDoubleTapSlop:I

    #@23
    .line 243
    iput v2, p0, Landroid/view/ViewConfiguration;->mWindowTouchSlop:I

    #@25
    .line 245
    const v0, 0x177000

    #@28
    iput v0, p0, Landroid/view/ViewConfiguration;->mMaximumDrawingCacheSize:I

    #@2a
    .line 246
    const/4 v0, 0x0

    #@2b
    iput v0, p0, Landroid/view/ViewConfiguration;->mOverscrollDistance:I

    #@2d
    .line 247
    const/4 v0, 0x6

    #@2e
    iput v0, p0, Landroid/view/ViewConfiguration;->mOverflingDistance:I

    #@30
    .line 248
    const/4 v0, 0x1

    #@31
    iput-boolean v0, p0, Landroid/view/ViewConfiguration;->mFadingMarqueeEnabled:Z

    #@33
    .line 249
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 14
    .parameter "context"

    #@0
    .prologue
    .line 261
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 262
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v5

    #@7
    .line 263
    .local v5, res:Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@a
    move-result-object v4

    #@b
    .line 264
    .local v4, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@e
    move-result-object v0

    #@f
    .line 265
    .local v0, config:Landroid/content/res/Configuration;
    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    #@11
    .line 267
    .local v1, density:F
    const/4 v10, 0x4

    #@12
    invoke-virtual {v0, v10}, Landroid/content/res/Configuration;->isLayoutSizeAtLeast(I)Z

    #@15
    move-result v10

    #@16
    if-eqz v10, :cond_c1

    #@18
    .line 268
    const/high16 v10, 0x3fc0

    #@1a
    mul-float v7, v1, v10

    #@1c
    .line 273
    .local v7, sizeAndDensity:F
    :goto_1c
    const/high16 v10, 0x4140

    #@1e
    mul-float/2addr v10, v7

    #@1f
    const/high16 v11, 0x3f00

    #@21
    add-float/2addr v10, v11

    #@22
    float-to-int v10, v10

    #@23
    iput v10, p0, Landroid/view/ViewConfiguration;->mEdgeSlop:I

    #@25
    .line 274
    const/high16 v10, 0x4140

    #@27
    mul-float/2addr v10, v7

    #@28
    const/high16 v11, 0x3f00

    #@2a
    add-float/2addr v10, v11

    #@2b
    float-to-int v10, v10

    #@2c
    iput v10, p0, Landroid/view/ViewConfiguration;->mFadingEdgeLength:I

    #@2e
    .line 275
    const/high16 v10, 0x4248

    #@30
    mul-float/2addr v10, v1

    #@31
    const/high16 v11, 0x3f00

    #@33
    add-float/2addr v10, v11

    #@34
    float-to-int v10, v10

    #@35
    iput v10, p0, Landroid/view/ViewConfiguration;->mMinimumFlingVelocity:I

    #@37
    .line 276
    const/high16 v10, 0x45fa

    #@39
    mul-float/2addr v10, v1

    #@3a
    const/high16 v11, 0x3f00

    #@3c
    add-float/2addr v10, v11

    #@3d
    float-to-int v10, v10

    #@3e
    iput v10, p0, Landroid/view/ViewConfiguration;->mMaximumFlingVelocity:I

    #@40
    .line 277
    const/high16 v10, 0x4120

    #@42
    mul-float/2addr v10, v1

    #@43
    const/high16 v11, 0x3f00

    #@45
    add-float/2addr v10, v11

    #@46
    float-to-int v10, v10

    #@47
    iput v10, p0, Landroid/view/ViewConfiguration;->mScrollbarSize:I

    #@49
    .line 278
    const/high16 v10, 0x42c8

    #@4b
    mul-float/2addr v10, v7

    #@4c
    const/high16 v11, 0x3f00

    #@4e
    add-float/2addr v10, v11

    #@4f
    float-to-int v10, v10

    #@50
    iput v10, p0, Landroid/view/ViewConfiguration;->mDoubleTapSlop:I

    #@52
    .line 279
    const/high16 v10, 0x4180

    #@54
    mul-float/2addr v10, v7

    #@55
    const/high16 v11, 0x3f00

    #@57
    add-float/2addr v10, v11

    #@58
    float-to-int v10, v10

    #@59
    iput v10, p0, Landroid/view/ViewConfiguration;->mWindowTouchSlop:I

    #@5b
    .line 282
    const-string/jumbo v10, "window"

    #@5e
    invoke-virtual {p1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@61
    move-result-object v8

    #@62
    check-cast v8, Landroid/view/WindowManager;

    #@64
    .line 283
    .local v8, win:Landroid/view/WindowManager;
    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@67
    move-result-object v2

    #@68
    .line 284
    .local v2, display:Landroid/view/Display;
    new-instance v6, Landroid/graphics/Point;

    #@6a
    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    #@6d
    .line 285
    .local v6, size:Landroid/graphics/Point;
    invoke-virtual {v2, v6}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@70
    .line 286
    iget v10, v6, Landroid/graphics/Point;->x:I

    #@72
    mul-int/lit8 v10, v10, 0x4

    #@74
    iget v11, v6, Landroid/graphics/Point;->y:I

    #@76
    mul-int/2addr v10, v11

    #@77
    iput v10, p0, Landroid/view/ViewConfiguration;->mMaximumDrawingCacheSize:I

    #@79
    .line 288
    const/4 v10, 0x0

    #@7a
    mul-float/2addr v10, v7

    #@7b
    const/high16 v11, 0x3f00

    #@7d
    add-float/2addr v10, v11

    #@7e
    float-to-int v10, v10

    #@7f
    iput v10, p0, Landroid/view/ViewConfiguration;->mOverscrollDistance:I

    #@81
    .line 289
    const/high16 v10, 0x40c0

    #@83
    mul-float/2addr v10, v7

    #@84
    const/high16 v11, 0x3f00

    #@86
    add-float/2addr v10, v11

    #@87
    float-to-int v10, v10

    #@88
    iput v10, p0, Landroid/view/ViewConfiguration;->mOverflingDistance:I

    #@8a
    .line 291
    iget-boolean v10, p0, Landroid/view/ViewConfiguration;->sHasPermanentMenuKeySet:Z

    #@8c
    if-nez v10, :cond_a4

    #@8e
    .line 292
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    #@91
    move-result-object v9

    #@92
    .line 294
    .local v9, wm:Landroid/view/IWindowManager;
    :try_start_92
    invoke-interface {v9}, Landroid/view/IWindowManager;->hasSystemNavBar()Z

    #@95
    move-result v10

    #@96
    if-nez v10, :cond_c4

    #@98
    invoke-interface {v9}, Landroid/view/IWindowManager;->hasNavigationBar()Z

    #@9b
    move-result v10

    #@9c
    if-nez v10, :cond_c4

    #@9e
    const/4 v10, 0x1

    #@9f
    :goto_9f
    iput-boolean v10, p0, Landroid/view/ViewConfiguration;->sHasPermanentMenuKey:Z

    #@a1
    .line 295
    const/4 v10, 0x1

    #@a2
    iput-boolean v10, p0, Landroid/view/ViewConfiguration;->sHasPermanentMenuKeySet:Z
    :try_end_a4
    .catch Landroid/os/RemoteException; {:try_start_92 .. :try_end_a4} :catch_c6

    #@a4
    .line 301
    .end local v9           #wm:Landroid/view/IWindowManager;
    :cond_a4
    :goto_a4
    const v10, 0x1110013

    #@a7
    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@aa
    move-result v10

    #@ab
    iput-boolean v10, p0, Landroid/view/ViewConfiguration;->mFadingMarqueeEnabled:Z

    #@ad
    .line 303
    const v10, 0x1050008

    #@b0
    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@b3
    move-result v10

    #@b4
    iput v10, p0, Landroid/view/ViewConfiguration;->mTouchSlop:I

    #@b6
    .line 305
    iget v10, p0, Landroid/view/ViewConfiguration;->mTouchSlop:I

    #@b8
    mul-int/lit8 v10, v10, 0x2

    #@ba
    iput v10, p0, Landroid/view/ViewConfiguration;->mPagingTouchSlop:I

    #@bc
    .line 307
    iget v10, p0, Landroid/view/ViewConfiguration;->mTouchSlop:I

    #@be
    iput v10, p0, Landroid/view/ViewConfiguration;->mDoubleTapTouchSlop:I

    #@c0
    .line 308
    return-void

    #@c1
    .line 270
    .end local v2           #display:Landroid/view/Display;
    .end local v6           #size:Landroid/graphics/Point;
    .end local v7           #sizeAndDensity:F
    .end local v8           #win:Landroid/view/WindowManager;
    :cond_c1
    move v7, v1

    #@c2
    .restart local v7       #sizeAndDensity:F
    goto/16 :goto_1c

    #@c4
    .line 294
    .restart local v2       #display:Landroid/view/Display;
    .restart local v6       #size:Landroid/graphics/Point;
    .restart local v8       #win:Landroid/view/WindowManager;
    .restart local v9       #wm:Landroid/view/IWindowManager;
    :cond_c4
    const/4 v10, 0x0

    #@c5
    goto :goto_9f

    #@c6
    .line 296
    :catch_c6
    move-exception v3

    #@c7
    .line 297
    .local v3, ex:Landroid/os/RemoteException;
    const/4 v10, 0x0

    #@c8
    iput-boolean v10, p0, Landroid/view/ViewConfiguration;->sHasPermanentMenuKey:Z

    #@ca
    goto :goto_a4
.end method

.method public static get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 318
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@7
    move-result-object v2

    #@8
    .line 319
    .local v2, metrics:Landroid/util/DisplayMetrics;
    const/high16 v3, 0x42c8

    #@a
    iget v4, v2, Landroid/util/DisplayMetrics;->density:F

    #@c
    mul-float/2addr v3, v4

    #@d
    float-to-int v1, v3

    #@e
    .line 321
    .local v1, density:I
    sget-object v3, Landroid/view/ViewConfiguration;->sConfigurations:Landroid/util/SparseArray;

    #@10
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/view/ViewConfiguration;

    #@16
    .line 322
    .local v0, configuration:Landroid/view/ViewConfiguration;
    if-nez v0, :cond_22

    #@18
    .line 323
    new-instance v0, Landroid/view/ViewConfiguration;

    #@1a
    .end local v0           #configuration:Landroid/view/ViewConfiguration;
    invoke-direct {v0, p0}, Landroid/view/ViewConfiguration;-><init>(Landroid/content/Context;)V

    #@1d
    .line 324
    .restart local v0       #configuration:Landroid/view/ViewConfiguration;
    sget-object v3, Landroid/view/ViewConfiguration;->sConfigurations:Landroid/util/SparseArray;

    #@1f
    invoke-virtual {v3, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@22
    .line 327
    :cond_22
    return-object v0
.end method

.method public static getDoubleTapSlop()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 520
    const/16 v0, 0x64

    #@2
    return v0
.end method

.method public static getDoubleTapTimeout()I
    .registers 1

    #@0
    .prologue
    .line 435
    const/16 v0, 0x12c

    #@2
    return v0
.end method

.method public static getEdgeSlop()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 466
    const/16 v0, 0xc

    #@2
    return v0
.end method

.method public static getFadingEdgeLength()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 370
    const/16 v0, 0xc

    #@2
    return v0
.end method

.method public static getGlobalActionKeyTimeout()J
    .registers 2

    #@0
    .prologue
    .line 655
    const-wide/16 v0, 0x1f4

    #@2
    return-wide v0
.end method

.method public static getHoverTapSlop()I
    .registers 1

    #@0
    .prologue
    .line 455
    const/16 v0, 0x14

    #@2
    return v0
.end method

.method public static getHoverTapTimeout()I
    .registers 1

    #@0
    .prologue
    .line 445
    const/16 v0, 0x96

    #@2
    return v0
.end method

.method public static getJumpTapTimeout()I
    .registers 1

    #@0
    .prologue
    .line 426
    const/16 v0, 0x1f4

    #@2
    return v0
.end method

.method public static getKeyRepeatDelay()I
    .registers 1

    #@0
    .prologue
    .line 408
    const/16 v0, 0x32

    #@2
    return v0
.end method

.method public static getKeyRepeatTimeout()I
    .registers 1

    #@0
    .prologue
    .line 401
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getLongPressTimeout()I
    .registers 2

    #@0
    .prologue
    .line 393
    const-string/jumbo v0, "long_press_timeout"

    #@3
    const/16 v1, 0x1f4

    #@5
    invoke-static {v0, v1}, Landroid/app/AppGlobals;->getIntCoreSetting(Ljava/lang/String;I)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public static getMaximumDrawingCacheSize()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 608
    const v0, 0x177000

    #@3
    return v0
.end method

.method public static getMaximumFlingVelocity()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 588
    const/16 v0, 0x1f40

    #@2
    return v0
.end method

.method public static getMinimumFlingVelocity()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 571
    const/16 v0, 0x32

    #@2
    return v0
.end method

.method public static getPressedStateDuration()I
    .registers 1

    #@0
    .prologue
    .line 385
    const/16 v0, 0x40

    #@2
    return v0
.end method

.method public static getScrollBarFadeDuration()I
    .registers 1

    #@0
    .prologue
    .line 353
    const/16 v0, 0xfa

    #@2
    return v0
.end method

.method public static getScrollBarSize()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 338
    const/16 v0, 0xa

    #@2
    return v0
.end method

.method public static getScrollDefaultDelay()I
    .registers 1

    #@0
    .prologue
    .line 360
    const/16 v0, 0x12c

    #@2
    return v0
.end method

.method public static getScrollFriction()F
    .registers 1

    #@0
    .prologue
    .line 665
    const v0, 0x3c75c28f

    #@3
    return v0
.end method

.method public static getSendRecurringAccessibilityEventsInterval()J
    .registers 2

    #@0
    .prologue
    .line 541
    const-wide/16 v0, 0x64

    #@2
    return-wide v0
.end method

.method public static getTapTimeout()I
    .registers 1

    #@0
    .prologue
    .line 417
    const/16 v0, 0xb4

    #@2
    return v0
.end method

.method public static getTouchSlop()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 484
    const/16 v0, 0x8

    #@2
    return v0
.end method

.method public static getWindowTouchSlop()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 553
    const/16 v0, 0x10

    #@2
    return v0
.end method

.method public static getZoomControlsTimeout()J
    .registers 2

    #@0
    .prologue
    .line 644
    const-wide/16 v0, 0xbb8

    #@2
    return-wide v0
.end method


# virtual methods
.method public getScaledDoubleTapSlop()I
    .registers 2

    #@0
    .prologue
    .line 528
    iget v0, p0, Landroid/view/ViewConfiguration;->mDoubleTapSlop:I

    #@2
    return v0
.end method

.method public getScaledDoubleTapTouchSlop()I
    .registers 2

    #@0
    .prologue
    .line 500
    iget v0, p0, Landroid/view/ViewConfiguration;->mDoubleTapTouchSlop:I

    #@2
    return v0
.end method

.method public getScaledEdgeSlop()I
    .registers 2

    #@0
    .prologue
    .line 474
    iget v0, p0, Landroid/view/ViewConfiguration;->mEdgeSlop:I

    #@2
    return v0
.end method

.method public getScaledFadingEdgeLength()I
    .registers 2

    #@0
    .prologue
    .line 377
    iget v0, p0, Landroid/view/ViewConfiguration;->mFadingEdgeLength:I

    #@2
    return v0
.end method

.method public getScaledMaximumDrawingCacheSize()I
    .registers 2

    #@0
    .prologue
    .line 617
    iget v0, p0, Landroid/view/ViewConfiguration;->mMaximumDrawingCacheSize:I

    #@2
    return v0
.end method

.method public getScaledMaximumFlingVelocity()I
    .registers 2

    #@0
    .prologue
    .line 595
    iget v0, p0, Landroid/view/ViewConfiguration;->mMaximumFlingVelocity:I

    #@2
    return v0
.end method

.method public getScaledMinimumFlingVelocity()I
    .registers 2

    #@0
    .prologue
    .line 578
    iget v0, p0, Landroid/view/ViewConfiguration;->mMinimumFlingVelocity:I

    #@2
    return v0
.end method

.method public getScaledOverflingDistance()I
    .registers 2

    #@0
    .prologue
    .line 633
    iget v0, p0, Landroid/view/ViewConfiguration;->mOverflingDistance:I

    #@2
    return v0
.end method

.method public getScaledOverscrollDistance()I
    .registers 2

    #@0
    .prologue
    .line 625
    iget v0, p0, Landroid/view/ViewConfiguration;->mOverscrollDistance:I

    #@2
    return v0
.end method

.method public getScaledPagingTouchSlop()I
    .registers 2

    #@0
    .prologue
    .line 508
    iget v0, p0, Landroid/view/ViewConfiguration;->mPagingTouchSlop:I

    #@2
    return v0
.end method

.method public getScaledScrollBarSize()I
    .registers 2

    #@0
    .prologue
    .line 346
    iget v0, p0, Landroid/view/ViewConfiguration;->mScrollbarSize:I

    #@2
    return v0
.end method

.method public getScaledTouchSlop()I
    .registers 2

    #@0
    .prologue
    .line 491
    iget v0, p0, Landroid/view/ViewConfiguration;->mTouchSlop:I

    #@2
    return v0
.end method

.method public getScaledWindowTouchSlop()I
    .registers 2

    #@0
    .prologue
    .line 561
    iget v0, p0, Landroid/view/ViewConfiguration;->mWindowTouchSlop:I

    #@2
    return v0
.end method

.method public hasPermanentMenuKey()Z
    .registers 2

    #@0
    .prologue
    .line 681
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isFadingMarqueeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 689
    iget-boolean v0, p0, Landroid/view/ViewConfiguration;->mFadingMarqueeEnabled:Z

    #@2
    return v0
.end method
