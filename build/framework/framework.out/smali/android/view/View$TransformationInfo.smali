.class Landroid/view/View$TransformationInfo;
.super Ljava/lang/Object;
.source "View.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TransformationInfo"
.end annotation


# instance fields
.field mAlpha:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mCamera:Landroid/graphics/Camera;

.field private mInverseMatrix:Landroid/graphics/Matrix;

.field private mInverseMatrixDirty:Z

.field private final mMatrix:Landroid/graphics/Matrix;

.field mMatrixDirty:Z

.field private mMatrixIsIdentity:Z

.field mPivotX:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mPivotY:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mPrevHeight:I

.field private mPrevWidth:I

.field mRotation:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mRotationX:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mRotationY:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mScaleX:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mScaleY:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mTranslationX:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mTranslationY:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private matrix3D:Landroid/graphics/Matrix;


# direct methods
.method constructor <init>()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, -0x1

    #@3
    const/high16 v2, 0x3f80

    #@5
    const/4 v1, 0x0

    #@6
    .line 2669
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 2677
    new-instance v0, Landroid/graphics/Matrix;

    #@b
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@e
    iput-object v0, p0, Landroid/view/View$TransformationInfo;->mMatrix:Landroid/graphics/Matrix;

    #@10
    .line 2693
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/view/View$TransformationInfo;->mMatrixDirty:Z

    #@13
    .line 2700
    iput-boolean v4, p0, Landroid/view/View$TransformationInfo;->mInverseMatrixDirty:Z

    #@15
    .line 2709
    iput-boolean v4, p0, Landroid/view/View$TransformationInfo;->mMatrixIsIdentity:Z

    #@17
    .line 2714
    iput-object v5, p0, Landroid/view/View$TransformationInfo;->mCamera:Landroid/graphics/Camera;

    #@19
    .line 2719
    iput-object v5, p0, Landroid/view/View$TransformationInfo;->matrix3D:Landroid/graphics/Matrix;

    #@1b
    .line 2726
    iput v3, p0, Landroid/view/View$TransformationInfo;->mPrevWidth:I

    #@1d
    .line 2727
    iput v3, p0, Landroid/view/View$TransformationInfo;->mPrevHeight:I

    #@1f
    .line 2732
    iput v1, p0, Landroid/view/View$TransformationInfo;->mRotationY:F

    #@21
    .line 2738
    iput v1, p0, Landroid/view/View$TransformationInfo;->mRotationX:F

    #@23
    .line 2744
    iput v1, p0, Landroid/view/View$TransformationInfo;->mRotation:F

    #@25
    .line 2750
    iput v1, p0, Landroid/view/View$TransformationInfo;->mTranslationX:F

    #@27
    .line 2756
    iput v1, p0, Landroid/view/View$TransformationInfo;->mTranslationY:F

    #@29
    .line 2763
    iput v2, p0, Landroid/view/View$TransformationInfo;->mScaleX:F

    #@2b
    .line 2770
    iput v2, p0, Landroid/view/View$TransformationInfo;->mScaleY:F

    #@2d
    .line 2776
    iput v1, p0, Landroid/view/View$TransformationInfo;->mPivotX:F

    #@2f
    .line 2782
    iput v1, p0, Landroid/view/View$TransformationInfo;->mPivotY:F

    #@31
    .line 2789
    iput v2, p0, Landroid/view/View$TransformationInfo;->mAlpha:F

    #@33
    return-void
.end method

.method static synthetic access$1400(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget-object v0, p0, Landroid/view/View$TransformationInfo;->mMatrix:Landroid/graphics/Matrix;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Landroid/view/View$TransformationInfo;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget-boolean v0, p0, Landroid/view/View$TransformationInfo;->mMatrixIsIdentity:Z

    #@2
    return v0
.end method

.method static synthetic access$1502(Landroid/view/View$TransformationInfo;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput-boolean p1, p0, Landroid/view/View$TransformationInfo;->mMatrixIsIdentity:Z

    #@2
    return p1
.end method

.method static synthetic access$1600(Landroid/view/View$TransformationInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget v0, p0, Landroid/view/View$TransformationInfo;->mPrevWidth:I

    #@2
    return v0
.end method

.method static synthetic access$1602(Landroid/view/View$TransformationInfo;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput p1, p0, Landroid/view/View$TransformationInfo;->mPrevWidth:I

    #@2
    return p1
.end method

.method static synthetic access$1700(Landroid/view/View$TransformationInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget v0, p0, Landroid/view/View$TransformationInfo;->mPrevHeight:I

    #@2
    return v0
.end method

.method static synthetic access$1702(Landroid/view/View$TransformationInfo;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput p1, p0, Landroid/view/View$TransformationInfo;->mPrevHeight:I

    #@2
    return p1
.end method

.method static synthetic access$1800(Landroid/view/View$TransformationInfo;)Landroid/graphics/Camera;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget-object v0, p0, Landroid/view/View$TransformationInfo;->mCamera:Landroid/graphics/Camera;

    #@2
    return-object v0
.end method

.method static synthetic access$1802(Landroid/view/View$TransformationInfo;Landroid/graphics/Camera;)Landroid/graphics/Camera;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput-object p1, p0, Landroid/view/View$TransformationInfo;->mCamera:Landroid/graphics/Camera;

    #@2
    return-object p1
.end method

.method static synthetic access$1900(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget-object v0, p0, Landroid/view/View$TransformationInfo;->matrix3D:Landroid/graphics/Matrix;

    #@2
    return-object v0
.end method

.method static synthetic access$1902(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput-object p1, p0, Landroid/view/View$TransformationInfo;->matrix3D:Landroid/graphics/Matrix;

    #@2
    return-object p1
.end method

.method static synthetic access$2000(Landroid/view/View$TransformationInfo;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget-boolean v0, p0, Landroid/view/View$TransformationInfo;->mInverseMatrixDirty:Z

    #@2
    return v0
.end method

.method static synthetic access$2002(Landroid/view/View$TransformationInfo;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput-boolean p1, p0, Landroid/view/View$TransformationInfo;->mInverseMatrixDirty:Z

    #@2
    return p1
.end method

.method static synthetic access$2100(Landroid/view/View$TransformationInfo;)Landroid/graphics/Matrix;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2669
    iget-object v0, p0, Landroid/view/View$TransformationInfo;->mInverseMatrix:Landroid/graphics/Matrix;

    #@2
    return-object v0
.end method

.method static synthetic access$2102(Landroid/view/View$TransformationInfo;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2669
    iput-object p1, p0, Landroid/view/View$TransformationInfo;->mInverseMatrix:Landroid/graphics/Matrix;

    #@2
    return-object p1
.end method
