.class final Landroid/view/Choreographer$CallbackRecord;
.super Ljava/lang/Object;
.source "Choreographer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Choreographer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CallbackRecord"
.end annotation


# instance fields
.field public action:Ljava/lang/Object;

.field public dueTime:J

.field public next:Landroid/view/Choreographer$CallbackRecord;

.field public token:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 739
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/Choreographer$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 739
    invoke-direct {p0}, Landroid/view/Choreographer$CallbackRecord;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public run(J)V
    .registers 5
    .parameter "frameTimeNanos"

    #@0
    .prologue
    .line 746
    iget-object v0, p0, Landroid/view/Choreographer$CallbackRecord;->token:Ljava/lang/Object;

    #@2
    invoke-static {}, Landroid/view/Choreographer;->access$400()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    if-ne v0, v1, :cond_10

    #@8
    .line 747
    iget-object v0, p0, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    #@a
    check-cast v0, Landroid/view/Choreographer$FrameCallback;

    #@c
    invoke-interface {v0, p1, p2}, Landroid/view/Choreographer$FrameCallback;->doFrame(J)V

    #@f
    .line 751
    :goto_f
    return-void

    #@10
    .line 749
    :cond_10
    iget-object v0, p0, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    #@12
    check-cast v0, Ljava/lang/Runnable;

    #@14
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@17
    goto :goto_f
.end method
