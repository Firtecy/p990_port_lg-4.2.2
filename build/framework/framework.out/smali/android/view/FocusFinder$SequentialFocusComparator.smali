.class final Landroid/view/FocusFinder$SequentialFocusComparator;
.super Ljava/lang/Object;
.source "FocusFinder.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/FocusFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SequentialFocusComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field private final mFirstRect:Landroid/graphics/Rect;

.field private mRoot:Landroid/view/ViewGroup;

.field private final mSecondRect:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 630
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 631
    new-instance v0, Landroid/graphics/Rect;

    #@5
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v0, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@a
    .line 632
    new-instance v0, Landroid/graphics/Rect;

    #@c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v0, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@11
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/FocusFinder$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 630
    invoke-direct {p0}, Landroid/view/FocusFinder$SequentialFocusComparator;-><init>()V

    #@3
    return-void
.end method

.method private getRect(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 4
    .parameter "view"
    .parameter "rect"

    #@0
    .prologue
    .line 676
    invoke-virtual {p1, p2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    #@3
    .line 677
    iget-object v0, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mRoot:Landroid/view/ViewGroup;

    #@5
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    #@8
    .line 678
    return-void
.end method


# virtual methods
.method public compare(Landroid/view/View;Landroid/view/View;)I
    .registers 8
    .parameter "first"
    .parameter "second"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v1, -0x1

    #@3
    .line 644
    if-ne p1, p2, :cond_6

    #@5
    .line 671
    :cond_5
    :goto_5
    return v0

    #@6
    .line 648
    :cond_6
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@8
    invoke-direct {p0, p1, v3}, Landroid/view/FocusFinder$SequentialFocusComparator;->getRect(Landroid/view/View;Landroid/graphics/Rect;)V

    #@b
    .line 649
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@d
    invoke-direct {p0, p2, v3}, Landroid/view/FocusFinder$SequentialFocusComparator;->getRect(Landroid/view/View;Landroid/graphics/Rect;)V

    #@10
    .line 651
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@12
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@14
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@16
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@18
    if-ge v3, v4, :cond_1c

    #@1a
    move v0, v1

    #@1b
    .line 652
    goto :goto_5

    #@1c
    .line 653
    :cond_1c
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@1e
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@20
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@22
    iget v4, v4, Landroid/graphics/Rect;->top:I

    #@24
    if-le v3, v4, :cond_28

    #@26
    move v0, v2

    #@27
    .line 654
    goto :goto_5

    #@28
    .line 655
    :cond_28
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@2a
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@2c
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@2e
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@30
    if-ge v3, v4, :cond_34

    #@32
    move v0, v1

    #@33
    .line 656
    goto :goto_5

    #@34
    .line 657
    :cond_34
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@36
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@38
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@3a
    iget v4, v4, Landroid/graphics/Rect;->left:I

    #@3c
    if-le v3, v4, :cond_40

    #@3e
    move v0, v2

    #@3f
    .line 658
    goto :goto_5

    #@40
    .line 659
    :cond_40
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@42
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@44
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@46
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@48
    if-ge v3, v4, :cond_4c

    #@4a
    move v0, v1

    #@4b
    .line 660
    goto :goto_5

    #@4c
    .line 661
    :cond_4c
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@4e
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    #@50
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@52
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    #@54
    if-le v3, v4, :cond_58

    #@56
    move v0, v2

    #@57
    .line 662
    goto :goto_5

    #@58
    .line 663
    :cond_58
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@5a
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@5c
    iget-object v4, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@5e
    iget v4, v4, Landroid/graphics/Rect;->right:I

    #@60
    if-ge v3, v4, :cond_64

    #@62
    move v0, v1

    #@63
    .line 664
    goto :goto_5

    #@64
    .line 665
    :cond_64
    iget-object v1, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mFirstRect:Landroid/graphics/Rect;

    #@66
    iget v1, v1, Landroid/graphics/Rect;->right:I

    #@68
    iget-object v3, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mSecondRect:Landroid/graphics/Rect;

    #@6a
    iget v3, v3, Landroid/graphics/Rect;->right:I

    #@6c
    if-le v1, v3, :cond_5

    #@6e
    move v0, v2

    #@6f
    .line 666
    goto :goto_5
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 630
    check-cast p1, Landroid/view/View;

    #@2
    .end local p1
    check-cast p2, Landroid/view/View;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/view/FocusFinder$SequentialFocusComparator;->compare(Landroid/view/View;Landroid/view/View;)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public recycle()V
    .registers 2

    #@0
    .prologue
    .line 636
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mRoot:Landroid/view/ViewGroup;

    #@3
    .line 637
    return-void
.end method

.method public setRoot(Landroid/view/ViewGroup;)V
    .registers 2
    .parameter "root"

    #@0
    .prologue
    .line 640
    iput-object p1, p0, Landroid/view/FocusFinder$SequentialFocusComparator;->mRoot:Landroid/view/ViewGroup;

    #@2
    .line 641
    return-void
.end method
