.class public Landroid/view/animation/BounceInterpolator;
.super Ljava/lang/Object;
.source "BounceInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    return-void
.end method

.method private static bounce(F)F
    .registers 3
    .parameter "t"

    #@0
    .prologue
    .line 35
    mul-float v0, p0, p0

    #@2
    const/high16 v1, 0x4100

    #@4
    mul-float/2addr v0, v1

    #@5
    return v0
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 4
    .parameter "t"

    #@0
    .prologue
    .line 45
    const v0, 0x3f8fb15b

    #@3
    mul-float/2addr p1, v0

    #@4
    .line 46
    const v0, 0x3eb4fdf4

    #@7
    cmpg-float v0, p1, v0

    #@9
    if-gez v0, :cond_10

    #@b
    invoke-static {p1}, Landroid/view/animation/BounceInterpolator;->bounce(F)F

    #@e
    move-result v0

    #@f
    .line 49
    :goto_f
    return v0

    #@10
    .line 47
    :cond_10
    const v0, 0x3f3da512

    #@13
    cmpg-float v0, p1, v0

    #@15
    if-gez v0, :cond_25

    #@17
    const v0, 0x3f0c14a5

    #@1a
    sub-float v0, p1, v0

    #@1c
    invoke-static {v0}, Landroid/view/animation/BounceInterpolator;->bounce(F)F

    #@1f
    move-result v0

    #@20
    const v1, 0x3f333333

    #@23
    add-float/2addr v0, v1

    #@24
    goto :goto_f

    #@25
    .line 48
    :cond_25
    const v0, 0x3f76e2eb

    #@28
    cmpg-float v0, p1, v0

    #@2a
    if-gez v0, :cond_3a

    #@2c
    const v0, 0x3f5a43fe

    #@2f
    sub-float v0, p1, v0

    #@31
    invoke-static {v0}, Landroid/view/animation/BounceInterpolator;->bounce(F)F

    #@34
    move-result v0

    #@35
    const v1, 0x3f666666

    #@38
    add-float/2addr v0, v1

    #@39
    goto :goto_f

    #@3a
    .line 49
    :cond_3a
    const v0, 0x3f859168

    #@3d
    sub-float v0, p1, v0

    #@3f
    invoke-static {v0}, Landroid/view/animation/BounceInterpolator;->bounce(F)F

    #@42
    move-result v0

    #@43
    const v1, 0x3f733333

    #@46
    add-float/2addr v0, v1

    #@47
    goto :goto_f
.end method
