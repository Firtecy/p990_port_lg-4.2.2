.class public Landroid/view/animation/AnimationSet;
.super Landroid/view/animation/Animation;
.source "AnimationSet.java"


# static fields
.field private static final PROPERTY_CHANGE_BOUNDS_MASK:I = 0x80

.field private static final PROPERTY_DURATION_MASK:I = 0x20

.field private static final PROPERTY_FILL_AFTER_MASK:I = 0x1

.field private static final PROPERTY_FILL_BEFORE_MASK:I = 0x2

.field private static final PROPERTY_MORPH_MATRIX_MASK:I = 0x40

.field private static final PROPERTY_REPEAT_MODE_MASK:I = 0x4

.field private static final PROPERTY_SHARE_INTERPOLATOR_MASK:I = 0x10

.field private static final PROPERTY_START_OFFSET_MASK:I = 0x8


# instance fields
.field private mAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field private mDirty:Z

.field private mFlags:I

.field private mHasAlpha:Z

.field private mLastEnd:J

.field private mStoredOffsets:[J

.field private mTempTransformation:Landroid/view/animation/Transformation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 81
    invoke-direct {p0, p1, p2}, Landroid/view/animation/Animation;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 62
    iput v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@7
    .line 66
    new-instance v1, Ljava/util/ArrayList;

    #@9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@c
    iput-object v1, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@e
    .line 68
    new-instance v1, Landroid/view/animation/Transformation;

    #@10
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@13
    iput-object v1, p0, Landroid/view/animation/AnimationSet;->mTempTransformation:Landroid/view/animation/Transformation;

    #@15
    .line 83
    sget-object v1, Lcom/android/internal/R$styleable;->AnimationSet:[I

    #@17
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@1a
    move-result-object v0

    #@1b
    .line 86
    .local v0, a:Landroid/content/res/TypedArray;
    const/16 v1, 0x10

    #@1d
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@20
    move-result v2

    #@21
    invoke-direct {p0, v1, v2}, Landroid/view/animation/AnimationSet;->setFlag(IZ)V

    #@24
    .line 88
    invoke-direct {p0}, Landroid/view/animation/AnimationSet;->init()V

    #@27
    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@2a
    move-result-object v1

    #@2b
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@2d
    const/16 v2, 0xe

    #@2f
    if-lt v1, v2, :cond_71

    #@31
    .line 92
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_3d

    #@37
    .line 93
    iget v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@39
    or-int/lit8 v1, v1, 0x20

    #@3b
    iput v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@3d
    .line 95
    :cond_3d
    const/4 v1, 0x2

    #@3e
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@41
    move-result v1

    #@42
    if-eqz v1, :cond_4a

    #@44
    .line 96
    iget v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@46
    or-int/lit8 v1, v1, 0x2

    #@48
    iput v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@4a
    .line 98
    :cond_4a
    const/4 v1, 0x3

    #@4b
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@4e
    move-result v1

    #@4f
    if-eqz v1, :cond_57

    #@51
    .line 99
    iget v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@53
    or-int/lit8 v1, v1, 0x1

    #@55
    iput v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@57
    .line 101
    :cond_57
    const/4 v1, 0x5

    #@58
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@5b
    move-result v1

    #@5c
    if-eqz v1, :cond_64

    #@5e
    .line 102
    iget v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@60
    or-int/lit8 v1, v1, 0x4

    #@62
    iput v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@64
    .line 104
    :cond_64
    const/4 v1, 0x4

    #@65
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@68
    move-result v1

    #@69
    if-eqz v1, :cond_71

    #@6b
    .line 105
    iget v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6d
    or-int/lit8 v1, v1, 0x8

    #@6f
    iput v1, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@71
    .line 109
    :cond_71
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@74
    .line 110
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 3
    .parameter "shareInterpolator"

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    #@3
    .line 62
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6
    .line 66
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@d
    .line 68
    new-instance v0, Landroid/view/animation/Transformation;

    #@f
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@12
    iput-object v0, p0, Landroid/view/animation/AnimationSet;->mTempTransformation:Landroid/view/animation/Transformation;

    #@14
    .line 121
    const/16 v0, 0x10

    #@16
    invoke-direct {p0, v0, p1}, Landroid/view/animation/AnimationSet;->setFlag(IZ)V

    #@19
    .line 122
    invoke-direct {p0}, Landroid/view/animation/AnimationSet;->init()V

    #@1c
    .line 123
    return-void
.end method

.method private init()V
    .registers 3

    #@0
    .prologue
    .line 150
    const-wide/16 v0, 0x0

    #@2
    iput-wide v0, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@4
    .line 151
    return-void
.end method

.method private setFlag(IZ)V
    .registers 5
    .parameter "mask"
    .parameter "value"

    #@0
    .prologue
    .line 142
    if-eqz p2, :cond_8

    #@2
    .line 143
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@4
    or-int/2addr v0, p1

    #@5
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@7
    .line 147
    :goto_7
    return-void

    #@8
    .line 145
    :cond_8
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@a
    xor-int/lit8 v1, p1, -0x1

    #@c
    and-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@f
    goto :goto_7
.end method


# virtual methods
.method public addAnimation(Landroid/view/animation/Animation;)V
    .registers 11
    .parameter "a"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 219
    iget-object v3, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7
    .line 221
    iget v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@9
    and-int/lit8 v3, v3, 0x40

    #@b
    if-nez v3, :cond_43

    #@d
    move v1, v2

    #@e
    .line 222
    .local v1, noMatrix:Z
    :goto_e
    if-eqz v1, :cond_1c

    #@10
    invoke-virtual {p1}, Landroid/view/animation/Animation;->willChangeTransformationMatrix()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_1c

    #@16
    .line 223
    iget v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@18
    or-int/lit8 v3, v3, 0x40

    #@1a
    iput v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@1c
    .line 226
    :cond_1c
    iget v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@1e
    and-int/lit16 v3, v3, 0x80

    #@20
    if-nez v3, :cond_23

    #@22
    move v0, v2

    #@23
    .line 229
    .local v0, changeBounds:Z
    :cond_23
    if-eqz v0, :cond_31

    #@25
    invoke-virtual {p1}, Landroid/view/animation/Animation;->willChangeBounds()Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_31

    #@2b
    .line 230
    iget v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2d
    or-int/lit16 v3, v3, 0x80

    #@2f
    iput v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@31
    .line 233
    :cond_31
    iget v3, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@33
    and-int/lit8 v3, v3, 0x20

    #@35
    const/16 v4, 0x20

    #@37
    if-ne v3, v4, :cond_45

    #@39
    .line 234
    iget-wide v3, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@3b
    iget-wide v5, p0, Landroid/view/animation/Animation;->mDuration:J

    #@3d
    add-long/2addr v3, v5

    #@3e
    iput-wide v3, p0, Landroid/view/animation/AnimationSet;->mLastEnd:J

    #@40
    .line 245
    :goto_40
    iput-boolean v2, p0, Landroid/view/animation/AnimationSet;->mDirty:Z

    #@42
    .line 246
    return-void

    #@43
    .end local v0           #changeBounds:Z
    .end local v1           #noMatrix:Z
    :cond_43
    move v1, v0

    #@44
    .line 221
    goto :goto_e

    #@45
    .line 236
    .restart local v0       #changeBounds:Z
    .restart local v1       #noMatrix:Z
    :cond_45
    iget-object v3, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@47
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@4a
    move-result v3

    #@4b
    if-ne v3, v2, :cond_60

    #@4d
    .line 237
    invoke-virtual {p1}, Landroid/view/animation/Animation;->getStartOffset()J

    #@50
    move-result-wide v3

    #@51
    invoke-virtual {p1}, Landroid/view/animation/Animation;->getDuration()J

    #@54
    move-result-wide v5

    #@55
    add-long/2addr v3, v5

    #@56
    iput-wide v3, p0, Landroid/view/animation/Animation;->mDuration:J

    #@58
    .line 238
    iget-wide v3, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@5a
    iget-wide v5, p0, Landroid/view/animation/Animation;->mDuration:J

    #@5c
    add-long/2addr v3, v5

    #@5d
    iput-wide v3, p0, Landroid/view/animation/AnimationSet;->mLastEnd:J

    #@5f
    goto :goto_40

    #@60
    .line 240
    :cond_60
    iget-wide v3, p0, Landroid/view/animation/AnimationSet;->mLastEnd:J

    #@62
    invoke-virtual {p1}, Landroid/view/animation/Animation;->getStartOffset()J

    #@65
    move-result-wide v5

    #@66
    invoke-virtual {p1}, Landroid/view/animation/Animation;->getDuration()J

    #@69
    move-result-wide v7

    #@6a
    add-long/2addr v5, v7

    #@6b
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    #@6e
    move-result-wide v3

    #@6f
    iput-wide v3, p0, Landroid/view/animation/AnimationSet;->mLastEnd:J

    #@71
    .line 241
    iget-wide v3, p0, Landroid/view/animation/AnimationSet;->mLastEnd:J

    #@73
    iget-wide v5, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@75
    sub-long/2addr v3, v5

    #@76
    iput-wide v3, p0, Landroid/view/animation/Animation;->mDuration:J

    #@78
    goto :goto_40
.end method

.method protected bridge synthetic clone()Landroid/view/animation/Animation;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/view/animation/AnimationSet;->clone()Landroid/view/animation/AnimationSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected clone()Landroid/view/animation/AnimationSet;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 127
    invoke-super {p0}, Landroid/view/animation/Animation;->clone()Landroid/view/animation/Animation;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/view/animation/AnimationSet;

    #@6
    .line 128
    .local v0, animation:Landroid/view/animation/AnimationSet;
    new-instance v4, Landroid/view/animation/Transformation;

    #@8
    invoke-direct {v4}, Landroid/view/animation/Transformation;-><init>()V

    #@b
    iput-object v4, v0, Landroid/view/animation/AnimationSet;->mTempTransformation:Landroid/view/animation/Transformation;

    #@d
    .line 129
    new-instance v4, Ljava/util/ArrayList;

    #@f
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@12
    iput-object v4, v0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@14
    .line 131
    iget-object v4, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v2

    #@1a
    .line 132
    .local v2, count:I
    iget-object v1, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@1c
    .line 134
    .local v1, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    const/4 v3, 0x0

    #@1d
    .local v3, i:I
    :goto_1d
    if-ge v3, v2, :cond_31

    #@1f
    .line 135
    iget-object v5, v0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v4

    #@25
    check-cast v4, Landroid/view/animation/Animation;

    #@27
    invoke-virtual {v4}, Landroid/view/animation/Animation;->clone()Landroid/view/animation/Animation;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2e
    .line 134
    add-int/lit8 v3, v3, 0x1

    #@30
    goto :goto_1d

    #@31
    .line 138
    :cond_31
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/view/animation/AnimationSet;->clone()Landroid/view/animation/AnimationSet;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public computeDurationHint()J
    .registers 9

    #@0
    .prologue
    .line 324
    const-wide/16 v4, 0x0

    #@2
    .line 325
    .local v4, duration:J
    iget-object v7, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v1

    #@8
    .line 326
    .local v1, count:I
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@a
    .line 327
    .local v0, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    add-int/lit8 v6, v1, -0x1

    #@c
    .local v6, i:I
    :goto_c
    if-ltz v6, :cond_20

    #@e
    .line 328
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v7

    #@12
    check-cast v7, Landroid/view/animation/Animation;

    #@14
    invoke-virtual {v7}, Landroid/view/animation/Animation;->computeDurationHint()J

    #@17
    move-result-wide v2

    #@18
    .line 329
    .local v2, d:J
    cmp-long v7, v2, v4

    #@1a
    if-lez v7, :cond_1d

    #@1c
    move-wide v4, v2

    #@1d
    .line 327
    :cond_1d
    add-int/lit8 v6, v6, -0x1

    #@1f
    goto :goto_c

    #@20
    .line 331
    .end local v2           #d:J
    :cond_20
    return-wide v4
.end method

.method public getAnimations()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 512
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getDuration()J
    .registers 9

    #@0
    .prologue
    .line 301
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@2
    .line 302
    .local v0, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 303
    .local v1, count:I
    const-wide/16 v2, 0x0

    #@8
    .line 305
    .local v2, duration:J
    iget v6, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@a
    and-int/lit8 v6, v6, 0x20

    #@c
    const/16 v7, 0x20

    #@e
    if-ne v6, v7, :cond_16

    #@10
    const/4 v4, 0x1

    #@11
    .line 306
    .local v4, durationSet:Z
    :goto_11
    if-eqz v4, :cond_18

    #@13
    .line 307
    iget-wide v2, p0, Landroid/view/animation/Animation;->mDuration:J

    #@15
    .line 314
    :cond_15
    return-wide v2

    #@16
    .line 305
    .end local v4           #durationSet:Z
    :cond_16
    const/4 v4, 0x0

    #@17
    goto :goto_11

    #@18
    .line 309
    .restart local v4       #durationSet:Z
    :cond_18
    const/4 v5, 0x0

    #@19
    .local v5, i:I
    :goto_19
    if-ge v5, v1, :cond_15

    #@1b
    .line 310
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v6

    #@1f
    check-cast v6, Landroid/view/animation/Animation;

    #@21
    invoke-virtual {v6}, Landroid/view/animation/Animation;->getDuration()J

    #@24
    move-result-wide v6

    #@25
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    #@28
    move-result-wide v2

    #@29
    .line 309
    add-int/lit8 v5, v5, 0x1

    #@2b
    goto :goto_19
.end method

.method public getStartTime()J
    .registers 9

    #@0
    .prologue
    .line 268
    const-wide v4, 0x7fffffffffffffffL

    #@5
    .line 270
    .local v4, startTime:J
    iget-object v6, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    .line 271
    .local v2, count:I
    iget-object v1, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@d
    .line 273
    .local v1, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    const/4 v3, 0x0

    #@e
    .local v3, i:I
    :goto_e
    if-ge v3, v2, :cond_21

    #@10
    .line 274
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/view/animation/Animation;

    #@16
    .line 275
    .local v0, a:Landroid/view/animation/Animation;
    invoke-virtual {v0}, Landroid/view/animation/Animation;->getStartTime()J

    #@19
    move-result-wide v6

    #@1a
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    #@1d
    move-result-wide v4

    #@1e
    .line 273
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_e

    #@21
    .line 278
    .end local v0           #a:Landroid/view/animation/Animation;
    :cond_21
    return-wide v4
.end method

.method public getTransformation(JLandroid/view/animation/Transformation;)Z
    .registers 15
    .parameter "currentTime"
    .parameter "t"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 370
    iget-object v10, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    .line 371
    .local v2, count:I
    iget-object v1, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@a
    .line 372
    .local v1, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    iget-object v7, p0, Landroid/view/animation/AnimationSet;->mTempTransformation:Landroid/view/animation/Transformation;

    #@c
    .line 374
    .local v7, temp:Landroid/view/animation/Transformation;
    const/4 v5, 0x0

    #@d
    .line 375
    .local v5, more:Z
    const/4 v6, 0x0

    #@e
    .line 376
    .local v6, started:Z
    const/4 v3, 0x1

    #@f
    .line 378
    .local v3, ended:Z
    invoke-virtual {p3}, Landroid/view/animation/Transformation;->clear()V

    #@12
    .line 380
    add-int/lit8 v4, v2, -0x1

    #@14
    .local v4, i:I
    :goto_14
    if-ltz v4, :cond_4a

    #@16
    .line 381
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/view/animation/Animation;

    #@1c
    .line 383
    .local v0, a:Landroid/view/animation/Animation;
    invoke-virtual {v7}, Landroid/view/animation/Transformation;->clear()V

    #@1f
    .line 384
    invoke-virtual {p0}, Landroid/view/animation/AnimationSet;->getScaleFactor()F

    #@22
    move-result v10

    #@23
    invoke-virtual {v0, p1, p2, v7, v10}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;F)Z

    #@26
    move-result v10

    #@27
    if-nez v10, :cond_2b

    #@29
    if-eqz v5, :cond_44

    #@2b
    :cond_2b
    move v5, v9

    #@2c
    .line 385
    :goto_2c
    invoke-virtual {p3, v7}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    #@2f
    .line 387
    if-nez v6, :cond_37

    #@31
    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    #@34
    move-result v10

    #@35
    if-eqz v10, :cond_46

    #@37
    :cond_37
    move v6, v9

    #@38
    .line 388
    :goto_38
    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    #@3b
    move-result v10

    #@3c
    if-eqz v10, :cond_48

    #@3e
    if-eqz v3, :cond_48

    #@40
    move v3, v9

    #@41
    .line 380
    :goto_41
    add-int/lit8 v4, v4, -0x1

    #@43
    goto :goto_14

    #@44
    :cond_44
    move v5, v8

    #@45
    .line 384
    goto :goto_2c

    #@46
    :cond_46
    move v6, v8

    #@47
    .line 387
    goto :goto_38

    #@48
    :cond_48
    move v3, v8

    #@49
    .line 388
    goto :goto_41

    #@4a
    .line 391
    .end local v0           #a:Landroid/view/animation/Animation;
    :cond_4a
    if-eqz v6, :cond_5b

    #@4c
    iget-boolean v8, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@4e
    if-nez v8, :cond_5b

    #@50
    .line 392
    iget-object v8, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@52
    if-eqz v8, :cond_59

    #@54
    .line 393
    iget-object v8, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@56
    invoke-interface {v8, p0}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V

    #@59
    .line 395
    :cond_59
    iput-boolean v9, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@5b
    .line 398
    :cond_5b
    iget-boolean v8, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@5d
    if-eq v3, v8, :cond_6a

    #@5f
    .line 399
    iget-object v8, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@61
    if-eqz v8, :cond_68

    #@63
    .line 400
    iget-object v8, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@65
    invoke-interface {v8, p0}, Landroid/view/animation/Animation$AnimationListener;->onAnimationEnd(Landroid/view/animation/Animation;)V

    #@68
    .line 402
    :cond_68
    iput-boolean v3, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@6a
    .line 405
    :cond_6a
    return v5
.end method

.method public hasAlpha()Z
    .registers 5

    #@0
    .prologue
    .line 182
    iget-boolean v3, p0, Landroid/view/animation/AnimationSet;->mDirty:Z

    #@2
    if-eqz v3, :cond_23

    #@4
    .line 183
    const/4 v3, 0x0

    #@5
    iput-boolean v3, p0, Landroid/view/animation/AnimationSet;->mHasAlpha:Z

    #@7
    iput-boolean v3, p0, Landroid/view/animation/AnimationSet;->mDirty:Z

    #@9
    .line 185
    iget-object v3, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v1

    #@f
    .line 186
    .local v1, count:I
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@11
    .line 188
    .local v0, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v1, :cond_23

    #@14
    .line 189
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    check-cast v3, Landroid/view/animation/Animation;

    #@1a
    invoke-virtual {v3}, Landroid/view/animation/Animation;->hasAlpha()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_26

    #@20
    .line 190
    const/4 v3, 0x1

    #@21
    iput-boolean v3, p0, Landroid/view/animation/AnimationSet;->mHasAlpha:Z

    #@23
    .line 196
    .end local v0           #animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    .end local v1           #count:I
    .end local v2           #i:I
    :cond_23
    iget-boolean v3, p0, Landroid/view/animation/AnimationSet;->mHasAlpha:Z

    #@25
    return v3

    #@26
    .line 188
    .restart local v0       #animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    .restart local v1       #count:I
    .restart local v2       #i:I
    :cond_26
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_12
.end method

.method public initialize(IIII)V
    .registers 32
    .parameter "width"
    .parameter "height"
    .parameter "parentWidth"
    .parameter "parentHeight"

    #@0
    .prologue
    .line 425
    invoke-super/range {p0 .. p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@3
    .line 427
    move-object/from16 v0, p0

    #@5
    iget v0, v0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@7
    move/from16 v25, v0

    #@9
    and-int/lit8 v25, v25, 0x20

    #@b
    const/16 v26, 0x20

    #@d
    move/from16 v0, v25

    #@f
    move/from16 v1, v26

    #@11
    if-ne v0, v1, :cond_f3

    #@13
    const/4 v9, 0x1

    #@14
    .line 428
    .local v9, durationSet:Z
    :goto_14
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@18
    move/from16 v25, v0

    #@1a
    and-int/lit8 v25, v25, 0x1

    #@1c
    const/16 v26, 0x1

    #@1e
    move/from16 v0, v25

    #@20
    move/from16 v1, v26

    #@22
    if-ne v0, v1, :cond_f6

    #@24
    const/4 v11, 0x1

    #@25
    .line 429
    .local v11, fillAfterSet:Z
    :goto_25
    move-object/from16 v0, p0

    #@27
    iget v0, v0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@29
    move/from16 v25, v0

    #@2b
    and-int/lit8 v25, v25, 0x2

    #@2d
    const/16 v26, 0x2

    #@2f
    move/from16 v0, v25

    #@31
    move/from16 v1, v26

    #@33
    if-ne v0, v1, :cond_f9

    #@35
    const/4 v13, 0x1

    #@36
    .line 430
    .local v13, fillBeforeSet:Z
    :goto_36
    move-object/from16 v0, p0

    #@38
    iget v0, v0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@3a
    move/from16 v25, v0

    #@3c
    and-int/lit8 v25, v25, 0x4

    #@3e
    const/16 v26, 0x4

    #@40
    move/from16 v0, v25

    #@42
    move/from16 v1, v26

    #@44
    if-ne v0, v1, :cond_fc

    #@46
    const/16 v19, 0x1

    #@48
    .line 431
    .local v19, repeatModeSet:Z
    :goto_48
    move-object/from16 v0, p0

    #@4a
    iget v0, v0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@4c
    move/from16 v25, v0

    #@4e
    and-int/lit8 v25, v25, 0x10

    #@50
    const/16 v26, 0x10

    #@52
    move/from16 v0, v25

    #@54
    move/from16 v1, v26

    #@56
    if-ne v0, v1, :cond_100

    #@58
    const/16 v20, 0x1

    #@5a
    .line 433
    .local v20, shareInterpolator:Z
    :goto_5a
    move-object/from16 v0, p0

    #@5c
    iget v0, v0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@5e
    move/from16 v25, v0

    #@60
    and-int/lit8 v25, v25, 0x8

    #@62
    const/16 v26, 0x8

    #@64
    move/from16 v0, v25

    #@66
    move/from16 v1, v26

    #@68
    if-ne v0, v1, :cond_104

    #@6a
    const/16 v23, 0x1

    #@6c
    .line 436
    .local v23, startOffsetSet:Z
    :goto_6c
    if-eqz v20, :cond_71

    #@6e
    .line 437
    invoke-virtual/range {p0 .. p0}, Landroid/view/animation/AnimationSet;->ensureInterpolator()V

    #@71
    .line 440
    :cond_71
    move-object/from16 v0, p0

    #@73
    iget-object v5, v0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@75
    .line 441
    .local v5, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@78
    move-result v6

    #@79
    .line 443
    .local v6, count:I
    move-object/from16 v0, p0

    #@7b
    iget-wide v7, v0, Landroid/view/animation/Animation;->mDuration:J

    #@7d
    .line 444
    .local v7, duration:J
    move-object/from16 v0, p0

    #@7f
    iget-boolean v10, v0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@81
    .line 445
    .local v10, fillAfter:Z
    move-object/from16 v0, p0

    #@83
    iget-boolean v12, v0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@85
    .line 446
    .local v12, fillBefore:Z
    move-object/from16 v0, p0

    #@87
    iget v0, v0, Landroid/view/animation/Animation;->mRepeatMode:I

    #@89
    move/from16 v18, v0

    #@8b
    .line 447
    .local v18, repeatMode:I
    move-object/from16 v0, p0

    #@8d
    iget-object v15, v0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@8f
    .line 448
    .local v15, interpolator:Landroid/view/animation/Interpolator;
    move-object/from16 v0, p0

    #@91
    iget-wide v0, v0, Landroid/view/animation/Animation;->mStartOffset:J

    #@93
    move-wide/from16 v21, v0

    #@95
    .line 451
    .local v21, startOffset:J
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Landroid/view/animation/AnimationSet;->mStoredOffsets:[J

    #@99
    move-object/from16 v24, v0

    #@9b
    .line 452
    .local v24, storedOffsets:[J
    if-eqz v23, :cond_108

    #@9d
    .line 453
    if-eqz v24, :cond_a8

    #@9f
    move-object/from16 v0, v24

    #@a1
    array-length v0, v0

    #@a2
    move/from16 v25, v0

    #@a4
    move/from16 v0, v25

    #@a6
    if-eq v0, v6, :cond_b2

    #@a8
    .line 454
    :cond_a8
    new-array v0, v6, [J

    #@aa
    move-object/from16 v24, v0

    #@ac
    .end local v24           #storedOffsets:[J
    move-object/from16 v0, v24

    #@ae
    move-object/from16 v1, p0

    #@b0
    iput-object v0, v1, Landroid/view/animation/AnimationSet;->mStoredOffsets:[J

    #@b2
    .line 460
    .restart local v24       #storedOffsets:[J
    :cond_b2
    :goto_b2
    const/4 v14, 0x0

    #@b3
    .local v14, i:I
    :goto_b3
    if-ge v14, v6, :cond_113

    #@b5
    .line 461
    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b8
    move-result-object v4

    #@b9
    check-cast v4, Landroid/view/animation/Animation;

    #@bb
    .line 462
    .local v4, a:Landroid/view/animation/Animation;
    if-eqz v9, :cond_c0

    #@bd
    .line 463
    invoke-virtual {v4, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    #@c0
    .line 465
    :cond_c0
    if-eqz v11, :cond_c5

    #@c2
    .line 466
    invoke-virtual {v4, v10}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    #@c5
    .line 468
    :cond_c5
    if-eqz v13, :cond_ca

    #@c7
    .line 469
    invoke-virtual {v4, v12}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    #@ca
    .line 471
    :cond_ca
    if-eqz v19, :cond_d1

    #@cc
    .line 472
    move/from16 v0, v18

    #@ce
    invoke-virtual {v4, v0}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    #@d1
    .line 474
    :cond_d1
    if-eqz v20, :cond_d6

    #@d3
    .line 475
    invoke-virtual {v4, v15}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@d6
    .line 477
    :cond_d6
    if-eqz v23, :cond_e5

    #@d8
    .line 478
    invoke-virtual {v4}, Landroid/view/animation/Animation;->getStartOffset()J

    #@db
    move-result-wide v16

    #@dc
    .line 479
    .local v16, offset:J
    add-long v25, v16, v21

    #@de
    move-wide/from16 v0, v25

    #@e0
    invoke-virtual {v4, v0, v1}, Landroid/view/animation/Animation;->setStartOffset(J)V

    #@e3
    .line 480
    aput-wide v16, v24, v14

    #@e5
    .line 482
    .end local v16           #offset:J
    :cond_e5
    move/from16 v0, p1

    #@e7
    move/from16 v1, p2

    #@e9
    move/from16 v2, p3

    #@eb
    move/from16 v3, p4

    #@ed
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/animation/Animation;->initialize(IIII)V

    #@f0
    .line 460
    add-int/lit8 v14, v14, 0x1

    #@f2
    goto :goto_b3

    #@f3
    .line 427
    .end local v4           #a:Landroid/view/animation/Animation;
    .end local v5           #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    .end local v6           #count:I
    .end local v7           #duration:J
    .end local v9           #durationSet:Z
    .end local v10           #fillAfter:Z
    .end local v11           #fillAfterSet:Z
    .end local v12           #fillBefore:Z
    .end local v13           #fillBeforeSet:Z
    .end local v14           #i:I
    .end local v15           #interpolator:Landroid/view/animation/Interpolator;
    .end local v18           #repeatMode:I
    .end local v19           #repeatModeSet:Z
    .end local v20           #shareInterpolator:Z
    .end local v21           #startOffset:J
    .end local v23           #startOffsetSet:Z
    .end local v24           #storedOffsets:[J
    :cond_f3
    const/4 v9, 0x0

    #@f4
    goto/16 :goto_14

    #@f6
    .line 428
    .restart local v9       #durationSet:Z
    :cond_f6
    const/4 v11, 0x0

    #@f7
    goto/16 :goto_25

    #@f9
    .line 429
    .restart local v11       #fillAfterSet:Z
    :cond_f9
    const/4 v13, 0x0

    #@fa
    goto/16 :goto_36

    #@fc
    .line 430
    .restart local v13       #fillBeforeSet:Z
    :cond_fc
    const/16 v19, 0x0

    #@fe
    goto/16 :goto_48

    #@100
    .line 431
    .restart local v19       #repeatModeSet:Z
    :cond_100
    const/16 v20, 0x0

    #@102
    goto/16 :goto_5a

    #@104
    .line 433
    .restart local v20       #shareInterpolator:Z
    :cond_104
    const/16 v23, 0x0

    #@106
    goto/16 :goto_6c

    #@108
    .line 456
    .restart local v5       #children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    .restart local v6       #count:I
    .restart local v7       #duration:J
    .restart local v10       #fillAfter:Z
    .restart local v12       #fillBefore:Z
    .restart local v15       #interpolator:Landroid/view/animation/Interpolator;
    .restart local v18       #repeatMode:I
    .restart local v21       #startOffset:J
    .restart local v23       #startOffsetSet:Z
    .restart local v24       #storedOffsets:[J
    :cond_108
    if-eqz v24, :cond_b2

    #@10a
    .line 457
    const/16 v24, 0x0

    #@10c
    move-object/from16 v0, v24

    #@10e
    move-object/from16 v1, p0

    #@110
    iput-object v0, v1, Landroid/view/animation/AnimationSet;->mStoredOffsets:[J

    #@112
    goto :goto_b2

    #@113
    .line 484
    .restart local v14       #i:I
    :cond_113
    return-void
.end method

.method public initializeInvalidateRegion(IIII)V
    .registers 18
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 338
    iget-object v7, p0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@2
    .line 339
    .local v7, region:Landroid/graphics/RectF;
    int-to-float v9, p1

    #@3
    int-to-float v10, p2

    #@4
    move/from16 v0, p3

    #@6
    int-to-float v11, v0

    #@7
    move/from16 v0, p4

    #@9
    int-to-float v12, v0

    #@a
    invoke-virtual {v7, v9, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    #@d
    .line 340
    const/high16 v9, -0x4080

    #@f
    const/high16 v10, -0x4080

    #@11
    invoke-virtual {v7, v9, v10}, Landroid/graphics/RectF;->inset(FF)V

    #@14
    .line 342
    iget-boolean v9, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@16
    if-eqz v9, :cond_5b

    #@18
    .line 343
    iget-object v9, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v3

    #@1e
    .line 344
    .local v3, count:I
    iget-object v2, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@20
    .line 345
    .local v2, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    iget-object v8, p0, Landroid/view/animation/AnimationSet;->mTempTransformation:Landroid/view/animation/Transformation;

    #@22
    .line 347
    .local v8, temp:Landroid/view/animation/Transformation;
    iget-object v6, p0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@24
    .line 349
    .local v6, previousTransformation:Landroid/view/animation/Transformation;
    add-int/lit8 v4, v3, -0x1

    #@26
    .local v4, i:I
    :goto_26
    if-ltz v4, :cond_5b

    #@28
    .line 350
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    check-cast v1, Landroid/view/animation/Animation;

    #@2e
    .line 351
    .local v1, a:Landroid/view/animation/Animation;
    invoke-virtual {v1}, Landroid/view/animation/Animation;->isFillEnabled()Z

    #@31
    move-result v9

    #@32
    if-eqz v9, :cond_44

    #@34
    invoke-virtual {v1}, Landroid/view/animation/Animation;->getFillBefore()Z

    #@37
    move-result v9

    #@38
    if-nez v9, :cond_44

    #@3a
    invoke-virtual {v1}, Landroid/view/animation/Animation;->getStartOffset()J

    #@3d
    move-result-wide v9

    #@3e
    const-wide/16 v11, 0x0

    #@40
    cmp-long v9, v9, v11

    #@42
    if-nez v9, :cond_56

    #@44
    .line 352
    :cond_44
    invoke-virtual {v8}, Landroid/view/animation/Transformation;->clear()V

    #@47
    .line 353
    iget-object v5, v1, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@49
    .line 354
    .local v5, interpolator:Landroid/view/animation/Interpolator;
    if-eqz v5, :cond_59

    #@4b
    const/4 v9, 0x0

    #@4c
    invoke-interface {v5, v9}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@4f
    move-result v9

    #@50
    :goto_50
    invoke-virtual {v1, v9, v8}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    #@53
    .line 356
    invoke-virtual {v6, v8}, Landroid/view/animation/Transformation;->compose(Landroid/view/animation/Transformation;)V

    #@56
    .line 349
    .end local v5           #interpolator:Landroid/view/animation/Interpolator;
    :cond_56
    add-int/lit8 v4, v4, -0x1

    #@58
    goto :goto_26

    #@59
    .line 354
    .restart local v5       #interpolator:Landroid/view/animation/Interpolator;
    :cond_59
    const/4 v9, 0x0

    #@5a
    goto :goto_50

    #@5b
    .line 360
    .end local v1           #a:Landroid/view/animation/Animation;
    .end local v2           #animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    .end local v3           #count:I
    .end local v4           #i:I
    .end local v5           #interpolator:Landroid/view/animation/Interpolator;
    .end local v6           #previousTransformation:Landroid/view/animation/Transformation;
    .end local v8           #temp:Landroid/view/animation/Transformation;
    :cond_5b
    return-void
.end method

.method public reset()V
    .registers 1

    #@0
    .prologue
    .line 488
    invoke-super {p0}, Landroid/view/animation/Animation;->reset()V

    #@3
    .line 489
    invoke-virtual {p0}, Landroid/view/animation/AnimationSet;->restoreChildrenStartOffset()V

    #@6
    .line 490
    return-void
.end method

.method restoreChildrenStartOffset()V
    .registers 8

    #@0
    .prologue
    .line 496
    iget-object v3, p0, Landroid/view/animation/AnimationSet;->mStoredOffsets:[J

    #@2
    .line 497
    .local v3, offsets:[J
    if-nez v3, :cond_5

    #@4
    .line 505
    :cond_4
    return-void

    #@5
    .line 499
    :cond_5
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@7
    .line 500
    .local v0, children:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v1

    #@b
    .line 502
    .local v1, count:I
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v1, :cond_4

    #@e
    .line 503
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, Landroid/view/animation/Animation;

    #@14
    aget-wide v5, v3, v2

    #@16
    invoke-virtual {v4, v5, v6}, Landroid/view/animation/Animation;->setStartOffset(J)V

    #@19
    .line 502
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_c
.end method

.method public restrictDuration(J)V
    .registers 7
    .parameter "durationMillis"

    #@0
    .prologue
    .line 283
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->restrictDuration(J)V

    #@3
    .line 285
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@5
    .line 286
    .local v0, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    .line 288
    .local v1, count:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v1, :cond_18

    #@c
    .line 289
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Landroid/view/animation/Animation;

    #@12
    invoke-virtual {v3, p1, p2}, Landroid/view/animation/Animation;->restrictDuration(J)V

    #@15
    .line 288
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_a

    #@18
    .line 291
    :cond_18
    return-void
.end method

.method public scaleCurrentDuration(F)V
    .registers 6
    .parameter "scale"

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@2
    .line 414
    .local v0, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 415
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_15

    #@9
    .line 416
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Landroid/view/animation/Animation;

    #@f
    invoke-virtual {v3, p1}, Landroid/view/animation/Animation;->scaleCurrentDuration(F)V

    #@12
    .line 415
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_7

    #@15
    .line 418
    :cond_15
    return-void
.end method

.method public setDuration(J)V
    .registers 7
    .parameter "durationMillis"

    #@0
    .prologue
    .line 207
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    or-int/lit8 v0, v0, 0x20

    #@4
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6
    .line 208
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->setDuration(J)V

    #@9
    .line 209
    iget-wide v0, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@b
    iget-wide v2, p0, Landroid/view/animation/Animation;->mDuration:J

    #@d
    add-long/2addr v0, v2

    #@e
    iput-wide v0, p0, Landroid/view/animation/AnimationSet;->mLastEnd:J

    #@10
    .line 210
    return-void
.end method

.method public setFillAfter(Z)V
    .registers 3
    .parameter "fillAfter"

    #@0
    .prologue
    .line 155
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    or-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6
    .line 156
    invoke-super {p0, p1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    #@9
    .line 157
    return-void
.end method

.method public setFillBefore(Z)V
    .registers 3
    .parameter "fillBefore"

    #@0
    .prologue
    .line 161
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    or-int/lit8 v0, v0, 0x2

    #@4
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6
    .line 162
    invoke-super {p0, p1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    #@9
    .line 163
    return-void
.end method

.method public setRepeatMode(I)V
    .registers 3
    .parameter "repeatMode"

    #@0
    .prologue
    .line 167
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    or-int/lit8 v0, v0, 0x4

    #@4
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6
    .line 168
    invoke-super {p0, p1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    #@9
    .line 169
    return-void
.end method

.method public setStartOffset(J)V
    .registers 4
    .parameter "startOffset"

    #@0
    .prologue
    .line 173
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    or-int/lit8 v0, v0, 0x8

    #@4
    iput v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@6
    .line 174
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    #@9
    .line 175
    return-void
.end method

.method public setStartTime(J)V
    .registers 8
    .parameter "startTimeMillis"

    #@0
    .prologue
    .line 255
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@3
    .line 257
    iget-object v4, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v2

    #@9
    .line 258
    .local v2, count:I
    iget-object v1, p0, Landroid/view/animation/AnimationSet;->mAnimations:Ljava/util/ArrayList;

    #@b
    .line 260
    .local v1, animations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/animation/Animation;>;"
    const/4 v3, 0x0

    #@c
    .local v3, i:I
    :goto_c
    if-ge v3, v2, :cond_1a

    #@e
    .line 261
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/view/animation/Animation;

    #@14
    .line 262
    .local v0, a:Landroid/view/animation/Animation;
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@17
    .line 260
    add-int/lit8 v3, v3, 0x1

    #@19
    goto :goto_c

    #@1a
    .line 264
    .end local v0           #a:Landroid/view/animation/Animation;
    :cond_1a
    return-void
.end method

.method public willChangeBounds()Z
    .registers 3

    #@0
    .prologue
    .line 522
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    and-int/lit16 v0, v0, 0x80

    #@4
    const/16 v1, 0x80

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public willChangeTransformationMatrix()Z
    .registers 3

    #@0
    .prologue
    .line 517
    iget v0, p0, Landroid/view/animation/AnimationSet;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x40

    #@4
    const/16 v1, 0x40

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
