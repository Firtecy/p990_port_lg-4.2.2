.class public Landroid/view/animation/AnticipateInterpolator;
.super Ljava/lang/Object;
.source "AnticipateInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final mTension:F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    const/high16 v0, 0x4000

    #@5
    iput v0, p0, Landroid/view/animation/AnticipateInterpolator;->mTension:F

    #@7
    .line 31
    return-void
.end method

.method public constructor <init>(F)V
    .registers 2
    .parameter "tension"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput p1, p0, Landroid/view/animation/AnticipateInterpolator;->mTension:F

    #@5
    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    sget-object v1, Lcom/android/internal/R$styleable;->AnticipateInterpolator:[I

    #@5
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v0

    #@9
    .line 46
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@a
    const/high16 v2, 0x4000

    #@c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@f
    move-result v1

    #@10
    iput v1, p0, Landroid/view/animation/AnticipateInterpolator;->mTension:F

    #@12
    .line 49
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@15
    .line 50
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 5
    .parameter "t"

    #@0
    .prologue
    .line 54
    mul-float v0, p1, p1

    #@2
    iget v1, p0, Landroid/view/animation/AnticipateInterpolator;->mTension:F

    #@4
    const/high16 v2, 0x3f80

    #@6
    add-float/2addr v1, v2

    #@7
    mul-float/2addr v1, p1

    #@8
    iget v2, p0, Landroid/view/animation/AnticipateInterpolator;->mTension:F

    #@a
    sub-float/2addr v1, v2

    #@b
    mul-float/2addr v0, v1

    #@c
    return v0
.end method
