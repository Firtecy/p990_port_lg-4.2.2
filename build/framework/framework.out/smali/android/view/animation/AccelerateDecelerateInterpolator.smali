.class public Landroid/view/animation/AccelerateDecelerateInterpolator;
.super Ljava/lang/Object;
.source "AccelerateDecelerateInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 36
    const/high16 v0, 0x3f80

    #@2
    add-float/2addr v0, p1

    #@3
    float-to-double v0, v0

    #@4
    const-wide v2, 0x400921fb54442d18L

    #@9
    mul-double/2addr v0, v2

    #@a
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    #@d
    move-result-wide v0

    #@e
    const-wide/high16 v2, 0x4000

    #@10
    div-double/2addr v0, v2

    #@11
    double-to-float v0, v0

    #@12
    const/high16 v1, 0x3f00

    #@14
    add-float/2addr v0, v1

    #@15
    return v0
.end method
