.class public Landroid/view/animation/AnticipateOvershootInterpolator;
.super Ljava/lang/Object;
.source "AnticipateOvershootInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final mTension:F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    const/high16 v0, 0x4040

    #@5
    iput v0, p0, Landroid/view/animation/AnticipateOvershootInterpolator;->mTension:F

    #@7
    .line 35
    return-void
.end method

.method public constructor <init>(F)V
    .registers 3
    .parameter "tension"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    const/high16 v0, 0x3fc0

    #@5
    mul-float/2addr v0, p1

    #@6
    iput v0, p0, Landroid/view/animation/AnticipateOvershootInterpolator;->mTension:F

    #@8
    .line 44
    return-void
.end method

.method public constructor <init>(FF)V
    .registers 4
    .parameter "tension"
    .parameter "extraTension"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    mul-float v0, p1, p2

    #@5
    iput v0, p0, Landroid/view/animation/AnticipateOvershootInterpolator;->mTension:F

    #@7
    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 59
    sget-object v1, Lcom/android/internal/R$styleable;->AnticipateOvershootInterpolator:[I

    #@5
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v0

    #@9
    .line 61
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@a
    const/high16 v2, 0x4000

    #@c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@f
    move-result v1

    #@10
    const/4 v2, 0x1

    #@11
    const/high16 v3, 0x3fc0

    #@13
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@16
    move-result v2

    #@17
    mul-float/2addr v1, v2

    #@18
    iput v1, p0, Landroid/view/animation/AnticipateOvershootInterpolator;->mTension:F

    #@1a
    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1d
    .line 65
    return-void
.end method

.method private static a(FF)F
    .registers 4
    .parameter "t"
    .parameter "s"

    #@0
    .prologue
    .line 68
    mul-float v0, p0, p0

    #@2
    const/high16 v1, 0x3f80

    #@4
    add-float/2addr v1, p1

    #@5
    mul-float/2addr v1, p0

    #@6
    sub-float/2addr v1, p1

    #@7
    mul-float/2addr v0, v1

    #@8
    return v0
.end method

.method private static o(FF)F
    .registers 4
    .parameter "t"
    .parameter "s"

    #@0
    .prologue
    .line 72
    mul-float v0, p0, p0

    #@2
    const/high16 v1, 0x3f80

    #@4
    add-float/2addr v1, p1

    #@5
    mul-float/2addr v1, p0

    #@6
    add-float/2addr v1, p1

    #@7
    mul-float/2addr v0, v1

    #@8
    return v0
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "t"

    #@0
    .prologue
    const/high16 v3, 0x3f00

    #@2
    const/high16 v2, 0x4000

    #@4
    .line 80
    cmpg-float v0, p1, v3

    #@6
    if-gez v0, :cond_12

    #@8
    mul-float v0, p1, v2

    #@a
    iget v1, p0, Landroid/view/animation/AnticipateOvershootInterpolator;->mTension:F

    #@c
    invoke-static {v0, v1}, Landroid/view/animation/AnticipateOvershootInterpolator;->a(FF)F

    #@f
    move-result v0

    #@10
    mul-float/2addr v0, v3

    #@11
    .line 81
    :goto_11
    return v0

    #@12
    :cond_12
    mul-float v0, p1, v2

    #@14
    sub-float/2addr v0, v2

    #@15
    iget v1, p0, Landroid/view/animation/AnticipateOvershootInterpolator;->mTension:F

    #@17
    invoke-static {v0, v1}, Landroid/view/animation/AnticipateOvershootInterpolator;->o(FF)F

    #@1a
    move-result v0

    #@1b
    add-float/2addr v0, v2

    #@1c
    mul-float/2addr v0, v3

    #@1d
    goto :goto_11
.end method
