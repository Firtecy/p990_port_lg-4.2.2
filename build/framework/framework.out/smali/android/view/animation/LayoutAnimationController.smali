.class public Landroid/view/animation/LayoutAnimationController;
.super Ljava/lang/Object;
.source "LayoutAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/animation/LayoutAnimationController$AnimationParameters;
    }
.end annotation


# static fields
.field public static final ORDER_NORMAL:I = 0x0

.field public static final ORDER_RANDOM:I = 0x2

.field public static final ORDER_REVERSE:I = 0x1


# instance fields
.field protected mAnimation:Landroid/view/animation/Animation;

.field private mDelay:F

.field private mDuration:J

.field protected mInterpolator:Landroid/view/animation/Interpolator;

.field private mMaxDelay:J

.field private mOrder:I

.field protected mRandomizer:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 103
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 104
    sget-object v3, Lcom/android/internal/R$styleable;->LayoutAnimation:[I

    #@6
    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 106
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x1

    #@b
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@e
    move-result-object v3

    #@f
    invoke-static {v3}, Landroid/view/animation/Animation$Description;->parseValue(Landroid/util/TypedValue;)Landroid/view/animation/Animation$Description;

    #@12
    move-result-object v1

    #@13
    .line 108
    .local v1, d:Landroid/view/animation/Animation$Description;
    iget v3, v1, Landroid/view/animation/Animation$Description;->value:F

    #@15
    iput v3, p0, Landroid/view/animation/LayoutAnimationController;->mDelay:F

    #@17
    .line 110
    const/4 v3, 0x3

    #@18
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1b
    move-result v3

    #@1c
    iput v3, p0, Landroid/view/animation/LayoutAnimationController;->mOrder:I

    #@1e
    .line 112
    const/4 v3, 0x2

    #@1f
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@22
    move-result v2

    #@23
    .line 113
    .local v2, resource:I
    if-lez v2, :cond_28

    #@25
    .line 114
    invoke-virtual {p0, p1, v2}, Landroid/view/animation/LayoutAnimationController;->setAnimation(Landroid/content/Context;I)V

    #@28
    .line 117
    :cond_28
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@2b
    move-result v2

    #@2c
    .line 118
    if-lez v2, :cond_31

    #@2e
    .line 119
    invoke-virtual {p0, p1, v2}, Landroid/view/animation/LayoutAnimationController;->setInterpolator(Landroid/content/Context;I)V

    #@31
    .line 122
    :cond_31
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@34
    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Animation;)V
    .registers 3
    .parameter "animation"

    #@0
    .prologue
    .line 132
    const/high16 v0, 0x3f00

    #@2
    invoke-direct {p0, p1, v0}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;F)V

    #@5
    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Animation;F)V
    .registers 3
    .parameter "animation"
    .parameter "delay"

    #@0
    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 143
    iput p2, p0, Landroid/view/animation/LayoutAnimationController;->mDelay:F

    #@5
    .line 144
    invoke-virtual {p0, p1}, Landroid/view/animation/LayoutAnimationController;->setAnimation(Landroid/view/animation/Animation;)V

    #@8
    .line 145
    return-void
.end method


# virtual methods
.method public getAnimation()Landroid/view/animation/Animation;
    .registers 2

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@2
    return-object v0
.end method

.method public final getAnimationForView(Landroid/view/View;)Landroid/view/animation/Animation;
    .registers 10
    .parameter "view"

    #@0
    .prologue
    .line 321
    invoke-virtual {p0, p1}, Landroid/view/animation/LayoutAnimationController;->getDelayForView(Landroid/view/View;)J

    #@3
    move-result-wide v4

    #@4
    iget-object v6, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@6
    invoke-virtual {v6}, Landroid/view/animation/Animation;->getStartOffset()J

    #@9
    move-result-wide v6

    #@a
    add-long v1, v4, v6

    #@c
    .line 322
    .local v1, delay:J
    iget-wide v4, p0, Landroid/view/animation/LayoutAnimationController;->mMaxDelay:J

    #@e
    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->max(JJ)J

    #@11
    move-result-wide v4

    #@12
    iput-wide v4, p0, Landroid/view/animation/LayoutAnimationController;->mMaxDelay:J

    #@14
    .line 325
    :try_start_14
    iget-object v4, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@16
    invoke-virtual {v4}, Landroid/view/animation/Animation;->clone()Landroid/view/animation/Animation;

    #@19
    move-result-object v0

    #@1a
    .line 326
    .local v0, animation:Landroid/view/animation/Animation;
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V
    :try_end_1d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_14 .. :try_end_1d} :catch_1e

    #@1d
    .line 329
    .end local v0           #animation:Landroid/view/animation/Animation;
    :goto_1d
    return-object v0

    #@1e
    .line 328
    :catch_1e
    move-exception v3

    #@1f
    .line 329
    .local v3, e:Ljava/lang/CloneNotSupportedException;
    const/4 v0, 0x0

    #@20
    goto :goto_1d
.end method

.method public getDelay()F
    .registers 2

    #@0
    .prologue
    .line 266
    iget v0, p0, Landroid/view/animation/LayoutAnimationController;->mDelay:F

    #@2
    return v0
.end method

.method protected getDelayForView(Landroid/view/View;)J
    .registers 12
    .parameter "view"

    #@0
    .prologue
    .line 369
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v1

    #@4
    .line 370
    .local v1, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, v1, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@6
    .line 372
    .local v3, params:Landroid/view/animation/LayoutAnimationController$AnimationParameters;
    if-nez v3, :cond_b

    #@8
    .line 373
    const-wide/16 v7, 0x0

    #@a
    .line 387
    :goto_a
    return-wide v7

    #@b
    .line 376
    :cond_b
    iget v7, p0, Landroid/view/animation/LayoutAnimationController;->mDelay:F

    #@d
    iget-object v8, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@f
    invoke-virtual {v8}, Landroid/view/animation/Animation;->getDuration()J

    #@12
    move-result-wide v8

    #@13
    long-to-float v8, v8

    #@14
    mul-float v0, v7, v8

    #@16
    .line 377
    .local v0, delay:F
    invoke-virtual {p0, v3}, Landroid/view/animation/LayoutAnimationController;->getTransformedIndex(Landroid/view/animation/LayoutAnimationController$AnimationParameters;)I

    #@19
    move-result v7

    #@1a
    int-to-float v7, v7

    #@1b
    mul-float/2addr v7, v0

    #@1c
    float-to-long v5, v7

    #@1d
    .line 378
    .local v5, viewDelay:J
    iget v7, v3, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->count:I

    #@1f
    int-to-float v7, v7

    #@20
    mul-float v4, v0, v7

    #@22
    .line 380
    .local v4, totalDelay:F
    iget-object v7, p0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@24
    if-nez v7, :cond_2d

    #@26
    .line 381
    new-instance v7, Landroid/view/animation/LinearInterpolator;

    #@28
    invoke-direct {v7}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@2b
    iput-object v7, p0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2d
    .line 384
    :cond_2d
    long-to-float v7, v5

    #@2e
    div-float v2, v7, v4

    #@30
    .line 385
    .local v2, normalizedDelay:F
    iget-object v7, p0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@32
    invoke-interface {v7, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@35
    move-result v2

    #@36
    .line 387
    mul-float v7, v2, v4

    #@38
    float-to-long v7, v7

    #@39
    goto :goto_a
.end method

.method public getInterpolator()Landroid/view/animation/Interpolator;
    .registers 2

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    return-object v0
.end method

.method public getOrder()I
    .registers 2

    #@0
    .prologue
    .line 156
    iget v0, p0, Landroid/view/animation/LayoutAnimationController;->mOrder:I

    #@2
    return v0
.end method

.method protected getTransformedIndex(Landroid/view/animation/LayoutAnimationController$AnimationParameters;)I
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 402
    invoke-virtual {p0}, Landroid/view/animation/LayoutAnimationController;->getOrder()I

    #@3
    move-result v0

    #@4
    packed-switch v0, :pswitch_data_2a

    #@7
    .line 412
    iget v0, p1, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->index:I

    #@9
    :goto_9
    return v0

    #@a
    .line 404
    :pswitch_a
    iget v0, p1, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->count:I

    #@c
    add-int/lit8 v0, v0, -0x1

    #@e
    iget v1, p1, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->index:I

    #@10
    sub-int/2addr v0, v1

    #@11
    goto :goto_9

    #@12
    .line 406
    :pswitch_12
    iget-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@14
    if-nez v0, :cond_1d

    #@16
    .line 407
    new-instance v0, Ljava/util/Random;

    #@18
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@1d
    .line 409
    :cond_1d
    iget v0, p1, Landroid/view/animation/LayoutAnimationController$AnimationParameters;->count:I

    #@1f
    int-to-float v0, v0

    #@20
    iget-object v1, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@22
    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    #@25
    move-result v1

    #@26
    mul-float/2addr v0, v1

    #@27
    float-to-int v0, v0

    #@28
    goto :goto_9

    #@29
    .line 402
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_a
        :pswitch_12
    .end packed-switch
.end method

.method public isDone()Z
    .registers 7

    #@0
    .prologue
    .line 340
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    iget-object v2, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@6
    invoke-virtual {v2}, Landroid/view/animation/Animation;->getStartTime()J

    #@9
    move-result-wide v2

    #@a
    iget-wide v4, p0, Landroid/view/animation/LayoutAnimationController;->mMaxDelay:J

    #@c
    add-long/2addr v2, v4

    #@d
    iget-wide v4, p0, Landroid/view/animation/LayoutAnimationController;->mDuration:J

    #@f
    add-long/2addr v2, v4

    #@10
    cmp-long v0, v0, v2

    #@12
    if-lez v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public setAnimation(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceID"

    #@0
    .prologue
    .line 184
    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/view/animation/LayoutAnimationController;->setAnimation(Landroid/view/animation/Animation;)V

    #@7
    .line 185
    return-void
.end method

.method public setAnimation(Landroid/view/animation/Animation;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 199
    iput-object p1, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@2
    .line 200
    iget-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@4
    const/4 v1, 0x1

    #@5
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    #@8
    .line 201
    return-void
.end method

.method public setDelay(F)V
    .registers 2
    .parameter "delay"

    #@0
    .prologue
    .line 282
    iput p1, p0, Landroid/view/animation/LayoutAnimationController;->mDelay:F

    #@2
    .line 283
    return-void
.end method

.method public setInterpolator(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resourceID"

    #@0
    .prologue
    .line 229
    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/view/animation/LayoutAnimationController;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@7
    .line 230
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .registers 2
    .parameter "interpolator"

    #@0
    .prologue
    .line 244
    iput-object p1, p0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    .line 245
    return-void
.end method

.method public setOrder(I)V
    .registers 2
    .parameter "order"

    #@0
    .prologue
    .line 168
    iput p1, p0, Landroid/view/animation/LayoutAnimationController;->mOrder:I

    #@2
    .line 169
    return-void
.end method

.method public start()V
    .registers 4

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@2
    invoke-virtual {v0}, Landroid/view/animation/Animation;->getDuration()J

    #@5
    move-result-wide v0

    #@6
    iput-wide v0, p0, Landroid/view/animation/LayoutAnimationController;->mDuration:J

    #@8
    .line 300
    const-wide/high16 v0, -0x8000

    #@a
    iput-wide v0, p0, Landroid/view/animation/LayoutAnimationController;->mMaxDelay:J

    #@c
    .line 301
    iget-object v0, p0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@e
    const-wide/16 v1, -0x1

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@13
    .line 302
    return-void
.end method

.method public willOverlap()Z
    .registers 3

    #@0
    .prologue
    .line 292
    iget v0, p0, Landroid/view/animation/LayoutAnimationController;->mDelay:F

    #@2
    const/high16 v1, 0x3f80

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method
