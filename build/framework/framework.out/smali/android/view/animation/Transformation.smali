.class public Landroid/view/animation/Transformation;
.super Ljava/lang/Object;
.source "Transformation.java"


# static fields
.field public static TYPE_ALPHA:I

.field public static TYPE_BOTH:I

.field public static TYPE_IDENTITY:I

.field public static TYPE_MATRIX:I


# instance fields
.field protected mAlpha:F

.field protected mMatrix:Landroid/graphics/Matrix;

.field protected mTransformationType:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 32
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/view/animation/Transformation;->TYPE_IDENTITY:I

    #@3
    .line 36
    const/4 v0, 0x1

    #@4
    sput v0, Landroid/view/animation/Transformation;->TYPE_ALPHA:I

    #@6
    .line 40
    const/4 v0, 0x2

    #@7
    sput v0, Landroid/view/animation/Transformation;->TYPE_MATRIX:I

    #@9
    .line 44
    sget v0, Landroid/view/animation/Transformation;->TYPE_ALPHA:I

    #@b
    sget v1, Landroid/view/animation/Transformation;->TYPE_MATRIX:I

    #@d
    or-int/2addr v0, v1

    #@e
    sput v0, Landroid/view/animation/Transformation;->TYPE_BOTH:I

    #@10
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    invoke-virtual {p0}, Landroid/view/animation/Transformation;->clear()V

    #@6
    .line 55
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@2
    if-nez v0, :cond_14

    #@4
    .line 64
    new-instance v0, Landroid/graphics/Matrix;

    #@6
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@b
    .line 68
    :goto_b
    const/high16 v0, 0x3f80

    #@d
    iput v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@f
    .line 69
    sget v0, Landroid/view/animation/Transformation;->TYPE_BOTH:I

    #@11
    iput v0, p0, Landroid/view/animation/Transformation;->mTransformationType:I

    #@13
    .line 70
    return-void

    #@14
    .line 66
    :cond_14
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@16
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    #@19
    goto :goto_b
.end method

.method public compose(Landroid/view/animation/Transformation;)V
    .registers 4
    .parameter "t"

    #@0
    .prologue
    .line 110
    iget v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@2
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getAlpha()F

    #@5
    move-result v1

    #@6
    mul-float/2addr v0, v1

    #@7
    iput v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@9
    .line 111
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@b
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    #@12
    .line 112
    return-void
.end method

.method public getAlpha()F
    .registers 2

    #@0
    .prologue
    .line 144
    iget v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@2
    return v0
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .registers 2

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@2
    return-object v0
.end method

.method public getTransformationType()I
    .registers 2

    #@0
    .prologue
    .line 79
    iget v0, p0, Landroid/view/animation/Transformation;->mTransformationType:I

    #@2
    return v0
.end method

.method public postCompose(Landroid/view/animation/Transformation;)V
    .registers 4
    .parameter "t"

    #@0
    .prologue
    .line 120
    iget v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@2
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getAlpha()F

    #@5
    move-result v1

    #@6
    mul-float/2addr v0, v1

    #@7
    iput v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@9
    .line 121
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@b
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    #@12
    .line 122
    return-void
.end method

.method public printShortString(Ljava/io/PrintWriter;)V
    .registers 3
    .parameter "pw"

    #@0
    .prologue
    .line 178
    const-string/jumbo v0, "{alpha="

    #@3
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6
    iget v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@b
    .line 179
    const-string v0, " matrix="

    #@d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    .line 180
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@12
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->printShortString(Ljava/io/PrintWriter;)V

    #@15
    .line 181
    const/16 v0, 0x7d

    #@17
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@1a
    .line 182
    return-void
.end method

.method public set(Landroid/view/animation/Transformation;)V
    .registers 4
    .parameter "t"

    #@0
    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getAlpha()F

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@6
    .line 100
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@8
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@f
    .line 101
    invoke-virtual {p1}, Landroid/view/animation/Transformation;->getTransformationType()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/view/animation/Transformation;->mTransformationType:I

    #@15
    .line 102
    return-void
.end method

.method public setAlpha(F)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 137
    iput p1, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@2
    .line 138
    return-void
.end method

.method public setTransformationType(I)V
    .registers 2
    .parameter "transformationType"

    #@0
    .prologue
    .line 90
    iput p1, p0, Landroid/view/animation/Transformation;->mTransformationType:I

    #@2
    .line 91
    return-void
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x40

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 160
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Landroid/view/animation/Transformation;->toShortString(Ljava/lang/StringBuilder;)V

    #@a
    .line 161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method public toShortString(Ljava/lang/StringBuilder;)V
    .registers 3
    .parameter "sb"

    #@0
    .prologue
    .line 168
    const-string/jumbo v0, "{alpha="

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6
    iget v0, p0, Landroid/view/animation/Transformation;->mAlpha:F

    #@8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@b
    .line 169
    const-string v0, " matrix="

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    iget-object v0, p0, Landroid/view/animation/Transformation;->mMatrix:Landroid/graphics/Matrix;

    #@12
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->toShortString(Ljava/lang/StringBuilder;)V

    #@15
    .line 170
    const/16 v0, 0x7d

    #@17
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a
    .line 171
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x40

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 150
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "Transformation"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 151
    invoke-virtual {p0, v0}, Landroid/view/animation/Transformation;->toShortString(Ljava/lang/StringBuilder;)V

    #@f
    .line 152
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method
