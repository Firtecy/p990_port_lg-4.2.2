.class public Landroid/view/animation/GridLayoutAnimationController;
.super Landroid/view/animation/LayoutAnimationController;
.source "GridLayoutAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    }
.end annotation


# static fields
.field public static final DIRECTION_BOTTOM_TO_TOP:I = 0x2

.field public static final DIRECTION_HORIZONTAL_MASK:I = 0x1

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x1

.field public static final DIRECTION_TOP_TO_BOTTOM:I = 0x0

.field public static final DIRECTION_VERTICAL_MASK:I = 0x2

.field public static final PRIORITY_COLUMN:I = 0x1

.field public static final PRIORITY_NONE:I = 0x0

.field public static final PRIORITY_ROW:I = 0x2


# instance fields
.field private mColumnDelay:F

.field private mDirection:I

.field private mDirectionPriority:I

.field private mRowDelay:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 113
    invoke-direct {p0, p1, p2}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 115
    sget-object v2, Lcom/android/internal/R$styleable;->GridLayoutAnimation:[I

    #@6
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 118
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Landroid/view/animation/Animation$Description;->parseValue(Landroid/util/TypedValue;)Landroid/view/animation/Animation$Description;

    #@11
    move-result-object v1

    #@12
    .line 120
    .local v1, d:Landroid/view/animation/Animation$Description;
    iget v2, v1, Landroid/view/animation/Animation$Description;->value:F

    #@14
    iput v2, p0, Landroid/view/animation/GridLayoutAnimationController;->mColumnDelay:F

    #@16
    .line 121
    const/4 v2, 0x1

    #@17
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v2}, Landroid/view/animation/Animation$Description;->parseValue(Landroid/util/TypedValue;)Landroid/view/animation/Animation$Description;

    #@1e
    move-result-object v1

    #@1f
    .line 123
    iget v2, v1, Landroid/view/animation/Animation$Description;->value:F

    #@21
    iput v2, p0, Landroid/view/animation/GridLayoutAnimationController;->mRowDelay:F

    #@23
    .line 125
    const/4 v2, 0x2

    #@24
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@27
    move-result v2

    #@28
    iput v2, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirection:I

    #@2a
    .line 127
    const/4 v2, 0x3

    #@2b
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2e
    move-result v2

    #@2f
    iput v2, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirectionPriority:I

    #@31
    .line 130
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@34
    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Animation;)V
    .registers 3
    .parameter "animation"

    #@0
    .prologue
    const/high16 v0, 0x3f00

    #@2
    .line 140
    invoke-direct {p0, p1, v0, v0}, Landroid/view/animation/GridLayoutAnimationController;-><init>(Landroid/view/animation/Animation;FF)V

    #@5
    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/view/animation/Animation;FF)V
    .registers 4
    .parameter "animation"
    .parameter "columnDelay"
    .parameter "rowDelay"

    #@0
    .prologue
    .line 152
    invoke-direct {p0, p1}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;)V

    #@3
    .line 153
    iput p2, p0, Landroid/view/animation/GridLayoutAnimationController;->mColumnDelay:F

    #@5
    .line 154
    iput p3, p0, Landroid/view/animation/GridLayoutAnimationController;->mRowDelay:F

    #@7
    .line 155
    return-void
.end method

.method private getTransformedColumnIndex(Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;)I
    .registers 6
    .parameter "params"

    #@0
    .prologue
    .line 346
    invoke-virtual {p0}, Landroid/view/animation/GridLayoutAnimationController;->getOrder()I

    #@3
    move-result v2

    #@4
    packed-switch v2, :pswitch_data_38

    #@7
    .line 358
    iget v1, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    #@9
    .line 362
    .local v1, index:I
    :goto_9
    iget v2, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirection:I

    #@b
    and-int/lit8 v0, v2, 0x1

    #@d
    .line 363
    .local v0, direction:I
    const/4 v2, 0x1

    #@e
    if-ne v0, v2, :cond_16

    #@10
    .line 364
    iget v2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    #@12
    add-int/lit8 v2, v2, -0x1

    #@14
    sub-int v1, v2, v1

    #@16
    .line 367
    :cond_16
    return v1

    #@17
    .line 348
    .end local v0           #direction:I
    .end local v1           #index:I
    :pswitch_17
    iget v2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    #@19
    add-int/lit8 v2, v2, -0x1

    #@1b
    iget v3, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    #@1d
    sub-int v1, v2, v3

    #@1f
    .line 349
    .restart local v1       #index:I
    goto :goto_9

    #@20
    .line 351
    .end local v1           #index:I
    :pswitch_20
    iget-object v2, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@22
    if-nez v2, :cond_2b

    #@24
    .line 352
    new-instance v2, Ljava/util/Random;

    #@26
    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    #@29
    iput-object v2, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@2b
    .line 354
    :cond_2b
    iget v2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    #@2d
    int-to-float v2, v2

    #@2e
    iget-object v3, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@30
    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    #@33
    move-result v3

    #@34
    mul-float/2addr v2, v3

    #@35
    float-to-int v1, v2

    #@36
    .line 355
    .restart local v1       #index:I
    goto :goto_9

    #@37
    .line 346
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_17
        :pswitch_20
    .end packed-switch
.end method

.method private getTransformedRowIndex(Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;)I
    .registers 6
    .parameter "params"

    #@0
    .prologue
    .line 372
    invoke-virtual {p0}, Landroid/view/animation/GridLayoutAnimationController;->getOrder()I

    #@3
    move-result v2

    #@4
    packed-switch v2, :pswitch_data_38

    #@7
    .line 384
    iget v1, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    #@9
    .line 388
    .local v1, index:I
    :goto_9
    iget v2, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirection:I

    #@b
    and-int/lit8 v0, v2, 0x2

    #@d
    .line 389
    .local v0, direction:I
    const/4 v2, 0x2

    #@e
    if-ne v0, v2, :cond_16

    #@10
    .line 390
    iget v2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    #@12
    add-int/lit8 v2, v2, -0x1

    #@14
    sub-int v1, v2, v1

    #@16
    .line 393
    :cond_16
    return v1

    #@17
    .line 374
    .end local v0           #direction:I
    .end local v1           #index:I
    :pswitch_17
    iget v2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    #@19
    add-int/lit8 v2, v2, -0x1

    #@1b
    iget v3, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    #@1d
    sub-int v1, v2, v3

    #@1f
    .line 375
    .restart local v1       #index:I
    goto :goto_9

    #@20
    .line 377
    .end local v1           #index:I
    :pswitch_20
    iget-object v2, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@22
    if-nez v2, :cond_2b

    #@24
    .line 378
    new-instance v2, Ljava/util/Random;

    #@26
    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    #@29
    iput-object v2, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@2b
    .line 380
    :cond_2b
    iget v2, p1, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    #@2d
    int-to-float v2, v2

    #@2e
    iget-object v3, p0, Landroid/view/animation/LayoutAnimationController;->mRandomizer:Ljava/util/Random;

    #@30
    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    #@33
    move-result v3

    #@34
    mul-float/2addr v2, v3

    #@35
    float-to-int v1, v2

    #@36
    .line 381
    .restart local v1       #index:I
    goto :goto_9

    #@37
    .line 372
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_17
        :pswitch_20
    .end packed-switch
.end method


# virtual methods
.method public getColumnDelay()F
    .registers 2

    #@0
    .prologue
    .line 169
    iget v0, p0, Landroid/view/animation/GridLayoutAnimationController;->mColumnDelay:F

    #@2
    return v0
.end method

.method protected getDelayForView(Landroid/view/View;)J
    .registers 19
    .parameter "view"

    #@0
    .prologue
    .line 298
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v6

    #@4
    .line 299
    .local v6, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v8, v6, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    #@6
    check-cast v8, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    #@8
    .line 301
    .local v8, params:Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    if-nez v8, :cond_d

    #@a
    .line 302
    const-wide/16 v15, 0x0

    #@c
    .line 341
    :goto_c
    return-wide v15

    #@d
    .line 305
    :cond_d
    move-object/from16 v0, p0

    #@f
    invoke-direct {v0, v8}, Landroid/view/animation/GridLayoutAnimationController;->getTransformedColumnIndex(Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;)I

    #@12
    move-result v1

    #@13
    .line 306
    .local v1, column:I
    move-object/from16 v0, p0

    #@15
    invoke-direct {v0, v8}, Landroid/view/animation/GridLayoutAnimationController;->getTransformedRowIndex(Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;)I

    #@18
    move-result v9

    #@19
    .line 308
    .local v9, row:I
    iget v11, v8, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    #@1b
    .line 309
    .local v11, rowsCount:I
    iget v3, v8, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    #@1d
    .line 311
    .local v3, columnsCount:I
    move-object/from16 v0, p0

    #@1f
    iget-object v15, v0, Landroid/view/animation/LayoutAnimationController;->mAnimation:Landroid/view/animation/Animation;

    #@21
    invoke-virtual {v15}, Landroid/view/animation/Animation;->getDuration()J

    #@24
    move-result-wide v4

    #@25
    .line 312
    .local v4, duration:J
    move-object/from16 v0, p0

    #@27
    iget v15, v0, Landroid/view/animation/GridLayoutAnimationController;->mColumnDelay:F

    #@29
    long-to-float v0, v4

    #@2a
    move/from16 v16, v0

    #@2c
    mul-float v2, v15, v16

    #@2e
    .line 313
    .local v2, columnDelay:F
    move-object/from16 v0, p0

    #@30
    iget v15, v0, Landroid/view/animation/GridLayoutAnimationController;->mRowDelay:F

    #@32
    long-to-float v0, v4

    #@33
    move/from16 v16, v0

    #@35
    mul-float v10, v15, v16

    #@37
    .line 318
    .local v10, rowDelay:F
    move-object/from16 v0, p0

    #@39
    iget-object v15, v0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@3b
    if-nez v15, :cond_46

    #@3d
    .line 319
    new-instance v15, Landroid/view/animation/LinearInterpolator;

    #@3f
    invoke-direct {v15}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@42
    move-object/from16 v0, p0

    #@44
    iput-object v15, v0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@46
    .line 322
    :cond_46
    move-object/from16 v0, p0

    #@48
    iget v15, v0, Landroid/view/animation/GridLayoutAnimationController;->mDirectionPriority:I

    #@4a
    packed-switch v15, :pswitch_data_a8

    #@4d
    .line 333
    int-to-float v15, v1

    #@4e
    mul-float/2addr v15, v2

    #@4f
    int-to-float v0, v9

    #@50
    move/from16 v16, v0

    #@52
    mul-float v16, v16, v10

    #@54
    add-float v15, v15, v16

    #@56
    float-to-long v13, v15

    #@57
    .line 334
    .local v13, viewDelay:J
    int-to-float v15, v3

    #@58
    mul-float/2addr v15, v2

    #@59
    int-to-float v0, v11

    #@5a
    move/from16 v16, v0

    #@5c
    mul-float v16, v16, v10

    #@5e
    add-float v12, v15, v16

    #@60
    .line 338
    .local v12, totalDelay:F
    :goto_60
    long-to-float v15, v13

    #@61
    div-float v7, v15, v12

    #@63
    .line 339
    .local v7, normalizedDelay:F
    move-object/from16 v0, p0

    #@65
    iget-object v15, v0, Landroid/view/animation/LayoutAnimationController;->mInterpolator:Landroid/view/animation/Interpolator;

    #@67
    invoke-interface {v15, v7}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@6a
    move-result v7

    #@6b
    .line 341
    mul-float v15, v7, v12

    #@6d
    float-to-long v15, v15

    #@6e
    goto :goto_c

    #@6f
    .line 324
    .end local v7           #normalizedDelay:F
    .end local v12           #totalDelay:F
    .end local v13           #viewDelay:J
    :pswitch_6f
    int-to-float v15, v9

    #@70
    mul-float/2addr v15, v10

    #@71
    mul-int v16, v1, v11

    #@73
    move/from16 v0, v16

    #@75
    int-to-float v0, v0

    #@76
    move/from16 v16, v0

    #@78
    mul-float v16, v16, v10

    #@7a
    add-float v15, v15, v16

    #@7c
    float-to-long v13, v15

    #@7d
    .line 325
    .restart local v13       #viewDelay:J
    int-to-float v15, v11

    #@7e
    mul-float/2addr v15, v10

    #@7f
    mul-int v16, v3, v11

    #@81
    move/from16 v0, v16

    #@83
    int-to-float v0, v0

    #@84
    move/from16 v16, v0

    #@86
    mul-float v16, v16, v10

    #@88
    add-float v12, v15, v16

    #@8a
    .line 326
    .restart local v12       #totalDelay:F
    goto :goto_60

    #@8b
    .line 328
    .end local v12           #totalDelay:F
    .end local v13           #viewDelay:J
    :pswitch_8b
    int-to-float v15, v1

    #@8c
    mul-float/2addr v15, v2

    #@8d
    mul-int v16, v9, v3

    #@8f
    move/from16 v0, v16

    #@91
    int-to-float v0, v0

    #@92
    move/from16 v16, v0

    #@94
    mul-float v16, v16, v2

    #@96
    add-float v15, v15, v16

    #@98
    float-to-long v13, v15

    #@99
    .line 329
    .restart local v13       #viewDelay:J
    int-to-float v15, v3

    #@9a
    mul-float/2addr v15, v2

    #@9b
    mul-int v16, v11, v3

    #@9d
    move/from16 v0, v16

    #@9f
    int-to-float v0, v0

    #@a0
    move/from16 v16, v0

    #@a2
    mul-float v16, v16, v2

    #@a4
    add-float v12, v15, v16

    #@a6
    .line 330
    .restart local v12       #totalDelay:F
    goto :goto_60

    #@a7
    .line 322
    nop

    #@a8
    :pswitch_data_a8
    .packed-switch 0x1
        :pswitch_6f
        :pswitch_8b
    .end packed-switch
.end method

.method public getDirection()I
    .registers 2

    #@0
    .prologue
    .line 231
    iget v0, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirection:I

    #@2
    return v0
.end method

.method public getDirectionPriority()I
    .registers 2

    #@0
    .prologue
    .line 266
    iget v0, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirectionPriority:I

    #@2
    return v0
.end method

.method public getRowDelay()F
    .registers 2

    #@0
    .prologue
    .line 198
    iget v0, p0, Landroid/view/animation/GridLayoutAnimationController;->mRowDelay:F

    #@2
    return v0
.end method

.method public setColumnDelay(F)V
    .registers 2
    .parameter "columnDelay"

    #@0
    .prologue
    .line 183
    iput p1, p0, Landroid/view/animation/GridLayoutAnimationController;->mColumnDelay:F

    #@2
    .line 184
    return-void
.end method

.method public setDirection(I)V
    .registers 2
    .parameter "direction"

    #@0
    .prologue
    .line 250
    iput p1, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirection:I

    #@2
    .line 251
    return-void
.end method

.method public setDirectionPriority(I)V
    .registers 2
    .parameter "directionPriority"

    #@0
    .prologue
    .line 282
    iput p1, p0, Landroid/view/animation/GridLayoutAnimationController;->mDirectionPriority:I

    #@2
    .line 283
    return-void
.end method

.method public setRowDelay(F)V
    .registers 2
    .parameter "rowDelay"

    #@0
    .prologue
    .line 212
    iput p1, p0, Landroid/view/animation/GridLayoutAnimationController;->mRowDelay:F

    #@2
    .line 213
    return-void
.end method

.method public willOverlap()Z
    .registers 3

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    .line 290
    iget v0, p0, Landroid/view/animation/GridLayoutAnimationController;->mColumnDelay:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-ltz v0, :cond_e

    #@8
    iget v0, p0, Landroid/view/animation/GridLayoutAnimationController;->mRowDelay:F

    #@a
    cmpg-float v0, v0, v1

    #@c
    if-gez v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method
