.class public Landroid/view/animation/AccelerateInterpolator;
.super Ljava/lang/Object;
.source "AccelerateInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final mDoubleFactor:D

.field private final mFactor:F


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Landroid/view/animation/AccelerateInterpolator;->mFactor:F

    #@7
    .line 34
    const-wide/high16 v0, 0x4000

    #@9
    iput-wide v0, p0, Landroid/view/animation/AccelerateInterpolator;->mDoubleFactor:D

    #@b
    .line 35
    return-void
.end method

.method public constructor <init>(F)V
    .registers 4
    .parameter "factor"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    iput p1, p0, Landroid/view/animation/AccelerateInterpolator;->mFactor:F

    #@5
    .line 47
    const/high16 v0, 0x4000

    #@7
    iget v1, p0, Landroid/view/animation/AccelerateInterpolator;->mFactor:F

    #@9
    mul-float/2addr v0, v1

    #@a
    float-to-double v0, v0

    #@b
    iput-wide v0, p0, Landroid/view/animation/AccelerateInterpolator;->mDoubleFactor:D

    #@d
    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    sget-object v1, Lcom/android/internal/R$styleable;->AccelerateInterpolator:[I

    #@5
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v0

    #@9
    .line 54
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@a
    const/high16 v2, 0x3f80

    #@c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@f
    move-result v1

    #@10
    iput v1, p0, Landroid/view/animation/AccelerateInterpolator;->mFactor:F

    #@12
    .line 55
    const/high16 v1, 0x4000

    #@14
    iget v2, p0, Landroid/view/animation/AccelerateInterpolator;->mFactor:F

    #@16
    mul-float/2addr v1, v2

    #@17
    float-to-double v1, v1

    #@18
    iput-wide v1, p0, Landroid/view/animation/AccelerateInterpolator;->mDoubleFactor:D

    #@1a
    .line 57
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1d
    .line 58
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 61
    iget v0, p0, Landroid/view/animation/AccelerateInterpolator;->mFactor:F

    #@2
    const/high16 v1, 0x3f80

    #@4
    cmpl-float v0, v0, v1

    #@6
    if-nez v0, :cond_b

    #@8
    .line 62
    mul-float v0, p1, p1

    #@a
    .line 64
    :goto_a
    return v0

    #@b
    :cond_b
    float-to-double v0, p1

    #@c
    iget-wide v2, p0, Landroid/view/animation/AccelerateInterpolator;->mDoubleFactor:D

    #@e
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    #@11
    move-result-wide v0

    #@12
    double-to-float v0, v0

    #@13
    goto :goto_a
.end method
