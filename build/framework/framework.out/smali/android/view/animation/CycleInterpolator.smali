.class public Landroid/view/animation/CycleInterpolator;
.super Ljava/lang/Object;
.source "CycleInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private mCycles:F


# direct methods
.method public constructor <init>(F)V
    .registers 2
    .parameter "cycles"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    iput p1, p0, Landroid/view/animation/CycleInterpolator;->mCycles:F

    #@5
    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    sget-object v1, Lcom/android/internal/R$styleable;->CycleInterpolator:[I

    #@5
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v0

    #@9
    .line 37
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@a
    const/high16 v2, 0x3f80

    #@c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@f
    move-result v1

    #@10
    iput v1, p0, Landroid/view/animation/CycleInterpolator;->mCycles:F

    #@12
    .line 39
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@15
    .line 40
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    .line 43
    const/high16 v0, 0x4000

    #@2
    iget v1, p0, Landroid/view/animation/CycleInterpolator;->mCycles:F

    #@4
    mul-float/2addr v0, v1

    #@5
    float-to-double v0, v0

    #@6
    const-wide v2, 0x400921fb54442d18L

    #@b
    mul-double/2addr v0, v2

    #@c
    float-to-double v2, p1

    #@d
    mul-double/2addr v0, v2

    #@e
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    #@11
    move-result-wide v0

    #@12
    double-to-float v0, v0

    #@13
    return v0
.end method
