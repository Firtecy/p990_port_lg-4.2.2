.class public abstract Landroid/view/animation/Animation;
.super Ljava/lang/Object;
.source "Animation.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/animation/Animation$AnimationListener;,
        Landroid/view/animation/Animation$Description;
    }
.end annotation


# static fields
.field public static final ABSOLUTE:I = 0x0

.field public static final INFINITE:I = -0x1

.field public static final RELATIVE_TO_PARENT:I = 0x2

.field public static final RELATIVE_TO_SELF:I = 0x1

.field public static final RESTART:I = 0x1

.field public static final REVERSE:I = 0x2

.field public static final START_ON_FIRST_FRAME:I = -0x1

#the value of this static final field might be set in the static constructor
.field private static final USE_CLOSEGUARD:Z = false

.field public static final ZORDER_BOTTOM:I = -0x1

.field public static final ZORDER_NORMAL:I = 0x0

.field public static final ZORDER_TOP:I = 0x1


# instance fields
.field private final guard:Ldalvik/system/CloseGuard;

.field private mBackgroundColor:I

.field mCycleFlip:Z

.field private mDetachWallpaper:Z

.field mDuration:J

.field mEnded:Z

.field mFillAfter:Z

.field mFillBefore:Z

.field mFillEnabled:Z

.field mInitialized:Z

.field mInterpolator:Landroid/view/animation/Interpolator;

.field mListener:Landroid/view/animation/Animation$AnimationListener;

.field private mListenerHandler:Landroid/os/Handler;

.field private mMore:Z

.field private mOnEnd:Ljava/lang/Runnable;

.field private mOnRepeat:Ljava/lang/Runnable;

.field private mOnStart:Ljava/lang/Runnable;

.field private mOneMoreTime:Z

.field mPreviousRegion:Landroid/graphics/RectF;

.field mPreviousTransformation:Landroid/view/animation/Transformation;

.field mRegion:Landroid/graphics/RectF;

.field mRepeatCount:I

.field mRepeatMode:I

.field mRepeated:I

.field private mScaleFactor:F

.field mStartOffset:J

.field mStartTime:J

.field mStarted:Z

.field mTransformation:Landroid/view/animation/Transformation;

.field private mZAdjustment:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 93
    const-string/jumbo v0, "log.closeguard.Animation"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    sput-boolean v0, Landroid/view/animation/Animation;->USE_CLOSEGUARD:Z

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 220
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 99
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@7
    .line 104
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@9
    .line 110
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@b
    .line 116
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mInitialized:Z

    #@d
    .line 123
    iput-boolean v3, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@f
    .line 129
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@11
    .line 134
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@13
    .line 139
    const-wide/16 v0, -0x1

    #@15
    iput-wide v0, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@17
    .line 156
    iput v2, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@19
    .line 161
    iput v2, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@1b
    .line 168
    iput v3, p0, Landroid/view/animation/Animation;->mRepeatMode:I

    #@1d
    .line 194
    const/high16 v0, 0x3f80

    #@1f
    iput v0, p0, Landroid/view/animation/Animation;->mScaleFactor:F

    #@21
    .line 199
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mDetachWallpaper:Z

    #@23
    .line 201
    iput-boolean v3, p0, Landroid/view/animation/Animation;->mMore:Z

    #@25
    .line 202
    iput-boolean v3, p0, Landroid/view/animation/Animation;->mOneMoreTime:Z

    #@27
    .line 204
    new-instance v0, Landroid/graphics/RectF;

    #@29
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@2c
    iput-object v0, p0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@2e
    .line 205
    new-instance v0, Landroid/graphics/RectF;

    #@30
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@33
    iput-object v0, p0, Landroid/view/animation/Animation;->mRegion:Landroid/graphics/RectF;

    #@35
    .line 206
    new-instance v0, Landroid/view/animation/Transformation;

    #@37
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@3a
    iput-object v0, p0, Landroid/view/animation/Animation;->mTransformation:Landroid/view/animation/Transformation;

    #@3c
    .line 207
    new-instance v0, Landroid/view/animation/Transformation;

    #@3e
    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    #@41
    iput-object v0, p0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@43
    .line 209
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@49
    .line 221
    invoke-virtual {p0}, Landroid/view/animation/Animation;->ensureInterpolator()V

    #@4c
    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 231
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 99
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@7
    .line 104
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@9
    .line 110
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@b
    .line 116
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mInitialized:Z

    #@d
    .line 123
    iput-boolean v5, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@f
    .line 129
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@11
    .line 134
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@13
    .line 139
    const-wide/16 v2, -0x1

    #@15
    iput-wide v2, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@17
    .line 156
    iput v4, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@19
    .line 161
    iput v4, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@1b
    .line 168
    iput v5, p0, Landroid/view/animation/Animation;->mRepeatMode:I

    #@1d
    .line 194
    const/high16 v2, 0x3f80

    #@1f
    iput v2, p0, Landroid/view/animation/Animation;->mScaleFactor:F

    #@21
    .line 199
    iput-boolean v4, p0, Landroid/view/animation/Animation;->mDetachWallpaper:Z

    #@23
    .line 201
    iput-boolean v5, p0, Landroid/view/animation/Animation;->mMore:Z

    #@25
    .line 202
    iput-boolean v5, p0, Landroid/view/animation/Animation;->mOneMoreTime:Z

    #@27
    .line 204
    new-instance v2, Landroid/graphics/RectF;

    #@29
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    #@2c
    iput-object v2, p0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@2e
    .line 205
    new-instance v2, Landroid/graphics/RectF;

    #@30
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    #@33
    iput-object v2, p0, Landroid/view/animation/Animation;->mRegion:Landroid/graphics/RectF;

    #@35
    .line 206
    new-instance v2, Landroid/view/animation/Transformation;

    #@37
    invoke-direct {v2}, Landroid/view/animation/Transformation;-><init>()V

    #@3a
    iput-object v2, p0, Landroid/view/animation/Animation;->mTransformation:Landroid/view/animation/Transformation;

    #@3c
    .line 207
    new-instance v2, Landroid/view/animation/Transformation;

    #@3e
    invoke-direct {v2}, Landroid/view/animation/Transformation;-><init>()V

    #@41
    iput-object v2, p0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@43
    .line 209
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@46
    move-result-object v2

    #@47
    iput-object v2, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@49
    .line 232
    sget-object v2, Lcom/android/internal/R$styleable;->Animation:[I

    #@4b
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@4e
    move-result-object v0

    #@4f
    .line 234
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x2

    #@50
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@53
    move-result v2

    #@54
    int-to-long v2, v2

    #@55
    invoke-virtual {p0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    #@58
    .line 235
    const/4 v2, 0x5

    #@59
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@5c
    move-result v2

    #@5d
    int-to-long v2, v2

    #@5e
    invoke-virtual {p0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    #@61
    .line 237
    const/16 v2, 0x9

    #@63
    iget-boolean v3, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@65
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@68
    move-result v2

    #@69
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    #@6c
    .line 238
    const/4 v2, 0x3

    #@6d
    iget-boolean v3, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@6f
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@72
    move-result v2

    #@73
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    #@76
    .line 239
    const/4 v2, 0x4

    #@77
    iget-boolean v3, p0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@79
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@7c
    move-result v2

    #@7d
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    #@80
    .line 241
    const/4 v2, 0x6

    #@81
    iget v3, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@83
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@86
    move-result v2

    #@87
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    #@8a
    .line 242
    const/4 v2, 0x7

    #@8b
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@8e
    move-result v2

    #@8f
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    #@92
    .line 244
    const/16 v2, 0x8

    #@94
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@97
    move-result v2

    #@98
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setZAdjustment(I)V

    #@9b
    .line 246
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@9e
    move-result v2

    #@9f
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setBackgroundColor(I)V

    #@a2
    .line 248
    const/16 v2, 0xa

    #@a4
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a7
    move-result v2

    #@a8
    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setDetachWallpaper(Z)V

    #@ab
    .line 250
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@ae
    move-result v1

    #@af
    .line 252
    .local v1, resID:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@b2
    .line 254
    if-lez v1, :cond_b7

    #@b4
    .line 255
    invoke-virtual {p0, p1, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    #@b7
    .line 258
    :cond_b7
    invoke-virtual {p0}, Landroid/view/animation/Animation;->ensureInterpolator()V

    #@ba
    .line 259
    return-void
.end method

.method private fireAnimationEnd()V
    .registers 3

    #@0
    .prologue
    .line 919
    iget-object v0, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 920
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@6
    if-nez v0, :cond_e

    #@8
    iget-object v0, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@a
    invoke-interface {v0, p0}, Landroid/view/animation/Animation$AnimationListener;->onAnimationEnd(Landroid/view/animation/Animation;)V

    #@d
    .line 923
    :cond_d
    :goto_d
    return-void

    #@e
    .line 921
    :cond_e
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@10
    iget-object v1, p0, Landroid/view/animation/Animation;->mOnEnd:Ljava/lang/Runnable;

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    #@15
    goto :goto_d
.end method

.method private fireAnimationRepeat()V
    .registers 3

    #@0
    .prologue
    .line 912
    iget-object v0, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 913
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@6
    if-nez v0, :cond_e

    #@8
    iget-object v0, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@a
    invoke-interface {v0, p0}, Landroid/view/animation/Animation$AnimationListener;->onAnimationRepeat(Landroid/view/animation/Animation;)V

    #@d
    .line 916
    :cond_d
    :goto_d
    return-void

    #@e
    .line 914
    :cond_e
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@10
    iget-object v1, p0, Landroid/view/animation/Animation;->mOnRepeat:Ljava/lang/Runnable;

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    #@15
    goto :goto_d
.end method

.method private fireAnimationStart()V
    .registers 3

    #@0
    .prologue
    .line 905
    iget-object v0, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 906
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@6
    if-nez v0, :cond_e

    #@8
    iget-object v0, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@a
    invoke-interface {v0, p0}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V

    #@d
    .line 909
    :cond_d
    :goto_d
    return-void

    #@e
    .line 907
    :cond_e
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@10
    iget-object v1, p0, Landroid/view/animation/Animation;->mOnStart:Ljava/lang/Runnable;

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    #@15
    goto :goto_d
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 3
    .parameter "interpolatedTime"
    .parameter "t"

    #@0
    .prologue
    .line 973
    return-void
.end method

.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 299
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@2
    if-eqz v0, :cond_13

    #@4
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@6
    if-nez v0, :cond_13

    #@8
    .line 300
    invoke-direct {p0}, Landroid/view/animation/Animation;->fireAnimationEnd()V

    #@b
    .line 301
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@e
    .line 302
    iget-object v0, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@10
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@13
    .line 305
    :cond_13
    const-wide/high16 v0, -0x8000

    #@15
    iput-wide v0, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@17
    .line 306
    const/4 v0, 0x0

    #@18
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mOneMoreTime:Z

    #@1a
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mMore:Z

    #@1c
    .line 307
    return-void
.end method

.method protected clone()Landroid/view/animation/Animation;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 263
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/view/animation/Animation;

    #@6
    .line 264
    .local v0, animation:Landroid/view/animation/Animation;
    new-instance v1, Landroid/graphics/RectF;

    #@8
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    #@b
    iput-object v1, v0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@d
    .line 265
    new-instance v1, Landroid/graphics/RectF;

    #@f
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    #@12
    iput-object v1, v0, Landroid/view/animation/Animation;->mRegion:Landroid/graphics/RectF;

    #@14
    .line 266
    new-instance v1, Landroid/view/animation/Transformation;

    #@16
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@19
    iput-object v1, v0, Landroid/view/animation/Animation;->mTransformation:Landroid/view/animation/Transformation;

    #@1b
    .line 267
    new-instance v1, Landroid/view/animation/Transformation;

    #@1d
    invoke-direct {v1}, Landroid/view/animation/Transformation;-><init>()V

    #@20
    iput-object v1, v0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@22
    .line 268
    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/view/animation/Animation;->clone()Landroid/view/animation/Animation;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public computeDurationHint()J
    .registers 5

    #@0
    .prologue
    .line 820
    invoke-virtual {p0}, Landroid/view/animation/Animation;->getStartOffset()J

    #@3
    move-result-wide v0

    #@4
    invoke-virtual {p0}, Landroid/view/animation/Animation;->getDuration()J

    #@7
    move-result-wide v2

    #@8
    add-long/2addr v0, v2

    #@9
    invoke-virtual {p0}, Landroid/view/animation/Animation;->getRepeatCount()I

    #@c
    move-result v2

    #@d
    add-int/lit8 v2, v2, 0x1

    #@f
    int-to-long v2, v2

    #@10
    mul-long/2addr v0, v2

    #@11
    return-wide v0
.end method

.method public detach()V
    .registers 2

    #@0
    .prologue
    .line 313
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@2
    if-eqz v0, :cond_13

    #@4
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@6
    if-nez v0, :cond_13

    #@8
    .line 314
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@b
    .line 315
    iget-object v0, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@d
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@10
    .line 316
    invoke-direct {p0}, Landroid/view/animation/Animation;->fireAnimationEnd()V

    #@13
    .line 318
    :cond_13
    return-void
.end method

.method protected ensureInterpolator()V
    .registers 2

    #@0
    .prologue
    .line 808
    iget-object v0, p0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 809
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    #@6
    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    #@9
    iput-object v0, p0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@b
    .line 811
    :cond_b
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 1053
    :try_start_0
    iget-object v0, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1054
    iget-object v0, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@6
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V
    :try_end_9
    .catchall {:try_start_0 .. :try_end_9} :catchall_d

    #@9
    .line 1057
    :cond_9
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@c
    .line 1059
    return-void

    #@d
    .line 1057
    :catchall_d
    move-exception v0

    #@e
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@11
    throw v0
.end method

.method public getBackgroundColor()I
    .registers 2

    #@0
    .prologue
    .line 757
    iget v0, p0, Landroid/view/animation/Animation;->mBackgroundColor:I

    #@2
    return v0
.end method

.method public getDetachWallpaper()Z
    .registers 2

    #@0
    .prologue
    .line 765
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mDetachWallpaper:Z

    #@2
    return v0
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 683
    iget-wide v0, p0, Landroid/view/animation/Animation;->mDuration:J

    #@2
    return-wide v0
.end method

.method public getFillAfter()Z
    .registers 2

    #@0
    .prologue
    .line 738
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@2
    return v0
.end method

.method public getFillBefore()Z
    .registers 2

    #@0
    .prologue
    .line 727
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@2
    return v0
.end method

.method public getInterpolator()Landroid/view/animation/Interpolator;
    .registers 2

    #@0
    .prologue
    .line 662
    iget-object v0, p0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    return-object v0
.end method

.method public getInvalidateRegion(IIIILandroid/graphics/RectF;Landroid/view/animation/Transformation;)V
    .registers 16
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "invalidate"
    .parameter "transformation"

    #@0
    .prologue
    const/high16 v8, -0x4080

    #@2
    .line 1012
    iget-object v2, p0, Landroid/view/animation/Animation;->mRegion:Landroid/graphics/RectF;

    #@4
    .line 1013
    .local v2, tempRegion:Landroid/graphics/RectF;
    iget-object v0, p0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@6
    .line 1015
    .local v0, previousRegion:Landroid/graphics/RectF;
    int-to-float v4, p1

    #@7
    int-to-float v5, p2

    #@8
    int-to-float v6, p3

    #@9
    int-to-float v7, p4

    #@a
    invoke-virtual {p5, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    #@d
    .line 1016
    invoke-virtual {p6}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4, p5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@14
    .line 1018
    invoke-virtual {p5, v8, v8}, Landroid/graphics/RectF;->inset(FF)V

    #@17
    .line 1019
    invoke-virtual {v2, p5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    #@1a
    .line 1020
    invoke-virtual {p5, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    #@1d
    .line 1022
    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    #@20
    .line 1024
    iget-object v3, p0, Landroid/view/animation/Animation;->mTransformation:Landroid/view/animation/Transformation;

    #@22
    .line 1025
    .local v3, tempTransformation:Landroid/view/animation/Transformation;
    iget-object v1, p0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@24
    .line 1027
    .local v1, previousTransformation:Landroid/view/animation/Transformation;
    invoke-virtual {v3, p6}, Landroid/view/animation/Transformation;->set(Landroid/view/animation/Transformation;)V

    #@27
    .line 1028
    invoke-virtual {p6, v1}, Landroid/view/animation/Transformation;->set(Landroid/view/animation/Transformation;)V

    #@2a
    .line 1029
    invoke-virtual {v1, v3}, Landroid/view/animation/Transformation;->set(Landroid/view/animation/Transformation;)V

    #@2d
    .line 1030
    return-void
.end method

.method public getRepeatCount()I
    .registers 2

    #@0
    .prologue
    .line 714
    iget v0, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@2
    return v0
.end method

.method public getRepeatMode()I
    .registers 2

    #@0
    .prologue
    .line 703
    iget v0, p0, Landroid/view/animation/Animation;->mRepeatMode:I

    #@2
    return v0
.end method

.method protected getScaleFactor()F
    .registers 2

    #@0
    .prologue
    .line 639
    iget v0, p0, Landroid/view/animation/Animation;->mScaleFactor:F

    #@2
    return v0
.end method

.method public getStartOffset()J
    .registers 3

    #@0
    .prologue
    .line 693
    iget-wide v0, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@2
    return-wide v0
.end method

.method public getStartTime()J
    .registers 3

    #@0
    .prologue
    .line 673
    iget-wide v0, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@2
    return-wide v0
.end method

.method public getTransformation(JLandroid/view/animation/Transformation;)Z
    .registers 15
    .parameter "currentTime"
    .parameter "outTransformation"

    #@0
    .prologue
    .line 834
    iget-wide v7, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@2
    const-wide/16 v9, -0x1

    #@4
    cmp-long v7, v7, v9

    #@6
    if-nez v7, :cond_a

    #@8
    .line 835
    iput-wide p1, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@a
    .line 838
    :cond_a
    invoke-virtual {p0}, Landroid/view/animation/Animation;->getStartOffset()J

    #@d
    move-result-wide v5

    #@e
    .line 839
    .local v5, startOffset:J
    iget-wide v0, p0, Landroid/view/animation/Animation;->mDuration:J

    #@10
    .line 841
    .local v0, duration:J
    const-wide/16 v7, 0x0

    #@12
    cmp-long v7, v0, v7

    #@14
    if-eqz v7, :cond_a6

    #@16
    .line 842
    iget-wide v7, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@18
    add-long/2addr v7, v5

    #@19
    sub-long v7, p1, v7

    #@1b
    long-to-float v7, v7

    #@1c
    long-to-float v8, v0

    #@1d
    div-float v4, v7, v8

    #@1f
    .line 849
    .local v4, normalizedTime:F
    :goto_1f
    const/high16 v7, 0x3f80

    #@21
    cmpl-float v7, v4, v7

    #@23
    if-ltz v7, :cond_b2

    #@25
    const/4 v2, 0x1

    #@26
    .line 850
    .local v2, expired:Z
    :goto_26
    if-nez v2, :cond_b5

    #@28
    const/4 v7, 0x1

    #@29
    :goto_29
    iput-boolean v7, p0, Landroid/view/animation/Animation;->mMore:Z

    #@2b
    .line 852
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@2d
    if-nez v7, :cond_3a

    #@2f
    const/high16 v7, 0x3f80

    #@31
    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    #@34
    move-result v7

    #@35
    const/4 v8, 0x0

    #@36
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@39
    move-result v4

    #@3a
    .line 854
    :cond_3a
    const/4 v7, 0x0

    #@3b
    cmpl-float v7, v4, v7

    #@3d
    if-gez v7, :cond_43

    #@3f
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@41
    if-eqz v7, :cond_82

    #@43
    :cond_43
    const/high16 v7, 0x3f80

    #@45
    cmpg-float v7, v4, v7

    #@47
    if-lez v7, :cond_4d

    #@49
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@4b
    if-eqz v7, :cond_82

    #@4d
    .line 855
    :cond_4d
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@4f
    if-nez v7, :cond_62

    #@51
    .line 856
    invoke-direct {p0}, Landroid/view/animation/Animation;->fireAnimationStart()V

    #@54
    .line 857
    const/4 v7, 0x1

    #@55
    iput-boolean v7, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@57
    .line 858
    sget-boolean v7, Landroid/view/animation/Animation;->USE_CLOSEGUARD:Z

    #@59
    if-eqz v7, :cond_62

    #@5b
    .line 859
    iget-object v7, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@5d
    const-string v8, "cancel or detach or getTransformation"

    #@5f
    invoke-virtual {v7, v8}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@62
    .line 863
    :cond_62
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@64
    if-eqz v7, :cond_71

    #@66
    const/high16 v7, 0x3f80

    #@68
    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    #@6b
    move-result v7

    #@6c
    const/4 v8, 0x0

    #@6d
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@70
    move-result v4

    #@71
    .line 865
    :cond_71
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@73
    if-eqz v7, :cond_79

    #@75
    .line 866
    const/high16 v7, 0x3f80

    #@77
    sub-float v4, v7, v4

    #@79
    .line 869
    :cond_79
    iget-object v7, p0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@7b
    invoke-interface {v7, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@7e
    move-result v3

    #@7f
    .line 870
    .local v3, interpolatedTime:F
    invoke-virtual {p0, v3, p3}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    #@82
    .line 873
    .end local v3           #interpolatedTime:F
    :cond_82
    if-eqz v2, :cond_99

    #@84
    .line 874
    iget v7, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@86
    iget v8, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@88
    if-ne v7, v8, :cond_b8

    #@8a
    .line 875
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@8c
    if-nez v7, :cond_99

    #@8e
    .line 876
    const/4 v7, 0x1

    #@8f
    iput-boolean v7, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@91
    .line 877
    iget-object v7, p0, Landroid/view/animation/Animation;->guard:Ldalvik/system/CloseGuard;

    #@93
    invoke-virtual {v7}, Ldalvik/system/CloseGuard;->close()V

    #@96
    .line 878
    invoke-direct {p0}, Landroid/view/animation/Animation;->fireAnimationEnd()V

    #@99
    .line 896
    :cond_99
    :goto_99
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mMore:Z

    #@9b
    if-nez v7, :cond_db

    #@9d
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mOneMoreTime:Z

    #@9f
    if-eqz v7, :cond_db

    #@a1
    .line 897
    const/4 v7, 0x0

    #@a2
    iput-boolean v7, p0, Landroid/view/animation/Animation;->mOneMoreTime:Z

    #@a4
    .line 898
    const/4 v7, 0x1

    #@a5
    .line 901
    :goto_a5
    return v7

    #@a6
    .line 846
    .end local v2           #expired:Z
    .end local v4           #normalizedTime:F
    :cond_a6
    iget-wide v7, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@a8
    cmp-long v7, p1, v7

    #@aa
    if-gez v7, :cond_af

    #@ac
    const/4 v4, 0x0

    #@ad
    .restart local v4       #normalizedTime:F
    :goto_ad
    goto/16 :goto_1f

    #@af
    .end local v4           #normalizedTime:F
    :cond_af
    const/high16 v4, 0x3f80

    #@b1
    goto :goto_ad

    #@b2
    .line 849
    .restart local v4       #normalizedTime:F
    :cond_b2
    const/4 v2, 0x0

    #@b3
    goto/16 :goto_26

    #@b5
    .line 850
    .restart local v2       #expired:Z
    :cond_b5
    const/4 v7, 0x0

    #@b6
    goto/16 :goto_29

    #@b8
    .line 881
    :cond_b8
    iget v7, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@ba
    if-lez v7, :cond_c2

    #@bc
    .line 882
    iget v7, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@be
    add-int/lit8 v7, v7, 0x1

    #@c0
    iput v7, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@c2
    .line 885
    :cond_c2
    iget v7, p0, Landroid/view/animation/Animation;->mRepeatMode:I

    #@c4
    const/4 v8, 0x2

    #@c5
    if-ne v7, v8, :cond_ce

    #@c7
    .line 886
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@c9
    if-nez v7, :cond_d9

    #@cb
    const/4 v7, 0x1

    #@cc
    :goto_cc
    iput-boolean v7, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@ce
    .line 889
    :cond_ce
    const-wide/16 v7, -0x1

    #@d0
    iput-wide v7, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@d2
    .line 890
    const/4 v7, 0x1

    #@d3
    iput-boolean v7, p0, Landroid/view/animation/Animation;->mMore:Z

    #@d5
    .line 892
    invoke-direct {p0}, Landroid/view/animation/Animation;->fireAnimationRepeat()V

    #@d8
    goto :goto_99

    #@d9
    .line 886
    :cond_d9
    const/4 v7, 0x0

    #@da
    goto :goto_cc

    #@db
    .line 901
    :cond_db
    iget-boolean v7, p0, Landroid/view/animation/Animation;->mMore:Z

    #@dd
    goto :goto_a5
.end method

.method public getTransformation(JLandroid/view/animation/Transformation;F)Z
    .registers 6
    .parameter "currentTime"
    .parameter "outTransformation"
    .parameter "scale"

    #@0
    .prologue
    .line 939
    iput p4, p0, Landroid/view/animation/Animation;->mScaleFactor:F

    #@2
    .line 940
    invoke-virtual {p0, p1, p2, p3}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getZAdjustment()I
    .registers 2

    #@0
    .prologue
    .line 750
    iget v0, p0, Landroid/view/animation/Animation;->mZAdjustment:I

    #@2
    return v0
.end method

.method public hasAlpha()Z
    .registers 2

    #@0
    .prologue
    .line 1067
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public hasEnded()Z
    .registers 2

    #@0
    .prologue
    .line 958
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@2
    return v0
.end method

.method public hasStarted()Z
    .registers 2

    #@0
    .prologue
    .line 949
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@2
    return v0
.end method

.method public initialize(IIII)V
    .registers 6
    .parameter "width"
    .parameter "height"
    .parameter "parentWidth"
    .parameter "parentHeight"

    #@0
    .prologue
    .line 346
    invoke-virtual {p0}, Landroid/view/animation/Animation;->reset()V

    #@3
    .line 347
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mInitialized:Z

    #@6
    .line 348
    return-void
.end method

.method public initializeInvalidateRegion(IIII)V
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/high16 v6, -0x4080

    #@2
    .line 1041
    iget-object v1, p0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@4
    .line 1042
    .local v1, region:Landroid/graphics/RectF;
    int-to-float v2, p1

    #@5
    int-to-float v3, p2

    #@6
    int-to-float v4, p3

    #@7
    int-to-float v5, p4

    #@8
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    #@b
    .line 1044
    invoke-virtual {v1, v6, v6}, Landroid/graphics/RectF;->inset(FF)V

    #@e
    .line 1045
    iget-boolean v2, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@10
    if-eqz v2, :cond_1e

    #@12
    .line 1046
    iget-object v0, p0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@14
    .line 1047
    .local v0, previousTransformation:Landroid/view/animation/Transformation;
    iget-object v2, p0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@16
    const/4 v3, 0x0

    #@17
    invoke-interface {v2, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    #@1a
    move-result v2

    #@1b
    invoke-virtual {p0, v2, v0}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    #@1e
    .line 1049
    .end local v0           #previousTransformation:Landroid/view/animation/Transformation;
    :cond_1e
    return-void
.end method

.method public isFillEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 556
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@2
    return v0
.end method

.method public isInitialized()Z
    .registers 2

    #@0
    .prologue
    .line 327
    iget-boolean v0, p0, Landroid/view/animation/Animation;->mInitialized:Z

    #@2
    return v0
.end method

.method public reset()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 277
    iget-object v0, p0, Landroid/view/animation/Animation;->mPreviousRegion:Landroid/graphics/RectF;

    #@4
    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    #@7
    .line 278
    iget-object v0, p0, Landroid/view/animation/Animation;->mPreviousTransformation:Landroid/view/animation/Transformation;

    #@9
    invoke-virtual {v0}, Landroid/view/animation/Transformation;->clear()V

    #@c
    .line 279
    iput-boolean v1, p0, Landroid/view/animation/Animation;->mInitialized:Z

    #@e
    .line 280
    iput-boolean v1, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@10
    .line 281
    iput v1, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@12
    .line 282
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mMore:Z

    #@14
    .line 283
    iput-boolean v2, p0, Landroid/view/animation/Animation;->mOneMoreTime:Z

    #@16
    .line 284
    const/4 v0, 0x0

    #@17
    iput-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@19
    .line 285
    return-void
.end method

.method protected resolveSize(IFII)F
    .registers 6
    .parameter "type"
    .parameter "value"
    .parameter "size"
    .parameter "parentSize"

    #@0
    .prologue
    .line 987
    packed-switch p1, :pswitch_data_a

    #@3
    .line 995
    .end local p2
    :goto_3
    :pswitch_3
    return p2

    #@4
    .line 991
    .restart local p2
    :pswitch_4
    int-to-float v0, p3

    #@5
    mul-float/2addr p2, v0

    #@6
    goto :goto_3

    #@7
    .line 993
    :pswitch_7
    int-to-float v0, p4

    #@8
    mul-float/2addr p2, v0

    #@9
    goto :goto_3

    #@a
    .line 987
    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method

.method public restrictDuration(J)V
    .registers 12
    .parameter "durationMillis"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 445
    iget-wide v2, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@5
    cmp-long v2, v2, p1

    #@7
    if-lez v2, :cond_10

    #@9
    .line 446
    iput-wide p1, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@b
    .line 447
    iput-wide v7, p0, Landroid/view/animation/Animation;->mDuration:J

    #@d
    .line 448
    iput v6, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@f
    .line 475
    :cond_f
    :goto_f
    return-void

    #@10
    .line 452
    :cond_10
    iget-wide v2, p0, Landroid/view/animation/Animation;->mDuration:J

    #@12
    iget-wide v4, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@14
    add-long v0, v2, v4

    #@16
    .line 453
    .local v0, dur:J
    cmp-long v2, v0, p1

    #@18
    if-lez v2, :cond_21

    #@1a
    .line 454
    iget-wide v2, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@1c
    sub-long v2, p1, v2

    #@1e
    iput-wide v2, p0, Landroid/view/animation/Animation;->mDuration:J

    #@20
    .line 455
    move-wide v0, p1

    #@21
    .line 458
    :cond_21
    iget-wide v2, p0, Landroid/view/animation/Animation;->mDuration:J

    #@23
    cmp-long v2, v2, v7

    #@25
    if-gtz v2, :cond_2c

    #@27
    .line 459
    iput-wide v7, p0, Landroid/view/animation/Animation;->mDuration:J

    #@29
    .line 460
    iput v6, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@2b
    goto :goto_f

    #@2c
    .line 466
    :cond_2c
    iget v2, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@2e
    if-ltz v2, :cond_3f

    #@30
    iget v2, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@32
    int-to-long v2, v2

    #@33
    cmp-long v2, v2, p1

    #@35
    if-gtz v2, :cond_3f

    #@37
    iget v2, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@39
    int-to-long v2, v2

    #@3a
    mul-long/2addr v2, v0

    #@3b
    cmp-long v2, v2, p1

    #@3d
    if-lez v2, :cond_f

    #@3f
    .line 470
    :cond_3f
    div-long v2, p1, v0

    #@41
    long-to-int v2, v2

    #@42
    add-int/lit8 v2, v2, -0x1

    #@44
    iput v2, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@46
    .line 471
    iget v2, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@48
    if-gez v2, :cond_f

    #@4a
    .line 472
    iput v6, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@4c
    goto :goto_f
.end method

.method public scaleCurrentDuration(F)V
    .registers 4
    .parameter "scale"

    #@0
    .prologue
    .line 483
    iget-wide v0, p0, Landroid/view/animation/Animation;->mDuration:J

    #@2
    long-to-float v0, v0

    #@3
    mul-float/2addr v0, p1

    #@4
    float-to-long v0, v0

    #@5
    iput-wide v0, p0, Landroid/view/animation/Animation;->mDuration:J

    #@7
    .line 484
    iget-wide v0, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@9
    long-to-float v0, v0

    #@a
    mul-float/2addr v0, p1

    #@b
    float-to-long v0, v0

    #@c
    iput-wide v0, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@e
    .line 485
    return-void
.end method

.method public setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 800
    iput-object p1, p0, Landroid/view/animation/Animation;->mListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    .line 801
    return-void
.end method

.method public setBackgroundColor(I)V
    .registers 2
    .parameter "bg"

    #@0
    .prologue
    .line 626
    iput p1, p0, Landroid/view/animation/Animation;->mBackgroundColor:I

    #@2
    .line 627
    return-void
.end method

.method public setDetachWallpaper(Z)V
    .registers 2
    .parameter "detachWallpaper"

    #@0
    .prologue
    .line 652
    iput-boolean p1, p0, Landroid/view/animation/Animation;->mDetachWallpaper:Z

    #@2
    .line 653
    return-void
.end method

.method public setDuration(J)V
    .registers 5
    .parameter "durationMillis"

    #@0
    .prologue
    .line 428
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p1, v0

    #@4
    if-gez v0, :cond_e

    #@6
    .line 429
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "Animation duration cannot be negative"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 431
    :cond_e
    iput-wide p1, p0, Landroid/view/animation/Animation;->mDuration:J

    #@10
    .line 432
    return-void
.end method

.method public setFillAfter(Z)V
    .registers 2
    .parameter "fillAfter"

    #@0
    .prologue
    .line 605
    iput-boolean p1, p0, Landroid/view/animation/Animation;->mFillAfter:Z

    #@2
    .line 606
    return-void
.end method

.method public setFillBefore(Z)V
    .registers 2
    .parameter "fillBefore"

    #@0
    .prologue
    .line 589
    iput-boolean p1, p0, Landroid/view/animation/Animation;->mFillBefore:Z

    #@2
    .line 590
    return-void
.end method

.method public setFillEnabled(Z)V
    .registers 2
    .parameter "fillEnabled"

    #@0
    .prologue
    .line 571
    iput-boolean p1, p0, Landroid/view/animation/Animation;->mFillEnabled:Z

    #@2
    .line 572
    return-void
.end method

.method public setInterpolator(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "resID"

    #@0
    .prologue
    .line 391
    invoke-static {p1, p2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@7
    .line 392
    return-void
.end method

.method public setInterpolator(Landroid/view/animation/Interpolator;)V
    .registers 2
    .parameter "i"

    #@0
    .prologue
    .line 402
    iput-object p1, p0, Landroid/view/animation/Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    #@2
    .line 403
    return-void
.end method

.method public setListenerHandler(Landroid/os/Handler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_19

    #@4
    .line 357
    new-instance v0, Landroid/view/animation/Animation$1;

    #@6
    invoke-direct {v0, p0}, Landroid/view/animation/Animation$1;-><init>(Landroid/view/animation/Animation;)V

    #@9
    iput-object v0, p0, Landroid/view/animation/Animation;->mOnStart:Ljava/lang/Runnable;

    #@b
    .line 364
    new-instance v0, Landroid/view/animation/Animation$2;

    #@d
    invoke-direct {v0, p0}, Landroid/view/animation/Animation$2;-><init>(Landroid/view/animation/Animation;)V

    #@10
    iput-object v0, p0, Landroid/view/animation/Animation;->mOnRepeat:Ljava/lang/Runnable;

    #@12
    .line 371
    new-instance v0, Landroid/view/animation/Animation$3;

    #@14
    invoke-direct {v0, p0}, Landroid/view/animation/Animation$3;-><init>(Landroid/view/animation/Animation;)V

    #@17
    iput-object v0, p0, Landroid/view/animation/Animation;->mOnEnd:Ljava/lang/Runnable;

    #@19
    .line 379
    :cond_19
    iput-object p1, p0, Landroid/view/animation/Animation;->mListenerHandler:Landroid/os/Handler;

    #@1b
    .line 380
    return-void
.end method

.method public setRepeatCount(I)V
    .registers 2
    .parameter "repeatCount"

    #@0
    .prologue
    .line 543
    if-gez p1, :cond_3

    #@2
    .line 544
    const/4 p1, -0x1

    #@3
    .line 546
    :cond_3
    iput p1, p0, Landroid/view/animation/Animation;->mRepeatCount:I

    #@5
    .line 547
    return-void
.end method

.method public setRepeatMode(I)V
    .registers 2
    .parameter "repeatMode"

    #@0
    .prologue
    .line 530
    iput p1, p0, Landroid/view/animation/Animation;->mRepeatMode:I

    #@2
    .line 531
    return-void
.end method

.method public setStartOffset(J)V
    .registers 3
    .parameter "startOffset"

    #@0
    .prologue
    .line 415
    iput-wide p1, p0, Landroid/view/animation/Animation;->mStartOffset:J

    #@2
    .line 416
    return-void
.end method

.method public setStartTime(J)V
    .registers 4
    .parameter "startTimeMillis"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 498
    iput-wide p1, p0, Landroid/view/animation/Animation;->mStartTime:J

    #@3
    .line 499
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mEnded:Z

    #@5
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mStarted:Z

    #@7
    .line 500
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mCycleFlip:Z

    #@9
    .line 501
    iput v0, p0, Landroid/view/animation/Animation;->mRepeated:I

    #@b
    .line 502
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/view/animation/Animation;->mMore:Z

    #@e
    .line 503
    return-void
.end method

.method public setZAdjustment(I)V
    .registers 2
    .parameter "zAdjustment"

    #@0
    .prologue
    .line 616
    iput p1, p0, Landroid/view/animation/Animation;->mZAdjustment:I

    #@2
    .line 617
    return-void
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    .line 510
    const-wide/16 v0, -0x1

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@5
    .line 511
    return-void
.end method

.method public startNow()V
    .registers 3

    #@0
    .prologue
    .line 518
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v0

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/view/animation/Animation;->setStartTime(J)V

    #@7
    .line 519
    return-void
.end method

.method public willChangeBounds()Z
    .registers 2

    #@0
    .prologue
    .line 789
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public willChangeTransformationMatrix()Z
    .registers 2

    #@0
    .prologue
    .line 777
    const/4 v0, 0x1

    #@1
    return v0
.end method
