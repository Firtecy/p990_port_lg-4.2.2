.class public Landroid/view/animation/Animation$Description;
.super Ljava/lang/Object;
.source "Animation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/animation/Animation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Description"
.end annotation


# instance fields
.field public type:I

.field public value:F


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1073
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static parseValue(Landroid/util/TypedValue;)Landroid/view/animation/Animation$Description;
    .registers 7
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 1098
    new-instance v0, Landroid/view/animation/Animation$Description;

    #@5
    invoke-direct {v0}, Landroid/view/animation/Animation$Description;-><init>()V

    #@8
    .line 1099
    .local v0, d:Landroid/view/animation/Animation$Description;
    if-nez p0, :cond_13

    #@a
    .line 1100
    iput v4, v0, Landroid/view/animation/Animation$Description;->type:I

    #@c
    .line 1101
    iput v5, v0, Landroid/view/animation/Animation$Description;->value:F

    #@e
    .line 1121
    :cond_e
    iput v4, v0, Landroid/view/animation/Animation$Description;->type:I

    #@10
    .line 1122
    iput v5, v0, Landroid/view/animation/Animation$Description;->value:F

    #@12
    .line 1124
    :goto_12
    return-object v0

    #@13
    .line 1103
    :cond_13
    iget v2, p0, Landroid/util/TypedValue;->type:I

    #@15
    const/4 v3, 0x6

    #@16
    if-ne v2, v3, :cond_2a

    #@18
    .line 1104
    iget v2, p0, Landroid/util/TypedValue;->data:I

    #@1a
    and-int/lit8 v2, v2, 0xf

    #@1c
    if-ne v2, v1, :cond_1f

    #@1e
    const/4 v1, 0x2

    #@1f
    :cond_1f
    iput v1, v0, Landroid/view/animation/Animation$Description;->type:I

    #@21
    .line 1107
    iget v1, p0, Landroid/util/TypedValue;->data:I

    #@23
    invoke-static {v1}, Landroid/util/TypedValue;->complexToFloat(I)F

    #@26
    move-result v1

    #@27
    iput v1, v0, Landroid/view/animation/Animation$Description;->value:F

    #@29
    goto :goto_12

    #@2a
    .line 1109
    :cond_2a
    iget v1, p0, Landroid/util/TypedValue;->type:I

    #@2c
    const/4 v2, 0x4

    #@2d
    if-ne v1, v2, :cond_38

    #@2f
    .line 1110
    iput v4, v0, Landroid/view/animation/Animation$Description;->type:I

    #@31
    .line 1111
    invoke-virtual {p0}, Landroid/util/TypedValue;->getFloat()F

    #@34
    move-result v1

    #@35
    iput v1, v0, Landroid/view/animation/Animation$Description;->value:F

    #@37
    goto :goto_12

    #@38
    .line 1113
    :cond_38
    iget v1, p0, Landroid/util/TypedValue;->type:I

    #@3a
    const/16 v2, 0x10

    #@3c
    if-lt v1, v2, :cond_e

    #@3e
    iget v1, p0, Landroid/util/TypedValue;->type:I

    #@40
    const/16 v2, 0x1f

    #@42
    if-gt v1, v2, :cond_e

    #@44
    .line 1115
    iput v4, v0, Landroid/view/animation/Animation$Description;->type:I

    #@46
    .line 1116
    iget v1, p0, Landroid/util/TypedValue;->data:I

    #@48
    int-to-float v1, v1

    #@49
    iput v1, v0, Landroid/view/animation/Animation$Description;->value:F

    #@4b
    goto :goto_12
.end method
