.class Landroid/view/ViewRootImpl$3;
.super Ljava/lang/Object;
.source "ViewRootImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/ViewRootImpl;->profileRendering(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2085
    iput-object p1, p0, Landroid/view/ViewRootImpl$3;->this$0:Landroid/view/ViewRootImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 2088
    const-string v1, "ViewRootImpl"

    #@2
    const-string v2, "Starting profiling thread"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2089
    :goto_7
    iget-object v1, p0, Landroid/view/ViewRootImpl$3;->this$0:Landroid/view/ViewRootImpl;

    #@9
    invoke-static {v1}, Landroid/view/ViewRootImpl;->access$000(Landroid/view/ViewRootImpl;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2c

    #@f
    .line 2090
    iget-object v1, p0, Landroid/view/ViewRootImpl$3;->this$0:Landroid/view/ViewRootImpl;

    #@11
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    #@13
    iget-object v1, v1, Landroid/view/View$AttachInfo;->mHandler:Landroid/os/Handler;

    #@15
    new-instance v2, Landroid/view/ViewRootImpl$3$1;

    #@17
    invoke-direct {v2, p0}, Landroid/view/ViewRootImpl$3$1;-><init>(Landroid/view/ViewRootImpl$3;)V

    #@1a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@1d
    .line 2099
    const-wide/16 v1, 0xf

    #@1f
    :try_start_1f
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_22
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_22} :catch_23

    #@22
    goto :goto_7

    #@23
    .line 2100
    :catch_23
    move-exception v0

    #@24
    .line 2101
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "ViewRootImpl"

    #@26
    const-string v2, "Exiting profiling thread"

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_7

    #@2c
    .line 2104
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_2c
    return-void
.end method
