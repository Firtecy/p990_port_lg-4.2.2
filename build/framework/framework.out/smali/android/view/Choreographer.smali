.class public final Landroid/view/Choreographer;
.super Ljava/lang/Object;
.source "Choreographer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/Choreographer$CallbackQueue;,
        Landroid/view/Choreographer$CallbackRecord;,
        Landroid/view/Choreographer$FrameDisplayEventReceiver;,
        Landroid/view/Choreographer$FrameHandler;,
        Landroid/view/Choreographer$FrameCallback;
    }
.end annotation


# static fields
.field public static final CALLBACK_ANIMATION:I = 0x1

.field public static final CALLBACK_INPUT:I = 0x0

.field private static final CALLBACK_LAST:I = 0x2

.field public static final CALLBACK_TRAVERSAL:I = 0x2

.field private static final DEBUG:Z = false

.field private static final DEFAULT_FRAME_DELAY:J = 0xaL

.field private static final FRAME_CALLBACK_TOKEN:Ljava/lang/Object; = null

.field private static final MSG_DO_FRAME:I = 0x0

.field private static final MSG_DO_SCHEDULE_CALLBACK:I = 0x2

.field private static final MSG_DO_SCHEDULE_VSYNC:I = 0x1

.field private static final NANOS_PER_MS:J = 0xf4240L

#the value of this static final field might be set in the static constructor
.field private static final SKIPPED_FRAME_WARNING_LIMIT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Choreographer"

.field private static final USE_FRAME_TIME:Z

.field private static final USE_VSYNC:Z

.field private static volatile sFrameDelay:J

.field private static final sThreadInstance:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/view/Choreographer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

.field private final mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

.field private mCallbacksRunning:Z

.field private final mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

.field private mFrameIntervalNanos:J

.field private mFrameScheduled:Z

.field private final mHandler:Landroid/view/Choreographer$FrameHandler;

.field private mLastFrameTimeNanos:J

.field private final mLock:Ljava/lang/Object;

.field private final mLooper:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 84
    const-wide/16 v0, 0xa

    #@3
    sput-wide v0, Landroid/view/Choreographer;->sFrameDelay:J

    #@5
    .line 87
    new-instance v0, Landroid/view/Choreographer$1;

    #@7
    invoke-direct {v0}, Landroid/view/Choreographer$1;-><init>()V

    #@a
    sput-object v0, Landroid/view/Choreographer;->sThreadInstance:Ljava/lang/ThreadLocal;

    #@c
    .line 100
    const-string v0, "debug.choreographer.vsync"

    #@e
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@11
    move-result v0

    #@12
    sput-boolean v0, Landroid/view/Choreographer;->USE_VSYNC:Z

    #@14
    .line 104
    const-string v0, "debug.choreographer.frametime"

    #@16
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@19
    move-result v0

    #@1a
    sput-boolean v0, Landroid/view/Choreographer;->USE_FRAME_TIME:Z

    #@1c
    .line 109
    const-string v0, "debug.choreographer.skipwarning"

    #@1e
    const/16 v1, 0x1e

    #@20
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@23
    move-result v0

    #@24
    sput v0, Landroid/view/Choreographer;->SKIPPED_FRAME_WARNING_LIMIT:I

    #@26
    .line 119
    new-instance v0, Landroid/view/Choreographer$2;

    #@28
    invoke-direct {v0}, Landroid/view/Choreographer$2;-><init>()V

    #@2b
    sput-object v0, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    #@2d
    return-void
.end method

.method private constructor <init>(Landroid/os/Looper;)V
    .registers 7
    .parameter "looper"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 163
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 123
    new-instance v1, Ljava/lang/Object;

    #@6
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v1, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@b
    .line 164
    iput-object p1, p0, Landroid/view/Choreographer;->mLooper:Landroid/os/Looper;

    #@d
    .line 165
    new-instance v1, Landroid/view/Choreographer$FrameHandler;

    #@f
    invoke-direct {v1, p0, p1}, Landroid/view/Choreographer$FrameHandler;-><init>(Landroid/view/Choreographer;Landroid/os/Looper;)V

    #@12
    iput-object v1, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@14
    .line 166
    sget-boolean v1, Landroid/view/Choreographer;->USE_VSYNC:Z

    #@16
    if-eqz v1, :cond_43

    #@18
    new-instance v1, Landroid/view/Choreographer$FrameDisplayEventReceiver;

    #@1a
    invoke-direct {v1, p0, p1}, Landroid/view/Choreographer$FrameDisplayEventReceiver;-><init>(Landroid/view/Choreographer;Landroid/os/Looper;)V

    #@1d
    :goto_1d
    iput-object v1, p0, Landroid/view/Choreographer;->mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

    #@1f
    .line 167
    const-wide/high16 v3, -0x8000

    #@21
    iput-wide v3, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    #@23
    .line 169
    const v1, 0x4e6e6b28

    #@26
    invoke-static {}, Landroid/view/Choreographer;->getRefreshRate()F

    #@29
    move-result v3

    #@2a
    div-float/2addr v1, v3

    #@2b
    float-to-long v3, v1

    #@2c
    iput-wide v3, p0, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    #@2e
    .line 171
    const/4 v1, 0x3

    #@2f
    new-array v1, v1, [Landroid/view/Choreographer$CallbackQueue;

    #@31
    iput-object v1, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    #@33
    .line 172
    const/4 v0, 0x0

    #@34
    .local v0, i:I
    :goto_34
    const/4 v1, 0x2

    #@35
    if-gt v0, v1, :cond_45

    #@37
    .line 173
    iget-object v1, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    #@39
    new-instance v3, Landroid/view/Choreographer$CallbackQueue;

    #@3b
    invoke-direct {v3, p0, v2}, Landroid/view/Choreographer$CallbackQueue;-><init>(Landroid/view/Choreographer;Landroid/view/Choreographer$1;)V

    #@3e
    aput-object v3, v1, v0

    #@40
    .line 172
    add-int/lit8 v0, v0, 0x1

    #@42
    goto :goto_34

    #@43
    .end local v0           #i:I
    :cond_43
    move-object v1, v2

    #@44
    .line 166
    goto :goto_1d

    #@45
    .line 175
    .restart local v0       #i:I
    :cond_45
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Looper;Landroid/view/Choreographer$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/view/Choreographer;-><init>(Landroid/os/Looper;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/view/Choreographer;)Landroid/view/Choreographer$FrameHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 69
    sget-object v0, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/view/Choreographer;JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/Choreographer;->obtainCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Landroid/view/Choreographer;Landroid/view/Choreographer$CallbackRecord;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/view/Choreographer;->recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V

    #@3
    return-void
.end method

.method public static getFrameDelay()J
    .registers 2

    #@0
    .prologue
    .line 210
    sget-wide v0, Landroid/view/Choreographer;->sFrameDelay:J

    #@2
    return-wide v0
.end method

.method public static getInstance()Landroid/view/Choreographer;
    .registers 1

    #@0
    .prologue
    .line 191
    sget-object v0, Landroid/view/Choreographer;->sThreadInstance:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/Choreographer;

    #@8
    return-object v0
.end method

.method private static getRefreshRate()F
    .registers 3

    #@0
    .prologue
    .line 178
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@8
    move-result-object v0

    #@9
    .line 180
    .local v0, di:Landroid/view/DisplayInfo;
    iget v1, v0, Landroid/view/DisplayInfo;->refreshRate:F

    #@b
    return v1
.end method

.method private isRunningOnLooperThreadLocked()Z
    .registers 3

    #@0
    .prologue
    .line 600
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/view/Choreographer;->mLooper:Landroid/os/Looper;

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private obtainCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;
    .registers 8
    .parameter "dueTime"
    .parameter "action"
    .parameter "token"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 604
    iget-object v0, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    #@3
    .line 605
    .local v0, callback:Landroid/view/Choreographer$CallbackRecord;
    if-nez v0, :cond_11

    #@5
    .line 606
    new-instance v0, Landroid/view/Choreographer$CallbackRecord;

    #@7
    .end local v0           #callback:Landroid/view/Choreographer$CallbackRecord;
    invoke-direct {v0, v2}, Landroid/view/Choreographer$CallbackRecord;-><init>(Landroid/view/Choreographer$1;)V

    #@a
    .line 611
    .restart local v0       #callback:Landroid/view/Choreographer$CallbackRecord;
    :goto_a
    iput-wide p1, v0, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    #@c
    .line 612
    iput-object p3, v0, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    #@e
    .line 613
    iput-object p4, v0, Landroid/view/Choreographer$CallbackRecord;->token:Ljava/lang/Object;

    #@10
    .line 614
    return-object v0

    #@11
    .line 608
    :cond_11
    iget-object v1, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@13
    iput-object v1, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    #@15
    .line 609
    iput-object v2, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@17
    goto :goto_a
.end method

.method private postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V
    .registers 14
    .parameter "callbackType"
    .parameter "action"
    .parameter "token"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 310
    iget-object v6, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 311
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v3

    #@7
    .line 312
    .local v3, now:J
    add-long v0, v3, p4

    #@9
    .line 313
    .local v0, dueTime:J
    iget-object v5, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    #@b
    aget-object v5, v5, p1

    #@d
    invoke-virtual {v5, v0, v1, p2, p3}, Landroid/view/Choreographer$CallbackQueue;->addCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)V

    #@10
    .line 315
    cmp-long v5, v0, v3

    #@12
    if-gtz v5, :cond_19

    #@14
    .line 316
    invoke-direct {p0, v3, v4}, Landroid/view/Choreographer;->scheduleFrameLocked(J)V

    #@17
    .line 323
    :goto_17
    monitor-exit v6

    #@18
    .line 324
    return-void

    #@19
    .line 318
    :cond_19
    iget-object v5, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@1b
    const/4 v7, 0x2

    #@1c
    invoke-virtual {v5, v7, p2}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1f
    move-result-object v2

    #@20
    .line 319
    .local v2, msg:Landroid/os/Message;
    iput p1, v2, Landroid/os/Message;->arg1:I

    #@22
    .line 320
    const/4 v5, 0x1

    #@23
    invoke-virtual {v2, v5}, Landroid/os/Message;->setAsynchronous(Z)V

    #@26
    .line 321
    iget-object v5, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@28
    invoke-virtual {v5, v2, v0, v1}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@2b
    goto :goto_17

    #@2c
    .line 323
    .end local v0           #dueTime:J
    .end local v2           #msg:Landroid/os/Message;
    .end local v3           #now:J
    :catchall_2c
    move-exception v5

    #@2d
    monitor-exit v6
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_2c

    #@2e
    throw v5
.end method

.method private recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 618
    iput-object v0, p1, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    #@3
    .line 619
    iput-object v0, p1, Landroid/view/Choreographer$CallbackRecord;->token:Ljava/lang/Object;

    #@5
    .line 620
    iget-object v0, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    #@7
    iput-object v0, p1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@9
    .line 621
    iput-object p1, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    #@b
    .line 622
    return-void
.end method

.method private removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter "callbackType"
    .parameter "action"
    .parameter "token"

    #@0
    .prologue
    .line 353
    iget-object v1, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 354
    :try_start_3
    iget-object v0, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    #@5
    aget-object v0, v0, p1

    #@7
    invoke-virtual {v0, p2, p3}, Landroid/view/Choreographer$CallbackQueue;->removeCallbacksLocked(Ljava/lang/Object;Ljava/lang/Object;)V

    #@a
    .line 355
    if-eqz p2, :cond_14

    #@c
    if-nez p3, :cond_14

    #@e
    .line 356
    iget-object v0, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@10
    const/4 v2, 0x2

    #@11
    invoke-virtual {v0, v2, p2}, Landroid/view/Choreographer$FrameHandler;->removeMessages(ILjava/lang/Object;)V

    #@14
    .line 358
    :cond_14
    monitor-exit v1

    #@15
    .line 359
    return-void

    #@16
    .line 358
    :catchall_16
    move-exception v0

    #@17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method private scheduleFrameLocked(J)V
    .registers 11
    .parameter "now"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 461
    iget-boolean v3, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    #@3
    if-nez v3, :cond_14

    #@5
    .line 462
    iput-boolean v7, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    #@7
    .line 463
    sget-boolean v3, Landroid/view/Choreographer;->USE_VSYNC:Z

    #@9
    if-eqz v3, :cond_24

    #@b
    .line 471
    invoke-direct {p0}, Landroid/view/Choreographer;->isRunningOnLooperThreadLocked()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_15

    #@11
    .line 472
    invoke-direct {p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    #@14
    .line 489
    :cond_14
    :goto_14
    return-void

    #@15
    .line 474
    :cond_15
    iget-object v3, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@17
    invoke-virtual {v3, v7}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(I)Landroid/os/Message;

    #@1a
    move-result-object v0

    #@1b
    .line 475
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v7}, Landroid/os/Message;->setAsynchronous(Z)V

    #@1e
    .line 476
    iget-object v3, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@20
    invoke-virtual {v3, v0}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    #@23
    goto :goto_14

    #@24
    .line 479
    .end local v0           #msg:Landroid/os/Message;
    :cond_24
    iget-wide v3, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    #@26
    const-wide/32 v5, 0xf4240

    #@29
    div-long/2addr v3, v5

    #@2a
    sget-wide v5, Landroid/view/Choreographer;->sFrameDelay:J

    #@2c
    add-long/2addr v3, v5

    #@2d
    invoke-static {v3, v4, p1, p2}, Ljava/lang/Math;->max(JJ)J

    #@30
    move-result-wide v1

    #@31
    .line 484
    .local v1, nextFrameTime:J
    iget-object v3, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@33
    const/4 v4, 0x0

    #@34
    invoke-virtual {v3, v4}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(I)Landroid/os/Message;

    #@37
    move-result-object v0

    #@38
    .line 485
    .restart local v0       #msg:Landroid/os/Message;
    invoke-virtual {v0, v7}, Landroid/os/Message;->setAsynchronous(Z)V

    #@3b
    .line 486
    iget-object v3, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    #@3d
    invoke-virtual {v3, v0, v1, v2}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@40
    goto :goto_14
.end method

.method private scheduleVsyncLocked()V
    .registers 2

    #@0
    .prologue
    .line 596
    iget-object v0, p0, Landroid/view/Choreographer;->mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

    #@2
    invoke-virtual {v0}, Landroid/view/Choreographer$FrameDisplayEventReceiver;->scheduleVsync()V

    #@5
    .line 597
    return-void
.end method

.method public static setFrameDelay(J)V
    .registers 2
    .parameter "frameDelay"

    #@0
    .prologue
    .line 229
    sput-wide p0, Landroid/view/Choreographer;->sFrameDelay:J

    #@2
    .line 230
    return-void
.end method

.method public static subtractFrameDelay(J)J
    .registers 6
    .parameter "delayMillis"

    #@0
    .prologue
    .line 255
    sget-wide v0, Landroid/view/Choreographer;->sFrameDelay:J

    #@2
    .line 256
    .local v0, frameDelay:J
    cmp-long v2, p0, v0

    #@4
    if-gtz v2, :cond_9

    #@6
    const-wide/16 v2, 0x0

    #@8
    :goto_8
    return-wide v2

    #@9
    :cond_9
    sub-long v2, p0, v0

    #@b
    goto :goto_8
.end method


# virtual methods
.method doCallbacks(IJ)V
    .registers 12
    .parameter "callbackType"
    .parameter "frameTimeNanos"

    #@0
    .prologue
    .line 544
    iget-object v6, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 548
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v3

    #@7
    .line 549
    .local v3, now:J
    iget-object v5, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    #@9
    aget-object v5, v5, p1

    #@b
    invoke-virtual {v5, v3, v4}, Landroid/view/Choreographer$CallbackQueue;->extractDueCallbacksLocked(J)Landroid/view/Choreographer$CallbackRecord;

    #@e
    move-result-object v1

    #@f
    .line 550
    .local v1, callbacks:Landroid/view/Choreographer$CallbackRecord;
    if-nez v1, :cond_13

    #@11
    .line 551
    monitor-exit v6

    #@12
    .line 574
    :goto_12
    return-void

    #@13
    .line 553
    :cond_13
    const/4 v5, 0x1

    #@14
    iput-boolean v5, p0, Landroid/view/Choreographer;->mCallbacksRunning:Z

    #@16
    .line 554
    monitor-exit v6
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_20

    #@17
    .line 556
    move-object v0, v1

    #@18
    .local v0, c:Landroid/view/Choreographer$CallbackRecord;
    :goto_18
    if-eqz v0, :cond_37

    #@1a
    .line 562
    :try_start_1a
    invoke-virtual {v0, p2, p3}, Landroid/view/Choreographer$CallbackRecord;->run(J)V

    #@1d
    .line 556
    iget-object v0, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_23

    #@1f
    goto :goto_18

    #@20
    .line 554
    .end local v0           #c:Landroid/view/Choreographer$CallbackRecord;
    .end local v1           #callbacks:Landroid/view/Choreographer$CallbackRecord;
    .end local v3           #now:J
    :catchall_20
    move-exception v5

    #@21
    :try_start_21
    monitor-exit v6
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v5

    #@23
    .line 565
    .restart local v0       #c:Landroid/view/Choreographer$CallbackRecord;
    .restart local v1       #callbacks:Landroid/view/Choreographer$CallbackRecord;
    .restart local v3       #now:J
    :catchall_23
    move-exception v5

    #@24
    iget-object v6, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@26
    monitor-enter v6

    #@27
    .line 566
    const/4 v7, 0x0

    #@28
    :try_start_28
    iput-boolean v7, p0, Landroid/view/Choreographer;->mCallbacksRunning:Z

    #@2a
    .line 568
    :cond_2a
    iget-object v2, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@2c
    .line 569
    .local v2, next:Landroid/view/Choreographer$CallbackRecord;
    invoke-direct {p0, v1}, Landroid/view/Choreographer;->recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V

    #@2f
    .line 570
    move-object v1, v2

    #@30
    .line 571
    if-nez v1, :cond_2a

    #@32
    .line 572
    monitor-exit v6
    :try_end_33
    .catchall {:try_start_28 .. :try_end_33} :catchall_34

    #@33
    .line 565
    throw v5

    #@34
    .line 572
    .end local v2           #next:Landroid/view/Choreographer$CallbackRecord;
    :catchall_34
    move-exception v5

    #@35
    :try_start_35
    monitor-exit v6
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v5

    #@37
    .line 565
    :cond_37
    iget-object v6, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@39
    monitor-enter v6

    #@3a
    .line 566
    const/4 v5, 0x0

    #@3b
    :try_start_3b
    iput-boolean v5, p0, Landroid/view/Choreographer;->mCallbacksRunning:Z

    #@3d
    .line 568
    :cond_3d
    iget-object v2, v1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    #@3f
    .line 569
    .restart local v2       #next:Landroid/view/Choreographer$CallbackRecord;
    invoke-direct {p0, v1}, Landroid/view/Choreographer;->recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V

    #@42
    .line 570
    move-object v1, v2

    #@43
    .line 571
    if-nez v1, :cond_3d

    #@45
    .line 572
    monitor-exit v6

    #@46
    goto :goto_12

    #@47
    .end local v2           #next:Landroid/view/Choreographer$CallbackRecord;
    :catchall_47
    move-exception v5

    #@48
    monitor-exit v6
    :try_end_49
    .catchall {:try_start_3b .. :try_end_49} :catchall_47

    #@49
    throw v5
.end method

.method doFrame(JI)V
    .registers 16
    .parameter "frameTimeNanos"
    .parameter "frame"

    #@0
    .prologue
    .line 493
    iget-object v9, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v9

    #@3
    .line 494
    :try_start_3
    iget-boolean v8, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    #@5
    if-nez v8, :cond_9

    #@7
    .line 495
    monitor-exit v9

    #@8
    .line 540
    :goto_8
    return-void

    #@9
    .line 498
    :cond_9
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    #@c
    move-result-wide v6

    #@d
    .line 499
    .local v6, startNanos:J
    sub-long v0, v6, p1

    #@f
    .line 500
    .local v0, jitterNanos:J
    iget-wide v10, p0, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    #@11
    cmp-long v8, v0, v10

    #@13
    if-ltz v8, :cond_4a

    #@15
    .line 501
    iget-wide v10, p0, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    #@17
    div-long v4, v0, v10

    #@19
    .line 502
    .local v4, skippedFrames:J
    sget v8, Landroid/view/Choreographer;->SKIPPED_FRAME_WARNING_LIMIT:I

    #@1b
    int-to-long v10, v8

    #@1c
    cmp-long v8, v4, v10

    #@1e
    if-ltz v8, :cond_44

    #@20
    .line 503
    const-string v8, "Choreographer"

    #@22
    new-instance v10, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v11, "Skipped "

    #@29
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v10

    #@2d
    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@30
    move-result-object v10

    #@31
    const-string v11, " frames!  "

    #@33
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v10

    #@37
    const-string v11, "The application may be doing too much work on its main thread."

    #@39
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v10

    #@3d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v10

    #@41
    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 506
    :cond_44
    iget-wide v10, p0, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    #@46
    rem-long v2, v0, v10

    #@48
    .line 514
    .local v2, lastFrameOffset:J
    sub-long p1, v6, v2

    #@4a
    .line 517
    .end local v2           #lastFrameOffset:J
    .end local v4           #skippedFrames:J
    :cond_4a
    iget-wide v10, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    #@4c
    cmp-long v8, p1, v10

    #@4e
    if-gez v8, :cond_58

    #@50
    .line 522
    invoke-direct {p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    #@53
    .line 523
    monitor-exit v9

    #@54
    goto :goto_8

    #@55
    .line 528
    .end local v0           #jitterNanos:J
    .end local v6           #startNanos:J
    :catchall_55
    move-exception v8

    #@56
    monitor-exit v9
    :try_end_57
    .catchall {:try_start_3 .. :try_end_57} :catchall_55

    #@57
    throw v8

    #@58
    .line 526
    .restart local v0       #jitterNanos:J
    .restart local v6       #startNanos:J
    :cond_58
    const/4 v8, 0x0

    #@59
    :try_start_59
    iput-boolean v8, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    #@5b
    .line 527
    iput-wide p1, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    #@5d
    .line 528
    monitor-exit v9
    :try_end_5e
    .catchall {:try_start_59 .. :try_end_5e} :catchall_55

    #@5e
    .line 530
    const/4 v8, 0x0

    #@5f
    invoke-virtual {p0, v8, p1, p2}, Landroid/view/Choreographer;->doCallbacks(IJ)V

    #@62
    .line 531
    const/4 v8, 0x1

    #@63
    invoke-virtual {p0, v8, p1, p2}, Landroid/view/Choreographer;->doCallbacks(IJ)V

    #@66
    .line 532
    const/4 v8, 0x2

    #@67
    invoke-virtual {p0, v8, p1, p2}, Landroid/view/Choreographer;->doCallbacks(IJ)V

    #@6a
    goto :goto_8
.end method

.method doScheduleCallback(I)V
    .registers 6
    .parameter "callbackType"

    #@0
    .prologue
    .line 585
    iget-object v3, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 586
    :try_start_3
    iget-boolean v2, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    #@5
    if-nez v2, :cond_18

    #@7
    .line 587
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@a
    move-result-wide v0

    #@b
    .line 588
    .local v0, now:J
    iget-object v2, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    #@d
    aget-object v2, v2, p1

    #@f
    invoke-virtual {v2, v0, v1}, Landroid/view/Choreographer$CallbackQueue;->hasDueCallbacksLocked(J)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_18

    #@15
    .line 589
    invoke-direct {p0, v0, v1}, Landroid/view/Choreographer;->scheduleFrameLocked(J)V

    #@18
    .line 592
    .end local v0           #now:J
    :cond_18
    monitor-exit v3

    #@19
    .line 593
    return-void

    #@1a
    .line 592
    :catchall_1a
    move-exception v2

    #@1b
    monitor-exit v3
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v2
.end method

.method doScheduleVsync()V
    .registers 3

    #@0
    .prologue
    .line 577
    iget-object v1, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 578
    :try_start_3
    iget-boolean v0, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 579
    invoke-direct {p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    #@a
    .line 581
    :cond_a
    monitor-exit v1

    #@b
    .line 582
    return-void

    #@c
    .line 581
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public getFrameTime()J
    .registers 5

    #@0
    .prologue
    .line 439
    invoke-virtual {p0}, Landroid/view/Choreographer;->getFrameTimeNanos()J

    #@3
    move-result-wide v0

    #@4
    const-wide/32 v2, 0xf4240

    #@7
    div-long/2addr v0, v2

    #@8
    return-wide v0
.end method

.method public getFrameTimeNanos()J
    .registers 4

    #@0
    .prologue
    .line 451
    iget-object v2, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 452
    :try_start_3
    iget-boolean v0, p0, Landroid/view/Choreographer;->mCallbacksRunning:Z

    #@5
    if-nez v0, :cond_12

    #@7
    .line 453
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "This method must only be called as part of a callback while a frame is in progress."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 457
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0

    #@12
    .line 456
    :cond_12
    :try_start_12
    sget-boolean v0, Landroid/view/Choreographer;->USE_FRAME_TIME:Z

    #@14
    if-eqz v0, :cond_1a

    #@16
    iget-wide v0, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    #@18
    :goto_18
    monitor-exit v2

    #@19
    return-wide v0

    #@1a
    :cond_1a
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_f

    #@1d
    move-result-wide v0

    #@1e
    goto :goto_18
.end method

.method public postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V
    .registers 10
    .parameter "callbackType"
    .parameter "action"
    .parameter "token"

    #@0
    .prologue
    .line 273
    const-wide/16 v4, 0x0

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    invoke-virtual/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayed(ILjava/lang/Runnable;Ljava/lang/Object;J)V

    #@9
    .line 274
    return-void
.end method

.method public postCallbackDelayed(ILjava/lang/Runnable;Ljava/lang/Object;J)V
    .registers 8
    .parameter "callbackType"
    .parameter "action"
    .parameter "token"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 292
    if-nez p2, :cond_a

    #@2
    .line 293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "action must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 295
    :cond_a
    if-ltz p1, :cond_f

    #@c
    const/4 v0, 0x2

    #@d
    if-le p1, v0, :cond_17

    #@f
    .line 296
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string v1, "callbackType is invalid"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 299
    :cond_17
    invoke-direct/range {p0 .. p5}, Landroid/view/Choreographer;->postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V

    #@1a
    .line 300
    return-void
.end method

.method public postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 373
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    #@5
    .line 374
    return-void
.end method

.method public postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V
    .registers 10
    .parameter "callback"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 389
    if-nez p1, :cond_a

    #@2
    .line 390
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "callback must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 393
    :cond_a
    const/4 v1, 0x1

    #@b
    sget-object v3, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    #@d
    move-object v0, p0

    #@e
    move-object v2, p1

    #@f
    move-wide v4, p2

    #@10
    invoke-direct/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V

    #@13
    .line 395
    return-void
.end method

.method public removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V
    .registers 6
    .parameter "callbackType"
    .parameter "action"
    .parameter "token"

    #@0
    .prologue
    .line 340
    if-ltz p1, :cond_5

    #@2
    const/4 v0, 0x2

    #@3
    if-le p1, v0, :cond_d

    #@5
    .line 341
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "callbackType is invalid"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 344
    :cond_d
    invoke-direct {p0, p1, p2, p3}, Landroid/view/Choreographer;->removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V

    #@10
    .line 345
    return-void
.end method

.method public removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 406
    if-nez p1, :cond_a

    #@2
    .line 407
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "callback must not be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 410
    :cond_a
    const/4 v0, 0x1

    #@b
    sget-object v1, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    #@d
    invoke-direct {p0, v0, p1, v1}, Landroid/view/Choreographer;->removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V

    #@10
    .line 411
    return-void
.end method
