.class public Landroid/view/WindowManager$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "WindowManager.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/WindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# static fields
.field public static final ALPHA_CHANGED:I = 0x80

.field public static final ANIMATION_CHANGED:I = 0x10

.field public static final BRIGHTNESS_OVERRIDE_FULL:F = 1.0f

.field public static final BRIGHTNESS_OVERRIDE_NONE:F = -1.0f

.field public static final BRIGHTNESS_OVERRIDE_OFF:F = 0.0f

.field public static final BUTTON_BRIGHTNESS_CHANGED:I = 0x1000

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/WindowManager$LayoutParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final DIM_AMOUNT_CHANGED:I = 0x20

.field public static final EVERYTHING_CHANGED:I = -0x1

.field public static final EXTEND_BYPASS_HOME_KEY:I = 0x200

.field public static final EXTEND_BYPASS_POWER_KEY:I = 0x800

.field public static final EXTEND_BYPASS_QUICKMEMO_FUNC:I = 0x80000

.field public static final EXTEND_BYPASS_RECENT_KEY:I = 0x4000

.field public static final EXTEND_DONOT_LCDOFF_KEY:I = 0x10000

.field public static final EXTEND_FIX_ASPECT_WVGA:I = 0x1000

.field public static final EXTEND_FLAGS_CHANGED:I = 0x40000

.field public static final EXTEND_IGNORE_CAPTURE_FUNC:I = 0x20000

.field public static final EXTEND_IGNORE_HOME_KEY:I = 0x100

.field public static final EXTEND_IGNORE_HOOK_KEY:I = 0x200000

.field public static final EXTEND_IGNORE_POWER_KEY:I = 0x400

.field public static final EXTEND_IGNORE_QUICKCLIP_FUNC:I = 0x40000

.field public static final EXTEND_IGNORE_QUICKVOICE_FUNC:I = 0x100000

.field public static final EXTEND_IGNORE_RECENT_KEY:I = 0x2000

.field public static final EXTEND_KEEP_HIDING_NAV:I = 0x400000

.field public static final EXTEND_SPLITWINDOW_DIALOG_EXCEPTION:I = 0x10000000

.field public static final EXTEND_VIDEO_MASK:I = -0x10000000

.field public static final EXTEND_VIDEO_PREVIEW:I = 0x20000000

.field public static final EXTEND_VIDEO_ZOOM:I = 0x10000000

.field public static final EXTUSAGE_SET_EXTERNAL_DISPLAY:I = 0x1

.field public static final EXTUSAGE_SKIP_SURFACEVIEW_RESIZING:I = 0x2

.field public static final FIRST_APPLICATION_WINDOW:I = 0x1

.field public static final FIRST_SUB_WINDOW:I = 0x3e8

.field public static final FIRST_SYSTEM_WINDOW:I = 0x7d0

.field public static final FLAGS_CHANGED:I = 0x4

.field public static final FLAG_ALLOW_LOCK_WHILE_SCREEN_ON:I = 0x1

.field public static final FLAG_ALT_FOCUSABLE_IM:I = 0x20000

.field public static final FLAG_BLUR_BEHIND:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_COMPATIBLE_WINDOW:I = 0x20000000

.field public static final FLAG_DIM_BEHIND:I = 0x2

.field public static final FLAG_DISMISS_KEYGUARD:I = 0x400000

.field public static final FLAG_DITHER:I = 0x1000
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_FORCE_NOT_FULLSCREEN:I = 0x800

.field public static final FLAG_FULLSCREEN:I = 0x400

.field public static final FLAG_HARDWARE_ACCELERATED:I = 0x1000000

.field public static final FLAG_IGNORE_CHEEK_PRESSES:I = 0x8000

.field public static final FLAG_KEEP_SCREEN_ON:I = 0x80

.field public static final FLAG_LAYOUT_INSET_DECOR:I = 0x10000

.field public static final FLAG_LAYOUT_IN_SCREEN:I = 0x100

.field public static final FLAG_LAYOUT_NO_LIMITS:I = 0x200

.field public static final FLAG_NEEDS_MENU_KEY:I = 0x8000000

.field public static final FLAG_NOT_FOCUSABLE:I = 0x8

.field public static final FLAG_NOT_TOUCHABLE:I = 0x10

.field public static final FLAG_NOT_TOUCH_MODAL:I = 0x20

.field public static final FLAG_SCALED:I = 0x4000

.field public static final FLAG_SECURE:I = 0x2000

.field public static final FLAG_SHOW_WALLPAPER:I = 0x100000

.field public static final FLAG_SHOW_WHEN_LOCKED:I = 0x80000

.field public static final FLAG_SLIPPERY:I = 0x4000000

.field public static final FLAG_SPLIT_TOUCH:I = 0x800000

.field public static final FLAG_SYSTEM_ERROR:I = 0x40000000

.field public static final FLAG_TOUCHABLE_WHEN_WAKING:I = 0x40

.field public static final FLAG_TURN_SCREEN_ON:I = 0x200000

.field public static final FLAG_WATCH_OUTSIDE_TOUCH:I = 0x40000

.field public static final FORMAT_CHANGED:I = 0x8

.field public static final INPUT_FEATURES_CHANGED:I = 0x8000

.field public static final INPUT_FEATURE_DISABLE_POINTER_GESTURES:I = 0x1

.field public static final INPUT_FEATURE_DISABLE_USER_ACTIVITY:I = 0x4

.field public static final INPUT_FEATURE_NO_INPUT_CHANNEL:I = 0x2

.field public static final LAST_APPLICATION_WINDOW:I = 0x63

.field public static final LAST_SUB_WINDOW:I = 0x7cf

.field public static final LAST_SYSTEM_WINDOW:I = 0xbb7

.field public static final LAYOUT_CHANGED:I = 0x1

.field public static final MEMORY_TYPE_CHANGED:I = 0x100

.field public static final MEMORY_TYPE_GPU:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MEMORY_TYPE_HARDWARE:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MEMORY_TYPE_NORMAL:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MEMORY_TYPE_PUSH_BUFFERS:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PRIVATE_FLAGS_CHANGED:I = 0x10000

.field public static final PRIVATE_FLAG_FAKE_HARDWARE_ACCELERATED:I = 0x1

.field public static final PRIVATE_FLAG_FORCE_HARDWARE_ACCELERATED:I = 0x2

.field public static final PRIVATE_FLAG_FORCE_SHOW_NAV_BAR:I = 0x20

.field public static final PRIVATE_FLAG_SET_NEEDS_MENU_KEY:I = 0x8

.field public static final PRIVATE_FLAG_SHOW_FOR_ALL_USERS:I = 0x10

.field public static final PRIVATE_FLAG_WANTS_OFFSET_NOTIFICATIONS:I = 0x4

.field public static final SCREEN_BRIGHTNESS_CHANGED:I = 0x800

.field public static final SCREEN_ORIENTATION_CHANGED:I = 0x400

.field public static final SOFT_INPUT_ADJUST_NOTHING:I = 0x30

.field public static final SOFT_INPUT_ADJUST_PAN:I = 0x20

.field public static final SOFT_INPUT_ADJUST_RESIZE:I = 0x10

.field public static final SOFT_INPUT_ADJUST_UNSPECIFIED:I = 0x0

.field public static final SOFT_INPUT_IS_FORWARD_NAVIGATION:I = 0x100

.field public static final SOFT_INPUT_MASK_ADJUST:I = 0xf0

.field public static final SOFT_INPUT_MASK_STATE:I = 0xf

.field public static final SOFT_INPUT_MODE_CHANGED:I = 0x200

.field public static final SOFT_INPUT_STATE_ALWAYS_HIDDEN:I = 0x3

.field public static final SOFT_INPUT_STATE_ALWAYS_VISIBLE:I = 0x5

.field public static final SOFT_INPUT_STATE_HIDDEN:I = 0x2

.field public static final SOFT_INPUT_STATE_UNCHANGED:I = 0x1

.field public static final SOFT_INPUT_STATE_UNSPECIFIED:I = 0x0

.field public static final SOFT_INPUT_STATE_VISIBLE:I = 0x4

.field public static final SYSTEM_UI_LISTENER_CHANGED:I = 0x4000

.field public static final SYSTEM_UI_VISIBILITY_CHANGED:I = 0x2000

.field public static final TITLE_CHANGED:I = 0x40

.field public static final TYPE_APPLICATION:I = 0x2

.field public static final TYPE_APPLICATION_ATTACHED_DIALOG:I = 0x3eb

.field public static final TYPE_APPLICATION_MEDIA:I = 0x3e9

.field public static final TYPE_APPLICATION_MEDIA_OVERLAY:I = 0x3ec

.field public static final TYPE_APPLICATION_PANEL:I = 0x3e8

.field public static final TYPE_APPLICATION_STARTING:I = 0x3

.field public static final TYPE_APPLICATION_SUB_PANEL:I = 0x3ea

.field public static final TYPE_BASE_APPLICATION:I = 0x1

.field public static final TYPE_BOOT_PROGRESS:I = 0x7e5

.field public static final TYPE_CHANGED:I = 0x2

.field public static final TYPE_DISPLAY_OVERLAY:I = 0x7ea

.field public static final TYPE_DRAG:I = 0x7e0

.field public static final TYPE_DREAM:I = 0x7e7

.field public static final TYPE_HIDDEN_NAV_CONSUMER:I = 0x7e6

.field public static final TYPE_INPUT_METHOD:I = 0x7db

.field public static final TYPE_INPUT_METHOD_DIALOG:I = 0x7dc

.field public static final TYPE_KEYGUARD:I = 0x7d4

.field public static final TYPE_KEYGUARD_DIALOG:I = 0x7d9

.field public static final TYPE_MAGNIFICATION_OVERLAY:I = 0x7eb

.field public static final TYPE_NAVIGATION_BAR:I = 0x7e3

.field public static final TYPE_NAVIGATION_BAR_PANEL:I = 0x7e8

.field public static final TYPE_PHONE:I = 0x7d2

.field public static final TYPE_POINTER:I = 0x7e2

.field public static final TYPE_PRIORITY_PHONE:I = 0x7d7

.field public static final TYPE_RECENTS_OVERLAY:I = 0x7ec

.field public static final TYPE_SEARCH_BAR:I = 0x7d1

.field public static final TYPE_SECURE_SYSTEM_OVERLAY:I = 0x7df

.field public static final TYPE_STATUS_BAR:I = 0x7d0

.field public static final TYPE_STATUS_BAR_PANEL:I = 0x7de

.field public static final TYPE_STATUS_BAR_SUB_PANEL:I = 0x7e1

.field public static final TYPE_SYSTEM_ALERT:I = 0x7d3

.field public static final TYPE_SYSTEM_DIALOG:I = 0x7d8

.field public static final TYPE_SYSTEM_ERROR:I = 0x7da

.field public static final TYPE_SYSTEM_OVERLAY:I = 0x7d6

.field public static final TYPE_TOAST:I = 0x7d5

.field public static final TYPE_UNIVERSE_BACKGROUND:I = 0x7e9

.field public static final TYPE_VOLUME_OVERLAY:I = 0x7e4

.field public static final TYPE_WALLPAPER:I = 0x7dd

.field public static final USER_ACTIVITY_TIMEOUT_CHANGED:I = 0x20000


# instance fields
.field public alpha:F

.field public buttonBrightness:F

.field public dimAmount:F

.field public extUsage:I

.field public extend:I

.field public flags:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        flagMapping = {
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1
                mask = 0x1
                name = "FLAG_ALLOW_LOCK_WHILE_SCREEN_ON"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x2
                mask = 0x2
                name = "FLAG_DIM_BEHIND"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x4
                mask = 0x4
                name = "FLAG_BLUR_BEHIND"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x8
                mask = 0x8
                name = "FLAG_NOT_FOCUSABLE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x10
                mask = 0x10
                name = "FLAG_NOT_TOUCHABLE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x20
                mask = 0x20
                name = "FLAG_NOT_TOUCH_MODAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x40
                mask = 0x40
                name = "FLAG_TOUCHABLE_WHEN_WAKING"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x80
                mask = 0x80
                name = "FLAG_KEEP_SCREEN_ON"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x100
                mask = 0x100
                name = "FLAG_LAYOUT_IN_SCREEN"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x200
                mask = 0x200
                name = "FLAG_LAYOUT_NO_LIMITS"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x400
                mask = 0x400
                name = "FLAG_FULLSCREEN"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800
                mask = 0x800
                name = "FLAG_FORCE_NOT_FULLSCREEN"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1000
                mask = 0x1000
                name = "FLAG_DITHER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x2000
                mask = 0x2000
                name = "FLAG_SECURE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x4000
                mask = 0x4000
                name = "FLAG_SCALED"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x8000
                mask = 0x8000
                name = "FLAG_IGNORE_CHEEK_PRESSES"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x10000
                mask = 0x10000
                name = "FLAG_LAYOUT_INSET_DECOR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x20000
                mask = 0x20000
                name = "FLAG_ALT_FOCUSABLE_IM"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x40000
                mask = 0x40000
                name = "FLAG_WATCH_OUTSIDE_TOUCH"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x80000
                mask = 0x80000
                name = "FLAG_SHOW_WHEN_LOCKED"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x100000
                mask = 0x100000
                name = "FLAG_SHOW_WALLPAPER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x200000
                mask = 0x200000
                name = "FLAG_TURN_SCREEN_ON"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x400000
                mask = 0x400000
                name = "FLAG_DISMISS_KEYGUARD"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800000
                mask = 0x800000
                name = "FLAG_SPLIT_TOUCH"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1000000
                mask = 0x1000000
                name = "FLAG_HARDWARE_ACCELERATED"
            .end subannotation
        }
    .end annotation
.end field

.field public format:I

.field public gravity:I

.field public hasSystemUiListeners:Z

.field public horizontalMargin:F

.field public horizontalWeight:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public inputFeatures:I

.field private mCompatibilityParamsBackup:[I

.field private mTitle:Ljava/lang/CharSequence;

.field public memoryType:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public packageName:Ljava/lang/String;

.field public privateFlags:I

.field public screenBrightness:F

.field public screenOrientation:I

.field public softInputMode:I

.field public subtreeSystemUiVisibility:I

.field public systemUiVisibility:I

.field public token:Landroid/os/IBinder;

.field public type:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "TYPE_BASE_APPLICATION"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "TYPE_APPLICATION"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "TYPE_APPLICATION_STARTING"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3e8
                to = "TYPE_APPLICATION_PANEL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3e9
                to = "TYPE_APPLICATION_MEDIA"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3ea
                to = "TYPE_APPLICATION_SUB_PANEL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3eb
                to = "TYPE_APPLICATION_ATTACHED_DIALOG"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3ec
                to = "TYPE_APPLICATION_MEDIA_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d0
                to = "TYPE_STATUS_BAR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d1
                to = "TYPE_SEARCH_BAR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d2
                to = "TYPE_PHONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d3
                to = "TYPE_SYSTEM_ALERT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d4
                to = "TYPE_KEYGUARD"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d5
                to = "TYPE_TOAST"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d6
                to = "TYPE_SYSTEM_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d7
                to = "TYPE_PRIORITY_PHONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d8
                to = "TYPE_SYSTEM_DIALOG"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7d9
                to = "TYPE_KEYGUARD_DIALOG"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7da
                to = "TYPE_SYSTEM_ERROR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7db
                to = "TYPE_INPUT_METHOD"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7dc
                to = "TYPE_INPUT_METHOD_DIALOG"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7dd
                to = "TYPE_WALLPAPER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7de
                to = "TYPE_STATUS_BAR_PANEL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7df
                to = "TYPE_SECURE_SYSTEM_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e0
                to = "TYPE_DRAG"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e1
                to = "TYPE_STATUS_BAR_SUB_PANEL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e2
                to = "TYPE_POINTER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e3
                to = "TYPE_NAVIGATION_BAR"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e4
                to = "TYPE_VOLUME_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e5
                to = "TYPE_BOOT_PROGRESS"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e6
                to = "TYPE_HIDDEN_NAV_CONSUMER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e7
                to = "TYPE_DREAM"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7e8
                to = "TYPE_NAVIGATION_BAR_PANEL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7ea
                to = "TYPE_DISPLAY_OVERLAY"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x7eb
                to = "TYPE_MAGNIFICATION_OVERLAY"
            .end subannotation
        }
    .end annotation
.end field

.field public userActivityTimeout:J

.field public verticalMargin:F

.field public verticalWeight:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public windowAnimations:I

.field public x:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public y:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1509
    new-instance v0, Landroid/view/WindowManager$LayoutParams$1;

    #@2
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams$1;-><init>()V

    #@5
    sput-object v0, Landroid/view/WindowManager$LayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/high16 v0, -0x4080

    #@4
    const/4 v3, 0x0

    #@5
    const/4 v2, -0x1

    #@6
    .line 1421
    invoke-direct {p0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@9
    .line 1143
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@b
    .line 1150
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@d
    .line 1179
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@f
    .line 1187
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@11
    .line 1193
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@13
    .line 1198
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@15
    .line 1208
    iput v2, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1422
    const/4 v0, 0x2

    #@22
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@24
    .line 1423
    iput v2, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@26
    .line 1424
    return-void
.end method

.method public constructor <init>(I)V
    .registers 6
    .parameter "_type"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/high16 v0, -0x4080

    #@4
    const/4 v3, 0x0

    #@5
    const/4 v2, -0x1

    #@6
    .line 1427
    invoke-direct {p0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@9
    .line 1143
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@b
    .line 1150
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@d
    .line 1179
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@f
    .line 1187
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@11
    .line 1193
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@13
    .line 1198
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@15
    .line 1208
    iput v2, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1428
    iput p1, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@23
    .line 1429
    iput v2, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@25
    .line 1430
    return-void
.end method

.method public constructor <init>(II)V
    .registers 7
    .parameter "_type"
    .parameter "_flags"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/high16 v0, -0x4080

    #@4
    const/4 v3, 0x0

    #@5
    const/4 v2, -0x1

    #@6
    .line 1433
    invoke-direct {p0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@9
    .line 1143
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@b
    .line 1150
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@d
    .line 1179
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@f
    .line 1187
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@11
    .line 1193
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@13
    .line 1198
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@15
    .line 1208
    iput v2, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v3, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1434
    iput p1, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@23
    .line 1435
    iput p2, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@25
    .line 1436
    iput v2, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@27
    .line 1437
    return-void
.end method

.method public constructor <init>(III)V
    .registers 8
    .parameter "_type"
    .parameter "_flags"
    .parameter "_format"

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    const/high16 v1, -0x4080

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v0, -0x1

    #@6
    .line 1440
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@9
    .line 1143
    iput v3, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@b
    .line 1150
    iput v3, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@d
    .line 1179
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@f
    .line 1187
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@11
    .line 1193
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@13
    .line 1198
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@15
    .line 1208
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1441
    iput p1, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@23
    .line 1442
    iput p2, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@25
    .line 1443
    iput p3, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@27
    .line 1444
    return-void
.end method

.method public constructor <init>(IIIII)V
    .registers 9
    .parameter "w"
    .parameter "h"
    .parameter "_type"
    .parameter "_flags"
    .parameter "_format"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/high16 v0, -0x4080

    #@4
    const/4 v2, 0x0

    #@5
    .line 1447
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@8
    .line 1143
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@a
    .line 1150
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@c
    .line 1179
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@e
    .line 1187
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@10
    .line 1193
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@12
    .line 1198
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@14
    .line 1208
    const/4 v0, -0x1

    #@15
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1448
    iput p3, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@23
    .line 1449
    iput p4, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@25
    .line 1450
    iput p5, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@27
    .line 1451
    return-void
.end method

.method public constructor <init>(IIIIIII)V
    .registers 11
    .parameter "w"
    .parameter "h"
    .parameter "xpos"
    .parameter "ypos"
    .parameter "_type"
    .parameter "_flags"
    .parameter "_format"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/high16 v0, -0x4080

    #@4
    const/4 v2, 0x0

    #@5
    .line 1455
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@8
    .line 1143
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@a
    .line 1150
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@c
    .line 1179
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@e
    .line 1187
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@10
    .line 1193
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@12
    .line 1198
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@14
    .line 1208
    const/4 v0, -0x1

    #@15
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1456
    iput p3, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@23
    .line 1457
    iput p4, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@25
    .line 1458
    iput p5, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@27
    .line 1459
    iput p6, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@29
    .line 1460
    iput p7, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@2b
    .line 1461
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/high16 v0, -0x4080

    #@4
    const/4 v2, 0x0

    #@5
    .line 1521
    invoke-direct {p0}, Landroid/view/ViewGroup$LayoutParams;-><init>()V

    #@8
    .line 1143
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@a
    .line 1150
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@c
    .line 1179
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@e
    .line 1187
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@10
    .line 1193
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@12
    .line 1198
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@14
    .line 1208
    const/4 v0, -0x1

    #@15
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@17
    .line 1286
    const-wide/16 v0, -0x1

    #@19
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@1b
    .line 1583
    iput-object v2, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@1d
    .line 1881
    const-string v0, ""

    #@1f
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@21
    .line 1522
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@27
    .line 1523
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2d
    .line 1524
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@33
    .line 1525
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v0

    #@37
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@39
    .line 1526
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3f
    .line 1527
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v0

    #@43
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@45
    .line 1528
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v0

    #@49
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@4b
    .line 1529
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v0

    #@4f
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@51
    .line 1530
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v0

    #@55
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@57
    .line 1531
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@5a
    move-result v0

    #@5b
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@5d
    .line 1532
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@60
    move-result v0

    #@61
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@63
    .line 1533
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@66
    move-result v0

    #@67
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@69
    .line 1534
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6c
    move-result v0

    #@6d
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@6f
    .line 1535
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@72
    move-result v0

    #@73
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@75
    .line 1536
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@78
    move-result v0

    #@79
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@7b
    .line 1537
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@7e
    move-result v0

    #@7f
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@81
    .line 1538
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@84
    move-result v0

    #@85
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@87
    .line 1539
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8a
    move-result-object v0

    #@8b
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@8d
    .line 1540
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@90
    move-result-object v0

    #@91
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@93
    .line 1541
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@95
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@98
    move-result-object v0

    #@99
    check-cast v0, Ljava/lang/CharSequence;

    #@9b
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@9d
    .line 1542
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v0

    #@a1
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@a3
    .line 1543
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a6
    move-result v0

    #@a7
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@a9
    .line 1544
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@ac
    move-result v0

    #@ad
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@af
    .line 1545
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b2
    move-result v0

    #@b3
    if-eqz v0, :cond_d1

    #@b5
    const/4 v0, 0x1

    #@b6
    :goto_b6
    iput-boolean v0, p0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@b8
    .line 1546
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@bb
    move-result v0

    #@bc
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@be
    .line 1547
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@c1
    move-result-wide v0

    #@c2
    iput-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@c4
    .line 1548
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v0

    #@c8
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@ca
    .line 1549
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@cd
    move-result v0

    #@ce
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@d0
    .line 1550
    return-void

    #@d1
    .line 1545
    :cond_d1
    const/4 v0, 0x0

    #@d2
    goto :goto_b6
.end method

.method public static mayUseInputMethod(I)Z
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 976
    const v0, 0x20008

    #@3
    and-int/2addr v0, p0

    #@4
    sparse-switch v0, :sswitch_data_c

    #@7
    .line 981
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    .line 979
    :sswitch_9
    const/4 v0, 0x1

    #@a
    goto :goto_8

    #@b
    .line 976
    nop

    #@c
    :sswitch_data_c
    .sparse-switch
        0x0 -> :sswitch_9
        0x20008 -> :sswitch_9
    .end sparse-switch
.end method


# virtual methods
.method backup()V
    .registers 4

    #@0
    .prologue
    .line 1856
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@2
    .line 1857
    .local v0, backup:[I
    if-nez v0, :cond_9

    #@4
    .line 1859
    const/4 v1, 0x4

    #@5
    new-array v0, v1, [I

    #@7
    .end local v0           #backup:[I
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@9
    .line 1861
    .restart local v0       #backup:[I
    :cond_9
    const/4 v1, 0x0

    #@a
    iget v2, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@c
    aput v2, v0, v1

    #@e
    .line 1862
    const/4 v1, 0x1

    #@f
    iget v2, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@11
    aput v2, v0, v1

    #@13
    .line 1863
    const/4 v1, 0x2

    #@14
    iget v2, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@16
    aput v2, v0, v1

    #@18
    .line 1864
    const/4 v1, 0x3

    #@19
    iget v2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1b
    aput v2, v0, v1

    #@1d
    .line 1865
    return-void
.end method

.method public final copyFrom(Landroid/view/WindowManager$LayoutParams;)I
    .registers 7
    .parameter "o"

    #@0
    .prologue
    .line 1586
    const/4 v0, 0x0

    #@1
    .line 1588
    .local v0, changes:I
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3
    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@5
    if-eq v1, v2, :cond_d

    #@7
    .line 1589
    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@9
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@b
    .line 1590
    or-int/lit8 v0, v0, 0x1

    #@d
    .line 1592
    :cond_d
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@f
    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@11
    if-eq v1, v2, :cond_19

    #@13
    .line 1593
    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@15
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@17
    .line 1594
    or-int/lit8 v0, v0, 0x1

    #@19
    .line 1596
    :cond_19
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@1b
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    #@1d
    if-eq v1, v2, :cond_25

    #@1f
    .line 1597
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    #@21
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@23
    .line 1598
    or-int/lit8 v0, v0, 0x1

    #@25
    .line 1600
    :cond_25
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@27
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    #@29
    if-eq v1, v2, :cond_31

    #@2b
    .line 1601
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    #@2d
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@2f
    .line 1602
    or-int/lit8 v0, v0, 0x1

    #@31
    .line 1604
    :cond_31
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@33
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@35
    cmpl-float v1, v1, v2

    #@37
    if-eqz v1, :cond_3f

    #@39
    .line 1605
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@3b
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->horizontalWeight:F

    #@3d
    .line 1606
    or-int/lit8 v0, v0, 0x1

    #@3f
    .line 1608
    :cond_3f
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@41
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@43
    cmpl-float v1, v1, v2

    #@45
    if-eqz v1, :cond_4d

    #@47
    .line 1609
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@49
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->verticalWeight:F

    #@4b
    .line 1610
    or-int/lit8 v0, v0, 0x1

    #@4d
    .line 1612
    :cond_4d
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@4f
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@51
    cmpl-float v1, v1, v2

    #@53
    if-eqz v1, :cond_5b

    #@55
    .line 1613
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@57
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@59
    .line 1614
    or-int/lit8 v0, v0, 0x1

    #@5b
    .line 1616
    :cond_5b
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@5d
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@5f
    cmpl-float v1, v1, v2

    #@61
    if-eqz v1, :cond_69

    #@63
    .line 1617
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@65
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@67
    .line 1618
    or-int/lit8 v0, v0, 0x1

    #@69
    .line 1620
    :cond_69
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@6b
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@6d
    if-eq v1, v2, :cond_75

    #@6f
    .line 1621
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@71
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@73
    .line 1622
    or-int/lit8 v0, v0, 0x2

    #@75
    .line 1624
    :cond_75
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@77
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@79
    if-eq v1, v2, :cond_81

    #@7b
    .line 1625
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@7d
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@7f
    .line 1626
    or-int/lit8 v0, v0, 0x4

    #@81
    .line 1628
    :cond_81
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@83
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@85
    if-eq v1, v2, :cond_8e

    #@87
    .line 1629
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@89
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@8b
    .line 1630
    const/high16 v1, 0x1

    #@8d
    or-int/2addr v0, v1

    #@8e
    .line 1632
    :cond_8e
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@90
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@92
    if-eq v1, v2, :cond_9a

    #@94
    .line 1633
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@96
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@98
    .line 1634
    or-int/lit16 v0, v0, 0x200

    #@9a
    .line 1636
    :cond_9a
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@9c
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@9e
    if-eq v1, v2, :cond_a6

    #@a0
    .line 1637
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@a2
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@a4
    .line 1638
    or-int/lit8 v0, v0, 0x1

    #@a6
    .line 1640
    :cond_a6
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@a8
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->format:I

    #@aa
    if-eq v1, v2, :cond_b2

    #@ac
    .line 1641
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->format:I

    #@ae
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@b0
    .line 1642
    or-int/lit8 v0, v0, 0x8

    #@b2
    .line 1644
    :cond_b2
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@b4
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@b6
    if-eq v1, v2, :cond_be

    #@b8
    .line 1645
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@ba
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@bc
    .line 1646
    or-int/lit8 v0, v0, 0x10

    #@be
    .line 1648
    :cond_be
    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@c0
    if-nez v1, :cond_c6

    #@c2
    .line 1651
    iget-object v1, p1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@c4
    iput-object v1, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@c6
    .line 1653
    :cond_c6
    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@c8
    if-nez v1, :cond_ce

    #@ca
    .line 1656
    iget-object v1, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@cc
    iput-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@ce
    .line 1658
    :cond_ce
    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@d0
    iget-object v2, p1, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@d2
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v1

    #@d6
    if-nez v1, :cond_de

    #@d8
    .line 1659
    iget-object v1, p1, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@da
    iput-object v1, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@dc
    .line 1660
    or-int/lit8 v0, v0, 0x40

    #@de
    .line 1662
    :cond_de
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@e0
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@e2
    cmpl-float v1, v1, v2

    #@e4
    if-eqz v1, :cond_ec

    #@e6
    .line 1663
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@e8
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@ea
    .line 1664
    or-int/lit16 v0, v0, 0x80

    #@ec
    .line 1666
    :cond_ec
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@ee
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@f0
    cmpl-float v1, v1, v2

    #@f2
    if-eqz v1, :cond_fa

    #@f4
    .line 1667
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@f6
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@f8
    .line 1668
    or-int/lit8 v0, v0, 0x20

    #@fa
    .line 1670
    :cond_fa
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@fc
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@fe
    cmpl-float v1, v1, v2

    #@100
    if-eqz v1, :cond_108

    #@102
    .line 1671
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@104
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@106
    .line 1672
    or-int/lit16 v0, v0, 0x800

    #@108
    .line 1674
    :cond_108
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@10a
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@10c
    cmpl-float v1, v1, v2

    #@10e
    if-eqz v1, :cond_116

    #@110
    .line 1675
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@112
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@114
    .line 1676
    or-int/lit16 v0, v0, 0x1000

    #@116
    .line 1679
    :cond_116
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@118
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@11a
    if-eq v1, v2, :cond_122

    #@11c
    .line 1680
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@11e
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@120
    .line 1681
    or-int/lit16 v0, v0, 0x400

    #@122
    .line 1684
    :cond_122
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@124
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@126
    if-ne v1, v2, :cond_12e

    #@128
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@12a
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@12c
    if-eq v1, v2, :cond_138

    #@12e
    .line 1686
    :cond_12e
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@130
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@132
    .line 1687
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@134
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@136
    .line 1688
    or-int/lit16 v0, v0, 0x2000

    #@138
    .line 1691
    :cond_138
    iget-boolean v1, p0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@13a
    iget-boolean v2, p1, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@13c
    if-eq v1, v2, :cond_144

    #@13e
    .line 1692
    iget-boolean v1, p1, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@140
    iput-boolean v1, p0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@142
    .line 1693
    or-int/lit16 v0, v0, 0x4000

    #@144
    .line 1696
    :cond_144
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@146
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@148
    if-eq v1, v2, :cond_152

    #@14a
    .line 1697
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@14c
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@14e
    .line 1698
    const v1, 0x8000

    #@151
    or-int/2addr v0, v1

    #@152
    .line 1701
    :cond_152
    iget-wide v1, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@154
    iget-wide v3, p1, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@156
    cmp-long v1, v1, v3

    #@158
    if-eqz v1, :cond_161

    #@15a
    .line 1702
    iget-wide v1, p1, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@15c
    iput-wide v1, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@15e
    .line 1703
    const/high16 v1, 0x2

    #@160
    or-int/2addr v0, v1

    #@161
    .line 1707
    :cond_161
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@163
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@165
    if-eq v1, v2, :cond_16e

    #@167
    .line 1708
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@169
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@16b
    .line 1709
    const/high16 v1, 0x4

    #@16d
    or-int/2addr v0, v1

    #@16e
    .line 1714
    :cond_16e
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@170
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@172
    if-eq v1, v2, :cond_17a

    #@174
    .line 1715
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@176
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@178
    .line 1716
    or-int/lit8 v0, v0, 0x1

    #@17a
    .line 1721
    :cond_17a
    return v0
.end method

.method public debug(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "output"

    #@0
    .prologue
    .line 1726
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "Contents of "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ":"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object p1

    #@1d
    .line 1727
    const-string v0, "Debug"

    #@1f
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1728
    const-string v0, ""

    #@24
    invoke-super {p0, v0}, Landroid/view/ViewGroup$LayoutParams;->debug(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object p1

    #@28
    .line 1729
    const-string v0, "Debug"

    #@2a
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1730
    const-string v0, "Debug"

    #@2f
    const-string v1, ""

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1731
    const-string v0, "Debug"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "WindowManager.LayoutParams={title="

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget-object v2, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    const-string/jumbo v2, "}"

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 1732
    const-string v0, ""

    #@57
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 1475
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1471
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method restore()V
    .registers 3

    #@0
    .prologue
    .line 1872
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mCompatibilityParamsBackup:[I

    #@2
    .line 1873
    .local v0, backup:[I
    if-eqz v0, :cond_18

    #@4
    .line 1874
    const/4 v1, 0x0

    #@5
    aget v1, v0, v1

    #@7
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@9
    .line 1875
    const/4 v1, 0x1

    #@a
    aget v1, v0, v1

    #@c
    iput v1, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@e
    .line 1876
    const/4 v1, 0x2

    #@f
    aget v1, v0, v1

    #@11
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@13
    .line 1877
    const/4 v1, 0x3

    #@14
    aget v1, v0, v1

    #@16
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@18
    .line 1879
    :cond_18
    return-void
.end method

.method public scale(F)V
    .registers 4
    .parameter "scale"

    #@0
    .prologue
    const/high16 v1, 0x3f00

    #@2
    .line 1841
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@4
    int-to-float v0, v0

    #@5
    mul-float/2addr v0, p1

    #@6
    add-float/2addr v0, v1

    #@7
    float-to-int v0, v0

    #@8
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@a
    .line 1842
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@c
    int-to-float v0, v0

    #@d
    mul-float/2addr v0, p1

    #@e
    add-float/2addr v0, v1

    #@f
    float-to-int v0, v0

    #@10
    iput v0, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@12
    .line 1843
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@14
    if-lez v0, :cond_1e

    #@16
    .line 1844
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@18
    int-to-float v0, v0

    #@19
    mul-float/2addr v0, p1

    #@1a
    add-float/2addr v0, v1

    #@1b
    float-to-int v0, v0

    #@1c
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1e
    .line 1846
    :cond_1e
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@20
    if-lez v0, :cond_2a

    #@22
    .line 1847
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@24
    int-to-float v0, v0

    #@25
    mul-float/2addr v0, p1

    #@26
    add-float/2addr v0, v1

    #@27
    float-to-int v0, v0

    #@28
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2a
    .line 1849
    :cond_2a
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 1464
    if-nez p1, :cond_4

    #@2
    .line 1465
    const-string p1, ""

    #@4
    .line 1467
    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->stringOrSpannedString(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@a
    .line 1468
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v5, -0x2

    #@1
    const/4 v2, 0x0

    #@2
    const/high16 v4, -0x4080

    #@4
    const/4 v3, -0x1

    #@5
    .line 1737
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    const/16 v1, 0x100

    #@9
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@c
    .line 1738
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "WM.LayoutParams{"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    .line 1739
    const-string v1, "("

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 1740
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    .line 1741
    const/16 v1, 0x2c

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@20
    .line 1742
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    .line 1743
    const-string v1, ")("

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 1744
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2c
    if-ne v1, v3, :cond_1a7

    #@2e
    const-string v1, "fill"

    #@30
    :goto_30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    .line 1745
    const/16 v1, 0x78

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@38
    .line 1746
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@3a
    if-ne v1, v3, :cond_1b8

    #@3c
    const-string v1, "fill"

    #@3e
    :goto_3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    .line 1747
    const-string v1, ")"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 1748
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@48
    cmpl-float v1, v1, v2

    #@4a
    if-eqz v1, :cond_56

    #@4c
    .line 1749
    const-string v1, " hm="

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    .line 1750
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@56
    .line 1752
    :cond_56
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@58
    cmpl-float v1, v1, v2

    #@5a
    if-eqz v1, :cond_66

    #@5c
    .line 1753
    const-string v1, " vm="

    #@5e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    .line 1754
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@66
    .line 1756
    :cond_66
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@68
    if-eqz v1, :cond_78

    #@6a
    .line 1757
    const-string v1, " gr=#"

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    .line 1758
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@71
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    .line 1760
    :cond_78
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@7a
    if-eqz v1, :cond_8a

    #@7c
    .line 1761
    const-string v1, " sim=#"

    #@7e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    .line 1762
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@83
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    .line 1764
    :cond_8a
    const-string v1, " ty="

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    .line 1765
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    .line 1766
    const-string v1, " fl=#"

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    .line 1767
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@9b
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@9e
    move-result-object v1

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    .line 1768
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@a4
    if-eqz v1, :cond_b5

    #@a6
    .line 1769
    const-string v1, " pfl=0x"

    #@a8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    iget v2, p0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@ae
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@b1
    move-result-object v2

    #@b2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    .line 1771
    :cond_b5
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@b7
    if-eq v1, v3, :cond_c3

    #@b9
    .line 1772
    const-string v1, " fmt="

    #@bb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    .line 1773
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@c0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    .line 1775
    :cond_c3
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@c5
    if-eqz v1, :cond_d5

    #@c7
    .line 1776
    const-string v1, " wanim=0x"

    #@c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    .line 1777
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@ce
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@d1
    move-result-object v1

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    .line 1779
    :cond_d5
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@d7
    if-eq v1, v3, :cond_e3

    #@d9
    .line 1780
    const-string v1, " or="

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    .line 1781
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@e0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    .line 1783
    :cond_e3
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@e5
    const/high16 v2, 0x3f80

    #@e7
    cmpl-float v1, v1, v2

    #@e9
    if-eqz v1, :cond_f5

    #@eb
    .line 1784
    const-string v1, " alpha="

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    .line 1785
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@f2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@f5
    .line 1787
    :cond_f5
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@f7
    cmpl-float v1, v1, v4

    #@f9
    if-eqz v1, :cond_105

    #@fb
    .line 1788
    const-string v1, " sbrt="

    #@fd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    .line 1789
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@105
    .line 1791
    :cond_105
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@107
    cmpl-float v1, v1, v4

    #@109
    if-eqz v1, :cond_115

    #@10b
    .line 1792
    const-string v1, " bbrt="

    #@10d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    .line 1793
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@112
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@115
    .line 1795
    :cond_115
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@117
    const/high16 v2, 0x2000

    #@119
    and-int/2addr v1, v2

    #@11a
    if-eqz v1, :cond_121

    #@11c
    .line 1796
    const-string v1, " compatible=true"

    #@11e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    .line 1798
    :cond_121
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@123
    if-eqz v1, :cond_133

    #@125
    .line 1799
    const-string v1, " sysui=0x"

    #@127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    .line 1800
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@12c
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12f
    move-result-object v1

    #@130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    .line 1802
    :cond_133
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@135
    if-eqz v1, :cond_145

    #@137
    .line 1803
    const-string v1, " vsysui=0x"

    #@139
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    .line 1804
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@13e
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@141
    move-result-object v1

    #@142
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    .line 1806
    :cond_145
    iget-boolean v1, p0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@147
    if-eqz v1, :cond_153

    #@149
    .line 1807
    const-string v1, " sysuil="

    #@14b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    .line 1808
    iget-boolean v1, p0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@150
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@153
    .line 1810
    :cond_153
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@155
    if-eqz v1, :cond_166

    #@157
    .line 1811
    const-string v1, " if=0x"

    #@159
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v1

    #@15d
    iget v2, p0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@15f
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@162
    move-result-object v2

    #@163
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    .line 1813
    :cond_166
    iget-wide v1, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@168
    const-wide/16 v3, 0x0

    #@16a
    cmp-long v1, v1, v3

    #@16c
    if-ltz v1, :cond_179

    #@16e
    .line 1814
    const-string v1, " userActivityTimeout="

    #@170
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v1

    #@174
    iget-wide v2, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@176
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@179
    .line 1817
    :cond_179
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@17b
    if-eqz v1, :cond_18b

    #@17d
    .line 1818
    const-string v1, " extend="

    #@17f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    .line 1819
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@184
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@187
    move-result-object v1

    #@188
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    .line 1825
    :cond_18b
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@18d
    if-eqz v1, :cond_19d

    #@18f
    .line 1826
    const-string v1, " extUsage="

    #@191
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    .line 1827
    iget v1, p0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@196
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@199
    move-result-object v1

    #@19a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    .line 1832
    :cond_19d
    const/16 v1, 0x7d

    #@19f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a2
    .line 1833
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v1

    #@1a6
    return-object v1

    #@1a7
    .line 1744
    :cond_1a7
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1a9
    if-ne v1, v5, :cond_1b0

    #@1ab
    const-string/jumbo v1, "wrap"

    #@1ae
    goto/16 :goto_30

    #@1b0
    :cond_1b0
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1b2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b5
    move-result-object v1

    #@1b6
    goto/16 :goto_30

    #@1b8
    .line 1746
    :cond_1b8
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1ba
    if-ne v1, v5, :cond_1c1

    #@1bc
    const-string/jumbo v1, "wrap"

    #@1bf
    goto/16 :goto_3e

    #@1c1
    :cond_1c1
    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1c3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c6
    move-result-object v1

    #@1c7
    goto/16 :goto_3e
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 1479
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 1480
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 1481
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 1482
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 1483
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 1484
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 1485
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 1486
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 1487
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 1488
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@32
    .line 1489
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@37
    .line 1490
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3c
    .line 1491
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@3e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 1492
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    #@43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@46
    .line 1493
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    #@48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@4b
    .line 1494
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    #@4d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@50
    .line 1495
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->buttonBrightness:F

    #@52
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@55
    .line 1496
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@57
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@5a
    .line 1497
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@5c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5f
    .line 1498
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->mTitle:Ljava/lang/CharSequence;

    #@61
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@64
    .line 1499
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    #@66
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@69
    .line 1500
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@6b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6e
    .line 1501
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@70
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@73
    .line 1502
    iget-boolean v0, p0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    #@75
    if-eqz v0, :cond_90

    #@77
    const/4 v0, 0x1

    #@78
    :goto_78
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    .line 1503
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@7d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@80
    .line 1504
    iget-wide v0, p0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    #@82
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@85
    .line 1505
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@87
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8a
    .line 1506
    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@8c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8f
    .line 1507
    return-void

    #@90
    .line 1502
    :cond_90
    const/4 v0, 0x0

    #@91
    goto :goto_78
.end method
