.class Landroid/view/View$2;
.super Ljava/lang/Object;
.source "View.java"

# interfaces
.implements Lcom/android/internal/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/View;->findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/internal/util/Predicate",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/View;

.field final synthetic val$id:I


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 6457
    iput-object p1, p0, Landroid/view/View$2;->this$0:Landroid/view/View;

    #@2
    iput p2, p0, Landroid/view/View$2;->val$id:I

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public apply(Landroid/view/View;)Z
    .registers 4
    .parameter "t"

    #@0
    .prologue
    .line 6460
    iget v0, p1, Landroid/view/View;->mNextFocusForwardId:I

    #@2
    iget v1, p0, Landroid/view/View$2;->val$id:I

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 6457
    check-cast p1, Landroid/view/View;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/view/View$2;->apply(Landroid/view/View;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method
