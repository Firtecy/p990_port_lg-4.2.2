.class final enum Landroid/view/VolumePanel$StreamResources;
.super Ljava/lang/Enum;
.source "VolumePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/VolumePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "StreamResources"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/view/VolumePanel$StreamResources;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/view/VolumePanel$StreamResources;

.field public static final enum AlarmStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum BluetoothSCOStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum DMBStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum FMStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum MasterStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum MediaStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum NotificationStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum RemoteStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum RingerStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum SYSTEMStream:Landroid/view/VolumePanel$StreamResources;

.field public static final enum VoiceStream:Landroid/view/VolumePanel$StreamResources;


# instance fields
.field descRes:I

.field iconMuteRes:I

.field iconRes:I

.field show:Z

.field streamType:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    .line 175
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@2
    const-string v1, "BluetoothSCOStream"

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v3, 0x6

    #@6
    const v4, 0x1040425

    #@9
    const v5, 0x2020237

    #@c
    const v6, 0x2020237

    #@f
    const/4 v7, 0x0

    #@10
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@13
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->BluetoothSCOStream:Landroid/view/VolumePanel$StreamResources;

    #@15
    .line 180
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@17
    const-string v1, "RingerStream"

    #@19
    const/4 v2, 0x1

    #@1a
    const/4 v3, 0x2

    #@1b
    const v4, 0x20902f9

    #@1e
    const v5, 0x2020244

    #@21
    const v6, 0x202024a

    #@24
    const/4 v7, 0x0

    #@25
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@28
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->RingerStream:Landroid/view/VolumePanel$StreamResources;

    #@2a
    .line 190
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@2c
    const-string v1, "VoiceStream"

    #@2e
    const/4 v2, 0x2

    #@2f
    const/4 v3, 0x0

    #@30
    const v4, 0x1040427

    #@33
    const v5, 0x2020243

    #@36
    const v6, 0x2020243

    #@39
    const/4 v7, 0x0

    #@3a
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@3d
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->VoiceStream:Landroid/view/VolumePanel$StreamResources;

    #@3f
    .line 195
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@41
    const-string v1, "AlarmStream"

    #@43
    const/4 v2, 0x3

    #@44
    const/4 v3, 0x4

    #@45
    const v4, 0x1040422

    #@48
    const v5, 0x2020233

    #@4b
    const v6, 0x2020235

    #@4e
    const/4 v7, 0x0

    #@4f
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@52
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->AlarmStream:Landroid/view/VolumePanel$StreamResources;

    #@54
    .line 200
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@56
    const-string v1, "MediaStream"

    #@58
    const/4 v2, 0x4

    #@59
    const/4 v3, 0x3

    #@5a
    const v4, 0x20902fe

    #@5d
    const v5, 0x202023e

    #@60
    const v6, 0x2020240

    #@63
    const/4 v7, 0x1

    #@64
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@67
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->MediaStream:Landroid/view/VolumePanel$StreamResources;

    #@69
    .line 211
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@6b
    const-string v1, "DMBStream"

    #@6d
    const/4 v2, 0x5

    #@6e
    const/16 v3, 0xb

    #@70
    const v4, 0x1040428

    #@73
    const v5, 0x2020249

    #@76
    const v6, 0x202024a

    #@79
    const/4 v7, 0x1

    #@7a
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@7d
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->DMBStream:Landroid/view/VolumePanel$StreamResources;

    #@7f
    .line 217
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@81
    const-string v1, "NotificationStream"

    #@83
    const/4 v2, 0x6

    #@84
    const/4 v3, 0x5

    #@85
    const v4, 0x20902fa

    #@88
    const v5, 0x2020241

    #@8b
    const v6, 0x2020242

    #@8e
    const/4 v7, 0x1

    #@8f
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@92
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->NotificationStream:Landroid/view/VolumePanel$StreamResources;

    #@94
    .line 223
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@96
    const-string v1, "FMStream"

    #@98
    const/4 v2, 0x7

    #@99
    const/16 v3, 0xa

    #@9b
    const v4, 0x1040428

    #@9e
    const v5, 0x2020249

    #@a1
    const v6, 0x202024a

    #@a4
    const/4 v7, 0x1

    #@a5
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@a8
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->FMStream:Landroid/view/VolumePanel$StreamResources;

    #@aa
    .line 229
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@ac
    const-string v1, "SYSTEMStream"

    #@ae
    const/16 v2, 0x8

    #@b0
    const/4 v3, 0x1

    #@b1
    const v4, 0x20902fd

    #@b4
    const v5, 0x2020247

    #@b7
    const v6, 0x2020248

    #@ba
    const/4 v7, 0x1

    #@bb
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@be
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->SYSTEMStream:Landroid/view/VolumePanel$StreamResources;

    #@c0
    .line 239
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@c2
    const-string v1, "MasterStream"

    #@c4
    const/16 v2, 0x9

    #@c6
    const/16 v3, -0x64

    #@c8
    const v4, 0x1040428

    #@cb
    const v5, 0x2020249

    #@ce
    const v6, 0x202024a

    #@d1
    const/4 v7, 0x0

    #@d2
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@d5
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->MasterStream:Landroid/view/VolumePanel$StreamResources;

    #@d7
    .line 244
    new-instance v0, Landroid/view/VolumePanel$StreamResources;

    #@d9
    const-string v1, "RemoteStream"

    #@db
    const/16 v2, 0xa

    #@dd
    const/16 v3, -0xc8

    #@df
    const v4, 0x1040428

    #@e2
    const v5, 0x108031f

    #@e5
    const v6, 0x1080313

    #@e8
    const/4 v7, 0x0

    #@e9
    invoke-direct/range {v0 .. v7}, Landroid/view/VolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    #@ec
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->RemoteStream:Landroid/view/VolumePanel$StreamResources;

    #@ee
    .line 174
    const/16 v0, 0xb

    #@f0
    new-array v0, v0, [Landroid/view/VolumePanel$StreamResources;

    #@f2
    const/4 v1, 0x0

    #@f3
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->BluetoothSCOStream:Landroid/view/VolumePanel$StreamResources;

    #@f5
    aput-object v2, v0, v1

    #@f7
    const/4 v1, 0x1

    #@f8
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->RingerStream:Landroid/view/VolumePanel$StreamResources;

    #@fa
    aput-object v2, v0, v1

    #@fc
    const/4 v1, 0x2

    #@fd
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->VoiceStream:Landroid/view/VolumePanel$StreamResources;

    #@ff
    aput-object v2, v0, v1

    #@101
    const/4 v1, 0x3

    #@102
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->AlarmStream:Landroid/view/VolumePanel$StreamResources;

    #@104
    aput-object v2, v0, v1

    #@106
    const/4 v1, 0x4

    #@107
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->MediaStream:Landroid/view/VolumePanel$StreamResources;

    #@109
    aput-object v2, v0, v1

    #@10b
    const/4 v1, 0x5

    #@10c
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->DMBStream:Landroid/view/VolumePanel$StreamResources;

    #@10e
    aput-object v2, v0, v1

    #@110
    const/4 v1, 0x6

    #@111
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->NotificationStream:Landroid/view/VolumePanel$StreamResources;

    #@113
    aput-object v2, v0, v1

    #@115
    const/4 v1, 0x7

    #@116
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->FMStream:Landroid/view/VolumePanel$StreamResources;

    #@118
    aput-object v2, v0, v1

    #@11a
    const/16 v1, 0x8

    #@11c
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->SYSTEMStream:Landroid/view/VolumePanel$StreamResources;

    #@11e
    aput-object v2, v0, v1

    #@120
    const/16 v1, 0x9

    #@122
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->MasterStream:Landroid/view/VolumePanel$StreamResources;

    #@124
    aput-object v2, v0, v1

    #@126
    const/16 v1, 0xa

    #@128
    sget-object v2, Landroid/view/VolumePanel$StreamResources;->RemoteStream:Landroid/view/VolumePanel$StreamResources;

    #@12a
    aput-object v2, v0, v1

    #@12c
    sput-object v0, Landroid/view/VolumePanel$StreamResources;->$VALUES:[Landroid/view/VolumePanel$StreamResources;

    #@12e
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIZ)V
    .registers 8
    .parameter
    .parameter
    .parameter "streamType"
    .parameter "descRes"
    .parameter "iconRes"
    .parameter "iconMuteRes"
    .parameter "show"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIZ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 257
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 258
    iput p3, p0, Landroid/view/VolumePanel$StreamResources;->streamType:I

    #@5
    .line 259
    iput p4, p0, Landroid/view/VolumePanel$StreamResources;->descRes:I

    #@7
    .line 260
    iput p5, p0, Landroid/view/VolumePanel$StreamResources;->iconRes:I

    #@9
    .line 261
    iput p6, p0, Landroid/view/VolumePanel$StreamResources;->iconMuteRes:I

    #@b
    .line 262
    iput-boolean p7, p0, Landroid/view/VolumePanel$StreamResources;->show:Z

    #@d
    .line 263
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/view/VolumePanel$StreamResources;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 174
    const-class v0, Landroid/view/VolumePanel$StreamResources;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/VolumePanel$StreamResources;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/view/VolumePanel$StreamResources;
    .registers 1

    #@0
    .prologue
    .line 174
    sget-object v0, Landroid/view/VolumePanel$StreamResources;->$VALUES:[Landroid/view/VolumePanel$StreamResources;

    #@2
    invoke-virtual {v0}, [Landroid/view/VolumePanel$StreamResources;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/view/VolumePanel$StreamResources;

    #@8
    return-object v0
.end method
