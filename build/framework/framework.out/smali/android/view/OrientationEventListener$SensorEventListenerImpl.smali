.class Landroid/view/OrientationEventListener$SensorEventListenerImpl;
.super Ljava/lang/Object;
.source "OrientationEventListener.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/OrientationEventListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SensorEventListenerImpl"
.end annotation


# static fields
.field private static final _DATA_X:I = 0x0

.field private static final _DATA_Y:I = 0x1

.field private static final _DATA_Z:I = 0x2


# instance fields
.field final synthetic this$0:Landroid/view/OrientationEventListener;


# direct methods
.method constructor <init>(Landroid/view/OrientationEventListener;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 113
    iput-object p1, p0, Landroid/view/OrientationEventListener$SensorEventListenerImpl;->this$0:Landroid/view/OrientationEventListener;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter "sensor"
    .parameter "accuracy"

    #@0
    .prologue
    .line 149
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 15
    .parameter "event"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 119
    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    #@3
    .line 120
    .local v7, values:[F
    const/4 v6, -0x1

    #@4
    .line 121
    .local v6, orientation:I
    const/4 v8, 0x0

    #@5
    aget v8, v7, v8

    #@7
    neg-float v1, v8

    #@8
    .line 122
    .local v1, X:F
    aget v8, v7, v12

    #@a
    neg-float v2, v8

    #@b
    .line 123
    .local v2, Y:F
    const/4 v8, 0x2

    #@c
    aget v8, v7, v8

    #@e
    neg-float v3, v8

    #@f
    .line 124
    .local v3, Z:F
    mul-float v8, v1, v1

    #@11
    mul-float v9, v2, v2

    #@13
    add-float v5, v8, v9

    #@15
    .line 126
    .local v5, magnitude:F
    const/high16 v8, 0x4080

    #@17
    mul-float/2addr v8, v5

    #@18
    mul-float v9, v3, v3

    #@1a
    cmpl-float v8, v8, v9

    #@1c
    if-ltz v8, :cond_3d

    #@1e
    .line 127
    const v0, 0x42652ee1

    #@21
    .line 128
    .local v0, OneEightyOverPi:F
    neg-float v8, v2

    #@22
    float-to-double v8, v8

    #@23
    float-to-double v10, v1

    #@24
    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    #@27
    move-result-wide v8

    #@28
    double-to-float v8, v8

    #@29
    mul-float v4, v8, v0

    #@2b
    .line 129
    .local v4, angle:F
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    #@2e
    move-result v8

    #@2f
    rsub-int/lit8 v6, v8, 0x5a

    #@31
    .line 131
    :goto_31
    const/16 v8, 0x168

    #@33
    if-lt v6, v8, :cond_38

    #@35
    .line 132
    add-int/lit16 v6, v6, -0x168

    #@37
    goto :goto_31

    #@38
    .line 134
    :cond_38
    :goto_38
    if-gez v6, :cond_3d

    #@3a
    .line 135
    add-int/lit16 v6, v6, 0x168

    #@3c
    goto :goto_38

    #@3d
    .line 138
    .end local v0           #OneEightyOverPi:F
    .end local v4           #angle:F
    :cond_3d
    iget-object v8, p0, Landroid/view/OrientationEventListener$SensorEventListenerImpl;->this$0:Landroid/view/OrientationEventListener;

    #@3f
    invoke-static {v8}, Landroid/view/OrientationEventListener;->access$000(Landroid/view/OrientationEventListener;)Landroid/view/OrientationListener;

    #@42
    move-result-object v8

    #@43
    if-eqz v8, :cond_50

    #@45
    .line 139
    iget-object v8, p0, Landroid/view/OrientationEventListener$SensorEventListenerImpl;->this$0:Landroid/view/OrientationEventListener;

    #@47
    invoke-static {v8}, Landroid/view/OrientationEventListener;->access$000(Landroid/view/OrientationEventListener;)Landroid/view/OrientationListener;

    #@4a
    move-result-object v8

    #@4b
    iget-object v9, p1, Landroid/hardware/SensorEvent;->values:[F

    #@4d
    invoke-virtual {v8, v12, v9}, Landroid/view/OrientationListener;->onSensorChanged(I[F)V

    #@50
    .line 141
    :cond_50
    iget-object v8, p0, Landroid/view/OrientationEventListener$SensorEventListenerImpl;->this$0:Landroid/view/OrientationEventListener;

    #@52
    invoke-static {v8}, Landroid/view/OrientationEventListener;->access$100(Landroid/view/OrientationEventListener;)I

    #@55
    move-result v8

    #@56
    if-eq v6, v8, :cond_62

    #@58
    .line 142
    iget-object v8, p0, Landroid/view/OrientationEventListener$SensorEventListenerImpl;->this$0:Landroid/view/OrientationEventListener;

    #@5a
    invoke-static {v8, v6}, Landroid/view/OrientationEventListener;->access$102(Landroid/view/OrientationEventListener;I)I

    #@5d
    .line 143
    iget-object v8, p0, Landroid/view/OrientationEventListener$SensorEventListenerImpl;->this$0:Landroid/view/OrientationEventListener;

    #@5f
    invoke-virtual {v8, v6}, Landroid/view/OrientationEventListener;->onOrientationChanged(I)V

    #@62
    .line 145
    :cond_62
    return-void
.end method
