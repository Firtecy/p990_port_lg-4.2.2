.class Landroid/view/View$ScrollabilityCache;
.super Ljava/lang/Object;
.source "View.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScrollabilityCache"
.end annotation


# static fields
.field public static final FADING:I = 0x2

.field public static final OFF:I = 0x0

.field public static final ON:I = 0x1

.field private static final OPAQUE:[F

.field private static final TRANSPARENT:[F


# instance fields
.field public fadeScrollBars:Z

.field public fadeStartTime:J

.field public fadingEdgeLength:I

.field public host:Landroid/view/View;

.field public interpolatorValues:[F

.field private mLastColor:I

.field public final matrix:Landroid/graphics/Matrix;

.field public final paint:Landroid/graphics/Paint;

.field public scrollBar:Landroid/widget/ScrollBarDrawable;

.field public scrollBarDefaultDelayBeforeFade:I

.field public scrollBarFadeDuration:I

.field public final scrollBarInterpolator:Landroid/graphics/Interpolator;

.field public scrollBarSize:I

.field public shader:Landroid/graphics/Shader;

.field public state:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 18265
    new-array v0, v3, [F

    #@4
    const/high16 v1, 0x437f

    #@6
    aput v1, v0, v2

    #@8
    sput-object v0, Landroid/view/View$ScrollabilityCache;->OPAQUE:[F

    #@a
    .line 18266
    new-array v0, v3, [F

    #@c
    const/4 v1, 0x0

    #@d
    aput v1, v0, v2

    #@f
    sput-object v0, Landroid/view/View$ScrollabilityCache;->TRANSPARENT:[F

    #@11
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewConfiguration;Landroid/view/View;)V
    .registers 11
    .parameter "configuration"
    .parameter "host"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 18282
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 18263
    new-instance v0, Landroid/graphics/Interpolator;

    #@7
    const/4 v2, 0x1

    #@8
    const/4 v3, 0x2

    #@9
    invoke-direct {v0, v2, v3}, Landroid/graphics/Interpolator;-><init>(II)V

    #@c
    iput-object v0, p0, Landroid/view/View$ScrollabilityCache;->scrollBarInterpolator:Landroid/graphics/Interpolator;

    #@e
    .line 18278
    iput v6, p0, Landroid/view/View$ScrollabilityCache;->state:I

    #@10
    .line 18283
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledFadingEdgeLength()I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/view/View$ScrollabilityCache;->fadingEdgeLength:I

    #@16
    .line 18284
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledScrollBarSize()I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/view/View$ScrollabilityCache;->scrollBarSize:I

    #@1c
    .line 18285
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollDefaultDelay()I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Landroid/view/View$ScrollabilityCache;->scrollBarDefaultDelayBeforeFade:I

    #@22
    .line 18286
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollBarFadeDuration()I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/view/View$ScrollabilityCache;->scrollBarFadeDuration:I

    #@28
    .line 18288
    new-instance v0, Landroid/graphics/Paint;

    #@2a
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@2d
    iput-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@2f
    .line 18289
    new-instance v0, Landroid/graphics/Matrix;

    #@31
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@34
    iput-object v0, p0, Landroid/view/View$ScrollabilityCache;->matrix:Landroid/graphics/Matrix;

    #@36
    .line 18292
    new-instance v0, Landroid/graphics/LinearGradient;

    #@38
    const/high16 v4, 0x3f80

    #@3a
    const/high16 v5, -0x100

    #@3c
    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@3e
    move v2, v1

    #@3f
    move v3, v1

    #@40
    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    #@43
    iput-object v0, p0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@45
    .line 18293
    iget-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@47
    iget-object v1, p0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@49
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@4c
    .line 18294
    iget-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@4e
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    #@50
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    #@52
    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@55
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@58
    .line 18296
    iput-object p2, p0, Landroid/view/View$ScrollabilityCache;->host:Landroid/view/View;

    #@5a
    .line 18297
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    .line 18318
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v4

    #@4
    .line 18319
    .local v4, now:J
    iget-wide v6, p0, Landroid/view/View$ScrollabilityCache;->fadeStartTime:J

    #@6
    cmp-long v6, v4, v6

    #@8
    if-ltz v6, :cond_26

    #@a
    .line 18324
    long-to-int v3, v4

    #@b
    .line 18325
    .local v3, nextFrame:I
    const/4 v0, 0x0

    #@c
    .line 18327
    .local v0, framesCount:I
    iget-object v2, p0, Landroid/view/View$ScrollabilityCache;->scrollBarInterpolator:Landroid/graphics/Interpolator;

    #@e
    .line 18330
    .local v2, interpolator:Landroid/graphics/Interpolator;
    add-int/lit8 v1, v0, 0x1

    #@10
    .end local v0           #framesCount:I
    .local v1, framesCount:I
    sget-object v6, Landroid/view/View$ScrollabilityCache;->OPAQUE:[F

    #@12
    invoke-virtual {v2, v0, v3, v6}, Landroid/graphics/Interpolator;->setKeyFrame(II[F)V

    #@15
    .line 18333
    iget v6, p0, Landroid/view/View$ScrollabilityCache;->scrollBarFadeDuration:I

    #@17
    add-int/2addr v3, v6

    #@18
    .line 18334
    sget-object v6, Landroid/view/View$ScrollabilityCache;->TRANSPARENT:[F

    #@1a
    invoke-virtual {v2, v1, v3, v6}, Landroid/graphics/Interpolator;->setKeyFrame(II[F)V

    #@1d
    .line 18336
    const/4 v6, 0x2

    #@1e
    iput v6, p0, Landroid/view/View$ScrollabilityCache;->state:I

    #@20
    .line 18339
    iget-object v6, p0, Landroid/view/View$ScrollabilityCache;->host:Landroid/view/View;

    #@22
    const/4 v7, 0x1

    #@23
    invoke-virtual {v6, v7}, Landroid/view/View;->invalidate(Z)V

    #@26
    .line 18341
    .end local v1           #framesCount:I
    .end local v2           #interpolator:Landroid/graphics/Interpolator;
    .end local v3           #nextFrame:I
    :cond_26
    return-void
.end method

.method public setFadeColor(I)V
    .registers 10
    .parameter "color"

    #@0
    .prologue
    const/high16 v5, -0x100

    #@2
    const/high16 v4, 0x3f80

    #@4
    const/4 v1, 0x0

    #@5
    .line 18300
    iget v0, p0, Landroid/view/View$ScrollabilityCache;->mLastColor:I

    #@7
    if-eq p1, v0, :cond_2b

    #@9
    .line 18301
    iput p1, p0, Landroid/view/View$ScrollabilityCache;->mLastColor:I

    #@b
    .line 18303
    if-eqz p1, :cond_2c

    #@d
    .line 18304
    new-instance v0, Landroid/graphics/LinearGradient;

    #@f
    or-int/2addr v5, p1

    #@10
    const v2, 0xffffff

    #@13
    and-int v6, p1, v2

    #@15
    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@17
    move v2, v1

    #@18
    move v3, v1

    #@19
    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    #@1c
    iput-object v0, p0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@1e
    .line 18306
    iget-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@20
    iget-object v1, p0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@22
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@25
    .line 18308
    iget-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@27
    const/4 v1, 0x0

    #@28
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@2b
    .line 18315
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 18310
    :cond_2c
    new-instance v0, Landroid/graphics/LinearGradient;

    #@2e
    const/4 v6, 0x0

    #@2f
    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@31
    move v2, v1

    #@32
    move v3, v1

    #@33
    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    #@36
    iput-object v0, p0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@38
    .line 18311
    iget-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@3a
    iget-object v1, p0, Landroid/view/View$ScrollabilityCache;->shader:Landroid/graphics/Shader;

    #@3c
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@3f
    .line 18312
    iget-object v0, p0, Landroid/view/View$ScrollabilityCache;->paint:Landroid/graphics/Paint;

    #@41
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    #@43
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    #@45
    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@48
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@4b
    goto :goto_2b
.end method
