.class Landroid/view/ViewRootImpl$W;
.super Landroid/view/IWindow$Stub;
.source "ViewRootImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "W"
.end annotation


# instance fields
.field private final mViewAncestor:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewRootImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowSession:Landroid/view/IWindowSession;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .registers 3
    .parameter "viewAncestor"

    #@0
    .prologue
    .line 5045
    invoke-direct {p0}, Landroid/view/IWindow$Stub;-><init>()V

    #@3
    .line 5046
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@a
    .line 5047
    iget-object v0, p1, Landroid/view/ViewRootImpl;->mWindowSession:Landroid/view/IWindowSession;

    #@c
    iput-object v0, p0, Landroid/view/ViewRootImpl$W;->mWindowSession:Landroid/view/IWindowSession;

    #@e
    .line 5048
    return-void
.end method

.method private static checkCallingPermission(Ljava/lang/String;)I
    .registers 5
    .parameter "permission"

    #@0
    .prologue
    .line 5097
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v2

    #@8
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@b
    move-result v3

    #@c
    invoke-interface {v1, p0, v2, v3}, Landroid/app/IActivityManager;->checkPermission(Ljava/lang/String;II)I
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_11

    #@f
    move-result v1

    #@10
    .line 5100
    :goto_10
    return v1

    #@11
    .line 5099
    :catch_11
    move-exception v0

    #@12
    .line 5100
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@13
    goto :goto_10
.end method


# virtual methods
.method public closeSystemDialogs(Ljava/lang/String;)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 5153
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5154
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5155
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl;->dispatchCloseSystemDialogs(Ljava/lang/String;)V

    #@d
    .line 5157
    :cond_d
    return-void
.end method

.method public dispatchAppVisibility(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 5068
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5069
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5070
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl;->dispatchAppVisibility(Z)V

    #@d
    .line 5072
    :cond_d
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 5181
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5182
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5183
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl;->dispatchDragEvent(Landroid/view/DragEvent;)V

    #@d
    .line 5185
    :cond_d
    return-void
.end method

.method public dispatchGetNewSurface()V
    .registers 3

    #@0
    .prologue
    .line 5082
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5083
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5084
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->dispatchGetNewSurface()V

    #@d
    .line 5086
    :cond_d
    return-void
.end method

.method public dispatchScreenState(Z)V
    .registers 4
    .parameter "on"

    #@0
    .prologue
    .line 5075
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5076
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5077
    invoke-virtual {v0, p1}, Landroid/view/ViewRootImpl;->dispatchScreenStateChange(Z)V

    #@d
    .line 5079
    :cond_d
    return-void
.end method

.method public dispatchSystemUiVisibilityChanged(IIII)V
    .registers 7
    .parameter "seq"
    .parameter "globalVisibility"
    .parameter "localValue"
    .parameter "localChanges"

    #@0
    .prologue
    .line 5189
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5190
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5191
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/ViewRootImpl;->dispatchSystemUiVisibilityChanged(IIII)V

    #@d
    .line 5194
    :cond_d
    return-void
.end method

.method public dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)V
    .registers 10
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"
    .parameter "sync"

    #@0
    .prologue
    .line 5171
    if-eqz p6, :cond_c

    #@2
    .line 5173
    :try_start_2
    iget-object v0, p0, Landroid/view/ViewRootImpl$W;->mWindowSession:Landroid/view/IWindowSession;

    #@4
    invoke-virtual {p0}, Landroid/view/ViewRootImpl$W;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    const/4 v2, 0x0

    #@9
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowSession;->wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_c} :catch_d

    #@c
    .line 5177
    :cond_c
    :goto_c
    return-void

    #@d
    .line 5174
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method public dispatchWallpaperOffsets(FFFFZ)V
    .registers 8
    .parameter "x"
    .parameter "y"
    .parameter "xStep"
    .parameter "yStep"
    .parameter "sync"

    #@0
    .prologue
    .line 5161
    if-eqz p5, :cond_b

    #@2
    .line 5163
    :try_start_2
    iget-object v0, p0, Landroid/view/ViewRootImpl$W;->mWindowSession:Landroid/view/IWindowSession;

    #@4
    invoke-virtual {p0}, Landroid/view/ViewRootImpl$W;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Landroid/view/IWindowSession;->wallpaperOffsetsComplete(Landroid/os/IBinder;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_b} :catch_c

    #@b
    .line 5167
    :cond_b
    :goto_b
    return-void

    #@c
    .line 5164
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public doneAnimating()V
    .registers 3

    #@0
    .prologue
    .line 5197
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5198
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5199
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->dispatchDoneAnimating()V

    #@d
    .line 5201
    :cond_d
    return-void
.end method

.method public executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .registers 12
    .parameter "command"
    .parameter "parameters"
    .parameter "out"

    #@0
    .prologue
    .line 5105
    iget-object v5, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v4

    #@6
    check-cast v4, Landroid/view/ViewRootImpl;

    #@8
    .line 5106
    .local v4, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v4, :cond_be

    #@a
    .line 5107
    iget-object v3, v4, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    #@c
    .line 5108
    .local v3, view:Landroid/view/View;
    if-eqz v3, :cond_be

    #@e
    .line 5109
    const-string v5, "android.permission.DUMP"

    #@10
    invoke-static {v5}, Landroid/view/ViewRootImpl$W;->checkCallingPermission(Ljava/lang/String;)I

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_41

    #@16
    .line 5111
    new-instance v5, Ljava/lang/SecurityException;

    #@18
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v7, "Insufficient permissions to invoke executeCommand() from pid="

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@26
    move-result v7

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, ", uid="

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@34
    move-result v7

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@40
    throw v5

    #@41
    .line 5116
    :cond_41
    const/4 v0, 0x0

    #@42
    .line 5119
    .local v0, clientStream:Ljava/io/OutputStream;
    :try_start_42
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_ART:Z

    #@44
    if-eqz v5, :cond_bf

    #@46
    const-string v5, "OFE_CMD"

    #@48
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v5

    #@4c
    if-eqz v5, :cond_bf

    #@4e
    .line 5121
    const-string v5, "ViewRootImpl"

    #@50
    new-instance v6, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v7, "executeCommand() - command: "

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    const-string v7, ", parameters: "

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 5122
    new-instance v1, Ljava/io/BufferedOutputStream;

    #@72
    new-instance v5, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    #@74
    invoke-direct {v5, p3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@77
    invoke-direct {v1, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_7a
    .catchall {:try_start_42 .. :try_end_7a} :catchall_dd
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_7a} :catch_ce

    #@7a
    .line 5124
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .local v1, clientStream:Ljava/io/OutputStream;
    :try_start_7a
    invoke-static {}, Landroid/view/ViewRootImpl;->access$900()Landroid/view/ViewRootImpl$IObjectFinderEngine;

    #@7d
    move-result-object v5

    #@7e
    if-eqz v5, :cond_b5

    #@80
    invoke-static {}, Landroid/view/ViewRootImpl;->access$900()Landroid/view/ViewRootImpl$IObjectFinderEngine;

    #@83
    move-result-object v5

    #@84
    invoke-interface {v5, v3, p2, v1}, Landroid/view/ViewRootImpl$IObjectFinderEngine;->execute(Landroid/view/View;Ljava/lang/String;Ljava/io/OutputStream;)Z

    #@87
    move-result v5

    #@88
    if-nez v5, :cond_b5

    #@8a
    .line 5126
    const-string v5, "ViewRootImpl"

    #@8c
    new-instance v6, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v7, "if statement - executeCommand() - command: "

    #@93
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v6

    #@97
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    const-string v7, ", parameters: "

    #@9d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v6

    #@a5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v6

    #@a9
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 5128
    invoke-static {}, Landroid/view/ViewRootImpl;->access$900()Landroid/view/ViewRootImpl$IObjectFinderEngine;

    #@af
    move-result-object v5

    #@b0
    const-string v6, "INVALID"

    #@b2
    invoke-interface {v5, v6, v1}, Landroid/view/ViewRootImpl$IObjectFinderEngine;->sendString(Ljava/lang/String;Ljava/io/OutputStream;)V

    #@b5
    .line 5130
    :cond_b5
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_b8
    .catchall {:try_start_7a .. :try_end_b8} :catchall_e9
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_b8} :catch_ec

    #@b8
    move-object v0, v1

    #@b9
    .line 5140
    .end local v1           #clientStream:Ljava/io/OutputStream;
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    :goto_b9
    if-eqz v0, :cond_be

    #@bb
    .line 5142
    :try_start_bb
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_c9

    #@be
    .line 5150
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .end local v3           #view:Landroid/view/View;
    :cond_be
    :goto_be
    return-void

    #@bf
    .line 5133
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    .restart local v3       #view:Landroid/view/View;
    :cond_bf
    :try_start_bf
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    #@c1
    invoke-direct {v1, p3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_c4
    .catchall {:try_start_bf .. :try_end_c4} :catchall_dd
    .catch Ljava/io/IOException; {:try_start_bf .. :try_end_c4} :catch_ce

    #@c4
    .line 5134
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .restart local v1       #clientStream:Ljava/io/OutputStream;
    :try_start_c4
    invoke-static {v3, p1, p2, v1}, Landroid/view/ViewDebug;->dispatchCommand(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_c7
    .catchall {:try_start_c4 .. :try_end_c7} :catchall_e9
    .catch Ljava/io/IOException; {:try_start_c4 .. :try_end_c7} :catch_ec

    #@c7
    move-object v0, v1

    #@c8
    .end local v1           #clientStream:Ljava/io/OutputStream;
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    goto :goto_b9

    #@c9
    .line 5143
    :catch_c9
    move-exception v2

    #@ca
    .line 5144
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@cd
    goto :goto_be

    #@ce
    .line 5137
    .end local v2           #e:Ljava/io/IOException;
    :catch_ce
    move-exception v2

    #@cf
    .line 5138
    .restart local v2       #e:Ljava/io/IOException;
    :goto_cf
    :try_start_cf
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d2
    .catchall {:try_start_cf .. :try_end_d2} :catchall_dd

    #@d2
    .line 5140
    if-eqz v0, :cond_be

    #@d4
    .line 5142
    :try_start_d4
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_d7
    .catch Ljava/io/IOException; {:try_start_d4 .. :try_end_d7} :catch_d8

    #@d7
    goto :goto_be

    #@d8
    .line 5143
    :catch_d8
    move-exception v2

    #@d9
    .line 5144
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@dc
    goto :goto_be

    #@dd
    .line 5140
    .end local v2           #e:Ljava/io/IOException;
    :catchall_dd
    move-exception v5

    #@de
    :goto_de
    if-eqz v0, :cond_e3

    #@e0
    .line 5142
    :try_start_e0
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_e3
    .catch Ljava/io/IOException; {:try_start_e0 .. :try_end_e3} :catch_e4

    #@e3
    .line 5145
    :cond_e3
    :goto_e3
    throw v5

    #@e4
    .line 5143
    :catch_e4
    move-exception v2

    #@e5
    .line 5144
    .restart local v2       #e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@e8
    goto :goto_e3

    #@e9
    .line 5140
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v1       #clientStream:Ljava/io/OutputStream;
    :catchall_e9
    move-exception v5

    #@ea
    move-object v0, v1

    #@eb
    .end local v1           #clientStream:Ljava/io/OutputStream;
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    goto :goto_de

    #@ec
    .line 5137
    .end local v0           #clientStream:Ljava/io/OutputStream;
    .restart local v1       #clientStream:Ljava/io/OutputStream;
    :catch_ec
    move-exception v2

    #@ed
    move-object v0, v1

    #@ee
    .end local v1           #clientStream:Ljava/io/OutputStream;
    .restart local v0       #clientStream:Ljava/io/OutputStream;
    goto :goto_cf
.end method

.method public moved(II)V
    .registers 5
    .parameter "newX"
    .parameter "newY"

    #@0
    .prologue
    .line 5061
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5062
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5063
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewRootImpl;->dispatchMoved(II)V

    #@d
    .line 5065
    :cond_d
    return-void
.end method

.method public resized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V
    .registers 12
    .parameter "frame"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "reportDraw"
    .parameter "newConfig"

    #@0
    .prologue
    .line 5052
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5053
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_12

    #@a
    move-object v1, p1

    #@b
    move-object v2, p2

    #@c
    move-object v3, p3

    #@d
    move v4, p4

    #@e
    move-object v5, p5

    #@f
    .line 5054
    invoke-virtual/range {v0 .. v5}, Landroid/view/ViewRootImpl;->dispatchResized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V

    #@12
    .line 5057
    :cond_12
    return-void
.end method

.method public windowFocusChanged(ZZ)V
    .registers 5
    .parameter "hasFocus"
    .parameter "inTouchMode"

    #@0
    .prologue
    .line 5089
    iget-object v1, p0, Landroid/view/ViewRootImpl$W;->mViewAncestor:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/ViewRootImpl;

    #@8
    .line 5090
    .local v0, viewAncestor:Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_d

    #@a
    .line 5091
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewRootImpl;->windowFocusChanged(ZZ)V

    #@d
    .line 5093
    :cond_d
    return-void
.end method
