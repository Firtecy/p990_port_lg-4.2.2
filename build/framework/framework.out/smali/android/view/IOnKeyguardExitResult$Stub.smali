.class public abstract Landroid/view/IOnKeyguardExitResult$Stub;
.super Landroid/os/Binder;
.source "IOnKeyguardExitResult.java"

# interfaces
.implements Landroid/view/IOnKeyguardExitResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IOnKeyguardExitResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/IOnKeyguardExitResult$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.IOnKeyguardExitResult"

.field static final TRANSACTION_onKeyguardExitResult:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.view.IOnKeyguardExitResult"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/IOnKeyguardExitResult$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/IOnKeyguardExitResult;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.view.IOnKeyguardExitResult"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/IOnKeyguardExitResult;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/view/IOnKeyguardExitResult;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/view/IOnKeyguardExitResult$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/IOnKeyguardExitResult$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 39
    sparse-switch p1, :sswitch_data_22

    #@4
    .line 55
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 43
    :sswitch_9
    const-string v2, "android.view.IOnKeyguardExitResult"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 48
    :sswitch_f
    const-string v2, "android.view.IOnKeyguardExitResult"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_1f

    #@1a
    move v0, v1

    #@1b
    .line 51
    .local v0, _arg0:Z
    :goto_1b
    invoke-virtual {p0, v0}, Landroid/view/IOnKeyguardExitResult$Stub;->onKeyguardExitResult(Z)V

    #@1e
    goto :goto_8

    #@1f
    .line 50
    .end local v0           #_arg0:Z
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_1b

    #@21
    .line 39
    nop

    #@22
    :sswitch_data_22
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
