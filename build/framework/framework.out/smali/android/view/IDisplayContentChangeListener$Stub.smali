.class public abstract Landroid/view/IDisplayContentChangeListener$Stub;
.super Landroid/os/Binder;
.source "IDisplayContentChangeListener.java"

# interfaces
.implements Landroid/view/IDisplayContentChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/IDisplayContentChangeListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/IDisplayContentChangeListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.view.IDisplayContentChangeListener"

.field static final TRANSACTION_onRectangleOnScreenRequested:I = 0x2

.field static final TRANSACTION_onRotationChanged:I = 0x4

.field static final TRANSACTION_onWindowLayersChanged:I = 0x3

.field static final TRANSACTION_onWindowTransition:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.view.IDisplayContentChangeListener"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/view/IDisplayContentChangeListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/view/IDisplayContentChangeListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.view.IDisplayContentChangeListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/view/IDisplayContentChangeListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/view/IDisplayContentChangeListener;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/view/IDisplayContentChangeListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_70

    #@4
    .line 101
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 47
    :sswitch_9
    const-string v4, "android.view.IDisplayContentChangeListener"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v4, "android.view.IDisplayContentChangeListener"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 56
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 58
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_2e

    #@22
    .line 59
    sget-object v4, Landroid/view/WindowInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Landroid/view/WindowInfo;

    #@2a
    .line 64
    .local v2, _arg2:Landroid/view/WindowInfo;
    :goto_2a
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/IDisplayContentChangeListener$Stub;->onWindowTransition(IILandroid/view/WindowInfo;)V

    #@2d
    goto :goto_8

    #@2e
    .line 62
    .end local v2           #_arg2:Landroid/view/WindowInfo;
    :cond_2e
    const/4 v2, 0x0

    #@2f
    .restart local v2       #_arg2:Landroid/view/WindowInfo;
    goto :goto_2a

    #@30
    .line 69
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Landroid/view/WindowInfo;
    :sswitch_30
    const-string v4, "android.view.IDisplayContentChangeListener"

    #@32
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v0

    #@39
    .line 73
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v4

    #@3d
    if-eqz v4, :cond_52

    #@3f
    .line 74
    sget-object v4, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@41
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@44
    move-result-object v1

    #@45
    check-cast v1, Landroid/graphics/Rect;

    #@47
    .line 80
    .local v1, _arg1:Landroid/graphics/Rect;
    :goto_47
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4a
    move-result v4

    #@4b
    if-eqz v4, :cond_54

    #@4d
    move v2, v3

    #@4e
    .line 81
    .local v2, _arg2:Z
    :goto_4e
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/IDisplayContentChangeListener$Stub;->onRectangleOnScreenRequested(ILandroid/graphics/Rect;Z)V

    #@51
    goto :goto_8

    #@52
    .line 77
    .end local v1           #_arg1:Landroid/graphics/Rect;
    .end local v2           #_arg2:Z
    :cond_52
    const/4 v1, 0x0

    #@53
    .restart local v1       #_arg1:Landroid/graphics/Rect;
    goto :goto_47

    #@54
    .line 80
    :cond_54
    const/4 v2, 0x0

    #@55
    goto :goto_4e

    #@56
    .line 86
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Landroid/graphics/Rect;
    :sswitch_56
    const-string v4, "android.view.IDisplayContentChangeListener"

    #@58
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5e
    move-result v0

    #@5f
    .line 89
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/view/IDisplayContentChangeListener$Stub;->onWindowLayersChanged(I)V

    #@62
    goto :goto_8

    #@63
    .line 94
    .end local v0           #_arg0:I
    :sswitch_63
    const-string v4, "android.view.IDisplayContentChangeListener"

    #@65
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v0

    #@6c
    .line 97
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/view/IDisplayContentChangeListener$Stub;->onRotationChanged(I)V

    #@6f
    goto :goto_8

    #@70
    .line 43
    :sswitch_data_70
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_30
        0x3 -> :sswitch_56
        0x4 -> :sswitch_63
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
