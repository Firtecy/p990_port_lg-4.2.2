.class Landroid/view/MenuInflater$MenuState;
.super Ljava/lang/Object;
.source "MenuInflater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/MenuInflater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenuState"
.end annotation


# static fields
.field private static final defaultGroupId:I = 0x0

.field private static final defaultItemCategory:I = 0x0

.field private static final defaultItemCheckable:I = 0x0

.field private static final defaultItemChecked:Z = false

.field private static final defaultItemEnabled:Z = true

.field private static final defaultItemId:I = 0x0

.field private static final defaultItemOrder:I = 0x0

.field private static final defaultItemVisible:Z = true


# instance fields
.field private groupCategory:I

.field private groupCheckable:I

.field private groupEnabled:Z

.field private groupId:I

.field private groupOrder:I

.field private groupVisible:Z

.field private itemActionProvider:Landroid/view/ActionProvider;

.field private itemActionProviderClassName:Ljava/lang/String;

.field private itemActionViewClassName:Ljava/lang/String;

.field private itemActionViewLayout:I

.field private itemAdded:Z

.field private itemAlphabeticShortcut:C

.field private itemCategoryOrder:I

.field private itemCheckable:I

.field private itemChecked:Z

.field private itemEnabled:Z

.field private itemIconResId:I

.field private itemId:I

.field private itemListenerMethodName:Ljava/lang/String;

.field private itemNumericShortcut:C

.field private itemShowAsAction:I

.field private itemTitle:Ljava/lang/CharSequence;

.field private itemTitleCondensed:Ljava/lang/CharSequence;

.field private itemVisible:Z

.field private menu:Landroid/view/Menu;

.field final synthetic this$0:Landroid/view/MenuInflater;


# direct methods
.method public constructor <init>(Landroid/view/MenuInflater;Landroid/view/Menu;)V
    .registers 3
    .parameter
    .parameter "menu"

    #@0
    .prologue
    .line 304
    iput-object p1, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 305
    iput-object p2, p0, Landroid/view/MenuInflater$MenuState;->menu:Landroid/view/Menu;

    #@7
    .line 307
    invoke-virtual {p0}, Landroid/view/MenuInflater$MenuState;->resetGroup()V

    #@a
    .line 308
    return-void
.end method

.method static synthetic access$000(Landroid/view/MenuInflater$MenuState;)Landroid/view/ActionProvider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/view/MenuInflater$MenuState;->itemActionProvider:Landroid/view/ActionProvider;

    #@2
    return-object v0
.end method

.method private getShortcut(Ljava/lang/String;)C
    .registers 3
    .parameter "shortcutString"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 391
    if-nez p1, :cond_4

    #@3
    .line 394
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@7
    move-result v0

    #@8
    goto :goto_3
.end method

.method private newInstance(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 10
    .parameter "className"
    .parameter
    .parameter "arguments"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 469
    .local p2, constructorSignature:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    :try_start_0
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@2
    invoke-static {v3}, Landroid/view/MenuInflater;->access$100(Landroid/view/MenuInflater;)Landroid/content/Context;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v3, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@d
    move-result-object v0

    #@e
    .line 470
    .local v0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@11
    move-result-object v1

    #@12
    .line 471
    .local v1, constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    invoke-virtual {v1, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_15} :catch_17

    #@15
    move-result-object v3

    #@16
    .line 475
    .end local v0           #clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v1           #constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    :goto_16
    return-object v3

    #@17
    .line 472
    :catch_17
    move-exception v2

    #@18
    .line 473
    .local v2, e:Ljava/lang/Exception;
    const-string v3, "MenuInflater"

    #@1a
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Cannot instantiate class: "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@30
    .line 475
    const/4 v3, 0x0

    #@31
    goto :goto_16
.end method

.method private setItem(Landroid/view/MenuItem;)V
    .registers 9
    .parameter "item"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 399
    iget-boolean v3, p0, Landroid/view/MenuInflater$MenuState;->itemChecked:Z

    #@3
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    #@6
    move-result-object v3

    #@7
    iget-boolean v5, p0, Landroid/view/MenuInflater$MenuState;->itemVisible:Z

    #@9
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    #@c
    move-result-object v3

    #@d
    iget-boolean v5, p0, Landroid/view/MenuInflater$MenuState;->itemEnabled:Z

    #@f
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    #@12
    move-result-object v5

    #@13
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemCheckable:I

    #@15
    if-lt v3, v4, :cond_54

    #@17
    move v3, v4

    #@18
    :goto_18
    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    #@1b
    move-result-object v3

    #@1c
    iget-object v5, p0, Landroid/view/MenuInflater$MenuState;->itemTitleCondensed:Ljava/lang/CharSequence;

    #@1e
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    #@21
    move-result-object v3

    #@22
    iget v5, p0, Landroid/view/MenuInflater$MenuState;->itemIconResId:I

    #@24
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    #@27
    move-result-object v3

    #@28
    iget-char v5, p0, Landroid/view/MenuInflater$MenuState;->itemAlphabeticShortcut:C

    #@2a
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    #@2d
    move-result-object v3

    #@2e
    iget-char v5, p0, Landroid/view/MenuInflater$MenuState;->itemNumericShortcut:C

    #@30
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setNumericShortcut(C)Landroid/view/MenuItem;

    #@33
    .line 408
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemShowAsAction:I

    #@35
    if-ltz v3, :cond_3c

    #@37
    .line 409
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemShowAsAction:I

    #@39
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    #@3c
    .line 412
    :cond_3c
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->itemListenerMethodName:Ljava/lang/String;

    #@3e
    if-eqz v3, :cond_66

    #@40
    .line 413
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@42
    invoke-static {v3}, Landroid/view/MenuInflater;->access$100(Landroid/view/MenuInflater;)Landroid/content/Context;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Landroid/content/Context;->isRestricted()Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_56

    #@4c
    .line 414
    new-instance v3, Ljava/lang/IllegalStateException;

    #@4e
    const-string v4, "The android:onClick attribute cannot be used within a restricted context"

    #@50
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@53
    throw v3

    #@54
    .line 399
    :cond_54
    const/4 v3, 0x0

    #@55
    goto :goto_18

    #@56
    .line 417
    :cond_56
    new-instance v3, Landroid/view/MenuInflater$InflatedOnMenuItemClickListener;

    #@58
    iget-object v5, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@5a
    invoke-static {v5}, Landroid/view/MenuInflater;->access$400(Landroid/view/MenuInflater;)Ljava/lang/Object;

    #@5d
    move-result-object v5

    #@5e
    iget-object v6, p0, Landroid/view/MenuInflater$MenuState;->itemListenerMethodName:Ljava/lang/String;

    #@60
    invoke-direct {v3, v5, v6}, Landroid/view/MenuInflater$InflatedOnMenuItemClickListener;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    #@63
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    #@66
    .line 421
    :cond_66
    instance-of v3, p1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@68
    if-eqz v3, :cond_75

    #@6a
    move-object v2, p1

    #@6b
    .line 422
    check-cast v2, Lcom/android/internal/view/menu/MenuItemImpl;

    #@6d
    .line 423
    .local v2, impl:Lcom/android/internal/view/menu/MenuItemImpl;
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemCheckable:I

    #@6f
    const/4 v5, 0x2

    #@70
    if-lt v3, v5, :cond_75

    #@72
    .line 424
    invoke-virtual {v2, v4}, Lcom/android/internal/view/menu/MenuItemImpl;->setExclusiveCheckable(Z)V

    #@75
    .line 428
    .end local v2           #impl:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_75
    const/4 v1, 0x0

    #@76
    .line 429
    .local v1, actionViewSpecified:Z
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewClassName:Ljava/lang/String;

    #@78
    if-eqz v3, :cond_90

    #@7a
    .line 430
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewClassName:Ljava/lang/String;

    #@7c
    invoke-static {}, Landroid/view/MenuInflater;->access$500()[Ljava/lang/Class;

    #@7f
    move-result-object v4

    #@80
    iget-object v5, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@82
    invoke-static {v5}, Landroid/view/MenuInflater;->access$600(Landroid/view/MenuInflater;)[Ljava/lang/Object;

    #@85
    move-result-object v5

    #@86
    invoke-direct {p0, v3, v4, v5}, Landroid/view/MenuInflater$MenuState;->newInstance(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    move-result-object v0

    #@8a
    check-cast v0, Landroid/view/View;

    #@8c
    .line 432
    .local v0, actionView:Landroid/view/View;
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    #@8f
    .line 433
    const/4 v1, 0x1

    #@90
    .line 435
    .end local v0           #actionView:Landroid/view/View;
    :cond_90
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewLayout:I

    #@92
    if-lez v3, :cond_9c

    #@94
    .line 436
    if-nez v1, :cond_a6

    #@96
    .line 437
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewLayout:I

    #@98
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    #@9b
    .line 438
    const/4 v1, 0x1

    #@9c
    .line 444
    :cond_9c
    :goto_9c
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->itemActionProvider:Landroid/view/ActionProvider;

    #@9e
    if-eqz v3, :cond_a5

    #@a0
    .line 445
    iget-object v3, p0, Landroid/view/MenuInflater$MenuState;->itemActionProvider:Landroid/view/ActionProvider;

    #@a2
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;

    #@a5
    .line 447
    :cond_a5
    return-void

    #@a6
    .line 440
    :cond_a6
    const-string v3, "MenuInflater"

    #@a8
    const-string v4, "Ignoring attribute \'itemActionViewLayout\'. Action view already specified."

    #@aa
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto :goto_9c
.end method


# virtual methods
.method public addItem()V
    .registers 6

    #@0
    .prologue
    .line 450
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/view/MenuInflater$MenuState;->itemAdded:Z

    #@3
    .line 451
    iget-object v0, p0, Landroid/view/MenuInflater$MenuState;->menu:Landroid/view/Menu;

    #@5
    iget v1, p0, Landroid/view/MenuInflater$MenuState;->groupId:I

    #@7
    iget v2, p0, Landroid/view/MenuInflater$MenuState;->itemId:I

    #@9
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemCategoryOrder:I

    #@b
    iget-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemTitle:Ljava/lang/CharSequence;

    #@d
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@10
    move-result-object v0

    #@11
    invoke-direct {p0, v0}, Landroid/view/MenuInflater$MenuState;->setItem(Landroid/view/MenuItem;)V

    #@14
    .line 452
    return-void
.end method

.method public addSubMenuItem()Landroid/view/SubMenu;
    .registers 7

    #@0
    .prologue
    .line 455
    const/4 v1, 0x1

    #@1
    iput-boolean v1, p0, Landroid/view/MenuInflater$MenuState;->itemAdded:Z

    #@3
    .line 456
    iget-object v1, p0, Landroid/view/MenuInflater$MenuState;->menu:Landroid/view/Menu;

    #@5
    iget v2, p0, Landroid/view/MenuInflater$MenuState;->groupId:I

    #@7
    iget v3, p0, Landroid/view/MenuInflater$MenuState;->itemId:I

    #@9
    iget v4, p0, Landroid/view/MenuInflater$MenuState;->itemCategoryOrder:I

    #@b
    iget-object v5, p0, Landroid/view/MenuInflater$MenuState;->itemTitle:Ljava/lang/CharSequence;

    #@d
    invoke-interface {v1, v2, v3, v4, v5}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    #@10
    move-result-object v0

    #@11
    .line 457
    .local v0, subMenu:Landroid/view/SubMenu;
    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    #@14
    move-result-object v1

    #@15
    invoke-direct {p0, v1}, Landroid/view/MenuInflater$MenuState;->setItem(Landroid/view/MenuItem;)V

    #@18
    .line 458
    return-object v0
.end method

.method public hasAddedItem()Z
    .registers 2

    #@0
    .prologue
    .line 462
    iget-boolean v0, p0, Landroid/view/MenuInflater$MenuState;->itemAdded:Z

    #@2
    return v0
.end method

.method public readGroup(Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 323
    iget-object v1, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@4
    invoke-static {v1}, Landroid/view/MenuInflater;->access$100(Landroid/view/MenuInflater;)Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    sget-object v2, Lcom/android/internal/R$styleable;->MenuGroup:[I

    #@a
    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@d
    move-result-object v0

    #@e
    .line 326
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@11
    move-result v1

    #@12
    iput v1, p0, Landroid/view/MenuInflater$MenuState;->groupId:I

    #@14
    .line 327
    const/4 v1, 0x3

    #@15
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@18
    move-result v1

    #@19
    iput v1, p0, Landroid/view/MenuInflater$MenuState;->groupCategory:I

    #@1b
    .line 328
    const/4 v1, 0x4

    #@1c
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1f
    move-result v1

    #@20
    iput v1, p0, Landroid/view/MenuInflater$MenuState;->groupOrder:I

    #@22
    .line 329
    const/4 v1, 0x5

    #@23
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@26
    move-result v1

    #@27
    iput v1, p0, Landroid/view/MenuInflater$MenuState;->groupCheckable:I

    #@29
    .line 330
    const/4 v1, 0x2

    #@2a
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2d
    move-result v1

    #@2e
    iput-boolean v1, p0, Landroid/view/MenuInflater$MenuState;->groupVisible:Z

    #@30
    .line 331
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@33
    move-result v1

    #@34
    iput-boolean v1, p0, Landroid/view/MenuInflater$MenuState;->groupEnabled:Z

    #@36
    .line 333
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@39
    .line 334
    return-void
.end method

.method public readItem(Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "attrs"

    #@0
    .prologue
    const/16 v8, 0xb

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 340
    iget-object v4, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@6
    invoke-static {v4}, Landroid/view/MenuInflater;->access$100(Landroid/view/MenuInflater;)Landroid/content/Context;

    #@9
    move-result-object v4

    #@a
    sget-object v7, Lcom/android/internal/R$styleable;->MenuItem:[I

    #@c
    invoke-virtual {v4, p1, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@f
    move-result-object v0

    #@10
    .line 344
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x2

    #@11
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@14
    move-result v4

    #@15
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemId:I

    #@17
    .line 345
    const/4 v4, 0x5

    #@18
    iget v7, p0, Landroid/view/MenuInflater$MenuState;->groupCategory:I

    #@1a
    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1d
    move-result v1

    #@1e
    .line 346
    .local v1, category:I
    const/4 v4, 0x6

    #@1f
    iget v7, p0, Landroid/view/MenuInflater$MenuState;->groupOrder:I

    #@21
    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@24
    move-result v3

    #@25
    .line 347
    .local v3, order:I
    const/high16 v4, -0x1

    #@27
    and-int/2addr v4, v1

    #@28
    const v7, 0xffff

    #@2b
    and-int/2addr v7, v3

    #@2c
    or-int/2addr v4, v7

    #@2d
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemCategoryOrder:I

    #@2f
    .line 348
    const/4 v4, 0x7

    #@30
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@33
    move-result-object v4

    #@34
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemTitle:Ljava/lang/CharSequence;

    #@36
    .line 349
    const/16 v4, 0x8

    #@38
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@3b
    move-result-object v4

    #@3c
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemTitleCondensed:Ljava/lang/CharSequence;

    #@3e
    .line 350
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@41
    move-result v4

    #@42
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemIconResId:I

    #@44
    .line 351
    const/16 v4, 0x9

    #@46
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-direct {p0, v4}, Landroid/view/MenuInflater$MenuState;->getShortcut(Ljava/lang/String;)C

    #@4d
    move-result v4

    #@4e
    iput-char v4, p0, Landroid/view/MenuInflater$MenuState;->itemAlphabeticShortcut:C

    #@50
    .line 353
    const/16 v4, 0xa

    #@52
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    invoke-direct {p0, v4}, Landroid/view/MenuInflater$MenuState;->getShortcut(Ljava/lang/String;)C

    #@59
    move-result v4

    #@5a
    iput-char v4, p0, Landroid/view/MenuInflater$MenuState;->itemNumericShortcut:C

    #@5c
    .line 355
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_d7

    #@62
    .line 357
    invoke-virtual {v0, v8, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@65
    move-result v4

    #@66
    if-eqz v4, :cond_d5

    #@68
    move v4, v5

    #@69
    :goto_69
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemCheckable:I

    #@6b
    .line 363
    :goto_6b
    const/4 v4, 0x3

    #@6c
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@6f
    move-result v4

    #@70
    iput-boolean v4, p0, Landroid/view/MenuInflater$MenuState;->itemChecked:Z

    #@72
    .line 364
    const/4 v4, 0x4

    #@73
    iget-boolean v7, p0, Landroid/view/MenuInflater$MenuState;->groupVisible:Z

    #@75
    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@78
    move-result v4

    #@79
    iput-boolean v4, p0, Landroid/view/MenuInflater$MenuState;->itemVisible:Z

    #@7b
    .line 365
    iget-boolean v4, p0, Landroid/view/MenuInflater$MenuState;->groupEnabled:Z

    #@7d
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@80
    move-result v4

    #@81
    iput-boolean v4, p0, Landroid/view/MenuInflater$MenuState;->itemEnabled:Z

    #@83
    .line 366
    const/16 v4, 0xd

    #@85
    const/4 v7, -0x1

    #@86
    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@89
    move-result v4

    #@8a
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemShowAsAction:I

    #@8c
    .line 367
    const/16 v4, 0xc

    #@8e
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemListenerMethodName:Ljava/lang/String;

    #@94
    .line 368
    const/16 v4, 0xe

    #@96
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@99
    move-result v4

    #@9a
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewLayout:I

    #@9c
    .line 369
    const/16 v4, 0xf

    #@9e
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v4

    #@a2
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewClassName:Ljava/lang/String;

    #@a4
    .line 370
    const/16 v4, 0x10

    #@a6
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionProviderClassName:Ljava/lang/String;

    #@ac
    .line 372
    iget-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionProviderClassName:Ljava/lang/String;

    #@ae
    if-eqz v4, :cond_dc

    #@b0
    move v2, v5

    #@b1
    .line 373
    .local v2, hasActionProvider:Z
    :goto_b1
    if-eqz v2, :cond_de

    #@b3
    iget v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewLayout:I

    #@b5
    if-nez v4, :cond_de

    #@b7
    iget-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionViewClassName:Ljava/lang/String;

    #@b9
    if-nez v4, :cond_de

    #@bb
    .line 374
    iget-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionProviderClassName:Ljava/lang/String;

    #@bd
    invoke-static {}, Landroid/view/MenuInflater;->access$200()[Ljava/lang/Class;

    #@c0
    move-result-object v5

    #@c1
    iget-object v7, p0, Landroid/view/MenuInflater$MenuState;->this$0:Landroid/view/MenuInflater;

    #@c3
    invoke-static {v7}, Landroid/view/MenuInflater;->access$300(Landroid/view/MenuInflater;)[Ljava/lang/Object;

    #@c6
    move-result-object v7

    #@c7
    invoke-direct {p0, v4, v5, v7}, Landroid/view/MenuInflater$MenuState;->newInstance(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    #@ca
    move-result-object v4

    #@cb
    check-cast v4, Landroid/view/ActionProvider;

    #@cd
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionProvider:Landroid/view/ActionProvider;

    #@cf
    .line 385
    :goto_cf
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@d2
    .line 387
    iput-boolean v6, p0, Landroid/view/MenuInflater$MenuState;->itemAdded:Z

    #@d4
    .line 388
    return-void

    #@d5
    .end local v2           #hasActionProvider:Z
    :cond_d5
    move v4, v6

    #@d6
    .line 357
    goto :goto_69

    #@d7
    .line 361
    :cond_d7
    iget v4, p0, Landroid/view/MenuInflater$MenuState;->groupCheckable:I

    #@d9
    iput v4, p0, Landroid/view/MenuInflater$MenuState;->itemCheckable:I

    #@db
    goto :goto_6b

    #@dc
    :cond_dc
    move v2, v6

    #@dd
    .line 372
    goto :goto_b1

    #@de
    .line 378
    .restart local v2       #hasActionProvider:Z
    :cond_de
    if-eqz v2, :cond_e7

    #@e0
    .line 379
    const-string v4, "MenuInflater"

    #@e2
    const-string v5, "Ignoring attribute \'actionProviderClass\'. Action view already specified."

    #@e4
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    .line 382
    :cond_e7
    const/4 v4, 0x0

    #@e8
    iput-object v4, p0, Landroid/view/MenuInflater$MenuState;->itemActionProvider:Landroid/view/ActionProvider;

    #@ea
    goto :goto_cf
.end method

.method public resetGroup()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 311
    iput v0, p0, Landroid/view/MenuInflater$MenuState;->groupId:I

    #@4
    .line 312
    iput v0, p0, Landroid/view/MenuInflater$MenuState;->groupCategory:I

    #@6
    .line 313
    iput v0, p0, Landroid/view/MenuInflater$MenuState;->groupOrder:I

    #@8
    .line 314
    iput v0, p0, Landroid/view/MenuInflater$MenuState;->groupCheckable:I

    #@a
    .line 315
    iput-boolean v1, p0, Landroid/view/MenuInflater$MenuState;->groupVisible:Z

    #@c
    .line 316
    iput-boolean v1, p0, Landroid/view/MenuInflater$MenuState;->groupEnabled:Z

    #@e
    .line 317
    return-void
.end method
