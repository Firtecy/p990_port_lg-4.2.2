.class public abstract Landroid/view/InputEvent;
.super Ljava/lang/Object;
.source "InputEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/view/InputEvent;",
            ">;"
        }
    .end annotation
.end field

.field protected static final PARCEL_TOKEN_KEY_EVENT:I = 0x2

.field protected static final PARCEL_TOKEN_MOTION_EVENT:I = 0x1

.field private static final TRACK_RECYCLED_LOCATION:Z

.field private static final mNextSeq:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected mRecycled:Z

.field private mRecycledLocation:Ljava/lang/RuntimeException;

.field protected mSeq:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@5
    sput-object v0, Landroid/view/InputEvent;->mNextSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    #@7
    .line 206
    new-instance v0, Landroid/view/InputEvent$1;

    #@9
    invoke-direct {v0}, Landroid/view/InputEvent$1;-><init>()V

    #@c
    sput-object v0, Landroid/view/InputEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e
    return-void
.end method

.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    sget-object v0, Landroid/view/InputEvent;->mNextSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/view/InputEvent;->mSeq:I

    #@b
    .line 47
    return-void
.end method


# virtual methods
.method public abstract copy()Landroid/view/InputEvent;
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 203
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final getDevice()Landroid/view/InputDevice;
    .registers 2

    #@0
    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/view/InputEvent;->getDeviceId()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Landroid/view/InputDevice;->getDevice(I)Landroid/view/InputDevice;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public abstract getDeviceId()I
.end method

.method public abstract getEventTime()J
.end method

.method public abstract getEventTimeNano()J
.end method

.method public getSequenceNumber()I
    .registers 2

    #@0
    .prologue
    .line 199
    iget v0, p0, Landroid/view/InputEvent;->mSeq:I

    #@2
    return v0
.end method

.method public abstract getSource()I
.end method

.method public abstract isTainted()Z
.end method

.method protected prepareForReuse()V
    .registers 2

    #@0
    .prologue
    .line 134
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/view/InputEvent;->mRecycled:Z

    #@3
    .line 135
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/view/InputEvent;->mRecycledLocation:Ljava/lang/RuntimeException;

    #@6
    .line 136
    sget-object v0, Landroid/view/InputEvent;->mNextSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/view/InputEvent;->mSeq:I

    #@e
    .line 137
    return-void
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 107
    iget-boolean v0, p0, Landroid/view/InputEvent;->mRecycled:Z

    #@2
    if-eqz v0, :cond_21

    #@4
    .line 108
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " recycled twice!"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 110
    :cond_21
    const/4 v0, 0x1

    #@22
    iput-boolean v0, p0, Landroid/view/InputEvent;->mRecycled:Z

    #@24
    .line 112
    return-void
.end method

.method public recycleIfNeededAfterDispatch()V
    .registers 1

    #@0
    .prologue
    .line 126
    invoke-virtual {p0}, Landroid/view/InputEvent;->recycle()V

    #@3
    .line 127
    return-void
.end method

.method public abstract setSource(I)V
.end method

.method public abstract setTainted(Z)V
.end method
