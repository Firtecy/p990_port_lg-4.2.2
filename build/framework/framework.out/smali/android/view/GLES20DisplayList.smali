.class Landroid/view/GLES20DisplayList;
.super Landroid/view/DisplayList;
.source "GLES20DisplayList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/GLES20DisplayList$DisplayListFinalizer;
    }
.end annotation


# instance fields
.field final mBitmaps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCanvas:Landroid/view/GLES20RecordingCanvas;

.field final mChildDisplayLists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/DisplayList;",
            ">;"
        }
    .end annotation
.end field

.field private mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

.field private final mName:Ljava/lang/String;

.field private mValid:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/view/DisplayList;-><init>()V

    #@3
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    #@5
    const/4 v1, 0x5

    #@6
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    iput-object v0, p0, Landroid/view/GLES20DisplayList;->mBitmaps:Ljava/util/ArrayList;

    #@b
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    #@d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v0, p0, Landroid/view/GLES20DisplayList;->mChildDisplayLists:Ljava/util/ArrayList;

    #@12
    .line 46
    iput-object p1, p0, Landroid/view/GLES20DisplayList;->mName:Ljava/lang/String;

    #@14
    .line 47
    return-void
.end method

.method private static native nOffsetLeftRight(II)V
.end method

.method private static native nOffsetTopBottom(II)V
.end method

.method private static native nReset(I)V
.end method

.method private static native nSetAlpha(IF)V
.end method

.method private static native nSetAnimationMatrix(II)V
.end method

.method private static native nSetBottom(II)V
.end method

.method private static native nSetCaching(IZ)V
.end method

.method private static native nSetCameraDistance(IF)V
.end method

.method private static native nSetClipChildren(IZ)V
.end method

.method private static native nSetHasOverlappingRendering(IZ)V
.end method

.method private static native nSetLeft(II)V
.end method

.method private static native nSetLeftTop(III)V
.end method

.method private static native nSetLeftTopRightBottom(IIIII)V
.end method

.method private static native nSetPivotX(IF)V
.end method

.method private static native nSetPivotY(IF)V
.end method

.method private static native nSetRight(II)V
.end method

.method private static native nSetRotation(IF)V
.end method

.method private static native nSetRotationX(IF)V
.end method

.method private static native nSetRotationY(IF)V
.end method

.method private static native nSetScaleX(IF)V
.end method

.method private static native nSetScaleY(IF)V
.end method

.method private static native nSetStaticMatrix(II)V
.end method

.method private static native nSetTop(II)V
.end method

.method private static native nSetTransformationInfo(IFFFFFFFF)V
.end method

.method private static native nSetTranslationX(IF)V
.end method

.method private static native nSetTranslationY(IF)V
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 83
    iget-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 84
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mBitmaps:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@9
    .line 85
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mChildDisplayLists:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@e
    .line 87
    :cond_e
    return-void
.end method

.method public end()V
    .registers 4

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@2
    if-eqz v0, :cond_1c

    #@4
    .line 104
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@6
    if-eqz v0, :cond_1d

    #@8
    .line 105
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@a
    iget-object v1, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@c
    iget v1, v1, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@e
    invoke-virtual {v0, v1}, Landroid/view/GLES20RecordingCanvas;->end(I)I

    #@11
    .line 110
    :goto_11
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@13
    invoke-virtual {v0}, Landroid/view/GLES20RecordingCanvas;->recycle()V

    #@16
    .line 111
    const/4 v0, 0x0

    #@17
    iput-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@19
    .line 112
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@1c
    .line 114
    :cond_1c
    return-void

    #@1d
    .line 107
    :cond_1d
    new-instance v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@1f
    iget-object v1, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@21
    const/4 v2, 0x0

    #@22
    invoke-virtual {v1, v2}, Landroid/view/GLES20RecordingCanvas;->end(I)I

    #@25
    move-result v1

    #@26
    invoke-direct {v0, v1}, Landroid/view/GLES20DisplayList$DisplayListFinalizer;-><init>(I)V

    #@29
    iput-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@2b
    .line 108
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@2d
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@2f
    iget-object v1, p0, Landroid/view/GLES20DisplayList;->mName:Ljava/lang/String;

    #@31
    invoke-static {v0, v1}, Landroid/view/GLES20Canvas;->setDisplayListName(ILjava/lang/String;)V

    #@34
    goto :goto_11
.end method

.method getNativeDisplayList()I
    .registers 3

    #@0
    .prologue
    .line 54
    iget-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@6
    if-nez v0, :cond_10

    #@8
    .line 55
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    const-string v1, "The display list is not valid."

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 57
    :cond_10
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@12
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@14
    return v0
.end method

.method public getSize()I
    .registers 2

    #@0
    .prologue
    .line 118
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 119
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0}, Landroid/view/GLES20Canvas;->getDisplayListSize(I)I

    #@d
    move-result v0

    #@e
    goto :goto_5
.end method

.method hasNativeDisplayList()Z
    .registers 2

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public invalidate()V
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 75
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@6
    invoke-virtual {v0}, Landroid/view/GLES20RecordingCanvas;->recycle()V

    #@9
    .line 76
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@c
    .line 78
    :cond_c
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@f
    .line 79
    return-void
.end method

.method public isValid()Z
    .registers 2

    #@0
    .prologue
    .line 98
    iget-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@2
    return v0
.end method

.method public offsetLeftRight(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 292
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 293
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nOffsetLeftRight(II)V

    #@d
    .line 295
    :cond_d
    return-void
.end method

.method public offsetTopBottom(I)V
    .registers 3
    .parameter "offset"

    #@0
    .prologue
    .line 299
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 300
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nOffsetTopBottom(II)V

    #@d
    .line 302
    :cond_d
    return-void
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 92
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0}, Landroid/view/GLES20DisplayList;->nReset(I)V

    #@d
    .line 94
    :cond_d
    return-void
.end method

.method public setAlpha(F)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 157
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 158
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetAlpha(IF)V

    #@d
    .line 160
    :cond_d
    return-void
.end method

.method public setAnimationMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 149
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 150
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v1, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    if-eqz p1, :cond_12

    #@c
    iget v0, p1, Landroid/graphics/Matrix;->native_instance:I

    #@e
    :goto_e
    invoke-static {v1, v0}, Landroid/view/GLES20DisplayList;->nSetAnimationMatrix(II)V

    #@11
    .line 153
    :cond_11
    return-void

    #@12
    .line 150
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_e
.end method

.method public setBottom(I)V
    .registers 3
    .parameter "bottom"

    #@0
    .prologue
    .line 271
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 272
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetBottom(II)V

    #@d
    .line 274
    :cond_d
    return-void
.end method

.method public setCaching(Z)V
    .registers 3
    .parameter "caching"

    #@0
    .prologue
    .line 128
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 129
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetCaching(IZ)V

    #@d
    .line 131
    :cond_d
    return-void
.end method

.method public setCameraDistance(F)V
    .registers 3
    .parameter "distance"

    #@0
    .prologue
    .line 243
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 244
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetCameraDistance(IF)V

    #@d
    .line 246
    :cond_d
    return-void
.end method

.method public setClipChildren(Z)V
    .registers 3
    .parameter "clipChildren"

    #@0
    .prologue
    .line 135
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 136
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetClipChildren(IZ)V

    #@d
    .line 138
    :cond_d
    return-void
.end method

.method public setHasOverlappingRendering(Z)V
    .registers 3
    .parameter "hasOverlappingRendering"

    #@0
    .prologue
    .line 164
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 165
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetHasOverlappingRendering(IZ)V

    #@d
    .line 167
    :cond_d
    return-void
.end method

.method public setLeft(I)V
    .registers 3
    .parameter "left"

    #@0
    .prologue
    .line 250
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 251
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetLeft(II)V

    #@d
    .line 253
    :cond_d
    return-void
.end method

.method public setLeftTop(II)V
    .registers 4
    .parameter "left"
    .parameter "top"

    #@0
    .prologue
    .line 278
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 279
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1, p2}, Landroid/view/GLES20DisplayList;->nSetLeftTop(III)V

    #@d
    .line 281
    :cond_d
    return-void
.end method

.method public setLeftTopRightBottom(IIII)V
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 285
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 286
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1, p2, p3, p4}, Landroid/view/GLES20DisplayList;->nSetLeftTopRightBottom(IIIII)V

    #@d
    .line 288
    :cond_d
    return-void
.end method

.method public setPivotX(F)V
    .registers 3
    .parameter "pivotX"

    #@0
    .prologue
    .line 229
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 230
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetPivotX(IF)V

    #@d
    .line 232
    :cond_d
    return-void
.end method

.method public setPivotY(F)V
    .registers 3
    .parameter "pivotY"

    #@0
    .prologue
    .line 236
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 237
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetPivotY(IF)V

    #@d
    .line 239
    :cond_d
    return-void
.end method

.method public setRight(I)V
    .registers 3
    .parameter "right"

    #@0
    .prologue
    .line 264
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 265
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetRight(II)V

    #@d
    .line 267
    :cond_d
    return-void
.end method

.method public setRotation(F)V
    .registers 3
    .parameter "rotation"

    #@0
    .prologue
    .line 185
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 186
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetRotation(IF)V

    #@d
    .line 188
    :cond_d
    return-void
.end method

.method public setRotationX(F)V
    .registers 3
    .parameter "rotationX"

    #@0
    .prologue
    .line 192
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 193
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetRotationX(IF)V

    #@d
    .line 195
    :cond_d
    return-void
.end method

.method public setRotationY(F)V
    .registers 3
    .parameter "rotationY"

    #@0
    .prologue
    .line 199
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 200
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetRotationY(IF)V

    #@d
    .line 202
    :cond_d
    return-void
.end method

.method public setScaleX(F)V
    .registers 3
    .parameter "scaleX"

    #@0
    .prologue
    .line 206
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 207
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetScaleX(IF)V

    #@d
    .line 209
    :cond_d
    return-void
.end method

.method public setScaleY(F)V
    .registers 3
    .parameter "scaleY"

    #@0
    .prologue
    .line 213
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 214
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetScaleY(IF)V

    #@d
    .line 216
    :cond_d
    return-void
.end method

.method public setStaticMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 142
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 143
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@c
    invoke-static {v0, v1}, Landroid/view/GLES20DisplayList;->nSetStaticMatrix(II)V

    #@f
    .line 145
    :cond_f
    return-void
.end method

.method public setTop(I)V
    .registers 3
    .parameter "top"

    #@0
    .prologue
    .line 257
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 258
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetTop(II)V

    #@d
    .line 260
    :cond_d
    return-void
.end method

.method public setTransformationInfo(FFFFFFFF)V
    .registers 18
    .parameter "alpha"
    .parameter "translationX"
    .parameter "translationY"
    .parameter "rotation"
    .parameter "rotationX"
    .parameter "rotationY"
    .parameter "scaleX"
    .parameter "scaleY"

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_17

    #@6
    .line 222
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    move v1, p1

    #@b
    move v2, p2

    #@c
    move v3, p3

    #@d
    move v4, p4

    #@e
    move v5, p5

    #@f
    move v6, p6

    #@10
    move/from16 v7, p7

    #@12
    move/from16 v8, p8

    #@14
    invoke-static/range {v0 .. v8}, Landroid/view/GLES20DisplayList;->nSetTransformationInfo(IFFFFFFFF)V

    #@17
    .line 225
    :cond_17
    return-void
.end method

.method public setTranslationX(F)V
    .registers 3
    .parameter "translationX"

    #@0
    .prologue
    .line 171
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 172
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetTranslationX(IF)V

    #@d
    .line 174
    :cond_d
    return-void
.end method

.method public setTranslationY(F)V
    .registers 3
    .parameter "translationY"

    #@0
    .prologue
    .line 178
    invoke-virtual {p0}, Landroid/view/GLES20DisplayList;->hasNativeDisplayList()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 179
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mFinalizer:Landroid/view/GLES20DisplayList$DisplayListFinalizer;

    #@8
    iget v0, v0, Landroid/view/GLES20DisplayList$DisplayListFinalizer;->mNativeDisplayList:I

    #@a
    invoke-static {v0, p1}, Landroid/view/GLES20DisplayList;->nSetTranslationY(IF)V

    #@d
    .line 181
    :cond_d
    return-void
.end method

.method public start()Landroid/view/HardwareCanvas;
    .registers 3

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 63
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Recording has already started"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 66
    :cond_c
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/view/GLES20DisplayList;->mValid:Z

    #@f
    .line 67
    invoke-static {p0}, Landroid/view/GLES20RecordingCanvas;->obtain(Landroid/view/GLES20DisplayList;)Landroid/view/GLES20RecordingCanvas;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@15
    .line 68
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@17
    invoke-virtual {v0}, Landroid/view/GLES20RecordingCanvas;->start()V

    #@1a
    .line 69
    iget-object v0, p0, Landroid/view/GLES20DisplayList;->mCanvas:Landroid/view/GLES20RecordingCanvas;

    #@1c
    return-object v0
.end method
