.class public Landroid/ddm/DdmHandleHeap;
.super Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;
.source "DdmHandleHeap.java"


# static fields
.field public static final CHUNK_HPDS:I

.field public static final CHUNK_HPDU:I

.field public static final CHUNK_HPGC:I

.field public static final CHUNK_HPIF:I

.field public static final CHUNK_HPSG:I

.field public static final CHUNK_NHSG:I

.field public static final CHUNK_REAE:I

.field public static final CHUNK_REAL:I

.field public static final CHUNK_REAQ:I

.field private static mInstance:Landroid/ddm/DdmHandleHeap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    const-string v0, "HPIF"

    #@2
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPIF:I

    #@8
    .line 34
    const-string v0, "HPSG"

    #@a
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPSG:I

    #@10
    .line 35
    const-string v0, "HPDU"

    #@12
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDU:I

    #@18
    .line 36
    const-string v0, "HPDS"

    #@1a
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@1d
    move-result v0

    #@1e
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDS:I

    #@20
    .line 37
    const-string v0, "NHSG"

    #@22
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@25
    move-result v0

    #@26
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_NHSG:I

    #@28
    .line 38
    const-string v0, "HPGC"

    #@2a
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@2d
    move-result v0

    #@2e
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPGC:I

    #@30
    .line 39
    const-string v0, "REAE"

    #@32
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@35
    move-result v0

    #@36
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_REAE:I

    #@38
    .line 40
    const-string v0, "REAQ"

    #@3a
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_REAQ:I

    #@40
    .line 41
    const-string v0, "REAL"

    #@42
    invoke-static {v0}, Landroid/ddm/DdmHandleHeap;->type(Ljava/lang/String;)I

    #@45
    move-result v0

    #@46
    sput v0, Landroid/ddm/DdmHandleHeap;->CHUNK_REAL:I

    #@48
    .line 43
    new-instance v0, Landroid/ddm/DdmHandleHeap;

    #@4a
    invoke-direct {v0}, Landroid/ddm/DdmHandleHeap;-><init>()V

    #@4d
    sput-object v0, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@4f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;-><init>()V

    #@3
    return-void
.end method

.method private handleHPDS(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 8
    .parameter "request"

    #@0
    .prologue
    .line 190
    invoke-static {p1}, Landroid/ddm/DdmHandleHeap;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v1

    #@4
    .line 197
    .local v1, in:Ljava/nio/ByteBuffer;
    const/4 v0, 0x0

    #@5
    .line 199
    .local v0, failMsg:Ljava/lang/String;
    :try_start_5
    invoke-static {}, Landroid/os/Debug;->dumpHprofDataDdms()V
    :try_end_8
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_8} :catch_15
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_8} :catch_19

    #@8
    .line 206
    :goto_8
    if-eqz v0, :cond_32

    #@a
    .line 207
    const-string v4, "ddm-heap"

    #@c
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 208
    const/4 v4, 0x1

    #@10
    invoke-static {v4, v0}, Landroid/ddm/DdmHandleHeap;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@13
    move-result-object v4

    #@14
    .line 210
    :goto_14
    return-object v4

    #@15
    .line 200
    :catch_15
    move-exception v3

    #@16
    .line 201
    .local v3, uoe:Ljava/lang/UnsupportedOperationException;
    const-string v0, "hprof dumps not supported in this VM"

    #@18
    .line 204
    goto :goto_8

    #@19
    .line 202
    .end local v3           #uoe:Ljava/lang/UnsupportedOperationException;
    :catch_19
    move-exception v2

    #@1a
    .line 203
    .local v2, re:Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v5, "Exception: "

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    goto :goto_8

    #@32
    .line 210
    .end local v2           #re:Ljava/lang/RuntimeException;
    :cond_32
    const/4 v4, 0x0

    #@33
    goto :goto_14
.end method

.method private handleHPDU(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 14
    .parameter "request"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 156
    invoke-static {p1}, Landroid/ddm/DdmHandleHeap;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@4
    move-result-object v1

    #@5
    .line 160
    .local v1, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    #@8
    move-result v3

    #@9
    .line 161
    .local v3, len:I
    invoke-static {v1, v3}, Landroid/ddm/DdmHandleHeap;->getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 166
    .local v0, fileName:Ljava/lang/String;
    :try_start_d
    invoke-static {v0}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_d .. :try_end_10} :catch_1f
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_29
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_10} :catch_2c

    #@10
    .line 167
    const/4 v6, 0x0

    #@11
    .line 178
    .local v6, result:B
    :goto_11
    const/4 v8, 0x1

    #@12
    new-array v5, v8, [B

    #@14
    aput-byte v6, v5, v11

    #@16
    .line 179
    .local v5, reply:[B
    new-instance v8, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@18
    sget v9, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDU:I

    #@1a
    array-length v10, v5

    #@1b
    invoke-direct {v8, v9, v5, v11, v10}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@1e
    return-object v8

    #@1f
    .line 168
    .end local v5           #reply:[B
    .end local v6           #result:B
    :catch_1f
    move-exception v7

    #@20
    .line 169
    .local v7, uoe:Ljava/lang/UnsupportedOperationException;
    const-string v8, "ddm-heap"

    #@22
    const-string v9, "hprof dumps not supported in this VM"

    #@24
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 170
    const/4 v6, -0x1

    #@28
    .line 175
    .restart local v6       #result:B
    goto :goto_11

    #@29
    .line 171
    .end local v6           #result:B
    .end local v7           #uoe:Ljava/lang/UnsupportedOperationException;
    :catch_29
    move-exception v2

    #@2a
    .line 172
    .local v2, ioe:Ljava/io/IOException;
    const/4 v6, -0x1

    #@2b
    .line 175
    .restart local v6       #result:B
    goto :goto_11

    #@2c
    .line 173
    .end local v2           #ioe:Ljava/io/IOException;
    .end local v6           #result:B
    :catch_2c
    move-exception v4

    #@2d
    .line 174
    .local v4, re:Ljava/lang/RuntimeException;
    const/4 v6, -0x1

    #@2e
    .restart local v6       #result:B
    goto :goto_11
.end method

.method private handleHPGC(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 3
    .parameter "request"

    #@0
    .prologue
    .line 222
    invoke-static {}, Ljava/lang/System;->gc()V

    #@3
    .line 224
    const/4 v0, 0x0

    #@4
    return-object v0
.end method

.method private handleHPIF(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 7
    .parameter "request"

    #@0
    .prologue
    .line 112
    invoke-static {p1}, Landroid/ddm/DdmHandleHeap;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 114
    .local v0, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    #@7
    move-result v2

    #@8
    .line 118
    .local v2, when:I
    invoke-static {v2}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->heapInfoNotify(I)Z

    #@b
    move-result v1

    #@c
    .line 119
    .local v1, ok:Z
    if-nez v1, :cond_16

    #@e
    .line 120
    const/4 v3, 0x1

    #@f
    const-string v4, "Unsupported HPIF what"

    #@11
    invoke-static {v3, v4}, Landroid/ddm/DdmHandleHeap;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@14
    move-result-object v3

    #@15
    .line 122
    :goto_15
    return-object v3

    #@16
    :cond_16
    const/4 v3, 0x0

    #@17
    goto :goto_15
.end method

.method private handleHPSGNHSG(Lorg/apache/harmony/dalvik/ddmc/Chunk;Z)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 9
    .parameter "request"
    .parameter "isNative"

    #@0
    .prologue
    .line 130
    invoke-static {p1}, Landroid/ddm/DdmHandleHeap;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 132
    .local v0, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    #@7
    move-result v3

    #@8
    .line 133
    .local v3, when:I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    #@b
    move-result v2

    #@c
    .line 138
    .local v2, what:I
    invoke-static {v3, v2, p2}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->heapSegmentNotify(IIZ)Z

    #@f
    move-result v1

    #@10
    .line 139
    .local v1, ok:Z
    if-nez v1, :cond_1a

    #@12
    .line 140
    const/4 v4, 0x1

    #@13
    const-string v5, "Unsupported HPSG what/when"

    #@15
    invoke-static {v4, v5}, Landroid/ddm/DdmHandleHeap;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@18
    move-result-object v4

    #@19
    .line 144
    :goto_19
    return-object v4

    #@1a
    :cond_1a
    const/4 v4, 0x0

    #@1b
    goto :goto_19
.end method

.method private handleREAE(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 5
    .parameter "request"

    #@0
    .prologue
    .line 231
    invoke-static {p1}, Landroid/ddm/DdmHandleHeap;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v1

    #@4
    .line 234
    .local v1, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_10

    #@a
    const/4 v0, 0x1

    #@b
    .line 239
    .local v0, enable:Z
    :goto_b
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->enableRecentAllocations(Z)V

    #@e
    .line 241
    const/4 v2, 0x0

    #@f
    return-object v2

    #@10
    .line 234
    .end local v0           #enable:Z
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_b
.end method

.method private handleREAL(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 7
    .parameter "request"

    #@0
    .prologue
    .line 265
    invoke-static {}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->getRecentAllocations()[B

    #@3
    move-result-object v0

    #@4
    .line 266
    .local v0, reply:[B
    new-instance v1, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@6
    sget v2, Landroid/ddm/DdmHandleHeap;->CHUNK_REAL:I

    #@8
    const/4 v3, 0x0

    #@9
    array-length v4, v0

    #@a
    invoke-direct {v1, v2, v0, v3, v4}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@d
    return-object v1
.end method

.method private handleREAQ(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 7
    .parameter "request"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 250
    new-array v0, v1, [B

    #@4
    .line 251
    .local v0, reply:[B
    invoke-static {}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->getRecentAllocationStatus()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_15

    #@a
    :goto_a
    aput-byte v1, v0, v2

    #@c
    .line 252
    new-instance v1, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@e
    sget v3, Landroid/ddm/DdmHandleHeap;->CHUNK_REAQ:I

    #@10
    array-length v4, v0

    #@11
    invoke-direct {v1, v3, v0, v2, v4}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@14
    return-object v1

    #@15
    :cond_15
    move v1, v2

    #@16
    .line 251
    goto :goto_a
.end method

.method public static register()V
    .registers 2

    #@0
    .prologue
    .line 53
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPIF:I

    #@2
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@4
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@7
    .line 54
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPSG:I

    #@9
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@b
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@e
    .line 55
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDU:I

    #@10
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@12
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@15
    .line 56
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDS:I

    #@17
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@19
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@1c
    .line 57
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_NHSG:I

    #@1e
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@20
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@23
    .line 58
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_HPGC:I

    #@25
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@27
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@2a
    .line 59
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_REAE:I

    #@2c
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@2e
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@31
    .line 60
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_REAQ:I

    #@33
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@35
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@38
    .line 61
    sget v0, Landroid/ddm/DdmHandleHeap;->CHUNK_REAL:I

    #@3a
    sget-object v1, Landroid/ddm/DdmHandleHeap;->mInstance:Landroid/ddm/DdmHandleHeap;

    #@3c
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@3f
    .line 62
    return-void
.end method


# virtual methods
.method public connected()V
    .registers 1

    #@0
    .prologue
    .line 68
    return-void
.end method

.method public disconnected()V
    .registers 1

    #@0
    .prologue
    .line 74
    return-void
.end method

.method public handleChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 6
    .parameter "request"

    #@0
    .prologue
    .line 82
    iget v0, p1, Lorg/apache/harmony/dalvik/ddmc/Chunk;->type:I

    #@2
    .line 84
    .local v0, type:I
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_HPIF:I

    #@4
    if-ne v0, v1, :cond_b

    #@6
    .line 85
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleHPIF(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@9
    move-result-object v1

    #@a
    .line 101
    :goto_a
    return-object v1

    #@b
    .line 86
    :cond_b
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_HPSG:I

    #@d
    if-ne v0, v1, :cond_15

    #@f
    .line 87
    const/4 v1, 0x0

    #@10
    invoke-direct {p0, p1, v1}, Landroid/ddm/DdmHandleHeap;->handleHPSGNHSG(Lorg/apache/harmony/dalvik/ddmc/Chunk;Z)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@13
    move-result-object v1

    #@14
    goto :goto_a

    #@15
    .line 88
    :cond_15
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDU:I

    #@17
    if-ne v0, v1, :cond_1e

    #@19
    .line 89
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleHPDU(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@1c
    move-result-object v1

    #@1d
    goto :goto_a

    #@1e
    .line 90
    :cond_1e
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_HPDS:I

    #@20
    if-ne v0, v1, :cond_27

    #@22
    .line 91
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleHPDS(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@25
    move-result-object v1

    #@26
    goto :goto_a

    #@27
    .line 92
    :cond_27
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_NHSG:I

    #@29
    if-ne v0, v1, :cond_31

    #@2b
    .line 93
    const/4 v1, 0x1

    #@2c
    invoke-direct {p0, p1, v1}, Landroid/ddm/DdmHandleHeap;->handleHPSGNHSG(Lorg/apache/harmony/dalvik/ddmc/Chunk;Z)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@2f
    move-result-object v1

    #@30
    goto :goto_a

    #@31
    .line 94
    :cond_31
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_HPGC:I

    #@33
    if-ne v0, v1, :cond_3a

    #@35
    .line 95
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleHPGC(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@38
    move-result-object v1

    #@39
    goto :goto_a

    #@3a
    .line 96
    :cond_3a
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_REAE:I

    #@3c
    if-ne v0, v1, :cond_43

    #@3e
    .line 97
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleREAE(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@41
    move-result-object v1

    #@42
    goto :goto_a

    #@43
    .line 98
    :cond_43
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_REAQ:I

    #@45
    if-ne v0, v1, :cond_4c

    #@47
    .line 99
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleREAQ(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@4a
    move-result-object v1

    #@4b
    goto :goto_a

    #@4c
    .line 100
    :cond_4c
    sget v1, Landroid/ddm/DdmHandleHeap;->CHUNK_REAL:I

    #@4e
    if-ne v0, v1, :cond_55

    #@50
    .line 101
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHeap;->handleREAL(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@53
    move-result-object v1

    #@54
    goto :goto_a

    #@55
    .line 103
    :cond_55
    new-instance v1, Ljava/lang/RuntimeException;

    #@57
    new-instance v2, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v3, "Unknown packet "

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->name(I)Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v2

    #@6e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@71
    throw v1
.end method
