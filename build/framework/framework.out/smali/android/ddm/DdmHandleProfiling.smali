.class public Landroid/ddm/DdmHandleProfiling;
.super Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;
.source "DdmHandleProfiling.java"


# static fields
.field public static final CHUNK_MPRE:I

.field public static final CHUNK_MPRQ:I

.field public static final CHUNK_MPRS:I

.field public static final CHUNK_MPSE:I

.field public static final CHUNK_MPSS:I

.field private static mInstance:Landroid/ddm/DdmHandleProfiling;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const-string v0, "MPRS"

    #@2
    invoke-static {v0}, Landroid/ddm/DdmHandleProfiling;->type(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRS:I

    #@8
    .line 33
    const-string v0, "MPRE"

    #@a
    invoke-static {v0}, Landroid/ddm/DdmHandleProfiling;->type(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    sput v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRE:I

    #@10
    .line 34
    const-string v0, "MPSS"

    #@12
    invoke-static {v0}, Landroid/ddm/DdmHandleProfiling;->type(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    sput v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPSS:I

    #@18
    .line 35
    const-string v0, "MPSE"

    #@1a
    invoke-static {v0}, Landroid/ddm/DdmHandleProfiling;->type(Ljava/lang/String;)I

    #@1d
    move-result v0

    #@1e
    sput v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPSE:I

    #@20
    .line 36
    const-string v0, "MPRQ"

    #@22
    invoke-static {v0}, Landroid/ddm/DdmHandleProfiling;->type(Ljava/lang/String;)I

    #@25
    move-result v0

    #@26
    sput v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRQ:I

    #@28
    .line 38
    new-instance v0, Landroid/ddm/DdmHandleProfiling;

    #@2a
    invoke-direct {v0}, Landroid/ddm/DdmHandleProfiling;-><init>()V

    #@2d
    sput-object v0, Landroid/ddm/DdmHandleProfiling;->mInstance:Landroid/ddm/DdmHandleProfiling;

    #@2f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;-><init>()V

    #@3
    return-void
.end method

.method private handleMPRE(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 9
    .parameter "request"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 120
    :try_start_1
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_4} :catch_13

    #@4
    .line 121
    const/4 v2, 0x0

    #@5
    .line 129
    .local v2, result:B
    :goto_5
    const/4 v3, 0x1

    #@6
    new-array v1, v3, [B

    #@8
    aput-byte v2, v1, v6

    #@a
    .line 130
    .local v1, reply:[B
    new-instance v3, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@c
    sget v4, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRE:I

    #@e
    array-length v5, v1

    #@f
    invoke-direct {v3, v4, v1, v6, v5}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@12
    return-object v3

    #@13
    .line 122
    .end local v1           #reply:[B
    .end local v2           #result:B
    :catch_13
    move-exception v0

    #@14
    .line 123
    .local v0, re:Ljava/lang/RuntimeException;
    const-string v3, "ddm-heap"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "Method profiling end failed: "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 125
    const/4 v2, 0x1

    #@31
    .restart local v2       #result:B
    goto :goto_5
.end method

.method private handleMPRQ(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 8
    .parameter "request"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 181
    invoke-static {}, Landroid/os/Debug;->isMethodTracingActive()Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_17

    #@8
    move v1, v2

    #@9
    .line 184
    .local v1, result:I
    :goto_9
    new-array v0, v2, [B

    #@b
    int-to-byte v2, v1

    #@c
    aput-byte v2, v0, v3

    #@e
    .line 185
    .local v0, reply:[B
    new-instance v2, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@10
    sget v4, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRQ:I

    #@12
    array-length v5, v0

    #@13
    invoke-direct {v2, v4, v0, v3, v5}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@16
    return-object v2

    #@17
    .end local v0           #reply:[B
    .end local v1           #result:I
    :cond_17
    move v1, v3

    #@18
    .line 181
    goto :goto_9
.end method

.method private handleMPRS(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 10
    .parameter "request"

    #@0
    .prologue
    .line 95
    invoke-static {p1}, Landroid/ddm/DdmHandleProfiling;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v3

    #@4
    .line 97
    .local v3, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    #@7
    move-result v0

    #@8
    .line 98
    .local v0, bufferSize:I
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    #@b
    move-result v2

    #@c
    .line 99
    .local v2, flags:I
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    #@f
    move-result v4

    #@10
    .line 100
    .local v4, len:I
    invoke-static {v3, v4}, Landroid/ddm/DdmHandleProfiling;->getString(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 106
    .local v1, fileName:Ljava/lang/String;
    :try_start_14
    invoke-static {v1, v0, v2}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;II)V
    :try_end_17
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_17} :catch_19

    #@17
    .line 107
    const/4 v6, 0x0

    #@18
    .line 109
    :goto_18
    return-object v6

    #@19
    .line 108
    :catch_19
    move-exception v5

    #@1a
    .line 109
    .local v5, re:Ljava/lang/RuntimeException;
    const/4 v6, 0x1

    #@1b
    invoke-virtual {v5}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@1e
    move-result-object v7

    #@1f
    invoke-static {v6, v7}, Landroid/ddm/DdmHandleProfiling;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@22
    move-result-object v6

    #@23
    goto :goto_18
.end method

.method private handleMPSE(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 7
    .parameter "request"

    #@0
    .prologue
    .line 165
    :try_start_0
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_6

    #@3
    .line 166
    const/4 v1, 0x0

    #@4
    .line 174
    .local v1, result:B
    const/4 v2, 0x0

    #@5
    .end local v1           #result:B
    :goto_5
    return-object v2

    #@6
    .line 167
    :catch_6
    move-exception v0

    #@7
    .line 168
    .local v0, re:Ljava/lang/RuntimeException;
    const-string v2, "ddm-heap"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "Method prof stream end failed: "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 170
    const/4 v2, 0x1

    #@24
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v2, v3}, Landroid/ddm/DdmHandleProfiling;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@2b
    move-result-object v2

    #@2c
    goto :goto_5
.end method

.method private handleMPSS(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 8
    .parameter "request"

    #@0
    .prologue
    .line 137
    invoke-static {p1}, Landroid/ddm/DdmHandleProfiling;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v2

    #@4
    .line 139
    .local v2, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    #@7
    move-result v0

    #@8
    .line 140
    .local v0, bufferSize:I
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    #@b
    move-result v1

    #@c
    .line 147
    .local v1, flags:I
    :try_start_c
    invoke-static {v0, v1}, Landroid/os/Debug;->startMethodTracingDdms(II)V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_f} :catch_11

    #@f
    .line 148
    const/4 v4, 0x0

    #@10
    .line 150
    :goto_10
    return-object v4

    #@11
    .line 149
    :catch_11
    move-exception v3

    #@12
    .line 150
    .local v3, re:Ljava/lang/RuntimeException;
    const/4 v4, 0x1

    #@13
    invoke-virtual {v3}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    invoke-static {v4, v5}, Landroid/ddm/DdmHandleProfiling;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@1a
    move-result-object v4

    #@1b
    goto :goto_10
.end method

.method public static register()V
    .registers 2

    #@0
    .prologue
    .line 48
    sget v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRS:I

    #@2
    sget-object v1, Landroid/ddm/DdmHandleProfiling;->mInstance:Landroid/ddm/DdmHandleProfiling;

    #@4
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@7
    .line 49
    sget v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRE:I

    #@9
    sget-object v1, Landroid/ddm/DdmHandleProfiling;->mInstance:Landroid/ddm/DdmHandleProfiling;

    #@b
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@e
    .line 50
    sget v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPSS:I

    #@10
    sget-object v1, Landroid/ddm/DdmHandleProfiling;->mInstance:Landroid/ddm/DdmHandleProfiling;

    #@12
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@15
    .line 51
    sget v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPSE:I

    #@17
    sget-object v1, Landroid/ddm/DdmHandleProfiling;->mInstance:Landroid/ddm/DdmHandleProfiling;

    #@19
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@1c
    .line 52
    sget v0, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRQ:I

    #@1e
    sget-object v1, Landroid/ddm/DdmHandleProfiling;->mInstance:Landroid/ddm/DdmHandleProfiling;

    #@20
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@23
    .line 53
    return-void
.end method


# virtual methods
.method public connected()V
    .registers 1

    #@0
    .prologue
    .line 59
    return-void
.end method

.method public disconnected()V
    .registers 1

    #@0
    .prologue
    .line 65
    return-void
.end method

.method public handleChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 6
    .parameter "request"

    #@0
    .prologue
    .line 73
    iget v0, p1, Lorg/apache/harmony/dalvik/ddmc/Chunk;->type:I

    #@2
    .line 75
    .local v0, type:I
    sget v1, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRS:I

    #@4
    if-ne v0, v1, :cond_b

    #@6
    .line 76
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleProfiling;->handleMPRS(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@9
    move-result-object v1

    #@a
    .line 84
    :goto_a
    return-object v1

    #@b
    .line 77
    :cond_b
    sget v1, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRE:I

    #@d
    if-ne v0, v1, :cond_14

    #@f
    .line 78
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleProfiling;->handleMPRE(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@12
    move-result-object v1

    #@13
    goto :goto_a

    #@14
    .line 79
    :cond_14
    sget v1, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPSS:I

    #@16
    if-ne v0, v1, :cond_1d

    #@18
    .line 80
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleProfiling;->handleMPSS(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@1b
    move-result-object v1

    #@1c
    goto :goto_a

    #@1d
    .line 81
    :cond_1d
    sget v1, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPSE:I

    #@1f
    if-ne v0, v1, :cond_26

    #@21
    .line 82
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleProfiling;->handleMPSE(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@24
    move-result-object v1

    #@25
    goto :goto_a

    #@26
    .line 83
    :cond_26
    sget v1, Landroid/ddm/DdmHandleProfiling;->CHUNK_MPRQ:I

    #@28
    if-ne v0, v1, :cond_2f

    #@2a
    .line 84
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleProfiling;->handleMPRQ(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@2d
    move-result-object v1

    #@2e
    goto :goto_a

    #@2f
    .line 86
    :cond_2f
    new-instance v1, Ljava/lang/RuntimeException;

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Unknown packet "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->name(I)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v1
.end method
