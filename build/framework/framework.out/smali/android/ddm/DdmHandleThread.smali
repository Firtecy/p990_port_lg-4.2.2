.class public Landroid/ddm/DdmHandleThread;
.super Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;
.source "DdmHandleThread.java"


# static fields
.field public static final CHUNK_STKL:I

.field public static final CHUNK_THCR:I

.field public static final CHUNK_THDE:I

.field public static final CHUNK_THEN:I

.field public static final CHUNK_THST:I

.field private static mInstance:Landroid/ddm/DdmHandleThread;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    const-string v0, "THEN"

    #@2
    invoke-static {v0}, Landroid/ddm/DdmHandleThread;->type(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/ddm/DdmHandleThread;->CHUNK_THEN:I

    #@8
    .line 32
    const-string v0, "THCR"

    #@a
    invoke-static {v0}, Landroid/ddm/DdmHandleThread;->type(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    sput v0, Landroid/ddm/DdmHandleThread;->CHUNK_THCR:I

    #@10
    .line 33
    const-string v0, "THDE"

    #@12
    invoke-static {v0}, Landroid/ddm/DdmHandleThread;->type(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    sput v0, Landroid/ddm/DdmHandleThread;->CHUNK_THDE:I

    #@18
    .line 34
    const-string v0, "THST"

    #@1a
    invoke-static {v0}, Landroid/ddm/DdmHandleThread;->type(Ljava/lang/String;)I

    #@1d
    move-result v0

    #@1e
    sput v0, Landroid/ddm/DdmHandleThread;->CHUNK_THST:I

    #@20
    .line 35
    const-string v0, "STKL"

    #@22
    invoke-static {v0}, Landroid/ddm/DdmHandleThread;->type(Ljava/lang/String;)I

    #@25
    move-result v0

    #@26
    sput v0, Landroid/ddm/DdmHandleThread;->CHUNK_STKL:I

    #@28
    .line 37
    new-instance v0, Landroid/ddm/DdmHandleThread;

    #@2a
    invoke-direct {v0}, Landroid/ddm/DdmHandleThread;-><init>()V

    #@2d
    sput-object v0, Landroid/ddm/DdmHandleThread;->mInstance:Landroid/ddm/DdmHandleThread;

    #@2f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;-><init>()V

    #@3
    return-void
.end method

.method private createStackChunk([Ljava/lang/StackTraceElement;I)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 11
    .parameter "trace"
    .parameter "threadId"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 146
    const/4 v1, 0x0

    #@2
    .line 148
    .local v1, bufferSize:I
    add-int/lit8 v1, v1, 0x4

    #@4
    .line 149
    add-int/lit8 v1, v1, 0x4

    #@6
    .line 150
    add-int/lit8 v1, v1, 0x4

    #@8
    .line 151
    move-object v0, p1

    #@9
    .local v0, arr$:[Ljava/lang/StackTraceElement;
    array-length v4, v0

    #@a
    .local v4, len$:I
    const/4 v3, 0x0

    #@b
    .local v3, i$:I
    :goto_b
    if-ge v3, v4, :cond_41

    #@d
    aget-object v2, v0, v3

    #@f
    .line 152
    .local v2, elem:Ljava/lang/StackTraceElement;
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@12
    move-result-object v6

    #@13
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@16
    move-result v6

    #@17
    mul-int/lit8 v6, v6, 0x2

    #@19
    add-int/lit8 v6, v6, 0x4

    #@1b
    add-int/2addr v1, v6

    #@1c
    .line 153
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@23
    move-result v6

    #@24
    mul-int/lit8 v6, v6, 0x2

    #@26
    add-int/lit8 v6, v6, 0x4

    #@28
    add-int/2addr v1, v6

    #@29
    .line 154
    add-int/lit8 v1, v1, 0x4

    #@2b
    .line 155
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    if-eqz v6, :cond_3c

    #@31
    .line 156
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@38
    move-result v6

    #@39
    mul-int/lit8 v6, v6, 0x2

    #@3b
    add-int/2addr v1, v6

    #@3c
    .line 157
    :cond_3c
    add-int/lit8 v1, v1, 0x4

    #@3e
    .line 151
    add-int/lit8 v3, v3, 0x1

    #@40
    goto :goto_b

    #@41
    .line 160
    .end local v2           #elem:Ljava/lang/StackTraceElement;
    :cond_41
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@44
    move-result-object v5

    #@45
    .line 161
    .local v5, out:Ljava/nio/ByteBuffer;
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@48
    .line 162
    invoke-virtual {v5, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@4b
    .line 163
    array-length v6, p1

    #@4c
    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@4f
    .line 164
    move-object v0, p1

    #@50
    array-length v4, v0

    #@51
    const/4 v3, 0x0

    #@52
    :goto_52
    if-ge v3, v4, :cond_a0

    #@54
    aget-object v2, v0, v3

    #@56
    .line 165
    .restart local v2       #elem:Ljava/lang/StackTraceElement;
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@59
    move-result-object v6

    #@5a
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@5d
    move-result v6

    #@5e
    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@61
    .line 166
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-static {v5, v6}, Landroid/ddm/DdmHandleThread;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@68
    .line 167
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@6f
    move-result v6

    #@70
    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@73
    .line 168
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    #@76
    move-result-object v6

    #@77
    invoke-static {v5, v6}, Landroid/ddm/DdmHandleThread;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@7a
    .line 169
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    #@7d
    move-result-object v6

    #@7e
    if-eqz v6, :cond_9c

    #@80
    .line 170
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@87
    move-result v6

    #@88
    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@8b
    .line 171
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    #@8e
    move-result-object v6

    #@8f
    invoke-static {v5, v6}, Landroid/ddm/DdmHandleThread;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@92
    .line 175
    :goto_92
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    #@95
    move-result v6

    #@96
    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@99
    .line 164
    add-int/lit8 v3, v3, 0x1

    #@9b
    goto :goto_52

    #@9c
    .line 173
    :cond_9c
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@9f
    goto :goto_92

    #@a0
    .line 178
    .end local v2           #elem:Ljava/lang/StackTraceElement;
    :cond_a0
    new-instance v6, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@a2
    sget v7, Landroid/ddm/DdmHandleThread;->CHUNK_STKL:I

    #@a4
    invoke-direct {v6, v7, v5}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(ILjava/nio/ByteBuffer;)V

    #@a7
    return-object v6
.end method

.method private handleSTKL(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 7
    .parameter "request"

    #@0
    .prologue
    .line 124
    invoke-static {p1}, Landroid/ddm/DdmHandleThread;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 127
    .local v0, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    #@7
    move-result v1

    #@8
    .line 131
    .local v1, threadId:I
    invoke-static {v1}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->getStackTraceById(I)[Ljava/lang/StackTraceElement;

    #@b
    move-result-object v2

    #@c
    .line 132
    .local v2, trace:[Ljava/lang/StackTraceElement;
    if-nez v2, :cond_16

    #@e
    .line 133
    const/4 v3, 0x1

    #@f
    const-string v4, "Stack trace unavailable"

    #@11
    invoke-static {v3, v4}, Landroid/ddm/DdmHandleThread;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@14
    move-result-object v3

    #@15
    .line 135
    :goto_15
    return-object v3

    #@16
    :cond_16
    invoke-direct {p0, v2, v1}, Landroid/ddm/DdmHandleThread;->createStackChunk([Ljava/lang/StackTraceElement;I)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@19
    move-result-object v3

    #@1a
    goto :goto_15
.end method

.method private handleTHEN(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 5
    .parameter "request"

    #@0
    .prologue
    .line 88
    invoke-static {p1}, Landroid/ddm/DdmHandleThread;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v1

    #@4
    .line 90
    .local v1, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_10

    #@a
    const/4 v0, 0x1

    #@b
    .line 93
    .local v0, enable:Z
    :goto_b
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->threadNotify(Z)V

    #@e
    .line 94
    const/4 v2, 0x0

    #@f
    return-object v2

    #@10
    .line 90
    .end local v0           #enable:Z
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_b
.end method

.method private handleTHST(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 8
    .parameter "request"

    #@0
    .prologue
    .line 101
    invoke-static {p1}, Landroid/ddm/DdmHandleThread;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 106
    .local v0, in:Ljava/nio/ByteBuffer;
    invoke-static {}, Lorg/apache/harmony/dalvik/ddmc/DdmVmInternal;->getThreadStats()[B

    #@7
    move-result-object v1

    #@8
    .line 107
    .local v1, status:[B
    if-eqz v1, :cond_14

    #@a
    .line 108
    new-instance v2, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@c
    sget v3, Landroid/ddm/DdmHandleThread;->CHUNK_THST:I

    #@e
    const/4 v4, 0x0

    #@f
    array-length v5, v1

    #@10
    invoke-direct {v2, v3, v1, v4, v5}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@13
    .line 110
    :goto_13
    return-object v2

    #@14
    :cond_14
    const/4 v2, 0x1

    #@15
    const-string v3, "Can\'t build THST chunk"

    #@17
    invoke-static {v2, v3}, Landroid/ddm/DdmHandleThread;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@1a
    move-result-object v2

    #@1b
    goto :goto_13
.end method

.method public static register()V
    .registers 2

    #@0
    .prologue
    .line 47
    sget v0, Landroid/ddm/DdmHandleThread;->CHUNK_THEN:I

    #@2
    sget-object v1, Landroid/ddm/DdmHandleThread;->mInstance:Landroid/ddm/DdmHandleThread;

    #@4
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@7
    .line 48
    sget v0, Landroid/ddm/DdmHandleThread;->CHUNK_THST:I

    #@9
    sget-object v1, Landroid/ddm/DdmHandleThread;->mInstance:Landroid/ddm/DdmHandleThread;

    #@b
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@e
    .line 49
    sget v0, Landroid/ddm/DdmHandleThread;->CHUNK_STKL:I

    #@10
    sget-object v1, Landroid/ddm/DdmHandleThread;->mInstance:Landroid/ddm/DdmHandleThread;

    #@12
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@15
    .line 50
    return-void
.end method


# virtual methods
.method public connected()V
    .registers 1

    #@0
    .prologue
    .line 56
    return-void
.end method

.method public disconnected()V
    .registers 1

    #@0
    .prologue
    .line 62
    return-void
.end method

.method public handleChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 6
    .parameter "request"

    #@0
    .prologue
    .line 70
    iget v0, p1, Lorg/apache/harmony/dalvik/ddmc/Chunk;->type:I

    #@2
    .line 72
    .local v0, type:I
    sget v1, Landroid/ddm/DdmHandleThread;->CHUNK_THEN:I

    #@4
    if-ne v0, v1, :cond_b

    #@6
    .line 73
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleThread;->handleTHEN(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@9
    move-result-object v1

    #@a
    .line 77
    :goto_a
    return-object v1

    #@b
    .line 74
    :cond_b
    sget v1, Landroid/ddm/DdmHandleThread;->CHUNK_THST:I

    #@d
    if-ne v0, v1, :cond_14

    #@f
    .line 75
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleThread;->handleTHST(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@12
    move-result-object v1

    #@13
    goto :goto_a

    #@14
    .line 76
    :cond_14
    sget v1, Landroid/ddm/DdmHandleThread;->CHUNK_STKL:I

    #@16
    if-ne v0, v1, :cond_1d

    #@18
    .line 77
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleThread;->handleSTKL(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@1b
    move-result-object v1

    #@1c
    goto :goto_a

    #@1d
    .line 79
    :cond_1d
    new-instance v1, Ljava/lang/RuntimeException;

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Unknown packet "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->name(I)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@39
    throw v1
.end method
