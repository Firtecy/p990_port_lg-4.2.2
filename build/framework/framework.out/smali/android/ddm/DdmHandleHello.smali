.class public Landroid/ddm/DdmHandleHello;
.super Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;
.source "DdmHandleHello.java"


# static fields
.field public static final CHUNK_FEAT:I

.field public static final CHUNK_HELO:I

.field public static final CHUNK_WAIT:I

.field private static mInstance:Landroid/ddm/DdmHandleHello;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    const-string v0, "HELO"

    #@2
    invoke-static {v0}, Landroid/ddm/DdmHandleHello;->type(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/ddm/DdmHandleHello;->CHUNK_HELO:I

    #@8
    .line 34
    const-string v0, "WAIT"

    #@a
    invoke-static {v0}, Landroid/ddm/DdmHandleHello;->type(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    sput v0, Landroid/ddm/DdmHandleHello;->CHUNK_WAIT:I

    #@10
    .line 35
    const-string v0, "FEAT"

    #@12
    invoke-static {v0}, Landroid/ddm/DdmHandleHello;->type(Ljava/lang/String;)I

    #@15
    move-result v0

    #@16
    sput v0, Landroid/ddm/DdmHandleHello;->CHUNK_FEAT:I

    #@18
    .line 37
    new-instance v0, Landroid/ddm/DdmHandleHello;

    #@1a
    invoke-direct {v0}, Landroid/ddm/DdmHandleHello;-><init>()V

    #@1d
    sput-object v0, Landroid/ddm/DdmHandleHello;->mInstance:Landroid/ddm/DdmHandleHello;

    #@1f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;-><init>()V

    #@3
    return-void
.end method

.method private handleFEAT(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 8
    .parameter "request"

    #@0
    .prologue
    .line 152
    invoke-static {}, Landroid/os/Debug;->getVmFeatureList()[Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 157
    .local v0, features:[Ljava/lang/String;
    array-length v4, v0

    #@5
    mul-int/lit8 v4, v4, 0x4

    #@7
    add-int/lit8 v3, v4, 0x4

    #@9
    .line 158
    .local v3, size:I
    array-length v4, v0

    #@a
    add-int/lit8 v1, v4, -0x1

    #@c
    .local v1, i:I
    :goto_c
    if-ltz v1, :cond_1a

    #@e
    .line 159
    aget-object v4, v0, v1

    #@10
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@13
    move-result v4

    #@14
    mul-int/lit8 v4, v4, 0x2

    #@16
    add-int/2addr v3, v4

    #@17
    .line 158
    add-int/lit8 v1, v1, -0x1

    #@19
    goto :goto_c

    #@1a
    .line 161
    :cond_1a
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@1d
    move-result-object v2

    #@1e
    .line 162
    .local v2, out:Ljava/nio/ByteBuffer;
    sget-object v4, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->CHUNK_ORDER:Ljava/nio/ByteOrder;

    #@20
    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@23
    .line 163
    array-length v4, v0

    #@24
    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@27
    .line 164
    array-length v4, v0

    #@28
    add-int/lit8 v1, v4, -0x1

    #@2a
    :goto_2a
    if-ltz v1, :cond_3d

    #@2c
    .line 165
    aget-object v4, v0, v1

    #@2e
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@31
    move-result v4

    #@32
    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@35
    .line 166
    aget-object v4, v0, v1

    #@37
    invoke-static {v2, v4}, Landroid/ddm/DdmHandleHello;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@3a
    .line 164
    add-int/lit8 v1, v1, -0x1

    #@3c
    goto :goto_2a

    #@3d
    .line 169
    :cond_3d
    new-instance v4, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@3f
    sget v5, Landroid/ddm/DdmHandleHello;->CHUNK_FEAT:I

    #@41
    invoke-direct {v4, v5, v2}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(ILjava/nio/ByteBuffer;)V

    #@44
    return-object v4
.end method

.method private handleHELO(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 12
    .parameter "request"

    #@0
    .prologue
    .line 105
    invoke-static {p1}, Landroid/ddm/DdmHandleHello;->wrapChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v1

    #@4
    .line 107
    .local v1, in:Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    #@7
    move-result v4

    #@8
    .line 114
    .local v4, serverProtoVers:I
    const-string/jumbo v8, "java.vm.name"

    #@b
    const-string v9, "?"

    #@d
    invoke-static {v8, v9}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v6

    #@11
    .line 115
    .local v6, vmName:Ljava/lang/String;
    const-string/jumbo v8, "java.vm.version"

    #@14
    const-string v9, "?"

    #@16
    invoke-static {v8, v9}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    .line 116
    .local v7, vmVersion:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    const-string v9, " v"

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    .line 121
    .local v5, vmIdent:Ljava/lang/String;
    invoke-static {}, Landroid/ddm/DdmHandleAppName;->getAppName()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    .line 123
    .local v0, appName:Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@38
    move-result v8

    #@39
    mul-int/lit8 v8, v8, 0x2

    #@3b
    add-int/lit8 v8, v8, 0x14

    #@3d
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@40
    move-result v9

    #@41
    mul-int/lit8 v9, v9, 0x2

    #@43
    add-int/2addr v8, v9

    #@44
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@47
    move-result-object v2

    #@48
    .line 125
    .local v2, out:Ljava/nio/ByteBuffer;
    sget-object v8, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->CHUNK_ORDER:Ljava/nio/ByteOrder;

    #@4a
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@4d
    .line 126
    const/4 v8, 0x1

    #@4e
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@51
    .line 127
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@54
    move-result v8

    #@55
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@58
    .line 128
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@5b
    move-result v8

    #@5c
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5f
    .line 129
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@62
    move-result v8

    #@63
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@66
    .line 130
    invoke-static {v2, v5}, Landroid/ddm/DdmHandleHello;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@69
    .line 131
    invoke-static {v2, v0}, Landroid/ddm/DdmHandleHello;->putString(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@6c
    .line 132
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@6f
    move-result v8

    #@70
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@73
    .line 134
    new-instance v3, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@75
    sget v8, Landroid/ddm/DdmHandleHello;->CHUNK_HELO:I

    #@77
    invoke-direct {v3, v8, v2}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(ILjava/nio/ByteBuffer;)V

    #@7a
    .line 140
    .local v3, reply:Lorg/apache/harmony/dalvik/ddmc/Chunk;
    invoke-static {}, Landroid/os/Debug;->waitingForDebugger()Z

    #@7d
    move-result v8

    #@7e
    if-eqz v8, :cond_84

    #@80
    .line 141
    const/4 v8, 0x0

    #@81
    invoke-static {v8}, Landroid/ddm/DdmHandleHello;->sendWAIT(I)V

    #@84
    .line 143
    :cond_84
    return-object v3
.end method

.method public static register()V
    .registers 2

    #@0
    .prologue
    .line 47
    sget v0, Landroid/ddm/DdmHandleHello;->CHUNK_HELO:I

    #@2
    sget-object v1, Landroid/ddm/DdmHandleHello;->mInstance:Landroid/ddm/DdmHandleHello;

    #@4
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@7
    .line 48
    sget v0, Landroid/ddm/DdmHandleHello;->CHUNK_FEAT:I

    #@9
    sget-object v1, Landroid/ddm/DdmHandleHello;->mInstance:Landroid/ddm/DdmHandleHello;

    #@b
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@e
    .line 49
    return-void
.end method

.method public static sendWAIT(I)V
    .registers 6
    .parameter "reason"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 177
    new-array v0, v4, [B

    #@4
    int-to-byte v2, p0

    #@5
    aput-byte v2, v0, v3

    #@7
    .line 178
    .local v0, data:[B
    new-instance v1, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@9
    sget v2, Landroid/ddm/DdmHandleHello;->CHUNK_WAIT:I

    #@b
    invoke-direct {v1, v2, v0, v3, v4}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@e
    .line 179
    .local v1, waitChunk:Lorg/apache/harmony/dalvik/ddmc/Chunk;
    invoke-static {v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->sendChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)V

    #@11
    .line 180
    return-void
.end method


# virtual methods
.method public connected()V
    .registers 1

    #@0
    .prologue
    .line 66
    return-void
.end method

.method public disconnected()V
    .registers 1

    #@0
    .prologue
    .line 75
    return-void
.end method

.method public handleChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 6
    .parameter "request"

    #@0
    .prologue
    .line 83
    iget v0, p1, Lorg/apache/harmony/dalvik/ddmc/Chunk;->type:I

    #@2
    .line 85
    .local v0, type:I
    sget v1, Landroid/ddm/DdmHandleHello;->CHUNK_HELO:I

    #@4
    if-ne v0, v1, :cond_b

    #@6
    .line 86
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHello;->handleHELO(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@9
    move-result-object v1

    #@a
    .line 88
    :goto_a
    return-object v1

    #@b
    .line 87
    :cond_b
    sget v1, Landroid/ddm/DdmHandleHello;->CHUNK_FEAT:I

    #@d
    if-ne v0, v1, :cond_14

    #@f
    .line 88
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleHello;->handleFEAT(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@12
    move-result-object v1

    #@13
    goto :goto_a

    #@14
    .line 90
    :cond_14
    new-instance v1, Ljava/lang/RuntimeException;

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Unknown packet "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->name(I)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@30
    throw v1
.end method
