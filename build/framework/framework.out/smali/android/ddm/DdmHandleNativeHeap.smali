.class public Landroid/ddm/DdmHandleNativeHeap;
.super Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;
.source "DdmHandleNativeHeap.java"


# static fields
.field public static final CHUNK_NHGT:I

.field private static mInstance:Landroid/ddm/DdmHandleNativeHeap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    const-string v0, "NHGT"

    #@2
    invoke-static {v0}, Landroid/ddm/DdmHandleNativeHeap;->type(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Landroid/ddm/DdmHandleNativeHeap;->CHUNK_NHGT:I

    #@8
    .line 32
    new-instance v0, Landroid/ddm/DdmHandleNativeHeap;

    #@a
    invoke-direct {v0}, Landroid/ddm/DdmHandleNativeHeap;-><init>()V

    #@d
    sput-object v0, Landroid/ddm/DdmHandleNativeHeap;->mInstance:Landroid/ddm/DdmHandleNativeHeap;

    #@f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;-><init>()V

    #@3
    return-void
.end method

.method private native getLeakInfo()[B
.end method

.method private handleNHGT(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 7
    .parameter "request"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Landroid/ddm/DdmHandleNativeHeap;->getLeakInfo()[B

    #@3
    move-result-object v0

    #@4
    .line 80
    .local v0, data:[B
    if-eqz v0, :cond_33

    #@6
    .line 82
    const-string v1, "ddm-nativeheap"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Sending "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    array-length v3, v0

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, " bytes"

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 83
    new-instance v1, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@27
    const-string v2, "NHGT"

    #@29
    invoke-static {v2}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->type(Ljava/lang/String;)I

    #@2c
    move-result v2

    #@2d
    const/4 v3, 0x0

    #@2e
    array-length v4, v0

    #@2f
    invoke-direct {v1, v2, v0, v3, v4}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@32
    .line 86
    :goto_32
    return-object v1

    #@33
    :cond_33
    const/4 v1, 0x1

    #@34
    const-string v2, "Something went wrong"

    #@36
    invoke-static {v1, v2}, Landroid/ddm/DdmHandleNativeHeap;->createFailChunk(ILjava/lang/String;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@39
    move-result-object v1

    #@3a
    goto :goto_32
.end method

.method public static register()V
    .registers 2

    #@0
    .prologue
    .line 42
    sget v0, Landroid/ddm/DdmHandleNativeHeap;->CHUNK_NHGT:I

    #@2
    sget-object v1, Landroid/ddm/DdmHandleNativeHeap;->mInstance:Landroid/ddm/DdmHandleNativeHeap;

    #@4
    invoke-static {v0, v1}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->registerHandler(ILorg/apache/harmony/dalvik/ddmc/ChunkHandler;)V

    #@7
    .line 43
    return-void
.end method


# virtual methods
.method public connected()V
    .registers 1

    #@0
    .prologue
    .line 49
    return-void
.end method

.method public disconnected()V
    .registers 1

    #@0
    .prologue
    .line 55
    return-void
.end method

.method public handleChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;
    .registers 6
    .parameter "request"

    #@0
    .prologue
    .line 61
    const-string v1, "ddm-nativeheap"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Handling "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget v3, p1, Lorg/apache/harmony/dalvik/ddmc/Chunk;->type:I

    #@f
    invoke-static {v3}, Landroid/ddm/DdmHandleNativeHeap;->name(I)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " chunk"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 62
    iget v0, p1, Lorg/apache/harmony/dalvik/ddmc/Chunk;->type:I

    #@26
    .line 64
    .local v0, type:I
    sget v1, Landroid/ddm/DdmHandleNativeHeap;->CHUNK_NHGT:I

    #@28
    if-ne v0, v1, :cond_2f

    #@2a
    .line 65
    invoke-direct {p0, p1}, Landroid/ddm/DdmHandleNativeHeap;->handleNHGT(Lorg/apache/harmony/dalvik/ddmc/Chunk;)Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@2d
    move-result-object v1

    #@2e
    return-object v1

    #@2f
    .line 67
    :cond_2f
    new-instance v1, Ljava/lang/RuntimeException;

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Unknown packet "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v0}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->name(I)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v1
.end method
