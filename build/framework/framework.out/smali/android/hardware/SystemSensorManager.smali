.class public Landroid/hardware/SystemSensorManager;
.super Landroid/hardware/SensorManager;
.source "SystemSensorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/SystemSensorManager$ListenerDelegate;,
        Landroid/hardware/SystemSensorManager$SensorThread;
    }
.end annotation


# static fields
.field private static final SENSOR_DISABLE:I = -0x1

.field private static sFullSensorsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation
.end field

.field static sHandleToSensor:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation
.end field

.field static final sListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/SystemSensorManager$ListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field static sPool:Landroid/hardware/SensorManager$SensorEventPool;

.field private static sQueue:I

.field private static sSensorModuleInitialized:Z

.field private static sSensorThread:Landroid/hardware/SystemSensorManager$SensorThread;


# instance fields
.field final mMainLooper:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 39
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/hardware/SystemSensorManager;->sSensorModuleInitialized:Z

    #@3
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    sput-object v0, Landroid/hardware/SystemSensorManager;->sFullSensorsList:Ljava/util/ArrayList;

    #@a
    .line 47
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    sput-object v0, Landroid/hardware/SystemSensorManager;->sHandleToSensor:Landroid/util/SparseArray;

    #@11
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    sput-object v0, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@18
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .registers 8
    .parameter "mainLooper"

    #@0
    .prologue
    .line 251
    invoke-direct {p0}, Landroid/hardware/SensorManager;-><init>()V

    #@3
    .line 252
    iput-object p1, p0, Landroid/hardware/SystemSensorManager;->mMainLooper:Landroid/os/Looper;

    #@5
    .line 254
    sget-object v4, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@7
    monitor-enter v4

    #@8
    .line 255
    :try_start_8
    sget-boolean v3, Landroid/hardware/SystemSensorManager;->sSensorModuleInitialized:Z

    #@a
    if-nez v3, :cond_47

    #@c
    .line 256
    const/4 v3, 0x1

    #@d
    sput-boolean v3, Landroid/hardware/SystemSensorManager;->sSensorModuleInitialized:Z

    #@f
    .line 258
    invoke-static {}, Landroid/hardware/SystemSensorManager;->nativeClassInit()V

    #@12
    .line 261
    invoke-static {}, Landroid/hardware/SystemSensorManager;->sensors_module_init()I

    #@15
    .line 262
    sget-object v0, Landroid/hardware/SystemSensorManager;->sFullSensorsList:Ljava/util/ArrayList;

    #@17
    .line 263
    .local v0, fullList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Sensor;>;"
    const/4 v1, 0x0

    #@18
    .line 265
    .local v1, i:I
    :cond_18
    new-instance v2, Landroid/hardware/Sensor;

    #@1a
    invoke-direct {v2}, Landroid/hardware/Sensor;-><init>()V

    #@1d
    .line 266
    .local v2, sensor:Landroid/hardware/Sensor;
    invoke-static {v2, v1}, Landroid/hardware/SystemSensorManager;->sensors_module_get_next_sensor(Landroid/hardware/Sensor;I)I

    #@20
    move-result v1

    #@21
    .line 268
    if-ltz v1, :cond_2f

    #@23
    .line 271
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    .line 272
    sget-object v3, Landroid/hardware/SystemSensorManager;->sHandleToSensor:Landroid/util/SparseArray;

    #@28
    invoke-virtual {v2}, Landroid/hardware/Sensor;->getHandle()I

    #@2b
    move-result v5

    #@2c
    invoke-virtual {v3, v5, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@2f
    .line 274
    :cond_2f
    if-gtz v1, :cond_18

    #@31
    .line 276
    new-instance v3, Landroid/hardware/SensorManager$SensorEventPool;

    #@33
    sget-object v5, Landroid/hardware/SystemSensorManager;->sFullSensorsList:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@38
    move-result v5

    #@39
    mul-int/lit8 v5, v5, 0x2

    #@3b
    invoke-direct {v3, v5}, Landroid/hardware/SensorManager$SensorEventPool;-><init>(I)V

    #@3e
    sput-object v3, Landroid/hardware/SystemSensorManager;->sPool:Landroid/hardware/SensorManager$SensorEventPool;

    #@40
    .line 277
    new-instance v3, Landroid/hardware/SystemSensorManager$SensorThread;

    #@42
    invoke-direct {v3}, Landroid/hardware/SystemSensorManager$SensorThread;-><init>()V

    #@45
    sput-object v3, Landroid/hardware/SystemSensorManager;->sSensorThread:Landroid/hardware/SystemSensorManager$SensorThread;

    #@47
    .line 279
    .end local v0           #fullList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Sensor;>;"
    .end local v1           #i:I
    .end local v2           #sensor:Landroid/hardware/Sensor;
    :cond_47
    monitor-exit v4

    #@48
    .line 280
    return-void

    #@49
    .line 279
    :catchall_49
    move-exception v3

    #@4a
    monitor-exit v4
    :try_end_4b
    .catchall {:try_start_8 .. :try_end_4b} :catchall_49

    #@4b
    throw v3
.end method

.method static synthetic access$000()I
    .registers 1

    #@0
    .prologue
    .line 37
    sget v0, Landroid/hardware/SystemSensorManager;->sQueue:I

    #@2
    return v0
.end method

.method static synthetic access$002(I)I
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 37
    sput p0, Landroid/hardware/SystemSensorManager;->sQueue:I

    #@2
    return p0
.end method

.method private disableSensorLocked(Landroid/hardware/Sensor;)Z
    .registers 8
    .parameter "sensor"

    #@0
    .prologue
    .line 302
    sget-object v4, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v2

    #@6
    .local v2, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_1a

    #@c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@12
    .line 303
    .local v1, i:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    invoke-virtual {v1, p1}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->hasSensor(Landroid/hardware/Sensor;)Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_6

    #@18
    .line 305
    const/4 v4, 0x1

    #@19
    .line 310
    .end local v1           #i:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :goto_19
    return v4

    #@1a
    .line 308
    :cond_1a
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    .line 309
    .local v3, name:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getHandle()I

    #@21
    move-result v0

    #@22
    .line 310
    .local v0, handle:I
    sget v4, Landroid/hardware/SystemSensorManager;->sQueue:I

    #@24
    const/4 v5, -0x1

    #@25
    invoke-static {v4, v3, v0, v5}, Landroid/hardware/SystemSensorManager;->sensors_enable_sensor(ILjava/lang/String;II)Z

    #@28
    move-result v4

    #@29
    goto :goto_19
.end method

.method private enableSensorLocked(Landroid/hardware/Sensor;I)Z
    .registers 9
    .parameter "sensor"
    .parameter "delay"

    #@0
    .prologue
    .line 289
    const/4 v4, 0x0

    #@1
    .line 290
    .local v4, result:Z
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v2

    #@7
    .local v2, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_27

    #@d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@13
    .line 291
    .local v1, i:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    invoke-virtual {v1, p1}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->hasSensor(Landroid/hardware/Sensor;)Z

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_7

    #@19
    .line 292
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    .line 293
    .local v3, name:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getHandle()I

    #@20
    move-result v0

    #@21
    .line 294
    .local v0, handle:I
    sget v5, Landroid/hardware/SystemSensorManager;->sQueue:I

    #@23
    invoke-static {v5, v3, v0, p2}, Landroid/hardware/SystemSensorManager;->sensors_enable_sensor(ILjava/lang/String;II)Z

    #@26
    move-result v4

    #@27
    .line 298
    .end local v0           #handle:I
    .end local v1           #i:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .end local v3           #name:Ljava/lang/String;
    :cond_27
    return v4
.end method

.method private getCaller()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    .line 402
    new-instance v6, Ljava/lang/Throwable;

    #@2
    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    #@5
    invoke-virtual {v6}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@8
    move-result-object v5

    #@9
    .line 403
    .local v5, stackTraceElements:[Ljava/lang/StackTraceElement;
    move-object v0, v5

    #@a
    .local v0, arr$:[Ljava/lang/StackTraceElement;
    array-length v3, v0

    #@b
    .local v3, len$:I
    const/4 v2, 0x0

    #@c
    .local v2, i$:I
    :goto_c
    if-ge v2, v3, :cond_49

    #@e
    aget-object v4, v0, v2

    #@10
    .line 404
    .local v4, method:Ljava/lang/StackTraceElement;
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 405
    .local v1, className:Ljava/lang/String;
    const-string v6, "android.hardware"

    #@16
    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_46

    #@1c
    .line 406
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    const-string v7, "."

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    const-string v7, "():"

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    #@3c
    move-result v7

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    .line 409
    .end local v1           #className:Ljava/lang/String;
    .end local v4           #method:Ljava/lang/StackTraceElement;
    :goto_45
    return-object v6

    #@46
    .line 403
    .restart local v1       #className:Ljava/lang/String;
    .restart local v4       #method:Ljava/lang/StackTraceElement;
    :cond_46
    add-int/lit8 v2, v2, 0x1

    #@48
    goto :goto_c

    #@49
    .line 409
    .end local v1           #className:Ljava/lang/String;
    .end local v4           #method:Ljava/lang/StackTraceElement;
    :cond_49
    const-string v6, "<Unknown>"

    #@4b
    goto :goto_45
.end method

.method private static native nativeClassInit()V
.end method

.method static native sensors_create_queue()I
.end method

.method static native sensors_data_poll(I[F[I[J)I
.end method

.method static native sensors_destroy_queue(I)V
.end method

.method static native sensors_enable_sensor(ILjava/lang/String;II)Z
.end method

.method private static native sensors_module_get_next_sensor(Landroid/hardware/Sensor;I)I
.end method

.method private static native sensors_module_init()I
.end method


# virtual methods
.method protected getFullSensorList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 285
    sget-object v0, Landroid/hardware/SystemSensorManager;->sFullSensorsList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method protected registerListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
    .registers 13
    .parameter "listener"
    .parameter "sensor"
    .parameter "delay"
    .parameter "handler"

    #@0
    .prologue
    .line 317
    const/4 v4, 0x1

    #@1
    .line 320
    .local v4, result:Z
    const-string v5, "SensorManager"

    #@3
    new-instance v6, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string/jumbo v7, "registerListenerImpl() [Sensor: "

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {p2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    const-string v7, ", Rate: "

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    const-string v7, ", SensorEventListener: "

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, "] by "

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-direct {p0}, Landroid/hardware/SystemSensorManager;->getCaller()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 323
    sget-object v6, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@42
    monitor-enter v6

    #@43
    .line 325
    const/4 v2, 0x0

    #@44
    .line 326
    .local v2, l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :try_start_44
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v1

    #@4a
    .local v1, i$:Ljava/util/Iterator;
    :cond_4a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v5

    #@4e
    if-eqz v5, :cond_ae

    #@50
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v0

    #@54
    check-cast v0, Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@56
    .line 327
    .local v0, i:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    invoke-virtual {v0}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->getListener()Ljava/lang/Object;
    :try_end_59
    .catchall {:try_start_44 .. :try_end_59} :catchall_a6

    #@59
    move-result-object v5

    #@5a
    if-ne v5, p1, :cond_4a

    #@5c
    .line 328
    move-object v2, v0

    #@5d
    move-object v3, v2

    #@5e
    .line 334
    .end local v0           #i:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .local v3, l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :goto_5e
    if-nez v3, :cond_91

    #@60
    .line 335
    :try_start_60
    new-instance v2, Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@62
    invoke-direct {v2, p0, p1, p2, p4}, Landroid/hardware/SystemSensorManager$ListenerDelegate;-><init>(Landroid/hardware/SystemSensorManager;Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;Landroid/os/Handler;)V
    :try_end_65
    .catchall {:try_start_60 .. :try_end_65} :catchall_a9

    #@65
    .line 336
    .end local v3           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v2       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :try_start_65
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@67
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6a
    .line 338
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@6c
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@6f
    move-result v5

    #@70
    if-nez v5, :cond_8f

    #@72
    .line 339
    sget-object v5, Landroid/hardware/SystemSensorManager;->sSensorThread:Landroid/hardware/SystemSensorManager$SensorThread;

    #@74
    invoke-virtual {v5}, Landroid/hardware/SystemSensorManager$SensorThread;->startLocked()Z

    #@77
    move-result v5

    #@78
    if-eqz v5, :cond_88

    #@7a
    .line 340
    invoke-direct {p0, p2, p3}, Landroid/hardware/SystemSensorManager;->enableSensorLocked(Landroid/hardware/Sensor;I)Z

    #@7d
    move-result v5

    #@7e
    if-nez v5, :cond_86

    #@80
    .line 342
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@82
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@85
    .line 343
    const/4 v4, 0x0

    #@86
    .line 362
    :cond_86
    :goto_86
    monitor-exit v6

    #@87
    .line 364
    return v4

    #@88
    .line 347
    :cond_88
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@8a
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_8d
    .catchall {:try_start_65 .. :try_end_8d} :catchall_a6

    #@8d
    .line 348
    const/4 v4, 0x0

    #@8e
    goto :goto_86

    #@8f
    .line 352
    :cond_8f
    const/4 v4, 0x0

    #@90
    goto :goto_86

    #@91
    .line 354
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v3       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :cond_91
    :try_start_91
    invoke-virtual {v3, p2}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->hasSensor(Landroid/hardware/Sensor;)Z

    #@94
    move-result v5

    #@95
    if-nez v5, :cond_ac

    #@97
    .line 355
    invoke-virtual {v3, p2}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->addSensor(Landroid/hardware/Sensor;)V

    #@9a
    .line 356
    invoke-direct {p0, p2, p3}, Landroid/hardware/SystemSensorManager;->enableSensorLocked(Landroid/hardware/Sensor;I)Z

    #@9d
    move-result v5

    #@9e
    if-nez v5, :cond_ac

    #@a0
    .line 358
    invoke-virtual {v3, p2}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->removeSensor(Landroid/hardware/Sensor;)I
    :try_end_a3
    .catchall {:try_start_91 .. :try_end_a3} :catchall_a9

    #@a3
    .line 359
    const/4 v4, 0x0

    #@a4
    move-object v2, v3

    #@a5
    .end local v3           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v2       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    goto :goto_86

    #@a6
    .line 362
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_a6
    move-exception v5

    #@a7
    :goto_a7
    :try_start_a7
    monitor-exit v6
    :try_end_a8
    .catchall {:try_start_a7 .. :try_end_a8} :catchall_a6

    #@a8
    throw v5

    #@a9
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :catchall_a9
    move-exception v5

    #@aa
    move-object v2, v3

    #@ab
    .end local v3           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v2       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    goto :goto_a7

    #@ac
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v3       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :cond_ac
    move-object v2, v3

    #@ad
    .end local v3           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v2       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    goto :goto_86

    #@ae
    :cond_ae
    move-object v3, v2

    #@af
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v3       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    goto :goto_5e
.end method

.method protected unregisterListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
    .registers 12
    .parameter "listener"
    .parameter "sensor"

    #@0
    .prologue
    .line 370
    sget-object v6, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v6

    #@3
    .line 371
    :try_start_3
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v4

    #@9
    .line 372
    .local v4, size:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v4, :cond_b4

    #@c
    .line 373
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@14
    .line 374
    .local v2, l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    invoke-virtual {v2}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->getListener()Ljava/lang/Object;

    #@17
    move-result-object v5

    #@18
    if-ne v5, p1, :cond_b6

    #@1a
    .line 375
    if-nez p2, :cond_71

    #@1c
    .line 376
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@21
    .line 378
    invoke-virtual {v2}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->getSensors()Ljava/util/List;

    #@24
    move-result-object v5

    #@25
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v1

    #@29
    .local v1, i$:Ljava/util/Iterator;
    :goto_29
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_b4

    #@2f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v3

    #@33
    check-cast v3, Landroid/hardware/Sensor;

    #@35
    .line 380
    .local v3, s:Landroid/hardware/Sensor;
    const-string v5, "SensorManager"

    #@37
    new-instance v7, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string/jumbo v8, "unregisterListenerImpl() [Sensor: "

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    const-string v8, ", SensorEventListener: "

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    const-string v8, "] by "

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-direct {p0}, Landroid/hardware/SystemSensorManager;->getCaller()Ljava/lang/String;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v7

    #@67
    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 383
    invoke-direct {p0, v3}, Landroid/hardware/SystemSensorManager;->disableSensorLocked(Landroid/hardware/Sensor;)Z

    #@6d
    goto :goto_29

    #@6e
    .line 397
    .end local v0           #i:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .end local v3           #s:Landroid/hardware/Sensor;
    .end local v4           #size:I
    :catchall_6e
    move-exception v5

    #@6f
    monitor-exit v6
    :try_end_70
    .catchall {:try_start_3 .. :try_end_70} :catchall_6e

    #@70
    throw v5

    #@71
    .line 385
    .restart local v0       #i:I
    .restart local v2       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .restart local v4       #size:I
    :cond_71
    :try_start_71
    invoke-virtual {v2, p2}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->removeSensor(Landroid/hardware/Sensor;)I

    #@74
    move-result v5

    #@75
    if-nez v5, :cond_b4

    #@77
    .line 387
    const-string v5, "SensorManager"

    #@79
    new-instance v7, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string/jumbo v8, "unregisterListenerImpl() [Sensor: "

    #@81
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v7

    #@85
    invoke-virtual {p2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v7

    #@8d
    const-string v8, ", SensorEventListener: "

    #@8f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v7

    #@93
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v7

    #@97
    const-string v8, "] by "

    #@99
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v7

    #@9d
    invoke-direct {p0}, Landroid/hardware/SystemSensorManager;->getCaller()Ljava/lang/String;

    #@a0
    move-result-object v8

    #@a1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v7

    #@a9
    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 391
    sget-object v5, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@ae
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@b1
    .line 392
    invoke-direct {p0, p2}, Landroid/hardware/SystemSensorManager;->disableSensorLocked(Landroid/hardware/Sensor;)Z

    #@b4
    .line 397
    .end local v2           #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :cond_b4
    monitor-exit v6
    :try_end_b5
    .catchall {:try_start_71 .. :try_end_b5} :catchall_6e

    #@b5
    .line 398
    return-void

    #@b6
    .line 372
    .restart local v2       #l:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    :cond_b6
    add-int/lit8 v0, v0, 0x1

    #@b8
    goto/16 :goto_a
.end method
