.class public Landroid/hardware/Camera$Parameters;
.super Ljava/lang/Object;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Parameters"
.end annotation


# static fields
.field public static final AE_BRACKET:Ljava/lang/String; = "AE-Bracket"

.field public static final AE_BRACKET_HDR:Ljava/lang/String; = "HDR"

.field public static final AE_BRACKET_HDR_OFF:Ljava/lang/String; = "Off"

.field public static final ANTIBANDING_50HZ:Ljava/lang/String; = "50hz"

.field public static final ANTIBANDING_60HZ:Ljava/lang/String; = "60hz"

.field public static final ANTIBANDING_AUTO:Ljava/lang/String; = "auto"

.field public static final ANTIBANDING_OFF:Ljava/lang/String; = "off"

.field public static final AUTO_EXPOSURE_CENTER_WEIGHTED:Ljava/lang/String; = "center-weighted"

.field public static final AUTO_EXPOSURE_FRAME_AVG:Ljava/lang/String; = "frame-average"

.field public static final AUTO_EXPOSURE_SPOT_METERING:Ljava/lang/String; = "spot-metering"

.field private static final BURST_SHOT_OFF:Ljava/lang/String; = "off"

.field private static final BURST_SHOT_ON:Ljava/lang/String; = "on"

.field public static final CONTINUOUS_AF_OFF:Ljava/lang/String; = "caf-off"

.field public static final CONTINUOUS_AF_ON:Ljava/lang/String; = "caf-on"

.field public static final DENOISE_OFF:Ljava/lang/String; = "denoise-off"

.field public static final DENOISE_ON:Ljava/lang/String; = "denoise-on"

.field public static final EFFECT_AQUA:Ljava/lang/String; = "aqua"

.field public static final EFFECT_BLACKBOARD:Ljava/lang/String; = "blackboard"

.field public static final EFFECT_MONO:Ljava/lang/String; = "mono"

.field public static final EFFECT_NEGATIVE:Ljava/lang/String; = "negative"

.field public static final EFFECT_NONE:Ljava/lang/String; = "none"

.field public static final EFFECT_POSTERIZE:Ljava/lang/String; = "posterize"

.field public static final EFFECT_SEPIA:Ljava/lang/String; = "sepia"

.field public static final EFFECT_SOLARIZE:Ljava/lang/String; = "solarize"

.field public static final EFFECT_WHITEBOARD:Ljava/lang/String; = "whiteboard"

.field public static final FACE_DETECTION_OFF:Ljava/lang/String; = "off"

.field public static final FACE_DETECTION_ON:Ljava/lang/String; = "on"

.field private static final FALSE:Ljava/lang/String; = "false"

.field public static final FLASH_MODE_AUTO:Ljava/lang/String; = "auto"

.field public static final FLASH_MODE_OFF:Ljava/lang/String; = "off"

.field public static final FLASH_MODE_ON:Ljava/lang/String; = "on"

.field public static final FLASH_MODE_RED_EYE:Ljava/lang/String; = "red-eye"

.field public static final FLASH_MODE_TORCH:Ljava/lang/String; = "torch"

.field public static final FOCUS_DISTANCE_FAR_INDEX:I = 0x2

.field public static final FOCUS_DISTANCE_NEAR_INDEX:I = 0x0

.field public static final FOCUS_DISTANCE_OPTIMAL_INDEX:I = 0x1

.field public static final FOCUS_MODE_AUTO:Ljava/lang/String; = "auto"

.field public static final FOCUS_MODE_CONTINUOUS_PICTURE:Ljava/lang/String; = "continuous-picture"

.field public static final FOCUS_MODE_CONTINUOUS_VIDEO:Ljava/lang/String; = "continuous-video"

.field public static final FOCUS_MODE_EDOF:Ljava/lang/String; = "edof"

.field public static final FOCUS_MODE_FIXED:Ljava/lang/String; = "fixed"

.field public static final FOCUS_MODE_INFINITY:Ljava/lang/String; = "infinity"

.field public static final FOCUS_MODE_MACRO:Ljava/lang/String; = "macro"

.field public static final FOCUS_MODE_NORMAL:Ljava/lang/String; = "normal"

.field public static final HISTOGRAM_DISABLE:Ljava/lang/String; = "disable"

.field public static final HISTOGRAM_ENABLE:Ljava/lang/String; = "enable"

.field public static final ISO_100:Ljava/lang/String; = "ISO100"

.field public static final ISO_1600:Ljava/lang/String; = "ISO1600"

.field public static final ISO_200:Ljava/lang/String; = "ISO200"

.field public static final ISO_400:Ljava/lang/String; = "ISO400"

.field public static final ISO_800:Ljava/lang/String; = "ISO800"

.field public static final ISO_AUTO:Ljava/lang/String; = "auto"

.field public static final ISO_HJR:Ljava/lang/String; = "ISO_HJR"

.field private static final KEY_ANTIBANDING:Ljava/lang/String; = "antibanding"

.field private static final KEY_AUTO_EXPOSURE_LOCK:Ljava/lang/String; = "auto-exposure-lock"

.field private static final KEY_AUTO_EXPOSURE_LOCK_SUPPORTED:Ljava/lang/String; = "auto-exposure-lock-supported"

.field private static final KEY_AUTO_WHITEBALANCE_LOCK:Ljava/lang/String; = "auto-whitebalance-lock"

.field private static final KEY_AUTO_WHITEBALANCE_LOCK_SUPPORTED:Ljava/lang/String; = "auto-whitebalance-lock-supported"

.field private static final KEY_BURST_SHOT:Ljava/lang/String; = "burst-shot"

.field private static final KEY_EFFECT:Ljava/lang/String; = "effect"

.field private static final KEY_EXPOSURE_COMPENSATION:Ljava/lang/String; = "exposure-compensation"

.field private static final KEY_EXPOSURE_COMPENSATION_STEP:Ljava/lang/String; = "exposure-compensation-step"

.field private static final KEY_FLASH_MODE:Ljava/lang/String; = "flash-mode"

.field private static final KEY_FOCAL_LENGTH:Ljava/lang/String; = "focal-length"

.field private static final KEY_FOCUS_AREAS:Ljava/lang/String; = "focus-areas"

.field private static final KEY_FOCUS_DISTANCES:Ljava/lang/String; = "focus-distances"

.field private static final KEY_FOCUS_MODE:Ljava/lang/String; = "focus-mode"

.field public static final KEY_FOCUS_MODE_OBJECT_TRACKING:Ljava/lang/String; = "object-tracking"

.field private static final KEY_GPS_ALTITUDE:Ljava/lang/String; = "gps-altitude"

.field private static final KEY_GPS_LATITUDE:Ljava/lang/String; = "gps-latitude"

.field private static final KEY_GPS_LONGITUDE:Ljava/lang/String; = "gps-longitude"

.field private static final KEY_GPS_PROCESSING_METHOD:Ljava/lang/String; = "gps-processing-method"

.field private static final KEY_GPS_TIMESTAMP:Ljava/lang/String; = "gps-timestamp"

.field private static final KEY_HORIZONTAL_VIEW_ANGLE:Ljava/lang/String; = "horizontal-view-angle"

.field private static final KEY_JPEG_QUALITY:Ljava/lang/String; = "jpeg-quality"

.field private static final KEY_JPEG_THUMBNAIL_HEIGHT:Ljava/lang/String; = "jpeg-thumbnail-height"

.field private static final KEY_JPEG_THUMBNAIL_QUALITY:Ljava/lang/String; = "jpeg-thumbnail-quality"

.field private static final KEY_JPEG_THUMBNAIL_SIZE:Ljava/lang/String; = "jpeg-thumbnail-size"

.field private static final KEY_JPEG_THUMBNAIL_WIDTH:Ljava/lang/String; = "jpeg-thumbnail-width"

.field public static final KEY_LG_MULTI_WINDOW_FOCUS_AREA:Ljava/lang/String; = "multi-window-focus-area"

.field private static final KEY_MAX_EXPOSURE_COMPENSATION:Ljava/lang/String; = "max-exposure-compensation"

.field private static final KEY_MAX_NUM_DETECTED_FACES_HW:Ljava/lang/String; = "max-num-detected-faces-hw"

.field private static final KEY_MAX_NUM_DETECTED_FACES_SW:Ljava/lang/String; = "max-num-detected-faces-sw"

.field private static final KEY_MAX_NUM_FOCUS_AREAS:Ljava/lang/String; = "max-num-focus-areas"

.field private static final KEY_MAX_NUM_METERING_AREAS:Ljava/lang/String; = "max-num-metering-areas"

.field private static final KEY_MAX_ZOOM:Ljava/lang/String; = "max-zoom"

.field private static final KEY_METERING_AREAS:Ljava/lang/String; = "metering-areas"

.field private static final KEY_MIN_EXPOSURE_COMPENSATION:Ljava/lang/String; = "min-exposure-compensation"

.field private static final KEY_PICTURE_FORMAT:Ljava/lang/String; = "picture-format"

.field private static final KEY_PICTURE_SIZE:Ljava/lang/String; = "picture-size"

.field private static final KEY_PREFERRED_PREVIEW_SIZE_FOR_VIDEO:Ljava/lang/String; = "preferred-preview-size-for-video"

.field private static final KEY_PREVIEW_FORMAT:Ljava/lang/String; = "preview-format"

.field private static final KEY_PREVIEW_FPS_RANGE:Ljava/lang/String; = "preview-fps-range"

.field private static final KEY_PREVIEW_FRAME_RATE:Ljava/lang/String; = "preview-frame-rate"

.field private static final KEY_PREVIEW_SIZE:Ljava/lang/String; = "preview-size"

.field public static final KEY_QC_AE_BRACKET_HDR:Ljava/lang/String; = "ae-bracket-hdr"

.field private static final KEY_QC_AUTO_EXPOSURE:Ljava/lang/String; = "auto-exposure"

.field private static final KEY_QC_CAMERA_MODE:Ljava/lang/String; = "camera-mode"

.field private static final KEY_QC_CONTINUOUS_AF:Ljava/lang/String; = "continuous-af"

.field private static final KEY_QC_CONTRAST:Ljava/lang/String; = "contrast"

.field private static final KEY_QC_DENOISE:Ljava/lang/String; = "denoise"

.field private static final KEY_QC_EXIF_DATETIME:Ljava/lang/String; = "exif-datetime"

.field private static final KEY_QC_FACE_DETECTION:Ljava/lang/String; = "face-detection"

.field private static final KEY_QC_GPS_ALTITUDE_REF:Ljava/lang/String; = "gps-altitude-ref"

.field private static final KEY_QC_GPS_LATITUDE_REF:Ljava/lang/String; = "gps-latitude-ref"

.field private static final KEY_QC_GPS_LONGITUDE_REF:Ljava/lang/String; = "gps-longitude-ref"

.field private static final KEY_QC_GPS_STATUS:Ljava/lang/String; = "gps-status"

.field private static final KEY_QC_HFR_SIZE:Ljava/lang/String; = "hfr-size"

.field private static final KEY_QC_HISTOGRAM:Ljava/lang/String; = "histogram"

.field private static final KEY_QC_ISO_MODE:Ljava/lang/String; = "iso"

.field private static final KEY_QC_LENSSHADE:Ljava/lang/String; = "lensshade"

.field private static final KEY_QC_MAX_CONTRAST:Ljava/lang/String; = "max-contrast"

.field private static final KEY_QC_MAX_SATURATION:Ljava/lang/String; = "max-saturation"

.field private static final KEY_QC_MAX_SHARPNESS:Ljava/lang/String; = "max-sharpness"

.field private static final KEY_QC_MEMORY_COLOR_ENHANCEMENT:Ljava/lang/String; = "mce"

.field private static final KEY_QC_POWER_MODE:Ljava/lang/String; = "power-mode"

.field private static final KEY_QC_POWER_MODE_SUPPORTED:Ljava/lang/String; = "power-mode-supported"

.field private static final KEY_QC_PREVIEW_FRAME_RATE_AUTO_MODE:Ljava/lang/String; = "frame-rate-auto"

.field private static final KEY_QC_PREVIEW_FRAME_RATE_FIXED_MODE:Ljava/lang/String; = "frame-rate-fixed"

.field private static final KEY_QC_PREVIEW_FRAME_RATE_MODE:Ljava/lang/String; = "preview-frame-rate-mode"

.field private static final KEY_QC_REDEYE_REDUCTION:Ljava/lang/String; = "redeye-reduction"

.field private static final KEY_QC_SATURATION:Ljava/lang/String; = "saturation"

.field private static final KEY_QC_SCENE_DETECT:Ljava/lang/String; = "scene-detect"

.field private static final KEY_QC_SELECTABLE_ZONE_AF:Ljava/lang/String; = "selectable-zone-af"

.field private static final KEY_QC_SHARPNESS:Ljava/lang/String; = "sharpness"

.field private static final KEY_QC_SKIN_TONE_ENHANCEMENT:Ljava/lang/String; = "skinToneEnhancement"

.field private static final KEY_QC_TOUCH_AF_AEC:Ljava/lang/String; = "touch-af-aec"

.field private static final KEY_QC_TOUCH_INDEX_AEC:Ljava/lang/String; = "touch-index-aec"

.field private static final KEY_QC_TOUCH_INDEX_AF:Ljava/lang/String; = "touch-index-af"

.field private static final KEY_QC_VIDEO_HIGH_FRAME_RATE:Ljava/lang/String; = "video-hfr"

.field private static final KEY_QC_ZSL:Ljava/lang/String; = "zsl"

.field private static final KEY_RECORDING_HINT:Ljava/lang/String; = "recording-hint"

.field private static final KEY_ROTATION:Ljava/lang/String; = "rotation"

.field private static final KEY_SCENE_MODE:Ljava/lang/String; = "scene-mode"

.field private static final KEY_SMOOTH_ZOOM_SUPPORTED:Ljava/lang/String; = "smooth-zoom-supported"

.field private static final KEY_VERTICAL_VIEW_ANGLE:Ljava/lang/String; = "vertical-view-angle"

.field private static final KEY_VIDEO_SIZE:Ljava/lang/String; = "video-size"

.field private static final KEY_VIDEO_SNAPSHOT_SUPPORTED:Ljava/lang/String; = "video-snapshot-supported"

.field private static final KEY_VIDEO_STABILIZATION:Ljava/lang/String; = "video-stabilization"

.field private static final KEY_VIDEO_STABILIZATION_SUPPORTED:Ljava/lang/String; = "video-stabilization-supported"

.field private static final KEY_WHITE_BALANCE:Ljava/lang/String; = "whitebalance"

.field private static final KEY_ZOOM:Ljava/lang/String; = "zoom"

.field private static final KEY_ZOOM_RATIOS:Ljava/lang/String; = "zoom-ratios"

.field private static final KEY_ZOOM_SUPPORTED:Ljava/lang/String; = "zoom-supported"

.field public static final LENSSHADE_DISABLE:Ljava/lang/String; = "disable"

.field public static final LENSSHADE_ENABLE:Ljava/lang/String; = "enable"

.field public static final LOW_POWER:Ljava/lang/String; = "Low_Power"

.field public static final MCE_DISABLE:Ljava/lang/String; = "disable"

.field public static final MCE_ENABLE:Ljava/lang/String; = "enable"

.field public static final NORMAL_POWER:Ljava/lang/String; = "Normal_Power"

.field public static final OBJECT_TRACKING_OFF:Ljava/lang/String; = "off"

.field public static final OBJECT_TRACKING_ON:Ljava/lang/String; = "on"

.field public static final PARAMETER_SUPERZOOM:Ljava/lang/String; = "superzoom"

.field private static final PIXEL_FORMAT_BAYER_RGGB:Ljava/lang/String; = "bayer-rggb"

.field private static final PIXEL_FORMAT_JPEG:Ljava/lang/String; = "jpeg"

.field private static final PIXEL_FORMAT_NV12:Ljava/lang/String; = "nv12"

.field private static final PIXEL_FORMAT_RAW:Ljava/lang/String; = "raw"

.field private static final PIXEL_FORMAT_RGB565:Ljava/lang/String; = "rgb565"

.field private static final PIXEL_FORMAT_YUV420P:Ljava/lang/String; = "yuv420p"

.field private static final PIXEL_FORMAT_YUV420SP:Ljava/lang/String; = "yuv420sp"

.field private static final PIXEL_FORMAT_YUV420SP_ADRENO:Ljava/lang/String; = "yuv420sp-adreno"

.field private static final PIXEL_FORMAT_YUV422I:Ljava/lang/String; = "yuv422i-yuyv"

.field private static final PIXEL_FORMAT_YUV422SP:Ljava/lang/String; = "yuv422sp"

.field private static final PIXEL_FORMAT_YV12:Ljava/lang/String; = "yv12"

.field public static final PREVIEW_FPS_MAX_INDEX:I = 0x1

.field public static final PREVIEW_FPS_MIN_INDEX:I = 0x0

.field public static final REDEYE_REDUCTION_DISABLE:Ljava/lang/String; = "disable"

.field public static final REDEYE_REDUCTION_ENABLE:Ljava/lang/String; = "enable"

.field public static final SCENE_DETECT_OFF:Ljava/lang/String; = "off"

.field public static final SCENE_DETECT_ON:Ljava/lang/String; = "on"

.field public static final SCENE_MODE_ACTION:Ljava/lang/String; = "action"

.field public static final SCENE_MODE_ASD:Ljava/lang/String; = "asd"

.field public static final SCENE_MODE_AUTO:Ljava/lang/String; = "auto"

.field public static final SCENE_MODE_BACKLIGHT:Ljava/lang/String; = "backlight"

.field public static final SCENE_MODE_BARCODE:Ljava/lang/String; = "barcode"

.field public static final SCENE_MODE_BEACH:Ljava/lang/String; = "beach"

.field public static final SCENE_MODE_CANDLELIGHT:Ljava/lang/String; = "candlelight"

.field public static final SCENE_MODE_FIREWORKS:Ljava/lang/String; = "fireworks"

.field public static final SCENE_MODE_FLOWERS:Ljava/lang/String; = "flowers"

.field public static final SCENE_MODE_HDR:Ljava/lang/String; = "hdr"

.field public static final SCENE_MODE_LANDSCAPE:Ljava/lang/String; = "landscape"

.field public static final SCENE_MODE_NIGHT:Ljava/lang/String; = "night"

.field public static final SCENE_MODE_NIGHT_PORTRAIT:Ljava/lang/String; = "night-portrait"

.field public static final SCENE_MODE_PARTY:Ljava/lang/String; = "party"

.field public static final SCENE_MODE_PORTRAIT:Ljava/lang/String; = "portrait"

.field public static final SCENE_MODE_SNOW:Ljava/lang/String; = "snow"

.field public static final SCENE_MODE_SPORTS:Ljava/lang/String; = "sports"

.field public static final SCENE_MODE_STEADYPHOTO:Ljava/lang/String; = "steadyphoto"

.field public static final SCENE_MODE_SUNSET:Ljava/lang/String; = "sunset"

.field public static final SCENE_MODE_THEATRE:Ljava/lang/String; = "theatre"

.field public static final SELECTABLE_ZONE_AF_AUTO:Ljava/lang/String; = "auto"

.field public static final SELECTABLE_ZONE_AF_CENTER_WEIGHTED:Ljava/lang/String; = "center-weighted"

.field public static final SELECTABLE_ZONE_AF_FRAME_AVERAGE:Ljava/lang/String; = "frame-average"

.field public static final SELECTABLE_ZONE_AF_SPOTMETERING:Ljava/lang/String; = "spot-metering"

.field public static final SKIN_TONE_ENHANCEMENT_DISABLE:Ljava/lang/String; = "disable"

.field public static final SKIN_TONE_ENHANCEMENT_ENABLE:Ljava/lang/String; = "enable"

.field private static final SUPPORTED_VALUES_SUFFIX:Ljava/lang/String; = "-values"

.field public static final TOUCH_AF_AEC_OFF:Ljava/lang/String; = "touch-off"

.field public static final TOUCH_AF_AEC_ON:Ljava/lang/String; = "touch-on"

.field private static final TRUE:Ljava/lang/String; = "true"

.field public static final VIDEO_HFR_2X:Ljava/lang/String; = "60"

.field public static final VIDEO_HFR_3X:Ljava/lang/String; = "90"

.field public static final VIDEO_HFR_4X:Ljava/lang/String; = "120"

.field public static final VIDEO_HFR_OFF:Ljava/lang/String; = "off"

.field public static final WHITE_BALANCE_AUTO:Ljava/lang/String; = "auto"

.field public static final WHITE_BALANCE_CLOUDY_DAYLIGHT:Ljava/lang/String; = "cloudy-daylight"

.field public static final WHITE_BALANCE_DAYLIGHT:Ljava/lang/String; = "daylight"

.field public static final WHITE_BALANCE_FLUORESCENT:Ljava/lang/String; = "fluorescent"

.field public static final WHITE_BALANCE_INCANDESCENT:Ljava/lang/String; = "incandescent"

.field public static final WHITE_BALANCE_SHADE:Ljava/lang/String; = "shade"

.field public static final WHITE_BALANCE_TWILIGHT:Ljava/lang/String; = "twilight"

.field public static final WHITE_BALANCE_WARM_FLUORESCENT:Ljava/lang/String; = "warm-fluorescent"

.field public static final ZSL_OFF:Ljava/lang/String; = "off"

.field public static final ZSL_ON:Ljava/lang/String; = "on"


# instance fields
.field private mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/hardware/Camera;


# direct methods
.method private constructor <init>(Landroid/hardware/Camera;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 2421
    iput-object p1, p0, Landroid/hardware/Camera$Parameters;->this$0:Landroid/hardware/Camera;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2422
    new-instance v0, Ljava/util/HashMap;

    #@7
    const/16 v1, 0x40

    #@9
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    #@c
    iput-object v0, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@e
    .line 2423
    return-void
.end method

.method synthetic constructor <init>(Landroid/hardware/Camera;Landroid/hardware/Camera$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 2033
    invoke-direct {p0, p1}, Landroid/hardware/Camera$Parameters;-><init>(Landroid/hardware/Camera;)V

    #@3
    return-void
.end method

.method private cameraFormatForPixelFormat(I)Ljava/lang/String;
    .registers 3
    .parameter "pixel_format"

    #@0
    .prologue
    .line 2995
    sparse-switch p1, :sswitch_data_20

    #@3
    .line 3003
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 2996
    :sswitch_5
    const-string/jumbo v0, "yuv422sp"

    #@8
    goto :goto_4

    #@9
    .line 2997
    :sswitch_9
    const-string/jumbo v0, "yuv420sp"

    #@c
    goto :goto_4

    #@d
    .line 2998
    :sswitch_d
    const-string/jumbo v0, "yuv422i-yuyv"

    #@10
    goto :goto_4

    #@11
    .line 2999
    :sswitch_11
    const-string/jumbo v0, "yuv420p"

    #@14
    goto :goto_4

    #@15
    .line 3000
    :sswitch_15
    const-string/jumbo v0, "rgb565"

    #@18
    goto :goto_4

    #@19
    .line 3001
    :sswitch_19
    const-string/jumbo v0, "jpeg"

    #@1c
    goto :goto_4

    #@1d
    .line 3002
    :sswitch_1d
    const-string v0, "bayer-rggb"

    #@1f
    goto :goto_4

    #@20
    .line 2995
    :sswitch_data_20
    .sparse-switch
        0x4 -> :sswitch_15
        0x10 -> :sswitch_5
        0x11 -> :sswitch_9
        0x14 -> :sswitch_d
        0x100 -> :sswitch_19
        0x200 -> :sswitch_1d
        0x32315659 -> :sswitch_11
    .end sparse-switch
.end method

.method private getFloat(Ljava/lang/String;F)F
    .registers 5
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4063
    :try_start_0
    iget-object v1, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Ljava/lang/String;

    #@8
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result p2

    #@c
    .line 4065
    .end local p2
    :goto_c
    return p2

    #@d
    .line 4064
    .restart local p2
    :catch_d
    move-exception v0

    #@e
    .line 4065
    .local v0, ex:Ljava/lang/NumberFormatException;
    goto :goto_c
.end method

.method private getInt(Ljava/lang/String;I)I
    .registers 5
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 4072
    :try_start_0
    iget-object v1, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Ljava/lang/String;

    #@8
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result p2

    #@c
    .line 4074
    .end local p2
    :goto_c
    return p2

    #@d
    .line 4073
    .restart local p2
    :catch_d
    move-exception v0

    #@e
    .line 4074
    .local v0, ex:Ljava/lang/NumberFormatException;
    goto :goto_c
.end method

.method private pixelFormatForCameraFormat(Ljava/lang/String;)I
    .registers 4
    .parameter "format"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3008
    if-nez p1, :cond_4

    #@3
    .line 3029
    :cond_3
    :goto_3
    return v0

    #@4
    .line 3011
    :cond_4
    const-string/jumbo v1, "yuv422sp"

    #@7
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_10

    #@d
    .line 3012
    const/16 v0, 0x10

    #@f
    goto :goto_3

    #@10
    .line 3014
    :cond_10
    const-string/jumbo v1, "yuv420sp"

    #@13
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_1c

    #@19
    .line 3015
    const/16 v0, 0x11

    #@1b
    goto :goto_3

    #@1c
    .line 3017
    :cond_1c
    const-string/jumbo v1, "yuv422i-yuyv"

    #@1f
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_28

    #@25
    .line 3018
    const/16 v0, 0x14

    #@27
    goto :goto_3

    #@28
    .line 3020
    :cond_28
    const-string/jumbo v1, "yuv420p"

    #@2b
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_35

    #@31
    .line 3021
    const v0, 0x32315659

    #@34
    goto :goto_3

    #@35
    .line 3023
    :cond_35
    const-string/jumbo v1, "rgb565"

    #@38
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_40

    #@3e
    .line 3024
    const/4 v0, 0x4

    #@3f
    goto :goto_3

    #@40
    .line 3026
    :cond_40
    const-string/jumbo v1, "jpeg"

    #@43
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_3

    #@49
    .line 3027
    const/16 v0, 0x100

    #@4b
    goto :goto_3
.end method

.method private same(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "s1"
    .parameter "s2"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 4172
    if-nez p1, :cond_6

    #@3
    if-nez p2, :cond_6

    #@5
    .line 4174
    :cond_5
    :goto_5
    return v0

    #@6
    .line 4173
    :cond_6
    if-eqz p1, :cond_e

    #@8
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_5

    #@e
    .line 4174
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_5
.end method

.method private set(Ljava/lang/String;Ljava/util/List;)V
    .registers 9
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, areas:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Area;>;"
    const/16 v5, 0x2c

    #@2
    .line 2516
    if-nez p2, :cond_a

    #@4
    .line 2517
    const-string v4, "(0,0,0,0,0)"

    #@6
    invoke-virtual {p0, p1, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 2538
    :goto_9
    return-void

    #@a
    .line 2519
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    .line 2520
    .local v1, buffer:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@10
    .local v2, i:I
    :goto_10
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@13
    move-result v4

    #@14
    if-ge v2, v4, :cond_5b

    #@16
    .line 2521
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/hardware/Camera$Area;

    #@1c
    .line 2522
    .local v0, area:Landroid/hardware/Camera$Area;
    iget-object v3, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@1e
    .line 2523
    .local v3, rect:Landroid/graphics/Rect;
    const/16 v4, 0x28

    #@20
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@23
    .line 2524
    iget v4, v3, Landroid/graphics/Rect;->left:I

    #@25
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    .line 2525
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    .line 2526
    iget v4, v3, Landroid/graphics/Rect;->top:I

    #@2d
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    .line 2527
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@33
    .line 2528
    iget v4, v3, Landroid/graphics/Rect;->right:I

    #@35
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    .line 2529
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3b
    .line 2530
    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    #@3d
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    .line 2531
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@43
    .line 2532
    iget v4, v0, Landroid/hardware/Camera$Area;->weight:I

    #@45
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    .line 2533
    const/16 v4, 0x29

    #@4a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4d
    .line 2534
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@50
    move-result v4

    #@51
    add-int/lit8 v4, v4, -0x1

    #@53
    if-eq v2, v4, :cond_58

    #@55
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@58
    .line 2520
    :cond_58
    add-int/lit8 v2, v2, 0x1

    #@5a
    goto :goto_10

    #@5b
    .line 2536
    .end local v0           #area:Landroid/hardware/Camera$Area;
    .end local v3           #rect:Landroid/graphics/Rect;
    :cond_5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {p0, p1, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    goto :goto_9
.end method

.method private split(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 7
    .parameter "str"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4011
    if-nez p1, :cond_4

    #@2
    const/4 v3, 0x0

    #@3
    .line 4019
    :cond_3
    return-object v3

    #@4
    .line 4013
    :cond_4
    new-instance v2, Landroid/text/TextUtils$SimpleStringSplitter;

    #@6
    const/16 v4, 0x2c

    #@8
    invoke-direct {v2, v4}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@b
    .line 4014
    .local v2, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v2, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@e
    .line 4015
    new-instance v3, Ljava/util/ArrayList;

    #@10
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@13
    .line 4016
    .local v3, substrings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v2}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v0

    #@17
    .local v0, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_3

    #@1d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Ljava/lang/String;

    #@23
    .line 4017
    .local v1, s:Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    goto :goto_17
.end method

.method private splitArea(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 15
    .parameter "str"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v11, 0x0

    #@3
    .line 4139
    if-eqz p1, :cond_1b

    #@5
    invoke-virtual {p1, v11}, Ljava/lang/String;->charAt(I)C

    #@8
    move-result v7

    #@9
    const/16 v8, 0x28

    #@b
    if-ne v7, v8, :cond_1b

    #@d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@10
    move-result v7

    #@11
    add-int/lit8 v7, v7, -0x1

    #@13
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v7

    #@17
    const/16 v8, 0x29

    #@19
    if-eq v7, v8, :cond_35

    #@1b
    .line 4141
    :cond_1b
    const-string v7, "Camera"

    #@1d
    new-instance v8, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v9, "Invalid area string="

    #@24
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v8

    #@28
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v8

    #@30
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    move-object v5, v6

    #@34
    .line 4168
    :cond_34
    :goto_34
    return-object v5

    #@35
    .line 4145
    :cond_35
    new-instance v5, Ljava/util/ArrayList;

    #@37
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@3a
    .line 4146
    .local v5, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Camera$Area;>;"
    const/4 v3, 0x1

    #@3b
    .line 4147
    .local v3, fromIndex:I
    const/4 v7, 0x5

    #@3c
    new-array v1, v7, [I

    #@3e
    .line 4149
    .local v1, array:[I
    :cond_3e
    const-string v7, "),("

    #@40
    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    #@43
    move-result v2

    #@44
    .line 4150
    .local v2, endIndex:I
    const/4 v7, -0x1

    #@45
    if-ne v2, v7, :cond_4d

    #@47
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4a
    move-result v7

    #@4b
    add-int/lit8 v2, v7, -0x1

    #@4d
    .line 4151
    :cond_4d
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@50
    move-result-object v7

    #@51
    invoke-direct {p0, v7, v1}, Landroid/hardware/Camera$Parameters;->splitInt(Ljava/lang/String;[I)V

    #@54
    .line 4152
    new-instance v4, Landroid/graphics/Rect;

    #@56
    aget v7, v1, v11

    #@58
    aget v8, v1, v12

    #@5a
    const/4 v9, 0x2

    #@5b
    aget v9, v1, v9

    #@5d
    const/4 v10, 0x3

    #@5e
    aget v10, v1, v10

    #@60
    invoke-direct {v4, v7, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    #@63
    .line 4153
    .local v4, rect:Landroid/graphics/Rect;
    new-instance v7, Landroid/hardware/Camera$Area;

    #@65
    const/4 v8, 0x4

    #@66
    aget v8, v1, v8

    #@68
    invoke-direct {v7, v4, v8}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    #@6b
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6e
    .line 4154
    add-int/lit8 v3, v2, 0x3

    #@70
    .line 4155
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@73
    move-result v7

    #@74
    add-int/lit8 v7, v7, -0x1

    #@76
    if-ne v2, v7, :cond_3e

    #@78
    .line 4157
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@7b
    move-result v7

    #@7c
    if-nez v7, :cond_80

    #@7e
    move-object v5, v6

    #@7f
    goto :goto_34

    #@80
    .line 4159
    :cond_80
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@83
    move-result v7

    #@84
    if-ne v7, v12, :cond_34

    #@86
    .line 4160
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@89
    move-result-object v0

    #@8a
    check-cast v0, Landroid/hardware/Camera$Area;

    #@8c
    .line 4161
    .local v0, area:Landroid/hardware/Camera$Area;
    iget-object v4, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@8e
    .line 4162
    iget v7, v4, Landroid/graphics/Rect;->left:I

    #@90
    if-nez v7, :cond_34

    #@92
    iget v7, v4, Landroid/graphics/Rect;->top:I

    #@94
    if-nez v7, :cond_34

    #@96
    iget v7, v4, Landroid/graphics/Rect;->right:I

    #@98
    if-nez v7, :cond_34

    #@9a
    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    #@9c
    if-nez v7, :cond_34

    #@9e
    iget v7, v0, Landroid/hardware/Camera$Area;->weight:I

    #@a0
    if-nez v7, :cond_34

    #@a2
    move-object v5, v6

    #@a3
    .line 4164
    goto :goto_34
.end method

.method private splitCoordinate(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .parameter "str"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/Camera$Coordinate;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 5230
    if-nez p1, :cond_5

    #@3
    move-object v1, v5

    #@4
    .line 5239
    :cond_4
    :goto_4
    return-object v1

    #@5
    .line 5231
    :cond_5
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@7
    const/16 v6, 0x2c

    #@9
    invoke-direct {v4, v6}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@c
    .line 5232
    .local v4, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v4, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@f
    .line 5233
    new-instance v1, Ljava/util/ArrayList;

    #@11
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@14
    .line 5234
    .local v1, coordinateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Camera$Coordinate;>;"
    invoke-interface {v4}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v2

    #@18
    .local v2, i$:Ljava/util/Iterator;
    :cond_18
    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_2e

    #@1e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v3

    #@22
    check-cast v3, Ljava/lang/String;

    #@24
    .line 5235
    .local v3, s:Ljava/lang/String;
    invoke-direct {p0, v3}, Landroid/hardware/Camera$Parameters;->strToCoordinate(Ljava/lang/String;)Landroid/hardware/Camera$Coordinate;

    #@27
    move-result-object v0

    #@28
    .line 5236
    .local v0, coordinate:Landroid/hardware/Camera$Coordinate;
    if-eqz v0, :cond_18

    #@2a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    goto :goto_18

    #@2e
    .line 5238
    .end local v0           #coordinate:Landroid/hardware/Camera$Coordinate;
    .end local v3           #s:Ljava/lang/String;
    :cond_2e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v6

    #@32
    if-nez v6, :cond_4

    #@34
    move-object v1, v5

    #@35
    goto :goto_4
.end method

.method private splitFloat(Ljava/lang/String;[F)V
    .registers 9
    .parameter "str"
    .parameter "output"

    #@0
    .prologue
    .line 4050
    if-nez p1, :cond_3

    #@2
    .line 4058
    :cond_2
    return-void

    #@3
    .line 4052
    :cond_3
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@5
    const/16 v5, 0x2c

    #@7
    invoke-direct {v4, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@a
    .line 4053
    .local v4, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v4, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@d
    .line 4054
    const/4 v1, 0x0

    #@e
    .line 4055
    .local v1, index:I
    invoke-interface {v4}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_2

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    check-cast v3, Ljava/lang/String;

    #@1e
    .line 4056
    .local v3, s:Ljava/lang/String;
    add-int/lit8 v2, v1, 0x1

    #@20
    .end local v1           #index:I
    .local v2, index:I
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@23
    move-result v5

    #@24
    aput v5, p2, v1

    #@26
    move v1, v2

    #@27
    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_12
.end method

.method private splitInt(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 8
    .parameter "str"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4025
    if-nez p1, :cond_5

    #@3
    move-object v3, v4

    #@4
    .line 4034
    :cond_4
    :goto_4
    return-object v3

    #@5
    .line 4027
    :cond_5
    new-instance v2, Landroid/text/TextUtils$SimpleStringSplitter;

    #@7
    const/16 v5, 0x2c

    #@9
    invoke-direct {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@c
    .line 4028
    .local v2, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v2, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@f
    .line 4029
    new-instance v3, Ljava/util/ArrayList;

    #@11
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@14
    .line 4030
    .local v3, substrings:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface {v2}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v0

    #@18
    .local v0, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_30

    #@1e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Ljava/lang/String;

    #@24
    .line 4031
    .local v1, s:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@27
    move-result v5

    #@28
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    goto :goto_18

    #@30
    .line 4033
    .end local v1           #s:Ljava/lang/String;
    :cond_30
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v5

    #@34
    if-nez v5, :cond_4

    #@36
    move-object v3, v4

    #@37
    goto :goto_4
.end method

.method private splitInt(Ljava/lang/String;[I)V
    .registers 9
    .parameter "str"
    .parameter "output"

    #@0
    .prologue
    .line 4038
    if-nez p1, :cond_3

    #@2
    .line 4046
    :cond_2
    return-void

    #@3
    .line 4040
    :cond_3
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@5
    const/16 v5, 0x2c

    #@7
    invoke-direct {v4, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@a
    .line 4041
    .local v4, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v4, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@d
    .line 4042
    const/4 v1, 0x0

    #@e
    .line 4043
    .local v1, index:I
    invoke-interface {v4}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_2

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v3

    #@1c
    check-cast v3, Ljava/lang/String;

    #@1e
    .line 4044
    .local v3, s:Ljava/lang/String;
    add-int/lit8 v2, v1, 0x1

    #@20
    .end local v1           #index:I
    .local v2, index:I
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23
    move-result v5

    #@24
    aput v5, p2, v1

    #@26
    move v1, v2

    #@27
    .end local v2           #index:I
    .restart local v1       #index:I
    goto :goto_12
.end method

.method private splitRange(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 10
    .parameter "str"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<[I>;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4114
    if-eqz p1, :cond_1a

    #@3
    const/4 v5, 0x0

    #@4
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    #@7
    move-result v5

    #@8
    const/16 v6, 0x28

    #@a
    if-ne v5, v6, :cond_1a

    #@c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@f
    move-result v5

    #@10
    add-int/lit8 v5, v5, -0x1

    #@12
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v5

    #@16
    const/16 v6, 0x29

    #@18
    if-eq v5, v6, :cond_34

    #@1a
    .line 4116
    :cond_1a
    const-string v5, "Camera"

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "Invalid range list string="

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    move-object v3, v4

    #@33
    .line 4132
    :cond_33
    :goto_33
    return-object v3

    #@34
    .line 4120
    :cond_34
    new-instance v3, Ljava/util/ArrayList;

    #@36
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@39
    .line 4121
    .local v3, rangeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[I>;"
    const/4 v1, 0x1

    #@3a
    .line 4123
    .local v1, fromIndex:I
    :cond_3a
    const/4 v5, 0x2

    #@3b
    new-array v2, v5, [I

    #@3d
    .line 4124
    .local v2, range:[I
    const-string v5, "),("

    #@3f
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    #@42
    move-result v0

    #@43
    .line 4125
    .local v0, endIndex:I
    const/4 v5, -0x1

    #@44
    if-ne v0, v5, :cond_4c

    #@46
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@49
    move-result v5

    #@4a
    add-int/lit8 v0, v5, -0x1

    #@4c
    .line 4126
    :cond_4c
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4f
    move-result-object v5

    #@50
    invoke-direct {p0, v5, v2}, Landroid/hardware/Camera$Parameters;->splitInt(Ljava/lang/String;[I)V

    #@53
    .line 4127
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@56
    .line 4128
    add-int/lit8 v1, v0, 0x3

    #@58
    .line 4129
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5b
    move-result v5

    #@5c
    add-int/lit8 v5, v5, -0x1

    #@5e
    if-ne v0, v5, :cond_3a

    #@60
    .line 4131
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@63
    move-result v5

    #@64
    if-nez v5, :cond_33

    #@66
    move-object v3, v4

    #@67
    goto :goto_33
.end method

.method private splitSize(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .parameter "str"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 4081
    if-nez p1, :cond_5

    #@3
    move-object v3, v5

    #@4
    .line 4091
    :cond_4
    :goto_4
    return-object v3

    #@5
    .line 4083
    :cond_5
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@7
    const/16 v6, 0x2c

    #@9
    invoke-direct {v4, v6}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@c
    .line 4084
    .local v4, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v4, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@f
    .line 4085
    new-instance v3, Ljava/util/ArrayList;

    #@11
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@14
    .line 4086
    .local v3, sizeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Camera$Size;>;"
    invoke-interface {v4}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v0

    #@18
    .local v0, i$:Ljava/util/Iterator;
    :cond_18
    :goto_18
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_2e

    #@1e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Ljava/lang/String;

    #@24
    .line 4087
    .local v1, s:Ljava/lang/String;
    invoke-direct {p0, v1}, Landroid/hardware/Camera$Parameters;->strToSize(Ljava/lang/String;)Landroid/hardware/Camera$Size;

    #@27
    move-result-object v2

    #@28
    .line 4088
    .local v2, size:Landroid/hardware/Camera$Size;
    if-eqz v2, :cond_18

    #@2a
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    goto :goto_18

    #@2e
    .line 4090
    .end local v1           #s:Ljava/lang/String;
    .end local v2           #size:Landroid/hardware/Camera$Size;
    :cond_2e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v6

    #@32
    if-nez v6, :cond_4

    #@34
    move-object v3, v5

    #@35
    goto :goto_4
.end method

.method private strToCoordinate(Ljava/lang/String;)Landroid/hardware/Camera$Coordinate;
    .registers 9
    .parameter "str"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 5245
    if-nez p1, :cond_4

    #@3
    .line 5255
    :goto_3
    return-object v3

    #@4
    .line 5247
    :cond_4
    const/16 v4, 0x78

    #@6
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@9
    move-result v0

    #@a
    .line 5248
    .local v0, pos:I
    const/4 v4, -0x1

    #@b
    if-eq v0, v4, :cond_28

    #@d
    .line 5249
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 5250
    .local v1, x:Ljava/lang/String;
    add-int/lit8 v3, v0, 0x1

    #@14
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 5251
    .local v2, y:Ljava/lang/String;
    new-instance v3, Landroid/hardware/Camera$Coordinate;

    #@1a
    iget-object v4, p0, Landroid/hardware/Camera$Parameters;->this$0:Landroid/hardware/Camera;

    #@1c
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1f
    move-result v5

    #@20
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23
    move-result v6

    #@24
    invoke-direct {v3, v4, v5, v6}, Landroid/hardware/Camera$Coordinate;-><init>(Landroid/hardware/Camera;II)V

    #@27
    goto :goto_3

    #@28
    .line 5254
    .end local v1           #x:Ljava/lang/String;
    .end local v2           #y:Ljava/lang/String;
    :cond_28
    const-string v4, "Camera"

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "Invalid Coordinate parameter string="

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_3
.end method

.method private strToSize(Ljava/lang/String;)Landroid/hardware/Camera$Size;
    .registers 9
    .parameter "str"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4097
    if-nez p1, :cond_4

    #@3
    .line 4107
    :goto_3
    return-object v3

    #@4
    .line 4099
    :cond_4
    const/16 v4, 0x78

    #@6
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@9
    move-result v1

    #@a
    .line 4100
    .local v1, pos:I
    const/4 v4, -0x1

    #@b
    if-eq v1, v4, :cond_28

    #@d
    .line 4101
    const/4 v3, 0x0

    #@e
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    .line 4102
    .local v2, width:Ljava/lang/String;
    add-int/lit8 v3, v1, 0x1

    #@14
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 4103
    .local v0, height:Ljava/lang/String;
    new-instance v3, Landroid/hardware/Camera$Size;

    #@1a
    iget-object v4, p0, Landroid/hardware/Camera$Parameters;->this$0:Landroid/hardware/Camera;

    #@1c
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1f
    move-result v5

    #@20
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23
    move-result v6

    #@24
    invoke-direct {v3, v4, v5, v6}, Landroid/hardware/Camera$Size;-><init>(Landroid/hardware/Camera;II)V

    #@27
    goto :goto_3

    #@28
    .line 4106
    .end local v0           #height:Ljava/lang/String;
    .end local v2           #width:Ljava/lang/String;
    :cond_28
    const-string v4, "Camera"

    #@2a
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "Invalid size parameter string="

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_3
.end method


# virtual methods
.method public dump()V
    .registers 6

    #@0
    .prologue
    .line 2431
    const-string v2, "Camera"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "dump: size="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-object v4, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@f
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    #@12
    move-result v4

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2432
    iget-object v2, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@20
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@23
    move-result-object v2

    #@24
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v0

    #@28
    .local v0, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_5f

    #@2e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;

    #@34
    .line 2433
    .local v1, k:Ljava/lang/String;
    const-string v3, "Camera"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "dump: "

    #@3d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    const-string v4, "="

    #@47
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    iget-object v2, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@4d
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@50
    move-result-object v2

    #@51
    check-cast v2, Ljava/lang/String;

    #@53
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_28

    #@5f
    .line 2435
    .end local v1           #k:Ljava/lang/String;
    :cond_5f
    return-void
.end method

.method public flatten()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 2446
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v3, 0x80

    #@4
    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 2447
    .local v0, flattened:Ljava/lang/StringBuilder;
    iget-object v3, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@9
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@c
    move-result-object v3

    #@d
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_36

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Ljava/lang/String;

    #@1d
    .line 2448
    .local v2, k:Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 2449
    const-string v3, "="

    #@22
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 2450
    iget-object v3, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@27
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    move-result-object v3

    #@2b
    check-cast v3, Ljava/lang/String;

    #@2d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 2451
    const-string v3, ";"

    #@32
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    goto :goto_11

    #@36
    .line 2454
    .end local v2           #k:Ljava/lang/String;
    :cond_36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@39
    move-result v3

    #@3a
    add-int/lit8 v3, v3, -0x1

    #@3c
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    #@3f
    .line 2455
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    return-object v3
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 2547
    iget-object v0, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public getAEBracket()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4949
    const-string v0, "ae-bracket-hdr"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAntibanding()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3259
    const-string v0, "antibanding"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAutoExposure()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5027
    const-string v0, "auto-exposure"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAutoExposureLock()Z
    .registers 3

    #@0
    .prologue
    .line 3554
    const-string v1, "auto-exposure-lock"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3555
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    return v1
.end method

.method public getAutoWhiteBalanceLock()Z
    .registers 3

    #@0
    .prologue
    .line 3626
    const-string v1, "auto-whitebalance-lock"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3627
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    return v1
.end method

.method public getBurstShotMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5086
    const-string v0, "burst-shot"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCameraMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5115
    const-string v0, "camera-mode"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getColorEffect()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3222
    const-string v0, "effect"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getContinuousAf()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5164
    const-string v0, "continuous-af"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getContrast()I
    .registers 2

    #@0
    .prologue
    .line 4794
    const-string v0, "contrast"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDenoise()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5153
    const-string v0, "denoise"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getExposureCompensation()I
    .registers 3

    #@0
    .prologue
    .line 3454
    const-string v0, "exposure-compensation"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getExposureCompensationStep()F
    .registers 3

    #@0
    .prologue
    .line 3502
    const-string v0, "exposure-compensation-step"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getFloat(Ljava/lang/String;F)F

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getFaceDetectionMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5213
    const-string v0, "face-detection"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFlashMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3350
    const-string v0, "flash-mode"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getFocalLength()F
    .registers 2

    #@0
    .prologue
    .line 3422
    const-string v0, "focal-length"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getFocusAreas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3811
    const-string v0, "focus-areas"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitArea(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getFocusDistances([F)V
    .registers 4
    .parameter "output"

    #@0
    .prologue
    .line 3749
    if-eqz p1, :cond_6

    #@2
    array-length v0, p1

    #@3
    const/4 v1, 0x3

    #@4
    if-eq v0, v1, :cond_f

    #@6
    .line 3750
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v1, "output must be a float array with three elements."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 3753
    :cond_f
    const-string v0, "focus-distances"

    #@11
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->splitFloat(Ljava/lang/String;[F)V

    #@18
    .line 3754
    return-void
.end method

.method public getFocusMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3390
    const-string v0, "focus-mode"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getHorizontalViewAngle()F
    .registers 2

    #@0
    .prologue
    .line 3432
    const-string v0, "horizontal-view-angle"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getISOValue()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4990
    const-string/jumbo v0, "iso"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getInt(Ljava/lang/String;)I
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 2557
    iget-object v0, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getJpegQuality()I
    .registers 2

    #@0
    .prologue
    .line 2725
    const-string/jumbo v0, "jpeg-quality"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getJpegThumbnailQuality()I
    .registers 2

    #@0
    .prologue
    .line 2706
    const-string/jumbo v0, "jpeg-thumbnail-quality"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getJpegThumbnailSize()Landroid/hardware/Camera$Size;
    .registers 5

    #@0
    .prologue
    .line 2674
    new-instance v0, Landroid/hardware/Camera$Size;

    #@2
    iget-object v1, p0, Landroid/hardware/Camera$Parameters;->this$0:Landroid/hardware/Camera;

    #@4
    const-string/jumbo v2, "jpeg-thumbnail-width"

    #@7
    invoke-virtual {p0, v2}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@a
    move-result v2

    #@b
    const-string/jumbo v3, "jpeg-thumbnail-height"

    #@e
    invoke-virtual {p0, v3}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@11
    move-result v3

    #@12
    invoke-direct {v0, v1, v2, v3}, Landroid/hardware/Camera$Size;-><init>(Landroid/hardware/Camera;II)V

    #@15
    return-object v0
.end method

.method public getLensShade()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5008
    const-string/jumbo v0, "lensshade"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getMaxContrast()I
    .registers 2

    #@0
    .prologue
    .line 4803
    const-string/jumbo v0, "max-contrast"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getMaxExposureCompensation()I
    .registers 3

    #@0
    .prologue
    .line 3479
    const-string/jumbo v0, "max-exposure-compensation"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMaxNumDetectedFaces()I
    .registers 3

    #@0
    .prologue
    .line 3901
    const-string/jumbo v0, "max-num-detected-faces-hw"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMaxNumFocusAreas()I
    .registers 3

    #@0
    .prologue
    .line 3765
    const-string/jumbo v0, "max-num-focus-areas"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMaxNumMeteringAreas()I
    .registers 3

    #@0
    .prologue
    .line 3833
    const-string/jumbo v0, "max-num-metering-areas"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMaxSaturation()I
    .registers 2

    #@0
    .prologue
    .line 4821
    const-string/jumbo v0, "max-saturation"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getMaxSharpness()I
    .registers 2

    #@0
    .prologue
    .line 4785
    const-string/jumbo v0, "max-sharpness"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getMaxZoom()I
    .registers 3

    #@0
    .prologue
    .line 3691
    const-string/jumbo v0, "max-zoom"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMemColorEnhance()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5045
    const-string/jumbo v0, "mce"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getMeteringAreas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3877
    const-string/jumbo v0, "metering-areas"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitArea(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getMinExposureCompensation()I
    .registers 3

    #@0
    .prologue
    .line 3490
    const-string/jumbo v0, "min-exposure-compensation"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMultiWindowFocusAreas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 5269
    const-string/jumbo v0, "multi-window-focus-area"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitArea(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getObjectTracking()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4450
    const-string/jumbo v0, "object-tracking"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getPictureFormat()I
    .registers 2

    #@0
    .prologue
    .line 2973
    const-string/jumbo v0, "picture-format"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->pixelFormatForCameraFormat(Ljava/lang/String;)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public getPictureSize()Landroid/hardware/Camera$Size;
    .registers 3

    #@0
    .prologue
    .line 2932
    const-string/jumbo v1, "picture-size"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2933
    .local v0, pair:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->strToSize(Ljava/lang/String;)Landroid/hardware/Camera$Size;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getPowerMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4971
    const-string/jumbo v0, "power-mode"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getPreferredPreviewSizeForVideo()Landroid/hardware/Camera$Size;
    .registers 3

    #@0
    .prologue
    .line 2646
    const-string/jumbo v1, "preferred-preview-size-for-video"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2647
    .local v0, pair:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->strToSize(Ljava/lang/String;)Landroid/hardware/Camera$Size;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getPreviewFormat()I
    .registers 2

    #@0
    .prologue
    .line 2885
    const-string/jumbo v0, "preview-format"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->pixelFormatForCameraFormat(Ljava/lang/String;)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public getPreviewFpsRange([I)V
    .registers 4
    .parameter "range"

    #@0
    .prologue
    .line 2792
    if-eqz p1, :cond_6

    #@2
    array-length v0, p1

    #@3
    const/4 v1, 0x2

    #@4
    if-eq v0, v1, :cond_f

    #@6
    .line 2793
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v1, "range must be an array with two elements."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 2796
    :cond_f
    const-string/jumbo v0, "preview-fps-range"

    #@12
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    invoke-direct {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->splitInt(Ljava/lang/String;[I)V

    #@19
    .line 2797
    return-void
.end method

.method public getPreviewFrameRate()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2750
    const-string/jumbo v0, "preview-frame-rate"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getPreviewFrameRateMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4904
    const-string/jumbo v0, "preview-frame-rate-mode"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getPreviewSize()Landroid/hardware/Camera$Size;
    .registers 3

    #@0
    .prologue
    .line 2595
    const-string/jumbo v1, "preview-size"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2596
    .local v0, pair:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->strToSize(Ljava/lang/String;)Landroid/hardware/Camera$Size;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getRedeyeReductionMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4881
    const-string/jumbo v0, "redeye-reduction"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getSaturation()I
    .registers 2

    #@0
    .prologue
    .line 4812
    const-string/jumbo v0, "saturation"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getSceneDetectMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4924
    const-string/jumbo v0, "scene-detect"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getSceneMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3307
    const-string/jumbo v0, "scene-mode"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getSelectableZoneAf()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5193
    const-string/jumbo v0, "selectable-zone-af"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getSharpness()I
    .registers 2

    #@0
    .prologue
    .line 4776
    const-string/jumbo v0, "sharpness"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getSupportedAntibanding()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3280
    const-string v1, "antibanding-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3281
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedAutoexposure()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4562
    const-string v1, "auto-exposure-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4563
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedColorEffects()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3243
    const-string v1, "effect-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3244
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedContinuousAfModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4607
    const-string v1, "continuous-af-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4608
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedDenoiseModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4619
    const-string v1, "denoise-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4620
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedFaceDetectionModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4642
    const-string v1, "face-detection-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4643
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedFlashModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3371
    const-string v1, "flash-mode-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3372
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedFocusModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3411
    const-string v1, "focus-mode-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3412
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedHfrSizes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4464
    const-string v1, "hfr-size-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4465
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitSize(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedHistogramModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4540
    const-string v1, "histogram-values"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 4541
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method public getSupportedIsoValues()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4518
    const-string/jumbo v1, "iso-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4519
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedJpegThumbnailSizes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2686
    const-string/jumbo v1, "jpeg-thumbnail-size-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2687
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitSize(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedLensShadeModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4529
    const-string/jumbo v1, "lensshade-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4530
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedMemColorEnhanceModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4573
    const-string/jumbo v1, "mce-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4574
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedPictureFormats()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2984
    const-string/jumbo v5, "picture-format-values"

    #@3
    invoke-virtual {p0, v5}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    .line 2985
    .local v4, str:Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    #@9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 2986
    .local v1, formats:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0, v4}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v2

    #@14
    .local v2, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_2e

    #@1a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/String;

    #@20
    .line 2987
    .local v3, s:Ljava/lang/String;
    invoke-direct {p0, v3}, Landroid/hardware/Camera$Parameters;->pixelFormatForCameraFormat(Ljava/lang/String;)I

    #@23
    move-result v0

    #@24
    .line 2988
    .local v0, f:I
    if-eqz v0, :cond_14

    #@26
    .line 2989
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    goto :goto_14

    #@2e
    .line 2991
    .end local v0           #f:I
    .end local v3           #s:Ljava/lang/String;
    :cond_2e
    return-object v1
.end method

.method public getSupportedPictureSizes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2943
    const-string/jumbo v1, "picture-size-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2944
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitSize(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedPreviewFormats()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2899
    const-string/jumbo v5, "preview-format-values"

    #@3
    invoke-virtual {p0, v5}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v4

    #@7
    .line 2900
    .local v4, str:Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    #@9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 2901
    .local v1, formats:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0, v4}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v2

    #@14
    .local v2, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_2e

    #@1a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/String;

    #@20
    .line 2902
    .local v3, s:Ljava/lang/String;
    invoke-direct {p0, v3}, Landroid/hardware/Camera$Parameters;->pixelFormatForCameraFormat(Ljava/lang/String;)I

    #@23
    move-result v0

    #@24
    .line 2903
    .local v0, f:I
    if-eqz v0, :cond_14

    #@26
    .line 2904
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    goto :goto_14

    #@2e
    .line 2906
    .end local v0           #f:I
    .end local v3           #s:Ljava/lang/String;
    :cond_2e
    return-object v1
.end method

.method public getSupportedPreviewFpsRange()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation

    #@0
    .prologue
    .line 2817
    const-string/jumbo v1, "preview-fps-range-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2818
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitRange(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedPreviewFrameRateModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4495
    const-string/jumbo v1, "preview-frame-rate-mode-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4496
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedPreviewFrameRates()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2762
    const-string/jumbo v1, "preview-frame-rate-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2763
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitInt(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedPreviewSizes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2606
    const-string/jumbo v1, "preview-size-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2607
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitSize(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedRedeyeReductionModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4654
    const-string/jumbo v1, "redeye-reduction-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4655
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedSceneDetectModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4507
    const-string/jumbo v1, "scene-detect-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4508
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedSceneModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3334
    const-string/jumbo v1, "scene-mode-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3335
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedSelectableZoneAf()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4630
    const-string/jumbo v1, "selectable-zone-af-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4631
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedSkinToneEnhancementModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4551
    const-string/jumbo v1, "skinToneEnhancement-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4552
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedTouchAfAec()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4476
    const-string/jumbo v1, "touch-af-aec-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4477
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedVideoHighFrameRateModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4595
    const-string/jumbo v1, "video-hfr-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4596
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedVideoSizes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2626
    const-string/jumbo v1, "video-size-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 2627
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitSize(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedWhiteBalance()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3202
    const-string/jumbo v1, "whitebalance-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3203
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getSupportedZSLModes()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 4584
    const-string/jumbo v1, "zsl-values"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4585
    .local v0, str:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->split(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getTouchAfAec()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 4860
    const-string/jumbo v0, "touch-af-aec"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getTouchIndexAec()Landroid/hardware/Camera$Coordinate;
    .registers 3

    #@0
    .prologue
    .line 4696
    const-string/jumbo v1, "touch-index-aec"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4697
    .local v0, pair:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->strToCoordinate(Ljava/lang/String;)Landroid/hardware/Camera$Coordinate;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getTouchIndexAf()Landroid/hardware/Camera$Coordinate;
    .registers 3

    #@0
    .prologue
    .line 4720
    const-string/jumbo v1, "touch-index-af"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4721
    .local v0, pair:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->strToCoordinate(Ljava/lang/String;)Landroid/hardware/Camera$Coordinate;

    #@a
    move-result-object v1

    #@b
    return-object v1
.end method

.method public getVerticalViewAngle()F
    .registers 2

    #@0
    .prologue
    .line 3442
    const-string/jumbo v0, "vertical-view-angle"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public getVideoHighFrameRate()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5133
    const-string/jumbo v0, "video-hfr"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getVideoStabilization()Z
    .registers 3

    #@0
    .prologue
    .line 3991
    const-string/jumbo v1, "video-stabilization"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3992
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    return v1
.end method

.method public getWhiteBalance()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3175
    const-string/jumbo v0, "whitebalance"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getZSLMode()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 5063
    const-string/jumbo v0, "zsl"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getZoom()I
    .registers 3

    #@0
    .prologue
    .line 3653
    const-string/jumbo v0, "zoom"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getZoomRatios()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3705
    const-string/jumbo v0, "zoom-ratios"

    #@3
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0}, Landroid/hardware/Camera$Parameters;->splitInt(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public isAutoExposureLockSupported()Z
    .registers 3

    #@0
    .prologue
    .line 3568
    const-string v1, "auto-exposure-lock-supported"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3569
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    return v1
.end method

.method public isAutoWhiteBalanceLockSupported()Z
    .registers 3

    #@0
    .prologue
    .line 3640
    const-string v1, "auto-whitebalance-lock-supported"

    #@2
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 3641
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@9
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    return v1
.end method

.method public isPowerModeSupported()Z
    .registers 3

    #@0
    .prologue
    .line 4766
    const-string/jumbo v1, "power-mode-supported"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4767
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    return v1
.end method

.method public isSmoothZoomSupported()Z
    .registers 3

    #@0
    .prologue
    .line 3715
    const-string/jumbo v1, "smooth-zoom-supported"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3716
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    return v1
.end method

.method public isVideoSnapshotSupported()Z
    .registers 3

    #@0
    .prologue
    .line 3954
    const-string/jumbo v1, "video-snapshot-supported"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3955
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    return v1
.end method

.method public isVideoStabilizationSupported()Z
    .registers 3

    #@0
    .prologue
    .line 4004
    const-string/jumbo v1, "video-stabilization-supported"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 4005
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    return v1
.end method

.method public isZoomSupported()Z
    .registers 3

    #@0
    .prologue
    .line 3677
    const-string/jumbo v1, "zoom-supported"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3678
    .local v0, str:Ljava/lang/String;
    const-string/jumbo v1, "true"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v1

    #@e
    return v1
.end method

.method public remove(Ljava/lang/String;)V
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 2483
    iget-object v0, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 2484
    return-void
.end method

.method public removeGpsData()V
    .registers 2

    #@0
    .prologue
    .line 3149
    const-string v0, "gps-latitude-ref"

    #@2
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@5
    .line 3150
    const-string v0, "gps-latitude"

    #@7
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@a
    .line 3151
    const-string v0, "gps-longitude-ref"

    #@c
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@f
    .line 3152
    const-string v0, "gps-longitude"

    #@11
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@14
    .line 3153
    const-string v0, "gps-altitude-ref"

    #@16
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@19
    .line 3154
    const-string v0, "gps-altitude"

    #@1b
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@1e
    .line 3155
    const-string v0, "gps-timestamp"

    #@20
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@23
    .line 3156
    const-string v0, "gps-processing-method"

    #@25
    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    #@28
    .line 3157
    return-void
.end method

.method public set(Ljava/lang/String;I)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 2512
    iget-object v0, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 2513
    return-void
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    const/16 v4, 0x3d

    #@2
    const/16 v3, 0x3b

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v1, -0x1

    #@6
    .line 2493
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@9
    move-result v0

    #@a
    if-ne v0, v1, :cond_18

    #@c
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    #@f
    move-result v0

    #@10
    if-ne v0, v1, :cond_18

    #@12
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    #@15
    move-result v0

    #@16
    if-eq v0, v1, :cond_37

    #@18
    .line 2494
    :cond_18
    const-string v0, "Camera"

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "Key \""

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, "\" contains invalid character (= or ; or \\0)"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 2503
    :goto_36
    return-void

    #@37
    .line 2497
    :cond_37
    invoke-virtual {p2, v4}, Ljava/lang/String;->indexOf(I)I

    #@3a
    move-result v0

    #@3b
    if-ne v0, v1, :cond_49

    #@3d
    invoke-virtual {p2, v3}, Ljava/lang/String;->indexOf(I)I

    #@40
    move-result v0

    #@41
    if-ne v0, v1, :cond_49

    #@43
    invoke-virtual {p2, v2}, Ljava/lang/String;->indexOf(I)I

    #@46
    move-result v0

    #@47
    if-eq v0, v1, :cond_68

    #@49
    .line 2498
    :cond_49
    const-string v0, "Camera"

    #@4b
    new-instance v1, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v2, "Value \""

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    const-string v2, "\" contains invalid character (= or ; or \\0)"

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_36

    #@68
    .line 2502
    :cond_68
    iget-object v0, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@6a
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6d
    goto :goto_36
.end method

.method public setAEBracket(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 4980
    const-string v0, "ae-bracket-hdr"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 4981
    return-void
.end method

.method public setAntibanding(Ljava/lang/String;)V
    .registers 3
    .parameter "antibanding"

    #@0
    .prologue
    .line 3269
    const-string v0, "antibanding"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 3270
    return-void
.end method

.method public setAutoExposure(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 5036
    const-string v0, "auto-exposure"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 5037
    return-void
.end method

.method public setAutoExposureLock(Z)V
    .registers 4
    .parameter "toggle"

    #@0
    .prologue
    .line 3539
    const-string v1, "auto-exposure-lock"

    #@2
    if-eqz p1, :cond_b

    #@4
    const-string/jumbo v0, "true"

    #@7
    :goto_7
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 3540
    return-void

    #@b
    .line 3539
    :cond_b
    const-string v0, "false"

    #@d
    goto :goto_7
.end method

.method public setAutoWhiteBalanceLock(Z)V
    .registers 4
    .parameter "toggle"

    #@0
    .prologue
    .line 3609
    const-string v1, "auto-whitebalance-lock"

    #@2
    if-eqz p1, :cond_b

    #@4
    const-string/jumbo v0, "true"

    #@7
    :goto_7
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 3610
    return-void

    #@b
    .line 3609
    :cond_b
    const-string v0, "false"

    #@d
    goto :goto_7
.end method

.method public setBurstShotMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 5102
    const-string v0, "burst-shot"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 5103
    return-void
.end method

.method public setCameraMode(I)V
    .registers 3
    .parameter "cameraMode"

    #@0
    .prologue
    .line 5124
    const-string v0, "camera-mode"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@5
    .line 5125
    return-void
.end method

.method public setColorEffect(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3232
    const-string v0, "effect"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 3233
    return-void
.end method

.method public setContinuousAf(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 5183
    const-string v0, "continuous-af"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 5184
    return-void
.end method

.method public setContrast(I)V
    .registers 5
    .parameter "contrast"

    #@0
    .prologue
    .line 4742
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxContrast()I

    #@5
    move-result v0

    #@6
    if-le p1, v0, :cond_21

    #@8
    .line 4743
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Invalid Contrast "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 4746
    :cond_21
    const-string v0, "contrast"

    #@23
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 4747
    return-void
.end method

.method public setDenoise(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 5174
    const-string v0, "denoise"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 5175
    return-void
.end method

.method public setExifDateTime(Ljava/lang/String;)V
    .registers 3
    .parameter "dateTime"

    #@0
    .prologue
    .line 4849
    const-string v0, "exif-datetime"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 4850
    return-void
.end method

.method public setExposureCompensation(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3468
    const-string v0, "exposure-compensation"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@5
    .line 3469
    return-void
.end method

.method public setFaceDetectionMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 5224
    const-string v0, "face-detection"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 5225
    return-void
.end method

.method public setFlashMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3360
    const-string v0, "flash-mode"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 3361
    return-void
.end method

.method public setFocusAreas(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3821
    .local p1, focusAreas:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Area;>;"
    const-string v0, "focus-areas"

    #@2
    invoke-direct {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/util/List;)V

    #@5
    .line 3822
    return-void
.end method

.method public setFocusMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3400
    const-string v0, "focus-mode"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 3401
    return-void
.end method

.method public setGpsAltitude(D)V
    .registers 5
    .parameter "altitude"

    #@0
    .prologue
    .line 3121
    const-string v0, "gps-altitude"

    #@2
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 3122
    return-void
.end method

.method public setGpsAltitudeRef(D)V
    .registers 5
    .parameter "altRef"

    #@0
    .prologue
    .line 4663
    const-string v0, "gps-altitude-ref"

    #@2
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 4664
    return-void
.end method

.method public setGpsLatitude(D)V
    .registers 5
    .parameter "latitude"

    #@0
    .prologue
    .line 3102
    const-string v0, "gps-latitude"

    #@2
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 3103
    return-void
.end method

.method public setGpsLatitudeRef(Ljava/lang/String;)V
    .registers 3
    .parameter "latRef"

    #@0
    .prologue
    .line 4830
    const-string v0, "gps-latitude-ref"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 4831
    return-void
.end method

.method public setGpsLongitude(D)V
    .registers 5
    .parameter "longitude"

    #@0
    .prologue
    .line 3112
    const-string v0, "gps-longitude"

    #@2
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 3113
    return-void
.end method

.method public setGpsLongitudeRef(Ljava/lang/String;)V
    .registers 3
    .parameter "lonRef"

    #@0
    .prologue
    .line 4839
    const-string v0, "gps-longitude-ref"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 4840
    return-void
.end method

.method public setGpsProcessingMethod(Ljava/lang/String;)V
    .registers 3
    .parameter "processing_method"

    #@0
    .prologue
    .line 3141
    const-string v0, "gps-processing-method"

    #@2
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 3142
    return-void
.end method

.method public setGpsStatus(D)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    .line 4673
    const-string v0, "gps-status"

    #@2
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 4674
    return-void
.end method

.method public setGpsTimestamp(J)V
    .registers 5
    .parameter "timestamp"

    #@0
    .prologue
    .line 3131
    const-string v0, "gps-timestamp"

    #@2
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 3132
    return-void
.end method

.method public setISOValue(Ljava/lang/String;)V
    .registers 3
    .parameter "iso"

    #@0
    .prologue
    .line 4999
    const-string/jumbo v0, "iso"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 5000
    return-void
.end method

.method public setJpegQuality(I)V
    .registers 3
    .parameter "quality"

    #@0
    .prologue
    .line 2716
    const-string/jumbo v0, "jpeg-quality"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@6
    .line 2717
    return-void
.end method

.method public setJpegThumbnailQuality(I)V
    .registers 3
    .parameter "quality"

    #@0
    .prologue
    .line 2697
    const-string/jumbo v0, "jpeg-thumbnail-quality"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@6
    .line 2698
    return-void
.end method

.method public setJpegThumbnailSize(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 2663
    const-string/jumbo v0, "jpeg-thumbnail-width"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@6
    .line 2664
    const-string/jumbo v0, "jpeg-thumbnail-height"

    #@9
    invoke-virtual {p0, v0, p2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@c
    .line 2665
    return-void
.end method

.method public setLensShade(Ljava/lang/String;)V
    .registers 3
    .parameter "lensshade"

    #@0
    .prologue
    .line 5017
    const-string/jumbo v0, "lensshade"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 5018
    return-void
.end method

.method public setMemColorEnhance(Ljava/lang/String;)V
    .registers 3
    .parameter "mce"

    #@0
    .prologue
    .line 5054
    const-string/jumbo v0, "mce"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 5055
    return-void
.end method

.method public setMeteringAreas(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Area;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3888
    .local p1, meteringAreas:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Area;>;"
    const-string/jumbo v0, "metering-areas"

    #@3
    invoke-direct {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/util/List;)V

    #@6
    .line 3889
    return-void
.end method

.method public setObjectTracking(Ljava/lang/String;)V
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 4441
    const-string v0, "Camera"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "visidon setObjectTracking string="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 4442
    const-string/jumbo v0, "object-tracking"

    #@1c
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 4443
    return-void
.end method

.method public setPictureFormat(I)V
    .registers 6
    .parameter "pixel_format"

    #@0
    .prologue
    .line 2957
    invoke-direct {p0, p1}, Landroid/hardware/Camera$Parameters;->cameraFormatForPixelFormat(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 2958
    .local v0, s:Ljava/lang/String;
    if-nez v0, :cond_1f

    #@6
    .line 2959
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Invalid pixel_format="

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 2963
    :cond_1f
    const-string/jumbo v1, "picture-format"

    #@22
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 2964
    return-void
.end method

.method public setPictureSize(II)V
    .registers 6
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 2921
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string/jumbo v2, "x"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 2922
    .local v0, v:Ljava/lang/String;
    const-string/jumbo v1, "picture-size"

    #@23
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 2923
    return-void
.end method

.method public setPowerMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 4959
    const-string/jumbo v0, "power-mode"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 4960
    return-void
.end method

.method public setPreviewFormat(I)V
    .registers 6
    .parameter "pixel_format"

    #@0
    .prologue
    .line 2867
    invoke-direct {p0, p1}, Landroid/hardware/Camera$Parameters;->cameraFormatForPixelFormat(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 2868
    .local v0, s:Ljava/lang/String;
    if-nez v0, :cond_1f

    #@6
    .line 2869
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Invalid pixel_format="

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 2873
    :cond_1f
    const-string/jumbo v1, "preview-format"

    #@22
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 2874
    return-void
.end method

.method public setPreviewFpsRange(II)V
    .registers 6
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 2779
    const-string/jumbo v0, "preview-fps-range"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, ""

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ","

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 2780
    return-void
.end method

.method public setPreviewFrameRate(I)V
    .registers 3
    .parameter "fps"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 2737
    const-string/jumbo v0, "preview-frame-rate"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@6
    .line 2738
    return-void
.end method

.method public setPreviewFrameRateMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 4913
    const-string/jumbo v0, "preview-frame-rate-mode"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 4914
    return-void
.end method

.method public setPreviewSize(II)V
    .registers 6
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 2584
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string/jumbo v2, "x"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 2585
    .local v0, v:Ljava/lang/String;
    const-string/jumbo v1, "preview-size"

    #@23
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 2586
    return-void
.end method

.method public setRecordingHint(Z)V
    .registers 4
    .parameter "hint"

    #@0
    .prologue
    .line 3923
    const-string/jumbo v1, "recording-hint"

    #@3
    if-eqz p1, :cond_c

    #@5
    const-string/jumbo v0, "true"

    #@8
    :goto_8
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 3924
    return-void

    #@c
    .line 3923
    :cond_c
    const-string v0, "false"

    #@e
    goto :goto_8
.end method

.method public setRedeyeReductionMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 4894
    const-string/jumbo v0, "redeye-reduction"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 4895
    return-void
.end method

.method public setRotation(I)V
    .registers 5
    .parameter "rotation"

    #@0
    .prologue
    .line 3086
    if-eqz p1, :cond_e

    #@2
    const/16 v0, 0x5a

    #@4
    if-eq p1, v0, :cond_e

    #@6
    const/16 v0, 0xb4

    #@8
    if-eq p1, v0, :cond_e

    #@a
    const/16 v0, 0x10e

    #@c
    if-ne p1, v0, :cond_19

    #@e
    .line 3088
    :cond_e
    const-string/jumbo v0, "rotation"

    #@11
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 3093
    return-void

    #@19
    .line 3090
    :cond_19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "Invalid rotation="

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0
.end method

.method public setSaturation(I)V
    .registers 5
    .parameter "saturation"

    #@0
    .prologue
    .line 4755
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxSaturation()I

    #@5
    move-result v0

    #@6
    if-le p1, v0, :cond_21

    #@8
    .line 4756
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Invalid Saturation "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 4759
    :cond_21
    const-string/jumbo v0, "saturation"

    #@24
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 4760
    return-void
.end method

.method public setSceneDetectMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 4937
    const-string/jumbo v0, "scene-detect"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 4938
    return-void
.end method

.method public setSceneMode(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3323
    const-string/jumbo v0, "scene-mode"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 3324
    return-void
.end method

.method public setSelectableZoneAf(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 5202
    const-string/jumbo v0, "selectable-zone-af"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 5203
    return-void
.end method

.method public setSharpness(I)V
    .registers 5
    .parameter "sharpness"

    #@0
    .prologue
    .line 4729
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxSharpness()I

    #@5
    move-result v0

    #@6
    if-le p1, v0, :cond_21

    #@8
    .line 4730
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Invalid Sharpness "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 4733
    :cond_21
    const-string/jumbo v0, "sharpness"

    #@24
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {p0, v0, v1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 4734
    return-void
.end method

.method public setTouchAfAec(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 4870
    const-string/jumbo v0, "touch-af-aec"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 4871
    return-void
.end method

.method public setTouchIndexAec(II)V
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 4684
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string/jumbo v2, "x"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 4685
    .local v0, v:Ljava/lang/String;
    const-string/jumbo v1, "touch-index-aec"

    #@23
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 4686
    return-void
.end method

.method public setTouchIndexAf(II)V
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 4708
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string/jumbo v2, "x"

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 4709
    .local v0, v:Ljava/lang/String;
    const-string/jumbo v1, "touch-index-af"

    #@23
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    .line 4710
    return-void
.end method

.method public setVideoHighFrameRate(Ljava/lang/String;)V
    .registers 3
    .parameter "hfr"

    #@0
    .prologue
    .line 5142
    const-string/jumbo v0, "video-hfr"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 5143
    return-void
.end method

.method public setVideoStabilization(Z)V
    .registers 4
    .parameter "toggle"

    #@0
    .prologue
    .line 3979
    const-string/jumbo v1, "video-stabilization"

    #@3
    if-eqz p1, :cond_c

    #@5
    const-string/jumbo v0, "true"

    #@8
    :goto_8
    invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 3980
    return-void

    #@c
    .line 3979
    :cond_c
    const-string v0, "false"

    #@e
    goto :goto_8
.end method

.method public setWhiteBalance(Ljava/lang/String;)V
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 3188
    const-string/jumbo v1, "whitebalance"

    #@3
    invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 3189
    .local v0, oldValue:Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Landroid/hardware/Camera$Parameters;->same(Ljava/lang/String;Ljava/lang/String;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_e

    #@d
    .line 3192
    :goto_d
    return-void

    #@e
    .line 3190
    :cond_e
    const-string/jumbo v1, "whitebalance"

    #@11
    invoke-virtual {p0, v1, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 3191
    const-string v1, "auto-whitebalance-lock"

    #@16
    const-string v2, "false"

    #@18
    invoke-virtual {p0, v1, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    goto :goto_d
.end method

.method public setZSLMode(Ljava/lang/String;)V
    .registers 3
    .parameter "zsl"

    #@0
    .prologue
    .line 5072
    const-string/jumbo v0, "zsl"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 5073
    return-void
.end method

.method public setZoom(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 3667
    const-string/jumbo v0, "zoom"

    #@3
    invoke-virtual {p0, v0, p1}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    #@6
    .line 3668
    return-void
.end method

.method public unflatten(Ljava/lang/String;)V
    .registers 9
    .parameter "flattened"

    #@0
    .prologue
    .line 2467
    iget-object v6, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    #@5
    .line 2469
    new-instance v4, Landroid/text/TextUtils$SimpleStringSplitter;

    #@7
    const/16 v6, 0x3b

    #@9
    invoke-direct {v4, v6}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    #@c
    .line 2470
    .local v4, splitter:Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v4, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    #@f
    .line 2471
    invoke-interface {v4}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v0

    #@13
    .local v0, i$:Ljava/util/Iterator;
    :cond_13
    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_39

    #@19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Ljava/lang/String;

    #@1f
    .line 2472
    .local v2, kv:Ljava/lang/String;
    const/16 v6, 0x3d

    #@21
    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    #@24
    move-result v3

    #@25
    .line 2473
    .local v3, pos:I
    const/4 v6, -0x1

    #@26
    if-eq v3, v6, :cond_13

    #@28
    .line 2476
    const/4 v6, 0x0

    #@29
    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 2477
    .local v1, k:Ljava/lang/String;
    add-int/lit8 v6, v3, 0x1

    #@2f
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    .line 2478
    .local v5, v:Ljava/lang/String;
    iget-object v6, p0, Landroid/hardware/Camera$Parameters;->mMap:Ljava/util/HashMap;

    #@35
    invoke-virtual {v6, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    goto :goto_13

    #@39
    .line 2480
    .end local v1           #k:Ljava/lang/String;
    .end local v2           #kv:Ljava/lang/String;
    .end local v3           #pos:I
    .end local v5           #v:Ljava/lang/String;
    :cond_39
    return-void
.end method
