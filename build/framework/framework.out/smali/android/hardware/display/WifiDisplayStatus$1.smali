.class final Landroid/hardware/display/WifiDisplayStatus$1;
.super Ljava/lang/Object;
.source "WifiDisplayStatus.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/display/WifiDisplayStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/hardware/display/WifiDisplayStatus;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/hardware/display/WifiDisplayStatus;
    .registers 11
    .parameter "in"

    #@0
    .prologue
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v1

    #@4
    .line 65
    .local v1, featureState:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v2

    #@8
    .line 66
    .local v2, scanState:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v3

    #@c
    .line 68
    .local v3, activeDisplayState:I
    const/4 v4, 0x0

    #@d
    .line 69
    .local v4, activeDisplay:Landroid/hardware/display/WifiDisplay;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_1b

    #@13
    .line 70
    sget-object v0, Landroid/hardware/display/WifiDisplay;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@18
    move-result-object v4

    #@19
    .end local v4           #activeDisplay:Landroid/hardware/display/WifiDisplay;
    check-cast v4, Landroid/hardware/display/WifiDisplay;

    #@1b
    .line 73
    .restart local v4       #activeDisplay:Landroid/hardware/display/WifiDisplay;
    :cond_1b
    sget-object v0, Landroid/hardware/display/WifiDisplay;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v8

    #@21
    invoke-interface {v0, v8}, Landroid/os/Parcelable$Creator;->newArray(I)[Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    check-cast v5, [Landroid/hardware/display/WifiDisplay;

    #@27
    .line 74
    .local v5, availableDisplays:[Landroid/hardware/display/WifiDisplay;
    const/4 v7, 0x0

    #@28
    .local v7, i:I
    :goto_28
    array-length v0, v5

    #@29
    if-ge v7, v0, :cond_38

    #@2b
    .line 75
    sget-object v0, Landroid/hardware/display/WifiDisplay;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Landroid/hardware/display/WifiDisplay;

    #@33
    aput-object v0, v5, v7

    #@35
    .line 74
    add-int/lit8 v7, v7, 0x1

    #@37
    goto :goto_28

    #@38
    .line 78
    :cond_38
    sget-object v0, Landroid/hardware/display/WifiDisplay;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v8

    #@3e
    invoke-interface {v0, v8}, Landroid/os/Parcelable$Creator;->newArray(I)[Ljava/lang/Object;

    #@41
    move-result-object v6

    #@42
    check-cast v6, [Landroid/hardware/display/WifiDisplay;

    #@44
    .line 79
    .local v6, rememberedDisplays:[Landroid/hardware/display/WifiDisplay;
    const/4 v7, 0x0

    #@45
    :goto_45
    array-length v0, v6

    #@46
    if-ge v7, v0, :cond_55

    #@48
    .line 80
    sget-object v0, Landroid/hardware/display/WifiDisplay;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4a
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4d
    move-result-object v0

    #@4e
    check-cast v0, Landroid/hardware/display/WifiDisplay;

    #@50
    aput-object v0, v6, v7

    #@52
    .line 79
    add-int/lit8 v7, v7, 0x1

    #@54
    goto :goto_45

    #@55
    .line 83
    :cond_55
    new-instance v0, Landroid/hardware/display/WifiDisplayStatus;

    #@57
    invoke-direct/range {v0 .. v6}, Landroid/hardware/display/WifiDisplayStatus;-><init>(IIILandroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)V

    #@5a
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-virtual {p0, p1}, Landroid/hardware/display/WifiDisplayStatus$1;->createFromParcel(Landroid/os/Parcel;)Landroid/hardware/display/WifiDisplayStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/hardware/display/WifiDisplayStatus;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 88
    new-array v0, p1, [Landroid/hardware/display/WifiDisplayStatus;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-virtual {p0, p1}, Landroid/hardware/display/WifiDisplayStatus$1;->newArray(I)[Landroid/hardware/display/WifiDisplayStatus;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
