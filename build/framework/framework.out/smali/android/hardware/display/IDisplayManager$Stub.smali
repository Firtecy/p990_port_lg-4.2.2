.class public abstract Landroid/hardware/display/IDisplayManager$Stub;
.super Landroid/os/Binder;
.source "IDisplayManager.java"

# interfaces
.implements Landroid/hardware/display/IDisplayManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/display/IDisplayManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/display/IDisplayManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.hardware.display.IDisplayManager"

.field static final TRANSACTION_connectWifiDisplay:I = 0x5

.field static final TRANSACTION_disconnectWifiDisplay:I = 0x6

.field static final TRANSACTION_forgetWifiDisplay:I = 0x8

.field static final TRANSACTION_getDisplayIds:I = 0x2

.field static final TRANSACTION_getDisplayInfo:I = 0x1

.field static final TRANSACTION_getWifiDisplayStatus:I = 0x9

.field static final TRANSACTION_registerCallback:I = 0x3

.field static final TRANSACTION_renameWifiDisplay:I = 0x7

.field static final TRANSACTION_scanWifiDisplays:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.hardware.display.IDisplayManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/hardware/display/IDisplayManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/hardware/display/IDisplayManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.hardware.display.IDisplayManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/hardware/display/IDisplayManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/hardware/display/IDisplayManager;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/hardware/display/IDisplayManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/hardware/display/IDisplayManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_ba

    #@5
    .line 137
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 43
    :sswitch_a
    const-string v4, "android.hardware.display.IDisplayManager"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v4, "android.hardware.display.IDisplayManager"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 51
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/hardware/display/IDisplayManager$Stub;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@1c
    move-result-object v2

    #@1d
    .line 52
    .local v2, _result:Landroid/view/DisplayInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 53
    if-eqz v2, :cond_29

    #@22
    .line 54
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 55
    invoke-virtual {v2, p3, v3}, Landroid/view/DisplayInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    goto :goto_9

    #@29
    .line 58
    :cond_29
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    goto :goto_9

    #@2d
    .line 64
    .end local v0           #_arg0:I
    .end local v2           #_result:Landroid/view/DisplayInfo;
    :sswitch_2d
    const-string v4, "android.hardware.display.IDisplayManager"

    #@2f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 65
    invoke-virtual {p0}, Landroid/hardware/display/IDisplayManager$Stub;->getDisplayIds()[I

    #@35
    move-result-object v2

    #@36
    .line 66
    .local v2, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@39
    .line 67
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    #@3c
    goto :goto_9

    #@3d
    .line 72
    .end local v2           #_result:[I
    :sswitch_3d
    const-string v4, "android.hardware.display.IDisplayManager"

    #@3f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@45
    move-result-object v4

    #@46
    invoke-static {v4}, Landroid/hardware/display/IDisplayManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/display/IDisplayManagerCallback;

    #@49
    move-result-object v0

    #@4a
    .line 75
    .local v0, _arg0:Landroid/hardware/display/IDisplayManagerCallback;
    invoke-virtual {p0, v0}, Landroid/hardware/display/IDisplayManager$Stub;->registerCallback(Landroid/hardware/display/IDisplayManagerCallback;)V

    #@4d
    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@50
    goto :goto_9

    #@51
    .line 81
    .end local v0           #_arg0:Landroid/hardware/display/IDisplayManagerCallback;
    :sswitch_51
    const-string v4, "android.hardware.display.IDisplayManager"

    #@53
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@56
    .line 82
    invoke-virtual {p0}, Landroid/hardware/display/IDisplayManager$Stub;->scanWifiDisplays()V

    #@59
    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    goto :goto_9

    #@5d
    .line 88
    :sswitch_5d
    const-string v4, "android.hardware.display.IDisplayManager"

    #@5f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@62
    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    .line 91
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/hardware/display/IDisplayManager$Stub;->connectWifiDisplay(Ljava/lang/String;)V

    #@69
    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c
    goto :goto_9

    #@6d
    .line 97
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_6d
    const-string v4, "android.hardware.display.IDisplayManager"

    #@6f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@72
    .line 98
    invoke-virtual {p0}, Landroid/hardware/display/IDisplayManager$Stub;->disconnectWifiDisplay()V

    #@75
    .line 99
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    goto :goto_9

    #@79
    .line 104
    :sswitch_79
    const-string v4, "android.hardware.display.IDisplayManager"

    #@7b
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7e
    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@81
    move-result-object v0

    #@82
    .line 108
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@85
    move-result-object v1

    #@86
    .line 109
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/hardware/display/IDisplayManager$Stub;->renameWifiDisplay(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 110
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    goto/16 :goto_9

    #@8e
    .line 115
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_8e
    const-string v4, "android.hardware.display.IDisplayManager"

    #@90
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@93
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@96
    move-result-object v0

    #@97
    .line 118
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/hardware/display/IDisplayManager$Stub;->forgetWifiDisplay(Ljava/lang/String;)V

    #@9a
    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9d
    goto/16 :goto_9

    #@9f
    .line 124
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_9f
    const-string v4, "android.hardware.display.IDisplayManager"

    #@a1
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a4
    .line 125
    invoke-virtual {p0}, Landroid/hardware/display/IDisplayManager$Stub;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    #@a7
    move-result-object v2

    #@a8
    .line 126
    .local v2, _result:Landroid/hardware/display/WifiDisplayStatus;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ab
    .line 127
    if-eqz v2, :cond_b5

    #@ad
    .line 128
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b0
    .line 129
    invoke-virtual {v2, p3, v3}, Landroid/hardware/display/WifiDisplayStatus;->writeToParcel(Landroid/os/Parcel;I)V

    #@b3
    goto/16 :goto_9

    #@b5
    .line 132
    :cond_b5
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@b8
    goto/16 :goto_9

    #@ba
    .line 39
    :sswitch_data_ba
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_3d
        0x4 -> :sswitch_51
        0x5 -> :sswitch_5d
        0x6 -> :sswitch_6d
        0x7 -> :sswitch_79
        0x8 -> :sswitch_8e
        0x9 -> :sswitch_9f
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
