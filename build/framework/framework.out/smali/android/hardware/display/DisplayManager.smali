.class public final Landroid/hardware/display/DisplayManager;
.super Ljava/lang/Object;
.source "DisplayManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/display/DisplayManager$DisplayListener;
    }
.end annotation


# static fields
.field public static final ACTION_WIFI_DISPLAY_STATUS_CHANGED:Ljava/lang/String; = "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

.field private static final DEBUG:Z = false

.field public static final DISPLAY_CATEGORY_PRESENTATION:Ljava/lang/String; = "android.hardware.display.category.PRESENTATION"

.field public static final EXTRA_WIFI_DISPLAY_STATUS:Ljava/lang/String; = "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

.field private static final TAG:Ljava/lang/String; = "DisplayManager"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDisplays:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/Display;",
            ">;"
        }
    .end annotation
.end field

.field private final mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

.field private final mLock:Ljava/lang/Object;

.field private final mTempDisplays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/Display;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/hardware/display/DisplayManager;->mLock:Ljava/lang/Object;

    #@a
    .line 43
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/hardware/display/DisplayManager;->mDisplays:Landroid/util/SparseArray;

    #@11
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    #@13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v0, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@18
    .line 83
    iput-object p1, p0, Landroid/hardware/display/DisplayManager;->mContext:Landroid/content/Context;

    #@1a
    .line 84
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    #@1d
    move-result-object v0

    #@1e
    iput-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@20
    .line 85
    return-void
.end method

.method private addMatchingDisplaysLocked(Ljava/util/ArrayList;[II)V
    .registers 8
    .parameter
    .parameter "displayIds"
    .parameter "matchType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/Display;",
            ">;[II)V"
        }
    .end annotation

    #@0
    .prologue
    .line 147
    .local p1, displays:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/Display;>;"
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    array-length v2, p2

    #@2
    if-ge v1, v2, :cond_1b

    #@4
    .line 148
    aget v2, p2, v1

    #@6
    const/4 v3, 0x1

    #@7
    invoke-direct {p0, v2, v3}, Landroid/hardware/display/DisplayManager;->getOrCreateDisplayLocked(IZ)Landroid/view/Display;

    #@a
    move-result-object v0

    #@b
    .line 149
    .local v0, display:Landroid/view/Display;
    if-eqz v0, :cond_18

    #@d
    if-ltz p3, :cond_15

    #@f
    invoke-virtual {v0}, Landroid/view/Display;->getType()I

    #@12
    move-result v2

    #@13
    if-ne v2, p3, :cond_18

    #@15
    .line 151
    :cond_15
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 147
    :cond_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 154
    .end local v0           #display:Landroid/view/Display;
    :cond_1b
    return-void
.end method

.method private getOrCreateDisplayLocked(IZ)Landroid/view/Display;
    .registers 6
    .parameter "displayId"
    .parameter "assumeValid"

    #@0
    .prologue
    .line 157
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mDisplays:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/Display;

    #@8
    .line 158
    .local v0, display:Landroid/view/Display;
    if-nez v0, :cond_1e

    #@a
    .line 159
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@c
    iget-object v2, p0, Landroid/hardware/display/DisplayManager;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v2, p1}, Landroid/content/Context;->getCompatibilityInfo(I)Landroid/view/CompatibilityInfoHolder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v1, p1, v2}, Landroid/hardware/display/DisplayManagerGlobal;->getCompatibleDisplay(ILandroid/view/CompatibilityInfoHolder;)Landroid/view/Display;

    #@15
    move-result-object v0

    #@16
    .line 161
    if-eqz v0, :cond_1d

    #@18
    .line 162
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mDisplays:Landroid/util/SparseArray;

    #@1a
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@1d
    .line 167
    :cond_1d
    :goto_1d
    return-object v0

    #@1e
    .line 164
    :cond_1e
    if-nez p2, :cond_1d

    #@20
    invoke-virtual {v0}, Landroid/view/Display;->isValid()Z

    #@23
    move-result v1

    #@24
    if-nez v1, :cond_1d

    #@26
    .line 165
    const/4 v0, 0x0

    #@27
    goto :goto_1d
.end method


# virtual methods
.method public connectWifiDisplay(Ljava/lang/String;)V
    .registers 3
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 219
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManagerGlobal;->connectWifiDisplay(Ljava/lang/String;)V

    #@5
    .line 220
    return-void
.end method

.method public disconnectWifiDisplay()V
    .registers 2

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManagerGlobal;->disconnectWifiDisplay()V

    #@5
    .line 229
    return-void
.end method

.method public forgetWifiDisplay(Ljava/lang/String;)V
    .registers 3
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManagerGlobal;->forgetWifiDisplay(Ljava/lang/String;)V

    #@5
    .line 263
    return-void
.end method

.method public getDisplay(I)Landroid/view/Display;
    .registers 4
    .parameter "displayId"

    #@0
    .prologue
    .line 97
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 98
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-direct {p0, p1, v0}, Landroid/hardware/display/DisplayManager;->getOrCreateDisplayLocked(IZ)Landroid/view/Display;

    #@7
    move-result-object v0

    #@8
    monitor-exit v1

    #@9
    return-object v0

    #@a
    .line 99
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public getDisplays()[Landroid/view/Display;
    .registers 2

    #@0
    .prologue
    .line 108
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getDisplays(Ljava/lang/String;)[Landroid/view/Display;
    .registers 6
    .parameter "category"

    #@0
    .prologue
    .line 128
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v1}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayIds()[I

    #@5
    move-result-object v0

    #@6
    .line 129
    .local v0, displayIds:[I
    iget-object v2, p0, Landroid/hardware/display/DisplayManager;->mLock:Ljava/lang/Object;

    #@8
    monitor-enter v2

    #@9
    .line 131
    if-nez p1, :cond_28

    #@b
    .line 132
    :try_start_b
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@d
    const/4 v3, -0x1

    #@e
    invoke-direct {p0, v1, v0, v3}, Landroid/hardware/display/DisplayManager;->addMatchingDisplaysLocked(Ljava/util/ArrayList;[II)V

    #@11
    .line 138
    :cond_11
    :goto_11
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@13
    iget-object v3, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v3

    #@19
    new-array v3, v3, [Landroid/view/Display;

    #@1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, [Landroid/view/Display;
    :try_end_21
    .catchall {:try_start_b .. :try_end_21} :catchall_43

    #@21
    .line 140
    :try_start_21
    iget-object v3, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@26
    monitor-exit v2
    :try_end_27
    .catchall {:try_start_21 .. :try_end_27} :catchall_4a

    #@27
    return-object v1

    #@28
    .line 133
    :cond_28
    :try_start_28
    const-string v1, "android.hardware.display.category.PRESENTATION"

    #@2a
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v1

    #@2e
    if-eqz v1, :cond_11

    #@30
    .line 134
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@32
    const/4 v3, 0x3

    #@33
    invoke-direct {p0, v1, v0, v3}, Landroid/hardware/display/DisplayManager;->addMatchingDisplaysLocked(Ljava/util/ArrayList;[II)V

    #@36
    .line 135
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@38
    const/4 v3, 0x2

    #@39
    invoke-direct {p0, v1, v0, v3}, Landroid/hardware/display/DisplayManager;->addMatchingDisplaysLocked(Ljava/util/ArrayList;[II)V

    #@3c
    .line 136
    iget-object v1, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@3e
    const/4 v3, 0x4

    #@3f
    invoke-direct {p0, v1, v0, v3}, Landroid/hardware/display/DisplayManager;->addMatchingDisplaysLocked(Ljava/util/ArrayList;[II)V
    :try_end_42
    .catchall {:try_start_28 .. :try_end_42} :catchall_43

    #@42
    goto :goto_11

    #@43
    .line 140
    :catchall_43
    move-exception v1

    #@44
    :try_start_44
    iget-object v3, p0, Landroid/hardware/display/DisplayManager;->mTempDisplays:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@49
    throw v1

    #@4a
    .line 142
    :catchall_4a
    move-exception v1

    #@4b
    monitor-exit v2
    :try_end_4c
    .catchall {:try_start_44 .. :try_end_4c} :catchall_4a

    #@4c
    throw v1
.end method

.method public getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;
    .registers 2

    #@0
    .prologue
    .line 274
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManagerGlobal;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
    .registers 4
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/hardware/display/DisplayManagerGlobal;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    #@5
    .line 182
    return-void
.end method

.method public renameWifiDisplay(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "deviceAddress"
    .parameter "alias"

    #@0
    .prologue
    .line 247
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/hardware/display/DisplayManagerGlobal;->renameWifiDisplay(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 248
    return-void
.end method

.method public scanWifiDisplays()V
    .registers 2

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManagerGlobal;->scanWifiDisplays()V

    #@5
    .line 202
    return-void
.end method

.method public unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Landroid/hardware/display/DisplayManager;->mGlobal:Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManagerGlobal;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    #@5
    .line 193
    return-void
.end method
