.class public final Landroid/hardware/display/DisplayManagerGlobal;
.super Ljava/lang/Object;
.source "DisplayManagerGlobal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/display/DisplayManagerGlobal$1;,
        Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;,
        Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final EVENT_DISPLAY_ADDED:I = 0x1

.field public static final EVENT_DISPLAY_CHANGED:I = 0x2

.field public static final EVENT_DISPLAY_REMOVED:I = 0x3

.field private static final TAG:Ljava/lang/String; = "DisplayManager"

.field private static final USE_CACHE:Z

.field private static sInstance:Landroid/hardware/display/DisplayManagerGlobal;


# instance fields
.field private mCallback:Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;

.field private mDisplayIdCache:[I

.field private final mDisplayInfoCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/DisplayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mDisplayListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final mDm:Landroid/hardware/display/IDisplayManager;

.field private final mLock:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Landroid/hardware/display/IDisplayManager;)V
    .registers 3
    .parameter "dm"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/hardware/display/DisplayManagerGlobal;->mLock:Ljava/lang/Object;

    #@a
    .line 66
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@11
    .line 69
    new-instance v0, Landroid/util/SparseArray;

    #@13
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@16
    iput-object v0, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayInfoCache:Landroid/util/SparseArray;

    #@18
    .line 73
    iput-object p1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@1a
    .line 74
    return-void
.end method

.method static synthetic access$100(Landroid/hardware/display/DisplayManagerGlobal;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/hardware/display/DisplayManagerGlobal;->handleDisplayEvent(II)V

    #@3
    return-void
.end method

.method private findDisplayListenerLocked(Landroid/hardware/display/DisplayManager$DisplayListener;)I
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 218
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 219
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_19

    #@9
    .line 220
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;

    #@11
    iget-object v2, v2, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->mListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@13
    if-ne v2, p1, :cond_16

    #@15
    .line 224
    .end local v0           #i:I
    :goto_15
    return v0

    #@16
    .line 219
    .restart local v0       #i:I
    :cond_16
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_7

    #@19
    .line 224
    :cond_19
    const/4 v0, -0x1

    #@1a
    goto :goto_15
.end method

.method public static getInstance()Landroid/hardware/display/DisplayManagerGlobal;
    .registers 4

    #@0
    .prologue
    .line 83
    const-class v2, Landroid/hardware/display/DisplayManagerGlobal;

    #@2
    monitor-enter v2

    #@3
    .line 84
    :try_start_3
    sget-object v1, Landroid/hardware/display/DisplayManagerGlobal;->sInstance:Landroid/hardware/display/DisplayManagerGlobal;

    #@5
    if-nez v1, :cond_1a

    #@7
    .line 85
    const-string v1, "display"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 86
    .local v0, b:Landroid/os/IBinder;
    if-eqz v0, :cond_1a

    #@f
    .line 87
    new-instance v1, Landroid/hardware/display/DisplayManagerGlobal;

    #@11
    invoke-static {v0}, Landroid/hardware/display/IDisplayManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/display/IDisplayManager;

    #@14
    move-result-object v3

    #@15
    invoke-direct {v1, v3}, Landroid/hardware/display/DisplayManagerGlobal;-><init>(Landroid/hardware/display/IDisplayManager;)V

    #@18
    sput-object v1, Landroid/hardware/display/DisplayManagerGlobal;->sInstance:Landroid/hardware/display/DisplayManagerGlobal;

    #@1a
    .line 90
    :cond_1a
    sget-object v1, Landroid/hardware/display/DisplayManagerGlobal;->sInstance:Landroid/hardware/display/DisplayManagerGlobal;

    #@1c
    monitor-exit v2

    #@1d
    return-object v1

    #@1e
    .line 91
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit v2
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v1
.end method

.method private handleDisplayEvent(II)V
    .registers 7
    .parameter "displayId"
    .parameter "event"

    #@0
    .prologue
    .line 240
    iget-object v3, p0, Landroid/hardware/display/DisplayManagerGlobal;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 249
    :try_start_3
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    .line 250
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_1a

    #@c
    .line 251
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;

    #@14
    invoke-virtual {v2, p1, p2}, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->sendDisplayEvent(II)V

    #@17
    .line 250
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_a

    #@1a
    .line 253
    :cond_1a
    monitor-exit v3

    #@1b
    .line 254
    return-void

    #@1c
    .line 253
    .end local v0           #i:I
    .end local v1           #numListeners:I
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v2
.end method

.method private registerCallbackIfNeededLocked()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 228
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mCallback:Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;

    #@3
    if-nez v1, :cond_13

    #@5
    .line 229
    new-instance v1, Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;

    #@7
    invoke-direct {v1, p0, v3}, Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;-><init>(Landroid/hardware/display/DisplayManagerGlobal;Landroid/hardware/display/DisplayManagerGlobal$1;)V

    #@a
    iput-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mCallback:Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;

    #@c
    .line 231
    :try_start_c
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@e
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mCallback:Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;

    #@10
    invoke-interface {v1, v2}, Landroid/hardware/display/IDisplayManager;->registerCallback(Landroid/hardware/display/IDisplayManagerCallback;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_13} :catch_14

    #@13
    .line 237
    :cond_13
    :goto_13
    return-void

    #@14
    .line 232
    :catch_14
    move-exception v0

    #@15
    .line 233
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@17
    const-string v2, "Failed to register callback with display manager service."

    #@19
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    .line 234
    iput-object v3, p0, Landroid/hardware/display/DisplayManagerGlobal;->mCallback:Landroid/hardware/display/DisplayManagerGlobal$DisplayManagerCallback;

    #@1e
    goto :goto_13
.end method


# virtual methods
.method public connectWifiDisplay(Ljava/lang/String;)V
    .registers 6
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 265
    if-nez p1, :cond_a

    #@2
    .line 266
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "deviceAddress must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 270
    :cond_a
    :try_start_a
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@c
    invoke-interface {v1, p1}, Landroid/hardware/display/IDisplayManager;->connectWifiDisplay(Ljava/lang/String;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    .line 274
    :goto_f
    return-void

    #@10
    .line 271
    :catch_10
    move-exception v0

    #@11
    .line 272
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "Failed to connect to Wifi display "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, "."

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_f
.end method

.method public disconnectWifiDisplay()V
    .registers 4

    #@0
    .prologue
    .line 278
    :try_start_0
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@2
    invoke-interface {v1}, Landroid/hardware/display/IDisplayManager;->disconnectWifiDisplay()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 282
    :goto_5
    return-void

    #@6
    .line 279
    :catch_6
    move-exception v0

    #@7
    .line 280
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@9
    const-string v2, "Failed to disconnect from Wifi display."

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public forgetWifiDisplay(Ljava/lang/String;)V
    .registers 5
    .parameter "deviceAddress"

    #@0
    .prologue
    .line 298
    if-nez p1, :cond_a

    #@2
    .line 299
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "deviceAddress must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 303
    :cond_a
    :try_start_a
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@c
    invoke-interface {v1, p1}, Landroid/hardware/display/IDisplayManager;->forgetWifiDisplay(Ljava/lang/String;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    .line 307
    :goto_f
    return-void

    #@10
    .line 304
    :catch_10
    move-exception v0

    #@11
    .line 305
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@13
    const-string v2, "Failed to forget Wifi display."

    #@15
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    goto :goto_f
.end method

.method public getCompatibleDisplay(ILandroid/view/CompatibilityInfoHolder;)Landroid/view/Display;
    .registers 5
    .parameter "displayId"
    .parameter "cih"

    #@0
    .prologue
    .line 171
    invoke-virtual {p0, p1}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@3
    move-result-object v0

    #@4
    .line 172
    .local v0, displayInfo:Landroid/view/DisplayInfo;
    if-nez v0, :cond_8

    #@6
    .line 173
    const/4 v1, 0x0

    #@7
    .line 175
    :goto_7
    return-object v1

    #@8
    :cond_8
    new-instance v1, Landroid/view/Display;

    #@a
    invoke-direct {v1, p0, p1, v0, p2}, Landroid/view/Display;-><init>(Landroid/hardware/display/DisplayManagerGlobal;ILandroid/view/DisplayInfo;Landroid/view/CompatibilityInfoHolder;)V

    #@d
    goto :goto_7
.end method

.method public getDisplayIds()[I
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 140
    :try_start_1
    iget-object v3, p0, Landroid/hardware/display/DisplayManagerGlobal;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v3
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_4} :catch_12

    #@4
    .line 147
    :try_start_4
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@6
    invoke-interface {v2}, Landroid/hardware/display/IDisplayManager;->getDisplayIds()[I

    #@9
    move-result-object v0

    #@a
    .line 151
    .local v0, displayIds:[I
    invoke-direct {p0}, Landroid/hardware/display/DisplayManagerGlobal;->registerCallbackIfNeededLocked()V

    #@d
    .line 152
    monitor-exit v3

    #@e
    .line 156
    .end local v0           #displayIds:[I
    :goto_e
    return-object v0

    #@f
    .line 153
    :catchall_f
    move-exception v2

    #@10
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_f

    #@11
    :try_start_11
    throw v2
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_12} :catch_12

    #@12
    .line 154
    :catch_12
    move-exception v1

    #@13
    .line 155
    .local v1, ex:Landroid/os/RemoteException;
    const-string v2, "DisplayManager"

    #@15
    const-string v3, "Could not get display ids from display manager."

    #@17
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 156
    const/4 v2, 0x1

    #@1b
    new-array v0, v2, [I

    #@1d
    aput v4, v0, v4

    #@1f
    goto :goto_e
.end method

.method public getDisplayInfo(I)Landroid/view/DisplayInfo;
    .registers 7
    .parameter "displayId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 103
    :try_start_1
    iget-object v4, p0, Landroid/hardware/display/DisplayManagerGlobal;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v4
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_4} :catch_17

    #@4
    .line 112
    :try_start_4
    iget-object v3, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@6
    invoke-interface {v3, p1}, Landroid/hardware/display/IDisplayManager;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    #@9
    move-result-object v1

    #@a
    .line 113
    .local v1, info:Landroid/view/DisplayInfo;
    if-nez v1, :cond_f

    #@c
    .line 114
    monitor-exit v4

    #@d
    move-object v1, v2

    #@e
    .line 129
    .end local v1           #info:Landroid/view/DisplayInfo;
    :goto_e
    return-object v1

    #@f
    .line 120
    .restart local v1       #info:Landroid/view/DisplayInfo;
    :cond_f
    invoke-direct {p0}, Landroid/hardware/display/DisplayManagerGlobal;->registerCallbackIfNeededLocked()V

    #@12
    .line 125
    monitor-exit v4

    #@13
    goto :goto_e

    #@14
    .line 126
    .end local v1           #info:Landroid/view/DisplayInfo;
    :catchall_14
    move-exception v3

    #@15
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_4 .. :try_end_16} :catchall_14

    #@16
    :try_start_16
    throw v3
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_17} :catch_17

    #@17
    .line 127
    :catch_17
    move-exception v0

    #@18
    .line 128
    .local v0, ex:Landroid/os/RemoteException;
    const-string v3, "DisplayManager"

    #@1a
    const-string v4, "Could not get display information from display manager."

    #@1c
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    move-object v1, v2

    #@20
    .line 129
    goto :goto_e
.end method

.method public getRealDisplay(I)Landroid/view/Display;
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/hardware/display/DisplayManagerGlobal;->getCompatibleDisplay(ILandroid/view/CompatibilityInfoHolder;)Landroid/view/Display;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;
    .registers 4

    #@0
    .prologue
    .line 311
    :try_start_0
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@2
    invoke-interface {v1}, Landroid/hardware/display/IDisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 314
    :goto_6
    return-object v1

    #@7
    .line 312
    :catch_7
    move-exception v0

    #@8
    .line 313
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@a
    const-string v2, "Failed to get Wifi display status."

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 314
    new-instance v1, Landroid/hardware/display/WifiDisplayStatus;

    #@11
    invoke-direct {v1}, Landroid/hardware/display/WifiDisplayStatus;-><init>()V

    #@14
    goto :goto_6
.end method

.method public registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
    .registers 7
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 189
    if-nez p1, :cond_b

    #@2
    .line 190
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "listener must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 193
    :cond_b
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mLock:Ljava/lang/Object;

    #@d
    monitor-enter v2

    #@e
    .line 194
    :try_start_e
    invoke-direct {p0, p1}, Landroid/hardware/display/DisplayManagerGlobal;->findDisplayListenerLocked(Landroid/hardware/display/DisplayManager$DisplayListener;)I

    #@11
    move-result v0

    #@12
    .line 195
    .local v0, index:I
    if-gez v0, :cond_21

    #@14
    .line 196
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@16
    new-instance v3, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;

    #@18
    invoke-direct {v3, p1, p2}, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;-><init>(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    #@1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 197
    invoke-direct {p0}, Landroid/hardware/display/DisplayManagerGlobal;->registerCallbackIfNeededLocked()V

    #@21
    .line 199
    :cond_21
    monitor-exit v2

    #@22
    .line 200
    return-void

    #@23
    .line 199
    .end local v0           #index:I
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_e .. :try_end_25} :catchall_23

    #@25
    throw v1
.end method

.method public renameWifiDisplay(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "deviceAddress"
    .parameter "alias"

    #@0
    .prologue
    .line 285
    if-nez p1, :cond_a

    #@2
    .line 286
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "deviceAddress must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 290
    :cond_a
    :try_start_a
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@c
    invoke-interface {v1, p1, p2}, Landroid/hardware/display/IDisplayManager;->renameWifiDisplay(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_10

    #@f
    .line 295
    :goto_f
    return-void

    #@10
    .line 291
    :catch_10
    move-exception v0

    #@11
    .line 292
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "Failed to rename Wifi display "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, " with alias "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    const-string v3, "."

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    goto :goto_f
.end method

.method public scanWifiDisplays()V
    .registers 4

    #@0
    .prologue
    .line 258
    :try_start_0
    iget-object v1, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDm:Landroid/hardware/display/IDisplayManager;

    #@2
    invoke-interface {v1}, Landroid/hardware/display/IDisplayManager;->scanWifiDisplays()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 262
    :goto_5
    return-void

    #@6
    .line 259
    :catch_6
    move-exception v0

    #@7
    .line 260
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "DisplayManager"

    #@9
    const-string v2, "Failed to scan for Wifi displays."

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 203
    if-nez p1, :cond_b

    #@2
    .line 204
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "listener must not be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 207
    :cond_b
    iget-object v3, p0, Landroid/hardware/display/DisplayManagerGlobal;->mLock:Ljava/lang/Object;

    #@d
    monitor-enter v3

    #@e
    .line 208
    :try_start_e
    invoke-direct {p0, p1}, Landroid/hardware/display/DisplayManagerGlobal;->findDisplayListenerLocked(Landroid/hardware/display/DisplayManager$DisplayListener;)I

    #@11
    move-result v1

    #@12
    .line 209
    .local v1, index:I
    if-ltz v1, :cond_24

    #@14
    .line 210
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;

    #@1c
    .line 211
    .local v0, d:Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->clearEvents()V

    #@1f
    .line 212
    iget-object v2, p0, Landroid/hardware/display/DisplayManagerGlobal;->mDisplayListeners:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@24
    .line 214
    .end local v0           #d:Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;
    :cond_24
    monitor-exit v3

    #@25
    .line 215
    return-void

    #@26
    .line 214
    .end local v1           #index:I
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_e .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method
