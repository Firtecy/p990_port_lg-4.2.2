.class final Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;
.super Landroid/os/Handler;
.source "DisplayManagerGlobal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/display/DisplayManagerGlobal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DisplayListenerDelegate"
.end annotation


# instance fields
.field public final mListener:Landroid/hardware/display/DisplayManager$DisplayListener;


# direct methods
.method public constructor <init>(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
    .registers 6
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 332
    if-eqz p2, :cond_e

    #@2
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    :goto_6
    const/4 v1, 0x0

    #@7
    const/4 v2, 0x1

    #@8
    invoke-direct {p0, v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    #@b
    .line 333
    iput-object p1, p0, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->mListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@d
    .line 334
    return-void

    #@e
    .line 332
    :cond_e
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@11
    move-result-object v0

    #@12
    goto :goto_6
.end method


# virtual methods
.method public clearEvents()V
    .registers 2

    #@0
    .prologue
    .line 342
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@4
    .line 343
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 347
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_1e

    #@5
    .line 358
    :goto_5
    return-void

    #@6
    .line 349
    :pswitch_6
    iget-object v0, p0, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->mListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@8
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@a
    invoke-interface {v0, v1}, Landroid/hardware/display/DisplayManager$DisplayListener;->onDisplayAdded(I)V

    #@d
    goto :goto_5

    #@e
    .line 352
    :pswitch_e
    iget-object v0, p0, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->mListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@10
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@12
    invoke-interface {v0, v1}, Landroid/hardware/display/DisplayManager$DisplayListener;->onDisplayChanged(I)V

    #@15
    goto :goto_5

    #@16
    .line 355
    :pswitch_16
    iget-object v0, p0, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->mListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    #@18
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@1a
    invoke-interface {v0, v1}, Landroid/hardware/display/DisplayManager$DisplayListener;->onDisplayRemoved(I)V

    #@1d
    goto :goto_5

    #@1e
    .line 347
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_e
        :pswitch_16
    .end packed-switch
.end method

.method public sendDisplayEvent(II)V
    .registers 5
    .parameter "displayId"
    .parameter "event"

    #@0
    .prologue
    .line 337
    const/4 v1, 0x0

    #@1
    invoke-virtual {p0, p2, p1, v1}, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->obtainMessage(III)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    .line 338
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, v0}, Landroid/hardware/display/DisplayManagerGlobal$DisplayListenerDelegate;->sendMessage(Landroid/os/Message;)Z

    #@8
    .line 339
    return-void
.end method
