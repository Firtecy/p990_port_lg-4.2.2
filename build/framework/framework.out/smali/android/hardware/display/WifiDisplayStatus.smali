.class public final Landroid/hardware/display/WifiDisplayStatus;
.super Ljava/lang/Object;
.source "WifiDisplayStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/hardware/display/WifiDisplayStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DISPLAY_STATE_CONNECTED:I = 0x2

.field public static final DISPLAY_STATE_CONNECTING:I = 0x1

.field public static final DISPLAY_STATE_NOT_CONNECTED:I = 0x0

.field public static final FEATURE_STATE_DISABLED:I = 0x1

.field public static final FEATURE_STATE_OFF:I = 0x2

.field public static final FEATURE_STATE_ON:I = 0x3

.field public static final FEATURE_STATE_UNAVAILABLE:I = 0x0

.field public static final SCAN_STATE_NOT_SCANNING:I = 0x0

.field public static final SCAN_STATE_SCANNING:I = 0x1


# instance fields
.field private final mActiveDisplay:Landroid/hardware/display/WifiDisplay;

.field private final mActiveDisplayState:I

.field private final mAvailableDisplays:[Landroid/hardware/display/WifiDisplay;

.field private final mFeatureState:I

.field private final mRememberedDisplays:[Landroid/hardware/display/WifiDisplay;

.field private final mScanState:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 62
    new-instance v0, Landroid/hardware/display/WifiDisplayStatus$1;

    #@2
    invoke-direct {v0}, Landroid/hardware/display/WifiDisplayStatus$1;-><init>()V

    #@5
    sput-object v0, Landroid/hardware/display/WifiDisplayStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 93
    const/4 v4, 0x0

    #@2
    sget-object v5, Landroid/hardware/display/WifiDisplay;->EMPTY_ARRAY:[Landroid/hardware/display/WifiDisplay;

    #@4
    sget-object v6, Landroid/hardware/display/WifiDisplay;->EMPTY_ARRAY:[Landroid/hardware/display/WifiDisplay;

    #@6
    move-object v0, p0

    #@7
    move v2, v1

    #@8
    move v3, v1

    #@9
    invoke-direct/range {v0 .. v6}, Landroid/hardware/display/WifiDisplayStatus;-><init>(IIILandroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)V

    #@c
    .line 95
    return-void
.end method

.method public constructor <init>(IIILandroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;[Landroid/hardware/display/WifiDisplay;)V
    .registers 9
    .parameter "featureState"
    .parameter "scanState"
    .parameter "activeDisplayState"
    .parameter "activeDisplay"
    .parameter "availableDisplays"
    .parameter "rememberedDisplays"

    #@0
    .prologue
    .line 99
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 100
    if-nez p5, :cond_d

    #@5
    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "availableDisplays must not be null"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 103
    :cond_d
    if-nez p6, :cond_18

    #@f
    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v1, "rememberedDisplays must not be null"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 107
    :cond_18
    iput p1, p0, Landroid/hardware/display/WifiDisplayStatus;->mFeatureState:I

    #@1a
    .line 108
    iput p2, p0, Landroid/hardware/display/WifiDisplayStatus;->mScanState:I

    #@1c
    .line 109
    iput p3, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplayState:I

    #@1e
    .line 110
    iput-object p4, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplay:Landroid/hardware/display/WifiDisplay;

    #@20
    .line 111
    iput-object p5, p0, Landroid/hardware/display/WifiDisplayStatus;->mAvailableDisplays:[Landroid/hardware/display/WifiDisplay;

    #@22
    .line 112
    iput-object p6, p0, Landroid/hardware/display/WifiDisplayStatus;->mRememberedDisplays:[Landroid/hardware/display/WifiDisplay;

    #@24
    .line 113
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 201
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getActiveDisplay()Landroid/hardware/display/WifiDisplay;
    .registers 2

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplay:Landroid/hardware/display/WifiDisplay;

    #@2
    return-object v0
.end method

.method public getActiveDisplayState()I
    .registers 2

    #@0
    .prologue
    .line 143
    iget v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplayState:I

    #@2
    return v0
.end method

.method public getAvailableDisplays()[Landroid/hardware/display/WifiDisplay;
    .registers 2

    #@0
    .prologue
    .line 162
    iget-object v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mAvailableDisplays:[Landroid/hardware/display/WifiDisplay;

    #@2
    return-object v0
.end method

.method public getFeatureState()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mFeatureState:I

    #@2
    return v0
.end method

.method public getRememberedDisplays()[Landroid/hardware/display/WifiDisplay;
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mRememberedDisplays:[Landroid/hardware/display/WifiDisplay;

    #@2
    return-object v0
.end method

.method public getScanState()I
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mScanState:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "WifiDisplayStatus{featureState="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/hardware/display/WifiDisplayStatus;->mFeatureState:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", scanState="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/hardware/display/WifiDisplayStatus;->mScanState:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", activeDisplayState="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplayState:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", activeDisplay="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplay:Landroid/hardware/display/WifiDisplay;

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ", availableDisplays="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Landroid/hardware/display/WifiDisplayStatus;->mAvailableDisplays:[Landroid/hardware/display/WifiDisplay;

    #@3d
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    const-string v1, ", rememberedDisplays="

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    iget-object v1, p0, Landroid/hardware/display/WifiDisplayStatus;->mRememberedDisplays:[Landroid/hardware/display/WifiDisplay;

    #@4d
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string/jumbo v1, "}"

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v0

    #@60
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 177
    iget v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mFeatureState:I

    #@2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 178
    iget v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mScanState:I

    #@7
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 179
    iget v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplayState:I

    #@c
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 181
    iget-object v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplay:Landroid/hardware/display/WifiDisplay;

    #@11
    if-eqz v4, :cond_30

    #@13
    .line 182
    const/4 v4, 0x1

    #@14
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 183
    iget-object v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mActiveDisplay:Landroid/hardware/display/WifiDisplay;

    #@19
    invoke-virtual {v4, p1, p2}, Landroid/hardware/display/WifiDisplay;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c
    .line 188
    :goto_1c
    iget-object v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mAvailableDisplays:[Landroid/hardware/display/WifiDisplay;

    #@1e
    array-length v4, v4

    #@1f
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 189
    iget-object v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mAvailableDisplays:[Landroid/hardware/display/WifiDisplay;

    #@24
    .local v0, arr$:[Landroid/hardware/display/WifiDisplay;
    array-length v3, v0

    #@25
    .local v3, len$:I
    const/4 v2, 0x0

    #@26
    .local v2, i$:I
    :goto_26
    if-ge v2, v3, :cond_35

    #@28
    aget-object v1, v0, v2

    #@2a
    .line 190
    .local v1, display:Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v1, p1, p2}, Landroid/hardware/display/WifiDisplay;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 189
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_26

    #@30
    .line 185
    .end local v0           #arr$:[Landroid/hardware/display/WifiDisplay;
    .end local v1           #display:Landroid/hardware/display/WifiDisplay;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_30
    const/4 v4, 0x0

    #@31
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    goto :goto_1c

    #@35
    .line 193
    .restart local v0       #arr$:[Landroid/hardware/display/WifiDisplay;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_35
    iget-object v4, p0, Landroid/hardware/display/WifiDisplayStatus;->mRememberedDisplays:[Landroid/hardware/display/WifiDisplay;

    #@37
    array-length v4, v4

    #@38
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    .line 194
    iget-object v0, p0, Landroid/hardware/display/WifiDisplayStatus;->mRememberedDisplays:[Landroid/hardware/display/WifiDisplay;

    #@3d
    array-length v3, v0

    #@3e
    const/4 v2, 0x0

    #@3f
    :goto_3f
    if-ge v2, v3, :cond_49

    #@41
    aget-object v1, v0, v2

    #@43
    .line 195
    .restart local v1       #display:Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v1, p1, p2}, Landroid/hardware/display/WifiDisplay;->writeToParcel(Landroid/os/Parcel;I)V

    #@46
    .line 194
    add-int/lit8 v2, v2, 0x1

    #@48
    goto :goto_3f

    #@49
    .line 197
    .end local v1           #display:Landroid/hardware/display/WifiDisplay;
    :cond_49
    return-void
.end method
