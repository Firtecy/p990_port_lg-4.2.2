.class public Landroid/hardware/Camera$Coordinate;
.super Ljava/lang/Object;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Coordinate"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/Camera;

.field public xCoordinate:I

.field public yCoordinate:I


# direct methods
.method public constructor <init>(Landroid/hardware/Camera;II)V
    .registers 4
    .parameter
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1862
    iput-object p1, p0, Landroid/hardware/Camera$Coordinate;->this$0:Landroid/hardware/Camera;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1863
    iput p2, p0, Landroid/hardware/Camera$Coordinate;->xCoordinate:I

    #@7
    .line 1864
    iput p3, p0, Landroid/hardware/Camera$Coordinate;->yCoordinate:I

    #@9
    .line 1865
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1875
    instance-of v2, p1, Landroid/hardware/Camera$Coordinate;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1879
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 1878
    check-cast v0, Landroid/hardware/Camera$Coordinate;

    #@9
    .line 1879
    .local v0, c:Landroid/hardware/Camera$Coordinate;
    iget v2, p0, Landroid/hardware/Camera$Coordinate;->xCoordinate:I

    #@b
    iget v3, v0, Landroid/hardware/Camera$Coordinate;->xCoordinate:I

    #@d
    if-ne v2, v3, :cond_5

    #@f
    iget v2, p0, Landroid/hardware/Camera$Coordinate;->yCoordinate:I

    #@11
    iget v3, v0, Landroid/hardware/Camera$Coordinate;->yCoordinate:I

    #@13
    if-ne v2, v3, :cond_5

    #@15
    const/4 v1, 0x1

    #@16
    goto :goto_5
.end method
