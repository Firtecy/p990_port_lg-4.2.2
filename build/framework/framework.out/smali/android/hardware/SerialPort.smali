.class public Landroid/hardware/SerialPort;
.super Ljava/lang/Object;
.source "SerialPort.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SerialPort"


# instance fields
.field private mFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private final mName:Ljava/lang/String;

.field private mNativeContext:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    iput-object p1, p0, Landroid/hardware/SerialPort;->mName:Ljava/lang/String;

    #@5
    .line 49
    return-void
.end method

.method private native native_close()V
.end method

.method private native native_open(Ljava/io/FileDescriptor;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native native_read_array([BI)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native native_read_direct(Ljava/nio/ByteBuffer;I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native native_send_break()V
.end method

.method private native native_write_array([BI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native native_write_direct(Ljava/nio/ByteBuffer;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Landroid/hardware/SerialPort;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 69
    iget-object v0, p0, Landroid/hardware/SerialPort;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@6
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    #@9
    .line 70
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/hardware/SerialPort;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@c
    .line 72
    :cond_c
    invoke-direct {p0}, Landroid/hardware/SerialPort;->native_close()V

    #@f
    .line 73
    return-void
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Landroid/hardware/SerialPort;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public open(Landroid/os/ParcelFileDescriptor;I)V
    .registers 4
    .parameter "pfd"
    .parameter "speed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 60
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p2}, Landroid/hardware/SerialPort;->native_open(Ljava/io/FileDescriptor;I)V

    #@7
    .line 61
    iput-object p1, p0, Landroid/hardware/SerialPort;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@9
    .line 62
    return-void
.end method

.method public read(Ljava/nio/ByteBuffer;)I
    .registers 4
    .parameter "buffer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 91
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 92
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    #@9
    move-result v0

    #@a
    invoke-direct {p0, p1, v0}, Landroid/hardware/SerialPort;->native_read_direct(Ljava/nio/ByteBuffer;I)I

    #@d
    move-result v0

    #@e
    .line 94
    :goto_e
    return v0

    #@f
    .line 93
    :cond_f
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_22

    #@15
    .line 94
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    #@1c
    move-result v1

    #@1d
    invoke-direct {p0, v0, v1}, Landroid/hardware/SerialPort;->native_read_array([BI)I

    #@20
    move-result v0

    #@21
    goto :goto_e

    #@22
    .line 96
    :cond_22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@24
    const-string v1, "buffer is not direct and has no array"

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0
.end method

.method public sendBreak()V
    .registers 1

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/hardware/SerialPort;->native_send_break()V

    #@3
    .line 121
    return-void
.end method

.method public write(Ljava/nio/ByteBuffer;I)V
    .registers 5
    .parameter "buffer"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 107
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 108
    invoke-direct {p0, p1, p2}, Landroid/hardware/SerialPort;->native_write_direct(Ljava/nio/ByteBuffer;I)V

    #@9
    .line 114
    :goto_9
    return-void

    #@a
    .line 109
    :cond_a
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 110
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@13
    move-result-object v0

    #@14
    invoke-direct {p0, v0, p2}, Landroid/hardware/SerialPort;->native_write_array([BI)V

    #@17
    goto :goto_9

    #@18
    .line 112
    :cond_18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v1, "buffer is not direct and has no array"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0
.end method
