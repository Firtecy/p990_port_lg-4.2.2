.class Landroid/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable;
.super Ljava/lang/Object;
.source "SystemSensorManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/SystemSensorManager$SensorThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorThreadRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/SystemSensorManager$SensorThread;


# direct methods
.method constructor <init>(Landroid/hardware/SystemSensorManager$SensorThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Landroid/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable;->this$0:Landroid/hardware/SystemSensorManager$SensorThread;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 93
    return-void
.end method

.method private open()Z
    .registers 2

    #@0
    .prologue
    .line 99
    invoke-static {}, Landroid/hardware/SystemSensorManager;->sensors_create_queue()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Landroid/hardware/SystemSensorManager;->access$002(I)I

    #@7
    .line 100
    const/4 v0, 0x1

    #@8
    return v0
.end method


# virtual methods
.method public run()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, -0x1

    #@2
    const/4 v10, 0x1

    #@3
    .line 105
    const/4 v9, 0x3

    #@4
    new-array v8, v9, [F

    #@6
    .line 106
    .local v8, values:[F
    new-array v6, v10, [I

    #@8
    .line 107
    .local v6, status:[I
    new-array v7, v10, [J

    #@a
    .line 108
    .local v7, timestamp:[J
    const/4 v9, -0x8

    #@b
    invoke-static {v9}, Landroid/os/Process;->setThreadPriority(I)V

    #@e
    .line 110
    invoke-direct {p0}, Landroid/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable;->open()Z

    #@11
    move-result v9

    #@12
    if-nez v9, :cond_15

    #@14
    .line 157
    :goto_14
    return-void

    #@15
    .line 114
    :cond_15
    monitor-enter p0

    #@16
    .line 116
    :try_start_16
    iget-object v9, p0, Landroid/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable;->this$0:Landroid/hardware/SystemSensorManager$SensorThread;

    #@18
    const/4 v10, 0x1

    #@19
    iput-boolean v10, v9, Landroid/hardware/SystemSensorManager$SensorThread;->mSensorsReady:Z

    #@1b
    .line 117
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    #@1e
    .line 118
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_16 .. :try_end_1f} :catchall_6d

    #@1f
    .line 122
    :goto_1f
    invoke-static {}, Landroid/hardware/SystemSensorManager;->access$000()I

    #@22
    move-result v9

    #@23
    invoke-static {v9, v8, v6, v7}, Landroid/hardware/SystemSensorManager;->sensors_data_poll(I[F[I[J)I

    #@26
    move-result v3

    #@27
    .line 124
    .local v3, sensor:I
    aget v0, v6, v12

    #@29
    .line 125
    .local v0, accuracy:I
    sget-object v10, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@2b
    monitor-enter v10

    #@2c
    .line 126
    if-eq v3, v11, :cond_36

    #@2e
    :try_start_2e
    sget-object v9, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    #@33
    move-result v9

    #@34
    if-eqz v9, :cond_70

    #@36
    .line 129
    :cond_36
    if-ne v3, v11, :cond_58

    #@38
    sget-object v9, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    #@3d
    move-result v9

    #@3e
    if-nez v9, :cond_58

    #@40
    .line 131
    const-string v9, "SensorManager"

    #@42
    new-instance v11, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v12, "_sensors_data_poll() failed, we bail out: sensors="

    #@49
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v11

    #@4d
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v11

    #@51
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v11

    #@55
    invoke-static {v9, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 134
    :cond_58
    invoke-static {}, Landroid/hardware/SystemSensorManager;->access$000()I

    #@5b
    move-result v9

    #@5c
    invoke-static {v9}, Landroid/hardware/SystemSensorManager;->sensors_destroy_queue(I)V

    #@5f
    .line 135
    const/4 v9, 0x0

    #@60
    invoke-static {v9}, Landroid/hardware/SystemSensorManager;->access$002(I)I

    #@63
    .line 136
    iget-object v9, p0, Landroid/hardware/SystemSensorManager$SensorThread$SensorThreadRunnable;->this$0:Landroid/hardware/SystemSensorManager$SensorThread;

    #@65
    const/4 v11, 0x0

    #@66
    iput-object v11, v9, Landroid/hardware/SystemSensorManager$SensorThread;->mThread:Ljava/lang/Thread;

    #@68
    .line 137
    monitor-exit v10

    #@69
    goto :goto_14

    #@6a
    .line 154
    :catchall_6a
    move-exception v9

    #@6b
    monitor-exit v10
    :try_end_6c
    .catchall {:try_start_2e .. :try_end_6c} :catchall_6a

    #@6c
    throw v9

    #@6d
    .line 118
    .end local v0           #accuracy:I
    .end local v3           #sensor:I
    :catchall_6d
    move-exception v9

    #@6e
    :try_start_6e
    monitor-exit p0
    :try_end_6f
    .catchall {:try_start_6e .. :try_end_6f} :catchall_6d

    #@6f
    throw v9

    #@70
    .line 139
    .restart local v0       #accuracy:I
    .restart local v3       #sensor:I
    :cond_70
    :try_start_70
    sget-object v9, Landroid/hardware/SystemSensorManager;->sHandleToSensor:Landroid/util/SparseArray;

    #@72
    invoke-virtual {v9, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@75
    move-result-object v4

    #@76
    check-cast v4, Landroid/hardware/Sensor;

    #@78
    .line 140
    .local v4, sensorObject:Landroid/hardware/Sensor;
    if-eqz v4, :cond_97

    #@7a
    .line 143
    sget-object v9, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@7c
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@7f
    move-result v5

    #@80
    .line 144
    .local v5, size:I
    const/4 v1, 0x0

    #@81
    .local v1, i:I
    :goto_81
    if-ge v1, v5, :cond_97

    #@83
    .line 145
    sget-object v9, Landroid/hardware/SystemSensorManager;->sListeners:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@88
    move-result-object v2

    #@89
    check-cast v2, Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@8b
    .line 146
    .local v2, listener:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    invoke-virtual {v2, v4}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->hasSensor(Landroid/hardware/Sensor;)Z

    #@8e
    move-result v9

    #@8f
    if-eqz v9, :cond_94

    #@91
    .line 149
    invoke-virtual {v2, v4, v8, v7, v0}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->onSensorChangedLocked(Landroid/hardware/Sensor;[F[JI)V

    #@94
    .line 144
    :cond_94
    add-int/lit8 v1, v1, 0x1

    #@96
    goto :goto_81

    #@97
    .line 154
    .end local v1           #i:I
    .end local v2           #listener:Landroid/hardware/SystemSensorManager$ListenerDelegate;
    .end local v5           #size:I
    :cond_97
    monitor-exit v10
    :try_end_98
    .catchall {:try_start_70 .. :try_end_98} :catchall_6a

    #@98
    goto :goto_1f
.end method
