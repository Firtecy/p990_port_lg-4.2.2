.class public Landroid/hardware/GeomagneticField;
.super Ljava/lang/Object;
.source "GeomagneticField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/GeomagneticField$LegendreTable;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

#the value of this static final field might be set in the static constructor
.field private static final BASE_TIME:J = 0x0L

.field private static final DELTA_G:[[F = null

.field private static final DELTA_H:[[F = null

.field private static final EARTH_REFERENCE_RADIUS_KM:F = 6371.2f

.field private static final EARTH_SEMI_MAJOR_AXIS_KM:F = 6378.137f

.field private static final EARTH_SEMI_MINOR_AXIS_KM:F = 6356.7524f

.field private static final G_COEFF:[[F

.field private static final H_COEFF:[[F

.field private static final SCHMIDT_QUASI_NORM_FACTORS:[[F


# instance fields
.field private mGcLatitudeRad:F

.field private mGcLongitudeRad:F

.field private mGcRadiusKm:F

.field private mX:F

.field private mY:F

.field private mZ:F


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x1

    #@5
    .line 33
    const-class v0, Landroid/hardware/GeomagneticField;

    #@7
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_21b

    #@d
    move v0, v1

    #@e
    :goto_e
    sput-boolean v0, Landroid/hardware/GeomagneticField;->$assertionsDisabled:Z

    #@10
    .line 52
    const/16 v0, 0xd

    #@12
    new-array v0, v0, [[F

    #@14
    new-array v3, v1, [F

    #@16
    const/4 v4, 0x0

    #@17
    aput v4, v3, v2

    #@19
    aput-object v3, v0, v2

    #@1b
    new-array v3, v5, [F

    #@1d
    fill-array-data v3, :array_21e

    #@20
    aput-object v3, v0, v1

    #@22
    new-array v3, v6, [F

    #@24
    fill-array-data v3, :array_226

    #@27
    aput-object v3, v0, v5

    #@29
    new-array v3, v7, [F

    #@2b
    fill-array-data v3, :array_230

    #@2e
    aput-object v3, v0, v6

    #@30
    const/4 v3, 0x5

    #@31
    new-array v3, v3, [F

    #@33
    fill-array-data v3, :array_23c

    #@36
    aput-object v3, v0, v7

    #@38
    const/4 v3, 0x5

    #@39
    const/4 v4, 0x6

    #@3a
    new-array v4, v4, [F

    #@3c
    fill-array-data v4, :array_24a

    #@3f
    aput-object v4, v0, v3

    #@41
    const/4 v3, 0x6

    #@42
    const/4 v4, 0x7

    #@43
    new-array v4, v4, [F

    #@45
    fill-array-data v4, :array_25a

    #@48
    aput-object v4, v0, v3

    #@4a
    const/4 v3, 0x7

    #@4b
    const/16 v4, 0x8

    #@4d
    new-array v4, v4, [F

    #@4f
    fill-array-data v4, :array_26c

    #@52
    aput-object v4, v0, v3

    #@54
    const/16 v3, 0x8

    #@56
    const/16 v4, 0x9

    #@58
    new-array v4, v4, [F

    #@5a
    fill-array-data v4, :array_280

    #@5d
    aput-object v4, v0, v3

    #@5f
    const/16 v3, 0x9

    #@61
    const/16 v4, 0xa

    #@63
    new-array v4, v4, [F

    #@65
    fill-array-data v4, :array_296

    #@68
    aput-object v4, v0, v3

    #@6a
    const/16 v3, 0xa

    #@6c
    const/16 v4, 0xb

    #@6e
    new-array v4, v4, [F

    #@70
    fill-array-data v4, :array_2ae

    #@73
    aput-object v4, v0, v3

    #@75
    const/16 v3, 0xb

    #@77
    const/16 v4, 0xc

    #@79
    new-array v4, v4, [F

    #@7b
    fill-array-data v4, :array_2c8

    #@7e
    aput-object v4, v0, v3

    #@80
    const/16 v3, 0xc

    #@82
    const/16 v4, 0xd

    #@84
    new-array v4, v4, [F

    #@86
    fill-array-data v4, :array_2e4

    #@89
    aput-object v4, v0, v3

    #@8b
    sput-object v0, Landroid/hardware/GeomagneticField;->G_COEFF:[[F

    #@8d
    .line 67
    const/16 v0, 0xd

    #@8f
    new-array v0, v0, [[F

    #@91
    new-array v3, v1, [F

    #@93
    const/4 v4, 0x0

    #@94
    aput v4, v3, v2

    #@96
    aput-object v3, v0, v2

    #@98
    new-array v3, v5, [F

    #@9a
    fill-array-data v3, :array_302

    #@9d
    aput-object v3, v0, v1

    #@9f
    new-array v3, v6, [F

    #@a1
    fill-array-data v3, :array_30a

    #@a4
    aput-object v3, v0, v5

    #@a6
    new-array v3, v7, [F

    #@a8
    fill-array-data v3, :array_314

    #@ab
    aput-object v3, v0, v6

    #@ad
    const/4 v3, 0x5

    #@ae
    new-array v3, v3, [F

    #@b0
    fill-array-data v3, :array_320

    #@b3
    aput-object v3, v0, v7

    #@b5
    const/4 v3, 0x5

    #@b6
    const/4 v4, 0x6

    #@b7
    new-array v4, v4, [F

    #@b9
    fill-array-data v4, :array_32e

    #@bc
    aput-object v4, v0, v3

    #@be
    const/4 v3, 0x6

    #@bf
    const/4 v4, 0x7

    #@c0
    new-array v4, v4, [F

    #@c2
    fill-array-data v4, :array_33e

    #@c5
    aput-object v4, v0, v3

    #@c7
    const/4 v3, 0x7

    #@c8
    const/16 v4, 0x8

    #@ca
    new-array v4, v4, [F

    #@cc
    fill-array-data v4, :array_350

    #@cf
    aput-object v4, v0, v3

    #@d1
    const/16 v3, 0x8

    #@d3
    const/16 v4, 0x9

    #@d5
    new-array v4, v4, [F

    #@d7
    fill-array-data v4, :array_364

    #@da
    aput-object v4, v0, v3

    #@dc
    const/16 v3, 0x9

    #@de
    const/16 v4, 0xa

    #@e0
    new-array v4, v4, [F

    #@e2
    fill-array-data v4, :array_37a

    #@e5
    aput-object v4, v0, v3

    #@e7
    const/16 v3, 0xa

    #@e9
    const/16 v4, 0xb

    #@eb
    new-array v4, v4, [F

    #@ed
    fill-array-data v4, :array_392

    #@f0
    aput-object v4, v0, v3

    #@f2
    const/16 v3, 0xb

    #@f4
    const/16 v4, 0xc

    #@f6
    new-array v4, v4, [F

    #@f8
    fill-array-data v4, :array_3ac

    #@fb
    aput-object v4, v0, v3

    #@fd
    const/16 v3, 0xc

    #@ff
    const/16 v4, 0xd

    #@101
    new-array v4, v4, [F

    #@103
    fill-array-data v4, :array_3c8

    #@106
    aput-object v4, v0, v3

    #@108
    sput-object v0, Landroid/hardware/GeomagneticField;->H_COEFF:[[F

    #@10a
    .line 82
    const/16 v0, 0xd

    #@10c
    new-array v0, v0, [[F

    #@10e
    new-array v3, v1, [F

    #@110
    const/4 v4, 0x0

    #@111
    aput v4, v3, v2

    #@113
    aput-object v3, v0, v2

    #@115
    new-array v3, v5, [F

    #@117
    fill-array-data v3, :array_3e6

    #@11a
    aput-object v3, v0, v1

    #@11c
    new-array v3, v6, [F

    #@11e
    fill-array-data v3, :array_3ee

    #@121
    aput-object v3, v0, v5

    #@123
    new-array v3, v7, [F

    #@125
    fill-array-data v3, :array_3f8

    #@128
    aput-object v3, v0, v6

    #@12a
    const/4 v3, 0x5

    #@12b
    new-array v3, v3, [F

    #@12d
    fill-array-data v3, :array_404

    #@130
    aput-object v3, v0, v7

    #@132
    const/4 v3, 0x5

    #@133
    const/4 v4, 0x6

    #@134
    new-array v4, v4, [F

    #@136
    fill-array-data v4, :array_412

    #@139
    aput-object v4, v0, v3

    #@13b
    const/4 v3, 0x6

    #@13c
    const/4 v4, 0x7

    #@13d
    new-array v4, v4, [F

    #@13f
    fill-array-data v4, :array_422

    #@142
    aput-object v4, v0, v3

    #@144
    const/4 v3, 0x7

    #@145
    const/16 v4, 0x8

    #@147
    new-array v4, v4, [F

    #@149
    fill-array-data v4, :array_434

    #@14c
    aput-object v4, v0, v3

    #@14e
    const/16 v3, 0x8

    #@150
    const/16 v4, 0x9

    #@152
    new-array v4, v4, [F

    #@154
    fill-array-data v4, :array_448

    #@157
    aput-object v4, v0, v3

    #@159
    const/16 v3, 0x9

    #@15b
    const/16 v4, 0xa

    #@15d
    new-array v4, v4, [F

    #@15f
    fill-array-data v4, :array_45e

    #@162
    aput-object v4, v0, v3

    #@164
    const/16 v3, 0xa

    #@166
    const/16 v4, 0xb

    #@168
    new-array v4, v4, [F

    #@16a
    fill-array-data v4, :array_476

    #@16d
    aput-object v4, v0, v3

    #@16f
    const/16 v3, 0xb

    #@171
    const/16 v4, 0xc

    #@173
    new-array v4, v4, [F

    #@175
    fill-array-data v4, :array_490

    #@178
    aput-object v4, v0, v3

    #@17a
    const/16 v3, 0xc

    #@17c
    const/16 v4, 0xd

    #@17e
    new-array v4, v4, [F

    #@180
    fill-array-data v4, :array_4ac

    #@183
    aput-object v4, v0, v3

    #@185
    sput-object v0, Landroid/hardware/GeomagneticField;->DELTA_G:[[F

    #@187
    .line 97
    const/16 v0, 0xd

    #@189
    new-array v0, v0, [[F

    #@18b
    new-array v3, v1, [F

    #@18d
    const/4 v4, 0x0

    #@18e
    aput v4, v3, v2

    #@190
    aput-object v3, v0, v2

    #@192
    new-array v2, v5, [F

    #@194
    fill-array-data v2, :array_4ca

    #@197
    aput-object v2, v0, v1

    #@199
    new-array v2, v6, [F

    #@19b
    fill-array-data v2, :array_4d2

    #@19e
    aput-object v2, v0, v5

    #@1a0
    new-array v2, v7, [F

    #@1a2
    fill-array-data v2, :array_4dc

    #@1a5
    aput-object v2, v0, v6

    #@1a7
    const/4 v2, 0x5

    #@1a8
    new-array v2, v2, [F

    #@1aa
    fill-array-data v2, :array_4e8

    #@1ad
    aput-object v2, v0, v7

    #@1af
    const/4 v2, 0x5

    #@1b0
    const/4 v3, 0x6

    #@1b1
    new-array v3, v3, [F

    #@1b3
    fill-array-data v3, :array_4f6

    #@1b6
    aput-object v3, v0, v2

    #@1b8
    const/4 v2, 0x6

    #@1b9
    const/4 v3, 0x7

    #@1ba
    new-array v3, v3, [F

    #@1bc
    fill-array-data v3, :array_506

    #@1bf
    aput-object v3, v0, v2

    #@1c1
    const/4 v2, 0x7

    #@1c2
    const/16 v3, 0x8

    #@1c4
    new-array v3, v3, [F

    #@1c6
    fill-array-data v3, :array_518

    #@1c9
    aput-object v3, v0, v2

    #@1cb
    const/16 v2, 0x8

    #@1cd
    const/16 v3, 0x9

    #@1cf
    new-array v3, v3, [F

    #@1d1
    fill-array-data v3, :array_52c

    #@1d4
    aput-object v3, v0, v2

    #@1d6
    const/16 v2, 0x9

    #@1d8
    const/16 v3, 0xa

    #@1da
    new-array v3, v3, [F

    #@1dc
    fill-array-data v3, :array_542

    #@1df
    aput-object v3, v0, v2

    #@1e1
    const/16 v2, 0xa

    #@1e3
    const/16 v3, 0xb

    #@1e5
    new-array v3, v3, [F

    #@1e7
    fill-array-data v3, :array_55a

    #@1ea
    aput-object v3, v0, v2

    #@1ec
    const/16 v2, 0xb

    #@1ee
    const/16 v3, 0xc

    #@1f0
    new-array v3, v3, [F

    #@1f2
    fill-array-data v3, :array_574

    #@1f5
    aput-object v3, v0, v2

    #@1f7
    const/16 v2, 0xc

    #@1f9
    const/16 v3, 0xd

    #@1fb
    new-array v3, v3, [F

    #@1fd
    fill-array-data v3, :array_590

    #@200
    aput-object v3, v0, v2

    #@202
    sput-object v0, Landroid/hardware/GeomagneticField;->DELTA_H:[[F

    #@204
    .line 112
    new-instance v0, Ljava/util/GregorianCalendar;

    #@206
    const/16 v2, 0x7da

    #@208
    invoke-direct {v0, v2, v1, v1}, Ljava/util/GregorianCalendar;-><init>(III)V

    #@20b
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    #@20e
    move-result-wide v0

    #@20f
    sput-wide v0, Landroid/hardware/GeomagneticField;->BASE_TIME:J

    #@211
    .line 118
    sget-object v0, Landroid/hardware/GeomagneticField;->G_COEFF:[[F

    #@213
    array-length v0, v0

    #@214
    invoke-static {v0}, Landroid/hardware/GeomagneticField;->computeSchmidtQuasiNormFactors(I)[[F

    #@217
    move-result-object v0

    #@218
    sput-object v0, Landroid/hardware/GeomagneticField;->SCHMIDT_QUASI_NORM_FACTORS:[[F

    #@21a
    return-void

    #@21b
    :cond_21b
    move v0, v2

    #@21c
    .line 33
    goto/16 :goto_e

    #@21e
    .line 52
    :array_21e
    .array-data 0x4
        0x33t 0x71t 0xe6t 0xc6t
        0x9at 0x49t 0xc6t 0xc4t
    .end array-data

    #@226
    :array_226
    .array-data 0x4
        0x9at 0xc9t 0x15t 0xc5t
        0x9at 0x21t 0x3dt 0x45t
        0x33t 0x93t 0xd0t 0x44t
    .end array-data

    #@230
    :array_230
    .array-data 0x4
        0x33t 0x83t 0xa7t 0x44t
        0x33t 0x63t 0x11t 0xc5t
        0xcdt 0xfct 0x99t 0x44t
        0x0t 0x80t 0x1et 0x44t
    .end array-data

    #@23c
    :array_23c
    .array-data 0x4
        0x66t 0x26t 0x64t 0x44t
        0x9at 0x39t 0x4at 0x44t
        0x33t 0xb3t 0x26t 0x43t
        0xcdt 0x8ct 0xb2t 0xc3t
        0xcdt 0xcct 0xb2t 0x42t
    .end array-data

    #@24a
    :array_24a
    .array-data 0x4
        0x66t 0xe6t 0x66t 0xc3t
        0x9at 0x99t 0xb2t 0x43t
        0xcdt 0x4ct 0x48t 0x43t
        0x9at 0x19t 0xdt 0xc3t
        0x0t 0x0t 0x23t 0xc3t
        0x9at 0x99t 0xf9t 0xc0t
    .end array-data

    #@25a
    :array_25a
    .array-data 0x4
        0x9at 0x99t 0x91t 0x42t
        0x33t 0x33t 0x89t 0x42t
        0x0t 0x0t 0x98t 0x42t
        0x66t 0x66t 0xdt 0xc3t
        0x66t 0x66t 0xb6t 0xc1t
        0x33t 0x33t 0x53t 0x41t
        0xcdt 0xcct 0x9bt 0xc2t
    .end array-data

    #@26c
    :array_26c
    .array-data 0x4
        0x0t 0x0t 0xa1t 0x42t
        0x33t 0x33t 0x96t 0xc2t
        0x66t 0x66t 0x96t 0xc0t
        0x33t 0x33t 0x35t 0x42t
        0x66t 0x66t 0x5et 0x41t
        0x66t 0x66t 0x26t 0x41t
        0x9at 0x99t 0xd9t 0x3ft
        0xcdt 0xcct 0x9ct 0x40t
    .end array-data

    #@280
    :array_280
    .array-data 0x4
        0x33t 0x33t 0xc3t 0x41t
        0x9at 0x99t 0x1t 0x41t
        0x0t 0x0t 0x68t 0xc1t
        0x33t 0x33t 0xb3t 0xc0t
        0x66t 0x66t 0x9at 0xc1t
        0x0t 0x0t 0x38t 0x41t
        0x66t 0x66t 0x2et 0x41t
        0x9at 0x99t 0x61t 0xc1t
        0xcdt 0xcct 0x6ct 0xc0t
    .end array-data

    #@296
    :array_296
    .array-data 0x4
        0xcdt 0xcct 0xact 0x40t
        0x66t 0x66t 0x16t 0x41t
        0x9at 0x99t 0x59t 0x40t
        0x66t 0x66t 0xa6t 0xc0t
        0x66t 0x66t 0x46t 0x40t
        0x66t 0x66t 0x46t 0xc1t
        0x33t 0x33t 0x33t 0xbft
        0x66t 0x66t 0x6t 0x41t
        0x0t 0x0t 0x8t 0xc1t
        0x9at 0x99t 0x21t 0xc1t
    .end array-data

    #@2ae
    :array_2ae
    .array-data 0x4
        0x0t 0x0t 0x0t 0xc0t
        0x9at 0x99t 0xc9t 0xc0t
        0x66t 0x66t 0x66t 0x3ft
        0xcdt 0xcct 0x8ct 0xbft
        0xcdt 0xcct 0x4ct 0xbet
        0x0t 0x0t 0x20t 0x40t
        0x9at 0x99t 0x99t 0xbet
        0xcdt 0xcct 0xct 0x40t
        0x66t 0x66t 0x46t 0x40t
        0x0t 0x0t 0x80t 0xbft
        0x33t 0x33t 0x33t 0xc0t
    .end array-data

    #@2c8
    :array_2c8
    .array-data 0x4
        0x0t 0x0t 0x40t 0x40t
        0x0t 0x0t 0xc0t 0xbft
        0x66t 0x66t 0x6t 0xc0t
        0x9at 0x99t 0xd9t 0x3ft
        0x0t 0x0t 0x0t 0xbft
        0x0t 0x0t 0x0t 0x3ft
        0xcdt 0xcct 0x4ct 0xbft
        0xcdt 0xcct 0xcct 0x3et
        0x66t 0x66t 0xe6t 0x3ft
        0xcdt 0xcct 0xcct 0x3dt
        0x33t 0x33t 0x33t 0x3ft
        0x33t 0x33t 0x73t 0x40t
    .end array-data

    #@2e4
    :array_2e4
    .array-data 0x4
        0xcdt 0xcct 0xct 0xc0t
        0xcdt 0xcct 0x4ct 0xbet
        0x9at 0x99t 0x99t 0x3et
        0x0t 0x0t 0x80t 0x3ft
        0x9at 0x99t 0x19t 0xbft
        0x66t 0x66t 0x66t 0x3ft
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x3ft
        0xcdt 0xcct 0xcct 0xbet
        0xcdt 0xcct 0xcct 0xbet
        0xcdt 0xcct 0x4ct 0x3et
        0xcdt 0xcct 0x4ct 0xbft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@302
    .line 67
    :array_302
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x83t 0x9at 0x45t
    .end array-data

    #@30a
    :array_30a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x3bt 0x29t 0xc5t
        0x66t 0x6t 0x10t 0xc4t
    .end array-data

    #@314
    :array_314
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x33t 0x20t 0xc3t
        0x66t 0xe6t 0x7bt 0x43t
        0x66t 0x26t 0x6t 0xc4t
    .end array-data

    #@320
    :array_320
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x33t 0x8ft 0x43t
        0x33t 0x33t 0x53t 0xc3t
        0xcdt 0x4ct 0x24t 0x43t
        0xcdt 0x8ct 0x9at 0xc3t
    .end array-data

    #@32e
    :array_32e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x66t 0x66t 0x32t 0x42t
        0x66t 0xe6t 0x3ct 0x43t
        0x66t 0x66t 0xect 0xc2t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xc9t 0x42t
    .end array-data

    #@33e
    :array_33e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x66t 0x66t 0xa6t 0xc1t
        0x66t 0x66t 0x30t 0x42t
        0x0t 0x0t 0x76t 0x42t
        0x9at 0x99t 0x84t 0xc2t
        0x66t 0x66t 0x46t 0x40t
        0x0t 0x0t 0x5ct 0x42t
    .end array-data

    #@350
    :array_350
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x9at 0x99t 0x67t 0xc2t
        0xcdt 0xcct 0xa8t 0xc1t
        0x0t 0x0t 0xd0t 0x40t
        0x33t 0x33t 0xc7t 0x41t
        0x0t 0x0t 0xe0t 0x40t
        0x9at 0x99t 0xddt 0xc1t
        0x33t 0x33t 0x53t 0xc0t
    .end array-data

    #@364
    :array_364
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x30t 0x41t
        0x0t 0x0t 0xa0t 0xc1t
        0x66t 0x66t 0x3et 0x41t
        0x33t 0x33t 0x8bt 0xc1t
        0x9at 0x99t 0x85t 0x41t
        0x0t 0x0t 0xe0t 0x40t
        0xcdt 0xcct 0x2ct 0xc1t
        0x9at 0x99t 0xd9t 0x3ft
    .end array-data

    #@37a
    :array_37a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0xa4t 0xc1t
        0x0t 0x0t 0x38t 0x41t
        0xcdt 0xcct 0x4ct 0x41t
        0x66t 0x66t 0xe6t 0xc0t
        0xcdt 0xcct 0xect 0xc0t
        0x0t 0x0t 0x0t 0x41t
        0x66t 0x66t 0x6t 0x40t
        0x33t 0x33t 0xc3t 0xc0t
        0x0t 0x0t 0xe0t 0x40t
    .end array-data

    #@392
    :array_392
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x33t 0x33t 0x40t
        0xcdt 0xcct 0xcct 0xbdt
        0x66t 0x66t 0x96t 0x40t
        0xcdt 0xcct 0x8ct 0x40t
        0x66t 0x66t 0xe6t 0xc0t
        0x0t 0x0t 0x80t 0xbft
        0x9at 0x99t 0x79t 0xc0t
        0x0t 0x0t 0x0t 0xc0t
        0x0t 0x0t 0x0t 0xc0t
        0xcdt 0xcct 0x4t 0xc1t
    .end array-data

    #@3ac
    :array_3ac
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0x4ct 0x3et
        0x9at 0x99t 0xd9t 0x3ft
        0x9at 0x99t 0x19t 0xbft
        0x66t 0x66t 0xe6t 0xbft
        0x66t 0x66t 0x66t 0x3ft
        0xcdt 0xcct 0xcct 0xbet
        0x0t 0x0t 0x20t 0xc0t
        0x66t 0x66t 0xa6t 0xbft
        0x66t 0x66t 0x6t 0xc0t
        0x33t 0x33t 0xf3t 0xbft
        0x66t 0x66t 0xe6t 0xbft
    .end array-data

    #@3c8
    :array_3c8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x66t 0x66t 0x66t 0xbft
        0x9at 0x99t 0x99t 0x3et
        0x66t 0x66t 0x6t 0x40t
        0x0t 0x0t 0x20t 0xc0t
        0x0t 0x0t 0x0t 0x3ft
        0x9at 0x99t 0x19t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0x9at 0x99t 0x99t 0x3et
        0x66t 0x66t 0x66t 0xbft
        0xcdt 0xcct 0x4ct 0xbet
        0x66t 0x66t 0x66t 0x3ft
    .end array-data

    #@3e6
    .line 82
    :array_3e6
    .array-data 0x4
        0x9at 0x99t 0x39t 0x41t
        0x0t 0x0t 0x84t 0x41t
    .end array-data

    #@3ee
    :array_3ee
    .array-data 0x4
        0x9at 0x99t 0x41t 0xc1t
        0xcdt 0xcct 0x8ct 0xc0t
        0x33t 0x33t 0xf3t 0x3ft
    .end array-data

    #@3f8
    :array_3f8
    .array-data 0x4
        0xcdt 0xcct 0xcct 0x3et
        0x33t 0x33t 0x83t 0xc0t
        0x9at 0x99t 0x39t 0xc0t
        0x66t 0x66t 0xf6t 0xc0t
    .end array-data

    #@404
    :array_404
    .array-data 0x4
        0x66t 0x66t 0xe6t 0xbft
        0x33t 0x33t 0x13t 0x40t
        0x33t 0x33t 0xbt 0xc1t
        0x33t 0x33t 0x93t 0x40t
        0x66t 0x66t 0x6t 0xc0t
    .end array-data

    #@412
    :array_412
    .array-data 0x4
        0x0t 0x0t 0x80t 0xbft
        0x9at 0x99t 0x19t 0x3ft
        0x66t 0x66t 0xe6t 0xbft
        0x0t 0x0t 0x80t 0xbft
        0x66t 0x66t 0x66t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    #@422
    :array_422
    .array-data 0x4
        0xcdt 0xcct 0x4ct 0xbet
        0xcdt 0xcct 0x4ct 0xbet
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x40t
        0x9at 0x99t 0xd9t 0xbft
        0x9at 0x99t 0x99t 0xbet
        0x9at 0x99t 0xd9t 0x3ft
    .end array-data

    #@434
    :array_434
    .array-data 0x4
        0xcdt 0xcct 0xcct 0x3dt
        0xcdt 0xcct 0xcct 0xbdt
        0x9at 0x99t 0x19t 0xbft
        0x66t 0x66t 0xa6t 0x3ft
        0xcdt 0xcct 0xcct 0x3et
        0x9at 0x99t 0x99t 0x3et
        0x33t 0x33t 0x33t 0xbft
        0x9at 0x99t 0x19t 0x3ft
    .end array-data

    #@448
    :array_448
    .array-data 0x4
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0x3dt
        0x9at 0x99t 0x19t 0xbft
        0xcdt 0xcct 0x4ct 0x3et
        0xcdt 0xcct 0x4ct 0xbet
        0x9at 0x99t 0x99t 0x3et
        0x9at 0x99t 0x99t 0x3et
        0x9at 0x99t 0x19t 0xbft
        0xcdt 0xcct 0x4ct 0x3et
    .end array-data

    #@45e
    :array_45e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x0t
        0x9at 0x99t 0x99t 0x3et
        0xcdt 0xcct 0xcct 0xbet
        0x9at 0x99t 0x99t 0xbet
        0xcdt 0xcct 0xcct 0x3dt
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0xbet
        0xcdt 0xcct 0x4ct 0xbet
    .end array-data

    #@476
    :array_476
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0x4ct 0x3et
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0x4ct 0xbet
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0x4ct 0xbet
        0xcdt 0xcct 0x4ct 0xbet
    .end array-data

    #@490
    :array_490
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@4ac
    :array_4ac
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0xcdt 0xcct 0xcct 0x3dt
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0x3dt
    .end array-data

    #@4ca
    .line 97
    :array_4ca
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x33t 0xcft 0xc1t
    .end array-data

    #@4d2
    :array_4d2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0xb4t 0xc1t
        0xcdt 0xcct 0x3ct 0xc1t
    .end array-data

    #@4dc
    :array_4dc
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x9at 0x99t 0xe9t 0x40t
        0x9at 0x99t 0x79t 0xc0t
        0x66t 0x66t 0x26t 0xc0t
    .end array-data

    #@4e8
    :array_4e8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0x8ct 0x3ft
        0xcdt 0xcct 0x2ct 0x40t
        0x9at 0x99t 0x79t 0x40t
        0xcdt 0xcct 0x4ct 0xbft
    .end array-data

    #@4f6
    :array_4f6
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3et
        0x66t 0x66t 0xe6t 0x3ft
        0x9at 0x99t 0x99t 0x3ft
        0x0t 0x0t 0x80t 0x40t
        0x9at 0x99t 0x19t 0xbft
    .end array-data

    #@506
    :array_506
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0x4ct 0xbet
        0x66t 0x66t 0x6t 0xc0t
        0xcdt 0xcct 0xcct 0xbet
        0x9at 0x99t 0x19t 0xbft
        0x0t 0x0t 0x0t 0x3ft
        0x66t 0x66t 0x66t 0x3ft
    .end array-data

    #@518
    :array_518
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x33t 0x33t 0x33t 0x3ft
        0x9at 0x99t 0x99t 0x3et
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0x4ct 0xbft
        0x9at 0x99t 0x99t 0xbet
        0x9at 0x99t 0x99t 0x3et
    .end array-data

    #@52c
    :array_52c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0x4ct 0x3et
        0xcdt 0xcct 0xcct 0x3et
        0xcdt 0xcct 0xcct 0x3et
        0xcdt 0xcct 0xcct 0x3dt
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0x3et
        0x9at 0x99t 0x99t 0x3et
    .end array-data

    #@542
    :array_542
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0x4ct 0xbet
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0x3dt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0x4ct 0xbet
        0x9at 0x99t 0x99t 0x3et
        0xcdt 0xcct 0x4ct 0x3et
    .end array-data

    #@55a
    :array_55a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0x4ct 0xbet
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
    .end array-data

    #@574
    :array_574
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
        0xcdt 0xcct 0xcct 0xbdt
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0xbdt
    .end array-data

    #@590
    :array_590
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xcdt 0xcct 0xcct 0x3dt
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(FFFJ)V
    .registers 32
    .parameter "gdLatitudeDeg"
    .parameter "gdLongitudeDeg"
    .parameter "altitudeMeters"
    .parameter "timeMillis"

    #@0
    .prologue
    .line 138
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 139
    sget-object v20, Landroid/hardware/GeomagneticField;->G_COEFF:[[F

    #@5
    move-object/from16 v0, v20

    #@7
    array-length v2, v0

    #@8
    .line 143
    .local v2, MAX_N:I
    const v20, 0x42b3ffff

    #@b
    const v21, -0x3d4c0001

    #@e
    move/from16 v0, v21

    #@10
    move/from16 v1, p1

    #@12
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@15
    move-result v21

    #@16
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(FF)F

    #@19
    move-result p1

    #@1a
    .line 145
    invoke-direct/range {p0 .. p3}, Landroid/hardware/GeomagneticField;->computeGeocentricCoordinates(FFF)V

    #@1d
    .line 149
    sget-boolean v20, Landroid/hardware/GeomagneticField;->$assertionsDisabled:Z

    #@1f
    if-nez v20, :cond_3b

    #@21
    sget-object v20, Landroid/hardware/GeomagneticField;->G_COEFF:[[F

    #@23
    move-object/from16 v0, v20

    #@25
    array-length v0, v0

    #@26
    move/from16 v20, v0

    #@28
    sget-object v21, Landroid/hardware/GeomagneticField;->H_COEFF:[[F

    #@2a
    move-object/from16 v0, v21

    #@2c
    array-length v0, v0

    #@2d
    move/from16 v21, v0

    #@2f
    move/from16 v0, v20

    #@31
    move/from16 v1, v21

    #@33
    if-eq v0, v1, :cond_3b

    #@35
    new-instance v20, Ljava/lang/AssertionError;

    #@37
    invoke-direct/range {v20 .. v20}, Ljava/lang/AssertionError;-><init>()V

    #@3a
    throw v20

    #@3b
    .line 155
    :cond_3b
    new-instance v13, Landroid/hardware/GeomagneticField$LegendreTable;

    #@3d
    add-int/lit8 v20, v2, -0x1

    #@3f
    const-wide v21, 0x3ff921fb54442d18L

    #@44
    move-object/from16 v0, p0

    #@46
    iget v0, v0, Landroid/hardware/GeomagneticField;->mGcLatitudeRad:F

    #@48
    move/from16 v23, v0

    #@4a
    move/from16 v0, v23

    #@4c
    float-to-double v0, v0

    #@4d
    move-wide/from16 v23, v0

    #@4f
    sub-double v21, v21, v23

    #@51
    move-wide/from16 v0, v21

    #@53
    double-to-float v0, v0

    #@54
    move/from16 v21, v0

    #@56
    move/from16 v0, v20

    #@58
    move/from16 v1, v21

    #@5a
    invoke-direct {v13, v0, v1}, Landroid/hardware/GeomagneticField$LegendreTable;-><init>(IF)V

    #@5d
    .line 161
    .local v13, legendre:Landroid/hardware/GeomagneticField$LegendreTable;
    add-int/lit8 v20, v2, 0x2

    #@5f
    move/from16 v0, v20

    #@61
    new-array v0, v0, [F

    #@63
    move-object/from16 v16, v0

    #@65
    .line 162
    .local v16, relativeRadiusPower:[F
    const/16 v20, 0x0

    #@67
    const/high16 v21, 0x3f80

    #@69
    aput v21, v16, v20

    #@6b
    .line 163
    const/16 v20, 0x1

    #@6d
    const v21, 0x45c7199a

    #@70
    move-object/from16 v0, p0

    #@72
    iget v0, v0, Landroid/hardware/GeomagneticField;->mGcRadiusKm:F

    #@74
    move/from16 v22, v0

    #@76
    div-float v21, v21, v22

    #@78
    aput v21, v16, v20

    #@7a
    .line 164
    const/4 v9, 0x2

    #@7b
    .local v9, i:I
    :goto_7b
    move-object/from16 v0, v16

    #@7d
    array-length v0, v0

    #@7e
    move/from16 v20, v0

    #@80
    move/from16 v0, v20

    #@82
    if-ge v9, v0, :cond_93

    #@84
    .line 165
    add-int/lit8 v20, v9, -0x1

    #@86
    aget v20, v16, v20

    #@88
    const/16 v21, 0x1

    #@8a
    aget v21, v16, v21

    #@8c
    mul-float v20, v20, v21

    #@8e
    aput v20, v16, v9

    #@90
    .line 164
    add-int/lit8 v9, v9, 0x1

    #@92
    goto :goto_7b

    #@93
    .line 171
    :cond_93
    new-array v0, v2, [F

    #@95
    move-object/from16 v17, v0

    #@97
    .line 172
    .local v17, sinMLon:[F
    new-array v3, v2, [F

    #@99
    .line 173
    .local v3, cosMLon:[F
    const/16 v20, 0x0

    #@9b
    const/16 v21, 0x0

    #@9d
    aput v21, v17, v20

    #@9f
    .line 174
    const/16 v20, 0x0

    #@a1
    const/high16 v21, 0x3f80

    #@a3
    aput v21, v3, v20

    #@a5
    .line 175
    const/16 v20, 0x1

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget v0, v0, Landroid/hardware/GeomagneticField;->mGcLongitudeRad:F

    #@ab
    move/from16 v21, v0

    #@ad
    move/from16 v0, v21

    #@af
    float-to-double v0, v0

    #@b0
    move-wide/from16 v21, v0

    #@b2
    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->sin(D)D

    #@b5
    move-result-wide v21

    #@b6
    move-wide/from16 v0, v21

    #@b8
    double-to-float v0, v0

    #@b9
    move/from16 v21, v0

    #@bb
    aput v21, v17, v20

    #@bd
    .line 176
    const/16 v20, 0x1

    #@bf
    move-object/from16 v0, p0

    #@c1
    iget v0, v0, Landroid/hardware/GeomagneticField;->mGcLongitudeRad:F

    #@c3
    move/from16 v21, v0

    #@c5
    move/from16 v0, v21

    #@c7
    float-to-double v0, v0

    #@c8
    move-wide/from16 v21, v0

    #@ca
    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->cos(D)D

    #@cd
    move-result-wide v21

    #@ce
    move-wide/from16 v0, v21

    #@d0
    double-to-float v0, v0

    #@d1
    move/from16 v21, v0

    #@d3
    aput v21, v3, v20

    #@d5
    .line 178
    const/4 v14, 0x2

    #@d6
    .local v14, m:I
    :goto_d6
    if-ge v14, v2, :cond_105

    #@d8
    .line 181
    shr-int/lit8 v18, v14, 0x1

    #@da
    .line 182
    .local v18, x:I
    sub-int v20, v14, v18

    #@dc
    aget v20, v17, v20

    #@de
    aget v21, v3, v18

    #@e0
    mul-float v20, v20, v21

    #@e2
    sub-int v21, v14, v18

    #@e4
    aget v21, v3, v21

    #@e6
    aget v22, v17, v18

    #@e8
    mul-float v21, v21, v22

    #@ea
    add-float v20, v20, v21

    #@ec
    aput v20, v17, v14

    #@ee
    .line 183
    sub-int v20, v14, v18

    #@f0
    aget v20, v3, v20

    #@f2
    aget v21, v3, v18

    #@f4
    mul-float v20, v20, v21

    #@f6
    sub-int v21, v14, v18

    #@f8
    aget v21, v17, v21

    #@fa
    aget v22, v17, v18

    #@fc
    mul-float v21, v21, v22

    #@fe
    sub-float v20, v20, v21

    #@100
    aput v20, v3, v14

    #@102
    .line 178
    add-int/lit8 v14, v14, 0x1

    #@104
    goto :goto_d6

    #@105
    .line 186
    .end local v18           #x:I
    :cond_105
    const/high16 v20, 0x3f80

    #@107
    move-object/from16 v0, p0

    #@109
    iget v0, v0, Landroid/hardware/GeomagneticField;->mGcLatitudeRad:F

    #@10b
    move/from16 v21, v0

    #@10d
    move/from16 v0, v21

    #@10f
    float-to-double v0, v0

    #@110
    move-wide/from16 v21, v0

    #@112
    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->cos(D)D

    #@115
    move-result-wide v21

    #@116
    move-wide/from16 v0, v21

    #@118
    double-to-float v0, v0

    #@119
    move/from16 v21, v0

    #@11b
    div-float v10, v20, v21

    #@11d
    .line 187
    .local v10, inverseCosLatitude:F
    sget-wide v20, Landroid/hardware/GeomagneticField;->BASE_TIME:J

    #@11f
    sub-long v20, p4, v20

    #@121
    move-wide/from16 v0, v20

    #@123
    long-to-float v0, v0

    #@124
    move/from16 v20, v0

    #@126
    const v21, 0x50eaf626

    #@129
    div-float v19, v20, v21

    #@12b
    .line 194
    .local v19, yearsSinceBase:F
    const/4 v5, 0x0

    #@12c
    .line 195
    .local v5, gcX:F
    const/4 v6, 0x0

    #@12d
    .line 196
    .local v6, gcY:F
    const/4 v7, 0x0

    #@12e
    .line 198
    .local v7, gcZ:F
    const/4 v15, 0x1

    #@12f
    .local v15, n:I
    :goto_12f
    if-ge v15, v2, :cond_1d8

    #@131
    .line 199
    const/4 v14, 0x0

    #@132
    :goto_132
    if-gt v14, v15, :cond_1d4

    #@134
    .line 201
    sget-object v20, Landroid/hardware/GeomagneticField;->G_COEFF:[[F

    #@136
    aget-object v20, v20, v15

    #@138
    aget v20, v20, v14

    #@13a
    sget-object v21, Landroid/hardware/GeomagneticField;->DELTA_G:[[F

    #@13c
    aget-object v21, v21, v15

    #@13e
    aget v21, v21, v14

    #@140
    mul-float v21, v21, v19

    #@142
    add-float v4, v20, v21

    #@144
    .line 202
    .local v4, g:F
    sget-object v20, Landroid/hardware/GeomagneticField;->H_COEFF:[[F

    #@146
    aget-object v20, v20, v15

    #@148
    aget v20, v20, v14

    #@14a
    sget-object v21, Landroid/hardware/GeomagneticField;->DELTA_H:[[F

    #@14c
    aget-object v21, v21, v15

    #@14e
    aget v21, v21, v14

    #@150
    mul-float v21, v21, v19

    #@152
    add-float v8, v20, v21

    #@154
    .line 209
    .local v8, h:F
    add-int/lit8 v20, v15, 0x2

    #@156
    aget v20, v16, v20

    #@158
    aget v21, v3, v14

    #@15a
    mul-float v21, v21, v4

    #@15c
    aget v22, v17, v14

    #@15e
    mul-float v22, v22, v8

    #@160
    add-float v21, v21, v22

    #@162
    mul-float v20, v20, v21

    #@164
    iget-object v0, v13, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@166
    move-object/from16 v21, v0

    #@168
    aget-object v21, v21, v15

    #@16a
    aget v21, v21, v14

    #@16c
    mul-float v20, v20, v21

    #@16e
    sget-object v21, Landroid/hardware/GeomagneticField;->SCHMIDT_QUASI_NORM_FACTORS:[[F

    #@170
    aget-object v21, v21, v15

    #@172
    aget v21, v21, v14

    #@174
    mul-float v20, v20, v21

    #@176
    add-float v5, v5, v20

    #@178
    .line 216
    add-int/lit8 v20, v15, 0x2

    #@17a
    aget v20, v16, v20

    #@17c
    int-to-float v0, v14

    #@17d
    move/from16 v21, v0

    #@17f
    mul-float v20, v20, v21

    #@181
    aget v21, v17, v14

    #@183
    mul-float v21, v21, v4

    #@185
    aget v22, v3, v14

    #@187
    mul-float v22, v22, v8

    #@189
    sub-float v21, v21, v22

    #@18b
    mul-float v20, v20, v21

    #@18d
    iget-object v0, v13, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@18f
    move-object/from16 v21, v0

    #@191
    aget-object v21, v21, v15

    #@193
    aget v21, v21, v14

    #@195
    mul-float v20, v20, v21

    #@197
    sget-object v21, Landroid/hardware/GeomagneticField;->SCHMIDT_QUASI_NORM_FACTORS:[[F

    #@199
    aget-object v21, v21, v15

    #@19b
    aget v21, v21, v14

    #@19d
    mul-float v20, v20, v21

    #@19f
    mul-float v20, v20, v10

    #@1a1
    add-float v6, v6, v20

    #@1a3
    .line 223
    add-int/lit8 v20, v15, 0x1

    #@1a5
    move/from16 v0, v20

    #@1a7
    int-to-float v0, v0

    #@1a8
    move/from16 v20, v0

    #@1aa
    add-int/lit8 v21, v15, 0x2

    #@1ac
    aget v21, v16, v21

    #@1ae
    mul-float v20, v20, v21

    #@1b0
    aget v21, v3, v14

    #@1b2
    mul-float v21, v21, v4

    #@1b4
    aget v22, v17, v14

    #@1b6
    mul-float v22, v22, v8

    #@1b8
    add-float v21, v21, v22

    #@1ba
    mul-float v20, v20, v21

    #@1bc
    iget-object v0, v13, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@1be
    move-object/from16 v21, v0

    #@1c0
    aget-object v21, v21, v15

    #@1c2
    aget v21, v21, v14

    #@1c4
    mul-float v20, v20, v21

    #@1c6
    sget-object v21, Landroid/hardware/GeomagneticField;->SCHMIDT_QUASI_NORM_FACTORS:[[F

    #@1c8
    aget-object v21, v21, v15

    #@1ca
    aget v21, v21, v14

    #@1cc
    mul-float v20, v20, v21

    #@1ce
    sub-float v7, v7, v20

    #@1d0
    .line 199
    add-int/lit8 v14, v14, 0x1

    #@1d2
    goto/16 :goto_132

    #@1d4
    .line 198
    .end local v4           #g:F
    .end local v8           #h:F
    :cond_1d4
    add-int/lit8 v15, v15, 0x1

    #@1d6
    goto/16 :goto_12f

    #@1d8
    .line 233
    :cond_1d8
    move/from16 v0, p1

    #@1da
    float-to-double v0, v0

    #@1db
    move-wide/from16 v20, v0

    #@1dd
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->toRadians(D)D

    #@1e0
    move-result-wide v20

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    iget v0, v0, Landroid/hardware/GeomagneticField;->mGcLatitudeRad:F

    #@1e5
    move/from16 v22, v0

    #@1e7
    move/from16 v0, v22

    #@1e9
    float-to-double v0, v0

    #@1ea
    move-wide/from16 v22, v0

    #@1ec
    sub-double v11, v20, v22

    #@1ee
    .line 234
    .local v11, latDiffRad:D
    float-to-double v0, v5

    #@1ef
    move-wide/from16 v20, v0

    #@1f1
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    #@1f4
    move-result-wide v22

    #@1f5
    mul-double v20, v20, v22

    #@1f7
    float-to-double v0, v7

    #@1f8
    move-wide/from16 v22, v0

    #@1fa
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    #@1fd
    move-result-wide v24

    #@1fe
    mul-double v22, v22, v24

    #@200
    add-double v20, v20, v22

    #@202
    move-wide/from16 v0, v20

    #@204
    double-to-float v0, v0

    #@205
    move/from16 v20, v0

    #@207
    move/from16 v0, v20

    #@209
    move-object/from16 v1, p0

    #@20b
    iput v0, v1, Landroid/hardware/GeomagneticField;->mX:F

    #@20d
    .line 236
    move-object/from16 v0, p0

    #@20f
    iput v6, v0, Landroid/hardware/GeomagneticField;->mY:F

    #@211
    .line 237
    neg-float v0, v5

    #@212
    move/from16 v20, v0

    #@214
    move/from16 v0, v20

    #@216
    float-to-double v0, v0

    #@217
    move-wide/from16 v20, v0

    #@219
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    #@21c
    move-result-wide v22

    #@21d
    mul-double v20, v20, v22

    #@21f
    float-to-double v0, v7

    #@220
    move-wide/from16 v22, v0

    #@222
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    #@225
    move-result-wide v24

    #@226
    mul-double v22, v22, v24

    #@228
    add-double v20, v20, v22

    #@22a
    move-wide/from16 v0, v20

    #@22c
    double-to-float v0, v0

    #@22d
    move/from16 v20, v0

    #@22f
    move/from16 v0, v20

    #@231
    move-object/from16 v1, p0

    #@233
    iput v0, v1, Landroid/hardware/GeomagneticField;->mZ:F

    #@235
    .line 239
    return-void
.end method

.method private computeGeocentricCoordinates(FFF)V
    .registers 19
    .parameter "gdLatitudeDeg"
    .parameter "gdLongitudeDeg"
    .parameter "altitudeMeters"

    #@0
    .prologue
    .line 307
    const/high16 v11, 0x447a

    #@2
    div-float v2, p3, v11

    #@4
    .line 308
    .local v2, altitudeKm:F
    const v1, 0x4c1b2f2f

    #@7
    .line 309
    .local v1, a2:F
    const v3, 0x4c1a253b

    #@a
    .line 310
    .local v3, b2:F
    move/from16 v0, p1

    #@c
    float-to-double v11, v0

    #@d
    invoke-static {v11, v12}, Ljava/lang/Math;->toRadians(D)D

    #@10
    move-result-wide v5

    #@11
    .line 311
    .local v5, gdLatRad:D
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    #@14
    move-result-wide v11

    #@15
    double-to-float v4, v11

    #@16
    .line 312
    .local v4, clat:F
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    #@19
    move-result-wide v11

    #@1a
    double-to-float v9, v11

    #@1b
    .line 313
    .local v9, slat:F
    div-float v10, v9, v4

    #@1d
    .line 314
    .local v10, tlat:F
    mul-float v11, v1, v4

    #@1f
    mul-float/2addr v11, v4

    #@20
    mul-float v12, v3, v9

    #@22
    mul-float/2addr v12, v9

    #@23
    add-float/2addr v11, v12

    #@24
    float-to-double v11, v11

    #@25
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@28
    move-result-wide v11

    #@29
    double-to-float v7, v11

    #@2a
    .line 317
    .local v7, latRad:F
    mul-float v11, v7, v2

    #@2c
    add-float/2addr v11, v3

    #@2d
    mul-float/2addr v11, v10

    #@2e
    mul-float v12, v7, v2

    #@30
    add-float/2addr v12, v1

    #@31
    div-float/2addr v11, v12

    #@32
    float-to-double v11, v11

    #@33
    invoke-static {v11, v12}, Ljava/lang/Math;->atan(D)D

    #@36
    move-result-wide v11

    #@37
    double-to-float v11, v11

    #@38
    iput v11, p0, Landroid/hardware/GeomagneticField;->mGcLatitudeRad:F

    #@3a
    .line 320
    move/from16 v0, p2

    #@3c
    float-to-double v11, v0

    #@3d
    invoke-static {v11, v12}, Ljava/lang/Math;->toRadians(D)D

    #@40
    move-result-wide v11

    #@41
    double-to-float v11, v11

    #@42
    iput v11, p0, Landroid/hardware/GeomagneticField;->mGcLongitudeRad:F

    #@44
    .line 322
    mul-float v11, v2, v2

    #@46
    const/high16 v12, 0x4000

    #@48
    mul-float/2addr v12, v2

    #@49
    mul-float v13, v1, v4

    #@4b
    mul-float/2addr v13, v4

    #@4c
    mul-float v14, v3, v9

    #@4e
    mul-float/2addr v14, v9

    #@4f
    add-float/2addr v13, v14

    #@50
    float-to-double v13, v13

    #@51
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    #@54
    move-result-wide v13

    #@55
    double-to-float v13, v13

    #@56
    mul-float/2addr v12, v13

    #@57
    add-float/2addr v11, v12

    #@58
    mul-float v12, v1, v1

    #@5a
    mul-float/2addr v12, v4

    #@5b
    mul-float/2addr v12, v4

    #@5c
    mul-float v13, v3, v3

    #@5e
    mul-float/2addr v13, v9

    #@5f
    mul-float/2addr v13, v9

    #@60
    add-float/2addr v12, v13

    #@61
    mul-float v13, v1, v4

    #@63
    mul-float/2addr v13, v4

    #@64
    mul-float v14, v3, v9

    #@66
    mul-float/2addr v14, v9

    #@67
    add-float/2addr v13, v14

    #@68
    div-float/2addr v12, v13

    #@69
    add-float v8, v11, v12

    #@6b
    .line 327
    .local v8, radSq:F
    float-to-double v11, v8

    #@6c
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    #@6f
    move-result-wide v11

    #@70
    double-to-float v11, v11

    #@71
    iput v11, p0, Landroid/hardware/GeomagneticField;->mGcRadiusKm:F

    #@73
    .line 328
    return-void
.end method

.method private static computeSchmidtQuasiNormFactors(I)[[F
    .registers 11
    .parameter "maxN"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 394
    add-int/lit8 v3, p0, 0x1

    #@4
    new-array v2, v3, [[F

    #@6
    .line 395
    .local v2, schmidtQuasiNorm:[[F
    new-array v3, v4, [F

    #@8
    const/high16 v5, 0x3f80

    #@a
    aput v5, v3, v9

    #@c
    aput-object v3, v2, v9

    #@e
    .line 396
    const/4 v1, 0x1

    #@f
    .local v1, n:I
    :goto_f
    if-gt v1, p0, :cond_52

    #@11
    .line 397
    add-int/lit8 v3, v1, 0x1

    #@13
    new-array v3, v3, [F

    #@15
    aput-object v3, v2, v1

    #@17
    .line 398
    aget-object v3, v2, v1

    #@19
    add-int/lit8 v5, v1, -0x1

    #@1b
    aget-object v5, v2, v5

    #@1d
    aget v5, v5, v9

    #@1f
    mul-int/lit8 v6, v1, 0x2

    #@21
    add-int/lit8 v6, v6, -0x1

    #@23
    int-to-float v6, v6

    #@24
    mul-float/2addr v5, v6

    #@25
    int-to-float v6, v1

    #@26
    div-float/2addr v5, v6

    #@27
    aput v5, v3, v9

    #@29
    .line 400
    const/4 v0, 0x1

    #@2a
    .local v0, m:I
    :goto_2a
    if-gt v0, v1, :cond_4f

    #@2c
    .line 401
    aget-object v5, v2, v1

    #@2e
    aget-object v3, v2, v1

    #@30
    add-int/lit8 v6, v0, -0x1

    #@32
    aget v6, v3, v6

    #@34
    sub-int v3, v1, v0

    #@36
    add-int/lit8 v7, v3, 0x1

    #@38
    if-ne v0, v4, :cond_4d

    #@3a
    const/4 v3, 0x2

    #@3b
    :goto_3b
    mul-int/2addr v3, v7

    #@3c
    int-to-float v3, v3

    #@3d
    add-int v7, v1, v0

    #@3f
    int-to-float v7, v7

    #@40
    div-float/2addr v3, v7

    #@41
    float-to-double v7, v3

    #@42
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    #@45
    move-result-wide v7

    #@46
    double-to-float v3, v7

    #@47
    mul-float/2addr v3, v6

    #@48
    aput v3, v5, v0

    #@4a
    .line 400
    add-int/lit8 v0, v0, 0x1

    #@4c
    goto :goto_2a

    #@4d
    :cond_4d
    move v3, v4

    #@4e
    .line 401
    goto :goto_3b

    #@4f
    .line 396
    :cond_4f
    add-int/lit8 v1, v1, 0x1

    #@51
    goto :goto_f

    #@52
    .line 406
    .end local v0           #m:I
    :cond_52
    return-object v2
.end method


# virtual methods
.method public getDeclination()F
    .registers 5

    #@0
    .prologue
    .line 268
    iget v0, p0, Landroid/hardware/GeomagneticField;->mY:F

    #@2
    float-to-double v0, v0

    #@3
    iget v2, p0, Landroid/hardware/GeomagneticField;->mX:F

    #@5
    float-to-double v2, v2

    #@6
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@9
    move-result-wide v0

    #@a
    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    #@d
    move-result-wide v0

    #@e
    double-to-float v0, v0

    #@f
    return v0
.end method

.method public getFieldStrength()F
    .registers 4

    #@0
    .prologue
    .line 291
    iget v0, p0, Landroid/hardware/GeomagneticField;->mX:F

    #@2
    iget v1, p0, Landroid/hardware/GeomagneticField;->mX:F

    #@4
    mul-float/2addr v0, v1

    #@5
    iget v1, p0, Landroid/hardware/GeomagneticField;->mY:F

    #@7
    iget v2, p0, Landroid/hardware/GeomagneticField;->mY:F

    #@9
    mul-float/2addr v1, v2

    #@a
    add-float/2addr v0, v1

    #@b
    iget v1, p0, Landroid/hardware/GeomagneticField;->mZ:F

    #@d
    iget v2, p0, Landroid/hardware/GeomagneticField;->mZ:F

    #@f
    mul-float/2addr v1, v2

    #@10
    add-float/2addr v0, v1

    #@11
    float-to-double v0, v0

    #@12
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@15
    move-result-wide v0

    #@16
    double-to-float v0, v0

    #@17
    return v0
.end method

.method public getHorizontalStrength()F
    .registers 4

    #@0
    .prologue
    .line 284
    iget v0, p0, Landroid/hardware/GeomagneticField;->mX:F

    #@2
    iget v1, p0, Landroid/hardware/GeomagneticField;->mX:F

    #@4
    mul-float/2addr v0, v1

    #@5
    iget v1, p0, Landroid/hardware/GeomagneticField;->mY:F

    #@7
    iget v2, p0, Landroid/hardware/GeomagneticField;->mY:F

    #@9
    mul-float/2addr v1, v2

    #@a
    add-float/2addr v0, v1

    #@b
    float-to-double v0, v0

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@f
    move-result-wide v0

    #@10
    double-to-float v0, v0

    #@11
    return v0
.end method

.method public getInclination()F
    .registers 5

    #@0
    .prologue
    .line 276
    iget v0, p0, Landroid/hardware/GeomagneticField;->mZ:F

    #@2
    float-to-double v0, v0

    #@3
    invoke-virtual {p0}, Landroid/hardware/GeomagneticField;->getHorizontalStrength()F

    #@6
    move-result v2

    #@7
    float-to-double v2, v2

    #@8
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@b
    move-result-wide v0

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    #@f
    move-result-wide v0

    #@10
    double-to-float v0, v0

    #@11
    return v0
.end method

.method public getX()F
    .registers 2

    #@0
    .prologue
    .line 245
    iget v0, p0, Landroid/hardware/GeomagneticField;->mX:F

    #@2
    return v0
.end method

.method public getY()F
    .registers 2

    #@0
    .prologue
    .line 252
    iget v0, p0, Landroid/hardware/GeomagneticField;->mY:F

    #@2
    return v0
.end method

.method public getZ()F
    .registers 2

    #@0
    .prologue
    .line 259
    iget v0, p0, Landroid/hardware/GeomagneticField;->mZ:F

    #@2
    return v0
.end method
