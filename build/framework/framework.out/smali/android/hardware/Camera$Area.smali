.class public Landroid/hardware/Camera$Area;
.super Ljava/lang/Object;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Area"
.end annotation


# instance fields
.field public rect:Landroid/graphics/Rect;

.field public weight:I


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;I)V
    .registers 3
    .parameter "rect"
    .parameter "weight"

    #@0
    .prologue
    .line 1966
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1967
    iput-object p1, p0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@5
    .line 1968
    iput p2, p0, Landroid/hardware/Camera$Area;->weight:I

    #@7
    .line 1969
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1979
    instance-of v2, p1, Landroid/hardware/Camera$Area;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1988
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 1982
    check-cast v0, Landroid/hardware/Camera$Area;

    #@9
    .line 1983
    .local v0, a:Landroid/hardware/Camera$Area;
    iget-object v2, p0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@b
    if-nez v2, :cond_19

    #@d
    .line 1984
    iget-object v2, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@f
    if-nez v2, :cond_5

    #@11
    .line 1988
    :cond_11
    iget v2, p0, Landroid/hardware/Camera$Area;->weight:I

    #@13
    iget v3, v0, Landroid/hardware/Camera$Area;->weight:I

    #@15
    if-ne v2, v3, :cond_5

    #@17
    const/4 v1, 0x1

    #@18
    goto :goto_5

    #@19
    .line 1986
    :cond_19
    iget-object v2, p0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@1b
    iget-object v3, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    #@1d
    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_11

    #@23
    goto :goto_5
.end method
