.class Landroid/hardware/Camera$EventHandler;
.super Landroid/os/Handler;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mCamera:Landroid/hardware/Camera;

.field final synthetic this$0:Landroid/hardware/Camera;


# direct methods
.method public constructor <init>(Landroid/hardware/Camera;Landroid/hardware/Camera;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "c"
    .parameter "looper"

    #@0
    .prologue
    .line 786
    iput-object p1, p0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@2
    .line 787
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 788
    iput-object p2, p0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@7
    .line 789
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 19
    .parameter "msg"

    #@0
    .prologue
    .line 793
    move-object/from16 v0, p1

    #@2
    iget v13, v0, Landroid/os/Message;->what:I

    #@4
    sparse-switch v13, :sswitch_data_344

    #@7
    .line 942
    const-string v13, "Camera"

    #@9
    new-instance v14, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v15, "Unknown message type "

    #@10
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v14

    #@14
    move-object/from16 v0, p1

    #@16
    iget v15, v0, Landroid/os/Message;->what:I

    #@18
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v14

    #@1c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v14

    #@20
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 943
    :cond_23
    :goto_23
    return-void

    #@24
    .line 795
    :sswitch_24
    move-object/from16 v0, p0

    #@26
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@28
    invoke-static {v13}, Landroid/hardware/Camera;->access$000(Landroid/hardware/Camera;)Landroid/hardware/Camera$ShutterCallback;

    #@2b
    move-result-object v13

    #@2c
    if-eqz v13, :cond_23

    #@2e
    .line 796
    move-object/from16 v0, p0

    #@30
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@32
    invoke-static {v13}, Landroid/hardware/Camera;->access$000(Landroid/hardware/Camera;)Landroid/hardware/Camera$ShutterCallback;

    #@35
    move-result-object v13

    #@36
    invoke-interface {v13}, Landroid/hardware/Camera$ShutterCallback;->onShutter()V

    #@39
    goto :goto_23

    #@3a
    .line 801
    :sswitch_3a
    move-object/from16 v0, p0

    #@3c
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@3e
    invoke-static {v13}, Landroid/hardware/Camera;->access$100(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;

    #@41
    move-result-object v13

    #@42
    if-eqz v13, :cond_23

    #@44
    .line 802
    move-object/from16 v0, p0

    #@46
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@48
    invoke-static {v13}, Landroid/hardware/Camera;->access$100(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;

    #@4b
    move-result-object v14

    #@4c
    move-object/from16 v0, p1

    #@4e
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@50
    check-cast v13, [B

    #@52
    check-cast v13, [B

    #@54
    move-object/from16 v0, p0

    #@56
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@58
    invoke-interface {v14, v13, v15}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    #@5b
    goto :goto_23

    #@5c
    .line 807
    :sswitch_5c
    move-object/from16 v0, p0

    #@5e
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@60
    invoke-static {v13}, Landroid/hardware/Camera;->access$200(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;

    #@63
    move-result-object v13

    #@64
    if-eqz v13, :cond_23

    #@66
    .line 808
    move-object/from16 v0, p0

    #@68
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@6a
    invoke-static {v13}, Landroid/hardware/Camera;->access$200(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;

    #@6d
    move-result-object v14

    #@6e
    move-object/from16 v0, p1

    #@70
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@72
    check-cast v13, [B

    #@74
    check-cast v13, [B

    #@76
    move-object/from16 v0, p0

    #@78
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@7a
    invoke-interface {v14, v13, v15}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    #@7d
    goto :goto_23

    #@7e
    .line 813
    :sswitch_7e
    move-object/from16 v0, p0

    #@80
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@82
    invoke-static {v13}, Landroid/hardware/Camera;->access$300(Landroid/hardware/Camera;)Landroid/hardware/Camera$PreviewCallback;

    #@85
    move-result-object v8

    #@86
    .line 814
    .local v8, pCb:Landroid/hardware/Camera$PreviewCallback;
    if-eqz v8, :cond_23

    #@88
    .line 815
    move-object/from16 v0, p0

    #@8a
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@8c
    invoke-static {v13}, Landroid/hardware/Camera;->access$400(Landroid/hardware/Camera;)Z

    #@8f
    move-result v13

    #@90
    if-eqz v13, :cond_ab

    #@92
    .line 819
    move-object/from16 v0, p0

    #@94
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@96
    const/4 v14, 0x0

    #@97
    invoke-static {v13, v14}, Landroid/hardware/Camera;->access$302(Landroid/hardware/Camera;Landroid/hardware/Camera$PreviewCallback;)Landroid/hardware/Camera$PreviewCallback;

    #@9a
    .line 826
    :cond_9a
    :goto_9a
    move-object/from16 v0, p1

    #@9c
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9e
    check-cast v13, [B

    #@a0
    check-cast v13, [B

    #@a2
    move-object/from16 v0, p0

    #@a4
    iget-object v14, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@a6
    invoke-interface {v8, v13, v14}, Landroid/hardware/Camera$PreviewCallback;->onPreviewFrame([BLandroid/hardware/Camera;)V

    #@a9
    goto/16 :goto_23

    #@ab
    .line 820
    :cond_ab
    move-object/from16 v0, p0

    #@ad
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@af
    invoke-static {v13}, Landroid/hardware/Camera;->access$500(Landroid/hardware/Camera;)Z

    #@b2
    move-result v13

    #@b3
    if-nez v13, :cond_9a

    #@b5
    .line 824
    move-object/from16 v0, p0

    #@b7
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@b9
    const/4 v14, 0x1

    #@ba
    const/4 v15, 0x0

    #@bb
    invoke-static {v13, v14, v15}, Landroid/hardware/Camera;->access$600(Landroid/hardware/Camera;ZZ)V

    #@be
    goto :goto_9a

    #@bf
    .line 831
    .end local v8           #pCb:Landroid/hardware/Camera$PreviewCallback;
    :sswitch_bf
    move-object/from16 v0, p0

    #@c1
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@c3
    invoke-static {v13}, Landroid/hardware/Camera;->access$700(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;

    #@c6
    move-result-object v13

    #@c7
    if-eqz v13, :cond_23

    #@c9
    .line 832
    move-object/from16 v0, p0

    #@cb
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@cd
    invoke-static {v13}, Landroid/hardware/Camera;->access$700(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;

    #@d0
    move-result-object v14

    #@d1
    move-object/from16 v0, p1

    #@d3
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d5
    check-cast v13, [B

    #@d7
    check-cast v13, [B

    #@d9
    move-object/from16 v0, p0

    #@db
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@dd
    invoke-interface {v14, v13, v15}, Landroid/hardware/Camera$PictureCallback;->onPictureTaken([BLandroid/hardware/Camera;)V

    #@e0
    goto/16 :goto_23

    #@e2
    .line 837
    :sswitch_e2
    const/4 v2, 0x0

    #@e3
    .line 838
    .local v2, cb:Landroid/hardware/Camera$AutoFocusCallback;
    move-object/from16 v0, p0

    #@e5
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@e7
    invoke-static {v13}, Landroid/hardware/Camera;->access$800(Landroid/hardware/Camera;)Ljava/lang/Object;

    #@ea
    move-result-object v14

    #@eb
    monitor-enter v14

    #@ec
    .line 839
    :try_start_ec
    move-object/from16 v0, p0

    #@ee
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@f0
    invoke-static {v13}, Landroid/hardware/Camera;->access$900(Landroid/hardware/Camera;)Landroid/hardware/Camera$AutoFocusCallback;

    #@f3
    move-result-object v2

    #@f4
    .line 840
    monitor-exit v14
    :try_end_f5
    .catchall {:try_start_ec .. :try_end_f5} :catchall_107

    #@f5
    .line 841
    if-eqz v2, :cond_23

    #@f7
    .line 842
    move-object/from16 v0, p1

    #@f9
    iget v13, v0, Landroid/os/Message;->arg1:I

    #@fb
    if-nez v13, :cond_10a

    #@fd
    const/4 v12, 0x0

    #@fe
    .line 843
    .local v12, success:Z
    :goto_fe
    move-object/from16 v0, p0

    #@100
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@102
    invoke-interface {v2, v12, v13}, Landroid/hardware/Camera$AutoFocusCallback;->onAutoFocus(ZLandroid/hardware/Camera;)V

    #@105
    goto/16 :goto_23

    #@107
    .line 840
    .end local v12           #success:Z
    :catchall_107
    move-exception v13

    #@108
    :try_start_108
    monitor-exit v14
    :try_end_109
    .catchall {:try_start_108 .. :try_end_109} :catchall_107

    #@109
    throw v13

    #@10a
    .line 842
    :cond_10a
    const/4 v12, 0x1

    #@10b
    goto :goto_fe

    #@10c
    .line 848
    .end local v2           #cb:Landroid/hardware/Camera$AutoFocusCallback;
    :sswitch_10c
    move-object/from16 v0, p0

    #@10e
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@110
    invoke-static {v13}, Landroid/hardware/Camera;->access$1000(Landroid/hardware/Camera;)Landroid/hardware/Camera$OnZoomChangeListener;

    #@113
    move-result-object v13

    #@114
    if-eqz v13, :cond_23

    #@116
    .line 849
    move-object/from16 v0, p0

    #@118
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@11a
    invoke-static {v13}, Landroid/hardware/Camera;->access$1000(Landroid/hardware/Camera;)Landroid/hardware/Camera$OnZoomChangeListener;

    #@11d
    move-result-object v14

    #@11e
    move-object/from16 v0, p1

    #@120
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@122
    move-object/from16 v0, p1

    #@124
    iget v13, v0, Landroid/os/Message;->arg2:I

    #@126
    if-eqz v13, :cond_136

    #@128
    const/4 v13, 0x1

    #@129
    :goto_129
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@12d
    move-object/from16 v16, v0

    #@12f
    move-object/from16 v0, v16

    #@131
    invoke-interface {v14, v15, v13, v0}, Landroid/hardware/Camera$OnZoomChangeListener;->onZoomChange(IZLandroid/hardware/Camera;)V

    #@134
    goto/16 :goto_23

    #@136
    :cond_136
    const/4 v13, 0x0

    #@137
    goto :goto_129

    #@138
    .line 854
    :sswitch_138
    move-object/from16 v0, p0

    #@13a
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@13c
    invoke-static {v13}, Landroid/hardware/Camera;->access$1100(Landroid/hardware/Camera;)Landroid/hardware/Camera$FaceDetectionListener;

    #@13f
    move-result-object v13

    #@140
    if-eqz v13, :cond_23

    #@142
    .line 855
    move-object/from16 v0, p0

    #@144
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@146
    invoke-static {v13}, Landroid/hardware/Camera;->access$1100(Landroid/hardware/Camera;)Landroid/hardware/Camera$FaceDetectionListener;

    #@149
    move-result-object v14

    #@14a
    move-object/from16 v0, p1

    #@14c
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14e
    check-cast v13, [Landroid/hardware/Camera$Face;

    #@150
    check-cast v13, [Landroid/hardware/Camera$Face;

    #@152
    move-object/from16 v0, p0

    #@154
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@156
    invoke-interface {v14, v13, v15}, Landroid/hardware/Camera$FaceDetectionListener;->onFaceDetection([Landroid/hardware/Camera$Face;Landroid/hardware/Camera;)V

    #@159
    goto/16 :goto_23

    #@15b
    .line 860
    :sswitch_15b
    const-string v13, "Camera"

    #@15d
    new-instance v14, Ljava/lang/StringBuilder;

    #@15f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@162
    const-string v15, "Error "

    #@164
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v14

    #@168
    move-object/from16 v0, p1

    #@16a
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@16c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v14

    #@170
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@173
    move-result-object v14

    #@174
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@177
    .line 861
    move-object/from16 v0, p0

    #@179
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@17b
    invoke-static {v13}, Landroid/hardware/Camera;->access$1200(Landroid/hardware/Camera;)Landroid/hardware/Camera$ErrorCallback;

    #@17e
    move-result-object v13

    #@17f
    if-eqz v13, :cond_23

    #@181
    .line 862
    move-object/from16 v0, p0

    #@183
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@185
    invoke-static {v13}, Landroid/hardware/Camera;->access$1200(Landroid/hardware/Camera;)Landroid/hardware/Camera$ErrorCallback;

    #@188
    move-result-object v13

    #@189
    move-object/from16 v0, p1

    #@18b
    iget v14, v0, Landroid/os/Message;->arg1:I

    #@18d
    move-object/from16 v0, p0

    #@18f
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@191
    invoke-interface {v13, v14, v15}, Landroid/hardware/Camera$ErrorCallback;->onError(ILandroid/hardware/Camera;)V

    #@194
    goto/16 :goto_23

    #@196
    .line 867
    :sswitch_196
    move-object/from16 v0, p0

    #@198
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@19a
    invoke-static {v13}, Landroid/hardware/Camera;->access$1300(Landroid/hardware/Camera;)Landroid/hardware/Camera$AutoFocusMoveCallback;

    #@19d
    move-result-object v13

    #@19e
    if-eqz v13, :cond_23

    #@1a0
    .line 868
    move-object/from16 v0, p0

    #@1a2
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@1a4
    invoke-static {v13}, Landroid/hardware/Camera;->access$1300(Landroid/hardware/Camera;)Landroid/hardware/Camera$AutoFocusMoveCallback;

    #@1a7
    move-result-object v14

    #@1a8
    move-object/from16 v0, p1

    #@1aa
    iget v13, v0, Landroid/os/Message;->arg1:I

    #@1ac
    if-nez v13, :cond_1b8

    #@1ae
    const/4 v13, 0x0

    #@1af
    :goto_1af
    move-object/from16 v0, p0

    #@1b1
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@1b3
    invoke-interface {v14, v13, v15}, Landroid/hardware/Camera$AutoFocusMoveCallback;->onAutoFocusMoving(ZLandroid/hardware/Camera;)V

    #@1b6
    goto/16 :goto_23

    #@1b8
    :cond_1b8
    const/4 v13, 0x1

    #@1b9
    goto :goto_1af

    #@1ba
    .line 873
    :sswitch_1ba
    const/16 v13, 0x101

    #@1bc
    new-array v11, v13, [I

    #@1be
    .line 874
    .local v11, statsdata:[I
    const/4 v5, 0x0

    #@1bf
    .local v5, i:I
    :goto_1bf
    const/16 v13, 0x101

    #@1c1
    if-ge v5, v13, :cond_1d6

    #@1c3
    .line 875
    move-object/from16 v0, p1

    #@1c5
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c7
    check-cast v13, [B

    #@1c9
    check-cast v13, [B

    #@1cb
    mul-int/lit8 v14, v5, 0x4

    #@1cd
    invoke-static {v13, v14}, Landroid/hardware/Camera;->access$1400([BI)I

    #@1d0
    move-result v13

    #@1d1
    aput v13, v11, v5

    #@1d3
    .line 874
    add-int/lit8 v5, v5, 0x1

    #@1d5
    goto :goto_1bf

    #@1d6
    .line 877
    :cond_1d6
    move-object/from16 v0, p0

    #@1d8
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@1da
    invoke-static {v13}, Landroid/hardware/Camera;->access$1500(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraDataCallback;

    #@1dd
    move-result-object v13

    #@1de
    if-eqz v13, :cond_23

    #@1e0
    .line 878
    move-object/from16 v0, p0

    #@1e2
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@1e4
    invoke-static {v13}, Landroid/hardware/Camera;->access$1500(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraDataCallback;

    #@1e7
    move-result-object v13

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    iget-object v14, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@1ec
    invoke-interface {v13, v11, v14}, Landroid/hardware/Camera$CameraDataCallback;->onCameraData([ILandroid/hardware/Camera;)V

    #@1ef
    goto/16 :goto_23

    #@1f1
    .line 883
    .end local v5           #i:I
    .end local v11           #statsdata:[I
    :sswitch_1f1
    move-object/from16 v0, p0

    #@1f3
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@1f5
    invoke-static {v13}, Landroid/hardware/Camera;->access$1600(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraMetaDataCallback;

    #@1f8
    move-result-object v13

    #@1f9
    if-eqz v13, :cond_212

    #@1fb
    .line 884
    move-object/from16 v0, p0

    #@1fd
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@1ff
    invoke-static {v13}, Landroid/hardware/Camera;->access$1600(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraMetaDataCallback;

    #@202
    move-result-object v14

    #@203
    move-object/from16 v0, p1

    #@205
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@207
    check-cast v13, [I

    #@209
    check-cast v13, [I

    #@20b
    move-object/from16 v0, p0

    #@20d
    iget-object v15, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@20f
    invoke-interface {v14, v13, v15}, Landroid/hardware/Camera$CameraMetaDataCallback;->onCameraMetaData([ILandroid/hardware/Camera;)V

    #@212
    .line 887
    :cond_212
    move-object/from16 v0, p0

    #@214
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@216
    invoke-static {v13}, Landroid/hardware/Camera;->access$1500(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraDataCallback;

    #@219
    move-result-object v13

    #@21a
    if-eqz v13, :cond_23

    #@21c
    .line 890
    const/4 v13, 0x5

    #@21d
    new-array v6, v13, [S

    #@21f
    .line 891
    .local v6, obt_data:[S
    move-object/from16 v0, p1

    #@221
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@223
    check-cast v13, [B

    #@225
    move-object v1, v13

    #@226
    check-cast v1, [B

    #@228
    .line 899
    .local v1, byteData:[B
    const/4 v13, 0x0

    #@229
    const/4 v14, 0x1

    #@22a
    aget-byte v14, v1, v14

    #@22c
    and-int/lit16 v14, v14, 0xff

    #@22e
    shl-int/lit8 v14, v14, 0x8

    #@230
    const/4 v15, 0x0

    #@231
    aget-byte v15, v1, v15

    #@233
    and-int/lit16 v15, v15, 0xff

    #@235
    or-int/2addr v14, v15

    #@236
    int-to-short v14, v14

    #@237
    aput-short v14, v6, v13

    #@239
    .line 900
    const/4 v13, 0x1

    #@23a
    const/4 v14, 0x3

    #@23b
    aget-byte v14, v1, v14

    #@23d
    and-int/lit16 v14, v14, 0xff

    #@23f
    shl-int/lit8 v14, v14, 0x8

    #@241
    const/4 v15, 0x2

    #@242
    aget-byte v15, v1, v15

    #@244
    and-int/lit16 v15, v15, 0xff

    #@246
    or-int/2addr v14, v15

    #@247
    int-to-short v14, v14

    #@248
    aput-short v14, v6, v13

    #@24a
    .line 901
    const/4 v13, 0x2

    #@24b
    const/4 v14, 0x5

    #@24c
    aget-byte v14, v1, v14

    #@24e
    and-int/lit16 v14, v14, 0xff

    #@250
    shl-int/lit8 v14, v14, 0x8

    #@252
    const/4 v15, 0x4

    #@253
    aget-byte v15, v1, v15

    #@255
    and-int/lit16 v15, v15, 0xff

    #@257
    or-int/2addr v14, v15

    #@258
    int-to-short v14, v14

    #@259
    aput-short v14, v6, v13

    #@25b
    .line 902
    const/4 v13, 0x3

    #@25c
    const/4 v14, 0x7

    #@25d
    aget-byte v14, v1, v14

    #@25f
    and-int/lit16 v14, v14, 0xff

    #@261
    shl-int/lit8 v14, v14, 0x8

    #@263
    const/4 v15, 0x6

    #@264
    aget-byte v15, v1, v15

    #@266
    and-int/lit16 v15, v15, 0xff

    #@268
    or-int/2addr v14, v15

    #@269
    int-to-short v14, v14

    #@26a
    aput-short v14, v6, v13

    #@26c
    .line 903
    const/4 v13, 0x4

    #@26d
    const/16 v14, 0x9

    #@26f
    aget-byte v14, v1, v14

    #@271
    and-int/lit16 v14, v14, 0xff

    #@273
    shl-int/lit8 v14, v14, 0x8

    #@275
    const/16 v15, 0x8

    #@277
    aget-byte v15, v1, v15

    #@279
    and-int/lit16 v15, v15, 0xff

    #@27b
    or-int/2addr v14, v15

    #@27c
    int-to-short v14, v14

    #@27d
    aput-short v14, v6, v13

    #@27f
    .line 907
    const/4 v13, 0x5

    #@280
    new-array v7, v13, [I

    #@282
    .line 908
    .local v7, obt_data_i:[I
    const/4 v5, 0x0

    #@283
    .restart local v5       #i:I
    :goto_283
    const/4 v13, 0x5

    #@284
    if-ge v5, v13, :cond_28d

    #@286
    .line 909
    aget-short v13, v6, v5

    #@288
    aput v13, v7, v5

    #@28a
    .line 908
    add-int/lit8 v5, v5, 0x1

    #@28c
    goto :goto_283

    #@28d
    .line 913
    :cond_28d
    move-object/from16 v0, p0

    #@28f
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@291
    invoke-static {v13}, Landroid/hardware/Camera;->access$1500(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraDataCallback;

    #@294
    move-result-object v13

    #@295
    move-object/from16 v0, p0

    #@297
    iget-object v14, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@299
    invoke-interface {v13, v7, v14}, Landroid/hardware/Camera$CameraDataCallback;->onCameraData([ILandroid/hardware/Camera;)V

    #@29c
    goto/16 :goto_23

    #@29e
    .line 921
    .end local v1           #byteData:[B
    .end local v5           #i:I
    .end local v6           #obt_data:[S
    .end local v7           #obt_data_i:[I
    :sswitch_29e
    move-object/from16 v0, p0

    #@2a0
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@2a2
    invoke-static {v13}, Landroid/hardware/Camera;->access$1700(Landroid/hardware/Camera;)Landroid/hardware/Camera$OisDataListener;

    #@2a5
    move-result-object v13

    #@2a6
    if-eqz v13, :cond_23

    #@2a8
    .line 922
    new-instance v3, Landroid/hardware/Camera$OisData;

    #@2aa
    invoke-direct {v3}, Landroid/hardware/Camera$OisData;-><init>()V

    #@2ad
    .line 923
    .local v3, data:Landroid/hardware/Camera$OisData;
    move-object/from16 v0, p1

    #@2af
    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2b1
    check-cast v13, [B

    #@2b3
    move-object v1, v13

    #@2b4
    check-cast v1, [B

    #@2b6
    .line 924
    .restart local v1       #byteData:[B
    const/4 v9, 0x0

    #@2b7
    .line 925
    .local v9, ptr:I
    add-int/lit8 v10, v9, 0x1

    #@2b9
    .end local v9           #ptr:I
    .local v10, ptr:I
    aget-byte v13, v1, v9

    #@2bb
    and-int/lit16 v13, v13, 0xff

    #@2bd
    add-int/lit8 v9, v10, 0x1

    #@2bf
    .end local v10           #ptr:I
    .restart local v9       #ptr:I
    aget-byte v14, v1, v10

    #@2c1
    and-int/lit16 v14, v14, 0xff

    #@2c3
    shl-int/lit8 v14, v14, 0x8

    #@2c5
    or-int v4, v13, v14

    #@2c7
    .line 926
    .local v4, dim:I
    const/4 v13, 0x3

    #@2c8
    if-gt v4, v13, :cond_2cc

    #@2ca
    if-gez v4, :cond_2cd

    #@2cc
    :cond_2cc
    const/4 v4, 0x0

    #@2cd
    .line 927
    :cond_2cd
    new-array v13, v4, [I

    #@2cf
    iput-object v13, v3, Landroid/hardware/Camera$OisData;->gyro:[I

    #@2d1
    .line 928
    new-array v13, v4, [I

    #@2d3
    iput-object v13, v3, Landroid/hardware/Camera$OisData;->lens_target:[I

    #@2d5
    .line 929
    new-array v13, v4, [I

    #@2d7
    iput-object v13, v3, Landroid/hardware/Camera$OisData;->lens_position:[I

    #@2d9
    .line 930
    const/4 v5, 0x0

    #@2da
    .restart local v5       #i:I
    move v10, v9

    #@2db
    .end local v9           #ptr:I
    .restart local v10       #ptr:I
    :goto_2db
    if-ge v5, v4, :cond_31c

    #@2dd
    .line 932
    iget-object v13, v3, Landroid/hardware/Camera$OisData;->gyro:[I

    #@2df
    add-int/lit8 v9, v10, 0x1

    #@2e1
    .end local v10           #ptr:I
    .restart local v9       #ptr:I
    aget-byte v14, v1, v10

    #@2e3
    and-int/lit16 v14, v14, 0xff

    #@2e5
    add-int/lit8 v10, v9, 0x1

    #@2e7
    .end local v9           #ptr:I
    .restart local v10       #ptr:I
    aget-byte v15, v1, v9

    #@2e9
    and-int/lit16 v15, v15, 0xff

    #@2eb
    shl-int/lit8 v15, v15, 0x8

    #@2ed
    or-int/2addr v14, v15

    #@2ee
    int-to-short v14, v14

    #@2ef
    aput v14, v13, v5

    #@2f1
    .line 933
    iget-object v13, v3, Landroid/hardware/Camera$OisData;->lens_target:[I

    #@2f3
    add-int/lit8 v9, v10, 0x1

    #@2f5
    .end local v10           #ptr:I
    .restart local v9       #ptr:I
    aget-byte v14, v1, v10

    #@2f7
    and-int/lit16 v14, v14, 0xff

    #@2f9
    add-int/lit8 v10, v9, 0x1

    #@2fb
    .end local v9           #ptr:I
    .restart local v10       #ptr:I
    aget-byte v15, v1, v9

    #@2fd
    and-int/lit16 v15, v15, 0xff

    #@2ff
    shl-int/lit8 v15, v15, 0x8

    #@301
    or-int/2addr v14, v15

    #@302
    int-to-short v14, v14

    #@303
    aput v14, v13, v5

    #@305
    .line 934
    iget-object v13, v3, Landroid/hardware/Camera$OisData;->lens_position:[I

    #@307
    add-int/lit8 v9, v10, 0x1

    #@309
    .end local v10           #ptr:I
    .restart local v9       #ptr:I
    aget-byte v14, v1, v10

    #@30b
    and-int/lit16 v14, v14, 0xff

    #@30d
    add-int/lit8 v10, v9, 0x1

    #@30f
    .end local v9           #ptr:I
    .restart local v10       #ptr:I
    aget-byte v15, v1, v9

    #@311
    and-int/lit16 v15, v15, 0xff

    #@313
    shl-int/lit8 v15, v15, 0x8

    #@315
    or-int/2addr v14, v15

    #@316
    int-to-short v14, v14

    #@317
    aput v14, v13, v5

    #@319
    .line 930
    add-int/lit8 v5, v5, 0x1

    #@31b
    goto :goto_2db

    #@31c
    .line 936
    :cond_31c
    add-int/lit8 v9, v10, 0x1

    #@31e
    .end local v10           #ptr:I
    .restart local v9       #ptr:I
    aget-byte v13, v1, v10

    #@320
    and-int/lit16 v13, v13, 0xff

    #@322
    add-int/lit8 v10, v9, 0x1

    #@324
    .end local v9           #ptr:I
    .restart local v10       #ptr:I
    aget-byte v14, v1, v9

    #@326
    and-int/lit16 v14, v14, 0xff

    #@328
    shl-int/lit8 v14, v14, 0x8

    #@32a
    or-int/2addr v13, v14

    #@32b
    if-lez v13, :cond_341

    #@32d
    const/4 v13, 0x1

    #@32e
    :goto_32e
    iput-boolean v13, v3, Landroid/hardware/Camera$OisData;->is_valid:Z

    #@330
    .line 937
    move-object/from16 v0, p0

    #@332
    iget-object v13, v0, Landroid/hardware/Camera$EventHandler;->this$0:Landroid/hardware/Camera;

    #@334
    invoke-static {v13}, Landroid/hardware/Camera;->access$1700(Landroid/hardware/Camera;)Landroid/hardware/Camera$OisDataListener;

    #@337
    move-result-object v13

    #@338
    move-object/from16 v0, p0

    #@33a
    iget-object v14, v0, Landroid/hardware/Camera$EventHandler;->mCamera:Landroid/hardware/Camera;

    #@33c
    invoke-interface {v13, v3, v14}, Landroid/hardware/Camera$OisDataListener;->onDataListen(Landroid/hardware/Camera$OisData;Landroid/hardware/Camera;)V

    #@33f
    goto/16 :goto_23

    #@341
    .line 936
    :cond_341
    const/4 v13, 0x0

    #@342
    goto :goto_32e

    #@343
    .line 793
    nop

    #@344
    :sswitch_data_344
    .sparse-switch
        0x1 -> :sswitch_15b
        0x2 -> :sswitch_24
        0x4 -> :sswitch_e2
        0x8 -> :sswitch_10c
        0x10 -> :sswitch_7e
        0x40 -> :sswitch_bf
        0x80 -> :sswitch_3a
        0x100 -> :sswitch_5c
        0x400 -> :sswitch_138
        0x800 -> :sswitch_196
        0x1000 -> :sswitch_1ba
        0x2000 -> :sswitch_1f1
        0x4000 -> :sswitch_29e
    .end sparse-switch
.end method
