.class public Landroid/hardware/usb/UsbDevice;
.super Ljava/lang/Object;
.source "UsbDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "UsbDevice"


# instance fields
.field private final mClass:I

.field private final mInterfaces:[Landroid/os/Parcelable;

.field private final mName:Ljava/lang/String;

.field private final mProductId:I

.field private final mProtocol:I

.field private final mSubclass:I

.field private final mVendorId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 182
    new-instance v0, Landroid/hardware/usb/UsbDevice$1;

    #@2
    invoke-direct {v0}, Landroid/hardware/usb/UsbDevice$1;-><init>()V

    #@5
    sput-object v0, Landroid/hardware/usb/UsbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIII[Landroid/os/Parcelable;)V
    .registers 8
    .parameter "name"
    .parameter "vendorId"
    .parameter "productId"
    .parameter "Class"
    .parameter "subClass"
    .parameter "protocol"
    .parameter "interfaces"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    iput-object p1, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@5
    .line 63
    iput p2, p0, Landroid/hardware/usb/UsbDevice;->mVendorId:I

    #@7
    .line 64
    iput p3, p0, Landroid/hardware/usb/UsbDevice;->mProductId:I

    #@9
    .line 65
    iput p4, p0, Landroid/hardware/usb/UsbDevice;->mClass:I

    #@b
    .line 66
    iput p5, p0, Landroid/hardware/usb/UsbDevice;->mSubclass:I

    #@d
    .line 67
    iput p6, p0, Landroid/hardware/usb/UsbDevice;->mProtocol:I

    #@f
    .line 68
    iput-object p7, p0, Landroid/hardware/usb/UsbDevice;->mInterfaces:[Landroid/os/Parcelable;

    #@11
    .line 69
    return-void
.end method

.method public static getDeviceId(Ljava/lang/String;)I
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 215
    invoke-static {p0}, Landroid/hardware/usb/UsbDevice;->native_get_device_id(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getDeviceName(I)Ljava/lang/String;
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 219
    invoke-static {p0}, Landroid/hardware/usb/UsbDevice;->native_get_device_name(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static native native_get_device_id(Ljava/lang/String;)I
.end method

.method private static native native_get_device_name(I)Ljava/lang/String;
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 201
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 160
    instance-of v0, p1, Landroid/hardware/usb/UsbDevice;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 161
    check-cast p1, Landroid/hardware/usb/UsbDevice;

    #@6
    .end local p1
    iget-object v0, p1, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@8
    iget-object v1, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    .line 165
    :goto_e
    return v0

    #@f
    .line 162
    .restart local p1
    :cond_f
    instance-of v0, p1, Ljava/lang/String;

    #@11
    if-eqz v0, :cond_1c

    #@13
    .line 163
    check-cast p1, Ljava/lang/String;

    #@15
    .end local p1
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@17
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    goto :goto_e

    #@1c
    .line 165
    .restart local p1
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_e
.end method

.method public getDeviceClass()I
    .registers 2

    #@0
    .prologue
    .line 119
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mClass:I

    #@2
    return v0
.end method

.method public getDeviceId()I
    .registers 2

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@2
    invoke-static {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceId(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getDeviceProtocol()I
    .registers 2

    #@0
    .prologue
    .line 137
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mProtocol:I

    #@2
    return v0
.end method

.method public getDeviceSubclass()I
    .registers 2

    #@0
    .prologue
    .line 128
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mSubclass:I

    #@2
    return v0
.end method

.method public getInterface(I)Landroid/hardware/usb/UsbInterface;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mInterfaces:[Landroid/os/Parcelable;

    #@2
    aget-object v0, v0, p1

    #@4
    check-cast v0, Landroid/hardware/usb/UsbInterface;

    #@6
    return-object v0
.end method

.method public getInterfaceCount()I
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mInterfaces:[Landroid/os/Parcelable;

    #@2
    array-length v0, v0

    #@3
    return v0
.end method

.method public getProductId()I
    .registers 2

    #@0
    .prologue
    .line 109
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mProductId:I

    #@2
    return v0
.end method

.method public getVendorId()I
    .registers 2

    #@0
    .prologue
    .line 100
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mVendorId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "UsbDevice[mName="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ",mVendorId="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/hardware/usb/UsbDevice;->mVendorId:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ",mProductId="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/hardware/usb/UsbDevice;->mProductId:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ",mClass="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/hardware/usb/UsbDevice;->mClass:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ",mSubclass="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Landroid/hardware/usb/UsbDevice;->mSubclass:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, ",mProtocol="

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Landroid/hardware/usb/UsbDevice;->mProtocol:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, ",mInterfaces="

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget-object v1, p0, Landroid/hardware/usb/UsbDevice;->mInterfaces:[Landroid/os/Parcelable;

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, "]"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 206
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mVendorId:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 207
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mProductId:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 208
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mClass:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 209
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mSubclass:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 210
    iget v0, p0, Landroid/hardware/usb/UsbDevice;->mProtocol:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 211
    iget-object v0, p0, Landroid/hardware/usb/UsbDevice;->mInterfaces:[Landroid/os/Parcelable;

    #@20
    const/4 v1, 0x0

    #@21
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    #@24
    .line 212
    return-void
.end method
