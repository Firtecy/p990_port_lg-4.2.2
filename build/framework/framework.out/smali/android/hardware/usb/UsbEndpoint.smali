.class public Landroid/hardware/usb/UsbEndpoint;
.super Ljava/lang/Object;
.source "UsbEndpoint.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/hardware/usb/UsbEndpoint;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAddress:I

.field private final mAttributes:I

.field private final mInterval:I

.field private final mMaxPacketSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 137
    new-instance v0, Landroid/hardware/usb/UsbEndpoint$1;

    #@2
    invoke-direct {v0}, Landroid/hardware/usb/UsbEndpoint$1;-><init>()V

    #@5
    sput-object v0, Landroid/hardware/usb/UsbEndpoint;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(IIII)V
    .registers 5
    .parameter "address"
    .parameter "attributes"
    .parameter "maxPacketSize"
    .parameter "interval"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    iput p1, p0, Landroid/hardware/usb/UsbEndpoint;->mAddress:I

    #@5
    .line 46
    iput p2, p0, Landroid/hardware/usb/UsbEndpoint;->mAttributes:I

    #@7
    .line 47
    iput p3, p0, Landroid/hardware/usb/UsbEndpoint;->mMaxPacketSize:I

    #@9
    .line 48
    iput p4, p0, Landroid/hardware/usb/UsbEndpoint;->mInterval:I

    #@b
    .line 49
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 153
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getAddress()I
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAddress:I

    #@2
    return v0
.end method

.method public getAttributes()I
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAttributes:I

    #@2
    return v0
.end method

.method public getDirection()I
    .registers 2

    #@0
    .prologue
    .line 85
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAddress:I

    #@2
    and-int/lit16 v0, v0, 0x80

    #@4
    return v0
.end method

.method public getEndpointNumber()I
    .registers 2

    #@0
    .prologue
    .line 70
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAddress:I

    #@2
    and-int/lit8 v0, v0, 0xf

    #@4
    return v0
.end method

.method public getInterval()I
    .registers 2

    #@0
    .prologue
    .line 128
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mInterval:I

    #@2
    return v0
.end method

.method public getMaxPacketSize()I
    .registers 2

    #@0
    .prologue
    .line 119
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mMaxPacketSize:I

    #@2
    return v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 110
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAttributes:I

    #@2
    and-int/lit8 v0, v0, 0x3

    #@4
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "UsbEndpoint[mAddress="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/hardware/usb/UsbEndpoint;->mAddress:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ",mAttributes="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/hardware/usb/UsbEndpoint;->mAttributes:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ",mMaxPacketSize="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/hardware/usb/UsbEndpoint;->mMaxPacketSize:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ",mInterval="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/hardware/usb/UsbEndpoint;->mInterval:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, "]"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 157
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAddress:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 158
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mAttributes:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 159
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mMaxPacketSize:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 160
    iget v0, p0, Landroid/hardware/usb/UsbEndpoint;->mInterval:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 161
    return-void
.end method
