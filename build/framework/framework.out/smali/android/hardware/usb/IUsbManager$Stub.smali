.class public abstract Landroid/hardware/usb/IUsbManager$Stub;
.super Landroid/os/Binder;
.source "IUsbManager.java"

# interfaces
.implements Landroid/hardware/usb/IUsbManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/usb/IUsbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/usb/IUsbManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.hardware.usb.IUsbManager"

.field static final TRANSACTION_allowUsbDebugging:I = 0x12

.field static final TRANSACTION_changeCurrentFunction:I = 0x10

.field static final TRANSACTION_clearDefaults:I = 0xe

.field static final TRANSACTION_denyUsbDebugging:I = 0x13

.field static final TRANSACTION_getCurrentAccessory:I = 0x3

.field static final TRANSACTION_getDeviceList:I = 0x1

.field static final TRANSACTION_grantAccessoryPermission:I = 0xc

.field static final TRANSACTION_grantDevicePermission:I = 0xb

.field static final TRANSACTION_hasAccessoryPermission:I = 0x8

.field static final TRANSACTION_hasDefaults:I = 0xd

.field static final TRANSACTION_hasDevicePermission:I = 0x7

.field static final TRANSACTION_openAccessory:I = 0x4

.field static final TRANSACTION_openDevice:I = 0x2

.field static final TRANSACTION_requestAccessoryPermission:I = 0xa

.field static final TRANSACTION_requestDevicePermission:I = 0x9

.field static final TRANSACTION_setAccessoryPackage:I = 0x6

.field static final TRANSACTION_setCurrentFunction:I = 0xf

.field static final TRANSACTION_setDevicePackage:I = 0x5

.field static final TRANSACTION_setMassStorageBackingFile:I = 0x11


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.hardware.usb.IUsbManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/hardware/usb/IUsbManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.hardware.usb.IUsbManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/hardware/usb/IUsbManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/hardware/usb/IUsbManager;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/hardware/usb/IUsbManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/hardware/usb/IUsbManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_258

    #@5
    .line 328
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 43
    :sswitch_a
    const-string v4, "android.hardware.usb.IUsbManager"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v6, "android.hardware.usb.IUsbManager"

    #@12
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 50
    new-instance v0, Landroid/os/Bundle;

    #@17
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1a
    .line 51
    .local v0, _arg0:Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->getDeviceList(Landroid/os/Bundle;)V

    #@1d
    .line 52
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 53
    if-eqz v0, :cond_29

    #@22
    .line 54
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 55
    invoke-virtual {v0, p3, v5}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    goto :goto_9

    #@29
    .line 58
    :cond_29
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    goto :goto_9

    #@2d
    .line 64
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_2d
    const-string v6, "android.hardware.usb.IUsbManager"

    #@2f
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .line 67
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->openDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@39
    move-result-object v3

    #@3a
    .line 68
    .local v3, _result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d
    .line 69
    if-eqz v3, :cond_46

    #@3f
    .line 70
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    .line 71
    invoke-virtual {v3, p3, v5}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@45
    goto :goto_9

    #@46
    .line 74
    :cond_46
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    goto :goto_9

    #@4a
    .line 80
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_4a
    const-string v6, "android.hardware.usb.IUsbManager"

    #@4c
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f
    .line 81
    invoke-virtual {p0}, Landroid/hardware/usb/IUsbManager$Stub;->getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;

    #@52
    move-result-object v3

    #@53
    .line 82
    .local v3, _result:Landroid/hardware/usb/UsbAccessory;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@56
    .line 83
    if-eqz v3, :cond_5f

    #@58
    .line 84
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    .line 85
    invoke-virtual {v3, p3, v5}, Landroid/hardware/usb/UsbAccessory;->writeToParcel(Landroid/os/Parcel;I)V

    #@5e
    goto :goto_9

    #@5f
    .line 88
    :cond_5f
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@62
    goto :goto_9

    #@63
    .line 94
    .end local v3           #_result:Landroid/hardware/usb/UsbAccessory;
    :sswitch_63
    const-string v6, "android.hardware.usb.IUsbManager"

    #@65
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v6

    #@6c
    if-eqz v6, :cond_86

    #@6e
    .line 97
    sget-object v6, Landroid/hardware/usb/UsbAccessory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@70
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@73
    move-result-object v0

    #@74
    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    #@76
    .line 102
    .local v0, _arg0:Landroid/hardware/usb/UsbAccessory;
    :goto_76
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;

    #@79
    move-result-object v3

    #@7a
    .line 103
    .local v3, _result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7d
    .line 104
    if-eqz v3, :cond_88

    #@7f
    .line 105
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@82
    .line 106
    invoke-virtual {v3, p3, v5}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@85
    goto :goto_9

    #@86
    .line 100
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :cond_86
    const/4 v0, 0x0

    #@87
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbAccessory;
    goto :goto_76

    #@88
    .line 109
    .restart local v3       #_result:Landroid/os/ParcelFileDescriptor;
    :cond_88
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@8b
    goto/16 :goto_9

    #@8d
    .line 115
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v3           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_8d
    const-string v4, "android.hardware.usb.IUsbManager"

    #@8f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@92
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@95
    move-result v4

    #@96
    if-eqz v4, :cond_b0

    #@98
    .line 118
    sget-object v4, Landroid/hardware/usb/UsbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@9a
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@9d
    move-result-object v0

    #@9e
    check-cast v0, Landroid/hardware/usb/UsbDevice;

    #@a0
    .line 124
    .local v0, _arg0:Landroid/hardware/usb/UsbDevice;
    :goto_a0
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    .line 126
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a7
    move-result v2

    #@a8
    .line 127
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/hardware/usb/IUsbManager$Stub;->setDevicePackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;I)V

    #@ab
    .line 128
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ae
    goto/16 :goto_9

    #@b0
    .line 121
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:I
    :cond_b0
    const/4 v0, 0x0

    #@b1
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbDevice;
    goto :goto_a0

    #@b2
    .line 133
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    :sswitch_b2
    const-string v4, "android.hardware.usb.IUsbManager"

    #@b4
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b7
    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ba
    move-result v4

    #@bb
    if-eqz v4, :cond_d5

    #@bd
    .line 136
    sget-object v4, Landroid/hardware/usb/UsbAccessory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@bf
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c2
    move-result-object v0

    #@c3
    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    #@c5
    .line 142
    .local v0, _arg0:Landroid/hardware/usb/UsbAccessory;
    :goto_c5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c8
    move-result-object v1

    #@c9
    .line 144
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cc
    move-result v2

    #@cd
    .line 145
    .restart local v2       #_arg2:I
    invoke-virtual {p0, v0, v1, v2}, Landroid/hardware/usb/IUsbManager$Stub;->setAccessoryPackage(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;I)V

    #@d0
    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d3
    goto/16 :goto_9

    #@d5
    .line 139
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:I
    :cond_d5
    const/4 v0, 0x0

    #@d6
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbAccessory;
    goto :goto_c5

    #@d7
    .line 151
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    :sswitch_d7
    const-string v6, "android.hardware.usb.IUsbManager"

    #@d9
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@df
    move-result v6

    #@e0
    if-eqz v6, :cond_f9

    #@e2
    .line 154
    sget-object v6, Landroid/hardware/usb/UsbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e4
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e7
    move-result-object v0

    #@e8
    check-cast v0, Landroid/hardware/usb/UsbDevice;

    #@ea
    .line 159
    .local v0, _arg0:Landroid/hardware/usb/UsbDevice;
    :goto_ea
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->hasDevicePermission(Landroid/hardware/usb/UsbDevice;)Z

    #@ed
    move-result v3

    #@ee
    .line 160
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f1
    .line 161
    if-eqz v3, :cond_f4

    #@f3
    move v4, v5

    #@f4
    :cond_f4
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@f7
    goto/16 :goto_9

    #@f9
    .line 157
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    .end local v3           #_result:Z
    :cond_f9
    const/4 v0, 0x0

    #@fa
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbDevice;
    goto :goto_ea

    #@fb
    .line 166
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    :sswitch_fb
    const-string v6, "android.hardware.usb.IUsbManager"

    #@fd
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@100
    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@103
    move-result v6

    #@104
    if-eqz v6, :cond_11d

    #@106
    .line 169
    sget-object v6, Landroid/hardware/usb/UsbAccessory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@108
    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10b
    move-result-object v0

    #@10c
    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    #@10e
    .line 174
    .local v0, _arg0:Landroid/hardware/usb/UsbAccessory;
    :goto_10e
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->hasAccessoryPermission(Landroid/hardware/usb/UsbAccessory;)Z

    #@111
    move-result v3

    #@112
    .line 175
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@115
    .line 176
    if-eqz v3, :cond_118

    #@117
    move v4, v5

    #@118
    :cond_118
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 172
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v3           #_result:Z
    :cond_11d
    const/4 v0, 0x0

    #@11e
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbAccessory;
    goto :goto_10e

    #@11f
    .line 181
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    :sswitch_11f
    const-string v4, "android.hardware.usb.IUsbManager"

    #@121
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@124
    .line 183
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@127
    move-result v4

    #@128
    if-eqz v4, :cond_14c

    #@12a
    .line 184
    sget-object v4, Landroid/hardware/usb/UsbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12c
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@12f
    move-result-object v0

    #@130
    check-cast v0, Landroid/hardware/usb/UsbDevice;

    #@132
    .line 190
    .local v0, _arg0:Landroid/hardware/usb/UsbDevice;
    :goto_132
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@135
    move-result-object v1

    #@136
    .line 192
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@139
    move-result v4

    #@13a
    if-eqz v4, :cond_14e

    #@13c
    .line 193
    sget-object v4, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@13e
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@141
    move-result-object v2

    #@142
    check-cast v2, Landroid/app/PendingIntent;

    #@144
    .line 198
    .local v2, _arg2:Landroid/app/PendingIntent;
    :goto_144
    invoke-virtual {p0, v0, v1, v2}, Landroid/hardware/usb/IUsbManager$Stub;->requestDevicePermission(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;Landroid/app/PendingIntent;)V

    #@147
    .line 199
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@14a
    goto/16 :goto_9

    #@14c
    .line 187
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Landroid/app/PendingIntent;
    :cond_14c
    const/4 v0, 0x0

    #@14d
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbDevice;
    goto :goto_132

    #@14e
    .line 196
    .restart local v1       #_arg1:Ljava/lang/String;
    :cond_14e
    const/4 v2, 0x0

    #@14f
    .restart local v2       #_arg2:Landroid/app/PendingIntent;
    goto :goto_144

    #@150
    .line 204
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Landroid/app/PendingIntent;
    :sswitch_150
    const-string v4, "android.hardware.usb.IUsbManager"

    #@152
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@155
    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@158
    move-result v4

    #@159
    if-eqz v4, :cond_17d

    #@15b
    .line 207
    sget-object v4, Landroid/hardware/usb/UsbAccessory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15d
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@160
    move-result-object v0

    #@161
    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    #@163
    .line 213
    .local v0, _arg0:Landroid/hardware/usb/UsbAccessory;
    :goto_163
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@166
    move-result-object v1

    #@167
    .line 215
    .restart local v1       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16a
    move-result v4

    #@16b
    if-eqz v4, :cond_17f

    #@16d
    .line 216
    sget-object v4, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@16f
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@172
    move-result-object v2

    #@173
    check-cast v2, Landroid/app/PendingIntent;

    #@175
    .line 221
    .restart local v2       #_arg2:Landroid/app/PendingIntent;
    :goto_175
    invoke-virtual {p0, v0, v1, v2}, Landroid/hardware/usb/IUsbManager$Stub;->requestAccessoryPermission(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;Landroid/app/PendingIntent;)V

    #@178
    .line 222
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17b
    goto/16 :goto_9

    #@17d
    .line 210
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Landroid/app/PendingIntent;
    :cond_17d
    const/4 v0, 0x0

    #@17e
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbAccessory;
    goto :goto_163

    #@17f
    .line 219
    .restart local v1       #_arg1:Ljava/lang/String;
    :cond_17f
    const/4 v2, 0x0

    #@180
    .restart local v2       #_arg2:Landroid/app/PendingIntent;
    goto :goto_175

    #@181
    .line 227
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Landroid/app/PendingIntent;
    :sswitch_181
    const-string v4, "android.hardware.usb.IUsbManager"

    #@183
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@186
    .line 229
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@189
    move-result v4

    #@18a
    if-eqz v4, :cond_1a0

    #@18c
    .line 230
    sget-object v4, Landroid/hardware/usb/UsbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18e
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@191
    move-result-object v0

    #@192
    check-cast v0, Landroid/hardware/usb/UsbDevice;

    #@194
    .line 236
    .local v0, _arg0:Landroid/hardware/usb/UsbDevice;
    :goto_194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@197
    move-result v1

    #@198
    .line 237
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/hardware/usb/IUsbManager$Stub;->grantDevicePermission(Landroid/hardware/usb/UsbDevice;I)V

    #@19b
    .line 238
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@19e
    goto/16 :goto_9

    #@1a0
    .line 233
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    .end local v1           #_arg1:I
    :cond_1a0
    const/4 v0, 0x0

    #@1a1
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbDevice;
    goto :goto_194

    #@1a2
    .line 243
    .end local v0           #_arg0:Landroid/hardware/usb/UsbDevice;
    :sswitch_1a2
    const-string v4, "android.hardware.usb.IUsbManager"

    #@1a4
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a7
    .line 245
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1aa
    move-result v4

    #@1ab
    if-eqz v4, :cond_1c1

    #@1ad
    .line 246
    sget-object v4, Landroid/hardware/usb/UsbAccessory;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1af
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1b2
    move-result-object v0

    #@1b3
    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    #@1b5
    .line 252
    .local v0, _arg0:Landroid/hardware/usb/UsbAccessory;
    :goto_1b5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b8
    move-result v1

    #@1b9
    .line 253
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/hardware/usb/IUsbManager$Stub;->grantAccessoryPermission(Landroid/hardware/usb/UsbAccessory;I)V

    #@1bc
    .line 254
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1bf
    goto/16 :goto_9

    #@1c1
    .line 249
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    .end local v1           #_arg1:I
    :cond_1c1
    const/4 v0, 0x0

    #@1c2
    .restart local v0       #_arg0:Landroid/hardware/usb/UsbAccessory;
    goto :goto_1b5

    #@1c3
    .line 259
    .end local v0           #_arg0:Landroid/hardware/usb/UsbAccessory;
    :sswitch_1c3
    const-string v6, "android.hardware.usb.IUsbManager"

    #@1c5
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c8
    .line 261
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1cb
    move-result-object v0

    #@1cc
    .line 263
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1cf
    move-result v1

    #@1d0
    .line 264
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/hardware/usb/IUsbManager$Stub;->hasDefaults(Ljava/lang/String;I)Z

    #@1d3
    move-result v3

    #@1d4
    .line 265
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d7
    .line 266
    if-eqz v3, :cond_1da

    #@1d9
    move v4, v5

    #@1da
    :cond_1da
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1dd
    goto/16 :goto_9

    #@1df
    .line 271
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v3           #_result:Z
    :sswitch_1df
    const-string v4, "android.hardware.usb.IUsbManager"

    #@1e1
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e4
    .line 273
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e7
    move-result-object v0

    #@1e8
    .line 275
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1eb
    move-result v1

    #@1ec
    .line 276
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/hardware/usb/IUsbManager$Stub;->clearDefaults(Ljava/lang/String;I)V

    #@1ef
    .line 277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f2
    goto/16 :goto_9

    #@1f4
    .line 282
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_1f4
    const-string v6, "android.hardware.usb.IUsbManager"

    #@1f6
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f9
    .line 284
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1fc
    move-result-object v0

    #@1fd
    .line 286
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@200
    move-result v6

    #@201
    if-eqz v6, :cond_20c

    #@203
    move v1, v5

    #@204
    .line 287
    .local v1, _arg1:Z
    :goto_204
    invoke-virtual {p0, v0, v1}, Landroid/hardware/usb/IUsbManager$Stub;->setCurrentFunction(Ljava/lang/String;Z)V

    #@207
    .line 288
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20a
    goto/16 :goto_9

    #@20c
    .end local v1           #_arg1:Z
    :cond_20c
    move v1, v4

    #@20d
    .line 286
    goto :goto_204

    #@20e
    .line 293
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_20e
    const-string v4, "android.hardware.usb.IUsbManager"

    #@210
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@213
    .line 295
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@216
    move-result-object v0

    #@217
    .line 296
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->changeCurrentFunction(Ljava/lang/String;)V

    #@21a
    .line 297
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21d
    goto/16 :goto_9

    #@21f
    .line 302
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_21f
    const-string v4, "android.hardware.usb.IUsbManager"

    #@221
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@224
    .line 304
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@227
    move-result-object v0

    #@228
    .line 305
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/hardware/usb/IUsbManager$Stub;->setMassStorageBackingFile(Ljava/lang/String;)V

    #@22b
    .line 306
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22e
    goto/16 :goto_9

    #@230
    .line 311
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_230
    const-string v6, "android.hardware.usb.IUsbManager"

    #@232
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@235
    .line 313
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@238
    move-result v6

    #@239
    if-eqz v6, :cond_248

    #@23b
    move v0, v5

    #@23c
    .line 315
    .local v0, _arg0:Z
    :goto_23c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@23f
    move-result-object v1

    #@240
    .line 316
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/hardware/usb/IUsbManager$Stub;->allowUsbDebugging(ZLjava/lang/String;)V

    #@243
    .line 317
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@246
    goto/16 :goto_9

    #@248
    .end local v0           #_arg0:Z
    .end local v1           #_arg1:Ljava/lang/String;
    :cond_248
    move v0, v4

    #@249
    .line 313
    goto :goto_23c

    #@24a
    .line 322
    :sswitch_24a
    const-string v4, "android.hardware.usb.IUsbManager"

    #@24c
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24f
    .line 323
    invoke-virtual {p0}, Landroid/hardware/usb/IUsbManager$Stub;->denyUsbDebugging()V

    #@252
    .line 324
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@255
    goto/16 :goto_9

    #@257
    .line 39
    nop

    #@258
    :sswitch_data_258
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_4a
        0x4 -> :sswitch_63
        0x5 -> :sswitch_8d
        0x6 -> :sswitch_b2
        0x7 -> :sswitch_d7
        0x8 -> :sswitch_fb
        0x9 -> :sswitch_11f
        0xa -> :sswitch_150
        0xb -> :sswitch_181
        0xc -> :sswitch_1a2
        0xd -> :sswitch_1c3
        0xe -> :sswitch_1df
        0xf -> :sswitch_1f4
        0x10 -> :sswitch_20e
        0x11 -> :sswitch_21f
        0x12 -> :sswitch_230
        0x13 -> :sswitch_24a
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
