.class public Landroid/hardware/usb/UsbRequest;
.super Ljava/lang/Object;
.source "UsbRequest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UsbRequest"


# instance fields
.field private mBuffer:Ljava/nio/ByteBuffer;

.field private mClientData:Ljava/lang/Object;

.field private mEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private mLength:I

.field private mNativeContext:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    return-void
.end method

.method private native native_cancel()Z
.end method

.method private native native_close()V
.end method

.method private native native_dequeue_array([BIZ)I
.end method

.method private native native_dequeue_direct()I
.end method

.method private native native_init(Landroid/hardware/usb/UsbDeviceConnection;IIII)Z
.end method

.method private native native_queue_array([BIZ)Z
.end method

.method private native native_queue_direct(Ljava/nio/ByteBuffer;IZ)Z
.end method


# virtual methods
.method public cancel()Z
    .registers 2

    #@0
    .prologue
    .line 174
    invoke-direct {p0}, Landroid/hardware/usb/UsbRequest;->native_cancel()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 71
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/hardware/usb/UsbRequest;->mEndpoint:Landroid/hardware/usb/UsbEndpoint;

    #@3
    .line 72
    invoke-direct {p0}, Landroid/hardware/usb/UsbRequest;->native_close()V

    #@6
    .line 73
    return-void
.end method

.method dequeue()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 154
    iget-object v3, p0, Landroid/hardware/usb/UsbRequest;->mEndpoint:Landroid/hardware/usb/UsbEndpoint;

    #@3
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_29

    #@9
    const/4 v1, 0x1

    #@a
    .line 156
    .local v1, out:Z
    :goto_a
    iget-object v3, p0, Landroid/hardware/usb/UsbRequest;->mBuffer:Ljava/nio/ByteBuffer;

    #@c
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->isDirect()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_2b

    #@12
    .line 157
    invoke-direct {p0}, Landroid/hardware/usb/UsbRequest;->native_dequeue_direct()I

    #@15
    move-result v0

    #@16
    .line 161
    .local v0, bytesRead:I
    :goto_16
    if-ltz v0, :cond_23

    #@18
    .line 162
    iget-object v3, p0, Landroid/hardware/usb/UsbRequest;->mBuffer:Ljava/nio/ByteBuffer;

    #@1a
    iget v4, p0, Landroid/hardware/usb/UsbRequest;->mLength:I

    #@1c
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    #@1f
    move-result v4

    #@20
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    #@23
    .line 164
    :cond_23
    const/4 v3, 0x0

    #@24
    iput-object v3, p0, Landroid/hardware/usb/UsbRequest;->mBuffer:Ljava/nio/ByteBuffer;

    #@26
    .line 165
    iput v2, p0, Landroid/hardware/usb/UsbRequest;->mLength:I

    #@28
    .line 166
    return-void

    #@29
    .end local v0           #bytesRead:I
    .end local v1           #out:Z
    :cond_29
    move v1, v2

    #@2a
    .line 154
    goto :goto_a

    #@2b
    .line 159
    .restart local v1       #out:Z
    :cond_2b
    iget-object v3, p0, Landroid/hardware/usb/UsbRequest;->mBuffer:Ljava/nio/ByteBuffer;

    #@2d
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    #@30
    move-result-object v3

    #@31
    iget v4, p0, Landroid/hardware/usb/UsbRequest;->mLength:I

    #@33
    invoke-direct {p0, v3, v4, v1}, Landroid/hardware/usb/UsbRequest;->native_dequeue_array([BIZ)I

    #@36
    move-result v0

    #@37
    .restart local v0       #bytesRead:I
    goto :goto_16
.end method

.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 78
    :try_start_0
    iget-object v0, p0, Landroid/hardware/usb/UsbRequest;->mEndpoint:Landroid/hardware/usb/UsbEndpoint;

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 79
    const-string v0, "UsbRequest"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "endpoint still open in finalize(): "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 80
    invoke-virtual {p0}, Landroid/hardware/usb/UsbRequest;->close()V
    :try_end_1f
    .catchall {:try_start_0 .. :try_end_1f} :catchall_23

    #@1f
    .line 83
    :cond_1f
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@22
    .line 85
    return-void

    #@23
    .line 83
    :catchall_23
    move-exception v0

    #@24
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@27
    throw v0
.end method

.method public getClientData()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Landroid/hardware/usb/UsbRequest;->mClientData:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getEndpoint()Landroid/hardware/usb/UsbEndpoint;
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/hardware/usb/UsbRequest;->mEndpoint:Landroid/hardware/usb/UsbEndpoint;

    #@2
    return-object v0
.end method

.method public initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z
    .registers 9
    .parameter "connection"
    .parameter "endpoint"

    #@0
    .prologue
    .line 62
    iput-object p2, p0, Landroid/hardware/usb/UsbRequest;->mEndpoint:Landroid/hardware/usb/UsbEndpoint;

    #@2
    .line 63
    invoke-virtual {p2}, Landroid/hardware/usb/UsbEndpoint;->getAddress()I

    #@5
    move-result v2

    #@6
    invoke-virtual {p2}, Landroid/hardware/usb/UsbEndpoint;->getAttributes()I

    #@9
    move-result v3

    #@a
    invoke-virtual {p2}, Landroid/hardware/usb/UsbEndpoint;->getMaxPacketSize()I

    #@d
    move-result v4

    #@e
    invoke-virtual {p2}, Landroid/hardware/usb/UsbEndpoint;->getInterval()I

    #@11
    move-result v5

    #@12
    move-object v0, p0

    #@13
    move-object v1, p1

    #@14
    invoke-direct/range {v0 .. v5}, Landroid/hardware/usb/UsbRequest;->native_init(Landroid/hardware/usb/UsbDeviceConnection;IIII)Z

    #@17
    move-result v0

    #@18
    return v0
.end method

.method public queue(Ljava/nio/ByteBuffer;I)Z
    .registers 7
    .parameter "buffer"
    .parameter "length"

    #@0
    .prologue
    .line 136
    iget-object v2, p0, Landroid/hardware/usb/UsbRequest;->mEndpoint:Landroid/hardware/usb/UsbEndpoint;

    #@2
    invoke-virtual {v2}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_1a

    #@8
    const/4 v0, 0x1

    #@9
    .line 138
    .local v0, out:Z
    :goto_9
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1c

    #@f
    .line 139
    invoke-direct {p0, p1, p2, v0}, Landroid/hardware/usb/UsbRequest;->native_queue_direct(Ljava/nio/ByteBuffer;IZ)Z

    #@12
    move-result v1

    #@13
    .line 145
    .local v1, result:Z
    :goto_13
    if-eqz v1, :cond_19

    #@15
    .line 147
    iput-object p1, p0, Landroid/hardware/usb/UsbRequest;->mBuffer:Ljava/nio/ByteBuffer;

    #@17
    .line 148
    iput p2, p0, Landroid/hardware/usb/UsbRequest;->mLength:I

    #@19
    .line 150
    :cond_19
    return v1

    #@1a
    .line 136
    .end local v0           #out:Z
    .end local v1           #result:Z
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_9

    #@1c
    .line 140
    .restart local v0       #out:Z
    :cond_1c
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_2b

    #@22
    .line 141
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@25
    move-result-object v2

    #@26
    invoke-direct {p0, v2, p2, v0}, Landroid/hardware/usb/UsbRequest;->native_queue_array([BIZ)Z

    #@29
    move-result v1

    #@2a
    .restart local v1       #result:Z
    goto :goto_13

    #@2b
    .line 143
    .end local v1           #result:Z
    :cond_2b
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@2d
    const-string v3, "buffer is not direct and has no array"

    #@2f
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v2
.end method

.method public setClientData(Ljava/lang/Object;)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 119
    iput-object p1, p0, Landroid/hardware/usb/UsbRequest;->mClientData:Ljava/lang/Object;

    #@2
    .line 120
    return-void
.end method
