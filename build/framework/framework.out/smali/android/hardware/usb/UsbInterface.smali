.class public Landroid/hardware/usb/UsbInterface;
.super Ljava/lang/Object;
.source "UsbInterface.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/hardware/usb/UsbInterface;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mClass:I

.field private final mEndpoints:[Landroid/os/Parcelable;

.field private final mId:I

.field private final mProtocol:I

.field private final mSubclass:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 120
    new-instance v0, Landroid/hardware/usb/UsbInterface$1;

    #@2
    invoke-direct {v0}, Landroid/hardware/usb/UsbInterface$1;-><init>()V

    #@5
    sput-object v0, Landroid/hardware/usb/UsbInterface;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(IIII[Landroid/os/Parcelable;)V
    .registers 6
    .parameter "id"
    .parameter "Class"
    .parameter "subClass"
    .parameter "protocol"
    .parameter "endpoints"

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    iput p1, p0, Landroid/hardware/usb/UsbInterface;->mId:I

    #@5
    .line 51
    iput p2, p0, Landroid/hardware/usb/UsbInterface;->mClass:I

    #@7
    .line 52
    iput p3, p0, Landroid/hardware/usb/UsbInterface;->mSubclass:I

    #@9
    .line 53
    iput p4, p0, Landroid/hardware/usb/UsbInterface;->mProtocol:I

    #@b
    .line 54
    iput-object p5, p0, Landroid/hardware/usb/UsbInterface;->mEndpoints:[Landroid/os/Parcelable;

    #@d
    .line 55
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 137
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Landroid/hardware/usb/UsbInterface;->mEndpoints:[Landroid/os/Parcelable;

    #@2
    aget-object v0, v0, p1

    #@4
    check-cast v0, Landroid/hardware/usb/UsbEndpoint;

    #@6
    return-object v0
.end method

.method public getEndpointCount()I
    .registers 2

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Landroid/hardware/usb/UsbInterface;->mEndpoints:[Landroid/os/Parcelable;

    #@2
    array-length v0, v0

    #@3
    return v0
.end method

.method public getId()I
    .registers 2

    #@0
    .prologue
    .line 64
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mId:I

    #@2
    return v0
.end method

.method public getInterfaceClass()I
    .registers 2

    #@0
    .prologue
    .line 74
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mClass:I

    #@2
    return v0
.end method

.method public getInterfaceProtocol()I
    .registers 2

    #@0
    .prologue
    .line 92
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mProtocol:I

    #@2
    return v0
.end method

.method public getInterfaceSubclass()I
    .registers 2

    #@0
    .prologue
    .line 83
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mSubclass:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "UsbInterface[mId="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/hardware/usb/UsbInterface;->mId:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ",mClass="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/hardware/usb/UsbInterface;->mClass:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ",mSubclass="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/hardware/usb/UsbInterface;->mSubclass:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ",mProtocol="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/hardware/usb/UsbInterface;->mProtocol:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ",mEndpoints="

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget-object v1, p0, Landroid/hardware/usb/UsbInterface;->mEndpoints:[Landroid/os/Parcelable;

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, "]"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 141
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mId:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 142
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mClass:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 143
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mSubclass:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 144
    iget v0, p0, Landroid/hardware/usb/UsbInterface;->mProtocol:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 145
    iget-object v0, p0, Landroid/hardware/usb/UsbInterface;->mEndpoints:[Landroid/os/Parcelable;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    #@1a
    .line 146
    return-void
.end method
