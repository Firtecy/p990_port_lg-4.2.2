.class public Landroid/hardware/usb/UsbManager;
.super Ljava/lang/Object;
.source "UsbManager.java"


# static fields
.field public static final ACTION_USB_ACCESSORY_ATTACHED:Ljava/lang/String; = "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"

.field public static final ACTION_USB_ACCESSORY_DETACHED:Ljava/lang/String; = "android.hardware.usb.action.USB_ACCESSORY_DETACHED"

.field public static final ACTION_USB_DEVICE_ATTACHED:Ljava/lang/String; = "android.hardware.usb.action.USB_DEVICE_ATTACHED"

.field public static final ACTION_USB_DEVICE_DETACHED:Ljava/lang/String; = "android.hardware.usb.action.USB_DEVICE_DETACHED"

.field public static final ACTION_USB_STATE:Ljava/lang/String; = "android.hardware.usb.action.USB_STATE"

.field public static final EXTRA_ACCESSORY:Ljava/lang/String; = "accessory"

.field public static final EXTRA_DEVICE:Ljava/lang/String; = "device"

.field public static final EXTRA_PERMISSION_GRANTED:Ljava/lang/String; = "permission"

.field private static final TAG:Ljava/lang/String; = "UsbManager"

.field public static final USB_CONFIGURED:Ljava/lang/String; = "configured"

.field public static final USB_CONNECTED:Ljava/lang/String; = "connected"

.field public static final USB_FUNCTION_ACCESSORY:Ljava/lang/String; = "accessory"

.field public static final USB_FUNCTION_ADB:Ljava/lang/String; = "adb"

.field public static final USB_FUNCTION_AUDIO_SOURCE:Ljava/lang/String; = "audio_source"

.field public static final USB_FUNCTION_CDROM_STORAGE:Ljava/lang/String; = "cdrom_storage"

.field public static final USB_FUNCTION_CHARGE_ONLY:Ljava/lang/String; = "charge_only"

.field public static final USB_FUNCTION_MASS_STORAGE:Ljava/lang/String; = "mass_storage"

.field public static final USB_FUNCTION_MTP:Ljava/lang/String; = "mtp"

.field public static final USB_FUNCTION_MTP_ONLY:Ljava/lang/String; = "mtp_only"

.field public static final USB_FUNCTION_NCM:Ljava/lang/String; = "ncm"

.field public static final USB_FUNCTION_PC_SUITE:Ljava/lang/String; = "pc_suite"

.field public static final USB_FUNCTION_PTP:Ljava/lang/String; = "ptp"

.field public static final USB_FUNCTION_PTP_ONLY:Ljava/lang/String; = "ptp_only"

.field public static final USB_FUNCTION_RNDIS:Ljava/lang/String; = "ecm"

.field public static final USB_FUNCTION_TETHER:Ljava/lang/String; = "ecm"

.field public static final USB_FUNCTION_USB_ENABLE_DIAG:Ljava/lang/String; = "usb_enable_diag"

.field public static final USB_FUNCTION_USB_ENABLE_ECM:Ljava/lang/String; = "usb_enable_ecm"

.field public static final USB_FUNCTION_USB_ENABLE_MTP:Ljava/lang/String; = "usb_enable_mtp"

.field public static final USB_FUNCTION_USB_ENABLE_TETHER:Ljava/lang/String; = "ecm,diag"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mService:Landroid/hardware/usb/IUsbManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/hardware/usb/IUsbManager;)V
    .registers 3
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    .line 318
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 319
    iput-object p1, p0, Landroid/hardware/usb/UsbManager;->mContext:Landroid/content/Context;

    #@5
    .line 320
    iput-object p2, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@7
    .line 321
    return-void
.end method

.method private static propertyContainsFunction(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .parameter "property"
    .parameter "function"

    #@0
    .prologue
    const/16 v5, 0x2c

    #@2
    const/4 v3, 0x0

    #@3
    .line 495
    const-string v4, ""

    #@5
    invoke-static {p0, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 496
    .local v1, functions:Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@c
    move-result v2

    #@d
    .line 497
    .local v2, index:I
    if-gez v2, :cond_10

    #@f
    .line 501
    :cond_f
    :goto_f
    return v3

    #@10
    .line 498
    :cond_10
    if-lez v2, :cond_1a

    #@12
    add-int/lit8 v4, v2, -0x1

    #@14
    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v4

    #@18
    if-ne v4, v5, :cond_f

    #@1a
    .line 499
    :cond_1a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1d
    move-result v4

    #@1e
    add-int v0, v2, v4

    #@20
    .line 500
    .local v0, charAfter:I
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@23
    move-result v4

    #@24
    if-ge v0, v4, :cond_2c

    #@26
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    #@29
    move-result v4

    #@2a
    if-ne v4, v5, :cond_f

    #@2c
    .line 501
    :cond_2c
    const/4 v3, 0x1

    #@2d
    goto :goto_f
.end method


# virtual methods
.method public changeCurrentFunction(Ljava/lang/String;)V
    .registers 5
    .parameter "function"

    #@0
    .prologue
    .line 560
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    invoke-interface {v1, p1}, Landroid/hardware/usb/IUsbManager;->changeCurrentFunction(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 564
    :goto_5
    return-void

    #@6
    .line 561
    :catch_6
    move-exception v0

    #@7
    .line 562
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@9
    const-string v2, "RemoteException in changeCurrentFunction"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public getAccessoryList()[Landroid/hardware/usb/UsbAccessory;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 379
    :try_start_1
    iget-object v3, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@3
    invoke-interface {v3}, Landroid/hardware/usb/IUsbManager;->getCurrentAccessory()Landroid/hardware/usb/UsbAccessory;

    #@6
    move-result-object v0

    #@7
    .line 380
    .local v0, accessory:Landroid/hardware/usb/UsbAccessory;
    if-nez v0, :cond_a

    #@9
    .line 387
    .end local v0           #accessory:Landroid/hardware/usb/UsbAccessory;
    :goto_9
    return-object v2

    #@a
    .line 383
    .restart local v0       #accessory:Landroid/hardware/usb/UsbAccessory;
    :cond_a
    const/4 v3, 0x1

    #@b
    new-array v3, v3, [Landroid/hardware/usb/UsbAccessory;

    #@d
    const/4 v4, 0x0

    #@e
    aput-object v0, v3, v4
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_12

    #@10
    move-object v2, v3

    #@11
    goto :goto_9

    #@12
    .line 385
    .end local v0           #accessory:Landroid/hardware/usb/UsbAccessory;
    :catch_12
    move-exception v1

    #@13
    .line 386
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "UsbManager"

    #@15
    const-string v4, "RemoteException in getAccessoryList"

    #@17
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    goto :goto_9
.end method

.method public getDefaultFunction()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 524
    const-string/jumbo v2, "persist.sys.usb.config"

    #@3
    const-string v3, ""

    #@5
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 525
    .local v1, functions:Ljava/lang/String;
    const/16 v2, 0x2c

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    #@e
    move-result v0

    #@f
    .line 526
    .local v0, commaIndex:I
    if-lez v0, :cond_16

    #@11
    .line 527
    const/4 v2, 0x0

    #@12
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 529
    .end local v1           #functions:Ljava/lang/String;
    :cond_16
    return-object v1
.end method

.method public getDeviceList()Ljava/util/HashMap;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 332
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 334
    .local v0, bundle:Landroid/os/Bundle;
    :try_start_5
    iget-object v5, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@7
    invoke-interface {v5, v0}, Landroid/hardware/usb/IUsbManager;->getDeviceList(Landroid/os/Bundle;)V

    #@a
    .line 335
    new-instance v4, Ljava/util/HashMap;

    #@c
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@f
    .line 336
    .local v4, result:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@12
    move-result-object v5

    #@13
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v2

    #@17
    .local v2, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v5

    #@1b
    if-eqz v5, :cond_36

    #@1d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Ljava/lang/String;

    #@23
    .line 337
    .local v3, name:Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v5

    #@27
    check-cast v5, Landroid/hardware/usb/UsbDevice;

    #@29
    invoke-virtual {v4, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_17

    #@2d
    .line 340
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #result:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :catch_2d
    move-exception v1

    #@2e
    .line 341
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "UsbManager"

    #@30
    const-string v6, "RemoteException in getDeviceList"

    #@32
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    .line 342
    const/4 v4, 0x0

    #@36
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_36
    return-object v4
.end method

.method public hasPermission(Landroid/hardware/usb/UsbAccessory;)Z
    .registers 5
    .parameter "accessory"

    #@0
    .prologue
    .line 435
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    invoke-interface {v1, p1}, Landroid/hardware/usb/IUsbManager;->hasAccessoryPermission(Landroid/hardware/usb/UsbAccessory;)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 438
    :goto_6
    return v1

    #@7
    .line 436
    :catch_7
    move-exception v0

    #@8
    .line 437
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@a
    const-string v2, "RemoteException in hasPermission"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 438
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public hasPermission(Landroid/hardware/usb/UsbDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 417
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    invoke-interface {v1, p1}, Landroid/hardware/usb/IUsbManager;->hasDevicePermission(Landroid/hardware/usb/UsbDevice;)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 420
    :goto_6
    return v1

    #@7
    .line 418
    :catch_7
    move-exception v0

    #@8
    .line 419
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@a
    const-string v2, "RemoteException in hasPermission"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 420
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public isFunctionEnabled(Ljava/lang/String;)Z
    .registers 3
    .parameter "function"

    #@0
    .prologue
    .line 513
    const-string/jumbo v0, "sys.usb.config"

    #@3
    invoke-static {v0, p1}, Landroid/hardware/usb/UsbManager;->propertyContainsFunction(Ljava/lang/String;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;
    .registers 5
    .parameter "accessory"

    #@0
    .prologue
    .line 399
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    invoke-interface {v1, p1}, Landroid/hardware/usb/IUsbManager;->openAccessory(Landroid/hardware/usb/UsbAccessory;)Landroid/os/ParcelFileDescriptor;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 402
    :goto_6
    return-object v1

    #@7
    .line 400
    :catch_7
    move-exception v0

    #@8
    .line 401
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@a
    const-string v2, "RemoteException in openAccessory"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 402
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
    .registers 9
    .parameter "device"

    #@0
    .prologue
    .line 355
    :try_start_0
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 356
    .local v1, deviceName:Ljava/lang/String;
    iget-object v5, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@6
    invoke-interface {v5, v1}, Landroid/hardware/usb/IUsbManager;->openDevice(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@9
    move-result-object v3

    #@a
    .line 357
    .local v3, pfd:Landroid/os/ParcelFileDescriptor;
    if-eqz v3, :cond_23

    #@c
    .line 358
    new-instance v0, Landroid/hardware/usb/UsbDeviceConnection;

    #@e
    invoke-direct {v0, p1}, Landroid/hardware/usb/UsbDeviceConnection;-><init>(Landroid/hardware/usb/UsbDevice;)V

    #@11
    .line 359
    .local v0, connection:Landroid/hardware/usb/UsbDeviceConnection;
    invoke-virtual {v0, v1, v3}, Landroid/hardware/usb/UsbDeviceConnection;->open(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)Z

    #@14
    move-result v4

    #@15
    .line 360
    .local v4, result:Z
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_18} :catch_1b

    #@18
    .line 361
    if-eqz v4, :cond_23

    #@1a
    .line 368
    .end local v0           #connection:Landroid/hardware/usb/UsbDeviceConnection;
    .end local v1           #deviceName:Ljava/lang/String;
    .end local v3           #pfd:Landroid/os/ParcelFileDescriptor;
    .end local v4           #result:Z
    :goto_1a
    return-object v0

    #@1b
    .line 365
    :catch_1b
    move-exception v2

    #@1c
    .line 366
    .local v2, e:Ljava/lang/Exception;
    const-string v5, "UsbManager"

    #@1e
    const-string v6, "exception in UsbManager.openDevice"

    #@20
    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    .line 368
    .end local v2           #e:Ljava/lang/Exception;
    :cond_23
    const/4 v0, 0x0

    #@24
    goto :goto_1a
.end method

.method public requestPermission(Landroid/hardware/usb/UsbAccessory;Landroid/app/PendingIntent;)V
    .registers 6
    .parameter "accessory"
    .parameter "pi"

    #@0
    .prologue
    .line 488
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    iget-object v2, p0, Landroid/hardware/usb/UsbManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-interface {v1, p1, v2, p2}, Landroid/hardware/usb/IUsbManager;->requestAccessoryPermission(Landroid/hardware/usb/UsbAccessory;Ljava/lang/String;Landroid/app/PendingIntent;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 492
    :goto_b
    return-void

    #@c
    .line 489
    :catch_c
    move-exception v0

    #@d
    .line 490
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@f
    const-string v2, "RemoteException in requestPermission"

    #@11
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b
.end method

.method public requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V
    .registers 6
    .parameter "device"
    .parameter "pi"

    #@0
    .prologue
    .line 462
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    iget-object v2, p0, Landroid/hardware/usb/UsbManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-interface {v1, p1, v2, p2}, Landroid/hardware/usb/IUsbManager;->requestDevicePermission(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;Landroid/app/PendingIntent;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 466
    :goto_b
    return-void

    #@c
    .line 463
    :catch_c
    move-exception v0

    #@d
    .line 464
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@f
    const-string v2, "RemoteException in requestPermission"

    #@11
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_b
.end method

.method public setCurrentFunction(Ljava/lang/String;Z)V
    .registers 6
    .parameter "function"
    .parameter "makeDefault"

    #@0
    .prologue
    .line 544
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/hardware/usb/IUsbManager;->setCurrentFunction(Ljava/lang/String;Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 548
    :goto_5
    return-void

    #@6
    .line 545
    :catch_6
    move-exception v0

    #@7
    .line 546
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@9
    const-string v2, "RemoteException in setCurrentFunction"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public setMassStorageBackingFile(Ljava/lang/String;)V
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 576
    :try_start_0
    iget-object v1, p0, Landroid/hardware/usb/UsbManager;->mService:Landroid/hardware/usb/IUsbManager;

    #@2
    invoke-interface {v1, p1}, Landroid/hardware/usb/IUsbManager;->setMassStorageBackingFile(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 580
    :goto_5
    return-void

    #@6
    .line 577
    :catch_6
    move-exception v0

    #@7
    .line 578
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UsbManager"

    #@9
    const-string v2, "RemoteException in setDefaultFunction"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method
