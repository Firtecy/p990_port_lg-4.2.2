.class Landroid/hardware/SystemSensorManager$ListenerDelegate$1;
.super Landroid/os/Handler;
.source "SystemSensorManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/hardware/SystemSensorManager$ListenerDelegate;-><init>(Landroid/hardware/SystemSensorManager;Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

.field final synthetic val$this$0:Landroid/hardware/SystemSensorManager;


# direct methods
.method constructor <init>(Landroid/hardware/SystemSensorManager$ListenerDelegate;Landroid/os/Looper;Landroid/hardware/SystemSensorManager;)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter

    #@0
    .prologue
    .line 177
    iput-object p1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@2
    iput-object p3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->val$this$0:Landroid/hardware/SystemSensorManager;

    #@4
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 180
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v2, Landroid/hardware/SensorEvent;

    #@4
    .line 181
    .local v2, t:Landroid/hardware/SensorEvent;
    iget-object v3, v2, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@6
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getHandle()I

    #@9
    move-result v1

    #@a
    .line 183
    .local v1, handle:I
    iget-object v3, v2, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@c
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    #@f
    move-result v3

    #@10
    packed-switch v3, :pswitch_data_68

    #@13
    .line 196
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@15
    iget-object v3, v3, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mFirstEvent:Landroid/util/SparseBooleanArray;

    #@17
    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@1a
    move-result v3

    #@1b
    if-nez v3, :cond_31

    #@1d
    .line 197
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@1f
    iget-object v3, v3, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mFirstEvent:Landroid/util/SparseBooleanArray;

    #@21
    const/4 v4, 0x1

    #@22
    invoke-virtual {v3, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@25
    .line 198
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@27
    invoke-static {v3}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->access$100(Landroid/hardware/SystemSensorManager$ListenerDelegate;)Landroid/hardware/SensorEventListener;

    #@2a
    move-result-object v3

    #@2b
    iget-object v4, v2, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@2d
    const/4 v5, 0x3

    #@2e
    invoke-interface {v3, v4, v5}, Landroid/hardware/SensorEventListener;->onAccuracyChanged(Landroid/hardware/Sensor;I)V

    #@31
    .line 204
    :cond_31
    :goto_31
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@33
    invoke-static {v3}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->access$100(Landroid/hardware/SystemSensorManager$ListenerDelegate;)Landroid/hardware/SensorEventListener;

    #@36
    move-result-object v3

    #@37
    invoke-interface {v3, v2}, Landroid/hardware/SensorEventListener;->onSensorChanged(Landroid/hardware/SensorEvent;)V

    #@3a
    .line 205
    sget-object v3, Landroid/hardware/SystemSensorManager;->sPool:Landroid/hardware/SensorManager$SensorEventPool;

    #@3c
    invoke-virtual {v3, v2}, Landroid/hardware/SensorManager$SensorEventPool;->returnToPool(Landroid/hardware/SensorEvent;)V

    #@3f
    .line 206
    return-void

    #@40
    .line 188
    :pswitch_40
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@42
    iget-object v3, v3, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorAccuracies:Landroid/util/SparseIntArray;

    #@44
    invoke-virtual {v3, v1}, Landroid/util/SparseIntArray;->get(I)I

    #@47
    move-result v0

    #@48
    .line 189
    .local v0, accuracy:I
    iget v3, v2, Landroid/hardware/SensorEvent;->accuracy:I

    #@4a
    if-ltz v3, :cond_31

    #@4c
    iget v3, v2, Landroid/hardware/SensorEvent;->accuracy:I

    #@4e
    if-eq v0, v3, :cond_31

    #@50
    .line 190
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@52
    iget-object v3, v3, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorAccuracies:Landroid/util/SparseIntArray;

    #@54
    iget v4, v2, Landroid/hardware/SensorEvent;->accuracy:I

    #@56
    invoke-virtual {v3, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    #@59
    .line 191
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;->this$1:Landroid/hardware/SystemSensorManager$ListenerDelegate;

    #@5b
    invoke-static {v3}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->access$100(Landroid/hardware/SystemSensorManager$ListenerDelegate;)Landroid/hardware/SensorEventListener;

    #@5e
    move-result-object v3

    #@5f
    iget-object v4, v2, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@61
    iget v5, v2, Landroid/hardware/SensorEvent;->accuracy:I

    #@63
    invoke-interface {v3, v4, v5}, Landroid/hardware/SensorEventListener;->onAccuracyChanged(Landroid/hardware/Sensor;I)V

    #@66
    goto :goto_31

    #@67
    .line 183
    nop

    #@68
    :pswitch_data_68
    .packed-switch 0x2
        :pswitch_40
        :pswitch_40
    .end packed-switch
.end method
