.class Landroid/hardware/GeomagneticField$LegendreTable;
.super Ljava/lang/Object;
.source "GeomagneticField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/GeomagneticField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LegendreTable"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final mP:[[F

.field public final mPDeriv:[[F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 335
    const-class v0, Landroid/hardware/GeomagneticField;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/hardware/GeomagneticField$LegendreTable;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(IF)V
    .registers 13
    .parameter "maxN"
    .parameter "thetaRad"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 351
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 356
    float-to-double v5, p2

    #@6
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    #@9
    move-result-wide v5

    #@a
    double-to-float v0, v5

    #@b
    .line 357
    .local v0, cos:F
    float-to-double v5, p2

    #@c
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    #@f
    move-result-wide v5

    #@10
    double-to-float v4, v5

    #@11
    .line 359
    .local v4, sin:F
    add-int/lit8 v5, p1, 0x1

    #@13
    new-array v5, v5, [[F

    #@15
    iput-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@17
    .line 360
    add-int/lit8 v5, p1, 0x1

    #@19
    new-array v5, v5, [[F

    #@1b
    iput-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@1d
    .line 361
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@1f
    new-array v6, v9, [F

    #@21
    const/high16 v7, 0x3f80

    #@23
    aput v7, v6, v8

    #@25
    aput-object v6, v5, v8

    #@27
    .line 362
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@29
    new-array v6, v9, [F

    #@2b
    const/4 v7, 0x0

    #@2c
    aput v7, v6, v8

    #@2e
    aput-object v6, v5, v8

    #@30
    .line 363
    const/4 v3, 0x1

    #@31
    .local v3, n:I
    :goto_31
    if-gt v3, p1, :cond_111

    #@33
    .line 364
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@35
    add-int/lit8 v6, v3, 0x1

    #@37
    new-array v6, v6, [F

    #@39
    aput-object v6, v5, v3

    #@3b
    .line 365
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@3d
    add-int/lit8 v6, v3, 0x1

    #@3f
    new-array v6, v6, [F

    #@41
    aput-object v6, v5, v3

    #@43
    .line 366
    const/4 v2, 0x0

    #@44
    .local v2, m:I
    :goto_44
    if-gt v2, v3, :cond_10d

    #@46
    .line 367
    if-ne v3, v2, :cond_79

    #@48
    .line 368
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@4a
    aget-object v5, v5, v3

    #@4c
    iget-object v6, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@4e
    add-int/lit8 v7, v3, -0x1

    #@50
    aget-object v6, v6, v7

    #@52
    add-int/lit8 v7, v2, -0x1

    #@54
    aget v6, v6, v7

    #@56
    mul-float/2addr v6, v4

    #@57
    aput v6, v5, v2

    #@59
    .line 369
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@5b
    aget-object v5, v5, v3

    #@5d
    iget-object v6, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@5f
    add-int/lit8 v7, v3, -0x1

    #@61
    aget-object v6, v6, v7

    #@63
    add-int/lit8 v7, v2, -0x1

    #@65
    aget v6, v6, v7

    #@67
    mul-float/2addr v6, v0

    #@68
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@6a
    add-int/lit8 v8, v3, -0x1

    #@6c
    aget-object v7, v7, v8

    #@6e
    add-int/lit8 v8, v2, -0x1

    #@70
    aget v7, v7, v8

    #@72
    mul-float/2addr v7, v4

    #@73
    add-float/2addr v6, v7

    #@74
    aput v6, v5, v2

    #@76
    .line 366
    :goto_76
    add-int/lit8 v2, v2, 0x1

    #@78
    goto :goto_44

    #@79
    .line 371
    :cond_79
    if-eq v3, v9, :cond_7f

    #@7b
    add-int/lit8 v5, v3, -0x1

    #@7d
    if-ne v2, v5, :cond_a9

    #@7f
    .line 372
    :cond_7f
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@81
    aget-object v5, v5, v3

    #@83
    iget-object v6, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@85
    add-int/lit8 v7, v3, -0x1

    #@87
    aget-object v6, v6, v7

    #@89
    aget v6, v6, v2

    #@8b
    mul-float/2addr v6, v0

    #@8c
    aput v6, v5, v2

    #@8e
    .line 373
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@90
    aget-object v5, v5, v3

    #@92
    neg-float v6, v4

    #@93
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@95
    add-int/lit8 v8, v3, -0x1

    #@97
    aget-object v7, v7, v8

    #@99
    aget v7, v7, v2

    #@9b
    mul-float/2addr v6, v7

    #@9c
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@9e
    add-int/lit8 v8, v3, -0x1

    #@a0
    aget-object v7, v7, v8

    #@a2
    aget v7, v7, v2

    #@a4
    mul-float/2addr v7, v0

    #@a5
    add-float/2addr v6, v7

    #@a6
    aput v6, v5, v2

    #@a8
    goto :goto_76

    #@a9
    .line 376
    :cond_a9
    sget-boolean v5, Landroid/hardware/GeomagneticField$LegendreTable;->$assertionsDisabled:Z

    #@ab
    if-nez v5, :cond_b9

    #@ad
    if-le v3, v9, :cond_b3

    #@af
    add-int/lit8 v5, v3, -0x1

    #@b1
    if-lt v2, v5, :cond_b9

    #@b3
    :cond_b3
    new-instance v5, Ljava/lang/AssertionError;

    #@b5
    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    #@b8
    throw v5

    #@b9
    .line 377
    :cond_b9
    add-int/lit8 v5, v3, -0x1

    #@bb
    add-int/lit8 v6, v3, -0x1

    #@bd
    mul-int/2addr v5, v6

    #@be
    mul-int v6, v2, v2

    #@c0
    sub-int/2addr v5, v6

    #@c1
    int-to-float v5, v5

    #@c2
    mul-int/lit8 v6, v3, 0x2

    #@c4
    add-int/lit8 v6, v6, -0x1

    #@c6
    mul-int/lit8 v7, v3, 0x2

    #@c8
    add-int/lit8 v7, v7, -0x3

    #@ca
    mul-int/2addr v6, v7

    #@cb
    int-to-float v6, v6

    #@cc
    div-float v1, v5, v6

    #@ce
    .line 379
    .local v1, k:F
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@d0
    aget-object v5, v5, v3

    #@d2
    iget-object v6, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@d4
    add-int/lit8 v7, v3, -0x1

    #@d6
    aget-object v6, v6, v7

    #@d8
    aget v6, v6, v2

    #@da
    mul-float/2addr v6, v0

    #@db
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@dd
    add-int/lit8 v8, v3, -0x2

    #@df
    aget-object v7, v7, v8

    #@e1
    aget v7, v7, v2

    #@e3
    mul-float/2addr v7, v1

    #@e4
    sub-float/2addr v6, v7

    #@e5
    aput v6, v5, v2

    #@e7
    .line 380
    iget-object v5, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@e9
    aget-object v5, v5, v3

    #@eb
    neg-float v6, v4

    #@ec
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mP:[[F

    #@ee
    add-int/lit8 v8, v3, -0x1

    #@f0
    aget-object v7, v7, v8

    #@f2
    aget v7, v7, v2

    #@f4
    mul-float/2addr v6, v7

    #@f5
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@f7
    add-int/lit8 v8, v3, -0x1

    #@f9
    aget-object v7, v7, v8

    #@fb
    aget v7, v7, v2

    #@fd
    mul-float/2addr v7, v0

    #@fe
    add-float/2addr v6, v7

    #@ff
    iget-object v7, p0, Landroid/hardware/GeomagneticField$LegendreTable;->mPDeriv:[[F

    #@101
    add-int/lit8 v8, v3, -0x2

    #@103
    aget-object v7, v7, v8

    #@105
    aget v7, v7, v2

    #@107
    mul-float/2addr v7, v1

    #@108
    sub-float/2addr v6, v7

    #@109
    aput v6, v5, v2

    #@10b
    goto/16 :goto_76

    #@10d
    .line 363
    .end local v1           #k:F
    :cond_10d
    add-int/lit8 v3, v3, 0x1

    #@10f
    goto/16 :goto_31

    #@111
    .line 385
    .end local v2           #m:I
    :cond_111
    return-void
.end method
