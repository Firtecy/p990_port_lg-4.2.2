.class public Landroid/hardware/Camera;
.super Ljava/lang/Object;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/Camera$1;,
        Landroid/hardware/Camera$Parameters;,
        Landroid/hardware/Camera$Area;,
        Landroid/hardware/Camera$Size;,
        Landroid/hardware/Camera$Coordinate;,
        Landroid/hardware/Camera$OisData;,
        Landroid/hardware/Camera$OisDataListener;,
        Landroid/hardware/Camera$CameraMetaDataCallback;,
        Landroid/hardware/Camera$CameraDataCallback;,
        Landroid/hardware/Camera$ErrorCallback;,
        Landroid/hardware/Camera$Face;,
        Landroid/hardware/Camera$FaceDetectionListener;,
        Landroid/hardware/Camera$OnZoomChangeListener;,
        Landroid/hardware/Camera$PictureCallback;,
        Landroid/hardware/Camera$ShutterCallback;,
        Landroid/hardware/Camera$AutoFocusMoveCallback;,
        Landroid/hardware/Camera$AutoFocusCallback;,
        Landroid/hardware/Camera$EventHandler;,
        Landroid/hardware/Camera$PreviewCallback;,
        Landroid/hardware/Camera$CameraInfo;
    }
.end annotation


# static fields
.field public static final ACTION_NEW_PICTURE:Ljava/lang/String; = "android.hardware.action.NEW_PICTURE"

.field public static final ACTION_NEW_VIDEO:Ljava/lang/String; = "android.hardware.action.NEW_VIDEO"

.field public static final CAMERA_ERROR_SERVER_DIED:I = 0x64

.field public static final CAMERA_ERROR_UNKNOWN:I = 0x1

.field private static final CAMERA_FACE_DETECTION_HW:I = 0x0

.field private static final CAMERA_FACE_DETECTION_SW:I = 0x1

.field private static final CAMERA_MSG_COMPRESSED_IMAGE:I = 0x100

.field private static final CAMERA_MSG_ERROR:I = 0x1

.field private static final CAMERA_MSG_FOCUS:I = 0x4

.field private static final CAMERA_MSG_FOCUS_MOVE:I = 0x800

.field private static final CAMERA_MSG_META_DATA:I = 0x2000

.field private static final CAMERA_MSG_OIS_DATA:I = 0x4000

.field private static final CAMERA_MSG_POSTVIEW_FRAME:I = 0x40

.field private static final CAMERA_MSG_PREVIEW_FRAME:I = 0x10

.field private static final CAMERA_MSG_PREVIEW_METADATA:I = 0x400

.field private static final CAMERA_MSG_RAW_IMAGE:I = 0x80

.field private static final CAMERA_MSG_RAW_IMAGE_NOTIFY:I = 0x200

.field private static final CAMERA_MSG_SHUTTER:I = 0x2

.field private static final CAMERA_MSG_STATS_DATA:I = 0x1000

.field private static final CAMERA_MSG_VIDEO_FRAME:I = 0x20

.field private static final CAMERA_MSG_ZOOM:I = 0x8

.field private static final TAG:Ljava/lang/String; = "Camera"


# instance fields
.field private mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field private mAutoFocusCallbackLock:Ljava/lang/Object;

.field private mAutoFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

.field private mCameraDataCallback:Landroid/hardware/Camera$CameraDataCallback;

.field private mCameraMetaDataCallback:Landroid/hardware/Camera$CameraMetaDataCallback;

.field private mErrorCallback:Landroid/hardware/Camera$ErrorCallback;

.field private mEventHandler:Landroid/hardware/Camera$EventHandler;

.field private mFaceDetectionRunning:Z

.field private mFaceListener:Landroid/hardware/Camera$FaceDetectionListener;

.field private mJpegCallback:Landroid/hardware/Camera$PictureCallback;

.field private mNativeContext:I

.field private mOisDataListener:Landroid/hardware/Camera$OisDataListener;

.field private mOisDataRunning:Z

.field private mOneShot:Z

.field private mPostviewCallback:Landroid/hardware/Camera$PictureCallback;

.field private mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

.field private mRawImageCallback:Landroid/hardware/Camera$PictureCallback;

.field private mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

.field private mWithBuffer:Z

.field private mZoomListener:Landroid/hardware/Camera$OnZoomChangeListener;


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 379
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 167
    iput-boolean v1, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@6
    .line 168
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Landroid/hardware/Camera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    #@d
    .line 175
    iput-boolean v1, p0, Landroid/hardware/Camera;->mOisDataRunning:Z

    #@f
    .line 380
    return-void
.end method

.method constructor <init>(I)V
    .registers 6
    .parameter "cameraId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 347
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 167
    iput-boolean v2, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@7
    .line 168
    new-instance v1, Ljava/lang/Object;

    #@9
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@c
    iput-object v1, p0, Landroid/hardware/Camera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    #@e
    .line 175
    iput-boolean v2, p0, Landroid/hardware/Camera;->mOisDataRunning:Z

    #@10
    .line 349
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@13
    move-result-object v1

    #@14
    if-eqz v1, :cond_1e

    #@16
    .line 350
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@19
    move-result-object v1

    #@1a
    const/4 v2, 0x1

    #@1b
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->setCameraState(Z)V

    #@1e
    .line 352
    :cond_1e
    iput-object v3, p0, Landroid/hardware/Camera;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    #@20
    .line 353
    iput-object v3, p0, Landroid/hardware/Camera;->mRawImageCallback:Landroid/hardware/Camera$PictureCallback;

    #@22
    .line 354
    iput-object v3, p0, Landroid/hardware/Camera;->mJpegCallback:Landroid/hardware/Camera$PictureCallback;

    #@24
    .line 355
    iput-object v3, p0, Landroid/hardware/Camera;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    #@26
    .line 356
    iput-object v3, p0, Landroid/hardware/Camera;->mPostviewCallback:Landroid/hardware/Camera$PictureCallback;

    #@28
    .line 357
    iput-object v3, p0, Landroid/hardware/Camera;->mZoomListener:Landroid/hardware/Camera$OnZoomChangeListener;

    #@2a
    .line 359
    iput-object v3, p0, Landroid/hardware/Camera;->mCameraDataCallback:Landroid/hardware/Camera$CameraDataCallback;

    #@2c
    .line 360
    iput-object v3, p0, Landroid/hardware/Camera;->mCameraMetaDataCallback:Landroid/hardware/Camera$CameraMetaDataCallback;

    #@2e
    .line 362
    iput-object v3, p0, Landroid/hardware/Camera;->mOisDataListener:Landroid/hardware/Camera$OisDataListener;

    #@30
    .line 365
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@33
    move-result-object v0

    #@34
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_46

    #@36
    .line 366
    new-instance v1, Landroid/hardware/Camera$EventHandler;

    #@38
    invoke-direct {v1, p0, p0, v0}, Landroid/hardware/Camera$EventHandler;-><init>(Landroid/hardware/Camera;Landroid/hardware/Camera;Landroid/os/Looper;)V

    #@3b
    iput-object v1, p0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@3d
    .line 373
    :goto_3d
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@3f
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@42
    invoke-direct {p0, v1, p1}, Landroid/hardware/Camera;->native_setup(Ljava/lang/Object;I)V

    #@45
    .line 374
    return-void

    #@46
    .line 367
    :cond_46
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@49
    move-result-object v0

    #@4a
    if-eqz v0, :cond_54

    #@4c
    .line 368
    new-instance v1, Landroid/hardware/Camera$EventHandler;

    #@4e
    invoke-direct {v1, p0, p0, v0}, Landroid/hardware/Camera$EventHandler;-><init>(Landroid/hardware/Camera;Landroid/hardware/Camera;Landroid/os/Looper;)V

    #@51
    iput-object v1, p0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@53
    goto :goto_3d

    #@54
    .line 370
    :cond_54
    iput-object v3, p0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@56
    goto :goto_3d
.end method

.method private final native _addCallbackBuffer([BI)V
.end method

.method private final native _enableOisDataListen(Z)Z
.end method

.method private final native _enableShutterSound(Z)Z
.end method

.method private static native _getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
.end method

.method private final native _startFaceDetection(I)V
.end method

.method private final native _stopFaceDetection()V
.end method

.method private final native _stopPreview()V
.end method

.method static synthetic access$000(Landroid/hardware/Camera;)Landroid/hardware/Camera$ShutterCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mRawImageCallback:Landroid/hardware/Camera$PictureCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Landroid/hardware/Camera;)Landroid/hardware/Camera$OnZoomChangeListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mZoomListener:Landroid/hardware/Camera$OnZoomChangeListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Landroid/hardware/Camera;)Landroid/hardware/Camera$FaceDetectionListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mFaceListener:Landroid/hardware/Camera$FaceDetectionListener;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Landroid/hardware/Camera;)Landroid/hardware/Camera$ErrorCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mErrorCallback:Landroid/hardware/Camera$ErrorCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Landroid/hardware/Camera;)Landroid/hardware/Camera$AutoFocusMoveCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mAutoFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1400([BI)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    invoke-static {p0, p1}, Landroid/hardware/Camera;->byteToInt([BI)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraDataCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mCameraDataCallback:Landroid/hardware/Camera$CameraDataCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Landroid/hardware/Camera;)Landroid/hardware/Camera$CameraMetaDataCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mCameraMetaDataCallback:Landroid/hardware/Camera$CameraMetaDataCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Landroid/hardware/Camera;)Landroid/hardware/Camera$OisDataListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mOisDataListener:Landroid/hardware/Camera$OisDataListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mJpegCallback:Landroid/hardware/Camera$PictureCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/hardware/Camera;)Landroid/hardware/Camera$PreviewCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Landroid/hardware/Camera;Landroid/hardware/Camera$PreviewCallback;)Landroid/hardware/Camera$PreviewCallback;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Landroid/hardware/Camera;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Landroid/hardware/Camera;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Landroid/hardware/Camera;->mOneShot:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Landroid/hardware/Camera;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Landroid/hardware/Camera;->mWithBuffer:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Landroid/hardware/Camera;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Landroid/hardware/Camera;->setHasPreviewCallback(ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/hardware/Camera;)Landroid/hardware/Camera$PictureCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mPostviewCallback:Landroid/hardware/Camera$PictureCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/hardware/Camera;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Landroid/hardware/Camera;)Landroid/hardware/Camera$AutoFocusCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/hardware/Camera;->mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    #@2
    return-object v0
.end method

.method private final addCallbackBuffer([BI)V
    .registers 6
    .parameter "callbackBuffer"
    .parameter "msgType"

    #@0
    .prologue
    .line 770
    const/16 v0, 0x10

    #@2
    if-eq p2, v0, :cond_21

    #@4
    const/16 v0, 0x80

    #@6
    if-eq p2, v0, :cond_21

    #@8
    .line 772
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Unsupported message type: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 776
    :cond_21
    invoke-direct {p0, p1, p2}, Landroid/hardware/Camera;->_addCallbackBuffer([BI)V

    #@24
    .line 777
    return-void
.end method

.method private static byteToInt([BI)I
    .registers 6
    .parameter "b"
    .parameter "offset"

    #@0
    .prologue
    .line 1647
    const/4 v2, 0x0

    #@1
    .line 1648
    .local v2, value:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    const/4 v3, 0x4

    #@3
    if-ge v0, v3, :cond_15

    #@5
    .line 1649
    rsub-int/lit8 v3, v0, 0x3

    #@7
    mul-int/lit8 v1, v3, 0x8

    #@9
    .line 1650
    .local v1, shift:I
    rsub-int/lit8 v3, v0, 0x3

    #@b
    add-int/2addr v3, p1

    #@c
    aget-byte v3, p0, v3

    #@e
    and-int/lit16 v3, v3, 0xff

    #@10
    shl-int/2addr v3, v1

    #@11
    add-int/2addr v2, v3

    #@12
    .line 1648
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_2

    #@15
    .line 1652
    .end local v1           #shift:I
    :cond_15
    return v2
.end method

.method private native enableFocusMoveCallback(I)V
.end method

.method public static getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
    .registers 7
    .parameter "cameraId"
    .parameter "cameraInfo"

    #@0
    .prologue
    .line 214
    invoke-static {p0, p1}, Landroid/hardware/Camera;->_getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    #@3
    .line 215
    const-string v3, "audio"

    #@5
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@8
    move-result-object v1

    #@9
    .line 216
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@c
    move-result-object v0

    #@d
    .line 218
    .local v0, audioService:Landroid/media/IAudioService;
    :try_start_d
    invoke-interface {v0}, Landroid/media/IAudioService;->isCameraSoundForced()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_16

    #@13
    .line 221
    const/4 v3, 0x0

    #@14
    iput-boolean v3, p1, Landroid/hardware/Camera$CameraInfo;->canDisableShutterSound:Z
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_16} :catch_17

    #@16
    .line 226
    :cond_16
    :goto_16
    return-void

    #@17
    .line 223
    :catch_17
    move-exception v2

    #@18
    .line 224
    .local v2, e:Landroid/os/RemoteException;
    const-string v3, "Camera"

    #@1a
    const-string v4, "Audio service is unavailable for queries"

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_16
.end method

.method public static getEmptyParameters()Landroid/hardware/Camera$Parameters;
    .registers 3

    #@0
    .prologue
    .line 1641
    new-instance v0, Landroid/hardware/Camera;

    #@2
    invoke-direct {v0}, Landroid/hardware/Camera;-><init>()V

    #@5
    .line 1642
    .local v0, camera:Landroid/hardware/Camera;
    new-instance v1, Landroid/hardware/Camera$Parameters;

    #@7
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    const/4 v2, 0x0

    #@b
    invoke-direct {v1, v0, v2}, Landroid/hardware/Camera$Parameters;-><init>(Landroid/hardware/Camera;Landroid/hardware/Camera$1;)V

    #@e
    return-object v1
.end method

.method public static native getNumberOfCameras()I
.end method

.method private final native native_autoFocus()V
.end method

.method private final native native_cancelAutoFocus()V
.end method

.method private final native native_cancelPicture()V
.end method

.method private final native native_getParameters()Ljava/lang/String;
.end method

.method private final native native_release()V
.end method

.method private final native native_sendHistogramData()V
.end method

.method private final native native_sendMetaData()V
.end method

.method private final native native_sendObjectTrackingCmd()V
.end method

.method private final native native_setFaceDetectionCb(Z)V
.end method

.method private final native native_setHistogramMode(Z)V
.end method

.method private final native native_setISPDataCallbackMode(Z)V
.end method

.method private final native native_setOBTDataCallbackMode(Z)V
.end method

.method private final native native_setParameters(Ljava/lang/String;)V
.end method

.method private final native native_setup(Ljava/lang/Object;I)V
.end method

.method private final native native_takePicture(I)V
.end method

.method public static open()Landroid/hardware/Camera;
    .registers 4

    #@0
    .prologue
    .line 336
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    #@3
    move-result v2

    #@4
    .line 337
    .local v2, numberOfCameras:I
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    #@6
    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    #@9
    .line 338
    .local v0, cameraInfo:Landroid/hardware/Camera$CameraInfo;
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_1c

    #@c
    .line 339
    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    #@f
    .line 340
    iget v3, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    #@11
    if-nez v3, :cond_19

    #@13
    .line 341
    new-instance v3, Landroid/hardware/Camera;

    #@15
    invoke-direct {v3, v1}, Landroid/hardware/Camera;-><init>(I)V

    #@18
    .line 344
    :goto_18
    return-object v3

    #@19
    .line 338
    :cond_19
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_a

    #@1c
    .line 344
    :cond_1c
    const/4 v3, 0x0

    #@1d
    goto :goto_18
.end method

.method public static open(I)Landroid/hardware/Camera;
    .registers 2
    .parameter "cameraId"

    #@0
    .prologue
    .line 326
    new-instance v0, Landroid/hardware/Camera;

    #@2
    invoke-direct {v0, p0}, Landroid/hardware/Camera;-><init>(I)V

    #@5
    return-object v0
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .registers 8
    .parameter "camera_ref"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 951
    check-cast p0, Ljava/lang/ref/WeakReference;

    #@2
    .end local p0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/hardware/Camera;

    #@8
    .line 952
    .local v0, c:Landroid/hardware/Camera;
    if-nez v0, :cond_b

    #@a
    .line 959
    :cond_a
    :goto_a
    return-void

    #@b
    .line 955
    :cond_b
    iget-object v2, v0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@d
    if-eqz v2, :cond_a

    #@f
    .line 956
    iget-object v2, v0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@11
    invoke-virtual {v2, p1, p2, p3, p4}, Landroid/hardware/Camera$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v1

    #@15
    .line 957
    .local v1, m:Landroid/os/Message;
    iget-object v2, v0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@17
    invoke-virtual {v2, v1}, Landroid/hardware/Camera$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    goto :goto_a
.end method

.method private final native setHasPreviewCallback(ZZ)V
.end method

.method private final native setPreviewDisplay(Landroid/view/Surface;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method


# virtual methods
.method public final addCallbackBuffer([B)V
    .registers 3
    .parameter "callbackBuffer"

    #@0
    .prologue
    .line 721
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, p1, v0}, Landroid/hardware/Camera;->_addCallbackBuffer([BI)V

    #@5
    .line 722
    return-void
.end method

.method public final addRawImageCallbackBuffer([B)V
    .registers 3
    .parameter "callbackBuffer"

    #@0
    .prologue
    .line 764
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, p1, v0}, Landroid/hardware/Camera;->addCallbackBuffer([BI)V

    #@5
    .line 765
    return-void
.end method

.method public final autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    .line 1037
    iget-object v1, p0, Landroid/hardware/Camera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1038
    :try_start_3
    iput-object p1, p0, Landroid/hardware/Camera;->mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    #@5
    .line 1039
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_a

    #@6
    .line 1040
    invoke-direct {p0}, Landroid/hardware/Camera;->native_autoFocus()V

    #@9
    .line 1041
    return-void

    #@a
    .line 1039
    :catchall_a
    move-exception v0

    #@b
    :try_start_b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_b .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public final cancelAutoFocus()V
    .registers 3

    #@0
    .prologue
    .line 1054
    iget-object v1, p0, Landroid/hardware/Camera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1055
    const/4 v0, 0x0

    #@4
    :try_start_4
    iput-object v0, p0, Landroid/hardware/Camera;->mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    #@6
    .line 1056
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_11

    #@7
    .line 1057
    invoke-direct {p0}, Landroid/hardware/Camera;->native_cancelAutoFocus()V

    #@a
    .line 1073
    iget-object v0, p0, Landroid/hardware/Camera;->mEventHandler:Landroid/hardware/Camera$EventHandler;

    #@c
    const/4 v1, 0x4

    #@d
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$EventHandler;->removeMessages(I)V

    #@10
    .line 1074
    return-void

    #@11
    .line 1056
    :catchall_11
    move-exception v0

    #@12
    :try_start_12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public final cancelPicture()V
    .registers 3

    #@0
    .prologue
    .line 1736
    invoke-direct {p0}, Landroid/hardware/Camera;->native_cancelPicture()V

    #@3
    .line 1737
    const-string v0, "Camera"

    #@5
    const-string v1, "cancelPicture "

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 1738
    return-void
.end method

.method public final enableShutterSound(Z)Z
    .registers 7
    .parameter "enabled"

    #@0
    .prologue
    .line 1317
    if-nez p1, :cond_1c

    #@2
    .line 1318
    const-string v3, "audio"

    #@4
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    .line 1319
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@b
    move-result-object v0

    #@c
    .line 1321
    .local v0, audioService:Landroid/media/IAudioService;
    :try_start_c
    invoke-interface {v0}, Landroid/media/IAudioService;->isCameraSoundForced()Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_f} :catch_14

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_1c

    #@12
    const/4 v3, 0x0

    #@13
    .line 1326
    .end local v0           #audioService:Landroid/media/IAudioService;
    .end local v1           #b:Landroid/os/IBinder;
    :goto_13
    return v3

    #@14
    .line 1322
    .restart local v0       #audioService:Landroid/media/IAudioService;
    .restart local v1       #b:Landroid/os/IBinder;
    :catch_14
    move-exception v2

    #@15
    .line 1323
    .local v2, e:Landroid/os/RemoteException;
    const-string v3, "Camera"

    #@17
    const-string v4, "Audio service is unavailable for queries"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1326
    .end local v0           #audioService:Landroid/media/IAudioService;
    .end local v1           #b:Landroid/os/IBinder;
    .end local v2           #e:Landroid/os/RemoteException;
    :cond_1c
    invoke-direct {p0, p1}, Landroid/hardware/Camera;->_enableShutterSound(Z)Z

    #@1f
    move-result v3

    #@20
    goto :goto_13
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 383
    invoke-virtual {p0}, Landroid/hardware/Camera;->release()V

    #@3
    .line 384
    return-void
.end method

.method public getParameters()Landroid/hardware/Camera$Parameters;
    .registers 4

    #@0
    .prologue
    .line 1627
    new-instance v0, Landroid/hardware/Camera$Parameters;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-direct {v0, p0, v2}, Landroid/hardware/Camera$Parameters;-><init>(Landroid/hardware/Camera;Landroid/hardware/Camera$1;)V

    #@6
    .line 1628
    .local v0, p:Landroid/hardware/Camera$Parameters;
    invoke-direct {p0}, Landroid/hardware/Camera;->native_getParameters()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 1629
    .local v1, s:Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->unflatten(Ljava/lang/String;)V

    #@d
    .line 1630
    return-object v0
.end method

.method public final native lock()V
.end method

.method public final native previewEnabled()Z
.end method

.method public final native reconnect()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final release()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 397
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v0

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 398
    iget v0, p0, Landroid/hardware/Camera;->mNativeContext:I

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 399
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@e
    move-result-object v0

    #@f
    invoke-interface {v0, v1}, Lcom/lge/cappuccino/IMdm;->setCameraState(Z)V

    #@12
    .line 403
    :cond_12
    invoke-direct {p0}, Landroid/hardware/Camera;->native_release()V

    #@15
    .line 404
    iput-boolean v1, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@17
    .line 405
    return-void
.end method

.method public final runObjectTracking()V
    .registers 1

    #@0
    .prologue
    .line 1779
    invoke-direct {p0}, Landroid/hardware/Camera;->native_sendObjectTrackingCmd()V

    #@3
    .line 1780
    return-void
.end method

.method public final sendHistogramData()V
    .registers 1

    #@0
    .prologue
    .line 1687
    invoke-direct {p0}, Landroid/hardware/Camera;->native_sendHistogramData()V

    #@3
    .line 1688
    return-void
.end method

.method public final sendMetaData()V
    .registers 1

    #@0
    .prologue
    .line 1724
    invoke-direct {p0}, Landroid/hardware/Camera;->native_sendMetaData()V

    #@3
    .line 1725
    return-void
.end method

.method public setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1102
    iput-object p1, p0, Landroid/hardware/Camera;->mAutoFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    #@2
    .line 1103
    iget-object v0, p0, Landroid/hardware/Camera;->mAutoFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    #@4
    if-eqz v0, :cond_b

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->enableFocusMoveCallback(I)V

    #@a
    .line 1104
    return-void

    #@b
    .line 1103
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_7
.end method

.method public final native setDisplayOrientation(I)V
.end method

.method public final setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 1602
    iput-object p1, p0, Landroid/hardware/Camera;->mErrorCallback:Landroid/hardware/Camera$ErrorCallback;

    #@2
    .line 1603
    return-void
.end method

.method public final setFaceDetectionCb(Landroid/hardware/Camera$CameraMetaDataCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1714
    iput-object p1, p0, Landroid/hardware/Camera;->mCameraMetaDataCallback:Landroid/hardware/Camera$CameraMetaDataCallback;

    #@2
    .line 1715
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->native_setFaceDetectionCb(Z)V

    #@8
    .line 1716
    return-void

    #@9
    .line 1715
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public final setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 1387
    iput-object p1, p0, Landroid/hardware/Camera;->mFaceListener:Landroid/hardware/Camera$FaceDetectionListener;

    #@2
    .line 1388
    return-void
.end method

.method public final setHistogramMode(Landroid/hardware/Camera$CameraDataCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1676
    iput-object p1, p0, Landroid/hardware/Camera;->mCameraDataCallback:Landroid/hardware/Camera$CameraDataCallback;

    #@2
    .line 1677
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->native_setHistogramMode(Z)V

    #@8
    .line 1678
    return-void

    #@9
    .line 1677
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public final setISPDataCallbackMode(Landroid/hardware/Camera$CameraDataCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1752
    iput-object p1, p0, Landroid/hardware/Camera;->mCameraDataCallback:Landroid/hardware/Camera$CameraDataCallback;

    #@2
    .line 1753
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->native_setISPDataCallbackMode(Z)V

    #@8
    .line 1754
    return-void

    #@9
    .line 1753
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public final setOBTDataCallbackMode(Landroid/hardware/Camera$CameraDataCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1768
    iput-object p1, p0, Landroid/hardware/Camera;->mCameraDataCallback:Landroid/hardware/Camera$CameraDataCallback;

    #@2
    .line 1769
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->native_setOBTDataCallbackMode(Z)V

    #@8
    .line 1770
    return-void

    #@9
    .line 1769
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method public final setOisDataListener(Landroid/hardware/Camera$OisDataListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1836
    iput-object p1, p0, Landroid/hardware/Camera;->mOisDataListener:Landroid/hardware/Camera$OisDataListener;

    #@4
    .line 1837
    if-eqz p1, :cond_10

    #@6
    iget-boolean v0, p0, Landroid/hardware/Camera;->mOisDataRunning:Z

    #@8
    if-nez v0, :cond_10

    #@a
    .line 1839
    iput-boolean v2, p0, Landroid/hardware/Camera;->mOisDataRunning:Z

    #@c
    .line 1840
    invoke-direct {p0, v2}, Landroid/hardware/Camera;->_enableOisDataListen(Z)Z

    #@f
    .line 1847
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1842
    :cond_10
    if-nez p1, :cond_f

    #@12
    iget-boolean v0, p0, Landroid/hardware/Camera;->mOisDataRunning:Z

    #@14
    if-eqz v0, :cond_f

    #@16
    .line 1844
    iput-boolean v1, p0, Landroid/hardware/Camera;->mOisDataRunning:Z

    #@18
    .line 1845
    invoke-direct {p0, v1}, Landroid/hardware/Camera;->_enableOisDataListen(Z)Z

    #@1b
    goto :goto_f
.end method

.method public final setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 645
    iput-object p1, p0, Landroid/hardware/Camera;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    #@4
    .line 646
    iput-boolean v0, p0, Landroid/hardware/Camera;->mOneShot:Z

    #@6
    .line 647
    iput-boolean v1, p0, Landroid/hardware/Camera;->mWithBuffer:Z

    #@8
    .line 648
    if-eqz p1, :cond_e

    #@a
    :goto_a
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera;->setHasPreviewCallback(ZZ)V

    #@d
    .line 649
    return-void

    #@e
    :cond_e
    move v0, v1

    #@f
    .line 648
    goto :goto_a
.end method

.method public setParameters(Landroid/hardware/Camera$Parameters;)V
    .registers 3
    .parameter "params"

    #@0
    .prologue
    .line 1616
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->native_setParameters(Ljava/lang/String;)V

    #@7
    .line 1617
    return-void
.end method

.method public final setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 622
    iput-object p1, p0, Landroid/hardware/Camera;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    #@3
    .line 623
    iput-boolean v1, p0, Landroid/hardware/Camera;->mOneShot:Z

    #@5
    .line 624
    iput-boolean v1, p0, Landroid/hardware/Camera;->mWithBuffer:Z

    #@7
    .line 627
    if-eqz p1, :cond_e

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera;->setHasPreviewCallback(ZZ)V

    #@d
    .line 628
    return-void

    #@e
    :cond_e
    move v0, v1

    #@f
    .line 627
    goto :goto_a
.end method

.method public final setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V
    .registers 4
    .parameter "cb"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 680
    iput-object p1, p0, Landroid/hardware/Camera;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    #@4
    .line 681
    iput-boolean v0, p0, Landroid/hardware/Camera;->mOneShot:Z

    #@6
    .line 682
    iput-boolean v1, p0, Landroid/hardware/Camera;->mWithBuffer:Z

    #@8
    .line 683
    if-eqz p1, :cond_b

    #@a
    move v0, v1

    #@b
    :cond_b
    invoke-direct {p0, v0, v1}, Landroid/hardware/Camera;->setHasPreviewCallback(ZZ)V

    #@e
    .line 684
    return-void
.end method

.method public final setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "holder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 488
    if-eqz p1, :cond_a

    #@2
    .line 489
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@5
    move-result-object v0

    #@6
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/Surface;)V

    #@9
    .line 493
    :goto_9
    return-void

    #@a
    .line 491
    :cond_a
    const/4 v0, 0x0

    #@b
    check-cast v0, Landroid/view/Surface;

    #@d
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/Surface;)V

    #@10
    goto :goto_9
.end method

.method public final native setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 1360
    iput-object p1, p0, Landroid/hardware/Camera;->mZoomListener:Landroid/hardware/Camera$OnZoomChangeListener;

    #@2
    .line 1361
    return-void
.end method

.method public final startFaceDetection()V
    .registers 3

    #@0
    .prologue
    .line 1423
    iget-boolean v0, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1424
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "Face detection is already running"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1426
    :cond_c
    const/4 v0, 0x0

    #@d
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->_startFaceDetection(I)V

    #@10
    .line 1427
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@13
    .line 1428
    return-void
.end method

.method public final native startPreview()V
.end method

.method public final native startSmoothZoom(I)V
.end method

.method public final stopFaceDetection()V
    .registers 2

    #@0
    .prologue
    .line 1436
    invoke-direct {p0}, Landroid/hardware/Camera;->_stopFaceDetection()V

    #@3
    .line 1437
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@6
    .line 1438
    return-void
.end method

.method public final stopPreview()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 583
    invoke-direct {p0}, Landroid/hardware/Camera;->_stopPreview()V

    #@4
    .line 584
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@7
    .line 586
    iput-object v2, p0, Landroid/hardware/Camera;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    #@9
    .line 587
    iput-object v2, p0, Landroid/hardware/Camera;->mRawImageCallback:Landroid/hardware/Camera$PictureCallback;

    #@b
    .line 588
    iput-object v2, p0, Landroid/hardware/Camera;->mPostviewCallback:Landroid/hardware/Camera$PictureCallback;

    #@d
    .line 589
    iput-object v2, p0, Landroid/hardware/Camera;->mJpegCallback:Landroid/hardware/Camera$PictureCallback;

    #@f
    .line 590
    iget-object v1, p0, Landroid/hardware/Camera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    #@11
    monitor-enter v1

    #@12
    .line 591
    const/4 v0, 0x0

    #@13
    :try_start_13
    iput-object v0, p0, Landroid/hardware/Camera;->mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    #@15
    .line 592
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_19

    #@16
    .line 593
    iput-object v2, p0, Landroid/hardware/Camera;->mAutoFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    #@18
    .line 594
    return-void

    #@19
    .line 592
    :catchall_19
    move-exception v0

    #@1a
    :try_start_1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public final native stopSmoothZoom()V
.end method

.method public final takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    .registers 5
    .parameter "shutter"
    .parameter "raw"
    .parameter "jpeg"

    #@0
    .prologue
    .line 1149
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0, p3}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    #@4
    .line 1150
    return-void
.end method

.method public final takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    .registers 7
    .parameter "shutter"
    .parameter "raw"
    .parameter "postview"
    .parameter "jpeg"

    #@0
    .prologue
    .line 1184
    iput-object p1, p0, Landroid/hardware/Camera;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    #@2
    .line 1185
    iput-object p2, p0, Landroid/hardware/Camera;->mRawImageCallback:Landroid/hardware/Camera$PictureCallback;

    #@4
    .line 1186
    iput-object p3, p0, Landroid/hardware/Camera;->mPostviewCallback:Landroid/hardware/Camera$PictureCallback;

    #@6
    .line 1187
    iput-object p4, p0, Landroid/hardware/Camera;->mJpegCallback:Landroid/hardware/Camera$PictureCallback;

    #@8
    .line 1190
    const/4 v0, 0x0

    #@9
    .line 1191
    .local v0, msgType:I
    iget-object v1, p0, Landroid/hardware/Camera;->mShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    #@b
    if-eqz v1, :cond_f

    #@d
    .line 1192
    or-int/lit8 v0, v0, 0x2

    #@f
    .line 1194
    :cond_f
    iget-object v1, p0, Landroid/hardware/Camera;->mRawImageCallback:Landroid/hardware/Camera$PictureCallback;

    #@11
    if-eqz v1, :cond_15

    #@13
    .line 1195
    or-int/lit16 v0, v0, 0x80

    #@15
    .line 1197
    :cond_15
    iget-object v1, p0, Landroid/hardware/Camera;->mPostviewCallback:Landroid/hardware/Camera$PictureCallback;

    #@17
    if-eqz v1, :cond_1b

    #@19
    .line 1198
    or-int/lit8 v0, v0, 0x40

    #@1b
    .line 1200
    :cond_1b
    iget-object v1, p0, Landroid/hardware/Camera;->mJpegCallback:Landroid/hardware/Camera$PictureCallback;

    #@1d
    if-eqz v1, :cond_21

    #@1f
    .line 1201
    or-int/lit16 v0, v0, 0x100

    #@21
    .line 1204
    :cond_21
    invoke-direct {p0, v0}, Landroid/hardware/Camera;->native_takePicture(I)V

    #@24
    .line 1205
    const/4 v1, 0x0

    #@25
    iput-boolean v1, p0, Landroid/hardware/Camera;->mFaceDetectionRunning:Z

    #@27
    .line 1206
    return-void
.end method

.method public native sendRawCommand(III)V
.end method

.method public final native unlock()V
.end method
