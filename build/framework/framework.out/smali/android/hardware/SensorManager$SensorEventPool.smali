.class public final Landroid/hardware/SensorManager$SensorEventPool;
.super Ljava/lang/Object;
.source "SensorManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/SensorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "SensorEventPool"
.end annotation


# instance fields
.field private mNumItemsInPool:I

.field private final mPool:[Landroid/hardware/SensorEvent;

.field private final mPoolSize:I


# direct methods
.method constructor <init>(I)V
    .registers 3
    .parameter "poolSize"

    #@0
    .prologue
    .line 1332
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1333
    iput p1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPoolSize:I

    #@5
    .line 1334
    iput p1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@7
    .line 1335
    new-array v0, p1, [Landroid/hardware/SensorEvent;

    #@9
    iput-object v0, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPool:[Landroid/hardware/SensorEvent;

    #@b
    .line 1336
    return-void
.end method

.method private createSensorEvent()Landroid/hardware/SensorEvent;
    .registers 3

    #@0
    .prologue
    .line 1329
    new-instance v0, Landroid/hardware/SensorEvent;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-direct {v0, v1}, Landroid/hardware/SensorEvent;-><init>(I)V

    #@6
    return-object v0
.end method


# virtual methods
.method getFromPool()Landroid/hardware/SensorEvent;
    .registers 5

    #@0
    .prologue
    .line 1339
    const/4 v1, 0x0

    #@1
    .line 1340
    .local v1, t:Landroid/hardware/SensorEvent;
    monitor-enter p0

    #@2
    .line 1341
    :try_start_2
    iget v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@4
    if-lez v2, :cond_1b

    #@6
    .line 1343
    iget v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPoolSize:I

    #@8
    iget v3, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@a
    sub-int v0, v2, v3

    #@c
    .line 1344
    .local v0, index:I
    iget-object v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPool:[Landroid/hardware/SensorEvent;

    #@e
    aget-object v1, v2, v0

    #@10
    .line 1345
    iget-object v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPool:[Landroid/hardware/SensorEvent;

    #@12
    const/4 v3, 0x0

    #@13
    aput-object v3, v2, v0

    #@15
    .line 1346
    iget v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@17
    add-int/lit8 v2, v2, -0x1

    #@19
    iput v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@1b
    .line 1348
    .end local v0           #index:I
    :cond_1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_2 .. :try_end_1c} :catchall_23

    #@1c
    .line 1349
    if-nez v1, :cond_22

    #@1e
    .line 1352
    invoke-direct {p0}, Landroid/hardware/SensorManager$SensorEventPool;->createSensorEvent()Landroid/hardware/SensorEvent;

    #@21
    move-result-object v1

    #@22
    .line 1354
    :cond_22
    return-object v1

    #@23
    .line 1348
    :catchall_23
    move-exception v2

    #@24
    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    throw v2
.end method

.method returnToPool(Landroid/hardware/SensorEvent;)V
    .registers 5
    .parameter "t"

    #@0
    .prologue
    .line 1358
    monitor-enter p0

    #@1
    .line 1360
    :try_start_1
    iget v1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@3
    iget v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPoolSize:I

    #@5
    if-ge v1, v2, :cond_17

    #@7
    .line 1362
    iget v1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@9
    add-int/lit8 v1, v1, 0x1

    #@b
    iput v1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@d
    .line 1363
    iget v1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPoolSize:I

    #@f
    iget v2, p0, Landroid/hardware/SensorManager$SensorEventPool;->mNumItemsInPool:I

    #@11
    sub-int v0, v1, v2

    #@13
    .line 1364
    .local v0, index:I
    iget-object v1, p0, Landroid/hardware/SensorManager$SensorEventPool;->mPool:[Landroid/hardware/SensorEvent;

    #@15
    aput-object p1, v1, v0

    #@17
    .line 1366
    .end local v0           #index:I
    :cond_17
    monitor-exit p0

    #@18
    .line 1367
    return-void

    #@19
    .line 1366
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method
