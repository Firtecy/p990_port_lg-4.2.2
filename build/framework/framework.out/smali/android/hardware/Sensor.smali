.class public final Landroid/hardware/Sensor;
.super Ljava/lang/Object;
.source "Sensor.java"


# static fields
.field public static final TYPE_ACCELEROMETER:I = 0x1

.field public static final TYPE_ALL:I = -0x1

.field public static final TYPE_AMBIENT_TEMPERATURE:I = 0xd

.field public static final TYPE_GRAVITY:I = 0x9

.field public static final TYPE_GYROSCOPE:I = 0x4

.field public static final TYPE_LIGHT:I = 0x5

.field public static final TYPE_LINEAR_ACCELERATION:I = 0xa

.field public static final TYPE_MAGNETIC_FIELD:I = 0x2

.field public static final TYPE_ORIENTATION:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TYPE_PRESSURE:I = 0x6

.field public static final TYPE_PROXIMITY:I = 0x8

.field public static final TYPE_RELATIVE_HUMIDITY:I = 0xc

.field public static final TYPE_ROTATION_VECTOR:I = 0xb

.field public static final TYPE_TEMPERATURE:I = 0x7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private mHandle:I

.field private mMaxRange:F

.field private mMinDelay:I

.field private mName:Ljava/lang/String;

.field private mPower:F

.field private mResolution:F

.field private mType:I

.field private mVendor:Ljava/lang/String;

.field private mVersion:I


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 136
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 137
    return-void
.end method


# virtual methods
.method getHandle()I
    .registers 2

    #@0
    .prologue
    .line 198
    iget v0, p0, Landroid/hardware/Sensor;->mHandle:I

    #@2
    return v0
.end method

.method public getMaximumRange()F
    .registers 2

    #@0
    .prologue
    .line 171
    iget v0, p0, Landroid/hardware/Sensor;->mMaxRange:F

    #@2
    return v0
.end method

.method public getMinDelay()I
    .registers 2

    #@0
    .prologue
    .line 194
    iget v0, p0, Landroid/hardware/Sensor;->mMinDelay:I

    #@2
    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/hardware/Sensor;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPower()F
    .registers 2

    #@0
    .prologue
    .line 185
    iget v0, p0, Landroid/hardware/Sensor;->mPower:F

    #@2
    return v0
.end method

.method public getResolution()F
    .registers 2

    #@0
    .prologue
    .line 178
    iget v0, p0, Landroid/hardware/Sensor;->mResolution:F

    #@2
    return v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 157
    iget v0, p0, Landroid/hardware/Sensor;->mType:I

    #@2
    return v0
.end method

.method public getVendor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/hardware/Sensor;->mVendor:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getVersion()I
    .registers 2

    #@0
    .prologue
    .line 164
    iget v0, p0, Landroid/hardware/Sensor;->mVersion:I

    #@2
    return v0
.end method

.method setRange(FF)V
    .registers 3
    .parameter "max"
    .parameter "res"

    #@0
    .prologue
    .line 202
    iput p1, p0, Landroid/hardware/Sensor;->mMaxRange:F

    #@2
    .line 203
    iput p2, p0, Landroid/hardware/Sensor;->mResolution:F

    #@4
    .line 204
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "{Sensor name=\""

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Landroid/hardware/Sensor;->mName:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, "\", vendor=\""

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Landroid/hardware/Sensor;->mVendor:Ljava/lang/String;

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, "\", version="

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    iget v1, p0, Landroid/hardware/Sensor;->mVersion:I

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string v1, ", type="

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget v1, p0, Landroid/hardware/Sensor;->mType:I

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v1, ", maxRange="

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    iget v1, p0, Landroid/hardware/Sensor;->mMaxRange:F

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    const-string v1, ", resolution="

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    iget v1, p0, Landroid/hardware/Sensor;->mResolution:F

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    const-string v1, ", power="

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    iget v1, p0, Landroid/hardware/Sensor;->mPower:F

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    const-string v1, ", minDelay="

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v0

    #@60
    iget v1, p0, Landroid/hardware/Sensor;->mMinDelay:I

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    const-string/jumbo v1, "}"

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v0

    #@71
    return-object v0
.end method
