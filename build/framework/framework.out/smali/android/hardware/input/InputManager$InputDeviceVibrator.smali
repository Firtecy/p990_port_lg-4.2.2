.class final Landroid/hardware/input/InputManager$InputDeviceVibrator;
.super Landroid/os/Vibrator;
.source "InputManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/input/InputManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "InputDeviceVibrator"
.end annotation


# instance fields
.field private final mDeviceId:I

.field private final mToken:Landroid/os/Binder;

.field final synthetic this$0:Landroid/hardware/input/InputManager;


# direct methods
.method public constructor <init>(Landroid/hardware/input/InputManager;I)V
    .registers 4
    .parameter
    .parameter "deviceId"

    #@0
    .prologue
    .line 785
    iput-object p1, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->this$0:Landroid/hardware/input/InputManager;

    #@2
    invoke-direct {p0}, Landroid/os/Vibrator;-><init>()V

    #@5
    .line 786
    iput p2, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->mDeviceId:I

    #@7
    .line 787
    new-instance v0, Landroid/os/Binder;

    #@9
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@c
    iput-object v0, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->mToken:Landroid/os/Binder;

    #@e
    .line 788
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 5

    #@0
    .prologue
    .line 815
    :try_start_0
    iget-object v1, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->this$0:Landroid/hardware/input/InputManager;

    #@2
    invoke-static {v1}, Landroid/hardware/input/InputManager;->access$200(Landroid/hardware/input/InputManager;)Landroid/hardware/input/IInputManager;

    #@5
    move-result-object v1

    #@6
    iget v2, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->mDeviceId:I

    #@8
    iget-object v3, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->mToken:Landroid/os/Binder;

    #@a
    invoke-interface {v1, v2, v3}, Landroid/hardware/input/IInputManager;->cancelVibrate(ILandroid/os/IBinder;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 819
    :goto_d
    return-void

    #@e
    .line 816
    :catch_e
    move-exception v0

    #@f
    .line 817
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@11
    const-string v2, "Failed to cancel vibration."

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public hasVibrator()Z
    .registers 2

    #@0
    .prologue
    .line 792
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public vibrate(J)V
    .registers 7
    .parameter "milliseconds"

    #@0
    .prologue
    .line 797
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [J

    #@3
    const/4 v1, 0x0

    #@4
    const-wide/16 v2, 0x0

    #@6
    aput-wide v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    aput-wide p1, v0, v1

    #@b
    const/4 v1, -0x1

    #@c
    invoke-virtual {p0, v0, v1}, Landroid/hardware/input/InputManager$InputDeviceVibrator;->vibrate([JI)V

    #@f
    .line 798
    return-void
.end method

.method public vibrate([JI)V
    .registers 7
    .parameter "pattern"
    .parameter "repeat"

    #@0
    .prologue
    .line 802
    array-length v1, p1

    #@1
    if-lt p2, v1, :cond_9

    #@3
    .line 803
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@5
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@8
    throw v1

    #@9
    .line 806
    :cond_9
    :try_start_9
    iget-object v1, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->this$0:Landroid/hardware/input/InputManager;

    #@b
    invoke-static {v1}, Landroid/hardware/input/InputManager;->access$200(Landroid/hardware/input/InputManager;)Landroid/hardware/input/IInputManager;

    #@e
    move-result-object v1

    #@f
    iget v2, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->mDeviceId:I

    #@11
    iget-object v3, p0, Landroid/hardware/input/InputManager$InputDeviceVibrator;->mToken:Landroid/os/Binder;

    #@13
    invoke-interface {v1, v2, p1, p2, v3}, Landroid/hardware/input/IInputManager;->vibrate(I[JILandroid/os/IBinder;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_16} :catch_17

    #@16
    .line 810
    :goto_16
    return-void

    #@17
    .line 807
    :catch_17
    move-exception v0

    #@18
    .line 808
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@1a
    const-string v2, "Failed to vibrate."

    #@1c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_16
.end method
