.class public final Landroid/hardware/input/InputManager;
.super Ljava/lang/Object;
.source "InputManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/input/InputManager$1;,
        Landroid/hardware/input/InputManager$InputDeviceVibrator;,
        Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;,
        Landroid/hardware/input/InputManager$InputDevicesChangedListener;,
        Landroid/hardware/input/InputManager$InputDeviceListener;
    }
.end annotation


# static fields
.field public static final ACTION_QUERY_KEYBOARD_LAYOUTS:Ljava/lang/String; = "android.hardware.input.action.QUERY_KEYBOARD_LAYOUTS"

.field private static final DEBUG:Z = false

.field public static final DEFAULT_POINTER_SPEED:I = 0x0

.field public static final INJECT_INPUT_EVENT_MODE_ASYNC:I = 0x0

.field public static final INJECT_INPUT_EVENT_MODE_WAIT_FOR_FINISH:I = 0x2

.field public static final INJECT_INPUT_EVENT_MODE_WAIT_FOR_RESULT:I = 0x1

.field public static final MAX_POINTER_SPEED:I = 0x7

.field public static final META_DATA_KEYBOARD_LAYOUTS:Ljava/lang/String; = "android.hardware.input.metadata.KEYBOARD_LAYOUTS"

.field public static final MIN_POINTER_SPEED:I = -0x7

.field private static final MSG_DEVICE_ADDED:I = 0x1

.field private static final MSG_DEVICE_CHANGED:I = 0x3

.field private static final MSG_DEVICE_REMOVED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "InputManager"

.field private static sInstance:Landroid/hardware/input/InputManager;


# instance fields
.field private final mIm:Landroid/hardware/input/IInputManager;

.field private final mInputDeviceListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private mInputDevices:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/InputDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mInputDevicesChangedListener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;

.field private final mInputDevicesLock:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Landroid/hardware/input/IInputManager;)V
    .registers 3
    .parameter "im"

    #@0
    .prologue
    .line 173
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@a
    .line 66
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@11
    .line 174
    iput-object p1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@13
    .line 175
    return-void
.end method

.method static synthetic access$100(Landroid/hardware/input/InputManager;[I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/hardware/input/InputManager;->onInputDevicesChanged([I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/hardware/input/InputManager;)Landroid/hardware/input/IInputManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@2
    return-object v0
.end method

.method private static containsDeviceId([II)Z
    .registers 4
    .parameter "deviceIdAndGeneration"
    .parameter "deviceId"

    #@0
    .prologue
    .line 704
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    array-length v1, p0

    #@2
    if-ge v0, v1, :cond_d

    #@4
    .line 705
    aget v1, p0, v0

    #@6
    if-ne v1, p1, :cond_a

    #@8
    .line 706
    const/4 v1, 0x1

    #@9
    .line 709
    :goto_9
    return v1

    #@a
    .line 704
    :cond_a
    add-int/lit8 v0, v0, 0x2

    #@c
    goto :goto_1

    #@d
    .line 709
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_9
.end method

.method private findInputDeviceListenerLocked(Landroid/hardware/input/InputManager$InputDeviceListener;)I
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 319
    iget-object v2, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 320
    .local v1, numListeners:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_19

    #@9
    .line 321
    iget-object v2, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;

    #@11
    iget-object v2, v2, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;->mListener:Landroid/hardware/input/InputManager$InputDeviceListener;

    #@13
    if-ne v2, p1, :cond_16

    #@15
    .line 325
    .end local v0           #i:I
    :goto_15
    return v0

    #@16
    .line 320
    .restart local v0       #i:I
    :cond_16
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_7

    #@19
    .line 325
    :cond_19
    const/4 v0, -0x1

    #@1a
    goto :goto_15
.end method

.method public static getInstance()Landroid/hardware/input/InputManager;
    .registers 4

    #@0
    .prologue
    .line 185
    const-class v2, Landroid/hardware/input/InputManager;

    #@2
    monitor-enter v2

    #@3
    .line 186
    :try_start_3
    sget-object v1, Landroid/hardware/input/InputManager;->sInstance:Landroid/hardware/input/InputManager;

    #@5
    if-nez v1, :cond_18

    #@7
    .line 187
    const-string v1, "input"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 188
    .local v0, b:Landroid/os/IBinder;
    new-instance v1, Landroid/hardware/input/InputManager;

    #@f
    invoke-static {v0}, Landroid/hardware/input/IInputManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/input/IInputManager;

    #@12
    move-result-object v3

    #@13
    invoke-direct {v1, v3}, Landroid/hardware/input/InputManager;-><init>(Landroid/hardware/input/IInputManager;)V

    #@16
    sput-object v1, Landroid/hardware/input/InputManager;->sInstance:Landroid/hardware/input/InputManager;

    #@18
    .line 190
    :cond_18
    sget-object v1, Landroid/hardware/input/InputManager;->sInstance:Landroid/hardware/input/InputManager;

    #@1a
    monitor-exit v2

    #@1b
    return-object v1

    #@1c
    .line 191
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method private onInputDevicesChanged([I)V
    .registers 10
    .parameter "deviceIdAndGeneration"

    #@0
    .prologue
    .line 657
    iget-object v6, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 658
    :try_start_3
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@8
    move-result v3

    #@9
    .local v3, i:I
    :cond_9
    :goto_9
    add-int/lit8 v3, v3, -0x1

    #@b
    if-lez v3, :cond_26

    #@d
    .line 659
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->keyAt(I)I

    #@12
    move-result v1

    #@13
    .line 660
    .local v1, deviceId:I
    invoke-static {p1, v1}, Landroid/hardware/input/InputManager;->containsDeviceId([II)Z

    #@16
    move-result v5

    #@17
    if-nez v5, :cond_9

    #@19
    .line 664
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@1b
    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->removeAt(I)V

    #@1e
    .line 665
    const/4 v5, 0x2

    #@1f
    invoke-direct {p0, v5, v1}, Landroid/hardware/input/InputManager;->sendMessageToInputDeviceListenersLocked(II)V

    #@22
    goto :goto_9

    #@23
    .line 692
    .end local v1           #deviceId:I
    .end local v3           #i:I
    :catchall_23
    move-exception v5

    #@24
    monitor-exit v6
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v5

    #@26
    .line 669
    .restart local v3       #i:I
    :cond_26
    const/4 v3, 0x0

    #@27
    :goto_27
    :try_start_27
    array-length v5, p1

    #@28
    if-ge v3, v5, :cond_60

    #@2a
    .line 670
    aget v1, p1, v3

    #@2c
    .line 671
    .restart local v1       #deviceId:I
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@2e
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@31
    move-result v4

    #@32
    .line 672
    .local v4, index:I
    if-ltz v4, :cond_55

    #@34
    .line 673
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@36
    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@39
    move-result-object v0

    #@3a
    check-cast v0, Landroid/view/InputDevice;

    #@3c
    .line 674
    .local v0, device:Landroid/view/InputDevice;
    if-eqz v0, :cond_52

    #@3e
    .line 675
    add-int/lit8 v5, v3, 0x1

    #@40
    aget v2, p1, v5

    #@42
    .line 676
    .local v2, generation:I
    invoke-virtual {v0}, Landroid/view/InputDevice;->getGeneration()I

    #@45
    move-result v5

    #@46
    if-eq v5, v2, :cond_52

    #@48
    .line 680
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@4a
    const/4 v7, 0x0

    #@4b
    invoke-virtual {v5, v4, v7}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    #@4e
    .line 681
    const/4 v5, 0x3

    #@4f
    invoke-direct {p0, v5, v1}, Landroid/hardware/input/InputManager;->sendMessageToInputDeviceListenersLocked(II)V

    #@52
    .line 669
    .end local v0           #device:Landroid/view/InputDevice;
    .end local v2           #generation:I
    :cond_52
    :goto_52
    add-int/lit8 v3, v3, 0x2

    #@54
    goto :goto_27

    #@55
    .line 688
    :cond_55
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@57
    const/4 v7, 0x0

    #@58
    invoke-virtual {v5, v1, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@5b
    .line 689
    const/4 v5, 0x1

    #@5c
    invoke-direct {p0, v5, v1}, Landroid/hardware/input/InputManager;->sendMessageToInputDeviceListenersLocked(II)V

    #@5f
    goto :goto_52

    #@60
    .line 692
    .end local v1           #deviceId:I
    .end local v4           #index:I
    :cond_60
    monitor-exit v6
    :try_end_61
    .catchall {:try_start_27 .. :try_end_61} :catchall_23

    #@61
    .line 693
    return-void
.end method

.method private populateInputDevicesLocked()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 626
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mInputDevicesChangedListener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;

    #@3
    if-nez v4, :cond_11

    #@5
    .line 627
    new-instance v3, Landroid/hardware/input/InputManager$InputDevicesChangedListener;

    #@7
    invoke-direct {v3, p0, v6}, Landroid/hardware/input/InputManager$InputDevicesChangedListener;-><init>(Landroid/hardware/input/InputManager;Landroid/hardware/input/InputManager$1;)V

    #@a
    .line 629
    .local v3, listener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;
    :try_start_a
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@c
    invoke-interface {v4, v3}, Landroid/hardware/input/IInputManager;->registerInputDevicesChangedListener(Landroid/hardware/input/IInputDevicesChangedListener;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_30

    #@f
    .line 634
    iput-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevicesChangedListener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;

    #@11
    .line 637
    .end local v3           #listener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;
    :cond_11
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@13
    if-nez v4, :cond_42

    #@15
    .line 640
    :try_start_15
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@17
    invoke-interface {v4}, Landroid/hardware/input/IInputManager;->getInputDeviceIds()[I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_39

    #@1a
    move-result-object v2

    #@1b
    .line 645
    .local v2, ids:[I
    new-instance v4, Landroid/util/SparseArray;

    #@1d
    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    #@20
    iput-object v4, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@22
    .line 646
    const/4 v1, 0x0

    #@23
    .local v1, i:I
    :goto_23
    array-length v4, v2

    #@24
    if-ge v1, v4, :cond_42

    #@26
    .line 647
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@28
    aget v5, v2, v1

    #@2a
    invoke-virtual {v4, v5, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@2d
    .line 646
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_23

    #@30
    .line 630
    .end local v1           #i:I
    .end local v2           #ids:[I
    .restart local v3       #listener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;
    :catch_30
    move-exception v0

    #@31
    .line 631
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@33
    const-string v5, "Could not get register input device changed listener"

    #@35
    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@38
    throw v4

    #@39
    .line 641
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v3           #listener:Landroid/hardware/input/InputManager$InputDevicesChangedListener;
    :catch_39
    move-exception v0

    #@3a
    .line 642
    .restart local v0       #ex:Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@3c
    const-string v5, "Could not get input device ids."

    #@3e
    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@41
    throw v4

    #@42
    .line 650
    .end local v0           #ex:Landroid/os/RemoteException;
    :cond_42
    return-void
.end method

.method private sendMessageToInputDeviceListenersLocked(II)V
    .registers 7
    .parameter "what"
    .parameter "deviceId"

    #@0
    .prologue
    .line 696
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 697
    .local v2, numListeners:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v2, :cond_1c

    #@9
    .line 698
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;

    #@11
    .line 699
    .local v1, listener:Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;
    const/4 v3, 0x0

    #@12
    invoke-virtual {v1, p1, p2, v3}, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;->obtainMessage(III)Landroid/os/Message;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v1, v3}, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;->sendMessage(Landroid/os/Message;)Z

    #@19
    .line 697
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_7

    #@1c
    .line 701
    .end local v1           #listener:Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;
    :cond_1c
    return-void
.end method


# virtual methods
.method public addKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 456
    if-nez p1, :cond_a

    #@2
    .line 457
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 459
    :cond_a
    if-nez p2, :cond_15

    #@c
    .line 460
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v2, "keyboardLayoutDescriptor must not be null"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 464
    :cond_15
    :try_start_15
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@17
    invoke-interface {v1, p1, p2}, Landroid/hardware/input/IInputManager;->addKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_1b

    #@1a
    .line 468
    :goto_1a
    return-void

    #@1b
    .line 465
    :catch_1b
    move-exception v0

    #@1c
    .line 466
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@1e
    const-string v2, "Could not add keyboard layout for input device."

    #@20
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_1a
.end method

.method public deviceHasKeys([I)[Z
    .registers 6
    .parameter "keyCodes"

    #@0
    .prologue
    .line 578
    array-length v1, p1

    #@1
    new-array v0, v1, [Z

    #@3
    .line 580
    .local v0, ret:[Z
    :try_start_3
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@5
    const/4 v2, -0x1

    #@6
    const/16 v3, -0x100

    #@8
    invoke-interface {v1, v2, v3, p1, v0}, Landroid/hardware/input/IInputManager;->hasKeys(II[I[Z)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_b} :catch_c

    #@b
    .line 584
    :goto_b
    return-object v0

    #@c
    .line 581
    :catch_c
    move-exception v1

    #@d
    goto :goto_b
.end method

.method public getCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    .line 380
    if-nez p1, :cond_a

    #@2
    .line 381
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 385
    :cond_a
    :try_start_a
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@c
    invoke-interface {v1, p1}, Landroid/hardware/input/IInputManager;->getCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;)Ljava/lang/String;
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result-object v1

    #@10
    .line 388
    :goto_10
    return-object v1

    #@11
    .line 386
    :catch_11
    move-exception v0

    #@12
    .line 387
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@14
    const-string v2, "Could not get current keyboard layout for input device."

    #@16
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19
    .line 388
    const/4 v1, 0x0

    #@1a
    goto :goto_10
.end method

.method public getInputDevice(I)Landroid/view/InputDevice;
    .registers 8
    .parameter "id"

    #@0
    .prologue
    .line 200
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 201
    :try_start_3
    invoke-direct {p0}, Landroid/hardware/input/InputManager;->populateInputDevicesLocked()V

    #@6
    .line 203
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@b
    move-result v1

    #@c
    .line 204
    .local v1, index:I
    if-gez v1, :cond_11

    #@e
    .line 205
    const/4 v2, 0x0

    #@f
    monitor-exit v4

    #@10
    .line 217
    :goto_10
    return-object v2

    #@11
    .line 208
    :cond_11
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Landroid/view/InputDevice;
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_28

    #@19
    .line 209
    .local v2, inputDevice:Landroid/view/InputDevice;
    if-nez v2, :cond_21

    #@1b
    .line 211
    :try_start_1b
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@1d
    invoke-interface {v3, p1}, Landroid/hardware/input/IInputManager;->getInputDevice(I)Landroid/view/InputDevice;
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_28
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_20} :catch_2b

    #@20
    move-result-object v2

    #@21
    .line 216
    :cond_21
    :try_start_21
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@23
    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    #@26
    .line 217
    monitor-exit v4

    #@27
    goto :goto_10

    #@28
    .line 218
    .end local v1           #index:I
    .end local v2           #inputDevice:Landroid/view/InputDevice;
    :catchall_28
    move-exception v3

    #@29
    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_28

    #@2a
    throw v3

    #@2b
    .line 212
    .restart local v1       #index:I
    .restart local v2       #inputDevice:Landroid/view/InputDevice;
    :catch_2b
    move-exception v0

    #@2c
    .line 213
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2c
    new-instance v3, Ljava/lang/RuntimeException;

    #@2e
    const-string v5, "Could not get input device information."

    #@30
    invoke-direct {v3, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@33
    throw v3
    :try_end_34
    .catchall {:try_start_2c .. :try_end_34} :catchall_28
.end method

.method public getInputDeviceByDescriptor(Ljava/lang/String;)Landroid/view/InputDevice;
    .registers 9
    .parameter "descriptor"

    #@0
    .prologue
    .line 228
    if-nez p1, :cond_a

    #@2
    .line 229
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v6, "descriptor must not be null."

    #@6
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v5

    #@a
    .line 232
    :cond_a
    iget-object v6, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@c
    monitor-enter v6

    #@d
    .line 233
    :try_start_d
    invoke-direct {p0}, Landroid/hardware/input/InputManager;->populateInputDevicesLocked()V

    #@10
    .line 235
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@12
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    #@15
    move-result v4

    #@16
    .line 236
    .local v4, numDevices:I
    const/4 v1, 0x0

    #@17
    .local v1, i:I
    :goto_17
    if-ge v1, v4, :cond_44

    #@19
    .line 237
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@1b
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    check-cast v3, Landroid/view/InputDevice;

    #@21
    .line 238
    .local v3, inputDevice:Landroid/view/InputDevice;
    if-nez v3, :cond_34

    #@23
    .line 239
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@25
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->keyAt(I)I
    :try_end_28
    .catchall {:try_start_d .. :try_end_28} :catchall_47

    #@28
    move-result v2

    #@29
    .line 241
    .local v2, id:I
    :try_start_29
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@2b
    invoke-interface {v5, v2}, Landroid/hardware/input/IInputManager;->getInputDevice(I)Landroid/view/InputDevice;
    :try_end_2e
    .catchall {:try_start_29 .. :try_end_2e} :catchall_47
    .catch Landroid/os/RemoteException; {:try_start_29 .. :try_end_2e} :catch_40

    #@2e
    move-result-object v3

    #@2f
    .line 246
    :try_start_2f
    iget-object v5, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@31
    invoke-virtual {v5, v1, v3}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    #@34
    .line 248
    .end local v2           #id:I
    :cond_34
    invoke-virtual {v3}, Landroid/view/InputDevice;->getDescriptor()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_41

    #@3e
    .line 249
    monitor-exit v6

    #@3f
    .line 252
    .end local v3           #inputDevice:Landroid/view/InputDevice;
    :goto_3f
    return-object v3

    #@40
    .line 242
    .restart local v2       #id:I
    .restart local v3       #inputDevice:Landroid/view/InputDevice;
    :catch_40
    move-exception v0

    #@41
    .line 236
    .end local v2           #id:I
    :cond_41
    add-int/lit8 v1, v1, 0x1

    #@43
    goto :goto_17

    #@44
    .line 252
    .end local v3           #inputDevice:Landroid/view/InputDevice;
    :cond_44
    const/4 v3, 0x0

    #@45
    monitor-exit v6

    #@46
    goto :goto_3f

    #@47
    .line 253
    .end local v1           #i:I
    .end local v4           #numDevices:I
    :catchall_47
    move-exception v5

    #@48
    monitor-exit v6
    :try_end_49
    .catchall {:try_start_2f .. :try_end_49} :catchall_47

    #@49
    throw v5
.end method

.method public getInputDeviceIds()[I
    .registers 6

    #@0
    .prologue
    .line 261
    iget-object v4, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 262
    :try_start_3
    invoke-direct {p0}, Landroid/hardware/input/InputManager;->populateInputDevicesLocked()V

    #@6
    .line 264
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@b
    move-result v0

    #@c
    .line 265
    .local v0, count:I
    new-array v2, v0, [I

    #@e
    .line 266
    .local v2, ids:[I
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_1c

    #@11
    .line 267
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevices:Landroid/util/SparseArray;

    #@13
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@16
    move-result v3

    #@17
    aput v3, v2, v1

    #@19
    .line 266
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_f

    #@1c
    .line 269
    :cond_1c
    monitor-exit v4

    #@1d
    return-object v2

    #@1e
    .line 270
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v2           #ids:[I
    :catchall_1e
    move-exception v3

    #@1f
    monitor-exit v4
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v3
.end method

.method public getInputDeviceVibrator(I)Landroid/os/Vibrator;
    .registers 3
    .parameter "deviceId"

    #@0
    .prologue
    .line 718
    new-instance v0, Landroid/hardware/input/InputManager$InputDeviceVibrator;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/hardware/input/InputManager$InputDeviceVibrator;-><init>(Landroid/hardware/input/InputManager;I)V

    #@5
    return-object v0
.end method

.method public getKeyboardLayout(Ljava/lang/String;)Landroid/hardware/input/KeyboardLayout;
    .registers 5
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 359
    if-nez p1, :cond_b

    #@2
    .line 360
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "keyboardLayoutDescriptor must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 364
    :cond_b
    :try_start_b
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@d
    invoke-interface {v1, p1}, Landroid/hardware/input/IInputManager;->getKeyboardLayout(Ljava/lang/String;)Landroid/hardware/input/KeyboardLayout;
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_10} :catch_12

    #@10
    move-result-object v1

    #@11
    .line 367
    :goto_11
    return-object v1

    #@12
    .line 365
    :catch_12
    move-exception v0

    #@13
    .line 366
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@15
    const-string v2, "Could not get keyboard layout information."

    #@17
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 367
    const/4 v1, 0x0

    #@1b
    goto :goto_11
.end method

.method public getKeyboardLayouts()[Landroid/hardware/input/KeyboardLayout;
    .registers 4

    #@0
    .prologue
    .line 342
    :try_start_0
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@2
    invoke-interface {v1}, Landroid/hardware/input/IInputManager;->getKeyboardLayouts()[Landroid/hardware/input/KeyboardLayout;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 345
    :goto_6
    return-object v1

    #@7
    .line 343
    :catch_7
    move-exception v0

    #@8
    .line 344
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@a
    const-string v2, "Could not get list of keyboard layout informations."

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 345
    const/4 v1, 0x0

    #@10
    new-array v1, v1, [Landroid/hardware/input/KeyboardLayout;

    #@12
    goto :goto_6
.end method

.method public getKeyboardLayoutsForInputDevice(Ljava/lang/String;)[Ljava/lang/String;
    .registers 5
    .parameter "inputDeviceDescriptor"

    #@0
    .prologue
    .line 430
    if-nez p1, :cond_a

    #@2
    .line 431
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 435
    :cond_a
    :try_start_a
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@c
    invoke-interface {v1, p1}, Landroid/hardware/input/IInputManager;->getKeyboardLayoutsForInputDevice(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result-object v1

    #@10
    .line 438
    :goto_10
    return-object v1

    #@11
    .line 436
    :catch_11
    move-exception v0

    #@12
    .line 437
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@14
    const-string v2, "Could not get keyboard layouts for input device."

    #@16
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19
    .line 438
    const-class v1, Ljava/lang/String;

    #@1b
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, [Ljava/lang/String;

    #@21
    goto :goto_10
.end method

.method public getPointerSpeed(Landroid/content/Context;)I
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 512
    const/4 v0, 0x0

    #@1
    .line 514
    .local v0, speed:I
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v1

    #@5
    const-string/jumbo v2, "pointer_speed"

    #@8
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_b
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_b} :catch_d

    #@b
    move-result v0

    #@c
    .line 518
    :goto_c
    return v0

    #@d
    .line 516
    :catch_d
    move-exception v1

    #@e
    goto :goto_c
.end method

.method public injectInputEvent(Landroid/view/InputEvent;I)Z
    .registers 6
    .parameter "event"
    .parameter "mode"

    #@0
    .prologue
    .line 609
    if-nez p1, :cond_a

    #@2
    .line 610
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "event must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 612
    :cond_a
    if-eqz p2, :cond_1b

    #@c
    const/4 v1, 0x2

    #@d
    if-eq p2, v1, :cond_1b

    #@f
    const/4 v1, 0x1

    #@10
    if-eq p2, v1, :cond_1b

    #@12
    .line 615
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@14
    const-string/jumbo v2, "mode is invalid"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 619
    :cond_1b
    :try_start_1b
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@1d
    invoke-interface {v1, p1, p2}, Landroid/hardware/input/IInputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_20} :catch_22

    #@20
    move-result v1

    #@21
    .line 621
    :goto_21
    return v1

    #@22
    .line 620
    :catch_22
    move-exception v0

    #@23
    .line 621
    .local v0, ex:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@24
    goto :goto_21
.end method

.method public registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V
    .registers 7
    .parameter "listener"
    .parameter "handler"

    #@0
    .prologue
    .line 284
    if-nez p1, :cond_b

    #@2
    .line 285
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "listener must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 288
    :cond_b
    iget-object v2, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@d
    monitor-enter v2

    #@e
    .line 289
    :try_start_e
    invoke-direct {p0, p1}, Landroid/hardware/input/InputManager;->findInputDeviceListenerLocked(Landroid/hardware/input/InputManager$InputDeviceListener;)I

    #@11
    move-result v0

    #@12
    .line 290
    .local v0, index:I
    if-gez v0, :cond_1e

    #@14
    .line 291
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@16
    new-instance v3, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;

    #@18
    invoke-direct {v3, p1, p2}, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;-><init>(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    #@1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 293
    :cond_1e
    monitor-exit v2

    #@1f
    .line 294
    return-void

    #@20
    .line 293
    .end local v0           #index:I
    :catchall_20
    move-exception v1

    #@21
    monitor-exit v2
    :try_end_22
    .catchall {:try_start_e .. :try_end_22} :catchall_20

    #@22
    throw v1
.end method

.method public removeKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 484
    if-nez p1, :cond_a

    #@2
    .line 485
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 487
    :cond_a
    if-nez p2, :cond_15

    #@c
    .line 488
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v2, "keyboardLayoutDescriptor must not be null"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 492
    :cond_15
    :try_start_15
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@17
    invoke-interface {v1, p1, p2}, Landroid/hardware/input/IInputManager;->removeKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_1b

    #@1a
    .line 496
    :goto_1a
    return-void

    #@1b
    .line 493
    :catch_1b
    move-exception v0

    #@1c
    .line 494
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@1e
    const-string v2, "Could not remove keyboard layout for input device."

    #@20
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_1a
.end method

.method public setCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "inputDeviceDescriptor"
    .parameter "keyboardLayoutDescriptor"

    #@0
    .prologue
    .line 406
    if-nez p1, :cond_a

    #@2
    .line 407
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "inputDeviceDescriptor must not be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 409
    :cond_a
    if-nez p2, :cond_15

    #@c
    .line 410
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v2, "keyboardLayoutDescriptor must not be null"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 414
    :cond_15
    :try_start_15
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@17
    invoke-interface {v1, p1, p2}, Landroid/hardware/input/IInputManager;->setCurrentKeyboardLayoutForInputDevice(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1a} :catch_1b

    #@1a
    .line 419
    :goto_1a
    return-void

    #@1b
    .line 416
    :catch_1b
    move-exception v0

    #@1c
    .line 417
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@1e
    const-string v2, "Could not set current keyboard layout for input device."

    #@20
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_1a
.end method

.method public setPointerSpeed(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "speed"

    #@0
    .prologue
    .line 534
    const/4 v0, -0x7

    #@1
    if-lt p2, v0, :cond_6

    #@3
    const/4 v0, 0x7

    #@4
    if-le p2, v0, :cond_f

    #@6
    .line 535
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v1, "speed out of range"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 538
    :cond_f
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v0

    #@13
    const-string/jumbo v1, "pointer_speed"

    #@16
    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@19
    .line 540
    return-void
.end method

.method public tryPointerSpeed(I)V
    .registers 5
    .parameter "speed"

    #@0
    .prologue
    .line 554
    const/4 v1, -0x7

    #@1
    if-lt p1, v1, :cond_6

    #@3
    const/4 v1, 0x7

    #@4
    if-le p1, v1, :cond_f

    #@6
    .line 555
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@8
    const-string/jumbo v2, "speed out of range"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 559
    :cond_f
    :try_start_f
    iget-object v1, p0, Landroid/hardware/input/InputManager;->mIm:Landroid/hardware/input/IInputManager;

    #@11
    invoke-interface {v1, p1}, Landroid/hardware/input/IInputManager;->tryPointerSpeed(I)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_15

    #@14
    .line 563
    :goto_14
    return-void

    #@15
    .line 560
    :catch_15
    move-exception v0

    #@16
    .line 561
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "InputManager"

    #@18
    const-string v2, "Could not set temporary pointer speed."

    #@1a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    goto :goto_14
.end method

.method public unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 304
    if-nez p1, :cond_b

    #@2
    .line 305
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "listener must not be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 308
    :cond_b
    iget-object v3, p0, Landroid/hardware/input/InputManager;->mInputDevicesLock:Ljava/lang/Object;

    #@d
    monitor-enter v3

    #@e
    .line 309
    :try_start_e
    invoke-direct {p0, p1}, Landroid/hardware/input/InputManager;->findInputDeviceListenerLocked(Landroid/hardware/input/InputManager$InputDeviceListener;)I

    #@11
    move-result v1

    #@12
    .line 310
    .local v1, index:I
    if-ltz v1, :cond_25

    #@14
    .line 311
    iget-object v2, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;

    #@1c
    .line 312
    .local v0, d:Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;
    const/4 v2, 0x0

    #@1d
    invoke-virtual {v0, v2}, Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@20
    .line 313
    iget-object v2, p0, Landroid/hardware/input/InputManager;->mInputDeviceListeners:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@25
    .line 315
    .end local v0           #d:Landroid/hardware/input/InputManager$InputDeviceListenerDelegate;
    :cond_25
    monitor-exit v3

    #@26
    .line 316
    return-void

    #@27
    .line 315
    .end local v1           #index:I
    :catchall_27
    move-exception v2

    #@28
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_e .. :try_end_29} :catchall_27

    #@29
    throw v2
.end method
