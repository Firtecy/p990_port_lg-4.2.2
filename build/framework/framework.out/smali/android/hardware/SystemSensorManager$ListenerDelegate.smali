.class Landroid/hardware/SystemSensorManager$ListenerDelegate;
.super Ljava/lang/Object;
.source "SystemSensorManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/SystemSensorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerDelegate"
.end annotation


# instance fields
.field public mFirstEvent:Landroid/util/SparseBooleanArray;

.field private final mHandler:Landroid/os/Handler;

.field public mSensorAccuracies:Landroid/util/SparseIntArray;

.field private final mSensorEventListener:Landroid/hardware/SensorEventListener;

.field private final mSensorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation
.end field

.field public mSensors:Landroid/util/SparseBooleanArray;

.field final synthetic this$0:Landroid/hardware/SystemSensorManager;


# direct methods
.method constructor <init>(Landroid/hardware/SystemSensorManager;Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;Landroid/os/Handler;)V
    .registers 7
    .parameter
    .parameter "listener"
    .parameter "sensor"
    .parameter "handler"

    #@0
    .prologue
    .line 171
    iput-object p1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->this$0:Landroid/hardware/SystemSensorManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 165
    new-instance v1, Ljava/util/ArrayList;

    #@7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorList:Ljava/util/ArrayList;

    #@c
    .line 167
    new-instance v1, Landroid/util/SparseBooleanArray;

    #@e
    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    #@11
    iput-object v1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensors:Landroid/util/SparseBooleanArray;

    #@13
    .line 168
    new-instance v1, Landroid/util/SparseBooleanArray;

    #@15
    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    #@18
    iput-object v1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mFirstEvent:Landroid/util/SparseBooleanArray;

    #@1a
    .line 169
    new-instance v1, Landroid/util/SparseIntArray;

    #@1c
    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    #@1f
    iput-object v1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorAccuracies:Landroid/util/SparseIntArray;

    #@21
    .line 172
    iput-object p2, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    #@23
    .line 173
    if-eqz p4, :cond_34

    #@25
    invoke-virtual {p4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@28
    move-result-object v0

    #@29
    .line 177
    .local v0, looper:Landroid/os/Looper;
    :goto_29
    new-instance v1, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;

    #@2b
    invoke-direct {v1, p0, v0, p1}, Landroid/hardware/SystemSensorManager$ListenerDelegate$1;-><init>(Landroid/hardware/SystemSensorManager$ListenerDelegate;Landroid/os/Looper;Landroid/hardware/SystemSensorManager;)V

    #@2e
    iput-object v1, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mHandler:Landroid/os/Handler;

    #@30
    .line 208
    invoke-virtual {p0, p3}, Landroid/hardware/SystemSensorManager$ListenerDelegate;->addSensor(Landroid/hardware/Sensor;)V

    #@33
    .line 209
    return-void

    #@34
    .line 173
    .end local v0           #looper:Landroid/os/Looper;
    :cond_34
    iget-object v0, p1, Landroid/hardware/SystemSensorManager;->mMainLooper:Landroid/os/Looper;

    #@36
    goto :goto_29
.end method

.method static synthetic access$100(Landroid/hardware/SystemSensorManager$ListenerDelegate;)Landroid/hardware/SensorEventListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    #@2
    return-object v0
.end method


# virtual methods
.method addSensor(Landroid/hardware/Sensor;)V
    .registers 5
    .parameter "sensor"

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensors:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getHandle()I

    #@5
    move-result v1

    #@6
    const/4 v2, 0x1

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@a
    .line 217
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f
    .line 218
    return-void
.end method

.method getListener()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    #@2
    return-object v0
.end method

.method getSensors()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method hasSensor(Landroid/hardware/Sensor;)Z
    .registers 4
    .parameter "sensor"

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensors:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getHandle()I

    #@5
    move-result v1

    #@6
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method onSensorChangedLocked(Landroid/hardware/Sensor;[F[JI)V
    .registers 12
    .parameter "sensor"
    .parameter "values"
    .parameter "timestamp"
    .parameter "accuracy"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 232
    sget-object v3, Landroid/hardware/SystemSensorManager;->sPool:Landroid/hardware/SensorManager$SensorEventPool;

    #@5
    invoke-virtual {v3}, Landroid/hardware/SensorManager$SensorEventPool;->getFromPool()Landroid/hardware/SensorEvent;

    #@8
    move-result-object v1

    #@9
    .line 233
    .local v1, t:Landroid/hardware/SensorEvent;
    iget-object v2, v1, Landroid/hardware/SensorEvent;->values:[F

    #@b
    .line 234
    .local v2, v:[F
    aget v3, p2, v5

    #@d
    aput v3, v2, v5

    #@f
    .line 235
    aget v3, p2, v6

    #@11
    aput v3, v2, v6

    #@13
    .line 236
    aget v3, p2, v4

    #@15
    aput v3, v2, v4

    #@17
    .line 237
    aget-wide v3, p3, v5

    #@19
    iput-wide v3, v1, Landroid/hardware/SensorEvent;->timestamp:J

    #@1b
    .line 238
    iput p4, v1, Landroid/hardware/SensorEvent;->accuracy:I

    #@1d
    .line 239
    iput-object p1, v1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    #@1f
    .line 240
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@22
    move-result-object v0

    #@23
    .line 241
    .local v0, msg:Landroid/os/Message;
    iput v5, v0, Landroid/os/Message;->what:I

    #@25
    .line 242
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@27
    .line 243
    invoke-virtual {v0, v6}, Landroid/os/Message;->setAsynchronous(Z)V

    #@2a
    .line 244
    iget-object v3, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mHandler:Landroid/os/Handler;

    #@2c
    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2f
    .line 245
    return-void
.end method

.method removeSensor(Landroid/hardware/Sensor;)I
    .registers 4
    .parameter "sensor"

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensors:Landroid/util/SparseBooleanArray;

    #@2
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getHandle()I

    #@5
    move-result v1

    #@6
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->delete(I)V

    #@9
    .line 221
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensorList:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@e
    .line 222
    iget-object v0, p0, Landroid/hardware/SystemSensorManager$ListenerDelegate;->mSensors:Landroid/util/SparseBooleanArray;

    #@10
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    #@13
    move-result v0

    #@14
    return v0
.end method
