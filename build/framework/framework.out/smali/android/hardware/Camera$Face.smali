.class public Landroid/hardware/Camera$Face;
.super Ljava/lang/Object;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Face"
.end annotation


# instance fields
.field public blinkDetected:I

.field public faceRecognised:I

.field public id:I

.field public leftEye:Landroid/graphics/Point;

.field public mouth:Landroid/graphics/Point;

.field public rect:Landroid/graphics/Rect;

.field public rightEye:Landroid/graphics/Point;

.field public score:I

.field public smileDegree:I

.field public smileScore:I


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1455
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1515
    const/4 v0, -0x1

    #@6
    iput v0, p0, Landroid/hardware/Camera$Face;->id:I

    #@8
    .line 1524
    iput-object v2, p0, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    #@a
    .line 1533
    iput-object v2, p0, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    #@c
    .line 1542
    iput-object v2, p0, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    #@e
    .line 1547
    iput v1, p0, Landroid/hardware/Camera$Face;->smileDegree:I

    #@10
    .line 1551
    iput v1, p0, Landroid/hardware/Camera$Face;->smileScore:I

    #@12
    .line 1555
    iput v1, p0, Landroid/hardware/Camera$Face;->blinkDetected:I

    #@14
    .line 1559
    iput v1, p0, Landroid/hardware/Camera$Face;->faceRecognised:I

    #@16
    .line 1456
    return-void
.end method
