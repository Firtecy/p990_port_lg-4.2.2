.class public abstract Landroid/hardware/SensorManager;
.super Ljava/lang/Object;
.source "SensorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/SensorManager$SensorEventPool;
    }
.end annotation


# static fields
.field public static final AXIS_MINUS_X:I = 0x81

.field public static final AXIS_MINUS_Y:I = 0x82

.field public static final AXIS_MINUS_Z:I = 0x83

.field public static final AXIS_X:I = 0x1

.field public static final AXIS_Y:I = 0x2

.field public static final AXIS_Z:I = 0x3

.field public static final DATA_X:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATA_Y:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATA_Z:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GRAVITY_DEATH_STAR_I:F = 3.5303614E-7f

.field public static final GRAVITY_EARTH:F = 9.80665f

.field public static final GRAVITY_JUPITER:F = 23.12f

.field public static final GRAVITY_MARS:F = 3.71f

.field public static final GRAVITY_MERCURY:F = 3.7f

.field public static final GRAVITY_MOON:F = 1.6f

.field public static final GRAVITY_NEPTUNE:F = 11.0f

.field public static final GRAVITY_PLUTO:F = 0.6f

.field public static final GRAVITY_SATURN:F = 8.96f

.field public static final GRAVITY_SUN:F = 275.0f

.field public static final GRAVITY_THE_ISLAND:F = 4.815162f

.field public static final GRAVITY_URANUS:F = 8.69f

.field public static final GRAVITY_VENUS:F = 8.87f

.field public static final LIGHT_CLOUDY:F = 100.0f

.field public static final LIGHT_FULLMOON:F = 0.25f

.field public static final LIGHT_NO_MOON:F = 0.001f

.field public static final LIGHT_OVERCAST:F = 10000.0f

.field public static final LIGHT_SHADE:F = 20000.0f

.field public static final LIGHT_SUNLIGHT:F = 110000.0f

.field public static final LIGHT_SUNLIGHT_MAX:F = 120000.0f

.field public static final LIGHT_SUNRISE:F = 400.0f

.field public static final MAGNETIC_FIELD_EARTH_MAX:F = 60.0f

.field public static final MAGNETIC_FIELD_EARTH_MIN:F = 30.0f

.field public static final PRESSURE_STANDARD_ATMOSPHERE:F = 1013.25f

.field public static final RAW_DATA_INDEX:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RAW_DATA_X:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RAW_DATA_Y:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RAW_DATA_Z:I = 0x5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_ACCELEROMETER:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_ALL:I = 0x7f
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_DELAY_FASTEST:I = 0x0

.field public static final SENSOR_DELAY_GAME:I = 0x1

.field public static final SENSOR_DELAY_NORMAL:I = 0x3

.field public static final SENSOR_DELAY_UI:I = 0x2

.field public static final SENSOR_LIGHT:I = 0x10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_MAGNETIC_FIELD:I = 0x8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_MAX:I = 0x40
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_MIN:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_ORIENTATION:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_ORIENTATION_RAW:I = 0x80
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_PROXIMITY:I = 0x20
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_STATUS_ACCURACY_HIGH:I = 0x3

.field public static final SENSOR_STATUS_ACCURACY_LOW:I = 0x1

.field public static final SENSOR_STATUS_ACCURACY_MEDIUM:I = 0x2

.field public static final SENSOR_STATUS_UNRELIABLE:I = 0x0

.field public static final SENSOR_TEMPERATURE:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SENSOR_TRICORDER:I = 0x40
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final STANDARD_GRAVITY:F = 9.80665f

.field protected static final TAG:Ljava/lang/String; = "SensorManager"

.field private static final mTempMatrix:[F


# instance fields
.field private mLegacySensorManager:Landroid/hardware/LegacySensorManager;

.field private final mSensorListByType:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Sensor;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 79
    const/16 v0, 0x10

    #@2
    new-array v0, v0, [F

    #@4
    sput-object v0, Landroid/hardware/SensorManager;->mTempMatrix:[F

    #@6
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 357
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 82
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Landroid/hardware/SensorManager;->mSensorListByType:Landroid/util/SparseArray;

    #@a
    .line 358
    return-void
.end method

.method public static getAltitude(FF)F
    .registers 9
    .parameter "p0"
    .parameter "p"

    #@0
    .prologue
    .line 1119
    const v0, 0x3e42dcae

    #@3
    .line 1120
    .local v0, coef:F
    const v1, 0x472d2a00

    #@6
    const/high16 v2, 0x3f80

    #@8
    div-float v3, p1, p0

    #@a
    float-to-double v3, v3

    #@b
    const-wide v5, 0x3fc85b95c0000000L

    #@10
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    #@13
    move-result-wide v3

    #@14
    double-to-float v3, v3

    #@15
    sub-float/2addr v2, v3

    #@16
    mul-float/2addr v1, v2

    #@17
    return v1
.end method

.method public static getAngleChange([F[F[F)V
    .registers 33
    .parameter "angleChange"
    .parameter "R"
    .parameter "prevR"

    #@0
    .prologue
    .line 1150
    const/4 v11, 0x0

    #@1
    .local v11, rd1:F
    const/4 v12, 0x0

    #@2
    .local v12, rd4:F
    const/4 v13, 0x0

    #@3
    .local v13, rd6:F
    const/4 v14, 0x0

    #@4
    .local v14, rd7:F
    const/4 v15, 0x0

    #@5
    .line 1151
    .local v15, rd8:F
    const/16 v16, 0x0

    #@7
    .local v16, ri0:F
    const/16 v17, 0x0

    #@9
    .local v17, ri1:F
    const/16 v18, 0x0

    #@b
    .local v18, ri2:F
    const/16 v19, 0x0

    #@d
    .local v19, ri3:F
    const/16 v20, 0x0

    #@f
    .local v20, ri4:F
    const/16 v21, 0x0

    #@11
    .local v21, ri5:F
    const/16 v22, 0x0

    #@13
    .local v22, ri6:F
    const/16 v23, 0x0

    #@15
    .local v23, ri7:F
    const/16 v24, 0x0

    #@17
    .line 1152
    .local v24, ri8:F
    const/4 v2, 0x0

    #@18
    .local v2, pri0:F
    const/4 v3, 0x0

    #@19
    .local v3, pri1:F
    const/4 v4, 0x0

    #@1a
    .local v4, pri2:F
    const/4 v5, 0x0

    #@1b
    .local v5, pri3:F
    const/4 v6, 0x0

    #@1c
    .local v6, pri4:F
    const/4 v7, 0x0

    #@1d
    .local v7, pri5:F
    const/4 v8, 0x0

    #@1e
    .local v8, pri6:F
    const/4 v9, 0x0

    #@1f
    .local v9, pri7:F
    const/4 v10, 0x0

    #@20
    .line 1154
    .local v10, pri8:F
    move-object/from16 v0, p1

    #@22
    array-length v0, v0

    #@23
    move/from16 v25, v0

    #@25
    const/16 v26, 0x9

    #@27
    move/from16 v0, v25

    #@29
    move/from16 v1, v26

    #@2b
    if-ne v0, v1, :cond_f5

    #@2d
    .line 1155
    const/16 v25, 0x0

    #@2f
    aget v16, p1, v25

    #@31
    .line 1156
    const/16 v25, 0x1

    #@33
    aget v17, p1, v25

    #@35
    .line 1157
    const/16 v25, 0x2

    #@37
    aget v18, p1, v25

    #@39
    .line 1158
    const/16 v25, 0x3

    #@3b
    aget v19, p1, v25

    #@3d
    .line 1159
    const/16 v25, 0x4

    #@3f
    aget v20, p1, v25

    #@41
    .line 1160
    const/16 v25, 0x5

    #@43
    aget v21, p1, v25

    #@45
    .line 1161
    const/16 v25, 0x6

    #@47
    aget v22, p1, v25

    #@49
    .line 1162
    const/16 v25, 0x7

    #@4b
    aget v23, p1, v25

    #@4d
    .line 1163
    const/16 v25, 0x8

    #@4f
    aget v24, p1, v25

    #@51
    .line 1176
    :cond_51
    :goto_51
    move-object/from16 v0, p2

    #@53
    array-length v0, v0

    #@54
    move/from16 v25, v0

    #@56
    const/16 v26, 0x9

    #@58
    move/from16 v0, v25

    #@5a
    move/from16 v1, v26

    #@5c
    if-ne v0, v1, :cond_128

    #@5e
    .line 1177
    const/16 v25, 0x0

    #@60
    aget v2, p2, v25

    #@62
    .line 1178
    const/16 v25, 0x1

    #@64
    aget v3, p2, v25

    #@66
    .line 1179
    const/16 v25, 0x2

    #@68
    aget v4, p2, v25

    #@6a
    .line 1180
    const/16 v25, 0x3

    #@6c
    aget v5, p2, v25

    #@6e
    .line 1181
    const/16 v25, 0x4

    #@70
    aget v6, p2, v25

    #@72
    .line 1182
    const/16 v25, 0x5

    #@74
    aget v7, p2, v25

    #@76
    .line 1183
    const/16 v25, 0x6

    #@78
    aget v8, p2, v25

    #@7a
    .line 1184
    const/16 v25, 0x7

    #@7c
    aget v9, p2, v25

    #@7e
    .line 1185
    const/16 v25, 0x8

    #@80
    aget v10, p2, v25

    #@82
    .line 1201
    :cond_82
    :goto_82
    mul-float v25, v2, v17

    #@84
    mul-float v26, v5, v20

    #@86
    add-float v25, v25, v26

    #@88
    mul-float v26, v8, v23

    #@8a
    add-float v11, v25, v26

    #@8c
    .line 1202
    mul-float v25, v3, v17

    #@8e
    mul-float v26, v6, v20

    #@90
    add-float v25, v25, v26

    #@92
    mul-float v26, v9, v23

    #@94
    add-float v12, v25, v26

    #@96
    .line 1203
    mul-float v25, v4, v16

    #@98
    mul-float v26, v7, v19

    #@9a
    add-float v25, v25, v26

    #@9c
    mul-float v26, v10, v22

    #@9e
    add-float v13, v25, v26

    #@a0
    .line 1204
    mul-float v25, v4, v17

    #@a2
    mul-float v26, v7, v20

    #@a4
    add-float v25, v25, v26

    #@a6
    mul-float v26, v10, v23

    #@a8
    add-float v14, v25, v26

    #@aa
    .line 1205
    mul-float v25, v4, v18

    #@ac
    mul-float v26, v7, v21

    #@ae
    add-float v25, v25, v26

    #@b0
    mul-float v26, v10, v24

    #@b2
    add-float v15, v25, v26

    #@b4
    .line 1207
    const/16 v25, 0x0

    #@b6
    float-to-double v0, v11

    #@b7
    move-wide/from16 v26, v0

    #@b9
    float-to-double v0, v12

    #@ba
    move-wide/from16 v28, v0

    #@bc
    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->atan2(DD)D

    #@bf
    move-result-wide v26

    #@c0
    move-wide/from16 v0, v26

    #@c2
    double-to-float v0, v0

    #@c3
    move/from16 v26, v0

    #@c5
    aput v26, p0, v25

    #@c7
    .line 1208
    const/16 v25, 0x1

    #@c9
    neg-float v0, v14

    #@ca
    move/from16 v26, v0

    #@cc
    move/from16 v0, v26

    #@ce
    float-to-double v0, v0

    #@cf
    move-wide/from16 v26, v0

    #@d1
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->asin(D)D

    #@d4
    move-result-wide v26

    #@d5
    move-wide/from16 v0, v26

    #@d7
    double-to-float v0, v0

    #@d8
    move/from16 v26, v0

    #@da
    aput v26, p0, v25

    #@dc
    .line 1209
    const/16 v25, 0x2

    #@de
    neg-float v0, v13

    #@df
    move/from16 v26, v0

    #@e1
    move/from16 v0, v26

    #@e3
    float-to-double v0, v0

    #@e4
    move-wide/from16 v26, v0

    #@e6
    float-to-double v0, v15

    #@e7
    move-wide/from16 v28, v0

    #@e9
    invoke-static/range {v26 .. v29}, Ljava/lang/Math;->atan2(DD)D

    #@ec
    move-result-wide v26

    #@ed
    move-wide/from16 v0, v26

    #@ef
    double-to-float v0, v0

    #@f0
    move/from16 v26, v0

    #@f2
    aput v26, p0, v25

    #@f4
    .line 1211
    return-void

    #@f5
    .line 1164
    :cond_f5
    move-object/from16 v0, p1

    #@f7
    array-length v0, v0

    #@f8
    move/from16 v25, v0

    #@fa
    const/16 v26, 0x10

    #@fc
    move/from16 v0, v25

    #@fe
    move/from16 v1, v26

    #@100
    if-ne v0, v1, :cond_51

    #@102
    .line 1165
    const/16 v25, 0x0

    #@104
    aget v16, p1, v25

    #@106
    .line 1166
    const/16 v25, 0x1

    #@108
    aget v17, p1, v25

    #@10a
    .line 1167
    const/16 v25, 0x2

    #@10c
    aget v18, p1, v25

    #@10e
    .line 1168
    const/16 v25, 0x4

    #@110
    aget v19, p1, v25

    #@112
    .line 1169
    const/16 v25, 0x5

    #@114
    aget v20, p1, v25

    #@116
    .line 1170
    const/16 v25, 0x6

    #@118
    aget v21, p1, v25

    #@11a
    .line 1171
    const/16 v25, 0x8

    #@11c
    aget v22, p1, v25

    #@11e
    .line 1172
    const/16 v25, 0x9

    #@120
    aget v23, p1, v25

    #@122
    .line 1173
    const/16 v25, 0xa

    #@124
    aget v24, p1, v25

    #@126
    goto/16 :goto_51

    #@128
    .line 1186
    :cond_128
    move-object/from16 v0, p2

    #@12a
    array-length v0, v0

    #@12b
    move/from16 v25, v0

    #@12d
    const/16 v26, 0x10

    #@12f
    move/from16 v0, v25

    #@131
    move/from16 v1, v26

    #@133
    if-ne v0, v1, :cond_82

    #@135
    .line 1187
    const/16 v25, 0x0

    #@137
    aget v2, p2, v25

    #@139
    .line 1188
    const/16 v25, 0x1

    #@13b
    aget v3, p2, v25

    #@13d
    .line 1189
    const/16 v25, 0x2

    #@13f
    aget v4, p2, v25

    #@141
    .line 1190
    const/16 v25, 0x4

    #@143
    aget v5, p2, v25

    #@145
    .line 1191
    const/16 v25, 0x5

    #@147
    aget v6, p2, v25

    #@149
    .line 1192
    const/16 v25, 0x6

    #@14b
    aget v7, p2, v25

    #@14d
    .line 1193
    const/16 v25, 0x8

    #@14f
    aget v8, p2, v25

    #@151
    .line 1194
    const/16 v25, 0x9

    #@153
    aget v9, p2, v25

    #@155
    .line 1195
    const/16 v25, 0xa

    #@157
    aget v10, p2, v25

    #@159
    goto/16 :goto_82
.end method

.method public static getInclination([F)F
    .registers 5
    .parameter "I"

    #@0
    .prologue
    const/4 v2, 0x5

    #@1
    .line 864
    array-length v0, p0

    #@2
    const/16 v1, 0x9

    #@4
    if-ne v0, v1, :cond_13

    #@6
    .line 865
    aget v0, p0, v2

    #@8
    float-to-double v0, v0

    #@9
    const/4 v2, 0x4

    #@a
    aget v2, p0, v2

    #@c
    float-to-double v2, v2

    #@d
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@10
    move-result-wide v0

    #@11
    double-to-float v0, v0

    #@12
    .line 867
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x6

    #@14
    aget v0, p0, v0

    #@16
    float-to-double v0, v0

    #@17
    aget v2, p0, v2

    #@19
    float-to-double v2, v2

    #@1a
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@1d
    move-result-wide v0

    #@1e
    double-to-float v0, v0

    #@1f
    goto :goto_12
.end method

.method private getLegacySensorManager()Landroid/hardware/LegacySensorManager;
    .registers 4

    #@0
    .prologue
    .line 1308
    iget-object v1, p0, Landroid/hardware/SensorManager;->mSensorListByType:Landroid/util/SparseArray;

    #@2
    monitor-enter v1

    #@3
    .line 1309
    :try_start_3
    iget-object v0, p0, Landroid/hardware/SensorManager;->mLegacySensorManager:Landroid/hardware/LegacySensorManager;

    #@5
    if-nez v0, :cond_15

    #@7
    .line 1310
    const-string v0, "SensorManager"

    #@9
    const-string v2, "This application is using deprecated SensorManager API which will be removed someday.  Please consider switching to the new API."

    #@b
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1312
    new-instance v0, Landroid/hardware/LegacySensorManager;

    #@10
    invoke-direct {v0, p0}, Landroid/hardware/LegacySensorManager;-><init>(Landroid/hardware/SensorManager;)V

    #@13
    iput-object v0, p0, Landroid/hardware/SensorManager;->mLegacySensorManager:Landroid/hardware/LegacySensorManager;

    #@15
    .line 1314
    :cond_15
    iget-object v0, p0, Landroid/hardware/SensorManager;->mLegacySensorManager:Landroid/hardware/LegacySensorManager;

    #@17
    monitor-exit v1

    #@18
    return-object v0

    #@19
    .line 1315
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method public static getOrientation([F[F)[F
    .registers 11
    .parameter "R"
    .parameter "values"

    #@0
    .prologue
    const/16 v8, 0x9

    #@2
    const/16 v7, 0x8

    #@4
    const/4 v6, 0x2

    #@5
    const/4 v5, 0x0

    #@6
    const/4 v4, 0x1

    #@7
    .line 1077
    array-length v0, p0

    #@8
    if-ne v0, v8, :cond_34

    #@a
    .line 1078
    aget v0, p0, v4

    #@c
    float-to-double v0, v0

    #@d
    const/4 v2, 0x4

    #@e
    aget v2, p0, v2

    #@10
    float-to-double v2, v2

    #@11
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@14
    move-result-wide v0

    #@15
    double-to-float v0, v0

    #@16
    aput v0, p1, v5

    #@18
    .line 1079
    const/4 v0, 0x7

    #@19
    aget v0, p0, v0

    #@1b
    neg-float v0, v0

    #@1c
    float-to-double v0, v0

    #@1d
    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    #@20
    move-result-wide v0

    #@21
    double-to-float v0, v0

    #@22
    aput v0, p1, v4

    #@24
    .line 1080
    const/4 v0, 0x6

    #@25
    aget v0, p0, v0

    #@27
    neg-float v0, v0

    #@28
    float-to-double v0, v0

    #@29
    aget v2, p0, v7

    #@2b
    float-to-double v2, v2

    #@2c
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@2f
    move-result-wide v0

    #@30
    double-to-float v0, v0

    #@31
    aput v0, p1, v6

    #@33
    .line 1086
    :goto_33
    return-object p1

    #@34
    .line 1082
    :cond_34
    aget v0, p0, v4

    #@36
    float-to-double v0, v0

    #@37
    const/4 v2, 0x5

    #@38
    aget v2, p0, v2

    #@3a
    float-to-double v2, v2

    #@3b
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@3e
    move-result-wide v0

    #@3f
    double-to-float v0, v0

    #@40
    aput v0, p1, v5

    #@42
    .line 1083
    aget v0, p0, v8

    #@44
    neg-float v0, v0

    #@45
    float-to-double v0, v0

    #@46
    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    #@49
    move-result-wide v0

    #@4a
    double-to-float v0, v0

    #@4b
    aput v0, p1, v4

    #@4d
    .line 1084
    aget v0, p0, v7

    #@4f
    neg-float v0, v0

    #@50
    float-to-double v0, v0

    #@51
    const/16 v2, 0xa

    #@53
    aget v2, p0, v2

    #@55
    float-to-double v2, v2

    #@56
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    #@59
    move-result-wide v0

    #@5a
    double-to-float v0, v0

    #@5b
    aput v0, p1, v6

    #@5d
    goto :goto_33
.end method

.method public static getQuaternionFromVector([F[F)V
    .registers 10
    .parameter "Q"
    .parameter "rv"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 1296
    array-length v1, p1

    #@6
    const/4 v2, 0x4

    #@7
    if-ne v1, v2, :cond_1a

    #@9
    .line 1297
    aget v0, p1, v7

    #@b
    aput v0, p0, v4

    #@d
    .line 1302
    :goto_d
    aget v0, p1, v4

    #@f
    aput v0, p0, v5

    #@11
    .line 1303
    aget v0, p1, v5

    #@13
    aput v0, p0, v6

    #@15
    .line 1304
    aget v0, p1, v6

    #@17
    aput v0, p0, v7

    #@19
    .line 1305
    return-void

    #@1a
    .line 1299
    :cond_1a
    const/high16 v1, 0x3f80

    #@1c
    aget v2, p1, v4

    #@1e
    aget v3, p1, v4

    #@20
    mul-float/2addr v2, v3

    #@21
    sub-float/2addr v1, v2

    #@22
    aget v2, p1, v5

    #@24
    aget v3, p1, v5

    #@26
    mul-float/2addr v2, v3

    #@27
    sub-float/2addr v1, v2

    #@28
    aget v2, p1, v6

    #@2a
    aget v3, p1, v6

    #@2c
    mul-float/2addr v2, v3

    #@2d
    sub-float/2addr v1, v2

    #@2e
    aput v1, p0, v4

    #@30
    .line 1300
    aget v1, p0, v4

    #@32
    cmpl-float v1, v1, v0

    #@34
    if-lez v1, :cond_3e

    #@36
    aget v0, p0, v4

    #@38
    float-to-double v0, v0

    #@39
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@3c
    move-result-wide v0

    #@3d
    double-to-float v0, v0

    #@3e
    :cond_3e
    aput v0, p0, v4

    #@40
    goto :goto_d
.end method

.method public static getRotationMatrix([F[F[F[F)Z
    .registers 31
    .parameter "R"
    .parameter "I"
    .parameter "gravity"
    .parameter "geomagnetic"

    #@0
    .prologue
    .line 789
    const/16 v20, 0x0

    #@2
    aget v2, p2, v20

    #@4
    .line 790
    .local v2, Ax:F
    const/16 v20, 0x1

    #@6
    aget v3, p2, v20

    #@8
    .line 791
    .local v3, Ay:F
    const/16 v20, 0x2

    #@a
    aget v4, p2, v20

    #@c
    .line 792
    .local v4, Az:F
    const/16 v20, 0x0

    #@e
    aget v5, p3, v20

    #@10
    .line 793
    .local v5, Ex:F
    const/16 v20, 0x1

    #@12
    aget v6, p3, v20

    #@14
    .line 794
    .local v6, Ey:F
    const/16 v20, 0x2

    #@16
    aget v7, p3, v20

    #@18
    .line 795
    .local v7, Ez:F
    mul-float v20, v6, v4

    #@1a
    mul-float v21, v7, v3

    #@1c
    sub-float v8, v20, v21

    #@1e
    .line 796
    .local v8, Hx:F
    mul-float v20, v7, v2

    #@20
    mul-float v21, v5, v4

    #@22
    sub-float v9, v20, v21

    #@24
    .line 797
    .local v9, Hy:F
    mul-float v20, v5, v3

    #@26
    mul-float v21, v6, v2

    #@28
    sub-float v10, v20, v21

    #@2a
    .line 798
    .local v10, Hz:F
    mul-float v20, v8, v8

    #@2c
    mul-float v21, v9, v9

    #@2e
    add-float v20, v20, v21

    #@30
    mul-float v21, v10, v10

    #@32
    add-float v20, v20, v21

    #@34
    move/from16 v0, v20

    #@36
    float-to-double v0, v0

    #@37
    move-wide/from16 v20, v0

    #@39
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sqrt(D)D

    #@3c
    move-result-wide v20

    #@3d
    move-wide/from16 v0, v20

    #@3f
    double-to-float v0, v0

    #@40
    move/from16 v18, v0

    #@42
    .line 799
    .local v18, normH:F
    const v20, 0x3dcccccd

    #@45
    cmpg-float v20, v18, v20

    #@47
    if-gez v20, :cond_4c

    #@49
    .line 802
    const/16 v20, 0x0

    #@4b
    .line 846
    :goto_4b
    return v20

    #@4c
    .line 804
    :cond_4c
    const/high16 v20, 0x3f80

    #@4e
    div-float v17, v20, v18

    #@50
    .line 805
    .local v17, invH:F
    mul-float v8, v8, v17

    #@52
    .line 806
    mul-float v9, v9, v17

    #@54
    .line 807
    mul-float v10, v10, v17

    #@56
    .line 808
    const/high16 v20, 0x3f80

    #@58
    mul-float v21, v2, v2

    #@5a
    mul-float v22, v3, v3

    #@5c
    add-float v21, v21, v22

    #@5e
    mul-float v22, v4, v4

    #@60
    add-float v21, v21, v22

    #@62
    move/from16 v0, v21

    #@64
    float-to-double v0, v0

    #@65
    move-wide/from16 v21, v0

    #@67
    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->sqrt(D)D

    #@6a
    move-result-wide v21

    #@6b
    move-wide/from16 v0, v21

    #@6d
    double-to-float v0, v0

    #@6e
    move/from16 v21, v0

    #@70
    div-float v15, v20, v21

    #@72
    .line 809
    .local v15, invA:F
    mul-float/2addr v2, v15

    #@73
    .line 810
    mul-float/2addr v3, v15

    #@74
    .line 811
    mul-float/2addr v4, v15

    #@75
    .line 812
    mul-float v20, v3, v10

    #@77
    mul-float v21, v4, v9

    #@79
    sub-float v11, v20, v21

    #@7b
    .line 813
    .local v11, Mx:F
    mul-float v20, v4, v8

    #@7d
    mul-float v21, v2, v10

    #@7f
    sub-float v12, v20, v21

    #@81
    .line 814
    .local v12, My:F
    mul-float v20, v2, v9

    #@83
    mul-float v21, v3, v8

    #@85
    sub-float v13, v20, v21

    #@87
    .line 815
    .local v13, Mz:F
    if-eqz p0, :cond_ba

    #@89
    .line 816
    move-object/from16 v0, p0

    #@8b
    array-length v0, v0

    #@8c
    move/from16 v20, v0

    #@8e
    const/16 v21, 0x9

    #@90
    move/from16 v0, v20

    #@92
    move/from16 v1, v21

    #@94
    if-ne v0, v1, :cond_134

    #@96
    .line 817
    const/16 v20, 0x0

    #@98
    aput v8, p0, v20

    #@9a
    const/16 v20, 0x1

    #@9c
    aput v9, p0, v20

    #@9e
    const/16 v20, 0x2

    #@a0
    aput v10, p0, v20

    #@a2
    .line 818
    const/16 v20, 0x3

    #@a4
    aput v11, p0, v20

    #@a6
    const/16 v20, 0x4

    #@a8
    aput v12, p0, v20

    #@aa
    const/16 v20, 0x5

    #@ac
    aput v13, p0, v20

    #@ae
    .line 819
    const/16 v20, 0x6

    #@b0
    aput v2, p0, v20

    #@b2
    const/16 v20, 0x7

    #@b4
    aput v3, p0, v20

    #@b6
    const/16 v20, 0x8

    #@b8
    aput v4, p0, v20

    #@ba
    .line 827
    :cond_ba
    :goto_ba
    if-eqz p1, :cond_130

    #@bc
    .line 831
    const/high16 v20, 0x3f80

    #@be
    mul-float v21, v5, v5

    #@c0
    mul-float v22, v6, v6

    #@c2
    add-float v21, v21, v22

    #@c4
    mul-float v22, v7, v7

    #@c6
    add-float v21, v21, v22

    #@c8
    move/from16 v0, v21

    #@ca
    float-to-double v0, v0

    #@cb
    move-wide/from16 v21, v0

    #@cd
    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->sqrt(D)D

    #@d0
    move-result-wide v21

    #@d1
    move-wide/from16 v0, v21

    #@d3
    double-to-float v0, v0

    #@d4
    move/from16 v21, v0

    #@d6
    div-float v16, v20, v21

    #@d8
    .line 832
    .local v16, invE:F
    mul-float v20, v5, v11

    #@da
    mul-float v21, v6, v12

    #@dc
    add-float v20, v20, v21

    #@de
    mul-float v21, v7, v13

    #@e0
    add-float v20, v20, v21

    #@e2
    mul-float v14, v20, v16

    #@e4
    .line 833
    .local v14, c:F
    mul-float v20, v5, v2

    #@e6
    mul-float v21, v6, v3

    #@e8
    add-float v20, v20, v21

    #@ea
    mul-float v21, v7, v4

    #@ec
    add-float v20, v20, v21

    #@ee
    mul-float v19, v20, v16

    #@f0
    .line 834
    .local v19, s:F
    move-object/from16 v0, p1

    #@f2
    array-length v0, v0

    #@f3
    move/from16 v20, v0

    #@f5
    const/16 v21, 0x9

    #@f7
    move/from16 v0, v20

    #@f9
    move/from16 v1, v21

    #@fb
    if-ne v0, v1, :cond_191

    #@fd
    .line 835
    const/16 v20, 0x0

    #@ff
    const/high16 v21, 0x3f80

    #@101
    aput v21, p1, v20

    #@103
    const/16 v20, 0x1

    #@105
    const/16 v21, 0x0

    #@107
    aput v21, p1, v20

    #@109
    const/16 v20, 0x2

    #@10b
    const/16 v21, 0x0

    #@10d
    aput v21, p1, v20

    #@10f
    .line 836
    const/16 v20, 0x3

    #@111
    const/16 v21, 0x0

    #@113
    aput v21, p1, v20

    #@115
    const/16 v20, 0x4

    #@117
    aput v14, p1, v20

    #@119
    const/16 v20, 0x5

    #@11b
    aput v19, p1, v20

    #@11d
    .line 837
    const/16 v20, 0x6

    #@11f
    const/16 v21, 0x0

    #@121
    aput v21, p1, v20

    #@123
    const/16 v20, 0x7

    #@125
    move/from16 v0, v19

    #@127
    neg-float v0, v0

    #@128
    move/from16 v21, v0

    #@12a
    aput v21, p1, v20

    #@12c
    const/16 v20, 0x8

    #@12e
    aput v14, p1, v20

    #@130
    .line 846
    .end local v14           #c:F
    .end local v16           #invE:F
    .end local v19           #s:F
    :cond_130
    :goto_130
    const/16 v20, 0x1

    #@132
    goto/16 :goto_4b

    #@134
    .line 820
    :cond_134
    move-object/from16 v0, p0

    #@136
    array-length v0, v0

    #@137
    move/from16 v20, v0

    #@139
    const/16 v21, 0x10

    #@13b
    move/from16 v0, v20

    #@13d
    move/from16 v1, v21

    #@13f
    if-ne v0, v1, :cond_ba

    #@141
    .line 821
    const/16 v20, 0x0

    #@143
    aput v8, p0, v20

    #@145
    const/16 v20, 0x1

    #@147
    aput v9, p0, v20

    #@149
    const/16 v20, 0x2

    #@14b
    aput v10, p0, v20

    #@14d
    const/16 v20, 0x3

    #@14f
    const/16 v21, 0x0

    #@151
    aput v21, p0, v20

    #@153
    .line 822
    const/16 v20, 0x4

    #@155
    aput v11, p0, v20

    #@157
    const/16 v20, 0x5

    #@159
    aput v12, p0, v20

    #@15b
    const/16 v20, 0x6

    #@15d
    aput v13, p0, v20

    #@15f
    const/16 v20, 0x7

    #@161
    const/16 v21, 0x0

    #@163
    aput v21, p0, v20

    #@165
    .line 823
    const/16 v20, 0x8

    #@167
    aput v2, p0, v20

    #@169
    const/16 v20, 0x9

    #@16b
    aput v3, p0, v20

    #@16d
    const/16 v20, 0xa

    #@16f
    aput v4, p0, v20

    #@171
    const/16 v20, 0xb

    #@173
    const/16 v21, 0x0

    #@175
    aput v21, p0, v20

    #@177
    .line 824
    const/16 v20, 0xc

    #@179
    const/16 v21, 0x0

    #@17b
    aput v21, p0, v20

    #@17d
    const/16 v20, 0xd

    #@17f
    const/16 v21, 0x0

    #@181
    aput v21, p0, v20

    #@183
    const/16 v20, 0xe

    #@185
    const/16 v21, 0x0

    #@187
    aput v21, p0, v20

    #@189
    const/16 v20, 0xf

    #@18b
    const/high16 v21, 0x3f80

    #@18d
    aput v21, p0, v20

    #@18f
    goto/16 :goto_ba

    #@191
    .line 838
    .restart local v14       #c:F
    .restart local v16       #invE:F
    .restart local v19       #s:F
    :cond_191
    move-object/from16 v0, p1

    #@193
    array-length v0, v0

    #@194
    move/from16 v20, v0

    #@196
    const/16 v21, 0x10

    #@198
    move/from16 v0, v20

    #@19a
    move/from16 v1, v21

    #@19c
    if-ne v0, v1, :cond_130

    #@19e
    .line 839
    const/16 v20, 0x0

    #@1a0
    const/high16 v21, 0x3f80

    #@1a2
    aput v21, p1, v20

    #@1a4
    const/16 v20, 0x1

    #@1a6
    const/16 v21, 0x0

    #@1a8
    aput v21, p1, v20

    #@1aa
    const/16 v20, 0x2

    #@1ac
    const/16 v21, 0x0

    #@1ae
    aput v21, p1, v20

    #@1b0
    .line 840
    const/16 v20, 0x4

    #@1b2
    const/16 v21, 0x0

    #@1b4
    aput v21, p1, v20

    #@1b6
    const/16 v20, 0x5

    #@1b8
    aput v14, p1, v20

    #@1ba
    const/16 v20, 0x6

    #@1bc
    aput v19, p1, v20

    #@1be
    .line 841
    const/16 v20, 0x8

    #@1c0
    const/16 v21, 0x0

    #@1c2
    aput v21, p1, v20

    #@1c4
    const/16 v20, 0x9

    #@1c6
    move/from16 v0, v19

    #@1c8
    neg-float v0, v0

    #@1c9
    move/from16 v21, v0

    #@1cb
    aput v21, p1, v20

    #@1cd
    const/16 v20, 0xa

    #@1cf
    aput v14, p1, v20

    #@1d1
    .line 842
    const/16 v20, 0x3

    #@1d3
    const/16 v21, 0x7

    #@1d5
    const/16 v22, 0xb

    #@1d7
    const/16 v23, 0xc

    #@1d9
    const/16 v24, 0xd

    #@1db
    const/16 v25, 0xe

    #@1dd
    const/16 v26, 0x0

    #@1df
    aput v26, p1, v25

    #@1e1
    aput v26, p1, v24

    #@1e3
    aput v26, p1, v23

    #@1e5
    aput v26, p1, v22

    #@1e7
    aput v26, p1, v21

    #@1e9
    aput v26, p1, v20

    #@1eb
    .line 843
    const/16 v20, 0xf

    #@1ed
    const/high16 v21, 0x3f80

    #@1ef
    aput v21, p1, v20

    #@1f1
    goto/16 :goto_130
.end method

.method public static getRotationMatrixFromVector([F[F)V
    .registers 20
    .parameter "R"
    .parameter "rotationVector"

    #@0
    .prologue
    .line 1235
    const/4 v14, 0x0

    #@1
    aget v2, p1, v14

    #@3
    .line 1236
    .local v2, q1:F
    const/4 v14, 0x1

    #@4
    aget v6, p1, v14

    #@6
    .line 1237
    .local v6, q2:F
    const/4 v14, 0x2

    #@7
    aget v9, p1, v14

    #@9
    .line 1239
    .local v9, q3:F
    move-object/from16 v0, p1

    #@b
    array-length v14, v0

    #@c
    const/4 v15, 0x4

    #@d
    if-ne v14, v15, :cond_7b

    #@f
    .line 1240
    const/4 v14, 0x3

    #@10
    aget v1, p1, v14

    #@12
    .line 1246
    .local v1, q0:F
    :goto_12
    const/high16 v14, 0x4000

    #@14
    mul-float/2addr v14, v2

    #@15
    mul-float v11, v14, v2

    #@17
    .line 1247
    .local v11, sq_q1:F
    const/high16 v14, 0x4000

    #@19
    mul-float/2addr v14, v6

    #@1a
    mul-float v12, v14, v6

    #@1c
    .line 1248
    .local v12, sq_q2:F
    const/high16 v14, 0x4000

    #@1e
    mul-float/2addr v14, v9

    #@1f
    mul-float v13, v14, v9

    #@21
    .line 1249
    .local v13, sq_q3:F
    const/high16 v14, 0x4000

    #@23
    mul-float/2addr v14, v2

    #@24
    mul-float v4, v14, v6

    #@26
    .line 1250
    .local v4, q1_q2:F
    const/high16 v14, 0x4000

    #@28
    mul-float/2addr v14, v9

    #@29
    mul-float v10, v14, v1

    #@2b
    .line 1251
    .local v10, q3_q0:F
    const/high16 v14, 0x4000

    #@2d
    mul-float/2addr v14, v2

    #@2e
    mul-float v5, v14, v9

    #@30
    .line 1252
    .local v5, q1_q3:F
    const/high16 v14, 0x4000

    #@32
    mul-float/2addr v14, v6

    #@33
    mul-float v7, v14, v1

    #@35
    .line 1253
    .local v7, q2_q0:F
    const/high16 v14, 0x4000

    #@37
    mul-float/2addr v14, v6

    #@38
    mul-float v8, v14, v9

    #@3a
    .line 1254
    .local v8, q2_q3:F
    const/high16 v14, 0x4000

    #@3c
    mul-float/2addr v14, v2

    #@3d
    mul-float v3, v14, v1

    #@3f
    .line 1256
    .local v3, q1_q0:F
    move-object/from16 v0, p0

    #@41
    array-length v14, v0

    #@42
    const/16 v15, 0x9

    #@44
    if-ne v14, v15, :cond_95

    #@46
    .line 1257
    const/4 v14, 0x0

    #@47
    const/high16 v15, 0x3f80

    #@49
    sub-float/2addr v15, v12

    #@4a
    sub-float/2addr v15, v13

    #@4b
    aput v15, p0, v14

    #@4d
    .line 1258
    const/4 v14, 0x1

    #@4e
    sub-float v15, v4, v10

    #@50
    aput v15, p0, v14

    #@52
    .line 1259
    const/4 v14, 0x2

    #@53
    add-float v15, v5, v7

    #@55
    aput v15, p0, v14

    #@57
    .line 1261
    const/4 v14, 0x3

    #@58
    add-float v15, v4, v10

    #@5a
    aput v15, p0, v14

    #@5c
    .line 1262
    const/4 v14, 0x4

    #@5d
    const/high16 v15, 0x3f80

    #@5f
    sub-float/2addr v15, v11

    #@60
    sub-float/2addr v15, v13

    #@61
    aput v15, p0, v14

    #@63
    .line 1263
    const/4 v14, 0x5

    #@64
    sub-float v15, v8, v3

    #@66
    aput v15, p0, v14

    #@68
    .line 1265
    const/4 v14, 0x6

    #@69
    sub-float v15, v5, v7

    #@6b
    aput v15, p0, v14

    #@6d
    .line 1266
    const/4 v14, 0x7

    #@6e
    add-float v15, v8, v3

    #@70
    aput v15, p0, v14

    #@72
    .line 1267
    const/16 v14, 0x8

    #@74
    const/high16 v15, 0x3f80

    #@76
    sub-float/2addr v15, v11

    #@77
    sub-float/2addr v15, v12

    #@78
    aput v15, p0, v14

    #@7a
    .line 1287
    :cond_7a
    :goto_7a
    return-void

    #@7b
    .line 1242
    .end local v1           #q0:F
    .end local v3           #q1_q0:F
    .end local v4           #q1_q2:F
    .end local v5           #q1_q3:F
    .end local v7           #q2_q0:F
    .end local v8           #q2_q3:F
    .end local v10           #q3_q0:F
    .end local v11           #sq_q1:F
    .end local v12           #sq_q2:F
    .end local v13           #sq_q3:F
    :cond_7b
    const/high16 v14, 0x3f80

    #@7d
    mul-float v15, v2, v2

    #@7f
    sub-float/2addr v14, v15

    #@80
    mul-float v15, v6, v6

    #@82
    sub-float/2addr v14, v15

    #@83
    mul-float v15, v9, v9

    #@85
    sub-float v1, v14, v15

    #@87
    .line 1243
    .restart local v1       #q0:F
    const/4 v14, 0x0

    #@88
    cmpl-float v14, v1, v14

    #@8a
    if-lez v14, :cond_93

    #@8c
    float-to-double v14, v1

    #@8d
    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    #@90
    move-result-wide v14

    #@91
    double-to-float v1, v14

    #@92
    :goto_92
    goto :goto_12

    #@93
    :cond_93
    const/4 v1, 0x0

    #@94
    goto :goto_92

    #@95
    .line 1268
    .restart local v3       #q1_q0:F
    .restart local v4       #q1_q2:F
    .restart local v5       #q1_q3:F
    .restart local v7       #q2_q0:F
    .restart local v8       #q2_q3:F
    .restart local v10       #q3_q0:F
    .restart local v11       #sq_q1:F
    .restart local v12       #sq_q2:F
    .restart local v13       #sq_q3:F
    :cond_95
    move-object/from16 v0, p0

    #@97
    array-length v14, v0

    #@98
    const/16 v15, 0x10

    #@9a
    if-ne v14, v15, :cond_7a

    #@9c
    .line 1269
    const/4 v14, 0x0

    #@9d
    const/high16 v15, 0x3f80

    #@9f
    sub-float/2addr v15, v12

    #@a0
    sub-float/2addr v15, v13

    #@a1
    aput v15, p0, v14

    #@a3
    .line 1270
    const/4 v14, 0x1

    #@a4
    sub-float v15, v4, v10

    #@a6
    aput v15, p0, v14

    #@a8
    .line 1271
    const/4 v14, 0x2

    #@a9
    add-float v15, v5, v7

    #@ab
    aput v15, p0, v14

    #@ad
    .line 1272
    const/4 v14, 0x3

    #@ae
    const/4 v15, 0x0

    #@af
    aput v15, p0, v14

    #@b1
    .line 1274
    const/4 v14, 0x4

    #@b2
    add-float v15, v4, v10

    #@b4
    aput v15, p0, v14

    #@b6
    .line 1275
    const/4 v14, 0x5

    #@b7
    const/high16 v15, 0x3f80

    #@b9
    sub-float/2addr v15, v11

    #@ba
    sub-float/2addr v15, v13

    #@bb
    aput v15, p0, v14

    #@bd
    .line 1276
    const/4 v14, 0x6

    #@be
    sub-float v15, v8, v3

    #@c0
    aput v15, p0, v14

    #@c2
    .line 1277
    const/4 v14, 0x7

    #@c3
    const/4 v15, 0x0

    #@c4
    aput v15, p0, v14

    #@c6
    .line 1279
    const/16 v14, 0x8

    #@c8
    sub-float v15, v5, v7

    #@ca
    aput v15, p0, v14

    #@cc
    .line 1280
    const/16 v14, 0x9

    #@ce
    add-float v15, v8, v3

    #@d0
    aput v15, p0, v14

    #@d2
    .line 1281
    const/16 v14, 0xa

    #@d4
    const/high16 v15, 0x3f80

    #@d6
    sub-float/2addr v15, v11

    #@d7
    sub-float/2addr v15, v12

    #@d8
    aput v15, p0, v14

    #@da
    .line 1282
    const/16 v14, 0xb

    #@dc
    const/4 v15, 0x0

    #@dd
    aput v15, p0, v14

    #@df
    .line 1284
    const/16 v14, 0xc

    #@e1
    const/16 v15, 0xd

    #@e3
    const/16 v16, 0xe

    #@e5
    const/16 v17, 0x0

    #@e7
    aput v17, p0, v16

    #@e9
    aput v17, p0, v15

    #@eb
    aput v17, p0, v14

    #@ed
    .line 1285
    const/16 v14, 0xf

    #@ef
    const/high16 v15, 0x3f80

    #@f1
    aput v15, p0, v14

    #@f3
    goto :goto_7a
.end method

.method public static remapCoordinateSystem([FII[F)Z
    .registers 8
    .parameter "inR"
    .parameter "X"
    .parameter "Y"
    .parameter "outR"

    #@0
    .prologue
    .line 949
    if-ne p0, p3, :cond_1a

    #@2
    .line 950
    sget-object v2, Landroid/hardware/SensorManager;->mTempMatrix:[F

    #@4
    .line 951
    .local v2, temp:[F
    monitor-enter v2

    #@5
    .line 953
    :try_start_5
    invoke-static {p0, p1, p2, v2}, Landroid/hardware/SensorManager;->remapCoordinateSystemImpl([FII[F)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_19

    #@b
    .line 954
    array-length v1, p3

    #@c
    .line 955
    .local v1, size:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v1, :cond_16

    #@f
    .line 956
    aget v3, v2, v0

    #@11
    aput v3, p3, v0

    #@13
    .line 955
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_d

    #@16
    .line 957
    :cond_16
    const/4 v3, 0x1

    #@17
    monitor-exit v2

    #@18
    .line 961
    .end local v0           #i:I
    .end local v1           #size:I
    .end local v2           #temp:[F
    :goto_18
    return v3

    #@19
    .line 959
    .restart local v2       #temp:[F
    :cond_19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_1f

    #@1a
    .line 961
    .end local v2           #temp:[F
    :cond_1a
    invoke-static {p0, p1, p2, p3}, Landroid/hardware/SensorManager;->remapCoordinateSystemImpl([FII[F)Z

    #@1d
    move-result v3

    #@1e
    goto :goto_18

    #@1f
    .line 959
    .restart local v2       #temp:[F
    :catchall_1f
    move-exception v3

    #@20
    :try_start_20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    #@21
    throw v3
.end method

.method private static remapCoordinateSystemImpl([FII[F)Z
    .registers 26
    .parameter "inR"
    .parameter "X"
    .parameter "Y"
    .parameter "outR"

    #@0
    .prologue
    .line 978
    move-object/from16 v0, p3

    #@2
    array-length v6, v0

    #@3
    .line 979
    .local v6, length:I
    move-object/from16 v0, p0

    #@5
    array-length v15, v0

    #@6
    if-eq v15, v6, :cond_a

    #@8
    .line 980
    const/4 v15, 0x0

    #@9
    .line 1022
    :goto_9
    return v15

    #@a
    .line 981
    :cond_a
    and-int/lit8 v15, p1, 0x7c

    #@c
    if-nez v15, :cond_12

    #@e
    and-int/lit8 v15, p2, 0x7c

    #@10
    if-eqz v15, :cond_14

    #@12
    .line 982
    :cond_12
    const/4 v15, 0x0

    #@13
    goto :goto_9

    #@14
    .line 983
    :cond_14
    and-int/lit8 v15, p1, 0x3

    #@16
    if-eqz v15, :cond_1c

    #@18
    and-int/lit8 v15, p2, 0x3

    #@1a
    if-nez v15, :cond_1e

    #@1c
    .line 984
    :cond_1c
    const/4 v15, 0x0

    #@1d
    goto :goto_9

    #@1e
    .line 985
    :cond_1e
    and-int/lit8 v15, p1, 0x3

    #@20
    and-int/lit8 v16, p2, 0x3

    #@22
    move/from16 v0, v16

    #@24
    if-ne v15, v0, :cond_28

    #@26
    .line 986
    const/4 v15, 0x0

    #@27
    goto :goto_9

    #@28
    .line 991
    :cond_28
    xor-int v1, p1, p2

    #@2a
    .line 994
    .local v1, Z:I
    and-int/lit8 v15, p1, 0x3

    #@2c
    add-int/lit8 v12, v15, -0x1

    #@2e
    .line 995
    .local v12, x:I
    and-int/lit8 v15, p2, 0x3

    #@30
    add-int/lit8 v13, v15, -0x1

    #@32
    .line 996
    .local v13, y:I
    and-int/lit8 v15, v1, 0x3

    #@34
    add-int/lit8 v14, v15, -0x1

    #@36
    .line 999
    .local v14, z:I
    add-int/lit8 v15, v14, 0x1

    #@38
    rem-int/lit8 v2, v15, 0x3

    #@3a
    .line 1000
    .local v2, axis_y:I
    add-int/lit8 v15, v14, 0x2

    #@3c
    rem-int/lit8 v3, v15, 0x3

    #@3e
    .line 1001
    .local v3, axis_z:I
    xor-int v15, v12, v2

    #@40
    xor-int v16, v13, v3

    #@42
    or-int v15, v15, v16

    #@44
    if-eqz v15, :cond_48

    #@46
    .line 1002
    xor-int/lit16 v1, v1, 0x80

    #@48
    .line 1004
    :cond_48
    const/16 v15, 0x80

    #@4a
    move/from16 v0, p1

    #@4c
    if-lt v0, v15, :cond_94

    #@4e
    const/4 v9, 0x1

    #@4f
    .line 1005
    .local v9, sx:Z
    :goto_4f
    const/16 v15, 0x80

    #@51
    move/from16 v0, p2

    #@53
    if-lt v0, v15, :cond_96

    #@55
    const/4 v10, 0x1

    #@56
    .line 1006
    .local v10, sy:Z
    :goto_56
    const/16 v15, 0x80

    #@58
    if-lt v1, v15, :cond_98

    #@5a
    const/4 v11, 0x1

    #@5b
    .line 1009
    .local v11, sz:Z
    :goto_5b
    const/16 v15, 0x10

    #@5d
    if-ne v6, v15, :cond_9a

    #@5f
    const/4 v8, 0x4

    #@60
    .line 1010
    .local v8, rowLength:I
    :goto_60
    const/4 v5, 0x0

    #@61
    .local v5, j:I
    :goto_61
    const/4 v15, 0x3

    #@62
    if-ge v5, v15, :cond_ae

    #@64
    .line 1011
    mul-int v7, v5, v8

    #@66
    .line 1012
    .local v7, offset:I
    const/4 v4, 0x0

    #@67
    .local v4, i:I
    :goto_67
    const/4 v15, 0x3

    #@68
    if-ge v4, v15, :cond_ab

    #@6a
    .line 1013
    if-ne v12, v4, :cond_77

    #@6c
    add-int v16, v7, v4

    #@6e
    if-eqz v9, :cond_9c

    #@70
    add-int/lit8 v15, v7, 0x0

    #@72
    aget v15, p0, v15

    #@74
    neg-float v15, v15

    #@75
    :goto_75
    aput v15, p3, v16

    #@77
    .line 1014
    :cond_77
    if-ne v13, v4, :cond_84

    #@79
    add-int v16, v7, v4

    #@7b
    if-eqz v10, :cond_a1

    #@7d
    add-int/lit8 v15, v7, 0x1

    #@7f
    aget v15, p0, v15

    #@81
    neg-float v15, v15

    #@82
    :goto_82
    aput v15, p3, v16

    #@84
    .line 1015
    :cond_84
    if-ne v14, v4, :cond_91

    #@86
    add-int v16, v7, v4

    #@88
    if-eqz v11, :cond_a6

    #@8a
    add-int/lit8 v15, v7, 0x2

    #@8c
    aget v15, p0, v15

    #@8e
    neg-float v15, v15

    #@8f
    :goto_8f
    aput v15, p3, v16

    #@91
    .line 1012
    :cond_91
    add-int/lit8 v4, v4, 0x1

    #@93
    goto :goto_67

    #@94
    .line 1004
    .end local v4           #i:I
    .end local v5           #j:I
    .end local v7           #offset:I
    .end local v8           #rowLength:I
    .end local v9           #sx:Z
    .end local v10           #sy:Z
    .end local v11           #sz:Z
    :cond_94
    const/4 v9, 0x0

    #@95
    goto :goto_4f

    #@96
    .line 1005
    .restart local v9       #sx:Z
    :cond_96
    const/4 v10, 0x0

    #@97
    goto :goto_56

    #@98
    .line 1006
    .restart local v10       #sy:Z
    :cond_98
    const/4 v11, 0x0

    #@99
    goto :goto_5b

    #@9a
    .line 1009
    .restart local v11       #sz:Z
    :cond_9a
    const/4 v8, 0x3

    #@9b
    goto :goto_60

    #@9c
    .line 1013
    .restart local v4       #i:I
    .restart local v5       #j:I
    .restart local v7       #offset:I
    .restart local v8       #rowLength:I
    :cond_9c
    add-int/lit8 v15, v7, 0x0

    #@9e
    aget v15, p0, v15

    #@a0
    goto :goto_75

    #@a1
    .line 1014
    :cond_a1
    add-int/lit8 v15, v7, 0x1

    #@a3
    aget v15, p0, v15

    #@a5
    goto :goto_82

    #@a6
    .line 1015
    :cond_a6
    add-int/lit8 v15, v7, 0x2

    #@a8
    aget v15, p0, v15

    #@aa
    goto :goto_8f

    #@ab
    .line 1010
    :cond_ab
    add-int/lit8 v5, v5, 0x1

    #@ad
    goto :goto_61

    #@ae
    .line 1018
    .end local v4           #i:I
    .end local v7           #offset:I
    :cond_ae
    const/16 v15, 0x10

    #@b0
    if-ne v6, v15, :cond_d1

    #@b2
    .line 1019
    const/4 v15, 0x3

    #@b3
    const/16 v16, 0x7

    #@b5
    const/16 v17, 0xb

    #@b7
    const/16 v18, 0xc

    #@b9
    const/16 v19, 0xd

    #@bb
    const/16 v20, 0xe

    #@bd
    const/16 v21, 0x0

    #@bf
    aput v21, p3, v20

    #@c1
    aput v21, p3, v19

    #@c3
    aput v21, p3, v18

    #@c5
    aput v21, p3, v17

    #@c7
    aput v21, p3, v16

    #@c9
    aput v21, p3, v15

    #@cb
    .line 1020
    const/16 v15, 0xf

    #@cd
    const/high16 v16, 0x3f80

    #@cf
    aput v16, p3, v15

    #@d1
    .line 1022
    :cond_d1
    const/4 v15, 0x1

    #@d2
    goto/16 :goto_9
.end method


# virtual methods
.method public getDefaultSensor(I)Landroid/hardware/Sensor;
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 429
    invoke-virtual {p0, p1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    .line 430
    .local v0, l:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/hardware/Sensor;

    #@13
    goto :goto_b
.end method

.method protected abstract getFullSensorList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation
.end method

.method public getSensorList(I)Ljava/util/List;
    .registers 8
    .parameter "type"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 393
    invoke-virtual {p0}, Landroid/hardware/SensorManager;->getFullSensorList()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    .line 394
    .local v0, fullList:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    iget-object v5, p0, Landroid/hardware/SensorManager;->mSensorListByType:Landroid/util/SparseArray;

    #@6
    monitor-enter v5

    #@7
    .line 395
    :try_start_7
    iget-object v4, p0, Landroid/hardware/SensorManager;->mSensorListByType:Landroid/util/SparseArray;

    #@9
    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Ljava/util/List;

    #@f
    .line 396
    .local v3, list:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    if-nez v3, :cond_1e

    #@11
    .line 397
    const/4 v4, -0x1

    #@12
    if-ne p1, v4, :cond_20

    #@14
    .line 398
    move-object v3, v0

    #@15
    .line 406
    :cond_15
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@18
    move-result-object v3

    #@19
    .line 407
    iget-object v4, p0, Landroid/hardware/SensorManager;->mSensorListByType:Landroid/util/SparseArray;

    #@1b
    invoke-virtual {v4, p1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@1e
    .line 409
    :cond_1e
    monitor-exit v5

    #@1f
    .line 410
    return-object v3

    #@20
    .line 400
    :cond_20
    new-instance v3, Ljava/util/ArrayList;

    #@22
    .end local v3           #list:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@25
    .line 401
    .restart local v3       #list:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v2

    #@29
    .local v2, i$:Ljava/util/Iterator;
    :cond_29
    :goto_29
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v4

    #@2d
    if-eqz v4, :cond_15

    #@2f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Landroid/hardware/Sensor;

    #@35
    .line 402
    .local v1, i:Landroid/hardware/Sensor;
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    #@38
    move-result v4

    #@39
    if-ne v4, p1, :cond_29

    #@3b
    .line 403
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3e
    goto :goto_29

    #@3f
    .line 409
    .end local v1           #i:Landroid/hardware/Sensor;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #list:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    :catchall_3f
    move-exception v4

    #@40
    monitor-exit v5
    :try_end_41
    .catchall {:try_start_7 .. :try_end_41} :catchall_3f

    #@41
    throw v4
.end method

.method public getSensors()I
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 373
    invoke-direct {p0}, Landroid/hardware/SensorManager;->getLegacySensorManager()Landroid/hardware/LegacySensorManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/hardware/LegacySensorManager;->getSensors()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
    .registers 5
    .parameter "listener"
    .parameter "sensor"
    .parameter "rate"

    #@0
    .prologue
    .line 586
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
    .registers 7
    .parameter "listener"
    .parameter "sensor"
    .parameter "rate"
    .parameter "handler"

    #@0
    .prologue
    .line 623
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_6

    #@4
    .line 624
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 646
    :goto_5
    return v1

    #@6
    .line 627
    :cond_6
    const/4 v0, -0x1

    #@7
    .line 628
    .local v0, delay:I
    packed-switch p3, :pswitch_data_1e

    #@a
    .line 642
    move v0, p3

    #@b
    .line 646
    :goto_b
    invoke-virtual {p0, p1, p2, v0, p4}, Landroid/hardware/SensorManager;->registerListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@e
    move-result v1

    #@f
    goto :goto_5

    #@10
    .line 630
    :pswitch_10
    const/4 v0, 0x0

    #@11
    .line 631
    goto :goto_b

    #@12
    .line 633
    :pswitch_12
    const/16 v0, 0x4e20

    #@14
    .line 634
    goto :goto_b

    #@15
    .line 636
    :pswitch_15
    const v0, 0x1046b

    #@18
    .line 637
    goto :goto_b

    #@19
    .line 639
    :pswitch_19
    const v0, 0x30d40

    #@1c
    .line 640
    goto :goto_b

    #@1d
    .line 628
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_10
        :pswitch_12
        :pswitch_15
        :pswitch_19
    .end packed-switch
.end method

.method public registerListener(Landroid/hardware/SensorListener;I)Z
    .registers 4
    .parameter "listener"
    .parameter "sensors"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 451
    const/4 v0, 0x3

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorListener;II)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public registerListener(Landroid/hardware/SensorListener;II)Z
    .registers 5
    .parameter "listener"
    .parameter "sensors"
    .parameter "rate"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 479
    invoke-direct {p0}, Landroid/hardware/SensorManager;->getLegacySensorManager()Landroid/hardware/LegacySensorManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2, p3}, Landroid/hardware/LegacySensorManager;->registerListener(Landroid/hardware/SensorListener;II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method protected abstract registerListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
.end method

.method public unregisterListener(Landroid/hardware/SensorEventListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 547
    if-nez p1, :cond_3

    #@2
    .line 552
    :goto_2
    return-void

    #@3
    .line 551
    :cond_3
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/hardware/SensorManager;->unregisterListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    #@7
    goto :goto_2
.end method

.method public unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
    .registers 3
    .parameter "listener"
    .parameter "sensor"

    #@0
    .prologue
    .line 529
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_5

    #@4
    .line 534
    :cond_4
    :goto_4
    return-void

    #@5
    .line 533
    :cond_5
    invoke-virtual {p0, p1, p2}, Landroid/hardware/SensorManager;->unregisterListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    #@8
    goto :goto_4
.end method

.method public unregisterListener(Landroid/hardware/SensorListener;)V
    .registers 3
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 494
    const/16 v0, 0xff

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorListener;I)V

    #@5
    .line 495
    return-void
.end method

.method public unregisterListener(Landroid/hardware/SensorListener;I)V
    .registers 4
    .parameter "listener"
    .parameter "sensors"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 512
    invoke-direct {p0}, Landroid/hardware/SensorManager;->getLegacySensorManager()Landroid/hardware/LegacySensorManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/hardware/LegacySensorManager;->unregisterListener(Landroid/hardware/SensorListener;I)V

    #@7
    .line 513
    return-void
.end method

.method protected abstract unregisterListenerImpl(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
.end method
