.class public Landroid/hardware/SerialManager;
.super Ljava/lang/Object;
.source "SerialManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SerialManager"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mService:Landroid/hardware/ISerialManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/hardware/ISerialManager;)V
    .registers 3
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    iput-object p1, p0, Landroid/hardware/SerialManager;->mContext:Landroid/content/Context;

    #@5
    .line 45
    iput-object p2, p0, Landroid/hardware/SerialManager;->mService:Landroid/hardware/ISerialManager;

    #@7
    .line 46
    return-void
.end method


# virtual methods
.method public getSerialPorts()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 55
    :try_start_0
    iget-object v1, p0, Landroid/hardware/SerialManager;->mService:Landroid/hardware/ISerialManager;

    #@2
    invoke-interface {v1}, Landroid/hardware/ISerialManager;->getSerialPorts()[Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 58
    :goto_6
    return-object v1

    #@7
    .line 56
    :catch_7
    move-exception v0

    #@8
    .line 57
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "SerialManager"

    #@a
    const-string v2, "RemoteException in getSerialPorts"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 58
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public openSerialPort(Ljava/lang/String;I)Landroid/hardware/SerialPort;
    .registers 9
    .parameter "name"
    .parameter "speed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 75
    :try_start_0
    iget-object v3, p0, Landroid/hardware/SerialManager;->mService:Landroid/hardware/ISerialManager;

    #@2
    invoke-interface {v3, p1}, Landroid/hardware/ISerialManager;->openSerialPort(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@5
    move-result-object v1

    #@6
    .line 76
    .local v1, pfd:Landroid/os/ParcelFileDescriptor;
    if-eqz v1, :cond_11

    #@8
    .line 77
    new-instance v2, Landroid/hardware/SerialPort;

    #@a
    invoke-direct {v2, p1}, Landroid/hardware/SerialPort;-><init>(Ljava/lang/String;)V

    #@d
    .line 78
    .local v2, port:Landroid/hardware/SerialPort;
    invoke-virtual {v2, v1, p2}, Landroid/hardware/SerialPort;->open(Landroid/os/ParcelFileDescriptor;I)V

    #@10
    .line 86
    .end local v1           #pfd:Landroid/os/ParcelFileDescriptor;
    .end local v2           #port:Landroid/hardware/SerialPort;
    :goto_10
    return-object v2

    #@11
    .line 81
    .restart local v1       #pfd:Landroid/os/ParcelFileDescriptor;
    :cond_11
    new-instance v3, Ljava/io/IOException;

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v5, "Could not open serial port "

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v4

    #@26
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@29
    throw v3
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_2a} :catch_2a

    #@2a
    .line 83
    .end local v1           #pfd:Landroid/os/ParcelFileDescriptor;
    :catch_2a
    move-exception v0

    #@2b
    .line 84
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "SerialManager"

    #@2d
    const-string v4, "exception in UsbManager.openDevice"

    #@2f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@32
    .line 86
    const/4 v2, 0x0

    #@33
    goto :goto_10
.end method
