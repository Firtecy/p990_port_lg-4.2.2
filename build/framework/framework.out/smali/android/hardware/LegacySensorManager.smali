.class final Landroid/hardware/LegacySensorManager;
.super Ljava/lang/Object;
.source "LegacySensorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/LegacySensorManager$LmsFilter;,
        Landroid/hardware/LegacySensorManager$LegacyListener;
    }
.end annotation


# static fields
.field private static sInitialized:Z

.field private static sRotation:I

.field private static sWindowManager:Landroid/view/IWindowManager;


# instance fields
.field private final mLegacyListenersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/hardware/SensorListener;",
            "Landroid/hardware/LegacySensorManager$LegacyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 36
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/hardware/LegacySensorManager;->sRotation:I

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/hardware/SensorManager;)V
    .registers 5
    .parameter "sensorManager"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@a
    .line 45
    iput-object p1, p0, Landroid/hardware/LegacySensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@c
    .line 47
    const-class v1, Landroid/hardware/SensorManager;

    #@e
    monitor-enter v1

    #@f
    .line 48
    :try_start_f
    sget-boolean v0, Landroid/hardware/LegacySensorManager;->sInitialized:Z

    #@11
    if-nez v0, :cond_31

    #@13
    .line 49
    const-string/jumbo v0, "window"

    #@16
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@19
    move-result-object v0

    #@1a
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Landroid/hardware/LegacySensorManager;->sWindowManager:Landroid/view/IWindowManager;

    #@20
    .line 51
    sget-object v0, Landroid/hardware/LegacySensorManager;->sWindowManager:Landroid/view/IWindowManager;
    :try_end_22
    .catchall {:try_start_f .. :try_end_22} :catchall_33

    #@22
    if-eqz v0, :cond_31

    #@24
    .line 55
    :try_start_24
    sget-object v0, Landroid/hardware/LegacySensorManager;->sWindowManager:Landroid/view/IWindowManager;

    #@26
    new-instance v2, Landroid/hardware/LegacySensorManager$1;

    #@28
    invoke-direct {v2, p0}, Landroid/hardware/LegacySensorManager$1;-><init>(Landroid/hardware/LegacySensorManager;)V

    #@2b
    invoke-interface {v0, v2}, Landroid/view/IWindowManager;->watchRotation(Landroid/view/IRotationWatcher;)I

    #@2e
    move-result v0

    #@2f
    sput v0, Landroid/hardware/LegacySensorManager;->sRotation:I
    :try_end_31
    .catchall {:try_start_24 .. :try_end_31} :catchall_33
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_31} :catch_36

    #@31
    .line 66
    :cond_31
    :goto_31
    :try_start_31
    monitor-exit v1

    #@32
    .line 67
    return-void

    #@33
    .line 66
    :catchall_33
    move-exception v0

    #@34
    monitor-exit v1
    :try_end_35
    .catchall {:try_start_31 .. :try_end_35} :catchall_33

    #@35
    throw v0

    #@36
    .line 62
    :catch_36
    move-exception v0

    #@37
    goto :goto_31
.end method

.method static getRotation()I
    .registers 2

    #@0
    .prologue
    .line 200
    const-class v1, Landroid/hardware/SensorManager;

    #@2
    monitor-enter v1

    #@3
    .line 201
    :try_start_3
    sget v0, Landroid/hardware/LegacySensorManager;->sRotation:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 202
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method static onRotationChanged(I)V
    .registers 3
    .parameter "rotation"

    #@0
    .prologue
    .line 194
    const-class v1, Landroid/hardware/SensorManager;

    #@2
    monitor-enter v1

    #@3
    .line 195
    :try_start_3
    sput p0, Landroid/hardware/LegacySensorManager;->sRotation:I

    #@5
    .line 196
    monitor-exit v1

    #@6
    .line 197
    return-void

    #@7
    .line 196
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private registerLegacyListener(IILandroid/hardware/SensorListener;II)Z
    .registers 11
    .parameter "legacyType"
    .parameter "type"
    .parameter "listener"
    .parameter "sensors"
    .parameter "rate"

    #@0
    .prologue
    .line 109
    const/4 v1, 0x0

    #@1
    .line 111
    .local v1, result:Z
    and-int v3, p4, p1

    #@3
    if-eqz v3, :cond_31

    #@5
    .line 113
    iget-object v3, p0, Landroid/hardware/LegacySensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@7
    invoke-virtual {v3, p2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@a
    move-result-object v2

    #@b
    .line 114
    .local v2, sensor:Landroid/hardware/Sensor;
    if-eqz v2, :cond_31

    #@d
    .line 119
    iget-object v4, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@f
    monitor-enter v4

    #@10
    .line 123
    :try_start_10
    iget-object v3, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@12
    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/hardware/LegacySensorManager$LegacyListener;

    #@18
    .line 124
    .local v0, legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    if-nez v0, :cond_24

    #@1a
    .line 127
    new-instance v0, Landroid/hardware/LegacySensorManager$LegacyListener;

    #@1c
    .end local v0           #legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    invoke-direct {v0, p3}, Landroid/hardware/LegacySensorManager$LegacyListener;-><init>(Landroid/hardware/SensorListener;)V

    #@1f
    .line 128
    .restart local v0       #legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    iget-object v3, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@21
    invoke-virtual {v3, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 132
    :cond_24
    invoke-virtual {v0, p1}, Landroid/hardware/LegacySensorManager$LegacyListener;->registerSensor(I)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_32

    #@2a
    .line 134
    iget-object v3, p0, Landroid/hardware/LegacySensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@2c
    invoke-virtual {v3, v0, v2, p5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    #@2f
    move-result v1

    #@30
    .line 138
    :goto_30
    monitor-exit v4

    #@31
    .line 141
    .end local v0           #legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    .end local v2           #sensor:Landroid/hardware/Sensor;
    :cond_31
    return v1

    #@32
    .line 136
    .restart local v0       #legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    .restart local v2       #sensor:Landroid/hardware/Sensor;
    :cond_32
    const/4 v1, 0x1

    #@33
    goto :goto_30

    #@34
    .line 138
    .end local v0           #legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    :catchall_34
    move-exception v3

    #@35
    monitor-exit v4
    :try_end_36
    .catchall {:try_start_10 .. :try_end_36} :catchall_34

    #@36
    throw v3
.end method

.method private unregisterLegacyListener(IILandroid/hardware/SensorListener;I)V
    .registers 9
    .parameter "legacyType"
    .parameter "type"
    .parameter "listener"
    .parameter "sensors"

    #@0
    .prologue
    .line 163
    and-int v2, p4, p1

    #@2
    if-eqz v2, :cond_30

    #@4
    .line 165
    iget-object v2, p0, Landroid/hardware/LegacySensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@6
    invoke-virtual {v2, p2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@9
    move-result-object v1

    #@a
    .line 166
    .local v1, sensor:Landroid/hardware/Sensor;
    if-eqz v1, :cond_30

    #@c
    .line 171
    iget-object v3, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@e
    monitor-enter v3

    #@f
    .line 173
    :try_start_f
    iget-object v2, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@11
    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/hardware/LegacySensorManager$LegacyListener;

    #@17
    .line 174
    .local v0, legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    if-eqz v0, :cond_2f

    #@19
    .line 177
    invoke-virtual {v0, p1}, Landroid/hardware/LegacySensorManager$LegacyListener;->unregisterSensor(I)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_2f

    #@1f
    .line 179
    iget-object v2, p0, Landroid/hardware/LegacySensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@21
    invoke-virtual {v2, v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    #@24
    .line 183
    invoke-virtual {v0}, Landroid/hardware/LegacySensorManager$LegacyListener;->hasSensors()Z

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_2f

    #@2a
    .line 184
    iget-object v2, p0, Landroid/hardware/LegacySensorManager;->mLegacyListenersMap:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v2, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 188
    :cond_2f
    monitor-exit v3

    #@30
    .line 191
    .end local v0           #legacyListener:Landroid/hardware/LegacySensorManager$LegacyListener;
    .end local v1           #sensor:Landroid/hardware/Sensor;
    :cond_30
    return-void

    #@31
    .line 188
    .restart local v1       #sensor:Landroid/hardware/Sensor;
    :catchall_31
    move-exception v2

    #@32
    monitor-exit v3
    :try_end_33
    .catchall {:try_start_f .. :try_end_33} :catchall_31

    #@33
    throw v2
.end method


# virtual methods
.method public getSensors()I
    .registers 6

    #@0
    .prologue
    .line 70
    const/4 v3, 0x0

    #@1
    .line 71
    .local v3, result:I
    iget-object v4, p0, Landroid/hardware/LegacySensorManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@3
    invoke-virtual {v4}, Landroid/hardware/SensorManager;->getFullSensorList()Ljava/util/List;

    #@6
    move-result-object v0

    #@7
    .line 72
    .local v0, fullList:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    .local v2, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_28

    #@11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/hardware/Sensor;

    #@17
    .line 73
    .local v1, i:Landroid/hardware/Sensor;
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    #@1a
    move-result v4

    #@1b
    packed-switch v4, :pswitch_data_2a

    #@1e
    goto :goto_b

    #@1f
    .line 75
    :pswitch_1f
    or-int/lit8 v3, v3, 0x2

    #@21
    .line 76
    goto :goto_b

    #@22
    .line 78
    :pswitch_22
    or-int/lit8 v3, v3, 0x8

    #@24
    .line 79
    goto :goto_b

    #@25
    .line 81
    :pswitch_25
    or-int/lit16 v3, v3, 0x81

    #@27
    goto :goto_b

    #@28
    .line 86
    .end local v1           #i:Landroid/hardware/Sensor;
    :cond_28
    return v3

    #@29
    .line 73
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method public registerListener(Landroid/hardware/SensorListener;II)Z
    .registers 16
    .parameter "listener"
    .parameter "sensors"
    .parameter "rate"

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v1, 0x2

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 90
    if-nez p1, :cond_7

    #@6
    .line 104
    :goto_6
    return v10

    #@7
    .line 93
    :cond_7
    const/4 v9, 0x0

    #@8
    .local v9, result:Z
    move-object v0, p0

    #@9
    move-object v3, p1

    #@a
    move v4, p2

    #@b
    move v5, p3

    #@c
    .line 94
    invoke-direct/range {v0 .. v5}, Landroid/hardware/LegacySensorManager;->registerLegacyListener(IILandroid/hardware/SensorListener;II)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_14

    #@12
    if-eqz v9, :cond_54

    #@14
    :cond_14
    move v9, v2

    #@15
    .line 96
    :goto_15
    const/16 v4, 0x8

    #@17
    move-object v3, p0

    #@18
    move v5, v1

    #@19
    move-object v6, p1

    #@1a
    move v7, p2

    #@1b
    move v8, p3

    #@1c
    invoke-direct/range {v3 .. v8}, Landroid/hardware/LegacySensorManager;->registerLegacyListener(IILandroid/hardware/SensorListener;II)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_24

    #@22
    if-eqz v9, :cond_56

    #@24
    :cond_24
    move v9, v2

    #@25
    .line 98
    :goto_25
    const/16 v4, 0x80

    #@27
    move-object v3, p0

    #@28
    move v5, v11

    #@29
    move-object v6, p1

    #@2a
    move v7, p2

    #@2b
    move v8, p3

    #@2c
    invoke-direct/range {v3 .. v8}, Landroid/hardware/LegacySensorManager;->registerLegacyListener(IILandroid/hardware/SensorListener;II)Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_34

    #@32
    if-eqz v9, :cond_58

    #@34
    :cond_34
    move v9, v2

    #@35
    :goto_35
    move-object v1, p0

    #@36
    move v3, v11

    #@37
    move-object v4, p1

    #@38
    move v5, p2

    #@39
    move v6, p3

    #@3a
    .line 100
    invoke-direct/range {v1 .. v6}, Landroid/hardware/LegacySensorManager;->registerLegacyListener(IILandroid/hardware/SensorListener;II)Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_42

    #@40
    if-eqz v9, :cond_5a

    #@42
    :cond_42
    move v9, v2

    #@43
    .line 102
    :goto_43
    const/4 v4, 0x4

    #@44
    const/4 v5, 0x7

    #@45
    move-object v3, p0

    #@46
    move-object v6, p1

    #@47
    move v7, p2

    #@48
    move v8, p3

    #@49
    invoke-direct/range {v3 .. v8}, Landroid/hardware/LegacySensorManager;->registerLegacyListener(IILandroid/hardware/SensorListener;II)Z

    #@4c
    move-result v0

    #@4d
    if-nez v0, :cond_51

    #@4f
    if-eqz v9, :cond_5c

    #@51
    :cond_51
    move v9, v2

    #@52
    :goto_52
    move v10, v9

    #@53
    .line 104
    goto :goto_6

    #@54
    :cond_54
    move v9, v10

    #@55
    .line 94
    goto :goto_15

    #@56
    :cond_56
    move v9, v10

    #@57
    .line 96
    goto :goto_25

    #@58
    :cond_58
    move v9, v10

    #@59
    .line 98
    goto :goto_35

    #@5a
    :cond_5a
    move v9, v10

    #@5b
    .line 100
    goto :goto_43

    #@5c
    :cond_5c
    move v9, v10

    #@5d
    .line 102
    goto :goto_52
.end method

.method public unregisterListener(Landroid/hardware/SensorListener;I)V
    .registers 7
    .parameter "listener"
    .parameter "sensors"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    const/4 v2, 0x2

    #@2
    const/4 v1, 0x1

    #@3
    .line 145
    if-nez p1, :cond_6

    #@5
    .line 158
    :goto_5
    return-void

    #@6
    .line 148
    :cond_6
    invoke-direct {p0, v2, v1, p1, p2}, Landroid/hardware/LegacySensorManager;->unregisterLegacyListener(IILandroid/hardware/SensorListener;I)V

    #@9
    .line 150
    const/16 v0, 0x8

    #@b
    invoke-direct {p0, v0, v2, p1, p2}, Landroid/hardware/LegacySensorManager;->unregisterLegacyListener(IILandroid/hardware/SensorListener;I)V

    #@e
    .line 152
    const/16 v0, 0x80

    #@10
    invoke-direct {p0, v0, v3, p1, p2}, Landroid/hardware/LegacySensorManager;->unregisterLegacyListener(IILandroid/hardware/SensorListener;I)V

    #@13
    .line 154
    invoke-direct {p0, v1, v3, p1, p2}, Landroid/hardware/LegacySensorManager;->unregisterLegacyListener(IILandroid/hardware/SensorListener;I)V

    #@16
    .line 156
    const/4 v0, 0x4

    #@17
    const/4 v1, 0x7

    #@18
    invoke-direct {p0, v0, v1, p1, p2}, Landroid/hardware/LegacySensorManager;->unregisterLegacyListener(IILandroid/hardware/SensorListener;I)V

    #@1b
    goto :goto_5
.end method
