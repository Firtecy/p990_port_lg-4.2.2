.class public Landroid/preference/VolumePreference$SeekBarVolumizer;
.super Ljava/lang/Object;
.source "VolumePreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/preference/VolumePreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SeekBarVolumizer"
.end annotation


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mLastProgress:I

.field private mOriginalStreamVolume:I

.field private mRingtone:Landroid/media/Ringtone;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mStreamType:I

.field private mVolumeBeforeMute:I

.field private mVolumeObserver:Landroid/database/ContentObserver;

.field private mVolumeReceiver:Landroid/content/BroadcastReceiver;

.field final synthetic this$0:Landroid/preference/VolumePreference;


# direct methods
.method public constructor <init>(Landroid/preference/VolumePreference;Landroid/content/Context;Landroid/widget/SeekBar;I)V
    .registers 11
    .parameter
    .parameter "context"
    .parameter "seekBar"
    .parameter "streamType"

    #@0
    .prologue
    .line 325
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;-><init>(Landroid/preference/VolumePreference;Landroid/content/Context;Landroid/widget/SeekBar;ILandroid/net/Uri;)V

    #@9
    .line 326
    return-void
.end method

.method public constructor <init>(Landroid/preference/VolumePreference;Landroid/content/Context;Landroid/widget/SeekBar;ILandroid/net/Uri;)V
    .registers 9
    .parameter
    .parameter "context"
    .parameter "seekBar"
    .parameter "streamType"
    .parameter "defaultUri"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 328
    iput-object p1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 251
    new-instance v1, Landroid/os/Handler;

    #@8
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@b
    iput-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mHandler:Landroid/os/Handler;

    #@d
    .line 258
    iput v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@f
    .line 260
    iput v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@11
    .line 264
    new-instance v1, Landroid/preference/VolumePreference$SeekBarVolumizer$1;

    #@13
    invoke-direct {v1, p0}, Landroid/preference/VolumePreference$SeekBarVolumizer$1;-><init>(Landroid/preference/VolumePreference$SeekBarVolumizer;)V

    #@16
    iput-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    #@18
    .line 313
    new-instance v1, Landroid/preference/VolumePreference$SeekBarVolumizer$2;

    #@1a
    iget-object v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mHandler:Landroid/os/Handler;

    #@1c
    invoke-direct {v1, p0, v2}, Landroid/preference/VolumePreference$SeekBarVolumizer$2;-><init>(Landroid/preference/VolumePreference$SeekBarVolumizer;Landroid/os/Handler;)V

    #@1f
    iput-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeObserver:Landroid/database/ContentObserver;

    #@21
    .line 329
    iput-object p2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mContext:Landroid/content/Context;

    #@23
    .line 330
    const-string v1, "audio"

    #@25
    invoke-virtual {p2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Landroid/media/AudioManager;

    #@2b
    iput-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@2d
    .line 331
    iput p4, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@2f
    .line 332
    iput-object p3, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@31
    .line 335
    new-instance v0, Landroid/content/IntentFilter;

    #@33
    const-string v1, "android.intent.action.HEADSET_PLUG"

    #@35
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@38
    .line 336
    .local v0, filter:Landroid/content/IntentFilter;
    const-string/jumbo v1, "lge.settings.intent.action.NOTI_RESUME"

    #@3b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3e
    .line 337
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    #@40
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@43
    .line 338
    const-string v1, "android.intent.action.PHONE_STATE"

    #@45
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@48
    .line 339
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    #@4a
    invoke-virtual {p2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@4d
    .line 342
    invoke-direct {p0, p3, p5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->initSeekBar(Landroid/widget/SeekBar;Landroid/net/Uri;)V

    #@50
    .line 343
    return-void
.end method

.method static synthetic access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 248
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/preference/VolumePreference$SeekBarVolumizer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mOriginalStreamVolume:I

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/preference/VolumePreference$SeekBarVolumizer;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 248
    iput p1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mOriginalStreamVolume:I

    #@2
    return p1
.end method

.method private initSeekBar(Landroid/widget/SeekBar;Landroid/net/Uri;)V
    .registers 7
    .parameter "seekBar"
    .parameter "defaultUri"

    #@0
    .prologue
    .line 346
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@2
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@4
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    #@7
    move-result v0

    #@8
    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    #@b
    .line 347
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@d
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@f
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mOriginalStreamVolume:I

    #@15
    .line 348
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mOriginalStreamVolume:I

    #@17
    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    #@1a
    .line 349
    invoke-virtual {p1, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    #@1d
    .line 351
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mContext:Landroid/content/Context;

    #@1f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v0

    #@23
    sget-object v1, Landroid/provider/Settings$System;->VOLUME_SETTINGS:[Ljava/lang/String;

    #@25
    iget v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@27
    aget-object v1, v1, v2

    #@29
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@2c
    move-result-object v1

    #@2d
    const/4 v2, 0x0

    #@2e
    iget-object v3, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeObserver:Landroid/database/ContentObserver;

    #@30
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@33
    .line 355
    if-nez p2, :cond_3c

    #@35
    .line 356
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@37
    const/4 v1, 0x2

    #@38
    if-ne v0, v1, :cond_47

    #@3a
    .line 357
    sget-object p2, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    #@3c
    .line 376
    :cond_3c
    :goto_3c
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mContext:Landroid/content/Context;

    #@3e
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@40
    invoke-static {v0, p2, v1}, Landroid/media/RingtoneManager;->getRingtoneWithStreamType(Landroid/content/Context;Landroid/net/Uri;I)Landroid/media/Ringtone;

    #@43
    move-result-object v0

    #@44
    iput-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@46
    .line 384
    return-void

    #@47
    .line 358
    :cond_47
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@49
    const/4 v1, 0x5

    #@4a
    if-ne v0, v1, :cond_4f

    #@4c
    .line 359
    sget-object p2, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@4e
    goto :goto_3c

    #@4f
    .line 361
    :cond_4f
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@51
    const/4 v1, 0x1

    #@52
    if-ne v0, v1, :cond_5b

    #@54
    .line 362
    const-string v0, "file:///system/media/audio/ui/Effect_Tick.ogg"

    #@56
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@59
    move-result-object p2

    #@5a
    goto :goto_3c

    #@5b
    .line 365
    :cond_5b
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@5d
    const/4 v1, 0x3

    #@5e
    if-ne v0, v1, :cond_67

    #@60
    .line 367
    const-string v0, "file:///system/media/audio/ui/LG_Media_volume.ogg"

    #@62
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@65
    move-result-object p2

    #@66
    goto :goto_3c

    #@67
    .line 370
    :cond_67
    sget-object p2, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    #@69
    goto :goto_3c
.end method


# virtual methods
.method public changeVolumeBy(I)V
    .registers 6
    .parameter "amount"

    #@0
    .prologue
    .line 482
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    #@5
    .line 483
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->isSamplePlaying()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_1c

    #@b
    .line 485
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@d
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@f
    iget-object v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@11
    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    #@14
    move-result v2

    #@15
    const/4 v3, 0x0

    #@16
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@19
    .line 487
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->startSample()V

    #@1c
    .line 489
    :cond_1c
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@1e
    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    #@21
    move-result v0

    #@22
    invoke-virtual {p0, v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->postSetVolume(I)V

    #@25
    .line 490
    const/4 v0, -0x1

    #@26
    iput v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@28
    .line 491
    return-void
.end method

.method public getSeekBar()Landroid/widget/SeekBar;
    .registers 2

    #@0
    .prologue
    .line 478
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@2
    return-object v0
.end method

.method public isSamplePlaying()Z
    .registers 2

    #@0
    .prologue
    .line 454
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@6
    invoke-virtual {v0}, Landroid/media/Ringtone;->isPlaying()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public muteVolume()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    .line 494
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@4
    if-eq v0, v2, :cond_18

    #@6
    .line 495
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@8
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@a
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    #@d
    .line 496
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->startSample()V

    #@10
    .line 497
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@12
    invoke-virtual {p0, v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->postSetVolume(I)V

    #@15
    .line 498
    iput v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@17
    .line 505
    :goto_17
    return-void

    #@18
    .line 500
    :cond_18
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@1a
    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    #@1d
    move-result v0

    #@1e
    iput v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeBeforeMute:I

    #@20
    .line 501
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@22
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    #@25
    .line 502
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stopSample()V

    #@28
    .line 503
    invoke-virtual {p0, v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->postSetVolume(I)V

    #@2b
    goto :goto_17
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .registers 4
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromTouch"

    #@0
    .prologue
    .line 420
    if-nez p3, :cond_3

    #@2
    .line 425
    :goto_2
    return-void

    #@3
    .line 424
    :cond_3
    invoke-virtual {p0, p2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->postSetVolume(I)V

    #@6
    goto :goto_2
.end method

.method public onRestoreInstanceState(Landroid/preference/VolumePreference$VolumeStore;)V
    .registers 4
    .parameter "volumeStore"

    #@0
    .prologue
    .line 515
    iget v0, p1, Landroid/preference/VolumePreference$VolumeStore;->volume:I

    #@2
    const/4 v1, -0x1

    #@3
    if-eq v0, v1, :cond_12

    #@5
    .line 516
    iget v0, p1, Landroid/preference/VolumePreference$VolumeStore;->originalVolume:I

    #@7
    iput v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mOriginalStreamVolume:I

    #@9
    .line 517
    iget v0, p1, Landroid/preference/VolumePreference$VolumeStore;->volume:I

    #@b
    iput v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@d
    .line 518
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@f
    invoke-virtual {p0, v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->postSetVolume(I)V

    #@12
    .line 520
    :cond_12
    return-void
.end method

.method public onSaveInstanceState(Landroid/preference/VolumePreference$VolumeStore;)V
    .registers 3
    .parameter "volumeStore"

    #@0
    .prologue
    .line 508
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@2
    if-ltz v0, :cond_c

    #@4
    .line 509
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@6
    iput v0, p1, Landroid/preference/VolumePreference$VolumeStore;->volume:I

    #@8
    .line 510
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mOriginalStreamVolume:I

    #@a
    iput v0, p1, Landroid/preference/VolumePreference$VolumeStore;->originalVolume:I

    #@c
    .line 512
    :cond_c
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2
    .parameter "seekBar"

    #@0
    .prologue
    .line 435
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 6
    .parameter "seekBar"

    #@0
    .prologue
    .line 439
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->isSamplePlaying()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_17

    #@6
    .line 440
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@8
    if-ltz v0, :cond_14

    #@a
    .line 441
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@c
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@e
    iget v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@14
    .line 443
    :cond_14
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->startSample()V

    #@17
    .line 446
    :cond_17
    return-void
.end method

.method postSetVolume(I)V
    .registers 3
    .parameter "progress"

    #@0
    .prologue
    .line 429
    iput p1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@2
    .line 430
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 431
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mHandler:Landroid/os/Handler;

    #@9
    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@c
    .line 432
    return-void
.end method

.method public revertVolume()V
    .registers 5

    #@0
    .prologue
    .line 403
    new-instance v0, Landroid/os/Handler;

    #@2
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@5
    new-instance v1, Landroid/preference/VolumePreference$SeekBarVolumizer$3;

    #@7
    invoke-direct {v1, p0}, Landroid/preference/VolumePreference$SeekBarVolumizer$3;-><init>(Landroid/preference/VolumePreference$SeekBarVolumizer;)V

    #@a
    const-wide/16 v2, 0x64

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@f
    .line 416
    return-void
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 449
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-static {v0, v1}, Landroid/preference/VolumePreference;->access$002(Landroid/preference/VolumePreference;Z)Z

    #@6
    .line 450
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@8
    iget v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@a
    iget v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mLastProgress:I

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@10
    .line 451
    return-void
.end method

.method public startSample()V
    .registers 3

    #@0
    .prologue
    .line 458
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@2
    invoke-virtual {v0, p0}, Landroid/preference/VolumePreference;->onSampleStarting(Landroid/preference/VolumePreference$SeekBarVolumizer;)V

    #@5
    .line 459
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@7
    if-eqz v0, :cond_19

    #@9
    .line 461
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mAudioManager:Landroid/media/AudioManager;

    #@b
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    iget v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mStreamType:I

    #@13
    const/4 v1, 0x3

    #@14
    if-ne v0, v1, :cond_1a

    #@16
    .line 462
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stopSample()V

    #@19
    .line 469
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 467
    :cond_1a
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@1c
    invoke-virtual {v0}, Landroid/media/Ringtone;->play()V

    #@1f
    goto :goto_19
.end method

.method public stop()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 387
    invoke-virtual {p0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stopSample()V

    #@4
    .line 388
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeObserver:Landroid/database/ContentObserver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@f
    .line 391
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    #@11
    if-eqz v0, :cond_1c

    #@13
    .line 392
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mContext:Landroid/content/Context;

    #@15
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    #@17
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@1a
    .line 393
    iput-object v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    .line 397
    :cond_1c
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mSeekBar:Landroid/widget/SeekBar;

    #@1e
    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    #@21
    .line 398
    return-void
.end method

.method public stopSample()V
    .registers 2

    #@0
    .prologue
    .line 472
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 473
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer;->mRingtone:Landroid/media/Ringtone;

    #@6
    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    #@9
    .line 475
    :cond_9
    return-void
.end method
