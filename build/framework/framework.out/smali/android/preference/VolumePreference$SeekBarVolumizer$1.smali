.class Landroid/preference/VolumePreference$SeekBarVolumizer$1;
.super Landroid/content/BroadcastReceiver;
.source "VolumePreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/preference/VolumePreference$SeekBarVolumizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;


# direct methods
.method constructor <init>(Landroid/preference/VolumePreference$SeekBarVolumizer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 264
    iput-object p1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    .line 267
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 269
    .local v0, action:Ljava/lang/String;
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@8
    iget-object v5, v5, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@a
    invoke-static {v5}, Landroid/preference/VolumePreference;->access$000(Landroid/preference/VolumePreference;)Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_8d

    #@10
    const-string v5, "android.intent.action.HEADSET_PLUG"

    #@12
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_8d

    #@18
    .line 270
    const-string/jumbo v5, "state"

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1f
    move-result v1

    #@20
    .line 271
    .local v1, headsetState:I
    if-ne v1, v8, :cond_2a

    #@22
    .line 273
    :try_start_22
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@25
    const-wide/16 v5, 0x64

    #@27
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2a
    .catch Ljava/lang/InterruptedException; {:try_start_22 .. :try_end_2a} :catch_140

    #@2a
    .line 277
    :cond_2a
    :goto_2a
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2c
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@2f
    move-result-object v5

    #@30
    iget-object v6, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@32
    invoke-static {v6}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@35
    move-result v6

    #@36
    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@39
    move-result v2

    #@3a
    .line 278
    .local v2, newOriginalvolume:I
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@3c
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;

    #@3f
    move-result-object v5

    #@40
    if-eqz v5, :cond_8d

    #@42
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@44
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5}, Landroid/media/AudioManager;->getRingerMode()I

    #@4b
    move-result v5

    #@4c
    if-eq v5, v9, :cond_57

    #@4e
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@50
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@53
    move-result v5

    #@54
    const/4 v6, 0x3

    #@55
    if-ne v5, v6, :cond_8d

    #@57
    .line 280
    :cond_57
    const-string v5, "VolumePreference"

    #@59
    new-instance v6, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v7, "ACTION_HEADSET_PLUG mOriginalStreamVolume = "

    #@60
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v6

    #@64
    iget-object v7, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@66
    invoke-static {v7}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$400(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@69
    move-result v7

    #@6a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    const-string v7, ", newOriginalvolume = "

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v6

    #@78
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 282
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@81
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v5, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    #@88
    .line 283
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@8a
    invoke-static {v5, v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$402(Landroid/preference/VolumePreference$SeekBarVolumizer;I)I

    #@8d
    .line 287
    .end local v1           #headsetState:I
    .end local v2           #newOriginalvolume:I
    :cond_8d
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@8f
    iget-object v5, v5, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@91
    invoke-static {v5}, Landroid/preference/VolumePreference;->access$000(Landroid/preference/VolumePreference;)Z

    #@94
    move-result v5

    #@95
    if-eqz v5, :cond_f6

    #@97
    const-string v5, "android.media.VOLUME_CHANGED_ACTION"

    #@99
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v5

    #@9d
    if-eqz v5, :cond_f6

    #@9f
    .line 288
    const-string v5, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    #@a1
    invoke-virtual {p2, v5, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@a4
    move-result v4

    #@a5
    .line 289
    .local v4, streamType:I
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@a7
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;

    #@aa
    move-result-object v5

    #@ab
    if-eqz v5, :cond_f6

    #@ad
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@af
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@b2
    move-result v5

    #@b3
    if-ne v5, v4, :cond_f6

    #@b5
    .line 290
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@b7
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@ba
    move-result-object v5

    #@bb
    iget-object v6, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@bd
    invoke-static {v6}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@c0
    move-result v6

    #@c1
    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@c4
    move-result v2

    #@c5
    .line 291
    .restart local v2       #newOriginalvolume:I
    const-string v5, "VolumePreference"

    #@c7
    new-instance v6, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v7, "VOLUME_CHANGED_ACTION mOriginalStreamVolume = "

    #@ce
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v6

    #@d2
    iget-object v7, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@d4
    invoke-static {v7}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$400(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@d7
    move-result v7

    #@d8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v6

    #@dc
    const-string v7, ", newOriginalvolume = "

    #@de
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v6

    #@e2
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v6

    #@e6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v6

    #@ea
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 293
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@ef
    invoke-static {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;

    #@f2
    move-result-object v5

    #@f3
    invoke-virtual {v5, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    #@f6
    .line 297
    .end local v2           #newOriginalvolume:I
    .end local v4           #streamType:I
    :cond_f6
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@f8
    iget-object v5, v5, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@fa
    invoke-static {v5}, Landroid/preference/VolumePreference;->access$000(Landroid/preference/VolumePreference;)Z

    #@fd
    move-result v5

    #@fe
    if-eqz v5, :cond_11e

    #@100
    const-string v5, "android.intent.action.PHONE_STATE"

    #@102
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@105
    move-result v5

    #@106
    if-eqz v5, :cond_11e

    #@108
    .line 298
    const-string/jumbo v5, "state"

    #@10b
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@10e
    move-result-object v3

    #@10f
    .line 299
    .local v3, state:Ljava/lang/String;
    if-eqz v3, :cond_11e

    #@111
    sget-object v5, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    #@113
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@116
    move-result v5

    #@117
    if-eqz v5, :cond_11e

    #@119
    .line 300
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@11b
    invoke-virtual {v5}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stopSample()V

    #@11e
    .line 305
    .end local v3           #state:Ljava/lang/String;
    :cond_11e
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@120
    iget-object v5, v5, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@122
    invoke-static {v5}, Landroid/preference/VolumePreference;->access$000(Landroid/preference/VolumePreference;)Z

    #@125
    move-result v5

    #@126
    if-nez v5, :cond_13f

    #@128
    const-string/jumbo v5, "lge.settings.intent.action.NOTI_RESUME"

    #@12b
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12e
    move-result v5

    #@12f
    if-eqz v5, :cond_13f

    #@131
    .line 306
    const-string v5, "VolumePreference"

    #@133
    const-string v6, "VolumeReceiver!!! onReceive..... NOTI_RESUME"

    #@135
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@138
    .line 307
    iget-object v5, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$1;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@13a
    iget-object v5, v5, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@13c
    invoke-static {v5, v8}, Landroid/preference/VolumePreference;->access$002(Landroid/preference/VolumePreference;Z)Z

    #@13f
    .line 309
    :cond_13f
    return-void

    #@140
    .line 274
    .restart local v1       #headsetState:I
    :catch_140
    move-exception v5

    #@141
    goto/16 :goto_2a
.end method
