.class public Landroid/preference/MultiCheckPreference;
.super Landroid/preference/DialogPreference;
.source "MultiCheckPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/MultiCheckPreference$SavedState;
    }
.end annotation


# instance fields
.field private mEntries:[Ljava/lang/CharSequence;

.field private mEntryValues:[Ljava/lang/String;

.field private mOrigValues:[Z

.field private mSetValues:[Z

.field private mSummary:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/MultiCheckPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 47
    sget-object v1, Lcom/android/internal/R$styleable;->ListPreference:[I

    #@6
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 49
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@10
    .line 50
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@12
    if-eqz v1, :cond_19

    #@14
    .line 51
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@16
    invoke-virtual {p0, v1}, Landroid/preference/MultiCheckPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@19
    .line 53
    :cond_19
    const/4 v1, 0x1

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {p0, v1}, Landroid/preference/MultiCheckPreference;->setEntryValuesCS([Ljava/lang/CharSequence;)V

    #@21
    .line 55
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@24
    .line 60
    sget-object v1, Lcom/android/internal/R$styleable;->Preference:[I

    #@26
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@29
    move-result-object v0

    #@2a
    .line 62
    const/4 v1, 0x7

    #@2b
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@31
    .line 63
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@34
    .line 64
    return-void
.end method

.method static synthetic access$000(Landroid/preference/MultiCheckPreference;)[Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 37
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@2
    return-object v0
.end method

.method private setEntryValuesCS([Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "values"

    #@0
    .prologue
    .line 125
    const/4 v1, 0x0

    #@1
    invoke-virtual {p0, v1}, Landroid/preference/MultiCheckPreference;->setValues([Z)V

    #@4
    .line 126
    if-eqz p1, :cond_1c

    #@6
    .line 127
    array-length v1, p1

    #@7
    new-array v1, v1, [Ljava/lang/String;

    #@9
    iput-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@b
    .line 128
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    array-length v1, p1

    #@d
    if-ge v0, v1, :cond_1c

    #@f
    .line 129
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@11
    aget-object v2, p1, v0

    #@13
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    aput-object v2, v1, v0

    #@19
    .line 128
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_c

    #@1c
    .line 132
    .end local v0           #i:I
    :cond_1c
    return-void
.end method


# virtual methods
.method public findIndexOfValue(Ljava/lang/String;)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 221
    if-eqz p1, :cond_1b

    #@2
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@4
    if-eqz v1, :cond_1b

    #@6
    .line 222
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@8
    array-length v1, v1

    #@9
    add-int/lit8 v0, v1, -0x1

    #@b
    .local v0, i:I
    :goto_b
    if-ltz v0, :cond_1b

    #@d
    .line 223
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@f
    aget-object v1, v1, v0

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_18

    #@17
    .line 228
    .end local v0           #i:I
    :goto_17
    return v0

    #@18
    .line 222
    .restart local v0       #i:I
    :cond_18
    add-int/lit8 v0, v0, -0x1

    #@1a
    goto :goto_b

    #@1b
    .line 228
    .end local v0           #i:I
    :cond_1b
    const/4 v0, -0x1

    #@1c
    goto :goto_17
.end method

.method public getEntries()[Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getEntryValues()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@2
    if-nez v0, :cond_9

    #@4
    .line 182
    invoke-super {p0}, Landroid/preference/DialogPreference;->getSummary()Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    .line 184
    :goto_8
    return-object v0

    #@9
    :cond_9
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@b
    goto :goto_8
.end method

.method public getValue(I)Z
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@2
    aget-boolean v0, v0, p1

    #@4
    return v0
.end method

.method public getValues()[Z
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@2
    return-object v0
.end method

.method protected onDialogClosed(Z)V
    .registers 6
    .parameter "positiveResult"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 252
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    #@4
    .line 254
    if-eqz p1, :cond_11

    #@6
    .line 255
    invoke-virtual {p0}, Landroid/preference/MultiCheckPreference;->getValues()[Z

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Landroid/preference/MultiCheckPreference;->callChangeListener(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_11

    #@10
    .line 260
    :goto_10
    return-void

    #@11
    .line 259
    :cond_11
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mOrigValues:[Z

    #@13
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@15
    iget-object v2, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@17
    array-length v2, v2

    #@18
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1b
    goto :goto_10
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .registers 4
    .parameter "a"
    .parameter "index"

    #@0
    .prologue
    .line 264
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .registers 5
    .parameter "builder"

    #@0
    .prologue
    .line 233
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    #@3
    .line 235
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@5
    if-eqz v0, :cond_b

    #@7
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@9
    if-nez v0, :cond_13

    #@b
    .line 236
    :cond_b
    new-instance v0, Ljava/lang/IllegalStateException;

    #@d
    const-string v1, "ListPreference requires an entries array and an entryValues array."

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 240
    :cond_13
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@15
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@17
    array-length v1, v1

    #@18
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([ZI)[Z

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/preference/MultiCheckPreference;->mOrigValues:[Z

    #@1e
    .line 241
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@20
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@22
    new-instance v2, Landroid/preference/MultiCheckPreference$1;

    #@24
    invoke-direct {v2, p0}, Landroid/preference/MultiCheckPreference$1;-><init>(Landroid/preference/MultiCheckPreference;)V

    #@27
    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    #@2a
    .line 248
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 286
    if-eqz p1, :cond_e

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    const-class v2, Landroid/preference/MultiCheckPreference$SavedState;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    .line 288
    :cond_e
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@11
    .line 295
    :goto_11
    return-void

    #@12
    :cond_12
    move-object v0, p1

    #@13
    .line 292
    check-cast v0, Landroid/preference/MultiCheckPreference$SavedState;

    #@15
    .line 293
    .local v0, myState:Landroid/preference/MultiCheckPreference$SavedState;
    invoke-virtual {v0}, Landroid/preference/MultiCheckPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    invoke-super {p0, v1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1c
    .line 294
    iget-object v1, v0, Landroid/preference/MultiCheckPreference$SavedState;->values:[Z

    #@1e
    invoke-virtual {p0, v1}, Landroid/preference/MultiCheckPreference;->setValues([Z)V

    #@21
    goto :goto_11
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 273
    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 274
    .local v1, superState:Landroid/os/Parcelable;
    invoke-virtual {p0}, Landroid/preference/MultiCheckPreference;->isPersistent()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 281
    .end local v1           #superState:Landroid/os/Parcelable;
    :goto_a
    return-object v1

    #@b
    .line 279
    .restart local v1       #superState:Landroid/os/Parcelable;
    :cond_b
    new-instance v0, Landroid/preference/MultiCheckPreference$SavedState;

    #@d
    invoke-direct {v0, v1}, Landroid/preference/MultiCheckPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@10
    .line 280
    .local v0, myState:Landroid/preference/MultiCheckPreference$SavedState;
    invoke-virtual {p0}, Landroid/preference/MultiCheckPreference;->getValues()[Z

    #@13
    move-result-object v2

    #@14
    iput-object v2, v0, Landroid/preference/MultiCheckPreference$SavedState;->values:[Z

    #@16
    move-object v1, v0

    #@17
    .line 281
    goto :goto_a
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 3
    .parameter "restoreValue"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 269
    return-void
.end method

.method public setEntries(I)V
    .registers 3
    .parameter "entriesResId"

    #@0
    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/preference/MultiCheckPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/preference/MultiCheckPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@f
    .line 92
    return-void
.end method

.method public setEntries([Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "entries"

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Landroid/preference/MultiCheckPreference;->mEntries:[Ljava/lang/CharSequence;

    #@2
    .line 82
    array-length v0, p1

    #@3
    new-array v0, v0, [Z

    #@5
    iput-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@7
    .line 83
    array-length v0, p1

    #@8
    new-array v0, v0, [Z

    #@a
    iput-object v0, p0, Landroid/preference/MultiCheckPreference;->mOrigValues:[Z

    #@c
    .line 84
    return-void
.end method

.method public setEntryValues(I)V
    .registers 3
    .parameter "entryValuesResId"

    #@0
    .prologue
    .line 121
    invoke-virtual {p0}, Landroid/preference/MultiCheckPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-direct {p0, v0}, Landroid/preference/MultiCheckPreference;->setEntryValuesCS([Ljava/lang/CharSequence;)V

    #@f
    .line 122
    return-void
.end method

.method public setEntryValues([Ljava/lang/String;)V
    .registers 4
    .parameter "entryValues"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 111
    iput-object p1, p0, Landroid/preference/MultiCheckPreference;->mEntryValues:[Ljava/lang/String;

    #@3
    .line 112
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@5
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    #@8
    .line 113
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mOrigValues:[Z

    #@a
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    #@d
    .line 114
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "summary"

    #@0
    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@3
    .line 200
    if-nez p1, :cond_d

    #@5
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 201
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@c
    .line 205
    :cond_c
    :goto_c
    return-void

    #@d
    .line 202
    :cond_d
    if-eqz p1, :cond_c

    #@f
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_c

    #@17
    .line 203
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/preference/MultiCheckPreference;->mSummary:Ljava/lang/String;

    #@1d
    goto :goto_c
.end method

.method public setValue(IZ)V
    .registers 4
    .parameter "index"
    .parameter "state"

    #@0
    .prologue
    .line 154
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@2
    aput-boolean p2, v0, p1

    #@4
    .line 155
    return-void
.end method

.method public setValues([Z)V
    .registers 6
    .parameter "values"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 161
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@3
    if-eqz v0, :cond_1d

    #@5
    .line 162
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@7
    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([ZZ)V

    #@a
    .line 163
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mOrigValues:[Z

    #@c
    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([ZZ)V

    #@f
    .line 164
    if-eqz p1, :cond_1d

    #@11
    .line 165
    iget-object v1, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@13
    array-length v0, p1

    #@14
    iget-object v2, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@16
    array-length v2, v2

    #@17
    if-ge v0, v2, :cond_1e

    #@19
    array-length v0, p1

    #@1a
    :goto_1a
    invoke-static {p1, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1d
    .line 169
    :cond_1d
    return-void

    #@1e
    .line 165
    :cond_1e
    iget-object v0, p0, Landroid/preference/MultiCheckPreference;->mSetValues:[Z

    #@20
    array-length v0, v0

    #@21
    goto :goto_1a
.end method
