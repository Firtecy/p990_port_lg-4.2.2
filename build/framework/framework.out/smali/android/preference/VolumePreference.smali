.class public Landroid/preference/VolumePreference;
.super Landroid/preference/SeekBarDialogPreference;
.source "VolumePreference.java"

# interfaces
.implements Landroid/preference/PreferenceManager$OnActivityStopListener;
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/VolumePreference$SeekBarVolumizer;,
        Landroid/preference/VolumePreference$SavedState;,
        Landroid/preference/VolumePreference$VolumeStore;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VolumePreference"


# instance fields
.field private mIsActive:Z

.field protected mOriginalRingerMode:I

.field private mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

.field private mStreamType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/preference/SeekBarDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 64
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Landroid/preference/VolumePreference;->mIsActive:Z

    #@7
    .line 70
    sget-object v1, Lcom/android/internal/R$styleable;->VolumePreference:[I

    #@9
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    .line 72
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@10
    move-result v1

    #@11
    iput v1, p0, Landroid/preference/VolumePreference;->mStreamType:I

    #@13
    .line 73
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@16
    .line 74
    return-void
.end method

.method static synthetic access$000(Landroid/preference/VolumePreference;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Landroid/preference/VolumePreference;->mIsActive:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/preference/VolumePreference;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Landroid/preference/VolumePreference;->mIsActive:Z

    #@2
    return p1
.end method

.method private cleanup()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 151
    invoke-virtual {p0}, Landroid/preference/VolumePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {v2, p0}, Landroid/preference/PreferenceManager;->unregisterOnActivityStopListener(Landroid/preference/PreferenceManager$OnActivityStopListener;)V

    #@8
    .line 153
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@a
    if-eqz v2, :cond_38

    #@c
    .line 154
    invoke-virtual {p0}, Landroid/preference/VolumePreference;->getDialog()Landroid/app/Dialog;

    #@f
    move-result-object v0

    #@10
    .line 155
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_31

    #@12
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_31

    #@18
    .line 156
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@1f
    move-result-object v2

    #@20
    const v3, 0x102035a

    #@23
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@26
    move-result-object v1

    #@27
    .line 158
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_2c

    #@29
    invoke-virtual {v1, v4}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@2c
    .line 160
    :cond_2c
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2e
    invoke-virtual {v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->revertVolume()V

    #@31
    .line 162
    .end local v1           #view:Landroid/view/View;
    :cond_31
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@33
    invoke-virtual {v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stop()V

    #@36
    .line 163
    iput-object v4, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@38
    .line 166
    .end local v0           #dialog:Landroid/app/Dialog;
    :cond_38
    return-void
.end method


# virtual methods
.method public onActivityStop()V
    .registers 2

    #@0
    .prologue
    .line 138
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 139
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@6
    invoke-virtual {v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stopSample()V

    #@9
    .line 143
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/preference/VolumePreference;->mIsActive:Z

    #@c
    .line 145
    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    #@0
    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/preference/SeekBarDialogPreference;->onBindDialogView(Landroid/view/View;)V

    #@3
    .line 84
    const v2, 0x102035a

    #@6
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/widget/SeekBar;

    #@c
    .line 85
    .local v1, seekBar:Landroid/widget/SeekBar;
    new-instance v2, Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@e
    invoke-virtual {p0}, Landroid/preference/VolumePreference;->getContext()Landroid/content/Context;

    #@11
    move-result-object v3

    #@12
    iget v4, p0, Landroid/preference/VolumePreference;->mStreamType:I

    #@14
    invoke-direct {v2, p0, v3, v1, v4}, Landroid/preference/VolumePreference$SeekBarVolumizer;-><init>(Landroid/preference/VolumePreference;Landroid/content/Context;Landroid/widget/SeekBar;I)V

    #@17
    iput-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@19
    .line 87
    invoke-virtual {p0}, Landroid/preference/VolumePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, p0}, Landroid/preference/PreferenceManager;->registerOnActivityStopListener(Landroid/preference/PreferenceManager$OnActivityStopListener;)V

    #@20
    .line 91
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@23
    .line 92
    const/4 v2, 0x1

    #@24
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    #@27
    .line 93
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    #@2a
    .line 96
    invoke-virtual {p0}, Landroid/preference/VolumePreference;->getContext()Landroid/content/Context;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, "audio"

    #@30
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Landroid/media/AudioManager;

    #@36
    .line 97
    .local v0, audioManager:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    #@39
    move-result v2

    #@3a
    iput v2, p0, Landroid/preference/VolumePreference;->mOriginalRingerMode:I

    #@3c
    .line 99
    return-void
.end method

.method protected onDialogClosed(Z)V
    .registers 3
    .parameter "positiveResult"

    #@0
    .prologue
    .line 128
    invoke-super {p0, p1}, Landroid/preference/SeekBarDialogPreference;->onDialogClosed(Z)V

    #@3
    .line 130
    if-nez p1, :cond_e

    #@5
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 131
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@b
    invoke-virtual {v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->revertVolume()V

    #@e
    .line 134
    :cond_e
    invoke-direct {p0}, Landroid/preference/VolumePreference;->cleanup()V

    #@11
    .line 135
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 103
    iget-object v3, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@4
    if-nez v3, :cond_7

    #@6
    .line 122
    :cond_6
    :goto_6
    return v1

    #@7
    .line 104
    :cond_7
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@a
    move-result v3

    #@b
    if-nez v3, :cond_13

    #@d
    move v0, v1

    #@e
    .line 105
    .local v0, isdown:Z
    :goto_e
    sparse-switch p2, :sswitch_data_2e

    #@11
    move v1, v2

    #@12
    .line 122
    goto :goto_6

    #@13
    .end local v0           #isdown:Z
    :cond_13
    move v0, v2

    #@14
    .line 104
    goto :goto_e

    #@15
    .line 107
    .restart local v0       #isdown:Z
    :sswitch_15
    if-eqz v0, :cond_6

    #@17
    .line 108
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@19
    const/4 v3, -0x1

    #@1a
    invoke-virtual {v2, v3}, Landroid/preference/VolumePreference$SeekBarVolumizer;->changeVolumeBy(I)V

    #@1d
    goto :goto_6

    #@1e
    .line 112
    :sswitch_1e
    if-eqz v0, :cond_6

    #@20
    .line 113
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@22
    invoke-virtual {v2, v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->changeVolumeBy(I)V

    #@25
    goto :goto_6

    #@26
    .line 117
    :sswitch_26
    if-eqz v0, :cond_6

    #@28
    .line 118
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2a
    invoke-virtual {v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->muteVolume()V

    #@2d
    goto :goto_6

    #@2e
    .line 105
    :sswitch_data_2e
    .sparse-switch
        0x18 -> :sswitch_1e
        0x19 -> :sswitch_15
        0xa4 -> :sswitch_26
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 191
    if-eqz p1, :cond_e

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    const-class v2, Landroid/preference/VolumePreference$SavedState;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    .line 193
    :cond_e
    invoke-super {p0, p1}, Landroid/preference/SeekBarDialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@11
    .line 202
    :cond_11
    :goto_11
    return-void

    #@12
    :cond_12
    move-object v0, p1

    #@13
    .line 197
    check-cast v0, Landroid/preference/VolumePreference$SavedState;

    #@15
    .line 198
    .local v0, myState:Landroid/preference/VolumePreference$SavedState;
    invoke-virtual {v0}, Landroid/preference/VolumePreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    invoke-super {p0, v1}, Landroid/preference/SeekBarDialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1c
    .line 199
    iget-object v1, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@1e
    if-eqz v1, :cond_11

    #@20
    .line 200
    iget-object v1, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@22
    invoke-virtual {v0}, Landroid/preference/VolumePreference$SavedState;->getVolumeStore()Landroid/preference/VolumePreference$VolumeStore;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v1, v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->onRestoreInstanceState(Landroid/preference/VolumePreference$VolumeStore;)V

    #@29
    goto :goto_11
.end method

.method protected onSampleStarting(Landroid/preference/VolumePreference$SeekBarVolumizer;)V
    .registers 3
    .parameter "volumizer"

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@6
    if-eq p1, v0, :cond_d

    #@8
    .line 170
    iget-object v0, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@a
    invoke-virtual {v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->stopSample()V

    #@d
    .line 172
    :cond_d
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 5

    #@0
    .prologue
    .line 176
    invoke-super {p0}, Landroid/preference/SeekBarDialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 177
    .local v1, superState:Landroid/os/Parcelable;
    invoke-virtual {p0}, Landroid/preference/VolumePreference;->isPersistent()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 186
    .end local v1           #superState:Landroid/os/Parcelable;
    :goto_a
    return-object v1

    #@b
    .line 182
    .restart local v1       #superState:Landroid/os/Parcelable;
    :cond_b
    new-instance v0, Landroid/preference/VolumePreference$SavedState;

    #@d
    invoke-direct {v0, v1}, Landroid/preference/VolumePreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@10
    .line 183
    .local v0, myState:Landroid/preference/VolumePreference$SavedState;
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@12
    if-eqz v2, :cond_1d

    #@14
    .line 184
    iget-object v2, p0, Landroid/preference/VolumePreference;->mSeekBarVolumizer:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@16
    invoke-virtual {v0}, Landroid/preference/VolumePreference$SavedState;->getVolumeStore()Landroid/preference/VolumePreference$VolumeStore;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Landroid/preference/VolumePreference$SeekBarVolumizer;->onSaveInstanceState(Landroid/preference/VolumePreference$VolumeStore;)V

    #@1d
    :cond_1d
    move-object v1, v0

    #@1e
    .line 186
    goto :goto_a
.end method

.method public setStreamType(I)V
    .registers 2
    .parameter "streamType"

    #@0
    .prologue
    .line 77
    iput p1, p0, Landroid/preference/VolumePreference;->mStreamType:I

    #@2
    .line 78
    return-void
.end method
