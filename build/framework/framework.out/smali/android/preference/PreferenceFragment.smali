.class public abstract Landroid/preference/PreferenceFragment;
.super Landroid/app/Fragment;
.source "PreferenceFragment.java"

# interfaces
.implements Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;
    }
.end annotation


# static fields
.field private static final FIRST_REQUEST_CODE:I = 0x64

.field private static final MSG_BIND_PREFERENCES:I = 0x1

.field private static final PREFERENCES_TAG:Ljava/lang/String; = "android:preferences"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mHavePrefs:Z

.field private mInitDone:Z

.field private mList:Landroid/widget/ListView;

.field private mListOnKeyListener:Landroid/view/View$OnKeyListener;

.field private mPreferenceManager:Landroid/preference/PreferenceManager;

.field private final mRequestFocus:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    #@3
    .line 119
    new-instance v0, Landroid/preference/PreferenceFragment$1;

    #@5
    invoke-direct {v0, p0}, Landroid/preference/PreferenceFragment$1;-><init>(Landroid/preference/PreferenceFragment;)V

    #@8
    iput-object v0, p0, Landroid/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    #@a
    .line 131
    new-instance v0, Landroid/preference/PreferenceFragment$2;

    #@c
    invoke-direct {v0, p0}, Landroid/preference/PreferenceFragment$2;-><init>(Landroid/preference/PreferenceFragment;)V

    #@f
    iput-object v0, p0, Landroid/preference/PreferenceFragment;->mRequestFocus:Ljava/lang/Runnable;

    #@11
    .line 364
    new-instance v0, Landroid/preference/PreferenceFragment$3;

    #@13
    invoke-direct {v0, p0}, Landroid/preference/PreferenceFragment$3;-><init>(Landroid/preference/PreferenceFragment;)V

    #@16
    iput-object v0, p0, Landroid/preference/PreferenceFragment;->mListOnKeyListener:Landroid/view/View$OnKeyListener;

    #@18
    return-void
.end method

.method static synthetic access$000(Landroid/preference/PreferenceFragment;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 103
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;->bindPreferences()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/preference/PreferenceFragment;)Landroid/widget/ListView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@2
    return-object v0
.end method

.method private bindPreferences()V
    .registers 3

    #@0
    .prologue
    .line 328
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 329
    .local v0, preferenceScreen:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_d

    #@6
    .line 330
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getListView()Landroid/widget/ListView;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->bind(Landroid/widget/ListView;)V

    #@d
    .line 332
    :cond_d
    return-void
.end method

.method private ensureList()V
    .registers 5

    #@0
    .prologue
    .line 341
    iget-object v2, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@2
    if-eqz v2, :cond_5

    #@4
    .line 362
    :goto_4
    return-void

    #@5
    .line 344
    :cond_5
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getView()Landroid/view/View;

    #@8
    move-result-object v1

    #@9
    .line 345
    .local v1, root:Landroid/view/View;
    if-nez v1, :cond_13

    #@b
    .line 346
    new-instance v2, Ljava/lang/IllegalStateException;

    #@d
    const-string v3, "Content view not yet created"

    #@f
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v2

    #@13
    .line 348
    :cond_13
    const v2, 0x102000a

    #@16
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    .line 349
    .local v0, rawListView:Landroid/view/View;
    instance-of v2, v0, Landroid/widget/ListView;

    #@1c
    if-nez v2, :cond_26

    #@1e
    .line 350
    new-instance v2, Ljava/lang/RuntimeException;

    #@20
    const-string v3, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    #@22
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v2

    #@26
    .line 354
    :cond_26
    check-cast v0, Landroid/widget/ListView;

    #@28
    .end local v0           #rawListView:Landroid/view/View;
    iput-object v0, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@2a
    .line 355
    iget-object v2, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@2c
    if-nez v2, :cond_36

    #@2e
    .line 356
    new-instance v2, Ljava/lang/RuntimeException;

    #@30
    const-string v3, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    #@32
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@35
    throw v2

    #@36
    .line 360
    :cond_36
    iget-object v2, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@38
    iget-object v3, p0, Landroid/preference/PreferenceFragment;->mListOnKeyListener:Landroid/view/View$OnKeyListener;

    #@3a
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@3d
    .line 361
    iget-object v2, p0, Landroid/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    #@3f
    iget-object v3, p0, Landroid/preference/PreferenceFragment;->mRequestFocus:Ljava/lang/Runnable;

    #@41
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@44
    goto :goto_4
.end method

.method private postBindPreferences()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 323
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 325
    :goto_9
    return-void

    #@a
    .line 324
    :cond_a
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@13
    goto :goto_9
.end method

.method private requirePreferenceManager()V
    .registers 3

    #@0
    .prologue
    .line 317
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 318
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "This should be called after super.onCreate."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 320
    :cond_c
    return-void
.end method


# virtual methods
.method public addPreferencesFromIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 271
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;->requirePreferenceManager()V

    #@3
    .line 273
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, p1, v1}, Landroid/preference/PreferenceManager;->inflateFromIntent(Landroid/content/Intent;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceFragment;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    #@10
    .line 274
    return-void
.end method

.method public addPreferencesFromResource(I)V
    .registers 5
    .parameter "preferencesResId"

    #@0
    .prologue
    .line 283
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;->requirePreferenceManager()V

    #@3
    .line 285
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getActivity()Landroid/app/Activity;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, p1, v2}, Landroid/preference/PreferenceManager;->inflateFromResource(Landroid/content/Context;ILandroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceFragment;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    #@14
    .line 287
    return-void
.end method

.method public findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 310
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 311
    const/4 v0, 0x0

    #@5
    .line 313
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@8
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getListView()Landroid/widget/ListView;
    .registers 2

    #@0
    .prologue
    .line 336
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;->ensureList()V

    #@3
    .line 337
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@5
    return-object v0
.end method

.method public getPreferenceManager()Landroid/preference/PreferenceManager;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    return-object v0
.end method

.method public getPreferenceScreen()Landroid/preference/PreferenceScreen;
    .registers 2

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 168
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    #@3
    .line 170
    iget-boolean v2, p0, Landroid/preference/PreferenceFragment;->mHavePrefs:Z

    #@5
    if-eqz v2, :cond_a

    #@7
    .line 171
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;->bindPreferences()V

    #@a
    .line 174
    :cond_a
    const/4 v2, 0x1

    #@b
    iput-boolean v2, p0, Landroid/preference/PreferenceFragment;->mInitDone:Z

    #@d
    .line 176
    if-eqz p1, :cond_20

    #@f
    .line 177
    const-string v2, "android:preferences"

    #@11
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@14
    move-result-object v0

    #@15
    .line 178
    .local v0, container:Landroid/os/Bundle;
    if-eqz v0, :cond_20

    #@17
    .line 179
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@1a
    move-result-object v1

    #@1b
    .line 180
    .local v1, preferenceScreen:Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_20

    #@1d
    .line 181
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->restoreHierarchyState(Landroid/os/Bundle;)V

    #@20
    .line 185
    .end local v0           #container:Landroid/os/Bundle;
    .end local v1           #preferenceScreen:Landroid/preference/PreferenceScreen;
    :cond_20
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 228
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    #@3
    .line 230
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {v0, p1, p2, p3}, Landroid/preference/PreferenceManager;->dispatchActivityResult(IILandroid/content/Intent;)V

    #@8
    .line 231
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 155
    new-instance v0, Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getActivity()Landroid/app/Activity;

    #@8
    move-result-object v1

    #@9
    const/16 v2, 0x64

    #@b
    invoke-direct {v0, v1, v2}, Landroid/preference/PreferenceManager;-><init>(Landroid/app/Activity;I)V

    #@e
    iput-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@10
    .line 156
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@12
    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->setFragment(Landroid/preference/PreferenceFragment;)V

    #@15
    .line 157
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 162
    const v0, 0x10900ab

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 210
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    #@3
    .line 211
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->dispatchActivityDestroy()V

    #@8
    .line 212
    return-void
.end method

.method public onDestroyView()V
    .registers 3

    #@0
    .prologue
    .line 202
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/preference/PreferenceFragment;->mList:Landroid/widget/ListView;

    #@3
    .line 203
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    #@5
    iget-object v1, p0, Landroid/preference/PreferenceFragment;->mRequestFocus:Ljava/lang/Runnable;

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@a
    .line 204
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mHandler:Landroid/os/Handler;

    #@c
    const/4 v1, 0x1

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@10
    .line 205
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    #@13
    .line 206
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 4
    .parameter "preferenceScreen"
    .parameter "preference"

    #@0
    .prologue
    .line 294
    invoke-virtual {p2}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_19

    #@6
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getActivity()Landroid/app/Activity;

    #@9
    move-result-object v0

    #@a
    instance-of v0, v0, Landroid/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;

    #@c
    if-eqz v0, :cond_19

    #@e
    .line 296
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getActivity()Landroid/app/Activity;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;

    #@14
    invoke-interface {v0, p0, p2}, Landroid/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;->onPreferenceStartFragment(Landroid/preference/PreferenceFragment;Landroid/preference/Preference;)Z

    #@17
    move-result v0

    #@18
    .line 299
    :goto_18
    return v0

    #@19
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_18
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    #@0
    .prologue
    .line 216
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 218
    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@6
    move-result-object v1

    #@7
    .line 219
    .local v1, preferenceScreen:Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_16

    #@9
    .line 220
    new-instance v0, Landroid/os/Bundle;

    #@b
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@e
    .line 221
    .local v0, container:Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->saveHierarchyState(Landroid/os/Bundle;)V

    #@11
    .line 222
    const-string v2, "android:preferences"

    #@13
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@16
    .line 224
    .end local v0           #container:Landroid/os/Bundle;
    :cond_16
    return-void
.end method

.method public onStart()V
    .registers 2

    #@0
    .prologue
    .line 189
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    #@3
    .line 190
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->setOnPreferenceTreeClickListener(Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;)V

    #@8
    .line 191
    return-void
.end method

.method public onStop()V
    .registers 3

    #@0
    .prologue
    .line 195
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    #@3
    .line 196
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->dispatchActivityStop()V

    #@8
    .line 197
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setOnPreferenceTreeClickListener(Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;)V

    #@e
    .line 198
    return-void
.end method

.method public setPreferenceScreen(Landroid/preference/PreferenceScreen;)V
    .registers 3
    .parameter "preferenceScreen"

    #@0
    .prologue
    .line 247
    iget-object v0, p0, Landroid/preference/PreferenceFragment;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->setPreferences(Landroid/preference/PreferenceScreen;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_14

    #@8
    if-eqz p1, :cond_14

    #@a
    .line 248
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/preference/PreferenceFragment;->mHavePrefs:Z

    #@d
    .line 249
    iget-boolean v0, p0, Landroid/preference/PreferenceFragment;->mInitDone:Z

    #@f
    if-eqz v0, :cond_14

    #@11
    .line 250
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;->postBindPreferences()V

    #@14
    .line 253
    :cond_14
    return-void
.end method
