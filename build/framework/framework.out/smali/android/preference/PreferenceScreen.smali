.class public final Landroid/preference/PreferenceScreen;
.super Landroid/preference/PreferenceGroup;
.source "PreferenceScreen.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/PreferenceScreen$SavedState;
    }
.end annotation


# instance fields
.field private mDialog:Landroid/app/Dialog;

.field private mListView:Landroid/widget/ListView;

.field private mRootAdapter:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 101
    const v0, 0x101008b

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/PreferenceGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 102
    return-void
.end method

.method private showDialog(Landroid/os/Bundle;)V
    .registers 9
    .parameter "state"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 160
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    #@4
    move-result-object v1

    #@5
    .line 161
    .local v1, context:Landroid/content/Context;
    iget-object v5, p0, Landroid/preference/PreferenceScreen;->mListView:Landroid/widget/ListView;

    #@7
    if-eqz v5, :cond_e

    #@9
    .line 162
    iget-object v5, p0, Landroid/preference/PreferenceScreen;->mListView:Landroid/widget/ListView;

    #@b
    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@e
    .line 165
    :cond_e
    const-string/jumbo v5, "layout_inflater"

    #@11
    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Landroid/view/LayoutInflater;

    #@17
    .line 167
    .local v3, inflater:Landroid/view/LayoutInflater;
    const v5, 0x10900ab

    #@1a
    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@1d
    move-result-object v0

    #@1e
    .line 169
    .local v0, childPrefScreen:Landroid/view/View;
    const v5, 0x102000a

    #@21
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Landroid/widget/ListView;

    #@27
    iput-object v5, p0, Landroid/preference/PreferenceScreen;->mListView:Landroid/widget/ListView;

    #@29
    .line 170
    iget-object v5, p0, Landroid/preference/PreferenceScreen;->mListView:Landroid/widget/ListView;

    #@2b
    invoke-virtual {p0, v5}, Landroid/preference/PreferenceScreen;->bind(Landroid/widget/ListView;)V

    #@2e
    .line 173
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    #@31
    move-result-object v4

    #@32
    .line 174
    .local v4, title:Ljava/lang/CharSequence;
    new-instance v2, Landroid/app/Dialog;

    #@34
    invoke-virtual {v1}, Landroid/content/Context;->getThemeResId()I

    #@37
    move-result v5

    #@38
    invoke-direct {v2, v1, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@3b
    iput-object v2, p0, Landroid/preference/PreferenceScreen;->mDialog:Landroid/app/Dialog;

    #@3d
    .line 175
    .local v2, dialog:Landroid/app/Dialog;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@40
    move-result v5

    #@41
    if-eqz v5, :cond_61

    #@43
    .line 176
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@46
    move-result-object v5

    #@47
    const/4 v6, 0x1

    #@48
    invoke-virtual {v5, v6}, Landroid/view/Window;->requestFeature(I)Z

    #@4b
    .line 180
    :goto_4b
    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    #@4e
    .line 181
    invoke-virtual {v2, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@51
    .line 182
    if-eqz p1, :cond_56

    #@53
    .line 183
    invoke-virtual {v2, p1}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@56
    .line 187
    :cond_56
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5, v2}, Landroid/preference/PreferenceManager;->addPreferencesScreen(Landroid/content/DialogInterface;)V

    #@5d
    .line 189
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    #@60
    .line 190
    return-void

    #@61
    .line 178
    :cond_61
    invoke-virtual {v2, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    #@64
    goto :goto_4b
.end method


# virtual methods
.method public bind(Landroid/widget/ListView;)V
    .registers 3
    .parameter "listView"

    #@0
    .prologue
    .line 144
    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@3
    .line 145
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@a
    .line 147
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->onAttachedToActivity()V

    #@d
    .line 148
    return-void
.end method

.method public getDialog()Landroid/app/Dialog;
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/preference/PreferenceScreen;->mDialog:Landroid/app/Dialog;

    #@2
    return-object v0
.end method

.method public getRootAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/preference/PreferenceScreen;->mRootAdapter:Landroid/widget/ListAdapter;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 120
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->onCreateRootAdapter()Landroid/widget/ListAdapter;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/preference/PreferenceScreen;->mRootAdapter:Landroid/widget/ListAdapter;

    #@a
    .line 123
    :cond_a
    iget-object v0, p0, Landroid/preference/PreferenceScreen;->mRootAdapter:Landroid/widget/ListAdapter;

    #@c
    return-object v0
.end method

.method protected isOnSameScreenAsChildren()Z
    .registers 2

    #@0
    .prologue
    .line 220
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onClick()V
    .registers 2

    #@0
    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getIntent()Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    if-nez v0, :cond_12

    #@6
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getFragment()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    if-nez v0, :cond_12

    #@c
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_13

    #@12
    .line 157
    :cond_12
    :goto_12
    return-void

    #@13
    .line 156
    :cond_13
    const/4 v0, 0x0

    #@14
    invoke-direct {p0, v0}, Landroid/preference/PreferenceScreen;->showDialog(Landroid/os/Bundle;)V

    #@17
    goto :goto_12
.end method

.method protected onCreateRootAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 133
    new-instance v0, Landroid/preference/PreferenceGroupAdapter;

    #@2
    invoke-direct {v0, p0}, Landroid/preference/PreferenceGroupAdapter;-><init>(Landroid/preference/PreferenceGroup;)V

    #@5
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "dialog"

    #@0
    .prologue
    .line 193
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/preference/PreferenceScreen;->mDialog:Landroid/app/Dialog;

    #@3
    .line 194
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->removePreferencesScreen(Landroid/content/DialogInterface;)V

    #@a
    .line 195
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9
    .parameter "parent"
    .parameter "view"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 208
    instance-of v2, p1, Landroid/widget/ListView;

    #@2
    if-eqz v2, :cond_b

    #@4
    .line 209
    check-cast p1, Landroid/widget/ListView;

    #@6
    .end local p1
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    #@9
    move-result v2

    #@a
    sub-int/2addr p3, v2

    #@b
    .line 211
    :cond_b
    invoke-virtual {p0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    .line 212
    .local v0, item:Ljava/lang/Object;
    instance-of v2, v0, Landroid/preference/Preference;

    #@15
    if-nez v2, :cond_18

    #@17
    .line 216
    :goto_17
    return-void

    #@18
    :cond_18
    move-object v1, v0

    #@19
    .line 214
    check-cast v1, Landroid/preference/Preference;

    #@1b
    .line 215
    .local v1, preference:Landroid/preference/Preference;
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->performClick(Landroid/preference/PreferenceScreen;)V

    #@1e
    goto :goto_17
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 239
    if-eqz p1, :cond_e

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    const-class v2, Landroid/preference/PreferenceScreen$SavedState;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    .line 241
    :cond_e
    invoke-super {p0, p1}, Landroid/preference/PreferenceGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@11
    .line 250
    :cond_11
    :goto_11
    return-void

    #@12
    :cond_12
    move-object v0, p1

    #@13
    .line 245
    check-cast v0, Landroid/preference/PreferenceScreen$SavedState;

    #@15
    .line 246
    .local v0, myState:Landroid/preference/PreferenceScreen$SavedState;
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    invoke-super {p0, v1}, Landroid/preference/PreferenceGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1c
    .line 247
    iget-boolean v1, v0, Landroid/preference/PreferenceScreen$SavedState;->isDialogShowing:Z

    #@1e
    if-eqz v1, :cond_11

    #@20
    .line 248
    iget-object v1, v0, Landroid/preference/PreferenceScreen$SavedState;->dialogBundle:Landroid/os/Bundle;

    #@22
    invoke-direct {p0, v1}, Landroid/preference/PreferenceScreen;->showDialog(Landroid/os/Bundle;)V

    #@25
    goto :goto_11
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 5

    #@0
    .prologue
    .line 225
    invoke-super {p0}, Landroid/preference/PreferenceGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v2

    #@4
    .line 226
    .local v2, superState:Landroid/os/Parcelable;
    iget-object v0, p0, Landroid/preference/PreferenceScreen;->mDialog:Landroid/app/Dialog;

    #@6
    .line 227
    .local v0, dialog:Landroid/app/Dialog;
    if-eqz v0, :cond_e

    #@8
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_10

    #@e
    :cond_e
    move-object v1, v2

    #@f
    .line 234
    :goto_f
    return-object v1

    #@10
    .line 231
    :cond_10
    new-instance v1, Landroid/preference/PreferenceScreen$SavedState;

    #@12
    invoke-direct {v1, v2}, Landroid/preference/PreferenceScreen$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@15
    .line 232
    .local v1, myState:Landroid/preference/PreferenceScreen$SavedState;
    const/4 v3, 0x1

    #@16
    iput-boolean v3, v1, Landroid/preference/PreferenceScreen$SavedState;->isDialogShowing:Z

    #@18
    .line 233
    invoke-virtual {v0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    #@1b
    move-result-object v3

    #@1c
    iput-object v3, v1, Landroid/preference/PreferenceScreen$SavedState;->dialogBundle:Landroid/os/Bundle;

    #@1e
    goto :goto_f
.end method
