.class public Landroid/preference/Preference;
.super Ljava/lang/Object;
.source "Preference.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Landroid/preference/OnDependencyChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/Preference$BaseSavedState;,
        Landroid/preference/Preference$OnPreferenceChangeInternalListener;,
        Landroid/preference/Preference$OnPreferenceClickListener;,
        Landroid/preference/Preference$OnPreferenceChangeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Landroid/preference/Preference;",
        ">;",
        "Landroid/preference/OnDependencyChangeListener;"
    }
.end annotation


# static fields
.field public static final DEFAULT_ORDER:I = 0x7fffffff


# instance fields
.field private mBaseMethodCalled:Z

.field private mClickable:Z

.field private mContext:Landroid/content/Context;

.field private mDefaultValue:Ljava/lang/Object;

.field private mDependencyKey:Ljava/lang/String;

.field private mDependencyMet:Z

.field private mDependents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mEnabled:Z

.field private mExtras:Landroid/os/Bundle;

.field private mFragment:Ljava/lang/String;

.field private mHasSpecifiedLayout:Z

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIconResId:I

.field private mId:J

.field private mIntent:Landroid/content/Intent;

.field private mKey:Ljava/lang/String;

.field private mLayoutResId:I

.field private mListener:Landroid/preference/Preference$OnPreferenceChangeInternalListener;

.field private mOnChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private mOnClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

.field private mOrder:I

.field private mPersistent:Z

.field private mPreferenceManager:Landroid/preference/PreferenceManager;

.field private mRequiresKey:Z

.field private mSelectable:Z

.field private mShouldDisableView:Z

.field private mSummary:Ljava/lang/CharSequence;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleRes:I

.field private mWidgetLayoutResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 311
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 312
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 302
    const v0, 0x101008e

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 303
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 213
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 98
    const v3, 0x7fffffff

    #@8
    iput v3, p0, Landroid/preference/Preference;->mOrder:I

    #@a
    .line 111
    iput-boolean v5, p0, Landroid/preference/Preference;->mEnabled:Z

    #@c
    .line 112
    iput-boolean v5, p0, Landroid/preference/Preference;->mSelectable:Z

    #@e
    .line 114
    iput-boolean v5, p0, Landroid/preference/Preference;->mPersistent:Z

    #@10
    .line 117
    iput-boolean v5, p0, Landroid/preference/Preference;->mDependencyMet:Z

    #@12
    .line 122
    iput-boolean v5, p0, Landroid/preference/Preference;->mShouldDisableView:Z

    #@14
    .line 124
    const v3, 0x10900a0

    #@17
    iput v3, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@19
    .line 126
    iput-boolean v4, p0, Landroid/preference/Preference;->mHasSpecifiedLayout:Z

    #@1b
    .line 135
    iput-boolean v4, p0, Landroid/preference/Preference;->mClickable:Z

    #@1d
    .line 214
    iput-object p1, p0, Landroid/preference/Preference;->mContext:Landroid/content/Context;

    #@1f
    .line 216
    sget-object v3, Lcom/android/internal/R$styleable;->Preference:[I

    #@21
    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@24
    move-result-object v0

    #@25
    .line 218
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getIndexCount()I

    #@28
    move-result v2

    #@29
    .local v2, i:I
    :goto_29
    if-ltz v2, :cond_a7

    #@2b
    .line 219
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getIndex(I)I

    #@2e
    move-result v1

    #@2f
    .line 220
    .local v1, attr:I
    packed-switch v1, :pswitch_data_be

    #@32
    .line 218
    :goto_32
    add-int/lit8 v2, v2, -0x1

    #@34
    goto :goto_29

    #@35
    .line 222
    :pswitch_35
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@38
    move-result v3

    #@39
    iput v3, p0, Landroid/preference/Preference;->mIconResId:I

    #@3b
    goto :goto_32

    #@3c
    .line 226
    :pswitch_3c
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    iput-object v3, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@42
    goto :goto_32

    #@43
    .line 230
    :pswitch_43
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@46
    move-result v3

    #@47
    iput v3, p0, Landroid/preference/Preference;->mTitleRes:I

    #@49
    .line 231
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    iput-object v3, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@4f
    goto :goto_32

    #@50
    .line 235
    :pswitch_50
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    iput-object v3, p0, Landroid/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    #@56
    goto :goto_32

    #@57
    .line 239
    :pswitch_57
    iget v3, p0, Landroid/preference/Preference;->mOrder:I

    #@59
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@5c
    move-result v3

    #@5d
    iput v3, p0, Landroid/preference/Preference;->mOrder:I

    #@5f
    goto :goto_32

    #@60
    .line 243
    :pswitch_60
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    iput-object v3, p0, Landroid/preference/Preference;->mFragment:Ljava/lang/String;

    #@66
    goto :goto_32

    #@67
    .line 247
    :pswitch_67
    iget v3, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@69
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@6c
    move-result v3

    #@6d
    iput v3, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@6f
    goto :goto_32

    #@70
    .line 251
    :pswitch_70
    iget v3, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@72
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@75
    move-result v3

    #@76
    iput v3, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@78
    goto :goto_32

    #@79
    .line 255
    :pswitch_79
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@7c
    move-result v3

    #@7d
    iput-boolean v3, p0, Landroid/preference/Preference;->mEnabled:Z

    #@7f
    goto :goto_32

    #@80
    .line 259
    :pswitch_80
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@83
    move-result v3

    #@84
    iput-boolean v3, p0, Landroid/preference/Preference;->mSelectable:Z

    #@86
    goto :goto_32

    #@87
    .line 263
    :pswitch_87
    iget-boolean v3, p0, Landroid/preference/Preference;->mPersistent:Z

    #@89
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@8c
    move-result v3

    #@8d
    iput-boolean v3, p0, Landroid/preference/Preference;->mPersistent:Z

    #@8f
    goto :goto_32

    #@90
    .line 267
    :pswitch_90
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v3

    #@94
    iput-object v3, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@96
    goto :goto_32

    #@97
    .line 271
    :pswitch_97
    invoke-virtual {p0, v0, v1}, Landroid/preference/Preference;->onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;

    #@9a
    move-result-object v3

    #@9b
    iput-object v3, p0, Landroid/preference/Preference;->mDefaultValue:Ljava/lang/Object;

    #@9d
    goto :goto_32

    #@9e
    .line 275
    :pswitch_9e
    iget-boolean v3, p0, Landroid/preference/Preference;->mShouldDisableView:Z

    #@a0
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@a3
    move-result v3

    #@a4
    iput-boolean v3, p0, Landroid/preference/Preference;->mShouldDisableView:Z

    #@a6
    goto :goto_32

    #@a7
    .line 279
    .end local v1           #attr:I
    :cond_a7
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@aa
    .line 281
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@ad
    move-result-object v3

    #@ae
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b1
    move-result-object v3

    #@b2
    const-string v4, "android.preference"

    #@b4
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b7
    move-result v3

    #@b8
    if-nez v3, :cond_bc

    #@ba
    .line 283
    iput-boolean v5, p0, Landroid/preference/Preference;->mHasSpecifiedLayout:Z

    #@bc
    .line 285
    :cond_bc
    return-void

    #@bd
    .line 220
    nop

    #@be
    :pswitch_data_be
    .packed-switch 0x0
        :pswitch_35
        :pswitch_87
        :pswitch_79
        :pswitch_67
        :pswitch_43
        :pswitch_80
        :pswitch_3c
        :pswitch_50
        :pswitch_57
        :pswitch_70
        :pswitch_90
        :pswitch_97
        :pswitch_9e
        :pswitch_60
    .end packed-switch
.end method

.method private dispatchSetInitialValue()V
    .registers 4

    #@0
    .prologue
    .line 1338
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    .line 1339
    .local v0, shouldPersist:Z
    if-eqz v0, :cond_12

    #@6
    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@9
    move-result-object v1

    #@a
    iget-object v2, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@c
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_1d

    #@12
    .line 1340
    :cond_12
    iget-object v1, p0, Landroid/preference/Preference;->mDefaultValue:Ljava/lang/Object;

    #@14
    if-eqz v1, :cond_1c

    #@16
    .line 1341
    const/4 v1, 0x0

    #@17
    iget-object v2, p0, Landroid/preference/Preference;->mDefaultValue:Ljava/lang/Object;

    #@19
    invoke-virtual {p0, v1, v2}, Landroid/preference/Preference;->onSetInitialValue(ZLjava/lang/Object;)V

    #@1c
    .line 1346
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 1344
    :cond_1d
    const/4 v1, 0x1

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-virtual {p0, v1, v2}, Landroid/preference/Preference;->onSetInitialValue(ZLjava/lang/Object;)V

    #@22
    goto :goto_1c
.end method

.method private registerDependency()V
    .registers 5

    #@0
    .prologue
    .line 1173
    iget-object v1, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_9

    #@8
    .line 1182
    :goto_8
    return-void

    #@9
    .line 1175
    :cond_9
    iget-object v1, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@b
    invoke-virtual {p0, v1}, Landroid/preference/Preference;->findPreferenceInHierarchy(Ljava/lang/String;)Landroid/preference/Preference;

    #@e
    move-result-object v0

    #@f
    .line 1176
    .local v0, preference:Landroid/preference/Preference;
    if-eqz v0, :cond_15

    #@11
    .line 1177
    invoke-direct {v0, p0}, Landroid/preference/Preference;->registerDependent(Landroid/preference/Preference;)V

    #@14
    goto :goto_8

    #@15
    .line 1179
    :cond_15
    new-instance v1, Ljava/lang/IllegalStateException;

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v3, "Dependency \""

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    iget-object v3, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v3, "\" not found for preference \""

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    iget-object v3, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, "\" (title: \""

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    iget-object v3, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    const-string v3, "\""

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v1
.end method

.method private registerDependent(Landroid/preference/Preference;)V
    .registers 3
    .parameter "dependent"

    #@0
    .prologue
    .line 1221
    iget-object v0, p0, Landroid/preference/Preference;->mDependents:Ljava/util/List;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1222
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/preference/Preference;->mDependents:Ljava/util/List;

    #@b
    .line 1225
    :cond_b
    iget-object v0, p0, Landroid/preference/Preference;->mDependents:Ljava/util/List;

    #@d
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@10
    .line 1227
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldDisableDependents()Z

    #@13
    move-result v0

    #@14
    invoke-virtual {p1, p0, v0}, Landroid/preference/Preference;->onDependencyChanged(Landroid/preference/Preference;Z)V

    #@17
    .line 1228
    return-void
.end method

.method private setEnabledStateOnViews(Landroid/view/View;Z)V
    .registers 6
    .parameter "v"
    .parameter "enabled"

    #@0
    .prologue
    .line 557
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    #@3
    .line 559
    instance-of v2, p1, Landroid/view/ViewGroup;

    #@5
    if-eqz v2, :cond_1c

    #@7
    move-object v1, p1

    #@8
    .line 560
    check-cast v1, Landroid/view/ViewGroup;

    #@a
    .line 561
    .local v1, vg:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@d
    move-result v2

    #@e
    add-int/lit8 v0, v2, -0x1

    #@10
    .local v0, i:I
    :goto_10
    if-ltz v0, :cond_1c

    #@12
    .line 562
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@15
    move-result-object v2

    #@16
    invoke-direct {p0, v2, p2}, Landroid/preference/Preference;->setEnabledStateOnViews(Landroid/view/View;Z)V

    #@19
    .line 561
    add-int/lit8 v0, v0, -0x1

    #@1b
    goto :goto_10

    #@1c
    .line 565
    .end local v0           #i:I
    .end local v1           #vg:Landroid/view/ViewGroup;
    :cond_1c
    return-void
.end method

.method private tryCommit(Landroid/content/SharedPreferences$Editor;)V
    .registers 4
    .parameter "editor"

    #@0
    .prologue
    .line 1369
    iget-object v1, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->shouldCommit()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_b

    #@8
    .line 1371
    :try_start_8
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_b
    .catch Ljava/lang/AbstractMethodError; {:try_start_8 .. :try_end_b} :catch_c

    #@b
    .line 1379
    :cond_b
    :goto_b
    return-void

    #@c
    .line 1372
    :catch_c
    move-exception v0

    #@d
    .line 1376
    .local v0, unused:Ljava/lang/AbstractMethodError;
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@10
    goto :goto_b
.end method

.method private unregisterDependency()V
    .registers 3

    #@0
    .prologue
    .line 1185
    iget-object v1, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@2
    if-eqz v1, :cond_f

    #@4
    .line 1186
    iget-object v1, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@6
    invoke-virtual {p0, v1}, Landroid/preference/Preference;->findPreferenceInHierarchy(Ljava/lang/String;)Landroid/preference/Preference;

    #@9
    move-result-object v0

    #@a
    .line 1187
    .local v0, oldDependency:Landroid/preference/Preference;
    if-eqz v0, :cond_f

    #@c
    .line 1188
    invoke-direct {v0, p0}, Landroid/preference/Preference;->unregisterDependent(Landroid/preference/Preference;)V

    #@f
    .line 1191
    .end local v0           #oldDependency:Landroid/preference/Preference;
    :cond_f
    return-void
.end method

.method private unregisterDependent(Landroid/preference/Preference;)V
    .registers 3
    .parameter "dependent"

    #@0
    .prologue
    .line 1239
    iget-object v0, p0, Landroid/preference/Preference;->mDependents:Ljava/util/List;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1240
    iget-object v0, p0, Landroid/preference/Preference;->mDependents:Ljava/util/List;

    #@6
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@9
    .line 1242
    :cond_9
    return-void
.end method


# virtual methods
.method protected callChangeListener(Ljava/lang/Object;)Z
    .registers 3
    .parameter "newValue"

    #@0
    .prologue
    .line 912
    iget-object v0, p0, Landroid/preference/Preference;->mOnChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/Preference;->mOnChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    #@8
    invoke-interface {v0, p0, p1}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public compareTo(Landroid/preference/Preference;)I
    .registers 4
    .parameter "another"

    #@0
    .prologue
    const v1, 0x7fffffff

    #@3
    .line 1092
    iget v0, p0, Landroid/preference/Preference;->mOrder:I

    #@5
    if-ne v0, v1, :cond_f

    #@7
    iget v0, p0, Landroid/preference/Preference;->mOrder:I

    #@9
    if-ne v0, v1, :cond_15

    #@b
    iget v0, p1, Landroid/preference/Preference;->mOrder:I

    #@d
    if-eq v0, v1, :cond_15

    #@f
    .line 1095
    :cond_f
    iget v0, p0, Landroid/preference/Preference;->mOrder:I

    #@11
    iget v1, p1, Landroid/preference/Preference;->mOrder:I

    #@13
    sub-int/2addr v0, v1

    #@14
    .line 1102
    :goto_14
    return v0

    #@15
    .line 1096
    :cond_15
    iget-object v0, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@17
    if-nez v0, :cond_1b

    #@19
    .line 1097
    const/4 v0, 0x1

    #@1a
    goto :goto_14

    #@1b
    .line 1098
    :cond_1b
    iget-object v0, p1, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@1d
    if-nez v0, :cond_21

    #@1f
    .line 1099
    const/4 v0, -0x1

    #@20
    goto :goto_14

    #@21
    .line 1102
    :cond_21
    iget-object v0, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@23
    iget-object v1, p1, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@25
    invoke-static {v0, v1}, Lcom/android/internal/util/CharSequences;->compareToIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    #@28
    move-result v0

    #@29
    goto :goto_14
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 80
    check-cast p1, Landroid/preference/Preference;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/preference/Preference;->compareTo(Landroid/preference/Preference;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method dispatchRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "container"

    #@0
    .prologue
    .line 1775
    invoke-virtual {p0}, Landroid/preference/Preference;->hasKey()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_20

    #@6
    .line 1776
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@8
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@b
    move-result-object v0

    #@c
    .line 1777
    .local v0, state:Landroid/os/Parcelable;
    if-eqz v0, :cond_20

    #@e
    .line 1778
    const/4 v1, 0x0

    #@f
    iput-boolean v1, p0, Landroid/preference/Preference;->mBaseMethodCalled:Z

    #@11
    .line 1779
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@14
    .line 1780
    iget-boolean v1, p0, Landroid/preference/Preference;->mBaseMethodCalled:Z

    #@16
    if-nez v1, :cond_20

    #@18
    .line 1781
    new-instance v1, Ljava/lang/IllegalStateException;

    #@1a
    const-string v2, "Derived class did not call super.onRestoreInstanceState()"

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 1786
    .end local v0           #state:Landroid/os/Parcelable;
    :cond_20
    return-void
.end method

.method dispatchSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "container"

    #@0
    .prologue
    .line 1722
    invoke-virtual {p0}, Landroid/preference/Preference;->hasKey()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_20

    #@6
    .line 1723
    const/4 v1, 0x0

    #@7
    iput-boolean v1, p0, Landroid/preference/Preference;->mBaseMethodCalled:Z

    #@9
    .line 1724
    invoke-virtual {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@c
    move-result-object v0

    #@d
    .line 1725
    .local v0, state:Landroid/os/Parcelable;
    iget-boolean v1, p0, Landroid/preference/Preference;->mBaseMethodCalled:Z

    #@f
    if-nez v1, :cond_19

    #@11
    .line 1726
    new-instance v1, Ljava/lang/IllegalStateException;

    #@13
    const-string v2, "Derived class did not call super.onSaveInstanceState()"

    #@15
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@18
    throw v1

    #@19
    .line 1729
    :cond_19
    if-eqz v0, :cond_20

    #@1b
    .line 1730
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@1d
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@20
    .line 1733
    .end local v0           #state:Landroid/os/Parcelable;
    :cond_20
    return-void
.end method

.method protected findPreferenceInHierarchy(Ljava/lang/String;)Landroid/preference/Preference;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1204
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@8
    if-nez v0, :cond_c

    #@a
    .line 1205
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 1208
    :goto_b
    return-object v0

    #@c
    :cond_c
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@e
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@11
    move-result-object v0

    #@12
    goto :goto_b
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 1013
    iget-object v0, p0, Landroid/preference/Preference;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getDependency()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1313
    iget-object v0, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getEditor()Landroid/content/SharedPreferences$Editor;
    .registers 2

    #@0
    .prologue
    .line 1061
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 1062
    const/4 v0, 0x0

    #@5
    .line 1065
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@8
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getExtras()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 374
    iget-object v0, p0, Landroid/preference/Preference;->mExtras:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 375
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/preference/Preference;->mExtras:Landroid/os/Bundle;

    #@b
    .line 377
    :cond_b
    iget-object v0, p0, Landroid/preference/Preference;->mExtras:Landroid/os/Bundle;

    #@d
    return-object v0
.end method

.method getFilterableStringBuilder()Ljava/lang/StringBuilder;
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x20

    #@2
    .line 1683
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 1684
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    #@a
    move-result-object v2

    #@b
    .line 1685
    .local v2, title:Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_18

    #@11
    .line 1686
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 1688
    :cond_18
    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    #@1b
    move-result-object v1

    #@1c
    .line 1689
    .local v1, summary:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_29

    #@22
    .line 1690
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    .line 1692
    :cond_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@2c
    move-result v3

    #@2d
    if-lez v3, :cond_38

    #@2f
    .line 1694
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@32
    move-result v3

    #@33
    add-int/lit8 v3, v3, -0x1

    #@35
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    #@38
    .line 1696
    :cond_38
    return-object v0
.end method

.method public getFragment()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 365
    iget-object v0, p0, Landroid/preference/Preference;->mFragment:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 682
    iget-object v0, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method getId()J
    .registers 3

    #@0
    .prologue
    .line 809
    iget-wide v0, p0, Landroid/preference/Preference;->mId:J

    #@2
    return-wide v0
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Landroid/preference/Preference;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 842
    iget-object v0, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLayoutResource()I
    .registers 2

    #@0
    .prologue
    .line 417
    iget v0, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@2
    return v0
.end method

.method public getOnPreferenceChangeListener()Landroid/preference/Preference$OnPreferenceChangeListener;
    .registers 2

    #@0
    .prologue
    .line 932
    iget-object v0, p0, Landroid/preference/Preference;->mOnChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    #@2
    return-object v0
.end method

.method public getOnPreferenceClickListener()Landroid/preference/Preference$OnPreferenceClickListener;
    .registers 2

    #@0
    .prologue
    .line 950
    iget-object v0, p0, Landroid/preference/Preference;->mOnClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    #@2
    return-object v0
.end method

.method public getOrder()I
    .registers 2

    #@0
    .prologue
    .line 597
    iget v0, p0, Landroid/preference/Preference;->mOrder:I

    #@2
    return v0
.end method

.method protected getPersistedBoolean(Z)Z
    .registers 4
    .parameter "defaultReturnValue"

    #@0
    .prologue
    .line 1655
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1659
    .end local p1
    :goto_6
    return p1

    #@7
    .restart local p1
    :cond_7
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@f
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@12
    move-result p1

    #@13
    goto :goto_6
.end method

.method protected getPersistedFloat(F)F
    .registers 4
    .parameter "defaultReturnValue"

    #@0
    .prologue
    .line 1567
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1571
    .end local p1
    :goto_6
    return p1

    #@7
    .restart local p1
    :cond_7
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@f
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    #@12
    move-result p1

    #@13
    goto :goto_6
.end method

.method protected getPersistedInt(I)I
    .registers 4
    .parameter "defaultReturnValue"

    #@0
    .prologue
    .line 1523
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1527
    .end local p1
    :goto_6
    return p1

    #@7
    .restart local p1
    :cond_7
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@f
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@12
    move-result p1

    #@13
    goto :goto_6
.end method

.method protected getPersistedLong(J)J
    .registers 5
    .parameter "defaultReturnValue"

    #@0
    .prologue
    .line 1611
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1615
    .end local p1
    :goto_6
    return-wide p1

    #@7
    .restart local p1
    :cond_7
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@f
    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    #@12
    move-result-wide p1

    #@13
    goto :goto_6
.end method

.method protected getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "defaultReturnValue"

    #@0
    .prologue
    .line 1424
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1428
    .end local p1
    :goto_6
    return-object p1

    #@7
    .restart local p1
    :cond_7
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@f
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object p1

    #@13
    goto :goto_6
.end method

.method protected getPersistedStringSet(Ljava/util/Set;)Ljava/util/Set;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1479
    .local p1, defaultReturnValue:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 1483
    .end local p1           #defaultReturnValue:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :goto_6
    return-object p1

    #@7
    .restart local p1       #defaultReturnValue:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_7
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@f
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    #@12
    move-result-object p1

    #@13
    goto :goto_6
.end method

.method public getPreferenceManager()Landroid/preference/PreferenceManager;
    .registers 2

    #@0
    .prologue
    .line 1142
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    return-object v0
.end method

.method public getSharedPreferences()Landroid/content/SharedPreferences;
    .registers 2

    #@0
    .prologue
    .line 1034
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 1035
    const/4 v0, 0x0

    #@5
    .line 1038
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@8
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getShouldDisableView()Z
    .registers 2

    #@0
    .prologue
    .line 799
    iget-boolean v0, p0, Landroid/preference/Preference;->mShouldDisableView:Z

    #@2
    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 692
    iget-object v0, p0, Landroid/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 645
    iget-object v0, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTitleRes()I
    .registers 2

    #@0
    .prologue
    .line 635
    iget v0, p0, Landroid/preference/Preference;->mTitleRes:I

    #@2
    return v0
.end method

.method public getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 3
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 461
    if-nez p1, :cond_6

    #@2
    .line 462
    invoke-virtual {p0, p2}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    #@5
    move-result-object p1

    #@6
    .line 464
    :cond_6
    invoke-virtual {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    #@9
    .line 465
    return-object p1
.end method

.method public getWidgetLayoutResource()I
    .registers 2

    #@0
    .prologue
    .line 444
    iget v0, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@2
    return v0
.end method

.method public hasKey()Z
    .registers 2

    #@0
    .prologue
    .line 866
    iget-object v0, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method hasSpecifiedLayout()Z
    .registers 2

    #@0
    .prologue
    .line 1663
    iget-boolean v0, p0, Landroid/preference/Preference;->mHasSpecifiedLayout:Z

    #@2
    return v0
.end method

.method public isClickable()Z
    .registers 2

    #@0
    .prologue
    .line 773
    iget-boolean v0, p0, Landroid/preference/Preference;->mClickable:Z

    #@2
    return v0
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 740
    iget-boolean v0, p0, Landroid/preference/Preference;->mEnabled:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Landroid/preference/Preference;->mDependencyMet:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isPersistent()Z
    .registers 2

    #@0
    .prologue
    .line 876
    iget-boolean v0, p0, Landroid/preference/Preference;->mPersistent:Z

    #@2
    return v0
.end method

.method public isSelectable()Z
    .registers 2

    #@0
    .prologue
    .line 761
    iget-boolean v0, p0, Landroid/preference/Preference;->mSelectable:Z

    #@2
    return v0
.end method

.method protected notifyChanged()V
    .registers 2

    #@0
    .prologue
    .line 1120
    iget-object v0, p0, Landroid/preference/Preference;->mListener:Landroid/preference/Preference$OnPreferenceChangeInternalListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1121
    iget-object v0, p0, Landroid/preference/Preference;->mListener:Landroid/preference/Preference$OnPreferenceChangeInternalListener;

    #@6
    invoke-interface {v0, p0}, Landroid/preference/Preference$OnPreferenceChangeInternalListener;->onPreferenceChange(Landroid/preference/Preference;)V

    #@9
    .line 1123
    :cond_9
    return-void
.end method

.method public notifyDependencyChange(Z)V
    .registers 6
    .parameter "disableDependents"

    #@0
    .prologue
    .line 1252
    iget-object v0, p0, Landroid/preference/Preference;->mDependents:Ljava/util/List;

    #@2
    .line 1254
    .local v0, dependents:Ljava/util/List;,"Ljava/util/List<Landroid/preference/Preference;>;"
    if-nez v0, :cond_5

    #@4
    .line 1262
    :cond_4
    return-void

    #@5
    .line 1258
    :cond_5
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@8
    move-result v1

    #@9
    .line 1259
    .local v1, dependentsCount:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v1, :cond_4

    #@c
    .line 1260
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Landroid/preference/Preference;

    #@12
    invoke-virtual {v3, p0, p1}, Landroid/preference/Preference;->onDependencyChanged(Landroid/preference/Preference;Z)V

    #@15
    .line 1259
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_a
.end method

.method protected notifyHierarchyChanged()V
    .registers 2

    #@0
    .prologue
    .line 1131
    iget-object v0, p0, Landroid/preference/Preference;->mListener:Landroid/preference/Preference$OnPreferenceChangeInternalListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1132
    iget-object v0, p0, Landroid/preference/Preference;->mListener:Landroid/preference/Preference$OnPreferenceChangeInternalListener;

    #@6
    invoke-interface {v0, p0}, Landroid/preference/Preference$OnPreferenceChangeInternalListener;->onPreferenceHierarchyChange(Landroid/preference/Preference;)V

    #@9
    .line 1134
    :cond_9
    return-void
.end method

.method protected onAttachedToActivity()V
    .registers 1

    #@0
    .prologue
    .line 1168
    invoke-direct {p0}, Landroid/preference/Preference;->registerDependency()V

    #@3
    .line 1169
    return-void
.end method

.method protected onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .registers 4
    .parameter "preferenceManager"

    #@0
    .prologue
    .line 1152
    iput-object p1, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    .line 1154
    invoke-virtual {p1}, Landroid/preference/PreferenceManager;->getNextId()J

    #@5
    move-result-wide v0

    #@6
    iput-wide v0, p0, Landroid/preference/Preference;->mId:J

    #@8
    .line 1156
    invoke-direct {p0}, Landroid/preference/Preference;->dispatchSetInitialValue()V

    #@b
    .line 1157
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .registers 11
    .parameter "view"

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x0

    #@3
    .line 511
    const v7, 0x1020016

    #@6
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v4

    #@a
    check-cast v4, Landroid/widget/TextView;

    #@c
    .line 513
    .local v4, titleView:Landroid/widget/TextView;
    if-eqz v4, :cond_1e

    #@e
    .line 514
    invoke-virtual {p0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    #@11
    move-result-object v3

    #@12
    .line 515
    .local v3, title:Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@15
    move-result v7

    #@16
    if-nez v7, :cond_7c

    #@18
    .line 516
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1b
    .line 517
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    #@1e
    .line 523
    .end local v3           #title:Ljava/lang/CharSequence;
    :cond_1e
    :goto_1e
    const v7, 0x1020010

    #@21
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Landroid/widget/TextView;

    #@27
    .line 525
    .local v2, summaryView:Landroid/widget/TextView;
    if-eqz v2, :cond_39

    #@29
    .line 526
    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    #@2c
    move-result-object v1

    #@2d
    .line 527
    .local v1, summary:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@30
    move-result v7

    #@31
    if-nez v7, :cond_80

    #@33
    .line 528
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@36
    .line 529
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    #@39
    .line 535
    .end local v1           #summary:Ljava/lang/CharSequence;
    :cond_39
    :goto_39
    const v7, 0x1020006

    #@3c
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Landroid/widget/ImageView;

    #@42
    .line 536
    .local v0, imageView:Landroid/widget/ImageView;
    if-eqz v0, :cond_70

    #@44
    .line 537
    iget v7, p0, Landroid/preference/Preference;->mIconResId:I

    #@46
    if-nez v7, :cond_4c

    #@48
    iget-object v7, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@4a
    if-eqz v7, :cond_69

    #@4c
    .line 538
    :cond_4c
    iget-object v7, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@4e
    if-nez v7, :cond_60

    #@50
    .line 539
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@57
    move-result-object v7

    #@58
    iget v8, p0, Landroid/preference/Preference;->mIconResId:I

    #@5a
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@5d
    move-result-object v7

    #@5e
    iput-object v7, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@60
    .line 541
    :cond_60
    iget-object v7, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@62
    if-eqz v7, :cond_69

    #@64
    .line 542
    iget-object v7, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@66
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@69
    .line 545
    :cond_69
    iget-object v7, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@6b
    if-eqz v7, :cond_84

    #@6d
    :goto_6d
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    #@70
    .line 548
    :cond_70
    iget-boolean v5, p0, Landroid/preference/Preference;->mShouldDisableView:Z

    #@72
    if-eqz v5, :cond_7b

    #@74
    .line 549
    invoke-virtual {p0}, Landroid/preference/Preference;->isEnabled()Z

    #@77
    move-result v5

    #@78
    invoke-direct {p0, p1, v5}, Landroid/preference/Preference;->setEnabledStateOnViews(Landroid/view/View;Z)V

    #@7b
    .line 551
    :cond_7b
    return-void

    #@7c
    .line 519
    .end local v0           #imageView:Landroid/widget/ImageView;
    .end local v2           #summaryView:Landroid/widget/TextView;
    .restart local v3       #title:Ljava/lang/CharSequence;
    :cond_7c
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    #@7f
    goto :goto_1e

    #@80
    .line 531
    .end local v3           #title:Ljava/lang/CharSequence;
    .restart local v1       #summary:Ljava/lang/CharSequence;
    .restart local v2       #summaryView:Landroid/widget/TextView;
    :cond_80
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    #@83
    goto :goto_39

    #@84
    .end local v1           #summary:Ljava/lang/CharSequence;
    .restart local v0       #imageView:Landroid/widget/ImageView;
    :cond_84
    move v5, v6

    #@85
    .line 545
    goto :goto_6d
.end method

.method protected onClick()V
    .registers 1

    #@0
    .prologue
    .line 819
    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "parent"

    #@0
    .prologue
    .line 482
    iget-object v3, p0, Landroid/preference/Preference;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v4, "layout_inflater"

    #@5
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Landroid/view/LayoutInflater;

    #@b
    .line 485
    .local v1, layoutInflater:Landroid/view/LayoutInflater;
    iget v3, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@d
    const/4 v4, 0x0

    #@e
    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@11
    move-result-object v0

    #@12
    .line 487
    .local v0, layout:Landroid/view/View;
    const v3, 0x1020018

    #@15
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/view/ViewGroup;

    #@1b
    .line 489
    .local v2, widgetFrame:Landroid/view/ViewGroup;
    if-eqz v2, :cond_26

    #@1d
    .line 490
    iget v3, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@1f
    if-eqz v3, :cond_27

    #@21
    .line 491
    iget v3, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@23
    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@26
    .line 496
    :cond_26
    :goto_26
    return-object v0

    #@27
    .line 493
    :cond_27
    const/16 v3, 0x8

    #@29
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@2c
    goto :goto_26
.end method

.method public onDependencyChanged(Landroid/preference/Preference;Z)V
    .registers 4
    .parameter "dependency"
    .parameter "disableDependent"

    #@0
    .prologue
    .line 1271
    iget-boolean v0, p0, Landroid/preference/Preference;->mDependencyMet:Z

    #@2
    if-ne v0, p2, :cond_13

    #@4
    .line 1272
    if-nez p2, :cond_14

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    iput-boolean v0, p0, Landroid/preference/Preference;->mDependencyMet:Z

    #@9
    .line 1275
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldDisableDependents()Z

    #@c
    move-result v0

    #@d
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->notifyDependencyChange(Z)V

    #@10
    .line 1277
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@13
    .line 1279
    :cond_13
    return-void

    #@14
    .line 1272
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_7
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .registers 4
    .parameter "a"
    .parameter "index"

    #@0
    .prologue
    .line 328
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 1001
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onPrepareForRemoval()V
    .registers 1

    #@0
    .prologue
    .line 1322
    invoke-direct {p0}, Landroid/preference/Preference;->unregisterDependency()V

    #@3
    .line 1323
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 1799
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/preference/Preference;->mBaseMethodCalled:Z

    #@3
    .line 1800
    sget-object v0, Landroid/preference/Preference$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    #@5
    if-eq p1, v0, :cond_11

    #@7
    if-eqz p1, :cond_11

    #@9
    .line 1801
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v1, "Wrong state class -- expecting Preference State"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 1803
    :cond_11
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 2

    #@0
    .prologue
    .line 1748
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/preference/Preference;->mBaseMethodCalled:Z

    #@3
    .line 1749
    sget-object v0, Landroid/preference/Preference$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    #@5
    return-object v0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 3
    .parameter "restorePersistedValue"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1366
    return-void
.end method

.method public peekExtras()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Landroid/preference/Preference;->mExtras:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method performClick(Landroid/preference/PreferenceScreen;)V
    .registers 6
    .parameter "preferenceScreen"

    #@0
    .prologue
    .line 966
    invoke-virtual {p0}, Landroid/preference/Preference;->isEnabled()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_d

    #@6
    invoke-virtual {p0}, Landroid/preference/Preference;->isClickable()Z

    #@9
    move-result v3

    #@a
    if-nez v3, :cond_d

    #@c
    .line 991
    :cond_c
    :goto_c
    return-void

    #@d
    .line 971
    :cond_d
    invoke-virtual {p0}, Landroid/preference/Preference;->onClick()V

    #@10
    .line 973
    iget-object v3, p0, Landroid/preference/Preference;->mOnClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    #@12
    if-eqz v3, :cond_1c

    #@14
    iget-object v3, p0, Landroid/preference/Preference;->mOnClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    #@16
    invoke-interface {v3, p0}, Landroid/preference/Preference$OnPreferenceClickListener;->onPreferenceClick(Landroid/preference/Preference;)Z

    #@19
    move-result v3

    #@1a
    if-nez v3, :cond_c

    #@1c
    .line 977
    :cond_1c
    invoke-virtual {p0}, Landroid/preference/Preference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@1f
    move-result-object v2

    #@20
    .line 978
    .local v2, preferenceManager:Landroid/preference/PreferenceManager;
    if-eqz v2, :cond_30

    #@22
    .line 979
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getOnPreferenceTreeClickListener()Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;

    #@25
    move-result-object v1

    #@26
    .line 981
    .local v1, listener:Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;
    if-eqz p1, :cond_30

    #@28
    if-eqz v1, :cond_30

    #@2a
    invoke-interface {v1, p1, p0}, Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_c

    #@30
    .line 987
    .end local v1           #listener:Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;
    :cond_30
    iget-object v3, p0, Landroid/preference/Preference;->mIntent:Landroid/content/Intent;

    #@32
    if-eqz v3, :cond_c

    #@34
    .line 988
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    #@37
    move-result-object v0

    #@38
    .line 989
    .local v0, context:Landroid/content/Context;
    iget-object v3, p0, Landroid/preference/Preference;->mIntent:Landroid/content/Intent;

    #@3a
    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@3d
    goto :goto_c
.end method

.method protected persistBoolean(Z)Z
    .registers 6
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 1629
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_21

    #@8
    .line 1630
    if-nez p1, :cond_b

    #@a
    move v1, v2

    #@b
    :cond_b
    invoke-virtual {p0, v1}, Landroid/preference/Preference;->getPersistedBoolean(Z)Z

    #@e
    move-result v1

    #@f
    if-ne p1, v1, :cond_12

    #@11
    .line 1640
    :goto_11
    return v2

    #@12
    .line 1635
    :cond_12
    iget-object v1, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@14
    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@17
    move-result-object v0

    #@18
    .line 1636
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@1a
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@1d
    .line 1637
    invoke-direct {p0, v0}, Landroid/preference/Preference;->tryCommit(Landroid/content/SharedPreferences$Editor;)V

    #@20
    goto :goto_11

    #@21
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_21
    move v2, v1

    #@22
    .line 1640
    goto :goto_11
.end method

.method protected persistFloat(F)Z
    .registers 5
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1541
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_21

    #@7
    .line 1542
    const/high16 v2, 0x7fc0

    #@9
    invoke-virtual {p0, v2}, Landroid/preference/Preference;->getPersistedFloat(F)F

    #@c
    move-result v2

    #@d
    cmpl-float v2, p1, v2

    #@f
    if-nez v2, :cond_12

    #@11
    .line 1552
    :goto_11
    return v1

    #@12
    .line 1547
    :cond_12
    iget-object v2, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@14
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@17
    move-result-object v0

    #@18
    .line 1548
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@1a
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    #@1d
    .line 1549
    invoke-direct {p0, v0}, Landroid/preference/Preference;->tryCommit(Landroid/content/SharedPreferences$Editor;)V

    #@20
    goto :goto_11

    #@21
    .line 1552
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_11
.end method

.method protected persistInt(I)Z
    .registers 5
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1497
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_1f

    #@7
    .line 1498
    xor-int/lit8 v2, p1, -0x1

    #@9
    invoke-virtual {p0, v2}, Landroid/preference/Preference;->getPersistedInt(I)I

    #@c
    move-result v2

    #@d
    if-ne p1, v2, :cond_10

    #@f
    .line 1508
    :goto_f
    return v1

    #@10
    .line 1503
    :cond_10
    iget-object v2, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@12
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@15
    move-result-object v0

    #@16
    .line 1504
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@18
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    #@1b
    .line 1505
    invoke-direct {p0, v0}, Landroid/preference/Preference;->tryCommit(Landroid/content/SharedPreferences$Editor;)V

    #@1e
    goto :goto_f

    #@1f
    .line 1508
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_f
.end method

.method protected persistLong(J)Z
    .registers 7
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1585
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_22

    #@7
    .line 1586
    const-wide/16 v2, -0x1

    #@9
    xor-long/2addr v2, p1

    #@a
    invoke-virtual {p0, v2, v3}, Landroid/preference/Preference;->getPersistedLong(J)J

    #@d
    move-result-wide v2

    #@e
    cmp-long v2, p1, v2

    #@10
    if-nez v2, :cond_13

    #@12
    .line 1596
    :goto_12
    return v1

    #@13
    .line 1591
    :cond_13
    iget-object v2, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@15
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@18
    move-result-object v0

    #@19
    .line 1592
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@1b
    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    #@1e
    .line 1593
    invoke-direct {p0, v0}, Landroid/preference/Preference;->tryCommit(Landroid/content/SharedPreferences$Editor;)V

    #@21
    goto :goto_12

    #@22
    .line 1596
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_12
.end method

.method protected persistString(Ljava/lang/String;)Z
    .registers 5
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1395
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_1e

    #@7
    .line 1397
    const/4 v2, 0x0

    #@8
    invoke-virtual {p0, v2}, Landroid/preference/Preference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    if-ne p1, v2, :cond_f

    #@e
    .line 1407
    :goto_e
    return v1

    #@f
    .line 1402
    :cond_f
    iget-object v2, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@11
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@14
    move-result-object v0

    #@15
    .line 1403
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@17
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@1a
    .line 1404
    invoke-direct {p0, v0}, Landroid/preference/Preference;->tryCommit(Landroid/content/SharedPreferences$Editor;)V

    #@1d
    goto :goto_e

    #@1e
    .line 1407
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_e
.end method

.method protected persistStringSet(Ljava/util/Set;)Z
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, values:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x1

    #@1
    .line 1447
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldPersist()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_22

    #@7
    .line 1449
    const/4 v2, 0x0

    #@8
    invoke-virtual {p0, v2}, Landroid/preference/Preference;->getPersistedStringSet(Ljava/util/Set;)Ljava/util/Set;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_13

    #@12
    .line 1459
    :goto_12
    return v1

    #@13
    .line 1454
    :cond_13
    iget-object v2, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@15
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getEditor()Landroid/content/SharedPreferences$Editor;

    #@18
    move-result-object v0

    #@19
    .line 1455
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@1b
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    #@1e
    .line 1456
    invoke-direct {p0, v0}, Landroid/preference/Preference;->tryCommit(Landroid/content/SharedPreferences$Editor;)V

    #@21
    goto :goto_12

    #@22
    .line 1459
    .end local v0           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_12
.end method

.method requireKey()V
    .registers 3

    #@0
    .prologue
    .line 853
    iget-object v0, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 854
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Preference does not have a key assigned."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 857
    :cond_c
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/preference/Preference;->mRequiresKey:Z

    #@f
    .line 858
    return-void
.end method

.method public restoreHierarchyState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "container"

    #@0
    .prologue
    .line 1761
    invoke-virtual {p0, p1}, Landroid/preference/Preference;->dispatchRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 1762
    return-void
.end method

.method public saveHierarchyState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "container"

    #@0
    .prologue
    .line 1708
    invoke-virtual {p0, p1}, Landroid/preference/Preference;->dispatchSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 1709
    return-void
.end method

.method public setClickable(Z)V
    .registers 3
    .parameter "clickable"

    #@0
    .prologue
    .line 766
    iget-boolean v0, p0, Landroid/preference/Preference;->mClickable:Z

    #@2
    if-eq v0, p1, :cond_6

    #@4
    .line 767
    iput-boolean p1, p0, Landroid/preference/Preference;->mClickable:Z

    #@6
    .line 770
    :cond_6
    return-void
.end method

.method public setDefaultValue(Ljava/lang/Object;)V
    .registers 2
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1333
    iput-object p1, p0, Landroid/preference/Preference;->mDefaultValue:Ljava/lang/Object;

    #@2
    .line 1334
    return-void
.end method

.method public setDependency(Ljava/lang/String;)V
    .registers 2
    .parameter "dependencyKey"

    #@0
    .prologue
    .line 1299
    invoke-direct {p0}, Landroid/preference/Preference;->unregisterDependency()V

    #@3
    .line 1302
    iput-object p1, p0, Landroid/preference/Preference;->mDependencyKey:Ljava/lang/String;

    #@5
    .line 1303
    invoke-direct {p0}, Landroid/preference/Preference;->registerDependency()V

    #@8
    .line 1304
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 724
    iget-boolean v0, p0, Landroid/preference/Preference;->mEnabled:Z

    #@2
    if-eq v0, p1, :cond_10

    #@4
    .line 725
    iput-boolean p1, p0, Landroid/preference/Preference;->mEnabled:Z

    #@6
    .line 728
    invoke-virtual {p0}, Landroid/preference/Preference;->shouldDisableDependents()Z

    #@9
    move-result v0

    #@a
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->notifyDependencyChange(Z)V

    #@d
    .line 730
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@10
    .line 732
    :cond_10
    return-void
.end method

.method public setFragment(Ljava/lang/String;)V
    .registers 2
    .parameter "fragment"

    #@0
    .prologue
    .line 356
    iput-object p1, p0, Landroid/preference/Preference;->mFragment:Ljava/lang/String;

    #@2
    .line 357
    return-void
.end method

.method public setIcon(I)V
    .registers 3
    .parameter "iconResId"

    #@0
    .prologue
    .line 671
    iput p1, p0, Landroid/preference/Preference;->mIconResId:I

    #@2
    .line 672
    iget-object v0, p0, Landroid/preference/Preference;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 673
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "icon"

    #@0
    .prologue
    .line 657
    if-nez p1, :cond_6

    #@2
    iget-object v0, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@4
    if-nez v0, :cond_c

    #@6
    :cond_6
    if-eqz p1, :cond_11

    #@8
    iget-object v0, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@a
    if-eq v0, p1, :cond_11

    #@c
    .line 658
    :cond_c
    iput-object p1, p0, Landroid/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    #@e
    .line 660
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@11
    .line 662
    :cond_11
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 338
    iput-object p1, p0, Landroid/preference/Preference;->mIntent:Landroid/content/Intent;

    #@2
    .line 339
    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 828
    iput-object p1, p0, Landroid/preference/Preference;->mKey:Ljava/lang/String;

    #@2
    .line 830
    iget-boolean v0, p0, Landroid/preference/Preference;->mRequiresKey:Z

    #@4
    if-eqz v0, :cond_f

    #@6
    invoke-virtual {p0}, Landroid/preference/Preference;->hasKey()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_f

    #@c
    .line 831
    invoke-virtual {p0}, Landroid/preference/Preference;->requireKey()V

    #@f
    .line 833
    :cond_f
    return-void
.end method

.method public setLayoutResource(I)V
    .registers 3
    .parameter "layoutResId"

    #@0
    .prologue
    .line 403
    iget v0, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@2
    if-eq p1, v0, :cond_7

    #@4
    .line 405
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/preference/Preference;->mHasSpecifiedLayout:Z

    #@7
    .line 408
    :cond_7
    iput p1, p0, Landroid/preference/Preference;->mLayoutResId:I

    #@9
    .line 409
    return-void
.end method

.method final setOnPreferenceChangeInternalListener(Landroid/preference/Preference$OnPreferenceChangeInternalListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 1113
    iput-object p1, p0, Landroid/preference/Preference;->mListener:Landroid/preference/Preference$OnPreferenceChangeInternalListener;

    #@2
    .line 1114
    return-void
.end method

.method public setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .registers 2
    .parameter "onPreferenceChangeListener"

    #@0
    .prologue
    .line 922
    iput-object p1, p0, Landroid/preference/Preference;->mOnChangeListener:Landroid/preference/Preference$OnPreferenceChangeListener;

    #@2
    .line 923
    return-void
.end method

.method public setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V
    .registers 2
    .parameter "onPreferenceClickListener"

    #@0
    .prologue
    .line 941
    iput-object p1, p0, Landroid/preference/Preference;->mOnClickListener:Landroid/preference/Preference$OnPreferenceClickListener;

    #@2
    .line 942
    return-void
.end method

.method public setOrder(I)V
    .registers 3
    .parameter "order"

    #@0
    .prologue
    .line 581
    iget v0, p0, Landroid/preference/Preference;->mOrder:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 582
    iput p1, p0, Landroid/preference/Preference;->mOrder:I

    #@6
    .line 585
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyHierarchyChanged()V

    #@9
    .line 587
    :cond_9
    return-void
.end method

.method public setPersistent(Z)V
    .registers 2
    .parameter "persistent"

    #@0
    .prologue
    .line 900
    iput-boolean p1, p0, Landroid/preference/Preference;->mPersistent:Z

    #@2
    .line 901
    return-void
.end method

.method public setSelectable(Z)V
    .registers 3
    .parameter "selectable"

    #@0
    .prologue
    .line 749
    iget-boolean v0, p0, Landroid/preference/Preference;->mSelectable:Z

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 750
    iput-boolean p1, p0, Landroid/preference/Preference;->mSelectable:Z

    #@6
    .line 751
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@9
    .line 753
    :cond_9
    return-void
.end method

.method public setShouldDisableView(Z)V
    .registers 2
    .parameter "shouldDisableView"

    #@0
    .prologue
    .line 789
    iput-boolean p1, p0, Landroid/preference/Preference;->mShouldDisableView:Z

    #@2
    .line 790
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@5
    .line 791
    return-void
.end method

.method public setSummary(I)V
    .registers 3
    .parameter "summaryResId"

    #@0
    .prologue
    .line 714
    iget-object v0, p0, Landroid/preference/Preference;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    #@9
    .line 715
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "summary"

    #@0
    .prologue
    .line 701
    if-nez p1, :cond_6

    #@2
    iget-object v0, p0, Landroid/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    #@4
    if-nez v0, :cond_10

    #@6
    :cond_6
    if-eqz p1, :cond_15

    #@8
    iget-object v0, p0, Landroid/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    #@a
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_15

    #@10
    .line 702
    :cond_10
    iput-object p1, p0, Landroid/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    #@12
    .line 703
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@15
    .line 705
    :cond_15
    return-void
.end method

.method public setTitle(I)V
    .registers 3
    .parameter "titleResId"

    #@0
    .prologue
    .line 623
    iget-object v0, p0, Landroid/preference/Preference;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    #@9
    .line 624
    iput p1, p0, Landroid/preference/Preference;->mTitleRes:I

    #@b
    .line 625
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 609
    if-nez p1, :cond_6

    #@2
    iget-object v0, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@4
    if-nez v0, :cond_10

    #@6
    :cond_6
    if-eqz p1, :cond_18

    #@8
    iget-object v0, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@a
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_18

    #@10
    .line 610
    :cond_10
    const/4 v0, 0x0

    #@11
    iput v0, p0, Landroid/preference/Preference;->mTitleRes:I

    #@13
    .line 611
    iput-object p1, p0, Landroid/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    #@15
    .line 612
    invoke-virtual {p0}, Landroid/preference/Preference;->notifyChanged()V

    #@18
    .line 614
    :cond_18
    return-void
.end method

.method public setWidgetLayoutResource(I)V
    .registers 3
    .parameter "widgetLayoutResId"

    #@0
    .prologue
    .line 431
    iget v0, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@2
    if-eq p1, v0, :cond_7

    #@4
    .line 433
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/preference/Preference;->mHasSpecifiedLayout:Z

    #@7
    .line 435
    :cond_7
    iput p1, p0, Landroid/preference/Preference;->mWidgetLayoutResId:I

    #@9
    .line 436
    return-void
.end method

.method public shouldCommit()Z
    .registers 2

    #@0
    .prologue
    .line 1077
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 1078
    const/4 v0, 0x0

    #@5
    .line 1081
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@8
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->shouldCommit()Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public shouldDisableDependents()Z
    .registers 2

    #@0
    .prologue
    .line 1288
    invoke-virtual {p0}, Landroid/preference/Preference;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method protected shouldPersist()Z
    .registers 2

    #@0
    .prologue
    .line 889
    iget-object v0, p0, Landroid/preference/Preference;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-eqz v0, :cond_12

    #@4
    invoke-virtual {p0}, Landroid/preference/Preference;->isPersistent()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_12

    #@a
    invoke-virtual {p0}, Landroid/preference/Preference;->hasKey()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1668
    invoke-virtual {p0}, Landroid/preference/Preference;->getFilterableStringBuilder()Ljava/lang/StringBuilder;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method
