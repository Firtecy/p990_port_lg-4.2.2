.class Landroid/preference/VolumePreference$SeekBarVolumizer$2;
.super Landroid/database/ContentObserver;
.source "VolumePreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/preference/VolumePreference$SeekBarVolumizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;


# direct methods
.method constructor <init>(Landroid/preference/VolumePreference$SeekBarVolumizer;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 313
    iput-object p1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$2;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 5
    .parameter "selfChange"

    #@0
    .prologue
    .line 316
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    #@3
    .line 317
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$2;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@5
    invoke-static {v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;

    #@8
    move-result-object v1

    #@9
    if-eqz v1, :cond_2c

    #@b
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$2;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@d
    invoke-static {v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@10
    move-result-object v1

    #@11
    if-eqz v1, :cond_2c

    #@13
    .line 318
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$2;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@15
    invoke-static {v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$2;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@1b
    invoke-static {v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@22
    move-result v0

    #@23
    .line 319
    .local v0, volume:I
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$2;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@25
    invoke-static {v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$300(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/widget/SeekBar;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    #@2c
    .line 321
    .end local v0           #volume:I
    :cond_2c
    return-void
.end method
