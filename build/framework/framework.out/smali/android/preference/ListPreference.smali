.class public Landroid/preference/ListPreference;
.super Landroid/preference/DialogPreference;
.source "ListPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/ListPreference$SavedState;
    }
.end annotation


# instance fields
.field private mClickedDialogEntryIndex:I

.field private mEntries:[Ljava/lang/CharSequence;

.field private mEntryValues:[Ljava/lang/CharSequence;

.field private mSummary:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 48
    sget-object v1, Lcom/android/internal/R$styleable;->ListPreference:[I

    #@6
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 50
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@10
    .line 51
    const/4 v1, 0x1

    #@11
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    #@14
    move-result-object v1

    #@15
    iput-object v1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@17
    .line 52
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1a
    .line 57
    sget-object v1, Lcom/android/internal/R$styleable;->Preference:[I

    #@1c
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1f
    move-result-object v0

    #@20
    .line 59
    const/4 v1, 0x7

    #@21
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@27
    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2a
    .line 61
    return-void
.end method

.method static synthetic access$002(Landroid/preference/ListPreference;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 38
    iput p1, p0, Landroid/preference/ListPreference;->mClickedDialogEntryIndex:I

    #@2
    return p1
.end method

.method private getValueIndex()I
    .registers 2

    #@0
    .prologue
    .line 224
    iget-object v0, p0, Landroid/preference/ListPreference;->mValue:Ljava/lang/String;

    #@2
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method


# virtual methods
.method public findIndexOfValue(Ljava/lang/String;)I
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 213
    if-eqz p1, :cond_1b

    #@2
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@4
    if-eqz v1, :cond_1b

    #@6
    .line 214
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@8
    array-length v1, v1

    #@9
    add-int/lit8 v0, v1, -0x1

    #@b
    .local v0, i:I
    :goto_b
    if-ltz v0, :cond_1b

    #@d
    .line 215
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@f
    aget-object v1, v1, v0

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_18

    #@17
    .line 220
    .end local v0           #i:I
    :goto_17
    return v0

    #@18
    .line 214
    .restart local v0       #i:I
    :cond_18
    add-int/lit8 v0, v0, -0x1

    #@1a
    goto :goto_b

    #@1b
    .line 220
    .end local v0           #i:I
    :cond_1b
    const/4 v0, -0x1

    #@1c
    goto :goto_17
.end method

.method public getEntries()[Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getEntry()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 202
    invoke-direct {p0}, Landroid/preference/ListPreference;->getValueIndex()I

    #@3
    move-result v0

    #@4
    .line 203
    .local v0, index:I
    if-ltz v0, :cond_f

    #@6
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@8
    if-eqz v1, :cond_f

    #@a
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@c
    aget-object v1, v1, v0

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method public getEntryValues()[Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .registers 5

    #@0
    .prologue
    .line 148
    invoke-virtual {p0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    .line 149
    .local v0, entry:Ljava/lang/CharSequence;
    iget-object v1, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@6
    if-eqz v1, :cond_a

    #@8
    if-nez v0, :cond_f

    #@a
    .line 150
    :cond_a
    invoke-super {p0}, Landroid/preference/DialogPreference;->getSummary()Ljava/lang/CharSequence;

    #@d
    move-result-object v1

    #@e
    .line 152
    :goto_e
    return-object v1

    #@f
    :cond_f
    iget-object v1, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@11
    const/4 v2, 0x1

    #@12
    new-array v2, v2, [Ljava/lang/Object;

    #@14
    const/4 v3, 0x0

    #@15
    aput-object v0, v2, v3

    #@17
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    goto :goto_e
.end method

.method public getValue()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/preference/ListPreference;->mValue:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method protected onDialogClosed(Z)V
    .registers 5
    .parameter "positiveResult"

    #@0
    .prologue
    .line 261
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    #@3
    .line 263
    if-eqz p1, :cond_20

    #@5
    iget v1, p0, Landroid/preference/ListPreference;->mClickedDialogEntryIndex:I

    #@7
    if-ltz v1, :cond_20

    #@9
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@b
    if-eqz v1, :cond_20

    #@d
    .line 264
    iget-object v1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@f
    iget v2, p0, Landroid/preference/ListPreference;->mClickedDialogEntryIndex:I

    #@11
    aget-object v1, v1, v2

    #@13
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 265
    .local v0, value:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->callChangeListener(Ljava/lang/Object;)Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_20

    #@1d
    .line 266
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    #@20
    .line 269
    .end local v0           #value:Ljava/lang/String;
    :cond_20
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .registers 4
    .parameter "a"
    .parameter "index"

    #@0
    .prologue
    .line 273
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .registers 6
    .parameter "builder"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 229
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    #@4
    .line 231
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@6
    if-eqz v0, :cond_c

    #@8
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@a
    if-nez v0, :cond_14

    #@c
    .line 232
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    #@e
    const-string v1, "ListPreference requires an entries array and an entryValues array."

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 236
    :cond_14
    invoke-direct {p0}, Landroid/preference/ListPreference;->getValueIndex()I

    #@17
    move-result v0

    #@18
    iput v0, p0, Landroid/preference/ListPreference;->mClickedDialogEntryIndex:I

    #@1a
    .line 237
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@1c
    iget v1, p0, Landroid/preference/ListPreference;->mClickedDialogEntryIndex:I

    #@1e
    new-instance v2, Landroid/preference/ListPreference$1;

    #@20
    invoke-direct {v2, p0}, Landroid/preference/ListPreference$1;-><init>(Landroid/preference/ListPreference;)V

    #@23
    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@26
    .line 256
    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@29
    .line 257
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 296
    if-eqz p1, :cond_e

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    const-class v2, Landroid/preference/ListPreference$SavedState;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    .line 298
    :cond_e
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@11
    .line 305
    :goto_11
    return-void

    #@12
    :cond_12
    move-object v0, p1

    #@13
    .line 302
    check-cast v0, Landroid/preference/ListPreference$SavedState;

    #@15
    .line 303
    .local v0, myState:Landroid/preference/ListPreference$SavedState;
    invoke-virtual {v0}, Landroid/preference/ListPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    invoke-super {p0, v1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1c
    .line 304
    iget-object v1, v0, Landroid/preference/ListPreference$SavedState;->value:Ljava/lang/String;

    #@1e
    invoke-virtual {p0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    #@21
    goto :goto_11
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 283
    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 284
    .local v1, superState:Landroid/os/Parcelable;
    invoke-virtual {p0}, Landroid/preference/ListPreference;->isPersistent()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 291
    .end local v1           #superState:Landroid/os/Parcelable;
    :goto_a
    return-object v1

    #@b
    .line 289
    .restart local v1       #superState:Landroid/os/Parcelable;
    :cond_b
    new-instance v0, Landroid/preference/ListPreference$SavedState;

    #@d
    invoke-direct {v0, v1}, Landroid/preference/ListPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@10
    .line 290
    .local v0, myState:Landroid/preference/ListPreference$SavedState;
    invoke-virtual {p0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    iput-object v2, v0, Landroid/preference/ListPreference$SavedState;->value:Ljava/lang/String;

    #@16
    move-object v1, v0

    #@17
    .line 291
    goto :goto_a
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 4
    .parameter "restoreValue"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 278
    if-eqz p1, :cond_c

    #@2
    iget-object v0, p0, Landroid/preference/ListPreference;->mValue:Ljava/lang/String;

    #@4
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object p2

    #@8
    .end local p2
    :goto_8
    invoke-virtual {p0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    #@b
    .line 279
    return-void

    #@c
    .line 278
    .restart local p2
    :cond_c
    check-cast p2, Ljava/lang/String;

    #@e
    goto :goto_8
.end method

.method public setEntries(I)V
    .registers 3
    .parameter "entriesResId"

    #@0
    .prologue
    .line 86
    invoke-virtual {p0}, Landroid/preference/ListPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    #@f
    .line 87
    return-void
.end method

.method public setEntries([Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "entries"

    #@0
    .prologue
    .line 78
    iput-object p1, p0, Landroid/preference/ListPreference;->mEntries:[Ljava/lang/CharSequence;

    #@2
    .line 79
    return-void
.end method

.method public setEntryValues(I)V
    .registers 3
    .parameter "entryValuesResId"

    #@0
    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/preference/ListPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    #@f
    .line 115
    return-void
.end method

.method public setEntryValues([Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "entryValues"

    #@0
    .prologue
    .line 106
    iput-object p1, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@2
    .line 107
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "summary"

    #@0
    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@3
    .line 168
    if-nez p1, :cond_d

    #@5
    iget-object v0, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 169
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@c
    .line 173
    :cond_c
    :goto_c
    return-void

    #@d
    .line 170
    :cond_d
    if-eqz p1, :cond_c

    #@f
    iget-object v0, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_c

    #@17
    .line 171
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/preference/ListPreference;->mSummary:Ljava/lang/String;

    #@1d
    goto :goto_c
.end method

.method public setValue(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 133
    iput-object p1, p0, Landroid/preference/ListPreference;->mValue:Ljava/lang/String;

    #@2
    .line 135
    invoke-virtual {p0, p1}, Landroid/preference/ListPreference;->persistString(Ljava/lang/String;)Z

    #@5
    .line 136
    return-void
.end method

.method public setValueIndex(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 182
    iget-object v0, p0, Landroid/preference/ListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    #@6
    aget-object v0, v0, p1

    #@8
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    #@f
    .line 184
    :cond_f
    return-void
.end method
