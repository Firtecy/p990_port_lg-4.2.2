.class public abstract Landroid/preference/DialogPreference;
.super Landroid/preference/Preference;
.source "DialogPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/preference/PreferenceManager$OnActivityDestroyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/DialogPreference$SavedState;
    }
.end annotation


# instance fields
.field private mBuilder:Landroid/app/AlertDialog$Builder;

.field private mDialog:Landroid/app/Dialog;

.field private mDialogIcon:Landroid/graphics/drawable/Drawable;

.field private mDialogLayoutResId:I

.field private mDialogMessage:Ljava/lang/CharSequence;

.field private mDialogTitle:Ljava/lang/CharSequence;

.field private mNegativeButtonText:Ljava/lang/CharSequence;

.field private mPositiveButtonText:Ljava/lang/CharSequence;

.field private mWhichButtonClicked:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 90
    const v0, 0x1010091

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 71
    sget-object v1, Lcom/android/internal/R$styleable;->DialogPreference:[I

    #@6
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 73
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/preference/DialogPreference;->mDialogTitle:Ljava/lang/CharSequence;

    #@10
    .line 74
    iget-object v1, p0, Landroid/preference/DialogPreference;->mDialogTitle:Ljava/lang/CharSequence;

    #@12
    if-nez v1, :cond_1a

    #@14
    .line 77
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getTitle()Ljava/lang/CharSequence;

    #@17
    move-result-object v1

    #@18
    iput-object v1, p0, Landroid/preference/DialogPreference;->mDialogTitle:Ljava/lang/CharSequence;

    #@1a
    .line 79
    :cond_1a
    const/4 v1, 0x1

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    iput-object v1, p0, Landroid/preference/DialogPreference;->mDialogMessage:Ljava/lang/CharSequence;

    #@21
    .line 80
    const/4 v1, 0x2

    #@22
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@25
    move-result-object v1

    #@26
    iput-object v1, p0, Landroid/preference/DialogPreference;->mDialogIcon:Landroid/graphics/drawable/Drawable;

    #@28
    .line 81
    const/4 v1, 0x3

    #@29
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    iput-object v1, p0, Landroid/preference/DialogPreference;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@2f
    .line 82
    const/4 v1, 0x4

    #@30
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    iput-object v1, p0, Landroid/preference/DialogPreference;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@36
    .line 83
    const/4 v1, 0x5

    #@37
    iget v2, p0, Landroid/preference/DialogPreference;->mDialogLayoutResId:I

    #@39
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@3c
    move-result v1

    #@3d
    iput v1, p0, Landroid/preference/DialogPreference;->mDialogLayoutResId:I

    #@3f
    .line 85
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@42
    .line 87
    return-void
.end method

.method private requestInputMethod(Landroid/app/Dialog;)V
    .registers 4
    .parameter "dialog"

    #@0
    .prologue
    .line 325
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    .line 326
    .local v0, window:Landroid/view/Window;
    const/4 v1, 0x5

    #@5
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    #@8
    .line 327
    return-void
.end method


# virtual methods
.method public getDialog()Landroid/app/Dialog;
    .registers 2

    #@0
    .prologue
    .line 402
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@2
    return-object v0
.end method

.method public getDialogIcon()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialogIcon:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getDialogLayoutResource()I
    .registers 2

    #@0
    .prologue
    .line 249
    iget v0, p0, Landroid/preference/DialogPreference;->mDialogLayoutResId:I

    #@2
    return v0
.end method

.method public getDialogMessage()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialogMessage:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getDialogTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialogTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getNegativeButtonText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Landroid/preference/DialogPreference;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getPositiveButtonText()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/preference/DialogPreference;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method protected needInputMethod()Z
    .registers 2

    #@0
    .prologue
    .line 318
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onActivityDestroy()V
    .registers 2

    #@0
    .prologue
    .line 410
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@6
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 415
    :cond_c
    :goto_c
    return-void

    #@d
    .line 414
    :cond_d
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@f
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    #@12
    goto :goto_c
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    .line 354
    const v3, 0x102000b

    #@3
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    .line 356
    .local v0, dialogMessageView:Landroid/view/View;
    if-eqz v0, :cond_29

    #@9
    .line 357
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getDialogMessage()Ljava/lang/CharSequence;

    #@c
    move-result-object v1

    #@d
    .line 358
    .local v1, message:Ljava/lang/CharSequence;
    const/16 v2, 0x8

    #@f
    .line 360
    .local v2, newVisibility:I
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_20

    #@15
    .line 361
    instance-of v3, v0, Landroid/widget/TextView;

    #@17
    if-eqz v3, :cond_1f

    #@19
    move-object v3, v0

    #@1a
    .line 362
    check-cast v3, Landroid/widget/TextView;

    #@1c
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1f
    .line 365
    :cond_1f
    const/4 v2, 0x0

    #@20
    .line 368
    :cond_20
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@23
    move-result v3

    #@24
    if-eq v3, v2, :cond_29

    #@26
    .line 369
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@29
    .line 372
    .end local v1           #message:Ljava/lang/CharSequence;
    .end local v2           #newVisibility:I
    :cond_29
    return-void
.end method

.method protected onClick()V
    .registers 2

    #@0
    .prologue
    .line 264
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@6
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_d

    #@c
    .line 267
    :goto_c
    return-void

    #@d
    .line 266
    :cond_d
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    #@11
    goto :goto_c
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 375
    iput p2, p0, Landroid/preference/DialogPreference;->mWhichButtonClicked:I

    #@2
    .line 376
    return-void
.end method

.method protected onCreateDialogView()Landroid/view/View;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 338
    iget v2, p0, Landroid/preference/DialogPreference;->mDialogLayoutResId:I

    #@3
    if-nez v2, :cond_6

    #@5
    .line 343
    :goto_5
    return-object v1

    #@6
    .line 342
    :cond_6
    iget-object v2, p0, Landroid/preference/DialogPreference;->mBuilder:Landroid/app/AlertDialog$Builder;

    #@8
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    #@b
    move-result-object v2

    #@c
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@f
    move-result-object v0

    #@10
    .line 343
    .local v0, inflater:Landroid/view/LayoutInflater;
    iget v2, p0, Landroid/preference/DialogPreference;->mDialogLayoutResId:I

    #@12
    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@15
    move-result-object v1

    #@16
    goto :goto_5
.end method

.method protected onDialogClosed(Z)V
    .registers 2
    .parameter "positiveResult"

    #@0
    .prologue
    .line 394
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter "dialog"

    #@0
    .prologue
    .line 380
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->unregisterOnActivityDestroyListener(Landroid/preference/PreferenceManager$OnActivityDestroyListener;)V

    #@7
    .line 382
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@a
    .line 383
    iget v0, p0, Landroid/preference/DialogPreference;->mWhichButtonClicked:I

    #@c
    const/4 v1, -0x1

    #@d
    if-ne v0, v1, :cond_14

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    #@13
    .line 384
    return-void

    #@14
    .line 383
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_10
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .registers 2
    .parameter "builder"

    #@0
    .prologue
    .line 260
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 432
    if-eqz p1, :cond_e

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    const-class v2, Landroid/preference/DialogPreference$SavedState;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    .line 434
    :cond_e
    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@11
    .line 443
    :cond_11
    :goto_11
    return-void

    #@12
    :cond_12
    move-object v0, p1

    #@13
    .line 438
    check-cast v0, Landroid/preference/DialogPreference$SavedState;

    #@15
    .line 439
    .local v0, myState:Landroid/preference/DialogPreference$SavedState;
    invoke-virtual {v0}, Landroid/preference/DialogPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    invoke-super {p0, v1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1c
    .line 440
    iget-boolean v1, v0, Landroid/preference/DialogPreference$SavedState;->isDialogShowing:Z

    #@1e
    if-eqz v1, :cond_11

    #@20
    .line 441
    iget-object v1, v0, Landroid/preference/DialogPreference$SavedState;->dialogBundle:Landroid/os/Bundle;

    #@22
    invoke-virtual {p0, v1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    #@25
    goto :goto_11
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 419
    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 420
    .local v1, superState:Landroid/os/Parcelable;
    iget-object v2, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@6
    if-eqz v2, :cond_10

    #@8
    iget-object v2, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@a
    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    #@d
    move-result v2

    #@e
    if-nez v2, :cond_12

    #@10
    :cond_10
    move-object v0, v1

    #@11
    .line 427
    :goto_11
    return-object v0

    #@12
    .line 424
    :cond_12
    new-instance v0, Landroid/preference/DialogPreference$SavedState;

    #@14
    invoke-direct {v0, v1}, Landroid/preference/DialogPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@17
    .line 425
    .local v0, myState:Landroid/preference/DialogPreference$SavedState;
    const/4 v2, 0x1

    #@18
    iput-boolean v2, v0, Landroid/preference/DialogPreference$SavedState;->isDialogShowing:Z

    #@1a
    .line 426
    iget-object v2, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@1c
    invoke-virtual {v2}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    #@1f
    move-result-object v2

    #@20
    iput-object v2, v0, Landroid/preference/DialogPreference$SavedState;->dialogBundle:Landroid/os/Bundle;

    #@22
    goto :goto_11
.end method

.method public setDialogIcon(I)V
    .registers 3
    .parameter "dialogIconRes"

    #@0
    .prologue
    .line 164
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/preference/DialogPreference;->mDialogIcon:Landroid/graphics/drawable/Drawable;

    #@e
    .line 165
    return-void
.end method

.method public setDialogIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "dialogIcon"

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Landroid/preference/DialogPreference;->mDialogIcon:Landroid/graphics/drawable/Drawable;

    #@2
    .line 155
    return-void
.end method

.method public setDialogLayoutResource(I)V
    .registers 2
    .parameter "dialogLayoutResId"

    #@0
    .prologue
    .line 239
    iput p1, p0, Landroid/preference/DialogPreference;->mDialogLayoutResId:I

    #@2
    .line 240
    return-void
.end method

.method public setDialogMessage(I)V
    .registers 3
    .parameter "dialogMessageResId"

    #@0
    .prologue
    .line 137
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    #@b
    .line 138
    return-void
.end method

.method public setDialogMessage(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "dialogMessage"

    #@0
    .prologue
    .line 129
    iput-object p1, p0, Landroid/preference/DialogPreference;->mDialogMessage:Ljava/lang/CharSequence;

    #@2
    .line 130
    return-void
.end method

.method public setDialogTitle(I)V
    .registers 3
    .parameter "dialogTitleResId"

    #@0
    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    #@b
    .line 108
    return-void
.end method

.method public setDialogTitle(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "dialogTitle"

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Landroid/preference/DialogPreference;->mDialogTitle:Ljava/lang/CharSequence;

    #@2
    .line 100
    return-void
.end method

.method public setNegativeButtonText(I)V
    .registers 3
    .parameter "negativeButtonTextResId"

    #@0
    .prologue
    .line 218
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    #@b
    .line 219
    return-void
.end method

.method public setNegativeButtonText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "negativeButtonText"

    #@0
    .prologue
    .line 210
    iput-object p1, p0, Landroid/preference/DialogPreference;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@2
    .line 211
    return-void
.end method

.method public setPositiveButtonText(I)V
    .registers 3
    .parameter "positiveButtonTextResId"

    #@0
    .prologue
    .line 190
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->setPositiveButtonText(Ljava/lang/CharSequence;)V

    #@b
    .line 191
    return-void
.end method

.method public setPositiveButtonText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "positiveButtonText"

    #@0
    .prologue
    .line 182
    iput-object p1, p0, Landroid/preference/DialogPreference;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@2
    .line 183
    return-void
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 277
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    .line 279
    .local v1, context:Landroid/content/Context;
    const/4 v3, -0x2

    #@5
    iput v3, p0, Landroid/preference/DialogPreference;->mWhichButtonClicked:I

    #@7
    .line 281
    new-instance v3, Landroid/app/AlertDialog$Builder;

    #@9
    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@c
    iget-object v4, p0, Landroid/preference/DialogPreference;->mDialogTitle:Ljava/lang/CharSequence;

    #@e
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@11
    move-result-object v3

    #@12
    iget-object v4, p0, Landroid/preference/DialogPreference;->mDialogIcon:Landroid/graphics/drawable/Drawable;

    #@14
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    #@17
    move-result-object v3

    #@18
    iget-object v4, p0, Landroid/preference/DialogPreference;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@1a
    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1d
    move-result-object v3

    #@1e
    iget-object v4, p0, Landroid/preference/DialogPreference;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@20
    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@23
    move-result-object v3

    #@24
    iput-object v3, p0, Landroid/preference/DialogPreference;->mBuilder:Landroid/app/AlertDialog$Builder;

    #@26
    .line 287
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->onCreateDialogView()Landroid/view/View;

    #@29
    move-result-object v0

    #@2a
    .line 288
    .local v0, contentView:Landroid/view/View;
    if-eqz v0, :cond_5d

    #@2c
    .line 289
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    #@2f
    .line 290
    iget-object v3, p0, Landroid/preference/DialogPreference;->mBuilder:Landroid/app/AlertDialog$Builder;

    #@31
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    #@34
    .line 295
    :goto_34
    iget-object v3, p0, Landroid/preference/DialogPreference;->mBuilder:Landroid/app/AlertDialog$Builder;

    #@36
    invoke-virtual {p0, v3}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    #@39
    .line 297
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3, p0}, Landroid/preference/PreferenceManager;->registerOnActivityDestroyListener(Landroid/preference/PreferenceManager$OnActivityDestroyListener;)V

    #@40
    .line 300
    iget-object v3, p0, Landroid/preference/DialogPreference;->mBuilder:Landroid/app/AlertDialog$Builder;

    #@42
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@45
    move-result-object v2

    #@46
    iput-object v2, p0, Landroid/preference/DialogPreference;->mDialog:Landroid/app/Dialog;

    #@48
    .line 301
    .local v2, dialog:Landroid/app/Dialog;
    if-eqz p1, :cond_4d

    #@4a
    .line 302
    invoke-virtual {v2, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@4d
    .line 304
    :cond_4d
    invoke-virtual {p0}, Landroid/preference/DialogPreference;->needInputMethod()Z

    #@50
    move-result v3

    #@51
    if-eqz v3, :cond_56

    #@53
    .line 305
    invoke-direct {p0, v2}, Landroid/preference/DialogPreference;->requestInputMethod(Landroid/app/Dialog;)V

    #@56
    .line 307
    :cond_56
    invoke-virtual {v2, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@59
    .line 308
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    #@5c
    .line 309
    return-void

    #@5d
    .line 292
    .end local v2           #dialog:Landroid/app/Dialog;
    :cond_5d
    iget-object v3, p0, Landroid/preference/DialogPreference;->mBuilder:Landroid/app/AlertDialog$Builder;

    #@5f
    iget-object v4, p0, Landroid/preference/DialogPreference;->mDialogMessage:Ljava/lang/CharSequence;

    #@61
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@64
    goto :goto_34
.end method
