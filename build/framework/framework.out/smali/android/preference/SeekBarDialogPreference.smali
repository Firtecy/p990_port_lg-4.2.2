.class public Landroid/preference/SeekBarDialogPreference;
.super Landroid/preference/DialogPreference;
.source "SeekBarDialogPreference.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SeekBarDialogPreference"


# instance fields
.field private mMyIcon:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 38
    const v0, 0x10900c5

    #@6
    invoke-virtual {p0, v0}, Landroid/preference/SeekBarDialogPreference;->setDialogLayoutResource(I)V

    #@9
    .line 39
    invoke-virtual {p0}, Landroid/preference/SeekBarDialogPreference;->createActionButtons()V

    #@c
    .line 42
    invoke-virtual {p0}, Landroid/preference/SeekBarDialogPreference;->getDialogIcon()Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Landroid/preference/SeekBarDialogPreference;->mMyIcon:Landroid/graphics/drawable/Drawable;

    #@12
    .line 43
    const/4 v0, 0x0

    #@13
    invoke-virtual {p0, v0}, Landroid/preference/SeekBarDialogPreference;->setDialogIcon(Landroid/graphics/drawable/Drawable;)V

    #@16
    .line 44
    return-void
.end method

.method protected static getSeekBar(Landroid/view/View;)Landroid/widget/SeekBar;
    .registers 2
    .parameter "dialogView"

    #@0
    .prologue
    .line 65
    const v0, 0x102035a

    #@3
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/SeekBar;

    #@9
    return-object v0
.end method


# virtual methods
.method public createActionButtons()V
    .registers 2

    #@0
    .prologue
    .line 48
    const v0, 0x104000a

    #@3
    invoke-virtual {p0, v0}, Landroid/preference/SeekBarDialogPreference;->setPositiveButtonText(I)V

    #@6
    .line 49
    const/high16 v0, 0x104

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/SeekBarDialogPreference;->setNegativeButtonText(I)V

    #@b
    .line 50
    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    #@3
    .line 56
    const v1, 0x1020006

    #@6
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/widget/ImageView;

    #@c
    .line 57
    .local v0, iconView:Landroid/widget/ImageView;
    iget-object v1, p0, Landroid/preference/SeekBarDialogPreference;->mMyIcon:Landroid/graphics/drawable/Drawable;

    #@e
    if-eqz v1, :cond_16

    #@10
    .line 58
    iget-object v1, p0, Landroid/preference/SeekBarDialogPreference;->mMyIcon:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@15
    .line 62
    :goto_15
    return-void

    #@16
    .line 60
    :cond_16
    const/16 v1, 0x8

    #@18
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@1b
    goto :goto_15
.end method
