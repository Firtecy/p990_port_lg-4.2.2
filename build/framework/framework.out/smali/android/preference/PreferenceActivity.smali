.class public abstract Landroid/preference/PreferenceActivity;
.super Landroid/app/ListActivity;
.source "PreferenceActivity.java"

# interfaces
.implements Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;
.implements Landroid/preference/PreferenceFragment$OnPreferenceStartFragmentCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/PreferenceActivity$Header;,
        Landroid/preference/PreferenceActivity$HeaderAdapter;
    }
.end annotation


# static fields
.field private static final BACK_STACK_PREFS:Ljava/lang/String; = ":android:prefs"

.field private static final CUR_HEADER_TAG:Ljava/lang/String; = ":android:cur_header"

.field public static final EXTRA_NO_HEADERS:Ljava/lang/String; = ":android:no_headers"

.field private static final EXTRA_PREFS_SET_BACK_TEXT:Ljava/lang/String; = "extra_prefs_set_back_text"

.field private static final EXTRA_PREFS_SET_NEXT_TEXT:Ljava/lang/String; = "extra_prefs_set_next_text"

.field private static final EXTRA_PREFS_SHOW_BUTTON_BAR:Ljava/lang/String; = "extra_prefs_show_button_bar"

.field private static final EXTRA_PREFS_SHOW_SKIP:Ljava/lang/String; = "extra_prefs_show_skip"

.field public static final EXTRA_SHOW_FRAGMENT:Ljava/lang/String; = ":android:show_fragment"

.field public static final EXTRA_SHOW_FRAGMENT_ARGUMENTS:Ljava/lang/String; = ":android:show_fragment_args"

.field public static final EXTRA_SHOW_FRAGMENT_SHORT_TITLE:Ljava/lang/String; = ":android:show_fragment_short_title"

.field public static final EXTRA_SHOW_FRAGMENT_TITLE:Ljava/lang/String; = ":android:show_fragment_title"

.field private static final FIRST_REQUEST_CODE:I = 0x64

.field private static final HEADERS_TAG:Ljava/lang/String; = ":android:headers"

.field public static final HEADER_ID_UNDEFINED:J = -0x1L

.field private static final MSG_BIND_PREFERENCES:I = 0x1

.field private static final MSG_BUILD_HEADERS:I = 0x2

.field private static final PREFERENCES_TAG:Ljava/lang/String; = ":android:preferences"


# instance fields
.field private mCurHeader:Landroid/preference/PreferenceActivity$Header;

.field private mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

.field private mHandler:Landroid/os/Handler;

.field private final mHeaders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;"
        }
    .end annotation
.end field

.field private mListFooter:Landroid/widget/FrameLayout;

.field private mNextButton:Landroid/widget/Button;

.field private mPreferenceManager:Landroid/preference/PreferenceManager;

.field private mPrefsContainer:Landroid/view/ViewGroup;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSinglePane:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 123
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    #@3
    .line 188
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@a
    .line 217
    new-instance v0, Landroid/preference/PreferenceActivity$1;

    #@c
    invoke-direct {v0, p0}, Landroid/preference/PreferenceActivity$1;-><init>(Landroid/preference/PreferenceActivity;)V

    #@f
    iput-object v0, p0, Landroid/preference/PreferenceActivity;->mHandler:Landroid/os/Handler;

    #@11
    .line 306
    return-void
.end method

.method static synthetic access$000(Landroid/preference/PreferenceActivity;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 123
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;->bindPreferences()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/preference/PreferenceActivity;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/preference/PreferenceActivity;)Landroid/widget/ListAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/preference/PreferenceActivity;)Landroid/widget/ListAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/preference/PreferenceActivity;)Landroid/preference/PreferenceActivity$Header;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@2
    return-object v0
.end method

.method private bindPreferences()V
    .registers 3

    #@0
    .prologue
    .line 1325
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@3
    move-result-object v0

    #@4
    .line 1326
    .local v0, preferenceScreen:Landroid/preference/PreferenceScreen;
    if-eqz v0, :cond_19

    #@6
    .line 1327
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getListView()Landroid/widget/ListView;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->bind(Landroid/widget/ListView;)V

    #@d
    .line 1328
    iget-object v1, p0, Landroid/preference/PreferenceActivity;->mSavedInstanceState:Landroid/os/Bundle;

    #@f
    if-eqz v1, :cond_19

    #@11
    .line 1329
    iget-object v1, p0, Landroid/preference/PreferenceActivity;->mSavedInstanceState:Landroid/os/Bundle;

    #@13
    invoke-super {p0, v1}, Landroid/app/ListActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@16
    .line 1330
    const/4 v1, 0x0

    #@17
    iput-object v1, p0, Landroid/preference/PreferenceActivity;->mSavedInstanceState:Landroid/os/Bundle;

    #@19
    .line 1333
    :cond_19
    return-void
.end method

.method private postBindPreferences()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1320
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 1322
    :goto_9
    return-void

    #@a
    .line 1321
    :cond_a
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@13
    goto :goto_9
.end method

.method private requirePreferenceManager()V
    .registers 3

    #@0
    .prologue
    .line 1348
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_18

    #@4
    .line 1349
    iget-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@6
    if-nez v0, :cond_10

    #@8
    .line 1350
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string v1, "This should be called after super.onCreate."

    #@c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 1352
    :cond_10
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "Modern two-pane PreferenceActivity requires use of a PreferenceFragment"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 1355
    :cond_18
    return-void
.end method

.method private switchToHeaderInner(Ljava/lang/String;Landroid/os/Bundle;I)V
    .registers 9
    .parameter "fragmentName"
    .parameter "args"
    .parameter "direction"

    #@0
    .prologue
    .line 1136
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    #@3
    move-result-object v2

    #@4
    const-string v3, ":android:prefs"

    #@6
    const/4 v4, 0x1

    #@7
    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    #@a
    .line 1138
    invoke-static {p0, p1, p2}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    #@d
    move-result-object v0

    #@e
    .line 1139
    .local v0, f:Landroid/app/Fragment;
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    #@15
    move-result-object v1

    #@16
    .line 1140
    .local v1, transaction:Landroid/app/FragmentTransaction;
    const/16 v2, 0x1003

    #@18
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    #@1b
    .line 1141
    const v2, 0x1020356

    #@1e
    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    #@21
    .line 1142
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    #@24
    .line 1143
    return-void
.end method


# virtual methods
.method public addPreferencesFromIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1406
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;->requirePreferenceManager()V

    #@3
    .line 1408
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, p1, v1}, Landroid/preference/PreferenceManager;->inflateFromIntent(Landroid/content/Intent;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    #@10
    .line 1409
    return-void
.end method

.method public addPreferencesFromResource(I)V
    .registers 4
    .parameter "preferencesResId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1422
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;->requirePreferenceManager()V

    #@3
    .line 1424
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, p0, p1, v1}, Landroid/preference/PreferenceManager;->inflateFromResource(Landroid/content/Context;ILandroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    #@10
    .line 1426
    return-void
.end method

.method findBestMatchingHeader(Landroid/preference/PreferenceActivity$Header;Ljava/util/ArrayList;)Landroid/preference/PreferenceActivity$Header;
    .registers 12
    .parameter "cur"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/PreferenceActivity$Header;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)",
            "Landroid/preference/PreferenceActivity$Header;"
        }
    .end annotation

    #@0
    .prologue
    .local p2, from:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/preference/PreferenceActivity$Header;>;"
    const/4 v8, 0x1

    #@1
    .line 1177
    new-instance v2, Ljava/util/ArrayList;

    #@3
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 1178
    .local v2, matches:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/preference/PreferenceActivity$Header;>;"
    const/4 v1, 0x0

    #@7
    .local v1, j:I
    :goto_7
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v4

    #@b
    if-ge v1, v4, :cond_2b

    #@d
    .line 1179
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    check-cast v3, Landroid/preference/PreferenceActivity$Header;

    #@13
    .line 1180
    .local v3, oh:Landroid/preference/PreferenceActivity$Header;
    if-eq p1, v3, :cond_25

    #@15
    iget-wide v4, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    #@17
    const-wide/16 v6, -0x1

    #@19
    cmp-long v4, v4, v6

    #@1b
    if-eqz v4, :cond_3a

    #@1d
    iget-wide v4, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    #@1f
    iget-wide v6, v3, Landroid/preference/PreferenceActivity$Header;->id:J

    #@21
    cmp-long v4, v4, v6

    #@23
    if-nez v4, :cond_3a

    #@25
    .line 1182
    :cond_25
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@28
    .line 1183
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b
    .line 1200
    .end local v3           #oh:Landroid/preference/PreferenceActivity$Header;
    :cond_2b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v0

    #@2f
    .line 1201
    .local v0, NM:I
    if-ne v0, v8, :cond_72

    #@31
    .line 1202
    const/4 v4, 0x0

    #@32
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@35
    move-result-object v4

    #@36
    check-cast v4, Landroid/preference/PreferenceActivity$Header;

    #@38
    move-object v3, v4

    #@39
    .line 1218
    :cond_39
    :goto_39
    return-object v3

    #@3a
    .line 1186
    .end local v0           #NM:I
    .restart local v3       #oh:Landroid/preference/PreferenceActivity$Header;
    :cond_3a
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@3c
    if-eqz v4, :cond_4e

    #@3e
    .line 1187
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@40
    iget-object v5, v3, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v4

    #@46
    if-eqz v4, :cond_4b

    #@48
    .line 1188
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4b
    .line 1178
    :cond_4b
    :goto_4b
    add-int/lit8 v1, v1, 0x1

    #@4d
    goto :goto_7

    #@4e
    .line 1190
    :cond_4e
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    #@50
    if-eqz v4, :cond_60

    #@52
    .line 1191
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    #@54
    iget-object v5, v3, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_4b

    #@5c
    .line 1192
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5f
    goto :goto_4b

    #@60
    .line 1194
    :cond_60
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@62
    if-eqz v4, :cond_4b

    #@64
    .line 1195
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@66
    iget-object v5, v3, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v4

    #@6c
    if-eqz v4, :cond_4b

    #@6e
    .line 1196
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@71
    goto :goto_4b

    #@72
    .line 1203
    .end local v3           #oh:Landroid/preference/PreferenceActivity$Header;
    .restart local v0       #NM:I
    :cond_72
    if-le v0, v8, :cond_aa

    #@74
    .line 1204
    const/4 v1, 0x0

    #@75
    :goto_75
    if-ge v1, v0, :cond_aa

    #@77
    .line 1205
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7a
    move-result-object v3

    #@7b
    check-cast v3, Landroid/preference/PreferenceActivity$Header;

    #@7d
    .line 1206
    .restart local v3       #oh:Landroid/preference/PreferenceActivity$Header;
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    #@7f
    if-eqz v4, :cond_8b

    #@81
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    #@83
    iget-object v5, v3, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v4

    #@89
    if-nez v4, :cond_39

    #@8b
    .line 1210
    :cond_8b
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    #@8d
    if-eqz v4, :cond_99

    #@8f
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    #@91
    iget-object v5, v3, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v4

    #@97
    if-nez v4, :cond_39

    #@99
    .line 1213
    :cond_99
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@9b
    if-eqz v4, :cond_a7

    #@9d
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@9f
    iget-object v5, v3, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@a1
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v4

    #@a5
    if-nez v4, :cond_39

    #@a7
    .line 1204
    :cond_a7
    add-int/lit8 v1, v1, 0x1

    #@a9
    goto :goto_75

    #@aa
    .line 1218
    .end local v3           #oh:Landroid/preference/PreferenceActivity$Header;
    :cond_aa
    const/4 v3, 0x0

    #@ab
    goto :goto_39
.end method

.method public findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .registers 3
    .parameter "key"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1452
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 1453
    const/4 v0, 0x0

    #@5
    .line 1456
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@8
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public finishPreferencePanel(Landroid/app/Fragment;ILandroid/content/Intent;)V
    .registers 6
    .parameter "caller"
    .parameter "resultCode"
    .parameter "resultData"

    #@0
    .prologue
    .line 1291
    iget-boolean v0, p0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1292
    invoke-virtual {p0, p2, p3}, Landroid/preference/PreferenceActivity;->setResult(ILandroid/content/Intent;)V

    #@7
    .line 1293
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->finish()V

    #@a
    .line 1304
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1296
    :cond_b
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    #@e
    .line 1297
    if-eqz p1, :cond_a

    #@10
    .line 1298
    invoke-virtual {p1}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    #@13
    move-result-object v0

    #@14
    if-eqz v0, :cond_a

    #@16
    .line 1299
    invoke-virtual {p1}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p1}, Landroid/app/Fragment;->getTargetRequestCode()I

    #@1d
    move-result v1

    #@1e
    invoke-virtual {v0, v1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    #@21
    goto :goto_a
.end method

.method public getHeaders()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 665
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method protected getNextButton()Landroid/widget/Button;
    .registers 2

    #@0
    .prologue
    .line 1473
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mNextButton:Landroid/widget/Button;

    #@2
    return-object v0
.end method

.method public getPreferenceManager()Landroid/preference/PreferenceManager;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1344
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    return-object v0
.end method

.method public getPreferenceScreen()Landroid/preference/PreferenceScreen;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1390
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1391
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@6
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@9
    move-result-object v0

    #@a
    .line 1393
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public hasHeaders()Z
    .registers 2

    #@0
    .prologue
    .line 656
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getListView()Landroid/widget/ListView;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_10

    #@a
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@c
    if-nez v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method protected hasNextButton()Z
    .registers 2

    #@0
    .prologue
    .line 1469
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mNextButton:Landroid/widget/Button;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public invalidateHeaders()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 741
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_e

    #@9
    .line 742
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@e
    .line 744
    :cond_e
    return-void
.end method

.method public isMultiPane()Z
    .registers 2

    #@0
    .prologue
    .line 673
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->hasHeaders()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_10

    #@6
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPrefsContainer:Landroid/view/ViewGroup;

    #@8
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public loadHeadersFromResource(ILjava/util/List;)V
    .registers 19
    .parameter "resid"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 754
    .local p2, target:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceActivity$Header;>;"
    const/4 v9, 0x0

    #@1
    .line 756
    .local v9, parser:Landroid/content/res/XmlResourceParser;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v13

    #@5
    move/from16 v0, p1

    #@7
    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@a
    move-result-object v9

    #@b
    .line 757
    invoke-static {v9}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@e
    move-result-object v1

    #@f
    .line 761
    .local v1, attrs:Landroid/util/AttributeSet;
    :cond_f
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@12
    move-result v12

    #@13
    .local v12, type:I
    const/4 v13, 0x1

    #@14
    if-eq v12, v13, :cond_19

    #@16
    const/4 v13, 0x2

    #@17
    if-ne v12, v13, :cond_f

    #@19
    .line 765
    :cond_19
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@1c
    move-result-object v7

    #@1d
    .line 766
    .local v7, nodeName:Ljava/lang/String;
    const-string/jumbo v13, "preference-headers"

    #@20
    invoke-virtual {v13, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v13

    #@24
    if-nez v13, :cond_5d

    #@26
    .line 767
    new-instance v13, Ljava/lang/RuntimeException;

    #@28
    new-instance v14, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v15, "XML document must start with <preference-headers> tag; found"

    #@2f
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v14

    #@33
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v14

    #@37
    const-string v15, " at "

    #@39
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v14

    #@3d
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    #@40
    move-result-object v15

    #@41
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v14

    #@45
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v14

    #@49
    invoke-direct {v13, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v13
    :try_end_4d
    .catchall {:try_start_1 .. :try_end_4d} :catchall_56
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_4d} :catch_4d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4d} :catch_138

    #@4d
    .line 867
    .end local v1           #attrs:Landroid/util/AttributeSet;
    .end local v7           #nodeName:Ljava/lang/String;
    .end local v12           #type:I
    :catch_4d
    move-exception v3

    #@4e
    .line 868
    .local v3, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_4e
    new-instance v13, Ljava/lang/RuntimeException;

    #@50
    const-string v14, "Error parsing headers"

    #@52
    invoke-direct {v13, v14, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@55
    throw v13
    :try_end_56
    .catchall {:try_start_4e .. :try_end_56} :catchall_56

    #@56
    .line 872
    .end local v3           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_56
    move-exception v13

    #@57
    if-eqz v9, :cond_5c

    #@59
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@5c
    :cond_5c
    throw v13

    #@5d
    .line 772
    .restart local v1       #attrs:Landroid/util/AttributeSet;
    .restart local v7       #nodeName:Ljava/lang/String;
    .restart local v12       #type:I
    :cond_5d
    const/4 v2, 0x0

    #@5e
    .line 774
    .local v2, curBundle:Landroid/os/Bundle;
    :try_start_5e
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@61
    move-result v8

    #@62
    .line 776
    .local v8, outerDepth:I
    :cond_62
    :goto_62
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@65
    move-result v12

    #@66
    const/4 v13, 0x1

    #@67
    if-eq v12, v13, :cond_183

    #@69
    const/4 v13, 0x3

    #@6a
    if-ne v12, v13, :cond_72

    #@6c
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@6f
    move-result v13

    #@70
    if-le v13, v8, :cond_183

    #@72
    .line 777
    :cond_72
    const/4 v13, 0x3

    #@73
    if-eq v12, v13, :cond_62

    #@75
    const/4 v13, 0x4

    #@76
    if-eq v12, v13, :cond_62

    #@78
    .line 781
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@7b
    move-result-object v7

    #@7c
    .line 782
    const-string v13, "header"

    #@7e
    invoke-virtual {v13, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v13

    #@82
    if-eqz v13, :cond_17e

    #@84
    .line 783
    new-instance v4, Landroid/preference/PreferenceActivity$Header;

    #@86
    invoke-direct {v4}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    #@89
    .line 785
    .local v4, header:Landroid/preference/PreferenceActivity$Header;
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@8c
    move-result-object v13

    #@8d
    sget-object v14, Lcom/android/internal/R$styleable;->PreferenceHeader:[I

    #@8f
    invoke-virtual {v13, v1, v14}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@92
    move-result-object v10

    #@93
    .line 787
    .local v10, sa:Landroid/content/res/TypedArray;
    const/4 v13, 0x1

    #@94
    const/4 v14, -0x1

    #@95
    invoke-virtual {v10, v13, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@98
    move-result v13

    #@99
    int-to-long v13, v13

    #@9a
    iput-wide v13, v4, Landroid/preference/PreferenceActivity$Header;->id:J

    #@9c
    .line 790
    const/4 v13, 0x2

    #@9d
    invoke-virtual {v10, v13}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@a0
    move-result-object v11

    #@a1
    .line 792
    .local v11, tv:Landroid/util/TypedValue;
    if-eqz v11, :cond_b0

    #@a3
    iget v13, v11, Landroid/util/TypedValue;->type:I

    #@a5
    const/4 v14, 0x3

    #@a6
    if-ne v13, v14, :cond_b0

    #@a8
    .line 793
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@aa
    if-eqz v13, :cond_141

    #@ac
    .line 794
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@ae
    iput v13, v4, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    #@b0
    .line 799
    :cond_b0
    :goto_b0
    const/4 v13, 0x3

    #@b1
    invoke-virtual {v10, v13}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@b4
    move-result-object v11

    #@b5
    .line 801
    if-eqz v11, :cond_c4

    #@b7
    iget v13, v11, Landroid/util/TypedValue;->type:I

    #@b9
    const/4 v14, 0x3

    #@ba
    if-ne v13, v14, :cond_c4

    #@bc
    .line 802
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@be
    if-eqz v13, :cond_147

    #@c0
    .line 803
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@c2
    iput v13, v4, Landroid/preference/PreferenceActivity$Header;->summaryRes:I

    #@c4
    .line 808
    :cond_c4
    :goto_c4
    const/4 v13, 0x5

    #@c5
    invoke-virtual {v10, v13}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@c8
    move-result-object v11

    #@c9
    .line 810
    if-eqz v11, :cond_d8

    #@cb
    iget v13, v11, Landroid/util/TypedValue;->type:I

    #@cd
    const/4 v14, 0x3

    #@ce
    if-ne v13, v14, :cond_d8

    #@d0
    .line 811
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@d2
    if-eqz v13, :cond_14d

    #@d4
    .line 812
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@d6
    iput v13, v4, Landroid/preference/PreferenceActivity$Header;->breadCrumbTitleRes:I

    #@d8
    .line 817
    :cond_d8
    :goto_d8
    const/4 v13, 0x6

    #@d9
    invoke-virtual {v10, v13}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@dc
    move-result-object v11

    #@dd
    .line 819
    if-eqz v11, :cond_ec

    #@df
    iget v13, v11, Landroid/util/TypedValue;->type:I

    #@e1
    const/4 v14, 0x3

    #@e2
    if-ne v13, v14, :cond_ec

    #@e4
    .line 820
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@e6
    if-eqz v13, :cond_152

    #@e8
    .line 821
    iget v13, v11, Landroid/util/TypedValue;->resourceId:I

    #@ea
    iput v13, v4, Landroid/preference/PreferenceActivity$Header;->breadCrumbShortTitleRes:I

    #@ec
    .line 826
    :cond_ec
    :goto_ec
    const/4 v13, 0x0

    #@ed
    const/4 v14, 0x0

    #@ee
    invoke-virtual {v10, v13, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@f1
    move-result v13

    #@f2
    iput v13, v4, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    #@f4
    .line 828
    const/4 v13, 0x4

    #@f5
    invoke-virtual {v10, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v13

    #@f9
    iput-object v13, v4, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@fb
    .line 830
    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V

    #@fe
    .line 832
    if-nez v2, :cond_105

    #@100
    .line 833
    new-instance v2, Landroid/os/Bundle;

    #@102
    .end local v2           #curBundle:Landroid/os/Bundle;
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@105
    .line 836
    .restart local v2       #curBundle:Landroid/os/Bundle;
    :cond_105
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@108
    move-result v5

    #@109
    .line 838
    .local v5, innerDepth:I
    :cond_109
    :goto_109
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@10c
    move-result v12

    #@10d
    const/4 v13, 0x1

    #@10e
    if-eq v12, v13, :cond_16e

    #@110
    const/4 v13, 0x3

    #@111
    if-ne v12, v13, :cond_119

    #@113
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@116
    move-result v13

    #@117
    if-le v13, v5, :cond_16e

    #@119
    .line 839
    :cond_119
    const/4 v13, 0x3

    #@11a
    if-eq v12, v13, :cond_109

    #@11c
    const/4 v13, 0x4

    #@11d
    if-eq v12, v13, :cond_109

    #@11f
    .line 843
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@122
    move-result-object v6

    #@123
    .line 844
    .local v6, innerNodeName:Ljava/lang/String;
    const-string v13, "extra"

    #@125
    invoke-virtual {v6, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@128
    move-result v13

    #@129
    if-eqz v13, :cond_157

    #@12b
    .line 845
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@12e
    move-result-object v13

    #@12f
    const-string v14, "extra"

    #@131
    invoke-virtual {v13, v14, v1, v2}, Landroid/content/res/Resources;->parseBundleExtra(Ljava/lang/String;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    #@134
    .line 846
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_137
    .catchall {:try_start_5e .. :try_end_137} :catchall_56
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5e .. :try_end_137} :catch_4d
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_137} :catch_138

    #@137
    goto :goto_109

    #@138
    .line 869
    .end local v1           #attrs:Landroid/util/AttributeSet;
    .end local v2           #curBundle:Landroid/os/Bundle;
    .end local v4           #header:Landroid/preference/PreferenceActivity$Header;
    .end local v5           #innerDepth:I
    .end local v6           #innerNodeName:Ljava/lang/String;
    .end local v7           #nodeName:Ljava/lang/String;
    .end local v8           #outerDepth:I
    .end local v10           #sa:Landroid/content/res/TypedArray;
    .end local v11           #tv:Landroid/util/TypedValue;
    .end local v12           #type:I
    :catch_138
    move-exception v3

    #@139
    .line 870
    .local v3, e:Ljava/io/IOException;
    :try_start_139
    new-instance v13, Ljava/lang/RuntimeException;

    #@13b
    const-string v14, "Error parsing headers"

    #@13d
    invoke-direct {v13, v14, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@140
    throw v13
    :try_end_141
    .catchall {:try_start_139 .. :try_end_141} :catchall_56

    #@141
    .line 796
    .end local v3           #e:Ljava/io/IOException;
    .restart local v1       #attrs:Landroid/util/AttributeSet;
    .restart local v2       #curBundle:Landroid/os/Bundle;
    .restart local v4       #header:Landroid/preference/PreferenceActivity$Header;
    .restart local v7       #nodeName:Ljava/lang/String;
    .restart local v8       #outerDepth:I
    .restart local v10       #sa:Landroid/content/res/TypedArray;
    .restart local v11       #tv:Landroid/util/TypedValue;
    .restart local v12       #type:I
    :cond_141
    :try_start_141
    iget-object v13, v11, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@143
    iput-object v13, v4, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    #@145
    goto/16 :goto_b0

    #@147
    .line 805
    :cond_147
    iget-object v13, v11, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@149
    iput-object v13, v4, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    #@14b
    goto/16 :goto_c4

    #@14d
    .line 814
    :cond_14d
    iget-object v13, v11, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@14f
    iput-object v13, v4, Landroid/preference/PreferenceActivity$Header;->breadCrumbTitle:Ljava/lang/CharSequence;

    #@151
    goto :goto_d8

    #@152
    .line 823
    :cond_152
    iget-object v13, v11, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    #@154
    iput-object v13, v4, Landroid/preference/PreferenceActivity$Header;->breadCrumbShortTitle:Ljava/lang/CharSequence;

    #@156
    goto :goto_ec

    #@157
    .line 848
    .restart local v5       #innerDepth:I
    .restart local v6       #innerNodeName:Ljava/lang/String;
    :cond_157
    const-string v13, "intent"

    #@159
    invoke-virtual {v6, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15c
    move-result v13

    #@15d
    if-eqz v13, :cond_16a

    #@15f
    .line 849
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@162
    move-result-object v13

    #@163
    invoke-static {v13, v9, v1}, Landroid/content/Intent;->parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/Intent;

    #@166
    move-result-object v13

    #@167
    iput-object v13, v4, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    #@169
    goto :goto_109

    #@16a
    .line 852
    :cond_16a
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@16d
    goto :goto_109

    #@16e
    .line 856
    .end local v6           #innerNodeName:Ljava/lang/String;
    :cond_16e
    invoke-virtual {v2}, Landroid/os/Bundle;->size()I

    #@171
    move-result v13

    #@172
    if-lez v13, :cond_177

    #@174
    .line 857
    iput-object v2, v4, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    #@176
    .line 858
    const/4 v2, 0x0

    #@177
    .line 861
    :cond_177
    move-object/from16 v0, p2

    #@179
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@17c
    goto/16 :goto_62

    #@17e
    .line 863
    .end local v4           #header:Landroid/preference/PreferenceActivity$Header;
    .end local v5           #innerDepth:I
    .end local v10           #sa:Landroid/content/res/TypedArray;
    .end local v11           #tv:Landroid/util/TypedValue;
    :cond_17e
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_181
    .catchall {:try_start_141 .. :try_end_181} :catchall_56
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_141 .. :try_end_181} :catch_4d
    .catch Ljava/io/IOException; {:try_start_141 .. :try_end_181} :catch_138

    #@181
    goto/16 :goto_62

    #@183
    .line 872
    :cond_183
    if-eqz v9, :cond_188

    #@185
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@188
    .line 875
    :cond_188
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 950
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onActivityResult(IILandroid/content/Intent;)V

    #@3
    .line 952
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 953
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0, p1, p2, p3}, Landroid/preference/PreferenceManager;->dispatchActivityResult(IILandroid/content/Intent;)V

    #@c
    .line 955
    :cond_c
    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 734
    .local p1, target:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceActivity$Header;>;"
    return-void
.end method

.method public onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;
    .registers 8
    .parameter "fragmentName"
    .parameter "args"
    .parameter "titleRes"
    .parameter "shortTitleRes"

    #@0
    .prologue
    .line 1020
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.MAIN"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1021
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    #@e
    .line 1022
    const-string v1, ":android:show_fragment"

    #@10
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13
    .line 1023
    const-string v1, ":android:show_fragment_args"

    #@15
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@18
    .line 1024
    const-string v1, ":android:show_fragment_title"

    #@1a
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1d
    .line 1025
    const-string v1, ":android:show_fragment_short_title"

    #@1f
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@22
    .line 1026
    const-string v1, ":android:no_headers"

    #@24
    const/4 v2, 0x1

    #@25
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@28
    .line 1027
    return-object v0
.end method

.method public onContentChanged()V
    .registers 2

    #@0
    .prologue
    .line 959
    invoke-super {p0}, Landroid/app/ListActivity;->onContentChanged()V

    #@3
    .line 961
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 962
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;->postBindPreferences()V

    #@a
    .line 964
    :cond_a
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 22
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 508
    invoke-super/range {p0 .. p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 510
    const v17, 0x10900a9

    #@6
    move-object/from16 v0, p0

    #@8
    move/from16 v1, v17

    #@a
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setContentView(I)V

    #@d
    .line 512
    const v17, 0x1020354

    #@10
    move-object/from16 v0, p0

    #@12
    move/from16 v1, v17

    #@14
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@17
    move-result-object v17

    #@18
    check-cast v17, Landroid/widget/FrameLayout;

    #@1a
    move-object/from16 v0, v17

    #@1c
    move-object/from16 v1, p0

    #@1e
    iput-object v0, v1, Landroid/preference/PreferenceActivity;->mListFooter:Landroid/widget/FrameLayout;

    #@20
    .line 513
    const v17, 0x1020355

    #@23
    move-object/from16 v0, p0

    #@25
    move/from16 v1, v17

    #@27
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@2a
    move-result-object v17

    #@2b
    check-cast v17, Landroid/view/ViewGroup;

    #@2d
    move-object/from16 v0, v17

    #@2f
    move-object/from16 v1, p0

    #@31
    iput-object v0, v1, Landroid/preference/PreferenceActivity;->mPrefsContainer:Landroid/view/ViewGroup;

    #@33
    .line 514
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->onIsHidingHeaders()Z

    #@36
    move-result v8

    #@37
    .line 515
    .local v8, hidingHeaders:Z
    if-nez v8, :cond_3f

    #@39
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    #@3c
    move-result v17

    #@3d
    if-nez v17, :cond_1ca

    #@3f
    :cond_3f
    const/16 v17, 0x1

    #@41
    :goto_41
    move/from16 v0, v17

    #@43
    move-object/from16 v1, p0

    #@45
    iput-boolean v0, v1, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@47
    .line 516
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    #@4a
    move-result-object v17

    #@4b
    const-string v18, ":android:show_fragment"

    #@4d
    invoke-virtual/range {v17 .. v18}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@50
    move-result-object v10

    #@51
    .line 517
    .local v10, initialFragment:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    #@54
    move-result-object v17

    #@55
    const-string v18, ":android:show_fragment_args"

    #@57
    invoke-virtual/range {v17 .. v18}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    #@5a
    move-result-object v9

    #@5b
    .line 518
    .local v9, initialArguments:Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    #@5e
    move-result-object v17

    #@5f
    const-string v18, ":android:show_fragment_title"

    #@61
    const/16 v19, 0x0

    #@63
    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@66
    move-result v13

    #@67
    .line 519
    .local v13, initialTitle:I
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    #@6a
    move-result-object v17

    #@6b
    const-string v18, ":android:show_fragment_short_title"

    #@6d
    const/16 v19, 0x0

    #@6f
    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@72
    move-result v11

    #@73
    .line 521
    .local v11, initialShortTitle:I
    if-eqz p1, :cond_1ce

    #@75
    .line 524
    const-string v17, ":android:headers"

    #@77
    move-object/from16 v0, p1

    #@79
    move-object/from16 v1, v17

    #@7b
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@7e
    move-result-object v7

    #@7f
    .line 525
    .local v7, headers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/preference/PreferenceActivity$Header;>;"
    if-eqz v7, :cond_bf

    #@81
    .line 526
    move-object/from16 v0, p0

    #@83
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@85
    move-object/from16 v17, v0

    #@87
    move-object/from16 v0, v17

    #@89
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@8c
    .line 527
    const-string v17, ":android:cur_header"

    #@8e
    const/16 v18, -0x1

    #@90
    move-object/from16 v0, p1

    #@92
    move-object/from16 v1, v17

    #@94
    move/from16 v2, v18

    #@96
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@99
    move-result v5

    #@9a
    .line 529
    .local v5, curHeader:I
    if-ltz v5, :cond_bf

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@a0
    move-object/from16 v17, v0

    #@a2
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@a5
    move-result v17

    #@a6
    move/from16 v0, v17

    #@a8
    if-ge v5, v0, :cond_bf

    #@aa
    .line 530
    move-object/from16 v0, p0

    #@ac
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@ae
    move-object/from16 v17, v0

    #@b0
    move-object/from16 v0, v17

    #@b2
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b5
    move-result-object v17

    #@b6
    check-cast v17, Landroid/preference/PreferenceActivity$Header;

    #@b8
    move-object/from16 v0, p0

    #@ba
    move-object/from16 v1, v17

    #@bc
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setSelectedHeader(Landroid/preference/PreferenceActivity$Header;)V

    #@bf
    .line 569
    .end local v5           #curHeader:I
    .end local v7           #headers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/preference/PreferenceActivity$Header;>;"
    :cond_bf
    :goto_bf
    if-eqz v10, :cond_22e

    #@c1
    move-object/from16 v0, p0

    #@c3
    iget-boolean v0, v0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@c5
    move/from16 v17, v0

    #@c7
    if-eqz v17, :cond_22e

    #@c9
    .line 571
    const v17, 0x1020353

    #@cc
    move-object/from16 v0, p0

    #@ce
    move/from16 v1, v17

    #@d0
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@d3
    move-result-object v17

    #@d4
    const/16 v18, 0x8

    #@d6
    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    #@d9
    .line 572
    move-object/from16 v0, p0

    #@db
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mPrefsContainer:Landroid/view/ViewGroup;

    #@dd
    move-object/from16 v17, v0

    #@df
    const/16 v18, 0x0

    #@e1
    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@e4
    .line 573
    if-eqz v13, :cond_f9

    #@e6
    .line 574
    move-object/from16 v0, p0

    #@e8
    invoke-virtual {v0, v13}, Landroid/preference/PreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    #@eb
    move-result-object v14

    #@ec
    .line 575
    .local v14, initialTitleStr:Ljava/lang/CharSequence;
    if-eqz v11, :cond_22b

    #@ee
    move-object/from16 v0, p0

    #@f0
    invoke-virtual {v0, v11}, Landroid/preference/PreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    #@f3
    move-result-object v12

    #@f4
    .line 577
    .local v12, initialShortTitleStr:Ljava/lang/CharSequence;
    :goto_f4
    move-object/from16 v0, p0

    #@f6
    invoke-virtual {v0, v14, v12}, Landroid/preference/PreferenceActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@f9
    .line 600
    .end local v12           #initialShortTitleStr:Ljava/lang/CharSequence;
    .end local v14           #initialTitleStr:Ljava/lang/CharSequence;
    :cond_f9
    :goto_f9
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    #@fc
    move-result-object v15

    #@fd
    .line 601
    .local v15, intent:Landroid/content/Intent;
    const-string v17, "extra_prefs_show_button_bar"

    #@ff
    const/16 v18, 0x0

    #@101
    move-object/from16 v0, v17

    #@103
    move/from16 v1, v18

    #@105
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@108
    move-result v17

    #@109
    if-eqz v17, :cond_1c9

    #@10b
    .line 603
    const v17, 0x1020287

    #@10e
    move-object/from16 v0, p0

    #@110
    move/from16 v1, v17

    #@112
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@115
    move-result-object v17

    #@116
    const/16 v18, 0x0

    #@118
    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    #@11b
    .line 605
    const v17, 0x1020357

    #@11e
    move-object/from16 v0, p0

    #@120
    move/from16 v1, v17

    #@122
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@125
    move-result-object v3

    #@126
    check-cast v3, Landroid/widget/Button;

    #@128
    .line 606
    .local v3, backButton:Landroid/widget/Button;
    new-instance v17, Landroid/preference/PreferenceActivity$2;

    #@12a
    move-object/from16 v0, v17

    #@12c
    move-object/from16 v1, p0

    #@12e
    invoke-direct {v0, v1}, Landroid/preference/PreferenceActivity$2;-><init>(Landroid/preference/PreferenceActivity;)V

    #@131
    move-object/from16 v0, v17

    #@133
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@136
    .line 612
    const v17, 0x1020358

    #@139
    move-object/from16 v0, p0

    #@13b
    move/from16 v1, v17

    #@13d
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@140
    move-result-object v16

    #@141
    check-cast v16, Landroid/widget/Button;

    #@143
    .line 613
    .local v16, skipButton:Landroid/widget/Button;
    new-instance v17, Landroid/preference/PreferenceActivity$3;

    #@145
    move-object/from16 v0, v17

    #@147
    move-object/from16 v1, p0

    #@149
    invoke-direct {v0, v1}, Landroid/preference/PreferenceActivity$3;-><init>(Landroid/preference/PreferenceActivity;)V

    #@14c
    invoke-virtual/range {v16 .. v17}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@14f
    .line 619
    const v17, 0x1020359

    #@152
    move-object/from16 v0, p0

    #@154
    move/from16 v1, v17

    #@156
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@159
    move-result-object v17

    #@15a
    check-cast v17, Landroid/widget/Button;

    #@15c
    move-object/from16 v0, v17

    #@15e
    move-object/from16 v1, p0

    #@160
    iput-object v0, v1, Landroid/preference/PreferenceActivity;->mNextButton:Landroid/widget/Button;

    #@162
    .line 620
    move-object/from16 v0, p0

    #@164
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mNextButton:Landroid/widget/Button;

    #@166
    move-object/from16 v17, v0

    #@168
    new-instance v18, Landroid/preference/PreferenceActivity$4;

    #@16a
    move-object/from16 v0, v18

    #@16c
    move-object/from16 v1, p0

    #@16e
    invoke-direct {v0, v1}, Landroid/preference/PreferenceActivity$4;-><init>(Landroid/preference/PreferenceActivity;)V

    #@171
    invoke-virtual/range {v17 .. v18}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@174
    .line 628
    const-string v17, "extra_prefs_set_next_text"

    #@176
    move-object/from16 v0, v17

    #@178
    invoke-virtual {v15, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@17b
    move-result v17

    #@17c
    if-eqz v17, :cond_197

    #@17e
    .line 629
    const-string v17, "extra_prefs_set_next_text"

    #@180
    move-object/from16 v0, v17

    #@182
    invoke-virtual {v15, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@185
    move-result-object v4

    #@186
    .line 630
    .local v4, buttonText:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@189
    move-result v17

    #@18a
    if-eqz v17, :cond_2d7

    #@18c
    .line 631
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mNextButton:Landroid/widget/Button;

    #@190
    move-object/from16 v17, v0

    #@192
    const/16 v18, 0x8

    #@194
    invoke-virtual/range {v17 .. v18}, Landroid/widget/Button;->setVisibility(I)V

    #@197
    .line 637
    .end local v4           #buttonText:Ljava/lang/String;
    :cond_197
    :goto_197
    const-string v17, "extra_prefs_set_back_text"

    #@199
    move-object/from16 v0, v17

    #@19b
    invoke-virtual {v15, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@19e
    move-result v17

    #@19f
    if-eqz v17, :cond_1b6

    #@1a1
    .line 638
    const-string v17, "extra_prefs_set_back_text"

    #@1a3
    move-object/from16 v0, v17

    #@1a5
    invoke-virtual {v15, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@1a8
    move-result-object v4

    #@1a9
    .line 639
    .restart local v4       #buttonText:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1ac
    move-result v17

    #@1ad
    if-eqz v17, :cond_2e4

    #@1af
    .line 640
    const/16 v17, 0x8

    #@1b1
    move/from16 v0, v17

    #@1b3
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    #@1b6
    .line 646
    .end local v4           #buttonText:Ljava/lang/String;
    :cond_1b6
    :goto_1b6
    const-string v17, "extra_prefs_show_skip"

    #@1b8
    const/16 v18, 0x0

    #@1ba
    move-object/from16 v0, v17

    #@1bc
    move/from16 v1, v18

    #@1be
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@1c1
    move-result v17

    #@1c2
    if-eqz v17, :cond_1c9

    #@1c4
    .line 647
    const/16 v17, 0x0

    #@1c6
    invoke-virtual/range {v16 .. v17}, Landroid/widget/Button;->setVisibility(I)V

    #@1c9
    .line 650
    .end local v3           #backButton:Landroid/widget/Button;
    .end local v16           #skipButton:Landroid/widget/Button;
    :cond_1c9
    return-void

    #@1ca
    .line 515
    .end local v9           #initialArguments:Landroid/os/Bundle;
    .end local v10           #initialFragment:Ljava/lang/String;
    .end local v11           #initialShortTitle:I
    .end local v13           #initialTitle:I
    .end local v15           #intent:Landroid/content/Intent;
    :cond_1ca
    const/16 v17, 0x0

    #@1cc
    goto/16 :goto_41

    #@1ce
    .line 535
    .restart local v9       #initialArguments:Landroid/os/Bundle;
    .restart local v10       #initialFragment:Ljava/lang/String;
    .restart local v11       #initialShortTitle:I
    .restart local v13       #initialTitle:I
    :cond_1ce
    if-eqz v10, :cond_1f6

    #@1d0
    move-object/from16 v0, p0

    #@1d2
    iget-boolean v0, v0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@1d4
    move/from16 v17, v0

    #@1d6
    if-eqz v17, :cond_1f6

    #@1d8
    .line 539
    move-object/from16 v0, p0

    #@1da
    invoke-virtual {v0, v10, v9}, Landroid/preference/PreferenceActivity;->switchToHeader(Ljava/lang/String;Landroid/os/Bundle;)V

    #@1dd
    .line 540
    if-eqz v13, :cond_bf

    #@1df
    .line 541
    move-object/from16 v0, p0

    #@1e1
    invoke-virtual {v0, v13}, Landroid/preference/PreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    #@1e4
    move-result-object v14

    #@1e5
    .line 542
    .restart local v14       #initialTitleStr:Ljava/lang/CharSequence;
    if-eqz v11, :cond_1f4

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    invoke-virtual {v0, v11}, Landroid/preference/PreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    #@1ec
    move-result-object v12

    #@1ed
    .line 544
    .restart local v12       #initialShortTitleStr:Ljava/lang/CharSequence;
    :goto_1ed
    move-object/from16 v0, p0

    #@1ef
    invoke-virtual {v0, v14, v12}, Landroid/preference/PreferenceActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@1f2
    goto/16 :goto_bf

    #@1f4
    .line 542
    .end local v12           #initialShortTitleStr:Ljava/lang/CharSequence;
    :cond_1f4
    const/4 v12, 0x0

    #@1f5
    goto :goto_1ed

    #@1f6
    .line 549
    .end local v14           #initialTitleStr:Ljava/lang/CharSequence;
    :cond_1f6
    move-object/from16 v0, p0

    #@1f8
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@1fa
    move-object/from16 v17, v0

    #@1fc
    move-object/from16 v0, p0

    #@1fe
    move-object/from16 v1, v17

    #@200
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->onBuildHeaders(Ljava/util/List;)V

    #@203
    .line 554
    move-object/from16 v0, p0

    #@205
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@207
    move-object/from16 v17, v0

    #@209
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@20c
    move-result v17

    #@20d
    if-lez v17, :cond_bf

    #@20f
    .line 555
    move-object/from16 v0, p0

    #@211
    iget-boolean v0, v0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@213
    move/from16 v17, v0

    #@215
    if-nez v17, :cond_bf

    #@217
    .line 556
    if-nez v10, :cond_224

    #@219
    .line 557
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->onGetInitialHeader()Landroid/preference/PreferenceActivity$Header;

    #@21c
    move-result-object v6

    #@21d
    .line 558
    .local v6, h:Landroid/preference/PreferenceActivity$Header;
    move-object/from16 v0, p0

    #@21f
    invoke-virtual {v0, v6}, Landroid/preference/PreferenceActivity;->switchToHeader(Landroid/preference/PreferenceActivity$Header;)V

    #@222
    goto/16 :goto_bf

    #@224
    .line 560
    .end local v6           #h:Landroid/preference/PreferenceActivity$Header;
    :cond_224
    move-object/from16 v0, p0

    #@226
    invoke-virtual {v0, v10, v9}, Landroid/preference/PreferenceActivity;->switchToHeader(Ljava/lang/String;Landroid/os/Bundle;)V

    #@229
    goto/16 :goto_bf

    #@22b
    .line 575
    .restart local v14       #initialTitleStr:Ljava/lang/CharSequence;
    :cond_22b
    const/4 v12, 0x0

    #@22c
    goto/16 :goto_f4

    #@22e
    .line 579
    .end local v14           #initialTitleStr:Ljava/lang/CharSequence;
    :cond_22e
    move-object/from16 v0, p0

    #@230
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@232
    move-object/from16 v17, v0

    #@234
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    #@237
    move-result v17

    #@238
    if-lez v17, :cond_285

    #@23a
    .line 580
    new-instance v17, Landroid/preference/PreferenceActivity$HeaderAdapter;

    #@23c
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@240
    move-object/from16 v18, v0

    #@242
    move-object/from16 v0, v17

    #@244
    move-object/from16 v1, p0

    #@246
    move-object/from16 v2, v18

    #@248
    invoke-direct {v0, v1, v2}, Landroid/preference/PreferenceActivity$HeaderAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    #@24b
    move-object/from16 v0, p0

    #@24d
    move-object/from16 v1, v17

    #@24f
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    #@252
    .line 581
    move-object/from16 v0, p0

    #@254
    iget-boolean v0, v0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@256
    move/from16 v17, v0

    #@258
    if-nez v17, :cond_f9

    #@25a
    .line 583
    invoke-virtual/range {p0 .. p0}, Landroid/preference/PreferenceActivity;->getListView()Landroid/widget/ListView;

    #@25d
    move-result-object v17

    #@25e
    const/16 v18, 0x1

    #@260
    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setChoiceMode(I)V

    #@263
    .line 584
    move-object/from16 v0, p0

    #@265
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@267
    move-object/from16 v17, v0

    #@269
    if-eqz v17, :cond_278

    #@26b
    .line 585
    move-object/from16 v0, p0

    #@26d
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@26f
    move-object/from16 v17, v0

    #@271
    move-object/from16 v0, p0

    #@273
    move-object/from16 v1, v17

    #@275
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setSelectedHeader(Landroid/preference/PreferenceActivity$Header;)V

    #@278
    .line 587
    :cond_278
    move-object/from16 v0, p0

    #@27a
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mPrefsContainer:Landroid/view/ViewGroup;

    #@27c
    move-object/from16 v17, v0

    #@27e
    const/16 v18, 0x0

    #@280
    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@283
    goto/16 :goto_f9

    #@285
    .line 592
    :cond_285
    const v17, 0x10900aa

    #@288
    move-object/from16 v0, p0

    #@28a
    move/from16 v1, v17

    #@28c
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setContentView(I)V

    #@28f
    .line 593
    const v17, 0x1020354

    #@292
    move-object/from16 v0, p0

    #@294
    move/from16 v1, v17

    #@296
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@299
    move-result-object v17

    #@29a
    check-cast v17, Landroid/widget/FrameLayout;

    #@29c
    move-object/from16 v0, v17

    #@29e
    move-object/from16 v1, p0

    #@2a0
    iput-object v0, v1, Landroid/preference/PreferenceActivity;->mListFooter:Landroid/widget/FrameLayout;

    #@2a2
    .line 594
    const v17, 0x1020356

    #@2a5
    move-object/from16 v0, p0

    #@2a7
    move/from16 v1, v17

    #@2a9
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@2ac
    move-result-object v17

    #@2ad
    check-cast v17, Landroid/view/ViewGroup;

    #@2af
    move-object/from16 v0, v17

    #@2b1
    move-object/from16 v1, p0

    #@2b3
    iput-object v0, v1, Landroid/preference/PreferenceActivity;->mPrefsContainer:Landroid/view/ViewGroup;

    #@2b5
    .line 595
    new-instance v17, Landroid/preference/PreferenceManager;

    #@2b7
    const/16 v18, 0x64

    #@2b9
    move-object/from16 v0, v17

    #@2bb
    move-object/from16 v1, p0

    #@2bd
    move/from16 v2, v18

    #@2bf
    invoke-direct {v0, v1, v2}, Landroid/preference/PreferenceManager;-><init>(Landroid/app/Activity;I)V

    #@2c2
    move-object/from16 v0, v17

    #@2c4
    move-object/from16 v1, p0

    #@2c6
    iput-object v0, v1, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2c8
    .line 596
    move-object/from16 v0, p0

    #@2ca
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2cc
    move-object/from16 v17, v0

    #@2ce
    move-object/from16 v0, v17

    #@2d0
    move-object/from16 v1, p0

    #@2d2
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setOnPreferenceTreeClickListener(Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;)V

    #@2d5
    goto/16 :goto_f9

    #@2d7
    .line 634
    .restart local v3       #backButton:Landroid/widget/Button;
    .restart local v4       #buttonText:Ljava/lang/String;
    .restart local v15       #intent:Landroid/content/Intent;
    .restart local v16       #skipButton:Landroid/widget/Button;
    :cond_2d7
    move-object/from16 v0, p0

    #@2d9
    iget-object v0, v0, Landroid/preference/PreferenceActivity;->mNextButton:Landroid/widget/Button;

    #@2db
    move-object/from16 v17, v0

    #@2dd
    move-object/from16 v0, v17

    #@2df
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@2e2
    goto/16 :goto_197

    #@2e4
    .line 643
    :cond_2e4
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@2e7
    goto/16 :goto_1b6
.end method

.method protected onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 898
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    #@3
    .line 900
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 901
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->dispatchActivityDestroy()V

    #@c
    .line 903
    :cond_c
    return-void
.end method

.method public onGetInitialHeader()Landroid/preference/PreferenceActivity$Header;
    .registers 3

    #@0
    .prologue
    .line 706
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    #@9
    return-object v0
.end method

.method public onGetNewHeader()Landroid/preference/PreferenceActivity$Header;
    .registers 2

    #@0
    .prologue
    .line 716
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .registers 10
    .parameter "header"
    .parameter "position"

    #@0
    .prologue
    .line 986
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_20

    #@4
    .line 987
    iget-boolean v0, p0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@6
    if-eqz v0, :cond_1c

    #@8
    .line 988
    iget v5, p1, Landroid/preference/PreferenceActivity$Header;->breadCrumbTitleRes:I

    #@a
    .line 989
    .local v5, titleRes:I
    iget v6, p1, Landroid/preference/PreferenceActivity$Header;->breadCrumbShortTitleRes:I

    #@c
    .line 990
    .local v6, shortTitleRes:I
    if-nez v5, :cond_11

    #@e
    .line 991
    iget v5, p1, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    #@10
    .line 992
    const/4 v6, 0x0

    #@11
    .line 994
    :cond_11
    iget-object v1, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@13
    iget-object v2, p1, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    #@15
    const/4 v3, 0x0

    #@16
    const/4 v4, 0x0

    #@17
    move-object v0, p0

    #@18
    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    #@1b
    .line 1002
    .end local v5           #titleRes:I
    .end local v6           #shortTitleRes:I
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 997
    :cond_1c
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceActivity;->switchToHeader(Landroid/preference/PreferenceActivity$Header;)V

    #@1f
    goto :goto_1b

    #@20
    .line 999
    :cond_20
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    #@22
    if-eqz v0, :cond_1b

    #@24
    .line 1000
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    #@26
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;)V

    #@29
    goto :goto_1b
.end method

.method public onIsHidingHeaders()Z
    .registers 4

    #@0
    .prologue
    .line 695
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getIntent()Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    const-string v1, ":android:no_headers"

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public onIsMultiPane()Z
    .registers 4

    #@0
    .prologue
    .line 682
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    const v2, 0x1110007

    #@7
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@a
    move-result v0

    #@b
    .line 684
    .local v0, preferMultiPane:Z
    return v0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 8
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 968
    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    #@3
    .line 970
    iget-object v1, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@5
    if-eqz v1, :cond_16

    #@7
    .line 971
    iget-object v1, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@9
    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    .line 972
    .local v0, item:Ljava/lang/Object;
    instance-of v1, v0, Landroid/preference/PreferenceActivity$Header;

    #@f
    if-eqz v1, :cond_16

    #@11
    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    #@13
    .end local v0           #item:Ljava/lang/Object;
    invoke-virtual {p0, v0, p3}, Landroid/preference/PreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    #@16
    .line 974
    :cond_16
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 1461
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1462
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@6
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->dispatchNewIntent(Landroid/content/Intent;)V

    #@9
    .line 1464
    :cond_9
    return-void
.end method

.method public onPreferenceStartFragment(Landroid/preference/PreferenceFragment;Landroid/preference/Preference;)Z
    .registers 10
    .parameter "caller"
    .parameter "pref"

    #@0
    .prologue
    .line 1308
    invoke-virtual {p2}, Landroid/preference/Preference;->getFragment()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {p2}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {p2}, Landroid/preference/Preference;->getTitleRes()I

    #@b
    move-result v3

    #@c
    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    #@f
    move-result-object v4

    #@10
    const/4 v5, 0x0

    #@11
    const/4 v6, 0x0

    #@12
    move-object v0, p0

    #@13
    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    #@16
    .line 1310
    const/4 v0, 0x1

    #@17
    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 4
    .parameter "preferenceScreen"
    .parameter "preference"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1436
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 931
    iget-object v2, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@2
    if-eqz v2, :cond_18

    #@4
    .line 932
    const-string v2, ":android:preferences"

    #@6
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 933
    .local v0, container:Landroid/os/Bundle;
    if-eqz v0, :cond_18

    #@c
    .line 934
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@f
    move-result-object v1

    #@10
    .line 935
    .local v1, preferenceScreen:Landroid/preference/PreferenceScreen;
    if-eqz v1, :cond_18

    #@12
    .line 936
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->restoreHierarchyState(Landroid/os/Bundle;)V

    #@15
    .line 937
    iput-object p1, p0, Landroid/preference/PreferenceActivity;->mSavedInstanceState:Landroid/os/Bundle;

    #@17
    .line 946
    .end local v0           #container:Landroid/os/Bundle;
    .end local v1           #preferenceScreen:Landroid/preference/PreferenceScreen;
    :goto_17
    return-void

    #@18
    .line 945
    :cond_18
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@1b
    goto :goto_17
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 7
    .parameter "outState"

    #@0
    .prologue
    .line 907
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 909
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v3

    #@9
    if-lez v3, :cond_25

    #@b
    .line 910
    const-string v3, ":android:headers"

    #@d
    iget-object v4, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@f
    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@12
    .line 911
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@14
    if-eqz v3, :cond_25

    #@16
    .line 912
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@18
    iget-object v4, p0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@1a
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@1d
    move-result v1

    #@1e
    .line 913
    .local v1, index:I
    if-ltz v1, :cond_25

    #@20
    .line 914
    const-string v3, ":android:cur_header"

    #@22
    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@25
    .line 919
    .end local v1           #index:I
    :cond_25
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@27
    if-eqz v3, :cond_3c

    #@29
    .line 920
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@2c
    move-result-object v2

    #@2d
    .line 921
    .local v2, preferenceScreen:Landroid/preference/PreferenceScreen;
    if-eqz v2, :cond_3c

    #@2f
    .line 922
    new-instance v0, Landroid/os/Bundle;

    #@31
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@34
    .line 923
    .local v0, container:Landroid/os/Bundle;
    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->saveHierarchyState(Landroid/os/Bundle;)V

    #@37
    .line 924
    const-string v3, ":android:preferences"

    #@39
    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@3c
    .line 927
    .end local v0           #container:Landroid/os/Bundle;
    .end local v2           #preferenceScreen:Landroid/preference/PreferenceScreen;
    :cond_3c
    return-void
.end method

.method protected onStop()V
    .registers 2

    #@0
    .prologue
    .line 889
    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    #@3
    .line 891
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 892
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@9
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->dispatchActivityStop()V

    #@c
    .line 894
    :cond_c
    return-void
.end method

.method public setListFooter(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    #@0
    .prologue
    .line 881
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mListFooter:Landroid/widget/FrameLayout;

    #@2
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    #@5
    .line 882
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mListFooter:Landroid/widget/FrameLayout;

    #@7
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@9
    const/4 v2, -0x1

    #@a
    const/4 v3, -0x2

    #@b
    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@e
    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@11
    .line 885
    return-void
.end method

.method public setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .registers 5
    .parameter "title"
    .parameter "shortTitle"
    .parameter "listener"

    #@0
    .prologue
    .line 1108
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1109
    iget-object v0, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@6
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    #@9
    .line 1111
    :cond_9
    return-void
.end method

.method public setPreferenceScreen(Landroid/preference/PreferenceScreen;)V
    .registers 4
    .parameter "preferenceScreen"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1367
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;->requirePreferenceManager()V

    #@3
    .line 1369
    iget-object v1, p0, Landroid/preference/PreferenceActivity;->mPreferenceManager:Landroid/preference/PreferenceManager;

    #@5
    invoke-virtual {v1, p1}, Landroid/preference/PreferenceManager;->setPreferences(Landroid/preference/PreferenceScreen;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_1d

    #@b
    if-eqz p1, :cond_1d

    #@d
    .line 1370
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;->postBindPreferences()V

    #@10
    .line 1371
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    #@17
    move-result-object v0

    #@18
    .line 1373
    .local v0, title:Ljava/lang/CharSequence;
    if-eqz v0, :cond_1d

    #@1a
    .line 1374
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->setTitle(Ljava/lang/CharSequence;)V

    #@1d
    .line 1377
    .end local v0           #title:Ljava/lang/CharSequence;
    :cond_1d
    return-void
.end method

.method setSelectedHeader(Landroid/preference/PreferenceActivity$Header;)V
    .registers 5
    .parameter "header"

    #@0
    .prologue
    .line 1114
    iput-object p1, p0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@2
    .line 1115
    iget-object v1, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@7
    move-result v0

    #@8
    .line 1116
    .local v0, index:I
    if-ltz v0, :cond_16

    #@a
    .line 1117
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getListView()Landroid/widget/ListView;

    #@d
    move-result-object v1

    #@e
    const/4 v2, 0x1

    #@f
    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    #@12
    .line 1121
    :goto_12
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceActivity;->showBreadCrumbs(Landroid/preference/PreferenceActivity$Header;)V

    #@15
    .line 1122
    return-void

    #@16
    .line 1119
    :cond_16
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getListView()Landroid/widget/ListView;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Landroid/widget/ListView;->clearChoices()V

    #@1d
    goto :goto_12
.end method

.method showBreadCrumbs(Landroid/preference/PreferenceActivity$Header;)V
    .registers 5
    .parameter "header"

    #@0
    .prologue
    .line 1125
    if-eqz p1, :cond_26

    #@2
    .line 1126
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceActivity$Header;->getBreadCrumbTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    .line 1127
    .local v0, title:Ljava/lang/CharSequence;
    if-nez v0, :cond_14

    #@c
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@13
    move-result-object v0

    #@14
    .line 1128
    :cond_14
    if-nez v0, :cond_1a

    #@16
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getTitle()Ljava/lang/CharSequence;

    #@19
    move-result-object v0

    #@1a
    .line 1129
    :cond_1a
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceActivity$Header;->getBreadCrumbShortTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {p0, v0, v1}, Landroid/preference/PreferenceActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@25
    .line 1133
    .end local v0           #title:Ljava/lang/CharSequence;
    :goto_25
    return-void

    #@26
    .line 1131
    :cond_26
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getTitle()Ljava/lang/CharSequence;

    #@29
    move-result-object v1

    #@2a
    const/4 v2, 0x0

    #@2b
    invoke-virtual {p0, v1, v2}, Landroid/preference/PreferenceActivity;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@2e
    goto :goto_25
.end method

.method public showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 9
    .parameter "title"
    .parameter "shortTitle"

    #@0
    .prologue
    const/16 v4, 0x8

    #@2
    const/4 v5, 0x0

    #@3
    .line 1072
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@5
    if-nez v3, :cond_3e

    #@7
    .line 1073
    const v3, 0x1020016

    #@a
    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@d
    move-result-object v1

    #@e
    .line 1076
    .local v1, crumbs:Landroid/view/View;
    :try_start_e
    check-cast v1, Landroid/app/FragmentBreadCrumbs;

    #@10
    .end local v1           #crumbs:Landroid/view/View;
    iput-object v1, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    :try_end_12
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_12} :catch_1c

    #@12
    .line 1080
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@14
    if-nez v3, :cond_1e

    #@16
    .line 1081
    if-eqz p1, :cond_1b

    #@18
    .line 1082
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceActivity;->setTitle(Ljava/lang/CharSequence;)V

    #@1b
    .line 1097
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 1077
    :catch_1c
    move-exception v2

    #@1d
    .line 1078
    .local v2, e:Ljava/lang/ClassCastException;
    goto :goto_1b

    #@1e
    .line 1086
    .end local v2           #e:Ljava/lang/ClassCastException;
    :cond_1e
    iget-boolean v3, p0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@20
    if-eqz v3, :cond_33

    #@22
    .line 1087
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@24
    invoke-virtual {v3, v4}, Landroid/app/FragmentBreadCrumbs;->setVisibility(I)V

    #@27
    .line 1089
    const v3, 0x102027f

    #@2a
    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->findViewById(I)Landroid/view/View;

    #@2d
    move-result-object v0

    #@2e
    .line 1090
    .local v0, bcSection:Landroid/view/View;
    if-eqz v0, :cond_33

    #@30
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    #@33
    .line 1092
    .end local v0           #bcSection:Landroid/view/View;
    :cond_33
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@35
    const/4 v4, 0x2

    #@36
    invoke-virtual {v3, v4}, Landroid/app/FragmentBreadCrumbs;->setMaxVisible(I)V

    #@39
    .line 1093
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@3b
    invoke-virtual {v3, p0}, Landroid/app/FragmentBreadCrumbs;->setActivity(Landroid/app/Activity;)V

    #@3e
    .line 1095
    :cond_3e
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@40
    invoke-virtual {v3, p1, p2}, Landroid/app/FragmentBreadCrumbs;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    #@43
    .line 1096
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    #@45
    invoke-virtual {v3, v5, v5, v5}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    #@48
    goto :goto_1b
.end method

.method public startPreferenceFragment(Landroid/app/Fragment;Z)V
    .registers 5
    .parameter "fragment"
    .parameter "push"

    #@0
    .prologue
    .line 1229
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    #@7
    move-result-object v0

    #@8
    .line 1230
    .local v0, transaction:Landroid/app/FragmentTransaction;
    const v1, 0x1020356

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    #@e
    .line 1231
    if-eqz p2, :cond_1e

    #@10
    .line 1232
    const/16 v1, 0x1001

    #@12
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    #@15
    .line 1233
    const-string v1, ":android:prefs"

    #@17
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    #@1a
    .line 1237
    :goto_1a
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    #@1d
    .line 1238
    return-void

    #@1e
    .line 1235
    :cond_1e
    const/16 v1, 0x1003

    #@20
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    #@23
    goto :goto_1a
.end method

.method public startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    .registers 16
    .parameter "fragmentClass"
    .parameter "args"
    .parameter "titleRes"
    .parameter "titleText"
    .parameter "resultTo"
    .parameter "resultRequestCode"

    #@0
    .prologue
    .line 1261
    iget-boolean v0, p0, Landroid/preference/PreferenceActivity;->mSinglePane:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 1262
    const/4 v6, 0x0

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move-object v2, p2

    #@8
    move-object v3, p5

    #@9
    move v4, p6

    #@a
    move v5, p3

    #@b
    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    #@e
    .line 1279
    :goto_e
    return-void

    #@f
    .line 1264
    :cond_f
    invoke-static {p0, p1, p2}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    #@12
    move-result-object v7

    #@13
    .line 1265
    .local v7, f:Landroid/app/Fragment;
    if-eqz p5, :cond_18

    #@15
    .line 1266
    invoke-virtual {v7, p5, p6}, Landroid/app/Fragment;->setTargetFragment(Landroid/app/Fragment;I)V

    #@18
    .line 1268
    :cond_18
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    #@1f
    move-result-object v8

    #@20
    .line 1269
    .local v8, transaction:Landroid/app/FragmentTransaction;
    const v0, 0x1020356

    #@23
    invoke-virtual {v8, v0, v7}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    #@26
    .line 1270
    if-eqz p3, :cond_39

    #@28
    .line 1271
    invoke-virtual {v8, p3}, Landroid/app/FragmentTransaction;->setBreadCrumbTitle(I)Landroid/app/FragmentTransaction;

    #@2b
    .line 1275
    :cond_2b
    :goto_2b
    const/16 v0, 0x1001

    #@2d
    invoke-virtual {v8, v0}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    #@30
    .line 1276
    const-string v0, ":android:prefs"

    #@32
    invoke-virtual {v8, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    #@35
    .line 1277
    invoke-virtual {v8}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    #@38
    goto :goto_e

    #@39
    .line 1272
    :cond_39
    if-eqz p4, :cond_2b

    #@3b
    .line 1273
    invoke-virtual {v8, p4}, Landroid/app/FragmentTransaction;->setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    #@3e
    goto :goto_2b
.end method

.method public startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;I)V
    .registers 12
    .parameter "fragmentName"
    .parameter "args"
    .parameter "resultTo"
    .parameter "resultRequestCode"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1036
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move v6, v5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    #@a
    .line 1037
    return-void
.end method

.method public startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V
    .registers 8
    .parameter "fragmentName"
    .parameter "args"
    .parameter "resultTo"
    .parameter "resultRequestCode"
    .parameter "titleRes"
    .parameter "shortTitleRes"

    #@0
    .prologue
    .line 1058
    invoke-virtual {p0, p1, p2, p5, p6}, Landroid/preference/PreferenceActivity;->onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 1059
    .local v0, intent:Landroid/content/Intent;
    if-nez p3, :cond_a

    #@6
    .line 1060
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;)V

    #@9
    .line 1064
    :goto_9
    return-void

    #@a
    .line 1062
    :cond_a
    invoke-virtual {p3, v0, p4}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    #@d
    goto :goto_9
.end method

.method public switchToHeader(Landroid/preference/PreferenceActivity$Header;)V
    .registers 6
    .parameter "header"

    #@0
    .prologue
    .line 1164
    iget-object v1, p0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@2
    if-ne v1, p1, :cond_f

    #@4
    .line 1167
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getFragmentManager()Landroid/app/FragmentManager;

    #@7
    move-result-object v1

    #@8
    const-string v2, ":android:prefs"

    #@a
    const/4 v3, 0x1

    #@b
    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    #@e
    .line 1174
    :goto_e
    return-void

    #@f
    .line 1170
    :cond_f
    iget-object v1, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@14
    move-result v1

    #@15
    iget-object v2, p0, Landroid/preference/PreferenceActivity;->mHeaders:Ljava/util/ArrayList;

    #@17
    iget-object v3, p0, Landroid/preference/PreferenceActivity;->mCurHeader:Landroid/preference/PreferenceActivity$Header;

    #@19
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@1c
    move-result v2

    #@1d
    sub-int v0, v1, v2

    #@1f
    .line 1171
    .local v0, direction:I
    iget-object v1, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    #@21
    iget-object v2, p1, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    #@23
    invoke-direct {p0, v1, v2, v0}, Landroid/preference/PreferenceActivity;->switchToHeaderInner(Ljava/lang/String;Landroid/os/Bundle;I)V

    #@26
    .line 1172
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceActivity;->setSelectedHeader(Landroid/preference/PreferenceActivity$Header;)V

    #@29
    goto :goto_e
.end method

.method public switchToHeader(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "fragmentName"
    .parameter "args"

    #@0
    .prologue
    .line 1153
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->setSelectedHeader(Landroid/preference/PreferenceActivity$Header;)V

    #@4
    .line 1154
    const/4 v0, 0x0

    #@5
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/PreferenceActivity;->switchToHeaderInner(Ljava/lang/String;Landroid/os/Bundle;I)V

    #@8
    .line 1155
    return-void
.end method
