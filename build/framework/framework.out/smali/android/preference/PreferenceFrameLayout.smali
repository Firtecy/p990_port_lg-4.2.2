.class public Landroid/preference/PreferenceFrameLayout;
.super Landroid/widget/FrameLayout;
.source "PreferenceFrameLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/PreferenceFrameLayout$LayoutParams;
    }
.end annotation


# static fields
.field private static final DEFAULT_BORDER_BOTTOM:I

.field private static final DEFAULT_BORDER_LEFT:I

.field private static final DEFAULT_BORDER_RIGHT:I

.field private static final DEFAULT_BORDER_TOP:I


# instance fields
.field private final mBorderBottom:I

.field private final mBorderLeft:I

.field private final mBorderRight:I

.field private final mBorderTop:I

.field private mPaddingApplied:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/PreferenceFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 45
    const v0, 0x10103f5

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/PreferenceFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 14
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/high16 v8, 0x3f00

    #@3
    const/4 v7, 0x0

    #@4
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@7
    .line 50
    sget-object v6, Lcom/android/internal/R$styleable;->PreferenceFrameLayout:[I

    #@9
    invoke-virtual {p1, p2, v6, p3, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    .line 53
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@14
    move-result-object v6

    #@15
    iget v5, v6, Landroid/util/DisplayMetrics;->density:F

    #@17
    .line 54
    .local v5, density:F
    mul-float v6, v5, v7

    #@19
    add-float/2addr v6, v8

    #@1a
    float-to-int v1, v6

    #@1b
    .line 55
    .local v1, defaultBorderTop:I
    mul-float v6, v5, v7

    #@1d
    add-float/2addr v6, v8

    #@1e
    float-to-int v2, v6

    #@1f
    .line 56
    .local v2, defaultBottomPadding:I
    mul-float v6, v5, v7

    #@21
    add-float/2addr v6, v8

    #@22
    float-to-int v3, v6

    #@23
    .line 57
    .local v3, defaultLeftPadding:I
    mul-float v6, v5, v7

    #@25
    add-float/2addr v6, v8

    #@26
    float-to-int v4, v6

    #@27
    .line 59
    .local v4, defaultRightPadding:I
    invoke-virtual {v0, v9, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@2a
    move-result v6

    #@2b
    iput v6, p0, Landroid/preference/PreferenceFrameLayout;->mBorderTop:I

    #@2d
    .line 62
    const/4 v6, 0x1

    #@2e
    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@31
    move-result v6

    #@32
    iput v6, p0, Landroid/preference/PreferenceFrameLayout;->mBorderBottom:I

    #@34
    .line 65
    const/4 v6, 0x2

    #@35
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@38
    move-result v6

    #@39
    iput v6, p0, Landroid/preference/PreferenceFrameLayout;->mBorderLeft:I

    #@3b
    .line 68
    const/4 v6, 0x3

    #@3c
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@3f
    move-result v6

    #@40
    iput v6, p0, Landroid/preference/PreferenceFrameLayout;->mBorderRight:I

    #@42
    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@45
    .line 73
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 13
    .parameter "child"

    #@0
    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingTop()I

    #@3
    move-result v3

    #@4
    .line 86
    .local v3, borderTop:I
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingBottom()I

    #@7
    move-result v0

    #@8
    .line 87
    .local v0, borderBottom:I
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingLeft()I

    #@b
    move-result v1

    #@c
    .line 88
    .local v1, borderLeft:I
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingRight()I

    #@f
    move-result v2

    #@10
    .line 90
    .local v2, borderRight:I
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@13
    move-result-object v5

    #@14
    .line 91
    .local v5, params:Landroid/view/ViewGroup$LayoutParams;
    instance-of v10, v5, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    #@16
    if-eqz v10, :cond_57

    #@18
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@1b
    move-result-object v10

    #@1c
    check-cast v10, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    #@1e
    move-object v4, v10

    #@1f
    .line 94
    .local v4, layoutParams:Landroid/preference/PreferenceFrameLayout$LayoutParams;
    :goto_1f
    if-eqz v4, :cond_59

    #@21
    iget-boolean v10, v4, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    #@23
    if-eqz v10, :cond_59

    #@25
    .line 95
    iget-boolean v10, p0, Landroid/preference/PreferenceFrameLayout;->mPaddingApplied:Z

    #@27
    if-eqz v10, :cond_38

    #@29
    .line 96
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderTop:I

    #@2b
    sub-int/2addr v3, v10

    #@2c
    .line 97
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderBottom:I

    #@2e
    sub-int/2addr v0, v10

    #@2f
    .line 98
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderLeft:I

    #@31
    sub-int/2addr v1, v10

    #@32
    .line 99
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderRight:I

    #@34
    sub-int/2addr v2, v10

    #@35
    .line 100
    const/4 v10, 0x0

    #@36
    iput-boolean v10, p0, Landroid/preference/PreferenceFrameLayout;->mPaddingApplied:Z

    #@38
    .line 114
    :cond_38
    :goto_38
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingTop()I

    #@3b
    move-result v9

    #@3c
    .line 115
    .local v9, previousTop:I
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingBottom()I

    #@3f
    move-result v6

    #@40
    .line 116
    .local v6, previousBottom:I
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingLeft()I

    #@43
    move-result v7

    #@44
    .line 117
    .local v7, previousLeft:I
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getPaddingRight()I

    #@47
    move-result v8

    #@48
    .line 118
    .local v8, previousRight:I
    if-ne v9, v3, :cond_50

    #@4a
    if-ne v6, v0, :cond_50

    #@4c
    if-ne v7, v1, :cond_50

    #@4e
    if-eq v8, v2, :cond_53

    #@50
    .line 120
    :cond_50
    invoke-virtual {p0, v1, v3, v2, v0}, Landroid/preference/PreferenceFrameLayout;->setPadding(IIII)V

    #@53
    .line 123
    :cond_53
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    #@56
    .line 124
    return-void

    #@57
    .line 91
    .end local v4           #layoutParams:Landroid/preference/PreferenceFrameLayout$LayoutParams;
    .end local v6           #previousBottom:I
    .end local v7           #previousLeft:I
    .end local v8           #previousRight:I
    .end local v9           #previousTop:I
    :cond_57
    const/4 v4, 0x0

    #@58
    goto :goto_1f

    #@59
    .line 105
    .restart local v4       #layoutParams:Landroid/preference/PreferenceFrameLayout$LayoutParams;
    :cond_59
    iget-boolean v10, p0, Landroid/preference/PreferenceFrameLayout;->mPaddingApplied:Z

    #@5b
    if-nez v10, :cond_38

    #@5d
    .line 106
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderTop:I

    #@5f
    add-int/2addr v3, v10

    #@60
    .line 107
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderBottom:I

    #@62
    add-int/2addr v0, v10

    #@63
    .line 108
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderLeft:I

    #@65
    add-int/2addr v1, v10

    #@66
    .line 109
    iget v10, p0, Landroid/preference/PreferenceFrameLayout;->mBorderRight:I

    #@68
    add-int/2addr v2, v10

    #@69
    .line 110
    const/4 v10, 0x1

    #@6a
    iput-boolean v10, p0, Landroid/preference/PreferenceFrameLayout;->mPaddingApplied:Z

    #@6c
    goto :goto_38
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/preference/PreferenceFrameLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 80
    new-instance v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Landroid/preference/PreferenceFrameLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/preference/PreferenceFrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceFrameLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/preference/PreferenceFrameLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceFrameLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/preference/PreferenceFrameLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
