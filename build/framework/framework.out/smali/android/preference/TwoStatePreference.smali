.class public abstract Landroid/preference/TwoStatePreference;
.super Landroid/preference/Preference;
.source "TwoStatePreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/TwoStatePreference$SavedState;
    }
.end annotation


# instance fields
.field mChecked:Z

.field private mCheckedSet:Z

.field private mDisableDependentsState:Z

.field private mSendClickAccessibilityEvent:Z

.field private mSummaryOff:Ljava/lang/CharSequence;

.field private mSummaryOn:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/TwoStatePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 50
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/TwoStatePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 47
    return-void
.end method


# virtual methods
.method public getDisableDependentsState()Z
    .registers 2

    #@0
    .prologue
    .line 170
    iget-boolean v0, p0, Landroid/preference/TwoStatePreference;->mDisableDependentsState:Z

    #@2
    return v0
.end method

.method public getSummaryOff()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/preference/TwoStatePreference;->mSummaryOff:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getSummaryOn()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/preference/TwoStatePreference;->mSummaryOn:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public isChecked()Z
    .registers 2

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@2
    return v0
.end method

.method protected onClick()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 59
    invoke-super {p0}, Landroid/preference/Preference;->onClick()V

    #@4
    .line 61
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_18

    #@a
    move v0, v1

    #@b
    .line 63
    .local v0, newValue:Z
    :goto_b
    iput-boolean v1, p0, Landroid/preference/TwoStatePreference;->mSendClickAccessibilityEvent:Z

    #@d
    .line 65
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0, v1}, Landroid/preference/TwoStatePreference;->callChangeListener(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_1a

    #@17
    .line 70
    :goto_17
    return-void

    #@18
    .line 61
    .end local v0           #newValue:Z
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_b

    #@1a
    .line 69
    .restart local v0       #newValue:Z
    :cond_1a
    invoke-virtual {p0, v0}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    #@1d
    goto :goto_17
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .registers 4
    .parameter "a"
    .parameter "index"

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4
    move-result v0

    #@5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 260
    if-eqz p1, :cond_e

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    const-class v2, Landroid/preference/TwoStatePreference$SavedState;

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    .line 262
    :cond_e
    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@11
    .line 269
    :goto_11
    return-void

    #@12
    :cond_12
    move-object v0, p1

    #@13
    .line 266
    check-cast v0, Landroid/preference/TwoStatePreference$SavedState;

    #@15
    .line 267
    .local v0, myState:Landroid/preference/TwoStatePreference$SavedState;
    invoke-virtual {v0}, Landroid/preference/TwoStatePreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@18
    move-result-object v1

    #@19
    invoke-super {p0, v1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1c
    .line 268
    iget-boolean v1, v0, Landroid/preference/TwoStatePreference$SavedState;->checked:Z

    #@1e
    invoke-virtual {p0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    #@21
    goto :goto_11
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 247
    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 248
    .local v1, superState:Landroid/os/Parcelable;
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->isPersistent()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 255
    .end local v1           #superState:Landroid/os/Parcelable;
    :goto_a
    return-object v1

    #@b
    .line 253
    .restart local v1       #superState:Landroid/os/Parcelable;
    :cond_b
    new-instance v0, Landroid/preference/TwoStatePreference$SavedState;

    #@d
    invoke-direct {v0, v1}, Landroid/preference/TwoStatePreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@10
    .line 254
    .local v0, myState:Landroid/preference/TwoStatePreference$SavedState;
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    #@13
    move-result v2

    #@14
    iput-boolean v2, v0, Landroid/preference/TwoStatePreference$SavedState;->checked:Z

    #@16
    move-object v1, v0

    #@17
    .line 255
    goto :goto_a
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 4
    .parameter "restoreValue"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 190
    if-eqz p1, :cond_c

    #@2
    iget-boolean v0, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@4
    invoke-virtual {p0, v0}, Landroid/preference/TwoStatePreference;->getPersistedBoolean(Z)Z

    #@7
    move-result v0

    #@8
    .end local p2
    :goto_8
    invoke-virtual {p0, v0}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    #@b
    .line 192
    return-void

    #@c
    .line 190
    .restart local p2
    :cond_c
    check-cast p2, Ljava/lang/Boolean;

    #@e
    .end local p2
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    #@11
    move-result v0

    #@12
    goto :goto_8
.end method

.method sendAccessibilityEvent(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@7
    move-result-object v0

    #@8
    .line 199
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    iget-boolean v2, p0, Landroid/preference/TwoStatePreference;->mSendClickAccessibilityEvent:Z

    #@a
    if-eqz v2, :cond_23

    #@c
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_23

    #@12
    .line 200
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    #@15
    move-result-object v1

    #@16
    .line 201
    .local v1, event:Landroid/view/accessibility/AccessibilityEvent;
    const/4 v2, 0x1

    #@17
    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    #@1a
    .line 202
    invoke-virtual {p1, v1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@1d
    .line 203
    invoke-virtual {p1, v1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    #@20
    .line 204
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@23
    .line 206
    .end local v1           #event:Landroid/view/accessibility/AccessibilityEvent;
    :cond_23
    const/4 v2, 0x0

    #@24
    iput-boolean v2, p0, Landroid/preference/TwoStatePreference;->mSendClickAccessibilityEvent:Z

    #@26
    .line 207
    return-void
.end method

.method public setChecked(Z)V
    .registers 5
    .parameter "checked"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 79
    iget-boolean v2, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@3
    if-eq v2, p1, :cond_20

    #@5
    move v0, v1

    #@6
    .line 80
    .local v0, changed:Z
    :goto_6
    if-nez v0, :cond_c

    #@8
    iget-boolean v2, p0, Landroid/preference/TwoStatePreference;->mCheckedSet:Z

    #@a
    if-nez v2, :cond_1f

    #@c
    .line 81
    :cond_c
    iput-boolean p1, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@e
    .line 82
    iput-boolean v1, p0, Landroid/preference/TwoStatePreference;->mCheckedSet:Z

    #@10
    .line 83
    invoke-virtual {p0, p1}, Landroid/preference/TwoStatePreference;->persistBoolean(Z)Z

    #@13
    .line 84
    if-eqz v0, :cond_1f

    #@15
    .line 85
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->shouldDisableDependents()Z

    #@18
    move-result v1

    #@19
    invoke-virtual {p0, v1}, Landroid/preference/TwoStatePreference;->notifyDependencyChange(Z)V

    #@1c
    .line 86
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->notifyChanged()V

    #@1f
    .line 89
    :cond_1f
    return-void

    #@20
    .line 79
    .end local v0           #changed:Z
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_6
.end method

.method public setDisableDependentsState(Z)V
    .registers 2
    .parameter "disableDependentsState"

    #@0
    .prologue
    .line 180
    iput-boolean p1, p0, Landroid/preference/TwoStatePreference;->mDisableDependentsState:Z

    #@2
    .line 181
    return-void
.end method

.method public setSummaryOff(I)V
    .registers 3
    .parameter "summaryResId"

    #@0
    .prologue
    .line 151
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/TwoStatePreference;->setSummaryOff(Ljava/lang/CharSequence;)V

    #@b
    .line 152
    return-void
.end method

.method public setSummaryOff(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "summary"

    #@0
    .prologue
    .line 140
    iput-object p1, p0, Landroid/preference/TwoStatePreference;->mSummaryOff:Ljava/lang/CharSequence;

    #@2
    .line 141
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_b

    #@8
    .line 142
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->notifyChanged()V

    #@b
    .line 144
    :cond_b
    return-void
.end method

.method public setSummaryOn(I)V
    .registers 3
    .parameter "summaryResId"

    #@0
    .prologue
    .line 123
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/preference/TwoStatePreference;->setSummaryOn(Ljava/lang/CharSequence;)V

    #@b
    .line 124
    return-void
.end method

.method public setSummaryOn(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "summary"

    #@0
    .prologue
    .line 112
    iput-object p1, p0, Landroid/preference/TwoStatePreference;->mSummaryOn:Ljava/lang/CharSequence;

    #@2
    .line 113
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 114
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->notifyChanged()V

    #@b
    .line 116
    :cond_b
    return-void
.end method

.method public shouldDisableDependents()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 102
    iget-boolean v3, p0, Landroid/preference/TwoStatePreference;->mDisableDependentsState:Z

    #@4
    if-eqz v3, :cond_12

    #@6
    iget-boolean v0, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@8
    .line 103
    .local v0, shouldDisable:Z
    :goto_8
    if-nez v0, :cond_10

    #@a
    invoke-super {p0}, Landroid/preference/Preference;->shouldDisableDependents()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_11

    #@10
    :cond_10
    move v1, v2

    #@11
    :cond_11
    return v1

    #@12
    .line 102
    .end local v0           #shouldDisable:Z
    :cond_12
    iget-boolean v3, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@14
    if-nez v3, :cond_18

    #@16
    move v0, v2

    #@17
    goto :goto_8

    #@18
    :cond_18
    move v0, v1

    #@19
    goto :goto_8
.end method

.method syncSummaryView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    #@0
    .prologue
    .line 215
    const v4, 0x1020010

    #@3
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Landroid/widget/TextView;

    #@9
    .line 216
    .local v2, summaryView:Landroid/widget/TextView;
    if-eqz v2, :cond_34

    #@b
    .line 217
    const/4 v3, 0x1

    #@c
    .line 218
    .local v3, useDefaultSummary:Z
    iget-boolean v4, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@e
    if-eqz v4, :cond_35

    #@10
    iget-object v4, p0, Landroid/preference/TwoStatePreference;->mSummaryOn:Ljava/lang/CharSequence;

    #@12
    if-eqz v4, :cond_35

    #@14
    .line 219
    iget-object v4, p0, Landroid/preference/TwoStatePreference;->mSummaryOn:Ljava/lang/CharSequence;

    #@16
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@19
    .line 220
    const/4 v3, 0x0

    #@1a
    .line 226
    :cond_1a
    :goto_1a
    if-eqz v3, :cond_26

    #@1c
    .line 227
    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->getSummary()Ljava/lang/CharSequence;

    #@1f
    move-result-object v1

    #@20
    .line 228
    .local v1, summary:Ljava/lang/CharSequence;
    if-eqz v1, :cond_26

    #@22
    .line 229
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@25
    .line 230
    const/4 v3, 0x0

    #@26
    .line 234
    .end local v1           #summary:Ljava/lang/CharSequence;
    :cond_26
    const/16 v0, 0x8

    #@28
    .line 235
    .local v0, newVisibility:I
    if-nez v3, :cond_2b

    #@2a
    .line 237
    const/4 v0, 0x0

    #@2b
    .line 239
    :cond_2b
    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    #@2e
    move-result v4

    #@2f
    if-eq v0, v4, :cond_34

    #@31
    .line 240
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    #@34
    .line 243
    .end local v0           #newVisibility:I
    .end local v3           #useDefaultSummary:Z
    :cond_34
    return-void

    #@35
    .line 221
    .restart local v3       #useDefaultSummary:Z
    :cond_35
    iget-boolean v4, p0, Landroid/preference/TwoStatePreference;->mChecked:Z

    #@37
    if-nez v4, :cond_1a

    #@39
    iget-object v4, p0, Landroid/preference/TwoStatePreference;->mSummaryOff:Ljava/lang/CharSequence;

    #@3b
    if-eqz v4, :cond_1a

    #@3d
    .line 222
    iget-object v4, p0, Landroid/preference/TwoStatePreference;->mSummaryOff:Ljava/lang/CharSequence;

    #@3f
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@42
    .line 223
    const/4 v3, 0x0

    #@43
    goto :goto_1a
.end method
