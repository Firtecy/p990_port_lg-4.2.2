.class Landroid/preference/PreferenceGroupAdapter;
.super Landroid/widget/BaseAdapter;
.source "PreferenceGroupAdapter.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeInternalListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PreferenceGroupAdapter"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mHasReturnedViewTypeCount:Z

.field private volatile mIsSyncing:Z

.field private mPreferenceGroup:Landroid/preference/PreferenceGroup;

.field private mPreferenceLayouts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferenceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncRunnable:Ljava/lang/Runnable;

.field private mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;


# direct methods
.method public constructor <init>(Landroid/preference/PreferenceGroup;)V
    .registers 5
    .parameter "preferenceGroup"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 114
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    #@4
    .line 74
    new-instance v0, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {v0, v1}, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;-><init>(Landroid/preference/PreferenceGroupAdapter$1;)V

    #@a
    iput-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@c
    .line 79
    iput-boolean v2, p0, Landroid/preference/PreferenceGroupAdapter;->mHasReturnedViewTypeCount:Z

    #@e
    .line 81
    iput-boolean v2, p0, Landroid/preference/PreferenceGroupAdapter;->mIsSyncing:Z

    #@10
    .line 83
    new-instance v0, Landroid/os/Handler;

    #@12
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@15
    iput-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mHandler:Landroid/os/Handler;

    #@17
    .line 85
    new-instance v0, Landroid/preference/PreferenceGroupAdapter$1;

    #@19
    invoke-direct {v0, p0}, Landroid/preference/PreferenceGroupAdapter$1;-><init>(Landroid/preference/PreferenceGroupAdapter;)V

    #@1c
    iput-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mSyncRunnable:Ljava/lang/Runnable;

    #@1e
    .line 115
    iput-object p1, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceGroup:Landroid/preference/PreferenceGroup;

    #@20
    .line 117
    iget-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceGroup:Landroid/preference/PreferenceGroup;

    #@22
    invoke-virtual {v0, p0}, Landroid/preference/PreferenceGroup;->setOnPreferenceChangeInternalListener(Landroid/preference/Preference$OnPreferenceChangeInternalListener;)V

    #@25
    .line 119
    new-instance v0, Ljava/util/ArrayList;

    #@27
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2a
    iput-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceList:Ljava/util/List;

    #@2c
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceLayouts:Ljava/util/ArrayList;

    #@33
    .line 122
    invoke-direct {p0}, Landroid/preference/PreferenceGroupAdapter;->syncMyPreferences()V

    #@36
    .line 123
    return-void
.end method

.method static synthetic access$100(Landroid/preference/PreferenceGroupAdapter;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Landroid/preference/PreferenceGroupAdapter;->syncMyPreferences()V

    #@3
    return-void
.end method

.method private addPreferenceClassName(Landroid/preference/Preference;)V
    .registers 5
    .parameter "preference"

    #@0
    .prologue
    .line 185
    const/4 v2, 0x0

    #@1
    invoke-direct {p0, p1, v2}, Landroid/preference/PreferenceGroupAdapter;->createPreferenceLayout(Landroid/preference/Preference;Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;)Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@4
    move-result-object v1

    #@5
    .line 186
    .local v1, pl:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;
    iget-object v2, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceLayouts:Ljava/util/ArrayList;

    #@7
    invoke-static {v2, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@a
    move-result v0

    #@b
    .line 189
    .local v0, insertPos:I
    if-gez v0, :cond_16

    #@d
    .line 191
    mul-int/lit8 v2, v0, -0x1

    #@f
    add-int/lit8 v0, v2, -0x1

    #@11
    .line 192
    iget-object v2, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceLayouts:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v2, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@16
    .line 194
    :cond_16
    return-void
.end method

.method private createPreferenceLayout(Landroid/preference/Preference;Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;)Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;
    .registers 5
    .parameter "preference"
    .parameter "in"

    #@0
    .prologue
    .line 177
    if-eqz p2, :cond_1d

    #@2
    move-object v0, p2

    #@3
    .line 178
    .local v0, pl:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;
    :goto_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-static {v0, v1}, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;->access$202(Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;Ljava/lang/String;)Ljava/lang/String;

    #@e
    .line 179
    invoke-virtual {p1}, Landroid/preference/Preference;->getLayoutResource()I

    #@11
    move-result v1

    #@12
    invoke-static {v0, v1}, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;->access$302(Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;I)I

    #@15
    .line 180
    invoke-virtual {p1}, Landroid/preference/Preference;->getWidgetLayoutResource()I

    #@18
    move-result v1

    #@19
    invoke-static {v0, v1}, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;->access$402(Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;I)I

    #@1c
    .line 181
    return-object v0

    #@1d
    .line 177
    .end local v0           #pl:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;
    :cond_1d
    new-instance v0, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@1f
    const/4 v1, 0x0

    #@20
    invoke-direct {v0, v1}, Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;-><init>(Landroid/preference/PreferenceGroupAdapter$1;)V

    #@23
    goto :goto_3
.end method

.method private flattenPreferenceGroup(Ljava/util/List;Landroid/preference/PreferenceGroup;)V
    .registers 8
    .parameter
    .parameter "group"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;",
            "Landroid/preference/PreferenceGroup;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 148
    .local p1, preferences:Ljava/util/List;,"Ljava/util/List<Landroid/preference/Preference;>;"
    invoke-virtual {p2}, Landroid/preference/PreferenceGroup;->sortPreferences()V

    #@3
    .line 150
    invoke-virtual {p2}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    #@6
    move-result v0

    #@7
    .line 151
    .local v0, groupSize:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_34

    #@a
    .line 152
    invoke-virtual {p2, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    #@d
    move-result-object v2

    #@e
    .line 154
    .local v2, preference:Landroid/preference/Preference;
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@11
    .line 156
    iget-boolean v4, p0, Landroid/preference/PreferenceGroupAdapter;->mHasReturnedViewTypeCount:Z

    #@13
    if-nez v4, :cond_1e

    #@15
    invoke-virtual {v2}, Landroid/preference/Preference;->hasSpecifiedLayout()Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_1e

    #@1b
    .line 157
    invoke-direct {p0, v2}, Landroid/preference/PreferenceGroupAdapter;->addPreferenceClassName(Landroid/preference/Preference;)V

    #@1e
    .line 160
    :cond_1e
    instance-of v4, v2, Landroid/preference/PreferenceGroup;

    #@20
    if-eqz v4, :cond_2e

    #@22
    move-object v3, v2

    #@23
    .line 161
    check-cast v3, Landroid/preference/PreferenceGroup;

    #@25
    .line 162
    .local v3, preferenceAsGroup:Landroid/preference/PreferenceGroup;
    invoke-virtual {v3}, Landroid/preference/PreferenceGroup;->isOnSameScreenAsChildren()Z

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_2e

    #@2b
    .line 163
    invoke-direct {p0, p1, v3}, Landroid/preference/PreferenceGroupAdapter;->flattenPreferenceGroup(Ljava/util/List;Landroid/preference/PreferenceGroup;)V

    #@2e
    .line 167
    .end local v3           #preferenceAsGroup:Landroid/preference/PreferenceGroup;
    :cond_2e
    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeInternalListener(Landroid/preference/Preference$OnPreferenceChangeInternalListener;)V

    #@31
    .line 151
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_8

    #@34
    .line 169
    .end local v2           #preference:Landroid/preference/Preference;
    :cond_34
    return-void
.end method

.method private syncMyPreferences()V
    .registers 3

    #@0
    .prologue
    .line 126
    monitor-enter p0

    #@1
    .line 127
    :try_start_1
    iget-boolean v1, p0, Landroid/preference/PreferenceGroupAdapter;->mIsSyncing:Z

    #@3
    if-eqz v1, :cond_7

    #@5
    .line 128
    monitor-exit p0

    #@6
    .line 144
    :goto_6
    return-void

    #@7
    .line 131
    :cond_7
    const/4 v1, 0x1

    #@8
    iput-boolean v1, p0, Landroid/preference/PreferenceGroupAdapter;->mIsSyncing:Z

    #@a
    .line 132
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_2c

    #@b
    .line 134
    new-instance v0, Ljava/util/ArrayList;

    #@d
    iget-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceList:Ljava/util/List;

    #@f
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@12
    move-result v1

    #@13
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@16
    .line 135
    .local v0, newPreferenceList:Ljava/util/List;,"Ljava/util/List<Landroid/preference/Preference;>;"
    iget-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceGroup:Landroid/preference/PreferenceGroup;

    #@18
    invoke-direct {p0, v0, v1}, Landroid/preference/PreferenceGroupAdapter;->flattenPreferenceGroup(Ljava/util/List;Landroid/preference/PreferenceGroup;)V

    #@1b
    .line 136
    iput-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceList:Ljava/util/List;

    #@1d
    .line 138
    invoke-virtual {p0}, Landroid/preference/PreferenceGroupAdapter;->notifyDataSetChanged()V

    #@20
    .line 140
    monitor-enter p0

    #@21
    .line 141
    const/4 v1, 0x0

    #@22
    :try_start_22
    iput-boolean v1, p0, Landroid/preference/PreferenceGroupAdapter;->mIsSyncing:Z

    #@24
    .line 142
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@27
    .line 143
    monitor-exit p0

    #@28
    goto :goto_6

    #@29
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_22 .. :try_end_2b} :catchall_29

    #@2b
    throw v1

    #@2c
    .line 132
    .end local v0           #newPreferenceList:Ljava/util/List;,"Ljava/util/List<Landroid/preference/Preference;>;"
    :catchall_2c
    move-exception v1

    #@2d
    :try_start_2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    #@2e
    throw v1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 234
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceList:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getItem(I)Landroid/preference/Preference;
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 201
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/preference/PreferenceGroupAdapter;->getCount()I

    #@5
    move-result v0

    #@6
    if-lt p1, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 202
    :goto_9
    return-object v0

    #@a
    :cond_a
    iget-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceList:Ljava/util/List;

    #@c
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/preference/Preference;

    #@12
    goto :goto_9
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroupAdapter;->getItem(I)Landroid/preference/Preference;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 206
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/preference/PreferenceGroupAdapter;->getCount()I

    #@5
    move-result v0

    #@6
    if-lt p1, v0, :cond_b

    #@8
    :cond_8
    const-wide/high16 v0, -0x8000

    #@a
    .line 207
    :goto_a
    return-wide v0

    #@b
    :cond_b
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroupAdapter;->getItem(I)Landroid/preference/Preference;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Landroid/preference/Preference;->getId()J

    #@12
    move-result-wide v0

    #@13
    goto :goto_a
.end method

.method public getItemViewType(I)I
    .registers 7
    .parameter "position"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 253
    iget-boolean v3, p0, Landroid/preference/PreferenceGroupAdapter;->mHasReturnedViewTypeCount:Z

    #@3
    if-nez v3, :cond_8

    #@5
    .line 254
    const/4 v3, 0x1

    #@6
    iput-boolean v3, p0, Landroid/preference/PreferenceGroupAdapter;->mHasReturnedViewTypeCount:Z

    #@8
    .line 257
    :cond_8
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroupAdapter;->getItem(I)Landroid/preference/Preference;

    #@b
    move-result-object v0

    #@c
    .line 258
    .local v0, preference:Landroid/preference/Preference;
    invoke-virtual {v0}, Landroid/preference/Preference;->hasSpecifiedLayout()Z

    #@f
    move-result v3

    #@10
    if-eqz v3, :cond_14

    #@12
    move v1, v2

    #@13
    .line 270
    :cond_13
    :goto_13
    return v1

    #@14
    .line 262
    :cond_14
    iget-object v3, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@16
    invoke-direct {p0, v0, v3}, Landroid/preference/PreferenceGroupAdapter;->createPreferenceLayout(Landroid/preference/Preference;Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;)Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@19
    move-result-object v3

    #@1a
    iput-object v3, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@1c
    .line 264
    iget-object v3, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceLayouts:Ljava/util/ArrayList;

    #@1e
    iget-object v4, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@20
    invoke-static {v3, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@23
    move-result v1

    #@24
    .line 265
    .local v1, viewType:I
    if-gez v1, :cond_13

    #@26
    move v1, v2

    #@27
    .line 268
    goto :goto_13
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 211
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroupAdapter;->getItem(I)Landroid/preference/Preference;

    #@3
    move-result-object v0

    #@4
    .line 213
    .local v0, preference:Landroid/preference/Preference;
    iget-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@6
    invoke-direct {p0, v0, v1}, Landroid/preference/PreferenceGroupAdapter;->createPreferenceLayout(Landroid/preference/Preference;Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;)Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@9
    move-result-object v1

    #@a
    iput-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@c
    .line 217
    iget-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceLayouts:Ljava/util/ArrayList;

    #@e
    iget-object v2, p0, Landroid/preference/PreferenceGroupAdapter;->mTempPreferenceLayout:Landroid/preference/PreferenceGroupAdapter$PreferenceLayout;

    #@10
    invoke-static {v1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@13
    move-result v1

    #@14
    if-gez v1, :cond_17

    #@16
    .line 218
    const/4 p2, 0x0

    #@17
    .line 221
    :cond_17
    invoke-virtual {v0, p2, p3}, Landroid/preference/Preference;->getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@1a
    move-result-object v1

    #@1b
    return-object v1
.end method

.method public getViewTypeCount()I
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 276
    iget-boolean v0, p0, Landroid/preference/PreferenceGroupAdapter;->mHasReturnedViewTypeCount:Z

    #@3
    if-nez v0, :cond_7

    #@5
    .line 277
    iput-boolean v1, p0, Landroid/preference/PreferenceGroupAdapter;->mHasReturnedViewTypeCount:Z

    #@7
    .line 280
    :cond_7
    iget-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mPreferenceLayouts:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v0

    #@d
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public hasStableIds()Z
    .registers 2

    #@0
    .prologue
    .line 248
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isEnabled(I)Z
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 226
    if-ltz p1, :cond_8

    #@2
    invoke-virtual {p0}, Landroid/preference/PreferenceGroupAdapter;->getCount()I

    #@5
    move-result v0

    #@6
    if-lt p1, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    .line 227
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroupAdapter;->getItem(I)Landroid/preference/Preference;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0}, Landroid/preference/Preference;->isSelectable()Z

    #@11
    move-result v0

    #@12
    goto :goto_9
.end method

.method public onPreferenceChange(Landroid/preference/Preference;)V
    .registers 2
    .parameter "preference"

    #@0
    .prologue
    .line 238
    invoke-virtual {p0}, Landroid/preference/PreferenceGroupAdapter;->notifyDataSetChanged()V

    #@3
    .line 239
    return-void
.end method

.method public onPreferenceHierarchyChange(Landroid/preference/Preference;)V
    .registers 4
    .parameter "preference"

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mSyncRunnable:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 243
    iget-object v0, p0, Landroid/preference/PreferenceGroupAdapter;->mHandler:Landroid/os/Handler;

    #@9
    iget-object v1, p0, Landroid/preference/PreferenceGroupAdapter;->mSyncRunnable:Ljava/lang/Runnable;

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@e
    .line 244
    return-void
.end method
