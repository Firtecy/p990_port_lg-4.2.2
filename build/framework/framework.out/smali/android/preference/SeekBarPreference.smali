.class public Landroid/preference/SeekBarPreference;
.super Landroid/preference/Preference;
.source "SeekBarPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/SeekBarPreference$SavedState;
    }
.end annotation


# instance fields
.field private mMax:I

.field private mProgress:I

.field private mTrackingTouch:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 50
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 42
    sget-object v1, Lcom/android/internal/R$styleable;->ProgressBar:[I

    #@5
    const/4 v2, 0x0

    #@6
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 44
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x2

    #@b
    iget v2, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@10
    move-result v1

    #@11
    invoke-virtual {p0, v1}, Landroid/preference/SeekBarPreference;->setMax(I)V

    #@14
    .line 45
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@17
    .line 46
    const v1, 0x10900ad

    #@1a
    invoke-virtual {p0, v1}, Landroid/preference/SeekBarPreference;->setLayoutResource(I)V

    #@1d
    .line 47
    return-void
.end method

.method private setProgress(IZ)V
    .registers 4
    .parameter "progress"
    .parameter "notifyChanged"

    #@0
    .prologue
    .line 112
    iget v0, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@2
    if-le p1, v0, :cond_6

    #@4
    .line 113
    iget p1, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@6
    .line 115
    :cond_6
    if-gez p1, :cond_9

    #@8
    .line 116
    const/4 p1, 0x0

    #@9
    .line 118
    :cond_9
    iget v0, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@b
    if-eq p1, v0, :cond_17

    #@d
    .line 119
    iput p1, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@f
    .line 120
    invoke-virtual {p0, p1}, Landroid/preference/SeekBarPreference;->persistInt(I)Z

    #@12
    .line 121
    if-eqz p2, :cond_17

    #@14
    .line 122
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->notifyChanged()V

    #@17
    .line 125
    :cond_17
    return-void
.end method


# virtual methods
.method public getProgress()I
    .registers 2

    #@0
    .prologue
    .line 128
    iget v0, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@2
    return v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 70
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    #@3
    .line 60
    const v1, 0x102035a

    #@6
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/widget/SeekBar;

    #@c
    .line 62
    .local v0, seekBar:Landroid/widget/SeekBar;
    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    #@f
    .line 63
    iget v1, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@11
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    #@14
    .line 64
    iget v1, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@16
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    #@19
    .line 65
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->isEnabled()Z

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    #@20
    .line 66
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .registers 4
    .parameter "a"
    .parameter "index"

    #@0
    .prologue
    .line 81
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4
    move-result v0

    #@5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 86
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@4
    move-result v1

    #@5
    if-eq v1, v0, :cond_27

    #@7
    .line 87
    const/16 v1, 0x51

    #@9
    if-eq p2, v1, :cond_f

    #@b
    const/16 v1, 0x46

    #@d
    if-ne p2, v1, :cond_19

    #@f
    .line 89
    :cond_f
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->getProgress()I

    #@12
    move-result v1

    #@13
    add-int/lit8 v1, v1, 0x1

    #@15
    invoke-virtual {p0, v1}, Landroid/preference/SeekBarPreference;->setProgress(I)V

    #@18
    .line 97
    :goto_18
    return v0

    #@19
    .line 92
    :cond_19
    const/16 v1, 0x45

    #@1b
    if-ne p2, v1, :cond_27

    #@1d
    .line 93
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->getProgress()I

    #@20
    move-result v1

    #@21
    add-int/lit8 v1, v1, -0x1

    #@23
    invoke-virtual {p0, v1}, Landroid/preference/SeekBarPreference;->setProgress(I)V

    #@26
    goto :goto_18

    #@27
    .line 97
    :cond_27
    const/4 v0, 0x0

    #@28
    goto :goto_18
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .registers 5
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    #@0
    .prologue
    .line 149
    if-eqz p3, :cond_9

    #@2
    iget-boolean v0, p0, Landroid/preference/SeekBarPreference;->mTrackingTouch:Z

    #@4
    if-nez v0, :cond_9

    #@6
    .line 150
    invoke-virtual {p0, p1}, Landroid/preference/SeekBarPreference;->syncProgress(Landroid/widget/SeekBar;)V

    #@9
    .line 152
    :cond_9
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 190
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v1

    #@4
    const-class v2, Landroid/preference/SeekBarPreference$SavedState;

    #@6
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_10

    #@c
    .line 192
    invoke-super {p0, p1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@f
    .line 202
    :goto_f
    return-void

    #@10
    :cond_10
    move-object v0, p1

    #@11
    .line 197
    check-cast v0, Landroid/preference/SeekBarPreference$SavedState;

    #@13
    .line 198
    .local v0, myState:Landroid/preference/SeekBarPreference$SavedState;
    invoke-virtual {v0}, Landroid/preference/SeekBarPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@16
    move-result-object v1

    #@17
    invoke-super {p0, v1}, Landroid/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@1a
    .line 199
    iget v1, v0, Landroid/preference/SeekBarPreference$SavedState;->progress:I

    #@1c
    iput v1, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@1e
    .line 200
    iget v1, v0, Landroid/preference/SeekBarPreference$SavedState;->max:I

    #@20
    iput v1, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@22
    .line 201
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->notifyChanged()V

    #@25
    goto :goto_f
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 175
    invoke-super {p0}, Landroid/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 176
    .local v1, superState:Landroid/os/Parcelable;
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->isPersistent()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_b

    #@a
    .line 185
    .end local v1           #superState:Landroid/os/Parcelable;
    :goto_a
    return-object v1

    #@b
    .line 182
    .restart local v1       #superState:Landroid/os/Parcelable;
    :cond_b
    new-instance v0, Landroid/preference/SeekBarPreference$SavedState;

    #@d
    invoke-direct {v0, v1}, Landroid/preference/SeekBarPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@10
    .line 183
    .local v0, myState:Landroid/preference/SeekBarPreference$SavedState;
    iget v2, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@12
    iput v2, v0, Landroid/preference/SeekBarPreference$SavedState;->progress:I

    #@14
    .line 184
    iget v2, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@16
    iput v2, v0, Landroid/preference/SeekBarPreference$SavedState;->max:I

    #@18
    move-object v1, v0

    #@19
    .line 185
    goto :goto_a
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 4
    .parameter "restoreValue"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 75
    if-eqz p1, :cond_c

    #@2
    iget v0, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@4
    invoke-virtual {p0, v0}, Landroid/preference/SeekBarPreference;->getPersistedInt(I)I

    #@7
    move-result v0

    #@8
    .end local p2
    :goto_8
    invoke-virtual {p0, v0}, Landroid/preference/SeekBarPreference;->setProgress(I)V

    #@b
    .line 77
    return-void

    #@c
    .line 75
    .restart local p2
    :cond_c
    check-cast p2, Ljava/lang/Integer;

    #@e
    .end local p2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    #@11
    move-result v0

    #@12
    goto :goto_8
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 3
    .parameter "seekBar"

    #@0
    .prologue
    .line 156
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/preference/SeekBarPreference;->mTrackingTouch:Z

    #@3
    .line 157
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 4
    .parameter "seekBar"

    #@0
    .prologue
    .line 161
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/preference/SeekBarPreference;->mTrackingTouch:Z

    #@3
    .line 162
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    #@6
    move-result v0

    #@7
    iget v1, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@9
    if-eq v0, v1, :cond_e

    #@b
    .line 163
    invoke-virtual {p0, p1}, Landroid/preference/SeekBarPreference;->syncProgress(Landroid/widget/SeekBar;)V

    #@e
    .line 165
    :cond_e
    return-void
.end method

.method public setMax(I)V
    .registers 3
    .parameter "max"

    #@0
    .prologue
    .line 101
    iget v0, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 102
    iput p1, p0, Landroid/preference/SeekBarPreference;->mMax:I

    #@6
    .line 103
    invoke-virtual {p0}, Landroid/preference/SeekBarPreference;->notifyChanged()V

    #@9
    .line 105
    :cond_9
    return-void
.end method

.method public setProgress(I)V
    .registers 3
    .parameter "progress"

    #@0
    .prologue
    .line 108
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/SeekBarPreference;->setProgress(IZ)V

    #@4
    .line 109
    return-void
.end method

.method syncProgress(Landroid/widget/SeekBar;)V
    .registers 4
    .parameter "seekBar"

    #@0
    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    #@3
    move-result v0

    #@4
    .line 137
    .local v0, progress:I
    iget v1, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@6
    if-eq v0, v1, :cond_16

    #@8
    .line 138
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0, v1}, Landroid/preference/SeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_17

    #@12
    .line 139
    const/4 v1, 0x0

    #@13
    invoke-direct {p0, v0, v1}, Landroid/preference/SeekBarPreference;->setProgress(IZ)V

    #@16
    .line 144
    :cond_16
    :goto_16
    return-void

    #@17
    .line 141
    :cond_17
    iget v1, p0, Landroid/preference/SeekBarPreference;->mProgress:I

    #@19
    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    #@1c
    goto :goto_16
.end method
