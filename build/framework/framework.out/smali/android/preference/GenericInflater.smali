.class abstract Landroid/preference/GenericInflater;
.super Ljava/lang/Object;
.source "GenericInflater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/GenericInflater$FactoryMerger;,
        Landroid/preference/GenericInflater$Factory;,
        Landroid/preference/GenericInflater$Parent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "P::",
        "Landroid/preference/GenericInflater$Parent;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final mConstructorSignature:[Ljava/lang/Class;

.field private static final sConstructorMap:Ljava/util/HashMap;


# instance fields
.field private final DEBUG:Z

.field private final mConstructorArgs:[Ljava/lang/Object;

.field protected final mContext:Landroid/content/Context;

.field private mDefaultPackage:Ljava/lang/String;

.field private mFactory:Landroid/preference/GenericInflater$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/preference/GenericInflater$Factory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mFactorySet:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 55
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/Class;

    #@3
    const/4 v1, 0x0

    #@4
    const-class v2, Landroid/content/Context;

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-class v2, Landroid/util/AttributeSet;

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Landroid/preference/GenericInflater;->mConstructorSignature:[Ljava/lang/Class;

    #@f
    .line 58
    new-instance v0, Ljava/util/HashMap;

    #@11
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@14
    sput-object v0, Landroid/preference/GenericInflater;->sConstructorMap:Ljava/util/HashMap;

    #@16
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 108
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/preference/GenericInflater;->DEBUG:Z

    #@6
    .line 53
    const/4 v0, 0x2

    #@7
    new-array v0, v0, [Ljava/lang/Object;

    #@9
    iput-object v0, p0, Landroid/preference/GenericInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@b
    .line 109
    iput-object p1, p0, Landroid/preference/GenericInflater;->mContext:Landroid/content/Context;

    #@d
    .line 110
    return-void
.end method

.method protected constructor <init>(Landroid/preference/GenericInflater;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter "newContext"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/GenericInflater",
            "<TT;TP;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 120
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p1, original:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/preference/GenericInflater;->DEBUG:Z

    #@6
    .line 53
    const/4 v0, 0x2

    #@7
    new-array v0, v0, [Ljava/lang/Object;

    #@9
    iput-object v0, p0, Landroid/preference/GenericInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@b
    .line 121
    iput-object p2, p0, Landroid/preference/GenericInflater;->mContext:Landroid/content/Context;

    #@d
    .line 122
    iget-object v0, p1, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@f
    iput-object v0, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@11
    .line 123
    return-void
.end method

.method private final createItemFromTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    .registers 9
    .parameter "parser"
    .parameter "name"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Ljava/lang/String;",
            "Landroid/util/AttributeSet;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    const/4 v2, 0x0

    #@1
    .line 424
    :try_start_1
    iget-object v3, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@3
    if-nez v3, :cond_15

    #@5
    .line 426
    .local v2, item:Ljava/lang/Object;,"TT;"
    :goto_5
    if-nez v2, :cond_14

    #@7
    .line 427
    const/4 v3, -0x1

    #@8
    const/16 v4, 0x2e

    #@a
    invoke-virtual {p2, v4}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v4

    #@e
    if-ne v3, v4, :cond_1e

    #@10
    .line 428
    invoke-virtual {p0, p2, p3}, Landroid/preference/GenericInflater;->onCreateItem(Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    .line 435
    :cond_14
    :goto_14
    return-object v2

    #@15
    .line 424
    .end local v2           #item:Ljava/lang/Object;,"TT;"
    :cond_15
    iget-object v3, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@17
    iget-object v4, p0, Landroid/preference/GenericInflater;->mContext:Landroid/content/Context;

    #@19
    invoke-interface {v3, p2, v4, p3}, Landroid/preference/GenericInflater$Factory;->onCreateItem(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    goto :goto_5

    #@1e
    .line 430
    .restart local v2       #item:Ljava/lang/Object;,"TT;"
    :cond_1e
    const/4 v3, 0x0

    #@1f
    invoke-virtual {p0, p2, v3, p3}, Landroid/preference/GenericInflater;->createItem(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    :try_end_22
    .catch Landroid/view/InflateException; {:try_start_1 .. :try_end_22} :catch_24
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_22} :catch_26
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_22} :catch_4b

    #@22
    move-result-object v2

    #@23
    goto :goto_14

    #@24
    .line 437
    .end local v2           #item:Ljava/lang/Object;,"TT;"
    :catch_24
    move-exception v0

    #@25
    .line 438
    .local v0, e:Landroid/view/InflateException;
    throw v0

    #@26
    .line 440
    .end local v0           #e:Landroid/view/InflateException;
    :catch_26
    move-exception v0

    #@27
    .line 441
    .local v0, e:Ljava/lang/ClassNotFoundException;
    new-instance v1, Landroid/view/InflateException;

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, ": Error inflating class "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-direct {v1, v3}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@47
    .line 444
    .local v1, ie:Landroid/view/InflateException;
    invoke-virtual {v1, v0}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@4a
    .line 445
    throw v1

    #@4b
    .line 447
    .end local v0           #e:Ljava/lang/ClassNotFoundException;
    .end local v1           #ie:Landroid/view/InflateException;
    :catch_4b
    move-exception v0

    #@4c
    .line 448
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Landroid/view/InflateException;

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    const-string v4, ": Error inflating class "

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-direct {v1, v3}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@6c
    .line 451
    .restart local v1       #ie:Landroid/view/InflateException;
    invoke-virtual {v1, v0}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@6f
    .line 452
    throw v1
.end method

.method private rInflate(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Object;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "parser"
    .parameter
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "TT;",
            "Landroid/util/AttributeSet;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 462
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p2, parent:Ljava/lang/Object;,"TT;"
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@3
    move-result v0

    #@4
    .line 466
    .local v0, depth:I
    :cond_4
    :goto_4
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7
    move-result v3

    #@8
    .local v3, type:I
    const/4 v4, 0x3

    #@9
    if-ne v3, v4, :cond_11

    #@b
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@e
    move-result v4

    #@f
    if-le v4, v0, :cond_2f

    #@11
    :cond_11
    const/4 v4, 0x1

    #@12
    if-eq v3, v4, :cond_2f

    #@14
    .line 468
    const/4 v4, 0x2

    #@15
    if-ne v3, v4, :cond_4

    #@17
    .line 472
    invoke-virtual {p0, p1, p2, p3}, Landroid/preference/GenericInflater;->onCreateCustomFromTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Object;Landroid/util/AttributeSet;)Z

    #@1a
    move-result v4

    #@1b
    if-nez v4, :cond_4

    #@1d
    .line 479
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 481
    .local v2, name:Ljava/lang/String;
    invoke-direct {p0, p1, v2, p3}, Landroid/preference/GenericInflater;->createItemFromTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;

    #@24
    move-result-object v1

    #@25
    .local v1, item:Ljava/lang/Object;,"TT;"
    move-object v4, p2

    #@26
    .line 488
    check-cast v4, Landroid/preference/GenericInflater$Parent;

    #@28
    invoke-interface {v4, v1}, Landroid/preference/GenericInflater$Parent;->addItemFromInflater(Ljava/lang/Object;)V

    #@2b
    .line 493
    invoke-direct {p0, p1, v1, p3}, Landroid/preference/GenericInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Object;Landroid/util/AttributeSet;)V

    #@2e
    goto :goto_4

    #@2f
    .line 499
    .end local v1           #item:Ljava/lang/Object;,"TT;"
    .end local v2           #name:Ljava/lang/String;
    :cond_2f
    return-void
.end method


# virtual methods
.method public abstract cloneInContext(Landroid/content/Context;)Landroid/preference/GenericInflater;
.end method

.method public final createItem(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    .registers 11
    .parameter "name"
    .parameter "prefix"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/util/AttributeSet;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Landroid/view/InflateException;
        }
    .end annotation

    #@0
    .prologue
    .line 369
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    sget-object v5, Landroid/preference/GenericInflater;->sConstructorMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Ljava/lang/reflect/Constructor;

    #@8
    .line 372
    .local v2, constructor:Ljava/lang/reflect/Constructor;
    if-nez v2, :cond_32

    #@a
    .line 375
    :try_start_a
    iget-object v5, p0, Landroid/preference/GenericInflater;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@f
    move-result-object v6

    #@10
    if-eqz p2, :cond_3c

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    :goto_23
    invoke-virtual {v6, v5}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@26
    move-result-object v1

    #@27
    .line 377
    .local v1, clazz:Ljava/lang/Class;
    sget-object v5, Landroid/preference/GenericInflater;->mConstructorSignature:[Ljava/lang/Class;

    #@29
    invoke-virtual {v1, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@2c
    move-result-object v2

    #@2d
    .line 378
    sget-object v5, Landroid/preference/GenericInflater;->sConstructorMap:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v5, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    .line 381
    .end local v1           #clazz:Ljava/lang/Class;
    :cond_32
    iget-object v0, p0, Landroid/preference/GenericInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@34
    .line 382
    .local v0, args:[Ljava/lang/Object;
    const/4 v5, 0x1

    #@35
    aput-object p3, v0, v5

    #@37
    .line 383
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_a .. :try_end_3a} :catch_3e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_a .. :try_end_3a} :catch_76
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_3a} :catch_78

    #@3a
    move-result-object v5

    #@3b
    return-object v5

    #@3c
    .end local v0           #args:[Ljava/lang/Object;
    :cond_3c
    move-object v5, p1

    #@3d
    .line 375
    goto :goto_23

    #@3e
    .line 385
    :catch_3e
    move-exception v3

    #@3f
    .line 386
    .local v3, e:Ljava/lang/NoSuchMethodException;
    new-instance v4, Landroid/view/InflateException;

    #@41
    new-instance v5, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    const-string v6, ": Error inflating class "

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    if-eqz p2, :cond_67

    #@56
    new-instance v6, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object p1

    #@67
    .end local p1
    :cond_67
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v5

    #@6f
    invoke-direct {v4, v5}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@72
    .line 390
    .local v4, ie:Landroid/view/InflateException;
    invoke-virtual {v4, v3}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@75
    .line 391
    throw v4

    #@76
    .line 393
    .end local v3           #e:Ljava/lang/NoSuchMethodException;
    .end local v4           #ie:Landroid/view/InflateException;
    .restart local p1
    :catch_76
    move-exception v3

    #@77
    .line 395
    .local v3, e:Ljava/lang/ClassNotFoundException;
    throw v3

    #@78
    .line 396
    .end local v3           #e:Ljava/lang/ClassNotFoundException;
    :catch_78
    move-exception v3

    #@79
    .line 397
    .local v3, e:Ljava/lang/Exception;
    new-instance v4, Landroid/view/InflateException;

    #@7b
    new-instance v5, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v5

    #@88
    const-string v6, ": Error inflating class "

    #@8a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v5

    #@8e
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v5

    #@9a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v5

    #@9e
    invoke-direct {v4, v5}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@a1
    .line 401
    .restart local v4       #ie:Landroid/view/InflateException;
    invoke-virtual {v4, v3}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@a4
    .line 402
    throw v4
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 165
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    iget-object v0, p0, Landroid/preference/GenericInflater;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getDefaultPackage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 157
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    iget-object v0, p0, Landroid/preference/GenericInflater;->mDefaultPackage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getFactory()Landroid/preference/GenericInflater$Factory;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/preference/GenericInflater$Factory",
            "<TT;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 174
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    iget-object v0, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@2
    return-object v0
.end method

.method public inflate(ILandroid/preference/GenericInflater$Parent;)Ljava/lang/Object;
    .registers 4
    .parameter "resource"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITP;)TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 220
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p2, root:Landroid/preference/GenericInflater$Parent;,"TP;"
    if-eqz p2, :cond_8

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Landroid/preference/GenericInflater;->inflate(ILandroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    return-object v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method public inflate(ILandroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;
    .registers 6
    .parameter "resource"
    .parameter
    .parameter "attachToRoot"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITP;Z)TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 261
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p2, root:Landroid/preference/GenericInflater$Parent;,"TP;"
    invoke-virtual {p0}, Landroid/preference/GenericInflater;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    #@b
    move-result-object v0

    #@c
    .line 263
    .local v0, parser:Landroid/content/res/XmlResourceParser;
    :try_start_c
    invoke-virtual {p0, v0, p2, p3}, Landroid/preference/GenericInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_14

    #@f
    move-result-object v1

    #@10
    .line 265
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    #@13
    return-object v1

    #@14
    :catchall_14
    move-exception v1

    #@15
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    #@18
    throw v1
.end method

.method public inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/preference/GenericInflater$Parent;)Ljava/lang/Object;
    .registers 4
    .parameter "parser"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "TP;)TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 240
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p2, root:Landroid/preference/GenericInflater$Parent;,"TP;"
    if-eqz p2, :cond_8

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Landroid/preference/GenericInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    return-object v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method public inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;
    .registers 15
    .parameter "parser"
    .parameter
    .parameter "attachToRoot"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "TP;Z)TT;"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p2, root:Landroid/preference/GenericInflater$Parent;,"TP;"
    const/4 v10, 0x2

    #@1
    .line 292
    iget-object v7, p0, Landroid/preference/GenericInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@3
    monitor-enter v7

    #@4
    .line 293
    :try_start_4
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@7
    move-result-object v0

    #@8
    .line 294
    .local v0, attrs:Landroid/util/AttributeSet;
    iget-object v6, p0, Landroid/preference/GenericInflater;->mConstructorArgs:[Ljava/lang/Object;

    #@a
    const/4 v8, 0x0

    #@b
    iget-object v9, p0, Landroid/preference/GenericInflater;->mContext:Landroid/content/Context;

    #@d
    aput-object v9, v6, v8
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_3a

    #@f
    .line 295
    move-object v3, p2

    #@10
    .line 301
    .local v3, result:Landroid/preference/GenericInflater$Parent;,"TT;"
    :cond_10
    :try_start_10
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@13
    move-result v4

    #@14
    .local v4, type:I
    if-eq v4, v10, :cond_19

    #@16
    const/4 v6, 0x1

    #@17
    if-ne v4, v6, :cond_10

    #@19
    .line 305
    :cond_19
    if-eq v4, v10, :cond_3d

    #@1b
    .line 306
    new-instance v6, Landroid/view/InflateException;

    #@1d
    new-instance v8, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@25
    move-result-object v9

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v8

    #@2a
    const-string v9, ": No start tag found!"

    #@2c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    invoke-direct {v6, v8}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@37
    throw v6
    :try_end_38
    .catchall {:try_start_10 .. :try_end_38} :catchall_3a
    .catch Landroid/view/InflateException; {:try_start_10 .. :try_end_38} :catch_38
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10 .. :try_end_38} :catch_50
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_38} :catch_5e

    #@38
    .line 331
    .end local v4           #type:I
    :catch_38
    move-exception v1

    #@39
    .line 332
    .local v1, e:Landroid/view/InflateException;
    :try_start_39
    throw v1

    #@3a
    .line 347
    .end local v0           #attrs:Landroid/util/AttributeSet;
    .end local v1           #e:Landroid/view/InflateException;
    .end local v3           #result:Landroid/preference/GenericInflater$Parent;,"TT;"
    :catchall_3a
    move-exception v6

    #@3b
    monitor-exit v7
    :try_end_3c
    .catchall {:try_start_39 .. :try_end_3c} :catchall_3a

    #@3c
    throw v6

    #@3d
    .line 317
    .restart local v0       #attrs:Landroid/util/AttributeSet;
    .restart local v3       #result:Landroid/preference/GenericInflater$Parent;,"TT;"
    .restart local v4       #type:I
    :cond_3d
    :try_start_3d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-direct {p0, p1, v6, v0}, Landroid/preference/GenericInflater;->createItemFromTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;

    #@44
    move-result-object v5

    #@45
    .line 320
    .local v5, xmlRoot:Ljava/lang/Object;,"TT;"
    check-cast v5, Landroid/preference/GenericInflater$Parent;

    #@47
    .end local v5           #xmlRoot:Ljava/lang/Object;,"TT;"
    invoke-virtual {p0, p2, p3, v5}, Landroid/preference/GenericInflater;->onMergeRoots(Landroid/preference/GenericInflater$Parent;ZLandroid/preference/GenericInflater$Parent;)Landroid/preference/GenericInflater$Parent;

    #@4a
    move-result-object v3

    #@4b
    .line 326
    invoke-direct {p0, p1, v3, v0}, Landroid/preference/GenericInflater;->rInflate(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Object;Landroid/util/AttributeSet;)V
    :try_end_4e
    .catchall {:try_start_3d .. :try_end_4e} :catchall_3a
    .catch Landroid/view/InflateException; {:try_start_3d .. :try_end_4e} :catch_38
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3d .. :try_end_4e} :catch_50
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_4e} :catch_5e

    #@4e
    .line 346
    :try_start_4e
    monitor-exit v7

    #@4f
    return-object v3

    #@50
    .line 334
    .end local v4           #type:I
    :catch_50
    move-exception v1

    #@51
    .line 335
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v2, Landroid/view/InflateException;

    #@53
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-direct {v2, v6}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@5a
    .line 336
    .local v2, ex:Landroid/view/InflateException;
    invoke-virtual {v2, v1}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@5d
    .line 337
    throw v2

    #@5e
    .line 338
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .end local v2           #ex:Landroid/view/InflateException;
    :catch_5e
    move-exception v1

    #@5f
    .line 339
    .local v1, e:Ljava/io/IOException;
    new-instance v2, Landroid/view/InflateException;

    #@61
    new-instance v6, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@69
    move-result-object v8

    #@6a
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    const-string v8, ": "

    #@70
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    invoke-direct {v2, v6}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    #@83
    .line 342
    .restart local v2       #ex:Landroid/view/InflateException;
    invoke-virtual {v2, v1}, Landroid/view/InflateException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    #@86
    .line 343
    throw v2
    :try_end_87
    .catchall {:try_start_4e .. :try_end_87} :catchall_3a
.end method

.method protected onCreateCustomFromTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/Object;Landroid/util/AttributeSet;)Z
    .registers 5
    .parameter "parser"
    .parameter
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "TT;",
            "Landroid/util/AttributeSet;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    #@0
    .prologue
    .line 514
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p2, parent:Ljava/lang/Object;,"TT;"
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onCreateItem(Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;
    .registers 4
    .parameter "name"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/AttributeSet;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 417
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    iget-object v0, p0, Landroid/preference/GenericInflater;->mDefaultPackage:Ljava/lang/String;

    #@2
    invoke-virtual {p0, p1, v0, p2}, Landroid/preference/GenericInflater;->createItem(Ljava/lang/String;Ljava/lang/String;Landroid/util/AttributeSet;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onMergeRoots(Landroid/preference/GenericInflater$Parent;ZLandroid/preference/GenericInflater$Parent;)Landroid/preference/GenericInflater$Parent;
    .registers 4
    .parameter
    .parameter "attachToGivenRoot"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;ZTP;)TP;"
        }
    .end annotation

    #@0
    .prologue
    .line 518
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p1, givenRoot:Landroid/preference/GenericInflater$Parent;,"TP;"
    .local p3, xmlRoot:Landroid/preference/GenericInflater$Parent;,"TP;"
    return-object p3
.end method

.method public setDefaultPackage(Ljava/lang/String;)V
    .registers 2
    .parameter "defaultPackage"

    #@0
    .prologue
    .line 147
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    iput-object p1, p0, Landroid/preference/GenericInflater;->mDefaultPackage:Ljava/lang/String;

    #@2
    .line 148
    return-void
.end method

.method public setFactory(Landroid/preference/GenericInflater$Factory;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/GenericInflater$Factory",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 192
    .local p0, this:Landroid/preference/GenericInflater;,"Landroid/preference/GenericInflater<TT;TP;>;"
    .local p1, factory:Landroid/preference/GenericInflater$Factory;,"Landroid/preference/GenericInflater$Factory<TT;>;"
    iget-boolean v0, p0, Landroid/preference/GenericInflater;->mFactorySet:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 193
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "A factory has already been set on this inflater"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 196
    :cond_c
    if-nez p1, :cond_16

    #@e
    .line 197
    new-instance v0, Ljava/lang/NullPointerException;

    #@10
    const-string v1, "Given factory can not be null"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 199
    :cond_16
    const/4 v0, 0x1

    #@17
    iput-boolean v0, p0, Landroid/preference/GenericInflater;->mFactorySet:Z

    #@19
    .line 200
    iget-object v0, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@1b
    if-nez v0, :cond_20

    #@1d
    .line 201
    iput-object p1, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@1f
    .line 205
    :goto_1f
    return-void

    #@20
    .line 203
    :cond_20
    new-instance v0, Landroid/preference/GenericInflater$FactoryMerger;

    #@22
    iget-object v1, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@24
    invoke-direct {v0, p1, v1}, Landroid/preference/GenericInflater$FactoryMerger;-><init>(Landroid/preference/GenericInflater$Factory;Landroid/preference/GenericInflater$Factory;)V

    #@27
    iput-object v0, p0, Landroid/preference/GenericInflater;->mFactory:Landroid/preference/GenericInflater$Factory;

    #@29
    goto :goto_1f
.end method
