.class public abstract Landroid/preference/PreferenceGroup;
.super Landroid/preference/Preference;
.source "PreferenceGroup.java"

# interfaces
.implements Landroid/preference/GenericInflater$Parent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/preference/Preference;",
        "Landroid/preference/GenericInflater$Parent",
        "<",
        "Landroid/preference/Preference;",
        ">;"
    }
.end annotation


# instance fields
.field private mAttachedToActivity:Z

.field private mCurrentPreferenceOrder:I

.field private mOrderingAsAdded:Z

.field private mPreferenceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/PreferenceGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 53
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@7
    .line 55
    iput v2, p0, Landroid/preference/PreferenceGroup;->mCurrentPreferenceOrder:I

    #@9
    .line 57
    iput-boolean v2, p0, Landroid/preference/PreferenceGroup;->mAttachedToActivity:Z

    #@b
    .line 62
    new-instance v1, Ljava/util/ArrayList;

    #@d
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v1, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@12
    .line 64
    sget-object v1, Lcom/android/internal/R$styleable;->PreferenceGroup:[I

    #@14
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@17
    move-result-object v0

    #@18
    .line 66
    .local v0, a:Landroid/content/res/TypedArray;
    iget-boolean v1, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@1a
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1d
    move-result v1

    #@1e
    iput-boolean v1, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@20
    .line 68
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@23
    .line 69
    return-void
.end method

.method private removePreferenceInt(Landroid/preference/Preference;)Z
    .registers 3
    .parameter "preference"

    #@0
    .prologue
    .line 187
    monitor-enter p0

    #@1
    .line 188
    :try_start_1
    invoke-virtual {p1}, Landroid/preference/Preference;->onPrepareForRemoval()V

    #@4
    .line 189
    iget-object v0, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@6
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    monitor-exit p0

    #@b
    return v0

    #@c
    .line 190
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method


# virtual methods
.method public addItemFromInflater(Landroid/preference/Preference;)V
    .registers 2
    .parameter "preference"

    #@0
    .prologue
    .line 104
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    #@3
    .line 105
    return-void
.end method

.method public bridge synthetic addItemFromInflater(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    check-cast p1, Landroid/preference/Preference;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroup;->addItemFromInflater(Landroid/preference/Preference;)V

    #@5
    return-void
.end method

.method public addPreference(Landroid/preference/Preference;)Z
    .registers 6
    .parameter "preference"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 133
    iget-object v1, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@3
    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_b

    #@9
    move v1, v2

    #@a
    .line 171
    :goto_a
    return v1

    #@b
    .line 138
    :cond_b
    invoke-virtual {p1}, Landroid/preference/Preference;->getOrder()I

    #@e
    move-result v1

    #@f
    const v3, 0x7fffffff

    #@12
    if-ne v1, v3, :cond_2d

    #@14
    .line 139
    iget-boolean v1, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@16
    if-eqz v1, :cond_21

    #@18
    .line 140
    iget v1, p0, Landroid/preference/PreferenceGroup;->mCurrentPreferenceOrder:I

    #@1a
    add-int/lit8 v3, v1, 0x1

    #@1c
    iput v3, p0, Landroid/preference/PreferenceGroup;->mCurrentPreferenceOrder:I

    #@1e
    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setOrder(I)V

    #@21
    .line 143
    :cond_21
    instance-of v1, p1, Landroid/preference/PreferenceGroup;

    #@23
    if-eqz v1, :cond_2d

    #@25
    move-object v1, p1

    #@26
    .line 146
    check-cast v1, Landroid/preference/PreferenceGroup;

    #@28
    iget-boolean v3, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@2a
    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroup;->setOrderingAsAdded(Z)V

    #@2d
    .line 150
    :cond_2d
    iget-object v1, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@2f
    invoke-static {v1, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@32
    move-result v0

    #@33
    .line 151
    .local v0, insertionIndex:I
    if-gez v0, :cond_39

    #@35
    .line 152
    mul-int/lit8 v1, v0, -0x1

    #@37
    add-int/lit8 v0, v1, -0x1

    #@39
    .line 155
    :cond_39
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroup;->onPrepareAddPreference(Landroid/preference/Preference;)Z

    #@3c
    move-result v1

    #@3d
    if-nez v1, :cond_41

    #@3f
    .line 156
    const/4 v1, 0x0

    #@40
    goto :goto_a

    #@41
    .line 159
    :cond_41
    monitor-enter p0

    #@42
    .line 160
    :try_start_42
    iget-object v1, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@44
    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    #@47
    .line 161
    monitor-exit p0
    :try_end_48
    .catchall {:try_start_42 .. :try_end_48} :catchall_5b

    #@48
    .line 163
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {p1, v1}, Landroid/preference/Preference;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    #@4f
    .line 165
    iget-boolean v1, p0, Landroid/preference/PreferenceGroup;->mAttachedToActivity:Z

    #@51
    if-eqz v1, :cond_56

    #@53
    .line 166
    invoke-virtual {p1}, Landroid/preference/Preference;->onAttachedToActivity()V

    #@56
    .line 169
    :cond_56
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->notifyHierarchyChanged()V

    #@59
    move v1, v2

    #@5a
    .line 171
    goto :goto_a

    #@5b
    .line 161
    :catchall_5b
    move-exception v1

    #@5c
    :try_start_5c
    monitor-exit p0
    :try_end_5d
    .catchall {:try_start_5c .. :try_end_5d} :catchall_5b

    #@5d
    throw v1
.end method

.method protected dispatchRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "container"

    #@0
    .prologue
    .line 322
    invoke-super {p0, p1}, Landroid/preference/Preference;->dispatchRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 325
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    #@6
    move-result v1

    #@7
    .line 326
    .local v1, preferenceCount:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_14

    #@a
    .line 327
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Landroid/preference/Preference;->dispatchRestoreInstanceState(Landroid/os/Bundle;)V

    #@11
    .line 326
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_8

    #@14
    .line 329
    :cond_14
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "container"

    #@0
    .prologue
    .line 311
    invoke-super {p0, p1}, Landroid/preference/Preference;->dispatchSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 314
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    #@6
    move-result v1

    #@7
    .line 315
    .local v1, preferenceCount:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_14

    #@a
    .line 316
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Landroid/preference/Preference;->dispatchSaveInstanceState(Landroid/os/Bundle;)V

    #@11
    .line 315
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_8

    #@14
    .line 318
    :cond_14
    return-void
.end method

.method public findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .registers 8
    .parameter "key"

    #@0
    .prologue
    .line 234
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getKey()Ljava/lang/String;

    #@3
    move-result-object v5

    #@4
    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_b

    #@a
    .line 255
    .end local p0
    :goto_a
    return-object p0

    #@b
    .line 237
    .restart local p0
    :cond_b
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    #@e
    move-result v3

    #@f
    .line 238
    .local v3, preferenceCount:I
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    if-ge v1, v3, :cond_35

    #@12
    .line 239
    invoke-virtual {p0, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    #@15
    move-result-object v2

    #@16
    .line 240
    .local v2, preference:Landroid/preference/Preference;
    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 242
    .local v0, curKey:Ljava/lang/String;
    if-eqz v0, :cond_24

    #@1c
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v5

    #@20
    if-eqz v5, :cond_24

    #@22
    move-object p0, v2

    #@23
    .line 243
    goto :goto_a

    #@24
    .line 246
    :cond_24
    instance-of v5, v2, Landroid/preference/PreferenceGroup;

    #@26
    if-eqz v5, :cond_32

    #@28
    .line 247
    check-cast v2, Landroid/preference/PreferenceGroup;

    #@2a
    .end local v2           #preference:Landroid/preference/Preference;
    invoke-virtual {v2, p1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@2d
    move-result-object v4

    #@2e
    .line 249
    .local v4, returnedPreference:Landroid/preference/Preference;
    if-eqz v4, :cond_32

    #@30
    move-object p0, v4

    #@31
    .line 250
    goto :goto_a

    #@32
    .line 238
    .end local v4           #returnedPreference:Landroid/preference/Preference;
    :cond_32
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_10

    #@35
    .line 255
    .end local v0           #curKey:Ljava/lang/String;
    :cond_35
    const/4 p0, 0x0

    #@36
    goto :goto_a
.end method

.method public getPreference(I)Landroid/preference/Preference;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@2
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/preference/Preference;

    #@8
    return-object v0
.end method

.method public getPreferenceCount()I
    .registers 2

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected isOnSameScreenAsChildren()Z
    .registers 2

    #@0
    .prologue
    .line 266
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isOrderingAsAdded()Z
    .registers 2

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@2
    return v0
.end method

.method protected onAttachedToActivity()V
    .registers 4

    #@0
    .prologue
    .line 271
    invoke-super {p0}, Landroid/preference/Preference;->onAttachedToActivity()V

    #@3
    .line 275
    const/4 v2, 0x1

    #@4
    iput-boolean v2, p0, Landroid/preference/PreferenceGroup;->mAttachedToActivity:Z

    #@6
    .line 278
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    #@9
    move-result v1

    #@a
    .line 279
    .local v1, preferenceCount:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v1, :cond_17

    #@d
    .line 280
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Landroid/preference/Preference;->onAttachedToActivity()V

    #@14
    .line 279
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_b

    #@17
    .line 282
    :cond_17
    return-void
.end method

.method protected onPrepareAddPreference(Landroid/preference/Preference;)Z
    .registers 3
    .parameter "preference"

    #@0
    .prologue
    .line 213
    invoke-super {p0}, Landroid/preference/Preference;->isEnabled()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    .line 214
    const/4 v0, 0x0

    #@7
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    #@a
    .line 217
    :cond_a
    const/4 v0, 0x1

    #@b
    return v0
.end method

.method protected onPrepareForRemoval()V
    .registers 2

    #@0
    .prologue
    .line 286
    invoke-super {p0}, Landroid/preference/Preference;->onPrepareForRemoval()V

    #@3
    .line 289
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/preference/PreferenceGroup;->mAttachedToActivity:Z

    #@6
    .line 290
    return-void
.end method

.method public removeAll()V
    .registers 4

    #@0
    .prologue
    .line 197
    monitor-enter p0

    #@1
    .line 198
    :try_start_1
    iget-object v1, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@3
    .line 199
    .local v1, preferenceList:Ljava/util/List;,"Ljava/util/List<Landroid/preference/Preference;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@6
    move-result v2

    #@7
    add-int/lit8 v0, v2, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_18

    #@b
    .line 200
    const/4 v2, 0x0

    #@c
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/preference/Preference;

    #@12
    invoke-direct {p0, v2}, Landroid/preference/PreferenceGroup;->removePreferenceInt(Landroid/preference/Preference;)Z

    #@15
    .line 199
    add-int/lit8 v0, v0, -0x1

    #@17
    goto :goto_9

    #@18
    .line 202
    :cond_18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1d

    #@19
    .line 203
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->notifyHierarchyChanged()V

    #@1c
    .line 204
    return-void

    #@1d
    .line 202
    .end local v0           #i:I
    .end local v1           #preferenceList:Ljava/util/List;,"Ljava/util/List<Landroid/preference/Preference;>;"
    :catchall_1d
    move-exception v2

    #@1e
    :try_start_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v2
.end method

.method public removePreference(Landroid/preference/Preference;)Z
    .registers 3
    .parameter "preference"

    #@0
    .prologue
    .line 181
    invoke-direct {p0, p1}, Landroid/preference/PreferenceGroup;->removePreferenceInt(Landroid/preference/Preference;)Z

    #@3
    move-result v0

    #@4
    .line 182
    .local v0, returnValue:Z
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->notifyHierarchyChanged()V

    #@7
    .line 183
    return v0
.end method

.method public setEnabled(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@3
    .line 297
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    #@6
    move-result v1

    #@7
    .line 298
    .local v1, preferenceCount:I
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_14

    #@a
    .line 299
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    #@11
    .line 298
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_8

    #@14
    .line 301
    :cond_14
    return-void
.end method

.method public setOrderingAsAdded(Z)V
    .registers 2
    .parameter "orderingAsAdded"

    #@0
    .prologue
    .line 87
    iput-boolean p1, p0, Landroid/preference/PreferenceGroup;->mOrderingAsAdded:Z

    #@2
    .line 88
    return-void
.end method

.method sortPreferences()V
    .registers 2

    #@0
    .prologue
    .line 304
    monitor-enter p0

    #@1
    .line 305
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceGroup;->mPreferenceList:Ljava/util/List;

    #@3
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@6
    .line 306
    monitor-exit p0

    #@7
    .line 307
    return-void

    #@8
    .line 306
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
