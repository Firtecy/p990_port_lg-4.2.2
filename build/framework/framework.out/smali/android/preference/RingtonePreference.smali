.class public Landroid/preference/RingtonePreference;
.super Landroid/preference/Preference;
.source "RingtonePreference.java"

# interfaces
.implements Landroid/preference/PreferenceManager$OnActivityResultListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "RingtonePreference"


# instance fields
.field private mRequestCode:I

.field private mRingtoneType:I

.field private mShowDefault:Z

.field private mShowSilent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 68
    const v0, 0x1010093

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 56
    sget-object v1, Lcom/android/internal/R$styleable;->RingtonePreference:[I

    #@7
    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@a
    move-result-object v0

    #@b
    .line 58
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    #@e
    move-result v1

    #@f
    iput v1, p0, Landroid/preference/RingtonePreference;->mRingtoneType:I

    #@11
    .line 60
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@14
    move-result v1

    #@15
    iput-boolean v1, p0, Landroid/preference/RingtonePreference;->mShowDefault:Z

    #@17
    .line 62
    const/4 v1, 0x2

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1b
    move-result v1

    #@1c
    iput-boolean v1, p0, Landroid/preference/RingtonePreference;->mShowSilent:Z

    #@1e
    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@21
    .line 65
    return-void
.end method


# virtual methods
.method public getRingtoneType()I
    .registers 2

    #@0
    .prologue
    .line 82
    iget v0, p0, Landroid/preference/RingtonePreference;->mRingtoneType:I

    #@2
    return v0
.end method

.method public getShowDefault()Z
    .registers 2

    #@0
    .prologue
    .line 101
    iget-boolean v0, p0, Landroid/preference/RingtonePreference;->mShowDefault:Z

    #@2
    return v0
.end method

.method public getShowSilent()Z
    .registers 2

    #@0
    .prologue
    .line 121
    iget-boolean v0, p0, Landroid/preference/RingtonePreference;->mShowSilent:Z

    #@2
    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .registers 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 232
    iget v1, p0, Landroid/preference/RingtonePreference;->mRequestCode:I

    #@2
    if-ne p1, v1, :cond_22

    #@4
    .line 234
    if-eqz p3, :cond_1d

    #@6
    .line 235
    const-string v1, "android.intent.extra.ringtone.PICKED_URI"

    #@8
    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/net/Uri;

    #@e
    .line 237
    .local v0, uri:Landroid/net/Uri;
    if-eqz v0, :cond_1f

    #@10
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    :goto_14
    invoke-virtual {p0, v1}, Landroid/preference/RingtonePreference;->callChangeListener(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1d

    #@1a
    .line 238
    invoke-virtual {p0, v0}, Landroid/preference/RingtonePreference;->onSaveRingtone(Landroid/net/Uri;)V

    #@1d
    .line 242
    .end local v0           #uri:Landroid/net/Uri;
    :cond_1d
    const/4 v1, 0x1

    #@1e
    .line 245
    :goto_1e
    return v1

    #@1f
    .line 237
    .restart local v0       #uri:Landroid/net/Uri;
    :cond_1f
    const-string v1, ""

    #@21
    goto :goto_14

    #@22
    .line 245
    .end local v0           #uri:Landroid/net/Uri;
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_1e
.end method

.method protected onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .registers 3
    .parameter "preferenceManager"

    #@0
    .prologue
    .line 224
    invoke-super {p0, p1}, Landroid/preference/Preference;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    #@3
    .line 226
    invoke-virtual {p1, p0}, Landroid/preference/PreferenceManager;->registerOnActivityResultListener(Landroid/preference/PreferenceManager$OnActivityResultListener;)V

    #@6
    .line 227
    invoke-virtual {p1}, Landroid/preference/PreferenceManager;->getNextRequestCode()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/preference/RingtonePreference;->mRequestCode:I

    #@c
    .line 228
    return-void
.end method

.method protected onClick()V
    .registers 5

    #@0
    .prologue
    .line 137
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v2, "android.intent.action.RINGTONE_PICKER"

    #@4
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 138
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/preference/RingtonePreference;->onPrepareRingtonePickerIntent(Landroid/content/Intent;)V

    #@a
    .line 139
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getFragment()Landroid/preference/PreferenceFragment;

    #@11
    move-result-object v1

    #@12
    .line 140
    .local v1, owningFragment:Landroid/preference/PreferenceFragment;
    if-eqz v1, :cond_1a

    #@14
    .line 141
    iget v2, p0, Landroid/preference/RingtonePreference;->mRequestCode:I

    #@16
    invoke-virtual {v1, v0, v2}, Landroid/preference/PreferenceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    #@19
    .line 145
    :goto_19
    return-void

    #@1a
    .line 143
    :cond_1a
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Landroid/preference/PreferenceManager;->getActivity()Landroid/app/Activity;

    #@21
    move-result-object v2

    #@22
    iget v3, p0, Landroid/preference/RingtonePreference;->mRequestCode:I

    #@24
    invoke-virtual {v2, v0, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    #@27
    goto :goto_19
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .registers 4
    .parameter "a"
    .parameter "index"

    #@0
    .prologue
    .line 198
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected onPrepareRingtonePickerIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "ringtonePickerIntent"

    #@0
    .prologue
    .line 156
    const-string v0, "android.intent.extra.ringtone.EXISTING_URI"

    #@2
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->onRestoreRingtone()Landroid/net/Uri;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@9
    .line 159
    const-string v0, "android.intent.extra.ringtone.SHOW_DEFAULT"

    #@b
    iget-boolean v1, p0, Landroid/preference/RingtonePreference;->mShowDefault:Z

    #@d
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@10
    .line 160
    iget-boolean v0, p0, Landroid/preference/RingtonePreference;->mShowDefault:Z

    #@12
    if-eqz v0, :cond_21

    #@14
    .line 161
    const-string v0, "android.intent.extra.ringtone.DEFAULT_URI"

    #@16
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getRingtoneType()I

    #@19
    move-result v1

    #@1a
    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@21
    .line 165
    :cond_21
    const-string v0, "android.intent.extra.ringtone.SHOW_SILENT"

    #@23
    iget-boolean v1, p0, Landroid/preference/RingtonePreference;->mShowSilent:Z

    #@25
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@28
    .line 166
    const-string v0, "android.intent.extra.ringtone.TYPE"

    #@2a
    iget v1, p0, Landroid/preference/RingtonePreference;->mRingtoneType:I

    #@2c
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2f
    .line 167
    const-string v0, "android.intent.extra.ringtone.TITLE"

    #@31
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getTitle()Ljava/lang/CharSequence;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    #@38
    .line 168
    return-void
.end method

.method protected onRestoreRingtone()Landroid/net/Uri;
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 192
    invoke-virtual {p0, v1}, Landroid/preference/RingtonePreference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 193
    .local v0, uriString:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_f

    #@b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@e
    move-result-object v1

    #@f
    :cond_f
    return-object v1
.end method

.method protected onSaveRingtone(Landroid/net/Uri;)V
    .registers 3
    .parameter "ringtoneUri"

    #@0
    .prologue
    .line 179
    if-eqz p1, :cond_a

    #@2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    :goto_6
    invoke-virtual {p0, v0}, Landroid/preference/RingtonePreference;->persistString(Ljava/lang/String;)Z

    #@9
    .line 180
    return-void

    #@a
    .line 179
    :cond_a
    const-string v0, ""

    #@c
    goto :goto_6
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .registers 5
    .parameter "restorePersistedValue"
    .parameter "defaultValueObj"

    #@0
    .prologue
    .line 203
    move-object v0, p2

    #@1
    check-cast v0, Ljava/lang/String;

    #@3
    .line 212
    .local v0, defaultValue:Ljava/lang/String;
    if-eqz p1, :cond_6

    #@5
    .line 220
    :cond_5
    :goto_5
    return-void

    #@6
    .line 217
    :cond_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_5

    #@c
    .line 218
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {p0, v1}, Landroid/preference/RingtonePreference;->onSaveRingtone(Landroid/net/Uri;)V

    #@13
    goto :goto_5
.end method

.method public setRingtoneType(I)V
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 92
    iput p1, p0, Landroid/preference/RingtonePreference;->mRingtoneType:I

    #@2
    .line 93
    return-void
.end method

.method public setShowDefault(Z)V
    .registers 2
    .parameter "showDefault"

    #@0
    .prologue
    .line 112
    iput-boolean p1, p0, Landroid/preference/RingtonePreference;->mShowDefault:Z

    #@2
    .line 113
    return-void
.end method

.method public setShowSilent(Z)V
    .registers 2
    .parameter "showSilent"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Landroid/preference/RingtonePreference;->mShowSilent:Z

    #@2
    .line 132
    return-void
.end method
