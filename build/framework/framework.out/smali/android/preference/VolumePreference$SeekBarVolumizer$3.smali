.class Landroid/preference/VolumePreference$SeekBarVolumizer$3;
.super Ljava/lang/Object;
.source "VolumePreference.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/preference/VolumePreference$SeekBarVolumizer;->revertVolume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;


# direct methods
.method constructor <init>(Landroid/preference/VolumePreference$SeekBarVolumizer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 403
    iput-object p1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 406
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@2
    invoke-static {v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x2

    #@7
    if-ne v0, v1, :cond_18

    #@9
    .line 407
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@b
    invoke-static {v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@e
    move-result-object v0

    #@f
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@11
    iget-object v1, v1, Landroid/preference/VolumePreference$SeekBarVolumizer;->this$0:Landroid/preference/VolumePreference;

    #@13
    iget v1, v1, Landroid/preference/VolumePreference;->mOriginalRingerMode:I

    #@15
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    #@18
    .line 411
    :cond_18
    iget-object v0, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@1a
    invoke-static {v0}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$200(Landroid/preference/VolumePreference$SeekBarVolumizer;)Landroid/media/AudioManager;

    #@1d
    move-result-object v0

    #@1e
    iget-object v1, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@20
    invoke-static {v1}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$100(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@23
    move-result v1

    #@24
    iget-object v2, p0, Landroid/preference/VolumePreference$SeekBarVolumizer$3;->this$1:Landroid/preference/VolumePreference$SeekBarVolumizer;

    #@26
    invoke-static {v2}, Landroid/preference/VolumePreference$SeekBarVolumizer;->access$400(Landroid/preference/VolumePreference$SeekBarVolumizer;)I

    #@29
    move-result v2

    #@2a
    const/16 v3, 0x80

    #@2c
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@2f
    .line 413
    return-void
.end method
