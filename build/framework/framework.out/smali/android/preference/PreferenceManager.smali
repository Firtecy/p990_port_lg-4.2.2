.class public Landroid/preference/PreferenceManager;
.super Ljava/lang/Object;
.source "PreferenceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/PreferenceManager$OnActivityDestroyListener;,
        Landroid/preference/PreferenceManager$OnActivityStopListener;,
        Landroid/preference/PreferenceManager$OnActivityResultListener;,
        Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;
    }
.end annotation


# static fields
.field public static final KEY_HAS_SET_DEFAULT_VALUES:Ljava/lang/String; = "_has_set_default_values"

.field public static final METADATA_KEY_PREFERENCES:Ljava/lang/String; = "android.preference"

.field private static final TAG:Ljava/lang/String; = "PreferenceManager"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mActivityDestroyListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceManager$OnActivityDestroyListener;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityResultListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceManager$OnActivityResultListener;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityStopListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceManager$OnActivityStopListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mFragment:Landroid/preference/PreferenceFragment;

.field private mNextId:J

.field private mNextRequestCode:I

.field private mNoCommit:Z

.field private mOnPreferenceTreeClickListener:Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;

.field private mPreferenceScreen:Landroid/preference/PreferenceScreen;

.field private mPreferencesScreens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/DialogInterface;",
            ">;"
        }
    .end annotation
.end field

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mSharedPreferencesMode:I

.field private mSharedPreferencesName:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/app/Activity;I)V
    .registers 5
    .parameter "activity"
    .parameter "firstRequestCode"

    #@0
    .prologue
    .line 141
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    const-wide/16 v0, 0x0

    #@5
    iput-wide v0, p0, Landroid/preference/PreferenceManager;->mNextId:J

    #@7
    .line 142
    iput-object p1, p0, Landroid/preference/PreferenceManager;->mActivity:Landroid/app/Activity;

    #@9
    .line 143
    iput p2, p0, Landroid/preference/PreferenceManager;->mNextRequestCode:I

    #@b
    .line 145
    invoke-direct {p0, p1}, Landroid/preference/PreferenceManager;->init(Landroid/content/Context;)V

    #@e
    .line 146
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 156
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    const-wide/16 v0, 0x0

    #@5
    iput-wide v0, p0, Landroid/preference/PreferenceManager;->mNextId:J

    #@7
    .line 157
    invoke-direct {p0, p1}, Landroid/preference/PreferenceManager;->init(Landroid/content/Context;)V

    #@a
    .line 158
    return-void
.end method

.method private dismissAllScreens()V
    .registers 4

    #@0
    .prologue
    .line 767
    monitor-enter p0

    #@1
    .line 769
    :try_start_1
    iget-object v2, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@3
    if-nez v2, :cond_7

    #@5
    .line 770
    monitor-exit p0

    #@6
    .line 780
    :cond_6
    return-void

    #@7
    .line 773
    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    #@9
    iget-object v2, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@b
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@e
    .line 774
    .local v1, screensToDismiss:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/DialogInterface;>;"
    iget-object v2, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@10
    invoke-interface {v2}, Ljava/util/List;->clear()V

    #@13
    .line 775
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_28

    #@14
    .line 777
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v2

    #@18
    add-int/lit8 v0, v2, -0x1

    #@1a
    .local v0, i:I
    :goto_1a
    if-ltz v0, :cond_6

    #@1c
    .line 778
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Landroid/content/DialogInterface;

    #@22
    invoke-interface {v2}, Landroid/content/DialogInterface;->dismiss()V

    #@25
    .line 777
    add-int/lit8 v0, v0, -0x1

    #@27
    goto :goto_1a

    #@28
    .line 775
    .end local v0           #i:I
    .end local v1           #screensToDismiss:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/DialogInterface;>;"
    :catchall_28
    move-exception v2

    #@29
    :try_start_29
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    #@2a
    throw v2
.end method

.method public static getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 366
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferencesName(Landroid/content/Context;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferencesMode()I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private static getDefaultSharedPreferencesMode()I
    .registers 1

    #@0
    .prologue
    .line 375
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method private static getDefaultSharedPreferencesName(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 371
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "_preferences"

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 161
    iput-object p1, p0, Landroid/preference/PreferenceManager;->mContext:Landroid/content/Context;

    #@2
    .line 163
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferencesName(Landroid/content/Context;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    #@9
    .line 164
    return-void
.end method

.method private queryIntentActivities(Landroid/content/Intent;)Ljava/util/List;
    .registers 4
    .parameter "queryIntent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v0

    #@6
    const/16 v1, 0x80

    #@8
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public static setDefaultValues(Landroid/content/Context;IZ)V
    .registers 5
    .parameter "context"
    .parameter "resId"
    .parameter "readAgain"

    #@0
    .prologue
    .line 441
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferencesName(Landroid/content/Context;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferencesMode()I

    #@7
    move-result v1

    #@8
    invoke-static {p0, v0, v1, p1, p2}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V

    #@b
    .line 443
    return-void
.end method

.method public static setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V
    .registers 12
    .parameter "context"
    .parameter "sharedPreferencesName"
    .parameter "sharedPreferencesMode"
    .parameter "resId"
    .parameter "readAgain"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 475
    const-string v4, "_has_set_default_values"

    #@3
    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@6
    move-result-object v0

    #@7
    .line 478
    .local v0, defaultValueSp:Landroid/content/SharedPreferences;
    if-nez p4, :cond_11

    #@9
    const-string v4, "_has_set_default_values"

    #@b
    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_2e

    #@11
    .line 479
    :cond_11
    new-instance v2, Landroid/preference/PreferenceManager;

    #@13
    invoke-direct {v2, p0}, Landroid/preference/PreferenceManager;-><init>(Landroid/content/Context;)V

    #@16
    .line 480
    .local v2, pm:Landroid/preference/PreferenceManager;
    invoke-virtual {v2, p1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    #@19
    .line 481
    invoke-virtual {v2, p2}, Landroid/preference/PreferenceManager;->setSharedPreferencesMode(I)V

    #@1c
    .line 482
    const/4 v4, 0x0

    #@1d
    invoke-virtual {v2, p0, p3, v4}, Landroid/preference/PreferenceManager;->inflateFromResource(Landroid/content/Context;ILandroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    #@20
    .line 484
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@23
    move-result-object v4

    #@24
    const-string v5, "_has_set_default_values"

    #@26
    const/4 v6, 0x1

    #@27
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@2a
    move-result-object v1

    #@2b
    .line 487
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    :try_start_2b
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_2e
    .catch Ljava/lang/AbstractMethodError; {:try_start_2b .. :try_end_2e} :catch_2f

    #@2e
    .line 495
    .end local v1           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v2           #pm:Landroid/preference/PreferenceManager;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 488
    .restart local v1       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v2       #pm:Landroid/preference/PreferenceManager;
    :catch_2f
    move-exception v3

    #@30
    .line 492
    .local v3, unused:Ljava/lang/AbstractMethodError;
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@33
    goto :goto_2e
.end method

.method private setNoCommit(Z)V
    .registers 4
    .parameter "noCommit"

    #@0
    .prologue
    .line 530
    if-nez p1, :cond_b

    #@2
    iget-object v1, p0, Landroid/preference/PreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 532
    :try_start_6
    iget-object v1, p0, Landroid/preference/PreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    #@8
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_b
    .catch Ljava/lang/AbstractMethodError; {:try_start_6 .. :try_end_b} :catch_e

    #@b
    .line 540
    :cond_b
    :goto_b
    iput-boolean p1, p0, Landroid/preference/PreferenceManager;->mNoCommit:Z

    #@d
    .line 541
    return-void

    #@e
    .line 533
    :catch_e
    move-exception v0

    #@f
    .line 537
    .local v0, unused:Ljava/lang/AbstractMethodError;
    iget-object v1, p0, Landroid/preference/PreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    #@11
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@14
    goto :goto_b
.end method


# virtual methods
.method addPreferencesScreen(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "screen"

    #@0
    .prologue
    .line 733
    monitor-enter p0

    #@1
    .line 735
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 736
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@c
    .line 739
    :cond_c
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@e
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@11
    .line 740
    monitor-exit p0

    #@12
    .line 741
    return-void

    #@13
    .line 740
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 279
    new-instance v0, Landroid/preference/PreferenceScreen;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p1, v1}, Landroid/preference/PreferenceScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@6
    .line 280
    .local v0, preferenceScreen:Landroid/preference/PreferenceScreen;
    invoke-virtual {v0, p0}, Landroid/preference/PreferenceScreen;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    #@9
    .line 281
    return-object v0
.end method

.method dispatchActivityDestroy()V
    .registers 6

    #@0
    .prologue
    .line 700
    const/4 v2, 0x0

    #@1
    .line 702
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityDestroyListener;>;"
    monitor-enter p0

    #@2
    .line 703
    :try_start_2
    iget-object v4, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@4
    if-eqz v4, :cond_e

    #@6
    .line 704
    new-instance v3, Ljava/util/ArrayList;

    #@8
    iget-object v4, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@a
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@d
    .end local v2           #list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityDestroyListener;>;"
    .local v3, list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityDestroyListener;>;"
    move-object v2, v3

    #@e
    .line 706
    .end local v3           #list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityDestroyListener;>;"
    .restart local v2       #list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityDestroyListener;>;"
    :cond_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_24

    #@f
    .line 708
    if-eqz v2, :cond_27

    #@11
    .line 709
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@14
    move-result v0

    #@15
    .line 710
    .local v0, N:I
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    if-ge v1, v0, :cond_27

    #@18
    .line 711
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v4

    #@1c
    check-cast v4, Landroid/preference/PreferenceManager$OnActivityDestroyListener;

    #@1e
    invoke-interface {v4}, Landroid/preference/PreferenceManager$OnActivityDestroyListener;->onActivityDestroy()V

    #@21
    .line 710
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_16

    #@24
    .line 706
    .end local v0           #N:I
    .end local v1           #i:I
    :catchall_24
    move-exception v4

    #@25
    :try_start_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v4

    #@27
    .line 716
    :cond_27
    invoke-direct {p0}, Landroid/preference/PreferenceManager;->dismissAllScreens()V

    #@2a
    .line 717
    return-void
.end method

.method dispatchActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 604
    monitor-enter p0

    #@1
    .line 605
    :try_start_1
    iget-object v3, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@3
    if-nez v3, :cond_7

    #@5
    monitor-exit p0

    #@6
    .line 615
    :cond_6
    return-void

    #@7
    .line 606
    :cond_7
    new-instance v2, Ljava/util/ArrayList;

    #@9
    iget-object v3, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@b
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@e
    .line 607
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityResultListener;>;"
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_25

    #@f
    .line 609
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v0

    #@13
    .line 610
    .local v0, N:I
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    if-ge v1, v0, :cond_6

    #@16
    .line 611
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Landroid/preference/PreferenceManager$OnActivityResultListener;

    #@1c
    invoke-interface {v3, p1, p2, p3}, Landroid/preference/PreferenceManager$OnActivityResultListener;->onActivityResult(IILandroid/content/Intent;)Z

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_6

    #@22
    .line 610
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_14

    #@25
    .line 607
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityResultListener;>;"
    :catchall_25
    move-exception v3

    #@26
    :try_start_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    #@27
    throw v3
.end method

.method dispatchActivityStop()V
    .registers 5

    #@0
    .prologue
    .line 654
    monitor-enter p0

    #@1
    .line 655
    :try_start_1
    iget-object v3, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@3
    if-nez v3, :cond_7

    #@5
    monitor-exit p0

    #@6
    .line 663
    :cond_6
    return-void

    #@7
    .line 656
    :cond_7
    new-instance v2, Ljava/util/ArrayList;

    #@9
    iget-object v3, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@b
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@e
    .line 657
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityStopListener;>;"
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_22

    #@f
    .line 659
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v0

    #@13
    .line 660
    .local v0, N:I
    const/4 v1, 0x0

    #@14
    .local v1, i:I
    :goto_14
    if-ge v1, v0, :cond_6

    #@16
    .line 661
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    check-cast v3, Landroid/preference/PreferenceManager$OnActivityStopListener;

    #@1c
    invoke-interface {v3}, Landroid/preference/PreferenceManager$OnActivityStopListener;->onActivityStop()V

    #@1f
    .line 660
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_14

    #@22
    .line 657
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #list:Ljava/util/List;,"Ljava/util/List<Landroid/preference/PreferenceManager$OnActivityStopListener;>;"
    :catchall_22
    move-exception v3

    #@23
    :try_start_23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    throw v3
.end method

.method dispatchNewIntent(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 760
    invoke-direct {p0}, Landroid/preference/PreferenceManager;->dismissAllScreens()V

    #@3
    .line 761
    return-void
.end method

.method public findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 410
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 411
    const/4 v0, 0x0

    #@5
    .line 414
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    #@8
    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method getActivity()Landroid/app/Activity;
    .registers 2

    #@0
    .prologue
    .line 555
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivity:Landroid/app/Activity;

    #@2
    return-object v0
.end method

.method getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 565
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method getEditor()Landroid/content/SharedPreferences$Editor;
    .registers 2

    #@0
    .prologue
    .line 507
    iget-boolean v0, p0, Landroid/preference/PreferenceManager;->mNoCommit:Z

    #@2
    if-eqz v0, :cond_15

    #@4
    .line 508
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    #@6
    if-nez v0, :cond_12

    #@8
    .line 509
    invoke-virtual {p0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@b
    move-result-object v0

    #@c
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    #@12
    .line 512
    :cond_12
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    #@14
    .line 514
    :goto_14
    return-object v0

    #@15
    :cond_15
    invoke-virtual {p0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@18
    move-result-object v0

    #@19
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@1c
    move-result-object v0

    #@1d
    goto :goto_14
.end method

.method getFragment()Landroid/preference/PreferenceFragment;
    .registers 2

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mFragment:Landroid/preference/PreferenceFragment;

    #@2
    return-object v0
.end method

.method getNextId()J
    .registers 5

    #@0
    .prologue
    .line 290
    monitor-enter p0

    #@1
    .line 291
    :try_start_1
    iget-wide v0, p0, Landroid/preference/PreferenceManager;->mNextId:J

    #@3
    const-wide/16 v2, 0x1

    #@5
    add-long/2addr v2, v0

    #@6
    iput-wide v2, p0, Landroid/preference/PreferenceManager;->mNextId:J

    #@8
    monitor-exit p0

    #@9
    return-wide v0

    #@a
    .line 292
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method getNextRequestCode()I
    .registers 3

    #@0
    .prologue
    .line 727
    monitor-enter p0

    #@1
    .line 728
    :try_start_1
    iget v0, p0, Landroid/preference/PreferenceManager;->mNextRequestCode:I

    #@3
    add-int/lit8 v1, v0, 0x1

    #@5
    iput v1, p0, Landroid/preference/PreferenceManager;->mNextRequestCode:I

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    .line 729
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method getOnPreferenceTreeClickListener()Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;
    .registers 2

    #@0
    .prologue
    .line 793
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mOnPreferenceTreeClickListener:Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;

    #@2
    return-object v0
.end method

.method getPreferenceScreen()Landroid/preference/PreferenceScreen;
    .registers 2

    #@0
    .prologue
    .line 384
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    #@2
    return-object v0
.end method

.method public getSharedPreferences()Landroid/content/SharedPreferences;
    .registers 4

    #@0
    .prologue
    .line 349
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 350
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Landroid/preference/PreferenceManager;->mSharedPreferencesName:Ljava/lang/String;

    #@8
    iget v2, p0, Landroid/preference/PreferenceManager;->mSharedPreferencesMode:I

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    #@10
    .line 354
    :cond_10
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    #@12
    return-object v0
.end method

.method public getSharedPreferencesMode()I
    .registers 2

    #@0
    .prologue
    .line 326
    iget v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferencesMode:I

    #@2
    return v0
.end method

.method public getSharedPreferencesName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 303
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferencesName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method inflateFromIntent(Landroid/content/Intent;Landroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .registers 16
    .parameter "queryIntent"
    .parameter "rootPreferences"

    #@0
    .prologue
    .line 209
    invoke-direct {p0, p1}, Landroid/preference/PreferenceManager;->queryIntentActivities(Landroid/content/Intent;)Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    .line 210
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v5, Ljava/util/HashSet;

    #@6
    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    #@9
    .line 212
    .local v5, inflatedRes:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@c
    move-result v10

    #@d
    add-int/lit8 v4, v10, -0x1

    #@f
    .local v4, i:I
    :goto_f
    if-ltz v4, :cond_9f

    #@11
    .line 213
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v10

    #@15
    check-cast v10, Landroid/content/pm/ResolveInfo;

    #@17
    iget-object v1, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@19
    .line 214
    .local v1, activityInfo:Landroid/content/pm/ActivityInfo;
    iget-object v7, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@1b
    .line 216
    .local v7, metaData:Landroid/os/Bundle;
    if-eqz v7, :cond_25

    #@1d
    const-string v10, "android.preference"

    #@1f
    invoke-virtual {v7, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@22
    move-result v10

    #@23
    if-nez v10, :cond_28

    #@25
    .line 212
    :cond_25
    :goto_25
    add-int/lit8 v4, v4, -0x1

    #@27
    goto :goto_f

    #@28
    .line 222
    :cond_28
    new-instance v10, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    iget-object v11, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@2f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v10

    #@33
    const-string v11, ":"

    #@35
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v10

    #@39
    iget-object v11, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@3b
    const-string v12, "android.preference"

    #@3d
    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@40
    move-result v11

    #@41
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v10

    #@45
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    .line 225
    .local v9, uniqueResId:Ljava/lang/String;
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@4c
    move-result v10

    #@4d
    if-nez v10, :cond_25

    #@4f
    .line 226
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@52
    .line 230
    :try_start_52
    iget-object v10, p0, Landroid/preference/PreferenceManager;->mContext:Landroid/content/Context;

    #@54
    iget-object v11, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@56
    const/4 v12, 0x0

    #@57
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_5a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_52 .. :try_end_5a} :catch_75

    #@5a
    move-result-object v2

    #@5b
    .line 237
    .local v2, context:Landroid/content/Context;
    new-instance v6, Landroid/preference/PreferenceInflater;

    #@5d
    invoke-direct {v6, v2, p0}, Landroid/preference/PreferenceInflater;-><init>(Landroid/content/Context;Landroid/preference/PreferenceManager;)V

    #@60
    .line 238
    .local v6, inflater:Landroid/preference/PreferenceInflater;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@63
    move-result-object v10

    #@64
    const-string v11, "android.preference"

    #@66
    invoke-virtual {v1, v10, v11}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@69
    move-result-object v8

    #@6a
    .line 240
    .local v8, parser:Landroid/content/res/XmlResourceParser;
    const/4 v10, 0x1

    #@6b
    invoke-virtual {v6, v8, p2, v10}, Landroid/preference/PreferenceInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;

    #@6e
    move-result-object p2

    #@6f
    .end local p2
    check-cast p2, Landroid/preference/PreferenceScreen;

    #@71
    .line 242
    .restart local p2
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->close()V

    #@74
    goto :goto_25

    #@75
    .line 231
    .end local v2           #context:Landroid/content/Context;
    .end local v6           #inflater:Landroid/preference/PreferenceInflater;
    .end local v8           #parser:Landroid/content/res/XmlResourceParser;
    :catch_75
    move-exception v3

    #@76
    .line 232
    .local v3, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v10, "PreferenceManager"

    #@78
    new-instance v11, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v12, "Could not create context for "

    #@7f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v11

    #@83
    iget-object v12, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@85
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v11

    #@89
    const-string v12, ": "

    #@8b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v11

    #@8f
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@92
    move-result-object v12

    #@93
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v11

    #@97
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v11

    #@9b
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    goto :goto_25

    #@9f
    .line 246
    .end local v1           #activityInfo:Landroid/content/pm/ActivityInfo;
    .end local v3           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v7           #metaData:Landroid/os/Bundle;
    .end local v9           #uniqueResId:Ljava/lang/String;
    :cond_9f
    invoke-virtual {p2, p0}, Landroid/preference/PreferenceScreen;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    #@a2
    .line 248
    return-object p2
.end method

.method public inflateFromResource(Landroid/content/Context;ILandroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;
    .registers 6
    .parameter "context"
    .parameter "resId"
    .parameter "rootPreferences"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 266
    invoke-direct {p0, v1}, Landroid/preference/PreferenceManager;->setNoCommit(Z)V

    #@4
    .line 268
    new-instance v0, Landroid/preference/PreferenceInflater;

    #@6
    invoke-direct {v0, p1, p0}, Landroid/preference/PreferenceInflater;-><init>(Landroid/content/Context;Landroid/preference/PreferenceManager;)V

    #@9
    .line 269
    .local v0, inflater:Landroid/preference/PreferenceInflater;
    invoke-virtual {v0, p2, p3, v1}, Landroid/preference/PreferenceInflater;->inflate(ILandroid/preference/GenericInflater$Parent;Z)Ljava/lang/Object;

    #@c
    move-result-object p3

    #@d
    .end local p3
    check-cast p3, Landroid/preference/PreferenceScreen;

    #@f
    .line 270
    .restart local p3
    invoke-virtual {p3, p0}, Landroid/preference/PreferenceScreen;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    #@12
    .line 273
    const/4 v1, 0x0

    #@13
    invoke-direct {p0, v1}, Landroid/preference/PreferenceManager;->setNoCommit(Z)V

    #@16
    .line 275
    return-object p3
.end method

.method registerOnActivityDestroyListener(Landroid/preference/PreferenceManager$OnActivityDestroyListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 671
    monitor-enter p0

    #@1
    .line 672
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 673
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@c
    .line 676
    :cond_c
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@e
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_19

    #@14
    .line 677
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@16
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@19
    .line 679
    :cond_19
    monitor-exit p0

    #@1a
    .line 680
    return-void

    #@1b
    .line 679
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method registerOnActivityResultListener(Landroid/preference/PreferenceManager$OnActivityResultListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 574
    monitor-enter p0

    #@1
    .line 575
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 576
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@c
    .line 579
    :cond_c
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@e
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_19

    #@14
    .line 580
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@16
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@19
    .line 582
    :cond_19
    monitor-exit p0

    #@1a
    .line 583
    return-void

    #@1b
    .line 582
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method registerOnActivityStopListener(Landroid/preference/PreferenceManager$OnActivityStopListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 623
    monitor-enter p0

    #@1
    .line 624
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 625
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@c
    .line 628
    :cond_c
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@e
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_19

    #@14
    .line 629
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@16
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@19
    .line 631
    :cond_19
    monitor-exit p0

    #@1a
    .line 632
    return-void

    #@1b
    .line 631
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method removePreferencesScreen(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "screen"

    #@0
    .prologue
    .line 744
    monitor-enter p0

    #@1
    .line 746
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@3
    if-nez v0, :cond_7

    #@5
    .line 747
    monitor-exit p0

    #@6
    .line 752
    :goto_6
    return-void

    #@7
    .line 750
    :cond_7
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferencesScreens:Ljava/util/List;

    #@9
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@c
    .line 751
    monitor-exit p0

    #@d
    goto :goto_6

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method setFragment(Landroid/preference/PreferenceFragment;)V
    .registers 2
    .parameter "fragment"

    #@0
    .prologue
    .line 170
    iput-object p1, p0, Landroid/preference/PreferenceManager;->mFragment:Landroid/preference/PreferenceFragment;

    #@2
    .line 171
    return-void
.end method

.method setOnPreferenceTreeClickListener(Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 789
    iput-object p1, p0, Landroid/preference/PreferenceManager;->mOnPreferenceTreeClickListener:Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;

    #@2
    .line 790
    return-void
.end method

.method setPreferences(Landroid/preference/PreferenceScreen;)Z
    .registers 3
    .parameter "preferenceScreen"

    #@0
    .prologue
    .line 394
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    #@2
    if-eq p1, v0, :cond_8

    #@4
    .line 395
    iput-object p1, p0, Landroid/preference/PreferenceManager;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    #@6
    .line 396
    const/4 v0, 0x1

    #@7
    .line 399
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public setSharedPreferencesMode(I)V
    .registers 3
    .parameter "sharedPreferencesMode"

    #@0
    .prologue
    .line 337
    iput p1, p0, Landroid/preference/PreferenceManager;->mSharedPreferencesMode:I

    #@2
    .line 338
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    #@5
    .line 339
    return-void
.end method

.method public setSharedPreferencesName(Ljava/lang/String;)V
    .registers 3
    .parameter "sharedPreferencesName"

    #@0
    .prologue
    .line 314
    iput-object p1, p0, Landroid/preference/PreferenceManager;->mSharedPreferencesName:Ljava/lang/String;

    #@2
    .line 315
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/preference/PreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    #@5
    .line 316
    return-void
.end method

.method shouldCommit()Z
    .registers 2

    #@0
    .prologue
    .line 526
    iget-boolean v0, p0, Landroid/preference/PreferenceManager;->mNoCommit:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method unregisterOnActivityDestroyListener(Landroid/preference/PreferenceManager$OnActivityDestroyListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 688
    monitor-enter p0

    #@1
    .line 689
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 690
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityDestroyListeners:Ljava/util/List;

    #@7
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@a
    .line 692
    :cond_a
    monitor-exit p0

    #@b
    .line 693
    return-void

    #@c
    .line 692
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method unregisterOnActivityResultListener(Landroid/preference/PreferenceManager$OnActivityResultListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 591
    monitor-enter p0

    #@1
    .line 592
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 593
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityResultListeners:Ljava/util/List;

    #@7
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@a
    .line 595
    :cond_a
    monitor-exit p0

    #@b
    .line 596
    return-void

    #@c
    .line 595
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method unregisterOnActivityStopListener(Landroid/preference/PreferenceManager$OnActivityStopListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 640
    monitor-enter p0

    #@1
    .line 641
    :try_start_1
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 642
    iget-object v0, p0, Landroid/preference/PreferenceManager;->mActivityStopListeners:Ljava/util/List;

    #@7
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@a
    .line 644
    :cond_a
    monitor-exit p0

    #@b
    .line 645
    return-void

    #@c
    .line 644
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method
