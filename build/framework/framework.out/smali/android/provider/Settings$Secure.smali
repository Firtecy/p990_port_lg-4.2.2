.class public final Landroid/provider/Settings$Secure;
.super Landroid/provider/Settings$NameValueTable;
.source "Settings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Secure"
.end annotation


# static fields
.field public static final ACCESSIBILITY_COLOR_CONFIG_MODE:Ljava/lang/String; = "accessibility_color_config_mode"

.field public static final ACCESSIBILITY_COLOR_CONTRAST:Ljava/lang/String; = "accessibility_color_contrast"

.field public static final ACCESSIBILITY_COLOR_HUE:Ljava/lang/String; = "accessibility_color_hue"

.field public static final ACCESSIBILITY_COLOR_INTENSITY:Ljava/lang/String; = "accessibility_color_intensity"

.field public static final ACCESSIBILITY_COLOR_INVERT_ENABLED:Ljava/lang/String; = "accessibility_color_invert_enabled"

.field public static final ACCESSIBILITY_COLOR_SAT:Ljava/lang/String; = "accessibility_color_sat"

.field public static final ACCESSIBILITY_DISPLAY_MAGNIFICATION_AUTO_UPDATE:Ljava/lang/String; = "accessibility_display_magnification_auto_update"

.field public static final ACCESSIBILITY_DISPLAY_MAGNIFICATION_ENABLED:Ljava/lang/String; = "accessibility_display_magnification_enabled"

.field public static final ACCESSIBILITY_DISPLAY_MAGNIFICATION_SCALE:Ljava/lang/String; = "accessibility_display_magnification_scale"

.field public static final ACCESSIBILITY_ENABLED:Ljava/lang/String; = "accessibility_enabled"

.field public static final ACCESSIBILITY_SCREEN_READER_URL:Ljava/lang/String; = "accessibility_script_injection_url"

.field public static final ACCESSIBILITY_SCRIPT_INJECTION:Ljava/lang/String; = "accessibility_script_injection"

.field public static final ACCESSIBILITY_SPEAK_PASSWORD:Ljava/lang/String; = "speak_password"

.field public static final ACCESSIBILITY_WEB_CONTENT_KEY_BINDINGS:Ljava/lang/String; = "accessibility_web_content_key_bindings"

.field public static final ADB_BLOCKED:Ljava/lang/String; = "adb_blocked"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ADB_ENABLED:Ljava/lang/String; = "adb_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ADMIN_LOCKED:Ljava/lang/String; = "admin_locked"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ALLOWED_GEOLOCATION_ORIGINS:Ljava/lang/String; = "allowed_geolocation_origins"

.field public static final ALLOW_MOCK_LOCATION:Ljava/lang/String; = "mock_location"

.field public static final ANDROID_ID:Ljava/lang/String; = "android_id"

.field public static final ANR_SHOW_BACKGROUND:Ljava/lang/String; = "anr_show_background"

.field public static final APN2DISABLE_MODE_ON:Ljava/lang/String; = "apn2_disable"

.field public static final APN_LOCKED:Ljava/lang/String; = "apn_locked"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BACKGROUND_DATA:Ljava/lang/String; = "background_data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BACKUP_AUTO_RESTORE:Ljava/lang/String; = "backup_auto_restore"

.field public static final BACKUP_ENABLED:Ljava/lang/String; = "backup_enabled"

.field public static final BACKUP_PROVISIONED:Ljava/lang/String; = "backup_provisioned"

.field public static final BACKUP_TRANSPORT:Ljava/lang/String; = "backup_transport"

.field public static final BLUETOOTH_ON:Ljava/lang/String; = "bluetooth_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BOOT_LOCK:Ljava/lang/String; = "boot_lock"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BUGREPORT_IN_POWER_MENU:Ljava/lang/String; = "bugreport_in_power_menu"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DATA_ACCEPT_INFO:Ljava/lang/String; = "data_accept_info"

.field public static final DATA_EHRPD_INTERNET_IPV6_ENABLED:Ljava/lang/String; = "data_ehrpd_internet_ipv6_enabled"

.field public static final DATA_ENCRYPTION:Ljava/lang/String; = "data_encryption"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATA_ROAMING:Ljava/lang/String; = "data_roaming"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_INPUT_METHOD:Ljava/lang/String; = "default_input_method"

.field public static final DEVELOPMENT_SETTINGS_ENABLED:Ljava/lang/String; = "development_settings_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEVICE_PROVISIONED:Ljava/lang/String; = "device_provisioned"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DISABLED_SYSTEM_INPUT_METHODS:Ljava/lang/String; = "disabled_system_input_methods"

.field public static final EMERGENCY_LOCK_TEXT:Ljava/lang/String; = "emergency_lock"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ENABLED_ACCESSIBILITY_SERVICES:Ljava/lang/String; = "enabled_accessibility_services"

.field public static final ENABLED_INPUT_METHODS:Ljava/lang/String; = "enabled_input_methods"

.field public static final ENABLE_MENU_WITHOUT_USIM:Ljava/lang/String; = "enable_menu_without_usim"

.field public static final ENHANCED_VOICE_PRIVACY_ENABLED:Ljava/lang/String; = "enhanced_voice_privacy_enabled"

.field public static final GUARD_DOM_DATA:Ljava/lang/String; = "rguard_domestic_data"

.field public static final GUARD_DOM_DATA_FORCED:Ljava/lang/String; = "rguard_domestic_data_forced"

.field public static final GUARD_DOM_VOICE:Ljava/lang/String; = "rguard_domestic_voice"

.field public static final GUARD_DOM_VOICE_FORCED:Ljava/lang/String; = "rguard_domestic_voice_forced"

.field public static final GUARD_GSM_DATA:Ljava/lang/String; = "rguard_gsm_data"

.field public static final GUARD_GSM_DATA_FORCED:Ljava/lang/String; = "rguard_gsm_data_forced"

.field public static final GUARD_GSM_OUTGOING_SMS:Ljava/lang/String; = "cbox_gsm_sms"

.field public static final GUARD_GSM_OUTGOING_SMS_FORCED:Ljava/lang/String; = "cbox_gsm_sms_forced"

.field public static final GUARD_GSM_VOICE:Ljava/lang/String; = "rguard_gms_voice"

.field public static final GUARD_GSM_VOICE_FORCED:Ljava/lang/String; = "rguard_gms_voice_forced"

.field public static final GUARD_INTL_DATA:Ljava/lang/String; = "rguard_international_data"

.field public static final GUARD_INTL_DATA_FORCED:Ljava/lang/String; = "rguard_international_data_forced"

.field public static final GUARD_INTL_OUTGOING_SMS:Ljava/lang/String; = "cbox_international_sms"

.field public static final GUARD_INTL_OUTGOING_SMS_FORCED:Ljava/lang/String; = "cbox_international_sms_forced"

.field public static final GUARD_INTL_VOICE:Ljava/lang/String; = "rguard_international_voice"

.field public static final GUARD_INTL_VOICE_FORCED:Ljava/lang/String; = "rguard_international_voice_forced"

.field public static final HTTP_PROXY:Ljava/lang/String; = "http_proxy"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INCALL_POWER_BUTTON_BEHAVIOR:Ljava/lang/String; = "incall_power_button_behavior"

.field public static final INCALL_POWER_BUTTON_BEHAVIOR_DEFAULT:I = 0x1

.field public static final INCALL_POWER_BUTTON_BEHAVIOR_HANGUP:I = 0x2

.field public static final INCALL_POWER_BUTTON_BEHAVIOR_SCREEN_OFF:I = 0x1

.field public static final INPUT_METHODS_SUBTYPE_HISTORY:Ljava/lang/String; = "input_methods_subtype_history"

.field public static final INPUT_METHOD_SELECTOR_VISIBILITY:Ljava/lang/String; = "input_method_selector_visibility"

.field public static final INSTALL_NON_MARKET_APPS:Ljava/lang/String; = "install_non_market_apps"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IRAT_TEST_MODE:Ljava/lang/String; = "irat_test_mode"

.field public static final KT_SHOW_ROAMING_INDEX:Ljava/lang/String; = "kt_roaming_index"

.field public static final KT_SHOW_ROAMING_PREFIX:Ljava/lang/String; = "show_roaming_prefix"

.field public static final KT_SHOW_ROAMING_USER:Ljava/lang/String; = "kt_roaming_user"

.field public static final LAST_SETUP_SHOWN:Ljava/lang/String; = "last_setup_shown"

.field public static final LOCATION_PROVIDERS_ALLOWED:Ljava/lang/String; = "location_providers_allowed"

.field public static final LOCK_BIOMETRIC_WEAK_FLAGS:Ljava/lang/String; = "lock_biometric_weak_flags"

.field public static final LOCK_PATTERN_ENABLED:Ljava/lang/String; = "lock_pattern_autolock"

.field public static final LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED:Ljava/lang/String; = "lock_pattern_tactile_feedback_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCK_PATTERN_VISIBLE:Ljava/lang/String; = "lock_pattern_visible_pattern"

.field public static final LOCK_SCREEN_APPWIDGET_IDS:Ljava/lang/String; = "lock_screen_appwidget_ids"

.field public static final LOCK_SCREEN_FALLBACK_APPWIDGET_ID:Ljava/lang/String; = "lock_screen_fallback_appwidget_id"

.field public static final LOCK_SCREEN_LOCK_AFTER_TIMEOUT:Ljava/lang/String; = "lock_screen_lock_after_timeout"

.field public static final LOCK_SCREEN_OWNER_INFO:Ljava/lang/String; = "lock_screen_owner_info"

.field public static final LOCK_SCREEN_OWNER_INFO_ENABLED:Ljava/lang/String; = "lock_screen_owner_info_enabled"

.field public static final LOCK_SCREEN_STICKY_APPWIDGET:Ljava/lang/String; = "lock_screen_sticky_appwidget"

.field public static final LOGGING_ID:Ljava/lang/String; = "logging_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LONG_PRESS_TIMEOUT:Ljava/lang/String; = "long_press_timeout"

.field public static final MODEM_TEST_MODE:Ljava/lang/String; = "modem_test_mode"

.field public static final MOUNT_PLAY_NOTIFICATION_SND:Ljava/lang/String; = "mount_play_not_snd"

.field public static final MOUNT_UMS_AUTOSTART:Ljava/lang/String; = "mount_ums_autostart"

.field public static final MOUNT_UMS_NOTIFY_ENABLED:Ljava/lang/String; = "mount_ums_notify_enabled"

.field public static final MOUNT_UMS_PROMPT:Ljava/lang/String; = "mount_ums_prompt"

.field private static final MOVED_TO_GLOBAL:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MOVED_TO_LOCK_SETTINGS:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_PREFERENCE:Ljava/lang/String; = "network_preference"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final OEM_RAD_DIALER_POPUP:Ljava/lang/String; = "oem_rad_dialer_popup"

.field public static final OEM_RAD_SETTING:Ljava/lang/String; = "oem_rad_setting"

.field public static final OEM_RAD_TEST:Ljava/lang/String; = "oem_rad_test"

.field public static final OEM_RAD_TEST_RCV_PRFIX:Ljava/lang/String; = "oem_rad_test_rcv_prfix"

.field public static final OLD_UICC_STATE:Ljava/lang/String; = "old_uicc_state"

.field public static final OTA_DELAY:Ljava/lang/String; = "ota_delay"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final OTA_USIM_RUNNING:Ljava/lang/String; = "ota_usim_running"

.field public static final PARENTAL_CONTROL_ENABLED:Ljava/lang/String; = "parental_control_enabled"

.field public static final PARENTAL_CONTROL_LAST_UPDATE:Ljava/lang/String; = "parental_control_last_update"

.field public static final PARENTAL_CONTROL_REDIRECT_URL:Ljava/lang/String; = "parental_control_redirect_url"

.field public static final PREFERRED_TTY_MODE:Ljava/lang/String; = "preferred_tty_mode"

.field public static final REVSETTING_HIDDEN:Ljava/lang/String; = "revsetting_hidden"

.field public static final RMODE_DOM_DATA:Ljava/lang/String; = "roaming_mode_domestic_data"

.field public static final RMODE_DOM_DATA_FORCED:Ljava/lang/String; = "roaming_mode_domestic_data_forced"

.field public static final RMODE_DOM_VOICE:Ljava/lang/String; = "roaming_mode_domestic_voice"

.field public static final RMODE_DOM_VOICE_FORCED:Ljava/lang/String; = "roaming_mode_domestic_voice_forced"

.field public static final RMODE_INTL_DATA:Ljava/lang/String; = "roaming_mode_international_data"

.field public static final RMODE_INTL_DATA_FORCED:Ljava/lang/String; = "roaming_mode_international_data_forced"

.field public static final RMODE_INTL_VOICE:Ljava/lang/String; = "roaming_mode_international_voice"

.field public static final RMODE_INTL_VOICE_FORCED:Ljava/lang/String; = "roaming_mode_international_voice_forced"

.field public static final RMODE_SEL_SYSTEM:Ljava/lang/String; = "roaming_mode_selected_system"

.field public static final ROAMING_DUALCLOCK:Ljava/lang/String; = "roaming_dualclock"

.field public static final SCREENSAVER_ACTIVATE_ON_DOCK:Ljava/lang/String; = "screensaver_activate_on_dock"

.field public static final SCREENSAVER_ACTIVATE_ON_SLEEP:Ljava/lang/String; = "screensaver_activate_on_sleep"

.field public static final SCREENSAVER_COMPONENTS:Ljava/lang/String; = "screensaver_components"

.field public static final SCREENSAVER_DEFAULT_COMPONENT:Ljava/lang/String; = "screensaver_default_component"

.field public static final SCREENSAVER_ENABLED:Ljava/lang/String; = "screensaver_enabled"

.field public static final SCREENSHOT_BLOCKED:Ljava/lang/String; = "screenshot_blocked"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SD_ENCRYPTION:Ljava/lang/String; = "sd_encryption"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SEARCH_GLOBAL_SEARCH_ACTIVITY:Ljava/lang/String; = "search_global_search_activity"

.field public static final SEARCH_MAX_RESULTS_PER_SOURCE:Ljava/lang/String; = "search_max_results_per_source"

.field public static final SEARCH_MAX_RESULTS_TO_DISPLAY:Ljava/lang/String; = "search_max_results_to_display"

.field public static final SEARCH_MAX_SHORTCUTS_RETURNED:Ljava/lang/String; = "search_max_shortcuts_returned"

.field public static final SEARCH_MAX_SOURCE_EVENT_AGE_MILLIS:Ljava/lang/String; = "search_max_source_event_age_millis"

.field public static final SEARCH_MAX_STAT_AGE_MILLIS:Ljava/lang/String; = "search_max_stat_age_millis"

.field public static final SEARCH_MIN_CLICKS_FOR_SOURCE_RANKING:Ljava/lang/String; = "search_min_clicks_for_source_ranking"

.field public static final SEARCH_MIN_IMPRESSIONS_FOR_SOURCE_RANKING:Ljava/lang/String; = "search_min_impressions_for_source_ranking"

.field public static final SEARCH_NUM_PROMOTED_SOURCES:Ljava/lang/String; = "search_num_promoted_sources"

.field public static final SEARCH_PER_SOURCE_CONCURRENT_QUERY_LIMIT:Ljava/lang/String; = "search_per_source_concurrent_query_limit"

.field public static final SEARCH_PREFILL_MILLIS:Ljava/lang/String; = "search_prefill_millis"

.field public static final SEARCH_PROMOTED_SOURCE_DEADLINE_MILLIS:Ljava/lang/String; = "search_promoted_source_deadline_millis"

.field public static final SEARCH_QUERY_THREAD_CORE_POOL_SIZE:Ljava/lang/String; = "search_query_thread_core_pool_size"

.field public static final SEARCH_QUERY_THREAD_MAX_POOL_SIZE:Ljava/lang/String; = "search_query_thread_max_pool_size"

.field public static final SEARCH_SHORTCUT_REFRESH_CORE_POOL_SIZE:Ljava/lang/String; = "search_shortcut_refresh_core_pool_size"

.field public static final SEARCH_SHORTCUT_REFRESH_MAX_POOL_SIZE:Ljava/lang/String; = "search_shortcut_refresh_max_pool_size"

.field public static final SEARCH_SOURCE_TIMEOUT_MILLIS:Ljava/lang/String; = "search_source_timeout_millis"

.field public static final SEARCH_THREAD_KEEPALIVE_SECONDS:Ljava/lang/String; = "search_thread_keepalive_seconds"

.field public static final SEARCH_WEB_RESULTS_OVERRIDE_LIMIT:Ljava/lang/String; = "search_web_results_override_limit"

.field public static final SELECTED_INPUT_METHOD_SUBTYPE:Ljava/lang/String; = "selected_input_method_subtype"

.field public static final SELECTED_SPELL_CHECKER:Ljava/lang/String; = "selected_spell_checker"

.field public static final SELECTED_SPELL_CHECKER_SUBTYPE:Ljava/lang/String; = "selected_spell_checker_subtype"

.field public static final SENDPDNTABLE_ENABLE_SAVE:Ljava/lang/String; = "SENDPDNTABLE_ENABLE_SAVE"

.field public static final SETTINGS_CLASSNAME:Ljava/lang/String; = "settings_classname"

.field public static final SETTINGS_TO_BACKUP:[Ljava/lang/String; = null

.field public static final SIM_ERR_POPUP_MSG:Ljava/lang/String; = "sim_err_popup_msg"

.field public static final SKT_LTE_NW_INDICATOR:Ljava/lang/String; = "skt_lte_network_indicator"

.field public static final SKT_OTA_USIM_DOWNLOAD:Ljava/lang/String; = "skt_ota_usim_download"

.field public static final SKT_ROAMING_DUALCLOCK:Ljava/lang/String; = "skt_roaming_dualclock"

.field public static final SPELL_CHECKER_ENABLED:Ljava/lang/String; = "spell_checker_enabled"

.field public static final SYS_PROP_SETTING_VERSION:Ljava/lang/String; = "sys.settings_secure_version"

.field public static final TETHERING_BLOCKED:Ljava/lang/String; = "tethering_blocked"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOUCH_EXPLORATION_ENABLED:Ljava/lang/String; = "touch_exploration_enabled"

.field public static final TOUCH_EXPLORATION_GRANTED_ACCESSIBILITY_SERVICES:Ljava/lang/String; = "touch_exploration_granted_accessibility_services"

.field public static final TTS_DEFAULT_COUNTRY:Ljava/lang/String; = "tts_default_country"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TTS_DEFAULT_LANG:Ljava/lang/String; = "tts_default_lang"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TTS_DEFAULT_LOCALE:Ljava/lang/String; = "tts_default_locale"

.field public static final TTS_DEFAULT_PITCH:Ljava/lang/String; = "tts_default_pitch"

.field public static final TTS_DEFAULT_RATE:Ljava/lang/String; = "tts_default_rate"

.field public static final TTS_DEFAULT_SYNTH:Ljava/lang/String; = "tts_default_synth"

.field public static final TTS_DEFAULT_VARIANT:Ljava/lang/String; = "tts_default_variant"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TTS_ENABLED_PLUGINS:Ljava/lang/String; = "tts_enabled_plugins"

.field public static final TTS_USE_DEFAULTS:Ljava/lang/String; = "tts_use_defaults"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TTY_MODE_ENABLED:Ljava/lang/String; = "tty_mode_enabled"

.field public static final UI_NIGHT_MODE:Ljava/lang/String; = "ui_night_mode"

.field public static final USB_BLOCKED:Ljava/lang/String; = "usb_blocked"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final USB_MASS_STORAGE_ENABLED:Ljava/lang/String; = "usb_mass_storage_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final USER_SETUP_COMPLETE:Ljava/lang/String; = "user_setup_complete"

.field public static final USE_GOOGLE_MAIL:Ljava/lang/String; = "use_google_mail"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final USIM_PERSO_CONTROL_KEY:Ljava/lang/String; = "usim_perso_control_key"

.field public static final USIM_PERSO_IMSI:Ljava/lang/String; = "usim_perso_imsi"

.field public static final USIM_PERSO_LOCKED:Ljava/lang/String; = "usim_perso_locked"

.field public static final VOICE_RECOGNITION_SERVICE:Ljava/lang/String; = "voice_recognition_service"

.field public static final WIFI_IDLE_MS:Ljava/lang/String; = "wifi_idle_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_MAX_DHCP_RETRY_COUNT:Ljava/lang/String; = "wifi_max_dhcp_retry_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS:Ljava/lang/String; = "wifi_mobile_data_transition_wakelock_timeout_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON:Ljava/lang/String; = "wifi_networks_available_notification_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY:Ljava/lang/String; = "wifi_networks_available_repeat_delay"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NUM_OPEN_NETWORKS_KEPT:Ljava/lang/String; = "wifi_num_open_networks_kept"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_ON:Ljava/lang/String; = "wifi_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE:Ljava/lang/String; = "wifi_watchdog_acceptable_packet_loss_percentage"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_AP_COUNT:Ljava/lang/String; = "wifi_watchdog_ap_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS:Ljava/lang/String; = "wifi_watchdog_background_check_delay_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED:Ljava/lang/String; = "wifi_watchdog_background_check_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS:Ljava/lang/String; = "wifi_watchdog_background_check_timeout_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT:Ljava/lang/String; = "wifi_watchdog_initial_ignored_ping_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_MAX_AP_CHECKS:Ljava/lang/String; = "wifi_watchdog_max_ap_checks"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_ON:Ljava/lang/String; = "wifi_watchdog_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_PING_COUNT:Ljava/lang/String; = "wifi_watchdog_ping_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_PING_DELAY_MS:Ljava/lang/String; = "wifi_watchdog_ping_delay_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_PING_TIMEOUT_MS:Ljava/lang/String; = "wifi_watchdog_ping_timeout_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_WATCH_LIST:Ljava/lang/String; = "wifi_watchdog_watch_list"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static sIsSystemProcess:Z

.field private static sLockSettings:Lcom/android/internal/widget/ILockSettings;

.field private static final sNameValueCache:Landroid/provider/Settings$NameValueCache;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    .line 3100
    const-string v0, "content://settings/secure"

    #@3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@6
    move-result-object v0

    #@7
    sput-object v0, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@9
    .line 3104
    new-instance v0, Landroid/provider/Settings$NameValueCache;

    #@b
    const-string/jumbo v1, "sys.settings_secure_version"

    #@e
    sget-object v2, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@10
    const-string v3, "GET_secure"

    #@12
    const-string v4, "PUT_secure"

    #@14
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/provider/Settings$NameValueCache;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    #@17
    sput-object v0, Landroid/provider/Settings$Secure;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    #@19
    .line 3110
    const/4 v0, 0x0

    #@1a
    sput-object v0, Landroid/provider/Settings$Secure;->sLockSettings:Lcom/android/internal/widget/ILockSettings;

    #@1c
    .line 3116
    new-instance v0, Ljava/util/HashSet;

    #@1e
    invoke-direct {v0, v5}, Ljava/util/HashSet;-><init>(I)V

    #@21
    sput-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_LOCK_SETTINGS:Ljava/util/HashSet;

    #@23
    .line 3117
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_LOCK_SETTINGS:Ljava/util/HashSet;

    #@25
    const-string/jumbo v1, "lock_pattern_autolock"

    #@28
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2b
    .line 3118
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_LOCK_SETTINGS:Ljava/util/HashSet;

    #@2d
    const-string/jumbo v1, "lock_pattern_visible_pattern"

    #@30
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@33
    .line 3119
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_LOCK_SETTINGS:Ljava/util/HashSet;

    #@35
    const-string/jumbo v1, "lock_pattern_tactile_feedback_enabled"

    #@38
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3b
    .line 3121
    new-instance v0, Ljava/util/HashSet;

    #@3d
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@40
    sput-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@42
    .line 3122
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@44
    const-string v1, "adb_enabled"

    #@46
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@49
    .line 3123
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@4b
    const-string v1, "assisted_gps_enabled"

    #@4d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@50
    .line 3124
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@52
    const-string v1, "bluetooth_on"

    #@54
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@57
    .line 3125
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@59
    const-string v1, "cdma_cell_broadcast_sms"

    #@5b
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5e
    .line 3126
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@60
    const-string/jumbo v1, "roaming_settings"

    #@63
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@66
    .line 3127
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@68
    const-string/jumbo v1, "subscription_mode"

    #@6b
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@6e
    .line 3128
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@70
    const-string v1, "data_activity_timeout_mobile"

    #@72
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@75
    .line 3129
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@77
    const-string v1, "data_activity_timeout_wifi"

    #@79
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@7c
    .line 3130
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@7e
    const-string v1, "data_roaming"

    #@80
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@83
    .line 3131
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@85
    const-string v1, "development_settings_enabled"

    #@87
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@8a
    .line 3132
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@8c
    const-string v1, "device_provisioned"

    #@8e
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@91
    .line 3133
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@93
    const-string v1, "display_density_forced"

    #@95
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@98
    .line 3134
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@9a
    const-string v1, "display_size_forced"

    #@9c
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@9f
    .line 3135
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@a1
    const-string v1, "download_manager_max_bytes_over_mobile"

    #@a3
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@a6
    .line 3136
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@a8
    const-string v1, "download_manager_recommended_max_bytes_over_mobile"

    #@aa
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@ad
    .line 3137
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@af
    const-string v1, "install_non_market_apps"

    #@b1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@b4
    .line 3138
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@b6
    const-string/jumbo v1, "mobile_data"

    #@b9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@bc
    .line 3139
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@be
    const-string/jumbo v1, "netstats_dev_bucket_duration"

    #@c1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@c4
    .line 3140
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@c6
    const-string/jumbo v1, "netstats_dev_delete_age"

    #@c9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@cc
    .line 3141
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@ce
    const-string/jumbo v1, "netstats_dev_persist_bytes"

    #@d1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@d4
    .line 3142
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@d6
    const-string/jumbo v1, "netstats_dev_rotate_age"

    #@d9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@dc
    .line 3143
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@de
    const-string/jumbo v1, "netstats_enabled"

    #@e1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e4
    .line 3144
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@e6
    const-string/jumbo v1, "netstats_global_alert_bytes"

    #@e9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@ec
    .line 3145
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@ee
    const-string/jumbo v1, "netstats_poll_interval"

    #@f1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@f4
    .line 3146
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@f6
    const-string/jumbo v1, "netstats_report_xt_over_dev"

    #@f9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@fc
    .line 3147
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@fe
    const-string/jumbo v1, "netstats_sample_enabled"

    #@101
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@104
    .line 3148
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@106
    const-string/jumbo v1, "netstats_time_cache_max_age"

    #@109
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@10c
    .line 3149
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@10e
    const-string/jumbo v1, "netstats_uid_bucket_duration"

    #@111
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@114
    .line 3150
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@116
    const-string/jumbo v1, "netstats_uid_delete_age"

    #@119
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@11c
    .line 3151
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@11e
    const-string/jumbo v1, "netstats_uid_persist_bytes"

    #@121
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@124
    .line 3152
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@126
    const-string/jumbo v1, "netstats_uid_rotate_age"

    #@129
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@12c
    .line 3153
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@12e
    const-string/jumbo v1, "netstats_uid_tag_bucket_duration"

    #@131
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@134
    .line 3154
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@136
    const-string/jumbo v1, "netstats_uid_tag_delete_age"

    #@139
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@13c
    .line 3155
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@13e
    const-string/jumbo v1, "netstats_uid_tag_persist_bytes"

    #@141
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@144
    .line 3156
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@146
    const-string/jumbo v1, "netstats_uid_tag_rotate_age"

    #@149
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@14c
    .line 3157
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@14e
    const-string/jumbo v1, "network_preference"

    #@151
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@154
    .line 3158
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@156
    const-string/jumbo v1, "nitz_update_diff"

    #@159
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@15c
    .line 3159
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@15e
    const-string/jumbo v1, "nitz_update_spacing"

    #@161
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@164
    .line 3160
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@166
    const-string/jumbo v1, "ntp_server"

    #@169
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@16c
    .line 3161
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@16e
    const-string/jumbo v1, "ntp_timeout"

    #@171
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@174
    .line 3162
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@176
    const-string/jumbo v1, "pdp_watchdog_error_poll_count"

    #@179
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@17c
    .line 3163
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@17e
    const-string/jumbo v1, "pdp_watchdog_long_poll_interval_ms"

    #@181
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@184
    .line 3164
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@186
    const-string/jumbo v1, "pdp_watchdog_max_pdp_reset_fail_count"

    #@189
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@18c
    .line 3165
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@18e
    const-string/jumbo v1, "pdp_watchdog_poll_interval_ms"

    #@191
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@194
    .line 3166
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@196
    const-string/jumbo v1, "pdp_watchdog_trigger_packet_count"

    #@199
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@19c
    .line 3167
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@19e
    const-string/jumbo v1, "sampling_profiler_ms"

    #@1a1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1a4
    .line 3168
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1a6
    const-string/jumbo v1, "setup_prepaid_data_service_url"

    #@1a9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1ac
    .line 3169
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1ae
    const-string/jumbo v1, "setup_prepaid_detection_redir_host"

    #@1b1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1b4
    .line 3170
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1b6
    const-string/jumbo v1, "setup_prepaid_detection_target_url"

    #@1b9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1bc
    .line 3171
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1be
    const-string/jumbo v1, "tether_dun_apn"

    #@1c1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1c4
    .line 3172
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1c6
    const-string/jumbo v1, "tether_dun_required"

    #@1c9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1cc
    .line 3174
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1ce
    const-string/jumbo v1, "tethering_pdn"

    #@1d1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1d4
    .line 3176
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1d6
    const-string/jumbo v1, "tether_supported"

    #@1d9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1dc
    .line 3177
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1de
    const-string/jumbo v1, "throttle_help_uri"

    #@1e1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1e4
    .line 3178
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1e6
    const-string/jumbo v1, "throttle_max_ntp_cache_age_sec"

    #@1e9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1ec
    .line 3179
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1ee
    const-string/jumbo v1, "throttle_notification_type"

    #@1f1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1f4
    .line 3180
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1f6
    const-string/jumbo v1, "throttle_polling_sec"

    #@1f9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1fc
    .line 3181
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1fe
    const-string/jumbo v1, "throttle_reset_day"

    #@201
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@204
    .line 3182
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@206
    const-string/jumbo v1, "throttle_threshold_bytes"

    #@209
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@20c
    .line 3183
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@20e
    const-string/jumbo v1, "throttle_value_kbitsps"

    #@211
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@214
    .line 3184
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@216
    const-string/jumbo v1, "usb_mass_storage_enabled"

    #@219
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@21c
    .line 3185
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@21e
    const-string/jumbo v1, "use_google_mail"

    #@221
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@224
    .line 3186
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@226
    const-string/jumbo v1, "web_autofill_query_url"

    #@229
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@22c
    .line 3187
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@22e
    const-string/jumbo v1, "wifi_country_code"

    #@231
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@234
    .line 3188
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@236
    const-string/jumbo v1, "wifi_framework_scan_interval_ms"

    #@239
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@23c
    .line 3189
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@23e
    const-string/jumbo v1, "wifi_frequency_band"

    #@241
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@244
    .line 3190
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@246
    const-string/jumbo v1, "wifi_idle_ms"

    #@249
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@24c
    .line 3191
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@24e
    const-string/jumbo v1, "wifi_max_dhcp_retry_count"

    #@251
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@254
    .line 3192
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@256
    const-string/jumbo v1, "wifi_mobile_data_transition_wakelock_timeout_ms"

    #@259
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@25c
    .line 3193
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@25e
    const-string/jumbo v1, "wifi_networks_available_notification_on"

    #@261
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@264
    .line 3194
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@266
    const-string/jumbo v1, "wifi_networks_available_repeat_delay"

    #@269
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@26c
    .line 3195
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@26e
    const-string/jumbo v1, "wifi_num_open_networks_kept"

    #@271
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@274
    .line 3196
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@276
    const-string/jumbo v1, "wifi_on"

    #@279
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@27c
    .line 3197
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@27e
    const-string/jumbo v1, "wifi_p2p_device_name"

    #@281
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@284
    .line 3198
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@286
    const-string/jumbo v1, "wifi_saved_state"

    #@289
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@28c
    .line 3199
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@28e
    const-string/jumbo v1, "wifi_supplicant_scan_interval_ms"

    #@291
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@294
    .line 3200
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@296
    const-string/jumbo v1, "wifi_suspend_optimizations_enabled"

    #@299
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@29c
    .line 3201
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@29e
    const-string/jumbo v1, "wifi_watchdog_on"

    #@2a1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2a4
    .line 3202
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2a6
    const-string/jumbo v1, "wifi_watchdog_poor_network_test_enabled"

    #@2a9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2ac
    .line 3203
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2ae
    const-string/jumbo v1, "wimax_networks_available_notification_on"

    #@2b1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2b4
    .line 3204
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2b6
    const-string/jumbo v1, "package_verifier_enable"

    #@2b9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2bc
    .line 3205
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2be
    const-string/jumbo v1, "verifier_timeout"

    #@2c1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2c4
    .line 3206
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2c6
    const-string/jumbo v1, "verifier_default_response"

    #@2c9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2cc
    .line 3207
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2ce
    const-string v1, "data_stall_alarm_non_aggressive_delay_in_ms"

    #@2d0
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2d3
    .line 3208
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2d5
    const-string v1, "data_stall_alarm_aggressive_delay_in_ms"

    #@2d7
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2da
    .line 3209
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2dc
    const-string v1, "gprs_register_check_period_ms"

    #@2de
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2e1
    .line 3210
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2e3
    const-string/jumbo v1, "wtf_is_fatal"

    #@2e6
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2e9
    .line 3211
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2eb
    const-string v1, "battery_discharge_duration_threshold"

    #@2ed
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2f0
    .line 3212
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2f2
    const-string v1, "battery_discharge_threshold"

    #@2f4
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2f7
    .line 3213
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2f9
    const-string/jumbo v1, "send_action_app_error"

    #@2fc
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2ff
    .line 3214
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@301
    const-string v1, "dropbox_age_seconds"

    #@303
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@306
    .line 3215
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@308
    const-string v1, "dropbox_max_files"

    #@30a
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@30d
    .line 3216
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@30f
    const-string v1, "dropbox_quota_kb"

    #@311
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@314
    .line 3217
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@316
    const-string v1, "dropbox_quota_percent"

    #@318
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@31b
    .line 3218
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@31d
    const-string v1, "dropbox_reserve_percent"

    #@31f
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@322
    .line 3219
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@324
    const-string v1, "dropbox:"

    #@326
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@329
    .line 3220
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@32b
    const-string/jumbo v1, "logcat_for_"

    #@32e
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@331
    .line 3221
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@333
    const-string/jumbo v1, "sys_free_storage_log_interval"

    #@336
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@339
    .line 3222
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@33b
    const-string v1, "disk_free_change_reporting_threshold"

    #@33d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@340
    .line 3223
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@342
    const-string/jumbo v1, "sys_storage_threshold_percentage"

    #@345
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@348
    .line 3224
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@34a
    const-string/jumbo v1, "sys_storage_threshold_max_bytes"

    #@34d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@350
    .line 3225
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@352
    const-string/jumbo v1, "sys_storage_full_threshold_bytes"

    #@355
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@358
    .line 3226
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@35a
    const-string/jumbo v1, "sync_max_retry_delay_in_seconds"

    #@35d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@360
    .line 3227
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@362
    const-string v1, "connectivity_change_delay"

    #@364
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@367
    .line 3228
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@369
    const-string v1, "captive_portal_detection_enabled"

    #@36b
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@36e
    .line 3229
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@370
    const-string v1, "captive_portal_server"

    #@372
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@375
    .line 3230
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@377
    const-string/jumbo v1, "nsd_on"

    #@37a
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@37d
    .line 3231
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@37f
    const-string/jumbo v1, "set_install_location"

    #@382
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@385
    .line 3232
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@387
    const-string v1, "default_install_location"

    #@389
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@38c
    .line 3233
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@38e
    const-string v1, "inet_condition_debounce_up_delay"

    #@390
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@393
    .line 3234
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@395
    const-string v1, "inet_condition_debounce_down_delay"

    #@397
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@39a
    .line 3235
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@39c
    const-string/jumbo v1, "read_external_storage_enforced_default"

    #@39f
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3a2
    .line 3236
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3a4
    const-string v1, "http_proxy"

    #@3a6
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3a9
    .line 3237
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3ab
    const-string v1, "global_http_proxy_host"

    #@3ad
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3b0
    .line 3238
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3b2
    const-string v1, "global_http_proxy_port"

    #@3b4
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3b7
    .line 3239
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3b9
    const-string v1, "global_http_proxy_exclusion_list"

    #@3bb
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3be
    .line 3240
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3c0
    const-string/jumbo v1, "set_global_http_proxy"

    #@3c3
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3c6
    .line 3241
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3c8
    const-string v1, "default_dns_server"

    #@3ca
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3cd
    .line 3242
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3cf
    const-string/jumbo v1, "preferred_network_mode"

    #@3d2
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3d5
    .line 3243
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3d7
    const-string/jumbo v1, "preferred_cdma_subscription"

    #@3da
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3dd
    .line 3245
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3df
    const-string/jumbo v1, "sd_encryption"

    #@3e2
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3e5
    .line 3246
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3e7
    const-string v1, "data_encryption"

    #@3e9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3ec
    .line 3247
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3ee
    const-string v1, "boot_lock"

    #@3f0
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3f3
    .line 3248
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3f5
    const-string v1, "adb_blocked"

    #@3f7
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3fa
    .line 3249
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@3fc
    const-string/jumbo v1, "usb_blocked"

    #@3ff
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@402
    .line 3250
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@404
    const-string v1, "admin_locked"

    #@406
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@409
    .line 3251
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@40b
    const-string/jumbo v1, "ota_delay"

    #@40e
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@411
    .line 3252
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@413
    const-string v1, "emergency_lock"

    #@415
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@418
    .line 3253
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@41a
    const-string/jumbo v1, "tethering_blocked"

    #@41d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@420
    .line 3254
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@422
    const-string/jumbo v1, "screenshot_blocked"

    #@425
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@428
    .line 3255
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@42a
    const-string v1, "apn_locked"

    #@42c
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@42f
    .line 4970
    const/16 v0, 0x27

    #@431
    new-array v0, v0, [Ljava/lang/String;

    #@433
    const/4 v1, 0x0

    #@434
    const-string v2, "bugreport_in_power_menu"

    #@436
    aput-object v2, v0, v1

    #@438
    const/4 v1, 0x1

    #@439
    const-string/jumbo v2, "mock_location"

    #@43c
    aput-object v2, v0, v1

    #@43e
    const/4 v1, 0x2

    #@43f
    const-string/jumbo v2, "parental_control_enabled"

    #@442
    aput-object v2, v0, v1

    #@444
    const-string/jumbo v1, "parental_control_redirect_url"

    #@447
    aput-object v1, v0, v5

    #@449
    const/4 v1, 0x4

    #@44a
    const-string/jumbo v2, "usb_mass_storage_enabled"

    #@44d
    aput-object v2, v0, v1

    #@44f
    const/4 v1, 0x5

    #@450
    const-string v2, "accessibility_display_magnification_enabled"

    #@452
    aput-object v2, v0, v1

    #@454
    const/4 v1, 0x6

    #@455
    const-string v2, "accessibility_display_magnification_scale"

    #@457
    aput-object v2, v0, v1

    #@459
    const/4 v1, 0x7

    #@45a
    const-string v2, "accessibility_display_magnification_auto_update"

    #@45c
    aput-object v2, v0, v1

    #@45e
    const/16 v1, 0x8

    #@460
    const-string v2, "accessibility_script_injection"

    #@462
    aput-object v2, v0, v1

    #@464
    const/16 v1, 0x9

    #@466
    const-string v2, "backup_auto_restore"

    #@468
    aput-object v2, v0, v1

    #@46a
    const/16 v1, 0xa

    #@46c
    const-string v2, "enabled_accessibility_services"

    #@46e
    aput-object v2, v0, v1

    #@470
    const/16 v1, 0xb

    #@472
    const-string/jumbo v2, "touch_exploration_granted_accessibility_services"

    #@475
    aput-object v2, v0, v1

    #@477
    const/16 v1, 0xc

    #@479
    const-string/jumbo v2, "touch_exploration_enabled"

    #@47c
    aput-object v2, v0, v1

    #@47e
    const/16 v1, 0xd

    #@480
    const-string v2, "accessibility_enabled"

    #@482
    aput-object v2, v0, v1

    #@484
    const/16 v1, 0xe

    #@486
    const-string/jumbo v2, "speak_password"

    #@489
    aput-object v2, v0, v1

    #@48b
    const/16 v1, 0xf

    #@48d
    const-string/jumbo v2, "tts_use_defaults"

    #@490
    aput-object v2, v0, v1

    #@492
    const/16 v1, 0x10

    #@494
    const-string/jumbo v2, "tts_default_rate"

    #@497
    aput-object v2, v0, v1

    #@499
    const/16 v1, 0x11

    #@49b
    const-string/jumbo v2, "tts_default_pitch"

    #@49e
    aput-object v2, v0, v1

    #@4a0
    const/16 v1, 0x12

    #@4a2
    const-string/jumbo v2, "tts_default_synth"

    #@4a5
    aput-object v2, v0, v1

    #@4a7
    const/16 v1, 0x13

    #@4a9
    const-string/jumbo v2, "tts_default_lang"

    #@4ac
    aput-object v2, v0, v1

    #@4ae
    const/16 v1, 0x14

    #@4b0
    const-string/jumbo v2, "tts_default_country"

    #@4b3
    aput-object v2, v0, v1

    #@4b5
    const/16 v1, 0x15

    #@4b7
    const-string/jumbo v2, "tts_enabled_plugins"

    #@4ba
    aput-object v2, v0, v1

    #@4bc
    const/16 v1, 0x16

    #@4be
    const-string/jumbo v2, "tts_default_locale"

    #@4c1
    aput-object v2, v0, v1

    #@4c3
    const/16 v1, 0x17

    #@4c5
    const-string/jumbo v2, "wifi_networks_available_notification_on"

    #@4c8
    aput-object v2, v0, v1

    #@4ca
    const/16 v1, 0x18

    #@4cc
    const-string/jumbo v2, "wifi_networks_available_repeat_delay"

    #@4cf
    aput-object v2, v0, v1

    #@4d1
    const/16 v1, 0x19

    #@4d3
    const-string/jumbo v2, "wifi_num_open_networks_kept"

    #@4d6
    aput-object v2, v0, v1

    #@4d8
    const/16 v1, 0x1a

    #@4da
    const-string/jumbo v2, "mount_play_not_snd"

    #@4dd
    aput-object v2, v0, v1

    #@4df
    const/16 v1, 0x1b

    #@4e1
    const-string/jumbo v2, "mount_ums_autostart"

    #@4e4
    aput-object v2, v0, v1

    #@4e6
    const/16 v1, 0x1c

    #@4e8
    const-string/jumbo v2, "mount_ums_prompt"

    #@4eb
    aput-object v2, v0, v1

    #@4ed
    const/16 v1, 0x1d

    #@4ef
    const-string/jumbo v2, "mount_ums_notify_enabled"

    #@4f2
    aput-object v2, v0, v1

    #@4f4
    const/16 v1, 0x1e

    #@4f6
    const-string/jumbo v2, "ui_night_mode"

    #@4f9
    aput-object v2, v0, v1

    #@4fb
    const/16 v1, 0x1f

    #@4fd
    const-string/jumbo v2, "skt_roaming_dualclock"

    #@500
    aput-object v2, v0, v1

    #@502
    const/16 v1, 0x20

    #@504
    const-string/jumbo v2, "show_roaming_prefix"

    #@507
    aput-object v2, v0, v1

    #@509
    const/16 v1, 0x21

    #@50b
    const-string/jumbo v2, "kt_roaming_index"

    #@50e
    aput-object v2, v0, v1

    #@510
    const/16 v1, 0x22

    #@512
    const-string/jumbo v2, "kt_roaming_user"

    #@515
    aput-object v2, v0, v1

    #@517
    const/16 v1, 0x23

    #@519
    const-string/jumbo v2, "roaming_dualclock"

    #@51c
    aput-object v2, v0, v1

    #@51e
    const/16 v1, 0x24

    #@520
    const-string/jumbo v2, "lock_screen_owner_info"

    #@523
    aput-object v2, v0, v1

    #@525
    const/16 v1, 0x25

    #@527
    const-string/jumbo v2, "lock_screen_owner_info_enabled"

    #@52a
    aput-object v2, v0, v1

    #@52c
    const/16 v1, 0x26

    #@52e
    const-string/jumbo v2, "revsetting_hidden"

    #@531
    aput-object v2, v0, v1

    #@533
    sput-object v0, Landroid/provider/Settings$Secure;->SETTINGS_TO_BACKUP:[Ljava/lang/String;

    #@535
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3094
    invoke-direct {p0}, Landroid/provider/Settings$NameValueTable;-><init>()V

    #@3
    return-void
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    .registers 3
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 3560
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 3526
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$Secure;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3532
    invoke-static {p0, p1, p3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3534
    .local v1, v:Ljava/lang/String;
    if-eqz v1, :cond_a

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_b

    #@9
    move-result p2

    #@a
    .line 3536
    .end local p2
    :cond_a
    :goto_a
    return p2

    #@b
    .line 3535
    .restart local p2
    :catch_b
    move-exception v0

    #@c
    .line 3536
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_a
.end method

.method public static getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)F
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 3566
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3567
    .local v1, v:Ljava/lang/String;
    if-nez v1, :cond_c

    #@6
    .line 3568
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@8
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 3571
    :cond_c
    :try_start_c
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_f
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_f} :catch_11

    #@f
    move-result v2

    #@10
    return v2

    #@11
    .line 3572
    :catch_11
    move-exception v0

    #@12
    .line 3573
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@14
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .registers 3
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 3387
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 3354
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 3393
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3395
    .local v1, v:Ljava/lang/String;
    :try_start_4
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    return v2

    #@9
    .line 3396
    :catch_9
    move-exception v0

    #@a
    .line 3397
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@c
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2
.end method

.method public static getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3359
    invoke-static {p0, p1, p3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3361
    .local v1, v:Ljava/lang/String;
    if-eqz v1, :cond_a

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_b

    #@9
    move-result p2

    #@a
    .line 3363
    .end local p2
    :cond_a
    :goto_a
    return p2

    #@b
    .line 3362
    .restart local p2
    :catch_b
    move-exception v0

    #@c
    .line 3363
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_a
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    .registers 4
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 3474
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 3439
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, p3, v0}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public static getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)J
    .registers 7
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 3480
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3482
    .local v1, valString:Ljava/lang/String;
    :try_start_4
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result-wide v2

    #@8
    return-wide v2

    #@9
    .line 3483
    :catch_9
    move-exception v0

    #@a
    .line 3484
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@c
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2
.end method

.method public static getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J
    .registers 9
    .parameter "cr"
    .parameter "name"
    .parameter "def"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3445
    invoke-static {p0, p1, p4}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 3448
    .local v1, valString:Ljava/lang/String;
    if-eqz v1, :cond_b

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_d

    #@9
    move-result-wide v2

    #@a
    .line 3452
    .local v2, value:J
    :goto_a
    return-wide v2

    #@b
    .end local v2           #value:J
    :cond_b
    move-wide v2, p2

    #@c
    .line 3448
    goto :goto_a

    #@d
    .line 3449
    :catch_d
    move-exception v0

    #@e
    .line 3450
    .local v0, e:Ljava/lang/NumberFormatException;
    move-wide v2, p2

    #@f
    .restart local v2       #value:J
    goto :goto_a
.end method

.method public static getMovedKeys(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3260
    .local p0, outKeySet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2
    invoke-virtual {p0, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@5
    .line 3261
    return-void
.end method

.method public static getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "resolver"
    .parameter "name"

    #@0
    .prologue
    .line 3270
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
    .registers 6
    .parameter "resolver"
    .parameter "name"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3276
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_31

    #@8
    .line 3277
    const-string v0, "Settings"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Setting "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " has moved from android.provider.Settings.Secure"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " to android.provider.Settings.Global."

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 3279
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 3299
    :goto_30
    return-object v0

    #@31
    .line 3282
    :cond_31
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_LOCK_SETTINGS:Ljava/util/HashSet;

    #@33
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_70

    #@39
    .line 3283
    const-class v1, Landroid/provider/Settings$Secure;

    #@3b
    monitor-enter v1

    #@3c
    .line 3284
    :try_start_3c
    sget-object v0, Landroid/provider/Settings$Secure;->sLockSettings:Lcom/android/internal/widget/ILockSettings;

    #@3e
    if-nez v0, :cond_58

    #@40
    .line 3285
    const-string/jumbo v0, "lock_settings"

    #@43
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@46
    move-result-object v0

    #@47
    invoke-static {v0}, Lcom/android/internal/widget/ILockSettings$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/ILockSettings;

    #@4a
    move-result-object v0

    #@4b
    sput-object v0, Landroid/provider/Settings$Secure;->sLockSettings:Lcom/android/internal/widget/ILockSettings;

    #@4d
    .line 3287
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@50
    move-result v0

    #@51
    const/16 v2, 0x3e8

    #@53
    if-ne v0, v2, :cond_6a

    #@55
    const/4 v0, 0x1

    #@56
    :goto_56
    sput-boolean v0, Landroid/provider/Settings$Secure;->sIsSystemProcess:Z

    #@58
    .line 3289
    :cond_58
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_3c .. :try_end_59} :catchall_6c

    #@59
    .line 3290
    sget-object v0, Landroid/provider/Settings$Secure;->sLockSettings:Lcom/android/internal/widget/ILockSettings;

    #@5b
    if-eqz v0, :cond_70

    #@5d
    sget-boolean v0, Landroid/provider/Settings$Secure;->sIsSystemProcess:Z

    #@5f
    if-nez v0, :cond_70

    #@61
    .line 3292
    :try_start_61
    sget-object v0, Landroid/provider/Settings$Secure;->sLockSettings:Lcom/android/internal/widget/ILockSettings;

    #@63
    const-string v1, "0"

    #@65
    invoke-interface {v0, p1, v1, p2}, Lcom/android/internal/widget/ILockSettings;->getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_68
    .catch Landroid/os/RemoteException; {:try_start_61 .. :try_end_68} :catch_6f

    #@68
    move-result-object v0

    #@69
    goto :goto_30

    #@6a
    .line 3287
    :cond_6a
    const/4 v0, 0x0

    #@6b
    goto :goto_56

    #@6c
    .line 3289
    :catchall_6c
    move-exception v0

    #@6d
    :try_start_6d
    monitor-exit v1
    :try_end_6e
    .catchall {:try_start_6d .. :try_end_6e} :catchall_6c

    #@6e
    throw v0

    #@6f
    .line 3293
    :catch_6f
    move-exception v0

    #@70
    .line 3299
    :cond_70
    sget-object v0, Landroid/provider/Settings$Secure;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    #@72
    invoke-virtual {v0, p0, p1, p2}, Landroid/provider/Settings$NameValueCache;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    goto :goto_30
.end method

.method public static getUriFor(Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 3331
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_33

    #@8
    .line 3332
    const-string v0, "Settings"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Setting "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " has moved from android.provider.Settings.Secure"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " to android.provider.Settings.Global, returning global URI."

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 3334
    sget-object v0, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    #@2e
    invoke-static {v0, p0}, Landroid/provider/Settings$Global;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@31
    move-result-object v0

    #@32
    .line 3336
    :goto_32
    return-object v0

    #@33
    :cond_33
    sget-object v0, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@35
    invoke-static {v0, p0}, Landroid/provider/Settings$Secure;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@38
    move-result-object v0

    #@39
    goto :goto_32
.end method

.method public static final isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .registers 3
    .parameter "cr"
    .parameter "provider"

    #@0
    .prologue
    .line 5030
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->isLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static final isLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 5
    .parameter "cr"
    .parameter "provider"
    .parameter "userId"

    #@0
    .prologue
    .line 5042
    const-string/jumbo v1, "location_providers_allowed"

    #@3
    invoke-static {p0, v1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 5044
    .local v0, allowedProviders:Ljava/lang/String;
    const/16 v1, 0x2c

    #@9
    invoke-static {v0, v1, p1}, Landroid/text/TextUtils;->delimitedStringContains(Ljava/lang/String;CLjava/lang/String;)Z

    #@c
    move-result v1

    #@d
    return v1
.end method

.method public static putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 3591
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$Secure;->putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3597
    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 3415
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3421
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 3502
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, p3, v0}, Landroid/provider/Settings$Secure;->putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3508
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p4}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 3310
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 7
    .parameter "resolver"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 3316
    sget-object v0, Landroid/provider/Settings$Secure;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_31

    #@8
    .line 3317
    const-string v0, "Settings"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Setting "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " has moved from android.provider.Settings.System"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " to android.provider.Settings.Global"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 3319
    invoke-static {p0, p1, p2, p3}, Landroid/provider/Settings$Global;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@2f
    move-result v0

    #@30
    .line 3321
    :goto_30
    return v0

    #@31
    :cond_31
    sget-object v0, Landroid/provider/Settings$Secure;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    #@33
    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/provider/Settings$NameValueCache;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@36
    move-result v0

    #@37
    goto :goto_30
.end method

.method public static final setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V
    .registers 4
    .parameter "cr"
    .parameter "provider"
    .parameter "enabled"

    #@0
    .prologue
    .line 5055
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$Secure;->setLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)V

    #@7
    .line 5056
    return-void
.end method

.method public static final setLocationProviderEnabledForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)V
    .registers 6
    .parameter "cr"
    .parameter "provider"
    .parameter "enabled"
    .parameter "userId"

    #@0
    .prologue
    .line 5069
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_19

    #@6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x3

    #@b
    invoke-interface {v0, v1, p2, p3, p1}, Lcom/lge/cappuccino/IMdm;->getMDMLocationPolicy(IZILjava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_19

    #@11
    .line 5072
    const-string v0, "MDM"

    #@13
    const-string v1, "Gps cannot be changed by Server Policy."

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 5087
    :goto_18
    return-void

    #@19
    .line 5080
    :cond_19
    if-eqz p2, :cond_35

    #@1b
    .line 5081
    new-instance v0, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v1, "+"

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object p1

    #@2e
    .line 5085
    :goto_2e
    const-string/jumbo v0, "location_providers_allowed"

    #@31
    invoke-static {p0, v0, p1, p3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@34
    goto :goto_18

    #@35
    .line 5083
    :cond_35
    new-instance v0, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v1, "-"

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object p1

    #@48
    goto :goto_2e
.end method
