.class public final Landroid/provider/Settings$System;
.super Landroid/provider/Settings$NameValueTable;
.source "Settings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "System"
.end annotation


# static fields
.field public static final ACCELEROMETER_ROTATION:Ljava/lang/String; = "accelerometer_rotation"

.field public static final ADB_ENABLED:Ljava/lang/String; = "adb_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ADVANCED_SETTINGS:Ljava/lang/String; = "advanced_settings"

.field public static final ADVANCED_SETTINGS_DEFAULT:I = 0x0

.field public static final AIRPLANE_MODE_ON:Ljava/lang/String; = "airplane_mode_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AIRPLANE_MODE_RADIOS:Ljava/lang/String; = "airplane_mode_radios"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String; = "airplane_mode_toggleable_radios"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ALARM_ALERT:Ljava/lang/String; = "alarm_alert"

.field public static final ALWAYS_FINISH_ACTIVITIES:Ljava/lang/String; = "always_finish_activities"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ANDROID_ID:Ljava/lang/String; = "android_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ANIMATOR_DURATION_SCALE:Ljava/lang/String; = "animator_duration_scale"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final APPEND_FOR_LAST_AUDIBLE:Ljava/lang/String; = "_last_audible"

.field public static final ASSIST_DIAL:Ljava/lang/String; = "assist_dial"

.field public static final ASSIST_DIAL_CHECK:Ljava/lang/String; = "assist_dial_check"

.field public static final ASSIST_DIAL_CURRENT_COUNTRY:Ljava/lang/String; = "assist_dial_current_country"

.field public static final ASSIST_DIAL_FROM_CONTACT:Ljava/lang/String; = "assist_dial_from_contact"

.field public static final ASSIST_DIAL_INIT_DB_CHECK:Ljava/lang/String; = "assist_dial_init_db_check"

.field public static final ASSIST_DIAL_NEW_NUMBER_CHECK:Ljava/lang/String; = "assist_dial_new_number_check"

.field public static final ASSIST_DIAL_OTA_MCC:Ljava/lang/String; = "assist_dial_ota_mcc"

.field public static final ASSIST_DIAL_OTA_SID:Ljava/lang/String; = "assist_dial_ota_sid"

.field public static final ASSIST_DIAL_POSITION:Ljava/lang/String; = "assist_dial_position"

.field public static final ASSIST_DIAL_REFERENCE_COUNTRY:Ljava/lang/String; = "assist_dial_reference_country"

.field public static final AUTO_TIME:Ljava/lang/String; = "auto_time"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUTO_TIME_ZONE:Ljava/lang/String; = "auto_time_zone"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BLUETOOTH_DISCOVERABILITY:Ljava/lang/String; = "bluetooth_discoverability"

.field public static final BLUETOOTH_DISCOVERABILITY_TIMEOUT:Ljava/lang/String; = "bluetooth_discoverability_timeout"

.field public static final BLUETOOTH_ON:Ljava/lang/String; = "bluetooth_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CAR_DOCK_SOUND:Ljava/lang/String; = "car_dock_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CAR_UNDOCK_SOUND:Ljava/lang/String; = "car_undock_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CMAS_EXERCISE_ALERT:Ljava/lang/String; = "cmas_exercise_alert"

.field public static final CMAS_TEST_MESSAGE:Ljava/lang/String; = "cmas_test_message"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DATA_ROAMING:Ljava/lang/String; = "data_roaming"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATA_ROAM_GUARD_FIRST_ACCESS:Ljava/lang/String; = "lg_data_roam_guard_first_access"

.field public static final DATE_FORMAT:Ljava/lang/String; = "date_format"

.field public static final DEBUG_APP:Ljava/lang/String; = "debug_app"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_NOTIFICATION_SIM2_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_NOTIFICATION_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_RINGTONE_SIM2_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_RINGTONE_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_RINGTONE_VIDEOCALL_URI:Landroid/net/Uri; = null

.field public static final DESK_DOCK_SOUND:Ljava/lang/String; = "desk_dock_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DESK_UNDOCK_SOUND:Ljava/lang/String; = "desk_undock_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEVICE_PROVISIONED:Ljava/lang/String; = "device_provisioned"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DIM_SCREEN:Ljava/lang/String; = "dim_screen"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DOCK_SOUNDS_ENABLED:Ljava/lang/String; = "dock_sounds_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DTMF_TONE_TYPE_WHEN_DIALING:Ljava/lang/String; = "dtmf_tone_type"

.field public static final DTMF_TONE_WHEN_DIALING:Ljava/lang/String; = "dtmf_tone"

.field public static final DUMMY_STRING_FOR_PADDING:Ljava/lang/String; = ""

.field public static final END_BUTTON_BEHAVIOR:Ljava/lang/String; = "end_button_behavior"

.field public static final END_BUTTON_BEHAVIOR_DEFAULT:I = 0x2

.field public static final END_BUTTON_BEHAVIOR_HOME:I = 0x1

.field public static final END_BUTTON_BEHAVIOR_SLEEP:I = 0x2

.field public static final ERI_SET:Ljava/lang/String; = "lg_eri_set"

.field public static final FONT_SCALE:Ljava/lang/String; = "font_scale"

.field public static final GO_HOME:Ljava/lang/String; = "go_home"

.field public static final GSM_DATA_ROAM_GUARD_FIRST_ACCESS:Ljava/lang/String; = "lg_gsm_data_roam_guard_first_access"

.field public static final GUARD_DOM_DATA:Ljava/lang/String; = "rguard_domestic_data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_DOM_DATA_FORCED:Ljava/lang/String; = "rguard_domestic_data_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_DOM_VOICE:Ljava/lang/String; = "rguard_domestic_voice"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_DOM_VOICE_FORCED:Ljava/lang/String; = "rguard_domestic_voice_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_INTL_DATA:Ljava/lang/String; = "rguard_international_data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_INTL_DATA_FORCED:Ljava/lang/String; = "rguard_international_data_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_INTL_OUTGOING_SMS:Ljava/lang/String; = "cbox_international_sms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_INTL_OUTGOING_SMS_FORCED:Ljava/lang/String; = "cbox_international_sms_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_INTL_VOICE:Ljava/lang/String; = "rguard_international_voice"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GUARD_INTL_VOICE_FORCED:Ljava/lang/String; = "rguard_international_voice_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HAPTIC_FEEDBACK_ENABLED:Ljava/lang/String; = "haptic_feedback_enabled"

.field public static final HEARING_AID:Ljava/lang/String; = "hearing_aid"

.field public static final HIDE_DISPLAY:Ljava/lang/String; = "hide_display"

.field public static final HIDE_ROTATION_LOCK_TOGGLE_FOR_ACCESSIBILITY:Ljava/lang/String; = "hide_rotation_lock_toggle_for_accessibility"

.field public static final HTTP_PROXY:Ljava/lang/String; = "http_proxy"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INSTALL_NON_MARKET_APPS:Ljava/lang/String; = "install_non_market_apps"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INT_PLUS_DIAL:Ljava/lang/String; = "lg_int_plus_dial"

.field public static final IS_OMADM_RUNNING:Ljava/lang/String; = "lg_is_omadm_running"

.field public static final LG_SMS_SETTING_REASSEMBLY:Ljava/lang/String; = "lg_sms_setting_reassembly"

.field public static final LOCAL_PLUS_DIAL:Ljava/lang/String; = "lg_local_plus_dial"

.field public static final LOCATION_PROVIDERS_ALLOWED:Ljava/lang/String; = "location_providers_allowed"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCKSCREEN_DISABLED:Ljava/lang/String; = "lockscreen.disabled"

.field public static final LOCKSCREEN_SOUNDS_ENABLED:Ljava/lang/String; = "lockscreen_sounds_enabled"

.field public static final LOCK_PATTERN_ENABLED:Ljava/lang/String; = "lock_pattern_autolock"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED:Ljava/lang/String; = "lock_pattern_tactile_feedback_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCK_PATTERN_VISIBLE:Ljava/lang/String; = "lock_pattern_visible_pattern"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCK_SOUND:Ljava/lang/String; = "lock_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOGGING_ID:Ljava/lang/String; = "logging_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOW_BATTERY_SOUND:Ljava/lang/String; = "low_battery_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MEDIA_BUTTON_RECEIVER:Ljava/lang/String; = "media_button_receiver"

.field public static final MODE_RINGER:Ljava/lang/String; = "mode_ringer"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MODE_RINGER_STREAMS_AFFECTED:Ljava/lang/String; = "mode_ringer_streams_affected"

.field private static final MOVED_TO_GLOBAL:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MOVED_TO_SECURE:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MUTE_STREAMS_AFFECTED:Ljava/lang/String; = "mute_streams_affected"

.field public static final NETWORK_CHANGE_AUTO_RETRY:Ljava/lang/String; = "network_change_auto_retry"

.field public static final NETWORK_CHANGE_AUTO_RETRY_NUMBER:Ljava/lang/String; = "network_change_auto_retry_number"

.field public static final NETWORK_PREFERENCE:Ljava/lang/String; = "network_preference"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NEXT_ALARM_FORMATTED:Ljava/lang/String; = "next_alarm_formatted"

.field public static final NOTIFICATIONS_USE_RING_VOLUME:Ljava/lang/String; = "notifications_use_ring_volume"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NOTIFICATION_LIGHT_PULSE:Ljava/lang/String; = "notification_light_pulse"

.field public static final NOTIFICATION_SOUND:Ljava/lang/String; = "notification_sound"

.field public static final NOTIFICATION_SOUND_SIM2:Ljava/lang/String; = "notification_sound_sim2"

.field public static final OMADM_CHECK_VERSION:Ljava/lang/String; = "lg_omadm_check_version"

.field public static final OMADM_HFA_ENABLED:Ljava/lang/String; = "lg_omadm_hfa_enabled"

.field public static final OMADM_LWMO_LOCK_CODE:Ljava/lang/String; = "lg_omadm_lwmo_lock_code"

.field public static final OMADM_LWMO_LOCK_STATE:Ljava/lang/String; = "lg_omadm_lwmo_lock_state"

.field public static final OTKSL_COUNT:Ljava/lang/String; = "otksl_count"

.field public static final PARENTAL_CONTROL_ENABLED:Ljava/lang/String; = "parental_control_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PARENTAL_CONTROL_LAST_UPDATE:Ljava/lang/String; = "parental_control_last_update"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PARENTAL_CONTROL_REDIRECT_URL:Ljava/lang/String; = "parental_control_redirect_url"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final POINTER_LOCATION:Ljava/lang/String; = "pointer_location"

.field public static final POINTER_SPEED:Ljava/lang/String; = "pointer_speed"

.field public static final POWER_SOUNDS_ENABLED:Ljava/lang/String; = "power_sounds_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PROXIMITY_SENSOR_CHECK:Ljava/lang/String; = "proximity_sensor_check"

.field public static final RADIO_BLUETOOTH:Ljava/lang/String; = "bluetooth"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RADIO_CELL:Ljava/lang/String; = "cell"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RADIO_NFC:Ljava/lang/String; = "nfc"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RADIO_WIFI:Ljava/lang/String; = "wifi"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RADIO_WIMAX:Ljava/lang/String; = "wimax"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final REFERENCE_COUNTRY_IN_ASSISTED_DIAL:Ljava/lang/String; = "reference_country_in_assisted_dial"

.field public static final RINGTONE:Ljava/lang/String; = "ringtone"

.field public static final RINGTONE_SIM2:Ljava/lang/String; = "ringtone_sim2"

.field public static final RINGTONE_VIDEOCALL:Ljava/lang/String; = "ringtone_videocall"

.field public static final RMODE_DOM_DATA:Ljava/lang/String; = "roaming_mode_domestic_data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_DOM_DATA_FORCED:Ljava/lang/String; = "roaming_mode_domestic_data_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_DOM_VOICE:Ljava/lang/String; = "roaming_mode_domestic_voice"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_DOM_VOICE_FORCED:Ljava/lang/String; = "roaming_mode_domestic_voice_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_INTL_DATA:Ljava/lang/String; = "roaming_mode_international_data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_INTL_DATA_FORCED:Ljava/lang/String; = "roaming_mode_international_data_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_INTL_VOICE:Ljava/lang/String; = "roaming_mode_international_voice"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_INTL_VOICE_FORCED:Ljava/lang/String; = "roaming_mode_international_voice_forced"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RMODE_SEL_SYSTEM:Ljava/lang/String; = "roaming_mode_selected_system"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SCREEN_AUTO_BRIGHTNESS_ADJ:Ljava/lang/String; = "screen_auto_brightness_adj"

.field public static final SCREEN_BRIGHTNESS:Ljava/lang/String; = "screen_brightness"

.field public static final SCREEN_BRIGHTNESS_MODE:Ljava/lang/String; = "screen_brightness_mode"

.field public static final SCREEN_BRIGHTNESS_MODE_AUTOMATIC:I = 0x1

.field public static final SCREEN_BRIGHTNESS_MODE_MANUAL:I = 0x0

.field public static final SCREEN_OFF_TIMEOUT:Ljava/lang/String; = "screen_off_timeout"

.field public static final SETTINGS_CLASSNAME:Ljava/lang/String; = "settings_classname"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SETTINGS_TO_BACKUP:[Ljava/lang/String; = null

.field public static final SETUP_WIZARD_HAS_RUN:Ljava/lang/String; = "setup_wizard_has_run"

.field public static final SHOW_GTALK_SERVICE_STATUS:Ljava/lang/String; = "SHOW_GTALK_SERVICE_STATUS"

.field public static final SHOW_PROCESSES:Ljava/lang/String; = "show_processes"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SHOW_TOUCHES:Ljava/lang/String; = "show_touches"

.field public static final SHOW_WEB_SUGGESTIONS:Ljava/lang/String; = "show_web_suggestions"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SIP_ADDRESS_ONLY:Ljava/lang/String; = "SIP_ADDRESS_ONLY"

.field public static final SIP_ALWAYS:Ljava/lang/String; = "SIP_ALWAYS"

.field public static final SIP_ASK_ME_EACH_TIME:Ljava/lang/String; = "SIP_ASK_ME_EACH_TIME"

.field public static final SIP_CALL_OPTIONS:Ljava/lang/String; = "sip_call_options"

.field public static final SIP_RECEIVE_CALLS:Ljava/lang/String; = "sip_receive_calls"

.field public static final SLIDE_ASIDE:Ljava/lang/String; = "multitasking_slide_aside"

.field public static final SOUND_EFFECTS_ENABLED:Ljava/lang/String; = "sound_effects_enabled"

.field public static final STAY_ON_WHILE_PLUGGED_IN:Ljava/lang/String; = "stay_on_while_plugged_in"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SYS_PROP_SETTING_VERSION:Ljava/lang/String; = "sys.settings_system_version"

.field public static final TAKE_SCREENSHOT:Ljava/lang/String; = "take_screenshot"

.field public static final TEXT_AUTO_CAPS:Ljava/lang/String; = "auto_caps"

.field public static final TEXT_AUTO_PUNCTUATE:Ljava/lang/String; = "auto_punctuate"

.field public static final TEXT_AUTO_REPLACE:Ljava/lang/String; = "auto_replace"

.field public static final TEXT_SHOW_PASSWORD:Ljava/lang/String; = "show_password"

.field public static final TIME_12_24:Ljava/lang/String; = "time_12_24"

.field public static final TRANSITION_ANIMATION_SCALE:Ljava/lang/String; = "transition_animation_scale"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TTY_MODE:Ljava/lang/String; = "tty_mode"

.field public static final UNLOCK_SOUND:Ljava/lang/String; = "unlock_sound"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final USB_MASS_STORAGE_ENABLED:Ljava/lang/String; = "usb_mass_storage_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final USER_ROTATION:Ljava/lang/String; = "user_rotation"

.field public static final USE_GOOGLE_MAIL:Ljava/lang/String; = "use_google_mail"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final VIBRATE_INPUT_DEVICES:Ljava/lang/String; = "vibrate_input_devices"

.field public static final VIBRATE_IN_SILENT:Ljava/lang/String; = "vibrate_in_silent"

.field public static final VIBRATE_ON:Ljava/lang/String; = "vibrate_on"

.field public static final VIBRATE_WHEN_RINGING:Ljava/lang/String; = "vibrate_when_ringing"

.field public static final VOLUME_ALARM:Ljava/lang/String; = "volume_alarm"

.field public static final VOLUME_BLUETOOTH_SCO:Ljava/lang/String; = "volume_bluetooth_sco"

.field public static final VOLUME_FM:Ljava/lang/String; = "volume_fm"

.field public static final VOLUME_MASTER:Ljava/lang/String; = "volume_master"

.field public static final VOLUME_MASTER_MUTE:Ljava/lang/String; = "volume_master_mute"

.field public static final VOLUME_MUSIC:Ljava/lang/String; = "volume_music"

.field public static final VOLUME_NOTIFICATION:Ljava/lang/String; = "volume_notification"

.field public static final VOLUME_RING:Ljava/lang/String; = "volume_ring"

.field public static final VOLUME_SETTINGS:[Ljava/lang/String; = null

.field public static final VOLUME_SYSTEM:Ljava/lang/String; = "volume_system"

.field public static final VOLUME_VOICE:Ljava/lang/String; = "volume_voice"

.field public static final WAIT_FOR_DEBUGGER:Ljava/lang/String; = "wait_for_debugger"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WALLPAPER_ACTIVITY:Ljava/lang/String; = "wallpaper_activity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_MAX_DHCP_RETRY_COUNT:Ljava/lang/String; = "wifi_max_dhcp_retry_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS:Ljava/lang/String; = "wifi_mobile_data_transition_wakelock_timeout_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON:Ljava/lang/String; = "wifi_networks_available_notification_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NETWORKS_AVAILABLE_NOTIFICATION_SETTINGS:Ljava/lang/String; = "wifi_networks_available_notification_settings"

.field public static final WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY:Ljava/lang/String; = "wifi_networks_available_repeat_delay"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NUM_OPEN_NETWORKS_KEPT:Ljava/lang/String; = "wifi_num_open_networks_kept"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_ON:Ljava/lang/String; = "wifi_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY:Ljava/lang/String; = "wifi_sleep_policy"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY_DEFAULT:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY_NEVER:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_STATIC_DNS1:Ljava/lang/String; = "wifi_static_dns1"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_STATIC_DNS2:Ljava/lang/String; = "wifi_static_dns2"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_STATIC_GATEWAY:Ljava/lang/String; = "wifi_static_gateway"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_STATIC_IP:Ljava/lang/String; = "wifi_static_ip"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_STATIC_NETMASK:Ljava/lang/String; = "wifi_static_netmask"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_USE_STATIC_IP:Ljava/lang/String; = "wifi_use_static_ip"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE:Ljava/lang/String; = "wifi_watchdog_acceptable_packet_loss_percentage"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_AP_COUNT:Ljava/lang/String; = "wifi_watchdog_ap_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS:Ljava/lang/String; = "wifi_watchdog_background_check_delay_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED:Ljava/lang/String; = "wifi_watchdog_background_check_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS:Ljava/lang/String; = "wifi_watchdog_background_check_timeout_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT:Ljava/lang/String; = "wifi_watchdog_initial_ignored_ping_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_MAX_AP_CHECKS:Ljava/lang/String; = "wifi_watchdog_max_ap_checks"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_ON:Ljava/lang/String; = "wifi_watchdog_on"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_PING_COUNT:Ljava/lang/String; = "wifi_watchdog_ping_count"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_PING_DELAY_MS:Ljava/lang/String; = "wifi_watchdog_ping_delay_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_PING_TIMEOUT_MS:Ljava/lang/String; = "wifi_watchdog_ping_timeout_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WINDOW_ANIMATION_SCALE:Ljava/lang/String; = "window_animation_scale"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WINDOW_ORIENTATION_LISTENER_LOG:Ljava/lang/String; = "window_orientation_listener_log"

.field private static final sNameValueCache:Landroid/provider/Settings$NameValueCache;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x3

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 912
    const-string v0, "content://settings/system"

    #@7
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@d
    .line 915
    new-instance v0, Landroid/provider/Settings$NameValueCache;

    #@f
    const-string/jumbo v1, "sys.settings_system_version"

    #@12
    sget-object v2, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@14
    const-string v3, "GET_system"

    #@16
    const-string v4, "PUT_system"

    #@18
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/provider/Settings$NameValueCache;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    sput-object v0, Landroid/provider/Settings$System;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    #@1d
    .line 923
    new-instance v0, Ljava/util/HashSet;

    #@1f
    const/16 v1, 0x1e

    #@21
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    #@24
    sput-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@26
    .line 924
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@28
    const-string v1, "android_id"

    #@2a
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2d
    .line 925
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@2f
    const-string v1, "http_proxy"

    #@31
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@34
    .line 926
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@36
    const-string/jumbo v1, "location_providers_allowed"

    #@39
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3c
    .line 927
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@3e
    const-string/jumbo v1, "lock_biometric_weak_flags"

    #@41
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@44
    .line 928
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@46
    const-string/jumbo v1, "lock_pattern_autolock"

    #@49
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@4c
    .line 929
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@4e
    const-string/jumbo v1, "lock_pattern_visible_pattern"

    #@51
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@54
    .line 930
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@56
    const-string/jumbo v1, "lock_pattern_tactile_feedback_enabled"

    #@59
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@5c
    .line 931
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@5e
    const-string/jumbo v1, "logging_id"

    #@61
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@64
    .line 932
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@66
    const-string/jumbo v1, "parental_control_enabled"

    #@69
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@6c
    .line 933
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@6e
    const-string/jumbo v1, "parental_control_last_update"

    #@71
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@74
    .line 934
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@76
    const-string/jumbo v1, "parental_control_redirect_url"

    #@79
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@7c
    .line 935
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@7e
    const-string/jumbo v1, "settings_classname"

    #@81
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@84
    .line 936
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@86
    const-string/jumbo v1, "use_google_mail"

    #@89
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@8c
    .line 937
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@8e
    const-string/jumbo v1, "wifi_networks_available_notification_on"

    #@91
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@94
    .line 938
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@96
    const-string/jumbo v1, "wifi_networks_available_repeat_delay"

    #@99
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@9c
    .line 939
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@9e
    const-string/jumbo v1, "wifi_num_open_networks_kept"

    #@a1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@a4
    .line 940
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@a6
    const-string/jumbo v1, "wifi_on"

    #@a9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@ac
    .line 941
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@ae
    const-string/jumbo v1, "wifi_watchdog_acceptable_packet_loss_percentage"

    #@b1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@b4
    .line 942
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@b6
    const-string/jumbo v1, "wifi_watchdog_ap_count"

    #@b9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@bc
    .line 943
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@be
    const-string/jumbo v1, "wifi_watchdog_background_check_delay_ms"

    #@c1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@c4
    .line 944
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@c6
    const-string/jumbo v1, "wifi_watchdog_background_check_enabled"

    #@c9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@cc
    .line 945
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@ce
    const-string/jumbo v1, "wifi_watchdog_background_check_timeout_ms"

    #@d1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@d4
    .line 946
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@d6
    const-string/jumbo v1, "wifi_watchdog_initial_ignored_ping_count"

    #@d9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@dc
    .line 947
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@de
    const-string/jumbo v1, "wifi_watchdog_max_ap_checks"

    #@e1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@e4
    .line 948
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@e6
    const-string/jumbo v1, "wifi_watchdog_on"

    #@e9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@ec
    .line 949
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@ee
    const-string/jumbo v1, "wifi_watchdog_ping_count"

    #@f1
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@f4
    .line 950
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@f6
    const-string/jumbo v1, "wifi_watchdog_ping_delay_ms"

    #@f9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@fc
    .line 951
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@fe
    const-string/jumbo v1, "wifi_watchdog_ping_timeout_ms"

    #@101
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@104
    .line 957
    new-instance v0, Ljava/util/HashSet;

    #@106
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@109
    sput-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@10b
    .line 958
    new-instance v0, Ljava/util/HashSet;

    #@10d
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@110
    sput-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@112
    .line 962
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@114
    const-string v1, "adb_enabled"

    #@116
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@119
    .line 963
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@11b
    const-string v1, "bluetooth_on"

    #@11d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@120
    .line 964
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@122
    const-string v1, "data_roaming"

    #@124
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@127
    .line 965
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@129
    const-string v1, "device_provisioned"

    #@12b
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@12e
    .line 966
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@130
    const-string v1, "install_non_market_apps"

    #@132
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@135
    .line 967
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@137
    const-string/jumbo v1, "usb_mass_storage_enabled"

    #@13a
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@13d
    .line 968
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@13f
    const-string v1, "http_proxy"

    #@141
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@144
    .line 971
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@146
    const-string v1, "airplane_mode_on"

    #@148
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@14b
    .line 972
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@14d
    const-string v1, "airplane_mode_radios"

    #@14f
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@152
    .line 973
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@154
    const-string v1, "airplane_mode_toggleable_radios"

    #@156
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@159
    .line 974
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@15b
    const-string v1, "auto_time"

    #@15d
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@160
    .line 975
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@162
    const-string v1, "auto_time_zone"

    #@164
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@167
    .line 976
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@169
    const-string v1, "car_dock_sound"

    #@16b
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@16e
    .line 977
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@170
    const-string v1, "car_undock_sound"

    #@172
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@175
    .line 978
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@177
    const-string v1, "desk_dock_sound"

    #@179
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@17c
    .line 979
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@17e
    const-string v1, "desk_undock_sound"

    #@180
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@183
    .line 980
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@185
    const-string v1, "dock_sounds_enabled"

    #@187
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@18a
    .line 981
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@18c
    const-string/jumbo v1, "lock_sound"

    #@18f
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@192
    .line 982
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@194
    const-string/jumbo v1, "unlock_sound"

    #@197
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@19a
    .line 983
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@19c
    const-string/jumbo v1, "low_battery_sound"

    #@19f
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1a2
    .line 984
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1a4
    const-string/jumbo v1, "power_sounds_enabled"

    #@1a7
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1aa
    .line 985
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1ac
    const-string/jumbo v1, "stay_on_while_plugged_in"

    #@1af
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1b2
    .line 986
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1b4
    const-string/jumbo v1, "wifi_sleep_policy"

    #@1b7
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1ba
    .line 987
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1bc
    const-string/jumbo v1, "mode_ringer"

    #@1bf
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1c2
    .line 988
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1c4
    const-string/jumbo v1, "window_animation_scale"

    #@1c7
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1ca
    .line 989
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1cc
    const-string/jumbo v1, "transition_animation_scale"

    #@1cf
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1d2
    .line 990
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1d4
    const-string v1, "animator_duration_scale"

    #@1d6
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1d9
    .line 991
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1db
    const-string v1, "fancy_ime_animations"

    #@1dd
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1e0
    .line 992
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1e2
    const-string v1, "compatibility_mode"

    #@1e4
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1e7
    .line 993
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1e9
    const-string v1, "emergency_tone"

    #@1eb
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1ee
    .line 994
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1f0
    const-string v1, "call_auto_retry"

    #@1f2
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1f5
    .line 995
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1f7
    const-string v1, "debug_app"

    #@1f9
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@1fc
    .line 996
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@1fe
    const-string/jumbo v1, "wait_for_debugger"

    #@201
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@204
    .line 997
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@206
    const-string/jumbo v1, "show_processes"

    #@209
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@20c
    .line 998
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@20e
    const-string v1, "always_finish_activities"

    #@210
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@213
    .line 1000
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@215
    const-string/jumbo v1, "roaming_mode_selected_system"

    #@218
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@21b
    .line 1001
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@21d
    const-string/jumbo v1, "roaming_mode_domestic_voice"

    #@220
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@223
    .line 1002
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@225
    const-string/jumbo v1, "roaming_mode_domestic_voice_forced"

    #@228
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@22b
    .line 1003
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@22d
    const-string/jumbo v1, "roaming_mode_domestic_data"

    #@230
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@233
    .line 1004
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@235
    const-string/jumbo v1, "roaming_mode_domestic_data_forced"

    #@238
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@23b
    .line 1005
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@23d
    const-string/jumbo v1, "roaming_mode_international_voice"

    #@240
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@243
    .line 1006
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@245
    const-string/jumbo v1, "roaming_mode_international_voice_forced"

    #@248
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@24b
    .line 1007
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@24d
    const-string/jumbo v1, "roaming_mode_international_data"

    #@250
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@253
    .line 1008
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@255
    const-string/jumbo v1, "roaming_mode_international_data_forced"

    #@258
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@25b
    .line 1009
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@25d
    const-string/jumbo v1, "rguard_domestic_voice"

    #@260
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@263
    .line 1010
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@265
    const-string/jumbo v1, "rguard_domestic_voice_forced"

    #@268
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@26b
    .line 1011
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@26d
    const-string/jumbo v1, "rguard_domestic_data"

    #@270
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@273
    .line 1012
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@275
    const-string/jumbo v1, "rguard_domestic_data_forced"

    #@278
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@27b
    .line 1013
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@27d
    const-string/jumbo v1, "rguard_international_voice"

    #@280
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@283
    .line 1014
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@285
    const-string/jumbo v1, "rguard_international_voice_forced"

    #@288
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@28b
    .line 1015
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@28d
    const-string/jumbo v1, "rguard_international_data"

    #@290
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@293
    .line 1016
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@295
    const-string/jumbo v1, "rguard_international_data_forced"

    #@298
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@29b
    .line 1017
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@29d
    const-string v1, "cbox_international_sms"

    #@29f
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2a2
    .line 1018
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@2a4
    const-string v1, "cbox_international_sms_forced"

    #@2a6
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@2a9
    .line 1916
    const/16 v0, 0xb

    #@2ab
    new-array v0, v0, [Ljava/lang/String;

    #@2ad
    const-string/jumbo v1, "volume_voice"

    #@2b0
    aput-object v1, v0, v5

    #@2b2
    const-string/jumbo v1, "volume_system"

    #@2b5
    aput-object v1, v0, v6

    #@2b7
    const-string/jumbo v1, "volume_ring"

    #@2ba
    aput-object v1, v0, v7

    #@2bc
    const-string/jumbo v1, "volume_music"

    #@2bf
    aput-object v1, v0, v8

    #@2c1
    const-string/jumbo v1, "volume_alarm"

    #@2c4
    aput-object v1, v0, v9

    #@2c6
    const/4 v1, 0x5

    #@2c7
    const-string/jumbo v2, "volume_notification"

    #@2ca
    aput-object v2, v0, v1

    #@2cc
    const/4 v1, 0x6

    #@2cd
    const-string/jumbo v2, "volume_bluetooth_sco"

    #@2d0
    aput-object v2, v0, v1

    #@2d2
    const/4 v1, 0x7

    #@2d3
    const-string v2, ""

    #@2d5
    aput-object v2, v0, v1

    #@2d7
    const/16 v1, 0x8

    #@2d9
    const-string v2, ""

    #@2db
    aput-object v2, v0, v1

    #@2dd
    const/16 v1, 0x9

    #@2df
    const-string v2, ""

    #@2e1
    aput-object v2, v0, v1

    #@2e3
    const/16 v1, 0xa

    #@2e5
    const-string/jumbo v2, "volume_fm"

    #@2e8
    aput-object v2, v0, v1

    #@2ea
    sput-object v0, Landroid/provider/Settings$System;->VOLUME_SETTINGS:[Ljava/lang/String;

    #@2ec
    .line 1952
    const-string/jumbo v0, "ringtone_sim2"

    #@2ef
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@2f2
    move-result-object v0

    #@2f3
    sput-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_SIM2_URI:Landroid/net/Uri;

    #@2f5
    .line 1964
    const-string/jumbo v0, "notification_sound_sim2"

    #@2f8
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@2fb
    move-result-object v0

    #@2fc
    sput-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_SIM2_URI:Landroid/net/Uri;

    #@2fe
    .line 1983
    const-string/jumbo v0, "ringtone_videocall"

    #@301
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@304
    move-result-object v0

    #@305
    sput-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_VIDEOCALL_URI:Landroid/net/Uri;

    #@307
    .line 1994
    const-string/jumbo v0, "ringtone"

    #@30a
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@30d
    move-result-object v0

    #@30e
    sput-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    #@310
    .line 2010
    const-string/jumbo v0, "notification_sound"

    #@313
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@316
    move-result-object v0

    #@317
    sput-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@319
    .line 2026
    const-string v0, "alarm_alert"

    #@31b
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@31e
    move-result-object v0

    #@31f
    sput-object v0, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    #@321
    .line 2652
    const/16 v0, 0x3f

    #@323
    new-array v0, v0, [Ljava/lang/String;

    #@325
    const-string/jumbo v1, "stay_on_while_plugged_in"

    #@328
    aput-object v1, v0, v5

    #@32a
    const-string/jumbo v1, "wifi_use_static_ip"

    #@32d
    aput-object v1, v0, v6

    #@32f
    const-string/jumbo v1, "wifi_static_ip"

    #@332
    aput-object v1, v0, v7

    #@334
    const-string/jumbo v1, "wifi_static_gateway"

    #@337
    aput-object v1, v0, v8

    #@339
    const-string/jumbo v1, "wifi_static_netmask"

    #@33c
    aput-object v1, v0, v9

    #@33e
    const/4 v1, 0x5

    #@33f
    const-string/jumbo v2, "wifi_static_dns1"

    #@342
    aput-object v2, v0, v1

    #@344
    const/4 v1, 0x6

    #@345
    const-string/jumbo v2, "wifi_static_dns2"

    #@348
    aput-object v2, v0, v1

    #@34a
    const/4 v1, 0x7

    #@34b
    const-string v2, "bluetooth_discoverability"

    #@34d
    aput-object v2, v0, v1

    #@34f
    const/16 v1, 0x8

    #@351
    const-string v2, "bluetooth_discoverability_timeout"

    #@353
    aput-object v2, v0, v1

    #@355
    const/16 v1, 0x9

    #@357
    const-string v2, "dim_screen"

    #@359
    aput-object v2, v0, v1

    #@35b
    const/16 v1, 0xa

    #@35d
    const-string/jumbo v2, "screen_off_timeout"

    #@360
    aput-object v2, v0, v1

    #@362
    const/16 v1, 0xb

    #@364
    const-string/jumbo v2, "screen_brightness"

    #@367
    aput-object v2, v0, v1

    #@369
    const/16 v1, 0xc

    #@36b
    const-string/jumbo v2, "screen_brightness_mode"

    #@36e
    aput-object v2, v0, v1

    #@370
    const/16 v1, 0xd

    #@372
    const-string/jumbo v2, "screen_auto_brightness_adj"

    #@375
    aput-object v2, v0, v1

    #@377
    const/16 v1, 0xe

    #@379
    const-string/jumbo v2, "vibrate_input_devices"

    #@37c
    aput-object v2, v0, v1

    #@37e
    const/16 v1, 0xf

    #@380
    const-string/jumbo v2, "mode_ringer"

    #@383
    aput-object v2, v0, v1

    #@385
    const/16 v1, 0x10

    #@387
    const-string/jumbo v2, "mode_ringer_streams_affected"

    #@38a
    aput-object v2, v0, v1

    #@38c
    const/16 v1, 0x11

    #@38e
    const-string/jumbo v2, "mute_streams_affected"

    #@391
    aput-object v2, v0, v1

    #@393
    const/16 v1, 0x12

    #@395
    const-string/jumbo v2, "volume_voice"

    #@398
    aput-object v2, v0, v1

    #@39a
    const/16 v1, 0x13

    #@39c
    const-string/jumbo v2, "volume_system"

    #@39f
    aput-object v2, v0, v1

    #@3a1
    const/16 v1, 0x14

    #@3a3
    const-string/jumbo v2, "volume_ring"

    #@3a6
    aput-object v2, v0, v1

    #@3a8
    const/16 v1, 0x15

    #@3aa
    const-string/jumbo v2, "volume_music"

    #@3ad
    aput-object v2, v0, v1

    #@3af
    const/16 v1, 0x16

    #@3b1
    const-string/jumbo v2, "volume_fm"

    #@3b4
    aput-object v2, v0, v1

    #@3b6
    const/16 v1, 0x17

    #@3b8
    const-string/jumbo v2, "volume_alarm"

    #@3bb
    aput-object v2, v0, v1

    #@3bd
    const/16 v1, 0x18

    #@3bf
    const-string/jumbo v2, "volume_notification"

    #@3c2
    aput-object v2, v0, v1

    #@3c4
    const/16 v1, 0x19

    #@3c6
    const-string/jumbo v2, "volume_bluetooth_sco"

    #@3c9
    aput-object v2, v0, v1

    #@3cb
    const/16 v1, 0x1a

    #@3cd
    const-string/jumbo v2, "volume_voice_last_audible"

    #@3d0
    aput-object v2, v0, v1

    #@3d2
    const/16 v1, 0x1b

    #@3d4
    const-string/jumbo v2, "volume_system_last_audible"

    #@3d7
    aput-object v2, v0, v1

    #@3d9
    const/16 v1, 0x1c

    #@3db
    const-string/jumbo v2, "volume_ring_last_audible"

    #@3de
    aput-object v2, v0, v1

    #@3e0
    const/16 v1, 0x1d

    #@3e2
    const-string/jumbo v2, "volume_music_last_audible"

    #@3e5
    aput-object v2, v0, v1

    #@3e7
    const/16 v1, 0x1e

    #@3e9
    const-string/jumbo v2, "volume_alarm_last_audible"

    #@3ec
    aput-object v2, v0, v1

    #@3ee
    const/16 v1, 0x1f

    #@3f0
    const-string/jumbo v2, "volume_notification_last_audible"

    #@3f3
    aput-object v2, v0, v1

    #@3f5
    const/16 v1, 0x20

    #@3f7
    const-string/jumbo v2, "volume_bluetooth_sco_last_audible"

    #@3fa
    aput-object v2, v0, v1

    #@3fc
    const/16 v1, 0x21

    #@3fe
    const-string v2, "auto_replace"

    #@400
    aput-object v2, v0, v1

    #@402
    const/16 v1, 0x22

    #@404
    const-string v2, "auto_caps"

    #@406
    aput-object v2, v0, v1

    #@408
    const/16 v1, 0x23

    #@40a
    const-string v2, "auto_punctuate"

    #@40c
    aput-object v2, v0, v1

    #@40e
    const/16 v1, 0x24

    #@410
    const-string/jumbo v2, "show_password"

    #@413
    aput-object v2, v0, v1

    #@415
    const/16 v1, 0x25

    #@417
    const-string v2, "auto_time"

    #@419
    aput-object v2, v0, v1

    #@41b
    const/16 v1, 0x26

    #@41d
    const-string v2, "auto_time_zone"

    #@41f
    aput-object v2, v0, v1

    #@421
    const/16 v1, 0x27

    #@423
    const-string/jumbo v2, "time_12_24"

    #@426
    aput-object v2, v0, v1

    #@428
    const/16 v1, 0x28

    #@42a
    const-string v2, "date_format"

    #@42c
    aput-object v2, v0, v1

    #@42e
    const/16 v1, 0x29

    #@430
    const-string v2, "dtmf_tone"

    #@432
    aput-object v2, v0, v1

    #@434
    const/16 v1, 0x2a

    #@436
    const-string v2, "dtmf_tone_type"

    #@438
    aput-object v2, v0, v1

    #@43a
    const/16 v1, 0x2b

    #@43c
    const-string v2, "hearing_aid"

    #@43e
    aput-object v2, v0, v1

    #@440
    const/16 v1, 0x2c

    #@442
    const-string/jumbo v2, "tty_mode"

    #@445
    aput-object v2, v0, v1

    #@447
    const/16 v1, 0x2d

    #@449
    const-string/jumbo v2, "sound_effects_enabled"

    #@44c
    aput-object v2, v0, v1

    #@44e
    const/16 v1, 0x2e

    #@450
    const-string v2, "haptic_feedback_enabled"

    #@452
    aput-object v2, v0, v1

    #@454
    const/16 v1, 0x2f

    #@456
    const-string/jumbo v2, "power_sounds_enabled"

    #@459
    aput-object v2, v0, v1

    #@45b
    const/16 v1, 0x30

    #@45d
    const-string v2, "dock_sounds_enabled"

    #@45f
    aput-object v2, v0, v1

    #@461
    const/16 v1, 0x31

    #@463
    const-string/jumbo v2, "lockscreen_sounds_enabled"

    #@466
    aput-object v2, v0, v1

    #@468
    const/16 v1, 0x32

    #@46a
    const-string/jumbo v2, "show_web_suggestions"

    #@46d
    aput-object v2, v0, v1

    #@46f
    const/16 v1, 0x33

    #@471
    const-string/jumbo v2, "notification_light_pulse"

    #@474
    aput-object v2, v0, v1

    #@476
    const/16 v1, 0x34

    #@478
    const-string/jumbo v2, "sip_call_options"

    #@47b
    aput-object v2, v0, v1

    #@47d
    const/16 v1, 0x35

    #@47f
    const-string/jumbo v2, "sip_receive_calls"

    #@482
    aput-object v2, v0, v1

    #@484
    const/16 v1, 0x36

    #@486
    const-string/jumbo v2, "lg_local_plus_dial"

    #@489
    aput-object v2, v0, v1

    #@48b
    const/16 v1, 0x37

    #@48d
    const-string/jumbo v2, "lg_int_plus_dial"

    #@490
    aput-object v2, v0, v1

    #@492
    const/16 v1, 0x38

    #@494
    const-string/jumbo v2, "proximity_sensor_check"

    #@497
    aput-object v2, v0, v1

    #@499
    const/16 v1, 0x39

    #@49b
    const-string/jumbo v2, "pointer_speed"

    #@49e
    aput-object v2, v0, v1

    #@4a0
    const/16 v1, 0x3a

    #@4a2
    const-string/jumbo v2, "wifi_networks_available_notification_settings"

    #@4a5
    aput-object v2, v0, v1

    #@4a7
    const/16 v1, 0x3b

    #@4a9
    const-string v2, "hide_display"

    #@4ab
    aput-object v2, v0, v1

    #@4ad
    const/16 v1, 0x3c

    #@4af
    const-string/jumbo v2, "take_screenshot"

    #@4b2
    aput-object v2, v0, v1

    #@4b4
    const/16 v1, 0x3d

    #@4b6
    const-string/jumbo v2, "multitasking_slide_aside"

    #@4b9
    aput-object v2, v0, v1

    #@4bb
    const/16 v1, 0x3e

    #@4bd
    const-string/jumbo v2, "vibrate_when_ringing"

    #@4c0
    aput-object v2, v0, v1

    #@4c2
    sput-object v0, Landroid/provider/Settings$System;->SETTINGS_TO_BACKUP:[Ljava/lang/String;

    #@4c4
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 906
    invoke-direct {p0}, Landroid/provider/Settings$NameValueTable;-><init>()V

    #@3
    return-void
.end method

.method public static clearConfiguration(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "inoutConfig"

    #@0
    .prologue
    .line 1403
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/content/res/Configuration;->fontScale:F

    #@3
    .line 1404
    return-void
.end method

.method public static getConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "cr"
    .parameter "outConfig"

    #@0
    .prologue
    .line 1385
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getConfigurationForUser(Landroid/content/ContentResolver;Landroid/content/res/Configuration;I)V

    #@7
    .line 1386
    return-void
.end method

.method public static getConfigurationForUser(Landroid/content/ContentResolver;Landroid/content/res/Configuration;I)V
    .registers 5
    .parameter "cr"
    .parameter "outConfig"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1391
    const-string v0, "font_scale"

    #@2
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    #@4
    invoke-static {p0, v0, v1, p2}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    #@7
    move-result v0

    #@8
    iput v0, p1, Landroid/content/res/Configuration;->fontScale:F

    #@a
    .line 1393
    iget v0, p1, Landroid/content/res/Configuration;->fontScale:F

    #@c
    const/4 v1, 0x0

    #@d
    cmpg-float v0, v0, v1

    #@f
    if-gez v0, :cond_15

    #@11
    .line 1394
    const/high16 v0, 0x3f80

    #@13
    iput v0, p1, Landroid/content/res/Configuration;->fontScale:F

    #@15
    .line 1396
    :cond_15
    return-void
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    .registers 3
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1328
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 1294
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1300
    invoke-static {p0, p1, p3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1302
    .local v1, v:Ljava/lang/String;
    if-eqz v1, :cond_a

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_b

    #@9
    move-result p2

    #@a
    .line 1304
    .end local p2
    :cond_a
    :goto_a
    return p2

    #@b
    .line 1303
    .restart local p2
    :catch_b
    move-exception v0

    #@c
    .line 1304
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_a
.end method

.method public static getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)F
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1334
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1335
    .local v1, v:Ljava/lang/String;
    if-nez v1, :cond_c

    #@6
    .line 1336
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@8
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 1339
    :cond_c
    :try_start_c
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_f
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_f} :catch_11

    #@f
    move-result v2

    #@10
    return v2

    #@11
    .line 1340
    :catch_11
    move-exception v0

    #@12
    .line 1341
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@14
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .registers 3
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1155
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 1122
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1161
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1163
    .local v1, v:Ljava/lang/String;
    :try_start_4
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    return v2

    #@9
    .line 1164
    :catch_9
    move-exception v0

    #@a
    .line 1165
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@c
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2
.end method

.method public static getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1127
    invoke-static {p0, p1, p3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1129
    .local v1, v:Ljava/lang/String;
    if-eqz v1, :cond_a

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_b

    #@9
    move-result p2

    #@a
    .line 1131
    .end local p2
    :cond_a
    :goto_a
    return p2

    #@b
    .line 1130
    .restart local p2
    :catch_b
    move-exception v0

    #@c
    .line 1131
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_a
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    .registers 4
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1242
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 1207
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, p3, v0}, Landroid/provider/Settings$System;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public static getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)J
    .registers 7
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1248
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1250
    .local v1, valString:Ljava/lang/String;
    :try_start_4
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_7} :catch_9

    #@7
    move-result-wide v2

    #@8
    return-wide v2

    #@9
    .line 1251
    :catch_9
    move-exception v0

    #@a
    .line 1252
    .local v0, e:Ljava/lang/NumberFormatException;
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    #@c
    invoke-direct {v2, p1}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@f
    throw v2
.end method

.method public static getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J
    .registers 9
    .parameter "cr"
    .parameter "name"
    .parameter "def"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1213
    invoke-static {p0, p1, p4}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1216
    .local v1, valString:Ljava/lang/String;
    if-eqz v1, :cond_b

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_d

    #@9
    move-result-wide v2

    #@a
    .line 1220
    .local v2, value:J
    :goto_a
    return-wide v2

    #@b
    .end local v2           #value:J
    :cond_b
    move-wide v2, p2

    #@c
    .line 1216
    goto :goto_a

    #@d
    .line 1217
    :catch_d
    move-exception v0

    #@e
    .line 1218
    .local v0, e:Ljava/lang/NumberFormatException;
    move-wide v2, p2

    #@f
    .restart local v2       #value:J
    goto :goto_a
.end method

.method public static getMovedKeys(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1025
    .local p0, outKeySet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2
    invoke-virtual {p0, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@5
    .line 1026
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@7
    invoke-virtual {p0, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@a
    .line 1027
    return-void
.end method

.method public static getNonLegacyMovedKeys(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1031
    .local p0, outKeySet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@2
    invoke-virtual {p0, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    #@5
    .line 1032
    return-void
.end method

.method public static getShowGTalkServiceStatus(Landroid/content/ContentResolver;)Z
    .registers 2
    .parameter "cr"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1432
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, v0}, Landroid/provider/Settings$System;->getShowGTalkServiceStatusForUser(Landroid/content/ContentResolver;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getShowGTalkServiceStatusForUser(Landroid/content/ContentResolver;I)Z
    .registers 4
    .parameter "cr"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1441
    const-string v1, "SHOW_GTALK_SERVICE_STATUS"

    #@3
    invoke-static {p0, v1, v0, p1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    const/4 v0, 0x1

    #@a
    :cond_a
    return v0
.end method

.method public static getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "resolver"
    .parameter "name"

    #@0
    .prologue
    .line 1041
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
    .registers 6
    .parameter "resolver"
    .parameter "name"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1047
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_31

    #@8
    .line 1048
    const-string v0, "Settings"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Setting "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " has moved from android.provider.Settings.System"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " to android.provider.Settings.Secure, returning read-only value."

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1050
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 1057
    :goto_30
    return-object v0

    #@31
    .line 1052
    :cond_31
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@33
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-nez v0, :cond_41

    #@39
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@3b
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_6a

    #@41
    .line 1053
    :cond_41
    const-string v0, "Settings"

    #@43
    new-instance v1, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v2, "Setting "

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    const-string v2, " has moved from android.provider.Settings.System"

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    const-string v2, " to android.provider.Settings.Global, returning read-only value."

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 1055
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    goto :goto_30

    #@6a
    .line 1057
    :cond_6a
    sget-object v0, Landroid/provider/Settings$System;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    #@6c
    invoke-virtual {v0, p0, p1, p2}, Landroid/provider/Settings$NameValueCache;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    goto :goto_30
.end method

.method public static getUriFor(Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 1094
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_33

    #@8
    .line 1095
    const-string v0, "Settings"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Setting "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " has moved from android.provider.Settings.System"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " to android.provider.Settings.Secure, returning Secure URI."

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1097
    sget-object v0, Landroid/provider/Settings$Secure;->CONTENT_URI:Landroid/net/Uri;

    #@2e
    invoke-static {v0, p0}, Landroid/provider/Settings$Secure;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@31
    move-result-object v0

    #@32
    .line 1104
    :goto_32
    return-object v0

    #@33
    .line 1099
    :cond_33
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@35
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@38
    move-result v0

    #@39
    if-nez v0, :cond_43

    #@3b
    sget-object v0, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@3d
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_6e

    #@43
    .line 1100
    :cond_43
    const-string v0, "Settings"

    #@45
    new-instance v1, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v2, "Setting "

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    const-string v2, " has moved from android.provider.Settings.System"

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    const-string v2, " to android.provider.Settings.Global, returning read-only global URI."

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 1102
    sget-object v0, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    #@69
    invoke-static {v0, p0}, Landroid/provider/Settings$Global;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@6c
    move-result-object v0

    #@6d
    goto :goto_32

    #@6e
    .line 1104
    :cond_6e
    sget-object v0, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@70
    invoke-static {v0, p0}, Landroid/provider/Settings$System;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@73
    move-result-object v0

    #@74
    goto :goto_32
.end method

.method public static hasInterestingConfigurationChanges(I)Z
    .registers 2
    .parameter "changes"

    #@0
    .prologue
    .line 1426
    const/high16 v0, 0x4000

    #@2
    and-int/2addr v0, p0

    #@3
    if-eqz v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public static putConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)Z
    .registers 3
    .parameter "cr"
    .parameter "config"

    #@0
    .prologue
    .line 1415
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->putConfigurationForUser(Landroid/content/ContentResolver;Landroid/content/res/Configuration;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putConfigurationForUser(Landroid/content/ContentResolver;Landroid/content/res/Configuration;I)Z
    .registers 5
    .parameter "cr"
    .parameter "config"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1421
    const-string v0, "font_scale"

    #@2
    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    #@4
    invoke-static {p0, v0, v1, p2}, Landroid/provider/Settings$System;->putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 1367
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$System;->putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1373
    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 1183
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1189
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 1270
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, p3, v0}, Landroid/provider/Settings$System;->putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)Z
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1276
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0, p4}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 1068
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 8
    .parameter "resolver"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1074
    sget-object v1, Landroid/provider/Settings$System;->MOVED_TO_SECURE:Ljava/util/HashSet;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_2e

    #@9
    .line 1075
    const-string v1, "Settings"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Setting "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, " has moved from android.provider.Settings.System"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, " to android.provider.Settings.Secure, value is unchanged."

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1084
    :goto_2d
    return v0

    #@2e
    .line 1079
    :cond_2e
    sget-object v1, Landroid/provider/Settings$System;->MOVED_TO_GLOBAL:Ljava/util/HashSet;

    #@30
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@33
    move-result v1

    #@34
    if-nez v1, :cond_3e

    #@36
    sget-object v1, Landroid/provider/Settings$System;->MOVED_TO_SECURE_THEN_GLOBAL:Ljava/util/HashSet;

    #@38
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_63

    #@3e
    .line 1080
    :cond_3e
    const-string v1, "Settings"

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "Setting "

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    const-string v3, " has moved from android.provider.Settings.System"

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    const-string v3, " to android.provider.Settings.Global, value is unchanged."

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_2d

    #@63
    .line 1084
    :cond_63
    sget-object v0, Landroid/provider/Settings$System;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    #@65
    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/provider/Settings$NameValueCache;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@68
    move-result v0

    #@69
    goto :goto_2d
.end method

.method public static setShowGTalkServiceStatus(Landroid/content/ContentResolver;Z)V
    .registers 3
    .parameter "cr"
    .parameter "flag"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1447
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->setShowGTalkServiceStatusForUser(Landroid/content/ContentResolver;ZI)V

    #@7
    .line 1448
    return-void
.end method

.method public static setShowGTalkServiceStatusForUser(Landroid/content/ContentResolver;ZI)V
    .registers 5
    .parameter "cr"
    .parameter "flag"
    .parameter "userHandle"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1457
    const-string v1, "SHOW_GTALK_SERVICE_STATUS"

    #@2
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-static {p0, v1, v0, p2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@8
    .line 1458
    return-void

    #@9
    .line 1457
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method
