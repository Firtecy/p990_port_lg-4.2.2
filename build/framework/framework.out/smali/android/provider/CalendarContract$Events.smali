.class public final Landroid/provider/CalendarContract$Events;
.super Ljava/lang/Object;
.source "CalendarContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/CalendarContract$SyncColumns;
.implements Landroid/provider/CalendarContract$EventsColumns;
.implements Landroid/provider/CalendarContract$CalendarColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/CalendarContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Events"
.end annotation


# static fields
.field public static final CONTENT_EXCEPTION_URI:Landroid/net/Uri; = null

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field private static final DEFAULT_SORT_ORDER:Ljava/lang/String; = ""

.field public static PROVIDER_WRITABLE_COLUMNS:[Ljava/lang/String;

.field public static final SYNC_WRITABLE_COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 1626
    const-string v0, "content://com.android.calendar/events"

    #@7
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    #@d
    .line 1634
    const-string v0, "content://com.android.calendar/exception"

    #@f
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v0

    #@13
    sput-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_EXCEPTION_URI:Landroid/net/Uri;

    #@15
    .line 1654
    const/16 v0, 0x18

    #@17
    new-array v0, v0, [Ljava/lang/String;

    #@19
    const-string v1, "account_name"

    #@1b
    aput-object v1, v0, v3

    #@1d
    const-string v1, "account_type"

    #@1f
    aput-object v1, v0, v4

    #@21
    const-string v1, "cal_sync1"

    #@23
    aput-object v1, v0, v5

    #@25
    const-string v1, "cal_sync2"

    #@27
    aput-object v1, v0, v6

    #@29
    const-string v1, "cal_sync3"

    #@2b
    aput-object v1, v0, v7

    #@2d
    const/4 v1, 0x5

    #@2e
    const-string v2, "cal_sync4"

    #@30
    aput-object v2, v0, v1

    #@32
    const/4 v1, 0x6

    #@33
    const-string v2, "cal_sync5"

    #@35
    aput-object v2, v0, v1

    #@37
    const/4 v1, 0x7

    #@38
    const-string v2, "cal_sync6"

    #@3a
    aput-object v2, v0, v1

    #@3c
    const/16 v1, 0x8

    #@3e
    const-string v2, "cal_sync7"

    #@40
    aput-object v2, v0, v1

    #@42
    const/16 v1, 0x9

    #@44
    const-string v2, "cal_sync8"

    #@46
    aput-object v2, v0, v1

    #@48
    const/16 v1, 0xa

    #@4a
    const-string v2, "cal_sync9"

    #@4c
    aput-object v2, v0, v1

    #@4e
    const/16 v1, 0xb

    #@50
    const-string v2, "cal_sync10"

    #@52
    aput-object v2, v0, v1

    #@54
    const/16 v1, 0xc

    #@56
    const-string v2, "allowedReminders"

    #@58
    aput-object v2, v0, v1

    #@5a
    const/16 v1, 0xd

    #@5c
    const-string v2, "allowedAttendeeTypes"

    #@5e
    aput-object v2, v0, v1

    #@60
    const/16 v1, 0xe

    #@62
    const-string v2, "allowedAvailability"

    #@64
    aput-object v2, v0, v1

    #@66
    const/16 v1, 0xf

    #@68
    const-string v2, "calendar_access_level"

    #@6a
    aput-object v2, v0, v1

    #@6c
    const/16 v1, 0x10

    #@6e
    const-string v2, "calendar_color"

    #@70
    aput-object v2, v0, v1

    #@72
    const/16 v1, 0x11

    #@74
    const-string v2, "calendar_timezone"

    #@76
    aput-object v2, v0, v1

    #@78
    const/16 v1, 0x12

    #@7a
    const-string v2, "canModifyTimeZone"

    #@7c
    aput-object v2, v0, v1

    #@7e
    const/16 v1, 0x13

    #@80
    const-string v2, "canOrganizerRespond"

    #@82
    aput-object v2, v0, v1

    #@84
    const/16 v1, 0x14

    #@86
    const-string v2, "calendar_displayName"

    #@88
    aput-object v2, v0, v1

    #@8a
    const/16 v1, 0x15

    #@8c
    const-string v2, "canPartiallyUpdate"

    #@8e
    aput-object v2, v0, v1

    #@90
    const/16 v1, 0x16

    #@92
    const-string/jumbo v2, "sync_events"

    #@95
    aput-object v2, v0, v1

    #@97
    const/16 v1, 0x17

    #@99
    const-string/jumbo v2, "visible"

    #@9c
    aput-object v2, v0, v1

    #@9e
    sput-object v0, Landroid/provider/CalendarContract$Events;->PROVIDER_WRITABLE_COLUMNS:[Ljava/lang/String;

    #@a0
    .line 1688
    const/16 v0, 0xc

    #@a2
    new-array v0, v0, [Ljava/lang/String;

    #@a4
    const-string v1, "_sync_id"

    #@a6
    aput-object v1, v0, v3

    #@a8
    const-string v1, "dirty"

    #@aa
    aput-object v1, v0, v4

    #@ac
    const-string/jumbo v1, "sync_data1"

    #@af
    aput-object v1, v0, v5

    #@b1
    const-string/jumbo v1, "sync_data2"

    #@b4
    aput-object v1, v0, v6

    #@b6
    const-string/jumbo v1, "sync_data3"

    #@b9
    aput-object v1, v0, v7

    #@bb
    const/4 v1, 0x5

    #@bc
    const-string/jumbo v2, "sync_data4"

    #@bf
    aput-object v2, v0, v1

    #@c1
    const/4 v1, 0x6

    #@c2
    const-string/jumbo v2, "sync_data5"

    #@c5
    aput-object v2, v0, v1

    #@c7
    const/4 v1, 0x7

    #@c8
    const-string/jumbo v2, "sync_data6"

    #@cb
    aput-object v2, v0, v1

    #@cd
    const/16 v1, 0x8

    #@cf
    const-string/jumbo v2, "sync_data7"

    #@d2
    aput-object v2, v0, v1

    #@d4
    const/16 v1, 0x9

    #@d6
    const-string/jumbo v2, "sync_data8"

    #@d9
    aput-object v2, v0, v1

    #@db
    const/16 v1, 0xa

    #@dd
    const-string/jumbo v2, "sync_data9"

    #@e0
    aput-object v2, v0, v1

    #@e2
    const/16 v1, 0xb

    #@e4
    const-string/jumbo v2, "sync_data10"

    #@e7
    aput-object v2, v0, v1

    #@e9
    sput-object v0, Landroid/provider/CalendarContract$Events;->SYNC_WRITABLE_COLUMNS:[Ljava/lang/String;

    #@eb
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1640
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
