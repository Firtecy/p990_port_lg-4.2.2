.class Landroid/provider/MediaStore$InternalThumbnails;
.super Ljava/lang/Object;
.source "MediaStore.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/MediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalThumbnails"
.end annotation


# static fields
.field static final DEFAULT_GROUP_ID:I = 0x0

.field private static final FULL_SCREEN_KIND:I = 0x2

.field private static final MICRO_KIND:I = 0x3

.field private static final MINI_KIND:I = 0x1

.field private static final PROJECTION:[Ljava/lang/String;

.field private static sThumbBuf:[B

.field private static final sThumbBufLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 551
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "_id"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "_data"

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Landroid/provider/MediaStore$InternalThumbnails;->PROJECTION:[Ljava/lang/String;

    #@f
    .line 553
    new-instance v0, Ljava/lang/Object;

    #@11
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@14
    sput-object v0, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBufLock:Ljava/lang/Object;

    #@16
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 547
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static cancelThumbnailRequest(Landroid/content/ContentResolver;JLandroid/net/Uri;J)V
    .registers 13
    .parameter "cr"
    .parameter "origId"
    .parameter "baseUri"
    .parameter "groupId"

    #@0
    .prologue
    .line 590
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@3
    move-result-object v0

    #@4
    const-string v2, "cancel"

    #@6
    const-string v3, "1"

    #@8
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@b
    move-result-object v0

    #@c
    const-string/jumbo v2, "orig_id"

    #@f
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@16
    move-result-object v0

    #@17
    const-string v2, "group_id"

    #@19
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@24
    move-result-object v1

    #@25
    .line 593
    .local v1, cancelUri:Landroid/net/Uri;
    const/4 v6, 0x0

    #@26
    .line 595
    .local v6, c:Landroid/database/Cursor;
    :try_start_26
    sget-object v2, Landroid/provider/MediaStore$InternalThumbnails;->PROJECTION:[Ljava/lang/String;

    #@28
    const/4 v3, 0x0

    #@29
    const/4 v4, 0x0

    #@2a
    const/4 v5, 0x0

    #@2b
    move-object v0, p0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2f
    .catchall {:try_start_26 .. :try_end_2f} :catchall_36

    #@2f
    move-result-object v6

    #@30
    .line 598
    if-eqz v6, :cond_35

    #@32
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@35
    .line 600
    :cond_35
    return-void

    #@36
    .line 598
    :catchall_36
    move-exception v0

    #@37
    if-eqz v6, :cond_3c

    #@39
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3c
    :cond_3c
    throw v0
.end method

.method private static getMiniThumbFromFile(Landroid/database/Cursor;Landroid/net/Uri;Landroid/content/ContentResolver;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 14
    .parameter "c"
    .parameter "baseUri"
    .parameter "cr"
    .parameter "options"

    #@0
    .prologue
    .line 557
    const/4 v0, 0x0

    #@1
    .line 558
    .local v0, bitmap:Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    #@2
    .line 560
    .local v6, thumbUri:Landroid/net/Uri;
    const/4 v7, 0x0

    #@3
    :try_start_3
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getLong(I)J

    #@6
    move-result-wide v4

    #@7
    .line 561
    .local v4, thumbId:J
    const/4 v7, 0x1

    #@8
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    .line 562
    .local v2, filePath:Ljava/lang/String;
    invoke-static {p1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@f
    move-result-object v6

    #@10
    .line 563
    const-string/jumbo v7, "r"

    #@13
    invoke-virtual {p2, v6, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@16
    move-result-object v3

    #@17
    .line 564
    .local v3, pfdInput:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1a
    move-result-object v7

    #@1b
    const/4 v8, 0x0

    #@1c
    invoke-static {v7, v8, p3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@1f
    move-result-object v0

    #@20
    .line 566
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_23
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_23} :catch_24
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_23} :catch_48
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_23} :catch_6c

    #@23
    .line 575
    .end local v2           #filePath:Ljava/lang/String;
    .end local v3           #pfdInput:Landroid/os/ParcelFileDescriptor;
    .end local v4           #thumbId:J
    :goto_23
    return-object v0

    #@24
    .line 567
    :catch_24
    move-exception v1

    #@25
    .line 568
    .local v1, ex:Ljava/io/FileNotFoundException;
    const-string v7, "MediaStore"

    #@27
    new-instance v8, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v9, "couldn\'t open thumbnail "

    #@2e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v8

    #@32
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v8

    #@36
    const-string v9, "; "

    #@38
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v8

    #@3c
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v8

    #@40
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v8

    #@44
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_23

    #@48
    .line 569
    .end local v1           #ex:Ljava/io/FileNotFoundException;
    :catch_48
    move-exception v1

    #@49
    .line 570
    .local v1, ex:Ljava/io/IOException;
    const-string v7, "MediaStore"

    #@4b
    new-instance v8, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v9, "couldn\'t open thumbnail "

    #@52
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    const-string v9, "; "

    #@5c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v8

    #@60
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v8

    #@68
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    goto :goto_23

    #@6c
    .line 571
    .end local v1           #ex:Ljava/io/IOException;
    :catch_6c
    move-exception v1

    #@6d
    .line 572
    .local v1, ex:Ljava/lang/OutOfMemoryError;
    const-string v7, "MediaStore"

    #@6f
    new-instance v8, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v9, "failed to allocate memory for thumbnail "

    #@76
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v8

    #@7e
    const-string v9, "; "

    #@80
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v8

    #@84
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v8

    #@8c
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    goto :goto_23
.end method

.method static getThumbnail(Landroid/content/ContentResolver;JJILandroid/graphics/BitmapFactory$Options;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    .registers 28
    .parameter "cr"
    .parameter "origId"
    .parameter "groupId"
    .parameter "kind"
    .parameter "options"
    .parameter "baseUri"
    .parameter "isVideo"

    #@0
    .prologue
    .line 618
    const/4 v11, 0x0

    #@1
    .line 619
    .local v11, bitmap:Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    #@2
    .line 623
    .local v15, filePath:Ljava/lang/String;
    new-instance v18, Landroid/media/MiniThumbFile;

    #@4
    if-eqz p8, :cond_5d

    #@6
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@8
    :goto_8
    move-object/from16 v0, v18

    #@a
    invoke-direct {v0, v3}, Landroid/media/MiniThumbFile;-><init>(Landroid/net/Uri;)V

    #@d
    .line 626
    .local v18, thumbFile:Landroid/media/MiniThumbFile;
    invoke-virtual/range {v18 .. v19}, Landroid/media/MiniThumbFile;->setCR(Landroid/content/ContentResolver;)V

    #@10
    .line 628
    const/4 v12, 0x0

    #@11
    .line 630
    .local v12, c:Landroid/database/Cursor;
    :try_start_11
    move-object/from16 v0, v18

    #@13
    move-wide/from16 v1, p1

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/media/MiniThumbFile;->getMagic(J)J

    #@18
    move-result-wide v16

    #@19
    .line 631
    .local v16, magic:J
    const-wide/16 v7, 0x0

    #@1b
    cmp-long v3, v16, v7

    #@1d
    if-eqz v3, :cond_c1

    #@1f
    .line 632
    const/4 v3, 0x3

    #@20
    move/from16 v0, p5

    #@22
    if-ne v0, v3, :cond_75

    #@24
    .line 633
    sget-object v5, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBufLock:Ljava/lang/Object;

    #@26
    monitor-enter v5
    :try_end_27
    .catchall {:try_start_11 .. :try_end_27} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_27} :catch_63

    #@27
    .line 634
    :try_start_27
    sget-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@29
    if-nez v3, :cond_31

    #@2b
    .line 635
    const/16 v3, 0x2710

    #@2d
    new-array v3, v3, [B

    #@2f
    sput-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@31
    .line 637
    :cond_31
    sget-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@33
    move-object/from16 v0, v18

    #@35
    move-wide/from16 v1, p1

    #@37
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MiniThumbFile;->getMiniThumbFromFile(J[B)[B

    #@3a
    move-result-object v3

    #@3b
    if-eqz v3, :cond_50

    #@3d
    .line 638
    sget-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@3f
    const/4 v7, 0x0

    #@40
    sget-object v8, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@42
    array-length v8, v8

    #@43
    invoke-static {v3, v7, v8}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    #@46
    move-result-object v11

    #@47
    .line 639
    if-nez v11, :cond_50

    #@49
    .line 640
    const-string v3, "MediaStore"

    #@4b
    const-string v7, "couldn\'t decode byte array."

    #@4d
    invoke-static {v3, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 643
    :cond_50
    monitor-exit v5
    :try_end_51
    .catchall {:try_start_27 .. :try_end_51} :catchall_60

    #@51
    .line 719
    if-eqz v12, :cond_56

    #@53
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@56
    .line 721
    :cond_56
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@59
    .line 722
    const/16 v18, 0x0

    #@5b
    move-object v3, v11

    #@5c
    .line 724
    .end local v16           #magic:J
    :goto_5c
    return-object v3

    #@5d
    .line 623
    .end local v12           #c:Landroid/database/Cursor;
    .end local v18           #thumbFile:Landroid/media/MiniThumbFile;
    :cond_5d
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@5f
    goto :goto_8

    #@60
    .line 643
    .restart local v12       #c:Landroid/database/Cursor;
    .restart local v16       #magic:J
    .restart local v18       #thumbFile:Landroid/media/MiniThumbFile;
    :catchall_60
    move-exception v3

    #@61
    :try_start_61
    monitor-exit v5
    :try_end_62
    .catchall {:try_start_61 .. :try_end_62} :catchall_60

    #@62
    :try_start_62
    throw v3
    :try_end_63
    .catchall {:try_start_62 .. :try_end_63} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_62 .. :try_end_63} :catch_63

    #@63
    .line 716
    .end local v16           #magic:J
    :catch_63
    move-exception v14

    #@64
    .line 717
    .local v14, ex:Landroid/database/sqlite/SQLiteException;
    :try_start_64
    const-string v3, "MediaStore"

    #@66
    invoke-static {v3, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_69
    .catchall {:try_start_64 .. :try_end_69} :catchall_1db

    #@69
    .line 719
    if-eqz v12, :cond_6e

    #@6b
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@6e
    .line 721
    :cond_6e
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@71
    .line 722
    const/16 v18, 0x0

    #@73
    .end local v14           #ex:Landroid/database/sqlite/SQLiteException;
    :goto_73
    move-object v3, v11

    #@74
    .line 724
    goto :goto_5c

    #@75
    .line 645
    .restart local v16       #magic:J
    :cond_75
    const/4 v3, 0x1

    #@76
    move/from16 v0, p5

    #@78
    if-ne v0, v3, :cond_c1

    #@7a
    .line 646
    if-eqz p8, :cond_be

    #@7c
    :try_start_7c
    const-string/jumbo v13, "video_id="

    #@7f
    .line 647
    .local v13, column:Ljava/lang/String;
    :goto_7f
    sget-object v5, Landroid/provider/MediaStore$InternalThumbnails;->PROJECTION:[Ljava/lang/String;

    #@81
    new-instance v3, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    move-wide/from16 v0, p1

    #@8c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v6

    #@94
    const/4 v7, 0x0

    #@95
    const/4 v8, 0x0

    #@96
    move-object/from16 v3, p0

    #@98
    move-object/from16 v4, p7

    #@9a
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@9d
    move-result-object v12

    #@9e
    .line 648
    if-eqz v12, :cond_c1

    #@a0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    #@a3
    move-result v3

    #@a4
    if-eqz v3, :cond_c1

    #@a6
    .line 649
    move-object/from16 v0, p7

    #@a8
    move-object/from16 v1, p0

    #@aa
    move-object/from16 v2, p6

    #@ac
    invoke-static {v12, v0, v1, v2}, Landroid/provider/MediaStore$InternalThumbnails;->getMiniThumbFromFile(Landroid/database/Cursor;Landroid/net/Uri;Landroid/content/ContentResolver;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_af
    .catchall {:try_start_7c .. :try_end_af} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7c .. :try_end_af} :catch_63

    #@af
    move-result-object v11

    #@b0
    .line 650
    if-eqz v11, :cond_c1

    #@b2
    .line 719
    if-eqz v12, :cond_b7

    #@b4
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@b7
    .line 721
    :cond_b7
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@ba
    .line 722
    const/16 v18, 0x0

    #@bc
    move-object v3, v11

    #@bd
    goto :goto_5c

    #@be
    .line 646
    .end local v13           #column:Ljava/lang/String;
    :cond_be
    :try_start_be
    const-string v13, "image_id="

    #@c0
    goto :goto_7f

    #@c1
    .line 657
    :cond_c1
    invoke-virtual/range {p7 .. p7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@c4
    move-result-object v3

    #@c5
    const-string v5, "blocking"

    #@c7
    const-string v7, "1"

    #@c9
    invoke-virtual {v3, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@cc
    move-result-object v3

    #@cd
    const-string/jumbo v5, "orig_id"

    #@d0
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@d3
    move-result-object v7

    #@d4
    invoke-virtual {v3, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@d7
    move-result-object v3

    #@d8
    const-string v5, "group_id"

    #@da
    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@dd
    move-result-object v7

    #@de
    invoke-virtual {v3, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@e1
    move-result-object v3

    #@e2
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@e5
    move-result-object v4

    #@e6
    .line 660
    .local v4, blockingUri:Landroid/net/Uri;
    if-eqz v12, :cond_eb

    #@e8
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@eb
    .line 661
    :cond_eb
    sget-object v5, Landroid/provider/MediaStore$InternalThumbnails;->PROJECTION:[Ljava/lang/String;

    #@ed
    const/4 v6, 0x0

    #@ee
    const/4 v7, 0x0

    #@ef
    const/4 v8, 0x0

    #@f0
    move-object/from16 v3, p0

    #@f2
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_f5
    .catchall {:try_start_be .. :try_end_f5} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_be .. :try_end_f5} :catch_63

    #@f5
    move-result-object v12

    #@f6
    .line 663
    if-nez v12, :cond_105

    #@f8
    const/4 v3, 0x0

    #@f9
    .line 719
    if-eqz v12, :cond_fe

    #@fb
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@fe
    .line 721
    :cond_fe
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@101
    .line 722
    const/16 v18, 0x0

    #@103
    goto/16 :goto_5c

    #@105
    .line 666
    :cond_105
    const/4 v3, 0x3

    #@106
    move/from16 v0, p5

    #@108
    if-ne v0, v3, :cond_1e7

    #@10a
    .line 668
    :try_start_10a
    move-object/from16 v0, v18

    #@10c
    move-wide/from16 v1, p1

    #@10e
    invoke-virtual {v0, v1, v2}, Landroid/media/MiniThumbFile;->getMagic(J)J

    #@111
    move-result-wide v16

    #@112
    .line 669
    const-string v3, "MediaStore"

    #@114
    new-instance v5, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v7, "[mediascanner] magic id = "

    #@11b
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v5

    #@11f
    move-wide/from16 v0, v16

    #@121
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@124
    move-result-object v5

    #@125
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v5

    #@129
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    .line 672
    const-wide/16 v7, 0x0

    #@12e
    cmp-long v3, v16, v7

    #@130
    if-eqz v3, :cond_15f

    #@132
    .line 674
    sget-object v5, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBufLock:Ljava/lang/Object;

    #@134
    monitor-enter v5
    :try_end_135
    .catchall {:try_start_10a .. :try_end_135} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10a .. :try_end_135} :catch_63

    #@135
    .line 675
    :try_start_135
    sget-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@137
    if-nez v3, :cond_13f

    #@139
    .line 676
    const/16 v3, 0x2710

    #@13b
    new-array v3, v3, [B

    #@13d
    sput-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@13f
    .line 678
    :cond_13f
    sget-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@141
    move-object/from16 v0, v18

    #@143
    move-wide/from16 v1, p1

    #@145
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MiniThumbFile;->getMiniThumbFromFile(J[B)[B

    #@148
    move-result-object v3

    #@149
    if-eqz v3, :cond_15e

    #@14b
    .line 679
    sget-object v3, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@14d
    const/4 v7, 0x0

    #@14e
    sget-object v8, Landroid/provider/MediaStore$InternalThumbnails;->sThumbBuf:[B

    #@150
    array-length v8, v8

    #@151
    invoke-static {v3, v7, v8}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    #@154
    move-result-object v11

    #@155
    .line 680
    if-nez v11, :cond_15e

    #@157
    .line 681
    const-string v3, "MediaStore"

    #@159
    const-string v7, "couldn\'t decode byte array."

    #@15b
    invoke-static {v3, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 684
    :cond_15e
    monitor-exit v5
    :try_end_15f
    .catchall {:try_start_135 .. :try_end_15f} :catchall_1d8

    #@15f
    .line 696
    :cond_15f
    :goto_15f
    if-nez v11, :cond_226

    #@161
    .line 697
    :try_start_161
    const-string v3, "MediaStore"

    #@163
    new-instance v5, Ljava/lang/StringBuilder;

    #@165
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@168
    const-string v7, "Create the thumbnail in memory: origId="

    #@16a
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v5

    #@16e
    move-wide/from16 v0, p1

    #@170
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@173
    move-result-object v5

    #@174
    const-string v7, ", kind="

    #@176
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v5

    #@17a
    move/from16 v0, p5

    #@17c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v5

    #@180
    const-string v7, ", isVideo="

    #@182
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v5

    #@186
    move/from16 v0, p8

    #@188
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v5

    #@18c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18f
    move-result-object v5

    #@190
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@193
    .line 699
    invoke-virtual/range {p7 .. p7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@196
    move-result-object v3

    #@197
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@19a
    move-result-object v5

    #@19b
    invoke-virtual {v3, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@19e
    move-result-object v3

    #@19f
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    #@1a2
    move-result-object v3

    #@1a3
    const-string/jumbo v5, "thumbnails"

    #@1a6
    const-string/jumbo v7, "media"

    #@1a9
    invoke-virtual {v3, v5, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1ac
    move-result-object v3

    #@1ad
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1b0
    move-result-object v6

    #@1b1
    .line 702
    .local v6, uri:Landroid/net/Uri;
    if-nez v15, :cond_21e

    #@1b3
    .line 703
    if-eqz v12, :cond_1b8

    #@1b5
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@1b8
    .line 704
    :cond_1b8
    sget-object v7, Landroid/provider/MediaStore$InternalThumbnails;->PROJECTION:[Ljava/lang/String;

    #@1ba
    const/4 v8, 0x0

    #@1bb
    const/4 v9, 0x0

    #@1bc
    const/4 v10, 0x0

    #@1bd
    move-object/from16 v5, p0

    #@1bf
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1c2
    move-result-object v12

    #@1c3
    .line 705
    if-eqz v12, :cond_1cb

    #@1c5
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1c8
    .catchall {:try_start_161 .. :try_end_1c8} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_161 .. :try_end_1c8} :catch_63

    #@1c8
    move-result v3

    #@1c9
    if-nez v3, :cond_219

    #@1cb
    .line 706
    :cond_1cb
    const/4 v3, 0x0

    #@1cc
    .line 719
    if-eqz v12, :cond_1d1

    #@1ce
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@1d1
    .line 721
    :cond_1d1
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@1d4
    .line 722
    const/16 v18, 0x0

    #@1d6
    goto/16 :goto_5c

    #@1d8
    .line 684
    .end local v6           #uri:Landroid/net/Uri;
    :catchall_1d8
    move-exception v3

    #@1d9
    :try_start_1d9
    monitor-exit v5
    :try_end_1da
    .catchall {:try_start_1d9 .. :try_end_1da} :catchall_1d8

    #@1da
    :try_start_1da
    throw v3
    :try_end_1db
    .catchall {:try_start_1da .. :try_end_1db} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1da .. :try_end_1db} :catch_63

    #@1db
    .line 719
    .end local v4           #blockingUri:Landroid/net/Uri;
    .end local v16           #magic:J
    :catchall_1db
    move-exception v3

    #@1dc
    if-eqz v12, :cond_1e1

    #@1de
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@1e1
    .line 721
    :cond_1e1
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@1e4
    .line 722
    const/16 v18, 0x0

    #@1e6
    throw v3

    #@1e7
    .line 687
    .restart local v4       #blockingUri:Landroid/net/Uri;
    .restart local v16       #magic:J
    :cond_1e7
    const/4 v3, 0x1

    #@1e8
    move/from16 v0, p5

    #@1ea
    if-ne v0, v3, :cond_1fe

    #@1ec
    .line 688
    :try_start_1ec
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    #@1ef
    move-result v3

    #@1f0
    if-eqz v3, :cond_15f

    #@1f2
    .line 689
    move-object/from16 v0, p7

    #@1f4
    move-object/from16 v1, p0

    #@1f6
    move-object/from16 v2, p6

    #@1f8
    invoke-static {v12, v0, v1, v2}, Landroid/provider/MediaStore$InternalThumbnails;->getMiniThumbFromFile(Landroid/database/Cursor;Landroid/net/Uri;Landroid/content/ContentResolver;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@1fb
    move-result-object v11

    #@1fc
    goto/16 :goto_15f

    #@1fe
    .line 692
    :cond_1fe
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@200
    new-instance v5, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v7, "Unsupported kind: "

    #@207
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v5

    #@20b
    move/from16 v0, p5

    #@20d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@210
    move-result-object v5

    #@211
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v5

    #@215
    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@218
    throw v3

    #@219
    .line 708
    .restart local v6       #uri:Landroid/net/Uri;
    :cond_219
    const/4 v3, 0x1

    #@21a
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@21d
    move-result-object v15

    #@21e
    .line 710
    :cond_21e
    if-eqz p8, :cond_232

    #@220
    .line 711
    move/from16 v0, p5

    #@222
    invoke-static {v15, v0}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_225
    .catchall {:try_start_1ec .. :try_end_225} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1ec .. :try_end_225} :catch_63

    #@225
    move-result-object v11

    #@226
    .line 719
    .end local v6           #uri:Landroid/net/Uri;
    :cond_226
    :goto_226
    if-eqz v12, :cond_22b

    #@228
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@22b
    .line 721
    :cond_22b
    invoke-virtual/range {v18 .. v18}, Landroid/media/MiniThumbFile;->deactivate()V

    #@22e
    .line 722
    const/16 v18, 0x0

    #@230
    .line 723
    goto/16 :goto_73

    #@232
    .line 713
    .restart local v6       #uri:Landroid/net/Uri;
    :cond_232
    :try_start_232
    move/from16 v0, p5

    #@234
    invoke-static {v15, v0}, Landroid/media/ThumbnailUtils;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_237
    .catchall {:try_start_232 .. :try_end_237} :catchall_1db
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_232 .. :try_end_237} :catch_63

    #@237
    move-result-object v11

    #@238
    goto :goto_226
.end method
