.class public final Landroid/provider/ContactsContract$CommonDataKinds$Im;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/ContactsContract$DataColumnsWithJoins;
.implements Landroid/provider/ContactsContract$CommonDataKinds$CommonColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract$CommonDataKinds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Im"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/im"

.field public static final CUSTOM_PROTOCOL:Ljava/lang/String; = "data6"

.field public static final PROTOCOL:Ljava/lang/String; = "data5"

.field public static final PROTOCOL_AIM:I = 0x0

.field public static final PROTOCOL_CUSTOM:I = -0x1

.field public static final PROTOCOL_GOOGLE_TALK:I = 0x5

.field public static final PROTOCOL_ICQ:I = 0x6

.field public static final PROTOCOL_JABBER:I = 0x7

.field public static final PROTOCOL_MSN:I = 0x1

.field public static final PROTOCOL_NETMEETING:I = 0x8

.field public static final PROTOCOL_QQ:I = 0x4

.field public static final PROTOCOL_SKYPE:I = 0x3

.field public static final PROTOCOL_YAHOO:I = 0x2

.field public static final TYPE_HOME:I = 0x1

.field public static final TYPE_OTHER:I = 0x3

.field public static final TYPE_WORK:I = 0x2


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5998
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final getProtocolLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "res"
    .parameter "type"
    .parameter "label"

    #@0
    .prologue
    .line 6085
    const/4 v1, -0x1

    #@1
    if-ne p1, v1, :cond_a

    #@3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 6089
    .end local p2
    :goto_9
    return-object p2

    #@a
    .line 6088
    .restart local p2
    :cond_a
    invoke-static {p1}, Landroid/provider/ContactsContract$CommonDataKinds$Im;->getProtocolLabelResource(I)I

    #@d
    move-result v0

    #@e
    .line 6089
    .local v0, labelRes:I
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@11
    move-result-object p2

    #@12
    goto :goto_9
.end method

.method public static final getProtocolLabelResource(I)I
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 6064
    packed-switch p0, :pswitch_data_2c

    #@3
    .line 6074
    const v0, 0x10402dc

    #@6
    :goto_6
    return v0

    #@7
    .line 6065
    :pswitch_7
    const v0, 0x10402dd

    #@a
    goto :goto_6

    #@b
    .line 6066
    :pswitch_b
    const v0, 0x10402de

    #@e
    goto :goto_6

    #@f
    .line 6067
    :pswitch_f
    const v0, 0x10402df

    #@12
    goto :goto_6

    #@13
    .line 6068
    :pswitch_13
    const v0, 0x10402e0

    #@16
    goto :goto_6

    #@17
    .line 6069
    :pswitch_17
    const v0, 0x10402e1

    #@1a
    goto :goto_6

    #@1b
    .line 6070
    :pswitch_1b
    const v0, 0x10402e2

    #@1e
    goto :goto_6

    #@1f
    .line 6071
    :pswitch_1f
    const v0, 0x10402e3

    #@22
    goto :goto_6

    #@23
    .line 6072
    :pswitch_23
    const v0, 0x10402e4

    #@26
    goto :goto_6

    #@27
    .line 6073
    :pswitch_27
    const v0, 0x10402e5

    #@2a
    goto :goto_6

    #@2b
    .line 6064
    nop

    #@2c
    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_13
        :pswitch_17
        :pswitch_1b
        :pswitch_1f
        :pswitch_23
        :pswitch_27
    .end packed-switch
.end method

.method public static final getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "res"
    .parameter "type"
    .parameter "label"

    #@0
    .prologue
    .line 6051
    if-nez p1, :cond_9

    #@2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 6055
    .end local p2
    :goto_8
    return-object p2

    #@9
    .line 6054
    .restart local p2
    :cond_9
    invoke-static {p1}, Landroid/provider/ContactsContract$CommonDataKinds$Im;->getTypeLabelResource(I)I

    #@c
    move-result v0

    #@d
    .line 6055
    .local v0, labelRes:I
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@10
    move-result-object p2

    #@11
    goto :goto_8
.end method

.method public static final getTypeLabelResource(I)I
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 6036
    packed-switch p0, :pswitch_data_14

    #@3
    .line 6040
    const v0, 0x10402d8

    #@6
    :goto_6
    return v0

    #@7
    .line 6037
    :pswitch_7
    const v0, 0x10402d9

    #@a
    goto :goto_6

    #@b
    .line 6038
    :pswitch_b
    const v0, 0x10402da

    #@e
    goto :goto_6

    #@f
    .line 6039
    :pswitch_f
    const v0, 0x10402db

    #@12
    goto :goto_6

    #@13
    .line 6036
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_7
        :pswitch_b
        :pswitch_f
    .end packed-switch
.end method
