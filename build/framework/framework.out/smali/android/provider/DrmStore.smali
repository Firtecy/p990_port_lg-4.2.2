.class public final Landroid/provider/DrmStore;
.super Ljava/lang/Object;
.source "DrmStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/DrmStore$Audio;,
        Landroid/provider/DrmStore$Images;,
        Landroid/provider/DrmStore$Columns;
    }
.end annotation


# static fields
.field private static final ACCESS_DRM_PERMISSION:Ljava/lang/String; = "android.permission.ACCESS_DRM"

.field public static final AUTHORITY:Ljava/lang/String; = "drm"

.field private static final TAG:Ljava/lang/String; = "DrmStore"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 89
    return-void
.end method

.method public static final addDrmFile(Landroid/content/ContentResolver;Ljava/io/File;Ljava/lang/String;)Landroid/content/Intent;
    .registers 11
    .parameter "cr"
    .parameter "file"
    .parameter "title"

    #@0
    .prologue
    .line 103
    const/4 v1, 0x0

    #@1
    .line 104
    .local v1, fis:Ljava/io/FileInputStream;
    const/4 v4, 0x0

    #@2
    .line 107
    .local v4, result:Landroid/content/Intent;
    :try_start_2
    new-instance v2, Ljava/io/FileInputStream;

    #@4
    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_47
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_7} :catch_2f

    #@7
    .line 108
    .end local v1           #fis:Ljava/io/FileInputStream;
    .local v2, fis:Ljava/io/FileInputStream;
    if-nez p2, :cond_1a

    #@9
    .line 109
    :try_start_9
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@c
    move-result-object p2

    #@d
    .line 110
    const/16 v5, 0x2e

    #@f
    invoke-virtual {p2, v5}, Ljava/lang/String;->lastIndexOf(I)I

    #@12
    move-result v3

    #@13
    .line 111
    .local v3, lastDot:I
    if-lez v3, :cond_1a

    #@15
    .line 112
    const/4 v5, 0x0

    #@16
    invoke-virtual {p2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@19
    move-result-object p2

    #@1a
    .line 115
    .end local v3           #lastDot:I
    :cond_1a
    invoke-static {p0, v2, p2}, Landroid/provider/DrmStore;->addDrmFile(Landroid/content/ContentResolver;Ljava/io/FileInputStream;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1d
    .catchall {:try_start_9 .. :try_end_1d} :catchall_57
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_1d} :catch_5a

    #@1d
    move-result-object v4

    #@1e
    .line 120
    if-eqz v2, :cond_23

    #@20
    .line 121
    :try_start_20
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_23} :catch_25

    #@23
    :cond_23
    move-object v1, v2

    #@24
    .line 127
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    :cond_24
    :goto_24
    return-object v4

    #@25
    .line 122
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_25
    move-exception v0

    #@26
    .line 123
    .local v0, e:Ljava/io/IOException;
    const-string v5, "DrmStore"

    #@28
    const-string v6, "IOException in DrmStore.addDrmFile()"

    #@2a
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2d
    move-object v1, v2

    #@2e
    .line 125
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_24

    #@2f
    .line 116
    .end local v0           #e:Ljava/io/IOException;
    :catch_2f
    move-exception v0

    #@30
    .line 117
    .local v0, e:Ljava/lang/Exception;
    :goto_30
    :try_start_30
    const-string v5, "DrmStore"

    #@32
    const-string/jumbo v6, "pushing file failed"

    #@35
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_38
    .catchall {:try_start_30 .. :try_end_38} :catchall_47

    #@38
    .line 120
    if-eqz v1, :cond_24

    #@3a
    .line 121
    :try_start_3a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3d} :catch_3e

    #@3d
    goto :goto_24

    #@3e
    .line 122
    :catch_3e
    move-exception v0

    #@3f
    .line 123
    .local v0, e:Ljava/io/IOException;
    const-string v5, "DrmStore"

    #@41
    const-string v6, "IOException in DrmStore.addDrmFile()"

    #@43
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_24

    #@47
    .line 119
    .end local v0           #e:Ljava/io/IOException;
    :catchall_47
    move-exception v5

    #@48
    .line 120
    :goto_48
    if-eqz v1, :cond_4d

    #@4a
    .line 121
    :try_start_4a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_4e

    #@4d
    .line 124
    :cond_4d
    :goto_4d
    throw v5

    #@4e
    .line 122
    :catch_4e
    move-exception v0

    #@4f
    .line 123
    .restart local v0       #e:Ljava/io/IOException;
    const-string v6, "DrmStore"

    #@51
    const-string v7, "IOException in DrmStore.addDrmFile()"

    #@53
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@56
    goto :goto_4d

    #@57
    .line 119
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catchall_57
    move-exception v5

    #@58
    move-object v1, v2

    #@59
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_48

    #@5a
    .line 116
    .end local v1           #fis:Ljava/io/FileInputStream;
    .restart local v2       #fis:Ljava/io/FileInputStream;
    :catch_5a
    move-exception v0

    #@5b
    move-object v1, v2

    #@5c
    .end local v2           #fis:Ljava/io/FileInputStream;
    .restart local v1       #fis:Ljava/io/FileInputStream;
    goto :goto_30
.end method

.method public static final addDrmFile(Landroid/content/ContentResolver;Ljava/io/FileInputStream;Ljava/lang/String;)Landroid/content/Intent;
    .registers 25
    .parameter "cr"
    .parameter "fis"
    .parameter "title"

    #@0
    .prologue
    .line 139
    const/4 v10, 0x0

    #@1
    .line 140
    .local v10, os:Ljava/io/OutputStream;
    const/4 v11, 0x0

    #@2
    .line 143
    .local v11, result:Landroid/content/Intent;
    :try_start_2
    new-instance v4, Landroid/drm/mobile1/DrmRawContent;

    #@4
    invoke-virtual/range {p1 .. p1}, Ljava/io/FileInputStream;->available()I

    #@7
    move-result v19

    #@8
    const-string v20, "application/vnd.oma.drm.message"

    #@a
    move-object/from16 v0, p1

    #@c
    move/from16 v1, v19

    #@e
    move-object/from16 v2, v20

    #@10
    invoke-direct {v4, v0, v1, v2}, Landroid/drm/mobile1/DrmRawContent;-><init>(Ljava/io/InputStream;ILjava/lang/String;)V

    #@13
    .line 145
    .local v4, content:Landroid/drm/mobile1/DrmRawContent;
    invoke-virtual {v4}, Landroid/drm/mobile1/DrmRawContent;->getContentType()Ljava/lang/String;

    #@16
    move-result-object v9

    #@17
    .line 146
    .local v9, mimeType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    #@1a
    move-result-object v19

    #@1b
    invoke-virtual/range {v19 .. v19}, Ljava/nio/channels/FileChannel;->size()J

    #@1e
    move-result-wide v14

    #@1f
    .line 148
    .local v14, size:J
    invoke-static {}, Landroid/drm/mobile1/DrmRightsManager;->getInstance()Landroid/drm/mobile1/DrmRightsManager;

    #@22
    move-result-object v8

    #@23
    .line 149
    .local v8, manager:Landroid/drm/mobile1/DrmRightsManager;
    invoke-virtual {v8, v4}, Landroid/drm/mobile1/DrmRightsManager;->queryRights(Landroid/drm/mobile1/DrmRawContent;)Landroid/drm/mobile1/DrmRights;

    #@26
    move-result-object v13

    #@27
    .line 150
    .local v13, rights:Landroid/drm/mobile1/DrmRights;
    invoke-virtual {v4, v13}, Landroid/drm/mobile1/DrmRawContent;->getContentInputStream(Landroid/drm/mobile1/DrmRights;)Ljava/io/InputStream;

    #@2a
    move-result-object v16

    #@2b
    .line 152
    .local v16, stream:Ljava/io/InputStream;
    const/4 v5, 0x0

    #@2c
    .line 153
    .local v5, contentUri:Landroid/net/Uri;
    const-string v19, "audio/"

    #@2e
    move-object/from16 v0, v19

    #@30
    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@33
    move-result v19

    #@34
    if-eqz v19, :cond_a4

    #@36
    .line 154
    sget-object v5, Landroid/provider/DrmStore$Audio;->CONTENT_URI:Landroid/net/Uri;

    #@38
    .line 161
    :goto_38
    if-eqz v5, :cond_e5

    #@3a
    .line 162
    new-instance v18, Landroid/content/ContentValues;

    #@3c
    const/16 v19, 0x3

    #@3e
    invoke-direct/range {v18 .. v19}, Landroid/content/ContentValues;-><init>(I)V

    #@41
    .line 163
    .local v18, values:Landroid/content/ContentValues;
    const-string/jumbo v19, "title"

    #@44
    move-object/from16 v0, v18

    #@46
    move-object/from16 v1, v19

    #@48
    move-object/from16 v2, p2

    #@4a
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 164
    const-string v19, "_size"

    #@4f
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@52
    move-result-object v20

    #@53
    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@56
    .line 165
    const-string/jumbo v19, "mime_type"

    #@59
    move-object/from16 v0, v18

    #@5b
    move-object/from16 v1, v19

    #@5d
    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 167
    move-object/from16 v0, p0

    #@62
    move-object/from16 v1, v18

    #@64
    invoke-virtual {v0, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@67
    move-result-object v17

    #@68
    .line 168
    .local v17, uri:Landroid/net/Uri;
    if-eqz v17, :cond_e5

    #@6a
    .line 169
    move-object/from16 v0, p0

    #@6c
    move-object/from16 v1, v17

    #@6e
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    #@71
    move-result-object v10

    #@72
    .line 171
    const/16 v19, 0x3e8

    #@74
    move/from16 v0, v19

    #@76
    new-array v3, v0, [B

    #@78
    .line 174
    .local v3, buffer:[B
    :goto_78
    move-object/from16 v0, v16

    #@7a
    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    #@7d
    move-result v6

    #@7e
    .local v6, count:I
    const/16 v19, -0x1

    #@80
    move/from16 v0, v19

    #@82
    if-eq v6, v0, :cond_da

    #@84
    .line 175
    const/16 v19, 0x0

    #@86
    move/from16 v0, v19

    #@88
    invoke-virtual {v10, v3, v0, v6}, Ljava/io/OutputStream;->write([BII)V
    :try_end_8b
    .catchall {:try_start_2 .. :try_end_8b} :catchall_ce
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_8b} :catch_8c

    #@8b
    goto :goto_78

    #@8c
    .line 182
    .end local v3           #buffer:[B
    .end local v4           #content:Landroid/drm/mobile1/DrmRawContent;
    .end local v5           #contentUri:Landroid/net/Uri;
    .end local v6           #count:I
    .end local v8           #manager:Landroid/drm/mobile1/DrmRightsManager;
    .end local v9           #mimeType:Ljava/lang/String;
    .end local v13           #rights:Landroid/drm/mobile1/DrmRights;
    .end local v14           #size:J
    .end local v16           #stream:Ljava/io/InputStream;
    .end local v17           #uri:Landroid/net/Uri;
    .end local v18           #values:Landroid/content/ContentValues;
    :catch_8c
    move-exception v7

    #@8d
    .line 183
    .local v7, e:Ljava/lang/Exception;
    :goto_8d
    :try_start_8d
    const-string v19, "DrmStore"

    #@8f
    const-string/jumbo v20, "pushing file failed"

    #@92
    move-object/from16 v0, v19

    #@94
    move-object/from16 v1, v20

    #@96
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_99
    .catchall {:try_start_8d .. :try_end_99} :catchall_ce

    #@99
    .line 186
    if-eqz p1, :cond_9e

    #@9b
    .line 187
    :try_start_9b
    invoke-virtual/range {p1 .. p1}, Ljava/io/FileInputStream;->close()V

    #@9e
    .line 188
    :cond_9e
    if-eqz v10, :cond_a3

    #@a0
    .line 189
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_a3
    .catch Ljava/io/IOException; {:try_start_9b .. :try_end_a3} :catch_fd

    #@a3
    .line 195
    .end local v7           #e:Ljava/lang/Exception;
    :cond_a3
    :goto_a3
    return-object v11

    #@a4
    .line 155
    .restart local v4       #content:Landroid/drm/mobile1/DrmRawContent;
    .restart local v5       #contentUri:Landroid/net/Uri;
    .restart local v8       #manager:Landroid/drm/mobile1/DrmRightsManager;
    .restart local v9       #mimeType:Ljava/lang/String;
    .restart local v13       #rights:Landroid/drm/mobile1/DrmRights;
    .restart local v14       #size:J
    .restart local v16       #stream:Ljava/io/InputStream;
    :cond_a4
    :try_start_a4
    const-string v19, "image/"

    #@a6
    move-object/from16 v0, v19

    #@a8
    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@ab
    move-result v19

    #@ac
    if-eqz v19, :cond_b1

    #@ae
    .line 156
    sget-object v5, Landroid/provider/DrmStore$Images;->CONTENT_URI:Landroid/net/Uri;

    #@b0
    goto :goto_38

    #@b1
    .line 158
    :cond_b1
    const-string v19, "DrmStore"

    #@b3
    new-instance v20, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string/jumbo v21, "unsupported mime type "

    #@bb
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v20

    #@bf
    move-object/from16 v0, v20

    #@c1
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v20

    #@c5
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v20

    #@c9
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_cc
    .catchall {:try_start_a4 .. :try_end_cc} :catchall_ce
    .catch Ljava/lang/Exception; {:try_start_a4 .. :try_end_cc} :catch_8c

    #@cc
    goto/16 :goto_38

    #@ce
    .line 185
    .end local v4           #content:Landroid/drm/mobile1/DrmRawContent;
    .end local v5           #contentUri:Landroid/net/Uri;
    .end local v8           #manager:Landroid/drm/mobile1/DrmRightsManager;
    .end local v9           #mimeType:Ljava/lang/String;
    .end local v13           #rights:Landroid/drm/mobile1/DrmRights;
    .end local v14           #size:J
    .end local v16           #stream:Ljava/io/InputStream;
    :catchall_ce
    move-exception v19

    #@cf
    .line 186
    :goto_cf
    if-eqz p1, :cond_d4

    #@d1
    .line 187
    :try_start_d1
    invoke-virtual/range {p1 .. p1}, Ljava/io/FileInputStream;->close()V

    #@d4
    .line 188
    :cond_d4
    if-eqz v10, :cond_d9

    #@d6
    .line 189
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_d9
    .catch Ljava/io/IOException; {:try_start_d1 .. :try_end_d9} :catch_10a

    #@d9
    .line 192
    :cond_d9
    :goto_d9
    throw v19

    #@da
    .line 177
    .restart local v3       #buffer:[B
    .restart local v4       #content:Landroid/drm/mobile1/DrmRawContent;
    .restart local v5       #contentUri:Landroid/net/Uri;
    .restart local v6       #count:I
    .restart local v8       #manager:Landroid/drm/mobile1/DrmRightsManager;
    .restart local v9       #mimeType:Ljava/lang/String;
    .restart local v13       #rights:Landroid/drm/mobile1/DrmRights;
    .restart local v14       #size:J
    .restart local v16       #stream:Ljava/io/InputStream;
    .restart local v17       #uri:Landroid/net/Uri;
    .restart local v18       #values:Landroid/content/ContentValues;
    :cond_da
    :try_start_da
    new-instance v12, Landroid/content/Intent;

    #@dc
    invoke-direct {v12}, Landroid/content/Intent;-><init>()V
    :try_end_df
    .catchall {:try_start_da .. :try_end_df} :catchall_ce
    .catch Ljava/lang/Exception; {:try_start_da .. :try_end_df} :catch_8c

    #@df
    .line 178
    .end local v11           #result:Landroid/content/Intent;
    .local v12, result:Landroid/content/Intent;
    :try_start_df
    move-object/from16 v0, v17

    #@e1
    invoke-virtual {v12, v0, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_e4
    .catchall {:try_start_df .. :try_end_e4} :catchall_117
    .catch Ljava/lang/Exception; {:try_start_df .. :try_end_e4} :catch_11a

    #@e4
    move-object v11, v12

    #@e5
    .line 186
    .end local v3           #buffer:[B
    .end local v6           #count:I
    .end local v12           #result:Landroid/content/Intent;
    .end local v17           #uri:Landroid/net/Uri;
    .end local v18           #values:Landroid/content/ContentValues;
    .restart local v11       #result:Landroid/content/Intent;
    :cond_e5
    if-eqz p1, :cond_ea

    #@e7
    .line 187
    :try_start_e7
    invoke-virtual/range {p1 .. p1}, Ljava/io/FileInputStream;->close()V

    #@ea
    .line 188
    :cond_ea
    if-eqz v10, :cond_a3

    #@ec
    .line 189
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_ef
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_ef} :catch_f0

    #@ef
    goto :goto_a3

    #@f0
    .line 190
    :catch_f0
    move-exception v7

    #@f1
    .line 191
    .local v7, e:Ljava/io/IOException;
    const-string v19, "DrmStore"

    #@f3
    const-string v20, "IOException in DrmStore.addDrmFile()"

    #@f5
    move-object/from16 v0, v19

    #@f7
    move-object/from16 v1, v20

    #@f9
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@fc
    goto :goto_a3

    #@fd
    .line 190
    .end local v4           #content:Landroid/drm/mobile1/DrmRawContent;
    .end local v5           #contentUri:Landroid/net/Uri;
    .end local v8           #manager:Landroid/drm/mobile1/DrmRightsManager;
    .end local v9           #mimeType:Ljava/lang/String;
    .end local v13           #rights:Landroid/drm/mobile1/DrmRights;
    .end local v14           #size:J
    .end local v16           #stream:Ljava/io/InputStream;
    .local v7, e:Ljava/lang/Exception;
    :catch_fd
    move-exception v7

    #@fe
    .line 191
    .local v7, e:Ljava/io/IOException;
    const-string v19, "DrmStore"

    #@100
    const-string v20, "IOException in DrmStore.addDrmFile()"

    #@102
    move-object/from16 v0, v19

    #@104
    move-object/from16 v1, v20

    #@106
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@109
    goto :goto_a3

    #@10a
    .line 190
    .end local v7           #e:Ljava/io/IOException;
    :catch_10a
    move-exception v7

    #@10b
    .line 191
    .restart local v7       #e:Ljava/io/IOException;
    const-string v20, "DrmStore"

    #@10d
    const-string v21, "IOException in DrmStore.addDrmFile()"

    #@10f
    move-object/from16 v0, v20

    #@111
    move-object/from16 v1, v21

    #@113
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@116
    goto :goto_d9

    #@117
    .line 185
    .end local v7           #e:Ljava/io/IOException;
    .end local v11           #result:Landroid/content/Intent;
    .restart local v3       #buffer:[B
    .restart local v4       #content:Landroid/drm/mobile1/DrmRawContent;
    .restart local v5       #contentUri:Landroid/net/Uri;
    .restart local v6       #count:I
    .restart local v8       #manager:Landroid/drm/mobile1/DrmRightsManager;
    .restart local v9       #mimeType:Ljava/lang/String;
    .restart local v12       #result:Landroid/content/Intent;
    .restart local v13       #rights:Landroid/drm/mobile1/DrmRights;
    .restart local v14       #size:J
    .restart local v16       #stream:Ljava/io/InputStream;
    .restart local v17       #uri:Landroid/net/Uri;
    .restart local v18       #values:Landroid/content/ContentValues;
    :catchall_117
    move-exception v19

    #@118
    move-object v11, v12

    #@119
    .end local v12           #result:Landroid/content/Intent;
    .restart local v11       #result:Landroid/content/Intent;
    goto :goto_cf

    #@11a
    .line 182
    .end local v11           #result:Landroid/content/Intent;
    .restart local v12       #result:Landroid/content/Intent;
    :catch_11a
    move-exception v7

    #@11b
    move-object v11, v12

    #@11c
    .end local v12           #result:Landroid/content/Intent;
    .restart local v11       #result:Landroid/content/Intent;
    goto/16 :goto_8d
.end method

.method public static enforceAccessDrmPermission(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 205
    const-string v0, "android.permission.ACCESS_DRM"

    #@2
    invoke-virtual {p0, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 207
    new-instance v0, Ljava/lang/SecurityException;

    #@a
    const-string v1, "Requires DRM permission"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 209
    :cond_10
    return-void
.end method
