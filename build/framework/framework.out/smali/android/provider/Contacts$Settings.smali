.class public final Landroid/provider/Contacts$Settings;
.super Ljava/lang/Object;
.source "Contacts.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/Contacts$SettingsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Contacts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Settings"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CONTENT_DIRECTORY:Ljava/lang/String; = "settings"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CONTENT_URI:Landroid/net/Uri; = null
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "key ASC"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SYNC_EVERYTHING:Ljava/lang/String; = "syncEverything"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 153
    const-string v0, "content://contacts/settings"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Contacts$Settings;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 146
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getSetting(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "cr"
    .parameter "account"
    .parameter "key"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 203
    const-string/jumbo v3, "key=?"

    #@6
    .line 204
    .local v3, selectString:Ljava/lang/String;
    new-array v4, v0, [Ljava/lang/String;

    #@8
    aput-object p2, v4, v7

    #@a
    .line 206
    .local v4, selectArgs:[Ljava/lang/String;
    sget-object v1, Landroid/provider/Contacts$Settings;->CONTENT_URI:Landroid/net/Uri;

    #@c
    new-array v2, v0, [Ljava/lang/String;

    #@e
    const-string/jumbo v0, "value"

    #@11
    aput-object v0, v2, v7

    #@13
    move-object v0, p0

    #@14
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@17
    move-result-object v6

    #@18
    .line 209
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_18
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1b
    .catchall {:try_start_18 .. :try_end_1b} :catchall_2b

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_22

    #@1e
    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@21
    :goto_21
    return-object v5

    #@22
    .line 210
    :cond_22
    const/4 v0, 0x0

    #@23
    :try_start_23
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_2b

    #@26
    move-result-object v5

    #@27
    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2a
    goto :goto_21

    #@2b
    :catchall_2b
    move-exception v0

    #@2c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2f
    throw v0
.end method

.method public static setSetting(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "cr"
    .parameter "account"
    .parameter "key"
    .parameter "value"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 222
    new-instance v0, Landroid/content/ContentValues;

    #@3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 229
    .local v0, values:Landroid/content/ContentValues;
    const-string/jumbo v1, "key"

    #@9
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 230
    const-string/jumbo v1, "value"

    #@f
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 231
    sget-object v1, Landroid/provider/Contacts$Settings;->CONTENT_URI:Landroid/net/Uri;

    #@14
    invoke-virtual {p0, v1, v0, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@17
    .line 232
    return-void
.end method
