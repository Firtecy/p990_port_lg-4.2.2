.class public final Landroid/provider/ContactsContract$RawContacts;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/ContactsContract$RawContactsColumns;
.implements Landroid/provider/ContactsContract$ContactOptionsColumns;
.implements Landroid/provider/ContactsContract$ContactNameColumns;
.implements Landroid/provider/ContactsContract$SyncColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RawContacts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/ContactsContract$RawContacts$EntityIteratorImpl;,
        Landroid/provider/ContactsContract$RawContacts$DisplayPhoto;,
        Landroid/provider/ContactsContract$RawContacts$StreamItems;,
        Landroid/provider/ContactsContract$RawContacts$Entity;,
        Landroid/provider/ContactsContract$RawContacts$Data;
    }
.end annotation


# static fields
.field public static final AGGREGATION_MODE_DEFAULT:I = 0x0

.field public static final AGGREGATION_MODE_DISABLED:I = 0x3

.field public static final AGGREGATION_MODE_IMMEDIATE:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AGGREGATION_MODE_SUSPENDED:I = 0x2

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/raw_contact"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/raw_contact"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 2619
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@2
    const-string/jumbo v1, "raw_contacts"

    #@5
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    #@b
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2612
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2613
    return-void
.end method

.method public static getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;
    .registers 13
    .parameter "resolver"
    .parameter "rawContactUri"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 2689
    const-string v0, "data"

    #@5
    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v1

    #@9
    .line 2690
    .local v1, dataUri:Landroid/net/Uri;
    const/4 v0, 0x2

    #@a
    new-array v2, v0, [Ljava/lang/String;

    #@c
    const-string v0, "contact_id"

    #@e
    aput-object v0, v2, v4

    #@10
    const-string/jumbo v0, "lookup"

    #@13
    aput-object v0, v2, v5

    #@15
    move-object v0, p0

    #@16
    move-object v4, v3

    #@17
    move-object v5, v3

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v8

    #@1c
    .line 2694
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@1d
    .line 2696
    .local v10, lookupUri:Landroid/net/Uri;
    if-eqz v8, :cond_39

    #@1f
    :try_start_1f
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_39

    #@25
    .line 2697
    const/4 v0, 0x0

    #@26
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@29
    move-result-wide v6

    #@2a
    .line 2698
    .local v6, contactId:J
    const/4 v0, 0x1

    #@2b
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2e
    move-result-object v9

    #@2f
    .line 2699
    .local v9, lookupKey:Ljava/lang/String;
    invoke-static {v6, v7, v9}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;
    :try_end_32
    .catchall {:try_start_1f .. :try_end_32} :catchall_3f

    #@32
    move-result-object v10

    #@33
    .line 2702
    .end local v10           #lookupUri:Landroid/net/Uri;
    if-eqz v8, :cond_38

    #@35
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@38
    .line 2704
    .end local v6           #contactId:J
    .end local v9           #lookupKey:Ljava/lang/String;
    :cond_38
    :goto_38
    return-object v10

    #@39
    .line 2702
    .restart local v10       #lookupUri:Landroid/net/Uri;
    :cond_39
    if-eqz v8, :cond_38

    #@3b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@3e
    goto :goto_38

    #@3f
    :catchall_3f
    move-exception v0

    #@40
    if-eqz v8, :cond_45

    #@42
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@45
    :cond_45
    throw v0
.end method

.method public static newEntityIterator(Landroid/database/Cursor;)Landroid/content/EntityIterator;
    .registers 2
    .parameter "cursor"

    #@0
    .prologue
    .line 2851
    new-instance v0, Landroid/provider/ContactsContract$RawContacts$EntityIteratorImpl;

    #@2
    invoke-direct {v0, p0}, Landroid/provider/ContactsContract$RawContacts$EntityIteratorImpl;-><init>(Landroid/database/Cursor;)V

    #@5
    return-object v0
.end method
