.class public final Landroid/provider/ContactsContract$Data;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/ContactsContract$DataColumnsWithJoins;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/data"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 4352
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@2
    const-string v1, "data"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@a
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 4346
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;
    .registers 13
    .parameter "resolver"
    .parameter "dataUri"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 4373
    const/4 v0, 0x2

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const-string v0, "contact_id"

    #@8
    aput-object v0, v2, v1

    #@a
    const-string/jumbo v0, "lookup"

    #@d
    aput-object v0, v2, v4

    #@f
    move-object v0, p0

    #@10
    move-object v1, p1

    #@11
    move-object v4, v3

    #@12
    move-object v5, v3

    #@13
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v8

    #@17
    .line 4377
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v10, 0x0

    #@18
    .line 4379
    .local v10, lookupUri:Landroid/net/Uri;
    if-eqz v8, :cond_34

    #@1a
    :try_start_1a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_34

    #@20
    .line 4380
    const/4 v0, 0x0

    #@21
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@24
    move-result-wide v6

    #@25
    .line 4381
    .local v6, contactId:J
    const/4 v0, 0x1

    #@26
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v9

    #@2a
    .line 4382
    .local v9, lookupKey:Ljava/lang/String;
    invoke-static {v6, v7, v9}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;
    :try_end_2d
    .catchall {:try_start_1a .. :try_end_2d} :catchall_3a

    #@2d
    move-result-object v10

    #@2e
    .line 4385
    .end local v10           #lookupUri:Landroid/net/Uri;
    if-eqz v8, :cond_33

    #@30
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@33
    .line 4387
    .end local v6           #contactId:J
    .end local v9           #lookupKey:Ljava/lang/String;
    :cond_33
    :goto_33
    return-object v10

    #@34
    .line 4385
    .restart local v10       #lookupUri:Landroid/net/Uri;
    :cond_34
    if-eqz v8, :cond_33

    #@36
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@39
    goto :goto_33

    #@3a
    :catchall_3a
    move-exception v0

    #@3b
    if-eqz v8, :cond_40

    #@3d
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@40
    :cond_40
    throw v0
.end method
