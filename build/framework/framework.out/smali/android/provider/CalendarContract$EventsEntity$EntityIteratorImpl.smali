.class Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;
.super Landroid/content/CursorEntityIterator;
.source "CalendarContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/CalendarContract$EventsEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EntityIteratorImpl"
.end annotation


# static fields
.field private static final ATTENDEES_PROJECTION:[Ljava/lang/String; = null

.field private static final COLUMN_ATTENDEE_EMAIL:I = 0x1

.field private static final COLUMN_ATTENDEE_IDENTITY:I = 0x5

.field private static final COLUMN_ATTENDEE_ID_NAMESPACE:I = 0x6

.field private static final COLUMN_ATTENDEE_NAME:I = 0x0

.field private static final COLUMN_ATTENDEE_RELATIONSHIP:I = 0x2

.field private static final COLUMN_ATTENDEE_STATUS:I = 0x4

.field private static final COLUMN_ATTENDEE_TYPE:I = 0x3

.field private static final COLUMN_ID:I = 0x0

.field private static final COLUMN_METHOD:I = 0x1

.field private static final COLUMN_MINUTES:I = 0x0

.field private static final COLUMN_NAME:I = 0x1

.field private static final COLUMN_VALUE:I = 0x2

.field private static final EXTENDED_PROJECTION:[Ljava/lang/String; = null

.field private static final REMINDERS_PROJECTION:[Ljava/lang/String; = null

.field private static final WHERE_EVENT_ID:Ljava/lang/String; = "event_id=?"


# instance fields
.field private final mProvider:Landroid/content/ContentProviderClient;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 1304
    new-array v0, v5, [Ljava/lang/String;

    #@6
    const-string/jumbo v1, "minutes"

    #@9
    aput-object v1, v0, v3

    #@b
    const-string/jumbo v1, "method"

    #@e
    aput-object v1, v0, v4

    #@10
    sput-object v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->REMINDERS_PROJECTION:[Ljava/lang/String;

    #@12
    .line 1311
    const/4 v0, 0x7

    #@13
    new-array v0, v0, [Ljava/lang/String;

    #@15
    const-string v1, "attendeeName"

    #@17
    aput-object v1, v0, v3

    #@19
    const-string v1, "attendeeEmail"

    #@1b
    aput-object v1, v0, v4

    #@1d
    const-string v1, "attendeeRelationship"

    #@1f
    aput-object v1, v0, v5

    #@21
    const-string v1, "attendeeType"

    #@23
    aput-object v1, v0, v6

    #@25
    const/4 v1, 0x4

    #@26
    const-string v2, "attendeeStatus"

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x5

    #@2b
    const-string v2, "attendeeIdentity"

    #@2d
    aput-object v2, v0, v1

    #@2f
    const/4 v1, 0x6

    #@30
    const-string v2, "attendeeIdNamespace"

    #@32
    aput-object v2, v0, v1

    #@34
    sput-object v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    #@36
    .line 1328
    new-array v0, v6, [Ljava/lang/String;

    #@38
    const-string v1, "_id"

    #@3a
    aput-object v1, v0, v3

    #@3c
    const-string/jumbo v1, "name"

    #@3f
    aput-object v1, v0, v4

    #@41
    const-string/jumbo v1, "value"

    #@44
    aput-object v1, v0, v5

    #@46
    sput-object v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->EXTENDED_PROJECTION:[Ljava/lang/String;

    #@48
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/ContentProviderClient;)V
    .registers 4
    .parameter "cursor"
    .parameter "provider"

    #@0
    .prologue
    .line 1346
    invoke-direct {p0, p1}, Landroid/content/CursorEntityIterator;-><init>(Landroid/database/Cursor;)V

    #@3
    .line 1347
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@6
    .line 1348
    iput-object p2, p0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mProvider:Landroid/content/ContentProviderClient;

    #@8
    .line 1349
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/content/ContentResolver;)V
    .registers 4
    .parameter "cursor"
    .parameter "resolver"

    #@0
    .prologue
    .line 1340
    invoke-direct {p0, p1}, Landroid/content/CursorEntityIterator;-><init>(Landroid/database/Cursor;)V

    #@3
    .line 1341
    iput-object p2, p0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@5
    .line 1342
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mProvider:Landroid/content/ContentProviderClient;

    #@8
    .line 1343
    return-void
.end method


# virtual methods
.method public getEntityAndIncrementCursor(Landroid/database/Cursor;)Landroid/content/Entity;
    .registers 18
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1354
    const-string v1, "_id"

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    move-object/from16 v0, p1

    #@a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@d
    move-result-wide v10

    #@e
    .line 1355
    .local v10, eventId:J
    new-instance v8, Landroid/content/ContentValues;

    #@10
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@13
    .line 1356
    .local v8, cv:Landroid/content/ContentValues;
    const-string v1, "_id"

    #@15
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1c
    .line 1357
    const-string v1, "calendar_id"

    #@1e
    move-object/from16 v0, p1

    #@20
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@23
    .line 1358
    const-string/jumbo v1, "title"

    #@26
    move-object/from16 v0, p1

    #@28
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@2b
    .line 1359
    const-string v1, "description"

    #@2d
    move-object/from16 v0, p1

    #@2f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@32
    .line 1360
    const-string v1, "eventLocation"

    #@34
    move-object/from16 v0, p1

    #@36
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@39
    .line 1361
    const-string v1, "eventStatus"

    #@3b
    move-object/from16 v0, p1

    #@3d
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@40
    .line 1362
    const-string/jumbo v1, "selfAttendeeStatus"

    #@43
    move-object/from16 v0, p1

    #@45
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@48
    .line 1363
    const-string v1, "dtstart"

    #@4a
    move-object/from16 v0, p1

    #@4c
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@4f
    .line 1364
    const-string v1, "dtend"

    #@51
    move-object/from16 v0, p1

    #@53
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@56
    .line 1365
    const-string v1, "duration"

    #@58
    move-object/from16 v0, p1

    #@5a
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@5d
    .line 1366
    const-string v1, "eventTimezone"

    #@5f
    move-object/from16 v0, p1

    #@61
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@64
    .line 1367
    const-string v1, "eventEndTimezone"

    #@66
    move-object/from16 v0, p1

    #@68
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@6b
    .line 1368
    const-string v1, "allDay"

    #@6d
    move-object/from16 v0, p1

    #@6f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@72
    .line 1369
    const-string v1, "accessLevel"

    #@74
    move-object/from16 v0, p1

    #@76
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@79
    .line 1370
    const-string v1, "availability"

    #@7b
    move-object/from16 v0, p1

    #@7d
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@80
    .line 1371
    const-string v1, "hasAlarm"

    #@82
    move-object/from16 v0, p1

    #@84
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@87
    .line 1372
    const-string v1, "hasExtendedProperties"

    #@89
    move-object/from16 v0, p1

    #@8b
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@8e
    .line 1374
    const-string/jumbo v1, "rrule"

    #@91
    move-object/from16 v0, p1

    #@93
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@96
    .line 1375
    const-string/jumbo v1, "rdate"

    #@99
    move-object/from16 v0, p1

    #@9b
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@9e
    .line 1376
    const-string v1, "exrule"

    #@a0
    move-object/from16 v0, p1

    #@a2
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@a5
    .line 1377
    const-string v1, "exdate"

    #@a7
    move-object/from16 v0, p1

    #@a9
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@ac
    .line 1378
    const-string/jumbo v1, "original_sync_id"

    #@af
    move-object/from16 v0, p1

    #@b1
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@b4
    .line 1379
    const-string/jumbo v1, "original_id"

    #@b7
    move-object/from16 v0, p1

    #@b9
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@bc
    .line 1380
    const-string/jumbo v1, "originalInstanceTime"

    #@bf
    move-object/from16 v0, p1

    #@c1
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@c4
    .line 1382
    const-string/jumbo v1, "originalAllDay"

    #@c7
    move-object/from16 v0, p1

    #@c9
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@cc
    .line 1383
    const-string/jumbo v1, "lastDate"

    #@cf
    move-object/from16 v0, p1

    #@d1
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@d4
    .line 1384
    const-string v1, "hasAttendeeData"

    #@d6
    move-object/from16 v0, p1

    #@d8
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@db
    .line 1385
    const-string v1, "guestsCanInviteOthers"

    #@dd
    move-object/from16 v0, p1

    #@df
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@e2
    .line 1387
    const-string v1, "guestsCanModify"

    #@e4
    move-object/from16 v0, p1

    #@e6
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@e9
    .line 1388
    const-string v1, "guestsCanSeeGuests"

    #@eb
    move-object/from16 v0, p1

    #@ed
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@f0
    .line 1389
    const-string v1, "customAppPackage"

    #@f2
    move-object/from16 v0, p1

    #@f4
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@f7
    .line 1390
    const-string v1, "customAppUri"

    #@f9
    move-object/from16 v0, p1

    #@fb
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@fe
    .line 1391
    const-string/jumbo v1, "uid2445"

    #@101
    move-object/from16 v0, p1

    #@103
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@106
    .line 1392
    const-string/jumbo v1, "organizer"

    #@109
    move-object/from16 v0, p1

    #@10b
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@10e
    .line 1393
    const-string v1, "isOrganizer"

    #@110
    move-object/from16 v0, p1

    #@112
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@115
    .line 1394
    const-string v1, "_sync_id"

    #@117
    move-object/from16 v0, p1

    #@119
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@11c
    .line 1395
    const-string v1, "dirty"

    #@11e
    move-object/from16 v0, p1

    #@120
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@123
    .line 1396
    const-string/jumbo v1, "lastSynced"

    #@126
    move-object/from16 v0, p1

    #@128
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@12b
    .line 1397
    const-string v1, "deleted"

    #@12d
    move-object/from16 v0, p1

    #@12f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@132
    .line 1398
    const-string/jumbo v1, "sync_data1"

    #@135
    move-object/from16 v0, p1

    #@137
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@13a
    .line 1399
    const-string/jumbo v1, "sync_data2"

    #@13d
    move-object/from16 v0, p1

    #@13f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@142
    .line 1400
    const-string/jumbo v1, "sync_data3"

    #@145
    move-object/from16 v0, p1

    #@147
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@14a
    .line 1401
    const-string/jumbo v1, "sync_data4"

    #@14d
    move-object/from16 v0, p1

    #@14f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@152
    .line 1402
    const-string/jumbo v1, "sync_data5"

    #@155
    move-object/from16 v0, p1

    #@157
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@15a
    .line 1403
    const-string/jumbo v1, "sync_data6"

    #@15d
    move-object/from16 v0, p1

    #@15f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@162
    .line 1404
    const-string/jumbo v1, "sync_data7"

    #@165
    move-object/from16 v0, p1

    #@167
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@16a
    .line 1405
    const-string/jumbo v1, "sync_data8"

    #@16d
    move-object/from16 v0, p1

    #@16f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@172
    .line 1406
    const-string/jumbo v1, "sync_data9"

    #@175
    move-object/from16 v0, p1

    #@177
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@17a
    .line 1407
    const-string/jumbo v1, "sync_data10"

    #@17d
    move-object/from16 v0, p1

    #@17f
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@182
    .line 1408
    const-string v1, "cal_sync1"

    #@184
    move-object/from16 v0, p1

    #@186
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@189
    .line 1409
    const-string v1, "cal_sync2"

    #@18b
    move-object/from16 v0, p1

    #@18d
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@190
    .line 1410
    const-string v1, "cal_sync3"

    #@192
    move-object/from16 v0, p1

    #@194
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@197
    .line 1411
    const-string v1, "cal_sync4"

    #@199
    move-object/from16 v0, p1

    #@19b
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@19e
    .line 1412
    const-string v1, "cal_sync5"

    #@1a0
    move-object/from16 v0, p1

    #@1a2
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1a5
    .line 1413
    const-string v1, "cal_sync6"

    #@1a7
    move-object/from16 v0, p1

    #@1a9
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1ac
    .line 1414
    const-string v1, "cal_sync7"

    #@1ae
    move-object/from16 v0, p1

    #@1b0
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1b3
    .line 1415
    const-string v1, "cal_sync8"

    #@1b5
    move-object/from16 v0, p1

    #@1b7
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1ba
    .line 1416
    const-string v1, "cal_sync9"

    #@1bc
    move-object/from16 v0, p1

    #@1be
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1c1
    .line 1417
    const-string v1, "cal_sync10"

    #@1c3
    move-object/from16 v0, p1

    #@1c5
    invoke-static {v0, v8, v1}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1c8
    .line 1419
    new-instance v9, Landroid/content/Entity;

    #@1ca
    invoke-direct {v9, v8}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    #@1cd
    .line 1421
    .local v9, entity:Landroid/content/Entity;
    move-object/from16 v0, p0

    #@1cf
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@1d1
    if-eqz v1, :cond_220

    #@1d3
    .line 1422
    move-object/from16 v0, p0

    #@1d5
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@1d7
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    #@1d9
    sget-object v3, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->REMINDERS_PROJECTION:[Ljava/lang/String;

    #@1db
    const-string v4, "event_id=?"

    #@1dd
    const/4 v5, 0x1

    #@1de
    new-array v5, v5, [Ljava/lang/String;

    #@1e0
    const/4 v6, 0x0

    #@1e1
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1e4
    move-result-object v15

    #@1e5
    aput-object v15, v5, v6

    #@1e7
    const/4 v6, 0x0

    #@1e8
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1eb
    move-result-object v14

    #@1ec
    .line 1433
    .local v14, subCursor:Landroid/database/Cursor;
    :goto_1ec
    :try_start_1ec
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@1ef
    move-result v1

    #@1f0
    if-eqz v1, :cond_23a

    #@1f2
    .line 1434
    new-instance v13, Landroid/content/ContentValues;

    #@1f4
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    #@1f7
    .line 1435
    .local v13, reminderValues:Landroid/content/ContentValues;
    const-string/jumbo v1, "minutes"

    #@1fa
    const/4 v2, 0x0

    #@1fb
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    #@1fe
    move-result v2

    #@1ff
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@202
    move-result-object v2

    #@203
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@206
    .line 1436
    const-string/jumbo v1, "method"

    #@209
    const/4 v2, 0x1

    #@20a
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    #@20d
    move-result v2

    #@20e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@211
    move-result-object v2

    #@212
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@215
    .line 1437
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    #@217
    invoke-virtual {v9, v1, v13}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V
    :try_end_21a
    .catchall {:try_start_1ec .. :try_end_21a} :catchall_21b

    #@21a
    goto :goto_1ec

    #@21b
    .line 1440
    .end local v13           #reminderValues:Landroid/content/ContentValues;
    :catchall_21b
    move-exception v1

    #@21c
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@21f
    throw v1

    #@220
    .line 1427
    .end local v14           #subCursor:Landroid/database/Cursor;
    :cond_220
    move-object/from16 v0, p0

    #@222
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mProvider:Landroid/content/ContentProviderClient;

    #@224
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    #@226
    sget-object v3, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->REMINDERS_PROJECTION:[Ljava/lang/String;

    #@228
    const-string v4, "event_id=?"

    #@22a
    const/4 v5, 0x1

    #@22b
    new-array v5, v5, [Ljava/lang/String;

    #@22d
    const/4 v6, 0x0

    #@22e
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@231
    move-result-object v15

    #@232
    aput-object v15, v5, v6

    #@234
    const/4 v6, 0x0

    #@235
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@238
    move-result-object v14

    #@239
    .restart local v14       #subCursor:Landroid/database/Cursor;
    goto :goto_1ec

    #@23a
    .line 1440
    :cond_23a
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@23d
    .line 1443
    move-object/from16 v0, p0

    #@23f
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@241
    if-eqz v1, :cond_2c4

    #@243
    .line 1444
    move-object/from16 v0, p0

    #@245
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@247
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    #@249
    sget-object v3, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    #@24b
    const-string v4, "event_id=?"

    #@24d
    const/4 v5, 0x1

    #@24e
    new-array v5, v5, [Ljava/lang/String;

    #@250
    const/4 v6, 0x0

    #@251
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@254
    move-result-object v15

    #@255
    aput-object v15, v5, v6

    #@257
    const/4 v6, 0x0

    #@258
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@25b
    move-result-object v14

    #@25c
    .line 1455
    :goto_25c
    :try_start_25c
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@25f
    move-result v1

    #@260
    if-eqz v1, :cond_2df

    #@262
    .line 1456
    new-instance v7, Landroid/content/ContentValues;

    #@264
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@267
    .line 1457
    .local v7, attendeeValues:Landroid/content/ContentValues;
    const-string v1, "attendeeName"

    #@269
    const/4 v2, 0x0

    #@26a
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@26d
    move-result-object v2

    #@26e
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@271
    .line 1459
    const-string v1, "attendeeEmail"

    #@273
    const/4 v2, 0x1

    #@274
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@277
    move-result-object v2

    #@278
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@27b
    .line 1461
    const-string v1, "attendeeRelationship"

    #@27d
    const/4 v2, 0x2

    #@27e
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    #@281
    move-result v2

    #@282
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@285
    move-result-object v2

    #@286
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@289
    .line 1463
    const-string v1, "attendeeType"

    #@28b
    const/4 v2, 0x3

    #@28c
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    #@28f
    move-result v2

    #@290
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@293
    move-result-object v2

    #@294
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@297
    .line 1465
    const-string v1, "attendeeStatus"

    #@299
    const/4 v2, 0x4

    #@29a
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getInt(I)I

    #@29d
    move-result v2

    #@29e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a1
    move-result-object v2

    #@2a2
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2a5
    .line 1467
    const-string v1, "attendeeIdentity"

    #@2a7
    const/4 v2, 0x5

    #@2a8
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2ab
    move-result-object v2

    #@2ac
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2af
    .line 1469
    const-string v1, "attendeeIdNamespace"

    #@2b1
    const/4 v2, 0x6

    #@2b2
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2b5
    move-result-object v2

    #@2b6
    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2b9
    .line 1471
    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    #@2bb
    invoke-virtual {v9, v1, v7}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V
    :try_end_2be
    .catchall {:try_start_25c .. :try_end_2be} :catchall_2bf

    #@2be
    goto :goto_25c

    #@2bf
    .line 1474
    .end local v7           #attendeeValues:Landroid/content/ContentValues;
    :catchall_2bf
    move-exception v1

    #@2c0
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@2c3
    throw v1

    #@2c4
    .line 1449
    :cond_2c4
    move-object/from16 v0, p0

    #@2c6
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mProvider:Landroid/content/ContentProviderClient;

    #@2c8
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    #@2ca
    sget-object v3, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->ATTENDEES_PROJECTION:[Ljava/lang/String;

    #@2cc
    const-string v4, "event_id=?"

    #@2ce
    const/4 v5, 0x1

    #@2cf
    new-array v5, v5, [Ljava/lang/String;

    #@2d1
    const/4 v6, 0x0

    #@2d2
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2d5
    move-result-object v15

    #@2d6
    aput-object v15, v5, v6

    #@2d8
    const/4 v6, 0x0

    #@2d9
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2dc
    move-result-object v14

    #@2dd
    goto/16 :goto_25c

    #@2df
    .line 1474
    :cond_2df
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@2e2
    .line 1477
    move-object/from16 v0, p0

    #@2e4
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@2e6
    if-eqz v1, :cond_337

    #@2e8
    .line 1478
    move-object/from16 v0, p0

    #@2ea
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mResolver:Landroid/content/ContentResolver;

    #@2ec
    sget-object v2, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    #@2ee
    sget-object v3, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->EXTENDED_PROJECTION:[Ljava/lang/String;

    #@2f0
    const-string v4, "event_id=?"

    #@2f2
    const/4 v5, 0x1

    #@2f3
    new-array v5, v5, [Ljava/lang/String;

    #@2f5
    const/4 v6, 0x0

    #@2f6
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2f9
    move-result-object v15

    #@2fa
    aput-object v15, v5, v6

    #@2fc
    const/4 v6, 0x0

    #@2fd
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@300
    move-result-object v14

    #@301
    .line 1489
    :goto_301
    :try_start_301
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@304
    move-result v1

    #@305
    if-eqz v1, :cond_351

    #@307
    .line 1490
    new-instance v12, Landroid/content/ContentValues;

    #@309
    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    #@30c
    .line 1491
    .local v12, extendedValues:Landroid/content/ContentValues;
    const-string v1, "_id"

    #@30e
    const/4 v2, 0x0

    #@30f
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@312
    move-result-object v2

    #@313
    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@316
    .line 1493
    const-string/jumbo v1, "name"

    #@319
    const/4 v2, 0x1

    #@31a
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@31d
    move-result-object v2

    #@31e
    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@321
    .line 1495
    const-string/jumbo v1, "value"

    #@324
    const/4 v2, 0x2

    #@325
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@328
    move-result-object v2

    #@329
    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@32c
    .line 1497
    sget-object v1, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    #@32e
    invoke-virtual {v9, v1, v12}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V
    :try_end_331
    .catchall {:try_start_301 .. :try_end_331} :catchall_332

    #@331
    goto :goto_301

    #@332
    .line 1500
    .end local v12           #extendedValues:Landroid/content/ContentValues;
    :catchall_332
    move-exception v1

    #@333
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@336
    throw v1

    #@337
    .line 1483
    :cond_337
    move-object/from16 v0, p0

    #@339
    iget-object v1, v0, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->mProvider:Landroid/content/ContentProviderClient;

    #@33b
    sget-object v2, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    #@33d
    sget-object v3, Landroid/provider/CalendarContract$EventsEntity$EntityIteratorImpl;->EXTENDED_PROJECTION:[Ljava/lang/String;

    #@33f
    const-string v4, "event_id=?"

    #@341
    const/4 v5, 0x1

    #@342
    new-array v5, v5, [Ljava/lang/String;

    #@344
    const/4 v6, 0x0

    #@345
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@348
    move-result-object v15

    #@349
    aput-object v15, v5, v6

    #@34b
    const/4 v6, 0x0

    #@34c
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@34f
    move-result-object v14

    #@350
    goto :goto_301

    #@351
    .line 1500
    :cond_351
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@354
    .line 1503
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    #@357
    .line 1504
    return-object v9
.end method
