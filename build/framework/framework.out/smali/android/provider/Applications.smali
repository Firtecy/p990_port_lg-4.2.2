.class public Landroid/provider/Applications;
.super Ljava/lang/Object;
.source "Applications.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/Applications$ApplicationColumns;
    }
.end annotation


# static fields
.field public static final APPLICATION_DIR_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.android.application"

.field public static final APPLICATION_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.android.application"

.field public static final APPLICATION_PATH:Ljava/lang/String; = "applications"

.field private static final APPLICATION_SUB_TYPE:Ljava/lang/String; = "vnd.android.application"

.field public static final AUTHORITY:Ljava/lang/String; = "applications"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final SEARCH_PATH:Ljava/lang/String; = "search"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 41
    const-string v0, "content://applications"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Applications;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static componentNameToUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "packageName"
    .parameter "className"

    #@0
    .prologue
    .line 111
    sget-object v0, Landroid/provider/Applications;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@5
    move-result-object v0

    #@6
    const-string v1, "applications"

    #@8
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@17
    move-result-object v0

    #@18
    return-object v0
.end method

.method public static search(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 8
    .parameter "resolver"
    .parameter "query"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 77
    sget-object v0, Landroid/provider/Applications;->CONTENT_URI:Landroid/net/Uri;

    #@3
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@6
    move-result-object v0

    #@7
    const-string/jumbo v3, "search"

    #@a
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@15
    move-result-object v1

    #@16
    .local v1, searchUri:Landroid/net/Uri;
    move-object v0, p0

    #@17
    move-object v3, v2

    #@18
    move-object v4, v2

    #@19
    move-object v5, v2

    #@1a
    .line 78
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1d
    move-result-object v0

    #@1e
    return-object v0
.end method

.method public static uriToComponentName(Landroid/net/Uri;)Landroid/content/ComponentName;
    .registers 7
    .parameter "appUri"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 91
    if-nez p0, :cond_4

    #@3
    .line 99
    :cond_3
    :goto_3
    return-object v3

    #@4
    .line 92
    :cond_4
    const-string v4, "content"

    #@6
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@9
    move-result-object v5

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_3

    #@10
    .line 93
    const-string v4, "applications"

    #@12
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_3

    #@1c
    .line 94
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@1f
    move-result-object v2

    #@20
    .line 95
    .local v2, pathSegments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@23
    move-result v4

    #@24
    const/4 v5, 0x3

    #@25
    if-ne v4, v5, :cond_3

    #@27
    .line 96
    const-string v4, "applications"

    #@29
    const/4 v5, 0x0

    #@2a
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v4

    #@32
    if-eqz v4, :cond_3

    #@34
    .line 97
    const/4 v3, 0x1

    #@35
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v1

    #@39
    check-cast v1, Ljava/lang/String;

    #@3b
    .line 98
    .local v1, packageName:Ljava/lang/String;
    const/4 v3, 0x2

    #@3c
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Ljava/lang/String;

    #@42
    .line 99
    .local v0, name:Ljava/lang/String;
    new-instance v3, Landroid/content/ComponentName;

    #@44
    invoke-direct {v3, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    goto :goto_3
.end method
