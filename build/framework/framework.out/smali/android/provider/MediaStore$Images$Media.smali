.class public final Landroid/provider/MediaStore$Images$Media;
.super Ljava/lang/Object;
.source "MediaStore.java"

# interfaces
.implements Landroid/provider/MediaStore$Images$ImageColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/MediaStore$Images;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Media"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/image"

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "bucket_display_name"

.field public static final EXTERNAL_CONTENT_URI:Landroid/net/Uri;

.field public static final INTERNAL_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 973
    const-string v0, "internal"

    #@2
    invoke-static {v0}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 980
    const-string v0, "external"

    #@a
    invoke-static {v0}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@10
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 798
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static final StoreThumbnail(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;JFFI)Landroid/graphics/Bitmap;
    .registers 23
    .parameter "cr"
    .parameter "source"
    .parameter "id"
    .parameter "width"
    .parameter "height"
    .parameter "kind"

    #@0
    .prologue
    .line 865
    new-instance v7, Landroid/graphics/Matrix;

    #@2
    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    #@5
    .line 867
    .local v7, matrix:Landroid/graphics/Matrix;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@8
    move-result v2

    #@9
    int-to-float v2, v2

    #@a
    div-float v10, p4, v2

    #@c
    .line 868
    .local v10, scaleX:F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@f
    move-result v2

    #@10
    int-to-float v2, v2

    #@11
    div-float v11, p5, v2

    #@13
    .line 870
    .local v11, scaleY:F
    invoke-virtual {v7, v10, v11}, Landroid/graphics/Matrix;->setScale(FF)V

    #@16
    .line 872
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x0

    #@18
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@1b
    move-result v5

    #@1c
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@1f
    move-result v6

    #@20
    const/4 v8, 0x1

    #@21
    move-object/from16 v2, p1

    #@23
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    #@26
    move-result-object v12

    #@27
    .line 877
    .local v12, thumb:Landroid/graphics/Bitmap;
    new-instance v15, Landroid/content/ContentValues;

    #@29
    const/4 v2, 0x4

    #@2a
    invoke-direct {v15, v2}, Landroid/content/ContentValues;-><init>(I)V

    #@2d
    .line 878
    .local v15, values:Landroid/content/ContentValues;
    const-string/jumbo v2, "kind"

    #@30
    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@37
    .line 879
    const-string v2, "image_id"

    #@39
    move-wide/from16 v0, p2

    #@3b
    long-to-int v3, v0

    #@3c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@43
    .line 880
    const-string v2, "height"

    #@45
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    #@48
    move-result v3

    #@49
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@50
    .line 881
    const-string/jumbo v2, "width"

    #@53
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    #@56
    move-result v3

    #@57
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5e
    .line 883
    sget-object v2, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@60
    move-object/from16 v0, p0

    #@62
    invoke-virtual {v0, v2, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@65
    move-result-object v14

    #@66
    .line 886
    .local v14, url:Landroid/net/Uri;
    :try_start_66
    move-object/from16 v0, p0

    #@68
    invoke-virtual {v0, v14}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    #@6b
    move-result-object v13

    #@6c
    .line 888
    .local v13, thumbOut:Ljava/io/OutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@6e
    const/16 v3, 0x64

    #@70
    invoke-virtual {v12, v2, v3, v13}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@73
    .line 889
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V
    :try_end_76
    .catch Ljava/io/FileNotFoundException; {:try_start_66 .. :try_end_76} :catch_77
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_76} :catch_7a

    #@76
    .line 896
    .end local v12           #thumb:Landroid/graphics/Bitmap;
    .end local v13           #thumbOut:Ljava/io/OutputStream;
    :goto_76
    return-object v12

    #@77
    .line 892
    .restart local v12       #thumb:Landroid/graphics/Bitmap;
    :catch_77
    move-exception v9

    #@78
    .line 893
    .local v9, ex:Ljava/io/FileNotFoundException;
    const/4 v12, 0x0

    #@79
    goto :goto_76

    #@7a
    .line 895
    .end local v9           #ex:Ljava/io/FileNotFoundException;
    :catch_7a
    move-exception v9

    #@7b
    .line 896
    .local v9, ex:Ljava/io/IOException;
    const/4 v12, 0x0

    #@7c
    goto :goto_76
.end method

.method public static final getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "cr"
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 825
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@3
    move-result-object v1

    #@4
    .line 826
    .local v1, input:Ljava/io/InputStream;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    #@7
    move-result-object v0

    #@8
    .line 827
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    #@b
    .line 828
    return-object v0
.end method

.method public static getContentUri(Ljava/lang/String;)Landroid/net/Uri;
    .registers 3
    .parameter "volumeName"

    #@0
    .prologue
    .line 966
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "content://media/"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "/images/media"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public static final insertImage(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 16
    .parameter "cr"
    .parameter "source"
    .parameter "title"
    .parameter "description"

    #@0
    .prologue
    .line 912
    new-instance v11, Landroid/content/ContentValues;

    #@2
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 913
    .local v11, values:Landroid/content/ContentValues;
    const-string/jumbo v0, "title"

    #@8
    invoke-virtual {v11, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 914
    const-string v0, "description"

    #@d
    invoke-virtual {v11, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 915
    const-string/jumbo v0, "mime_type"

    #@13
    const-string v4, "image/jpeg"

    #@15
    invoke-virtual {v11, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 917
    const/4 v10, 0x0

    #@19
    .line 918
    .local v10, url:Landroid/net/Uri;
    const/4 v9, 0x0

    #@1a
    .line 921
    .local v9, stringUrl:Ljava/lang/String;
    :try_start_1a
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@1c
    invoke-virtual {p0, v0, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1f
    move-result-object v10

    #@20
    .line 923
    if-eqz p1, :cond_60

    #@22
    .line 924
    invoke-virtual {p0, v10}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_25} :catch_4f

    #@25
    move-result-object v8

    #@26
    .line 926
    .local v8, imageOut:Ljava/io/OutputStream;
    :try_start_26
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@28
    const/16 v4, 0x32

    #@2a
    invoke-virtual {p1, v0, v4, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2d
    .catchall {:try_start_26 .. :try_end_2d} :catchall_4a

    #@2d
    .line 928
    :try_start_2d
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    #@30
    .line 931
    invoke-static {v10}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@33
    move-result-wide v2

    #@34
    .line 933
    .local v2, id:J
    const/4 v0, 0x1

    #@35
    const/4 v4, 0x0

    #@36
    invoke-static {p0, v2, v3, v0, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@39
    move-result-object v1

    #@3a
    .line 936
    .local v1, miniThumb:Landroid/graphics/Bitmap;
    const/high16 v4, 0x4248

    #@3c
    const/high16 v5, 0x4248

    #@3e
    const/4 v6, 0x3

    #@3f
    move-object v0, p0

    #@40
    invoke-static/range {v0 .. v6}, Landroid/provider/MediaStore$Images$Media;->StoreThumbnail(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;JFFI)Landroid/graphics/Bitmap;
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_43} :catch_4f

    #@43
    .line 951
    .end local v1           #miniThumb:Landroid/graphics/Bitmap;
    .end local v2           #id:J
    .end local v8           #imageOut:Ljava/io/OutputStream;
    :cond_43
    :goto_43
    if-eqz v10, :cond_49

    #@45
    .line 952
    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    .line 955
    :cond_49
    return-object v9

    #@4a
    .line 928
    .restart local v8       #imageOut:Ljava/io/OutputStream;
    :catchall_4a
    move-exception v0

    #@4b
    :try_start_4b
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    #@4e
    throw v0
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_4f} :catch_4f

    #@4f
    .line 943
    .end local v8           #imageOut:Ljava/io/OutputStream;
    :catch_4f
    move-exception v7

    #@50
    .line 944
    .local v7, e:Ljava/lang/Exception;
    const-string v0, "MediaStore"

    #@52
    const-string v4, "Failed to insert image"

    #@54
    invoke-static {v0, v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@57
    .line 945
    if-eqz v10, :cond_43

    #@59
    .line 946
    const/4 v0, 0x0

    #@5a
    const/4 v4, 0x0

    #@5b
    invoke-virtual {p0, v10, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@5e
    .line 947
    const/4 v10, 0x0

    #@5f
    goto :goto_43

    #@60
    .line 939
    .end local v7           #e:Ljava/lang/Exception;
    :cond_60
    :try_start_60
    const-string v0, "MediaStore"

    #@62
    const-string v4, "Failed to create thumbnail, removing original"

    #@64
    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 940
    const/4 v0, 0x0

    #@68
    const/4 v4, 0x0

    #@69
    invoke-virtual {p0, v10, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_60 .. :try_end_6c} :catch_4f

    #@6c
    .line 941
    const/4 v10, 0x0

    #@6d
    goto :goto_43
.end method

.method public static final insertImage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "cr"
    .parameter "imagePath"
    .parameter "name"
    .parameter "description"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 844
    new-instance v2, Ljava/io/FileInputStream;

    #@2
    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@5
    .line 846
    .local v2, stream:Ljava/io/FileInputStream;
    :try_start_5
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@8
    move-result-object v0

    #@9
    .line 847
    .local v0, bm:Landroid/graphics/Bitmap;
    invoke-static {p0, v0, p2, p3}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 848
    .local v1, ret:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_14

    #@10
    .line 852
    :try_start_10
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_13} :catch_19

    #@13
    .line 854
    :goto_13
    return-object v1

    #@14
    .line 851
    .end local v0           #bm:Landroid/graphics/Bitmap;
    .end local v1           #ret:Ljava/lang/String;
    :catchall_14
    move-exception v3

    #@15
    .line 852
    :try_start_15
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_18} :catch_1b

    #@18
    .line 854
    :goto_18
    throw v3

    #@19
    .line 853
    .restart local v0       #bm:Landroid/graphics/Bitmap;
    .restart local v1       #ret:Ljava/lang/String;
    :catch_19
    move-exception v3

    #@1a
    goto :goto_13

    #@1b
    .end local v0           #bm:Landroid/graphics/Bitmap;
    .end local v1           #ret:Ljava/lang/String;
    :catch_1b
    move-exception v4

    #@1c
    goto :goto_18
.end method

.method public static final query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 9
    .parameter "cr"
    .parameter "uri"
    .parameter "projection"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 800
    const-string v5, "bucket_display_name"

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v4, v3

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static final query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 11
    .parameter "cr"
    .parameter "uri"
    .parameter "projection"
    .parameter "where"
    .parameter "orderBy"

    #@0
    .prologue
    .line 805
    const/4 v4, 0x0

    #@1
    if-nez p4, :cond_e

    #@3
    const-string v5, "bucket_display_name"

    #@5
    :goto_5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move-object v2, p2

    #@8
    move-object v3, p3

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v0

    #@d
    return-object v0

    #@e
    :cond_e
    move-object v5, p4

    #@f
    goto :goto_5
.end method

.method public static final query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "cr"
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "orderBy"

    #@0
    .prologue
    .line 811
    if-nez p5, :cond_e

    #@2
    const-string v5, "bucket_display_name"

    #@4
    :goto_4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move-object v3, p3

    #@8
    move-object v4, p4

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v0

    #@d
    return-object v0

    #@e
    :cond_e
    move-object v5, p5

    #@f
    goto :goto_4
.end method
