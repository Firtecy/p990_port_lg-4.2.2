.class public final Landroid/provider/BrowserContract$SyncState;
.super Ljava/lang/Object;
.source "BrowserContract.java"

# interfaces
.implements Landroid/provider/SyncStateContract$Columns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/BrowserContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SyncState"
.end annotation


# static fields
.field public static final CONTENT_DIRECTORY:Ljava/lang/String; = "syncstate"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 518
    sget-object v0, Landroid/provider/BrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@2
    const-string/jumbo v1, "syncstate"

    #@5
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Landroid/provider/BrowserContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    #@b
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 510
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static get(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)[B
    .registers 3
    .parameter "provider"
    .parameter "account"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 526
    sget-object v0, Landroid/provider/BrowserContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-static {p0, v0, p1}, Landroid/provider/SyncStateContract$Helpers;->get(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static getWithUri(Landroid/content/ContentProviderClient;Landroid/accounts/Account;)Landroid/util/Pair;
    .registers 3
    .parameter "provider"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/accounts/Account;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 534
    sget-object v0, Landroid/provider/BrowserContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-static {p0, v0, p1}, Landroid/provider/SyncStateContract$Helpers;->getWithUri(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)Landroid/util/Pair;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static newSetOperation(Landroid/accounts/Account;[B)Landroid/content/ContentProviderOperation;
    .registers 3
    .parameter "account"
    .parameter "data"

    #@0
    .prologue
    .line 549
    sget-object v0, Landroid/provider/BrowserContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-static {v0, p0, p1}, Landroid/provider/SyncStateContract$Helpers;->newSetOperation(Landroid/net/Uri;Landroid/accounts/Account;[B)Landroid/content/ContentProviderOperation;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static set(Landroid/content/ContentProviderClient;Landroid/accounts/Account;[B)V
    .registers 4
    .parameter "provider"
    .parameter "account"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 542
    sget-object v0, Landroid/provider/BrowserContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-static {p0, v0, p1, p2}, Landroid/provider/SyncStateContract$Helpers;->set(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;[B)V

    #@5
    .line 543
    return-void
.end method
