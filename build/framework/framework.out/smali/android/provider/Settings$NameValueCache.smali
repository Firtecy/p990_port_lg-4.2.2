.class Landroid/provider/Settings$NameValueCache;
.super Ljava/lang/Object;
.source "Settings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NameValueCache"
.end annotation


# static fields
.field private static final NAME_EQ_PLACEHOLDER:Ljava/lang/String; = "name=?"

.field private static final SELECT_VALUE:[Ljava/lang/String;


# instance fields
.field private final mCallGetCommand:Ljava/lang/String;

.field private final mCallSetCommand:Ljava/lang/String;

.field private mContentProvider:Landroid/content/IContentProvider;

.field private final mUri:Landroid/net/Uri;

.field private final mValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mValuesVersion:J

.field private final mVersionSystemProperty:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 739
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string/jumbo v2, "value"

    #@7
    aput-object v2, v0, v1

    #@9
    sput-object v0, Landroid/provider/Settings$NameValueCache;->SELECT_VALUE:[Ljava/lang/String;

    #@b
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "versionSystemProperty"
    .parameter "uri"
    .parameter "getCommand"
    .parameter "setCommand"

    #@0
    .prologue
    .line 756
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 744
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/provider/Settings$NameValueCache;->mValues:Ljava/util/HashMap;

    #@a
    .line 745
    const-wide/16 v0, 0x0

    #@c
    iput-wide v0, p0, Landroid/provider/Settings$NameValueCache;->mValuesVersion:J

    #@e
    .line 748
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Landroid/provider/Settings$NameValueCache;->mContentProvider:Landroid/content/IContentProvider;

    #@11
    .line 757
    iput-object p1, p0, Landroid/provider/Settings$NameValueCache;->mVersionSystemProperty:Ljava/lang/String;

    #@13
    .line 758
    iput-object p2, p0, Landroid/provider/Settings$NameValueCache;->mUri:Landroid/net/Uri;

    #@15
    .line 759
    iput-object p3, p0, Landroid/provider/Settings$NameValueCache;->mCallGetCommand:Ljava/lang/String;

    #@17
    .line 760
    iput-object p4, p0, Landroid/provider/Settings$NameValueCache;->mCallSetCommand:Ljava/lang/String;

    #@19
    .line 761
    return-void
.end method

.method private lazyGetProvider(Landroid/content/ContentResolver;)Landroid/content/IContentProvider;
    .registers 5
    .parameter "cr"

    #@0
    .prologue
    .line 764
    const/4 v0, 0x0

    #@1
    .line 765
    .local v0, cp:Landroid/content/IContentProvider;
    monitor-enter p0

    #@2
    .line 766
    :try_start_2
    iget-object v0, p0, Landroid/provider/Settings$NameValueCache;->mContentProvider:Landroid/content/IContentProvider;

    #@4
    .line 767
    if-nez v0, :cond_13

    #@6
    .line 768
    iget-object v2, p0, Landroid/provider/Settings$NameValueCache;->mUri:Landroid/net/Uri;

    #@8
    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {p1, v2}, Landroid/content/ContentResolver;->acquireProvider(Ljava/lang/String;)Landroid/content/IContentProvider;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Landroid/provider/Settings$NameValueCache;->mContentProvider:Landroid/content/IContentProvider;

    #@12
    .end local v0           #cp:Landroid/content/IContentProvider;
    .local v1, cp:Landroid/content/IContentProvider;
    move-object v0, v1

    #@13
    .line 770
    .end local v1           #cp:Landroid/content/IContentProvider;
    .restart local v0       #cp:Landroid/content/IContentProvider;
    :cond_13
    monitor-exit p0

    #@14
    .line 771
    return-object v0

    #@15
    .line 770
    :catchall_15
    move-exception v2

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_2 .. :try_end_17} :catchall_15

    #@17
    throw v2
.end method


# virtual methods
.method public getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
    .registers 22
    .parameter "cr"
    .parameter "name"
    .parameter "userHandle"

    #@0
    .prologue
    .line 813
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v3

    #@4
    move/from16 v0, p3

    #@6
    if-ne v0, v3, :cond_45

    #@8
    const/4 v14, 0x1

    #@9
    .line 814
    .local v14, isSelf:Z
    :goto_9
    if-eqz v14, :cond_48

    #@b
    .line 815
    move-object/from16 v0, p0

    #@d
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mVersionSystemProperty:Ljava/lang/String;

    #@f
    const-wide/16 v4, 0x0

    #@11
    invoke-static {v3, v4, v5}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@14
    move-result-wide v15

    #@15
    .line 818
    .local v15, newValuesVersion:J
    monitor-enter p0

    #@16
    .line 819
    :try_start_16
    move-object/from16 v0, p0

    #@18
    iget-wide v3, v0, Landroid/provider/Settings$NameValueCache;->mValuesVersion:J

    #@1a
    cmp-long v3, v3, v15

    #@1c
    if-eqz v3, :cond_29

    #@1e
    .line 825
    move-object/from16 v0, p0

    #@20
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mValues:Ljava/util/HashMap;

    #@22
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@25
    .line 826
    move-object/from16 v0, p0

    #@27
    iput-wide v15, v0, Landroid/provider/Settings$NameValueCache;->mValuesVersion:J

    #@29
    .line 829
    :cond_29
    move-object/from16 v0, p0

    #@2b
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mValues:Ljava/util/HashMap;

    #@2d
    move-object/from16 v0, p2

    #@2f
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_47

    #@35
    .line 830
    move-object/from16 v0, p0

    #@37
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mValues:Ljava/util/HashMap;

    #@39
    move-object/from16 v0, p2

    #@3b
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Ljava/lang/String;

    #@41
    monitor-exit p0

    #@42
    move-object/from16 v17, v3

    #@44
    .line 896
    .end local v15           #newValuesVersion:J
    :cond_44
    :goto_44
    return-object v17

    #@45
    .line 813
    .end local v14           #isSelf:Z
    :cond_45
    const/4 v14, 0x0

    #@46
    goto :goto_9

    #@47
    .line 832
    .restart local v14       #isSelf:Z
    .restart local v15       #newValuesVersion:J
    :cond_47
    monitor-exit p0
    :try_end_48
    .catchall {:try_start_16 .. :try_end_48} :catchall_cf

    #@48
    .line 838
    .end local v15           #newValuesVersion:J
    :cond_48
    invoke-direct/range {p0 .. p1}, Landroid/provider/Settings$NameValueCache;->lazyGetProvider(Landroid/content/ContentResolver;)Landroid/content/IContentProvider;

    #@4b
    move-result-object v2

    #@4c
    .line 844
    .local v2, cp:Landroid/content/IContentProvider;
    move-object/from16 v0, p0

    #@4e
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mCallGetCommand:Ljava/lang/String;

    #@50
    if-eqz v3, :cond_86

    #@52
    .line 846
    const/4 v9, 0x0

    #@53
    .line 847
    .local v9, args:Landroid/os/Bundle;
    if-nez v14, :cond_62

    #@55
    .line 848
    :try_start_55
    new-instance v10, Landroid/os/Bundle;

    #@57
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V
    :try_end_5a
    .catch Landroid/os/RemoteException; {:try_start_55 .. :try_end_5a} :catch_85

    #@5a
    .line 849
    .end local v9           #args:Landroid/os/Bundle;
    .local v10, args:Landroid/os/Bundle;
    :try_start_5a
    const-string v3, "_user"

    #@5c
    move/from16 v0, p3

    #@5e
    invoke-virtual {v10, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_61
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_61} :catch_130

    #@61
    move-object v9, v10

    #@62
    .line 851
    .end local v10           #args:Landroid/os/Bundle;
    .restart local v9       #args:Landroid/os/Bundle;
    :cond_62
    :try_start_62
    move-object/from16 v0, p0

    #@64
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mCallGetCommand:Ljava/lang/String;

    #@66
    move-object/from16 v0, p2

    #@68
    invoke-interface {v2, v3, v0, v9}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@6b
    move-result-object v11

    #@6c
    .line 852
    .local v11, b:Landroid/os/Bundle;
    if-eqz v11, :cond_86

    #@6e
    .line 853
    invoke-virtual {v11}, Landroid/os/Bundle;->getPairValue()Ljava/lang/String;

    #@71
    move-result-object v17

    #@72
    .line 855
    .local v17, value:Ljava/lang/String;
    if-eqz v14, :cond_44

    #@74
    .line 856
    monitor-enter p0
    :try_end_75
    .catch Landroid/os/RemoteException; {:try_start_62 .. :try_end_75} :catch_85

    #@75
    .line 857
    :try_start_75
    move-object/from16 v0, p0

    #@77
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mValues:Ljava/util/HashMap;

    #@79
    move-object/from16 v0, p2

    #@7b
    move-object/from16 v1, v17

    #@7d
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@80
    .line 858
    monitor-exit p0

    #@81
    goto :goto_44

    #@82
    :catchall_82
    move-exception v3

    #@83
    monitor-exit p0
    :try_end_84
    .catchall {:try_start_75 .. :try_end_84} :catchall_82

    #@84
    :try_start_84
    throw v3
    :try_end_85
    .catch Landroid/os/RemoteException; {:try_start_84 .. :try_end_85} :catch_85

    #@85
    .line 868
    .end local v11           #b:Landroid/os/Bundle;
    .end local v17           #value:Ljava/lang/String;
    :catch_85
    move-exception v3

    #@86
    .line 874
    .end local v9           #args:Landroid/os/Bundle;
    :cond_86
    :goto_86
    const/4 v12, 0x0

    #@87
    .line 876
    .local v12, c:Landroid/database/Cursor;
    :try_start_87
    move-object/from16 v0, p0

    #@89
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mUri:Landroid/net/Uri;

    #@8b
    sget-object v4, Landroid/provider/Settings$NameValueCache;->SELECT_VALUE:[Ljava/lang/String;

    #@8d
    const-string/jumbo v5, "name=?"

    #@90
    const/4 v6, 0x1

    #@91
    new-array v6, v6, [Ljava/lang/String;

    #@93
    const/4 v7, 0x0

    #@94
    aput-object p2, v6, v7

    #@96
    const/4 v7, 0x0

    #@97
    const/4 v8, 0x0

    #@98
    invoke-interface/range {v2 .. v8}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@9b
    move-result-object v12

    #@9c
    .line 878
    if-nez v12, :cond_d2

    #@9e
    .line 879
    const-string v3, "Settings"

    #@a0
    new-instance v4, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v5, "Can\'t get key "

    #@a7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v4

    #@ab
    move-object/from16 v0, p2

    #@ad
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v4

    #@b1
    const-string v5, " from "

    #@b3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v4

    #@b7
    move-object/from16 v0, p0

    #@b9
    iget-object v5, v0, Landroid/provider/Settings$NameValueCache;->mUri:Landroid/net/Uri;

    #@bb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v4

    #@bf
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v4

    #@c3
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c6
    .catchall {:try_start_87 .. :try_end_c6} :catchall_129
    .catch Landroid/os/RemoteException; {:try_start_87 .. :try_end_c6} :catch_f7

    #@c6
    .line 880
    const/16 v17, 0x0

    #@c8
    .line 896
    if-eqz v12, :cond_44

    #@ca
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@cd
    goto/16 :goto_44

    #@cf
    .line 832
    .end local v2           #cp:Landroid/content/IContentProvider;
    .end local v12           #c:Landroid/database/Cursor;
    .restart local v15       #newValuesVersion:J
    :catchall_cf
    move-exception v3

    #@d0
    :try_start_d0
    monitor-exit p0
    :try_end_d1
    .catchall {:try_start_d0 .. :try_end_d1} :catchall_cf

    #@d1
    throw v3

    #@d2
    .line 883
    .end local v15           #newValuesVersion:J
    .restart local v2       #cp:Landroid/content/IContentProvider;
    .restart local v12       #c:Landroid/database/Cursor;
    :cond_d2
    :try_start_d2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    #@d5
    move-result v3

    #@d6
    if-eqz v3, :cond_f1

    #@d8
    const/4 v3, 0x0

    #@d9
    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@dc
    move-result-object v17

    #@dd
    .line 884
    .restart local v17       #value:Ljava/lang/String;
    :goto_dd
    monitor-enter p0
    :try_end_de
    .catchall {:try_start_d2 .. :try_end_de} :catchall_129
    .catch Landroid/os/RemoteException; {:try_start_d2 .. :try_end_de} :catch_f7

    #@de
    .line 885
    :try_start_de
    move-object/from16 v0, p0

    #@e0
    iget-object v3, v0, Landroid/provider/Settings$NameValueCache;->mValues:Ljava/util/HashMap;

    #@e2
    move-object/from16 v0, p2

    #@e4
    move-object/from16 v1, v17

    #@e6
    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e9
    .line 886
    monitor-exit p0
    :try_end_ea
    .catchall {:try_start_de .. :try_end_ea} :catchall_f4

    #@ea
    .line 896
    if-eqz v12, :cond_44

    #@ec
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@ef
    goto/16 :goto_44

    #@f1
    .line 883
    .end local v17           #value:Ljava/lang/String;
    :cond_f1
    const/16 v17, 0x0

    #@f3
    goto :goto_dd

    #@f4
    .line 886
    .restart local v17       #value:Ljava/lang/String;
    :catchall_f4
    move-exception v3

    #@f5
    :try_start_f5
    monitor-exit p0
    :try_end_f6
    .catchall {:try_start_f5 .. :try_end_f6} :catchall_f4

    #@f6
    :try_start_f6
    throw v3
    :try_end_f7
    .catchall {:try_start_f6 .. :try_end_f7} :catchall_129
    .catch Landroid/os/RemoteException; {:try_start_f6 .. :try_end_f7} :catch_f7

    #@f7
    .line 892
    .end local v17           #value:Ljava/lang/String;
    :catch_f7
    move-exception v13

    #@f8
    .line 893
    .local v13, e:Landroid/os/RemoteException;
    :try_start_f8
    const-string v3, "Settings"

    #@fa
    new-instance v4, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v5, "Can\'t get key "

    #@101
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v4

    #@105
    move-object/from16 v0, p2

    #@107
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v4

    #@10b
    const-string v5, " from "

    #@10d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v4

    #@111
    move-object/from16 v0, p0

    #@113
    iget-object v5, v0, Landroid/provider/Settings$NameValueCache;->mUri:Landroid/net/Uri;

    #@115
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v4

    #@119
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v4

    #@11d
    invoke-static {v3, v4, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_120
    .catchall {:try_start_f8 .. :try_end_120} :catchall_129

    #@120
    .line 894
    const/16 v17, 0x0

    #@122
    .line 896
    if-eqz v12, :cond_44

    #@124
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@127
    goto/16 :goto_44

    #@129
    .end local v13           #e:Landroid/os/RemoteException;
    :catchall_129
    move-exception v3

    #@12a
    if-eqz v12, :cond_12f

    #@12c
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@12f
    :cond_12f
    throw v3

    #@130
    .line 868
    .end local v12           #c:Landroid/database/Cursor;
    .restart local v10       #args:Landroid/os/Bundle;
    :catch_130
    move-exception v3

    #@131
    move-object v9, v10

    #@132
    .end local v10           #args:Landroid/os/Bundle;
    .restart local v9       #args:Landroid/os/Bundle;
    goto/16 :goto_86
.end method

.method public putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 13
    .parameter "cr"
    .parameter "name"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 778
    :try_start_1
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v4

    #@5
    if-eqz v4, :cond_82

    #@7
    const-string v4, "airplane_mode_on"

    #@9
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_28

    #@f
    const-string v4, "data_roaming"

    #@11
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_28

    #@17
    const-string v4, "enabled_input_methods"

    #@19
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v4

    #@1d
    if-nez v4, :cond_28

    #@1f
    const-string/jumbo v4, "show_password"

    #@22
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_82

    #@28
    :cond_28
    const-string v4, "0"

    #@2a
    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_82

    #@30
    .line 783
    const-string v4, "airplane_mode_on"

    #@32
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_46

    #@38
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3b
    move-result-object v4

    #@3c
    const/4 v5, 0x0

    #@3d
    const-string v6, "LGMDMAirplaneModeUIAdpater"

    #@3f
    const/4 v7, 0x0

    #@40
    invoke-interface {v4, v5, v6, v7}, Lcom/lge/cappuccino/IMdm;->checkDisabledSettingsProvider(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;)Z

    #@43
    move-result v4

    #@44
    if-nez v4, :cond_81

    #@46
    :cond_46
    const-string v4, "data_roaming"

    #@48
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v4

    #@4c
    if-eqz v4, :cond_5b

    #@4e
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@51
    move-result-object v4

    #@52
    const/4 v5, 0x0

    #@53
    const-string v6, "LGMDMDataRoamingAdapter"

    #@55
    invoke-interface {v4, v5, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@58
    move-result v4

    #@59
    if-nez v4, :cond_81

    #@5b
    :cond_5b
    const-string v4, "enabled_input_methods"

    #@5d
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v4

    #@61
    if-eqz v4, :cond_6e

    #@63
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@66
    move-result-object v4

    #@67
    const/4 v5, 0x0

    #@68
    invoke-interface {v4, v5, p3}, Lcom/lge/cappuccino/IMdm;->checkAllowMicrophoneIME(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@6b
    move-result v4

    #@6c
    if-nez v4, :cond_81

    #@6e
    :cond_6e
    const-string/jumbo v4, "show_password"

    #@71
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v4

    #@75
    if-eqz v4, :cond_82

    #@77
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7a
    move-result-object v4

    #@7b
    invoke-interface {v4}, Lcom/lge/cappuccino/IMdm;->checkDisallowPasswordTypingVisible()Z

    #@7e
    move-result v4

    #@7f
    if-eqz v4, :cond_82

    #@81
    .line 809
    :cond_81
    :goto_81
    return v3

    #@82
    .line 800
    :cond_82
    new-instance v0, Landroid/os/Bundle;

    #@84
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@87
    .line 801
    .local v0, arg:Landroid/os/Bundle;
    const-string/jumbo v4, "value"

    #@8a
    invoke-virtual {v0, v4, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@8d
    .line 802
    const-string v4, "_user"

    #@8f
    invoke-virtual {v0, v4, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@92
    .line 803
    invoke-direct {p0, p1}, Landroid/provider/Settings$NameValueCache;->lazyGetProvider(Landroid/content/ContentResolver;)Landroid/content/IContentProvider;

    #@95
    move-result-object v1

    #@96
    .line 804
    .local v1, cp:Landroid/content/IContentProvider;
    iget-object v4, p0, Landroid/provider/Settings$NameValueCache;->mCallSetCommand:Ljava/lang/String;

    #@98
    invoke-interface {v1, v4, p2, v0}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_9b
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_9b} :catch_9d

    #@9b
    .line 809
    const/4 v3, 0x1

    #@9c
    goto :goto_81

    #@9d
    .line 805
    .end local v0           #arg:Landroid/os/Bundle;
    .end local v1           #cp:Landroid/content/IContentProvider;
    :catch_9d
    move-exception v2

    #@9e
    .line 806
    .local v2, e:Landroid/os/RemoteException;
    const-string v4, "Settings"

    #@a0
    new-instance v5, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v6, "Can\'t set key "

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v5

    #@af
    const-string v6, " in "

    #@b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    iget-object v6, p0, Landroid/provider/Settings$NameValueCache;->mUri:Landroid/net/Uri;

    #@b7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v5

    #@bb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v5

    #@bf
    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c2
    goto :goto_81
.end method