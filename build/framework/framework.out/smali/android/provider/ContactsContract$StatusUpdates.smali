.class public Landroid/provider/ContactsContract$StatusUpdates;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/ContactsContract$StatusColumns;
.implements Landroid/provider/ContactsContract$PresenceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StatusUpdates"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/status-update"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/status-update"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final PROFILE_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 4953
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@2
    const-string/jumbo v1, "status_updates"

    #@5
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Landroid/provider/ContactsContract$StatusUpdates;->CONTENT_URI:Landroid/net/Uri;

    #@b
    .line 4958
    sget-object v0, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    #@d
    const-string/jumbo v1, "status_updates"

    #@10
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@13
    move-result-object v0

    #@14
    sput-object v0, Landroid/provider/ContactsContract$StatusUpdates;->PROFILE_CONTENT_URI:Landroid/net/Uri;

    #@16
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 4948
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/provider/ContactsContract$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 4943
    invoke-direct {p0}, Landroid/provider/ContactsContract$StatusUpdates;-><init>()V

    #@3
    return-void
.end method

.method public static final getPresenceIconResourceId(I)I
    .registers 2
    .parameter "status"

    #@0
    .prologue
    .line 4968
    packed-switch p0, :pswitch_data_18

    #@3
    .line 4980
    const v0, 0x108006a

    #@6
    :goto_6
    return v0

    #@7
    .line 4970
    :pswitch_7
    const v0, 0x108006b

    #@a
    goto :goto_6

    #@b
    .line 4973
    :pswitch_b
    const v0, 0x1080067

    #@e
    goto :goto_6

    #@f
    .line 4975
    :pswitch_f
    const v0, 0x1080068

    #@12
    goto :goto_6

    #@13
    .line 4977
    :pswitch_13
    const v0, 0x1080069

    #@16
    goto :goto_6

    #@17
    .line 4968
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_13
        :pswitch_b
        :pswitch_b
        :pswitch_f
        :pswitch_7
    .end packed-switch
.end method

.method public static final getPresencePrecedence(I)I
    .registers 1
    .parameter "status"

    #@0
    .prologue
    .line 4993
    return p0
.end method
