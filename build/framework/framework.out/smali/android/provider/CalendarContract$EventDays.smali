.class public final Landroid/provider/CalendarContract$EventDays;
.super Ljava/lang/Object;
.source "CalendarContract.java"

# interfaces
.implements Landroid/provider/CalendarContract$EventDaysColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/CalendarContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventDays"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field private static final SELECTION:Ljava/lang/String; = "selected=1"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1993
    const-string v0, "content://com.android.calendar/instances/groupbyday"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/CalendarContract$EventDays;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2000
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final query(Landroid/content/ContentResolver;II[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "cr"
    .parameter "startDay"
    .parameter "numDays"
    .parameter "projection"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2018
    const/4 v0, 0x1

    #@2
    if-ge p2, v0, :cond_5

    #@4
    .line 2025
    :goto_4
    return-object v4

    #@5
    .line 2021
    :cond_5
    add-int v0, p1, p2

    #@7
    add-int/lit8 v7, v0, -0x1

    #@9
    .line 2022
    .local v7, endDay:I
    sget-object v0, Landroid/provider/CalendarContract$EventDays;->CONTENT_URI:Landroid/net/Uri;

    #@b
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@e
    move-result-object v6

    #@f
    .line 2023
    .local v6, builder:Landroid/net/Uri$Builder;
    int-to-long v0, p1

    #@10
    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    #@13
    .line 2024
    int-to-long v0, v7

    #@14
    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    #@17
    .line 2025
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@1a
    move-result-object v1

    #@1b
    const-string/jumbo v3, "selected=1"

    #@1e
    const-string/jumbo v5, "startDay"

    #@21
    move-object v0, p0

    #@22
    move-object v2, p3

    #@23
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@26
    move-result-object v4

    #@27
    goto :goto_4
.end method
