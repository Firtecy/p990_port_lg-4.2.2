.class public Landroid/provider/SearchRecentSuggestions;
.super Ljava/lang/Object;
.source "SearchRecentSuggestions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/SearchRecentSuggestions$SuggestionColumns;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SearchSuggestions"

.field private static final MAX_HISTORY_COUNT:I = 0xfa

.field public static final QUERIES_PROJECTION_1LINE:[Ljava/lang/String; = null

.field public static final QUERIES_PROJECTION_2LINE:[Ljava/lang/String; = null

.field public static final QUERIES_PROJECTION_DATE_INDEX:I = 0x1

.field public static final QUERIES_PROJECTION_DISPLAY1_INDEX:I = 0x3

.field public static final QUERIES_PROJECTION_DISPLAY2_INDEX:I = 0x4

.field public static final QUERIES_PROJECTION_QUERY_INDEX:I = 0x2

.field private static final sWritesInProgress:Ljava/util/concurrent/Semaphore;


# instance fields
.field private final mAuthority:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mSuggestionsUri:Landroid/net/Uri;

.field private final mTwoLineDisplay:Z


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 72
    new-array v0, v6, [Ljava/lang/String;

    #@7
    const-string v1, "_id"

    #@9
    aput-object v1, v0, v2

    #@b
    const-string v1, "date"

    #@d
    aput-object v1, v0, v3

    #@f
    const-string/jumbo v1, "query"

    #@12
    aput-object v1, v0, v4

    #@14
    const-string v1, "display1"

    #@16
    aput-object v1, v0, v5

    #@18
    sput-object v0, Landroid/provider/SearchRecentSuggestions;->QUERIES_PROJECTION_1LINE:[Ljava/lang/String;

    #@1a
    .line 84
    const/4 v0, 0x5

    #@1b
    new-array v0, v0, [Ljava/lang/String;

    #@1d
    const-string v1, "_id"

    #@1f
    aput-object v1, v0, v2

    #@21
    const-string v1, "date"

    #@23
    aput-object v1, v0, v3

    #@25
    const-string/jumbo v1, "query"

    #@28
    aput-object v1, v0, v4

    #@2a
    const-string v1, "display1"

    #@2c
    aput-object v1, v0, v5

    #@2e
    const-string v1, "display2"

    #@30
    aput-object v1, v0, v6

    #@32
    sput-object v0, Landroid/provider/SearchRecentSuggestions;->QUERIES_PROJECTION_2LINE:[Ljava/lang/String;

    #@34
    .line 116
    new-instance v0, Ljava/util/concurrent/Semaphore;

    #@36
    invoke-direct {v0, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    #@39
    sput-object v0, Landroid/provider/SearchRecentSuggestions;->sWritesInProgress:Ljava/util/concurrent/Semaphore;

    #@3b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .registers 6
    .parameter "context"
    .parameter "authority"
    .parameter "mode"

    #@0
    .prologue
    .line 131
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 132
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_d

    #@9
    and-int/lit8 v0, p3, 0x1

    #@b
    if-nez v0, :cond_13

    #@d
    .line 134
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@12
    throw v0

    #@13
    .line 137
    :cond_13
    and-int/lit8 v0, p3, 0x2

    #@15
    if-eqz v0, :cond_45

    #@17
    const/4 v0, 0x1

    #@18
    :goto_18
    iput-boolean v0, p0, Landroid/provider/SearchRecentSuggestions;->mTwoLineDisplay:Z

    #@1a
    .line 140
    iput-object p1, p0, Landroid/provider/SearchRecentSuggestions;->mContext:Landroid/content/Context;

    #@1c
    .line 141
    new-instance v0, Ljava/lang/String;

    #@1e
    invoke-direct {v0, p2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Landroid/provider/SearchRecentSuggestions;->mAuthority:Ljava/lang/String;

    #@23
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v1, "content://"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    iget-object v1, p0, Landroid/provider/SearchRecentSuggestions;->mAuthority:Ljava/lang/String;

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    const-string v1, "/suggestions"

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@41
    move-result-object v0

    #@42
    iput-object v0, p0, Landroid/provider/SearchRecentSuggestions;->mSuggestionsUri:Landroid/net/Uri;

    #@44
    .line 145
    return-void

    #@45
    .line 137
    :cond_45
    const/4 v0, 0x0

    #@46
    goto :goto_18
.end method

.method static synthetic access$000(Landroid/provider/SearchRecentSuggestions;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/provider/SearchRecentSuggestions;->saveRecentQueryBlocking(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100()Ljava/util/concurrent/Semaphore;
    .registers 1

    #@0
    .prologue
    .line 55
    sget-object v0, Landroid/provider/SearchRecentSuggestions;->sWritesInProgress:Ljava/util/concurrent/Semaphore;

    #@2
    return-object v0
.end method

.method private saveRecentQueryBlocking(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "queryString"
    .parameter "line2"

    #@0
    .prologue
    .line 189
    iget-object v5, p0, Landroid/provider/SearchRecentSuggestions;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    .line 190
    .local v0, cr:Landroid/content/ContentResolver;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v2

    #@a
    .line 194
    .local v2, now:J
    :try_start_a
    new-instance v4, Landroid/content/ContentValues;

    #@c
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@f
    .line 195
    .local v4, values:Landroid/content/ContentValues;
    const-string v5, "display1"

    #@11
    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 196
    iget-boolean v5, p0, Landroid/provider/SearchRecentSuggestions;->mTwoLineDisplay:Z

    #@16
    if-eqz v5, :cond_1d

    #@18
    .line 197
    const-string v5, "display2"

    #@1a
    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 199
    :cond_1d
    const-string/jumbo v5, "query"

    #@20
    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 200
    const-string v5, "date"

    #@25
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2c
    .line 201
    iget-object v5, p0, Landroid/provider/SearchRecentSuggestions;->mSuggestionsUri:Landroid/net/Uri;

    #@2e
    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_31
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_31} :catch_37

    #@31
    .line 207
    .end local v4           #values:Landroid/content/ContentValues;
    :goto_31
    const/16 v5, 0xfa

    #@33
    invoke-virtual {p0, v0, v5}, Landroid/provider/SearchRecentSuggestions;->truncateHistory(Landroid/content/ContentResolver;I)V

    #@36
    .line 208
    return-void

    #@37
    .line 202
    :catch_37
    move-exception v1

    #@38
    .line 203
    .local v1, e:Ljava/lang/RuntimeException;
    const-string v5, "SearchSuggestions"

    #@3a
    const-string/jumbo v6, "saveRecentQuery"

    #@3d
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    goto :goto_31
.end method


# virtual methods
.method public clearHistory()V
    .registers 3

    #@0
    .prologue
    .line 219
    iget-object v1, p0, Landroid/provider/SearchRecentSuggestions;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    .line 220
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v1, 0x0

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/provider/SearchRecentSuggestions;->truncateHistory(Landroid/content/ContentResolver;I)V

    #@a
    .line 221
    return-void
.end method

.method public saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "queryString"
    .parameter "line2"

    #@0
    .prologue
    .line 162
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 176
    :goto_6
    return-void

    #@7
    .line 165
    :cond_7
    iget-boolean v0, p0, Landroid/provider/SearchRecentSuggestions;->mTwoLineDisplay:Z

    #@9
    if-nez v0, :cond_17

    #@b
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_17

    #@11
    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@16
    throw v0

    #@17
    .line 169
    :cond_17
    new-instance v0, Landroid/provider/SearchRecentSuggestions$1;

    #@19
    const-string/jumbo v1, "saveRecentQuery"

    #@1c
    invoke-direct {v0, p0, v1, p1, p2}, Landroid/provider/SearchRecentSuggestions$1;-><init>(Landroid/provider/SearchRecentSuggestions;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    invoke-virtual {v0}, Landroid/provider/SearchRecentSuggestions$1;->start()V

    #@22
    goto :goto_6
.end method

.method protected truncateHistory(Landroid/content/ContentResolver;I)V
    .registers 7
    .parameter "cr"
    .parameter "maxEntries"

    #@0
    .prologue
    .line 230
    if-gez p2, :cond_8

    #@2
    .line 231
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@7
    throw v2

    #@8
    .line 236
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 237
    .local v1, selection:Ljava/lang/String;
    if-lez p2, :cond_28

    #@b
    .line 238
    :try_start_b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "_id IN (SELECT _id FROM suggestions ORDER BY date DESC LIMIT -1 OFFSET "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, ")"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 243
    :cond_28
    iget-object v2, p0, Landroid/provider/SearchRecentSuggestions;->mSuggestionsUri:Landroid/net/Uri;

    #@2a
    const/4 v3, 0x0

    #@2b
    invoke-virtual {p1, v2, v1, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2e
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_2e} :catch_2f

    #@2e
    .line 247
    :goto_2e
    return-void

    #@2f
    .line 244
    :catch_2f
    move-exception v0

    #@30
    .line 245
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "SearchSuggestions"

    #@32
    const-string/jumbo v3, "truncateHistory"

    #@35
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    goto :goto_2e
.end method

.method waitForSave()V
    .registers 2

    #@0
    .prologue
    .line 184
    :cond_0
    sget-object v0, Landroid/provider/SearchRecentSuggestions;->sWritesInProgress:Ljava/util/concurrent/Semaphore;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    #@5
    .line 185
    sget-object v0, Landroid/provider/SearchRecentSuggestions;->sWritesInProgress:Ljava/util/concurrent/Semaphore;

    #@7
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    #@a
    move-result v0

    #@b
    if-gtz v0, :cond_0

    #@d
    .line 186
    return-void
.end method
