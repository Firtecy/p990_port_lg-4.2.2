.class Landroid/provider/ContactsContract$RawContacts$EntityIteratorImpl;
.super Landroid/content/CursorEntityIterator;
.source "ContactsContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract$RawContacts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EntityIteratorImpl"
.end annotation


# static fields
.field private static final DATA_KEYS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 2855
    const/16 v0, 0x13

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    const/4 v1, 0x0

    #@5
    const-string v2, "data1"

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    const-string v2, "data2"

    #@c
    aput-object v2, v0, v1

    #@e
    const/4 v1, 0x2

    #@f
    const-string v2, "data3"

    #@11
    aput-object v2, v0, v1

    #@13
    const/4 v1, 0x3

    #@14
    const-string v2, "data4"

    #@16
    aput-object v2, v0, v1

    #@18
    const/4 v1, 0x4

    #@19
    const-string v2, "data5"

    #@1b
    aput-object v2, v0, v1

    #@1d
    const/4 v1, 0x5

    #@1e
    const-string v2, "data6"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string v2, "data7"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    const-string v2, "data8"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/16 v1, 0x8

    #@2e
    const-string v2, "data9"

    #@30
    aput-object v2, v0, v1

    #@32
    const/16 v1, 0x9

    #@34
    const-string v2, "data10"

    #@36
    aput-object v2, v0, v1

    #@38
    const/16 v1, 0xa

    #@3a
    const-string v2, "data11"

    #@3c
    aput-object v2, v0, v1

    #@3e
    const/16 v1, 0xb

    #@40
    const-string v2, "data12"

    #@42
    aput-object v2, v0, v1

    #@44
    const/16 v1, 0xc

    #@46
    const-string v2, "data13"

    #@48
    aput-object v2, v0, v1

    #@4a
    const/16 v1, 0xd

    #@4c
    const-string v2, "data14"

    #@4e
    aput-object v2, v0, v1

    #@50
    const/16 v1, 0xe

    #@52
    const-string v2, "data15"

    #@54
    aput-object v2, v0, v1

    #@56
    const/16 v1, 0xf

    #@58
    const-string v2, "data_sync1"

    #@5a
    aput-object v2, v0, v1

    #@5c
    const/16 v1, 0x10

    #@5e
    const-string v2, "data_sync2"

    #@60
    aput-object v2, v0, v1

    #@62
    const/16 v1, 0x11

    #@64
    const-string v2, "data_sync3"

    #@66
    aput-object v2, v0, v1

    #@68
    const/16 v1, 0x12

    #@6a
    const-string v2, "data_sync4"

    #@6c
    aput-object v2, v0, v1

    #@6e
    sput-object v0, Landroid/provider/ContactsContract$RawContacts$EntityIteratorImpl;->DATA_KEYS:[Ljava/lang/String;

    #@70
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .registers 2
    .parameter "cursor"

    #@0
    .prologue
    .line 2877
    invoke-direct {p0, p1}, Landroid/content/CursorEntityIterator;-><init>(Landroid/database/Cursor;)V

    #@3
    .line 2878
    return-void
.end method


# virtual methods
.method public getEntityAndIncrementCursor(Landroid/database/Cursor;)Landroid/content/Entity;
    .registers 15
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2883
    const-string v10, "_id"

    #@2
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5
    move-result v2

    #@6
    .line 2884
    .local v2, columnRawContactId:I
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    #@9
    move-result-wide v8

    #@a
    .line 2887
    .local v8, rawContactId:J
    new-instance v4, Landroid/content/ContentValues;

    #@c
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@f
    .line 2888
    .local v4, cv:Landroid/content/ContentValues;
    const-string v10, "account_name"

    #@11
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@14
    .line 2889
    const-string v10, "account_type"

    #@16
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@19
    .line 2890
    const-string v10, "data_set"

    #@1b
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@1e
    .line 2891
    const-string v10, "_id"

    #@20
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@23
    .line 2892
    const-string v10, "dirty"

    #@25
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@28
    .line 2893
    const-string/jumbo v10, "version"

    #@2b
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@2e
    .line 2894
    const-string/jumbo v10, "sourceid"

    #@31
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@34
    .line 2895
    const-string/jumbo v10, "sync1"

    #@37
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@3a
    .line 2896
    const-string/jumbo v10, "sync2"

    #@3d
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@40
    .line 2897
    const-string/jumbo v10, "sync3"

    #@43
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@46
    .line 2898
    const-string/jumbo v10, "sync4"

    #@49
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@4c
    .line 2899
    const-string v10, "deleted"

    #@4e
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@51
    .line 2900
    const-string v10, "contact_id"

    #@53
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@56
    .line 2901
    const-string/jumbo v10, "starred"

    #@59
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@5c
    .line 2902
    const-string/jumbo v10, "name_verified"

    #@5f
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorIntToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@62
    .line 2903
    new-instance v3, Landroid/content/Entity;

    #@64
    invoke-direct {v3, v4}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    #@67
    .line 2907
    .local v3, contact:Landroid/content/Entity;
    :cond_67
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    #@6a
    move-result-wide v10

    #@6b
    cmp-long v10, v8, v10

    #@6d
    if-eqz v10, :cond_70

    #@6f
    .line 2945
    :goto_6f
    return-object v3

    #@70
    .line 2911
    :cond_70
    new-instance v4, Landroid/content/ContentValues;

    #@72
    .end local v4           #cv:Landroid/content/ContentValues;
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@75
    .line 2912
    .restart local v4       #cv:Landroid/content/ContentValues;
    const-string v10, "_id"

    #@77
    const-string v11, "data_id"

    #@79
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7c
    move-result v11

    #@7d
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    #@80
    move-result-wide v11

    #@81
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@84
    move-result-object v11

    #@85
    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@88
    .line 2913
    const-string/jumbo v10, "res_package"

    #@8b
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@8e
    .line 2915
    const-string/jumbo v10, "mimetype"

    #@91
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@94
    .line 2916
    const-string/jumbo v10, "is_primary"

    #@97
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@9a
    .line 2917
    const-string/jumbo v10, "is_super_primary"

    #@9d
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@a0
    .line 2919
    const-string v10, "data_version"

    #@a2
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorLongToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@a5
    .line 2920
    const-string v10, "group_sourceid"

    #@a7
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@aa
    .line 2922
    const-string v10, "data_version"

    #@ac
    invoke-static {p1, v4, v10}, Landroid/database/DatabaseUtils;->cursorStringToContentValuesIfPresent(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;)V

    #@af
    .line 2924
    sget-object v0, Landroid/provider/ContactsContract$RawContacts$EntityIteratorImpl;->DATA_KEYS:[Ljava/lang/String;

    #@b1
    .local v0, arr$:[Ljava/lang/String;
    array-length v7, v0

    #@b2
    .local v7, len$:I
    const/4 v5, 0x0

    #@b3
    .local v5, i$:I
    :goto_b3
    if-ge v5, v7, :cond_dc

    #@b5
    aget-object v6, v0, v5

    #@b7
    .line 2925
    .local v6, key:Ljava/lang/String;
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@ba
    move-result v1

    #@bb
    .line 2926
    .local v1, columnIndex:I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getType(I)I

    #@be
    move-result v10

    #@bf
    packed-switch v10, :pswitch_data_e8

    #@c2
    .line 2939
    new-instance v10, Ljava/lang/IllegalStateException;

    #@c4
    const-string v11, "Invalid or unhandled data type"

    #@c6
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c9
    throw v10

    #@ca
    .line 2933
    :pswitch_ca
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cd
    move-result-object v10

    #@ce
    invoke-virtual {v4, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d1
    .line 2924
    :goto_d1
    :pswitch_d1
    add-int/lit8 v5, v5, 0x1

    #@d3
    goto :goto_b3

    #@d4
    .line 2936
    :pswitch_d4
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    #@d7
    move-result-object v10

    #@d8
    invoke-virtual {v4, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@db
    goto :goto_d1

    #@dc
    .line 2942
    .end local v1           #columnIndex:I
    .end local v6           #key:Ljava/lang/String;
    :cond_dc
    sget-object v10, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@de
    invoke-virtual {v3, v10, v4}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    #@e1
    .line 2943
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    #@e4
    move-result v10

    #@e5
    if-nez v10, :cond_67

    #@e7
    goto :goto_6f

    #@e8
    .line 2926
    :pswitch_data_e8
    .packed-switch 0x0
        :pswitch_d1
        :pswitch_ca
        :pswitch_ca
        :pswitch_ca
        :pswitch_d4
    .end packed-switch
.end method
