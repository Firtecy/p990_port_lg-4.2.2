.class public final Landroid/provider/Settings$Bookmarks;
.super Ljava/lang/Object;
.source "Settings.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Bookmarks"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final FOLDER:Ljava/lang/String; = "folder"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final INTENT:Ljava/lang/String; = "intent"

.field public static final ORDERING:Ljava/lang/String; = "ordering"

.field public static final SHORTCUT:Ljava/lang/String; = "shortcut"

.field private static final TAG:Ljava/lang/String; = "Bookmarks"

.field public static final TITLE:Ljava/lang/String; = "title"

.field private static final sIntentProjection:[Ljava/lang/String; = null

.field private static final sShortcutProjection:[Ljava/lang/String; = null

.field private static final sShortcutSelection:Ljava/lang/String; = "shortcut=?"


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 6810
    const-string v0, "content://settings/bookmarks"

    #@4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/Settings$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    #@a
    .line 6863
    new-array v0, v3, [Ljava/lang/String;

    #@c
    const-string v1, "intent"

    #@e
    aput-object v1, v0, v2

    #@10
    sput-object v0, Landroid/provider/Settings$Bookmarks;->sIntentProjection:[Ljava/lang/String;

    #@12
    .line 6864
    const/4 v0, 0x2

    #@13
    new-array v0, v0, [Ljava/lang/String;

    #@15
    const-string v1, "_id"

    #@17
    aput-object v1, v0, v2

    #@19
    const-string/jumbo v1, "shortcut"

    #@1c
    aput-object v1, v0, v3

    #@1e
    sput-object v0, Landroid/provider/Settings$Bookmarks;->sShortcutProjection:[Ljava/lang/String;

    #@20
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 6803
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static add(Landroid/content/ContentResolver;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;CI)Landroid/net/Uri;
    .registers 12
    .parameter "cr"
    .parameter "intent"
    .parameter "title"
    .parameter "folder"
    .parameter "shortcut"
    .parameter "ordering"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 6927
    if-eqz p4, :cond_14

    #@3
    .line 6928
    sget-object v1, Landroid/provider/Settings$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    #@5
    const-string/jumbo v2, "shortcut=?"

    #@8
    const/4 v3, 0x1

    #@9
    new-array v3, v3, [Ljava/lang/String;

    #@b
    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    aput-object v4, v3, v5

    #@11
    invoke-virtual {p0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@14
    .line 6932
    :cond_14
    new-instance v0, Landroid/content/ContentValues;

    #@16
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@19
    .line 6933
    .local v0, values:Landroid/content/ContentValues;
    if-eqz p2, :cond_21

    #@1b
    const-string/jumbo v1, "title"

    #@1e
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 6934
    :cond_21
    if-eqz p3, :cond_28

    #@23
    const-string v1, "folder"

    #@25
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 6935
    :cond_28
    const-string v1, "intent"

    #@2a
    invoke-virtual {p1, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 6936
    if-eqz p4, :cond_3d

    #@33
    const-string/jumbo v1, "shortcut"

    #@36
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3d
    .line 6937
    :cond_3d
    const-string/jumbo v1, "ordering"

    #@40
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@47
    .line 6938
    sget-object v1, Landroid/provider/Settings$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    #@49
    invoke-virtual {p0, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@4c
    move-result-object v1

    #@4d
    return-object v1
.end method

.method public static getIntentForShortcut(Landroid/content/ContentResolver;C)Landroid/content/Intent;
    .registers 12
    .parameter "cr"
    .parameter "shortcut"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 6879
    const/4 v8, 0x0

    #@2
    .line 6881
    .local v8, intent:Landroid/content/Intent;
    sget-object v1, Landroid/provider/Settings$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    #@4
    sget-object v2, Landroid/provider/Settings$Bookmarks;->sIntentProjection:[Ljava/lang/String;

    #@6
    const-string/jumbo v3, "shortcut=?"

    #@9
    const/4 v0, 0x1

    #@a
    new-array v4, v0, [Ljava/lang/String;

    #@c
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    aput-object v0, v4, v5

    #@12
    const-string/jumbo v5, "ordering"

    #@15
    move-object v0, p0

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@19
    move-result-object v6

    #@1a
    .line 6886
    .local v6, c:Landroid/database/Cursor;
    :goto_1a
    if-nez v8, :cond_42

    #@1c
    :try_start_1c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_3b

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_42

    #@22
    .line 6888
    :try_start_22
    const-string v0, "intent"

    #@24
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@27
    move-result v0

    #@28
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v9

    #@2c
    .line 6889
    .local v9, intentURI:Ljava/lang/String;
    const/4 v0, 0x0

    #@2d
    invoke-static {v9, v0}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_30
    .catchall {:try_start_22 .. :try_end_30} :catchall_3b
    .catch Ljava/net/URISyntaxException; {:try_start_22 .. :try_end_30} :catch_48
    .catch Ljava/lang/IllegalArgumentException; {:try_start_22 .. :try_end_30} :catch_32

    #@30
    move-result-object v8

    #@31
    goto :goto_1a

    #@32
    .line 6892
    .end local v9           #intentURI:Ljava/lang/String;
    :catch_32
    move-exception v7

    #@33
    .line 6894
    .local v7, e:Ljava/lang/IllegalArgumentException;
    :try_start_33
    const-string v0, "Bookmarks"

    #@35
    const-string v1, "Intent column not found"

    #@37
    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3a
    .catchall {:try_start_33 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_1a

    #@3b
    .line 6898
    .end local v7           #e:Ljava/lang/IllegalArgumentException;
    :catchall_3b
    move-exception v0

    #@3c
    if-eqz v6, :cond_41

    #@3e
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@41
    :cond_41
    throw v0

    #@42
    :cond_42
    if-eqz v6, :cond_47

    #@44
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@47
    .line 6901
    :cond_47
    return-object v8

    #@48
    .line 6890
    :catch_48
    move-exception v0

    #@49
    goto :goto_1a
.end method

.method public static getLabelForFolder(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "r"
    .parameter "folder"

    #@0
    .prologue
    .line 6953
    return-object p1
.end method

.method public static getTitle(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .registers 13
    .parameter "context"
    .parameter "cursor"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, -0x1

    #@2
    .line 6968
    const-string/jumbo v8, "title"

    #@5
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@8
    move-result v7

    #@9
    .line 6969
    .local v7, titleColumn:I
    const-string v8, "intent"

    #@b
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@e
    move-result v3

    #@f
    .line 6970
    .local v3, intentColumn:I
    if-eq v7, v9, :cond_13

    #@11
    if-ne v3, v9, :cond_1b

    #@13
    .line 6971
    :cond_13
    new-instance v8, Ljava/lang/IllegalArgumentException;

    #@15
    const-string v9, "The cursor must contain the TITLE and INTENT columns."

    #@17
    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v8

    #@1b
    .line 6975
    :cond_1b
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    .line 6976
    .local v6, title:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@22
    move-result v8

    #@23
    if-nez v8, :cond_26

    #@25
    .line 6994
    .end local v6           #title:Ljava/lang/String;
    :goto_25
    return-object v6

    #@26
    .line 6980
    .restart local v6       #title:Ljava/lang/String;
    :cond_26
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    .line 6981
    .local v4, intentUri:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2d
    move-result v8

    #@2e
    if-eqz v8, :cond_33

    #@30
    .line 6982
    const-string v6, ""

    #@32
    goto :goto_25

    #@33
    .line 6987
    :cond_33
    const/4 v8, 0x0

    #@34
    :try_start_34
    invoke-static {v4, v8}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_37
    .catch Ljava/net/URISyntaxException; {:try_start_34 .. :try_end_37} :catch_48

    #@37
    move-result-object v2

    #@38
    .line 6992
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3b
    move-result-object v5

    #@3c
    .line 6993
    .local v5, packageManager:Landroid/content/pm/PackageManager;
    invoke-virtual {v5, v2, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@3f
    move-result-object v1

    #@40
    .line 6994
    .local v1, info:Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_4c

    #@42
    invoke-virtual {v1, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@45
    move-result-object v8

    #@46
    :goto_46
    move-object v6, v8

    #@47
    goto :goto_25

    #@48
    .line 6988
    .end local v1           #info:Landroid/content/pm/ResolveInfo;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v5           #packageManager:Landroid/content/pm/PackageManager;
    :catch_48
    move-exception v0

    #@49
    .line 6989
    .local v0, e:Ljava/net/URISyntaxException;
    const-string v6, ""

    #@4b
    goto :goto_25

    #@4c
    .line 6994
    .end local v0           #e:Ljava/net/URISyntaxException;
    .restart local v1       #info:Landroid/content/pm/ResolveInfo;
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v5       #packageManager:Landroid/content/pm/PackageManager;
    :cond_4c
    const-string v8, ""

    #@4e
    goto :goto_46
.end method
