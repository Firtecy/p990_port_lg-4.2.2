.class public Landroid/provider/CallLog$Calls;
.super Ljava/lang/Object;
.source "CallLog.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/CallLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Calls"
.end annotation


# static fields
.field public static final ALLOW_VOICEMAILS_PARAM_KEY:Ljava/lang/String; = "allow_voicemails"

.field public static final CACHED_FORMATTED_NUMBER:Ljava/lang/String; = "formatted_number"

.field public static final CACHED_LOOKUP_URI:Ljava/lang/String; = "lookup_uri"

.field public static final CACHED_MATCHED_NUMBER:Ljava/lang/String; = "matched_number"

.field public static final CACHED_NAME:Ljava/lang/String; = "name"

.field public static final CACHED_NORMALIZED_NUMBER:Ljava/lang/String; = "normalized_number"

.field public static final CACHED_NUMBER_LABEL:Ljava/lang/String; = "numberlabel"

.field public static final CACHED_NUMBER_TYPE:Ljava/lang/String; = "numbertype"

.field public static final CACHED_PHOTO_ID:Ljava/lang/String; = "photo_id"

.field public static final CONTENT_FILTER_URI:Landroid/net/Uri; = null

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/calls"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/calls"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri; = null

.field public static final COUNTRY_ISO:Ljava/lang/String; = "countryiso"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "date DESC"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final GEOCODED_LOCATION:Ljava/lang/String; = "geocoded_location"

.field public static final INCOMING_TYPE:I = 0x1

.field public static final IS_READ:Ljava/lang/String; = "is_read"

.field public static final LIMIT_PARAM_KEY:Ljava/lang/String; = "limit"

.field public static final MISSED_TYPE:I = 0x3

.field public static final NEW:Ljava/lang/String; = "new"

.field public static final NUMBER:Ljava/lang/String; = "number"

.field public static final OFFSET_PARAM_KEY:Ljava/lang/String; = "offset"

.field public static final OUTGOING_TYPE:I = 0x2

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final VOICEMAIL_TYPE:I = 0x4

.field public static final VOICEMAIL_URI:Ljava/lang/String; = "voicemail_uri"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 52
    const-string v0, "content://call_log/calls"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 58
    const-string v0, "content://call_log/calls/filter"

    #@a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/provider/CallLog$Calls;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@10
    .line 94
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@12
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@15
    move-result-object v0

    #@16
    const-string v1, "allow_voicemails"

    #@18
    const-string/jumbo v2, "true"

    #@1b
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@22
    move-result-object v0

    #@23
    sput-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    #@25
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;IIJI)Landroid/net/Uri;
    .registers 23
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"

    #@0
    .prologue
    .line 280
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 284
    .local v1, resolver:Landroid/content/ContentResolver;
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@6
    move/from16 v0, p3

    #@8
    if-ne v0, v2, :cond_e9

    #@a
    .line 285
    const-string p2, "-2"

    #@c
    .line 286
    if-eqz p0, :cond_12

    #@e
    const-string v2, ""

    #@10
    iput-object v2, p0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@12
    .line 296
    :cond_12
    :goto_12
    new-instance v12, Landroid/content/ContentValues;

    #@14
    const/4 v2, 0x5

    #@15
    invoke-direct {v12, v2}, Landroid/content/ContentValues;-><init>(I)V

    #@18
    .line 298
    .local v12, values:Landroid/content/ContentValues;
    const-string/jumbo v2, "number"

    #@1b
    move-object/from16 v0, p2

    #@1d
    invoke-virtual {v12, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 299
    const-string/jumbo v2, "type"

    #@23
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2a
    .line 300
    const-string v2, "date"

    #@2c
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@33
    .line 301
    const-string v2, "duration"

    #@35
    move/from16 v0, p7

    #@37
    int-to-long v3, v0

    #@38
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@3f
    .line 302
    const-string/jumbo v2, "new"

    #@42
    const/4 v3, 0x1

    #@43
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4a
    .line 303
    const/4 v2, 0x3

    #@4b
    move/from16 v0, p4

    #@4d
    if-ne v0, v2, :cond_5a

    #@4f
    .line 304
    const-string/jumbo v2, "is_read"

    #@52
    const/4 v3, 0x0

    #@53
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5a
    .line 306
    :cond_5a
    if-eqz p0, :cond_78

    #@5c
    .line 307
    const-string/jumbo v2, "name"

    #@5f
    iget-object v3, p0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@61
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 308
    const-string/jumbo v2, "numbertype"

    #@67
    iget v3, p0, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@69
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@70
    .line 309
    const-string/jumbo v2, "numberlabel"

    #@73
    iget-object v3, p0, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@75
    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@78
    .line 312
    :cond_78
    if-eqz p0, :cond_df

    #@7a
    iget-wide v2, p0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@7c
    const-wide/16 v4, 0x0

    #@7e
    cmp-long v2, v2, v4

    #@80
    if-lez v2, :cond_df

    #@82
    .line 321
    iget-object v2, p0, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@84
    if-eqz v2, :cond_10f

    #@86
    .line 322
    iget-object v9, p0, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@88
    .line 323
    .local v9, normalizedPhoneNumber:Ljava/lang/String;
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@8a
    const/4 v3, 0x1

    #@8b
    new-array v3, v3, [Ljava/lang/String;

    #@8d
    const/4 v4, 0x0

    #@8e
    const-string v5, "_id"

    #@90
    aput-object v5, v3, v4

    #@92
    const-string v4, "contact_id =? AND data4 =?"

    #@94
    const/4 v5, 0x2

    #@95
    new-array v5, v5, [Ljava/lang/String;

    #@97
    const/4 v6, 0x0

    #@98
    iget-wide v13, p0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@9a
    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@9d
    move-result-object v13

    #@9e
    aput-object v13, v5, v6

    #@a0
    const/4 v6, 0x1

    #@a1
    aput-object v9, v5, v6

    #@a3
    const/4 v6, 0x0

    #@a4
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@a7
    move-result-object v7

    #@a8
    .line 339
    .end local v9           #normalizedPhoneNumber:Ljava/lang/String;
    .local v7, cursor:Landroid/database/Cursor;
    :goto_a8
    if-eqz v7, :cond_df

    #@aa
    .line 341
    :try_start_aa
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@ad
    move-result v2

    #@ae
    if-lez v2, :cond_dc

    #@b0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@b3
    move-result v2

    #@b4
    if-eqz v2, :cond_dc

    #@b6
    .line 342
    sget-object v2, Landroid/provider/ContactsContract$DataUsageFeedback;->FEEDBACK_URI:Landroid/net/Uri;

    #@b8
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@bb
    move-result-object v2

    #@bc
    const/4 v3, 0x0

    #@bd
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c0
    move-result-object v3

    #@c1
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@c4
    move-result-object v2

    #@c5
    const-string/jumbo v3, "type"

    #@c8
    const-string v4, "call"

    #@ca
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@cd
    move-result-object v2

    #@ce
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@d1
    move-result-object v8

    #@d2
    .line 347
    .local v8, feedbackUri:Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    #@d4
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@d7
    const/4 v3, 0x0

    #@d8
    const/4 v4, 0x0

    #@d9
    invoke-virtual {v1, v8, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_dc
    .catchall {:try_start_aa .. :try_end_dc} :catchall_13f

    #@dc
    .line 350
    .end local v8           #feedbackUri:Landroid/net/Uri;
    :cond_dc
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@df
    .line 355
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_df
    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@e1
    invoke-virtual {v1, v2, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@e4
    move-result-object v11

    #@e5
    .line 357
    .local v11, result:Landroid/net/Uri;
    invoke-static/range {p1 .. p1}, Landroid/provider/CallLog$Calls;->removeExpiredEntries(Landroid/content/Context;)V

    #@e8
    .line 359
    return-object v11

    #@e9
    .line 287
    .end local v11           #result:Landroid/net/Uri;
    .end local v12           #values:Landroid/content/ContentValues;
    :cond_e9
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_PAYPHONE:I

    #@eb
    move/from16 v0, p3

    #@ed
    if-ne v0, v2, :cond_f9

    #@ef
    .line 288
    const-string p2, "-3"

    #@f1
    .line 289
    if-eqz p0, :cond_12

    #@f3
    const-string v2, ""

    #@f5
    iput-object v2, p0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@f7
    goto/16 :goto_12

    #@f9
    .line 290
    :cond_f9
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@fc
    move-result v2

    #@fd
    if-nez v2, :cond_105

    #@ff
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@101
    move/from16 v0, p3

    #@103
    if-ne v0, v2, :cond_12

    #@105
    .line 292
    :cond_105
    const-string p2, "-1"

    #@107
    .line 293
    if-eqz p0, :cond_12

    #@109
    const-string v2, ""

    #@10b
    iput-object v2, p0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@10d
    goto/16 :goto_12

    #@10f
    .line 329
    .restart local v12       #values:Landroid/content/ContentValues;
    :cond_10f
    iget-object v2, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@111
    if-eqz v2, :cond_13c

    #@113
    iget-object v10, p0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@115
    .line 330
    .local v10, phoneNumber:Ljava/lang/String;
    :goto_115
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Callable;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@117
    invoke-static {v10}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@11a
    move-result-object v3

    #@11b
    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@11e
    move-result-object v2

    #@11f
    const/4 v3, 0x1

    #@120
    new-array v3, v3, [Ljava/lang/String;

    #@122
    const/4 v4, 0x0

    #@123
    const-string v5, "_id"

    #@125
    aput-object v5, v3, v4

    #@127
    const-string v4, "contact_id =?"

    #@129
    const/4 v5, 0x1

    #@12a
    new-array v5, v5, [Ljava/lang/String;

    #@12c
    const/4 v6, 0x0

    #@12d
    iget-wide v13, p0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@12f
    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@132
    move-result-object v13

    #@133
    aput-object v13, v5, v6

    #@135
    const/4 v6, 0x0

    #@136
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@139
    move-result-object v7

    #@13a
    .restart local v7       #cursor:Landroid/database/Cursor;
    goto/16 :goto_a8

    #@13c
    .end local v7           #cursor:Landroid/database/Cursor;
    .end local v10           #phoneNumber:Ljava/lang/String;
    :cond_13c
    move-object/from16 v10, p2

    #@13e
    .line 329
    goto :goto_115

    #@13f
    .line 350
    .restart local v7       #cursor:Landroid/database/Cursor;
    :catchall_13f
    move-exception v2

    #@140
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@143
    throw v2
.end method

.method public static getLastOutgoingCall(Landroid/content/Context;)Ljava/lang/String;
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 369
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 370
    .local v0, resolver:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@5
    .line 372
    .local v6, c:Landroid/database/Cursor;
    :try_start_5
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x1

    #@8
    new-array v2, v2, [Ljava/lang/String;

    #@a
    const/4 v3, 0x0

    #@b
    const-string/jumbo v4, "number"

    #@e
    aput-object v4, v2, v3

    #@10
    const-string/jumbo v3, "type = 2"

    #@13
    const/4 v4, 0x0

    #@14
    const-string v5, "date DESC LIMIT 1"

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@19
    move-result-object v6

    #@1a
    .line 378
    if-eqz v6, :cond_22

    #@1c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_2a

    #@22
    .line 379
    :cond_22
    const-string v1, ""
    :try_end_24
    .catchall {:try_start_5 .. :try_end_24} :catchall_35

    #@24
    .line 383
    if-eqz v6, :cond_29

    #@26
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@29
    :cond_29
    :goto_29
    return-object v1

    #@2a
    .line 381
    :cond_2a
    const/4 v1, 0x0

    #@2b
    :try_start_2b
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_35

    #@2e
    move-result-object v1

    #@2f
    .line 383
    if-eqz v6, :cond_29

    #@31
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@34
    goto :goto_29

    #@35
    :catchall_35
    move-exception v1

    #@36
    if-eqz v6, :cond_3b

    #@38
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3b
    :cond_3b
    throw v1
.end method

.method private static removeExpiredEntries(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 388
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 389
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@6
    const-string v2, "_id IN (SELECT _id FROM calls ORDER BY date DESC LIMIT -1 OFFSET 500)"

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@c
    .line 392
    return-void
.end method
