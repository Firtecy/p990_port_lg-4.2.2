.class public final Landroid/provider/BrowserContract$Settings;
.super Ljava/lang/Object;
.source "BrowserContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/BrowserContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Settings"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final KEY_SYNC_ENABLED:Ljava/lang/String; = "sync_enabled"

.field public static final VALUE:Ljava/lang/String; = "value"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 699
    sget-object v0, Landroid/provider/BrowserContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@2
    const-string/jumbo v1, "settings"

    #@5
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Landroid/provider/BrowserContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    #@b
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 694
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static isSyncEnabled(Landroid/content/Context;)Z
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 720
    const/4 v6, 0x0

    #@3
    .line 722
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    sget-object v1, Landroid/provider/BrowserContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    #@9
    const/4 v2, 0x1

    #@a
    new-array v2, v2, [Ljava/lang/String;

    #@c
    const/4 v3, 0x0

    #@d
    const-string/jumbo v4, "value"

    #@10
    aput-object v4, v2, v3

    #@12
    const-string/jumbo v3, "key=?"

    #@15
    const/4 v4, 0x1

    #@16
    new-array v4, v4, [Ljava/lang/String;

    #@18
    const/4 v5, 0x0

    #@19
    const-string/jumbo v9, "sync_enabled"

    #@1c
    aput-object v9, v4, v5

    #@1e
    const/4 v5, 0x0

    #@1f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@22
    move-result-object v6

    #@23
    .line 724
    if-eqz v6, :cond_2b

    #@25
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_42

    #@28
    move-result v0

    #@29
    if-nez v0, :cond_32

    #@2b
    .line 729
    :cond_2b
    if-eqz v6, :cond_30

    #@2d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@30
    :cond_30
    move v0, v8

    #@31
    :cond_31
    :goto_31
    return v0

    #@32
    .line 727
    :cond_32
    const/4 v0, 0x0

    #@33
    :try_start_33
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_42

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_40

    #@39
    move v0, v7

    #@3a
    .line 729
    :goto_3a
    if-eqz v6, :cond_31

    #@3c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3f
    goto :goto_31

    #@40
    :cond_40
    move v0, v8

    #@41
    .line 727
    goto :goto_3a

    #@42
    .line 729
    :catchall_42
    move-exception v0

    #@43
    if-eqz v6, :cond_48

    #@45
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@48
    :cond_48
    throw v0
.end method

.method public static setSyncEnabled(Landroid/content/Context;Z)V
    .registers 5
    .parameter "context"
    .parameter "enabled"

    #@0
    .prologue
    .line 737
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 738
    .local v0, values:Landroid/content/ContentValues;
    const-string/jumbo v1, "key"

    #@8
    const-string/jumbo v2, "sync_enabled"

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 739
    const-string/jumbo v2, "value"

    #@11
    if-eqz p1, :cond_25

    #@13
    const/4 v1, 0x1

    #@14
    :goto_14
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1b
    .line 740
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1e
    move-result-object v1

    #@1f
    sget-object v2, Landroid/provider/BrowserContract$Settings;->CONTENT_URI:Landroid/net/Uri;

    #@21
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@24
    .line 741
    return-void

    #@25
    .line 739
    :cond_25
    const/4 v1, 0x0

    #@26
    goto :goto_14
.end method
