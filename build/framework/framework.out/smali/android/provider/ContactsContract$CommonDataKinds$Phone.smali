.class public final Landroid/provider/ContactsContract$CommonDataKinds$Phone;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/ContactsContract$DataColumnsWithJoins;
.implements Landroid/provider/ContactsContract$CommonDataKinds$CommonColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract$CommonDataKinds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Phone"
.end annotation


# static fields
.field public static final CONTENT_FILTER_URI:Landroid/net/Uri; = null

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/phone_v2"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/phone_v2"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final NORMALIZED_NUMBER:Ljava/lang/String; = "data4"

.field public static final NUMBER:Ljava/lang/String; = "data1"

.field public static final SEARCH_DISPLAY_NAME_KEY:Ljava/lang/String; = "search_display_name"

.field public static final SEARCH_PHONE_NUMBER_KEY:Ljava/lang/String; = "search_phone_number"

.field public static final TYPE_ASSISTANT:I = 0x13

.field public static final TYPE_CALLBACK:I = 0x8

.field public static final TYPE_CAR:I = 0x9

.field public static final TYPE_COMPANY_MAIN:I = 0xa

.field public static final TYPE_FAX_HOME:I = 0x5

.field public static final TYPE_FAX_WORK:I = 0x4

.field public static final TYPE_HOME:I = 0x1

.field public static final TYPE_ISDN:I = 0xb

.field public static final TYPE_MAIN:I = 0xc

.field public static final TYPE_MMS:I = 0x14

.field public static final TYPE_MOBILE:I = 0x2

.field public static final TYPE_OTHER:I = 0x7

.field public static final TYPE_OTHER_FAX:I = 0xd

.field public static final TYPE_PAGER:I = 0x6

.field public static final TYPE_RADIO:I = 0xe

.field public static final TYPE_TELEX:I = 0xf

.field public static final TYPE_TTY_TDD:I = 0x10

.field public static final TYPE_WORK:I = 0x3

.field public static final TYPE_WORK_MOBILE:I = 0x11

.field public static final TYPE_WORK_PAGER:I = 0x12


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 5436
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string/jumbo v1, "phones"

    #@5
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@b
    .line 5445
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@d
    const-string v1, "filter"

    #@f
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v0

    #@13
    sput-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@15
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 5420
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final getDisplayLabel(Landroid/content/Context;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 4
    .parameter "context"
    .parameter "type"
    .parameter "label"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 5515
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1, p2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static final getDisplayLabel(Landroid/content/Context;ILjava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "context"
    .parameter "type"
    .parameter "label"
    .parameter "labelArray"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 5505
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1, p2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static final getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "res"
    .parameter "type"
    .parameter "label"

    #@0
    .prologue
    .line 5555
    if-eqz p1, :cond_6

    #@2
    const/16 v1, 0x13

    #@4
    if-ne p1, v1, :cond_d

    #@6
    :cond_6
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_d

    #@c
    .line 5559
    .end local p2
    :goto_c
    return-object p2

    #@d
    .line 5558
    .restart local p2
    :cond_d
    invoke-static {p1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabelResource(I)I

    #@10
    move-result v0

    #@11
    .line 5559
    .local v0, labelRes:I
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@14
    move-result-object p2

    #@15
    goto :goto_c
.end method

.method public static final getTypeLabelResource(I)I
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 5523
    packed-switch p0, :pswitch_data_58

    #@3
    .line 5544
    const v0, 0x10402b6

    #@6
    :goto_6
    return v0

    #@7
    .line 5524
    :pswitch_7
    const v0, 0x10402b7

    #@a
    goto :goto_6

    #@b
    .line 5525
    :pswitch_b
    const v0, 0x10402b8

    #@e
    goto :goto_6

    #@f
    .line 5526
    :pswitch_f
    const v0, 0x10402b9

    #@12
    goto :goto_6

    #@13
    .line 5527
    :pswitch_13
    const v0, 0x10402ba

    #@16
    goto :goto_6

    #@17
    .line 5528
    :pswitch_17
    const v0, 0x10402bb

    #@1a
    goto :goto_6

    #@1b
    .line 5529
    :pswitch_1b
    const v0, 0x10402bc

    #@1e
    goto :goto_6

    #@1f
    .line 5530
    :pswitch_1f
    const v0, 0x10402bd

    #@22
    goto :goto_6

    #@23
    .line 5531
    :pswitch_23
    const v0, 0x10402be

    #@26
    goto :goto_6

    #@27
    .line 5532
    :pswitch_27
    const v0, 0x10402bf

    #@2a
    goto :goto_6

    #@2b
    .line 5533
    :pswitch_2b
    const v0, 0x10402c0

    #@2e
    goto :goto_6

    #@2f
    .line 5534
    :pswitch_2f
    const v0, 0x10402c1

    #@32
    goto :goto_6

    #@33
    .line 5535
    :pswitch_33
    const v0, 0x10402c2

    #@36
    goto :goto_6

    #@37
    .line 5536
    :pswitch_37
    const v0, 0x10402c3

    #@3a
    goto :goto_6

    #@3b
    .line 5537
    :pswitch_3b
    const v0, 0x10402c4

    #@3e
    goto :goto_6

    #@3f
    .line 5538
    :pswitch_3f
    const v0, 0x10402c5

    #@42
    goto :goto_6

    #@43
    .line 5539
    :pswitch_43
    const v0, 0x10402c6

    #@46
    goto :goto_6

    #@47
    .line 5540
    :pswitch_47
    const v0, 0x10402c7

    #@4a
    goto :goto_6

    #@4b
    .line 5541
    :pswitch_4b
    const v0, 0x10402c8

    #@4e
    goto :goto_6

    #@4f
    .line 5542
    :pswitch_4f
    const v0, 0x10402c9

    #@52
    goto :goto_6

    #@53
    .line 5543
    :pswitch_53
    const v0, 0x10402ca

    #@56
    goto :goto_6

    #@57
    .line 5523
    nop

    #@58
    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_13
        :pswitch_17
        :pswitch_1b
        :pswitch_1f
        :pswitch_23
        :pswitch_27
        :pswitch_2b
        :pswitch_2f
        :pswitch_33
        :pswitch_37
        :pswitch_3b
        :pswitch_3f
        :pswitch_43
        :pswitch_47
        :pswitch_4b
        :pswitch_4f
        :pswitch_53
    .end packed-switch
.end method
