.class public final Landroid/provider/ContactsContract;
.super Ljava/lang/Object;
.source "ContactsContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/ContactsContract$1;,
        Landroid/provider/ContactsContract$Intents;,
        Landroid/provider/ContactsContract$DisplayPhoto;,
        Landroid/provider/ContactsContract$QuickContact;,
        Landroid/provider/ContactsContract$DataUsageFeedback;,
        Landroid/provider/ContactsContract$ProviderStatus;,
        Landroid/provider/ContactsContract$Settings;,
        Landroid/provider/ContactsContract$SettingsColumns;,
        Landroid/provider/ContactsContract$AggregationExceptions;,
        Landroid/provider/ContactsContract$Groups;,
        Landroid/provider/ContactsContract$GroupsColumns;,
        Landroid/provider/ContactsContract$CommonDataKinds;,
        Landroid/provider/ContactsContract$SearchSnippetColumns;,
        Landroid/provider/ContactsContract$Presence;,
        Landroid/provider/ContactsContract$StatusUpdates;,
        Landroid/provider/ContactsContract$PresenceColumns;,
        Landroid/provider/ContactsContract$PhoneLookup;,
        Landroid/provider/ContactsContract$PhoneLookupColumns;,
        Landroid/provider/ContactsContract$RawContactsEntity;,
        Landroid/provider/ContactsContract$Data;,
        Landroid/provider/ContactsContract$DataColumnsWithJoins;,
        Landroid/provider/ContactsContract$DataColumns;,
        Landroid/provider/ContactsContract$PhotoFilesColumns;,
        Landroid/provider/ContactsContract$PhotoFiles;,
        Landroid/provider/ContactsContract$StreamItemPhotosColumns;,
        Landroid/provider/ContactsContract$StreamItemPhotos;,
        Landroid/provider/ContactsContract$StreamItemsColumns;,
        Landroid/provider/ContactsContract$StreamItems;,
        Landroid/provider/ContactsContract$StatusColumns;,
        Landroid/provider/ContactsContract$RawContacts;,
        Landroid/provider/ContactsContract$RawContactsColumns;,
        Landroid/provider/ContactsContract$Profile;,
        Landroid/provider/ContactsContract$Contacts;,
        Landroid/provider/ContactsContract$ContactCounts;,
        Landroid/provider/ContactsContract$ContactNameColumns;,
        Landroid/provider/ContactsContract$DisplayNameSources;,
        Landroid/provider/ContactsContract$PhoneticNameStyle;,
        Landroid/provider/ContactsContract$FullNameStyle;,
        Landroid/provider/ContactsContract$ContactStatusColumns;,
        Landroid/provider/ContactsContract$ContactsColumns;,
        Landroid/provider/ContactsContract$ContactOptionsColumns;,
        Landroid/provider/ContactsContract$SyncColumns;,
        Landroid/provider/ContactsContract$BaseSyncColumns;,
        Landroid/provider/ContactsContract$ProfileSyncState;,
        Landroid/provider/ContactsContract$SyncState;,
        Landroid/provider/ContactsContract$SyncStateColumns;,
        Landroid/provider/ContactsContract$Directory;,
        Landroid/provider/ContactsContract$Preferences;,
        Landroid/provider/ContactsContract$Authorization;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.android.contacts"

.field public static final AUTHORITY_URI:Landroid/net/Uri; = null

.field public static final CALLER_IS_SYNCADAPTER:Ljava/lang/String; = "caller_is_syncadapter"

.field public static final DEFERRED_SNIPPETING:Ljava/lang/String; = "deferred_snippeting"

.field public static final DEFERRED_SNIPPETING_QUERY:Ljava/lang/String; = "deferred_snippeting_query"

.field public static final DIRECTORY_PARAM_KEY:Ljava/lang/String; = "directory"

.field public static final LIMIT_PARAM_KEY:Ljava/lang/String; = "limit"

.field public static final PRIMARY_ACCOUNT_NAME:Ljava/lang/String; = "name_for_primary_account"

.field public static final PRIMARY_ACCOUNT_TYPE:Ljava/lang/String; = "type_for_primary_account"

.field public static final REMOVE_DUPLICATE_ENTRIES:Ljava/lang/String; = "remove_duplicate_entries"

.field private static SPLIT_PATTERN:Ljava/util/regex/Pattern; = null

.field public static final STREQUENT_PHONE_ONLY:Ljava/lang/String; = "strequent_phone_only"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 118
    const-string v0, "content://com.android.contacts"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@8
    .line 8491
    const-string v0, "([\\w-\\.]+)@((?:[\\w]+\\.)+)([a-zA-Z]{2,4})|[\\w]+"

    #@a
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/provider/ContactsContract;->SPLIT_PATTERN:Ljava/util/regex/Pattern;

    #@10
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 114
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 7867
    return-void
.end method

.method public static isProfileId(J)Z
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 2140
    const-wide v0, 0x7fffffff80000000L

    #@5
    cmp-long v0, p0, v0

    #@7
    if-ltz v0, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public static snippetize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;CCLjava/lang/String;I)Ljava/lang/String;
    .registers 36
    .parameter "content"
    .parameter "displayName"
    .parameter "query"
    .parameter "snippetStartMatch"
    .parameter "snippetEndMatch"
    .parameter "snippetEllipsis"
    .parameter "snippetMaxTokens"

    #@0
    .prologue
    .line 8399
    if-eqz p2, :cond_27

    #@2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@5
    move-result-object v13

    #@6
    .line 8400
    .local v13, lowerQuery:Ljava/lang/String;
    :goto_6
    invoke-static/range {p0 .. p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9
    move-result v24

    #@a
    if-nez v24, :cond_24

    #@c
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@f
    move-result v24

    #@10
    if-nez v24, :cond_24

    #@12
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@15
    move-result v24

    #@16
    if-nez v24, :cond_24

    #@18
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@1b
    move-result-object v24

    #@1c
    move-object/from16 v0, v24

    #@1e
    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@21
    move-result v24

    #@22
    if-nez v24, :cond_29

    #@24
    .line 8402
    :cond_24
    const/16 v24, 0x0

    #@26
    .line 8482
    :goto_26
    return-object v24

    #@27
    .line 8399
    .end local v13           #lowerQuery:Ljava/lang/String;
    :cond_27
    const/4 v13, 0x0

    #@28
    goto :goto_6

    #@29
    .line 8407
    .restart local v13       #lowerQuery:Ljava/lang/String;
    :cond_29
    if-eqz p1, :cond_61

    #@2b
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@2e
    move-result-object v12

    #@2f
    .line 8408
    .local v12, lowerDisplayName:Ljava/lang/String;
    :goto_2f
    new-instance v19, Ljava/util/ArrayList;

    #@31
    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    #@34
    .line 8409
    .local v19, nameTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/ArrayList;

    #@36
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@39
    .line 8410
    .local v18, nameTokenOffsets:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@3c
    move-result-object v24

    #@3d
    move-object/from16 v0, v24

    #@3f
    move-object/from16 v1, v19

    #@41
    move-object/from16 v2, v18

    #@43
    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract;->split(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    #@46
    .line 8411
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v8

    #@4a
    .local v8, i$:Ljava/util/Iterator;
    :cond_4a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v24

    #@4e
    if-eqz v24, :cond_64

    #@50
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v17

    #@54
    check-cast v17, Ljava/lang/String;

    #@56
    .line 8412
    .local v17, nameToken:Ljava/lang/String;
    move-object/from16 v0, v17

    #@58
    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5b
    move-result v24

    #@5c
    if-eqz v24, :cond_4a

    #@5e
    .line 8413
    const/16 v24, 0x0

    #@60
    goto :goto_26

    #@61
    .line 8407
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v12           #lowerDisplayName:Ljava/lang/String;
    .end local v17           #nameToken:Ljava/lang/String;
    .end local v18           #nameTokenOffsets:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v19           #nameTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_61
    const-string v12, ""

    #@63
    goto :goto_2f

    #@64
    .line 8417
    .restart local v8       #i$:Ljava/util/Iterator;
    .restart local v12       #lowerDisplayName:Ljava/lang/String;
    .restart local v18       #nameTokenOffsets:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v19       #nameTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_64
    const-string v24, "\n"

    #@66
    move-object/from16 v0, p0

    #@68
    move-object/from16 v1, v24

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    .line 8420
    .local v5, contentLines:[Ljava/lang/String;
    move-object v3, v5

    #@6f
    .local v3, arr$:[Ljava/lang/String;
    array-length v10, v3

    #@70
    .local v10, len$:I
    const/4 v8, 0x0

    #@71
    .local v8, i$:I
    :goto_71
    if-ge v8, v10, :cond_18e

    #@73
    aget-object v4, v3, v8

    #@75
    .line 8421
    .local v4, contentLine:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@78
    move-result-object v24

    #@79
    move-object/from16 v0, v24

    #@7b
    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@7e
    move-result v24

    #@7f
    if-eqz v24, :cond_18a

    #@81
    .line 8424
    new-instance v11, Ljava/util/ArrayList;

    #@83
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    #@86
    .line 8425
    .local v11, lineTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v23, Ljava/util/ArrayList;

    #@88
    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    #@8b
    .line 8426
    .local v23, tokenOffsets:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v0, v23

    #@8d
    invoke-static {v4, v11, v0}, Landroid/provider/ContactsContract;->split(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    #@90
    .line 8430
    new-instance v16, Ljava/util/ArrayList;

    #@92
    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    #@95
    .line 8432
    .local v16, markedTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, -0x1

    #@96
    .line 8433
    .local v6, firstToken:I
    const/4 v9, -0x1

    #@97
    .line 8434
    .local v9, lastToken:I
    const/4 v7, 0x0

    #@98
    .local v7, i:I
    :goto_98
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@9b
    move-result v24

    #@9c
    move/from16 v0, v24

    #@9e
    if-ge v7, v0, :cond_115

    #@a0
    .line 8435
    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a3
    move-result-object v22

    #@a4
    check-cast v22, Ljava/lang/String;

    #@a6
    .line 8436
    .local v22, token:Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@a9
    move-result-object v14

    #@aa
    .line 8437
    .local v14, lowerToken:Ljava/lang/String;
    invoke-virtual {v14, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@ad
    move-result v24

    #@ae
    if-eqz v24, :cond_10d

    #@b0
    .line 8440
    new-instance v24, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    move-object/from16 v0, v24

    #@b7
    move/from16 v1, p3

    #@b9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v24

    #@bd
    move-object/from16 v0, v24

    #@bf
    move-object/from16 v1, v22

    #@c1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v24

    #@c5
    move-object/from16 v0, v24

    #@c7
    move/from16 v1, p4

    #@c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v24

    #@cd
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v24

    #@d1
    move-object/from16 v0, v16

    #@d3
    move-object/from16 v1, v24

    #@d5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d8
    .line 8444
    const/16 v24, -0x1

    #@da
    move/from16 v0, v24

    #@dc
    if-ne v6, v0, :cond_10a

    #@de
    .line 8445
    const/16 v24, 0x0

    #@e0
    invoke-static/range {p6 .. p6}, Ljava/lang/Math;->abs(I)I

    #@e3
    move-result v25

    #@e4
    move/from16 v0, v25

    #@e6
    int-to-double v0, v0

    #@e7
    move-wide/from16 v25, v0

    #@e9
    const-wide/high16 v27, 0x4000

    #@eb
    div-double v25, v25, v27

    #@ed
    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->floor(D)D

    #@f0
    move-result-wide v25

    #@f1
    move-wide/from16 v0, v25

    #@f3
    double-to-int v0, v0

    #@f4
    move/from16 v25, v0

    #@f6
    sub-int v25, v7, v25

    #@f8
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->max(II)I

    #@fb
    move-result v6

    #@fc
    .line 8449
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@ff
    move-result v24

    #@100
    invoke-static/range {p6 .. p6}, Ljava/lang/Math;->abs(I)I

    #@103
    move-result v25

    #@104
    add-int v25, v25, v6

    #@106
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    #@109
    move-result v9

    #@10a
    .line 8434
    :cond_10a
    :goto_10a
    add-int/lit8 v7, v7, 0x1

    #@10c
    goto :goto_98

    #@10d
    .line 8454
    :cond_10d
    move-object/from16 v0, v16

    #@10f
    move-object/from16 v1, v22

    #@111
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@114
    goto :goto_10a

    #@115
    .line 8459
    .end local v14           #lowerToken:Ljava/lang/String;
    .end local v22           #token:Ljava/lang/String;
    :cond_115
    const/16 v24, -0x1

    #@117
    move/from16 v0, v24

    #@119
    if-le v6, v0, :cond_18a

    #@11b
    .line 8460
    new-instance v21, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    .line 8461
    .local v21, sb:Ljava/lang/StringBuilder;
    if-lez v6, :cond_129

    #@122
    .line 8462
    move-object/from16 v0, v21

    #@124
    move-object/from16 v1, p5

    #@126
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    .line 8464
    :cond_129
    move v7, v6

    #@12a
    :goto_12a
    if-ge v7, v9, :cond_175

    #@12c
    .line 8465
    move-object/from16 v0, v16

    #@12e
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@131
    move-result-object v15

    #@132
    check-cast v15, Ljava/lang/String;

    #@134
    .line 8466
    .local v15, markedToken:Ljava/lang/String;
    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@137
    move-result-object v20

    #@138
    check-cast v20, Ljava/lang/String;

    #@13a
    .line 8467
    .local v20, originalToken:Ljava/lang/String;
    move-object/from16 v0, v21

    #@13c
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    .line 8468
    add-int/lit8 v24, v9, -0x1

    #@141
    move/from16 v0, v24

    #@143
    if-ge v7, v0, :cond_172

    #@145
    .line 8470
    move-object/from16 v0, v23

    #@147
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14a
    move-result-object v24

    #@14b
    check-cast v24, Ljava/lang/Integer;

    #@14d
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    #@150
    move-result v24

    #@151
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    #@154
    move-result v25

    #@155
    add-int v25, v25, v24

    #@157
    add-int/lit8 v24, v7, 0x1

    #@159
    invoke-interface/range {v23 .. v24}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@15c
    move-result-object v24

    #@15d
    check-cast v24, Ljava/lang/Integer;

    #@15f
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    #@162
    move-result v24

    #@163
    move/from16 v0, v25

    #@165
    move/from16 v1, v24

    #@167
    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16a
    move-result-object v24

    #@16b
    move-object/from16 v0, v21

    #@16d
    move-object/from16 v1, v24

    #@16f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    .line 8464
    :cond_172
    add-int/lit8 v7, v7, 0x1

    #@174
    goto :goto_12a

    #@175
    .line 8475
    .end local v15           #markedToken:Ljava/lang/String;
    .end local v20           #originalToken:Ljava/lang/String;
    :cond_175
    invoke-interface {v11}, Ljava/util/List;->size()I

    #@178
    move-result v24

    #@179
    move/from16 v0, v24

    #@17b
    if-ge v9, v0, :cond_184

    #@17d
    .line 8476
    move-object/from16 v0, v21

    #@17f
    move-object/from16 v1, p5

    #@181
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    .line 8478
    :cond_184
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@187
    move-result-object v24

    #@188
    goto/16 :goto_26

    #@18a
    .line 8420
    .end local v6           #firstToken:I
    .end local v7           #i:I
    .end local v9           #lastToken:I
    .end local v11           #lineTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v16           #markedTokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v21           #sb:Ljava/lang/StringBuilder;
    .end local v23           #tokenOffsets:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_18a
    add-int/lit8 v8, v8, 0x1

    #@18c
    goto/16 :goto_71

    #@18e
    .line 8482
    .end local v4           #contentLine:Ljava/lang/String;
    :cond_18e
    const/16 v24, 0x0

    #@190
    goto/16 :goto_26
.end method

.method private static split(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .registers 5
    .parameter "content"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 8505
    .local p1, tokens:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p2, offsets:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    sget-object v1, Landroid/provider/ContactsContract;->SPLIT_PATTERN:Ljava/util/regex/Pattern;

    #@2
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@5
    move-result-object v0

    #@6
    .line 8506
    .local v0, matcher:Ljava/util/regex/Matcher;
    :goto_6
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1f

    #@c
    .line 8507
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@13
    .line 8508
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    #@16
    move-result v1

    #@17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v1

    #@1b
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1e
    goto :goto_6

    #@1f
    .line 8510
    :cond_1f
    return-void
.end method
