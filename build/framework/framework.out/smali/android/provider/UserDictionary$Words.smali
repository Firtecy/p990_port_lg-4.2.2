.class public Landroid/provider/UserDictionary$Words;
.super Ljava/lang/Object;
.source "UserDictionary.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/UserDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Words"
.end annotation


# static fields
.field public static final APP_ID:Ljava/lang/String; = "appid"

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.google.userword"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.google.userword"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "frequency DESC"

.field public static final FREQUENCY:Ljava/lang/String; = "frequency"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final LOCALE_TYPE_ALL:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCALE_TYPE_CURRENT:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SHORTCUT:Ljava/lang/String; = "shortcut"

.field public static final WORD:Ljava/lang/String; = "word"

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const-string v0, "content://user_dictionary/words"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/UserDictionary$Words;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addWord(Landroid/content/Context;Ljava/lang/String;II)V
    .registers 6
    .parameter "context"
    .parameter "word"
    .parameter "frequency"
    .parameter "localeType"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 132
    if-eqz p3, :cond_6

    #@3
    if-eq p3, v1, :cond_6

    #@5
    .line 145
    :goto_5
    return-void

    #@6
    .line 138
    :cond_6
    if-ne p3, v1, :cond_11

    #@8
    .line 139
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@b
    move-result-object v0

    #@c
    .line 144
    .local v0, locale:Ljava/util/Locale;
    :goto_c
    const/4 v1, 0x0

    #@d
    invoke-static {p0, p1, p2, v1, v0}, Landroid/provider/UserDictionary$Words;->addWord(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V

    #@10
    goto :goto_5

    #@11
    .line 141
    .end local v0           #locale:Ljava/util/Locale;
    :cond_11
    const/4 v0, 0x0

    #@12
    .restart local v0       #locale:Ljava/util/Locale;
    goto :goto_c
.end method

.method public static addWord(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/Locale;)V
    .registers 11
    .parameter "context"
    .parameter "word"
    .parameter "frequency"
    .parameter "shortcut"
    .parameter "locale"

    #@0
    .prologue
    .line 160
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    .line 162
    .local v1, resolver:Landroid/content/ContentResolver;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v4

    #@8
    if-eqz v4, :cond_b

    #@a
    .line 181
    :goto_a
    return-void

    #@b
    .line 166
    :cond_b
    if-gez p2, :cond_e

    #@d
    const/4 p2, 0x0

    #@e
    .line 167
    :cond_e
    const/16 v4, 0xff

    #@10
    if-le p2, v4, :cond_14

    #@12
    const/16 p2, 0xff

    #@14
    .line 169
    :cond_14
    const/4 v0, 0x5

    #@15
    .line 170
    .local v0, COLUMN_COUNT:I
    new-instance v3, Landroid/content/ContentValues;

    #@17
    const/4 v4, 0x5

    #@18
    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    #@1b
    .line 172
    .local v3, values:Landroid/content/ContentValues;
    const-string/jumbo v4, "word"

    #@1e
    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 173
    const-string v4, "frequency"

    #@23
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2a
    .line 174
    const-string/jumbo v5, "locale"

    #@2d
    if-nez p4, :cond_4a

    #@2f
    const/4 v4, 0x0

    #@30
    :goto_30
    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 175
    const-string v4, "appid"

    #@35
    const/4 v5, 0x0

    #@36
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3d
    .line 176
    const-string/jumbo v4, "shortcut"

    #@40
    invoke-virtual {v3, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 178
    sget-object v4, Landroid/provider/UserDictionary$Words;->CONTENT_URI:Landroid/net/Uri;

    #@45
    invoke-virtual {v1, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@48
    move-result-object v2

    #@49
    .line 181
    .local v2, result:Landroid/net/Uri;
    goto :goto_a

    #@4a
    .line 174
    .end local v2           #result:Landroid/net/Uri;
    :cond_4a
    invoke-virtual {p4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    goto :goto_30
.end method
