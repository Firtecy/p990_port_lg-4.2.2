.class public Landroid/provider/Browser;
.super Ljava/lang/Object;
.source "Browser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/Browser$SearchColumns;,
        Landroid/provider/Browser$BookmarkColumns;
    }
.end annotation


# static fields
.field public static final BOOKMARKS_URI:Landroid/net/Uri; = null

.field public static final EXTRA_APPLICATION_ID:Ljava/lang/String; = "com.android.browser.application_id"

.field public static final EXTRA_CREATE_NEW_TAB:Ljava/lang/String; = "create_new_tab"

.field public static final EXTRA_HEADERS:Ljava/lang/String; = "com.android.browser.headers"

.field public static final EXTRA_SHARE_FAVICON:Ljava/lang/String; = "share_favicon"

.field public static final EXTRA_SHARE_SCREENSHOT:Ljava/lang/String; = "share_screenshot"

.field public static final HISTORY_PROJECTION:[Ljava/lang/String; = null

.field public static final HISTORY_PROJECTION_BOOKMARK_INDEX:I = 0x4

.field public static final HISTORY_PROJECTION_DATE_INDEX:I = 0x3

.field public static final HISTORY_PROJECTION_FAVICON_INDEX:I = 0x6

.field public static final HISTORY_PROJECTION_ID_INDEX:I = 0x0

.field public static final HISTORY_PROJECTION_THUMBNAIL_INDEX:I = 0x7

.field public static final HISTORY_PROJECTION_TITLE_INDEX:I = 0x5

.field public static final HISTORY_PROJECTION_TOUCH_ICON_INDEX:I = 0x8

.field public static final HISTORY_PROJECTION_URL_INDEX:I = 0x1

.field public static final HISTORY_PROJECTION_VISITS_INDEX:I = 0x2

.field public static final INITIAL_ZOOM_LEVEL:Ljava/lang/String; = "browser.initialZoomLevel"

.field private static final LOGTAG:Ljava/lang/String; = "browser"

.field private static final MAX_HISTORY_COUNT:I = 0xfa

.field public static final SEARCHES_PROJECTION:[Ljava/lang/String; = null

.field public static final SEARCHES_PROJECTION_DATE_INDEX:I = 0x2

.field public static final SEARCHES_PROJECTION_SEARCH_INDEX:I = 0x1

.field public static final SEARCHES_URI:Landroid/net/Uri; = null

.field public static final TRUNCATE_HISTORY_PROJECTION:[Ljava/lang/String; = null

.field public static final TRUNCATE_HISTORY_PROJECTION_ID_INDEX:I = 0x0

.field public static final TRUNCATE_N_OLDEST:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 44
    const-string v0, "content://browser/bookmarks"

    #@6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    #@c
    .line 78
    const/16 v0, 0xa

    #@e
    new-array v0, v0, [Ljava/lang/String;

    #@10
    const-string v1, "_id"

    #@12
    aput-object v1, v0, v3

    #@14
    const-string/jumbo v1, "url"

    #@17
    aput-object v1, v0, v4

    #@19
    const-string/jumbo v1, "visits"

    #@1c
    aput-object v1, v0, v5

    #@1e
    const-string v1, "date"

    #@20
    aput-object v1, v0, v6

    #@22
    const/4 v1, 0x4

    #@23
    const-string v2, "bookmark"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x5

    #@28
    const-string/jumbo v2, "title"

    #@2b
    aput-object v2, v0, v1

    #@2d
    const/4 v1, 0x6

    #@2e
    const-string v2, "favicon"

    #@30
    aput-object v2, v0, v1

    #@32
    const/4 v1, 0x7

    #@33
    const-string/jumbo v2, "thumbnail"

    #@36
    aput-object v2, v0, v1

    #@38
    const/16 v1, 0x8

    #@3a
    const-string/jumbo v2, "touch_icon"

    #@3d
    aput-object v2, v0, v1

    #@3f
    const/16 v1, 0x9

    #@41
    const-string/jumbo v2, "user_entered"

    #@44
    aput-object v2, v0, v1

    #@46
    sput-object v0, Landroid/provider/Browser;->HISTORY_PROJECTION:[Ljava/lang/String;

    #@48
    .line 109
    new-array v0, v5, [Ljava/lang/String;

    #@4a
    const-string v1, "_id"

    #@4c
    aput-object v1, v0, v3

    #@4e
    const-string v1, "date"

    #@50
    aput-object v1, v0, v4

    #@52
    sput-object v0, Landroid/provider/Browser;->TRUNCATE_HISTORY_PROJECTION:[Ljava/lang/String;

    #@54
    .line 125
    const-string v0, "content://browser/searches"

    #@56
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@59
    move-result-object v0

    #@5a
    sput-object v0, Landroid/provider/Browser;->SEARCHES_URI:Landroid/net/Uri;

    #@5c
    .line 131
    new-array v0, v6, [Ljava/lang/String;

    #@5e
    const-string v1, "_id"

    #@60
    aput-object v1, v0, v3

    #@62
    const-string/jumbo v1, "search"

    #@65
    aput-object v1, v0, v4

    #@67
    const-string v1, "date"

    #@69
    aput-object v1, v0, v5

    #@6b
    sput-object v0, Landroid/provider/Browser;->SEARCHES_PROJECTION:[Ljava/lang/String;

    #@6d
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 677
    return-void
.end method

.method private static final addOrUrlEquals(Ljava/lang/StringBuilder;)V
    .registers 2
    .parameter "sb"

    #@0
    .prologue
    .line 295
    const-string v0, " OR url = "

    #@2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 296
    return-void
.end method

.method public static final addSearchUrl(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .registers 6
    .parameter "cr"
    .parameter "search"

    #@0
    .prologue
    .line 573
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 574
    .local v0, values:Landroid/content/ContentValues;
    const-string/jumbo v1, "search"

    #@8
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 575
    const-string v1, "date"

    #@d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@10
    move-result-wide v2

    #@11
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@18
    .line 576
    sget-object v1, Landroid/provider/BrowserContract$Searches;->CONTENT_URI:Landroid/net/Uri;

    #@1a
    invoke-virtual {p0, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1d
    .line 577
    return-void
.end method

.method public static final canClearHistory(Landroid/content/ContentResolver;)Z
    .registers 12
    .parameter "cr"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 466
    const/4 v6, 0x0

    #@3
    .line 467
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@4
    .line 469
    .local v8, ret:Z
    :try_start_4
    sget-object v1, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@6
    const/4 v0, 0x2

    #@7
    new-array v2, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    const-string v3, "_id"

    #@c
    aput-object v3, v2, v0

    #@e
    const/4 v0, 0x1

    #@f
    const-string/jumbo v3, "visits"

    #@12
    aput-object v3, v2, v0

    #@14
    const/4 v3, 0x0

    #@15
    const/4 v4, 0x0

    #@16
    const/4 v5, 0x0

    #@17
    move-object v0, p0

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v6

    #@1c
    .line 472
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_1f
    .catchall {:try_start_4 .. :try_end_1f} :catchall_39
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_1f} :catch_2b

    #@1f
    move-result v0

    #@20
    if-lez v0, :cond_29

    #@22
    move v8, v9

    #@23
    .line 476
    :goto_23
    if-eqz v6, :cond_28

    #@25
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@28
    .line 478
    :cond_28
    :goto_28
    return v8

    #@29
    :cond_29
    move v8, v10

    #@2a
    .line 472
    goto :goto_23

    #@2b
    .line 473
    :catch_2b
    move-exception v7

    #@2c
    .line 474
    .local v7, e:Ljava/lang/IllegalStateException;
    :try_start_2c
    const-string v0, "browser"

    #@2e
    const-string v1, "canClearHistory"

    #@30
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_33
    .catchall {:try_start_2c .. :try_end_33} :catchall_39

    #@33
    .line 476
    if-eqz v6, :cond_28

    #@35
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@38
    goto :goto_28

    #@39
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catchall_39
    move-exception v0

    #@3a
    if-eqz v6, :cond_3f

    #@3c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3f
    :cond_3f
    throw v0
.end method

.method public static final clearHistory(Landroid/content/ContentResolver;)V
    .registers 2
    .parameter "cr"

    #@0
    .prologue
    .line 488
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/provider/Browser;->deleteHistoryWhere(Landroid/content/ContentResolver;Ljava/lang/String;)V

    #@4
    .line 489
    return-void
.end method

.method public static final clearSearches(Landroid/content/ContentResolver;)V
    .registers 5
    .parameter "cr"

    #@0
    .prologue
    .line 588
    :try_start_0
    sget-object v1, Landroid/provider/BrowserContract$Searches;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    invoke-virtual {p0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 592
    :goto_7
    return-void

    #@8
    .line 589
    :catch_8
    move-exception v0

    #@9
    .line 590
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "browser"

    #@b
    const-string v2, "clearSearches"

    #@d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public static final deleteFromHistory(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .registers 6
    .parameter "cr"
    .parameter "url"

    #@0
    .prologue
    .line 561
    sget-object v0, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@2
    const-string/jumbo v1, "url=?"

    #@5
    const/4 v2, 0x1

    #@6
    new-array v2, v2, [Ljava/lang/String;

    #@8
    const/4 v3, 0x0

    #@9
    aput-object p1, v2, v3

    #@b
    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@e
    .line 562
    return-void
.end method

.method public static final deleteHistoryTimeFrame(Landroid/content/ContentResolver;JJ)V
    .registers 10
    .parameter "cr"
    .parameter "begin"
    .parameter "end"

    #@0
    .prologue
    const-wide/16 v3, -0x1

    #@2
    .line 537
    const-string v0, "date"

    #@4
    .line 538
    .local v0, date:Ljava/lang/String;
    cmp-long v2, v3, p1

    #@6
    if-nez v2, :cond_2f

    #@8
    .line 539
    cmp-long v2, v3, p3

    #@a
    if-nez v2, :cond_10

    #@c
    .line 540
    invoke-static {p0}, Landroid/provider/Browser;->clearHistory(Landroid/content/ContentResolver;)V

    #@f
    .line 551
    :goto_f
    return-void

    #@10
    .line 543
    :cond_10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    const-string v3, " < "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    .line 550
    .local v1, whereClause:Ljava/lang/String;
    :goto_2b
    invoke-static {p0, v1}, Landroid/provider/Browser;->deleteHistoryWhere(Landroid/content/ContentResolver;Ljava/lang/String;)V

    #@2e
    goto :goto_f

    #@2f
    .line 544
    .end local v1           #whereClause:Ljava/lang/String;
    :cond_2f
    cmp-long v2, v3, p3

    #@31
    if-nez v2, :cond_4f

    #@33
    .line 545
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    const-string v3, " >= "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    .restart local v1       #whereClause:Ljava/lang/String;
    goto :goto_2b

    #@4f
    .line 547
    .end local v1           #whereClause:Ljava/lang/String;
    :cond_4f
    new-instance v2, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v3, " >= "

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    const-string v3, " AND "

    #@68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    const-string v3, " < "

    #@72
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    .restart local v1       #whereClause:Ljava/lang/String;
    goto :goto_2b
.end method

.method private static final deleteHistoryWhere(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .registers 11
    .parameter "cr"
    .parameter "whereClause"

    #@0
    .prologue
    .line 503
    const/4 v6, 0x0

    #@1
    .line 505
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_1
    sget-object v1, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const/4 v0, 0x1

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    const-string/jumbo v3, "url"

    #@a
    aput-object v3, v2, v0

    #@c
    const/4 v4, 0x0

    #@d
    const/4 v5, 0x0

    #@e
    move-object v0, p0

    #@f
    move-object v3, p1

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v6

    #@14
    .line 507
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_32

    #@1a
    .line 508
    invoke-static {}, Landroid/webkit/WebIconDatabase;->getInstance()Landroid/webkit/WebIconDatabase;

    #@1d
    move-result-object v8

    #@1e
    .line 512
    .local v8, iconDb:Landroid/webkit/WebIconDatabase;
    :cond_1e
    const/4 v0, 0x0

    #@1f
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v8, v0}, Landroid/webkit/WebIconDatabase;->releaseIconForPageUrl(Ljava/lang/String;)V

    #@26
    .line 513
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_1e

    #@2c
    .line 515
    sget-object v0, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@2e
    const/4 v1, 0x0

    #@2f
    invoke-virtual {p0, v0, p1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_32
    .catchall {:try_start_1 .. :try_end_32} :catchall_46
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_32} :catch_38

    #@32
    .line 521
    .end local v8           #iconDb:Landroid/webkit/WebIconDatabase;
    :cond_32
    if-eqz v6, :cond_37

    #@34
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@37
    .line 523
    :cond_37
    :goto_37
    return-void

    #@38
    .line 517
    :catch_38
    move-exception v7

    #@39
    .line 518
    .local v7, e:Ljava/lang/IllegalStateException;
    :try_start_39
    const-string v0, "browser"

    #@3b
    const-string v1, "deleteHistoryWhere"

    #@3d
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_40
    .catchall {:try_start_39 .. :try_end_40} :catchall_46

    #@40
    .line 521
    if-eqz v6, :cond_37

    #@42
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@45
    goto :goto_37

    #@46
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catchall_46
    move-exception v0

    #@47
    if-eqz v6, :cond_4c

    #@49
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@4c
    :cond_4c
    throw v0
.end method

.method public static final getAllBookmarks(Landroid/content/ContentResolver;)Landroid/database/Cursor;
    .registers 7
    .parameter "cr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 274
    sget-object v1, Landroid/provider/BrowserContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const/4 v0, 0x1

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    const-string/jumbo v3, "url"

    #@a
    aput-object v3, v2, v0

    #@c
    const-string v3, "folder = 0"

    #@e
    move-object v0, p0

    #@f
    move-object v5, v4

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method public static final getAllVisitedUrls(Landroid/content/ContentResolver;)Landroid/database/Cursor;
    .registers 7
    .parameter "cr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 289
    sget-object v1, Landroid/provider/BrowserContract$Combined;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const/4 v0, 0x1

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    const-string/jumbo v4, "url"

    #@a
    aput-object v4, v2, v0

    #@c
    const-string v5, "created ASC"

    #@e
    move-object v0, p0

    #@f
    move-object v4, v3

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method public static final getVisitedHistory(Landroid/content/ContentResolver;)[Ljava/lang/String;
    .registers 11
    .parameter "cr"

    #@0
    .prologue
    .line 398
    const/4 v6, 0x0

    #@1
    .line 399
    .local v6, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    #@2
    .line 401
    .local v9, str:[Ljava/lang/String;
    const/4 v0, 0x1

    #@3
    :try_start_3
    new-array v2, v0, [Ljava/lang/String;

    #@5
    const/4 v0, 0x0

    #@6
    const-string/jumbo v1, "url"

    #@9
    aput-object v1, v2, v0

    #@b
    .line 404
    .local v2, projection:[Ljava/lang/String;
    sget-object v1, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@d
    const-string/jumbo v3, "visits > 0"

    #@10
    const/4 v4, 0x0

    #@11
    const/4 v5, 0x0

    #@12
    move-object v0, p0

    #@13
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v6

    #@17
    .line 405
    if-nez v6, :cond_22

    #@19
    const/4 v0, 0x0

    #@1a
    new-array v0, v0, [Ljava/lang/String;
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_51
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_1c} :catch_40

    #@1c
    .line 416
    if-eqz v6, :cond_21

    #@1e
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@21
    .line 418
    .end local v2           #projection:[Ljava/lang/String;
    :cond_21
    :goto_21
    return-object v0

    #@22
    .line 406
    .restart local v2       #projection:[Ljava/lang/String;
    :cond_22
    :try_start_22
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@25
    move-result v0

    #@26
    new-array v9, v0, [Ljava/lang/String;

    #@28
    .line 407
    const/4 v8, 0x0

    #@29
    .line 408
    .local v8, i:I
    :goto_29
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_39

    #@2f
    .line 409
    const/4 v0, 0x0

    #@30
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    aput-object v0, v9, v8
    :try_end_36
    .catchall {:try_start_22 .. :try_end_36} :catchall_51
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_36} :catch_40

    #@36
    .line 410
    add-int/lit8 v8, v8, 0x1

    #@38
    goto :goto_29

    #@39
    .line 416
    :cond_39
    if-eqz v6, :cond_3e

    #@3b
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3e
    .end local v2           #projection:[Ljava/lang/String;
    .end local v8           #i:I
    :cond_3e
    :goto_3e
    move-object v0, v9

    #@3f
    .line 418
    goto :goto_21

    #@40
    .line 412
    :catch_40
    move-exception v7

    #@41
    .line 413
    .local v7, e:Ljava/lang/IllegalStateException;
    :try_start_41
    const-string v0, "browser"

    #@43
    const-string v1, "getVisitedHistory"

    #@45
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    .line 414
    const/4 v0, 0x0

    #@49
    new-array v9, v0, [Ljava/lang/String;
    :try_end_4b
    .catchall {:try_start_41 .. :try_end_4b} :catchall_51

    #@4b
    .line 416
    if-eqz v6, :cond_3e

    #@4d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@50
    goto :goto_3e

    #@51
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catchall_51
    move-exception v0

    #@52
    if-eqz v6, :cond_57

    #@54
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@57
    :cond_57
    throw v0
.end method

.method private static final getVisitedLike(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "cr"
    .parameter "url"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 299
    const/4 v7, 0x0

    #@2
    .line 300
    .local v7, secure:Z
    move-object v6, p1

    #@3
    .line 301
    .local v6, compareString:Ljava/lang/String;
    const-string v0, "http://"

    #@5
    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_73

    #@b
    .line 302
    const/4 v0, 0x7

    #@c
    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@f
    move-result-object v6

    #@10
    .line 307
    :cond_10
    :goto_10
    const-string/jumbo v0, "www."

    #@13
    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_1e

    #@19
    .line 308
    const/4 v0, 0x4

    #@1a
    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1d
    move-result-object v6

    #@1e
    .line 310
    :cond_1e
    const/4 v8, 0x0

    #@1f
    .line 311
    .local v8, whereClause:Ljava/lang/StringBuilder;
    if-eqz v7, :cond_83

    #@21
    .line 312
    new-instance v8, Ljava/lang/StringBuilder;

    #@23
    .end local v8           #whereClause:Ljava/lang/StringBuilder;
    const-string/jumbo v0, "url = "

    #@26
    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@29
    .line 313
    .restart local v8       #whereClause:Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v1, "https://"

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    invoke-static {v8, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@3f
    .line 315
    invoke-static {v8}, Landroid/provider/Browser;->addOrUrlEquals(Ljava/lang/StringBuilder;)V

    #@42
    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v1, "https://www."

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-static {v8, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@58
    .line 333
    :goto_58
    sget-object v1, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@5a
    const/4 v0, 0x2

    #@5b
    new-array v2, v0, [Ljava/lang/String;

    #@5d
    const/4 v0, 0x0

    #@5e
    const-string v3, "_id"

    #@60
    aput-object v3, v2, v0

    #@62
    const/4 v0, 0x1

    #@63
    const-string/jumbo v3, "visits"

    #@66
    aput-object v3, v2, v0

    #@68
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    move-object v0, p0

    #@6d
    move-object v5, v4

    #@6e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@71
    move-result-object v0

    #@72
    return-object v0

    #@73
    .line 303
    .end local v8           #whereClause:Ljava/lang/StringBuilder;
    :cond_73
    const-string v0, "https://"

    #@75
    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@78
    move-result v0

    #@79
    if-eqz v0, :cond_10

    #@7b
    .line 304
    const/16 v0, 0x8

    #@7d
    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    .line 305
    const/4 v7, 0x1

    #@82
    goto :goto_10

    #@83
    .line 319
    .restart local v8       #whereClause:Ljava/lang/StringBuilder;
    :cond_83
    new-instance v8, Ljava/lang/StringBuilder;

    #@85
    .end local v8           #whereClause:Ljava/lang/StringBuilder;
    const-string/jumbo v0, "url = "

    #@88
    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@8b
    .line 320
    .restart local v8       #whereClause:Ljava/lang/StringBuilder;
    invoke-static {v8, v6}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@8e
    .line 322
    invoke-static {v8}, Landroid/provider/Browser;->addOrUrlEquals(Ljava/lang/StringBuilder;)V

    #@91
    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string/jumbo v1, "www."

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v0

    #@a1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v9

    #@a5
    .line 324
    .local v9, wwwString:Ljava/lang/String;
    invoke-static {v8, v9}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@a8
    .line 326
    invoke-static {v8}, Landroid/provider/Browser;->addOrUrlEquals(Ljava/lang/StringBuilder;)V

    #@ab
    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v1, "http://"

    #@b2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v0

    #@b6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v0

    #@ba
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v0

    #@be
    invoke-static {v8, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@c1
    .line 329
    invoke-static {v8}, Landroid/provider/Browser;->addOrUrlEquals(Ljava/lang/StringBuilder;)V

    #@c4
    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v1, "http://"

    #@cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v0

    #@d7
    invoke-static {v8, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@da
    goto/16 :goto_58
.end method

.method public static final requestAllIcons(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V
    .registers 4
    .parameter "cr"
    .parameter "where"
    .parameter "listener"

    #@0
    .prologue
    .line 607
    invoke-static {}, Landroid/webkit/WebIconDatabase;->getInstance()Landroid/webkit/WebIconDatabase;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p0, p1, p2}, Landroid/webkit/WebIconDatabase;->bulkRequestIconForPageUrl(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/webkit/WebIconDatabase$IconListener;)V

    #@7
    .line 608
    return-void
.end method

.method public static final saveBookmark(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "c"
    .parameter "title"
    .parameter "url"

    #@0
    .prologue
    .line 161
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.INSERT"

    #@4
    sget-object v2, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    #@6
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@9
    .line 162
    .local v0, i:Landroid/content/Intent;
    const-string/jumbo v1, "title"

    #@c
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@f
    .line 163
    const-string/jumbo v1, "url"

    #@12
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 164
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@18
    .line 165
    return-void
.end method

.method public static final sendString(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "string"

    #@0
    .prologue
    .line 198
    const v0, 0x104041b

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-static {p0, p1, v0}, Landroid/provider/Browser;->sendString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 199
    return-void
.end method

.method public static final sendString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "c"
    .parameter "stringToSend"
    .parameter "chooserDialogTitle"

    #@0
    .prologue
    .line 214
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "android.intent.action.SEND"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 215
    .local v1, send:Landroid/content/Intent;
    const-string/jumbo v2, "text/plain"

    #@a
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@d
    .line 216
    const-string v2, "android.intent.extra.TEXT"

    #@f
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@12
    .line 219
    :try_start_12
    invoke-static {v1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    #@15
    move-result-object v0

    #@16
    .line 221
    .local v0, i:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@18
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@1b
    .line 222
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1e
    .catch Landroid/content/ActivityNotFoundException; {:try_start_12 .. :try_end_1e} :catch_1f

    #@1e
    .line 226
    .end local v0           #i:Landroid/content/Intent;
    :goto_1e
    return-void

    #@1f
    .line 223
    :catch_1f
    move-exception v2

    #@20
    goto :goto_1e
.end method

.method public static final sendStringEx(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "string"

    #@0
    .prologue
    .line 238
    const v0, 0x104041b

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-static {p0, p1, v0}, Landroid/provider/Browser;->sendStringEx(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 239
    return-void
.end method

.method public static final sendStringEx(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "c"
    .parameter "stringToSend"
    .parameter "chooserDialogTitle"

    #@0
    .prologue
    .line 252
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "android.intent.action.SEND"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 253
    .local v1, send:Landroid/content/Intent;
    const-string/jumbo v2, "text/plain"

    #@a
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@d
    .line 254
    const-string v2, "android.intent.extra.TEXT"

    #@f
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@12
    .line 256
    :try_start_12
    invoke-static {v1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    #@15
    move-result-object v0

    #@16
    .line 257
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_19
    .catch Landroid/content/ActivityNotFoundException; {:try_start_12 .. :try_end_19} :catch_1a

    #@19
    .line 261
    .end local v0           #i:Landroid/content/Intent;
    :goto_19
    return-void

    #@1a
    .line 258
    :catch_1a
    move-exception v2

    #@1b
    goto :goto_19
.end method

.method public static final truncateHistory(Landroid/content/ContentResolver;)V
    .registers 11
    .parameter "cr"

    #@0
    .prologue
    .line 434
    const/4 v6, 0x0

    #@1
    .line 438
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_1
    sget-object v1, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const/4 v0, 0x3

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    const-string v3, "_id"

    #@9
    aput-object v3, v2, v0

    #@b
    const/4 v0, 0x1

    #@c
    const-string/jumbo v3, "url"

    #@f
    aput-object v3, v2, v0

    #@11
    const/4 v0, 0x2

    #@12
    const-string v3, "date"

    #@14
    aput-object v3, v2, v0

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x0

    #@18
    const-string v5, "date ASC"

    #@1a
    move-object v0, p0

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v6

    #@1f
    .line 442
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_53

    #@25
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@28
    move-result v0

    #@29
    const/16 v1, 0xfa

    #@2b
    if-lt v0, v1, :cond_53

    #@2d
    .line 443
    invoke-static {}, Landroid/webkit/WebIconDatabase;->getInstance()Landroid/webkit/WebIconDatabase;

    #@30
    move-result-object v9

    #@31
    .line 445
    .local v9, iconDb:Landroid/webkit/WebIconDatabase;
    const/4 v8, 0x0

    #@32
    .local v8, i:I
    :goto_32
    const/4 v0, 0x5

    #@33
    if-ge v8, v0, :cond_53

    #@35
    .line 446
    sget-object v0, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@37
    const/4 v1, 0x0

    #@38
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    #@3b
    move-result-wide v1

    #@3c
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@3f
    move-result-object v0

    #@40
    const/4 v1, 0x0

    #@41
    const/4 v2, 0x0

    #@42
    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@45
    .line 448
    const/4 v0, 0x1

    #@46
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {v9, v0}, Landroid/webkit/WebIconDatabase;->releaseIconForPageUrl(Ljava/lang/String;)V

    #@4d
    .line 449
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_50
    .catchall {:try_start_1 .. :try_end_50} :catchall_6b
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_50} :catch_5c

    #@50
    move-result v0

    #@51
    if-nez v0, :cond_59

    #@53
    .line 455
    .end local v8           #i:I
    .end local v9           #iconDb:Landroid/webkit/WebIconDatabase;
    :cond_53
    if-eqz v6, :cond_58

    #@55
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@58
    .line 457
    :cond_58
    :goto_58
    return-void

    #@59
    .line 445
    .restart local v8       #i:I
    .restart local v9       #iconDb:Landroid/webkit/WebIconDatabase;
    :cond_59
    add-int/lit8 v8, v8, 0x1

    #@5b
    goto :goto_32

    #@5c
    .line 452
    .end local v8           #i:I
    .end local v9           #iconDb:Landroid/webkit/WebIconDatabase;
    :catch_5c
    move-exception v7

    #@5d
    .line 453
    .local v7, e:Ljava/lang/IllegalStateException;
    :try_start_5d
    const-string v0, "browser"

    #@5f
    const-string/jumbo v1, "truncateHistory"

    #@62
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_65
    .catchall {:try_start_5d .. :try_end_65} :catchall_6b

    #@65
    .line 455
    if-eqz v6, :cond_58

    #@67
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6a
    goto :goto_58

    #@6b
    .end local v7           #e:Ljava/lang/IllegalStateException;
    :catchall_6b
    move-exception v0

    #@6c
    if-eqz v6, :cond_71

    #@6e
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@71
    :cond_71
    throw v0
.end method

.method public static final updateVisitedHistory(Landroid/content/ContentResolver;Ljava/lang/String;Z)V
    .registers 13
    .parameter "cr"
    .parameter "url"
    .parameter "real"

    #@0
    .prologue
    .line 349
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v2

    #@4
    .line 350
    .local v2, now:J
    const/4 v0, 0x0

    #@5
    .line 352
    .local v0, c:Landroid/database/Cursor;
    :try_start_5
    invoke-static {p0, p1}, Landroid/provider/Browser;->getVisitedLike(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;

    #@8
    move-result-object v0

    #@9
    .line 354
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@c
    move-result v7

    #@d
    if-eqz v7, :cond_61

    #@f
    .line 355
    new-instance v5, Landroid/content/ContentValues;

    #@11
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    #@14
    .line 356
    .local v5, values:Landroid/content/ContentValues;
    if-eqz p2, :cond_46

    #@16
    .line 357
    const-string/jumbo v7, "visits"

    #@19
    const/4 v8, 0x1

    #@1a
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    #@1d
    move-result v8

    #@1e
    add-int/lit8 v8, v8, 0x1

    #@20
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@27
    .line 361
    :goto_27
    const-string v7, "date"

    #@29
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@30
    .line 362
    sget-object v7, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@32
    const/4 v8, 0x0

    #@33
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    #@36
    move-result-wide v8

    #@37
    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@3a
    move-result-object v7

    #@3b
    const/4 v8, 0x0

    #@3c
    const/4 v9, 0x0

    #@3d
    invoke-virtual {p0, v7, v5, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_40
    .catchall {:try_start_5 .. :try_end_40} :catchall_a6
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_40} :catch_52

    #@40
    .line 387
    :goto_40
    if-eqz v0, :cond_45

    #@42
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@45
    .line 389
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_45
    :goto_45
    return-void

    #@46
    .line 359
    .restart local v5       #values:Landroid/content/ContentValues;
    :cond_46
    :try_start_46
    const-string/jumbo v7, "user_entered"

    #@49
    const/4 v8, 0x1

    #@4a
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d
    move-result-object v8

    #@4e
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_51
    .catchall {:try_start_46 .. :try_end_51} :catchall_a6
    .catch Ljava/lang/IllegalStateException; {:try_start_46 .. :try_end_51} :catch_52

    #@51
    goto :goto_27

    #@52
    .line 384
    .end local v5           #values:Landroid/content/ContentValues;
    :catch_52
    move-exception v1

    #@53
    .line 385
    .local v1, e:Ljava/lang/IllegalStateException;
    :try_start_53
    const-string v7, "browser"

    #@55
    const-string/jumbo v8, "updateVisitedHistory"

    #@58
    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5b
    .catchall {:try_start_53 .. :try_end_5b} :catchall_a6

    #@5b
    .line 387
    if-eqz v0, :cond_45

    #@5d
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@60
    goto :goto_45

    #@61
    .line 365
    .end local v1           #e:Ljava/lang/IllegalStateException;
    :cond_61
    :try_start_61
    invoke-static {p0}, Landroid/provider/Browser;->truncateHistory(Landroid/content/ContentResolver;)V

    #@64
    .line 366
    new-instance v5, Landroid/content/ContentValues;

    #@66
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    #@69
    .line 369
    .restart local v5       #values:Landroid/content/ContentValues;
    if-eqz p2, :cond_ad

    #@6b
    .line 370
    const/4 v6, 0x1

    #@6c
    .line 371
    .local v6, visits:I
    const/4 v4, 0x0

    #@6d
    .line 376
    .local v4, user_entered:I
    :goto_6d
    const-string/jumbo v7, "url"

    #@70
    invoke-virtual {v5, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 377
    const-string/jumbo v7, "visits"

    #@76
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7d
    .line 378
    const-string v7, "date"

    #@7f
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@82
    move-result-object v8

    #@83
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@86
    .line 379
    const-string/jumbo v7, "title"

    #@89
    invoke-virtual {v5, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8c
    .line 380
    const-string v7, "created"

    #@8e
    const/4 v8, 0x0

    #@8f
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@92
    move-result-object v8

    #@93
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@96
    .line 381
    const-string/jumbo v7, "user_entered"

    #@99
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v8

    #@9d
    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a0
    .line 382
    sget-object v7, Landroid/provider/BrowserContract$History;->CONTENT_URI:Landroid/net/Uri;

    #@a2
    invoke-virtual {p0, v7, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_a5
    .catchall {:try_start_61 .. :try_end_a5} :catchall_a6
    .catch Ljava/lang/IllegalStateException; {:try_start_61 .. :try_end_a5} :catch_52

    #@a5
    goto :goto_40

    #@a6
    .line 387
    .end local v4           #user_entered:I
    .end local v5           #values:Landroid/content/ContentValues;
    .end local v6           #visits:I
    :catchall_a6
    move-exception v7

    #@a7
    if-eqz v0, :cond_ac

    #@a9
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@ac
    :cond_ac
    throw v7

    #@ad
    .line 373
    .restart local v5       #values:Landroid/content/ContentValues;
    :cond_ad
    const/4 v6, 0x0

    #@ae
    .line 374
    .restart local v6       #visits:I
    const/4 v4, 0x1

    #@af
    .restart local v4       #user_entered:I
    goto :goto_6d
.end method
