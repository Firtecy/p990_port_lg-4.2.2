.class public final Landroid/provider/Settings$AssistDial;
.super Ljava/lang/Object;
.source "Settings.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AssistDial"
.end annotation


# static fields
.field public static final AREA:Ljava/lang/String; = "area"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final COUNTRYCODE:Ljava/lang/String; = "countrycode"

.field public static final COUNTRYINDEX:Ljava/lang/String; = "countryindex"

.field public static final COUNTRYNAME:Ljava/lang/String; = "countryname"

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "countryindex ASC"

.field public static final IDDPREFIX:Ljava/lang/String; = "iddprefix"

.field public static final LENGTH:Ljava/lang/String; = "length"

.field public static final MCC:Ljava/lang/String; = "mcc"

.field public static final NANP:Ljava/lang/String; = "nanp"

.field public static final NDDPREFIX:Ljava/lang/String; = "nddprefix"

.field public static final PROJECTION:[Ljava/lang/String; = null

.field public static final TABLE_NAME:Ljava/lang/String; = "assist_dial"

.field private static countryOriginDataTable:[Landroid/provider/ReferenceCountry;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 7005
    const-string v0, "content://settings/assist_dial"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 7029
    const/4 v0, 0x0

    #@9
    sput-object v0, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@b
    .line 7031
    const/16 v0, 0x9

    #@d
    new-array v0, v0, [Ljava/lang/String;

    #@f
    const/4 v1, 0x0

    #@10
    const-string v2, "countryindex"

    #@12
    aput-object v2, v0, v1

    #@14
    const/4 v1, 0x1

    #@15
    const-string v2, "countryname"

    #@17
    aput-object v2, v0, v1

    #@19
    const/4 v1, 0x2

    #@1a
    const-string/jumbo v2, "mcc"

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x3

    #@20
    const-string v2, "countrycode"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x4

    #@25
    const-string v2, "iddprefix"

    #@27
    aput-object v2, v0, v1

    #@29
    const/4 v1, 0x5

    #@2a
    const-string/jumbo v2, "nddprefix"

    #@2d
    aput-object v2, v0, v1

    #@2f
    const/4 v1, 0x6

    #@30
    const-string/jumbo v2, "nanp"

    #@33
    aput-object v2, v0, v1

    #@35
    const/4 v1, 0x7

    #@36
    const-string v2, "area"

    #@38
    aput-object v2, v0, v1

    #@3a
    const/16 v1, 0x8

    #@3c
    const-string/jumbo v2, "length"

    #@3f
    aput-object v2, v0, v1

    #@41
    sput-object v0, Landroid/provider/Settings$AssistDial;->PROJECTION:[Ljava/lang/String;

    #@43
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 7003
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static initAssistDialCountryDetailList(Landroid/content/ContentResolver;)V
    .registers 9
    .parameter "resolver"

    #@0
    .prologue
    .line 7036
    const-string v5, "Settings"

    #@2
    const-string v6, "initAssistDialCountryDetailList()"

    #@4
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 7037
    invoke-static {}, Landroid/provider/Settings$AssistDial;->initCountryOriginDataTable()V

    #@a
    .line 7039
    const-string v0, "000"

    #@c
    .line 7040
    .local v0, areaCode:Ljava/lang/String;
    const-string v2, "10"

    #@e
    .line 7042
    .local v2, numLength:Ljava/lang/String;
    sget-object v5, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@10
    if-nez v5, :cond_1a

    #@12
    .line 7043
    const-string v5, "Settings"

    #@14
    const-string v6, "List is null"

    #@16
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 7070
    :goto_19
    return-void

    #@1a
    .line 7045
    :cond_1a
    sget-object v5, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@1c
    array-length v3, v5

    #@1d
    .line 7046
    .local v3, size:I
    new-array v4, v3, [Landroid/content/ContentValues;

    #@1f
    .line 7048
    .local v4, values:[Landroid/content/ContentValues;
    const/4 v1, 0x0

    #@20
    .local v1, i:I
    :goto_20
    if-ge v1, v3, :cond_ac

    #@22
    .line 7049
    new-instance v5, Landroid/content/ContentValues;

    #@24
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    #@27
    aput-object v5, v4, v1

    #@29
    .line 7050
    aget-object v5, v4, v1

    #@2b
    const-string v6, "countryindex"

    #@2d
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@2f
    aget-object v7, v7, v1

    #@31
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getIndex()I

    #@34
    move-result v7

    #@35
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3c
    .line 7051
    aget-object v5, v4, v1

    #@3e
    const-string v6, "countryname"

    #@40
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@42
    aget-object v7, v7, v1

    #@44
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 7052
    aget-object v5, v4, v1

    #@4d
    const-string/jumbo v6, "mcc"

    #@50
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@52
    aget-object v7, v7, v1

    #@54
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getMccCode()Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 7053
    aget-object v5, v4, v1

    #@5d
    const-string v6, "countrycode"

    #@5f
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@61
    aget-object v7, v7, v1

    #@63
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@66
    move-result-object v7

    #@67
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 7054
    aget-object v5, v4, v1

    #@6c
    const-string v6, "iddprefix"

    #@6e
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@70
    aget-object v7, v7, v1

    #@72
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@79
    .line 7055
    aget-object v5, v4, v1

    #@7b
    const-string/jumbo v6, "nddprefix"

    #@7e
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@80
    aget-object v7, v7, v1

    #@82
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@85
    move-result-object v7

    #@86
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 7056
    aget-object v5, v4, v1

    #@8b
    const-string/jumbo v6, "nanp"

    #@8e
    sget-object v7, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@90
    aget-object v7, v7, v1

    #@92
    invoke-virtual {v7}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@95
    move-result-object v7

    #@96
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@99
    .line 7057
    aget-object v5, v4, v1

    #@9b
    const-string v6, "area"

    #@9d
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 7058
    aget-object v5, v4, v1

    #@a2
    const-string/jumbo v6, "length"

    #@a5
    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    .line 7048
    add-int/lit8 v1, v1, 0x1

    #@aa
    goto/16 :goto_20

    #@ac
    .line 7061
    :cond_ac
    if-nez p0, :cond_b7

    #@ae
    .line 7063
    const-string v5, "Settings"

    #@b0
    const-string v6, "Resolver is null"

    #@b2
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    goto/16 :goto_19

    #@b7
    .line 7066
    :cond_b7
    sget-object v5, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@b9
    invoke-virtual {p0, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    #@bc
    goto/16 :goto_19
.end method

.method public static initAssistDialCountryDetailList(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .registers 9
    .parameter "resolver"
    .parameter "areaCode"

    #@0
    .prologue
    .line 7074
    const-string v4, "Settings"

    #@2
    const-string v5, "initAssistDialCountryDetailList()"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 7075
    invoke-static {}, Landroid/provider/Settings$AssistDial;->initCountryOriginDataTable()V

    #@a
    .line 7078
    const-string v1, "10"

    #@c
    .line 7080
    .local v1, numLength:Ljava/lang/String;
    sget-object v4, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@e
    if-nez v4, :cond_18

    #@10
    .line 7081
    const-string v4, "Settings"

    #@12
    const-string v5, "List is null"

    #@14
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 7107
    :goto_17
    return-void

    #@18
    .line 7083
    :cond_18
    sget-object v4, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@1a
    array-length v2, v4

    #@1b
    .line 7084
    .local v2, size:I
    new-array v3, v2, [Landroid/content/ContentValues;

    #@1d
    .line 7086
    .local v3, values:[Landroid/content/ContentValues;
    const/4 v0, 0x0

    #@1e
    .local v0, i:I
    :goto_1e
    if-ge v0, v2, :cond_aa

    #@20
    .line 7087
    new-instance v4, Landroid/content/ContentValues;

    #@22
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@25
    aput-object v4, v3, v0

    #@27
    .line 7088
    aget-object v4, v3, v0

    #@29
    const-string v5, "countryindex"

    #@2b
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@2d
    aget-object v6, v6, v0

    #@2f
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getIndex()I

    #@32
    move-result v6

    #@33
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3a
    .line 7089
    aget-object v4, v3, v0

    #@3c
    const-string v5, "countryname"

    #@3e
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@40
    aget-object v6, v6, v0

    #@42
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getCountryName()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 7090
    aget-object v4, v3, v0

    #@4b
    const-string/jumbo v5, "mcc"

    #@4e
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@50
    aget-object v6, v6, v0

    #@52
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getMccCode()Ljava/lang/String;

    #@55
    move-result-object v6

    #@56
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    .line 7091
    aget-object v4, v3, v0

    #@5b
    const-string v5, "countrycode"

    #@5d
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@5f
    aget-object v6, v6, v0

    #@61
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getCountryCode()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 7092
    aget-object v4, v3, v0

    #@6a
    const-string v5, "iddprefix"

    #@6c
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@6e
    aget-object v6, v6, v0

    #@70
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 7093
    aget-object v4, v3, v0

    #@79
    const-string/jumbo v5, "nddprefix"

    #@7c
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@7e
    aget-object v6, v6, v0

    #@80
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getNddPrefix()Ljava/lang/String;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@87
    .line 7094
    aget-object v4, v3, v0

    #@89
    const-string/jumbo v5, "nanp"

    #@8c
    sget-object v6, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@8e
    aget-object v6, v6, v0

    #@90
    invoke-virtual {v6}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@93
    move-result-object v6

    #@94
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@97
    .line 7095
    aget-object v4, v3, v0

    #@99
    const-string v5, "area"

    #@9b
    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9e
    .line 7096
    aget-object v4, v3, v0

    #@a0
    const-string/jumbo v5, "length"

    #@a3
    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    .line 7086
    add-int/lit8 v0, v0, 0x1

    #@a8
    goto/16 :goto_1e

    #@aa
    .line 7099
    :cond_aa
    if-nez p0, :cond_b5

    #@ac
    .line 7101
    const-string v4, "Settings"

    #@ae
    const-string v5, "Resolver is null"

    #@b0
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    goto/16 :goto_17

    #@b5
    .line 7104
    :cond_b5
    sget-object v4, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@b7
    invoke-virtual {p0, v4, v3}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    #@ba
    goto/16 :goto_17
.end method

.method private static initCountryOriginDataTable()V
    .registers 16

    #@0
    .prologue
    const/4 v15, 0x4

    #@1
    const/4 v14, 0x3

    #@2
    const/4 v13, 0x2

    #@3
    const/4 v12, 0x1

    #@4
    const/4 v1, 0x0

    #@5
    .line 7111
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@8
    move-result-object v10

    #@9
    .line 7112
    .local v10, r:Landroid/content/res/Resources;
    const/16 v0, 0xdf

    #@b
    new-array v11, v0, [Landroid/provider/ReferenceCountry;

    #@d
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@f
    const v2, 0x2090174

    #@12
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    const-string v3, "310,311,312,313,314,315,316"

    #@18
    const-string v4, "1"

    #@1a
    const-string v5, "011"

    #@1c
    const-string v6, "1"

    #@1e
    const-string v7, "1"

    #@20
    const-string v8, "000"

    #@22
    const-string v9, "10"

    #@24
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@27
    aput-object v0, v11, v1

    #@29
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@2b
    const v1, 0x20901da

    #@2e
    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    const-string v3, "412"

    #@34
    const-string v4, "93"

    #@36
    const-string v5, "00"

    #@38
    const-string v6, "0"

    #@3a
    const-string v7, "0"

    #@3c
    const-string v8, "000"

    #@3e
    const-string v9, "10"

    #@40
    move v1, v12

    #@41
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@44
    aput-object v0, v11, v12

    #@46
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@48
    const v1, 0x20901db

    #@4b
    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    const-string v3, "276"

    #@51
    const-string v4, "355"

    #@53
    const-string v5, "00"

    #@55
    const-string v6, "0"

    #@57
    const-string v7, "0"

    #@59
    const-string v8, "000"

    #@5b
    const-string v9, "10"

    #@5d
    move v1, v13

    #@5e
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@61
    aput-object v0, v11, v13

    #@63
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@65
    const v1, 0x2090175

    #@68
    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    const-string v3, "603"

    #@6e
    const-string v4, "213"

    #@70
    const-string v5, "00"

    #@72
    const-string v6, "0"

    #@74
    const-string v7, "0"

    #@76
    const-string v8, "000"

    #@78
    const-string v9, "10"

    #@7a
    move v1, v14

    #@7b
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    aput-object v0, v11, v14

    #@80
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@82
    const v1, 0x20901dc

    #@85
    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    const-string v3, "544"

    #@8b
    const-string v4, "1"

    #@8d
    const-string v5, "011"

    #@8f
    const-string v6, "1"

    #@91
    const-string v7, "1"

    #@93
    const-string v8, "000"

    #@95
    const-string v9, "10"

    #@97
    move v1, v15

    #@98
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@9b
    aput-object v0, v11, v15

    #@9d
    const/4 v12, 0x5

    #@9e
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@a0
    const/4 v1, 0x5

    #@a1
    const v2, 0x20901dd

    #@a4
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a7
    move-result-object v2

    #@a8
    const-string v3, "213"

    #@aa
    const-string v4, "376"

    #@ac
    const-string v5, "00"

    #@ae
    const-string v6, ""

    #@b0
    const-string v7, "0"

    #@b2
    const-string v8, "000"

    #@b4
    const-string v9, "10"

    #@b6
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b9
    aput-object v0, v11, v12

    #@bb
    const/4 v12, 0x6

    #@bc
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@be
    const/4 v1, 0x6

    #@bf
    const v2, 0x2090176

    #@c2
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@c5
    move-result-object v2

    #@c6
    const-string v3, "631"

    #@c8
    const-string v4, "244"

    #@ca
    const-string v5, "00"

    #@cc
    const-string v6, "0"

    #@ce
    const-string v7, "0"

    #@d0
    const-string v8, "000"

    #@d2
    const-string v9, "10"

    #@d4
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d7
    aput-object v0, v11, v12

    #@d9
    const/4 v12, 0x7

    #@da
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@dc
    const/4 v1, 0x7

    #@dd
    const v2, 0x20901de

    #@e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e3
    move-result-object v2

    #@e4
    const-string v3, "365"

    #@e6
    const-string v4, "1"

    #@e8
    const-string v5, "011"

    #@ea
    const-string v6, "1"

    #@ec
    const-string v7, "1"

    #@ee
    const-string v8, "000"

    #@f0
    const-string v9, "10"

    #@f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@f5
    aput-object v0, v11, v12

    #@f7
    const/16 v12, 0x8

    #@f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@fb
    const/16 v1, 0x8

    #@fd
    const v2, 0x20901df

    #@100
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@103
    move-result-object v2

    #@104
    const-string v3, "344"

    #@106
    const-string v4, "1"

    #@108
    const-string v5, "011"

    #@10a
    const-string v6, "1"

    #@10c
    const-string v7, "1"

    #@10e
    const-string v8, "000"

    #@110
    const-string v9, "10"

    #@112
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@115
    aput-object v0, v11, v12

    #@117
    const/16 v12, 0x9

    #@119
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@11b
    const/16 v1, 0x9

    #@11d
    const v2, 0x2090177

    #@120
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@123
    move-result-object v2

    #@124
    const-string v3, "722"

    #@126
    const-string v4, "54"

    #@128
    const-string v5, "00"

    #@12a
    const-string v6, "0"

    #@12c
    const-string v7, "0"

    #@12e
    const-string v8, "000"

    #@130
    const-string v9, "10"

    #@132
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@135
    aput-object v0, v11, v12

    #@137
    const/16 v12, 0xa

    #@139
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@13b
    const/16 v1, 0xa

    #@13d
    const v2, 0x20901e0

    #@140
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@143
    move-result-object v2

    #@144
    const-string v3, "283"

    #@146
    const-string v4, "374"

    #@148
    const-string v5, "00"

    #@14a
    const-string v6, "0"

    #@14c
    const-string v7, "0"

    #@14e
    const-string v8, "000"

    #@150
    const-string v9, "10"

    #@152
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@155
    aput-object v0, v11, v12

    #@157
    const/16 v12, 0xb

    #@159
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@15b
    const/16 v1, 0xb

    #@15d
    const v2, 0x20901e1

    #@160
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@163
    move-result-object v2

    #@164
    const-string v3, "363"

    #@166
    const-string v4, "297"

    #@168
    const-string v5, "00"

    #@16a
    const-string v6, ""

    #@16c
    const-string v7, "0"

    #@16e
    const-string v8, "000"

    #@170
    const-string v9, "10"

    #@172
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@175
    aput-object v0, v11, v12

    #@177
    const/16 v12, 0xc

    #@179
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@17b
    const/16 v1, 0xc

    #@17d
    const v2, 0x20901e2

    #@180
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@183
    move-result-object v2

    #@184
    const-string v3, "505"

    #@186
    const-string v4, "61"

    #@188
    const-string v5, "0011"

    #@18a
    const-string v6, "0"

    #@18c
    const-string v7, "0"

    #@18e
    const-string v8, "000"

    #@190
    const-string v9, "10"

    #@192
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@195
    aput-object v0, v11, v12

    #@197
    const/16 v12, 0xd

    #@199
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@19b
    const/16 v1, 0xd

    #@19d
    const v2, 0x20901e3

    #@1a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a3
    move-result-object v2

    #@1a4
    const-string v3, "232"

    #@1a6
    const-string v4, "43"

    #@1a8
    const-string v5, "00"

    #@1aa
    const-string v6, "0"

    #@1ac
    const-string v7, "0"

    #@1ae
    const-string v8, "000"

    #@1b0
    const-string v9, "10"

    #@1b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1b5
    aput-object v0, v11, v12

    #@1b7
    const/16 v12, 0xe

    #@1b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1bb
    const/16 v1, 0xe

    #@1bd
    const v2, 0x20901e4

    #@1c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1c3
    move-result-object v2

    #@1c4
    const-string v3, "400"

    #@1c6
    const-string v4, "994"

    #@1c8
    const-string v5, "00"

    #@1ca
    const-string v6, "0"

    #@1cc
    const-string v7, "0"

    #@1ce
    const-string v8, "000"

    #@1d0
    const-string v9, "10"

    #@1d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1d5
    aput-object v0, v11, v12

    #@1d7
    const/16 v12, 0xf

    #@1d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1db
    const/16 v1, 0xf

    #@1dd
    const v2, 0x2090178

    #@1e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1e3
    move-result-object v2

    #@1e4
    const-string v3, "364"

    #@1e6
    const-string v4, "1"

    #@1e8
    const-string v5, "011"

    #@1ea
    const-string v6, "1"

    #@1ec
    const-string v7, "1"

    #@1ee
    const-string v8, "000"

    #@1f0
    const-string v9, "10"

    #@1f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1f5
    aput-object v0, v11, v12

    #@1f7
    const/16 v12, 0x10

    #@1f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1fb
    const/16 v1, 0x10

    #@1fd
    const v2, 0x20901e5

    #@200
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@203
    move-result-object v2

    #@204
    const-string v3, "426"

    #@206
    const-string v4, "973"

    #@208
    const-string v5, "00"

    #@20a
    const-string v6, ""

    #@20c
    const-string v7, "0"

    #@20e
    const-string v8, "000"

    #@210
    const-string v9, "10"

    #@212
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@215
    aput-object v0, v11, v12

    #@217
    const/16 v12, 0x11

    #@219
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@21b
    const/16 v1, 0x11

    #@21d
    const v2, 0x2090179

    #@220
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@223
    move-result-object v2

    #@224
    const-string v3, "470"

    #@226
    const-string v4, "880"

    #@228
    const-string v5, "00"

    #@22a
    const-string v6, "0"

    #@22c
    const-string v7, "0"

    #@22e
    const-string v8, "000"

    #@230
    const-string v9, "10"

    #@232
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@235
    aput-object v0, v11, v12

    #@237
    const/16 v12, 0x12

    #@239
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@23b
    const/16 v1, 0x12

    #@23d
    const v2, 0x209017a

    #@240
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@243
    move-result-object v2

    #@244
    const-string v3, "342"

    #@246
    const-string v4, "1"

    #@248
    const-string v5, "011"

    #@24a
    const-string v6, "1"

    #@24c
    const-string v7, "1"

    #@24e
    const-string v8, "000"

    #@250
    const-string v9, "10"

    #@252
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@255
    aput-object v0, v11, v12

    #@257
    const/16 v12, 0x13

    #@259
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@25b
    const/16 v1, 0x13

    #@25d
    const v2, 0x209017b

    #@260
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@263
    move-result-object v2

    #@264
    const-string v3, "257"

    #@266
    const-string v4, "375"

    #@268
    const-string v5, "810"

    #@26a
    const-string v6, "8"

    #@26c
    const-string v7, "0"

    #@26e
    const-string v8, "000"

    #@270
    const-string v9, "10"

    #@272
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@275
    aput-object v0, v11, v12

    #@277
    const/16 v12, 0x14

    #@279
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@27b
    const/16 v1, 0x14

    #@27d
    const v2, 0x20901e6

    #@280
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@283
    move-result-object v2

    #@284
    const-string v3, "206"

    #@286
    const-string v4, "32"

    #@288
    const-string v5, "00"

    #@28a
    const-string v6, "0"

    #@28c
    const-string v7, "0"

    #@28e
    const-string v8, "000"

    #@290
    const-string v9, "10"

    #@292
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@295
    aput-object v0, v11, v12

    #@297
    const/16 v12, 0x15

    #@299
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@29b
    const/16 v1, 0x15

    #@29d
    const v2, 0x209017c

    #@2a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2a3
    move-result-object v2

    #@2a4
    const-string v3, "702"

    #@2a6
    const-string v4, "501"

    #@2a8
    const-string v5, "00"

    #@2aa
    const-string v6, ""

    #@2ac
    const-string v7, "0"

    #@2ae
    const-string v8, "000"

    #@2b0
    const-string v9, "10"

    #@2b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2b5
    aput-object v0, v11, v12

    #@2b7
    const/16 v12, 0x16

    #@2b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@2bb
    const/16 v1, 0x16

    #@2bd
    const v2, 0x20901e7

    #@2c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2c3
    move-result-object v2

    #@2c4
    const-string v3, "616,"

    #@2c6
    const-string v4, "229"

    #@2c8
    const-string v5, "00"

    #@2ca
    const-string v6, ""

    #@2cc
    const-string v7, "0"

    #@2ce
    const-string v8, "000"

    #@2d0
    const-string v9, "10"

    #@2d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2d5
    aput-object v0, v11, v12

    #@2d7
    const/16 v12, 0x17

    #@2d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@2db
    const/16 v1, 0x17

    #@2dd
    const v2, 0x209017d

    #@2e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2e3
    move-result-object v2

    #@2e4
    const-string v3, "350"

    #@2e6
    const-string v4, "1"

    #@2e8
    const-string v5, "011"

    #@2ea
    const-string v6, "1"

    #@2ec
    const-string v7, "1"

    #@2ee
    const-string v8, "000"

    #@2f0
    const-string v9, "10"

    #@2f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2f5
    aput-object v0, v11, v12

    #@2f7
    const/16 v12, 0x18

    #@2f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@2fb
    const/16 v1, 0x18

    #@2fd
    const v2, 0x20901e8

    #@300
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@303
    move-result-object v2

    #@304
    const-string v3, "402"

    #@306
    const-string v4, "975"

    #@308
    const-string v5, "00"

    #@30a
    const-string v6, ""

    #@30c
    const-string v7, "0"

    #@30e
    const-string v8, "000"

    #@310
    const-string v9, "10"

    #@312
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@315
    aput-object v0, v11, v12

    #@317
    const/16 v12, 0x19

    #@319
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@31b
    const/16 v1, 0x19

    #@31d
    const v2, 0x20901e9

    #@320
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@323
    move-result-object v2

    #@324
    const-string v3, "736"

    #@326
    const-string v4, "591"

    #@328
    const-string v5, "00"

    #@32a
    const-string v6, "0"

    #@32c
    const-string v7, "0"

    #@32e
    const-string v8, "000"

    #@330
    const-string v9, "10"

    #@332
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@335
    aput-object v0, v11, v12

    #@337
    const/16 v12, 0x1a

    #@339
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@33b
    const/16 v1, 0x1a

    #@33d
    const v2, 0x20901ea

    #@340
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@343
    move-result-object v2

    #@344
    const-string v3, "218"

    #@346
    const-string v4, "387"

    #@348
    const-string v5, "00"

    #@34a
    const-string v6, "0"

    #@34c
    const-string v7, "0"

    #@34e
    const-string v8, "000"

    #@350
    const-string v9, "10"

    #@352
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@355
    aput-object v0, v11, v12

    #@357
    const/16 v12, 0x1b

    #@359
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@35b
    const/16 v1, 0x1b

    #@35d
    const v2, 0x209017e

    #@360
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@363
    move-result-object v2

    #@364
    const-string v3, "652"

    #@366
    const-string v4, "267"

    #@368
    const-string v5, "00"

    #@36a
    const-string v6, ""

    #@36c
    const-string v7, "0"

    #@36e
    const-string v8, "000"

    #@370
    const-string v9, "10"

    #@372
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@375
    aput-object v0, v11, v12

    #@377
    const/16 v12, 0x1c

    #@379
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@37b
    const/16 v1, 0x1c

    #@37d
    const v2, 0x209017f

    #@380
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@383
    move-result-object v2

    #@384
    const-string v3, "724"

    #@386
    const-string v4, "55"

    #@388
    const-string v5, "0015"

    #@38a
    const-string v6, "0"

    #@38c
    const-string v7, "0"

    #@38e
    const-string v8, "000"

    #@390
    const-string v9, "10"

    #@392
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@395
    aput-object v0, v11, v12

    #@397
    const/16 v12, 0x1d

    #@399
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@39b
    const/16 v1, 0x1d

    #@39d
    const v2, 0x2090180

    #@3a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3a3
    move-result-object v2

    #@3a4
    const-string v3, "348"

    #@3a6
    const-string v4, "1"

    #@3a8
    const-string v5, "011"

    #@3aa
    const-string v6, "1"

    #@3ac
    const-string v7, "1"

    #@3ae
    const-string v8, "000"

    #@3b0
    const-string v9, "10"

    #@3b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@3b5
    aput-object v0, v11, v12

    #@3b7
    const/16 v12, 0x1e

    #@3b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@3bb
    const/16 v1, 0x1e

    #@3bd
    const v2, 0x20901eb

    #@3c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3c3
    move-result-object v2

    #@3c4
    const-string v3, "528"

    #@3c6
    const-string v4, "673"

    #@3c8
    const-string v5, "00"

    #@3ca
    const-string v6, ""

    #@3cc
    const-string v7, "0"

    #@3ce
    const-string v8, "000"

    #@3d0
    const-string v9, "10"

    #@3d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@3d5
    aput-object v0, v11, v12

    #@3d7
    const/16 v12, 0x1f

    #@3d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@3db
    const/16 v1, 0x1f

    #@3dd
    const v2, 0x20901ec

    #@3e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3e3
    move-result-object v2

    #@3e4
    const-string v3, "284"

    #@3e6
    const-string v4, "359"

    #@3e8
    const-string v5, "00"

    #@3ea
    const-string v6, "0"

    #@3ec
    const-string v7, "0"

    #@3ee
    const-string v8, "000"

    #@3f0
    const-string v9, "10"

    #@3f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@3f5
    aput-object v0, v11, v12

    #@3f7
    const/16 v12, 0x20

    #@3f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@3fb
    const/16 v1, 0x20

    #@3fd
    const v2, 0x20901ed

    #@400
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@403
    move-result-object v2

    #@404
    const-string v3, "613"

    #@406
    const-string v4, "226"

    #@408
    const-string v5, "00"

    #@40a
    const-string v6, ""

    #@40c
    const-string v7, "0"

    #@40e
    const-string v8, "000"

    #@410
    const-string v9, "10"

    #@412
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@415
    aput-object v0, v11, v12

    #@417
    const/16 v12, 0x21

    #@419
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@41b
    const/16 v1, 0x21

    #@41d
    const v2, 0x20901ee

    #@420
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@423
    move-result-object v2

    #@424
    const-string v3, "642"

    #@426
    const-string v4, "257"

    #@428
    const-string v5, "00"

    #@42a
    const-string v6, ""

    #@42c
    const-string v7, "0"

    #@42e
    const-string v8, "000"

    #@430
    const-string v9, "10"

    #@432
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@435
    aput-object v0, v11, v12

    #@437
    const/16 v12, 0x22

    #@439
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@43b
    const/16 v1, 0x22

    #@43d
    const v2, 0x20901ef

    #@440
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@443
    move-result-object v2

    #@444
    const-string v3, "456"

    #@446
    const-string v4, "855"

    #@448
    const-string v5, "001"

    #@44a
    const-string v6, "0"

    #@44c
    const-string v7, "0"

    #@44e
    const-string v8, "000"

    #@450
    const-string v9, "10"

    #@452
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@455
    aput-object v0, v11, v12

    #@457
    const/16 v12, 0x23

    #@459
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@45b
    const/16 v1, 0x23

    #@45d
    const v2, 0x2090181

    #@460
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@463
    move-result-object v2

    #@464
    const-string v3, "624"

    #@466
    const-string v4, "237"

    #@468
    const-string v5, "00"

    #@46a
    const-string v6, ""

    #@46c
    const-string v7, "0"

    #@46e
    const-string v8, "000"

    #@470
    const-string v9, "10"

    #@472
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@475
    aput-object v0, v11, v12

    #@477
    const/16 v12, 0x24

    #@479
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@47b
    const/16 v1, 0x24

    #@47d
    const v2, 0x2090182

    #@480
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@483
    move-result-object v2

    #@484
    const-string v3, "302"

    #@486
    const-string v4, "1"

    #@488
    const-string v5, "011"

    #@48a
    const-string v6, "1"

    #@48c
    const-string v7, "1"

    #@48e
    const-string v8, "000"

    #@490
    const-string v9, "10"

    #@492
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@495
    aput-object v0, v11, v12

    #@497
    const/16 v12, 0x25

    #@499
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@49b
    const/16 v1, 0x25

    #@49d
    const v2, 0x2090183

    #@4a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4a3
    move-result-object v2

    #@4a4
    const-string v3, "625"

    #@4a6
    const-string v4, "238"

    #@4a8
    const-string v5, "0"

    #@4aa
    const-string v6, ""

    #@4ac
    const-string v7, "0"

    #@4ae
    const-string v8, "000"

    #@4b0
    const-string v9, "10"

    #@4b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@4b5
    aput-object v0, v11, v12

    #@4b7
    const/16 v12, 0x26

    #@4b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@4bb
    const/16 v1, 0x26

    #@4bd
    const v2, 0x2090184

    #@4c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4c3
    move-result-object v2

    #@4c4
    const-string v3, "346"

    #@4c6
    const-string v4, "1"

    #@4c8
    const-string v5, "011"

    #@4ca
    const-string v6, "1"

    #@4cc
    const-string v7, "1"

    #@4ce
    const-string v8, "000"

    #@4d0
    const-string v9, "10"

    #@4d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@4d5
    aput-object v0, v11, v12

    #@4d7
    const/16 v12, 0x27

    #@4d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@4db
    const/16 v1, 0x27

    #@4dd
    const v2, 0x2090185

    #@4e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4e3
    move-result-object v2

    #@4e4
    const-string v3, "623"

    #@4e6
    const-string v4, "236"

    #@4e8
    const-string v5, "00"

    #@4ea
    const-string v6, ""

    #@4ec
    const-string v7, "0"

    #@4ee
    const-string v8, "000"

    #@4f0
    const-string v9, "10"

    #@4f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@4f5
    aput-object v0, v11, v12

    #@4f7
    const/16 v12, 0x28

    #@4f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@4fb
    const/16 v1, 0x28

    #@4fd
    const v2, 0x20901f0

    #@500
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@503
    move-result-object v2

    #@504
    const-string v3, "622"

    #@506
    const-string v4, "235"

    #@508
    const-string v5, "15"

    #@50a
    const-string v6, ""

    #@50c
    const-string v7, "0"

    #@50e
    const-string v8, "000"

    #@510
    const-string v9, "10"

    #@512
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@515
    aput-object v0, v11, v12

    #@517
    const/16 v12, 0x29

    #@519
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@51b
    const/16 v1, 0x29

    #@51d
    const v2, 0x2090186

    #@520
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@523
    move-result-object v2

    #@524
    const-string v3, "730"

    #@526
    const-string v4, "56"

    #@528
    const-string v5, "00"

    #@52a
    const-string v6, ""

    #@52c
    const-string v7, "0"

    #@52e
    const-string v8, "000"

    #@530
    const-string v9, "10"

    #@532
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@535
    aput-object v0, v11, v12

    #@537
    const/16 v12, 0x2a

    #@539
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@53b
    const/16 v1, 0x2a

    #@53d
    const v2, 0x2090187

    #@540
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@543
    move-result-object v2

    #@544
    const-string v3, "460"

    #@546
    const-string v4, "86"

    #@548
    const-string v5, "00"

    #@54a
    const-string v6, "0"

    #@54c
    const-string v7, "0"

    #@54e
    const-string v8, "000"

    #@550
    const-string v9, "10"

    #@552
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@555
    aput-object v0, v11, v12

    #@557
    const/16 v12, 0x2b

    #@559
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@55b
    const/16 v1, 0x2b

    #@55d
    const v2, 0x2090188

    #@560
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@563
    move-result-object v2

    #@564
    const-string v3, "732"

    #@566
    const-string v4, "57"

    #@568
    const-string v5, "00"

    #@56a
    const-string v6, ""

    #@56c
    const-string v7, "0"

    #@56e
    const-string v8, "000"

    #@570
    const-string v9, "10"

    #@572
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@575
    aput-object v0, v11, v12

    #@577
    const/16 v12, 0x2c

    #@579
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@57b
    const/16 v1, 0x2c

    #@57d
    const v2, 0x20901f1

    #@580
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@583
    move-result-object v2

    #@584
    const-string v3, "654"

    #@586
    const-string v4, "269"

    #@588
    const-string v5, "00"

    #@58a
    const-string v6, ""

    #@58c
    const-string v7, "0"

    #@58e
    const-string v8, "000"

    #@590
    const-string v9, "10"

    #@592
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@595
    aput-object v0, v11, v12

    #@597
    const/16 v12, 0x2d

    #@599
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@59b
    const/16 v1, 0x2d

    #@59d
    const v2, 0x20901f2

    #@5a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5a3
    move-result-object v2

    #@5a4
    const-string v3, "548"

    #@5a6
    const-string v4, "682"

    #@5a8
    const-string v5, "00"

    #@5aa
    const-string v6, ""

    #@5ac
    const-string v7, "0"

    #@5ae
    const-string v8, "000"

    #@5b0
    const-string v9, "10"

    #@5b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@5b5
    aput-object v0, v11, v12

    #@5b7
    const/16 v12, 0x2e

    #@5b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@5bb
    const/16 v1, 0x2e

    #@5bd
    const v2, 0x20901f3

    #@5c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5c3
    move-result-object v2

    #@5c4
    const-string v3, "712"

    #@5c6
    const-string v4, "506"

    #@5c8
    const-string v5, "00"

    #@5ca
    const-string v6, ""

    #@5cc
    const-string v7, "0"

    #@5ce
    const-string v8, "000"

    #@5d0
    const-string v9, "10"

    #@5d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@5d5
    aput-object v0, v11, v12

    #@5d7
    const/16 v12, 0x2f

    #@5d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@5db
    const/16 v1, 0x2f

    #@5dd
    const v2, 0x2090189

    #@5e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5e3
    move-result-object v2

    #@5e4
    const-string v3, "612"

    #@5e6
    const-string v4, "225"

    #@5e8
    const-string v5, "00"

    #@5ea
    const-string v6, ""

    #@5ec
    const-string v7, "0"

    #@5ee
    const-string v8, "000"

    #@5f0
    const-string v9, "10"

    #@5f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@5f5
    aput-object v0, v11, v12

    #@5f7
    const/16 v12, 0x30

    #@5f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@5fb
    const/16 v1, 0x30

    #@5fd
    const v2, 0x20901f4

    #@600
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@603
    move-result-object v2

    #@604
    const-string v3, "219"

    #@606
    const-string v4, "385"

    #@608
    const-string v5, "00"

    #@60a
    const-string v6, "0"

    #@60c
    const-string v7, "0"

    #@60e
    const-string v8, "000"

    #@610
    const-string v9, "10"

    #@612
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@615
    aput-object v0, v11, v12

    #@617
    const/16 v12, 0x31

    #@619
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@61b
    const/16 v1, 0x31

    #@61d
    const v2, 0x20901f5

    #@620
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@623
    move-result-object v2

    #@624
    const-string v3, "368"

    #@626
    const-string v4, "53"

    #@628
    const-string v5, "119"

    #@62a
    const-string v6, "0"

    #@62c
    const-string v7, "0"

    #@62e
    const-string v8, "000"

    #@630
    const-string v9, "10"

    #@632
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@635
    aput-object v0, v11, v12

    #@637
    const/16 v12, 0x32

    #@639
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@63b
    const/16 v1, 0x32

    #@63d
    const v2, 0x20901f6

    #@640
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@643
    move-result-object v2

    #@644
    const-string v3, "280"

    #@646
    const-string v4, "357"

    #@648
    const-string v5, "00"

    #@64a
    const-string v6, ""

    #@64c
    const-string v7, "0"

    #@64e
    const-string v8, "000"

    #@650
    const-string v9, "10"

    #@652
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@655
    aput-object v0, v11, v12

    #@657
    const/16 v12, 0x33

    #@659
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@65b
    const/16 v1, 0x33

    #@65d
    const v2, 0x209018a

    #@660
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@663
    move-result-object v2

    #@664
    const-string v3, "230"

    #@666
    const-string v4, "420"

    #@668
    const-string v5, "00"

    #@66a
    const-string v6, ""

    #@66c
    const-string v7, "0"

    #@66e
    const-string v8, "000"

    #@670
    const-string v9, "10"

    #@672
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@675
    aput-object v0, v11, v12

    #@677
    const/16 v12, 0x34

    #@679
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@67b
    const/16 v1, 0x34

    #@67d
    const v2, 0x209018b

    #@680
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@683
    move-result-object v2

    #@684
    const-string v3, "630"

    #@686
    const-string v4, "243"

    #@688
    const-string v5, "00"

    #@68a
    const-string v6, "0"

    #@68c
    const-string v7, "0"

    #@68e
    const-string v8, "000"

    #@690
    const-string v9, "10"

    #@692
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@695
    aput-object v0, v11, v12

    #@697
    const/16 v12, 0x35

    #@699
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@69b
    const/16 v1, 0x35

    #@69d
    const v2, 0x209018c

    #@6a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6a3
    move-result-object v2

    #@6a4
    const-string v3, "238"

    #@6a6
    const-string v4, "45"

    #@6a8
    const-string v5, "00"

    #@6aa
    const-string v6, ""

    #@6ac
    const-string v7, "0"

    #@6ae
    const-string v8, "000"

    #@6b0
    const-string v9, "10"

    #@6b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@6b5
    aput-object v0, v11, v12

    #@6b7
    const/16 v12, 0x36

    #@6b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@6bb
    const/16 v1, 0x36

    #@6bd
    const v2, 0x209018d

    #@6c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6c3
    move-result-object v2

    #@6c4
    const-string v3, "638"

    #@6c6
    const-string v4, "253"

    #@6c8
    const-string v5, "00"

    #@6ca
    const-string v6, ""

    #@6cc
    const-string v7, "0"

    #@6ce
    const-string v8, "000"

    #@6d0
    const-string v9, "10"

    #@6d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@6d5
    aput-object v0, v11, v12

    #@6d7
    const/16 v12, 0x37

    #@6d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@6db
    const/16 v1, 0x37

    #@6dd
    const v2, 0x20901f7

    #@6e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6e3
    move-result-object v2

    #@6e4
    const-string v3, "366"

    #@6e6
    const-string v4, "1"

    #@6e8
    const-string v5, "011"

    #@6ea
    const-string v6, "1"

    #@6ec
    const-string v7, "1"

    #@6ee
    const-string v8, "000"

    #@6f0
    const-string v9, "10"

    #@6f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@6f5
    aput-object v0, v11, v12

    #@6f7
    const/16 v12, 0x38

    #@6f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@6fb
    const/16 v1, 0x38

    #@6fd
    const v2, 0x209018e

    #@700
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@703
    move-result-object v2

    #@704
    const-string v3, "370"

    #@706
    const-string v4, "1"

    #@708
    const-string v5, "011"

    #@70a
    const-string v6, "1"

    #@70c
    const-string v7, "1"

    #@70e
    const-string v8, "000"

    #@710
    const-string v9, "10"

    #@712
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@715
    aput-object v0, v11, v12

    #@717
    const/16 v12, 0x39

    #@719
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@71b
    const/16 v1, 0x39

    #@71d
    const v2, 0x20901f8

    #@720
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@723
    move-result-object v2

    #@724
    const-string v3, "514"

    #@726
    const-string v4, "670"

    #@728
    const-string v5, "00"

    #@72a
    const-string v6, ""

    #@72c
    const-string v7, "0"

    #@72e
    const-string v8, "000"

    #@730
    const-string v9, "10"

    #@732
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@735
    aput-object v0, v11, v12

    #@737
    const/16 v12, 0x3a

    #@739
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@73b
    const/16 v1, 0x3a

    #@73d
    const v2, 0x209018f

    #@740
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@743
    move-result-object v2

    #@744
    const-string v3, "740"

    #@746
    const-string v4, "593"

    #@748
    const-string v5, "00"

    #@74a
    const-string v6, "0"

    #@74c
    const-string v7, "0"

    #@74e
    const-string v8, "000"

    #@750
    const-string v9, "10"

    #@752
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@755
    aput-object v0, v11, v12

    #@757
    const/16 v12, 0x3b

    #@759
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@75b
    const/16 v1, 0x3b

    #@75d
    const v2, 0x2090190

    #@760
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@763
    move-result-object v2

    #@764
    const-string v3, "602"

    #@766
    const-string v4, "20"

    #@768
    const-string v5, "00"

    #@76a
    const-string v6, "0"

    #@76c
    const-string v7, "0"

    #@76e
    const-string v8, "000"

    #@770
    const-string v9, "10"

    #@772
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@775
    aput-object v0, v11, v12

    #@777
    const/16 v12, 0x3c

    #@779
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@77b
    const/16 v1, 0x3c

    #@77d
    const v2, 0x2090191

    #@780
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@783
    move-result-object v2

    #@784
    const-string v3, "706"

    #@786
    const-string v4, "503"

    #@788
    const-string v5, "00"

    #@78a
    const-string v6, ""

    #@78c
    const-string v7, "0"

    #@78e
    const-string v8, "000"

    #@790
    const-string v9, "10"

    #@792
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@795
    aput-object v0, v11, v12

    #@797
    const/16 v12, 0x3d

    #@799
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@79b
    const/16 v1, 0x3d

    #@79d
    const v2, 0x20901f9

    #@7a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@7a3
    move-result-object v2

    #@7a4
    const-string v3, "627"

    #@7a6
    const-string v4, "240"

    #@7a8
    const-string v5, "00"

    #@7aa
    const-string v6, ""

    #@7ac
    const-string v7, "0"

    #@7ae
    const-string v8, "000"

    #@7b0
    const-string v9, "10"

    #@7b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@7b5
    aput-object v0, v11, v12

    #@7b7
    const/16 v12, 0x3e

    #@7b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@7bb
    const/16 v1, 0x3e

    #@7bd
    const v2, 0x20901fa

    #@7c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@7c3
    move-result-object v2

    #@7c4
    const-string v3, "657"

    #@7c6
    const-string v4, "291"

    #@7c8
    const-string v5, "00"

    #@7ca
    const-string v6, "00"

    #@7cc
    const-string v7, "0"

    #@7ce
    const-string v8, "000"

    #@7d0
    const-string v9, "10"

    #@7d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@7d5
    aput-object v0, v11, v12

    #@7d7
    const/16 v12, 0x3f

    #@7d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@7db
    const/16 v1, 0x3f

    #@7dd
    const v2, 0x2090192

    #@7e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@7e3
    move-result-object v2

    #@7e4
    const-string v3, "248"

    #@7e6
    const-string v4, "372"

    #@7e8
    const-string v5, "00"

    #@7ea
    const-string v6, ""

    #@7ec
    const-string v7, "0"

    #@7ee
    const-string v8, "000"

    #@7f0
    const-string v9, "10"

    #@7f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@7f5
    aput-object v0, v11, v12

    #@7f7
    const/16 v12, 0x40

    #@7f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@7fb
    const/16 v1, 0x40

    #@7fd
    const v2, 0x2090193

    #@800
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@803
    move-result-object v2

    #@804
    const-string v3, "636"

    #@806
    const-string v4, "251"

    #@808
    const-string v5, "00"

    #@80a
    const-string v6, "0"

    #@80c
    const-string v7, "0"

    #@80e
    const-string v8, "000"

    #@810
    const-string v9, "10"

    #@812
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@815
    aput-object v0, v11, v12

    #@817
    const/16 v12, 0x41

    #@819
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@81b
    const/16 v1, 0x41

    #@81d
    const v2, 0x20901fb

    #@820
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@823
    move-result-object v2

    #@824
    const-string v3, "288"

    #@826
    const-string v4, "298"

    #@828
    const-string v5, "00"

    #@82a
    const-string v6, ""

    #@82c
    const-string v7, "0"

    #@82e
    const-string v8, "000"

    #@830
    const-string v9, "10"

    #@832
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@835
    aput-object v0, v11, v12

    #@837
    const/16 v12, 0x42

    #@839
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@83b
    const/16 v1, 0x42

    #@83d
    const v2, 0x20901fc

    #@840
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@843
    move-result-object v2

    #@844
    const-string v3, "550"

    #@846
    const-string v4, "691"

    #@848
    const-string v5, "011"

    #@84a
    const-string v6, "1"

    #@84c
    const-string v7, "0"

    #@84e
    const-string v8, "000"

    #@850
    const-string v9, "10"

    #@852
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@855
    aput-object v0, v11, v12

    #@857
    const/16 v12, 0x43

    #@859
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@85b
    const/16 v1, 0x43

    #@85d
    const v2, 0x2090194

    #@860
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@863
    move-result-object v2

    #@864
    const-string v3, "542"

    #@866
    const-string v4, "679"

    #@868
    const-string v5, "00"

    #@86a
    const-string v6, ""

    #@86c
    const-string v7, "0"

    #@86e
    const-string v8, "000"

    #@870
    const-string v9, "10"

    #@872
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@875
    aput-object v0, v11, v12

    #@877
    const/16 v12, 0x44

    #@879
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@87b
    const/16 v1, 0x44

    #@87d
    const v2, 0x20901fd

    #@880
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@883
    move-result-object v2

    #@884
    const-string v3, "244"

    #@886
    const-string v4, "358"

    #@888
    const-string v5, "00"

    #@88a
    const-string v6, "0"

    #@88c
    const-string v7, "0"

    #@88e
    const-string v8, "000"

    #@890
    const-string v9, "10"

    #@892
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@895
    aput-object v0, v11, v12

    #@897
    const/16 v12, 0x45

    #@899
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@89b
    const/16 v1, 0x45

    #@89d
    const v2, 0x20901fe

    #@8a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@8a3
    move-result-object v2

    #@8a4
    const-string v3, "208"

    #@8a6
    const-string v4, "33"

    #@8a8
    const-string v5, "00"

    #@8aa
    const-string v6, "0"

    #@8ac
    const-string v7, "0"

    #@8ae
    const-string v8, "000"

    #@8b0
    const-string v9, "10"

    #@8b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8b5
    aput-object v0, v11, v12

    #@8b7
    const/16 v12, 0x46

    #@8b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@8bb
    const/16 v1, 0x46

    #@8bd
    const v2, 0x20901ff

    #@8c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@8c3
    move-result-object v2

    #@8c4
    const-string v3, "742"

    #@8c6
    const-string v4, "594"

    #@8c8
    const-string v5, "00"

    #@8ca
    const-string v6, ""

    #@8cc
    const-string v7, "0"

    #@8ce
    const-string v8, "000"

    #@8d0
    const-string v9, "10"

    #@8d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8d5
    aput-object v0, v11, v12

    #@8d7
    const/16 v12, 0x47

    #@8d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@8db
    const/16 v1, 0x47

    #@8dd
    const v2, 0x2090200

    #@8e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@8e3
    move-result-object v2

    #@8e4
    const-string v3, "547"

    #@8e6
    const-string v4, "689"

    #@8e8
    const-string v5, "00"

    #@8ea
    const-string v6, ""

    #@8ec
    const-string v7, "0"

    #@8ee
    const-string v8, "000"

    #@8f0
    const-string v9, "10"

    #@8f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8f5
    aput-object v0, v11, v12

    #@8f7
    const/16 v12, 0x48

    #@8f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@8fb
    const/16 v1, 0x48

    #@8fd
    const v2, 0x2090201

    #@900
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@903
    move-result-object v2

    #@904
    const-string v3, "628"

    #@906
    const-string v4, "241"

    #@908
    const-string v5, "00"

    #@90a
    const-string v6, ""

    #@90c
    const-string v7, "0"

    #@90e
    const-string v8, "000"

    #@910
    const-string v9, "10"

    #@912
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@915
    aput-object v0, v11, v12

    #@917
    const/16 v12, 0x49

    #@919
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@91b
    const/16 v1, 0x49

    #@91d
    const v2, 0x2090195

    #@920
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@923
    move-result-object v2

    #@924
    const-string v3, "607"

    #@926
    const-string v4, "220"

    #@928
    const-string v5, "00"

    #@92a
    const-string v6, ""

    #@92c
    const-string v7, "0"

    #@92e
    const-string v8, "000"

    #@930
    const-string v9, "10"

    #@932
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@935
    aput-object v0, v11, v12

    #@937
    const/16 v12, 0x4a

    #@939
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@93b
    const/16 v1, 0x4a

    #@93d
    const v2, 0x2090202

    #@940
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@943
    move-result-object v2

    #@944
    const-string v3, "282"

    #@946
    const-string v4, "995"

    #@948
    const-string v5, "810"

    #@94a
    const-string v6, "8"

    #@94c
    const-string v7, "0"

    #@94e
    const-string v8, "000"

    #@950
    const-string v9, "10"

    #@952
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@955
    aput-object v0, v11, v12

    #@957
    const/16 v12, 0x4b

    #@959
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@95b
    const/16 v1, 0x4b

    #@95d
    const v2, 0x2090203

    #@960
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@963
    move-result-object v2

    #@964
    const-string v3, "262"

    #@966
    const-string v4, "49"

    #@968
    const-string v5, "00"

    #@96a
    const-string v6, "0"

    #@96c
    const-string v7, "0"

    #@96e
    const-string v8, "000"

    #@970
    const-string v9, "10"

    #@972
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@975
    aput-object v0, v11, v12

    #@977
    const/16 v12, 0x4c

    #@979
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@97b
    const/16 v1, 0x4c

    #@97d
    const v2, 0x2090196

    #@980
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@983
    move-result-object v2

    #@984
    const-string v3, "620"

    #@986
    const-string v4, "233"

    #@988
    const-string v5, "00"

    #@98a
    const-string v6, "0"

    #@98c
    const-string v7, "0"

    #@98e
    const-string v8, "000"

    #@990
    const-string v9, "10"

    #@992
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@995
    aput-object v0, v11, v12

    #@997
    const/16 v12, 0x4d

    #@999
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@99b
    const/16 v1, 0x4d

    #@99d
    const v2, 0x2090204

    #@9a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@9a3
    move-result-object v2

    #@9a4
    const-string v3, "266"

    #@9a6
    const-string v4, "350"

    #@9a8
    const-string v5, "00"

    #@9aa
    const-string v6, ""

    #@9ac
    const-string v7, "0"

    #@9ae
    const-string v8, "000"

    #@9b0
    const-string v9, "10"

    #@9b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@9b5
    aput-object v0, v11, v12

    #@9b7
    const/16 v12, 0x4e

    #@9b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@9bb
    const/16 v1, 0x4e

    #@9bd
    const v2, 0x2090205

    #@9c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@9c3
    move-result-object v2

    #@9c4
    const-string v3, "202"

    #@9c6
    const-string v4, "30"

    #@9c8
    const-string v5, "00"

    #@9ca
    const-string v6, "0"

    #@9cc
    const-string v7, "0"

    #@9ce
    const-string v8, "000"

    #@9d0
    const-string v9, "10"

    #@9d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@9d5
    aput-object v0, v11, v12

    #@9d7
    const/16 v12, 0x4f

    #@9d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@9db
    const/16 v1, 0x4f

    #@9dd
    const v2, 0x2090206

    #@9e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@9e3
    move-result-object v2

    #@9e4
    const-string v3, "290"

    #@9e6
    const-string v4, "299"

    #@9e8
    const-string v5, "00"

    #@9ea
    const-string v6, ""

    #@9ec
    const-string v7, "0"

    #@9ee
    const-string v8, "000"

    #@9f0
    const-string v9, "10"

    #@9f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@9f5
    aput-object v0, v11, v12

    #@9f7
    const/16 v12, 0x50

    #@9f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@9fb
    const/16 v1, 0x50

    #@9fd
    const v2, 0x2090207

    #@a00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a03
    move-result-object v2

    #@a04
    const-string v3, "352"

    #@a06
    const-string v4, "1"

    #@a08
    const-string v5, "011"

    #@a0a
    const-string v6, "1"

    #@a0c
    const-string v7, "1"

    #@a0e
    const-string v8, "000"

    #@a10
    const-string v9, "10"

    #@a12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a15
    aput-object v0, v11, v12

    #@a17
    const/16 v12, 0x51

    #@a19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@a1b
    const/16 v1, 0x51

    #@a1d
    const v2, 0x2090208

    #@a20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a23
    move-result-object v2

    #@a24
    const-string v3, "340"

    #@a26
    const-string v4, "590"

    #@a28
    const-string v5, "00"

    #@a2a
    const-string v6, ""

    #@a2c
    const-string v7, "0"

    #@a2e
    const-string v8, "000"

    #@a30
    const-string v9, "10"

    #@a32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a35
    aput-object v0, v11, v12

    #@a37
    const/16 v12, 0x52

    #@a39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@a3b
    const/16 v1, 0x52

    #@a3d
    const v2, 0x2090197

    #@a40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a43
    move-result-object v2

    #@a44
    const-string v3, "535"

    #@a46
    const-string v4, "1"

    #@a48
    const-string v5, "011"

    #@a4a
    const-string v6, "1"

    #@a4c
    const-string v7, "1"

    #@a4e
    const-string v8, "000"

    #@a50
    const-string v9, "10"

    #@a52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a55
    aput-object v0, v11, v12

    #@a57
    const/16 v12, 0x53

    #@a59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@a5b
    const/16 v1, 0x53

    #@a5d
    const v2, 0x2090198

    #@a60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a63
    move-result-object v2

    #@a64
    const-string v3, "704"

    #@a66
    const-string v4, "502"

    #@a68
    const-string v5, "00"

    #@a6a
    const-string v6, ""

    #@a6c
    const-string v7, "0"

    #@a6e
    const-string v8, "000"

    #@a70
    const-string v9, "10"

    #@a72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a75
    aput-object v0, v11, v12

    #@a77
    const/16 v12, 0x54

    #@a79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@a7b
    const/16 v1, 0x54

    #@a7d
    const v2, 0x2090209

    #@a80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@a83
    move-result-object v2

    #@a84
    const-string v3, "611"

    #@a86
    const-string v4, "224"

    #@a88
    const-string v5, "00"

    #@a8a
    const-string v6, ""

    #@a8c
    const-string v7, "0"

    #@a8e
    const-string v8, "000"

    #@a90
    const-string v9, "10"

    #@a92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@a95
    aput-object v0, v11, v12

    #@a97
    const/16 v12, 0x55

    #@a99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@a9b
    const/16 v1, 0x55

    #@a9d
    const v2, 0x209020a

    #@aa0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@aa3
    move-result-object v2

    #@aa4
    const-string v3, "632"

    #@aa6
    const-string v4, "245"

    #@aa8
    const-string v5, "00"

    #@aaa
    const-string v6, ""

    #@aac
    const-string v7, "0"

    #@aae
    const-string v8, "000"

    #@ab0
    const-string v9, "10"

    #@ab2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@ab5
    aput-object v0, v11, v12

    #@ab7
    const/16 v12, 0x56

    #@ab9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@abb
    const/16 v1, 0x56

    #@abd
    const v2, 0x209020b

    #@ac0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ac3
    move-result-object v2

    #@ac4
    const-string v3, "738"

    #@ac6
    const-string v4, "592"

    #@ac8
    const-string v5, "001"

    #@aca
    const-string v6, ""

    #@acc
    const-string v7, "0"

    #@ace
    const-string v8, "000"

    #@ad0
    const-string v9, "10"

    #@ad2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@ad5
    aput-object v0, v11, v12

    #@ad7
    const/16 v12, 0x57

    #@ad9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@adb
    const/16 v1, 0x57

    #@add
    const v2, 0x2090199

    #@ae0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ae3
    move-result-object v2

    #@ae4
    const-string v3, "372"

    #@ae6
    const-string v4, "509"

    #@ae8
    const-string v5, "00"

    #@aea
    const-string v6, ""

    #@aec
    const-string v7, "0"

    #@aee
    const-string v8, "000"

    #@af0
    const-string v9, "10"

    #@af2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@af5
    aput-object v0, v11, v12

    #@af7
    const/16 v12, 0x58

    #@af9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@afb
    const/16 v1, 0x58

    #@afd
    const v2, 0x209019a

    #@b00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@b03
    move-result-object v2

    #@b04
    const-string v3, "708"

    #@b06
    const-string v4, "504"

    #@b08
    const-string v5, "00"

    #@b0a
    const-string v6, ""

    #@b0c
    const-string v7, "0"

    #@b0e
    const-string v8, "000"

    #@b10
    const-string v9, "10"

    #@b12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b15
    aput-object v0, v11, v12

    #@b17
    const/16 v12, 0x59

    #@b19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@b1b
    const/16 v1, 0x59

    #@b1d
    const v2, 0x209019b

    #@b20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@b23
    move-result-object v2

    #@b24
    const-string v3, "454"

    #@b26
    const-string v4, "852"

    #@b28
    const-string v5, "001"

    #@b2a
    const-string v6, ""

    #@b2c
    const-string v7, "0"

    #@b2e
    const-string v8, "000"

    #@b30
    const-string v9, "10"

    #@b32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b35
    aput-object v0, v11, v12

    #@b37
    const/16 v12, 0x5a

    #@b39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@b3b
    const/16 v1, 0x5a

    #@b3d
    const v2, 0x209019c

    #@b40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@b43
    move-result-object v2

    #@b44
    const-string v3, "216"

    #@b46
    const-string v4, "36"

    #@b48
    const-string v5, "00"

    #@b4a
    const-string v6, "06"

    #@b4c
    const-string v7, "0"

    #@b4e
    const-string v8, "000"

    #@b50
    const-string v9, "10"

    #@b52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b55
    aput-object v0, v11, v12

    #@b57
    const/16 v12, 0x5b

    #@b59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@b5b
    const/16 v1, 0x5b

    #@b5d
    const v2, 0x209019d

    #@b60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@b63
    move-result-object v2

    #@b64
    const-string v3, "274"

    #@b66
    const-string v4, "354"

    #@b68
    const-string v5, "00"

    #@b6a
    const-string v6, ""

    #@b6c
    const-string v7, "0"

    #@b6e
    const-string v8, "000"

    #@b70
    const-string v9, "10"

    #@b72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b75
    aput-object v0, v11, v12

    #@b77
    const/16 v12, 0x5c

    #@b79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@b7b
    const/16 v1, 0x5c

    #@b7d
    const v2, 0x209019e

    #@b80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@b83
    move-result-object v2

    #@b84
    const-string v3, "404,405"

    #@b86
    const-string v4, "91"

    #@b88
    const-string v5, "00"

    #@b8a
    const-string v6, "0"

    #@b8c
    const-string v7, "0"

    #@b8e
    const-string v8, "000"

    #@b90
    const-string v9, "10"

    #@b92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b95
    aput-object v0, v11, v12

    #@b97
    const/16 v12, 0x5d

    #@b99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@b9b
    const/16 v1, 0x5d

    #@b9d
    const v2, 0x209019f

    #@ba0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ba3
    move-result-object v2

    #@ba4
    const-string v3, "510"

    #@ba6
    const-string v4, "62"

    #@ba8
    const-string v5, "001"

    #@baa
    const-string v6, "0"

    #@bac
    const-string v7, "0"

    #@bae
    const-string v8, "000"

    #@bb0
    const-string v9, "10"

    #@bb2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@bb5
    aput-object v0, v11, v12

    #@bb7
    const/16 v12, 0x5e

    #@bb9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@bbb
    const/16 v1, 0x5e

    #@bbd
    const v2, 0x209020c

    #@bc0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@bc3
    move-result-object v2

    #@bc4
    const-string v3, "432"

    #@bc6
    const-string v4, "98"

    #@bc8
    const-string v5, "00"

    #@bca
    const-string v6, "0"

    #@bcc
    const-string v7, "0"

    #@bce
    const-string v8, "000"

    #@bd0
    const-string v9, "10"

    #@bd2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@bd5
    aput-object v0, v11, v12

    #@bd7
    const/16 v12, 0x5f

    #@bd9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@bdb
    const/16 v1, 0x5f

    #@bdd
    const v2, 0x20901a0

    #@be0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@be3
    move-result-object v2

    #@be4
    const-string v3, "418"

    #@be6
    const-string v4, "964"

    #@be8
    const-string v5, "00"

    #@bea
    const-string v6, "0"

    #@bec
    const-string v7, "0"

    #@bee
    const-string v8, "000"

    #@bf0
    const-string v9, "10"

    #@bf2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@bf5
    aput-object v0, v11, v12

    #@bf7
    const/16 v12, 0x60

    #@bf9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@bfb
    const/16 v1, 0x60

    #@bfd
    const v2, 0x20901a1

    #@c00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@c03
    move-result-object v2

    #@c04
    const-string v3, "272"

    #@c06
    const-string v4, "353"

    #@c08
    const-string v5, "00"

    #@c0a
    const-string v6, "0"

    #@c0c
    const-string v7, "0"

    #@c0e
    const-string v8, "000"

    #@c10
    const-string v9, "10"

    #@c12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@c15
    aput-object v0, v11, v12

    #@c17
    const/16 v12, 0x61

    #@c19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@c1b
    const/16 v1, 0x61

    #@c1d
    const v2, 0x20901a2

    #@c20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@c23
    move-result-object v2

    #@c24
    const-string v3, "425"

    #@c26
    const-string v4, "972"

    #@c28
    const-string v5, "00"

    #@c2a
    const-string v6, "0"

    #@c2c
    const-string v7, "0"

    #@c2e
    const-string v8, "000"

    #@c30
    const-string v9, "10"

    #@c32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@c35
    aput-object v0, v11, v12

    #@c37
    const/16 v12, 0x62

    #@c39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@c3b
    const/16 v1, 0x62

    #@c3d
    const v2, 0x209020d

    #@c40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@c43
    move-result-object v2

    #@c44
    const-string v3, "222"

    #@c46
    const-string v4, "39"

    #@c48
    const-string v5, "00"

    #@c4a
    const-string v6, ""

    #@c4c
    const-string v7, "0"

    #@c4e
    const-string v8, "000"

    #@c50
    const-string v9, "10"

    #@c52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@c55
    aput-object v0, v11, v12

    #@c57
    const/16 v12, 0x63

    #@c59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@c5b
    const/16 v1, 0x63

    #@c5d
    const v2, 0x20901a3

    #@c60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@c63
    move-result-object v2

    #@c64
    const-string v3, "338"

    #@c66
    const-string v4, "1"

    #@c68
    const-string v5, "011"

    #@c6a
    const-string v6, "1"

    #@c6c
    const-string v7, "1"

    #@c6e
    const-string v8, "000"

    #@c70
    const-string v9, "10"

    #@c72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@c75
    aput-object v0, v11, v12

    #@c77
    const/16 v12, 0x64

    #@c79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@c7b
    const/16 v1, 0x64

    #@c7d
    const v2, 0x20901a4

    #@c80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@c83
    move-result-object v2

    #@c84
    const-string v3, "440,441"

    #@c86
    const-string v4, "81"

    #@c88
    const-string v5, "010"

    #@c8a
    const-string v6, "0"

    #@c8c
    const-string v7, "0"

    #@c8e
    const-string v8, "000"

    #@c90
    const-string v9, "10"

    #@c92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@c95
    aput-object v0, v11, v12

    #@c97
    const/16 v12, 0x65

    #@c99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@c9b
    const/16 v1, 0x65

    #@c9d
    const v2, 0x20901a5

    #@ca0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ca3
    move-result-object v2

    #@ca4
    const-string v3, "416"

    #@ca6
    const-string v4, "962"

    #@ca8
    const-string v5, "00"

    #@caa
    const-string v6, "0"

    #@cac
    const-string v7, "0"

    #@cae
    const-string v8, "000"

    #@cb0
    const-string v9, "10"

    #@cb2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@cb5
    aput-object v0, v11, v12

    #@cb7
    const/16 v12, 0x66

    #@cb9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@cbb
    const/16 v1, 0x66

    #@cbd
    const v2, 0x209020e

    #@cc0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@cc3
    move-result-object v2

    #@cc4
    const-string v3, "401"

    #@cc6
    const-string v4, "7"

    #@cc8
    const-string v5, "810"

    #@cca
    const-string v6, "8"

    #@ccc
    const-string v7, "0"

    #@cce
    const-string v8, "000"

    #@cd0
    const-string v9, "10"

    #@cd2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@cd5
    aput-object v0, v11, v12

    #@cd7
    const/16 v12, 0x67

    #@cd9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@cdb
    const/16 v1, 0x67

    #@cdd
    const v2, 0x20901a6

    #@ce0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ce3
    move-result-object v2

    #@ce4
    const-string v3, "639"

    #@ce6
    const-string v4, "254"

    #@ce8
    const-string v5, "00"

    #@cea
    const-string v6, "0"

    #@cec
    const-string v7, "0"

    #@cee
    const-string v8, "000"

    #@cf0
    const-string v9, "10"

    #@cf2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@cf5
    aput-object v0, v11, v12

    #@cf7
    const/16 v12, 0x68

    #@cf9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@cfb
    const/16 v1, 0x68

    #@cfd
    const v2, 0x209020f

    #@d00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d03
    move-result-object v2

    #@d04
    const-string v3, "545"

    #@d06
    const-string v4, "686"

    #@d08
    const-string v5, "00"

    #@d0a
    const-string v6, ""

    #@d0c
    const-string v7, "0"

    #@d0e
    const-string v8, "000"

    #@d10
    const-string v9, "10"

    #@d12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d15
    aput-object v0, v11, v12

    #@d17
    const/16 v12, 0x69

    #@d19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@d1b
    const/16 v1, 0x69

    #@d1d
    const v2, 0x20901a7

    #@d20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d23
    move-result-object v2

    #@d24
    const-string v3, "467"

    #@d26
    const-string v4, "850"

    #@d28
    const-string v5, "00"

    #@d2a
    const-string v6, "0"

    #@d2c
    const-string v7, "0"

    #@d2e
    const-string v8, "000"

    #@d30
    const-string v9, "10"

    #@d32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d35
    aput-object v0, v11, v12

    #@d37
    const/16 v12, 0x6a

    #@d39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@d3b
    const/16 v1, 0x6a

    #@d3d
    const v2, 0x20901a8

    #@d40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d43
    move-result-object v2

    #@d44
    const-string v3, "450"

    #@d46
    const-string v4, "82"

    #@d48
    const-string v5, "00700"

    #@d4a
    const-string v6, "0"

    #@d4c
    const-string v7, "0"

    #@d4e
    const-string v8, "000"

    #@d50
    const-string v9, "10"

    #@d52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d55
    aput-object v0, v11, v12

    #@d57
    const/16 v12, 0x6b

    #@d59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@d5b
    const/16 v1, 0x6b

    #@d5d
    const v2, 0x20901a9

    #@d60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d63
    move-result-object v2

    #@d64
    const-string v3, "419"

    #@d66
    const-string v4, "965"

    #@d68
    const-string v5, "00"

    #@d6a
    const-string v6, ""

    #@d6c
    const-string v7, "0"

    #@d6e
    const-string v8, "000"

    #@d70
    const-string v9, "10"

    #@d72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d75
    aput-object v0, v11, v12

    #@d77
    const/16 v12, 0x6c

    #@d79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@d7b
    const/16 v1, 0x6c

    #@d7d
    const v2, 0x2090210

    #@d80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@d83
    move-result-object v2

    #@d84
    const-string v3, "437"

    #@d86
    const-string v4, "996"

    #@d88
    const-string v5, "00"

    #@d8a
    const-string v6, "0"

    #@d8c
    const-string v7, "0"

    #@d8e
    const-string v8, "000"

    #@d90
    const-string v9, "10"

    #@d92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@d95
    aput-object v0, v11, v12

    #@d97
    const/16 v12, 0x6d

    #@d99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@d9b
    const/16 v1, 0x6d

    #@d9d
    const v2, 0x20901aa

    #@da0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@da3
    move-result-object v2

    #@da4
    const-string v3, "457"

    #@da6
    const-string v4, "856"

    #@da8
    const-string v5, "00"

    #@daa
    const-string v6, "0"

    #@dac
    const-string v7, "0"

    #@dae
    const-string v8, "000"

    #@db0
    const-string v9, "10"

    #@db2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@db5
    aput-object v0, v11, v12

    #@db7
    const/16 v12, 0x6e

    #@db9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@dbb
    const/16 v1, 0x6e

    #@dbd
    const v2, 0x20901ab

    #@dc0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@dc3
    move-result-object v2

    #@dc4
    const-string v3, "247"

    #@dc6
    const-string v4, "371"

    #@dc8
    const-string v5, "00"

    #@dca
    const-string v6, ""

    #@dcc
    const-string v7, "0"

    #@dce
    const-string v8, "000"

    #@dd0
    const-string v9, "10"

    #@dd2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@dd5
    aput-object v0, v11, v12

    #@dd7
    const/16 v12, 0x6f

    #@dd9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@ddb
    const/16 v1, 0x6f

    #@ddd
    const v2, 0x2090211

    #@de0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@de3
    move-result-object v2

    #@de4
    const-string v3, "415"

    #@de6
    const-string v4, "961"

    #@de8
    const-string v5, "00"

    #@dea
    const-string v6, "0"

    #@dec
    const-string v7, "0"

    #@dee
    const-string v8, "000"

    #@df0
    const-string v9, "10"

    #@df2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@df5
    aput-object v0, v11, v12

    #@df7
    const/16 v12, 0x70

    #@df9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@dfb
    const/16 v1, 0x70

    #@dfd
    const v2, 0x2090212

    #@e00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e03
    move-result-object v2

    #@e04
    const-string v3, "651"

    #@e06
    const-string v4, "266"

    #@e08
    const-string v5, "00"

    #@e0a
    const-string v6, ""

    #@e0c
    const-string v7, "0"

    #@e0e
    const-string v8, "000"

    #@e10
    const-string v9, "10"

    #@e12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@e15
    aput-object v0, v11, v12

    #@e17
    const/16 v12, 0x71

    #@e19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@e1b
    const/16 v1, 0x71

    #@e1d
    const v2, 0x2090213

    #@e20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e23
    move-result-object v2

    #@e24
    const-string v3, "618"

    #@e26
    const-string v4, "231"

    #@e28
    const-string v5, "00"

    #@e2a
    const-string v6, ""

    #@e2c
    const-string v7, "0"

    #@e2e
    const-string v8, "000"

    #@e30
    const-string v9, "10"

    #@e32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@e35
    aput-object v0, v11, v12

    #@e37
    const/16 v12, 0x72

    #@e39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@e3b
    const/16 v1, 0x72

    #@e3d
    const v2, 0x2090214

    #@e40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e43
    move-result-object v2

    #@e44
    const-string v3, "606"

    #@e46
    const-string v4, "218"

    #@e48
    const-string v5, "00"

    #@e4a
    const-string v6, "0"

    #@e4c
    const-string v7, "0"

    #@e4e
    const-string v8, "000"

    #@e50
    const-string v9, "10"

    #@e52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@e55
    aput-object v0, v11, v12

    #@e57
    const/16 v12, 0x73

    #@e59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@e5b
    const/16 v1, 0x73

    #@e5d
    const v2, 0x2090215

    #@e60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e63
    move-result-object v2

    #@e64
    const-string v3, "295"

    #@e66
    const-string v4, "423"

    #@e68
    const-string v5, "00"

    #@e6a
    const-string v6, ""

    #@e6c
    const-string v7, "0"

    #@e6e
    const-string v8, "000"

    #@e70
    const-string v9, "10"

    #@e72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@e75
    aput-object v0, v11, v12

    #@e77
    const/16 v12, 0x74

    #@e79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@e7b
    const/16 v1, 0x74

    #@e7d
    const v2, 0x2090216

    #@e80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@e83
    move-result-object v2

    #@e84
    const-string v3, "246"

    #@e86
    const-string v4, "370"

    #@e88
    const-string v5, "00"

    #@e8a
    const-string v6, "0"

    #@e8c
    const-string v7, "0"

    #@e8e
    const-string v8, "000"

    #@e90
    const-string v9, "10"

    #@e92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@e95
    aput-object v0, v11, v12

    #@e97
    const/16 v12, 0x75

    #@e99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@e9b
    const/16 v1, 0x75

    #@e9d
    const v2, 0x2090217

    #@ea0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ea3
    move-result-object v2

    #@ea4
    const-string v3, "270"

    #@ea6
    const-string v4, "352"

    #@ea8
    const-string v5, "00"

    #@eaa
    const-string v6, ""

    #@eac
    const-string v7, "0"

    #@eae
    const-string v8, "000"

    #@eb0
    const-string v9, "10"

    #@eb2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@eb5
    aput-object v0, v11, v12

    #@eb7
    const/16 v12, 0x76

    #@eb9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@ebb
    const/16 v1, 0x76

    #@ebd
    const v2, 0x2090218

    #@ec0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ec3
    move-result-object v2

    #@ec4
    const-string v3, "455"

    #@ec6
    const-string v4, "853"

    #@ec8
    const-string v5, "00"

    #@eca
    const-string v6, ""

    #@ecc
    const-string v7, "0"

    #@ece
    const-string v8, "000"

    #@ed0
    const-string v9, "10"

    #@ed2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@ed5
    aput-object v0, v11, v12

    #@ed7
    const/16 v12, 0x77

    #@ed9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@edb
    const/16 v1, 0x77

    #@edd
    const v2, 0x20901ac

    #@ee0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ee3
    move-result-object v2

    #@ee4
    const-string v3, "646"

    #@ee6
    const-string v4, "261"

    #@ee8
    const-string v5, "00"

    #@eea
    const-string v6, ""

    #@eec
    const-string v7, "0"

    #@eee
    const-string v8, "000"

    #@ef0
    const-string v9, "10"

    #@ef2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@ef5
    aput-object v0, v11, v12

    #@ef7
    const/16 v12, 0x78

    #@ef9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@efb
    const/16 v1, 0x78

    #@efd
    const v2, 0x2090219

    #@f00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f03
    move-result-object v2

    #@f04
    const-string v3, "650"

    #@f06
    const-string v4, "265"

    #@f08
    const-string v5, "00"

    #@f0a
    const-string v6, ""

    #@f0c
    const-string v7, "0"

    #@f0e
    const-string v8, "000"

    #@f10
    const-string v9, "10"

    #@f12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@f15
    aput-object v0, v11, v12

    #@f17
    const/16 v12, 0x79

    #@f19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@f1b
    const/16 v1, 0x79

    #@f1d
    const v2, 0x20901ad

    #@f20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f23
    move-result-object v2

    #@f24
    const-string v3, "502"

    #@f26
    const-string v4, "60"

    #@f28
    const-string v5, "00"

    #@f2a
    const-string v6, "0"

    #@f2c
    const-string v7, "0"

    #@f2e
    const-string v8, "000"

    #@f30
    const-string v9, "10"

    #@f32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@f35
    aput-object v0, v11, v12

    #@f37
    const/16 v12, 0x7a

    #@f39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@f3b
    const/16 v1, 0x7a

    #@f3d
    const v2, 0x209021a

    #@f40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f43
    move-result-object v2

    #@f44
    const-string v3, "472"

    #@f46
    const-string v4, "960"

    #@f48
    const-string v5, "00"

    #@f4a
    const-string v6, ""

    #@f4c
    const-string v7, "0"

    #@f4e
    const-string v8, "000"

    #@f50
    const-string v9, "10"

    #@f52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@f55
    aput-object v0, v11, v12

    #@f57
    const/16 v12, 0x7b

    #@f59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@f5b
    const/16 v1, 0x7b

    #@f5d
    const v2, 0x20901ae

    #@f60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f63
    move-result-object v2

    #@f64
    const-string v3, "610"

    #@f66
    const-string v4, "223"

    #@f68
    const-string v5, "00"

    #@f6a
    const-string v6, ""

    #@f6c
    const-string v7, "0"

    #@f6e
    const-string v8, "000"

    #@f70
    const-string v9, "10"

    #@f72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@f75
    aput-object v0, v11, v12

    #@f77
    const/16 v12, 0x7c

    #@f79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@f7b
    const/16 v1, 0x7c

    #@f7d
    const v2, 0x209021b

    #@f80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@f83
    move-result-object v2

    #@f84
    const-string v3, "278"

    #@f86
    const-string v4, "356"

    #@f88
    const-string v5, "00"

    #@f8a
    const-string v6, ""

    #@f8c
    const-string v7, "0"

    #@f8e
    const-string v8, "000"

    #@f90
    const-string v9, "10"

    #@f92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@f95
    aput-object v0, v11, v12

    #@f97
    const/16 v12, 0x7d

    #@f99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@f9b
    const/16 v1, 0x7d

    #@f9d
    const v2, 0x209021c

    #@fa0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@fa3
    move-result-object v2

    #@fa4
    const-string v3, "901"

    #@fa6
    const-string v4, "1"

    #@fa8
    const-string v5, "011"

    #@faa
    const-string v6, "1"

    #@fac
    const-string v7, "1"

    #@fae
    const-string v8, "000"

    #@fb0
    const-string v9, "10"

    #@fb2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@fb5
    aput-object v0, v11, v12

    #@fb7
    const/16 v12, 0x7e

    #@fb9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@fbb
    const/16 v1, 0x7e

    #@fbd
    const v2, 0x209021d

    #@fc0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@fc3
    move-result-object v2

    #@fc4
    const-string v3, "551"

    #@fc6
    const-string v4, "692"

    #@fc8
    const-string v5, "011"

    #@fca
    const-string v6, "1"

    #@fcc
    const-string v7, "0"

    #@fce
    const-string v8, "000"

    #@fd0
    const-string v9, "10"

    #@fd2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@fd5
    aput-object v0, v11, v12

    #@fd7
    const/16 v12, 0x7f

    #@fd9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@fdb
    const/16 v1, 0x7f

    #@fdd
    const v2, 0x209021e

    #@fe0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@fe3
    move-result-object v2

    #@fe4
    const-string v3, "340"

    #@fe6
    const-string v4, "596"

    #@fe8
    const-string v5, "00"

    #@fea
    const-string v6, ""

    #@fec
    const-string v7, "0"

    #@fee
    const-string v8, "000"

    #@ff0
    const-string v9, "10"

    #@ff2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@ff5
    aput-object v0, v11, v12

    #@ff7
    const/16 v12, 0x80

    #@ff9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@ffb
    const/16 v1, 0x80

    #@ffd
    const v2, 0x20901af

    #@1000
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1003
    move-result-object v2

    #@1004
    const-string v3, "609"

    #@1006
    const-string v4, "222"

    #@1008
    const-string v5, "00"

    #@100a
    const-string v6, ""

    #@100c
    const-string v7, "0"

    #@100e
    const-string v8, "000"

    #@1010
    const-string v9, "10"

    #@1012
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1015
    aput-object v0, v11, v12

    #@1017
    const/16 v12, 0x81

    #@1019
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@101b
    const/16 v1, 0x81

    #@101d
    const v2, 0x20901b0

    #@1020
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1023
    move-result-object v2

    #@1024
    const-string v3, "617"

    #@1026
    const-string v4, "230"

    #@1028
    const-string v5, "020"

    #@102a
    const-string v6, ""

    #@102c
    const-string v7, "0"

    #@102e
    const-string v8, "000"

    #@1030
    const-string v9, "10"

    #@1032
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1035
    aput-object v0, v11, v12

    #@1037
    const/16 v12, 0x82

    #@1039
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@103b
    const/16 v1, 0x82

    #@103d
    const v2, 0x20901b1

    #@1040
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1043
    move-result-object v2

    #@1044
    const-string v3, "334"

    #@1046
    const-string v4, "52"

    #@1048
    const-string v5, "00"

    #@104a
    const-string v6, "01"

    #@104c
    const-string v7, "0"

    #@104e
    const-string v8, "000"

    #@1050
    const-string v9, "10"

    #@1052
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1055
    aput-object v0, v11, v12

    #@1057
    const/16 v12, 0x83

    #@1059
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@105b
    const/16 v1, 0x83

    #@105d
    const v2, 0x20901b2

    #@1060
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1063
    move-result-object v2

    #@1064
    const-string v3, "259"

    #@1066
    const-string v4, "373"

    #@1068
    const-string v5, "00"

    #@106a
    const-string v6, "0"

    #@106c
    const-string v7, "0"

    #@106e
    const-string v8, "000"

    #@1070
    const-string v9, "10"

    #@1072
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1075
    aput-object v0, v11, v12

    #@1077
    const/16 v12, 0x84

    #@1079
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@107b
    const/16 v1, 0x84

    #@107d
    const v2, 0x209021f

    #@1080
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1083
    move-result-object v2

    #@1084
    const-string v3, "212"

    #@1086
    const-string v4, "377"

    #@1088
    const-string v5, "00"

    #@108a
    const-string v6, ""

    #@108c
    const-string v7, "0"

    #@108e
    const-string v8, "000"

    #@1090
    const-string v9, "10"

    #@1092
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1095
    aput-object v0, v11, v12

    #@1097
    const/16 v12, 0x85

    #@1099
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@109b
    const/16 v1, 0x85

    #@109d
    const v2, 0x20901b3

    #@10a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@10a3
    move-result-object v2

    #@10a4
    const-string v3, "428"

    #@10a6
    const-string v4, "976"

    #@10a8
    const-string v5, "001"

    #@10aa
    const-string v6, "0"

    #@10ac
    const-string v7, "0"

    #@10ae
    const-string v8, "000"

    #@10b0
    const-string v9, "10"

    #@10b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@10b5
    aput-object v0, v11, v12

    #@10b7
    const/16 v12, 0x86

    #@10b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@10bb
    const/16 v1, 0x86

    #@10bd
    const v2, 0x2090220

    #@10c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@10c3
    move-result-object v2

    #@10c4
    const-string v3, "354"

    #@10c6
    const-string v4, "1"

    #@10c8
    const-string v5, "011"

    #@10ca
    const-string v6, "1"

    #@10cc
    const-string v7, "1"

    #@10ce
    const-string v8, "000"

    #@10d0
    const-string v9, "10"

    #@10d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@10d5
    aput-object v0, v11, v12

    #@10d7
    const/16 v12, 0x87

    #@10d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@10db
    const/16 v1, 0x87

    #@10dd
    const v2, 0x20901b4

    #@10e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@10e3
    move-result-object v2

    #@10e4
    const-string v3, "604"

    #@10e6
    const-string v4, "212"

    #@10e8
    const-string v5, "00"

    #@10ea
    const-string v6, "0"

    #@10ec
    const-string v7, "0"

    #@10ee
    const-string v8, "000"

    #@10f0
    const-string v9, "10"

    #@10f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@10f5
    aput-object v0, v11, v12

    #@10f7
    const/16 v12, 0x88

    #@10f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@10fb
    const/16 v1, 0x88

    #@10fd
    const v2, 0x20901b5

    #@1100
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1103
    move-result-object v2

    #@1104
    const-string v3, "643"

    #@1106
    const-string v4, "258"

    #@1108
    const-string v5, "00"

    #@110a
    const-string v6, ""

    #@110c
    const-string v7, "0"

    #@110e
    const-string v8, "000"

    #@1110
    const-string v9, "10"

    #@1112
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1115
    aput-object v0, v11, v12

    #@1117
    const/16 v12, 0x89

    #@1119
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@111b
    const/16 v1, 0x89

    #@111d
    const v2, 0x20901b6

    #@1120
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1123
    move-result-object v2

    #@1124
    const-string v3, "414"

    #@1126
    const-string v4, "95"

    #@1128
    const-string v5, "00"

    #@112a
    const-string v6, "0"

    #@112c
    const-string v7, "0"

    #@112e
    const-string v8, "000"

    #@1130
    const-string v9, "10"

    #@1132
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1135
    aput-object v0, v11, v12

    #@1137
    const/16 v12, 0x8a

    #@1139
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@113b
    const/16 v1, 0x8a

    #@113d
    const v2, 0x20901b7

    #@1140
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1143
    move-result-object v2

    #@1144
    const-string v3, "649"

    #@1146
    const-string v4, "264"

    #@1148
    const-string v5, "00"

    #@114a
    const-string v6, "0"

    #@114c
    const-string v7, "0"

    #@114e
    const-string v8, "000"

    #@1150
    const-string v9, "10"

    #@1152
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1155
    aput-object v0, v11, v12

    #@1157
    const/16 v12, 0x8b

    #@1159
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@115b
    const/16 v1, 0x8b

    #@115d
    const v2, 0x2090221

    #@1160
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1163
    move-result-object v2

    #@1164
    const-string v3, "536"

    #@1166
    const-string v4, "674"

    #@1168
    const-string v5, "00"

    #@116a
    const-string v6, ""

    #@116c
    const-string v7, "0"

    #@116e
    const-string v8, "000"

    #@1170
    const-string v9, "10"

    #@1172
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1175
    aput-object v0, v11, v12

    #@1177
    const/16 v12, 0x8c

    #@1179
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@117b
    const/16 v1, 0x8c

    #@117d
    const v2, 0x2090222

    #@1180
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1183
    move-result-object v2

    #@1184
    const-string v3, "429"

    #@1186
    const-string v4, "977"

    #@1188
    const-string v5, "00"

    #@118a
    const-string v6, "0"

    #@118c
    const-string v7, "0"

    #@118e
    const-string v8, "000"

    #@1190
    const-string v9, "10"

    #@1192
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1195
    aput-object v0, v11, v12

    #@1197
    const/16 v12, 0x8d

    #@1199
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@119b
    const/16 v1, 0x8d

    #@119d
    const v2, 0x2090223

    #@11a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@11a3
    move-result-object v2

    #@11a4
    const-string v3, "204"

    #@11a6
    const-string v4, "31"

    #@11a8
    const-string v5, "00"

    #@11aa
    const-string v6, "0"

    #@11ac
    const-string v7, "0"

    #@11ae
    const-string v8, "000"

    #@11b0
    const-string v9, "10"

    #@11b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@11b5
    aput-object v0, v11, v12

    #@11b7
    const/16 v12, 0x8e

    #@11b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@11bb
    const/16 v1, 0x8e

    #@11bd
    const v2, 0x20901b8

    #@11c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@11c3
    move-result-object v2

    #@11c4
    const-string v3, "362"

    #@11c6
    const-string v4, "599"

    #@11c8
    const-string v5, "00"

    #@11ca
    const-string v6, "0"

    #@11cc
    const-string v7, "0"

    #@11ce
    const-string v8, "000"

    #@11d0
    const-string v9, "10"

    #@11d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@11d5
    aput-object v0, v11, v12

    #@11d7
    const/16 v12, 0x8f

    #@11d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@11db
    const/16 v1, 0x8f

    #@11dd
    const v2, 0x2090224

    #@11e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@11e3
    move-result-object v2

    #@11e4
    const-string v3, "546"

    #@11e6
    const-string v4, "687"

    #@11e8
    const-string v5, "00"

    #@11ea
    const-string v6, ""

    #@11ec
    const-string v7, "0"

    #@11ee
    const-string v8, "000"

    #@11f0
    const-string v9, "10"

    #@11f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@11f5
    aput-object v0, v11, v12

    #@11f7
    const/16 v12, 0x90

    #@11f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@11fb
    const/16 v1, 0x90

    #@11fd
    const v2, 0x20901b9

    #@1200
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1203
    move-result-object v2

    #@1204
    const-string v3, "530"

    #@1206
    const-string v4, "64"

    #@1208
    const-string v5, "00"

    #@120a
    const-string v6, "0"

    #@120c
    const-string v7, "0"

    #@120e
    const-string v8, "000"

    #@1210
    const-string v9, "10"

    #@1212
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1215
    aput-object v0, v11, v12

    #@1217
    const/16 v12, 0x91

    #@1219
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@121b
    const/16 v1, 0x91

    #@121d
    const v2, 0x20901ba

    #@1220
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1223
    move-result-object v2

    #@1224
    const-string v3, "710"

    #@1226
    const-string v4, "505"

    #@1228
    const-string v5, "00"

    #@122a
    const-string v6, ""

    #@122c
    const-string v7, "0"

    #@122e
    const-string v8, "000"

    #@1230
    const-string v9, "10"

    #@1232
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1235
    aput-object v0, v11, v12

    #@1237
    const/16 v12, 0x92

    #@1239
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@123b
    const/16 v1, 0x92

    #@123d
    const v2, 0x20901bb

    #@1240
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1243
    move-result-object v2

    #@1244
    const-string v3, "614"

    #@1246
    const-string v4, "227"

    #@1248
    const-string v5, "00"

    #@124a
    const-string v6, ""

    #@124c
    const-string v7, "0"

    #@124e
    const-string v8, "000"

    #@1250
    const-string v9, "10"

    #@1252
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1255
    aput-object v0, v11, v12

    #@1257
    const/16 v12, 0x93

    #@1259
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@125b
    const/16 v1, 0x93

    #@125d
    const v2, 0x20901bc

    #@1260
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1263
    move-result-object v2

    #@1264
    const-string v3, "621"

    #@1266
    const-string v4, "234"

    #@1268
    const-string v5, "009"

    #@126a
    const-string v6, "0"

    #@126c
    const-string v7, "0"

    #@126e
    const-string v8, "000"

    #@1270
    const-string v9, "10"

    #@1272
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1275
    aput-object v0, v11, v12

    #@1277
    const/16 v12, 0x94

    #@1279
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@127b
    const/16 v1, 0x94

    #@127d
    const v2, 0x20901bd

    #@1280
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1283
    move-result-object v2

    #@1284
    const-string v3, "534"

    #@1286
    const-string v4, "1"

    #@1288
    const-string v5, "011"

    #@128a
    const-string v6, "1"

    #@128c
    const-string v7, "0"

    #@128e
    const-string v8, "000"

    #@1290
    const-string v9, "10"

    #@1292
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1295
    aput-object v0, v11, v12

    #@1297
    const/16 v12, 0x95

    #@1299
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@129b
    const/16 v1, 0x95

    #@129d
    const v2, 0x20901be

    #@12a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@12a3
    move-result-object v2

    #@12a4
    const-string v3, "242"

    #@12a6
    const-string v4, "47"

    #@12a8
    const-string v5, "00"

    #@12aa
    const-string v6, ""

    #@12ac
    const-string v7, "0"

    #@12ae
    const-string v8, "000"

    #@12b0
    const-string v9, "10"

    #@12b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@12b5
    aput-object v0, v11, v12

    #@12b7
    const/16 v12, 0x96

    #@12b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@12bb
    const/16 v1, 0x96

    #@12bd
    const v2, 0x20901bf

    #@12c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@12c3
    move-result-object v2

    #@12c4
    const-string v3, "422"

    #@12c6
    const-string v4, "968"

    #@12c8
    const-string v5, "00"

    #@12ca
    const-string v6, ""

    #@12cc
    const-string v7, "0"

    #@12ce
    const-string v8, "000"

    #@12d0
    const-string v9, "10"

    #@12d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@12d5
    aput-object v0, v11, v12

    #@12d7
    const/16 v12, 0x97

    #@12d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@12db
    const/16 v1, 0x97

    #@12dd
    const v2, 0x20901c0

    #@12e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@12e3
    move-result-object v2

    #@12e4
    const-string v3, "410"

    #@12e6
    const-string v4, "92"

    #@12e8
    const-string v5, "00"

    #@12ea
    const-string v6, "0"

    #@12ec
    const-string v7, "0"

    #@12ee
    const-string v8, "000"

    #@12f0
    const-string v9, "10"

    #@12f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@12f5
    aput-object v0, v11, v12

    #@12f7
    const/16 v12, 0x98

    #@12f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@12fb
    const/16 v1, 0x98

    #@12fd
    const v2, 0x2090225

    #@1300
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1303
    move-result-object v2

    #@1304
    const-string v3, "552"

    #@1306
    const-string v4, "680"

    #@1308
    const-string v5, "11"

    #@130a
    const-string v6, ""

    #@130c
    const-string v7, "0"

    #@130e
    const-string v8, "000"

    #@1310
    const-string v9, "10"

    #@1312
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1315
    aput-object v0, v11, v12

    #@1317
    const/16 v12, 0x99

    #@1319
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@131b
    const/16 v1, 0x99

    #@131d
    const v2, 0x2090226

    #@1320
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1323
    move-result-object v2

    #@1324
    const-string v3, "714"

    #@1326
    const-string v4, "507"

    #@1328
    const-string v5, "00"

    #@132a
    const-string v6, ""

    #@132c
    const-string v7, "0"

    #@132e
    const-string v8, "000"

    #@1330
    const-string v9, "10"

    #@1332
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1335
    aput-object v0, v11, v12

    #@1337
    const/16 v12, 0x9a

    #@1339
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@133b
    const/16 v1, 0x9a

    #@133d
    const v2, 0x2090227

    #@1340
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1343
    move-result-object v2

    #@1344
    const-string v3, "537"

    #@1346
    const-string v4, "675"

    #@1348
    const-string v5, "5"

    #@134a
    const-string v6, ""

    #@134c
    const-string v7, "0"

    #@134e
    const-string v8, "000"

    #@1350
    const-string v9, "10"

    #@1352
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1355
    aput-object v0, v11, v12

    #@1357
    const/16 v12, 0x9b

    #@1359
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@135b
    const/16 v1, 0x9b

    #@135d
    const v2, 0x2090228

    #@1360
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1363
    move-result-object v2

    #@1364
    const-string v3, "744"

    #@1366
    const-string v4, "595"

    #@1368
    const-string v5, "2"

    #@136a
    const-string v6, "0"

    #@136c
    const-string v7, "0"

    #@136e
    const-string v8, "000"

    #@1370
    const-string v9, "10"

    #@1372
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1375
    aput-object v0, v11, v12

    #@1377
    const/16 v12, 0x9c

    #@1379
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@137b
    const/16 v1, 0x9c

    #@137d
    const v2, 0x20901c1

    #@1380
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1383
    move-result-object v2

    #@1384
    const-string v3, "716"

    #@1386
    const-string v4, "51"

    #@1388
    const-string v5, "00"

    #@138a
    const-string v6, "0"

    #@138c
    const-string v7, "0"

    #@138e
    const-string v8, "000"

    #@1390
    const-string v9, "10"

    #@1392
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1395
    aput-object v0, v11, v12

    #@1397
    const/16 v12, 0x9d

    #@1399
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@139b
    const/16 v1, 0x9d

    #@139d
    const v2, 0x2090229

    #@13a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@13a3
    move-result-object v2

    #@13a4
    const-string v3, "515"

    #@13a6
    const-string v4, "63"

    #@13a8
    const-string v5, "00"

    #@13aa
    const-string v6, "0"

    #@13ac
    const-string v7, "0"

    #@13ae
    const-string v8, "000"

    #@13b0
    const-string v9, "10"

    #@13b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@13b5
    aput-object v0, v11, v12

    #@13b7
    const/16 v12, 0x9e

    #@13b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@13bb
    const/16 v1, 0x9e

    #@13bd
    const v2, 0x20901c2

    #@13c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@13c3
    move-result-object v2

    #@13c4
    const-string v3, "260"

    #@13c6
    const-string v4, "48"

    #@13c8
    const-string v5, "00"

    #@13ca
    const-string v6, "0"

    #@13cc
    const-string v7, "0"

    #@13ce
    const-string v8, "000"

    #@13d0
    const-string v9, "10"

    #@13d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@13d5
    aput-object v0, v11, v12

    #@13d7
    const/16 v12, 0x9f

    #@13d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@13db
    const/16 v1, 0x9f

    #@13dd
    const v2, 0x20901c3

    #@13e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@13e3
    move-result-object v2

    #@13e4
    const-string v3, "268"

    #@13e6
    const-string v4, "351"

    #@13e8
    const-string v5, "00"

    #@13ea
    const-string v6, ""

    #@13ec
    const-string v7, "0"

    #@13ee
    const-string v8, "000"

    #@13f0
    const-string v9, "10"

    #@13f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@13f5
    aput-object v0, v11, v12

    #@13f7
    const/16 v12, 0xa0

    #@13f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@13fb
    const/16 v1, 0xa0

    #@13fd
    const v2, 0x20901c4

    #@1400
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1403
    move-result-object v2

    #@1404
    const-string v3, "330"

    #@1406
    const-string v4, "1"

    #@1408
    const-string v5, "011"

    #@140a
    const-string v6, "1"

    #@140c
    const-string v7, "1"

    #@140e
    const-string v8, "000"

    #@1410
    const-string v9, "10"

    #@1412
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1415
    aput-object v0, v11, v12

    #@1417
    const/16 v12, 0xa1

    #@1419
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@141b
    const/16 v1, 0xa1

    #@141d
    const v2, 0x209022a

    #@1420
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1423
    move-result-object v2

    #@1424
    const-string v3, "427"

    #@1426
    const-string v4, "974"

    #@1428
    const-string v5, "00"

    #@142a
    const-string v6, ""

    #@142c
    const-string v7, "0"

    #@142e
    const-string v8, "000"

    #@1430
    const-string v9, "10"

    #@1432
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1435
    aput-object v0, v11, v12

    #@1437
    const/16 v12, 0xa2

    #@1439
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@143b
    const/16 v1, 0xa2

    #@143d
    const v2, 0x209022c

    #@1440
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1443
    move-result-object v2

    #@1444
    const-string v3, "294"

    #@1446
    const-string v4, "389"

    #@1448
    const-string v5, "00"

    #@144a
    const-string v6, "0"

    #@144c
    const-string v7, "0"

    #@144e
    const-string v8, "000"

    #@1450
    const-string v9, "10"

    #@1452
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1455
    aput-object v0, v11, v12

    #@1457
    const/16 v12, 0xa3

    #@1459
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@145b
    const/16 v1, 0xa3

    #@145d
    const v2, 0x209022d

    #@1460
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1463
    move-result-object v2

    #@1464
    const-string v3, "629"

    #@1466
    const-string v4, "242"

    #@1468
    const-string v5, "00"

    #@146a
    const-string v6, ""

    #@146c
    const-string v7, "0"

    #@146e
    const-string v8, "000"

    #@1470
    const-string v9, "10"

    #@1472
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1475
    aput-object v0, v11, v12

    #@1477
    const/16 v12, 0xa4

    #@1479
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@147b
    const/16 v1, 0xa4

    #@147d
    const v2, 0x209022b

    #@1480
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1483
    move-result-object v2

    #@1484
    const-string v3, "647"

    #@1486
    const-string v4, "262"

    #@1488
    const-string v5, "00"

    #@148a
    const-string v6, "0"

    #@148c
    const-string v7, "0"

    #@148e
    const-string v8, "000"

    #@1490
    const-string v9, "10"

    #@1492
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1495
    aput-object v0, v11, v12

    #@1497
    const/16 v12, 0xa5

    #@1499
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@149b
    const/16 v1, 0xa5

    #@149d
    const v2, 0x20901c5

    #@14a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@14a3
    move-result-object v2

    #@14a4
    const-string v3, "226"

    #@14a6
    const-string v4, "40"

    #@14a8
    const-string v5, "00"

    #@14aa
    const-string v6, "0"

    #@14ac
    const-string v7, "0"

    #@14ae
    const-string v8, "000"

    #@14b0
    const-string v9, "10"

    #@14b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@14b5
    aput-object v0, v11, v12

    #@14b7
    const/16 v12, 0xa6

    #@14b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@14bb
    const/16 v1, 0xa6

    #@14bd
    const v2, 0x20901c6

    #@14c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@14c3
    move-result-object v2

    #@14c4
    const-string v3, "250"

    #@14c6
    const-string v4, "7"

    #@14c8
    const-string v5, "810"

    #@14ca
    const-string v6, "8"

    #@14cc
    const-string v7, "0"

    #@14ce
    const-string v8, "000"

    #@14d0
    const-string v9, "10"

    #@14d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@14d5
    aput-object v0, v11, v12

    #@14d7
    const/16 v12, 0xa7

    #@14d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@14db
    const/16 v1, 0xa7

    #@14dd
    const v2, 0x20901c7

    #@14e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@14e3
    move-result-object v2

    #@14e4
    const-string v3, "635"

    #@14e6
    const-string v4, "250"

    #@14e8
    const-string v5, "00"

    #@14ea
    const-string v6, ""

    #@14ec
    const-string v7, "0"

    #@14ee
    const-string v8, "000"

    #@14f0
    const-string v9, "10"

    #@14f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@14f5
    aput-object v0, v11, v12

    #@14f7
    const/16 v12, 0xa8

    #@14f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@14fb
    const/16 v1, 0xa8

    #@14fd
    const v2, 0x209022f

    #@1500
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1503
    move-result-object v2

    #@1504
    const-string v3, "356"

    #@1506
    const-string v4, "1"

    #@1508
    const-string v5, "011"

    #@150a
    const-string v6, "1"

    #@150c
    const-string v7, "1"

    #@150e
    const-string v8, "000"

    #@1510
    const-string v9, "10"

    #@1512
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1515
    aput-object v0, v11, v12

    #@1517
    const/16 v12, 0xa9

    #@1519
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@151b
    const/16 v1, 0xa9

    #@151d
    const v2, 0x2090230

    #@1520
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1523
    move-result-object v2

    #@1524
    const-string v3, "358"

    #@1526
    const-string v4, "1"

    #@1528
    const-string v5, "011"

    #@152a
    const-string v6, "1"

    #@152c
    const-string v7, "1"

    #@152e
    const-string v8, "000"

    #@1530
    const-string v9, "10"

    #@1532
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1535
    aput-object v0, v11, v12

    #@1537
    const/16 v12, 0xaa

    #@1539
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@153b
    const/16 v1, 0xaa

    #@153d
    const v2, 0x2090231

    #@1540
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1543
    move-result-object v2

    #@1544
    const-string v3, "308"

    #@1546
    const-string v4, "508"

    #@1548
    const-string v5, "00"

    #@154a
    const-string v6, ""

    #@154c
    const-string v7, "0"

    #@154e
    const-string v8, "000"

    #@1550
    const-string v9, "10"

    #@1552
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1555
    aput-object v0, v11, v12

    #@1557
    const/16 v12, 0xab

    #@1559
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@155b
    const/16 v1, 0xab

    #@155d
    const v2, 0x2090232

    #@1560
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1563
    move-result-object v2

    #@1564
    const-string v3, "360"

    #@1566
    const-string v4, "1"

    #@1568
    const-string v5, "011"

    #@156a
    const-string v6, "1"

    #@156c
    const-string v7, "1"

    #@156e
    const-string v8, "000"

    #@1570
    const-string v9, "10"

    #@1572
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1575
    aput-object v0, v11, v12

    #@1577
    const/16 v12, 0xac

    #@1579
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@157b
    const/16 v1, 0xac

    #@157d
    const v2, 0x2090233

    #@1580
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1583
    move-result-object v2

    #@1584
    const-string v3, "549"

    #@1586
    const-string v4, "685"

    #@1588
    const-string v5, "0"

    #@158a
    const-string v6, ""

    #@158c
    const-string v7, "0"

    #@158e
    const-string v8, "000"

    #@1590
    const-string v9, "10"

    #@1592
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1595
    aput-object v0, v11, v12

    #@1597
    const/16 v12, 0xad

    #@1599
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@159b
    const/16 v1, 0xad

    #@159d
    const v2, 0x2090234

    #@15a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@15a3
    move-result-object v2

    #@15a4
    const-string v3, "292"

    #@15a6
    const-string v4, "378"

    #@15a8
    const-string v5, "00"

    #@15aa
    const-string v6, ""

    #@15ac
    const-string v7, "0"

    #@15ae
    const-string v8, "000"

    #@15b0
    const-string v9, "10"

    #@15b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@15b5
    aput-object v0, v11, v12

    #@15b7
    const/16 v12, 0xae

    #@15b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@15bb
    const/16 v1, 0xae

    #@15bd
    const v2, 0x209022e

    #@15c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@15c3
    move-result-object v2

    #@15c4
    const-string v3, "626"

    #@15c6
    const-string v4, "239"

    #@15c8
    const-string v5, "00"

    #@15ca
    const-string v6, ""

    #@15cc
    const-string v7, "0"

    #@15ce
    const-string v8, "000"

    #@15d0
    const-string v9, "10"

    #@15d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@15d5
    aput-object v0, v11, v12

    #@15d7
    const/16 v12, 0xaf

    #@15d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@15db
    const/16 v1, 0xaf

    #@15dd
    const v2, 0x20901c8

    #@15e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@15e3
    move-result-object v2

    #@15e4
    const-string v3, "420"

    #@15e6
    const-string v4, "966"

    #@15e8
    const-string v5, "00"

    #@15ea
    const-string v6, "0"

    #@15ec
    const-string v7, "0"

    #@15ee
    const-string v8, "000"

    #@15f0
    const-string v9, "10"

    #@15f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@15f5
    aput-object v0, v11, v12

    #@15f7
    const/16 v12, 0xb0

    #@15f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@15fb
    const/16 v1, 0xb0

    #@15fd
    const v2, 0x2090235

    #@1600
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1603
    move-result-object v2

    #@1604
    const-string v3, "608"

    #@1606
    const-string v4, "221"

    #@1608
    const-string v5, "00"

    #@160a
    const-string v6, ""

    #@160c
    const-string v7, "0"

    #@160e
    const-string v8, "000"

    #@1610
    const-string v9, "10"

    #@1612
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1615
    aput-object v0, v11, v12

    #@1617
    const/16 v12, 0xb1

    #@1619
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@161b
    const/16 v1, 0xb1

    #@161d
    const v2, 0x2090236

    #@1620
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1623
    move-result-object v2

    #@1624
    const-string v3, "220"

    #@1626
    const-string v4, "381"

    #@1628
    const-string v5, "99"

    #@162a
    const-string v6, "0"

    #@162c
    const-string v7, "0"

    #@162e
    const-string v8, "000"

    #@1630
    const-string v9, "10"

    #@1632
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1635
    aput-object v0, v11, v12

    #@1637
    const/16 v12, 0xb2

    #@1639
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@163b
    const/16 v1, 0xb2

    #@163d
    const v2, 0x2090237

    #@1640
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1643
    move-result-object v2

    #@1644
    const-string v3, "633"

    #@1646
    const-string v4, "248"

    #@1648
    const-string v5, "00"

    #@164a
    const-string v6, ""

    #@164c
    const-string v7, "0"

    #@164e
    const-string v8, "000"

    #@1650
    const-string v9, "10"

    #@1652
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1655
    aput-object v0, v11, v12

    #@1657
    const/16 v12, 0xb3

    #@1659
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@165b
    const/16 v1, 0xb3

    #@165d
    const v2, 0x2090238

    #@1660
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1663
    move-result-object v2

    #@1664
    const-string v3, "619"

    #@1666
    const-string v4, "232"

    #@1668
    const-string v5, "00"

    #@166a
    const-string v6, "0"

    #@166c
    const-string v7, "0"

    #@166e
    const-string v8, "000"

    #@1670
    const-string v9, "10"

    #@1672
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1675
    aput-object v0, v11, v12

    #@1677
    const/16 v12, 0xb4

    #@1679
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@167b
    const/16 v1, 0xb4

    #@167d
    const v2, 0x2090239

    #@1680
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1683
    move-result-object v2

    #@1684
    const-string v3, "525"

    #@1686
    const-string v4, "65"

    #@1688
    const-string v5, "001"

    #@168a
    const-string v6, ""

    #@168c
    const-string v7, "0"

    #@168e
    const-string v8, "000"

    #@1690
    const-string v9, "10"

    #@1692
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1695
    aput-object v0, v11, v12

    #@1697
    const/16 v12, 0xb5

    #@1699
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@169b
    const/16 v1, 0xb5

    #@169d
    const v2, 0x209023a

    #@16a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@16a3
    move-result-object v2

    #@16a4
    const-string v3, "231"

    #@16a6
    const-string v4, "421"

    #@16a8
    const-string v5, "00"

    #@16aa
    const-string v6, "0"

    #@16ac
    const-string v7, "0"

    #@16ae
    const-string v8, "000"

    #@16b0
    const-string v9, "10"

    #@16b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@16b5
    aput-object v0, v11, v12

    #@16b7
    const/16 v12, 0xb6

    #@16b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@16bb
    const/16 v1, 0xb6

    #@16bd
    const v2, 0x209023b

    #@16c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@16c3
    move-result-object v2

    #@16c4
    const-string v3, "293"

    #@16c6
    const-string v4, "386"

    #@16c8
    const-string v5, "00"

    #@16ca
    const-string v6, "0"

    #@16cc
    const-string v7, "0"

    #@16ce
    const-string v8, "000"

    #@16d0
    const-string v9, "10"

    #@16d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@16d5
    aput-object v0, v11, v12

    #@16d7
    const/16 v12, 0xb7

    #@16d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@16db
    const/16 v1, 0xb7

    #@16dd
    const v2, 0x209023c

    #@16e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@16e3
    move-result-object v2

    #@16e4
    const-string v3, "540"

    #@16e6
    const-string v4, "677"

    #@16e8
    const-string v5, "00"

    #@16ea
    const-string v6, ""

    #@16ec
    const-string v7, "0"

    #@16ee
    const-string v8, "000"

    #@16f0
    const-string v9, "10"

    #@16f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@16f5
    aput-object v0, v11, v12

    #@16f7
    const/16 v12, 0xb8

    #@16f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@16fb
    const/16 v1, 0xb8

    #@16fd
    const v2, 0x209023d

    #@1700
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1703
    move-result-object v2

    #@1704
    const-string v3, "637"

    #@1706
    const-string v4, "252"

    #@1708
    const-string v5, "00"

    #@170a
    const-string v6, ""

    #@170c
    const-string v7, "0"

    #@170e
    const-string v8, "000"

    #@1710
    const-string v9, "10"

    #@1712
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1715
    aput-object v0, v11, v12

    #@1717
    const/16 v12, 0xb9

    #@1719
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@171b
    const/16 v1, 0xb9

    #@171d
    const v2, 0x20901c9

    #@1720
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1723
    move-result-object v2

    #@1724
    const-string v3, "655"

    #@1726
    const-string v4, "27"

    #@1728
    const-string v5, "00"

    #@172a
    const-string v6, "0"

    #@172c
    const-string v7, "0"

    #@172e
    const-string v8, "000"

    #@1730
    const-string v9, "10"

    #@1732
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1735
    aput-object v0, v11, v12

    #@1737
    const/16 v12, 0xba

    #@1739
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@173b
    const/16 v1, 0xba

    #@173d
    const v2, 0x209023e

    #@1740
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1743
    move-result-object v2

    #@1744
    const-string v3, "214"

    #@1746
    const-string v4, "34"

    #@1748
    const-string v5, "00"

    #@174a
    const-string v6, ""

    #@174c
    const-string v7, "0"

    #@174e
    const-string v8, "000"

    #@1750
    const-string v9, "10"

    #@1752
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1755
    aput-object v0, v11, v12

    #@1757
    const/16 v12, 0xbb

    #@1759
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@175b
    const/16 v1, 0xbb

    #@175d
    const v2, 0x209023f

    #@1760
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1763
    move-result-object v2

    #@1764
    const-string v3, "413"

    #@1766
    const-string v4, "94"

    #@1768
    const-string v5, "00"

    #@176a
    const-string v6, "0"

    #@176c
    const-string v7, "0"

    #@176e
    const-string v8, "000"

    #@1770
    const-string v9, "10"

    #@1772
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1775
    aput-object v0, v11, v12

    #@1777
    const/16 v12, 0xbc

    #@1779
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@177b
    const/16 v1, 0xbc

    #@177d
    const v2, 0x2090240

    #@1780
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1783
    move-result-object v2

    #@1784
    const-string v3, "634"

    #@1786
    const-string v4, "249"

    #@1788
    const-string v5, "00"

    #@178a
    const-string v6, "0"

    #@178c
    const-string v7, "0"

    #@178e
    const-string v8, "000"

    #@1790
    const-string v9, "10"

    #@1792
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1795
    aput-object v0, v11, v12

    #@1797
    const/16 v12, 0xbd

    #@1799
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@179b
    const/16 v1, 0xbd

    #@179d
    const v2, 0x20901ca

    #@17a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@17a3
    move-result-object v2

    #@17a4
    const-string v3, "746"

    #@17a6
    const-string v4, "597"

    #@17a8
    const-string v5, "00"

    #@17aa
    const-string v6, "0"

    #@17ac
    const-string v7, "0"

    #@17ae
    const-string v8, "000"

    #@17b0
    const-string v9, "10"

    #@17b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@17b5
    aput-object v0, v11, v12

    #@17b7
    const/16 v12, 0xbe

    #@17b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@17bb
    const/16 v1, 0xbe

    #@17bd
    const v2, 0x20901cb

    #@17c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@17c3
    move-result-object v2

    #@17c4
    const-string v3, "653"

    #@17c6
    const-string v4, "268"

    #@17c8
    const-string v5, "00"

    #@17ca
    const-string v6, ""

    #@17cc
    const-string v7, "0"

    #@17ce
    const-string v8, "000"

    #@17d0
    const-string v9, "10"

    #@17d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@17d5
    aput-object v0, v11, v12

    #@17d7
    const/16 v12, 0xbf

    #@17d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@17db
    const/16 v1, 0xbf

    #@17dd
    const v2, 0x20901cc

    #@17e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@17e3
    move-result-object v2

    #@17e4
    const-string v3, "240"

    #@17e6
    const-string v4, "46"

    #@17e8
    const-string v5, "00"

    #@17ea
    const-string v6, "0"

    #@17ec
    const-string v7, "0"

    #@17ee
    const-string v8, "000"

    #@17f0
    const-string v9, "10"

    #@17f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@17f5
    aput-object v0, v11, v12

    #@17f7
    const/16 v12, 0xc0

    #@17f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@17fb
    const/16 v1, 0xc0

    #@17fd
    const v2, 0x2090241

    #@1800
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1803
    move-result-object v2

    #@1804
    const-string v3, "228"

    #@1806
    const-string v4, "41"

    #@1808
    const-string v5, "00"

    #@180a
    const-string v6, "0"

    #@180c
    const-string v7, "0"

    #@180e
    const-string v8, "000"

    #@1810
    const-string v9, "10"

    #@1812
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1815
    aput-object v0, v11, v12

    #@1817
    const/16 v12, 0xc1

    #@1819
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@181b
    const/16 v1, 0xc1

    #@181d
    const v2, 0x2090242

    #@1820
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1823
    move-result-object v2

    #@1824
    const-string v3, "417"

    #@1826
    const-string v4, "963"

    #@1828
    const-string v5, "00"

    #@182a
    const-string v6, "0"

    #@182c
    const-string v7, "0"

    #@182e
    const-string v8, "000"

    #@1830
    const-string v9, "10"

    #@1832
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1835
    aput-object v0, v11, v12

    #@1837
    const/16 v12, 0xc2

    #@1839
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@183b
    const/16 v1, 0xc2

    #@183d
    const v2, 0x2090243

    #@1840
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1843
    move-result-object v2

    #@1844
    const-string v3, "466"

    #@1846
    const-string v4, "886"

    #@1848
    const-string v5, "002"

    #@184a
    const-string v6, "0"

    #@184c
    const-string v7, "0"

    #@184e
    const-string v8, "000"

    #@1850
    const-string v9, "10"

    #@1852
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1855
    aput-object v0, v11, v12

    #@1857
    const/16 v12, 0xc3

    #@1859
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@185b
    const/16 v1, 0xc3

    #@185d
    const v2, 0x2090244

    #@1860
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1863
    move-result-object v2

    #@1864
    const-string v3, "436"

    #@1866
    const-string v4, "992"

    #@1868
    const-string v5, "810"

    #@186a
    const-string v6, "8"

    #@186c
    const-string v7, "0"

    #@186e
    const-string v8, "000"

    #@1870
    const-string v9, "10"

    #@1872
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1875
    aput-object v0, v11, v12

    #@1877
    const/16 v12, 0xc4

    #@1879
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@187b
    const/16 v1, 0xc4

    #@187d
    const v2, 0x20901cd

    #@1880
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1883
    move-result-object v2

    #@1884
    const-string v3, "640"

    #@1886
    const-string v4, "255"

    #@1888
    const-string v5, "000"

    #@188a
    const-string v6, "0"

    #@188c
    const-string v7, "0"

    #@188e
    const-string v8, "000"

    #@1890
    const-string v9, "10"

    #@1892
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1895
    aput-object v0, v11, v12

    #@1897
    const/16 v12, 0xc5

    #@1899
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@189b
    const/16 v1, 0xc5

    #@189d
    const v2, 0x20901ce

    #@18a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@18a3
    move-result-object v2

    #@18a4
    const-string v3, "520"

    #@18a6
    const-string v4, "66"

    #@18a8
    const-string v5, "001"

    #@18aa
    const-string v6, "0"

    #@18ac
    const-string v7, "0"

    #@18ae
    const-string v8, "000"

    #@18b0
    const-string v9, "10"

    #@18b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@18b5
    aput-object v0, v11, v12

    #@18b7
    const/16 v12, 0xc6

    #@18b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@18bb
    const/16 v1, 0xc6

    #@18bd
    const v2, 0x20901cf

    #@18c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@18c3
    move-result-object v2

    #@18c4
    const-string v3, "615"

    #@18c6
    const-string v4, "228"

    #@18c8
    const-string v5, "00"

    #@18ca
    const-string v6, ""

    #@18cc
    const-string v7, "0"

    #@18ce
    const-string v8, "000"

    #@18d0
    const-string v9, "10"

    #@18d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@18d5
    aput-object v0, v11, v12

    #@18d7
    const/16 v12, 0xc7

    #@18d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@18db
    const/16 v1, 0xc7

    #@18dd
    const v2, 0x20901d0

    #@18e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@18e3
    move-result-object v2

    #@18e4
    const-string v3, "539"

    #@18e6
    const-string v4, "676"

    #@18e8
    const-string v5, "00"

    #@18ea
    const-string v6, ""

    #@18ec
    const-string v7, "0"

    #@18ee
    const-string v8, "000"

    #@18f0
    const-string v9, "10"

    #@18f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@18f5
    aput-object v0, v11, v12

    #@18f7
    const/16 v12, 0xc8

    #@18f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@18fb
    const/16 v1, 0xc8

    #@18fd
    const v2, 0x20901d1

    #@1900
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1903
    move-result-object v2

    #@1904
    const-string v3, "374"

    #@1906
    const-string v4, "1"

    #@1908
    const-string v5, "011"

    #@190a
    const-string v6, "1"

    #@190c
    const-string v7, "1"

    #@190e
    const-string v8, "000"

    #@1910
    const-string v9, "10"

    #@1912
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1915
    aput-object v0, v11, v12

    #@1917
    const/16 v12, 0xc9

    #@1919
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@191b
    const/16 v1, 0xc9

    #@191d
    const v2, 0x2090245

    #@1920
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1923
    move-result-object v2

    #@1924
    const-string v3, "605"

    #@1926
    const-string v4, "216"

    #@1928
    const-string v5, "00"

    #@192a
    const-string v6, ""

    #@192c
    const-string v7, "0"

    #@192e
    const-string v8, "000"

    #@1930
    const-string v9, "10"

    #@1932
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1935
    aput-object v0, v11, v12

    #@1937
    const/16 v12, 0xca

    #@1939
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@193b
    const/16 v1, 0xca

    #@193d
    const v2, 0x2090246

    #@1940
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1943
    move-result-object v2

    #@1944
    const-string v3, "286"

    #@1946
    const-string v4, "90"

    #@1948
    const-string v5, "00"

    #@194a
    const-string v6, "0"

    #@194c
    const-string v7, "0"

    #@194e
    const-string v8, "000"

    #@1950
    const-string v9, "10"

    #@1952
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1955
    aput-object v0, v11, v12

    #@1957
    const/16 v12, 0xcb

    #@1959
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@195b
    const/16 v1, 0xcb

    #@195d
    const v2, 0x2090247

    #@1960
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1963
    move-result-object v2

    #@1964
    const-string v3, "438"

    #@1966
    const-string v4, "993"

    #@1968
    const-string v5, "810"

    #@196a
    const-string v6, "8"

    #@196c
    const-string v7, "0"

    #@196e
    const-string v8, "000"

    #@1970
    const-string v9, "10"

    #@1972
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1975
    aput-object v0, v11, v12

    #@1977
    const/16 v12, 0xcc

    #@1979
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@197b
    const/16 v1, 0xcc

    #@197d
    const v2, 0x2090248

    #@1980
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1983
    move-result-object v2

    #@1984
    const-string v3, "376"

    #@1986
    const-string v4, "1"

    #@1988
    const-string v5, "011"

    #@198a
    const-string v6, "1"

    #@198c
    const-string v7, "1"

    #@198e
    const-string v8, "000"

    #@1990
    const-string v9, "10"

    #@1992
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1995
    aput-object v0, v11, v12

    #@1997
    const/16 v12, 0xcd

    #@1999
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@199b
    const/16 v1, 0xcd

    #@199d
    const v2, 0x20901d2

    #@19a0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@19a3
    move-result-object v2

    #@19a4
    const-string v3, "641"

    #@19a6
    const-string v4, "256"

    #@19a8
    const-string v5, "000"

    #@19aa
    const-string v6, "0"

    #@19ac
    const-string v7, "0"

    #@19ae
    const-string v8, "000"

    #@19b0
    const-string v9, "10"

    #@19b2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@19b5
    aput-object v0, v11, v12

    #@19b7
    const/16 v12, 0xce

    #@19b9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@19bb
    const/16 v1, 0xce

    #@19bd
    const v2, 0x20901d3

    #@19c0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@19c3
    move-result-object v2

    #@19c4
    const-string v3, "255"

    #@19c6
    const-string v4, "380"

    #@19c8
    const-string v5, "810"

    #@19ca
    const-string v6, "0"

    #@19cc
    const-string v7, "0"

    #@19ce
    const-string v8, "000"

    #@19d0
    const-string v9, "10"

    #@19d2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@19d5
    aput-object v0, v11, v12

    #@19d7
    const/16 v12, 0xcf

    #@19d9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@19db
    const/16 v1, 0xcf

    #@19dd
    const v2, 0x2090249

    #@19e0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@19e3
    move-result-object v2

    #@19e4
    const-string v3, "424"

    #@19e6
    const-string v4, "971"

    #@19e8
    const-string v5, "00"

    #@19ea
    const-string v6, "0"

    #@19ec
    const-string v7, "0"

    #@19ee
    const-string v8, "000"

    #@19f0
    const-string v9, "10"

    #@19f2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@19f5
    aput-object v0, v11, v12

    #@19f7
    const/16 v12, 0xd0

    #@19f9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@19fb
    const/16 v1, 0xd0

    #@19fd
    const v2, 0x209024a

    #@1a00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a03
    move-result-object v2

    #@1a04
    const-string v3, "430"

    #@1a06
    const-string v4, "971"

    #@1a08
    const-string v5, "00"

    #@1a0a
    const-string v6, "0"

    #@1a0c
    const-string v7, "0"

    #@1a0e
    const-string v8, "000"

    #@1a10
    const-string v9, "10"

    #@1a12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1a15
    aput-object v0, v11, v12

    #@1a17
    const/16 v12, 0xd1

    #@1a19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1a1b
    const/16 v1, 0xd1

    #@1a1d
    const v2, 0x209024b

    #@1a20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a23
    move-result-object v2

    #@1a24
    const-string v3, "431"

    #@1a26
    const-string v4, "971"

    #@1a28
    const-string v5, "00"

    #@1a2a
    const-string v6, "0"

    #@1a2c
    const-string v7, "0"

    #@1a2e
    const-string v8, "000"

    #@1a30
    const-string v9, "10"

    #@1a32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1a35
    aput-object v0, v11, v12

    #@1a37
    const/16 v12, 0xd2

    #@1a39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1a3b
    const/16 v1, 0xd2

    #@1a3d
    const v2, 0x209024c

    #@1a40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a43
    move-result-object v2

    #@1a44
    const-string v3, "234,235"

    #@1a46
    const-string v4, "44"

    #@1a48
    const-string v5, "00"

    #@1a4a
    const-string v6, "0"

    #@1a4c
    const-string v7, "0"

    #@1a4e
    const-string v8, "000"

    #@1a50
    const-string v9, "10"

    #@1a52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1a55
    aput-object v0, v11, v12

    #@1a57
    const/16 v12, 0xd3

    #@1a59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1a5b
    const/16 v1, 0xd3

    #@1a5d
    const v2, 0x20901d4

    #@1a60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a63
    move-result-object v2

    #@1a64
    const-string v3, "332"

    #@1a66
    const-string v4, "1"

    #@1a68
    const-string v5, "011"

    #@1a6a
    const-string v6, "1"

    #@1a6c
    const-string v7, "1"

    #@1a6e
    const-string v8, "000"

    #@1a70
    const-string v9, "10"

    #@1a72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1a75
    aput-object v0, v11, v12

    #@1a77
    const/16 v12, 0xd4

    #@1a79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1a7b
    const/16 v1, 0xd4

    #@1a7d
    const v2, 0x209024d

    #@1a80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a83
    move-result-object v2

    #@1a84
    const-string v3, "748"

    #@1a86
    const-string v4, "598"

    #@1a88
    const-string v5, "00"

    #@1a8a
    const-string v6, "0"

    #@1a8c
    const-string v7, "0"

    #@1a8e
    const-string v8, "000"

    #@1a90
    const-string v9, "10"

    #@1a92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1a95
    aput-object v0, v11, v12

    #@1a97
    const/16 v12, 0xd5

    #@1a99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1a9b
    const/16 v1, 0xd5

    #@1a9d
    const v2, 0x20901d5

    #@1aa0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1aa3
    move-result-object v2

    #@1aa4
    const-string v3, "434"

    #@1aa6
    const-string v4, "998"

    #@1aa8
    const-string v5, "810"

    #@1aaa
    const-string v6, "8"

    #@1aac
    const-string v7, "0"

    #@1aae
    const-string v8, "000"

    #@1ab0
    const-string v9, "10"

    #@1ab2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1ab5
    aput-object v0, v11, v12

    #@1ab7
    const/16 v12, 0xd6

    #@1ab9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1abb
    const/16 v1, 0xd6

    #@1abd
    const v2, 0x209024e

    #@1ac0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1ac3
    move-result-object v2

    #@1ac4
    const-string v3, "541"

    #@1ac6
    const-string v4, "678"

    #@1ac8
    const-string v5, "00"

    #@1aca
    const-string v6, ""

    #@1acc
    const-string v7, "0"

    #@1ace
    const-string v8, "000"

    #@1ad0
    const-string v9, "10"

    #@1ad2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1ad5
    aput-object v0, v11, v12

    #@1ad7
    const/16 v12, 0xd7

    #@1ad9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1adb
    const/16 v1, 0xd7

    #@1add
    const v2, 0x209024f

    #@1ae0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1ae3
    move-result-object v2

    #@1ae4
    const-string v3, "225"

    #@1ae6
    const-string v4, "39"

    #@1ae8
    const-string v5, "00"

    #@1aea
    const-string v6, ""

    #@1aec
    const-string v7, "0"

    #@1aee
    const-string v8, "000"

    #@1af0
    const-string v9, "10"

    #@1af2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1af5
    aput-object v0, v11, v12

    #@1af7
    const/16 v12, 0xd8

    #@1af9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1afb
    const/16 v1, 0xd8

    #@1afd
    const v2, 0x20901d6

    #@1b00
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b03
    move-result-object v2

    #@1b04
    const-string v3, "734"

    #@1b06
    const-string v4, "58"

    #@1b08
    const-string v5, "00"

    #@1b0a
    const-string v6, "0"

    #@1b0c
    const-string v7, "0"

    #@1b0e
    const-string v8, "000"

    #@1b10
    const-string v9, "10"

    #@1b12
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1b15
    aput-object v0, v11, v12

    #@1b17
    const/16 v12, 0xd9

    #@1b19
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1b1b
    const/16 v1, 0xd9

    #@1b1d
    const v2, 0x2090250

    #@1b20
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b23
    move-result-object v2

    #@1b24
    const-string v3, "452"

    #@1b26
    const-string v4, "84"

    #@1b28
    const-string v5, "00"

    #@1b2a
    const-string v6, "0"

    #@1b2c
    const-string v7, "0"

    #@1b2e
    const-string v8, "000"

    #@1b30
    const-string v9, "10"

    #@1b32
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1b35
    aput-object v0, v11, v12

    #@1b37
    const/16 v12, 0xda

    #@1b39
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1b3b
    const/16 v1, 0xda

    #@1b3d
    const v2, 0x2090251

    #@1b40
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b43
    move-result-object v2

    #@1b44
    const-string v3, "543"

    #@1b46
    const-string v4, "681"

    #@1b48
    const-string v5, "19"

    #@1b4a
    const-string v6, ""

    #@1b4c
    const-string v7, "0"

    #@1b4e
    const-string v8, "000"

    #@1b50
    const-string v9, "10"

    #@1b52
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1b55
    aput-object v0, v11, v12

    #@1b57
    const/16 v12, 0xdb

    #@1b59
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1b5b
    const/16 v1, 0xdb

    #@1b5d
    const v2, 0x20901d7

    #@1b60
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b63
    move-result-object v2

    #@1b64
    const-string v3, "421"

    #@1b66
    const-string v4, "967"

    #@1b68
    const-string v5, "00"

    #@1b6a
    const-string v6, "0"

    #@1b6c
    const-string v7, "0"

    #@1b6e
    const-string v8, "000"

    #@1b70
    const-string v9, "10"

    #@1b72
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1b75
    aput-object v0, v11, v12

    #@1b77
    const/16 v12, 0xdc

    #@1b79
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1b7b
    const/16 v1, 0xdc

    #@1b7d
    const v2, 0x20901d8

    #@1b80
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1b83
    move-result-object v2

    #@1b84
    const-string v3, "645"

    #@1b86
    const-string v4, "260"

    #@1b88
    const-string v5, "00"

    #@1b8a
    const-string v6, "0"

    #@1b8c
    const-string v7, "0"

    #@1b8e
    const-string v8, "000"

    #@1b90
    const-string v9, "10"

    #@1b92
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1b95
    aput-object v0, v11, v12

    #@1b97
    const/16 v12, 0xdd

    #@1b99
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1b9b
    const/16 v1, 0xdd

    #@1b9d
    const v2, 0x2090252

    #@1ba0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1ba3
    move-result-object v2

    #@1ba4
    const-string v3, "648"

    #@1ba6
    const-string v4, "263"

    #@1ba8
    const-string v5, "00"

    #@1baa
    const-string v6, "0"

    #@1bac
    const-string v7, "0"

    #@1bae
    const-string v8, "000"

    #@1bb0
    const-string v9, "10"

    #@1bb2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1bb5
    aput-object v0, v11, v12

    #@1bb7
    const/16 v12, 0xde

    #@1bb9
    new-instance v0, Landroid/provider/ReferenceCountry;

    #@1bbb
    const/16 v1, 0xde

    #@1bbd
    const v2, 0x2090253

    #@1bc0
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1bc3
    move-result-object v2

    #@1bc4
    const-string v3, "1024"

    #@1bc6
    const-string v4, ""

    #@1bc8
    const-string v5, ""

    #@1bca
    const-string v6, ""

    #@1bcc
    const-string v7, "0"

    #@1bce
    const-string v8, ""

    #@1bd0
    const-string v9, "10"

    #@1bd2
    invoke-direct/range {v0 .. v9}, Landroid/provider/ReferenceCountry;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1bd5
    aput-object v0, v11, v12

    #@1bd7
    sput-object v11, Landroid/provider/Settings$AssistDial;->countryOriginDataTable:[Landroid/provider/ReferenceCountry;

    #@1bd9
    .line 7343
    return-void
.end method
