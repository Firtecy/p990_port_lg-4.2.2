.class public final Landroid/provider/SyncStateContract$Helpers;
.super Ljava/lang/Object;
.source "SyncStateContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/SyncStateContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Helpers"
.end annotation


# static fields
.field private static final DATA_PROJECTION:[Ljava/lang/String; = null

.field private static final SELECT_BY_ACCOUNT:Ljava/lang/String; = "account_name=? AND account_type=?"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 59
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "data"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "_id"

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Landroid/provider/SyncStateContract$Helpers;->DATA_PROJECTION:[Ljava/lang/String;

    #@f
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static get(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)[B
    .registers 10
    .parameter "provider"
    .parameter "uri"
    .parameter "account"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 75
    sget-object v2, Landroid/provider/SyncStateContract$Helpers;->DATA_PROJECTION:[Ljava/lang/String;

    #@3
    const-string v3, "account_name=? AND account_type=?"

    #@5
    const/4 v0, 0x2

    #@6
    new-array v4, v0, [Ljava/lang/String;

    #@8
    const/4 v0, 0x0

    #@9
    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@b
    aput-object v1, v4, v0

    #@d
    const/4 v0, 0x1

    #@e
    iget-object v1, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@10
    aput-object v1, v4, v0

    #@12
    move-object v0, p0

    #@13
    move-object v1, p1

    #@14
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@17
    move-result-object v6

    #@18
    .line 79
    .local v6, c:Landroid/database/Cursor;
    if-nez v6, :cond_20

    #@1a
    .line 80
    new-instance v0, Landroid/os/RemoteException;

    #@1c
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@1f
    throw v0

    #@20
    .line 84
    :cond_20
    :try_start_20
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_34

    #@26
    .line 85
    const-string v0, "data"

    #@28
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@2b
    move-result v0

    #@2c
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_2f
    .catchall {:try_start_20 .. :try_end_2f} :catchall_38

    #@2f
    move-result-object v5

    #@30
    .line 88
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@33
    .line 90
    :goto_33
    return-object v5

    #@34
    .line 88
    :cond_34
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@37
    goto :goto_33

    #@38
    :catchall_38
    move-exception v0

    #@39
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3c
    throw v0
.end method

.method public static getWithUri(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)Landroid/util/Pair;
    .registers 14
    .parameter "provider"
    .parameter "uri"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentProviderClient;",
            "Landroid/net/Uri;",
            "Landroid/accounts/Account;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 130
    sget-object v2, Landroid/provider/SyncStateContract$Helpers;->DATA_PROJECTION:[Ljava/lang/String;

    #@4
    const-string v3, "account_name=? AND account_type=?"

    #@6
    const/4 v0, 0x2

    #@7
    new-array v4, v0, [Ljava/lang/String;

    #@9
    const/4 v0, 0x0

    #@a
    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@c
    aput-object v1, v4, v0

    #@e
    iget-object v0, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@10
    aput-object v0, v4, v10

    #@12
    move-object v0, p0

    #@13
    move-object v1, p1

    #@14
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@17
    move-result-object v7

    #@18
    .line 133
    .local v7, c:Landroid/database/Cursor;
    if-nez v7, :cond_20

    #@1a
    .line 134
    new-instance v0, Landroid/os/RemoteException;

    #@1c
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@1f
    throw v0

    #@20
    .line 138
    :cond_20
    :try_start_20
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_41

    #@26
    .line 139
    const/4 v0, 0x1

    #@27
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    #@2a
    move-result-wide v8

    #@2b
    .line 140
    .local v8, rowId:J
    const-string v0, "data"

    #@2d
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@30
    move-result v0

    #@31
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    #@34
    move-result-object v6

    #@35
    .line 141
    .local v6, blob:[B
    invoke-static {p1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@38
    move-result-object v0

    #@39
    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_3c
    .catchall {:try_start_20 .. :try_end_3c} :catchall_45

    #@3c
    move-result-object v5

    #@3d
    .line 144
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@40
    .line 146
    .end local v6           #blob:[B
    .end local v8           #rowId:J
    :goto_40
    return-object v5

    #@41
    .line 144
    :cond_41
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@44
    goto :goto_40

    #@45
    :catchall_45
    move-exception v0

    #@46
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@49
    throw v0
.end method

.method public static insert(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;[B)Landroid/net/Uri;
    .registers 7
    .parameter "provider"
    .parameter "uri"
    .parameter "account"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 114
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 115
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "data"

    #@7
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@a
    .line 116
    const-string v1, "account_name"

    #@c
    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 117
    const-string v1, "account_type"

    #@13
    iget-object v2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 118
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1b
    move-result-object v1

    #@1c
    return-object v1
.end method

.method public static newSetOperation(Landroid/net/Uri;Landroid/accounts/Account;[B)Landroid/content/ContentProviderOperation;
    .registers 7
    .parameter "uri"
    .parameter "account"
    .parameter "data"

    #@0
    .prologue
    .line 160
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 161
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "data"

    #@7
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@a
    .line 162
    invoke-static {p0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    #@d
    move-result-object v1

    #@e
    const-string v2, "account_name"

    #@10
    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "account_type"

    #@18
    iget-object v3, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@1a
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    #@25
    move-result-object v1

    #@26
    return-object v1
.end method

.method public static newUpdateOperation(Landroid/net/Uri;[B)Landroid/content/ContentProviderOperation;
    .registers 4
    .parameter "uri"
    .parameter "data"

    #@0
    .prologue
    .line 179
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 180
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "data"

    #@7
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@a
    .line 181
    invoke-static {p0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    #@15
    move-result-object v1

    #@16
    return-object v1
.end method

.method public static set(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;[B)V
    .registers 7
    .parameter "provider"
    .parameter "uri"
    .parameter "account"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 105
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 106
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "data"

    #@7
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@a
    .line 107
    const-string v1, "account_name"

    #@c
    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 108
    const-string v1, "account_type"

    #@13
    iget-object v2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 109
    invoke-virtual {p0, p1, v0}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1b
    .line 110
    return-void
.end method

.method public static update(Landroid/content/ContentProviderClient;Landroid/net/Uri;[B)V
    .registers 6
    .parameter "provider"
    .parameter "uri"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 123
    new-instance v0, Landroid/content/ContentValues;

    #@3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 124
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "data"

    #@8
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@b
    .line 125
    invoke-virtual {p0, p1, v0, v2, v2}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@e
    .line 126
    return-void
.end method
