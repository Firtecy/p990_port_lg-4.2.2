.class public final Landroid/provider/MediaStore$Audio;
.super Ljava/lang/Object;
.source "MediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/MediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Audio"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/MediaStore$Audio$Albums;,
        Landroid/provider/MediaStore$Audio$AlbumColumns;,
        Landroid/provider/MediaStore$Audio$Artists;,
        Landroid/provider/MediaStore$Audio$ArtistColumns;,
        Landroid/provider/MediaStore$Audio$Playlists;,
        Landroid/provider/MediaStore$Audio$PlaylistsColumns;,
        Landroid/provider/MediaStore$Audio$Genres;,
        Landroid/provider/MediaStore$Audio$GenresColumns;,
        Landroid/provider/MediaStore$Audio$Media;,
        Landroid/provider/MediaStore$Audio$AudioColumns;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1154
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1828
    return-void
.end method

.method public static keyFor(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "name"

    #@0
    .prologue
    const/16 v7, 0x2e

    #@2
    .line 1309
    if-eqz p0, :cond_d5

    #@4
    .line 1310
    const/4 v4, 0x0

    #@5
    .line 1311
    .local v4, sortfirst:Z
    const-string v5, "<unknown>"

    #@7
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_10

    #@d
    .line 1312
    const-string v2, "\u0001"

    #@f
    .line 1356
    .end local v4           #sortfirst:Z
    :cond_f
    :goto_f
    return-object v2

    #@10
    .line 1316
    .restart local v4       #sortfirst:Z
    :cond_10
    const-string v5, "\u0001"

    #@12
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@15
    move-result v5

    #@16
    if-eqz v5, :cond_19

    #@18
    .line 1317
    const/4 v4, 0x1

    #@19
    .line 1319
    :cond_19
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@20
    move-result-object p0

    #@21
    .line 1320
    const-string/jumbo v5, "the "

    #@24
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_2f

    #@2a
    .line 1321
    const/4 v5, 0x4

    #@2b
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2e
    move-result-object p0

    #@2f
    .line 1323
    :cond_2f
    const-string v5, "an "

    #@31
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@34
    move-result v5

    #@35
    if-eqz v5, :cond_3c

    #@37
    .line 1324
    const/4 v5, 0x3

    #@38
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3b
    move-result-object p0

    #@3c
    .line 1326
    :cond_3c
    const-string v5, "a "

    #@3e
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@41
    move-result v5

    #@42
    if-eqz v5, :cond_49

    #@44
    .line 1327
    const/4 v5, 0x2

    #@45
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@48
    move-result-object p0

    #@49
    .line 1329
    :cond_49
    const-string v5, ", the"

    #@4b
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@4e
    move-result v5

    #@4f
    if-nez v5, :cond_79

    #@51
    const-string v5, ",the"

    #@53
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@56
    move-result v5

    #@57
    if-nez v5, :cond_79

    #@59
    const-string v5, ", an"

    #@5b
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@5e
    move-result v5

    #@5f
    if-nez v5, :cond_79

    #@61
    const-string v5, ",an"

    #@63
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@66
    move-result v5

    #@67
    if-nez v5, :cond_79

    #@69
    const-string v5, ", a"

    #@6b
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@6e
    move-result v5

    #@6f
    if-nez v5, :cond_79

    #@71
    const-string v5, ",a"

    #@73
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@76
    move-result v5

    #@77
    if-eqz v5, :cond_84

    #@79
    .line 1332
    :cond_79
    const/4 v5, 0x0

    #@7a
    const/16 v6, 0x2c

    #@7c
    invoke-virtual {p0, v6}, Ljava/lang/String;->lastIndexOf(I)I

    #@7f
    move-result v6

    #@80
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@83
    move-result-object p0

    #@84
    .line 1334
    :cond_84
    const-string v5, "[\\[\\]\\(\\)\"\'.,?!]"

    #@86
    const-string v6, ""

    #@88
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@8f
    move-result-object p0

    #@90
    .line 1335
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@93
    move-result v5

    #@94
    if-lez v5, :cond_d1

    #@96
    .line 1339
    new-instance v0, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    .line 1340
    .local v0, b:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@9e
    .line 1341
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@a1
    move-result v3

    #@a2
    .line 1342
    .local v3, nl:I
    const/4 v1, 0x0

    #@a3
    .local v1, i:I
    :goto_a3
    if-ge v1, v3, :cond_b2

    #@a5
    .line 1343
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@a8
    move-result v5

    #@a9
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ac
    .line 1344
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@af
    .line 1342
    add-int/lit8 v1, v1, 0x1

    #@b1
    goto :goto_a3

    #@b2
    .line 1346
    :cond_b2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object p0

    #@b6
    .line 1347
    invoke-static {p0}, Landroid/database/DatabaseUtils;->getCollationKey(Ljava/lang/String;)Ljava/lang/String;

    #@b9
    move-result-object v2

    #@ba
    .line 1348
    .local v2, key:Ljava/lang/String;
    if-eqz v4, :cond_f

    #@bc
    .line 1349
    new-instance v5, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v6, "\u0001"

    #@c3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v5

    #@c7
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v5

    #@cb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v2

    #@cf
    goto/16 :goto_f

    #@d1
    .line 1353
    .end local v0           #b:Ljava/lang/StringBuilder;
    .end local v1           #i:I
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #nl:I
    :cond_d1
    const-string v2, ""

    #@d3
    goto/16 :goto_f

    #@d5
    .line 1356
    .end local v4           #sortfirst:Z
    :cond_d5
    const/4 v2, 0x0

    #@d6
    goto/16 :goto_f
.end method
