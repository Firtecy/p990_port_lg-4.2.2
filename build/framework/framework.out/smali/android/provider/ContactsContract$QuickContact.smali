.class public final Landroid/provider/ContactsContract$QuickContact;
.super Ljava/lang/Object;
.source "ContactsContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QuickContact"
.end annotation


# static fields
.field public static final ACTION_QUICK_CONTACT:Ljava/lang/String; = "com.android.contacts.action.QUICK_CONTACT"

.field public static final EXTRA_EXCLUDE_MIMES:Ljava/lang/String; = "exclude_mimes"

.field public static final EXTRA_MODE:Ljava/lang/String; = "mode"

.field public static final EXTRA_TARGET_RECT:Ljava/lang/String; = "target_rect"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MODE_LARGE:I = 0x3

.field public static final MODE_MEDIUM:I = 0x2

.field public static final MODE_SMALL:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 7641
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static composeQuickContactsIntent(Landroid/content/Context;Landroid/graphics/Rect;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/content/Intent;
    .registers 10
    .parameter "context"
    .parameter "target"
    .parameter "lookupUri"
    .parameter "mode"
    .parameter "excludeMimes"

    #@0
    .prologue
    .line 7718
    move-object v0, p0

    #@1
    .line 7720
    .local v0, actualContext:Landroid/content/Context;
    :goto_1
    instance-of v3, v0, Landroid/content/ContextWrapper;

    #@3
    if-eqz v3, :cond_10

    #@5
    instance-of v3, v0, Landroid/app/Activity;

    #@7
    if-nez v3, :cond_10

    #@9
    .line 7721
    check-cast v0, Landroid/content/ContextWrapper;

    #@b
    .end local v0           #actualContext:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    #@e
    move-result-object v0

    #@f
    .restart local v0       #actualContext:Landroid/content/Context;
    goto :goto_1

    #@10
    .line 7723
    :cond_10
    instance-of v3, v0, Landroid/app/Activity;

    #@12
    if-eqz v3, :cond_33

    #@14
    const/high16 v2, 0x8

    #@16
    .line 7728
    .local v2, intentFlags:I
    :goto_16
    new-instance v3, Landroid/content/Intent;

    #@18
    const-string v4, "com.android.contacts.action.QUICK_CONTACT"

    #@1a
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1d
    invoke-virtual {v3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@20
    move-result-object v1

    #@21
    .line 7730
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@24
    .line 7731
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@27
    .line 7732
    const-string/jumbo v3, "mode"

    #@2a
    invoke-virtual {v1, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2d
    .line 7733
    const-string v3, "exclude_mimes"

    #@2f
    invoke-virtual {v1, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@32
    .line 7734
    return-object v1

    #@33
    .line 7723
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #intentFlags:I
    :cond_33
    const v2, 0x10008000

    #@36
    goto :goto_16
.end method

.method public static composeQuickContactsIntent(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/content/Intent;
    .registers 12
    .parameter "context"
    .parameter "target"
    .parameter "lookupUri"
    .parameter "mode"
    .parameter "excludeMimes"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/high16 v5, 0x3f00

    #@4
    .line 7697
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {v3}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@b
    move-result-object v3

    #@c
    iget v0, v3, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@e
    .line 7698
    .local v0, appScale:F
    const/4 v3, 0x2

    #@f
    new-array v1, v3, [I

    #@11
    .line 7699
    .local v1, pos:[I
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    #@14
    .line 7701
    new-instance v2, Landroid/graphics/Rect;

    #@16
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@19
    .line 7702
    .local v2, rect:Landroid/graphics/Rect;
    aget v3, v1, v4

    #@1b
    int-to-float v3, v3

    #@1c
    mul-float/2addr v3, v0

    #@1d
    add-float/2addr v3, v5

    #@1e
    float-to-int v3, v3

    #@1f
    iput v3, v2, Landroid/graphics/Rect;->left:I

    #@21
    .line 7703
    aget v3, v1, v6

    #@23
    int-to-float v3, v3

    #@24
    mul-float/2addr v3, v0

    #@25
    add-float/2addr v3, v5

    #@26
    float-to-int v3, v3

    #@27
    iput v3, v2, Landroid/graphics/Rect;->top:I

    #@29
    .line 7704
    aget v3, v1, v4

    #@2b
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    #@2e
    move-result v4

    #@2f
    add-int/2addr v3, v4

    #@30
    int-to-float v3, v3

    #@31
    mul-float/2addr v3, v0

    #@32
    add-float/2addr v3, v5

    #@33
    float-to-int v3, v3

    #@34
    iput v3, v2, Landroid/graphics/Rect;->right:I

    #@36
    .line 7705
    aget v3, v1, v6

    #@38
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@3b
    move-result v4

    #@3c
    add-int/2addr v3, v4

    #@3d
    int-to-float v3, v3

    #@3e
    mul-float/2addr v3, v0

    #@3f
    add-float/2addr v3, v5

    #@40
    float-to-int v3, v3

    #@41
    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    #@43
    .line 7707
    invoke-static {p0, v2, p2, p3, p4}, Landroid/provider/ContactsContract$QuickContact;->composeQuickContactsIntent(Landroid/content/Context;Landroid/graphics/Rect;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/content/Intent;

    #@46
    move-result-object v3

    #@47
    return-object v3
.end method

.method public static showQuickContact(Landroid/content/Context;Landroid/graphics/Rect;Landroid/net/Uri;I[Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "target"
    .parameter "lookupUri"
    .parameter "mode"
    .parameter "excludeMimes"

    #@0
    .prologue
    .line 7796
    invoke-static {p0, p1, p2, p3, p4}, Landroid/provider/ContactsContract$QuickContact;->composeQuickContactsIntent(Landroid/content/Context;Landroid/graphics/Rect;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 7798
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@7
    .line 7799
    return-void
.end method

.method public static showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "target"
    .parameter "lookupUri"
    .parameter "mode"
    .parameter "excludeMimes"

    #@0
    .prologue
    .line 7763
    invoke-static {p0, p1, p2, p3, p4}, Landroid/provider/ContactsContract$QuickContact;->composeQuickContactsIntent(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 7765
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@7
    .line 7766
    return-void
.end method
