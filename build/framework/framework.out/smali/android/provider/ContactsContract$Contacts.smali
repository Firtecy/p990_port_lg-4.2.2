.class public Landroid/provider/ContactsContract$Contacts;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/ContactsContract$ContactsColumns;
.implements Landroid/provider/ContactsContract$ContactOptionsColumns;
.implements Landroid/provider/ContactsContract$ContactNameColumns;
.implements Landroid/provider/ContactsContract$ContactStatusColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Contacts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/ContactsContract$Contacts$Photo;,
        Landroid/provider/ContactsContract$Contacts$AggregationSuggestions;,
        Landroid/provider/ContactsContract$Contacts$StreamItems;,
        Landroid/provider/ContactsContract$Contacts$Entity;,
        Landroid/provider/ContactsContract$Contacts$Data;
    }
.end annotation


# static fields
.field public static final CONTENT_FILTER_URI:Landroid/net/Uri; = null

.field public static final CONTENT_FREQUENT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_GROUP_URI:Landroid/net/Uri; = null

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/contact"

.field public static final CONTENT_LOOKUP_URI:Landroid/net/Uri; = null

.field public static final CONTENT_MULTI_VCARD_URI:Landroid/net/Uri; = null

.field public static final CONTENT_STREQUENT_FILTER_URI:Landroid/net/Uri; = null

.field public static final CONTENT_STREQUENT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/contact"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_VCARD_TYPE:Ljava/lang/String; = "text/x-vcard"

.field public static final CONTENT_VCARD_URI:Landroid/net/Uri; = null

.field public static final QUERY_PARAMETER_VCARD_NO_PHOTO:Ljava/lang/String; = "nophoto"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 1438
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@2
    const-string v1, "contacts"

    #@4
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@a
    .line 1456
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const-string/jumbo v1, "lookup"

    #@f
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v0

    #@13
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    #@15
    .line 1467
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@17
    const-string v1, "as_vcard"

    #@19
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@1c
    move-result-object v0

    #@1d
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_VCARD_URI:Landroid/net/Uri;

    #@1f
    .line 1495
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@21
    const-string v1, "as_multi_vcard"

    #@23
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@26
    move-result-object v0

    #@27
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_MULTI_VCARD_URI:Landroid/net/Uri;

    #@29
    .line 1587
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@2b
    const-string v1, "filter"

    #@2d
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@30
    move-result-object v0

    #@31
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@33
    .line 1595
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@35
    const-string/jumbo v1, "strequent"

    #@38
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@3b
    move-result-object v0

    #@3c
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_STREQUENT_URI:Landroid/net/Uri;

    #@3e
    .line 1602
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@40
    const-string v1, "frequent"

    #@42
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@45
    move-result-object v0

    #@46
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_FREQUENT_URI:Landroid/net/Uri;

    #@48
    .line 1611
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_STREQUENT_URI:Landroid/net/Uri;

    #@4a
    const-string v1, "filter"

    #@4c
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@4f
    move-result-object v0

    #@50
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_STREQUENT_FILTER_URI:Landroid/net/Uri;

    #@52
    .line 1614
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@54
    const-string v1, "group"

    #@56
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@59
    move-result-object v0

    #@5a
    sput-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_GROUP_URI:Landroid/net/Uri;

    #@5c
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1433
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getLookupUri(JLjava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "contactId"
    .parameter "lookupKey"

    #@0
    .prologue
    .line 1530
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    #@2
    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static getLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;
    .registers 12
    .parameter "resolver"
    .parameter "contactUri"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 1506
    const/4 v0, 0x2

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const-string/jumbo v0, "lookup"

    #@9
    aput-object v0, v2, v1

    #@b
    const-string v0, "_id"

    #@d
    aput-object v0, v2, v4

    #@f
    move-object v0, p0

    #@10
    move-object v1, p1

    #@11
    move-object v4, v3

    #@12
    move-object v5, v3

    #@13
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@16
    move-result-object v6

    #@17
    .line 1509
    .local v6, c:Landroid/database/Cursor;
    if-nez v6, :cond_1a

    #@19
    .line 1522
    :goto_19
    return-object v3

    #@1a
    .line 1514
    :cond_1a
    :try_start_1a
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_32

    #@20
    .line 1515
    const/4 v0, 0x0

    #@21
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v9

    #@25
    .line 1516
    .local v9, lookupKey:Ljava/lang/String;
    const/4 v0, 0x1

    #@26
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@29
    move-result-wide v7

    #@2a
    .line 1517
    .local v7, contactId:J
    invoke-static {v7, v8, v9}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;
    :try_end_2d
    .catchall {:try_start_1a .. :try_end_2d} :catchall_36

    #@2d
    move-result-object v3

    #@2e
    .line 1520
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@31
    goto :goto_19

    #@32
    .end local v7           #contactId:J
    .end local v9           #lookupKey:Ljava/lang/String;
    :cond_32
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@35
    goto :goto_19

    #@36
    :catchall_36
    move-exception v0

    #@37
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3a
    throw v0
.end method

.method public static lookupContact(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;
    .registers 11
    .parameter "resolver"
    .parameter "lookupUri"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 1540
    if-nez p1, :cond_5

    #@4
    .line 1557
    :cond_4
    :goto_4
    return-object v3

    #@5
    .line 1544
    :cond_5
    const/4 v0, 0x1

    #@6
    new-array v2, v0, [Ljava/lang/String;

    #@8
    const-string v0, "_id"

    #@a
    aput-object v0, v2, v1

    #@c
    move-object v0, p0

    #@d
    move-object v1, p1

    #@e
    move-object v4, v3

    #@f
    move-object v5, v3

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v6

    #@14
    .line 1545
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_4

    #@16
    .line 1550
    :try_start_16
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_2b

    #@1c
    .line 1551
    const/4 v0, 0x0

    #@1d
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@20
    move-result-wide v7

    #@21
    .line 1552
    .local v7, contactId:J
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@23
    invoke-static {v0, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_26
    .catchall {:try_start_16 .. :try_end_26} :catchall_2f

    #@26
    move-result-object v3

    #@27
    .line 1555
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2a
    goto :goto_4

    #@2b
    .end local v7           #contactId:J
    :cond_2b
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2e
    goto :goto_4

    #@2f
    :catchall_2f
    move-exception v0

    #@30
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@33
    throw v0
.end method

.method public static markAsContacted(Landroid/content/ContentResolver;J)V
    .registers 9
    .parameter "resolver"
    .parameter "contactId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1574
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@3
    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@6
    move-result-object v0

    #@7
    .line 1575
    .local v0, uri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@9
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@c
    .line 1577
    .local v1, values:Landroid/content/ContentValues;
    const-string/jumbo v2, "last_time_contacted"

    #@f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@12
    move-result-wide v3

    #@13
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1a
    .line 1578
    invoke-virtual {p0, v0, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1d
    .line 1579
    return-void
.end method

.method public static openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;
    .registers 3
    .parameter "cr"
    .parameter "contactUri"

    #@0
    .prologue
    .line 2032
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;
    .registers 13
    .parameter "cr"
    .parameter "contactUri"
    .parameter "preferHighres"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 1985
    if-eqz p2, :cond_17

    #@4
    .line 1986
    const-string v0, "display_photo"

    #@6
    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v8

    #@a
    .line 1990
    .local v8, displayPhotoUri:Landroid/net/Uri;
    :try_start_a
    const-string/jumbo v0, "r"

    #@d
    invoke-virtual {p0, v8, v0}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@10
    move-result-object v9

    #@11
    .line 1991
    .local v9, fd:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v9}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_14} :catch_16

    #@14
    move-result-object v3

    #@15
    .line 2016
    .end local v8           #displayPhotoUri:Landroid/net/Uri;
    .end local v9           #fd:Landroid/content/res/AssetFileDescriptor;
    :cond_15
    :goto_15
    return-object v3

    #@16
    .line 1992
    .restart local v8       #displayPhotoUri:Landroid/net/Uri;
    :catch_16
    move-exception v0

    #@17
    .line 1997
    .end local v8           #displayPhotoUri:Landroid/net/Uri;
    :cond_17
    const-string/jumbo v0, "photo"

    #@1a
    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v1

    #@1e
    .line 1998
    .local v1, photoUri:Landroid/net/Uri;
    if-eqz v1, :cond_15

    #@20
    .line 2001
    const/4 v0, 0x1

    #@21
    new-array v2, v0, [Ljava/lang/String;

    #@23
    const-string v0, "data15"

    #@25
    aput-object v0, v2, v4

    #@27
    move-object v0, p0

    #@28
    move-object v4, v3

    #@29
    move-object v5, v3

    #@2a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2d
    move-result-object v6

    #@2e
    .line 2006
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_36

    #@30
    :try_start_30
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_54

    #@33
    move-result v0

    #@34
    if-nez v0, :cond_3c

    #@36
    .line 2015
    :cond_36
    if-eqz v6, :cond_15

    #@38
    .line 2016
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3b
    goto :goto_15

    #@3c
    .line 2009
    :cond_3c
    const/4 v0, 0x0

    #@3d
    :try_start_3d
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_54

    #@40
    move-result-object v7

    #@41
    .line 2010
    .local v7, data:[B
    if-nez v7, :cond_49

    #@43
    .line 2015
    if-eqz v6, :cond_15

    #@45
    .line 2016
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@48
    goto :goto_15

    #@49
    .line 2013
    :cond_49
    :try_start_49
    new-instance v3, Ljava/io/ByteArrayInputStream;

    #@4b
    invoke-direct {v3, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_4e
    .catchall {:try_start_49 .. :try_end_4e} :catchall_54

    #@4e
    .line 2015
    if-eqz v6, :cond_15

    #@50
    .line 2016
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@53
    goto :goto_15

    #@54
    .line 2015
    .end local v7           #data:[B
    :catchall_54
    move-exception v0

    #@55
    if-eqz v6, :cond_5a

    #@57
    .line 2016
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5a
    :cond_5a
    throw v0
.end method
