.class public final Landroid/provider/CalendarContract$Reminders;
.super Ljava/lang/Object;
.source "CalendarContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/CalendarContract$RemindersColumns;
.implements Landroid/provider/CalendarContract$EventsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/CalendarContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reminders"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field private static final REMINDERS_WHERE:Ljava/lang/String; = "event_id=?"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2084
    const-string v0, "content://com.android.calendar/reminders"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2089
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final query(Landroid/content/ContentResolver;J[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 10
    .parameter "cr"
    .parameter "eventId"
    .parameter "projection"

    #@0
    .prologue
    .line 2101
    const/4 v0, 0x1

    #@1
    new-array v4, v0, [Ljava/lang/String;

    #@3
    const/4 v0, 0x0

    #@4
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    aput-object v1, v4, v0

    #@a
    .line 2102
    .local v4, remArgs:[Ljava/lang/String;
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    #@c
    const-string v3, "event_id=?"

    #@e
    const/4 v5, 0x0

    #@f
    move-object v0, p0

    #@10
    move-object v2, p3

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v0

    #@15
    return-object v0
.end method
