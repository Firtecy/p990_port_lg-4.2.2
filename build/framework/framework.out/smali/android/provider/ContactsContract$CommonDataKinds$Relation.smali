.class public final Landroid/provider/ContactsContract$CommonDataKinds$Relation;
.super Ljava/lang/Object;
.source "ContactsContract.java"

# interfaces
.implements Landroid/provider/ContactsContract$DataColumnsWithJoins;
.implements Landroid/provider/ContactsContract$CommonDataKinds$CommonColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/ContactsContract$CommonDataKinds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Relation"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/relation"

.field public static final NAME:Ljava/lang/String; = "data1"

.field public static final TYPE_ASSISTANT:I = 0x1

.field public static final TYPE_BROTHER:I = 0x2

.field public static final TYPE_CHILD:I = 0x3

.field public static final TYPE_DOMESTIC_PARTNER:I = 0x4

.field public static final TYPE_FATHER:I = 0x5

.field public static final TYPE_FRIEND:I = 0x6

.field public static final TYPE_MANAGER:I = 0x7

.field public static final TYPE_MOTHER:I = 0x8

.field public static final TYPE_PARENT:I = 0x9

.field public static final TYPE_PARTNER:I = 0xa

.field public static final TYPE_REFERRED_BY:I = 0xb

.field public static final TYPE_RELATIVE:I = 0xc

.field public static final TYPE_SISTER:I = 0xd

.field public static final TYPE_SPOUSE:I = 0xe


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 6325
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "res"
    .parameter "type"
    .parameter "label"

    #@0
    .prologue
    .line 6384
    if-nez p1, :cond_9

    #@2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 6388
    .end local p2
    :goto_8
    return-object p2

    #@9
    .line 6387
    .restart local p2
    :cond_9
    invoke-static {p1}, Landroid/provider/ContactsContract$CommonDataKinds$Relation;->getTypeLabelResource(I)I

    #@c
    move-result v0

    #@d
    .line 6388
    .local v0, labelRes:I
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@10
    move-result-object p2

    #@11
    goto :goto_8
.end method

.method public static final getTypeLabelResource(I)I
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 6356
    packed-switch p0, :pswitch_data_40

    #@3
    .line 6373
    const v0, 0x10402e8

    #@6
    :goto_6
    return v0

    #@7
    .line 6357
    :pswitch_7
    const v0, 0x10402ea

    #@a
    goto :goto_6

    #@b
    .line 6358
    :pswitch_b
    const v0, 0x10402eb

    #@e
    goto :goto_6

    #@f
    .line 6359
    :pswitch_f
    const v0, 0x10402ec

    #@12
    goto :goto_6

    #@13
    .line 6361
    :pswitch_13
    const v0, 0x10402ed

    #@16
    goto :goto_6

    #@17
    .line 6362
    :pswitch_17
    const v0, 0x10402ee

    #@1a
    goto :goto_6

    #@1b
    .line 6363
    :pswitch_1b
    const v0, 0x10402ef

    #@1e
    goto :goto_6

    #@1f
    .line 6364
    :pswitch_1f
    const v0, 0x10402f0

    #@22
    goto :goto_6

    #@23
    .line 6365
    :pswitch_23
    const v0, 0x10402f1

    #@26
    goto :goto_6

    #@27
    .line 6366
    :pswitch_27
    const v0, 0x10402f2

    #@2a
    goto :goto_6

    #@2b
    .line 6367
    :pswitch_2b
    const v0, 0x10402f3

    #@2e
    goto :goto_6

    #@2f
    .line 6369
    :pswitch_2f
    const v0, 0x10402f4

    #@32
    goto :goto_6

    #@33
    .line 6370
    :pswitch_33
    const v0, 0x10402f5

    #@36
    goto :goto_6

    #@37
    .line 6371
    :pswitch_37
    const v0, 0x10402f6

    #@3a
    goto :goto_6

    #@3b
    .line 6372
    :pswitch_3b
    const v0, 0x10402f7

    #@3e
    goto :goto_6

    #@3f
    .line 6356
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_13
        :pswitch_17
        :pswitch_1b
        :pswitch_1f
        :pswitch_23
        :pswitch_27
        :pswitch_2b
        :pswitch_2f
        :pswitch_33
        :pswitch_37
        :pswitch_3b
    .end packed-switch
.end method
