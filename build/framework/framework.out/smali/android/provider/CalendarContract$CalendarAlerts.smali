.class public final Landroid/provider/CalendarContract$CalendarAlerts;
.super Ljava/lang/Object;
.source "CalendarContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Landroid/provider/CalendarContract$CalendarAlertsColumns;
.implements Landroid/provider/CalendarContract$EventsColumns;
.implements Landroid/provider/CalendarContract$CalendarColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/CalendarContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CalendarAlerts"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final CONTENT_URI_BY_INSTANCE:Landroid/net/Uri; = null

.field private static final DEBUG:Z = false

.field private static final SORT_ORDER_ALARMTIME_ASC:Ljava/lang/String; = "alarmTime ASC"

.field public static final TABLE_NAME:Ljava/lang/String; = "CalendarAlerts"

.field private static final WHERE_ALARM_EXISTS:Ljava/lang/String; = "event_id=? AND begin=? AND alarmTime=?"

.field private static final WHERE_FINDNEXTALARMTIME:Ljava/lang/String; = "alarmTime>=?"

.field private static final WHERE_RESCHEDULE_MISSED_ALARMS:Ljava/lang/String; = "state=0 AND alarmTime<? AND alarmTime>? AND end>=?"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2212
    const-string v0, "content://com.android.calendar/calendar_alerts"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 2238
    const-string v0, "content://com.android.calendar/calendar_alerts/by_instance"

    #@a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI_BY_INSTANCE:Landroid/net/Uri;

    #@10
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2218
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final alarmExists(Landroid/content/ContentResolver;JJJ)Z
    .registers 16
    .parameter "cr"
    .parameter "eventId"
    .parameter "begin"
    .parameter "alarmTime"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2403
    new-array v2, v8, [Ljava/lang/String;

    #@4
    const-string v0, "alarmTime"

    #@6
    aput-object v0, v2, v5

    #@8
    .line 2404
    .local v2, projection:[Ljava/lang/String;
    sget-object v1, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    #@a
    const-string v3, "event_id=? AND begin=? AND alarmTime=?"

    #@c
    const/4 v0, 0x3

    #@d
    new-array v4, v0, [Ljava/lang/String;

    #@f
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    aput-object v0, v4, v5

    #@15
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    aput-object v0, v4, v8

    #@1b
    const/4 v0, 0x2

    #@1c
    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    aput-object v5, v4, v0

    #@22
    const/4 v5, 0x0

    #@23
    move-object v0, p0

    #@24
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@27
    move-result-object v6

    #@28
    .line 2408
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v7, 0x0

    #@29
    .line 2410
    .local v7, found:Z
    if-eqz v6, :cond_32

    #@2b
    :try_start_2b
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_38

    #@2e
    move-result v0

    #@2f
    if-lez v0, :cond_32

    #@31
    .line 2411
    const/4 v7, 0x1

    #@32
    .line 2414
    :cond_32
    if-eqz v6, :cond_37

    #@34
    .line 2415
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@37
    .line 2418
    :cond_37
    return v7

    #@38
    .line 2414
    :catchall_38
    move-exception v0

    #@39
    if-eqz v6, :cond_3e

    #@3b
    .line 2415
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3e
    :cond_3e
    throw v0
.end method

.method public static final findNextAlarmTime(Landroid/content/ContentResolver;J)J
    .registers 13
    .parameter "cr"
    .parameter "millis"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2278
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "alarmTime>="

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v9

    #@15
    .line 2281
    .local v9, selection:Ljava/lang/String;
    new-array v2, v4, [Ljava/lang/String;

    #@17
    const-string v0, "alarmTime"

    #@19
    aput-object v0, v2, v5

    #@1b
    .line 2282
    .local v2, projection:[Ljava/lang/String;
    sget-object v1, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    #@1d
    const-string v3, "alarmTime>=?"

    #@1f
    new-array v4, v4, [Ljava/lang/String;

    #@21
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    aput-object v0, v4, v5

    #@27
    const-string v5, "alarmTime ASC"

    #@29
    move-object v0, p0

    #@2a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2d
    move-result-object v8

    #@2e
    .line 2286
    .local v8, cursor:Landroid/database/Cursor;
    const-wide/16 v6, -0x1

    #@30
    .line 2288
    .local v6, alarmTime:J
    if-eqz v8, :cond_3d

    #@32
    :try_start_32
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_3d

    #@38
    .line 2289
    const/4 v0, 0x0

    #@39
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_3c
    .catchall {:try_start_32 .. :try_end_3c} :catchall_43

    #@3c
    move-result-wide v6

    #@3d
    .line 2292
    :cond_3d
    if-eqz v8, :cond_42

    #@3f
    .line 2293
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@42
    .line 2296
    :cond_42
    return-wide v6

    #@43
    .line 2292
    :catchall_43
    move-exception v0

    #@44
    if-eqz v8, :cond_49

    #@46
    .line 2293
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@49
    :cond_49
    throw v0
.end method

.method public static final insert(Landroid/content/ContentResolver;JJJJI)Landroid/net/Uri;
    .registers 16
    .parameter "cr"
    .parameter "eventId"
    .parameter "begin"
    .parameter "end"
    .parameter "alarmTime"
    .parameter "minutes"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2251
    new-instance v2, Landroid/content/ContentValues;

    #@3
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 2252
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "event_id"

    #@8
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@f
    .line 2253
    const-string v3, "begin"

    #@11
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@18
    .line 2254
    const-string v3, "end"

    #@1a
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@21
    .line 2255
    const-string v3, "alarmTime"

    #@23
    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2a
    .line 2256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2d
    move-result-wide v0

    #@2e
    .line 2257
    .local v0, currentTime:J
    const-string v3, "creationTime"

    #@30
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@37
    .line 2258
    const-string/jumbo v3, "receivedTime"

    #@3a
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@41
    .line 2259
    const-string/jumbo v3, "notifyTime"

    #@44
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@4b
    .line 2260
    const-string/jumbo v3, "state"

    #@4e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@55
    .line 2261
    const-string/jumbo v3, "minutes"

    #@58
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5f
    .line 2262
    sget-object v3, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    #@61
    invoke-virtual {p0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@64
    move-result-object v3

    #@65
    return-object v3
.end method

.method public static final rescheduleMissedAlarms(Landroid/content/ContentResolver;Landroid/content/Context;Landroid/app/AlarmManager;)V
    .registers 20
    .parameter "cr"
    .parameter "context"
    .parameter "manager"

    #@0
    .prologue
    .line 2314
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v15

    #@4
    .line 2315
    .local v15, now:J
    const-wide/32 v2, 0x5265c00

    #@7
    sub-long v10, v15, v2

    #@9
    .line 2316
    .local v10, ancient:J
    const/4 v2, 0x1

    #@a
    new-array v4, v2, [Ljava/lang/String;

    #@c
    const/4 v2, 0x0

    #@d
    const-string v3, "alarmTime"

    #@f
    aput-object v3, v4, v2

    #@11
    .line 2322
    .local v4, projection:[Ljava/lang/String;
    sget-object v3, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    #@13
    const-string/jumbo v5, "state=0 AND alarmTime<? AND alarmTime>? AND end>=?"

    #@16
    const/4 v2, 0x3

    #@17
    new-array v6, v2, [Ljava/lang/String;

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    aput-object v7, v6, v2

    #@20
    const/4 v2, 0x1

    #@21
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    aput-object v7, v6, v2

    #@27
    const/4 v2, 0x2

    #@28
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2b
    move-result-object v7

    #@2c
    aput-object v7, v6, v2

    #@2e
    const-string v7, "alarmTime ASC"

    #@30
    move-object/from16 v2, p0

    #@32
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@35
    move-result-object v12

    #@36
    .line 2326
    .local v12, cursor:Landroid/database/Cursor;
    if-nez v12, :cond_39

    #@38
    .line 2350
    :goto_38
    return-void

    #@39
    .line 2335
    :cond_39
    const-wide/16 v8, -0x1

    #@3b
    .line 2337
    .local v8, alarmTime:J
    :cond_3b
    :goto_3b
    :try_start_3b
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_53

    #@41
    .line 2338
    const/4 v2, 0x0

    #@42
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    #@45
    move-result-wide v13

    #@46
    .line 2339
    .local v13, newAlarmTime:J
    cmp-long v2, v8, v13

    #@48
    if-eqz v2, :cond_3b

    #@4a
    .line 2343
    move-object/from16 v0, p1

    #@4c
    move-object/from16 v1, p2

    #@4e
    invoke-static {v0, v1, v13, v14}, Landroid/provider/CalendarContract$CalendarAlerts;->scheduleAlarm(Landroid/content/Context;Landroid/app/AlarmManager;J)V
    :try_end_51
    .catchall {:try_start_3b .. :try_end_51} :catchall_57

    #@51
    .line 2344
    move-wide v8, v13

    #@52
    goto :goto_3b

    #@53
    .line 2348
    .end local v13           #newAlarmTime:J
    :cond_53
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@56
    goto :goto_38

    #@57
    :catchall_57
    move-exception v2

    #@58
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@5b
    throw v2
.end method

.method public static scheduleAlarm(Landroid/content/Context;Landroid/app/AlarmManager;J)V
    .registers 8
    .parameter "context"
    .parameter "manager"
    .parameter "alarmTime"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2374
    if-nez p1, :cond_b

    #@3
    .line 2375
    const-string v2, "alarm"

    #@5
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object p1

    #@9
    .end local p1
    check-cast p1, Landroid/app/AlarmManager;

    #@b
    .line 2378
    .restart local p1
    :cond_b
    new-instance v0, Landroid/content/Intent;

    #@d
    const-string v2, "android.intent.action.EVENT_REMINDER"

    #@f
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@12
    .line 2379
    .local v0, intent:Landroid/content/Intent;
    sget-object v2, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    #@14
    invoke-static {v2, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@1b
    .line 2380
    const-string v2, "alarmTime"

    #@1d
    invoke-virtual {v0, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@20
    .line 2381
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@23
    move-result-object v1

    #@24
    .line 2382
    .local v1, pi:Landroid/app/PendingIntent;
    invoke-virtual {p1, v3, p2, p3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@27
    .line 2383
    return-void
.end method
