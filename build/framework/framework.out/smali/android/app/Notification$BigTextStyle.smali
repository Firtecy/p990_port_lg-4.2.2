.class public Landroid/app/Notification$BigTextStyle;
.super Landroid/app/Notification$Style;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BigTextStyle"
.end annotation


# instance fields
.field private mBigText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 1860
    invoke-direct {p0}, Landroid/app/Notification$Style;-><init>()V

    #@3
    .line 1861
    return-void
.end method

.method public constructor <init>(Landroid/app/Notification$Builder;)V
    .registers 2
    .parameter "builder"

    #@0
    .prologue
    .line 1863
    invoke-direct {p0}, Landroid/app/Notification$Style;-><init>()V

    #@3
    .line 1864
    invoke-virtual {p0, p1}, Landroid/app/Notification$BigTextStyle;->setBuilder(Landroid/app/Notification$Builder;)V

    #@6
    .line 1865
    return-void
.end method

.method private makeBigContentView()Landroid/widget/RemoteViews;
    .registers 9

    #@0
    .prologue
    const v7, 0x102033b

    #@3
    const/4 v2, 0x0

    #@4
    .line 1895
    iget-object v1, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@6
    invoke-static {v1}, Landroid/app/Notification$Builder;->access$700(Landroid/app/Notification$Builder;)Ljava/lang/CharSequence;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_3e

    #@c
    iget-object v1, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@e
    invoke-static {v1}, Landroid/app/Notification$Builder;->access$400(Landroid/app/Notification$Builder;)Ljava/lang/CharSequence;

    #@11
    move-result-object v1

    #@12
    if-eqz v1, :cond_3e

    #@14
    const/4 v6, 0x1

    #@15
    .line 1896
    .local v6, hadThreeLines:Z
    :goto_15
    iget-object v1, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@17
    const/4 v3, 0x0

    #@18
    invoke-static {v1, v3}, Landroid/app/Notification$Builder;->access$702(Landroid/app/Notification$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@1b
    .line 1898
    const v1, 0x1090095

    #@1e
    invoke-virtual {p0, v1}, Landroid/app/Notification$BigTextStyle;->getStandardView(I)Landroid/widget/RemoteViews;

    #@21
    move-result-object v0

    #@22
    .line 1900
    .local v0, contentView:Landroid/widget/RemoteViews;
    if-eqz v6, :cond_2d

    #@24
    .line 1902
    const v1, 0x1020337

    #@27
    move v3, v2

    #@28
    move v4, v2

    #@29
    move v5, v2

    #@2a
    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    #@2d
    .line 1905
    :cond_2d
    iget-object v1, p0, Landroid/app/Notification$BigTextStyle;->mBigText:Ljava/lang/CharSequence;

    #@2f
    invoke-virtual {v0, v7, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@32
    .line 1906
    invoke-virtual {v0, v7, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@35
    .line 1907
    const v1, 0x1020015

    #@38
    const/16 v2, 0x8

    #@3a
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@3d
    .line 1909
    return-object v0

    #@3e
    .end local v0           #contentView:Landroid/widget/RemoteViews;
    .end local v6           #hadThreeLines:Z
    :cond_3e
    move v6, v2

    #@3f
    .line 1895
    goto :goto_15
.end method


# virtual methods
.method public bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
    .registers 2
    .parameter "cs"

    #@0
    .prologue
    .line 1889
    iput-object p1, p0, Landroid/app/Notification$BigTextStyle;->mBigText:Ljava/lang/CharSequence;

    #@2
    .line 1890
    return-object p0
.end method

.method public build()Landroid/app/Notification;
    .registers 3

    #@0
    .prologue
    .line 1914
    invoke-virtual {p0}, Landroid/app/Notification$BigTextStyle;->checkBuilder()V

    #@3
    .line 1915
    iget-object v1, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@5
    invoke-static {v1}, Landroid/app/Notification$Builder;->access$500(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    #@8
    move-result-object v0

    #@9
    .line 1916
    .local v0, wip:Landroid/app/Notification;
    invoke-direct {p0}, Landroid/app/Notification$BigTextStyle;->makeBigContentView()Landroid/widget/RemoteViews;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@f
    .line 1917
    return-object v0
.end method

.method public setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 1872
    invoke-virtual {p0, p1}, Landroid/app/Notification$BigTextStyle;->internalSetBigContentTitle(Ljava/lang/CharSequence;)V

    #@3
    .line 1873
    return-object p0
.end method

.method public setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
    .registers 2
    .parameter "cs"

    #@0
    .prologue
    .line 1880
    invoke-virtual {p0, p1}, Landroid/app/Notification$BigTextStyle;->internalSetSummaryText(Ljava/lang/CharSequence;)V

    #@3
    .line 1881
    return-object p0
.end method
