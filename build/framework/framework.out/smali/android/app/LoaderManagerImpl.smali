.class Landroid/app/LoaderManagerImpl;
.super Landroid/app/LoaderManager;
.source "LoaderManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/LoaderManagerImpl$LoaderInfo;
    }
.end annotation


# static fields
.field static DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "LoaderManager"


# instance fields
.field mActivity:Landroid/app/Activity;

.field mCreatingLoader:Z

.field final mInactiveLoaders:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/app/LoaderManagerImpl$LoaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mLoaders:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/app/LoaderManagerImpl$LoaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field mRetaining:Z

.field mRetainingStarted:Z

.field mStarted:Z

.field final mWho:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 202
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@3
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/app/Activity;Z)V
    .registers 5
    .parameter "who"
    .parameter "activity"
    .parameter "started"

    #@0
    .prologue
    .line 533
    invoke-direct {p0}, Landroid/app/LoaderManager;-><init>()V

    #@3
    .line 207
    new-instance v0, Landroid/util/SparseArray;

    #@5
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@8
    iput-object v0, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@a
    .line 213
    new-instance v0, Landroid/util/SparseArray;

    #@c
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@f
    iput-object v0, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@11
    .line 534
    iput-object p1, p0, Landroid/app/LoaderManagerImpl;->mWho:Ljava/lang/String;

    #@13
    .line 535
    iput-object p2, p0, Landroid/app/LoaderManagerImpl;->mActivity:Landroid/app/Activity;

    #@15
    .line 536
    iput-boolean p3, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@17
    .line 537
    return-void
.end method

.method private createAndInstallLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/app/LoaderManagerImpl$LoaderInfo;
    .registers 7
    .parameter "id"
    .parameter "args"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/app/LoaderManagerImpl$LoaderInfo;"
        }
    .end annotation

    #@0
    .prologue
    .local p3, callback:Landroid/app/LoaderManager$LoaderCallbacks;,"Landroid/app/LoaderManager$LoaderCallbacks<Ljava/lang/Object;>;"
    const/4 v2, 0x0

    #@1
    .line 554
    const/4 v1, 0x1

    #@2
    :try_start_2
    iput-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@4
    .line 555
    invoke-direct {p0, p1, p2, p3}, Landroid/app/LoaderManagerImpl;->createLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@7
    move-result-object v0

    #@8
    .line 556
    .local v0, info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    invoke-virtual {p0, v0}, Landroid/app/LoaderManagerImpl;->installLoader(Landroid/app/LoaderManagerImpl$LoaderInfo;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_e

    #@b
    .line 559
    iput-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@d
    .line 557
    return-object v0

    #@e
    .line 559
    .end local v0           #info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :catchall_e
    move-exception v1

    #@f
    iput-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@11
    throw v1
.end method

.method private createLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/app/LoaderManagerImpl$LoaderInfo;
    .registers 6
    .parameter "id"
    .parameter "args"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/app/LoaderManagerImpl$LoaderInfo;"
        }
    .end annotation

    #@0
    .prologue
    .line 545
    .local p3, callback:Landroid/app/LoaderManager$LoaderCallbacks;,"Landroid/app/LoaderManager$LoaderCallbacks<Ljava/lang/Object;>;"
    new-instance v0, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/app/LoaderManagerImpl$LoaderInfo;-><init>(Landroid/app/LoaderManagerImpl;ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)V

    #@5
    .line 546
    .local v0, info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    invoke-interface {p3, p1, p2}, Landroid/app/LoaderManager$LoaderCallbacks;->onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;

    #@8
    move-result-object v1

    #@9
    .line 547
    .local v1, loader:Landroid/content/Loader;,"Landroid/content/Loader<Ljava/lang/Object;>;"
    iput-object v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@b
    .line 548
    return-object v0
.end method


# virtual methods
.method public destroyLoader(I)V
    .registers 7
    .parameter "id"

    #@0
    .prologue
    .line 716
    iget-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@2
    if-eqz v2, :cond_c

    #@4
    .line 717
    new-instance v2, Ljava/lang/IllegalStateException;

    #@6
    const-string v3, "Called while creating a loader"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 720
    :cond_c
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@e
    if-eqz v2, :cond_32

    #@10
    const-string v2, "LoaderManager"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "destroyLoader in "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, " of "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 721
    :cond_32
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@34
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@37
    move-result v0

    #@38
    .line 722
    .local v0, idx:I
    if-ltz v0, :cond_4a

    #@3a
    .line 723
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@3c
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3f
    move-result-object v1

    #@40
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@42
    .line 724
    .local v1, info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@44
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->removeAt(I)V

    #@47
    .line 725
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@4a
    .line 727
    .end local v1           #info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_4a
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@4c
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    #@4f
    move-result v0

    #@50
    .line 728
    if-ltz v0, :cond_62

    #@52
    .line 729
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@54
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@57
    move-result-object v1

    #@58
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@5a
    .line 730
    .restart local v1       #info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@5c
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->removeAt(I)V

    #@5f
    .line 731
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@62
    .line 733
    .end local v1           #info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_62
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mActivity:Landroid/app/Activity;

    #@64
    if-eqz v2, :cond_73

    #@66
    invoke-virtual {p0}, Landroid/app/LoaderManagerImpl;->hasRunningLoaders()Z

    #@69
    move-result v2

    #@6a
    if-nez v2, :cond_73

    #@6c
    .line 734
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mActivity:Landroid/app/Activity;

    #@6e
    iget-object v2, v2, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@70
    invoke-virtual {v2}, Landroid/app/FragmentManagerImpl;->startPendingDeferredFragments()V

    #@73
    .line 736
    :cond_73
    return-void
.end method

.method doDestroy()V
    .registers 5

    #@0
    .prologue
    .line 831
    iget-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    #@2
    if-nez v1, :cond_38

    #@4
    .line 832
    sget-boolean v1, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@6
    if-eqz v1, :cond_20

    #@8
    const-string v1, "LoaderManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Destroying Active in "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 833
    :cond_20
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@22
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@25
    move-result v1

    #@26
    add-int/lit8 v0, v1, -0x1

    #@28
    .local v0, i:I
    :goto_28
    if-ltz v0, :cond_38

    #@2a
    .line 834
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2c
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@32
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@35
    .line 833
    add-int/lit8 v0, v0, -0x1

    #@37
    goto :goto_28

    #@38
    .line 838
    .end local v0           #i:I
    :cond_38
    sget-boolean v1, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@3a
    if-eqz v1, :cond_54

    #@3c
    const-string v1, "LoaderManager"

    #@3e
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v3, "Destroying Inactive in "

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 839
    :cond_54
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@56
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@59
    move-result v1

    #@5a
    add-int/lit8 v0, v1, -0x1

    #@5c
    .restart local v0       #i:I
    :goto_5c
    if-ltz v0, :cond_6c

    #@5e
    .line 840
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@60
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@63
    move-result-object v1

    #@64
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@66
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@69
    .line 839
    add-int/lit8 v0, v0, -0x1

    #@6b
    goto :goto_5c

    #@6c
    .line 842
    :cond_6c
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@6e
    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    #@71
    .line 843
    return-void
.end method

.method doReportNextStart()V
    .registers 4

    #@0
    .prologue
    .line 819
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_18

    #@a
    .line 820
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@12
    const/4 v2, 0x1

    #@13
    iput-boolean v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mReportNextStart:Z

    #@15
    .line 819
    add-int/lit8 v0, v0, -0x1

    #@17
    goto :goto_8

    #@18
    .line 822
    :cond_18
    return-void
.end method

.method doReportStart()V
    .registers 3

    #@0
    .prologue
    .line 825
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_18

    #@a
    .line 826
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@12
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->reportStart()V

    #@15
    .line 825
    add-int/lit8 v0, v0, -0x1

    #@17
    goto :goto_8

    #@18
    .line 828
    :cond_18
    return-void
.end method

.method doRetain()V
    .registers 6

    #@0
    .prologue
    .line 792
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@2
    if-eqz v2, :cond_1c

    #@4
    const-string v2, "LoaderManager"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Retaining in "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 793
    :cond_1c
    iget-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@1e
    if-nez v2, :cond_43

    #@20
    .line 794
    new-instance v0, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "here"

    #@24
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    .line 795
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@2a
    .line 796
    const-string v2, "LoaderManager"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Called doRetain when not started: "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    .line 805
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_42
    return-void

    #@43
    .line 800
    :cond_43
    const/4 v2, 0x1

    #@44
    iput-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    #@46
    .line 801
    const/4 v2, 0x0

    #@47
    iput-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@49
    .line 802
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@4b
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@4e
    move-result v2

    #@4f
    add-int/lit8 v1, v2, -0x1

    #@51
    .local v1, i:I
    :goto_51
    if-ltz v1, :cond_42

    #@53
    .line 803
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@55
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@58
    move-result-object v2

    #@59
    check-cast v2, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@5b
    invoke-virtual {v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->retain()V

    #@5e
    .line 802
    add-int/lit8 v1, v1, -0x1

    #@60
    goto :goto_51
.end method

.method doStart()V
    .registers 6

    #@0
    .prologue
    .line 759
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@2
    if-eqz v2, :cond_1c

    #@4
    const-string v2, "LoaderManager"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Starting in "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 760
    :cond_1c
    iget-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@1e
    if-eqz v2, :cond_43

    #@20
    .line 761
    new-instance v0, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "here"

    #@24
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    .line 762
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@2a
    .line 763
    const-string v2, "LoaderManager"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Called doStart when already started: "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    .line 774
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_42
    return-void

    #@43
    .line 767
    :cond_43
    const/4 v2, 0x1

    #@44
    iput-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@46
    .line 771
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@48
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@4b
    move-result v2

    #@4c
    add-int/lit8 v1, v2, -0x1

    #@4e
    .local v1, i:I
    :goto_4e
    if-ltz v1, :cond_42

    #@50
    .line 772
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@52
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@55
    move-result-object v2

    #@56
    check-cast v2, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@58
    invoke-virtual {v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->start()V

    #@5b
    .line 771
    add-int/lit8 v1, v1, -0x1

    #@5d
    goto :goto_4e
.end method

.method doStop()V
    .registers 6

    #@0
    .prologue
    .line 777
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@2
    if-eqz v2, :cond_1c

    #@4
    const-string v2, "LoaderManager"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Stopping in "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 778
    :cond_1c
    iget-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@1e
    if-nez v2, :cond_43

    #@20
    .line 779
    new-instance v0, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "here"

    #@24
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    .line 780
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@2a
    .line 781
    const-string v2, "LoaderManager"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Called doStop when not started: "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    .line 789
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_42
    return-void

    #@43
    .line 785
    :cond_43
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@45
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@48
    move-result v2

    #@49
    add-int/lit8 v1, v2, -0x1

    #@4b
    .local v1, i:I
    :goto_4b
    if-ltz v1, :cond_5b

    #@4d
    .line 786
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@4f
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@52
    move-result-object v2

    #@53
    check-cast v2, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@55
    invoke-virtual {v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->stop()V

    #@58
    .line 785
    add-int/lit8 v1, v1, -0x1

    #@5a
    goto :goto_4b

    #@5b
    .line 788
    :cond_5b
    const/4 v2, 0x0

    #@5c
    iput-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@5e
    goto :goto_42
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 858
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@5
    move-result v3

    #@6
    if-lez v3, :cond_57

    #@8
    .line 859
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b
    const-string v3, "Active Loaders:"

    #@d
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10
    .line 860
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, "    "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    .line 861
    .local v1, innerPrefix:Ljava/lang/String;
    const/4 v0, 0x0

    #@24
    .local v0, i:I
    :goto_24
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@26
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@29
    move-result v3

    #@2a
    if-ge v0, v3, :cond_57

    #@2c
    .line 862
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2e
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@31
    move-result-object v2

    #@32
    check-cast v2, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@34
    .line 863
    .local v2, li:Landroid/app/LoaderManagerImpl$LoaderInfo;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37
    const-string v3, "  #"

    #@39
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@3e
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@41
    move-result v3

    #@42
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@45
    .line 864
    const-string v3, ": "

    #@47
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    invoke-virtual {v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->toString()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@51
    .line 865
    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/app/LoaderManagerImpl$LoaderInfo;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@54
    .line 861
    add-int/lit8 v0, v0, 0x1

    #@56
    goto :goto_24

    #@57
    .line 868
    .end local v0           #i:I
    .end local v1           #innerPrefix:Ljava/lang/String;
    .end local v2           #li:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_57
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@59
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@5c
    move-result v3

    #@5d
    if-lez v3, :cond_ae

    #@5f
    .line 869
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    const-string v3, "Inactive Loaders:"

    #@64
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@67
    .line 870
    new-instance v3, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, "    "

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v1

    #@7a
    .line 871
    .restart local v1       #innerPrefix:Ljava/lang/String;
    const/4 v0, 0x0

    #@7b
    .restart local v0       #i:I
    :goto_7b
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@7d
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@80
    move-result v3

    #@81
    if-ge v0, v3, :cond_ae

    #@83
    .line 872
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@85
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@88
    move-result-object v2

    #@89
    check-cast v2, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@8b
    .line 873
    .restart local v2       #li:Landroid/app/LoaderManagerImpl$LoaderInfo;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8e
    const-string v3, "  #"

    #@90
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@93
    iget-object v3, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@95
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@98
    move-result v3

    #@99
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@9c
    .line 874
    const-string v3, ": "

    #@9e
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a1
    invoke-virtual {v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->toString()Ljava/lang/String;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a8
    .line 875
    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/app/LoaderManagerImpl$LoaderInfo;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@ab
    .line 871
    add-int/lit8 v0, v0, 0x1

    #@ad
    goto :goto_7b

    #@ae
    .line 878
    .end local v0           #i:I
    .end local v1           #innerPrefix:Ljava/lang/String;
    .end local v2           #li:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_ae
    return-void
.end method

.method finishRetain()V
    .registers 5

    #@0
    .prologue
    .line 808
    iget-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    #@2
    if-eqz v1, :cond_3b

    #@4
    .line 809
    sget-boolean v1, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@6
    if-eqz v1, :cond_20

    #@8
    const-string v1, "LoaderManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Finished Retaining in "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 811
    :cond_20
    const/4 v1, 0x0

    #@21
    iput-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    #@23
    .line 812
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@25
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@28
    move-result v1

    #@29
    add-int/lit8 v0, v1, -0x1

    #@2b
    .local v0, i:I
    :goto_2b
    if-ltz v0, :cond_3b

    #@2d
    .line 813
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2f
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@35
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->finishRetain()V

    #@38
    .line 812
    add-int/lit8 v0, v0, -0x1

    #@3a
    goto :goto_2b

    #@3b
    .line 816
    .end local v0           #i:I
    :cond_3b
    return-void
.end method

.method public getLoader(I)Landroid/content/Loader;
    .registers 5
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I)",
            "Landroid/content/Loader",
            "<TD;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 744
    iget-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 745
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string v2, "Called while creating a loader"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 748
    :cond_c
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@14
    .line 749
    .local v0, loaderInfo:Landroid/app/LoaderManagerImpl$LoaderInfo;
    if-eqz v0, :cond_22

    #@16
    .line 750
    iget-object v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@18
    if-eqz v1, :cond_1f

    #@1a
    .line 751
    iget-object v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@1c
    iget-object v1, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@1e
    .line 755
    :goto_1e
    return-object v1

    #@1f
    .line 753
    :cond_1f
    iget-object v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@21
    goto :goto_1e

    #@22
    .line 755
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_1e
.end method

.method public hasRunningLoaders()Z
    .registers 6

    #@0
    .prologue
    .line 881
    const/4 v3, 0x0

    #@1
    .line 882
    .local v3, loadersRunning:Z
    iget-object v4, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    #@6
    move-result v0

    #@7
    .line 883
    .local v0, count:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_21

    #@a
    .line 884
    iget-object v4, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@c
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@12
    .line 885
    .local v2, li:Landroid/app/LoaderManagerImpl$LoaderInfo;
    iget-boolean v4, v2, Landroid/app/LoaderManagerImpl$LoaderInfo;->mStarted:Z

    #@14
    if-eqz v4, :cond_1f

    #@16
    iget-boolean v4, v2, Landroid/app/LoaderManagerImpl$LoaderInfo;->mDeliveredData:Z

    #@18
    if-nez v4, :cond_1f

    #@1a
    const/4 v4, 0x1

    #@1b
    :goto_1b
    or-int/2addr v3, v4

    #@1c
    .line 883
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_8

    #@1f
    .line 885
    :cond_1f
    const/4 v4, 0x0

    #@20
    goto :goto_1b

    #@21
    .line 887
    .end local v2           #li:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_21
    return v3
.end method

.method public initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I",
            "Landroid/os/Bundle;",
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<TD;>;)",
            "Landroid/content/Loader",
            "<TD;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 599
    .local p3, callback:Landroid/app/LoaderManager$LoaderCallbacks;,"Landroid/app/LoaderManager$LoaderCallbacks<TD;>;"
    iget-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 600
    new-instance v1, Ljava/lang/IllegalStateException;

    #@6
    const-string v2, "Called while creating a loader"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 603
    :cond_c
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@14
    .line 605
    .local v0, info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    sget-boolean v1, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@16
    if-eqz v1, :cond_3a

    #@18
    const-string v1, "LoaderManager"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "initLoader in "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, ": args="

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 607
    :cond_3a
    if-nez v0, :cond_6e

    #@3c
    .line 609
    invoke-direct {p0, p1, p2, p3}, Landroid/app/LoaderManagerImpl;->createAndInstallLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@3f
    move-result-object v0

    #@40
    .line 610
    sget-boolean v1, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@42
    if-eqz v1, :cond_5c

    #@44
    const-string v1, "LoaderManager"

    #@46
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "  Created new loader "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 616
    :cond_5c
    :goto_5c
    iget-boolean v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mHaveData:Z

    #@5e
    if-eqz v1, :cond_6b

    #@60
    iget-boolean v1, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@62
    if-eqz v1, :cond_6b

    #@64
    .line 618
    iget-object v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@66
    iget-object v2, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mData:Ljava/lang/Object;

    #@68
    invoke-virtual {v0, v1, v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->callOnLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V

    #@6b
    .line 621
    :cond_6b
    iget-object v1, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@6d
    return-object v1

    #@6e
    .line 612
    :cond_6e
    sget-boolean v1, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@70
    if-eqz v1, :cond_8a

    #@72
    const-string v1, "LoaderManager"

    #@74
    new-instance v2, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v3, "  Re-using existing loader "

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v2

    #@83
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v2

    #@87
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 613
    :cond_8a
    iput-object p3, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    #@8c
    goto :goto_5c
.end method

.method installLoader(Landroid/app/LoaderManagerImpl$LoaderInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@2
    iget v1, p1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mId:I

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@7
    .line 565
    iget-boolean v0, p0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 569
    invoke-virtual {p1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->start()V

    #@e
    .line 571
    :cond_e
    return-void
.end method

.method public restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;
    .registers 10
    .parameter "id"
    .parameter "args"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I",
            "Landroid/os/Bundle;",
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<TD;>;)",
            "Landroid/content/Loader",
            "<TD;>;"
        }
    .end annotation

    #@0
    .prologue
    .local p3, callback:Landroid/app/LoaderManager$LoaderCallbacks;,"Landroid/app/LoaderManager$LoaderCallbacks<TD;>;"
    const/4 v5, 0x0

    #@1
    .line 649
    iget-boolean v2, p0, Landroid/app/LoaderManagerImpl;->mCreatingLoader:Z

    #@3
    if-eqz v2, :cond_d

    #@5
    .line 650
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7
    const-string v3, "Called while creating a loader"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 653
    :cond_d
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@15
    .line 654
    .local v1, info:Landroid/app/LoaderManagerImpl$LoaderInfo;
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@17
    if-eqz v2, :cond_3c

    #@19
    const-string v2, "LoaderManager"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string/jumbo v4, "restartLoader in "

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, ": args="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 655
    :cond_3c
    if-eqz v1, :cond_78

    #@3e
    .line 656
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@40
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@46
    .line 657
    .local v0, inactive:Landroid/app/LoaderManagerImpl$LoaderInfo;
    if-eqz v0, :cond_e4

    #@48
    .line 658
    iget-boolean v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mHaveData:Z

    #@4a
    if-eqz v2, :cond_7f

    #@4c
    .line 663
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@4e
    if-eqz v2, :cond_68

    #@50
    const-string v2, "LoaderManager"

    #@52
    new-instance v3, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v4, "  Removing last inactive loader: "

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 664
    :cond_68
    const/4 v2, 0x0

    #@69
    iput-boolean v2, v0, Landroid/app/LoaderManagerImpl$LoaderInfo;->mDeliveredData:Z

    #@6b
    .line 665
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@6e
    .line 666
    iget-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@70
    invoke-virtual {v2}, Landroid/content/Loader;->abandon()V

    #@73
    .line 667
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@75
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@78
    .line 704
    .end local v0           #inactive:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_78
    :goto_78
    invoke-direct {p0, p1, p2, p3}, Landroid/app/LoaderManagerImpl;->createAndInstallLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@7b
    move-result-object v1

    #@7c
    .line 705
    iget-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@7e
    :goto_7e
    return-object v2

    #@7f
    .line 671
    .restart local v0       #inactive:Landroid/app/LoaderManagerImpl$LoaderInfo;
    :cond_7f
    iget-boolean v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mStarted:Z

    #@81
    if-nez v2, :cond_97

    #@83
    .line 675
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@85
    if-eqz v2, :cond_8e

    #@87
    const-string v2, "LoaderManager"

    #@89
    const-string v3, "  Current loader is stopped; replacing"

    #@8b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 676
    :cond_8e
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mLoaders:Landroid/util/SparseArray;

    #@90
    invoke-virtual {v2, p1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@93
    .line 677
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@96
    goto :goto_78

    #@97
    .line 682
    :cond_97
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@99
    if-eqz v2, :cond_a2

    #@9b
    const-string v2, "LoaderManager"

    #@9d
    const-string v3, "  Current loader is running; attempting to cancel"

    #@9f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 683
    :cond_a2
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl$LoaderInfo;->cancel()V

    #@a5
    .line 684
    iget-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@a7
    if-eqz v2, :cond_ce

    #@a9
    .line 685
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@ab
    if-eqz v2, :cond_c7

    #@ad
    const-string v2, "LoaderManager"

    #@af
    new-instance v3, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v4, "  Removing pending loader: "

    #@b6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v3

    #@ba
    iget-object v4, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v3

    #@c0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v3

    #@c4
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 686
    :cond_c7
    iget-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@c9
    invoke-virtual {v2}, Landroid/app/LoaderManagerImpl$LoaderInfo;->destroy()V

    #@cc
    .line 687
    iput-object v5, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@ce
    .line 689
    :cond_ce
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@d0
    if-eqz v2, :cond_d9

    #@d2
    const-string v2, "LoaderManager"

    #@d4
    const-string v3, "  Enqueuing as new pending loader"

    #@d6
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d9
    .line 690
    :cond_d9
    invoke-direct {p0, p1, p2, p3}, Landroid/app/LoaderManagerImpl;->createLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@dc
    move-result-object v2

    #@dd
    iput-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@df
    .line 692
    iget-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mPendingLoader:Landroid/app/LoaderManagerImpl$LoaderInfo;

    #@e1
    iget-object v2, v2, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@e3
    goto :goto_7e

    #@e4
    .line 698
    :cond_e4
    sget-boolean v2, Landroid/app/LoaderManagerImpl;->DEBUG:Z

    #@e6
    if-eqz v2, :cond_100

    #@e8
    const-string v2, "LoaderManager"

    #@ea
    new-instance v3, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v4, "  Making last loader inactive: "

    #@f1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v3

    #@f5
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v3

    #@f9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v3

    #@fd
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@100
    .line 699
    :cond_100
    iget-object v2, v1, Landroid/app/LoaderManagerImpl$LoaderInfo;->mLoader:Landroid/content/Loader;

    #@102
    invoke-virtual {v2}, Landroid/content/Loader;->abandon()V

    #@105
    .line 700
    iget-object v2, p0, Landroid/app/LoaderManagerImpl;->mInactiveLoaders:Landroid/util/SparseArray;

    #@107
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@10a
    goto/16 :goto_78
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 847
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 848
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "LoaderManager{"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 849
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@f
    move-result v1

    #@10
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 850
    const-string v1, " in "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 851
    iget-object v1, p0, Landroid/app/LoaderManagerImpl;->mActivity:Landroid/app/Activity;

    #@1e
    invoke-static {v1, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    #@21
    .line 852
    const-string/jumbo v1, "}}"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 853
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    return-object v1
.end method

.method updateActivity(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 540
    iput-object p1, p0, Landroid/app/LoaderManagerImpl;->mActivity:Landroid/app/Activity;

    #@2
    .line 541
    return-void
.end method
