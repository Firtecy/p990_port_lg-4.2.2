.class Landroid/app/Presentation$2;
.super Ljava/lang/Object;
.source "Presentation.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Presentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/app/Presentation;


# direct methods
.method constructor <init>(Landroid/app/Presentation;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 323
    iput-object p1, p0, Landroid/app/Presentation$2;->this$0:Landroid/app/Presentation;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .registers 2
    .parameter "displayId"

    #@0
    .prologue
    .line 326
    return-void
.end method

.method public onDisplayChanged(I)V
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 337
    iget-object v0, p0, Landroid/app/Presentation$2;->this$0:Landroid/app/Presentation;

    #@2
    invoke-static {v0}, Landroid/app/Presentation;->access$000(Landroid/app/Presentation;)Landroid/view/Display;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    #@9
    move-result v0

    #@a
    if-ne p1, v0, :cond_11

    #@c
    .line 338
    iget-object v0, p0, Landroid/app/Presentation$2;->this$0:Landroid/app/Presentation;

    #@e
    invoke-static {v0}, Landroid/app/Presentation;->access$200(Landroid/app/Presentation;)V

    #@11
    .line 340
    :cond_11
    return-void
.end method

.method public onDisplayRemoved(I)V
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Landroid/app/Presentation$2;->this$0:Landroid/app/Presentation;

    #@2
    invoke-static {v0}, Landroid/app/Presentation;->access$000(Landroid/app/Presentation;)Landroid/view/Display;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    #@9
    move-result v0

    #@a
    if-ne p1, v0, :cond_11

    #@c
    .line 331
    iget-object v0, p0, Landroid/app/Presentation$2;->this$0:Landroid/app/Presentation;

    #@e
    invoke-static {v0}, Landroid/app/Presentation;->access$100(Landroid/app/Presentation;)V

    #@11
    .line 333
    :cond_11
    return-void
.end method
