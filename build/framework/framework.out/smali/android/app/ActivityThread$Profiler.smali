.class final Landroid/app/ActivityThread$Profiler;
.super Ljava/lang/Object;
.source "ActivityThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Profiler"
.end annotation


# instance fields
.field autoStopProfiler:Z

.field handlingProfiling:Z

.field profileFd:Landroid/os/ParcelFileDescriptor;

.field profileFile:Ljava/lang/String;

.field profiling:Z


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 445
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public setProfiler(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .registers 4
    .parameter "file"
    .parameter "fd"

    #@0
    .prologue
    .line 452
    iget-boolean v0, p0, Landroid/app/ActivityThread$Profiler;->profiling:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 453
    if-eqz p2, :cond_9

    #@6
    .line 455
    :try_start_6
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_9} :catch_18

    #@9
    .line 471
    :cond_9
    :goto_9
    return-void

    #@a
    .line 462
    :cond_a
    iget-object v0, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 464
    :try_start_e
    iget-object v0, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@10
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_13} :catch_1a

    #@13
    .line 469
    :cond_13
    :goto_13
    iput-object p1, p0, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@15
    .line 470
    iput-object p2, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@17
    goto :goto_9

    #@18
    .line 456
    :catch_18
    move-exception v0

    #@19
    goto :goto_9

    #@1a
    .line 465
    :catch_1a
    move-exception v0

    #@1b
    goto :goto_13
.end method

.method public startProfiling()V
    .registers 7

    #@0
    .prologue
    .line 473
    iget-object v2, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@2
    if-eqz v2, :cond_8

    #@4
    iget-boolean v2, p0, Landroid/app/ActivityThread$Profiler;->profiling:Z

    #@6
    if-eqz v2, :cond_9

    #@8
    .line 489
    :cond_8
    :goto_8
    return-void

    #@9
    .line 477
    :cond_9
    :try_start_9
    iget-object v2, p0, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@b
    iget-object v3, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@d
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@10
    move-result-object v3

    #@11
    const/high16 v4, 0x80

    #@13
    const/4 v5, 0x0

    #@14
    invoke-static {v2, v3, v4, v5}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;Ljava/io/FileDescriptor;II)V

    #@17
    .line 479
    const/4 v2, 0x1

    #@18
    iput-boolean v2, p0, Landroid/app/ActivityThread$Profiler;->profiling:Z
    :try_end_1a
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_8

    #@1b
    .line 480
    :catch_1b
    move-exception v0

    #@1c
    .line 481
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "ActivityThread"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "Profiling failed on path "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    iget-object v4, p0, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 483
    :try_start_36
    iget-object v2, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@38
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    #@3b
    .line 484
    const/4 v2, 0x0

    #@3c
    iput-object v2, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_3e} :catch_3f

    #@3e
    goto :goto_8

    #@3f
    .line 485
    :catch_3f
    move-exception v1

    #@40
    .line 486
    .local v1, e2:Ljava/io/IOException;
    const-string v2, "ActivityThread"

    #@42
    const-string v3, "Failure closing profile fd"

    #@44
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@47
    goto :goto_8
.end method

.method public stopProfiling()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 491
    iget-boolean v0, p0, Landroid/app/ActivityThread$Profiler;->profiling:Z

    #@3
    if-eqz v0, :cond_18

    #@5
    .line 492
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/app/ActivityThread$Profiler;->profiling:Z

    #@8
    .line 493
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    #@b
    .line 494
    iget-object v0, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 496
    :try_start_f
    iget-object v0, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@11
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_14} :catch_19

    #@14
    .line 500
    :cond_14
    :goto_14
    iput-object v1, p0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@16
    .line 501
    iput-object v1, p0, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@18
    .line 503
    :cond_18
    return-void

    #@19
    .line 497
    :catch_19
    move-exception v0

    #@1a
    goto :goto_14
.end method
