.class final Landroid/app/BackStackState;
.super Ljava/lang/Object;
.source "BackStackRecord.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/BackStackState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mBreadCrumbShortTitleRes:I

.field final mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

.field final mBreadCrumbTitleRes:I

.field final mBreadCrumbTitleText:Ljava/lang/CharSequence;

.field final mIndex:I

.field final mName:Ljava/lang/String;

.field final mOps:[I

.field final mTransition:I

.field final mTransitionStyle:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 157
    new-instance v0, Landroid/app/BackStackState$1;

    #@2
    invoke-direct {v0}, Landroid/app/BackStackState$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/BackStackState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/app/FragmentManagerImpl;Landroid/app/BackStackRecord;)V
    .registers 11
    .parameter "fm"
    .parameter "bse"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    const/4 v2, 0x0

    #@4
    .line 42
    .local v2, numRemoved:I
    iget-object v3, p2, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@6
    .line 43
    .local v3, op:Landroid/app/BackStackRecord$Op;
    :goto_6
    if-eqz v3, :cond_16

    #@8
    .line 44
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@a
    if-eqz v6, :cond_13

    #@c
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v6

    #@12
    add-int/2addr v2, v6

    #@13
    .line 45
    :cond_13
    iget-object v3, v3, Landroid/app/BackStackRecord$Op;->next:Landroid/app/BackStackRecord$Op;

    #@15
    goto :goto_6

    #@16
    .line 47
    :cond_16
    iget v6, p2, Landroid/app/BackStackRecord;->mNumOp:I

    #@18
    mul-int/lit8 v6, v6, 0x7

    #@1a
    add-int/2addr v6, v2

    #@1b
    new-array v6, v6, [I

    #@1d
    iput-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@1f
    .line 49
    iget-boolean v6, p2, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@21
    if-nez v6, :cond_2b

    #@23
    .line 50
    new-instance v6, Ljava/lang/IllegalStateException;

    #@25
    const-string v7, "Not on back stack"

    #@27
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v6

    #@2b
    .line 53
    :cond_2b
    iget-object v3, p2, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@2d
    .line 54
    const/4 v4, 0x0

    #@2e
    .local v4, pos:I
    move v5, v4

    #@2f
    .line 55
    .end local v4           #pos:I
    .local v5, pos:I
    :goto_2f
    if-eqz v3, :cond_9e

    #@31
    .line 56
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@33
    add-int/lit8 v4, v5, 0x1

    #@35
    .end local v5           #pos:I
    .restart local v4       #pos:I
    iget v7, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    #@37
    aput v7, v6, v5

    #@39
    .line 57
    iget-object v7, p0, Landroid/app/BackStackState;->mOps:[I

    #@3b
    add-int/lit8 v5, v4, 0x1

    #@3d
    .end local v4           #pos:I
    .restart local v5       #pos:I
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@3f
    if-eqz v6, :cond_8f

    #@41
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@43
    iget v6, v6, Landroid/app/Fragment;->mIndex:I

    #@45
    :goto_45
    aput v6, v7, v4

    #@47
    .line 58
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@49
    add-int/lit8 v4, v5, 0x1

    #@4b
    .end local v5           #pos:I
    .restart local v4       #pos:I
    iget v7, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@4d
    aput v7, v6, v5

    #@4f
    .line 59
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@51
    add-int/lit8 v5, v4, 0x1

    #@53
    .end local v4           #pos:I
    .restart local v5       #pos:I
    iget v7, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@55
    aput v7, v6, v4

    #@57
    .line 60
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@59
    add-int/lit8 v4, v5, 0x1

    #@5b
    .end local v5           #pos:I
    .restart local v4       #pos:I
    iget v7, v3, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@5d
    aput v7, v6, v5

    #@5f
    .line 61
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@61
    add-int/lit8 v5, v4, 0x1

    #@63
    .end local v4           #pos:I
    .restart local v5       #pos:I
    iget v7, v3, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@65
    aput v7, v6, v4

    #@67
    .line 62
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@69
    if-eqz v6, :cond_96

    #@6b
    .line 63
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@6d
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@70
    move-result v0

    #@71
    .line 64
    .local v0, N:I
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@73
    add-int/lit8 v4, v5, 0x1

    #@75
    .end local v5           #pos:I
    .restart local v4       #pos:I
    aput v0, v6, v5

    #@77
    .line 65
    const/4 v1, 0x0

    #@78
    .local v1, i:I
    move v5, v4

    #@79
    .end local v4           #pos:I
    .restart local v5       #pos:I
    :goto_79
    if-ge v1, v0, :cond_91

    #@7b
    .line 66
    iget-object v7, p0, Landroid/app/BackStackState;->mOps:[I

    #@7d
    add-int/lit8 v4, v5, 0x1

    #@7f
    .end local v5           #pos:I
    .restart local v4       #pos:I
    iget-object v6, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@81
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@84
    move-result-object v6

    #@85
    check-cast v6, Landroid/app/Fragment;

    #@87
    iget v6, v6, Landroid/app/Fragment;->mIndex:I

    #@89
    aput v6, v7, v5

    #@8b
    .line 65
    add-int/lit8 v1, v1, 0x1

    #@8d
    move v5, v4

    #@8e
    .end local v4           #pos:I
    .restart local v5       #pos:I
    goto :goto_79

    #@8f
    .line 57
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_8f
    const/4 v6, -0x1

    #@90
    goto :goto_45

    #@91
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_91
    move v4, v5

    #@92
    .line 71
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v5           #pos:I
    .restart local v4       #pos:I
    :goto_92
    iget-object v3, v3, Landroid/app/BackStackRecord$Op;->next:Landroid/app/BackStackRecord$Op;

    #@94
    move v5, v4

    #@95
    .end local v4           #pos:I
    .restart local v5       #pos:I
    goto :goto_2f

    #@96
    .line 69
    :cond_96
    iget-object v6, p0, Landroid/app/BackStackState;->mOps:[I

    #@98
    add-int/lit8 v4, v5, 0x1

    #@9a
    .end local v5           #pos:I
    .restart local v4       #pos:I
    const/4 v7, 0x0

    #@9b
    aput v7, v6, v5

    #@9d
    goto :goto_92

    #@9e
    .line 73
    .end local v4           #pos:I
    .restart local v5       #pos:I
    :cond_9e
    iget v6, p2, Landroid/app/BackStackRecord;->mTransition:I

    #@a0
    iput v6, p0, Landroid/app/BackStackState;->mTransition:I

    #@a2
    .line 74
    iget v6, p2, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@a4
    iput v6, p0, Landroid/app/BackStackState;->mTransitionStyle:I

    #@a6
    .line 75
    iget-object v6, p2, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@a8
    iput-object v6, p0, Landroid/app/BackStackState;->mName:Ljava/lang/String;

    #@aa
    .line 76
    iget v6, p2, Landroid/app/BackStackRecord;->mIndex:I

    #@ac
    iput v6, p0, Landroid/app/BackStackState;->mIndex:I

    #@ae
    .line 77
    iget v6, p2, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@b0
    iput v6, p0, Landroid/app/BackStackState;->mBreadCrumbTitleRes:I

    #@b2
    .line 78
    iget-object v6, p2, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@b4
    iput-object v6, p0, Landroid/app/BackStackState;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@b6
    .line 79
    iget v6, p2, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@b8
    iput v6, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleRes:I

    #@ba
    .line 80
    iget-object v6, p2, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@bc
    iput-object v6, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@be
    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/app/BackStackState;->mOps:[I

    #@9
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/app/BackStackState;->mTransition:I

    #@f
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/app/BackStackState;->mTransitionStyle:I

    #@15
    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/app/BackStackState;->mName:Ljava/lang/String;

    #@1b
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/app/BackStackState;->mIndex:I

    #@21
    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    iput v0, p0, Landroid/app/BackStackState;->mBreadCrumbTitleRes:I

    #@27
    .line 90
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Ljava/lang/CharSequence;

    #@2f
    iput-object v0, p0, Landroid/app/BackStackState;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@31
    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v0

    #@35
    iput v0, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleRes:I

    #@37
    .line 92
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@39
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Ljava/lang/CharSequence;

    #@3f
    iput-object v0, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@41
    .line 93
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 142
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public instantiate(Landroid/app/FragmentManagerImpl;)Landroid/app/BackStackRecord;
    .registers 16
    .parameter "fm"

    #@0
    .prologue
    const/4 v13, 0x1

    #@1
    .line 96
    new-instance v1, Landroid/app/BackStackRecord;

    #@3
    invoke-direct {v1, p1}, Landroid/app/BackStackRecord;-><init>(Landroid/app/FragmentManagerImpl;)V

    #@6
    .line 97
    .local v1, bse:Landroid/app/BackStackRecord;
    const/4 v7, 0x0

    #@7
    .line 98
    .local v7, pos:I
    const/4 v5, 0x0

    #@8
    .line 99
    .local v5, num:I
    :goto_8
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@a
    array-length v10, v10

    #@b
    if-ge v7, v10, :cond_df

    #@d
    .line 100
    new-instance v6, Landroid/app/BackStackRecord$Op;

    #@f
    invoke-direct {v6}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@12
    .line 101
    .local v6, op:Landroid/app/BackStackRecord$Op;
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@14
    add-int/lit8 v8, v7, 0x1

    #@16
    .end local v7           #pos:I
    .local v8, pos:I
    aget v10, v10, v7

    #@18
    iput v10, v6, Landroid/app/BackStackRecord$Op;->cmd:I

    #@1a
    .line 102
    sget-boolean v10, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@1c
    if-eqz v10, :cond_4e

    #@1e
    const-string v10, "FragmentManager"

    #@20
    new-instance v11, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v12, "Instantiate "

    #@27
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v11

    #@2b
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v11

    #@2f
    const-string v12, " op #"

    #@31
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v11

    #@35
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v11

    #@39
    const-string v12, " base fragment #"

    #@3b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v11

    #@3f
    iget-object v12, p0, Landroid/app/BackStackState;->mOps:[I

    #@41
    aget v12, v12, v8

    #@43
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v11

    #@47
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v11

    #@4b
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 104
    :cond_4e
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@50
    add-int/lit8 v7, v8, 0x1

    #@52
    .end local v8           #pos:I
    .restart local v7       #pos:I
    aget v3, v10, v8

    #@54
    .line 105
    .local v3, findex:I
    if-ltz v3, :cond_d3

    #@56
    .line 106
    iget-object v10, p1, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@58
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5b
    move-result-object v2

    #@5c
    check-cast v2, Landroid/app/Fragment;

    #@5e
    .line 107
    .local v2, f:Landroid/app/Fragment;
    iput-object v2, v6, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@60
    .line 111
    .end local v2           #f:Landroid/app/Fragment;
    :goto_60
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@62
    add-int/lit8 v8, v7, 0x1

    #@64
    .end local v7           #pos:I
    .restart local v8       #pos:I
    aget v10, v10, v7

    #@66
    iput v10, v6, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@68
    .line 112
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@6a
    add-int/lit8 v7, v8, 0x1

    #@6c
    .end local v8           #pos:I
    .restart local v7       #pos:I
    aget v10, v10, v8

    #@6e
    iput v10, v6, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@70
    .line 113
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@72
    add-int/lit8 v8, v7, 0x1

    #@74
    .end local v7           #pos:I
    .restart local v8       #pos:I
    aget v10, v10, v7

    #@76
    iput v10, v6, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@78
    .line 114
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@7a
    add-int/lit8 v7, v8, 0x1

    #@7c
    .end local v8           #pos:I
    .restart local v7       #pos:I
    aget v10, v10, v8

    #@7e
    iput v10, v6, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@80
    .line 115
    iget-object v10, p0, Landroid/app/BackStackState;->mOps:[I

    #@82
    add-int/lit8 v8, v7, 0x1

    #@84
    .end local v7           #pos:I
    .restart local v8       #pos:I
    aget v0, v10, v7

    #@86
    .line 116
    .local v0, N:I
    if-lez v0, :cond_d7

    #@88
    .line 117
    new-instance v10, Ljava/util/ArrayList;

    #@8a
    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@8d
    iput-object v10, v6, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@8f
    .line 118
    const/4 v4, 0x0

    #@90
    .local v4, i:I
    :goto_90
    if-ge v4, v0, :cond_d7

    #@92
    .line 119
    sget-boolean v10, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@94
    if-eqz v10, :cond_bc

    #@96
    const-string v10, "FragmentManager"

    #@98
    new-instance v11, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v12, "Instantiate "

    #@9f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v11

    #@a3
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v11

    #@a7
    const-string v12, " set remove fragment #"

    #@a9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v11

    #@ad
    iget-object v12, p0, Landroid/app/BackStackState;->mOps:[I

    #@af
    aget v12, v12, v8

    #@b1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v11

    #@b5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v11

    #@b9
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 121
    :cond_bc
    iget-object v10, p1, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@be
    iget-object v11, p0, Landroid/app/BackStackState;->mOps:[I

    #@c0
    add-int/lit8 v7, v8, 0x1

    #@c2
    .end local v8           #pos:I
    .restart local v7       #pos:I
    aget v11, v11, v8

    #@c4
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c7
    move-result-object v9

    #@c8
    check-cast v9, Landroid/app/Fragment;

    #@ca
    .line 122
    .local v9, r:Landroid/app/Fragment;
    iget-object v10, v6, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@cc
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cf
    .line 118
    add-int/lit8 v4, v4, 0x1

    #@d1
    move v8, v7

    #@d2
    .end local v7           #pos:I
    .restart local v8       #pos:I
    goto :goto_90

    #@d3
    .line 109
    .end local v0           #N:I
    .end local v4           #i:I
    .end local v8           #pos:I
    .end local v9           #r:Landroid/app/Fragment;
    .restart local v7       #pos:I
    :cond_d3
    const/4 v10, 0x0

    #@d4
    iput-object v10, v6, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@d6
    goto :goto_60

    #@d7
    .end local v7           #pos:I
    .restart local v0       #N:I
    .restart local v8       #pos:I
    :cond_d7
    move v7, v8

    #@d8
    .line 125
    .end local v8           #pos:I
    .restart local v7       #pos:I
    invoke-virtual {v1, v6}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@db
    .line 126
    add-int/lit8 v5, v5, 0x1

    #@dd
    .line 127
    goto/16 :goto_8

    #@df
    .line 128
    .end local v0           #N:I
    .end local v3           #findex:I
    .end local v6           #op:Landroid/app/BackStackRecord$Op;
    :cond_df
    iget v10, p0, Landroid/app/BackStackState;->mTransition:I

    #@e1
    iput v10, v1, Landroid/app/BackStackRecord;->mTransition:I

    #@e3
    .line 129
    iget v10, p0, Landroid/app/BackStackState;->mTransitionStyle:I

    #@e5
    iput v10, v1, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@e7
    .line 130
    iget-object v10, p0, Landroid/app/BackStackState;->mName:Ljava/lang/String;

    #@e9
    iput-object v10, v1, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@eb
    .line 131
    iget v10, p0, Landroid/app/BackStackState;->mIndex:I

    #@ed
    iput v10, v1, Landroid/app/BackStackRecord;->mIndex:I

    #@ef
    .line 132
    iput-boolean v13, v1, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@f1
    .line 133
    iget v10, p0, Landroid/app/BackStackState;->mBreadCrumbTitleRes:I

    #@f3
    iput v10, v1, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@f5
    .line 134
    iget-object v10, p0, Landroid/app/BackStackState;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@f7
    iput-object v10, v1, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@f9
    .line 135
    iget v10, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleRes:I

    #@fb
    iput v10, v1, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@fd
    .line 136
    iget-object v10, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@ff
    iput-object v10, v1, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@101
    .line 137
    invoke-virtual {v1, v13}, Landroid/app/BackStackRecord;->bumpBackStackNesting(I)V

    #@104
    .line 138
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 146
    iget-object v0, p0, Landroid/app/BackStackState;->mOps:[I

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@6
    .line 147
    iget v0, p0, Landroid/app/BackStackState;->mTransition:I

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 148
    iget v0, p0, Landroid/app/BackStackState;->mTransitionStyle:I

    #@d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 149
    iget-object v0, p0, Landroid/app/BackStackState;->mName:Ljava/lang/String;

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 150
    iget v0, p0, Landroid/app/BackStackState;->mIndex:I

    #@17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 151
    iget v0, p0, Landroid/app/BackStackState;->mBreadCrumbTitleRes:I

    #@1c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 152
    iget-object v0, p0, Landroid/app/BackStackState;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@21
    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@24
    .line 153
    iget v0, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleRes:I

    #@26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 154
    iget-object v0, p0, Landroid/app/BackStackState;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@2b
    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@2e
    .line 155
    return-void
.end method
