.class public Landroid/app/Instrumentation$ActivityMonitor;
.super Ljava/lang/Object;
.source "Instrumentation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Instrumentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityMonitor"
.end annotation


# instance fields
.field private final mBlock:Z

.field private final mClass:Ljava/lang/String;

.field mHits:I

.field mLastActivity:Landroid/app/Activity;

.field private final mResult:Landroid/app/Instrumentation$ActivityResult;

.field private final mWhich:Landroid/content/IntentFilter;


# direct methods
.method public constructor <init>(Landroid/content/IntentFilter;Landroid/app/Instrumentation$ActivityResult;Z)V
    .registers 6
    .parameter "which"
    .parameter "result"
    .parameter "block"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 444
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 425
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@7
    .line 428
    iput-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@9
    .line 445
    iput-object p1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mWhich:Landroid/content/IntentFilter;

    #@b
    .line 446
    iput-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mClass:Ljava/lang/String;

    #@d
    .line 447
    iput-object p2, p0, Landroid/app/Instrumentation$ActivityMonitor;->mResult:Landroid/app/Instrumentation$ActivityResult;

    #@f
    .line 448
    iput-boolean p3, p0, Landroid/app/Instrumentation$ActivityMonitor;->mBlock:Z

    #@11
    .line 449
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/app/Instrumentation$ActivityResult;Z)V
    .registers 6
    .parameter "cls"
    .parameter "result"
    .parameter "block"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 465
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 425
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@7
    .line 428
    iput-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@9
    .line 466
    iput-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mWhich:Landroid/content/IntentFilter;

    #@b
    .line 467
    iput-object p1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mClass:Ljava/lang/String;

    #@d
    .line 468
    iput-object p2, p0, Landroid/app/Instrumentation$ActivityMonitor;->mResult:Landroid/app/Instrumentation$ActivityResult;

    #@f
    .line 469
    iput-boolean p3, p0, Landroid/app/Instrumentation$ActivityMonitor;->mBlock:Z

    #@11
    .line 470
    return-void
.end method


# virtual methods
.method public final getFilter()Landroid/content/IntentFilter;
    .registers 2

    #@0
    .prologue
    .line 476
    iget-object v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mWhich:Landroid/content/IntentFilter;

    #@2
    return-object v0
.end method

.method public final getHits()I
    .registers 2

    #@0
    .prologue
    .line 499
    iget v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@2
    return v0
.end method

.method public final getLastActivity()Landroid/app/Activity;
    .registers 2

    #@0
    .prologue
    .line 507
    iget-object v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@2
    return-object v0
.end method

.method public final getResult()Landroid/app/Instrumentation$ActivityResult;
    .registers 2

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mResult:Landroid/app/Instrumentation$ActivityResult;

    #@2
    return-object v0
.end method

.method public final isBlocking()Z
    .registers 2

    #@0
    .prologue
    .line 492
    iget-boolean v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mBlock:Z

    #@2
    return v0
.end method

.method final match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z
    .registers 11
    .parameter "who"
    .parameter "activity"
    .parameter "intent"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 560
    monitor-enter p0

    #@3
    .line 561
    :try_start_3
    iget-object v3, p0, Landroid/app/Instrumentation$ActivityMonitor;->mWhich:Landroid/content/IntentFilter;

    #@5
    if-eqz v3, :cond_18

    #@7
    iget-object v3, p0, Landroid/app/Instrumentation$ActivityMonitor;->mWhich:Landroid/content/IntentFilter;

    #@9
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v4

    #@d
    const/4 v5, 0x1

    #@e
    const-string v6, "Instrumentation"

    #@10
    invoke-virtual {v3, v4, p3, v5, v6}, Landroid/content/IntentFilter;->match(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I

    #@13
    move-result v3

    #@14
    if-gez v3, :cond_18

    #@16
    .line 564
    monitor-exit p0

    #@17
    .line 581
    :goto_17
    return v1

    #@18
    .line 566
    :cond_18
    iget-object v3, p0, Landroid/app/Instrumentation$ActivityMonitor;->mClass:Ljava/lang/String;

    #@1a
    if-eqz v3, :cond_45

    #@1c
    .line 567
    const/4 v0, 0x0

    #@1d
    .line 568
    .local v0, cls:Ljava/lang/String;
    if-eqz p2, :cond_36

    #@1f
    .line 569
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 573
    :cond_27
    :goto_27
    if-eqz v0, :cond_31

    #@29
    iget-object v3, p0, Landroid/app/Instrumentation$ActivityMonitor;->mClass:Ljava/lang/String;

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v3

    #@2f
    if-nez v3, :cond_45

    #@31
    .line 574
    :cond_31
    monitor-exit p0

    #@32
    goto :goto_17

    #@33
    .line 582
    .end local v0           #cls:Ljava/lang/String;
    :catchall_33
    move-exception v1

    #@34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_3 .. :try_end_35} :catchall_33

    #@35
    throw v1

    #@36
    .line 570
    .restart local v0       #cls:Ljava/lang/String;
    :cond_36
    :try_start_36
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@39
    move-result-object v3

    #@3a
    if-eqz v3, :cond_27

    #@3c
    .line 571
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    goto :goto_27

    #@45
    .line 577
    .end local v0           #cls:Ljava/lang/String;
    :cond_45
    if-eqz p2, :cond_4c

    #@47
    .line 578
    iput-object p2, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@49
    .line 579
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@4c
    .line 581
    :cond_4c
    monitor-exit p0
    :try_end_4d
    .catchall {:try_start_36 .. :try_end_4d} :catchall_33

    #@4d
    move v1, v2

    #@4e
    goto :goto_17
.end method

.method public final waitForActivity()Landroid/app/Activity;
    .registers 3

    #@0
    .prologue
    .line 517
    monitor-enter p0

    #@1
    .line 518
    :goto_1
    :try_start_1
    iget-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_12

    #@3
    if-nez v1, :cond_b

    #@5
    .line 520
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_12
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 521
    :catch_9
    move-exception v1

    #@a
    goto :goto_1

    #@b
    .line 524
    :cond_b
    :try_start_b
    iget-object v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@d
    .line 525
    .local v0, res:Landroid/app/Activity;
    const/4 v1, 0x0

    #@e
    iput-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@10
    .line 526
    monitor-exit p0

    #@11
    return-object v0

    #@12
    .line 527
    .end local v0           #res:Landroid/app/Activity;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_b .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public final waitForActivityWithTimeout(J)Landroid/app/Activity;
    .registers 5
    .parameter "timeOut"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 540
    monitor-enter p0

    #@2
    .line 541
    :try_start_2
    iget-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_16

    #@4
    if-nez v1, :cond_9

    #@6
    .line 543
    :try_start_6
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_16
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_9} :catch_19

    #@9
    .line 547
    :cond_9
    :goto_9
    :try_start_9
    iget-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@b
    if-nez v1, :cond_f

    #@d
    .line 548
    monitor-exit p0

    #@e
    .line 552
    :goto_e
    return-object v0

    #@f
    .line 550
    :cond_f
    iget-object v0, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@11
    .line 551
    .local v0, res:Landroid/app/Activity;
    const/4 v1, 0x0

    #@12
    iput-object v1, p0, Landroid/app/Instrumentation$ActivityMonitor;->mLastActivity:Landroid/app/Activity;

    #@14
    .line 552
    monitor-exit p0

    #@15
    goto :goto_e

    #@16
    .line 554
    .end local v0           #res:Landroid/app/Activity;
    :catchall_16
    move-exception v1

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_9 .. :try_end_18} :catchall_16

    #@18
    throw v1

    #@19
    .line 544
    :catch_19
    move-exception v1

    #@1a
    goto :goto_9
.end method
