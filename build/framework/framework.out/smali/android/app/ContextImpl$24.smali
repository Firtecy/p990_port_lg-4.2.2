.class final Landroid/app/ContextImpl$24;
.super Landroid/app/ContextImpl$ServiceFetcher;
.source "ContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ContextImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 440
    invoke-direct {p0}, Landroid/app/ContextImpl$ServiceFetcher;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createService(Landroid/app/ContextImpl;)Ljava/lang/Object;
    .registers 10
    .parameter "ctx"

    #@0
    .prologue
    .line 442
    invoke-virtual {p1}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 443
    .local v0, outerContext:Landroid/content/Context;
    new-instance v1, Landroid/app/NotificationManager;

    #@6
    new-instance v2, Landroid/view/ContextThemeWrapper;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@c
    move-result-object v4

    #@d
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@f
    const v5, 0x103000b

    #@12
    const v6, 0x103006f

    #@15
    const v7, 0x103012e

    #@18
    invoke-static {v3, v4, v5, v6, v7}, Landroid/content/res/Resources;->selectSystemTheme(IIIII)I

    #@1b
    move-result v3

    #@1c
    invoke-direct {v2, v0, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    #@1f
    iget-object v3, p1, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@21
    invoke-virtual {v3}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@24
    move-result-object v3

    #@25
    invoke-direct {v1, v2, v3}, Landroid/app/NotificationManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@28
    return-object v1
.end method
