.class Landroid/app/ActivityThread$ResourcesKey;
.super Ljava/lang/Object;
.source "ActivityThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResourcesKey"
.end annotation


# instance fields
.field private final mDisplayId:I

.field private final mHash:I

.field private final mOverrideConfiguration:Landroid/content/res/Configuration;

.field private final mResDir:Ljava/lang/String;

.field private final mScale:F


# direct methods
.method constructor <init>(Ljava/lang/String;ILandroid/content/res/Configuration;F)V
    .registers 8
    .parameter "resDir"
    .parameter "displayId"
    .parameter "overrideConfiguration"
    .parameter "scale"

    #@0
    .prologue
    .line 1521
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1522
    iput-object p1, p0, Landroid/app/ActivityThread$ResourcesKey;->mResDir:Ljava/lang/String;

    #@5
    .line 1523
    iput p2, p0, Landroid/app/ActivityThread$ResourcesKey;->mDisplayId:I

    #@7
    .line 1524
    if-eqz p3, :cond_12

    #@9
    .line 1525
    sget-object v1, Landroid/content/res/Configuration;->EMPTY:Landroid/content/res/Configuration;

    #@b
    invoke-virtual {v1, p3}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 1526
    const/4 p3, 0x0

    #@12
    .line 1529
    :cond_12
    iput-object p3, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@14
    .line 1530
    iput p4, p0, Landroid/app/ActivityThread$ResourcesKey;->mScale:F

    #@16
    .line 1531
    const/16 v0, 0x11

    #@18
    .line 1532
    .local v0, hash:I
    iget-object v1, p0, Landroid/app/ActivityThread$ResourcesKey;->mResDir:Ljava/lang/String;

    #@1a
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@1d
    move-result v1

    #@1e
    add-int/lit16 v0, v1, 0x20f

    #@20
    .line 1533
    mul-int/lit8 v1, v0, 0x1f

    #@22
    iget v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mDisplayId:I

    #@24
    add-int v0, v1, v2

    #@26
    .line 1534
    mul-int/lit8 v2, v0, 0x1f

    #@28
    iget-object v1, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@2a
    if-eqz v1, :cond_41

    #@2c
    iget-object v1, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@2e
    invoke-virtual {v1}, Landroid/content/res/Configuration;->hashCode()I

    #@31
    move-result v1

    #@32
    :goto_32
    add-int v0, v2, v1

    #@34
    .line 1536
    mul-int/lit8 v1, v0, 0x1f

    #@36
    iget v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mScale:F

    #@38
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@3b
    move-result v2

    #@3c
    add-int v0, v1, v2

    #@3e
    .line 1537
    iput v0, p0, Landroid/app/ActivityThread$ResourcesKey;->mHash:I

    #@40
    .line 1538
    return-void

    #@41
    .line 1534
    :cond_41
    const/4 v1, 0x0

    #@42
    goto :goto_32
.end method

.method static synthetic access$3000(Landroid/app/ActivityThread$ResourcesKey;)Landroid/content/res/Configuration;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1514
    iget-object v0, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@2
    return-object v0
.end method

.method static synthetic access$3300(Landroid/app/ActivityThread$ResourcesKey;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1514
    iget v0, p0, Landroid/app/ActivityThread$ResourcesKey;->mDisplayId:I

    #@2
    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1547
    instance-of v2, p1, Landroid/app/ActivityThread$ResourcesKey;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1568
    :cond_5
    :goto_5
    return v1

    #@6
    :cond_6
    move-object v0, p1

    #@7
    .line 1550
    check-cast v0, Landroid/app/ActivityThread$ResourcesKey;

    #@9
    .line 1551
    .local v0, peer:Landroid/app/ActivityThread$ResourcesKey;
    iget-object v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mResDir:Ljava/lang/String;

    #@b
    iget-object v3, v0, Landroid/app/ActivityThread$ResourcesKey;->mResDir:Ljava/lang/String;

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_5

    #@13
    .line 1554
    iget v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mDisplayId:I

    #@15
    iget v3, v0, Landroid/app/ActivityThread$ResourcesKey;->mDisplayId:I

    #@17
    if-ne v2, v3, :cond_5

    #@19
    .line 1557
    iget-object v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@1b
    iget-object v3, v0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@1d
    if-eq v2, v3, :cond_31

    #@1f
    .line 1558
    iget-object v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@21
    if-eqz v2, :cond_5

    #@23
    iget-object v2, v0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@25
    if-eqz v2, :cond_5

    #@27
    .line 1561
    iget-object v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@29
    iget-object v3, v0, Landroid/app/ActivityThread$ResourcesKey;->mOverrideConfiguration:Landroid/content/res/Configuration;

    #@2b
    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_5

    #@31
    .line 1565
    :cond_31
    iget v2, p0, Landroid/app/ActivityThread$ResourcesKey;->mScale:F

    #@33
    iget v3, v0, Landroid/app/ActivityThread$ResourcesKey;->mScale:F

    #@35
    cmpl-float v2, v2, v3

    #@37
    if-nez v2, :cond_5

    #@39
    .line 1568
    const/4 v1, 0x1

    #@3a
    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 1542
    iget v0, p0, Landroid/app/ActivityThread$ResourcesKey;->mHash:I

    #@2
    return v0
.end method
