.class public Landroid/app/admin/DevicePolicyManager;
.super Ljava/lang/Object;
.source "DevicePolicyManager.java"


# static fields
.field public static final ACTION_ADD_DEVICE_ADMIN:Ljava/lang/String; = "android.app.action.ADD_DEVICE_ADMIN"

.field public static final ACTION_DEVICE_POLICY_MANAGER_STATE_CHANGED:Ljava/lang/String; = "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

.field public static final ACTION_SET_NEW_PASSWORD:Ljava/lang/String; = "android.app.action.SET_NEW_PASSWORD"

.field public static final ACTION_START_ENCRYPTION:Ljava/lang/String; = "android.app.action.START_ENCRYPTION"

.field public static final ENCRYPTION_STATUS_ACTIVATING:I = 0x2

.field public static final ENCRYPTION_STATUS_ACTIVE:I = 0x3

.field public static final ENCRYPTION_STATUS_INACTIVE:I = 0x1

.field public static final ENCRYPTION_STATUS_UNSUPPORTED:I = 0x0

.field public static final EXTRA_ADD_EXPLANATION:Ljava/lang/String; = "android.app.extra.ADD_EXPLANATION"

.field public static final EXTRA_DEVICE_ADMIN:Ljava/lang/String; = "android.app.extra.DEVICE_ADMIN"

.field public static final KEYGUARD_DISABLE_FEATURES_ALL:I = 0x7fffffff

.field public static final KEYGUARD_DISABLE_FEATURES_NONE:I = 0x0

.field public static final KEYGUARD_DISABLE_SECURE_CAMERA:I = 0x2

.field public static final KEYGUARD_DISABLE_WIDGETS_ALL:I = 0x1

.field public static final PASSWORD_QUALITY_ALPHABETIC:I = 0x40000

.field public static final PASSWORD_QUALITY_ALPHANUMERIC:I = 0x50000

.field public static final PASSWORD_QUALITY_BIOMETRIC_WEAK:I = 0x8000

.field public static final PASSWORD_QUALITY_COMPLEX:I = 0x60000

.field public static final PASSWORD_QUALITY_NUMERIC:I = 0x20000

.field public static final PASSWORD_QUALITY_SOMETHING:I = 0x10000

.field public static final PASSWORD_QUALITY_UNSPECIFIED:I = 0x0

.field public static final RESET_PASSWORD_REQUIRE_ENTRY:I = 0x1

.field private static TAG:Ljava/lang/String; = null

.field public static final WIPE_EXTERNAL_STORAGE:I = 0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mService:Landroid/app/admin/IDevicePolicyManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const-string v0, "DevicePolicyManager"

    #@2
    sput-object v0, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 59
    iput-object p1, p0, Landroid/app/admin/DevicePolicyManager;->mContext:Landroid/content/Context;

    #@5
    .line 60
    const-string v0, "device_policy"

    #@7
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@11
    .line 62
    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/os/Handler;)Landroid/app/admin/DevicePolicyManager;
    .registers 4
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 66
    new-instance v0, Landroid/app/admin/DevicePolicyManager;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/app/admin/DevicePolicyManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@5
    .line 67
    .local v0, me:Landroid/app/admin/DevicePolicyManager;
    iget-object v1, v0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@7
    if-eqz v1, :cond_a

    #@9
    .end local v0           #me:Landroid/app/admin/DevicePolicyManager;
    :goto_9
    return-object v0

    #@a
    .restart local v0       #me:Landroid/app/admin/DevicePolicyManager;
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method


# virtual methods
.method public getActiveAdmins()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 149
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 151
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, v2}, Landroid/app/admin/IDevicePolicyManager;->getActiveAdmins(I)Ljava/util/List;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 156
    :goto_e
    return-object v1

    #@f
    .line 152
    :catch_f
    move-exception v0

    #@10
    .line 153
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 156
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public getAdminInfo(Landroid/content/ComponentName;)Landroid/app/admin/DeviceAdminInfo;
    .registers 9
    .parameter "cn"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1475
    :try_start_1
    iget-object v3, p0, Landroid/app/admin/DevicePolicyManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@6
    move-result-object v3

    #@7
    const/16 v5, 0x80

    #@9
    invoke-virtual {v3, p1, v5}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_c} :catch_1c

    #@c
    move-result-object v0

    #@d
    .line 1482
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    new-instance v2, Landroid/content/pm/ResolveInfo;

    #@f
    invoke-direct {v2}, Landroid/content/pm/ResolveInfo;-><init>()V

    #@12
    .line 1483
    .local v2, ri:Landroid/content/pm/ResolveInfo;
    iput-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@14
    .line 1486
    :try_start_14
    new-instance v3, Landroid/app/admin/DeviceAdminInfo;

    #@16
    iget-object v5, p0, Landroid/app/admin/DevicePolicyManager;->mContext:Landroid/content/Context;

    #@18
    invoke-direct {v3, v5, v2}, Landroid/app/admin/DeviceAdminInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_1b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_14 .. :try_end_1b} :catch_37
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_1b} :catch_52

    #@1b
    .line 1492
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #ri:Landroid/content/pm/ResolveInfo;
    :goto_1b
    return-object v3

    #@1c
    .line 1477
    :catch_1c
    move-exception v1

    #@1d
    .line 1478
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "Unable to retrieve device policy "

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-static {v3, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    move-object v3, v4

    #@36
    .line 1479
    goto :goto_1b

    #@37
    .line 1487
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v2       #ri:Landroid/content/pm/ResolveInfo;
    :catch_37
    move-exception v1

    #@38
    .line 1488
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    sget-object v3, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@3a
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v6, "Unable to parse device policy "

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    invoke-static {v3, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@50
    move-object v3, v4

    #@51
    .line 1489
    goto :goto_1b

    #@52
    .line 1490
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_52
    move-exception v1

    #@53
    .line 1491
    .local v1, e:Ljava/io/IOException;
    sget-object v3, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@55
    new-instance v5, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v6, "Unable to parse device policy "

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-static {v3, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6b
    move-object v3, v4

    #@6c
    .line 1492
    goto :goto_1b
.end method

.method public getCameraDisabled(Landroid/content/ComponentName;)Z
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 1387
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getCameraDisabled(Landroid/content/ComponentName;I)Z
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1392
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 1394
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;I)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 1399
    :goto_a
    return v1

    #@b
    .line 1395
    :catch_b
    move-exception v0

    #@c
    .line 1396
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 1399
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getCurrentFailedPasswordAttempts()I
    .registers 4

    #@0
    .prologue
    .line 908
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 910
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, v2}, Landroid/app/admin/IDevicePolicyManager;->getCurrentFailedPasswordAttempts(I)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 915
    :goto_e
    return v1

    #@f
    .line 911
    :catch_f
    move-exception v0

    #@10
    .line 912
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 915
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, -0x1

    #@18
    goto :goto_e
.end method

.method public getCurrentFailedPasswordAttemptsMDM()I
    .registers 4

    #@0
    .prologue
    .line 923
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 925
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, v2}, Landroid/app/admin/IDevicePolicyManager;->getCurrentFailedPasswordAttemptsMDM(I)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 930
    :goto_e
    return v1

    #@f
    .line 926
    :catch_f
    move-exception v0

    #@10
    .line 927
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 930
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, -0x1

    #@18
    goto :goto_e
.end method

.method public getGlobalProxyAdmin()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 1194
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 1196
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, v2}, Landroid/app/admin/IDevicePolicyManager;->getGlobalProxyAdmin(I)Landroid/content/ComponentName;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 1201
    :goto_e
    return-object v1

    #@f
    .line 1197
    :catch_f
    move-exception v0

    #@10
    .line 1198
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 1201
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public getKeyguardDisabledFeatures(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 1435
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1440
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 1442
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 1447
    :goto_a
    return v1

    #@b
    .line 1443
    :catch_b
    move-exception v0

    #@c
    .line 1444
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 1447
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 971
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 976
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 978
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 983
    :goto_a
    return v1

    #@b
    .line 979
    :catch_b
    move-exception v0

    #@c
    .line 980
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 983
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getMaximumTimeToLock(Landroid/content/ComponentName;)J
    .registers 4
    .parameter "admin"

    #@0
    .prologue
    .line 1055
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getMaximumTimeToLock(Landroid/content/ComponentName;I)J

    #@7
    move-result-wide v0

    #@8
    return-wide v0
.end method

.method public getMaximumTimeToLock(Landroid/content/ComponentName;I)J
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1060
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 1062
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getMaximumTimeToLock(Landroid/content/ComponentName;I)J
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result-wide v1

    #@a
    .line 1067
    :goto_a
    return-wide v1

    #@b
    .line 1063
    :catch_b
    move-exception v0

    #@c
    .line 1064
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 1067
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const-wide/16 v1, 0x0

    #@15
    goto :goto_a
.end method

.method public getPasswordExpiration(Landroid/content/ComponentName;)J
    .registers 5
    .parameter "admin"

    #@0
    .prologue
    .line 832
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 834
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordExpiration(Landroid/content/ComponentName;I)J
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result-wide v1

    #@e
    .line 839
    :goto_e
    return-wide v1

    #@f
    .line 835
    :catch_f
    move-exception v0

    #@10
    .line 836
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 839
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const-wide/16 v1, 0x0

    #@19
    goto :goto_e
.end method

.method public getPasswordExpirationTimeout(Landroid/content/ComponentName;)J
    .registers 5
    .parameter "admin"

    #@0
    .prologue
    .line 812
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 814
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordExpirationTimeout(Landroid/content/ComponentName;I)J
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result-wide v1

    #@e
    .line 819
    :goto_e
    return-wide v1

    #@f
    .line 815
    :catch_f
    move-exception v0

    #@10
    .line 816
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 819
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const-wide/16 v1, 0x0

    #@19
    goto :goto_e
.end method

.method public getPasswordHistoryLength(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 850
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordHistoryLength(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordHistoryLength(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 855
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 857
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordHistoryLength(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 862
    :goto_a
    return v1

    #@b
    .line 858
    :catch_b
    move-exception v0

    #@c
    .line 859
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 862
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMaximumLength(I)I
    .registers 3
    .parameter "quality"

    #@0
    .prologue
    .line 873
    const/16 v0, 0x10

    #@2
    return v0
.end method

.method public getPasswordMinimumLength(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 374
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumLength(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 379
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 381
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 386
    :goto_a
    return v1

    #@b
    .line 382
    :catch_b
    move-exception v0

    #@c
    .line 383
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 386
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMinimumLetters(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 552
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLetters(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumLetters(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 557
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 559
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumLetters(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 564
    :goto_a
    return v1

    #@b
    .line 560
    :catch_b
    move-exception v0

    #@c
    .line 561
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 564
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMinimumLowerCase(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 494
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 499
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 501
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 506
    :goto_a
    return v1

    #@b
    .line 502
    :catch_b
    move-exception v0

    #@c
    .line 503
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 506
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMinimumNonLetter(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 726
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 731
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 733
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 738
    :goto_a
    return v1

    #@b
    .line 734
    :catch_b
    move-exception v0

    #@c
    .line 735
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 738
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMinimumNumeric(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 610
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 615
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 617
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 622
    :goto_a
    return v1

    #@b
    .line 618
    :catch_b
    move-exception v0

    #@c
    .line 619
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 622
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMinimumSymbols(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 668
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 673
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 675
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 680
    :goto_a
    return v1

    #@b
    .line 676
    :catch_b
    move-exception v0

    #@c
    .line 677
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 680
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordMinimumUpperCase(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 434
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 439
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 441
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 446
    :goto_a
    return v1

    #@b
    .line 442
    :catch_b
    move-exception v0

    #@c
    .line 443
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 446
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getPasswordQuality(Landroid/content/ComponentName;)I
    .registers 3
    .parameter "admin"

    #@0
    .prologue
    .line 321
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPasswordQuality(Landroid/content/ComponentName;I)I
    .registers 6
    .parameter "admin"
    .parameter "userHandle"

    #@0
    .prologue
    .line 326
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 328
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1, p2}, Landroid/app/admin/IDevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 333
    :goto_a
    return v1

    #@b
    .line 329
    :catch_b
    move-exception v0

    #@c
    .line 330
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 333
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;)V
    .registers 6
    .parameter "admin"
    .parameter "result"

    #@0
    .prologue
    .line 1500
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 1502
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 1507
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1503
    :catch_e
    move-exception v0

    #@f
    .line 1504
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public getStorageEncryption(Landroid/content/ComponentName;)Z
    .registers 5
    .parameter "admin"

    #@0
    .prologue
    .line 1316
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 1318
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->getStorageEncryption(Landroid/content/ComponentName;I)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 1323
    :goto_e
    return v1

    #@f
    .line 1319
    :catch_f
    move-exception v0

    #@10
    .line 1320
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 1323
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public getStorageEncryptionStatus()I
    .registers 2

    #@0
    .prologue
    .line 1343
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/app/admin/DevicePolicyManager;->getStorageEncryptionStatus(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getStorageEncryptionStatus(I)I
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 1348
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 1350
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1}, Landroid/app/admin/IDevicePolicyManager;->getStorageEncryptionStatus(I)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 1355
    :goto_a
    return v1

    #@b
    .line 1351
    :catch_b
    move-exception v0

    #@c
    .line 1352
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@e
    const-string v2, "Failed talking with device policy service"

    #@10
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13
    .line 1355
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public hasGrantedPolicy(Landroid/content/ComponentName;I)Z
    .registers 6
    .parameter "admin"
    .parameter "usesPolicy"

    #@0
    .prologue
    .line 213
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 215
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->hasGrantedPolicy(Landroid/content/ComponentName;II)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 220
    :goto_e
    return v1

    #@f
    .line 216
    :catch_f
    move-exception v0

    #@10
    .line 217
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 220
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public isActivePasswordSufficient()Z
    .registers 4

    #@0
    .prologue
    .line 889
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 891
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, v2}, Landroid/app/admin/IDevicePolicyManager;->isActivePasswordSufficient(I)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 896
    :goto_e
    return v1

    #@f
    .line 892
    :catch_f
    move-exception v0

    #@10
    .line 893
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 896
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public isAdminActive(Landroid/content/ComponentName;)Z
    .registers 5
    .parameter "who"

    #@0
    .prologue
    .line 133
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 135
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;I)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 140
    :goto_e
    return v1

    #@f
    .line 136
    :catch_f
    move-exception v0

    #@10
    .line 137
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 140
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public lockNow()V
    .registers 4

    #@0
    .prologue
    .line 1079
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1081
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1}, Landroid/app/admin/IDevicePolicyManager;->lockNow()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1086
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1082
    :catch_a
    move-exception v0

    #@b
    .line 1083
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@d
    const-string v2, "Failed talking with device policy service"

    #@f
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public packageHasActiveAdmins(Ljava/lang/String;)Z
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 165
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 167
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->packageHasActiveAdmins(Ljava/lang/String;I)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 172
    :goto_e
    return v1

    #@f
    .line 168
    :catch_f
    move-exception v0

    #@10
    .line 169
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 172
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public removeActiveAdmin(Landroid/content/ComponentName;)V
    .registers 6
    .parameter "who"

    #@0
    .prologue
    .line 182
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_19

    #@4
    .line 185
    :try_start_4
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7
    move-result-object v1

    #@8
    if-eqz v1, :cond_1a

    #@a
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@d
    move-result-object v1

    #@e
    const/4 v2, 0x0

    #@f
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@12
    move-result v3

    #@13
    invoke-interface {v1, p1, v2, v3}, Lcom/lge/cappuccino/IMdm;->checkDisabledRemoveAdmin(Landroid/content/ComponentName;Ljava/lang/String;I)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_1a

    #@19
    .line 201
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 191
    :cond_1a
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@1c
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@1f
    move-result v2

    #@20
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->removeActiveAdmin(Landroid/content/ComponentName;I)V

    #@23
    .line 193
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@26
    move-result-object v1

    #@27
    if-eqz v1, :cond_19

    #@29
    .line 194
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2c
    move-result-object v1

    #@2d
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@30
    move-result v2

    #@31
    invoke-interface {v1, p1, v2}, Lcom/lge/cappuccino/IMdm;->removeActiveAdmin(Landroid/content/ComponentName;I)V
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_34} :catch_35

    #@34
    goto :goto_19

    #@35
    .line 197
    :catch_35
    move-exception v0

    #@36
    .line 198
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@38
    const-string v2, "Failed talking with device policy service"

    #@3a
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    goto :goto_19
.end method

.method public reportFailedPasswordAttempt(I)V
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 1528
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1530
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1}, Landroid/app/admin/IDevicePolicyManager;->reportFailedPasswordAttempt(I)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1535
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1531
    :catch_a
    move-exception v0

    #@b
    .line 1532
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@d
    const-string v2, "Failed talking with device policy service"

    #@f
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public reportSuccessfulPasswordAttempt(I)V
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 1541
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1543
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-interface {v1, p1}, Landroid/app/admin/IDevicePolicyManager;->reportSuccessfulPasswordAttempt(I)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1548
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1544
    :catch_a
    move-exception v0

    #@b
    .line 1545
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@d
    const-string v2, "Failed talking with device policy service"

    #@f
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    goto :goto_9
.end method

.method public resetPassword(Ljava/lang/String;I)Z
    .registers 6
    .parameter "password"
    .parameter "flags"

    #@0
    .prologue
    .line 1015
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 1017
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->resetPassword(Ljava/lang/String;II)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 1022
    :goto_e
    return v1

    #@f
    .line 1018
    :catch_f
    move-exception v0

    #@10
    .line 1019
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 1022
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public setActiveAdmin(Landroid/content/ComponentName;Z)V
    .registers 6
    .parameter "policyReceiver"
    .parameter "refreshing"

    #@0
    .prologue
    .line 1454
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_1e

    #@4
    .line 1456
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setActiveAdmin(Landroid/content/ComponentName;ZI)V

    #@d
    .line 1458
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@10
    move-result-object v1

    #@11
    if-eqz v1, :cond_1e

    #@13
    .line 1459
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@16
    move-result-object v1

    #@17
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@1a
    move-result v2

    #@1b
    invoke-interface {v1, p1, p2, v2}, Lcom/lge/cappuccino/IMdm;->setActiveAdmin(Landroid/content/ComponentName;ZI)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_1e} :catch_1f

    #@1e
    .line 1466
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 1462
    :catch_1f
    move-exception v0

    #@20
    .line 1463
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@22
    const-string v2, "Failed talking with device policy service"

    #@24
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_1e
.end method

.method public setActivePasswordState(IIIIIIIII)V
    .registers 21
    .parameter "quality"
    .parameter "length"
    .parameter "letters"
    .parameter "uppercase"
    .parameter "lowercase"
    .parameter "numbers"
    .parameter "symbols"
    .parameter "nonletter"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1514
    iget-object v0, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 1516
    :try_start_4
    iget-object v0, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    move v1, p1

    #@7
    move v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    move/from16 v5, p5

    #@c
    move/from16 v6, p6

    #@e
    move/from16 v7, p7

    #@10
    move/from16 v8, p8

    #@12
    move/from16 v9, p9

    #@14
    invoke-interface/range {v0 .. v9}, Landroid/app/admin/IDevicePolicyManager;->setActivePasswordState(IIIIIIIII)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_17} :catch_18

    #@17
    .line 1522
    :cond_17
    :goto_17
    return-void

    #@18
    .line 1518
    :catch_18
    move-exception v10

    #@19
    .line 1519
    .local v10, e:Landroid/os/RemoteException;
    sget-object v0, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@1b
    const-string v1, "Failed talking with device policy service"

    #@1d
    invoke-static {v0, v1, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@20
    goto :goto_17
.end method

.method public setCameraDisabled(Landroid/content/ComponentName;Z)V
    .registers 6
    .parameter "admin"
    .parameter "disabled"

    #@0
    .prologue
    .line 1371
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 1373
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setCameraDisabled(Landroid/content/ComponentName;ZI)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 1378
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1374
    :catch_e
    move-exception v0

    #@f
    .line 1375
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setGlobalProxy(Landroid/content/ComponentName;Ljava/net/Proxy;Ljava/util/List;)Landroid/content/ComponentName;
    .registers 17
    .parameter "admin"
    .parameter "proxySpec"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "Ljava/net/Proxy;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation

    #@0
    .prologue
    .line 1142
    .local p3, exclusionList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p2, :cond_8

    #@2
    .line 1143
    new-instance v11, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v11}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v11

    #@8
    .line 1145
    :cond_8
    iget-object v11, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@a
    if-eqz v11, :cond_3b

    #@c
    .line 1149
    :try_start_c
    sget-object v11, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    #@e
    invoke-virtual {p2, v11}, Ljava/net/Proxy;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v11

    #@12
    if-eqz v11, :cond_21

    #@14
    .line 1150
    const/4 v6, 0x0

    #@15
    .line 1151
    .local v6, hostSpec:Ljava/lang/String;
    const/4 v2, 0x0

    #@16
    .line 1179
    .local v2, exclSpec:Ljava/lang/String;
    :goto_16
    iget-object v11, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@18
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@1b
    move-result v12

    #@1c
    invoke-interface {v11, p1, v6, v2, v12}, Landroid/app/admin/IDevicePolicyManager;->setGlobalProxy(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/ComponentName;

    #@1f
    move-result-object v11

    #@20
    .line 1184
    .end local v2           #exclSpec:Ljava/lang/String;
    .end local v6           #hostSpec:Ljava/lang/String;
    :goto_20
    return-object v11

    #@21
    .line 1153
    :cond_21
    invoke-virtual {p2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    #@24
    move-result-object v11

    #@25
    sget-object v12, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    #@27
    invoke-virtual {v11, v12}, Ljava/net/Proxy$Type;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v11

    #@2b
    if-nez v11, :cond_3d

    #@2d
    .line 1154
    new-instance v11, Ljava/lang/IllegalArgumentException;

    #@2f
    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@32
    throw v11
    :try_end_33
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_33} :catch_33

    #@33
    .line 1180
    :catch_33
    move-exception v0

    #@34
    .line 1181
    .local v0, e:Landroid/os/RemoteException;
    sget-object v11, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@36
    const-string v12, "Failed talking with device policy service"

    #@38
    invoke-static {v11, v12, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3b
    .line 1184
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_3b
    const/4 v11, 0x0

    #@3c
    goto :goto_20

    #@3d
    .line 1156
    :cond_3d
    :try_start_3d
    invoke-virtual {p2}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    #@40
    move-result-object v10

    #@41
    check-cast v10, Ljava/net/InetSocketAddress;

    #@43
    .line 1157
    .local v10, sa:Ljava/net/InetSocketAddress;
    invoke-virtual {v10}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    .line 1158
    .local v5, hostName:Ljava/lang/String;
    invoke-virtual {v10}, Ljava/net/InetSocketAddress;->getPort()I

    #@4a
    move-result v9

    #@4b
    .line 1159
    .local v9, port:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    .line 1160
    .local v4, hostBuilder:Ljava/lang/StringBuilder;
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v11

    #@54
    const-string v12, ":"

    #@56
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v11

    #@5a
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5d
    move-result-object v12

    #@5e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v11

    #@62
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    .line 1162
    .restart local v6       #hostSpec:Ljava/lang/String;
    if-nez p3, :cond_72

    #@68
    .line 1163
    const-string v2, ""

    #@6a
    .line 1177
    .restart local v2       #exclSpec:Ljava/lang/String;
    :goto_6a
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6d
    move-result-object v11

    #@6e
    invoke-static {v5, v11, v2}, Landroid/net/Proxy;->validate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@71
    goto :goto_16

    #@72
    .line 1165
    .end local v2           #exclSpec:Ljava/lang/String;
    :cond_72
    new-instance v8, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    .line 1166
    .local v8, listBuilder:Ljava/lang/StringBuilder;
    const/4 v3, 0x1

    #@78
    .line 1167
    .local v3, firstDomain:Z
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@7b
    move-result-object v7

    #@7c
    .local v7, i$:Ljava/util/Iterator;
    :goto_7c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@7f
    move-result v11

    #@80
    if-eqz v11, :cond_9b

    #@82
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@85
    move-result-object v1

    #@86
    check-cast v1, Ljava/lang/String;

    #@88
    .line 1168
    .local v1, exclDomain:Ljava/lang/String;
    if-nez v3, :cond_99

    #@8a
    .line 1169
    const-string v11, ","

    #@8c
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    .line 1173
    :goto_90
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@93
    move-result-object v11

    #@94
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v8

    #@98
    goto :goto_7c

    #@99
    .line 1171
    :cond_99
    const/4 v3, 0x0

    #@9a
    goto :goto_90

    #@9b
    .line 1175
    .end local v1           #exclDomain:Ljava/lang/String;
    :cond_9b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_9e
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_9e} :catch_33

    #@9e
    move-result-object v2

    #@9f
    .restart local v2       #exclSpec:Ljava/lang/String;
    goto :goto_6a
.end method

.method public setKeyguardDisabledFeatures(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "which"

    #@0
    .prologue
    .line 1417
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 1419
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setKeyguardDisabledFeatures(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 1424
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1420
    :catch_e
    move-exception v0

    #@f
    .line 1421
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "num"

    #@0
    .prologue
    .line 954
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 956
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 961
    :cond_d
    :goto_d
    return-void

    #@e
    .line 957
    :catch_e
    move-exception v0

    #@f
    .line 958
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setMaximumTimeToLock(Landroid/content/ComponentName;J)V
    .registers 7
    .parameter "admin"
    .parameter "timeMs"

    #@0
    .prologue
    .line 1039
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 1041
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, p3, v2}, Landroid/app/admin/IDevicePolicyManager;->setMaximumTimeToLock(Landroid/content/ComponentName;JI)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 1046
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1042
    :catch_e
    move-exception v0

    #@f
    .line 1043
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordExpirationTimeout(Landroid/content/ComponentName;J)V
    .registers 7
    .parameter "admin"
    .parameter "timeout"

    #@0
    .prologue
    .line 793
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 795
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, p3, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordExpirationTimeout(Landroid/content/ComponentName;JI)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 800
    :cond_d
    :goto_d
    return-void

    #@e
    .line 796
    :catch_e
    move-exception v0

    #@f
    .line 797
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordHistoryLength(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 764
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 766
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordHistoryLength(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 771
    :cond_d
    :goto_d
    return-void

    #@e
    .line 767
    :catch_e
    move-exception v0

    #@f
    .line 768
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumLength(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 358
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 360
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumLength(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 365
    :cond_d
    :goto_d
    return-void

    #@e
    .line 361
    :catch_e
    move-exception v0

    #@f
    .line 362
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumLetters(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 531
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 533
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumLetters(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 538
    :cond_d
    :goto_d
    return-void

    #@e
    .line 534
    :catch_e
    move-exception v0

    #@f
    .line 535
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumLowerCase(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 472
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 474
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumLowerCase(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 479
    :cond_d
    :goto_d
    return-void

    #@e
    .line 475
    :catch_e
    move-exception v0

    #@f
    .line 476
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumNonLetter(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 705
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 707
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumNonLetter(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 712
    :cond_d
    :goto_d
    return-void

    #@e
    .line 708
    :catch_e
    move-exception v0

    #@f
    .line 709
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumNumeric(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 589
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 591
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumNumeric(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 596
    :cond_d
    :goto_d
    return-void

    #@e
    .line 592
    :catch_e
    move-exception v0

    #@f
    .line 593
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumSymbols(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 647
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 649
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumSymbols(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 654
    :cond_d
    :goto_d
    return-void

    #@e
    .line 650
    :catch_e
    move-exception v0

    #@f
    .line 651
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordMinimumUpperCase(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "length"

    #@0
    .prologue
    .line 412
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 414
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordMinimumUpperCase(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 419
    :cond_d
    :goto_d
    return-void

    #@e
    .line 415
    :catch_e
    move-exception v0

    #@f
    .line 416
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setPasswordQuality(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "admin"
    .parameter "quality"

    #@0
    .prologue
    .line 305
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 307
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setPasswordQuality(Landroid/content/ComponentName;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 312
    :cond_d
    :goto_d
    return-void

    #@e
    .line 308
    :catch_e
    move-exception v0

    #@f
    .line 309
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method

.method public setStorageEncryption(Landroid/content/ComponentName;Z)I
    .registers 6
    .parameter "admin"
    .parameter "encrypt"

    #@0
    .prologue
    .line 1296
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_17

    #@4
    .line 1298
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, p2, v2}, Landroid/app/admin/IDevicePolicyManager;->setStorageEncryption(Landroid/content/ComponentName;ZI)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 1303
    :goto_e
    return v1

    #@f
    .line 1299
    :catch_f
    move-exception v0

    #@10
    .line 1300
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@12
    const-string v2, "Failed talking with device policy service"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 1303
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method public wipeData(I)V
    .registers 5
    .parameter "flags"

    #@0
    .prologue
    .line 1108
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@2
    if-eqz v1, :cond_d

    #@4
    .line 1110
    :try_start_4
    iget-object v1, p0, Landroid/app/admin/DevicePolicyManager;->mService:Landroid/app/admin/IDevicePolicyManager;

    #@6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@9
    move-result v2

    #@a
    invoke-interface {v1, p1, v2}, Landroid/app/admin/IDevicePolicyManager;->wipeData(II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_e

    #@d
    .line 1115
    :cond_d
    :goto_d
    return-void

    #@e
    .line 1111
    :catch_e
    move-exception v0

    #@f
    .line 1112
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/admin/DevicePolicyManager;->TAG:Ljava/lang/String;

    #@11
    const-string v2, "Failed talking with device policy service"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    goto :goto_d
.end method
