.class public final Landroid/app/admin/DeviceAdminInfo;
.super Ljava/lang/Object;
.source "DeviceAdminInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/admin/DeviceAdminInfo$PolicyInfo;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/admin/DeviceAdminInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String; = "DeviceAdminInfo"

.field public static final USES_ENCRYPTED_STORAGE:I = 0x7

.field public static final USES_POLICY_DISABLE_CAMERA:I = 0x8

.field public static final USES_POLICY_DISABLE_KEYGUARD_FEATURES:I = 0x9

.field public static final USES_POLICY_EXPIRE_PASSWORD:I = 0x6

.field public static final USES_POLICY_FORCE_LOCK:I = 0x3

.field public static final USES_POLICY_LIMIT_PASSWORD:I = 0x0

.field public static final USES_POLICY_RESET_PASSWORD:I = 0x2

.field public static final USES_POLICY_SETS_GLOBAL_PROXY:I = 0x5

.field public static final USES_POLICY_WATCH_LOGIN:I = 0x1

.field public static final USES_POLICY_WIPE_DATA:I = 0x4

.field static sKnownPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static sPoliciesDisplayOrder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/admin/DeviceAdminInfo$PolicyInfo;",
            ">;"
        }
    .end annotation
.end field

.field static sRevKnownPolicies:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/app/admin/DeviceAdminInfo$PolicyInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mReceiver:Landroid/content/pm/ResolveInfo;

.field mUsesPolicies:I

.field mVisible:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    .line 164
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    sput-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@7
    .line 165
    new-instance v2, Ljava/util/HashMap;

    #@9
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@c
    sput-object v2, Landroid/app/admin/DeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    #@e
    .line 166
    new-instance v2, Landroid/util/SparseArray;

    #@10
    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    #@13
    sput-object v2, Landroid/app/admin/DeviceAdminInfo;->sRevKnownPolicies:Landroid/util/SparseArray;

    #@15
    .line 169
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@17
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@19
    const/4 v4, 0x4

    #@1a
    const-string/jumbo v5, "wipe-data"

    #@1d
    const v6, 0x10402aa

    #@20
    const v7, 0x10402ab

    #@23
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@26
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29
    .line 172
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@2b
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@2d
    const/4 v4, 0x2

    #@2e
    const-string/jumbo v5, "reset-password"

    #@31
    const v6, 0x10402a6

    #@34
    const v7, 0x10402a7

    #@37
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@3a
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d
    .line 175
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@3f
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@41
    const/4 v4, 0x0

    #@42
    const-string/jumbo v5, "limit-password"

    #@45
    const v6, 0x10402a2

    #@48
    const v7, 0x10402a3

    #@4b
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@4e
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    .line 178
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@53
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@55
    const/4 v4, 0x1

    #@56
    const-string/jumbo v5, "watch-login"

    #@59
    const v6, 0x10402a4

    #@5c
    const v7, 0x10402a5

    #@5f
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@62
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@65
    .line 181
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@67
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@69
    const/4 v4, 0x3

    #@6a
    const-string v5, "force-lock"

    #@6c
    const v6, 0x10402a8

    #@6f
    const v7, 0x10402a9

    #@72
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@75
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@78
    .line 184
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@7a
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@7c
    const/4 v4, 0x5

    #@7d
    const-string/jumbo v5, "set-global-proxy"

    #@80
    const v6, 0x10402ac

    #@83
    const v7, 0x10402ad

    #@86
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@89
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8c
    .line 187
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@8e
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@90
    const/4 v4, 0x6

    #@91
    const-string v5, "expire-password"

    #@93
    const v6, 0x10402ae

    #@96
    const v7, 0x10402af

    #@99
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@9c
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9f
    .line 190
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@a1
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@a3
    const/4 v4, 0x7

    #@a4
    const-string v5, "encrypted-storage"

    #@a6
    const v6, 0x10402b0

    #@a9
    const v7, 0x10402b1

    #@ac
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@af
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b2
    .line 193
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@b4
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@b6
    const/16 v4, 0x8

    #@b8
    const-string v5, "disable-camera"

    #@ba
    const v6, 0x10402b2

    #@bd
    const v7, 0x10402b3

    #@c0
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@c3
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c6
    .line 196
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@c8
    new-instance v3, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@ca
    const/16 v4, 0x9

    #@cc
    const-string v5, "disable-keyguard-features"

    #@ce
    const v6, 0x10402b4

    #@d1
    const v7, 0x10402b5

    #@d4
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    #@d7
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@da
    .line 201
    const/4 v0, 0x0

    #@db
    .local v0, i:I
    :goto_db
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@dd
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@e0
    move-result v2

    #@e1
    if-ge v0, v2, :cond_102

    #@e3
    .line 202
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@e5
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e8
    move-result-object v1

    #@e9
    check-cast v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@eb
    .line 203
    .local v1, pi:Landroid/app/admin/DeviceAdminInfo$PolicyInfo;
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sRevKnownPolicies:Landroid/util/SparseArray;

    #@ed
    iget v3, v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->ident:I

    #@ef
    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@f2
    .line 204
    sget-object v2, Landroid/app/admin/DeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    #@f4
    iget-object v3, v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->tag:Ljava/lang/String;

    #@f6
    iget v4, v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->ident:I

    #@f8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fb
    move-result-object v4

    #@fc
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@ff
    .line 201
    add-int/lit8 v0, v0, 0x1

    #@101
    goto :goto_db

    #@102
    .line 484
    .end local v1           #pi:Landroid/app/admin/DeviceAdminInfo$PolicyInfo;
    :cond_102
    new-instance v2, Landroid/app/admin/DeviceAdminInfo$1;

    #@104
    invoke-direct {v2}, Landroid/app/admin/DeviceAdminInfo$1;-><init>()V

    #@107
    sput-object v2, Landroid/app/admin/DeviceAdminInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .registers 22
    .parameter "context"
    .parameter "receiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 231
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 232
    move-object/from16 v0, p2

    #@5
    move-object/from16 v1, p0

    #@7
    iput-object v0, v1, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@9
    .line 233
    move-object/from16 v0, p2

    #@b
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@d
    .line 235
    .local v2, ai:Landroid/content/pm/ActivityInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@10
    move-result-object v9

    #@11
    .line 237
    .local v9, pm:Landroid/content/pm/PackageManager;
    const/4 v8, 0x0

    #@12
    .line 239
    .local v8, parser:Landroid/content/res/XmlResourceParser;
    :try_start_12
    const-string v16, "android.app.device_admin"

    #@14
    move-object/from16 v0, v16

    #@16
    invoke-virtual {v2, v9, v0}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@19
    move-result-object v8

    #@1a
    .line 240
    if-nez v8, :cond_49

    #@1c
    .line 241
    new-instance v16, Lorg/xmlpull/v1/XmlPullParserException;

    #@1e
    const-string v17, "No android.app.device_admin meta-data"

    #@20
    invoke-direct/range {v16 .. v17}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@23
    throw v16
    :try_end_24
    .catchall {:try_start_12 .. :try_end_24} :catchall_42
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_24} :catch_24

    #@24
    .line 305
    :catch_24
    move-exception v4

    #@25
    .line 306
    .local v4, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_25
    new-instance v16, Lorg/xmlpull/v1/XmlPullParserException;

    #@27
    new-instance v17, Ljava/lang/StringBuilder;

    #@29
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v18, "Unable to create context for: "

    #@2e
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v17

    #@32
    iget-object v0, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@34
    move-object/from16 v18, v0

    #@36
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v17

    #@3a
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v17

    #@3e
    invoke-direct/range {v16 .. v17}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@41
    throw v16
    :try_end_42
    .catchall {:try_start_25 .. :try_end_42} :catchall_42

    #@42
    .line 309
    .end local v4           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_42
    move-exception v16

    #@43
    if-eqz v8, :cond_48

    #@45
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->close()V

    #@48
    :cond_48
    throw v16

    #@49
    .line 245
    :cond_49
    :try_start_49
    iget-object v0, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4b
    move-object/from16 v16, v0

    #@4d
    move-object/from16 v0, v16

    #@4f
    invoke-virtual {v9, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@52
    move-result-object v11

    #@53
    .line 247
    .local v11, res:Landroid/content/res/Resources;
    invoke-static {v8}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@56
    move-result-object v3

    #@57
    .line 251
    .local v3, attrs:Landroid/util/AttributeSet;
    :cond_57
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->next()I

    #@5a
    move-result v14

    #@5b
    .local v14, type:I
    const/16 v16, 0x1

    #@5d
    move/from16 v0, v16

    #@5f
    if-eq v14, v0, :cond_67

    #@61
    const/16 v16, 0x2

    #@63
    move/from16 v0, v16

    #@65
    if-ne v14, v0, :cond_57

    #@67
    .line 254
    :cond_67
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    .line 255
    .local v6, nodeName:Ljava/lang/String;
    const-string v16, "device-admin"

    #@6d
    move-object/from16 v0, v16

    #@6f
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v16

    #@73
    if-nez v16, :cond_7d

    #@75
    .line 256
    new-instance v16, Lorg/xmlpull/v1/XmlPullParserException;

    #@77
    const-string v17, "Meta-data does not start with device-admin tag"

    #@79
    invoke-direct/range {v16 .. v17}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@7c
    throw v16

    #@7d
    .line 260
    :cond_7d
    sget-object v16, Lcom/android/internal/R$styleable;->DeviceAdmin:[I

    #@7f
    move-object/from16 v0, v16

    #@81
    invoke-virtual {v11, v3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@84
    move-result-object v12

    #@85
    .line 263
    .local v12, sa:Landroid/content/res/TypedArray;
    const/16 v16, 0x0

    #@87
    const/16 v17, 0x1

    #@89
    move/from16 v0, v16

    #@8b
    move/from16 v1, v17

    #@8d
    invoke-virtual {v12, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@90
    move-result v16

    #@91
    move/from16 v0, v16

    #@93
    move-object/from16 v1, p0

    #@95
    iput-boolean v0, v1, Landroid/app/admin/DeviceAdminInfo;->mVisible:Z

    #@97
    .line 266
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->recycle()V

    #@9a
    .line 268
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@9d
    move-result v7

    #@9e
    .line 270
    .local v7, outerDepth:I
    :cond_9e
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->next()I

    #@a1
    move-result v14

    #@a2
    const/16 v16, 0x1

    #@a4
    move/from16 v0, v16

    #@a6
    if-eq v14, v0, :cond_15c

    #@a8
    const/16 v16, 0x3

    #@aa
    move/from16 v0, v16

    #@ac
    if-ne v14, v0, :cond_b6

    #@ae
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@b1
    move-result v16

    #@b2
    move/from16 v0, v16

    #@b4
    if-le v0, v7, :cond_15c

    #@b6
    .line 271
    :cond_b6
    const/16 v16, 0x3

    #@b8
    move/from16 v0, v16

    #@ba
    if-eq v14, v0, :cond_9e

    #@bc
    const/16 v16, 0x4

    #@be
    move/from16 v0, v16

    #@c0
    if-eq v14, v0, :cond_9e

    #@c2
    .line 274
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@c5
    move-result-object v13

    #@c6
    .line 275
    .local v13, tagName:Ljava/lang/String;
    const-string/jumbo v16, "uses-policies"

    #@c9
    move-object/from16 v0, v16

    #@cb
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v16

    #@cf
    if-eqz v16, :cond_9e

    #@d1
    .line 276
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@d4
    move-result v5

    #@d5
    .line 278
    .local v5, innerDepth:I
    :cond_d5
    :goto_d5
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->next()I

    #@d8
    move-result v14

    #@d9
    const/16 v16, 0x1

    #@db
    move/from16 v0, v16

    #@dd
    if-eq v14, v0, :cond_9e

    #@df
    const/16 v16, 0x3

    #@e1
    move/from16 v0, v16

    #@e3
    if-ne v14, v0, :cond_ed

    #@e5
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getDepth()I

    #@e8
    move-result v16

    #@e9
    move/from16 v0, v16

    #@eb
    if-le v0, v5, :cond_9e

    #@ed
    .line 279
    :cond_ed
    const/16 v16, 0x3

    #@ef
    move/from16 v0, v16

    #@f1
    if-eq v14, v0, :cond_d5

    #@f3
    const/16 v16, 0x4

    #@f5
    move/from16 v0, v16

    #@f7
    if-eq v14, v0, :cond_d5

    #@f9
    .line 282
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@fc
    move-result-object v10

    #@fd
    .line 283
    .local v10, policyName:Ljava/lang/String;
    sget-object v16, Landroid/app/admin/DeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    #@ff
    move-object/from16 v0, v16

    #@101
    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@104
    move-result-object v15

    #@105
    check-cast v15, Ljava/lang/Integer;

    #@107
    .line 284
    .local v15, val:Ljava/lang/Integer;
    if-eqz v15, :cond_120

    #@109
    .line 285
    move-object/from16 v0, p0

    #@10b
    iget v0, v0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@10d
    move/from16 v16, v0

    #@10f
    const/16 v17, 0x1

    #@111
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    #@114
    move-result v18

    #@115
    shl-int v17, v17, v18

    #@117
    or-int v16, v16, v17

    #@119
    move/from16 v0, v16

    #@11b
    move-object/from16 v1, p0

    #@11d
    iput v0, v1, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@11f
    goto :goto_d5

    #@120
    .line 292
    :cond_120
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@123
    move-result-object v16

    #@124
    if-eqz v16, :cond_132

    #@126
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@129
    move-result-object v16

    #@12a
    move-object/from16 v0, v16

    #@12c
    invoke-interface {v0, v10}, Lcom/lge/cappuccino/IMdm;->isLGMDMPolicyTag(Ljava/lang/String;)Z

    #@12f
    move-result v16

    #@130
    if-nez v16, :cond_d5

    #@132
    .line 297
    :cond_132
    const-string v16, "DeviceAdminInfo"

    #@134
    new-instance v17, Ljava/lang/StringBuilder;

    #@136
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    const-string v18, "Unknown tag under uses-policies of "

    #@13b
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v17

    #@13f
    invoke-virtual/range {p0 .. p0}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@142
    move-result-object v18

    #@143
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@146
    move-result-object v17

    #@147
    const-string v18, ": "

    #@149
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v17

    #@14d
    move-object/from16 v0, v17

    #@14f
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v17

    #@153
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v17

    #@157
    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15a
    .catchall {:try_start_49 .. :try_end_15a} :catchall_42
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_49 .. :try_end_15a} :catch_24

    #@15a
    goto/16 :goto_d5

    #@15c
    .line 309
    .end local v5           #innerDepth:I
    .end local v10           #policyName:Ljava/lang/String;
    .end local v13           #tagName:Ljava/lang/String;
    .end local v15           #val:Ljava/lang/Integer;
    :cond_15c
    if-eqz v8, :cond_161

    #@15e
    invoke-interface {v8}, Landroid/content/res/XmlResourceParser;->close()V

    #@161
    .line 311
    :cond_161
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 313
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 314
    sget-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@b
    iput-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@d
    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@13
    .line 316
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 496
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 461
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "Receiver:"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@16
    .line 462
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "  "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ResolveInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@2e
    .line 463
    return-void
.end method

.method public getActivityInfo()Landroid/content/pm/ActivityInfo;
    .registers 2

    #@0
    .prologue
    .line 338
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    return-object v0
.end method

.method public getComponent()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 345
    new-instance v0, Landroid/content/ComponentName;

    #@2
    iget-object v1, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@4
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@6
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8
    iget-object v2, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@a
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@c
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@e
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 322
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getReceiverName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getTagForPolicy(I)Ljava/lang/String;
    .registers 3
    .parameter "policyIdent"

    #@0
    .prologue
    .line 416
    sget-object v0, Landroid/app/admin/DeviceAdminInfo;->sRevKnownPolicies:Landroid/util/SparseArray;

    #@2
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@8
    iget-object v0, v0, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->tag:Ljava/lang/String;

    #@a
    return-object v0
.end method

.method public getUsedPolicies()Ljava/util/ArrayList;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/admin/DeviceAdminInfo$PolicyInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 421
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 422
    .local v2, res:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/admin/DeviceAdminInfo$PolicyInfo;>;"
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    sget-object v3, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    if-ge v0, v3, :cond_24

    #@e
    .line 423
    sget-object v3, Landroid/app/admin/DeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    #@16
    .line 424
    .local v1, pi:Landroid/app/admin/DeviceAdminInfo$PolicyInfo;
    iget v3, v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->ident:I

    #@18
    invoke-virtual {p0, v3}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_21

    #@1e
    .line 425
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 422
    :cond_21
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_6

    #@24
    .line 428
    .end local v1           #pi:Landroid/app/admin/DeviceAdminInfo$PolicyInfo;
    :cond_24
    return-object v2
.end method

.method public isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 394
    iget-boolean v0, p0, Landroid/app/admin/DeviceAdminInfo;->mVisible:Z

    #@2
    return v0
.end method

.method public loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "pm"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 366
    iget-object v2, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    iget v2, v2, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@6
    if-eqz v2, :cond_26

    #@8
    .line 367
    iget-object v2, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@a
    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@c
    .line 368
    .local v1, packageName:Ljava/lang/String;
    const/4 v0, 0x0

    #@d
    .line 369
    .local v0, applicationInfo:Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_1b

    #@f
    .line 370
    iget-object v2, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@11
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@13
    iget-object v1, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@15
    .line 371
    iget-object v2, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@17
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@19
    iget-object v0, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1b
    .line 373
    :cond_1b
    iget-object v2, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@1d
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@1f
    iget v2, v2, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@21
    invoke-virtual {p1, v1, v2, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@24
    move-result-object v2

    #@25
    return-object v2

    #@26
    .line 376
    .end local v0           #applicationInfo:Landroid/content/pm/ApplicationInfo;
    .end local v1           #packageName:Ljava/lang/String;
    :cond_26
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@28
    invoke-direct {v2}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    #@2b
    throw v2
.end method

.method public loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public readPoliciesFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 7
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 445
    const/4 v2, 0x0

    #@1
    const-string v3, "flags"

    #@3
    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@a
    move-result-wide v0

    #@b
    .line 446
    .local v0, tempUsesPolicies:J
    const-wide/16 v2, 0x0

    #@d
    cmp-long v2, v0, v2

    #@f
    if-gez v2, :cond_1f

    #@11
    .line 447
    long-to-int v2, v0

    #@12
    invoke-static {v2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    const/4 v3, 0x2

    #@1b
    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@1e
    move-result-wide v0

    #@1f
    .line 450
    :cond_1f
    const-wide/16 v2, 0x400

    #@21
    cmp-long v2, v0, v2

    #@23
    if-lez v2, :cond_47

    #@25
    .line 451
    const-wide/16 v2, 0x1ff

    #@27
    and-long/2addr v2, v0

    #@28
    long-to-int v2, v2

    #@29
    iput v2, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@2b
    .line 452
    const-string v2, "MDM:DeviceAdminInfo"

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string/jumbo v4, "isUpgrade: "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    iget v4, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 458
    :goto_46
    return-void

    #@47
    .line 454
    :cond_47
    long-to-int v2, v0

    #@48
    iput v2, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@4a
    .line 455
    const-string v2, "MDM:DeviceAdminInfo"

    #@4c
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v4, "isCommon: "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    iget v4, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_46
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DeviceAdminInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@d
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@f
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string/jumbo v1, "}"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    return-object v0
.end method

.method public usesPolicy(I)Z
    .registers 5
    .parameter "policyIdent"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 407
    iget v1, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@3
    shl-int v2, v0, p1

    #@5
    and-int/2addr v1, v2

    #@6
    if-eqz v1, :cond_9

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public writePoliciesToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 5
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 434
    const/4 v0, 0x0

    #@1
    const-string v1, "flags"

    #@3
    iget v2, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@5
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@c
    .line 435
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 477
    iget-object v0, p0, Landroid/app/admin/DeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 478
    iget v0, p0, Landroid/app/admin/DeviceAdminInfo;->mUsesPolicies:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 479
    return-void
.end method
