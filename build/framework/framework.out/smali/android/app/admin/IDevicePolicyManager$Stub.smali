.class public abstract Landroid/app/admin/IDevicePolicyManager$Stub;
.super Landroid/os/Binder;
.source "IDevicePolicyManager.java"

# interfaces
.implements Landroid/app/admin/IDevicePolicyManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/admin/IDevicePolicyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/admin/IDevicePolicyManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.admin.IDevicePolicyManager"

.field static final TRANSACTION_getActiveAdmins:I = 0x2a

.field static final TRANSACTION_getCameraDisabled:I = 0x25

.field static final TRANSACTION_getCurrentFailedPasswordAttempts:I = 0x17

.field static final TRANSACTION_getCurrentFailedPasswordAttemptsMDM:I = 0x32

.field static final TRANSACTION_getGlobalProxyAdmin:I = 0x20

.field static final TRANSACTION_getKeyguardDisabledFeatures:I = 0x27

.field static final TRANSACTION_getMaximumFailedPasswordsForWipe:I = 0x19

.field static final TRANSACTION_getMaximumTimeToLock:I = 0x1c

.field static final TRANSACTION_getPasswordExpiration:I = 0x15

.field static final TRANSACTION_getPasswordExpirationTimeout:I = 0x14

.field static final TRANSACTION_getPasswordHistoryLength:I = 0x12

.field static final TRANSACTION_getPasswordMinimumLength:I = 0x4

.field static final TRANSACTION_getPasswordMinimumLetters:I = 0xa

.field static final TRANSACTION_getPasswordMinimumLowerCase:I = 0x8

.field static final TRANSACTION_getPasswordMinimumNonLetter:I = 0x10

.field static final TRANSACTION_getPasswordMinimumNumeric:I = 0xc

.field static final TRANSACTION_getPasswordMinimumSymbols:I = 0xe

.field static final TRANSACTION_getPasswordMinimumUpperCase:I = 0x6

.field static final TRANSACTION_getPasswordQuality:I = 0x2

.field static final TRANSACTION_getRemoveWarning:I = 0x2c

.field static final TRANSACTION_getStorageEncryption:I = 0x22

.field static final TRANSACTION_getStorageEncryptionStatus:I = 0x23

.field static final TRANSACTION_hasGrantedPolicy:I = 0x2e

.field static final TRANSACTION_isActivePasswordSufficient:I = 0x16

.field static final TRANSACTION_isAdminActive:I = 0x29

.field static final TRANSACTION_lockNow:I = 0x1d

.field static final TRANSACTION_packageHasActiveAdmins:I = 0x2b

.field static final TRANSACTION_removeActiveAdmin:I = 0x2d

.field static final TRANSACTION_reportFailedPasswordAttempt:I = 0x30

.field static final TRANSACTION_reportSuccessfulPasswordAttempt:I = 0x31

.field static final TRANSACTION_resetPassword:I = 0x1a

.field static final TRANSACTION_setActiveAdmin:I = 0x28

.field static final TRANSACTION_setActivePasswordState:I = 0x2f

.field static final TRANSACTION_setCameraDisabled:I = 0x24

.field static final TRANSACTION_setGlobalProxy:I = 0x1f

.field static final TRANSACTION_setKeyguardDisabledFeatures:I = 0x26

.field static final TRANSACTION_setMaximumFailedPasswordsForWipe:I = 0x18

.field static final TRANSACTION_setMaximumTimeToLock:I = 0x1b

.field static final TRANSACTION_setPasswordExpirationTimeout:I = 0x13

.field static final TRANSACTION_setPasswordHistoryLength:I = 0x11

.field static final TRANSACTION_setPasswordMinimumLength:I = 0x3

.field static final TRANSACTION_setPasswordMinimumLetters:I = 0x9

.field static final TRANSACTION_setPasswordMinimumLowerCase:I = 0x7

.field static final TRANSACTION_setPasswordMinimumNonLetter:I = 0xf

.field static final TRANSACTION_setPasswordMinimumNumeric:I = 0xb

.field static final TRANSACTION_setPasswordMinimumSymbols:I = 0xd

.field static final TRANSACTION_setPasswordMinimumUpperCase:I = 0x5

.field static final TRANSACTION_setPasswordQuality:I = 0x1

.field static final TRANSACTION_setStorageEncryption:I = 0x21

.field static final TRANSACTION_wipeData:I = 0x1e


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.app.admin.IDevicePolicyManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/admin/IDevicePolicyManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/admin/IDevicePolicyManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/app/admin/IDevicePolicyManager;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/app/admin/IDevicePolicyManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/admin/IDevicePolicyManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 21
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_878

    #@3
    .line 866
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 46
    :sswitch_8
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 47
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 51
    :sswitch_11
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_3a

    #@1e
    .line 54
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    move-object/from16 v0, p2

    #@22
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/content/ComponentName;

    #@28
    .line 60
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v3

    #@2c
    .line 62
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v4

    #@30
    .line 63
    .local v4, _arg2:I
    move-object/from16 v0, p0

    #@32
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordQuality(Landroid/content/ComponentName;II)V

    #@35
    .line 64
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    .line 65
    const/4 v1, 0x1

    #@39
    goto :goto_7

    #@3a
    .line 57
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_3a
    const/4 v2, 0x0

    #@3b
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_28

    #@3c
    .line 69
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_3c
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@3e
    move-object/from16 v0, p2

    #@40
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43
    .line 71
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_67

    #@49
    .line 72
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4b
    move-object/from16 v0, p2

    #@4d
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@50
    move-result-object v2

    #@51
    check-cast v2, Landroid/content/ComponentName;

    #@53
    .line 78
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v3

    #@57
    .line 79
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@59
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordQuality(Landroid/content/ComponentName;I)I

    #@5c
    move-result v13

    #@5d
    .line 80
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@60
    .line 81
    move-object/from16 v0, p3

    #@62
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    .line 82
    const/4 v1, 0x1

    #@66
    goto :goto_7

    #@67
    .line 75
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_67
    const/4 v2, 0x0

    #@68
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_53

    #@69
    .line 86
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_69
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@6b
    move-object/from16 v0, p2

    #@6d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 88
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v1

    #@74
    if-eqz v1, :cond_93

    #@76
    .line 89
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@78
    move-object/from16 v0, p2

    #@7a
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7d
    move-result-object v2

    #@7e
    check-cast v2, Landroid/content/ComponentName;

    #@80
    .line 95
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_80
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v3

    #@84
    .line 97
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@87
    move-result v4

    #@88
    .line 98
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@8a
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumLength(Landroid/content/ComponentName;II)V

    #@8d
    .line 99
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    .line 100
    const/4 v1, 0x1

    #@91
    goto/16 :goto_7

    #@93
    .line 92
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_93
    const/4 v2, 0x0

    #@94
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_80

    #@95
    .line 104
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_95
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@97
    move-object/from16 v0, p2

    #@99
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9c
    .line 106
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9f
    move-result v1

    #@a0
    if-eqz v1, :cond_c1

    #@a2
    .line 107
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a4
    move-object/from16 v0, p2

    #@a6
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a9
    move-result-object v2

    #@aa
    check-cast v2, Landroid/content/ComponentName;

    #@ac
    .line 113
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_ac
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@af
    move-result v3

    #@b0
    .line 114
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@b2
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    #@b5
    move-result v13

    #@b6
    .line 115
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b9
    .line 116
    move-object/from16 v0, p3

    #@bb
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@be
    .line 117
    const/4 v1, 0x1

    #@bf
    goto/16 :goto_7

    #@c1
    .line 110
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_c1
    const/4 v2, 0x0

    #@c2
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_ac

    #@c3
    .line 121
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_c3
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@c5
    move-object/from16 v0, p2

    #@c7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ca
    .line 123
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@cd
    move-result v1

    #@ce
    if-eqz v1, :cond_ed

    #@d0
    .line 124
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d2
    move-object/from16 v0, p2

    #@d4
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d7
    move-result-object v2

    #@d8
    check-cast v2, Landroid/content/ComponentName;

    #@da
    .line 130
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_da
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@dd
    move-result v3

    #@de
    .line 132
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e1
    move-result v4

    #@e2
    .line 133
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@e4
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumUpperCase(Landroid/content/ComponentName;II)V

    #@e7
    .line 134
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ea
    .line 135
    const/4 v1, 0x1

    #@eb
    goto/16 :goto_7

    #@ed
    .line 127
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_ed
    const/4 v2, 0x0

    #@ee
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_da

    #@ef
    .line 139
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_ef
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@f1
    move-object/from16 v0, p2

    #@f3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f6
    .line 141
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f9
    move-result v1

    #@fa
    if-eqz v1, :cond_11b

    #@fc
    .line 142
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@fe
    move-object/from16 v0, p2

    #@100
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@103
    move-result-object v2

    #@104
    check-cast v2, Landroid/content/ComponentName;

    #@106
    .line 148
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_106
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@109
    move-result v3

    #@10a
    .line 149
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@10c
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I

    #@10f
    move-result v13

    #@110
    .line 150
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@113
    .line 151
    move-object/from16 v0, p3

    #@115
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@118
    .line 152
    const/4 v1, 0x1

    #@119
    goto/16 :goto_7

    #@11b
    .line 145
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_11b
    const/4 v2, 0x0

    #@11c
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_106

    #@11d
    .line 156
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_11d
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@11f
    move-object/from16 v0, p2

    #@121
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@124
    .line 158
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@127
    move-result v1

    #@128
    if-eqz v1, :cond_147

    #@12a
    .line 159
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12c
    move-object/from16 v0, p2

    #@12e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@131
    move-result-object v2

    #@132
    check-cast v2, Landroid/content/ComponentName;

    #@134
    .line 165
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_134
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@137
    move-result v3

    #@138
    .line 167
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@13b
    move-result v4

    #@13c
    .line 168
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@13e
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumLowerCase(Landroid/content/ComponentName;II)V

    #@141
    .line 169
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@144
    .line 170
    const/4 v1, 0x1

    #@145
    goto/16 :goto_7

    #@147
    .line 162
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_147
    const/4 v2, 0x0

    #@148
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_134

    #@149
    .line 174
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_149
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@14b
    move-object/from16 v0, p2

    #@14d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@150
    .line 176
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@153
    move-result v1

    #@154
    if-eqz v1, :cond_175

    #@156
    .line 177
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@158
    move-object/from16 v0, p2

    #@15a
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@15d
    move-result-object v2

    #@15e
    check-cast v2, Landroid/content/ComponentName;

    #@160
    .line 183
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_160
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@163
    move-result v3

    #@164
    .line 184
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@166
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I

    #@169
    move-result v13

    #@16a
    .line 185
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@16d
    .line 186
    move-object/from16 v0, p3

    #@16f
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@172
    .line 187
    const/4 v1, 0x1

    #@173
    goto/16 :goto_7

    #@175
    .line 180
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_175
    const/4 v2, 0x0

    #@176
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_160

    #@177
    .line 191
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_177
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@179
    move-object/from16 v0, p2

    #@17b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17e
    .line 193
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@181
    move-result v1

    #@182
    if-eqz v1, :cond_1a1

    #@184
    .line 194
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@186
    move-object/from16 v0, p2

    #@188
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@18b
    move-result-object v2

    #@18c
    check-cast v2, Landroid/content/ComponentName;

    #@18e
    .line 200
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_18e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@191
    move-result v3

    #@192
    .line 202
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@195
    move-result v4

    #@196
    .line 203
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@198
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumLetters(Landroid/content/ComponentName;II)V

    #@19b
    .line 204
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@19e
    .line 205
    const/4 v1, 0x1

    #@19f
    goto/16 :goto_7

    #@1a1
    .line 197
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_1a1
    const/4 v2, 0x0

    #@1a2
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_18e

    #@1a3
    .line 209
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_1a3
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@1a5
    move-object/from16 v0, p2

    #@1a7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1aa
    .line 211
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ad
    move-result v1

    #@1ae
    if-eqz v1, :cond_1cf

    #@1b0
    .line 212
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1b2
    move-object/from16 v0, p2

    #@1b4
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1b7
    move-result-object v2

    #@1b8
    check-cast v2, Landroid/content/ComponentName;

    #@1ba
    .line 218
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_1ba
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1bd
    move-result v3

    #@1be
    .line 219
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@1c0
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumLetters(Landroid/content/ComponentName;I)I

    #@1c3
    move-result v13

    #@1c4
    .line 220
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c7
    .line 221
    move-object/from16 v0, p3

    #@1c9
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@1cc
    .line 222
    const/4 v1, 0x1

    #@1cd
    goto/16 :goto_7

    #@1cf
    .line 215
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_1cf
    const/4 v2, 0x0

    #@1d0
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_1ba

    #@1d1
    .line 226
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_1d1
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@1d3
    move-object/from16 v0, p2

    #@1d5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d8
    .line 228
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1db
    move-result v1

    #@1dc
    if-eqz v1, :cond_1fb

    #@1de
    .line 229
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e0
    move-object/from16 v0, p2

    #@1e2
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1e5
    move-result-object v2

    #@1e6
    check-cast v2, Landroid/content/ComponentName;

    #@1e8
    .line 235
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_1e8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1eb
    move-result v3

    #@1ec
    .line 237
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ef
    move-result v4

    #@1f0
    .line 238
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@1f2
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumNumeric(Landroid/content/ComponentName;II)V

    #@1f5
    .line 239
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f8
    .line 240
    const/4 v1, 0x1

    #@1f9
    goto/16 :goto_7

    #@1fb
    .line 232
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_1fb
    const/4 v2, 0x0

    #@1fc
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_1e8

    #@1fd
    .line 244
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_1fd
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@1ff
    move-object/from16 v0, p2

    #@201
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@204
    .line 246
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@207
    move-result v1

    #@208
    if-eqz v1, :cond_229

    #@20a
    .line 247
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20c
    move-object/from16 v0, p2

    #@20e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@211
    move-result-object v2

    #@212
    check-cast v2, Landroid/content/ComponentName;

    #@214
    .line 253
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_214
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@217
    move-result v3

    #@218
    .line 254
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@21a
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I

    #@21d
    move-result v13

    #@21e
    .line 255
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@221
    .line 256
    move-object/from16 v0, p3

    #@223
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@226
    .line 257
    const/4 v1, 0x1

    #@227
    goto/16 :goto_7

    #@229
    .line 250
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_229
    const/4 v2, 0x0

    #@22a
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_214

    #@22b
    .line 261
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_22b
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@22d
    move-object/from16 v0, p2

    #@22f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@232
    .line 263
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@235
    move-result v1

    #@236
    if-eqz v1, :cond_255

    #@238
    .line 264
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@23a
    move-object/from16 v0, p2

    #@23c
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23f
    move-result-object v2

    #@240
    check-cast v2, Landroid/content/ComponentName;

    #@242
    .line 270
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_242
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@245
    move-result v3

    #@246
    .line 272
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@249
    move-result v4

    #@24a
    .line 273
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@24c
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumSymbols(Landroid/content/ComponentName;II)V

    #@24f
    .line 274
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@252
    .line 275
    const/4 v1, 0x1

    #@253
    goto/16 :goto_7

    #@255
    .line 267
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_255
    const/4 v2, 0x0

    #@256
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_242

    #@257
    .line 279
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_257
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@259
    move-object/from16 v0, p2

    #@25b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25e
    .line 281
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@261
    move-result v1

    #@262
    if-eqz v1, :cond_283

    #@264
    .line 282
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@266
    move-object/from16 v0, p2

    #@268
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@26b
    move-result-object v2

    #@26c
    check-cast v2, Landroid/content/ComponentName;

    #@26e
    .line 288
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_26e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@271
    move-result v3

    #@272
    .line 289
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@274
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I

    #@277
    move-result v13

    #@278
    .line 290
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@27b
    .line 291
    move-object/from16 v0, p3

    #@27d
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@280
    .line 292
    const/4 v1, 0x1

    #@281
    goto/16 :goto_7

    #@283
    .line 285
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_283
    const/4 v2, 0x0

    #@284
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_26e

    #@285
    .line 296
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_285
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@287
    move-object/from16 v0, p2

    #@289
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28c
    .line 298
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@28f
    move-result v1

    #@290
    if-eqz v1, :cond_2af

    #@292
    .line 299
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@294
    move-object/from16 v0, p2

    #@296
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@299
    move-result-object v2

    #@29a
    check-cast v2, Landroid/content/ComponentName;

    #@29c
    .line 305
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_29c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@29f
    move-result v3

    #@2a0
    .line 307
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2a3
    move-result v4

    #@2a4
    .line 308
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@2a6
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordMinimumNonLetter(Landroid/content/ComponentName;II)V

    #@2a9
    .line 309
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ac
    .line 310
    const/4 v1, 0x1

    #@2ad
    goto/16 :goto_7

    #@2af
    .line 302
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_2af
    const/4 v2, 0x0

    #@2b0
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_29c

    #@2b1
    .line 314
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_2b1
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@2b3
    move-object/from16 v0, p2

    #@2b5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b8
    .line 316
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2bb
    move-result v1

    #@2bc
    if-eqz v1, :cond_2dd

    #@2be
    .line 317
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c0
    move-object/from16 v0, p2

    #@2c2
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c5
    move-result-object v2

    #@2c6
    check-cast v2, Landroid/content/ComponentName;

    #@2c8
    .line 323
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_2c8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2cb
    move-result v3

    #@2cc
    .line 324
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@2ce
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I

    #@2d1
    move-result v13

    #@2d2
    .line 325
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d5
    .line 326
    move-object/from16 v0, p3

    #@2d7
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@2da
    .line 327
    const/4 v1, 0x1

    #@2db
    goto/16 :goto_7

    #@2dd
    .line 320
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_2dd
    const/4 v2, 0x0

    #@2de
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_2c8

    #@2df
    .line 331
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_2df
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@2e1
    move-object/from16 v0, p2

    #@2e3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e6
    .line 333
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2e9
    move-result v1

    #@2ea
    if-eqz v1, :cond_309

    #@2ec
    .line 334
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2ee
    move-object/from16 v0, p2

    #@2f0
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2f3
    move-result-object v2

    #@2f4
    check-cast v2, Landroid/content/ComponentName;

    #@2f6
    .line 340
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_2f6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2f9
    move-result v3

    #@2fa
    .line 342
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2fd
    move-result v4

    #@2fe
    .line 343
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@300
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordHistoryLength(Landroid/content/ComponentName;II)V

    #@303
    .line 344
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@306
    .line 345
    const/4 v1, 0x1

    #@307
    goto/16 :goto_7

    #@309
    .line 337
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_309
    const/4 v2, 0x0

    #@30a
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_2f6

    #@30b
    .line 349
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_30b
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@30d
    move-object/from16 v0, p2

    #@30f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@312
    .line 351
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@315
    move-result v1

    #@316
    if-eqz v1, :cond_337

    #@318
    .line 352
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@31a
    move-object/from16 v0, p2

    #@31c
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31f
    move-result-object v2

    #@320
    check-cast v2, Landroid/content/ComponentName;

    #@322
    .line 358
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_322
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@325
    move-result v3

    #@326
    .line 359
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@328
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordHistoryLength(Landroid/content/ComponentName;I)I

    #@32b
    move-result v13

    #@32c
    .line 360
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@32f
    .line 361
    move-object/from16 v0, p3

    #@331
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@334
    .line 362
    const/4 v1, 0x1

    #@335
    goto/16 :goto_7

    #@337
    .line 355
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_337
    const/4 v2, 0x0

    #@338
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_322

    #@339
    .line 366
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_339
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@33b
    move-object/from16 v0, p2

    #@33d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@340
    .line 368
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@343
    move-result v1

    #@344
    if-eqz v1, :cond_363

    #@346
    .line 369
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@348
    move-object/from16 v0, p2

    #@34a
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@34d
    move-result-object v2

    #@34e
    check-cast v2, Landroid/content/ComponentName;

    #@350
    .line 375
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_350
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@353
    move-result-wide v11

    #@354
    .line 377
    .local v11, _arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@357
    move-result v4

    #@358
    .line 378
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@35a
    invoke-virtual {v0, v2, v11, v12, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setPasswordExpirationTimeout(Landroid/content/ComponentName;JI)V

    #@35d
    .line 379
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@360
    .line 380
    const/4 v1, 0x1

    #@361
    goto/16 :goto_7

    #@363
    .line 372
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v4           #_arg2:I
    .end local v11           #_arg1:J
    :cond_363
    const/4 v2, 0x0

    #@364
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_350

    #@365
    .line 384
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_365
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@367
    move-object/from16 v0, p2

    #@369
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36c
    .line 386
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@36f
    move-result v1

    #@370
    if-eqz v1, :cond_391

    #@372
    .line 387
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@374
    move-object/from16 v0, p2

    #@376
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@379
    move-result-object v2

    #@37a
    check-cast v2, Landroid/content/ComponentName;

    #@37c
    .line 393
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_37c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@37f
    move-result v3

    #@380
    .line 394
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@382
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordExpirationTimeout(Landroid/content/ComponentName;I)J

    #@385
    move-result-wide v13

    #@386
    .line 395
    .local v13, _result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@389
    .line 396
    move-object/from16 v0, p3

    #@38b
    invoke-virtual {v0, v13, v14}, Landroid/os/Parcel;->writeLong(J)V

    #@38e
    .line 397
    const/4 v1, 0x1

    #@38f
    goto/16 :goto_7

    #@391
    .line 390
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:J
    :cond_391
    const/4 v2, 0x0

    #@392
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_37c

    #@393
    .line 401
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_393
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@395
    move-object/from16 v0, p2

    #@397
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@39a
    .line 403
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@39d
    move-result v1

    #@39e
    if-eqz v1, :cond_3bf

    #@3a0
    .line 404
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3a2
    move-object/from16 v0, p2

    #@3a4
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3a7
    move-result-object v2

    #@3a8
    check-cast v2, Landroid/content/ComponentName;

    #@3aa
    .line 410
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_3aa
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ad
    move-result v3

    #@3ae
    .line 411
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@3b0
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getPasswordExpiration(Landroid/content/ComponentName;I)J

    #@3b3
    move-result-wide v13

    #@3b4
    .line 412
    .restart local v13       #_result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b7
    .line 413
    move-object/from16 v0, p3

    #@3b9
    invoke-virtual {v0, v13, v14}, Landroid/os/Parcel;->writeLong(J)V

    #@3bc
    .line 414
    const/4 v1, 0x1

    #@3bd
    goto/16 :goto_7

    #@3bf
    .line 407
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:J
    :cond_3bf
    const/4 v2, 0x0

    #@3c0
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_3aa

    #@3c1
    .line 418
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_3c1
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@3c3
    move-object/from16 v0, p2

    #@3c5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c8
    .line 420
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3cb
    move-result v2

    #@3cc
    .line 421
    .local v2, _arg0:I
    move-object/from16 v0, p0

    #@3ce
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->isActivePasswordSufficient(I)Z

    #@3d1
    move-result v13

    #@3d2
    .line 422
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d5
    .line 423
    if-eqz v13, :cond_3e0

    #@3d7
    const/4 v1, 0x1

    #@3d8
    :goto_3d8
    move-object/from16 v0, p3

    #@3da
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3dd
    .line 424
    const/4 v1, 0x1

    #@3de
    goto/16 :goto_7

    #@3e0
    .line 423
    :cond_3e0
    const/4 v1, 0x0

    #@3e1
    goto :goto_3d8

    #@3e2
    .line 428
    .end local v2           #_arg0:I
    .end local v13           #_result:Z
    :sswitch_3e2
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@3e4
    move-object/from16 v0, p2

    #@3e6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e9
    .line 430
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ec
    move-result v2

    #@3ed
    .line 431
    .restart local v2       #_arg0:I
    move-object/from16 v0, p0

    #@3ef
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->getCurrentFailedPasswordAttempts(I)I

    #@3f2
    move-result v13

    #@3f3
    .line 432
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f6
    .line 433
    move-object/from16 v0, p3

    #@3f8
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@3fb
    .line 434
    const/4 v1, 0x1

    #@3fc
    goto/16 :goto_7

    #@3fe
    .line 438
    .end local v2           #_arg0:I
    .end local v13           #_result:I
    :sswitch_3fe
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@400
    move-object/from16 v0, p2

    #@402
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@405
    .line 440
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@408
    move-result v1

    #@409
    if-eqz v1, :cond_428

    #@40b
    .line 441
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40d
    move-object/from16 v0, p2

    #@40f
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@412
    move-result-object v2

    #@413
    check-cast v2, Landroid/content/ComponentName;

    #@415
    .line 447
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_415
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@418
    move-result v3

    #@419
    .line 449
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@41c
    move-result v4

    #@41d
    .line 450
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@41f
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;II)V

    #@422
    .line 451
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@425
    .line 452
    const/4 v1, 0x1

    #@426
    goto/16 :goto_7

    #@428
    .line 444
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_428
    const/4 v2, 0x0

    #@429
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_415

    #@42a
    .line 456
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_42a
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@42c
    move-object/from16 v0, p2

    #@42e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@431
    .line 458
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@434
    move-result v1

    #@435
    if-eqz v1, :cond_456

    #@437
    .line 459
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@439
    move-object/from16 v0, p2

    #@43b
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43e
    move-result-object v2

    #@43f
    check-cast v2, Landroid/content/ComponentName;

    #@441
    .line 465
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_441
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@444
    move-result v3

    #@445
    .line 466
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@447
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I

    #@44a
    move-result v13

    #@44b
    .line 467
    .restart local v13       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@44e
    .line 468
    move-object/from16 v0, p3

    #@450
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@453
    .line 469
    const/4 v1, 0x1

    #@454
    goto/16 :goto_7

    #@456
    .line 462
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_456
    const/4 v2, 0x0

    #@457
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_441

    #@458
    .line 473
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_458
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@45a
    move-object/from16 v0, p2

    #@45c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45f
    .line 475
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@462
    move-result-object v2

    #@463
    .line 477
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@466
    move-result v3

    #@467
    .line 479
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@46a
    move-result v4

    #@46b
    .line 480
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@46d
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->resetPassword(Ljava/lang/String;II)Z

    #@470
    move-result v13

    #@471
    .line 481
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@474
    .line 482
    if-eqz v13, :cond_47f

    #@476
    const/4 v1, 0x1

    #@477
    :goto_477
    move-object/from16 v0, p3

    #@479
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@47c
    .line 483
    const/4 v1, 0x1

    #@47d
    goto/16 :goto_7

    #@47f
    .line 482
    :cond_47f
    const/4 v1, 0x0

    #@480
    goto :goto_477

    #@481
    .line 487
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v13           #_result:Z
    :sswitch_481
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@483
    move-object/from16 v0, p2

    #@485
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@488
    .line 489
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@48b
    move-result v1

    #@48c
    if-eqz v1, :cond_4ab

    #@48e
    .line 490
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@490
    move-object/from16 v0, p2

    #@492
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@495
    move-result-object v2

    #@496
    check-cast v2, Landroid/content/ComponentName;

    #@498
    .line 496
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_498
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@49b
    move-result-wide v11

    #@49c
    .line 498
    .restart local v11       #_arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@49f
    move-result v4

    #@4a0
    .line 499
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@4a2
    invoke-virtual {v0, v2, v11, v12, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setMaximumTimeToLock(Landroid/content/ComponentName;JI)V

    #@4a5
    .line 500
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a8
    .line 501
    const/4 v1, 0x1

    #@4a9
    goto/16 :goto_7

    #@4ab
    .line 493
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v4           #_arg2:I
    .end local v11           #_arg1:J
    :cond_4ab
    const/4 v2, 0x0

    #@4ac
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_498

    #@4ad
    .line 505
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_4ad
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@4af
    move-object/from16 v0, p2

    #@4b1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b4
    .line 507
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4b7
    move-result v1

    #@4b8
    if-eqz v1, :cond_4d9

    #@4ba
    .line 508
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4bc
    move-object/from16 v0, p2

    #@4be
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4c1
    move-result-object v2

    #@4c2
    check-cast v2, Landroid/content/ComponentName;

    #@4c4
    .line 514
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_4c4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4c7
    move-result v3

    #@4c8
    .line 515
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@4ca
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getMaximumTimeToLock(Landroid/content/ComponentName;I)J

    #@4cd
    move-result-wide v13

    #@4ce
    .line 516
    .local v13, _result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d1
    .line 517
    move-object/from16 v0, p3

    #@4d3
    invoke-virtual {v0, v13, v14}, Landroid/os/Parcel;->writeLong(J)V

    #@4d6
    .line 518
    const/4 v1, 0x1

    #@4d7
    goto/16 :goto_7

    #@4d9
    .line 511
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:J
    :cond_4d9
    const/4 v2, 0x0

    #@4da
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_4c4

    #@4db
    .line 522
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_4db
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@4dd
    move-object/from16 v0, p2

    #@4df
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e2
    .line 523
    invoke-virtual/range {p0 .. p0}, Landroid/app/admin/IDevicePolicyManager$Stub;->lockNow()V

    #@4e5
    .line 524
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e8
    .line 525
    const/4 v1, 0x1

    #@4e9
    goto/16 :goto_7

    #@4eb
    .line 529
    :sswitch_4eb
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@4ed
    move-object/from16 v0, p2

    #@4ef
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f2
    .line 531
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4f5
    move-result v2

    #@4f6
    .line 533
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4f9
    move-result v3

    #@4fa
    .line 534
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@4fc
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->wipeData(II)V

    #@4ff
    .line 535
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@502
    .line 536
    const/4 v1, 0x1

    #@503
    goto/16 :goto_7

    #@505
    .line 540
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    :sswitch_505
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@507
    move-object/from16 v0, p2

    #@509
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50c
    .line 542
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@50f
    move-result v1

    #@510
    if-eqz v1, :cond_542

    #@512
    .line 543
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@514
    move-object/from16 v0, p2

    #@516
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@519
    move-result-object v2

    #@51a
    check-cast v2, Landroid/content/ComponentName;

    #@51c
    .line 549
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_51c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@51f
    move-result-object v3

    #@520
    .line 551
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@523
    move-result-object v4

    #@524
    .line 553
    .local v4, _arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@527
    move-result v5

    #@528
    .line 554
    .local v5, _arg3:I
    move-object/from16 v0, p0

    #@52a
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/app/admin/IDevicePolicyManager$Stub;->setGlobalProxy(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/ComponentName;

    #@52d
    move-result-object v13

    #@52e
    .line 555
    .local v13, _result:Landroid/content/ComponentName;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@531
    .line 556
    if-eqz v13, :cond_544

    #@533
    .line 557
    const/4 v1, 0x1

    #@534
    move-object/from16 v0, p3

    #@536
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@539
    .line 558
    const/4 v1, 0x1

    #@53a
    move-object/from16 v0, p3

    #@53c
    invoke-virtual {v13, v0, v1}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@53f
    .line 563
    :goto_53f
    const/4 v1, 0x1

    #@540
    goto/16 :goto_7

    #@542
    .line 546
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v5           #_arg3:I
    .end local v13           #_result:Landroid/content/ComponentName;
    :cond_542
    const/4 v2, 0x0

    #@543
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_51c

    #@544
    .line 561
    .restart local v3       #_arg1:Ljava/lang/String;
    .restart local v4       #_arg2:Ljava/lang/String;
    .restart local v5       #_arg3:I
    .restart local v13       #_result:Landroid/content/ComponentName;
    :cond_544
    const/4 v1, 0x0

    #@545
    move-object/from16 v0, p3

    #@547
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@54a
    goto :goto_53f

    #@54b
    .line 567
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v5           #_arg3:I
    .end local v13           #_result:Landroid/content/ComponentName;
    :sswitch_54b
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@54d
    move-object/from16 v0, p2

    #@54f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@552
    .line 569
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@555
    move-result v2

    #@556
    .line 570
    .local v2, _arg0:I
    move-object/from16 v0, p0

    #@558
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->getGlobalProxyAdmin(I)Landroid/content/ComponentName;

    #@55b
    move-result-object v13

    #@55c
    .line 571
    .restart local v13       #_result:Landroid/content/ComponentName;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@55f
    .line 572
    if-eqz v13, :cond_570

    #@561
    .line 573
    const/4 v1, 0x1

    #@562
    move-object/from16 v0, p3

    #@564
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@567
    .line 574
    const/4 v1, 0x1

    #@568
    move-object/from16 v0, p3

    #@56a
    invoke-virtual {v13, v0, v1}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@56d
    .line 579
    :goto_56d
    const/4 v1, 0x1

    #@56e
    goto/16 :goto_7

    #@570
    .line 577
    :cond_570
    const/4 v1, 0x0

    #@571
    move-object/from16 v0, p3

    #@573
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@576
    goto :goto_56d

    #@577
    .line 583
    .end local v2           #_arg0:I
    .end local v13           #_result:Landroid/content/ComponentName;
    :sswitch_577
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@579
    move-object/from16 v0, p2

    #@57b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@57e
    .line 585
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@581
    move-result v1

    #@582
    if-eqz v1, :cond_5aa

    #@584
    .line 586
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@586
    move-object/from16 v0, p2

    #@588
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@58b
    move-result-object v2

    #@58c
    check-cast v2, Landroid/content/ComponentName;

    #@58e
    .line 592
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_58e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@591
    move-result v1

    #@592
    if-eqz v1, :cond_5ac

    #@594
    const/4 v3, 0x1

    #@595
    .line 594
    .local v3, _arg1:Z
    :goto_595
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@598
    move-result v4

    #@599
    .line 595
    .local v4, _arg2:I
    move-object/from16 v0, p0

    #@59b
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setStorageEncryption(Landroid/content/ComponentName;ZI)I

    #@59e
    move-result v13

    #@59f
    .line 596
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a2
    .line 597
    move-object/from16 v0, p3

    #@5a4
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@5a7
    .line 598
    const/4 v1, 0x1

    #@5a8
    goto/16 :goto_7

    #@5aa
    .line 589
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Z
    .end local v4           #_arg2:I
    .end local v13           #_result:I
    :cond_5aa
    const/4 v2, 0x0

    #@5ab
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_58e

    #@5ac
    .line 592
    :cond_5ac
    const/4 v3, 0x0

    #@5ad
    goto :goto_595

    #@5ae
    .line 602
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_5ae
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@5b0
    move-object/from16 v0, p2

    #@5b2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b5
    .line 604
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5b8
    move-result v1

    #@5b9
    if-eqz v1, :cond_5dd

    #@5bb
    .line 605
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5bd
    move-object/from16 v0, p2

    #@5bf
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5c2
    move-result-object v2

    #@5c3
    check-cast v2, Landroid/content/ComponentName;

    #@5c5
    .line 611
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_5c5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5c8
    move-result v3

    #@5c9
    .line 612
    .local v3, _arg1:I
    move-object/from16 v0, p0

    #@5cb
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getStorageEncryption(Landroid/content/ComponentName;I)Z

    #@5ce
    move-result v13

    #@5cf
    .line 613
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d2
    .line 614
    if-eqz v13, :cond_5df

    #@5d4
    const/4 v1, 0x1

    #@5d5
    :goto_5d5
    move-object/from16 v0, p3

    #@5d7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5da
    .line 615
    const/4 v1, 0x1

    #@5db
    goto/16 :goto_7

    #@5dd
    .line 608
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :cond_5dd
    const/4 v2, 0x0

    #@5de
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_5c5

    #@5df
    .line 614
    .restart local v3       #_arg1:I
    .restart local v13       #_result:Z
    :cond_5df
    const/4 v1, 0x0

    #@5e0
    goto :goto_5d5

    #@5e1
    .line 619
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :sswitch_5e1
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@5e3
    move-object/from16 v0, p2

    #@5e5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5e8
    .line 621
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5eb
    move-result v2

    #@5ec
    .line 622
    .local v2, _arg0:I
    move-object/from16 v0, p0

    #@5ee
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->getStorageEncryptionStatus(I)I

    #@5f1
    move-result v13

    #@5f2
    .line 623
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5f5
    .line 624
    move-object/from16 v0, p3

    #@5f7
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@5fa
    .line 625
    const/4 v1, 0x1

    #@5fb
    goto/16 :goto_7

    #@5fd
    .line 629
    .end local v2           #_arg0:I
    .end local v13           #_result:I
    :sswitch_5fd
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@5ff
    move-object/from16 v0, p2

    #@601
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@604
    .line 631
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@607
    move-result v1

    #@608
    if-eqz v1, :cond_62a

    #@60a
    .line 632
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@60c
    move-object/from16 v0, p2

    #@60e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@611
    move-result-object v2

    #@612
    check-cast v2, Landroid/content/ComponentName;

    #@614
    .line 638
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_614
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@617
    move-result v1

    #@618
    if-eqz v1, :cond_62c

    #@61a
    const/4 v3, 0x1

    #@61b
    .line 640
    .local v3, _arg1:Z
    :goto_61b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@61e
    move-result v4

    #@61f
    .line 641
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@621
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setCameraDisabled(Landroid/content/ComponentName;ZI)V

    #@624
    .line 642
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@627
    .line 643
    const/4 v1, 0x1

    #@628
    goto/16 :goto_7

    #@62a
    .line 635
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Z
    .end local v4           #_arg2:I
    :cond_62a
    const/4 v2, 0x0

    #@62b
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_614

    #@62c
    .line 638
    :cond_62c
    const/4 v3, 0x0

    #@62d
    goto :goto_61b

    #@62e
    .line 647
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_62e
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@630
    move-object/from16 v0, p2

    #@632
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@635
    .line 649
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@638
    move-result v1

    #@639
    if-eqz v1, :cond_65d

    #@63b
    .line 650
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@63d
    move-object/from16 v0, p2

    #@63f
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@642
    move-result-object v2

    #@643
    check-cast v2, Landroid/content/ComponentName;

    #@645
    .line 656
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_645
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@648
    move-result v3

    #@649
    .line 657
    .local v3, _arg1:I
    move-object/from16 v0, p0

    #@64b
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getCameraDisabled(Landroid/content/ComponentName;I)Z

    #@64e
    move-result v13

    #@64f
    .line 658
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@652
    .line 659
    if-eqz v13, :cond_65f

    #@654
    const/4 v1, 0x1

    #@655
    :goto_655
    move-object/from16 v0, p3

    #@657
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@65a
    .line 660
    const/4 v1, 0x1

    #@65b
    goto/16 :goto_7

    #@65d
    .line 653
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :cond_65d
    const/4 v2, 0x0

    #@65e
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_645

    #@65f
    .line 659
    .restart local v3       #_arg1:I
    .restart local v13       #_result:Z
    :cond_65f
    const/4 v1, 0x0

    #@660
    goto :goto_655

    #@661
    .line 664
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :sswitch_661
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@663
    move-object/from16 v0, p2

    #@665
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@668
    .line 666
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@66b
    move-result v1

    #@66c
    if-eqz v1, :cond_68b

    #@66e
    .line 667
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@670
    move-object/from16 v0, p2

    #@672
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@675
    move-result-object v2

    #@676
    check-cast v2, Landroid/content/ComponentName;

    #@678
    .line 673
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_678
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@67b
    move-result v3

    #@67c
    .line 675
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@67f
    move-result v4

    #@680
    .line 676
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@682
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setKeyguardDisabledFeatures(Landroid/content/ComponentName;II)V

    #@685
    .line 677
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@688
    .line 678
    const/4 v1, 0x1

    #@689
    goto/16 :goto_7

    #@68b
    .line 670
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :cond_68b
    const/4 v2, 0x0

    #@68c
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_678

    #@68d
    .line 682
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_68d
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@68f
    move-object/from16 v0, p2

    #@691
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@694
    .line 684
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@697
    move-result v1

    #@698
    if-eqz v1, :cond_6b9

    #@69a
    .line 685
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@69c
    move-object/from16 v0, p2

    #@69e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6a1
    move-result-object v2

    #@6a2
    check-cast v2, Landroid/content/ComponentName;

    #@6a4
    .line 691
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_6a4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6a7
    move-result v3

    #@6a8
    .line 692
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@6aa
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    #@6ad
    move-result v13

    #@6ae
    .line 693
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6b1
    .line 694
    move-object/from16 v0, p3

    #@6b3
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@6b6
    .line 695
    const/4 v1, 0x1

    #@6b7
    goto/16 :goto_7

    #@6b9
    .line 688
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:I
    :cond_6b9
    const/4 v2, 0x0

    #@6ba
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_6a4

    #@6bb
    .line 699
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_6bb
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@6bd
    move-object/from16 v0, p2

    #@6bf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c2
    .line 701
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6c5
    move-result v1

    #@6c6
    if-eqz v1, :cond_6e8

    #@6c8
    .line 702
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6ca
    move-object/from16 v0, p2

    #@6cc
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6cf
    move-result-object v2

    #@6d0
    check-cast v2, Landroid/content/ComponentName;

    #@6d2
    .line 708
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_6d2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6d5
    move-result v1

    #@6d6
    if-eqz v1, :cond_6ea

    #@6d8
    const/4 v3, 0x1

    #@6d9
    .line 710
    .local v3, _arg1:Z
    :goto_6d9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6dc
    move-result v4

    #@6dd
    .line 711
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@6df
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->setActiveAdmin(Landroid/content/ComponentName;ZI)V

    #@6e2
    .line 712
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e5
    .line 713
    const/4 v1, 0x1

    #@6e6
    goto/16 :goto_7

    #@6e8
    .line 705
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Z
    .end local v4           #_arg2:I
    :cond_6e8
    const/4 v2, 0x0

    #@6e9
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_6d2

    #@6ea
    .line 708
    :cond_6ea
    const/4 v3, 0x0

    #@6eb
    goto :goto_6d9

    #@6ec
    .line 717
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_6ec
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@6ee
    move-object/from16 v0, p2

    #@6f0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6f3
    .line 719
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6f6
    move-result v1

    #@6f7
    if-eqz v1, :cond_71b

    #@6f9
    .line 720
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6fb
    move-object/from16 v0, p2

    #@6fd
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@700
    move-result-object v2

    #@701
    check-cast v2, Landroid/content/ComponentName;

    #@703
    .line 726
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_703
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@706
    move-result v3

    #@707
    .line 727
    .local v3, _arg1:I
    move-object/from16 v0, p0

    #@709
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->isAdminActive(Landroid/content/ComponentName;I)Z

    #@70c
    move-result v13

    #@70d
    .line 728
    .local v13, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@710
    .line 729
    if-eqz v13, :cond_71d

    #@712
    const/4 v1, 0x1

    #@713
    :goto_713
    move-object/from16 v0, p3

    #@715
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@718
    .line 730
    const/4 v1, 0x1

    #@719
    goto/16 :goto_7

    #@71b
    .line 723
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :cond_71b
    const/4 v2, 0x0

    #@71c
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_703

    #@71d
    .line 729
    .restart local v3       #_arg1:I
    .restart local v13       #_result:Z
    :cond_71d
    const/4 v1, 0x0

    #@71e
    goto :goto_713

    #@71f
    .line 734
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :sswitch_71f
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@721
    move-object/from16 v0, p2

    #@723
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@726
    .line 736
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@729
    move-result v2

    #@72a
    .line 737
    .local v2, _arg0:I
    move-object/from16 v0, p0

    #@72c
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->getActiveAdmins(I)Ljava/util/List;

    #@72f
    move-result-object v15

    #@730
    .line 738
    .local v15, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@733
    .line 739
    move-object/from16 v0, p3

    #@735
    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@738
    .line 740
    const/4 v1, 0x1

    #@739
    goto/16 :goto_7

    #@73b
    .line 744
    .end local v2           #_arg0:I
    .end local v15           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/ComponentName;>;"
    :sswitch_73b
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@73d
    move-object/from16 v0, p2

    #@73f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@742
    .line 746
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@745
    move-result-object v2

    #@746
    .line 748
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@749
    move-result v3

    #@74a
    .line 749
    .restart local v3       #_arg1:I
    move-object/from16 v0, p0

    #@74c
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->packageHasActiveAdmins(Ljava/lang/String;I)Z

    #@74f
    move-result v13

    #@750
    .line 750
    .restart local v13       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@753
    .line 751
    if-eqz v13, :cond_75e

    #@755
    const/4 v1, 0x1

    #@756
    :goto_756
    move-object/from16 v0, p3

    #@758
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@75b
    .line 752
    const/4 v1, 0x1

    #@75c
    goto/16 :goto_7

    #@75e
    .line 751
    :cond_75e
    const/4 v1, 0x0

    #@75f
    goto :goto_756

    #@760
    .line 756
    .end local v2           #_arg0:Ljava/lang/String;
    .end local v3           #_arg1:I
    .end local v13           #_result:Z
    :sswitch_760
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@762
    move-object/from16 v0, p2

    #@764
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@767
    .line 758
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@76a
    move-result v1

    #@76b
    if-eqz v1, :cond_796

    #@76d
    .line 759
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@76f
    move-object/from16 v0, p2

    #@771
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@774
    move-result-object v2

    #@775
    check-cast v2, Landroid/content/ComponentName;

    #@777
    .line 765
    .local v2, _arg0:Landroid/content/ComponentName;
    :goto_777
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@77a
    move-result v1

    #@77b
    if-eqz v1, :cond_798

    #@77d
    .line 766
    sget-object v1, Landroid/os/RemoteCallback;->CREATOR:Landroid/os/Parcelable$Creator;

    #@77f
    move-object/from16 v0, p2

    #@781
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@784
    move-result-object v3

    #@785
    check-cast v3, Landroid/os/RemoteCallback;

    #@787
    .line 772
    .local v3, _arg1:Landroid/os/RemoteCallback;
    :goto_787
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@78a
    move-result v4

    #@78b
    .line 773
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@78d
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;I)V

    #@790
    .line 774
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@793
    .line 775
    const/4 v1, 0x1

    #@794
    goto/16 :goto_7

    #@796
    .line 762
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Landroid/os/RemoteCallback;
    .end local v4           #_arg2:I
    :cond_796
    const/4 v2, 0x0

    #@797
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_777

    #@798
    .line 769
    :cond_798
    const/4 v3, 0x0

    #@799
    .restart local v3       #_arg1:Landroid/os/RemoteCallback;
    goto :goto_787

    #@79a
    .line 779
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:Landroid/os/RemoteCallback;
    :sswitch_79a
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@79c
    move-object/from16 v0, p2

    #@79e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a1
    .line 781
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7a4
    move-result v1

    #@7a5
    if-eqz v1, :cond_7c0

    #@7a7
    .line 782
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7a9
    move-object/from16 v0, p2

    #@7ab
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7ae
    move-result-object v2

    #@7af
    check-cast v2, Landroid/content/ComponentName;

    #@7b1
    .line 788
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_7b1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7b4
    move-result v3

    #@7b5
    .line 789
    .local v3, _arg1:I
    move-object/from16 v0, p0

    #@7b7
    invoke-virtual {v0, v2, v3}, Landroid/app/admin/IDevicePolicyManager$Stub;->removeActiveAdmin(Landroid/content/ComponentName;I)V

    #@7ba
    .line 790
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7bd
    .line 791
    const/4 v1, 0x1

    #@7be
    goto/16 :goto_7

    #@7c0
    .line 785
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    :cond_7c0
    const/4 v2, 0x0

    #@7c1
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_7b1

    #@7c2
    .line 795
    .end local v2           #_arg0:Landroid/content/ComponentName;
    :sswitch_7c2
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@7c4
    move-object/from16 v0, p2

    #@7c6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c9
    .line 797
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7cc
    move-result v1

    #@7cd
    if-eqz v1, :cond_7f5

    #@7cf
    .line 798
    sget-object v1, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7d1
    move-object/from16 v0, p2

    #@7d3
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7d6
    move-result-object v2

    #@7d7
    check-cast v2, Landroid/content/ComponentName;

    #@7d9
    .line 804
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    :goto_7d9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7dc
    move-result v3

    #@7dd
    .line 806
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7e0
    move-result v4

    #@7e1
    .line 807
    .restart local v4       #_arg2:I
    move-object/from16 v0, p0

    #@7e3
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/admin/IDevicePolicyManager$Stub;->hasGrantedPolicy(Landroid/content/ComponentName;II)Z

    #@7e6
    move-result v13

    #@7e7
    .line 808
    .restart local v13       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7ea
    .line 809
    if-eqz v13, :cond_7f7

    #@7ec
    const/4 v1, 0x1

    #@7ed
    :goto_7ed
    move-object/from16 v0, p3

    #@7ef
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@7f2
    .line 810
    const/4 v1, 0x1

    #@7f3
    goto/16 :goto_7

    #@7f5
    .line 801
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v13           #_result:Z
    :cond_7f5
    const/4 v2, 0x0

    #@7f6
    .restart local v2       #_arg0:Landroid/content/ComponentName;
    goto :goto_7d9

    #@7f7
    .line 809
    .restart local v3       #_arg1:I
    .restart local v4       #_arg2:I
    .restart local v13       #_result:Z
    :cond_7f7
    const/4 v1, 0x0

    #@7f8
    goto :goto_7ed

    #@7f9
    .line 814
    .end local v2           #_arg0:Landroid/content/ComponentName;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v13           #_result:Z
    :sswitch_7f9
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@7fb
    move-object/from16 v0, p2

    #@7fd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@800
    .line 816
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@803
    move-result v2

    #@804
    .line 818
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@807
    move-result v3

    #@808
    .line 820
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@80b
    move-result v4

    #@80c
    .line 822
    .restart local v4       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@80f
    move-result v5

    #@810
    .line 824
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@813
    move-result v6

    #@814
    .line 826
    .local v6, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@817
    move-result v7

    #@818
    .line 828
    .local v7, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@81b
    move-result v8

    #@81c
    .line 830
    .local v8, _arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@81f
    move-result v9

    #@820
    .line 832
    .local v9, _arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@823
    move-result v10

    #@824
    .local v10, _arg8:I
    move-object/from16 v1, p0

    #@826
    .line 833
    invoke-virtual/range {v1 .. v10}, Landroid/app/admin/IDevicePolicyManager$Stub;->setActivePasswordState(IIIIIIIII)V

    #@829
    .line 834
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@82c
    .line 835
    const/4 v1, 0x1

    #@82d
    goto/16 :goto_7

    #@82f
    .line 839
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:I
    .end local v6           #_arg4:I
    .end local v7           #_arg5:I
    .end local v8           #_arg6:I
    .end local v9           #_arg7:I
    .end local v10           #_arg8:I
    :sswitch_82f
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@831
    move-object/from16 v0, p2

    #@833
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@836
    .line 841
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@839
    move-result v2

    #@83a
    .line 842
    .restart local v2       #_arg0:I
    move-object/from16 v0, p0

    #@83c
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->reportFailedPasswordAttempt(I)V

    #@83f
    .line 843
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@842
    .line 844
    const/4 v1, 0x1

    #@843
    goto/16 :goto_7

    #@845
    .line 848
    .end local v2           #_arg0:I
    :sswitch_845
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@847
    move-object/from16 v0, p2

    #@849
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84c
    .line 850
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@84f
    move-result v2

    #@850
    .line 851
    .restart local v2       #_arg0:I
    move-object/from16 v0, p0

    #@852
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->reportSuccessfulPasswordAttempt(I)V

    #@855
    .line 852
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@858
    .line 853
    const/4 v1, 0x1

    #@859
    goto/16 :goto_7

    #@85b
    .line 857
    .end local v2           #_arg0:I
    :sswitch_85b
    const-string v1, "android.app.admin.IDevicePolicyManager"

    #@85d
    move-object/from16 v0, p2

    #@85f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@862
    .line 859
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@865
    move-result v2

    #@866
    .line 860
    .restart local v2       #_arg0:I
    move-object/from16 v0, p0

    #@868
    invoke-virtual {v0, v2}, Landroid/app/admin/IDevicePolicyManager$Stub;->getCurrentFailedPasswordAttemptsMDM(I)I

    #@86b
    move-result v13

    #@86c
    .line 861
    .local v13, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@86f
    .line 862
    move-object/from16 v0, p3

    #@871
    invoke-virtual {v0, v13}, Landroid/os/Parcel;->writeInt(I)V

    #@874
    .line 863
    const/4 v1, 0x1

    #@875
    goto/16 :goto_7

    #@877
    .line 42
    nop

    #@878
    :sswitch_data_878
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_3c
        0x3 -> :sswitch_69
        0x4 -> :sswitch_95
        0x5 -> :sswitch_c3
        0x6 -> :sswitch_ef
        0x7 -> :sswitch_11d
        0x8 -> :sswitch_149
        0x9 -> :sswitch_177
        0xa -> :sswitch_1a3
        0xb -> :sswitch_1d1
        0xc -> :sswitch_1fd
        0xd -> :sswitch_22b
        0xe -> :sswitch_257
        0xf -> :sswitch_285
        0x10 -> :sswitch_2b1
        0x11 -> :sswitch_2df
        0x12 -> :sswitch_30b
        0x13 -> :sswitch_339
        0x14 -> :sswitch_365
        0x15 -> :sswitch_393
        0x16 -> :sswitch_3c1
        0x17 -> :sswitch_3e2
        0x18 -> :sswitch_3fe
        0x19 -> :sswitch_42a
        0x1a -> :sswitch_458
        0x1b -> :sswitch_481
        0x1c -> :sswitch_4ad
        0x1d -> :sswitch_4db
        0x1e -> :sswitch_4eb
        0x1f -> :sswitch_505
        0x20 -> :sswitch_54b
        0x21 -> :sswitch_577
        0x22 -> :sswitch_5ae
        0x23 -> :sswitch_5e1
        0x24 -> :sswitch_5fd
        0x25 -> :sswitch_62e
        0x26 -> :sswitch_661
        0x27 -> :sswitch_68d
        0x28 -> :sswitch_6bb
        0x29 -> :sswitch_6ec
        0x2a -> :sswitch_71f
        0x2b -> :sswitch_73b
        0x2c -> :sswitch_760
        0x2d -> :sswitch_79a
        0x2e -> :sswitch_7c2
        0x2f -> :sswitch_7f9
        0x30 -> :sswitch_82f
        0x31 -> :sswitch_845
        0x32 -> :sswitch_85b
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
