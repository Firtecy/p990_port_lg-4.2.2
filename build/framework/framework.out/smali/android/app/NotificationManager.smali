.class public Landroid/app/NotificationManager;
.super Ljava/lang/Object;
.source "NotificationManager.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static localLOGV:Z

.field private static sService:Landroid/app/INotificationManager;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 70
    const-string v0, "NotificationManager"

    #@2
    sput-object v0, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@4
    .line 71
    const/4 v0, 0x0

    #@5
    sput-boolean v0, Landroid/app/NotificationManager;->localLOGV:Z

    #@7
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    iput-object p1, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@5
    .line 89
    return-void
.end method

.method public static from(Landroid/content/Context;)Landroid/app/NotificationManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 93
    const-string/jumbo v0, "notification"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/app/NotificationManager;

    #@9
    return-object v0
.end method

.method public static getService()Landroid/app/INotificationManager;
    .registers 2

    #@0
    .prologue
    .line 78
    sget-object v1, Landroid/app/NotificationManager;->sService:Landroid/app/INotificationManager;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 79
    sget-object v1, Landroid/app/NotificationManager;->sService:Landroid/app/INotificationManager;

    #@6
    .line 83
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 81
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string/jumbo v1, "notification"

    #@a
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    .line 82
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    #@11
    move-result-object v1

    #@12
    sput-object v1, Landroid/app/NotificationManager;->sService:Landroid/app/INotificationManager;

    #@14
    .line 83
    sget-object v1, Landroid/app/NotificationManager;->sService:Landroid/app/INotificationManager;

    #@16
    goto :goto_6
.end method


# virtual methods
.method public cancel(I)V
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 173
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    #@4
    .line 174
    return-void
.end method

.method public cancel(Ljava/lang/String;I)V
    .registers 8
    .parameter "tag"
    .parameter "id"

    #@0
    .prologue
    .line 183
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@3
    move-result-object v1

    #@4
    .line 184
    .local v1, service:Landroid/app/INotificationManager;
    iget-object v2, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 185
    .local v0, pkg:Ljava/lang/String;
    sget-boolean v2, Landroid/app/NotificationManager;->localLOGV:Z

    #@c
    if-eqz v2, :cond_30

    #@e
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ": cancel("

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, ")"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 187
    :cond_30
    :try_start_30
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@33
    move-result v2

    #@34
    invoke-interface {v1, v0, p1, p2, v2}, Landroid/app/INotificationManager;->cancelNotificationWithTag(Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_37} :catch_38

    #@37
    .line 190
    :goto_37
    return-void

    #@38
    .line 188
    :catch_38
    move-exception v2

    #@39
    goto :goto_37
.end method

.method public cancelAll()V
    .registers 6

    #@0
    .prologue
    .line 212
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@3
    move-result-object v1

    #@4
    .line 213
    .local v1, service:Landroid/app/INotificationManager;
    iget-object v2, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 214
    .local v0, pkg:Ljava/lang/String;
    sget-boolean v2, Landroid/app/NotificationManager;->localLOGV:Z

    #@c
    if-eqz v2, :cond_26

    #@e
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ": cancelAll()"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 216
    :cond_26
    :try_start_26
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@29
    move-result v2

    #@2a
    invoke-interface {v1, v0, v2}, Landroid/app/INotificationManager;->cancelAllNotifications(Ljava/lang/String;I)V
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2d} :catch_2e

    #@2d
    .line 219
    :goto_2d
    return-void

    #@2e
    .line 217
    :catch_2e
    move-exception v2

    #@2f
    goto :goto_2d
.end method

.method public cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V
    .registers 9
    .parameter "tag"
    .parameter "id"
    .parameter "user"

    #@0
    .prologue
    .line 197
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@3
    move-result-object v1

    #@4
    .line 198
    .local v1, service:Landroid/app/INotificationManager;
    iget-object v2, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 199
    .local v0, pkg:Ljava/lang/String;
    sget-boolean v2, Landroid/app/NotificationManager;->localLOGV:Z

    #@c
    if-eqz v2, :cond_30

    #@e
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ": cancel("

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, ")"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 201
    :cond_30
    :try_start_30
    invoke-virtual {p3}, Landroid/os/UserHandle;->getIdentifier()I

    #@33
    move-result v2

    #@34
    invoke-interface {v1, v0, p1, p2, v2}, Landroid/app/INotificationManager;->cancelNotificationWithTag(Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_37} :catch_38

    #@37
    .line 204
    :goto_37
    return-void

    #@38
    .line 202
    :catch_38
    move-exception v2

    #@39
    goto :goto_37
.end method

.method public notify(ILandroid/app/Notification;)V
    .registers 4
    .parameter "id"
    .parameter "notification"

    #@0
    .prologue
    .line 108
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    #@4
    .line 109
    return-void
.end method

.method public notify(Ljava/lang/String;ILandroid/app/Notification;)V
    .registers 11
    .parameter "tag"
    .parameter "id"
    .parameter "notification"

    #@0
    .prologue
    .line 124
    const/4 v2, 0x1

    #@1
    new-array v5, v2, [I

    #@3
    .line 125
    .local v5, idOut:[I
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@6
    move-result-object v0

    #@7
    .line 126
    .local v0, service:Landroid/app/INotificationManager;
    iget-object v2, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 127
    .local v1, pkg:Ljava/lang/String;
    if-eqz p3, :cond_36

    #@f
    .line 128
    iget-object v2, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@11
    if-eqz v2, :cond_36

    #@13
    .line 129
    iget-object v2, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@15
    invoke-virtual {v2}, Landroid/net/Uri;->getCanonicalUri()Landroid/net/Uri;

    #@18
    move-result-object v2

    #@19
    iput-object v2, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@1b
    .line 130
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string/jumbo v4, "notify: notification.sound "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    iget-object v4, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 133
    :cond_36
    sget-boolean v2, Landroid/app/NotificationManager;->localLOGV:Z

    #@38
    if-eqz v2, :cond_66

    #@3a
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    const-string v4, ": notify("

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, ", "

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    const-string v4, ")"

    #@5b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 135
    :cond_66
    :try_start_66
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@69
    move-result v6

    #@6a
    move-object v2, p1

    #@6b
    move v3, p2

    #@6c
    move-object v4, p3

    #@6d
    invoke-interface/range {v0 .. v6}, Landroid/app/INotificationManager;->enqueueNotificationWithTag(Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V

    #@70
    .line 137
    const/4 v2, 0x0

    #@71
    aget v2, v5, v2

    #@73
    if-eq p2, v2, :cond_9b

    #@75
    .line 138
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@77
    new-instance v3, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string/jumbo v4, "notify: id corrupted: sent "

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    const-string v4, ", got back "

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    const/4 v4, 0x0

    #@8e
    aget v4, v5, v4

    #@90
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v3

    #@94
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v3

    #@98
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9b
    .catch Landroid/os/RemoteException; {:try_start_66 .. :try_end_9b} :catch_9c

    #@9b
    .line 142
    :cond_9b
    :goto_9b
    return-void

    #@9c
    .line 140
    :catch_9c
    move-exception v2

    #@9d
    goto :goto_9b
.end method

.method public notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
    .registers 12
    .parameter "tag"
    .parameter "id"
    .parameter "notification"
    .parameter "user"

    #@0
    .prologue
    .line 149
    const/4 v2, 0x1

    #@1
    new-array v5, v2, [I

    #@3
    .line 150
    .local v5, idOut:[I
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@6
    move-result-object v0

    #@7
    .line 151
    .local v0, service:Landroid/app/INotificationManager;
    iget-object v2, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 152
    .local v1, pkg:Ljava/lang/String;
    iget-object v2, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@f
    if-eqz v2, :cond_19

    #@11
    .line 153
    iget-object v2, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@13
    invoke-virtual {v2}, Landroid/net/Uri;->getCanonicalUri()Landroid/net/Uri;

    #@16
    move-result-object v2

    #@17
    iput-object v2, p3, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@19
    .line 155
    :cond_19
    sget-boolean v2, Landroid/app/NotificationManager;->localLOGV:Z

    #@1b
    if-eqz v2, :cond_49

    #@1d
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, ": notify("

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    const-string v4, ", "

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, ")"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 157
    :cond_49
    :try_start_49
    invoke-virtual {p4}, Landroid/os/UserHandle;->getIdentifier()I

    #@4c
    move-result v6

    #@4d
    move-object v2, p1

    #@4e
    move v3, p2

    #@4f
    move-object v4, p3

    #@50
    invoke-interface/range {v0 .. v6}, Landroid/app/INotificationManager;->enqueueNotificationWithTag(Ljava/lang/String;Ljava/lang/String;ILandroid/app/Notification;[II)V

    #@53
    .line 159
    const/4 v2, 0x0

    #@54
    aget v2, v5, v2

    #@56
    if-eq p2, v2, :cond_7e

    #@58
    .line 160
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@5a
    new-instance v3, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string/jumbo v4, "notify: id corrupted: sent "

    #@62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    const-string v4, ", got back "

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const/4 v4, 0x0

    #@71
    aget v4, v5, v4

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7e
    .catch Landroid/os/RemoteException; {:try_start_49 .. :try_end_7e} :catch_7f

    #@7e
    .line 164
    :cond_7e
    :goto_7e
    return-void

    #@7f
    .line 162
    :catch_7f
    move-exception v2

    #@80
    goto :goto_7e
.end method

.method public stopAlertSound()V
    .registers 6

    #@0
    .prologue
    .line 230
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    #@3
    move-result-object v1

    #@4
    .line 231
    .local v1, service:Landroid/app/INotificationManager;
    iget-object v2, p0, Landroid/app/NotificationManager;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 232
    .local v0, pkg:Ljava/lang/String;
    sget-boolean v2, Landroid/app/NotificationManager;->localLOGV:Z

    #@c
    if-eqz v2, :cond_26

    #@e
    .line 233
    sget-object v2, Landroid/app/NotificationManager;->TAG:Ljava/lang/String;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ": stopAlertSound()"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 236
    :cond_26
    if-eqz v1, :cond_2b

    #@28
    .line 237
    :try_start_28
    invoke-interface {v1}, Landroid/app/INotificationManager;->stopAlertSound()V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    .line 241
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 239
    :catch_2c
    move-exception v2

    #@2d
    goto :goto_2b
.end method
