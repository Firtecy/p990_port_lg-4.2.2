.class public abstract Landroid/app/IntentService;
.super Landroid/app/Service;
.source "IntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/IntentService$ServiceHandler;
    }
.end annotation


# instance fields
.field private mName:Ljava/lang/String;

.field private mRedelivery:Z

.field private volatile mServiceHandler:Landroid/app/IntentService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 77
    iput-object p1, p0, Landroid/app/IntentService;->mName:Ljava/lang/String;

    #@5
    .line 78
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 146
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate()V
    .registers 4

    #@0
    .prologue
    .line 106
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 107
    new-instance v0, Landroid/os/HandlerThread;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "IntentService["

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p0, Landroid/app/IntentService;->mName:Ljava/lang/String;

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "]"

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@23
    .line 108
    .local v0, thread:Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@26
    .line 110
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@29
    move-result-object v1

    #@2a
    iput-object v1, p0, Landroid/app/IntentService;->mServiceLooper:Landroid/os/Looper;

    #@2c
    .line 111
    new-instance v1, Landroid/app/IntentService$ServiceHandler;

    #@2e
    iget-object v2, p0, Landroid/app/IntentService;->mServiceLooper:Landroid/os/Looper;

    #@30
    invoke-direct {v1, p0, v2}, Landroid/app/IntentService$ServiceHandler;-><init>(Landroid/app/IntentService;Landroid/os/Looper;)V

    #@33
    iput-object v1, p0, Landroid/app/IntentService;->mServiceHandler:Landroid/app/IntentService$ServiceHandler;

    #@35
    .line 112
    return-void
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Landroid/app/IntentService;->mServiceLooper:Landroid/os/Looper;

    #@2
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@5
    .line 137
    return-void
.end method

.method protected abstract onHandleIntent(Landroid/content/Intent;)V
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 5
    .parameter "intent"
    .parameter "startId"

    #@0
    .prologue
    .line 116
    iget-object v1, p0, Landroid/app/IntentService;->mServiceHandler:Landroid/app/IntentService$ServiceHandler;

    #@2
    invoke-virtual {v1}, Landroid/app/IntentService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    .line 117
    .local v0, msg:Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@8
    .line 118
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 119
    iget-object v1, p0, Landroid/app/IntentService;->mServiceHandler:Landroid/app/IntentService$ServiceHandler;

    #@c
    invoke-virtual {v1, v0}, Landroid/app/IntentService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@f
    .line 120
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 130
    invoke-virtual {p0, p1, p3}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    #@3
    .line 131
    iget-boolean v0, p0, Landroid/app/IntentService;->mRedelivery:Z

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x3

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x2

    #@a
    goto :goto_8
.end method

.method public setIntentRedelivery(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 97
    iput-boolean p1, p0, Landroid/app/IntentService;->mRedelivery:Z

    #@2
    .line 98
    return-void
.end method
