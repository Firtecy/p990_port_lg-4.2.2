.class Landroid/app/IThumbnailReceiver$Stub$Proxy;
.super Ljava/lang/Object;
.source "IThumbnailReceiver.java"

# interfaces
.implements Landroid/app/IThumbnailReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IThumbnailReceiver$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 86
    iput-object p1, p0, Landroid/app/IThumbnailReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 87
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/app/IThumbnailReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public finished()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 126
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.IThumbnailReceiver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 127
    iget-object v1, p0, Landroid/app/IThumbnailReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x2

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 130
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 132
    return-void

    #@15
    .line 130
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 94
    const-string v0, "android.app.IThumbnailReceiver"

    #@2
    return-object v0
.end method

.method public newThumbnail(ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    .registers 9
    .parameter "id"
    .parameter "thumbnail"
    .parameter "description"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 98
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 100
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.IThumbnailReceiver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 101
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 102
    if-eqz p2, :cond_2c

    #@e
    .line 103
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 104
    const/4 v1, 0x0

    #@13
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 109
    :goto_16
    if-eqz p3, :cond_36

    #@18
    .line 110
    const/4 v1, 0x1

    #@19
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 111
    const/4 v1, 0x0

    #@1d
    invoke-static {p3, v0, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@20
    .line 116
    :goto_20
    iget-object v1, p0, Landroid/app/IThumbnailReceiver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/4 v2, 0x1

    #@23
    const/4 v3, 0x0

    #@24
    const/4 v4, 0x1

    #@25
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_28
    .catchall {:try_start_4 .. :try_end_28} :catchall_31

    #@28
    .line 119
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 121
    return-void

    #@2c
    .line 107
    :cond_2c
    const/4 v1, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_16

    #@31
    .line 119
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1

    #@36
    .line 114
    :cond_36
    const/4 v1, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_31

    #@3a
    goto :goto_20
.end method
