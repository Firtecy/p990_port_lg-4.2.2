.class final Landroid/app/FragmentState;
.super Ljava/lang/Object;
.source "Fragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/FragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mArguments:Landroid/os/Bundle;

.field final mClassName:Ljava/lang/String;

.field final mContainerId:I

.field final mDetached:Z

.field final mFragmentId:I

.field final mFromLayout:Z

.field final mIndex:I

.field mInstance:Landroid/app/Fragment;

.field final mRetainInstance:Z

.field mSavedFragmentState:Landroid/os/Bundle;

.field final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 135
    new-instance v0, Landroid/app/FragmentState$1;

    #@2
    invoke-direct {v0}, Landroid/app/FragmentState$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/FragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/app/Fragment;)V
    .registers 3
    .parameter "frag"

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 64
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/app/FragmentState;->mClassName:Ljava/lang/String;

    #@d
    .line 65
    iget v0, p1, Landroid/app/Fragment;->mIndex:I

    #@f
    iput v0, p0, Landroid/app/FragmentState;->mIndex:I

    #@11
    .line 66
    iget-boolean v0, p1, Landroid/app/Fragment;->mFromLayout:Z

    #@13
    iput-boolean v0, p0, Landroid/app/FragmentState;->mFromLayout:Z

    #@15
    .line 67
    iget v0, p1, Landroid/app/Fragment;->mFragmentId:I

    #@17
    iput v0, p0, Landroid/app/FragmentState;->mFragmentId:I

    #@19
    .line 68
    iget v0, p1, Landroid/app/Fragment;->mContainerId:I

    #@1b
    iput v0, p0, Landroid/app/FragmentState;->mContainerId:I

    #@1d
    .line 69
    iget-object v0, p1, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@1f
    iput-object v0, p0, Landroid/app/FragmentState;->mTag:Ljava/lang/String;

    #@21
    .line 70
    iget-boolean v0, p1, Landroid/app/Fragment;->mRetainInstance:Z

    #@23
    iput-boolean v0, p0, Landroid/app/FragmentState;->mRetainInstance:Z

    #@25
    .line 71
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    #@27
    iput-boolean v0, p0, Landroid/app/FragmentState;->mDetached:Z

    #@29
    .line 72
    iget-object v0, p1, Landroid/app/Fragment;->mArguments:Landroid/os/Bundle;

    #@2b
    iput-object v0, p0, Landroid/app/FragmentState;->mArguments:Landroid/os/Bundle;

    #@2d
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/app/FragmentState;->mClassName:Ljava/lang/String;

    #@b
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/app/FragmentState;->mIndex:I

    #@11
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_4a

    #@17
    move v0, v1

    #@18
    :goto_18
    iput-boolean v0, p0, Landroid/app/FragmentState;->mFromLayout:Z

    #@1a
    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v0

    #@1e
    iput v0, p0, Landroid/app/FragmentState;->mFragmentId:I

    #@20
    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v0

    #@24
    iput v0, p0, Landroid/app/FragmentState;->mContainerId:I

    #@26
    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    iput-object v0, p0, Landroid/app/FragmentState;->mTag:Ljava/lang/String;

    #@2c
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_4c

    #@32
    move v0, v1

    #@33
    :goto_33
    iput-boolean v0, p0, Landroid/app/FragmentState;->mRetainInstance:Z

    #@35
    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_4e

    #@3b
    :goto_3b
    iput-boolean v1, p0, Landroid/app/FragmentState;->mDetached:Z

    #@3d
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@40
    move-result-object v0

    #@41
    iput-object v0, p0, Landroid/app/FragmentState;->mArguments:Landroid/os/Bundle;

    #@43
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@49
    .line 86
    return-void

    #@4a
    :cond_4a
    move v0, v2

    #@4b
    .line 78
    goto :goto_18

    #@4c
    :cond_4c
    move v0, v2

    #@4d
    .line 82
    goto :goto_33

    #@4e
    :cond_4e
    move v1, v2

    #@4f
    .line 83
    goto :goto_3b
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 119
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public instantiate(Landroid/app/Activity;Landroid/app/Fragment;)Landroid/app/Fragment;
    .registers 6
    .parameter "activity"
    .parameter "parent"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 90
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@6
    .line 115
    :goto_6
    return-object v0

    #@7
    .line 93
    :cond_7
    iget-object v0, p0, Landroid/app/FragmentState;->mArguments:Landroid/os/Bundle;

    #@9
    if-eqz v0, :cond_14

    #@b
    .line 94
    iget-object v0, p0, Landroid/app/FragmentState;->mArguments:Landroid/os/Bundle;

    #@d
    invoke-virtual {p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@14
    .line 97
    :cond_14
    iget-object v0, p0, Landroid/app/FragmentState;->mClassName:Ljava/lang/String;

    #@16
    iget-object v1, p0, Landroid/app/FragmentState;->mArguments:Landroid/os/Bundle;

    #@18
    invoke-static {p1, v0, v1}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@1e
    .line 99
    iget-object v0, p0, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@20
    if-eqz v0, :cond_31

    #@22
    .line 100
    iget-object v0, p0, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@24
    invoke-virtual {p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@2b
    .line 101
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@2d
    iget-object v1, p0, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@2f
    iput-object v1, v0, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@31
    .line 103
    :cond_31
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@33
    iget v1, p0, Landroid/app/FragmentState;->mIndex:I

    #@35
    invoke-virtual {v0, v1, p2}, Landroid/app/Fragment;->setIndex(ILandroid/app/Fragment;)V

    #@38
    .line 104
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@3a
    iget-boolean v1, p0, Landroid/app/FragmentState;->mFromLayout:Z

    #@3c
    iput-boolean v1, v0, Landroid/app/Fragment;->mFromLayout:Z

    #@3e
    .line 105
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@40
    const/4 v1, 0x1

    #@41
    iput-boolean v1, v0, Landroid/app/Fragment;->mRestored:Z

    #@43
    .line 106
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@45
    iget v1, p0, Landroid/app/FragmentState;->mFragmentId:I

    #@47
    iput v1, v0, Landroid/app/Fragment;->mFragmentId:I

    #@49
    .line 107
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@4b
    iget v1, p0, Landroid/app/FragmentState;->mContainerId:I

    #@4d
    iput v1, v0, Landroid/app/Fragment;->mContainerId:I

    #@4f
    .line 108
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@51
    iget-object v1, p0, Landroid/app/FragmentState;->mTag:Ljava/lang/String;

    #@53
    iput-object v1, v0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@55
    .line 109
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@57
    iget-boolean v1, p0, Landroid/app/FragmentState;->mRetainInstance:Z

    #@59
    iput-boolean v1, v0, Landroid/app/Fragment;->mRetainInstance:Z

    #@5b
    .line 110
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@5d
    iget-boolean v1, p0, Landroid/app/FragmentState;->mDetached:Z

    #@5f
    iput-boolean v1, v0, Landroid/app/Fragment;->mDetached:Z

    #@61
    .line 111
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@63
    iget-object v1, p1, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@65
    iput-object v1, v0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@67
    .line 112
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@69
    if-eqz v0, :cond_85

    #@6b
    const-string v0, "FragmentManager"

    #@6d
    new-instance v1, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v2, "Instantiated fragment "

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    iget-object v2, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@7a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 115
    :cond_85
    iget-object v0, p0, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@87
    goto/16 :goto_6
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 123
    iget-object v0, p0, Landroid/app/FragmentState;->mClassName:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 124
    iget v0, p0, Landroid/app/FragmentState;->mIndex:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 125
    iget-boolean v0, p0, Landroid/app/FragmentState;->mFromLayout:Z

    #@e
    if-eqz v0, :cond_3d

    #@10
    move v0, v1

    #@11
    :goto_11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 126
    iget v0, p0, Landroid/app/FragmentState;->mFragmentId:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 127
    iget v0, p0, Landroid/app/FragmentState;->mContainerId:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 128
    iget-object v0, p0, Landroid/app/FragmentState;->mTag:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 129
    iget-boolean v0, p0, Landroid/app/FragmentState;->mRetainInstance:Z

    #@25
    if-eqz v0, :cond_3f

    #@27
    move v0, v1

    #@28
    :goto_28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 130
    iget-boolean v0, p0, Landroid/app/FragmentState;->mDetached:Z

    #@2d
    if-eqz v0, :cond_41

    #@2f
    :goto_2f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 131
    iget-object v0, p0, Landroid/app/FragmentState;->mArguments:Landroid/os/Bundle;

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@37
    .line 132
    iget-object v0, p0, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@3c
    .line 133
    return-void

    #@3d
    :cond_3d
    move v0, v2

    #@3e
    .line 125
    goto :goto_11

    #@3f
    :cond_3f
    move v0, v2

    #@40
    .line 129
    goto :goto_28

    #@41
    :cond_41
    move v1, v2

    #@42
    .line 130
    goto :goto_2f
.end method
