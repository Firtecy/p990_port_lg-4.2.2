.class public abstract Landroid/app/backup/IBackupManager$Stub;
.super Landroid/os/Binder;
.source "IBackupManager.java"

# interfaces
.implements Landroid/app/backup/IBackupManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/backup/IBackupManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/backup/IBackupManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.backup.IBackupManager"

.field static final TRANSACTION_acknowledgeFullBackupOrRestore:I = 0xf

.field static final TRANSACTION_agentConnected:I = 0x3

.field static final TRANSACTION_agentDisconnected:I = 0x4

.field static final TRANSACTION_backupNow:I = 0xc

.field static final TRANSACTION_beginRestoreSession:I = 0x15

.field static final TRANSACTION_clearBackupData:I = 0x2

.field static final TRANSACTION_dataChanged:I = 0x1

.field static final TRANSACTION_fullBackup:I = 0xd

.field static final TRANSACTION_fullRestore:I = 0xe

.field static final TRANSACTION_getConfigurationIntent:I = 0x13

.field static final TRANSACTION_getCurrentTransport:I = 0x10

.field static final TRANSACTION_getDestinationString:I = 0x14

.field static final TRANSACTION_hasBackupPassword:I = 0xb

.field static final TRANSACTION_isBackupEnabled:I = 0x9

.field static final TRANSACTION_listAllTransports:I = 0x11

.field static final TRANSACTION_opComplete:I = 0x16

.field static final TRANSACTION_restoreAtInstall:I = 0x5

.field static final TRANSACTION_selectBackupTransport:I = 0x12

.field static final TRANSACTION_setAutoRestore:I = 0x7

.field static final TRANSACTION_setBackupEnabled:I = 0x6

.field static final TRANSACTION_setBackupPassword:I = 0xa

.field static final TRANSACTION_setBackupProvisioned:I = 0x8


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 25
    const-string v0, "android.app.backup.IBackupManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/backup/IBackupManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 26
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 33
    if-nez p0, :cond_4

    #@2
    .line 34
    const/4 v0, 0x0

    #@3
    .line 40
    :goto_3
    return-object v0

    #@4
    .line 36
    :cond_4
    const-string v1, "android.app.backup.IBackupManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 37
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/backup/IBackupManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 38
    check-cast v0, Landroid/app/backup/IBackupManager;

    #@12
    goto :goto_3

    #@13
    .line 40
    :cond_13
    new-instance v0, Landroid/app/backup/IBackupManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/backup/IBackupManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 44
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 48
    sparse-switch p1, :sswitch_data_226

    #@5
    .line 295
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v8

    #@9
    :goto_9
    return v8

    #@a
    .line 52
    :sswitch_a
    const-string v0, "android.app.backup.IBackupManager"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 57
    :sswitch_10
    const-string v0, "android.app.backup.IBackupManager"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 60
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->dataChanged(Ljava/lang/String;)V

    #@1c
    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f
    goto :goto_9

    #@20
    .line 66
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_20
    const-string v0, "android.app.backup.IBackupManager"

    #@22
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    .line 69
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->clearBackupData(Ljava/lang/String;)V

    #@2c
    .line 70
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f
    goto :goto_9

    #@30
    .line 75
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_30
    const-string v0, "android.app.backup.IBackupManager"

    #@32
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    .line 79
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3c
    move-result-object v2

    #@3d
    .line 80
    .local v2, _arg1:Landroid/os/IBinder;
    invoke-virtual {p0, v1, v2}, Landroid/app/backup/IBackupManager$Stub;->agentConnected(Ljava/lang/String;Landroid/os/IBinder;)V

    #@40
    .line 81
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@43
    goto :goto_9

    #@44
    .line 86
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Landroid/os/IBinder;
    :sswitch_44
    const-string v0, "android.app.backup.IBackupManager"

    #@46
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49
    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4c
    move-result-object v1

    #@4d
    .line 89
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->agentDisconnected(Ljava/lang/String;)V

    #@50
    .line 90
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@53
    goto :goto_9

    #@54
    .line 95
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_54
    const-string v0, "android.app.backup.IBackupManager"

    #@56
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59
    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    .line 99
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@60
    move-result v2

    #@61
    .line 100
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/app/backup/IBackupManager$Stub;->restoreAtInstall(Ljava/lang/String;I)V

    #@64
    .line 101
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    goto :goto_9

    #@68
    .line 106
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    :sswitch_68
    const-string v9, "android.app.backup.IBackupManager"

    #@6a
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6d
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v9

    #@71
    if-eqz v9, :cond_7b

    #@73
    move v1, v8

    #@74
    .line 109
    .local v1, _arg0:Z
    :goto_74
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->setBackupEnabled(Z)V

    #@77
    .line 110
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7a
    goto :goto_9

    #@7b
    .end local v1           #_arg0:Z
    :cond_7b
    move v1, v0

    #@7c
    .line 108
    goto :goto_74

    #@7d
    .line 115
    :sswitch_7d
    const-string v9, "android.app.backup.IBackupManager"

    #@7f
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@82
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@85
    move-result v9

    #@86
    if-eqz v9, :cond_91

    #@88
    move v1, v8

    #@89
    .line 118
    .restart local v1       #_arg0:Z
    :goto_89
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->setAutoRestore(Z)V

    #@8c
    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8f
    goto/16 :goto_9

    #@91
    .end local v1           #_arg0:Z
    :cond_91
    move v1, v0

    #@92
    .line 117
    goto :goto_89

    #@93
    .line 124
    :sswitch_93
    const-string v9, "android.app.backup.IBackupManager"

    #@95
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@98
    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9b
    move-result v9

    #@9c
    if-eqz v9, :cond_a7

    #@9e
    move v1, v8

    #@9f
    .line 127
    .restart local v1       #_arg0:Z
    :goto_9f
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->setBackupProvisioned(Z)V

    #@a2
    .line 128
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a5
    goto/16 :goto_9

    #@a7
    .end local v1           #_arg0:Z
    :cond_a7
    move v1, v0

    #@a8
    .line 126
    goto :goto_9f

    #@a9
    .line 133
    :sswitch_a9
    const-string v9, "android.app.backup.IBackupManager"

    #@ab
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 134
    invoke-virtual {p0}, Landroid/app/backup/IBackupManager$Stub;->isBackupEnabled()Z

    #@b1
    move-result v7

    #@b2
    .line 135
    .local v7, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b5
    .line 136
    if-eqz v7, :cond_b8

    #@b7
    move v0, v8

    #@b8
    :cond_b8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@bb
    goto/16 :goto_9

    #@bd
    .line 141
    .end local v7           #_result:Z
    :sswitch_bd
    const-string v9, "android.app.backup.IBackupManager"

    #@bf
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c2
    .line 143
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c5
    move-result-object v1

    #@c6
    .line 145
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c9
    move-result-object v2

    #@ca
    .line 146
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/backup/IBackupManager$Stub;->setBackupPassword(Ljava/lang/String;Ljava/lang/String;)Z

    #@cd
    move-result v7

    #@ce
    .line 147
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d1
    .line 148
    if-eqz v7, :cond_d4

    #@d3
    move v0, v8

    #@d4
    :cond_d4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d7
    goto/16 :goto_9

    #@d9
    .line 153
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v7           #_result:Z
    :sswitch_d9
    const-string v9, "android.app.backup.IBackupManager"

    #@db
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@de
    .line 154
    invoke-virtual {p0}, Landroid/app/backup/IBackupManager$Stub;->hasBackupPassword()Z

    #@e1
    move-result v7

    #@e2
    .line 155
    .restart local v7       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e5
    .line 156
    if-eqz v7, :cond_e8

    #@e7
    move v0, v8

    #@e8
    :cond_e8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@eb
    goto/16 :goto_9

    #@ed
    .line 161
    .end local v7           #_result:Z
    :sswitch_ed
    const-string v0, "android.app.backup.IBackupManager"

    #@ef
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f2
    .line 162
    invoke-virtual {p0}, Landroid/app/backup/IBackupManager$Stub;->backupNow()V

    #@f5
    .line 163
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f8
    goto/16 :goto_9

    #@fa
    .line 168
    :sswitch_fa
    const-string v9, "android.app.backup.IBackupManager"

    #@fc
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ff
    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@102
    move-result v9

    #@103
    if-eqz v9, :cond_136

    #@105
    .line 171
    sget-object v9, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@107
    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10a
    move-result-object v1

    #@10b
    check-cast v1, Landroid/os/ParcelFileDescriptor;

    #@10d
    .line 177
    .local v1, _arg0:Landroid/os/ParcelFileDescriptor;
    :goto_10d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@110
    move-result v9

    #@111
    if-eqz v9, :cond_138

    #@113
    move v2, v8

    #@114
    .line 179
    .local v2, _arg1:Z
    :goto_114
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@117
    move-result v9

    #@118
    if-eqz v9, :cond_13a

    #@11a
    move v3, v8

    #@11b
    .line 181
    .local v3, _arg2:Z
    :goto_11b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11e
    move-result v9

    #@11f
    if-eqz v9, :cond_13c

    #@121
    move v4, v8

    #@122
    .line 183
    .local v4, _arg3:Z
    :goto_122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@125
    move-result v9

    #@126
    if-eqz v9, :cond_13e

    #@128
    move v5, v8

    #@129
    .line 185
    .local v5, _arg4:Z
    :goto_129
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@12c
    move-result-object v6

    #@12d
    .local v6, _arg5:[Ljava/lang/String;
    move-object v0, p0

    #@12e
    .line 186
    invoke-virtual/range {v0 .. v6}, Landroid/app/backup/IBackupManager$Stub;->fullBackup(Landroid/os/ParcelFileDescriptor;ZZZZ[Ljava/lang/String;)V

    #@131
    .line 187
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@134
    goto/16 :goto_9

    #@136
    .line 174
    .end local v1           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Z
    .end local v4           #_arg3:Z
    .end local v5           #_arg4:Z
    .end local v6           #_arg5:[Ljava/lang/String;
    :cond_136
    const/4 v1, 0x0

    #@137
    .restart local v1       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_10d

    #@138
    :cond_138
    move v2, v0

    #@139
    .line 177
    goto :goto_114

    #@13a
    .restart local v2       #_arg1:Z
    :cond_13a
    move v3, v0

    #@13b
    .line 179
    goto :goto_11b

    #@13c
    .restart local v3       #_arg2:Z
    :cond_13c
    move v4, v0

    #@13d
    .line 181
    goto :goto_122

    #@13e
    .restart local v4       #_arg3:Z
    :cond_13e
    move v5, v0

    #@13f
    .line 183
    goto :goto_129

    #@140
    .line 192
    .end local v1           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Z
    .end local v4           #_arg3:Z
    :sswitch_140
    const-string v0, "android.app.backup.IBackupManager"

    #@142
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@145
    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@148
    move-result v0

    #@149
    if-eqz v0, :cond_15b

    #@14b
    .line 195
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14d
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@150
    move-result-object v1

    #@151
    check-cast v1, Landroid/os/ParcelFileDescriptor;

    #@153
    .line 200
    .restart local v1       #_arg0:Landroid/os/ParcelFileDescriptor;
    :goto_153
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->fullRestore(Landroid/os/ParcelFileDescriptor;)V

    #@156
    .line 201
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@159
    goto/16 :goto_9

    #@15b
    .line 198
    .end local v1           #_arg0:Landroid/os/ParcelFileDescriptor;
    :cond_15b
    const/4 v1, 0x0

    #@15c
    .restart local v1       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_153

    #@15d
    .line 206
    .end local v1           #_arg0:Landroid/os/ParcelFileDescriptor;
    :sswitch_15d
    const-string v9, "android.app.backup.IBackupManager"

    #@15f
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@162
    .line 208
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@165
    move-result v1

    #@166
    .line 210
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@169
    move-result v9

    #@16a
    if-eqz v9, :cond_186

    #@16c
    move v2, v8

    #@16d
    .line 212
    .restart local v2       #_arg1:Z
    :goto_16d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@170
    move-result-object v3

    #@171
    .line 214
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@174
    move-result-object v4

    #@175
    .line 216
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@178
    move-result-object v0

    #@179
    invoke-static {v0}, Landroid/app/backup/IFullBackupRestoreObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IFullBackupRestoreObserver;

    #@17c
    move-result-object v5

    #@17d
    .local v5, _arg4:Landroid/app/backup/IFullBackupRestoreObserver;
    move-object v0, p0

    #@17e
    .line 217
    invoke-virtual/range {v0 .. v5}, Landroid/app/backup/IBackupManager$Stub;->acknowledgeFullBackupOrRestore(IZLjava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;)V

    #@181
    .line 218
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@184
    goto/16 :goto_9

    #@186
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Landroid/app/backup/IFullBackupRestoreObserver;
    :cond_186
    move v2, v0

    #@187
    .line 210
    goto :goto_16d

    #@188
    .line 223
    .end local v1           #_arg0:I
    :sswitch_188
    const-string v0, "android.app.backup.IBackupManager"

    #@18a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18d
    .line 224
    invoke-virtual {p0}, Landroid/app/backup/IBackupManager$Stub;->getCurrentTransport()Ljava/lang/String;

    #@190
    move-result-object v7

    #@191
    .line 225
    .local v7, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@194
    .line 226
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@197
    goto/16 :goto_9

    #@199
    .line 231
    .end local v7           #_result:Ljava/lang/String;
    :sswitch_199
    const-string v0, "android.app.backup.IBackupManager"

    #@19b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19e
    .line 232
    invoke-virtual {p0}, Landroid/app/backup/IBackupManager$Stub;->listAllTransports()[Ljava/lang/String;

    #@1a1
    move-result-object v7

    #@1a2
    .line 233
    .local v7, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a5
    .line 234
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@1a8
    goto/16 :goto_9

    #@1aa
    .line 239
    .end local v7           #_result:[Ljava/lang/String;
    :sswitch_1aa
    const-string v0, "android.app.backup.IBackupManager"

    #@1ac
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1af
    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b2
    move-result-object v1

    #@1b3
    .line 242
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->selectBackupTransport(Ljava/lang/String;)Ljava/lang/String;

    #@1b6
    move-result-object v7

    #@1b7
    .line 243
    .local v7, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ba
    .line 244
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1bd
    goto/16 :goto_9

    #@1bf
    .line 249
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v7           #_result:Ljava/lang/String;
    :sswitch_1bf
    const-string v9, "android.app.backup.IBackupManager"

    #@1c1
    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c4
    .line 251
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c7
    move-result-object v1

    #@1c8
    .line 252
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->getConfigurationIntent(Ljava/lang/String;)Landroid/content/Intent;

    #@1cb
    move-result-object v7

    #@1cc
    .line 253
    .local v7, _result:Landroid/content/Intent;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1cf
    .line 254
    if-eqz v7, :cond_1d9

    #@1d1
    .line 255
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@1d4
    .line 256
    invoke-virtual {v7, p3, v8}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d7
    goto/16 :goto_9

    #@1d9
    .line 259
    :cond_1d9
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1dc
    goto/16 :goto_9

    #@1de
    .line 265
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v7           #_result:Landroid/content/Intent;
    :sswitch_1de
    const-string v0, "android.app.backup.IBackupManager"

    #@1e0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e3
    .line 267
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e6
    move-result-object v1

    #@1e7
    .line 268
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->getDestinationString(Ljava/lang/String;)Ljava/lang/String;

    #@1ea
    move-result-object v7

    #@1eb
    .line 269
    .local v7, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1ee
    .line 270
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f1
    goto/16 :goto_9

    #@1f3
    .line 275
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v7           #_result:Ljava/lang/String;
    :sswitch_1f3
    const-string v0, "android.app.backup.IBackupManager"

    #@1f5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f8
    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1fb
    move-result-object v1

    #@1fc
    .line 279
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1ff
    move-result-object v2

    #@200
    .line 280
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/backup/IBackupManager$Stub;->beginRestoreSession(Ljava/lang/String;Ljava/lang/String;)Landroid/app/backup/IRestoreSession;

    #@203
    move-result-object v7

    #@204
    .line 281
    .local v7, _result:Landroid/app/backup/IRestoreSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@207
    .line 282
    if-eqz v7, :cond_212

    #@209
    invoke-interface {v7}, Landroid/app/backup/IRestoreSession;->asBinder()Landroid/os/IBinder;

    #@20c
    move-result-object v0

    #@20d
    :goto_20d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@210
    goto/16 :goto_9

    #@212
    :cond_212
    const/4 v0, 0x0

    #@213
    goto :goto_20d

    #@214
    .line 287
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v7           #_result:Landroid/app/backup/IRestoreSession;
    :sswitch_214
    const-string v0, "android.app.backup.IBackupManager"

    #@216
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@219
    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21c
    move-result v1

    #@21d
    .line 290
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Landroid/app/backup/IBackupManager$Stub;->opComplete(I)V

    #@220
    .line 291
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@223
    goto/16 :goto_9

    #@225
    .line 48
    nop

    #@226
    :sswitch_data_226
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_30
        0x4 -> :sswitch_44
        0x5 -> :sswitch_54
        0x6 -> :sswitch_68
        0x7 -> :sswitch_7d
        0x8 -> :sswitch_93
        0x9 -> :sswitch_a9
        0xa -> :sswitch_bd
        0xb -> :sswitch_d9
        0xc -> :sswitch_ed
        0xd -> :sswitch_fa
        0xe -> :sswitch_140
        0xf -> :sswitch_15d
        0x10 -> :sswitch_188
        0x11 -> :sswitch_199
        0x12 -> :sswitch_1aa
        0x13 -> :sswitch_1bf
        0x14 -> :sswitch_1de
        0x15 -> :sswitch_1f3
        0x16 -> :sswitch_214
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
