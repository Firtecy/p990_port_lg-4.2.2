.class public Landroid/app/backup/FullBackupDataOutput;
.super Ljava/lang/Object;
.source "FullBackupDataOutput.java"


# instance fields
.field private mData:Landroid/app/backup/BackupDataOutput;


# direct methods
.method public constructor <init>(Landroid/os/ParcelFileDescriptor;)V
    .registers 4
    .parameter "fd"

    #@0
    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 16
    new-instance v0, Landroid/app/backup/BackupDataOutput;

    #@5
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@8
    move-result-object v1

    #@9
    invoke-direct {v0, v1}, Landroid/app/backup/BackupDataOutput;-><init>(Ljava/io/FileDescriptor;)V

    #@c
    iput-object v0, p0, Landroid/app/backup/FullBackupDataOutput;->mData:Landroid/app/backup/BackupDataOutput;

    #@e
    .line 17
    return-void
.end method


# virtual methods
.method public getData()Landroid/app/backup/BackupDataOutput;
    .registers 2

    #@0
    .prologue
    .line 20
    iget-object v0, p0, Landroid/app/backup/FullBackupDataOutput;->mData:Landroid/app/backup/BackupDataOutput;

    #@2
    return-object v0
.end method
