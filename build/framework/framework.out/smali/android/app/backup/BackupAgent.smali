.class public abstract Landroid/app/backup/BackupAgent;
.super Landroid/content/ContextWrapper;
.source "BackupAgent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/backup/BackupAgent$1;,
        Landroid/app/backup/BackupAgent$BackupServiceBinder;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "BackupAgent"

.field public static final TYPE_DIRECTORY:I = 0x2

.field public static final TYPE_EOF:I = 0x0

.field public static final TYPE_FILE:I = 0x1

.field public static final TYPE_SYMLINK:I = 0x3


# instance fields
.field private final mBinder:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 125
    invoke-direct {p0, v1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@4
    .line 477
    new-instance v0, Landroid/app/backup/BackupAgent$BackupServiceBinder;

    #@6
    invoke-direct {v0, p0, v1}, Landroid/app/backup/BackupAgent$BackupServiceBinder;-><init>(Landroid/app/backup/BackupAgent;Landroid/app/backup/BackupAgent$1;)V

    #@9
    invoke-virtual {v0}, Landroid/app/backup/BackupAgent$BackupServiceBinder;->asBinder()Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/app/backup/BackupAgent;->mBinder:Landroid/os/IBinder;

    #@f
    .line 126
    return-void
.end method


# virtual methods
.method public attach(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 481
    invoke-virtual {p0, p1}, Landroid/app/backup/BackupAgent;->attachBaseContext(Landroid/content/Context;)V

    #@3
    .line 482
    return-void
.end method

.method public final fullBackupFile(Ljava/io/File;Landroid/app/backup/FullBackupDataOutput;)V
    .registers 17
    .parameter "file"
    .parameter "output"

    #@0
    .prologue
    .line 279
    invoke-virtual {p0}, Landroid/app/backup/BackupAgent;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@3
    move-result-object v6

    #@4
    .line 282
    .local v6, appInfo:Landroid/content/pm/ApplicationInfo;
    :try_start_4
    new-instance v0, Ljava/io/File;

    #@6
    iget-object v2, v6, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@8
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@e
    move-result-object v12

    #@f
    .line 283
    .local v12, mainDir:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/app/backup/BackupAgent;->getFilesDir()Ljava/io/File;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@16
    move-result-object v10

    #@17
    .line 284
    .local v10, filesDir:Ljava/lang/String;
    const-string v0, "foo"

    #@19
    invoke-virtual {p0, v0}, Landroid/app/backup/BackupAgent;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@24
    move-result-object v8

    #@25
    .line 285
    .local v8, dbDir:Ljava/lang/String;
    const-string v0, "foo"

    #@27
    invoke-virtual {p0, v0}, Landroid/app/backup/BackupAgent;->getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@32
    move-result-object v13

    #@33
    .line 286
    .local v13, spDir:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/app/backup/BackupAgent;->getCacheDir()Ljava/io/File;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@3a
    move-result-object v7

    #@3b
    .line 287
    .local v7, cacheDir:Ljava/lang/String;
    iget-object v0, v6, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@3d
    if-nez v0, :cond_59

    #@3f
    const/4 v11, 0x0

    #@40
    .line 293
    .local v11, libDir:Ljava/lang/String;
    :goto_40
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_43
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_43} :catch_65

    #@43
    move-result-object v4

    #@44
    .line 299
    .local v4, filePath:Ljava/lang/String;
    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@47
    move-result v0

    #@48
    if-nez v0, :cond_50

    #@4a
    invoke-virtual {v4, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4d
    move-result v0

    #@4e
    if-eqz v0, :cond_6e

    #@50
    .line 300
    :cond_50
    const-string v0, "BackupAgent"

    #@52
    const-string/jumbo v2, "lib and cache files are not backed up"

    #@55
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 328
    .end local v4           #filePath:Ljava/lang/String;
    .end local v7           #cacheDir:Ljava/lang/String;
    .end local v8           #dbDir:Ljava/lang/String;
    .end local v10           #filesDir:Ljava/lang/String;
    .end local v11           #libDir:Ljava/lang/String;
    .end local v12           #mainDir:Ljava/lang/String;
    .end local v13           #spDir:Ljava/lang/String;
    :goto_58
    return-void

    #@59
    .line 287
    .restart local v7       #cacheDir:Ljava/lang/String;
    .restart local v8       #dbDir:Ljava/lang/String;
    .restart local v10       #filesDir:Ljava/lang/String;
    .restart local v12       #mainDir:Ljava/lang/String;
    .restart local v13       #spDir:Ljava/lang/String;
    :cond_59
    :try_start_59
    new-instance v0, Ljava/io/File;

    #@5b
    iget-object v2, v6, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@5d
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@60
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_63} :catch_65

    #@63
    move-result-object v11

    #@64
    goto :goto_40

    #@65
    .line 294
    .end local v7           #cacheDir:Ljava/lang/String;
    .end local v8           #dbDir:Ljava/lang/String;
    .end local v10           #filesDir:Ljava/lang/String;
    .end local v12           #mainDir:Ljava/lang/String;
    .end local v13           #spDir:Ljava/lang/String;
    :catch_65
    move-exception v9

    #@66
    .line 295
    .local v9, e:Ljava/io/IOException;
    const-string v0, "BackupAgent"

    #@68
    const-string v2, "Unable to obtain canonical paths"

    #@6a
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_58

    #@6e
    .line 305
    .end local v9           #e:Ljava/io/IOException;
    .restart local v4       #filePath:Ljava/lang/String;
    .restart local v7       #cacheDir:Ljava/lang/String;
    .restart local v8       #dbDir:Ljava/lang/String;
    .restart local v10       #filesDir:Ljava/lang/String;
    .restart local v11       #libDir:Ljava/lang/String;
    .restart local v12       #mainDir:Ljava/lang/String;
    .restart local v13       #spDir:Ljava/lang/String;
    :cond_6e
    const/4 v3, 0x0

    #@6f
    .line 306
    .local v3, rootpath:Ljava/lang/String;
    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@72
    move-result v0

    #@73
    if-eqz v0, :cond_b1

    #@75
    .line 307
    const-string v1, "db"

    #@77
    .line 308
    .local v1, domain:Ljava/lang/String;
    move-object v3, v8

    #@78
    .line 324
    :goto_78
    const-string v0, "BackupAgent"

    #@7a
    new-instance v2, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v5, "backupFile() of "

    #@81
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    const-string v5, " => domain="

    #@8b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    const-string v5, " rootpath="

    #@95
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v2

    #@9d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v2

    #@a1
    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 326
    invoke-virtual {p0}, Landroid/app/backup/BackupAgent;->getPackageName()Ljava/lang/String;

    #@a7
    move-result-object v0

    #@a8
    const/4 v2, 0x0

    #@a9
    invoke-virtual/range {p2 .. p2}, Landroid/app/backup/FullBackupDataOutput;->getData()Landroid/app/backup/BackupDataOutput;

    #@ac
    move-result-object v5

    #@ad
    invoke-static/range {v0 .. v5}, Landroid/app/backup/FullBackup;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I

    #@b0
    goto :goto_58

    #@b1
    .line 309
    .end local v1           #domain:Ljava/lang/String;
    :cond_b1
    invoke-virtual {v4, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b4
    move-result v0

    #@b5
    if-eqz v0, :cond_bc

    #@b7
    .line 310
    const-string/jumbo v1, "sp"

    #@ba
    .line 311
    .restart local v1       #domain:Ljava/lang/String;
    move-object v3, v13

    #@bb
    goto :goto_78

    #@bc
    .line 312
    .end local v1           #domain:Ljava/lang/String;
    :cond_bc
    invoke-virtual {v4, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@bf
    move-result v0

    #@c0
    if-eqz v0, :cond_c6

    #@c2
    .line 313
    const-string v1, "f"

    #@c4
    .line 314
    .restart local v1       #domain:Ljava/lang/String;
    move-object v3, v10

    #@c5
    goto :goto_78

    #@c6
    .line 315
    .end local v1           #domain:Ljava/lang/String;
    :cond_c6
    invoke-virtual {v4, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c9
    move-result v0

    #@ca
    if-eqz v0, :cond_d1

    #@cc
    .line 316
    const-string/jumbo v1, "r"

    #@cf
    .line 317
    .restart local v1       #domain:Ljava/lang/String;
    move-object v3, v12

    #@d0
    goto :goto_78

    #@d1
    .line 319
    .end local v1           #domain:Ljava/lang/String;
    :cond_d1
    const-string v0, "BackupAgent"

    #@d3
    new-instance v2, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v5, "File "

    #@da
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v2

    #@de
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v2

    #@e2
    const-string v5, " is in an unsupported location; skipping"

    #@e4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v2

    #@e8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v2

    #@ec
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto/16 :goto_58
.end method

.method protected final fullBackupFileTree(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;Landroid/app/backup/FullBackupDataOutput;)V
    .registers 23
    .parameter "packageName"
    .parameter "domain"
    .parameter "rootPath"
    .parameter
    .parameter "output"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/app/backup/FullBackupDataOutput;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 340
    .local p4, excludes:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v14, Ljava/io/File;

    #@2
    move-object/from16 v0, p3

    #@4
    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 341
    .local v14, rootFile:Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_c6

    #@d
    .line 342
    new-instance v15, Ljava/util/LinkedList;

    #@f
    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    #@12
    .line 343
    .local v15, scanQueue:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/io/File;>;"
    invoke-virtual {v15, v14}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@15
    .line 345
    :cond_15
    :goto_15
    invoke-virtual {v15}, Ljava/util/LinkedList;->size()I

    #@18
    move-result v1

    #@19
    if-lez v1, :cond_c6

    #@1b
    .line 346
    const/4 v1, 0x0

    #@1c
    invoke-virtual {v15, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    #@1f
    move-result-object v11

    #@20
    check-cast v11, Ljava/io/File;

    #@22
    .line 349
    .local v11, file:Ljava/io/File;
    :try_start_22
    invoke-virtual {v11}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 352
    .local v5, filePath:Ljava/lang/String;
    if-eqz p4, :cond_30

    #@28
    move-object/from16 v0, p4

    #@2a
    invoke-virtual {v0, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@2d
    move-result v1

    #@2e
    if-nez v1, :cond_15

    #@30
    .line 357
    :cond_30
    sget-object v1, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@32
    invoke-interface {v1, v5}, Llibcore/io/Os;->lstat(Ljava/lang/String;)Llibcore/io/StructStat;

    #@35
    move-result-object v16

    #@36
    .line 358
    .local v16, stat:Llibcore/io/StructStat;
    move-object/from16 v0, v16

    #@38
    iget v1, v0, Llibcore/io/StructStat;->st_mode:I

    #@3a
    invoke-static {v1}, Llibcore/io/OsConstants;->S_ISLNK(I)Z

    #@3d
    move-result v1

    #@3e
    if-eqz v1, :cond_73

    #@40
    .line 359
    const-string v1, "BackupAgent"

    #@42
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v3, "Symlink (skipping)!: "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_58} :catch_59
    .catch Llibcore/io/ErrnoException; {:try_start_22 .. :try_end_58} :catch_91

    #@58
    goto :goto_15

    #@59
    .line 369
    .end local v5           #filePath:Ljava/lang/String;
    .end local v16           #stat:Llibcore/io/StructStat;
    :catch_59
    move-exception v9

    #@5a
    .line 370
    .local v9, e:Ljava/io/IOException;
    const-string v1, "BackupAgent"

    #@5c
    new-instance v2, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v3, "Error canonicalizing path of "

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_15

    #@73
    .line 361
    .end local v9           #e:Ljava/io/IOException;
    .restart local v5       #filePath:Ljava/lang/String;
    .restart local v16       #stat:Llibcore/io/StructStat;
    :cond_73
    :try_start_73
    move-object/from16 v0, v16

    #@75
    iget v1, v0, Llibcore/io/StructStat;->st_mode:I

    #@77
    invoke-static {v1}, Llibcore/io/OsConstants;->S_ISDIR(I)Z

    #@7a
    move-result v1

    #@7b
    if-eqz v1, :cond_b6

    #@7d
    .line 362
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@80
    move-result-object v8

    #@81
    .line 363
    .local v8, contents:[Ljava/io/File;
    if-eqz v8, :cond_b6

    #@83
    .line 364
    move-object v7, v8

    #@84
    .local v7, arr$:[Ljava/io/File;
    array-length v13, v7

    #@85
    .local v13, len$:I
    const/4 v12, 0x0

    #@86
    .local v12, i$:I
    :goto_86
    if-ge v12, v13, :cond_b6

    #@88
    aget-object v10, v7, v12

    #@8a
    .line 365
    .local v10, entry:Ljava/io/File;
    const/4 v1, 0x0

    #@8b
    invoke-virtual {v15, v1, v10}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_8e} :catch_59
    .catch Llibcore/io/ErrnoException; {:try_start_73 .. :try_end_8e} :catch_91

    #@8e
    .line 364
    add-int/lit8 v12, v12, 0x1

    #@90
    goto :goto_86

    #@91
    .line 372
    .end local v5           #filePath:Ljava/lang/String;
    .end local v7           #arr$:[Ljava/io/File;
    .end local v8           #contents:[Ljava/io/File;
    .end local v10           #entry:Ljava/io/File;
    .end local v12           #i$:I
    .end local v13           #len$:I
    .end local v16           #stat:Llibcore/io/StructStat;
    :catch_91
    move-exception v9

    #@92
    .line 373
    .local v9, e:Llibcore/io/ErrnoException;
    const-string v1, "BackupAgent"

    #@94
    new-instance v2, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v3, "Error scanning file "

    #@9b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v2

    #@9f
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v2

    #@a3
    const-string v3, " : "

    #@a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v2

    #@a9
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v2

    #@ad
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v2

    #@b1
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    goto/16 :goto_15

    #@b6
    .line 378
    .end local v9           #e:Llibcore/io/ErrnoException;
    .restart local v5       #filePath:Ljava/lang/String;
    .restart local v16       #stat:Llibcore/io/StructStat;
    :cond_b6
    const/4 v3, 0x0

    #@b7
    invoke-virtual/range {p5 .. p5}, Landroid/app/backup/FullBackupDataOutput;->getData()Landroid/app/backup/BackupDataOutput;

    #@ba
    move-result-object v6

    #@bb
    move-object/from16 v1, p1

    #@bd
    move-object/from16 v2, p2

    #@bf
    move-object/from16 v4, p3

    #@c1
    invoke-static/range {v1 .. v6}, Landroid/app/backup/FullBackup;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I

    #@c4
    goto/16 :goto_15

    #@c6
    .line 382
    .end local v5           #filePath:Ljava/lang/String;
    .end local v11           #file:Ljava/io/File;
    .end local v15           #scanQueue:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Ljava/io/File;>;"
    .end local v16           #stat:Llibcore/io/StructStat;
    :cond_c6
    return-void
.end method

.method public abstract onBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final onBind()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 474
    iget-object v0, p0, Landroid/app/backup/BackupAgent;->mBinder:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public onCreate()V
    .registers 1

    #@0
    .prologue
    .line 136
    return-void
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 145
    return-void
.end method

.method public onFullBackup(Landroid/app/backup/FullBackupDataOutput;)V
    .registers 25
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 220
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@3
    move-result-object v20

    #@4
    .line 222
    .local v20, appInfo:Landroid/content/pm/ApplicationInfo;
    new-instance v1, Ljava/io/File;

    #@6
    move-object/from16 v0, v20

    #@8
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@a
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@d
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@10
    move-result-object v4

    #@11
    .line 223
    .local v4, rootDir:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getFilesDir()Ljava/io/File;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@18
    move-result-object v9

    #@19
    .line 224
    .local v9, filesDir:Ljava/lang/String;
    const-string v1, "foo"

    #@1b
    move-object/from16 v0, p0

    #@1d
    invoke-virtual {v0, v1}, Landroid/app/backup/BackupAgent;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@28
    move-result-object v13

    #@29
    .line 225
    .local v13, databaseDir:Ljava/lang/String;
    const-string v1, "foo"

    #@2b
    move-object/from16 v0, p0

    #@2d
    invoke-virtual {v0, v1}, Landroid/app/backup/BackupAgent;->getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@38
    move-result-object v17

    #@39
    .line 226
    .local v17, sharedPrefsDir:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getCacheDir()Ljava/io/File;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@40
    move-result-object v21

    #@41
    .line 227
    .local v21, cacheDir:Ljava/lang/String;
    move-object/from16 v0, v20

    #@43
    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@45
    if-eqz v1, :cond_b6

    #@47
    new-instance v1, Ljava/io/File;

    #@49
    move-object/from16 v0, v20

    #@4b
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@4d
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@50
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@53
    move-result-object v22

    #@54
    .line 232
    .local v22, libDir:Ljava/lang/String;
    :goto_54
    new-instance v5, Ljava/util/HashSet;

    #@56
    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    #@59
    .line 233
    .local v5, filterSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getPackageName()Ljava/lang/String;

    #@5c
    move-result-object v2

    #@5d
    .line 236
    .local v2, packageName:Ljava/lang/String;
    if-eqz v22, :cond_64

    #@5f
    .line 237
    move-object/from16 v0, v22

    #@61
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@64
    .line 239
    :cond_64
    move-object/from16 v0, v21

    #@66
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@69
    .line 240
    invoke-virtual {v5, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@6c
    .line 241
    move-object/from16 v0, v17

    #@6e
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@71
    .line 242
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@74
    .line 243
    const-string/jumbo v3, "r"

    #@77
    move-object/from16 v1, p0

    #@79
    move-object/from16 v6, p1

    #@7b
    invoke-virtual/range {v1 .. v6}, Landroid/app/backup/BackupAgent;->fullBackupFileTree(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;Landroid/app/backup/FullBackupDataOutput;)V

    #@7e
    .line 246
    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@81
    .line 247
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@84
    .line 248
    const-string v8, "f"

    #@86
    move-object/from16 v6, p0

    #@88
    move-object v7, v2

    #@89
    move-object v10, v5

    #@8a
    move-object/from16 v11, p1

    #@8c
    invoke-virtual/range {v6 .. v11}, Landroid/app/backup/BackupAgent;->fullBackupFileTree(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;Landroid/app/backup/FullBackupDataOutput;)V

    #@8f
    .line 250
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@92
    .line 251
    invoke-virtual {v5, v13}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@95
    .line 252
    const-string v12, "db"

    #@97
    move-object/from16 v10, p0

    #@99
    move-object v11, v2

    #@9a
    move-object v14, v5

    #@9b
    move-object/from16 v15, p1

    #@9d
    invoke-virtual/range {v10 .. v15}, Landroid/app/backup/BackupAgent;->fullBackupFileTree(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;Landroid/app/backup/FullBackupDataOutput;)V

    #@a0
    .line 254
    invoke-virtual {v5, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@a3
    .line 255
    move-object/from16 v0, v17

    #@a5
    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@a8
    .line 256
    const-string/jumbo v16, "sp"

    #@ab
    move-object/from16 v14, p0

    #@ad
    move-object v15, v2

    #@ae
    move-object/from16 v18, v5

    #@b0
    move-object/from16 v19, p1

    #@b2
    invoke-virtual/range {v14 .. v19}, Landroid/app/backup/BackupAgent;->fullBackupFileTree(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;Landroid/app/backup/FullBackupDataOutput;)V

    #@b5
    .line 257
    return-void

    #@b6
    .line 227
    .end local v2           #packageName:Ljava/lang/String;
    .end local v5           #filterSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v22           #libDir:Ljava/lang/String;
    :cond_b6
    const/16 v22, 0x0

    #@b8
    goto :goto_54
.end method

.method public abstract onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected onRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJ)V
    .registers 29
    .parameter "data"
    .parameter "size"
    .parameter "type"
    .parameter "domain"
    .parameter "path"
    .parameter "mode"
    .parameter "mtime"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 424
    const/16 v16, 0x0

    #@2
    .line 426
    .local v16, basePath:Ljava/lang/String;
    const-string v2, "BackupAgent"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string/jumbo v4, "onRestoreFile() size="

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    move-wide/from16 v0, p2

    #@12
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, " type="

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    move/from16 v0, p4

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, " domain="

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    move-object/from16 v0, p5

    #@2a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    const-string v4, " relpath="

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    move-object/from16 v0, p6

    #@36
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v4, " mode="

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    move-wide/from16 v0, p7

    #@42
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, " mtime="

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    move-wide/from16 v0, p9

    #@4e
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 431
    const-string v2, "f"

    #@5b
    move-object/from16 v0, p5

    #@5d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v2

    #@61
    if-eqz v2, :cond_d9

    #@63
    .line 432
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getFilesDir()Ljava/io/File;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@6a
    move-result-object v16

    #@6b
    .line 447
    :goto_6b
    if-eqz v16, :cond_178

    #@6d
    .line 449
    new-instance v6, Ljava/io/File;

    #@6f
    move-object/from16 v0, v16

    #@71
    move-object/from16 v1, p6

    #@73
    invoke-direct {v6, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 450
    .local v6, outFile:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@79
    move-result-object v17

    #@7a
    .line 451
    .local v17, outPath:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    move-object/from16 v0, v16

    #@81
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    sget-char v3, Ljava/io/File;->separatorChar:C

    #@87
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v2

    #@8f
    move-object/from16 v0, v17

    #@91
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@94
    move-result v2

    #@95
    if-eqz v2, :cond_15e

    #@97
    .line 452
    const-string v2, "BackupAgent"

    #@99
    new-instance v3, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v4, "["

    #@a0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    move-object/from16 v0, p5

    #@a6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    const-string v4, " : "

    #@ac
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    move-object/from16 v0, p6

    #@b2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v3

    #@b6
    const-string v4, "] mapped to "

    #@b8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v3

    #@bc
    move-object/from16 v0, v17

    #@be
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v3

    #@c6
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c9
    move-object/from16 v2, p0

    #@cb
    move-object/from16 v3, p1

    #@cd
    move-wide/from16 v4, p2

    #@cf
    move/from16 v7, p4

    #@d1
    move-wide/from16 v8, p7

    #@d3
    move-wide/from16 v10, p9

    #@d5
    .line 453
    invoke-virtual/range {v2 .. v11}, Landroid/app/backup/BackupAgent;->onRestoreFile(Landroid/os/ParcelFileDescriptor;JLjava/io/File;IJJ)V

    #@d8
    .line 468
    .end local v6           #outFile:Ljava/io/File;
    .end local v17           #outPath:Ljava/lang/String;
    :goto_d8
    return-void

    #@d9
    .line 433
    :cond_d9
    const-string v2, "db"

    #@db
    move-object/from16 v0, p5

    #@dd
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0
    move-result v2

    #@e1
    if-eqz v2, :cond_f5

    #@e3
    .line 434
    const-string v2, "foo"

    #@e5
    move-object/from16 v0, p0

    #@e7
    invoke-virtual {v0, v2}, Landroid/app/backup/BackupAgent;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    #@ea
    move-result-object v2

    #@eb
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@ee
    move-result-object v2

    #@ef
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@f2
    move-result-object v16

    #@f3
    goto/16 :goto_6b

    #@f5
    .line 435
    :cond_f5
    const-string/jumbo v2, "r"

    #@f8
    move-object/from16 v0, p5

    #@fa
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fd
    move-result v2

    #@fe
    if-eqz v2, :cond_111

    #@100
    .line 436
    new-instance v2, Ljava/io/File;

    #@102
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@105
    move-result-object v3

    #@106
    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@108
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10b
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@10e
    move-result-object v16

    #@10f
    goto/16 :goto_6b

    #@111
    .line 437
    :cond_111
    const-string/jumbo v2, "sp"

    #@114
    move-object/from16 v0, p5

    #@116
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@119
    move-result v2

    #@11a
    if-eqz v2, :cond_12e

    #@11c
    .line 438
    const-string v2, "foo"

    #@11e
    move-object/from16 v0, p0

    #@120
    invoke-virtual {v0, v2}, Landroid/app/backup/BackupAgent;->getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;

    #@123
    move-result-object v2

    #@124
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@127
    move-result-object v2

    #@128
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@12b
    move-result-object v16

    #@12c
    goto/16 :goto_6b

    #@12e
    .line 439
    :cond_12e
    const-string v2, "c"

    #@130
    move-object/from16 v0, p5

    #@132
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@135
    move-result v2

    #@136
    if-eqz v2, :cond_142

    #@138
    .line 440
    invoke-virtual/range {p0 .. p0}, Landroid/app/backup/BackupAgent;->getCacheDir()Ljava/io/File;

    #@13b
    move-result-object v2

    #@13c
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@13f
    move-result-object v16

    #@140
    goto/16 :goto_6b

    #@142
    .line 443
    :cond_142
    const-string v2, "BackupAgent"

    #@144
    new-instance v3, Ljava/lang/StringBuilder;

    #@146
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@149
    const-string v4, "Unrecognized domain "

    #@14b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v3

    #@14f
    move-object/from16 v0, p5

    #@151
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v3

    #@155
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@158
    move-result-object v3

    #@159
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    goto/16 :goto_6b

    #@15e
    .line 458
    .restart local v6       #outFile:Ljava/io/File;
    .restart local v17       #outPath:Ljava/lang/String;
    :cond_15e
    const-string v2, "BackupAgent"

    #@160
    new-instance v3, Ljava/lang/StringBuilder;

    #@162
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@165
    const-string v4, "Cross-domain restore attempt: "

    #@167
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v3

    #@16b
    move-object/from16 v0, v17

    #@16d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v3

    #@171
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v3

    #@175
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@178
    .line 466
    .end local v6           #outFile:Ljava/io/File;
    .end local v17           #outPath:Ljava/lang/String;
    :cond_178
    const-string v2, "BackupAgent"

    #@17a
    new-instance v3, Ljava/lang/StringBuilder;

    #@17c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17f
    const-string v4, "[ skipping file "

    #@181
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v3

    #@185
    move-object/from16 v0, p6

    #@187
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v3

    #@18b
    const-string v4, "]"

    #@18d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v3

    #@191
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@194
    move-result-object v3

    #@195
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@198
    .line 467
    const/4 v15, 0x0

    #@199
    move-object/from16 v7, p1

    #@19b
    move-wide/from16 v8, p2

    #@19d
    move/from16 v10, p4

    #@19f
    move-wide/from16 v11, p7

    #@1a1
    move-wide/from16 v13, p9

    #@1a3
    invoke-static/range {v7 .. v15}, Landroid/app/backup/FullBackup;->restoreFile(Landroid/os/ParcelFileDescriptor;JIJJLjava/io/File;)V

    #@1a6
    goto/16 :goto_d8
.end method

.method public onRestoreFile(Landroid/os/ParcelFileDescriptor;JLjava/io/File;IJJ)V
    .registers 19
    .parameter "data"
    .parameter "size"
    .parameter "destination"
    .parameter "type"
    .parameter "mode"
    .parameter "mtime"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 413
    move-object v0, p1

    #@1
    move-wide v1, p2

    #@2
    move v3, p5

    #@3
    move-wide v4, p6

    #@4
    move-wide/from16 v6, p8

    #@6
    move-object v8, p4

    #@7
    invoke-static/range {v0 .. v8}, Landroid/app/backup/FullBackup;->restoreFile(Landroid/os/ParcelFileDescriptor;JIJJLjava/io/File;)V

    #@a
    .line 414
    return-void
.end method
