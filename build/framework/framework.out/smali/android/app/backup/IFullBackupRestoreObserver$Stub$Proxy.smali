.class Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;
.super Ljava/lang/Object;
.source "IFullBackupRestoreObserver.java"

# interfaces
.implements Landroid/app/backup/IFullBackupRestoreObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/backup/IFullBackupRestoreObserver$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 104
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 105
    iput-object p1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 106
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 113
    const-string v0, "android.app.backup.IFullBackupRestoreObserver"

    #@2
    return-object v0
.end method

.method public onBackupPackage(Ljava/lang/String;)V
    .registers 7
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 140
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 141
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 142
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x2

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 145
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 147
    return-void

    #@18
    .line 145
    :catchall_18
    move-exception v1

    #@19
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    throw v1
.end method

.method public onEndBackup()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 155
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 156
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x3

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 159
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 161
    return-void

    #@15
    .line 159
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public onEndRestore()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 200
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 202
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 203
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x6

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 206
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 208
    return-void

    #@15
    .line 206
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public onRestorePackage(Ljava/lang/String;)V
    .registers 7
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 187
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 188
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 189
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@e
    const/4 v2, 0x5

    #@f
    const/4 v3, 0x0

    #@10
    const/4 v4, 0x1

    #@11
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_18

    #@14
    .line 192
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@17
    .line 194
    return-void

    #@18
    .line 192
    :catchall_18
    move-exception v1

    #@19
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    throw v1
.end method

.method public onStartBackup()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 120
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 122
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 123
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x1

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 126
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 128
    return-void

    #@15
    .line 126
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public onStartRestore()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 169
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 170
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x4

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 173
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 175
    return-void

    #@15
    .line 173
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public onTimeout()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 214
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 216
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.backup.IFullBackupRestoreObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 217
    iget-object v1, p0, Landroid/app/backup/IFullBackupRestoreObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x7

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 220
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 222
    return-void

    #@15
    .line 220
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method
