.class Landroid/app/backup/BackupAgent$BackupServiceBinder;
.super Landroid/app/IBackupAgent$Stub;
.source "BackupAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/backup/BackupAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackupServiceBinder"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BackupServiceBinder"


# instance fields
.field final synthetic this$0:Landroid/app/backup/BackupAgent;


# direct methods
.method private constructor <init>(Landroid/app/backup/BackupAgent;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 485
    iput-object p1, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@2
    invoke-direct {p0}, Landroid/app/IBackupAgent$Stub;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/backup/BackupAgent;Landroid/app/backup/BackupAgent$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 485
    invoke-direct {p0, p1}, Landroid/app/backup/BackupAgent$BackupServiceBinder;-><init>(Landroid/app/backup/BackupAgent;)V

    #@3
    return-void
.end method


# virtual methods
.method public doBackup(Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    .registers 13
    .parameter "oldState"
    .parameter "data"
    .parameter "newState"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 494
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 496
    .local v1, ident:J
    const-string v4, "BackupServiceBinder"

    #@6
    const-string v5, "doBackup() invoked"

    #@8
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 497
    new-instance v3, Landroid/app/backup/BackupDataOutput;

    #@d
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@10
    move-result-object v4

    #@11
    invoke-direct {v3, v4}, Landroid/app/backup/BackupDataOutput;-><init>(Ljava/io/FileDescriptor;)V

    #@14
    .line 500
    .local v3, output:Landroid/app/backup/BackupDataOutput;
    :try_start_14
    iget-object v4, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@16
    invoke-virtual {v4, p1, v3, p3}, Landroid/app/backup/BackupAgent;->onBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    :try_end_19
    .catchall {:try_start_14 .. :try_end_19} :catchall_50
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_19} :catch_20
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_19} :catch_58

    #@19
    .line 508
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 510
    :try_start_1c
    invoke-interface {p5, p4}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1f} :catch_85

    #@1f
    .line 515
    :goto_1f
    return-void

    #@20
    .line 501
    :catch_20
    move-exception v0

    #@21
    .line 502
    .local v0, ex:Ljava/io/IOException;
    :try_start_21
    const-string v4, "BackupServiceBinder"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string/jumbo v6, "onBackup ("

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    iget-object v6, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@31
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    const-string v6, ") threw"

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    .line 503
    new-instance v4, Ljava/lang/RuntimeException;

    #@4c
    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@4f
    throw v4
    :try_end_50
    .catchall {:try_start_21 .. :try_end_50} :catchall_50

    #@50
    .line 508
    .end local v0           #ex:Ljava/io/IOException;
    :catchall_50
    move-exception v4

    #@51
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@54
    .line 510
    :try_start_54
    invoke-interface {p5, p4}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_57
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_57} :catch_83

    #@57
    .line 508
    :goto_57
    throw v4

    #@58
    .line 504
    :catch_58
    move-exception v0

    #@59
    .line 505
    .local v0, ex:Ljava/lang/RuntimeException;
    :try_start_59
    const-string v4, "BackupServiceBinder"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string/jumbo v6, "onBackup ("

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    iget-object v6, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@69
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    const-string v6, ") threw"

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v4, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@82
    .line 506
    throw v0
    :try_end_83
    .catchall {:try_start_59 .. :try_end_83} :catchall_50

    #@83
    .line 511
    .end local v0           #ex:Ljava/lang/RuntimeException;
    :catch_83
    move-exception v5

    #@84
    goto :goto_57

    #@85
    :catch_85
    move-exception v4

    #@86
    goto :goto_1f
.end method

.method public doFullBackup(Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    .registers 13
    .parameter "data"
    .parameter "token"
    .parameter "callbackBinder"

    #@0
    .prologue
    .line 548
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v3

    #@4
    .line 550
    .local v3, ident:J
    const-string v6, "BackupServiceBinder"

    #@6
    const-string v7, "doFullBackup() invoked"

    #@8
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 553
    :try_start_b
    iget-object v6, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@d
    new-instance v7, Landroid/app/backup/FullBackupDataOutput;

    #@f
    invoke-direct {v7, p1}, Landroid/app/backup/FullBackupDataOutput;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@12
    invoke-virtual {v6, v7}, Landroid/app/backup/BackupAgent;->onFullBackup(Landroid/app/backup/FullBackupDataOutput;)V
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_5b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_15} :catch_2b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_15} :catch_72

    #@15
    .line 564
    :try_start_15
    new-instance v5, Ljava/io/FileOutputStream;

    #@17
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1a
    move-result-object v6

    #@1b
    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@1e
    .line 565
    .local v5, out:Ljava/io/FileOutputStream;
    const/4 v6, 0x4

    #@1f
    new-array v0, v6, [B

    #@21
    .line 566
    .local v0, buf:[B
    invoke-virtual {v5, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_24} :catch_a8

    #@24
    .line 571
    .end local v0           #buf:[B
    .end local v5           #out:Ljava/io/FileOutputStream;
    :goto_24
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    .line 573
    :try_start_27
    invoke-interface {p3, p2}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2a} :catch_b2

    #@2a
    .line 578
    :goto_2a
    return-void

    #@2b
    .line 554
    :catch_2b
    move-exception v2

    #@2c
    .line 555
    .local v2, ex:Ljava/io/IOException;
    :try_start_2c
    const-string v6, "BackupServiceBinder"

    #@2e
    new-instance v7, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string/jumbo v8, "onBackup ("

    #@36
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    iget-object v8, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@3c
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3f
    move-result-object v8

    #@40
    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@43
    move-result-object v8

    #@44
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    const-string v8, ") threw"

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v7

    #@52
    invoke-static {v6, v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@55
    .line 556
    new-instance v6, Ljava/lang/RuntimeException;

    #@57
    invoke-direct {v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@5a
    throw v6
    :try_end_5b
    .catchall {:try_start_2c .. :try_end_5b} :catchall_5b

    #@5b
    .line 563
    .end local v2           #ex:Ljava/io/IOException;
    :catchall_5b
    move-exception v6

    #@5c
    .line 564
    :try_start_5c
    new-instance v5, Ljava/io/FileOutputStream;

    #@5e
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@61
    move-result-object v7

    #@62
    invoke-direct {v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@65
    .line 565
    .restart local v5       #out:Ljava/io/FileOutputStream;
    const/4 v7, 0x4

    #@66
    new-array v0, v7, [B

    #@68
    .line 566
    .restart local v0       #buf:[B
    invoke-virtual {v5, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_5c .. :try_end_6b} :catch_9d

    #@6b
    .line 571
    .end local v0           #buf:[B
    .end local v5           #out:Ljava/io/FileOutputStream;
    :goto_6b
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@6e
    .line 573
    :try_start_6e
    invoke-interface {p3, p2}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_71
    .catch Landroid/os/RemoteException; {:try_start_6e .. :try_end_71} :catch_a6

    #@71
    .line 563
    :goto_71
    throw v6

    #@72
    .line 557
    :catch_72
    move-exception v2

    #@73
    .line 558
    .local v2, ex:Ljava/lang/RuntimeException;
    :try_start_73
    const-string v6, "BackupServiceBinder"

    #@75
    new-instance v7, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string/jumbo v8, "onBackup ("

    #@7d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v7

    #@81
    iget-object v8, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@83
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8a
    move-result-object v8

    #@8b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v7

    #@8f
    const-string v8, ") threw"

    #@91
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v7

    #@95
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v7

    #@99
    invoke-static {v6, v7, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9c
    .line 559
    throw v2
    :try_end_9d
    .catchall {:try_start_73 .. :try_end_9d} :catchall_5b

    #@9d
    .line 567
    .end local v2           #ex:Ljava/lang/RuntimeException;
    :catch_9d
    move-exception v1

    #@9e
    .line 568
    .local v1, e:Ljava/io/IOException;
    const-string v7, "BackupServiceBinder"

    #@a0
    const-string v8, "Unable to finalize backup stream!"

    #@a2
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_6b

    #@a6
    .line 574
    .end local v1           #e:Ljava/io/IOException;
    :catch_a6
    move-exception v7

    #@a7
    goto :goto_71

    #@a8
    .line 567
    :catch_a8
    move-exception v1

    #@a9
    .line 568
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "BackupServiceBinder"

    #@ab
    const-string v7, "Unable to finalize backup stream!"

    #@ad
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    goto/16 :goto_24

    #@b2
    .line 574
    .end local v1           #e:Ljava/io/IOException;
    :catch_b2
    move-exception v6

    #@b3
    goto/16 :goto_2a
.end method

.method public doRestore(Landroid/os/ParcelFileDescriptor;ILandroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    .registers 13
    .parameter "data"
    .parameter "appVersionCode"
    .parameter "newState"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 522
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 524
    .local v1, ident:J
    const-string v4, "BackupServiceBinder"

    #@6
    const-string v5, "doRestore() invoked"

    #@8
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 525
    new-instance v3, Landroid/app/backup/BackupDataInput;

    #@d
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@10
    move-result-object v4

    #@11
    invoke-direct {v3, v4}, Landroid/app/backup/BackupDataInput;-><init>(Ljava/io/FileDescriptor;)V

    #@14
    .line 527
    .local v3, input:Landroid/app/backup/BackupDataInput;
    :try_start_14
    iget-object v4, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@16
    invoke-virtual {v4, v3, p2, p3}, Landroid/app/backup/BackupAgent;->onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
    :try_end_19
    .catchall {:try_start_14 .. :try_end_19} :catchall_50
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_19} :catch_20
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_19} :catch_58

    #@19
    .line 535
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 537
    :try_start_1c
    invoke-interface {p5, p4}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1f} :catch_85

    #@1f
    .line 542
    :goto_1f
    return-void

    #@20
    .line 528
    :catch_20
    move-exception v0

    #@21
    .line 529
    .local v0, ex:Ljava/io/IOException;
    :try_start_21
    const-string v4, "BackupServiceBinder"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string/jumbo v6, "onRestore ("

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    iget-object v6, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@31
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    const-string v6, ") threw"

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v5

    #@47
    invoke-static {v4, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    .line 530
    new-instance v4, Ljava/lang/RuntimeException;

    #@4c
    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@4f
    throw v4
    :try_end_50
    .catchall {:try_start_21 .. :try_end_50} :catchall_50

    #@50
    .line 535
    .end local v0           #ex:Ljava/io/IOException;
    :catchall_50
    move-exception v4

    #@51
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@54
    .line 537
    :try_start_54
    invoke-interface {p5, p4}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_57
    .catch Landroid/os/RemoteException; {:try_start_54 .. :try_end_57} :catch_83

    #@57
    .line 535
    :goto_57
    throw v4

    #@58
    .line 531
    :catch_58
    move-exception v0

    #@59
    .line 532
    .local v0, ex:Ljava/lang/RuntimeException;
    :try_start_59
    const-string v4, "BackupServiceBinder"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string/jumbo v6, "onRestore ("

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    iget-object v6, p0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@69
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    const-string v6, ") threw"

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v4, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@82
    .line 533
    throw v0
    :try_end_83
    .catchall {:try_start_59 .. :try_end_83} :catchall_50

    #@83
    .line 538
    .end local v0           #ex:Ljava/lang/RuntimeException;
    :catch_83
    move-exception v5

    #@84
    goto :goto_57

    #@85
    :catch_85
    move-exception v4

    #@86
    goto :goto_1f
.end method

.method public doRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJILandroid/app/backup/IBackupManager;)V
    .registers 29
    .parameter "data"
    .parameter "size"
    .parameter "type"
    .parameter "domain"
    .parameter "path"
    .parameter "mode"
    .parameter "mtime"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 584
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v14

    #@4
    .line 586
    .local v14, ident:J
    :try_start_4
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Landroid/app/backup/BackupAgent$BackupServiceBinder;->this$0:Landroid/app/backup/BackupAgent;

    #@8
    move-object/from16 v3, p1

    #@a
    move-wide/from16 v4, p2

    #@c
    move/from16 v6, p4

    #@e
    move-object/from16 v7, p5

    #@10
    move-object/from16 v8, p6

    #@12
    move-wide/from16 v9, p7

    #@14
    move-wide/from16 v11, p9

    #@16
    invoke-virtual/range {v2 .. v12}, Landroid/app/backup/BackupAgent;->onRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJ)V
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_2b
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_19} :catch_24

    #@19
    .line 590
    invoke-static {v14, v15}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 592
    :try_start_1c
    move-object/from16 v0, p12

    #@1e
    move/from16 v1, p11

    #@20
    invoke-interface {v0, v1}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_23} :catch_39

    #@23
    .line 597
    :goto_23
    return-void

    #@24
    .line 587
    :catch_24
    move-exception v13

    #@25
    .line 588
    .local v13, e:Ljava/io/IOException;
    :try_start_25
    new-instance v2, Ljava/lang/RuntimeException;

    #@27
    invoke-direct {v2, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@2a
    throw v2
    :try_end_2b
    .catchall {:try_start_25 .. :try_end_2b} :catchall_2b

    #@2b
    .line 590
    .end local v13           #e:Ljava/io/IOException;
    :catchall_2b
    move-exception v2

    #@2c
    invoke-static {v14, v15}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2f
    .line 592
    :try_start_2f
    move-object/from16 v0, p12

    #@31
    move/from16 v1, p11

    #@33
    invoke-interface {v0, v1}, Landroid/app/backup/IBackupManager;->opComplete(I)V
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_36} :catch_37

    #@36
    .line 590
    :goto_36
    throw v2

    #@37
    .line 593
    :catch_37
    move-exception v3

    #@38
    goto :goto_36

    #@39
    :catch_39
    move-exception v2

    #@3a
    goto :goto_23
.end method
