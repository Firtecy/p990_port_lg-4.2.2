.class public Landroid/app/backup/BackupManager;
.super Ljava/lang/Object;
.source "BackupManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BackupManager"

.field private static sService:Landroid/app/backup/IBackupManager;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    iput-object p1, p0, Landroid/app/backup/BackupManager;->mContext:Landroid/content/Context;

    #@5
    .line 81
    return-void
.end method

.method private static checkServiceBinder()V
    .registers 1

    #@0
    .prologue
    .line 65
    sget-object v0, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 66
    const-string v0, "backup"

    #@6
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    invoke-static {v0}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@10
    .line 69
    :cond_10
    return-void
.end method

.method public static dataChanged(Ljava/lang/String;)V
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 112
    invoke-static {}, Landroid/app/backup/BackupManager;->checkServiceBinder()V

    #@3
    .line 113
    sget-object v1, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@5
    if-eqz v1, :cond_c

    #@7
    .line 115
    :try_start_7
    sget-object v1, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@9
    invoke-interface {v1, p0}, Landroid/app/backup/IBackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_d

    #@c
    .line 120
    :cond_c
    :goto_c
    return-void

    #@d
    .line 116
    :catch_d
    move-exception v0

    #@e
    .line 117
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManager"

    #@10
    const-string v2, "dataChanged(pkg) couldn\'t connect"

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    goto :goto_c
.end method


# virtual methods
.method public beginRestoreSession()Landroid/app/backup/RestoreSession;
    .registers 8

    #@0
    .prologue
    .line 169
    const/4 v2, 0x0

    #@1
    .line 170
    .local v2, session:Landroid/app/backup/RestoreSession;
    invoke-static {}, Landroid/app/backup/BackupManager;->checkServiceBinder()V

    #@4
    .line 171
    sget-object v4, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@6
    if-eqz v4, :cond_1a

    #@8
    .line 174
    :try_start_8
    sget-object v4, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@a
    const/4 v5, 0x0

    #@b
    const/4 v6, 0x0

    #@c
    invoke-interface {v4, v5, v6}, Landroid/app/backup/IBackupManager;->beginRestoreSession(Ljava/lang/String;Ljava/lang/String;)Landroid/app/backup/IRestoreSession;

    #@f
    move-result-object v0

    #@10
    .line 175
    .local v0, binder:Landroid/app/backup/IRestoreSession;
    if-eqz v0, :cond_1a

    #@12
    .line 176
    new-instance v3, Landroid/app/backup/RestoreSession;

    #@14
    iget-object v4, p0, Landroid/app/backup/BackupManager;->mContext:Landroid/content/Context;

    #@16
    invoke-direct {v3, v4, v0}, Landroid/app/backup/RestoreSession;-><init>(Landroid/content/Context;Landroid/app/backup/IRestoreSession;)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_19} :catch_1b

    #@19
    .end local v2           #session:Landroid/app/backup/RestoreSession;
    .local v3, session:Landroid/app/backup/RestoreSession;
    move-object v2, v3

    #@1a
    .line 182
    .end local v0           #binder:Landroid/app/backup/IRestoreSession;
    .end local v3           #session:Landroid/app/backup/RestoreSession;
    .restart local v2       #session:Landroid/app/backup/RestoreSession;
    :cond_1a
    :goto_1a
    return-object v2

    #@1b
    .line 178
    :catch_1b
    move-exception v1

    #@1c
    .line 179
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "BackupManager"

    #@1e
    const-string v5, "beginRestoreSession() couldn\'t connect"

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_1a
.end method

.method public dataChanged()V
    .registers 4

    #@0
    .prologue
    .line 90
    invoke-static {}, Landroid/app/backup/BackupManager;->checkServiceBinder()V

    #@3
    .line 91
    sget-object v1, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@5
    if-eqz v1, :cond_12

    #@7
    .line 93
    :try_start_7
    sget-object v1, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@9
    iget-object v2, p0, Landroid/app/backup/BackupManager;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v1, v2}, Landroid/app/backup/IBackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_12} :catch_13

    #@12
    .line 98
    :cond_12
    :goto_12
    return-void

    #@13
    .line 94
    :catch_13
    move-exception v0

    #@14
    .line 95
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManager"

    #@16
    const-string v2, "dataChanged() couldn\'t connect"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_12
.end method

.method public requestRestore(Landroid/app/backup/RestoreObserver;)I
    .registers 10
    .parameter "observer"

    #@0
    .prologue
    .line 141
    const/4 v2, -0x1

    #@1
    .line 142
    .local v2, result:I
    invoke-static {}, Landroid/app/backup/BackupManager;->checkServiceBinder()V

    #@4
    .line 143
    sget-object v5, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@6
    if-eqz v5, :cond_2f

    #@8
    .line 144
    const/4 v3, 0x0

    #@9
    .line 146
    .local v3, session:Landroid/app/backup/RestoreSession;
    :try_start_9
    sget-object v5, Landroid/app/backup/BackupManager;->sService:Landroid/app/backup/IBackupManager;

    #@b
    iget-object v6, p0, Landroid/app/backup/BackupManager;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@10
    move-result-object v6

    #@11
    const/4 v7, 0x0

    #@12
    invoke-interface {v5, v6, v7}, Landroid/app/backup/IBackupManager;->beginRestoreSession(Ljava/lang/String;Ljava/lang/String;)Landroid/app/backup/IRestoreSession;

    #@15
    move-result-object v0

    #@16
    .line 148
    .local v0, binder:Landroid/app/backup/IRestoreSession;
    if-eqz v0, :cond_2a

    #@18
    .line 149
    new-instance v4, Landroid/app/backup/RestoreSession;

    #@1a
    iget-object v5, p0, Landroid/app/backup/BackupManager;->mContext:Landroid/content/Context;

    #@1c
    invoke-direct {v4, v5, v0}, Landroid/app/backup/RestoreSession;-><init>(Landroid/content/Context;Landroid/app/backup/IRestoreSession;)V
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_1f} :catch_30

    #@1f
    .line 150
    .end local v3           #session:Landroid/app/backup/RestoreSession;
    .local v4, session:Landroid/app/backup/RestoreSession;
    :try_start_1f
    iget-object v5, p0, Landroid/app/backup/BackupManager;->mContext:Landroid/content/Context;

    #@21
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v4, v5, p1}, Landroid/app/backup/RestoreSession;->restorePackage(Ljava/lang/String;Landroid/app/backup/RestoreObserver;)I
    :try_end_28
    .catchall {:try_start_1f .. :try_end_28} :catchall_46
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_28} :catch_49

    #@28
    move-result v2

    #@29
    move-object v3, v4

    #@2a
    .line 155
    .end local v4           #session:Landroid/app/backup/RestoreSession;
    .restart local v3       #session:Landroid/app/backup/RestoreSession;
    :cond_2a
    if-eqz v3, :cond_2f

    #@2c
    .line 156
    invoke-virtual {v3}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    #@2f
    .line 160
    .end local v0           #binder:Landroid/app/backup/IRestoreSession;
    .end local v3           #session:Landroid/app/backup/RestoreSession;
    :cond_2f
    :goto_2f
    return v2

    #@30
    .line 152
    .restart local v3       #session:Landroid/app/backup/RestoreSession;
    :catch_30
    move-exception v1

    #@31
    .line 153
    .local v1, e:Landroid/os/RemoteException;
    :goto_31
    :try_start_31
    const-string v5, "BackupManager"

    #@33
    const-string/jumbo v6, "restoreSelf() unable to contact service"

    #@36
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_39
    .catchall {:try_start_31 .. :try_end_39} :catchall_3f

    #@39
    .line 155
    if-eqz v3, :cond_2f

    #@3b
    .line 156
    invoke-virtual {v3}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    #@3e
    goto :goto_2f

    #@3f
    .line 155
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_3f
    move-exception v5

    #@40
    :goto_40
    if-eqz v3, :cond_45

    #@42
    .line 156
    invoke-virtual {v3}, Landroid/app/backup/RestoreSession;->endRestoreSession()V

    #@45
    :cond_45
    throw v5

    #@46
    .line 155
    .end local v3           #session:Landroid/app/backup/RestoreSession;
    .restart local v0       #binder:Landroid/app/backup/IRestoreSession;
    .restart local v4       #session:Landroid/app/backup/RestoreSession;
    :catchall_46
    move-exception v5

    #@47
    move-object v3, v4

    #@48
    .end local v4           #session:Landroid/app/backup/RestoreSession;
    .restart local v3       #session:Landroid/app/backup/RestoreSession;
    goto :goto_40

    #@49
    .line 152
    .end local v3           #session:Landroid/app/backup/RestoreSession;
    .restart local v4       #session:Landroid/app/backup/RestoreSession;
    :catch_49
    move-exception v1

    #@4a
    move-object v3, v4

    #@4b
    .end local v4           #session:Landroid/app/backup/RestoreSession;
    .restart local v3       #session:Landroid/app/backup/RestoreSession;
    goto :goto_31
.end method
