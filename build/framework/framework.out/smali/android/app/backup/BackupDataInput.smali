.class public Landroid/app/backup/BackupDataInput;
.super Ljava/lang/Object;
.source "BackupDataInput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/backup/BackupDataInput$1;,
        Landroid/app/backup/BackupDataInput$EntityHeader;
    }
.end annotation


# instance fields
.field mBackupReader:I

.field private mHeader:Landroid/app/backup/BackupDataInput$EntityHeader;

.field private mHeaderReady:Z


# direct methods
.method public constructor <init>(Ljava/io/FileDescriptor;)V
    .registers 5
    .parameter "fd"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 64
    new-instance v0, Landroid/app/backup/BackupDataInput$EntityHeader;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {v0, v1}, Landroid/app/backup/BackupDataInput$EntityHeader;-><init>(Landroid/app/backup/BackupDataInput$1;)V

    #@9
    iput-object v0, p0, Landroid/app/backup/BackupDataInput;->mHeader:Landroid/app/backup/BackupDataInput$EntityHeader;

    #@b
    .line 74
    if-nez p1, :cond_13

    #@d
    new-instance v0, Ljava/lang/NullPointerException;

    #@f
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@12
    throw v0

    #@13
    .line 75
    :cond_13
    invoke-static {p1}, Landroid/app/backup/BackupDataInput;->ctor(Ljava/io/FileDescriptor;)I

    #@16
    move-result v0

    #@17
    iput v0, p0, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@19
    .line 76
    iget v0, p0, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@1b
    if-nez v0, :cond_36

    #@1d
    .line 77
    new-instance v0, Ljava/lang/RuntimeException;

    #@1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "Native initialization failed with fd="

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@35
    throw v0

    #@36
    .line 79
    :cond_36
    return-void
.end method

.method private static native ctor(Ljava/io/FileDescriptor;)I
.end method

.method private static native dtor(I)V
.end method

.method private native readEntityData_native(I[BII)I
.end method

.method private native readNextHeader_native(ILandroid/app/backup/BackupDataInput$EntityHeader;)I
.end method

.method private native skipEntityData_native(I)I
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 84
    :try_start_0
    iget v0, p0, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@2
    invoke-static {v0}, Landroid/app/backup/BackupDataInput;->dtor(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 86
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 88
    return-void

    #@9
    .line 86
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method public getDataSize()I
    .registers 3

    #@0
    .prologue
    .line 137
    iget-boolean v0, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 138
    iget-object v0, p0, Landroid/app/backup/BackupDataInput;->mHeader:Landroid/app/backup/BackupDataInput$EntityHeader;

    #@6
    iget v0, v0, Landroid/app/backup/BackupDataInput$EntityHeader;->dataSize:I

    #@8
    return v0

    #@9
    .line 140
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    #@b
    const-string v1, "Entity header not read"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0
.end method

.method public getKey()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 122
    iget-boolean v0, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 123
    iget-object v0, p0, Landroid/app/backup/BackupDataInput;->mHeader:Landroid/app/backup/BackupDataInput$EntityHeader;

    #@6
    iget-object v0, v0, Landroid/app/backup/BackupDataInput$EntityHeader;->key:Ljava/lang/String;

    #@8
    return-object v0

    #@9
    .line 125
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    #@b
    const-string v1, "Entity header not read"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0
.end method

.method public readEntityData([BII)I
    .registers 8
    .parameter "data"
    .parameter "offset"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 160
    iget-boolean v1, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@2
    if-eqz v1, :cond_2b

    #@4
    .line 161
    iget v1, p0, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@6
    invoke-direct {p0, v1, p1, p2, p3}, Landroid/app/backup/BackupDataInput;->readEntityData_native(I[BII)I

    #@9
    move-result v0

    #@a
    .line 162
    .local v0, result:I
    if-ltz v0, :cond_d

    #@c
    .line 163
    return v0

    #@d
    .line 165
    :cond_d
    new-instance v1, Ljava/io/IOException;

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string/jumbo v3, "result=0x"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v1

    #@2b
    .line 168
    .end local v0           #result:I
    :cond_2b
    new-instance v1, Ljava/lang/IllegalStateException;

    #@2d
    const-string v2, "Entity header not read"

    #@2f
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@32
    throw v1
.end method

.method public readNextHeader()Z
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 100
    iget v3, p0, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@4
    iget-object v4, p0, Landroid/app/backup/BackupDataInput;->mHeader:Landroid/app/backup/BackupDataInput$EntityHeader;

    #@6
    invoke-direct {p0, v3, v4}, Landroid/app/backup/BackupDataInput;->readNextHeader_native(ILandroid/app/backup/BackupDataInput$EntityHeader;)I

    #@9
    move-result v0

    #@a
    .line 101
    .local v0, result:I
    if-nez v0, :cond_f

    #@c
    .line 103
    iput-boolean v1, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@e
    .line 108
    :goto_e
    return v1

    #@f
    .line 105
    :cond_f
    if-lez v0, :cond_15

    #@11
    .line 107
    iput-boolean v2, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@13
    move v1, v2

    #@14
    .line 108
    goto :goto_e

    #@15
    .line 111
    :cond_15
    iput-boolean v2, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@17
    .line 112
    new-instance v1, Ljava/io/IOException;

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "failed: 0x"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@33
    throw v1
.end method

.method public skipEntityData()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 181
    iget-boolean v0, p0, Landroid/app/backup/BackupDataInput;->mHeaderReady:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 182
    iget v0, p0, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@6
    invoke-direct {p0, v0}, Landroid/app/backup/BackupDataInput;->skipEntityData_native(I)I

    #@9
    .line 186
    return-void

    #@a
    .line 184
    :cond_a
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    const-string v1, "Entity header not read"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
.end method
