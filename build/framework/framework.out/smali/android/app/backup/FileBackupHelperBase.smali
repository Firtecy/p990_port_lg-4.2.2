.class Landroid/app/backup/FileBackupHelperBase;
.super Ljava/lang/Object;
.source "FileBackupHelperBase.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FileBackupHelperBase"


# instance fields
.field mContext:Landroid/content/Context;

.field mExceptionLogged:Z

.field mPtr:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    invoke-static {}, Landroid/app/backup/FileBackupHelperBase;->ctor()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/app/backup/FileBackupHelperBase;->mPtr:I

    #@9
    .line 38
    iput-object p1, p0, Landroid/app/backup/FileBackupHelperBase;->mContext:Landroid/content/Context;

    #@b
    .line 39
    return-void
.end method

.method private static native ctor()I
.end method

.method private static native dtor(I)V
.end method

.method static performBackup_checked(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 15
    .parameter "oldState"
    .parameter "data"
    .parameter "newState"
    .parameter "files"
    .parameter "keys"

    #@0
    .prologue
    .line 55
    array-length v7, p3

    #@1
    if-nez v7, :cond_4

    #@3
    .line 82
    :cond_3
    return-void

    #@4
    .line 59
    :cond_4
    move-object v0, p3

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@6
    .local v4, len$:I
    const/4 v3, 0x0

    #@7
    .local v3, i$:I
    :goto_7
    if-ge v3, v4, :cond_30

    #@9
    aget-object v2, v0, v3

    #@b
    .line 60
    .local v2, f:Ljava/lang/String;
    const/4 v7, 0x0

    #@c
    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v7

    #@10
    const/16 v8, 0x2f

    #@12
    if-eq v7, v8, :cond_2d

    #@14
    .line 61
    new-instance v7, Ljava/lang/RuntimeException;

    #@16
    new-instance v8, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v9, "files must have all absolute paths: "

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v8

    #@29
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v7

    #@2d
    .line 59
    :cond_2d
    add-int/lit8 v3, v3, 0x1

    #@2f
    goto :goto_7

    #@30
    .line 65
    .end local v2           #f:Ljava/lang/String;
    :cond_30
    array-length v7, p3

    #@31
    array-length v8, p4

    #@32
    if-eq v7, v8, :cond_59

    #@34
    .line 66
    new-instance v7, Ljava/lang/RuntimeException;

    #@36
    new-instance v8, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v9, "files.length="

    #@3d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    array-length v9, p3

    #@42
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v8

    #@46
    const-string v9, " keys.length="

    #@48
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v8

    #@4c
    array-length v9, p4

    #@4d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v8

    #@51
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v8

    #@55
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@58
    throw v7

    #@59
    .line 70
    :cond_59
    if-eqz p0, :cond_6b

    #@5b
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5e
    move-result-object v6

    #@5f
    .line 71
    .local v6, oldStateFd:Ljava/io/FileDescriptor;
    :goto_5f
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@62
    move-result-object v5

    #@63
    .line 72
    .local v5, newStateFd:Ljava/io/FileDescriptor;
    if-nez v5, :cond_6d

    #@65
    .line 73
    new-instance v7, Ljava/lang/NullPointerException;

    #@67
    invoke-direct {v7}, Ljava/lang/NullPointerException;-><init>()V

    #@6a
    throw v7

    #@6b
    .line 70
    .end local v5           #newStateFd:Ljava/io/FileDescriptor;
    .end local v6           #oldStateFd:Ljava/io/FileDescriptor;
    :cond_6b
    const/4 v6, 0x0

    #@6c
    goto :goto_5f

    #@6d
    .line 76
    .restart local v5       #newStateFd:Ljava/io/FileDescriptor;
    .restart local v6       #oldStateFd:Ljava/io/FileDescriptor;
    :cond_6d
    iget v7, p1, Landroid/app/backup/BackupDataOutput;->mBackupWriter:I

    #@6f
    invoke-static {v6, v7, v5, p3, p4}, Landroid/app/backup/FileBackupHelperBase;->performBackup_native(Ljava/io/FileDescriptor;ILjava/io/FileDescriptor;[Ljava/lang/String;[Ljava/lang/String;)I

    #@72
    move-result v1

    #@73
    .line 78
    .local v1, err:I
    if-eqz v1, :cond_3

    #@75
    .line 80
    new-instance v7, Ljava/lang/RuntimeException;

    #@77
    new-instance v8, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v9, "Backup failed 0x"

    #@7e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@85
    move-result-object v9

    #@86
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v8

    #@8e
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@91
    throw v7
.end method

.method private static native performBackup_native(Ljava/io/FileDescriptor;ILjava/io/FileDescriptor;[Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method private static native writeFile_native(ILjava/lang/String;I)I
.end method

.method private static native writeSnapshot_native(ILjava/io/FileDescriptor;)I
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    :try_start_0
    iget v0, p0, Landroid/app/backup/FileBackupHelperBase;->mPtr:I

    #@2
    invoke-static {v0}, Landroid/app/backup/FileBackupHelperBase;->dtor(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 45
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 47
    return-void

    #@9
    .line 45
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method isKeyInList(Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 8
    .parameter "key"
    .parameter "list"

    #@0
    .prologue
    .line 110
    move-object v0, p2

    #@1
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@2
    .local v2, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_12

    #@5
    aget-object v3, v0, v1

    #@7
    .line 111
    .local v3, s:Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_f

    #@d
    .line 112
    const/4 v4, 0x1

    #@e
    .line 115
    .end local v3           #s:Ljava/lang/String;
    :goto_e
    return v4

    #@f
    .line 110
    .restart local v3       #s:Ljava/lang/String;
    :cond_f
    add-int/lit8 v1, v1, 0x1

    #@11
    goto :goto_3

    #@12
    .line 115
    .end local v3           #s:Ljava/lang/String;
    :cond_12
    const/4 v4, 0x0

    #@13
    goto :goto_e
.end method

.method writeFile(Ljava/io/File;Landroid/app/backup/BackupDataInputStream;)Z
    .registers 9
    .parameter "f"
    .parameter "in"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 85
    const/4 v1, -0x1

    #@2
    .line 88
    .local v1, result:I
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    .line 89
    .local v0, parent:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@9
    .line 91
    iget v3, p0, Landroid/app/backup/FileBackupHelperBase;->mPtr:I

    #@b
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    iget-object v5, p2, Landroid/app/backup/BackupDataInputStream;->mData:Landroid/app/backup/BackupDataInput;

    #@11
    iget v5, v5, Landroid/app/backup/BackupDataInput;->mBackupReader:I

    #@13
    invoke-static {v3, v4, v5}, Landroid/app/backup/FileBackupHelperBase;->writeFile_native(ILjava/lang/String;I)I

    #@16
    move-result v1

    #@17
    .line 92
    if-eqz v1, :cond_55

    #@19
    .line 94
    iget-boolean v3, p0, Landroid/app/backup/FileBackupHelperBase;->mExceptionLogged:Z

    #@1b
    if-nez v3, :cond_55

    #@1d
    .line 95
    const-string v3, "FileBackupHelperBase"

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, "Failed restoring file \'"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, "\' for app \'"

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    iget-object v5, p0, Landroid/app/backup/FileBackupHelperBase;->mContext:Landroid/content/Context;

    #@36
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    const-string v5, "\' result=0x"

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 98
    iput-boolean v2, p0, Landroid/app/backup/FileBackupHelperBase;->mExceptionLogged:Z

    #@55
    .line 101
    :cond_55
    if-nez v1, :cond_58

    #@57
    :goto_57
    return v2

    #@58
    :cond_58
    const/4 v2, 0x0

    #@59
    goto :goto_57
.end method

.method public writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V
    .registers 5
    .parameter "fd"

    #@0
    .prologue
    .line 105
    iget v1, p0, Landroid/app/backup/FileBackupHelperBase;->mPtr:I

    #@2
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@5
    move-result-object v2

    #@6
    invoke-static {v1, v2}, Landroid/app/backup/FileBackupHelperBase;->writeSnapshot_native(ILjava/io/FileDescriptor;)I

    #@9
    move-result v0

    #@a
    .line 107
    .local v0, result:I
    return-void
.end method
