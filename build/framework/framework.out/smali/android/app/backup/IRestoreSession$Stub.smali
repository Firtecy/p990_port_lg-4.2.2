.class public abstract Landroid/app/backup/IRestoreSession$Stub;
.super Landroid/os/Binder;
.source "IRestoreSession.java"

# interfaces
.implements Landroid/app/backup/IRestoreSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/backup/IRestoreSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/backup/IRestoreSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.backup.IRestoreSession"

.field static final TRANSACTION_endRestoreSession:I = 0x5

.field static final TRANSACTION_getAvailableRestoreSets:I = 0x1

.field static final TRANSACTION_restoreAll:I = 0x2

.field static final TRANSACTION_restorePackage:I = 0x4

.field static final TRANSACTION_restoreSome:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.app.backup.IRestoreSession"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/backup/IRestoreSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/backup/IRestoreSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.app.backup.IRestoreSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/backup/IRestoreSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/app/backup/IRestoreSession;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/app/backup/IRestoreSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/backup/IRestoreSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 44
    sparse-switch p1, :sswitch_data_8c

    #@4
    .line 107
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v5

    #@8
    :goto_8
    return v5

    #@9
    .line 48
    :sswitch_9
    const-string v6, "android.app.backup.IRestoreSession"

    #@b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 53
    :sswitch_f
    const-string v6, "android.app.backup.IRestoreSession"

    #@11
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v6

    #@18
    invoke-static {v6}, Landroid/app/backup/IRestoreObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IRestoreObserver;

    #@1b
    move-result-object v0

    #@1c
    .line 56
    .local v0, _arg0:Landroid/app/backup/IRestoreObserver;
    invoke-virtual {p0, v0}, Landroid/app/backup/IRestoreSession$Stub;->getAvailableRestoreSets(Landroid/app/backup/IRestoreObserver;)I

    #@1f
    move-result v4

    #@20
    .line 57
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 58
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    goto :goto_8

    #@27
    .line 63
    .end local v0           #_arg0:Landroid/app/backup/IRestoreObserver;
    .end local v4           #_result:I
    :sswitch_27
    const-string v6, "android.app.backup.IRestoreSession"

    #@29
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v0

    #@30
    .line 67
    .local v0, _arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@33
    move-result-object v6

    #@34
    invoke-static {v6}, Landroid/app/backup/IRestoreObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IRestoreObserver;

    #@37
    move-result-object v2

    #@38
    .line 68
    .local v2, _arg1:Landroid/app/backup/IRestoreObserver;
    invoke-virtual {p0, v0, v1, v2}, Landroid/app/backup/IRestoreSession$Stub;->restoreAll(JLandroid/app/backup/IRestoreObserver;)I

    #@3b
    move-result v4

    #@3c
    .line 69
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    .line 70
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@42
    goto :goto_8

    #@43
    .line 75
    .end local v0           #_arg0:J
    .end local v2           #_arg1:Landroid/app/backup/IRestoreObserver;
    .end local v4           #_result:I
    :sswitch_43
    const-string v6, "android.app.backup.IRestoreSession"

    #@45
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48
    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@4b
    move-result-wide v0

    #@4c
    .line 79
    .restart local v0       #_arg0:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4f
    move-result-object v6

    #@50
    invoke-static {v6}, Landroid/app/backup/IRestoreObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IRestoreObserver;

    #@53
    move-result-object v2

    #@54
    .line 81
    .restart local v2       #_arg1:Landroid/app/backup/IRestoreObserver;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    .line 82
    .local v3, _arg2:[Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/app/backup/IRestoreSession$Stub;->restoreSome(JLandroid/app/backup/IRestoreObserver;[Ljava/lang/String;)I

    #@5b
    move-result v4

    #@5c
    .line 83
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5f
    .line 84
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@62
    goto :goto_8

    #@63
    .line 89
    .end local v0           #_arg0:J
    .end local v2           #_arg1:Landroid/app/backup/IRestoreObserver;
    .end local v3           #_arg2:[Ljava/lang/String;
    .end local v4           #_result:I
    :sswitch_63
    const-string v6, "android.app.backup.IRestoreSession"

    #@65
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6b
    move-result-object v0

    #@6c
    .line 93
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@6f
    move-result-object v6

    #@70
    invoke-static {v6}, Landroid/app/backup/IRestoreObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IRestoreObserver;

    #@73
    move-result-object v2

    #@74
    .line 94
    .restart local v2       #_arg1:Landroid/app/backup/IRestoreObserver;
    invoke-virtual {p0, v0, v2}, Landroid/app/backup/IRestoreSession$Stub;->restorePackage(Ljava/lang/String;Landroid/app/backup/IRestoreObserver;)I

    #@77
    move-result v4

    #@78
    .line 95
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7b
    .line 96
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@7e
    goto :goto_8

    #@7f
    .line 101
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Landroid/app/backup/IRestoreObserver;
    .end local v4           #_result:I
    :sswitch_7f
    const-string v6, "android.app.backup.IRestoreSession"

    #@81
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@84
    .line 102
    invoke-virtual {p0}, Landroid/app/backup/IRestoreSession$Stub;->endRestoreSession()V

    #@87
    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8a
    goto/16 :goto_8

    #@8c
    .line 44
    :sswitch_data_8c
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_27
        0x3 -> :sswitch_43
        0x4 -> :sswitch_63
        0x5 -> :sswitch_7f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
