.class public Landroid/app/backup/RestoreSession;
.super Ljava/lang/Object;
.source "RestoreSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/backup/RestoreSession$RestoreObserverWrapper;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "RestoreSession"


# instance fields
.field mBinder:Landroid/app/backup/IRestoreSession;

.field final mContext:Landroid/content/Context;

.field mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/app/backup/IRestoreSession;)V
    .registers 4
    .parameter "context"
    .parameter "binder"

    #@0
    .prologue
    .line 174
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@6
    .line 175
    iput-object p1, p0, Landroid/app/backup/RestoreSession;->mContext:Landroid/content/Context;

    #@8
    .line 176
    iput-object p2, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@a
    .line 177
    return-void
.end method


# virtual methods
.method public endRestoreSession()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 162
    :try_start_1
    iget-object v1, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@3
    invoke-interface {v1}, Landroid/app/backup/IRestoreSession;->endRestoreSession()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_12
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_9

    #@6
    .line 166
    :goto_6
    iput-object v3, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@8
    .line 168
    return-void

    #@9
    .line 163
    :catch_9
    move-exception v0

    #@a
    .line 164
    .local v0, e:Landroid/os/RemoteException;
    :try_start_a
    const-string v1, "RestoreSession"

    #@c
    const-string v2, "Can\'t contact server to get available sets"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_a .. :try_end_11} :catchall_12

    #@11
    goto :goto_6

    #@12
    .line 166
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_12
    move-exception v1

    #@13
    iput-object v3, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@15
    throw v1
.end method

.method public getAvailableRestoreSets(Landroid/app/backup/RestoreObserver;)I
    .registers 7
    .parameter "observer"

    #@0
    .prologue
    .line 51
    const/4 v1, -0x1

    #@1
    .line 52
    .local v1, err:I
    new-instance v2, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@3
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mContext:Landroid/content/Context;

    #@5
    invoke-direct {v2, p0, v3, p1}, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;-><init>(Landroid/app/backup/RestoreSession;Landroid/content/Context;Landroid/app/backup/RestoreObserver;)V

    #@8
    .line 54
    .local v2, obsWrapper:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;
    :try_start_8
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@a
    invoke-interface {v3, v2}, Landroid/app/backup/IRestoreSession;->getAvailableRestoreSets(Landroid/app/backup/IRestoreObserver;)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 58
    :goto_e
    return v1

    #@f
    .line 55
    :catch_f
    move-exception v0

    #@10
    .line 56
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "RestoreSession"

    #@12
    const-string v4, "Can\'t contact server to get available sets"

    #@14
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    goto :goto_e
.end method

.method public restoreAll(JLandroid/app/backup/RestoreObserver;)I
    .registers 8
    .parameter "token"
    .parameter "observer"

    #@0
    .prologue
    .line 75
    const/4 v1, -0x1

    #@1
    .line 76
    .local v1, err:I
    iget-object v2, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@3
    if-eqz v2, :cond_f

    #@5
    .line 77
    const-string v2, "RestoreSession"

    #@7
    const-string/jumbo v3, "restoreAll() called during active restore"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 78
    const/4 v2, -0x1

    #@e
    .line 86
    :goto_e
    return v2

    #@f
    .line 80
    :cond_f
    new-instance v2, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@11
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mContext:Landroid/content/Context;

    #@13
    invoke-direct {v2, p0, v3, p3}, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;-><init>(Landroid/app/backup/RestoreSession;Landroid/content/Context;Landroid/app/backup/RestoreObserver;)V

    #@16
    iput-object v2, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@18
    .line 82
    :try_start_18
    iget-object v2, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@1a
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@1c
    invoke-interface {v2, p1, p2, v3}, Landroid/app/backup/IRestoreSession;->restoreAll(JLandroid/app/backup/IRestoreObserver;)I
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1f} :catch_22

    #@1f
    move-result v1

    #@20
    :goto_20
    move v2, v1

    #@21
    .line 86
    goto :goto_e

    #@22
    .line 83
    :catch_22
    move-exception v0

    #@23
    .line 84
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "RestoreSession"

    #@25
    const-string v3, "Can\'t contact server to restore"

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_20
.end method

.method public restorePackage(Ljava/lang/String;Landroid/app/backup/RestoreObserver;)I
    .registers 7
    .parameter "packageName"
    .parameter "observer"

    #@0
    .prologue
    .line 139
    const/4 v1, -0x1

    #@1
    .line 140
    .local v1, err:I
    iget-object v2, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@3
    if-eqz v2, :cond_f

    #@5
    .line 141
    const-string v2, "RestoreSession"

    #@7
    const-string/jumbo v3, "restorePackage() called during active restore"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 142
    const/4 v2, -0x1

    #@e
    .line 150
    :goto_e
    return v2

    #@f
    .line 144
    :cond_f
    new-instance v2, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@11
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mContext:Landroid/content/Context;

    #@13
    invoke-direct {v2, p0, v3, p2}, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;-><init>(Landroid/app/backup/RestoreSession;Landroid/content/Context;Landroid/app/backup/RestoreObserver;)V

    #@16
    iput-object v2, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@18
    .line 146
    :try_start_18
    iget-object v2, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@1a
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@1c
    invoke-interface {v2, p1, v3}, Landroid/app/backup/IRestoreSession;->restorePackage(Ljava/lang/String;Landroid/app/backup/IRestoreObserver;)I
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1f} :catch_22

    #@1f
    move-result v1

    #@20
    :goto_20
    move v2, v1

    #@21
    .line 150
    goto :goto_e

    #@22
    .line 147
    :catch_22
    move-exception v0

    #@23
    .line 148
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "RestoreSession"

    #@25
    const-string v3, "Can\'t contact server to restore package"

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_20
.end method

.method public restoreSome(JLandroid/app/backup/RestoreObserver;[Ljava/lang/String;)I
    .registers 9
    .parameter "token"
    .parameter "observer"
    .parameter "packages"

    #@0
    .prologue
    .line 109
    const/4 v1, -0x1

    #@1
    .line 110
    .local v1, err:I
    iget-object v2, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@3
    if-eqz v2, :cond_f

    #@5
    .line 111
    const-string v2, "RestoreSession"

    #@7
    const-string/jumbo v3, "restoreAll() called during active restore"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 112
    const/4 v2, -0x1

    #@e
    .line 120
    :goto_e
    return v2

    #@f
    .line 114
    :cond_f
    new-instance v2, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@11
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mContext:Landroid/content/Context;

    #@13
    invoke-direct {v2, p0, v3, p3}, Landroid/app/backup/RestoreSession$RestoreObserverWrapper;-><init>(Landroid/app/backup/RestoreSession;Landroid/content/Context;Landroid/app/backup/RestoreObserver;)V

    #@16
    iput-object v2, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@18
    .line 116
    :try_start_18
    iget-object v2, p0, Landroid/app/backup/RestoreSession;->mBinder:Landroid/app/backup/IRestoreSession;

    #@1a
    iget-object v3, p0, Landroid/app/backup/RestoreSession;->mObserver:Landroid/app/backup/RestoreSession$RestoreObserverWrapper;

    #@1c
    invoke-interface {v2, p1, p2, v3, p4}, Landroid/app/backup/IRestoreSession;->restoreSome(JLandroid/app/backup/IRestoreObserver;[Ljava/lang/String;)I
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1f} :catch_22

    #@1f
    move-result v1

    #@20
    :goto_20
    move v2, v1

    #@21
    .line 120
    goto :goto_e

    #@22
    .line 117
    :catch_22
    move-exception v0

    #@23
    .line 118
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "RestoreSession"

    #@25
    const-string v3, "Can\'t contact server to restore packages"

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_20
.end method
