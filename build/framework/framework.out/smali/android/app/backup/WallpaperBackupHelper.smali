.class public Landroid/app/backup/WallpaperBackupHelper;
.super Landroid/app/backup/FileBackupHelperBase;
.source "WallpaperBackupHelper.java"

# interfaces
.implements Landroid/app/backup/BackupHelper;


# static fields
.field private static final DEBUG:Z = false

.field private static final STAGE_FILE:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "WallpaperBackupHelper"

.field public static final WALLPAPER_IMAGE:Ljava/lang/String; = null

.field public static final WALLPAPER_IMAGE_KEY:Ljava/lang/String; = "/data/data/com.android.settings/files/wallpaper"

.field public static final WALLPAPER_INFO:Ljava/lang/String; = null

.field public static final WALLPAPER_INFO_KEY:Ljava/lang/String; = "/data/system/wallpaper_info.xml"


# instance fields
.field mContext:Landroid/content/Context;

.field mDesiredMinHeight:D

.field mDesiredMinWidth:D

.field mFiles:[Ljava/lang/String;

.field mKeys:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 44
    new-instance v0, Ljava/io/File;

    #@3
    invoke-static {v3}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@6
    move-result-object v1

    #@7
    const-string/jumbo v2, "wallpaper"

    #@a
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@d
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    sput-object v0, Landroid/app/backup/WallpaperBackupHelper;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@13
    .line 47
    new-instance v0, Ljava/io/File;

    #@15
    invoke-static {v3}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@18
    move-result-object v1

    #@19
    const-string/jumbo v2, "wallpaper_info.xml"

    #@1c
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1f
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    sput-object v0, Landroid/app/backup/WallpaperBackupHelper;->WALLPAPER_INFO:Ljava/lang/String;

    #@25
    .line 59
    new-instance v0, Ljava/io/File;

    #@27
    invoke-static {v3}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@2a
    move-result-object v1

    #@2b
    const-string/jumbo v2, "wallpaper-tmp"

    #@2e
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@31
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    sput-object v0, Landroid/app/backup/WallpaperBackupHelper;->STAGE_FILE:Ljava/lang/String;

    #@37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 12
    .parameter "context"
    .parameter "files"
    .parameter "keys"

    #@0
    .prologue
    const-wide/16 v6, 0x0

    #@2
    .line 77
    invoke-direct {p0, p1}, Landroid/app/backup/FileBackupHelperBase;-><init>(Landroid/content/Context;)V

    #@5
    .line 79
    iput-object p1, p0, Landroid/app/backup/WallpaperBackupHelper;->mContext:Landroid/content/Context;

    #@7
    .line 80
    iput-object p2, p0, Landroid/app/backup/WallpaperBackupHelper;->mFiles:[Ljava/lang/String;

    #@9
    .line 81
    iput-object p3, p0, Landroid/app/backup/WallpaperBackupHelper;->mKeys:[Ljava/lang/String;

    #@b
    .line 84
    const-string/jumbo v4, "wallpaper"

    #@e
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Landroid/app/WallpaperManager;

    #@14
    .line 85
    .local v3, wpm:Landroid/app/WallpaperManager;
    invoke-virtual {v3}, Landroid/app/WallpaperManager;->getDesiredMinimumWidth()I

    #@17
    move-result v4

    #@18
    int-to-double v4, v4

    #@19
    iput-wide v4, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinWidth:D

    #@1b
    .line 86
    invoke-virtual {v3}, Landroid/app/WallpaperManager;->getDesiredMinimumHeight()I

    #@1e
    move-result v4

    #@1f
    int-to-double v4, v4

    #@20
    iput-wide v4, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinHeight:D

    #@22
    .line 88
    iget-wide v4, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinWidth:D

    #@24
    cmpg-double v4, v4, v6

    #@26
    if-lez v4, :cond_2e

    #@28
    iget-wide v4, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinHeight:D

    #@2a
    cmpg-double v4, v4, v6

    #@2c
    if-gtz v4, :cond_4d

    #@2e
    .line 89
    :cond_2e
    const-string/jumbo v4, "window"

    #@31
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@34
    move-result-object v2

    #@35
    check-cast v2, Landroid/view/WindowManager;

    #@37
    .line 90
    .local v2, wm:Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@3a
    move-result-object v0

    #@3b
    .line 91
    .local v0, d:Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    #@3d
    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    #@40
    .line 92
    .local v1, size:Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    #@43
    .line 93
    iget v4, v1, Landroid/graphics/Point;->x:I

    #@45
    int-to-double v4, v4

    #@46
    iput-wide v4, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinWidth:D

    #@48
    .line 94
    iget v4, v1, Landroid/graphics/Point;->y:I

    #@4a
    int-to-double v4, v4

    #@4b
    iput-wide v4, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinHeight:D

    #@4d
    .line 100
    .end local v0           #d:Landroid/view/Display;
    .end local v1           #size:Landroid/graphics/Point;
    .end local v2           #wm:Landroid/view/WindowManager;
    :cond_4d
    return-void
.end method


# virtual methods
.method public performBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .registers 6
    .parameter "oldState"
    .parameter "data"
    .parameter "newState"

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/app/backup/WallpaperBackupHelper;->mFiles:[Ljava/lang/String;

    #@2
    iget-object v1, p0, Landroid/app/backup/WallpaperBackupHelper;->mKeys:[Ljava/lang/String;

    #@4
    invoke-static {p1, p2, p3, v0, v1}, Landroid/app/backup/WallpaperBackupHelper;->performBackup_checked(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;[Ljava/lang/String;[Ljava/lang/String;)V

    #@7
    .line 110
    return-void
.end method

.method public restoreEntity(Landroid/app/backup/BackupDataInputStream;)V
    .registers 13
    .parameter "data"

    #@0
    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/app/backup/BackupDataInputStream;->getKey()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 119
    .local v3, key:Ljava/lang/String;
    iget-object v7, p0, Landroid/app/backup/WallpaperBackupHelper;->mKeys:[Ljava/lang/String;

    #@6
    invoke-virtual {p0, v3, v7}, Landroid/app/backup/WallpaperBackupHelper;->isKeyInList(Ljava/lang/String;[Ljava/lang/String;)Z

    #@9
    move-result v7

    #@a
    if-eqz v7, :cond_64

    #@c
    .line 120
    const-string v7, "/data/data/com.android.settings/files/wallpaper"

    #@e
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v7

    #@12
    if-eqz v7, :cond_69

    #@14
    .line 122
    new-instance v0, Ljava/io/File;

    #@16
    sget-object v7, Landroid/app/backup/WallpaperBackupHelper;->STAGE_FILE:Ljava/lang/String;

    #@18
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1b
    .line 123
    .local v0, f:Ljava/io/File;
    invoke-virtual {p0, v0, p1}, Landroid/app/backup/WallpaperBackupHelper;->writeFile(Ljava/io/File;Landroid/app/backup/BackupDataInputStream;)Z

    #@1e
    move-result v7

    #@1f
    if-eqz v7, :cond_64

    #@21
    .line 126
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    #@23
    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@26
    .line 127
    .local v4, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x1

    #@27
    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    #@29
    .line 128
    sget-object v7, Landroid/app/backup/WallpaperBackupHelper;->STAGE_FILE:Ljava/lang/String;

    #@2b
    invoke-static {v7, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@2e
    .line 139
    iget-wide v7, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinWidth:D

    #@30
    iget v9, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    #@32
    int-to-double v9, v9

    #@33
    div-double v5, v7, v9

    #@35
    .line 140
    .local v5, widthRatio:D
    iget-wide v7, p0, Landroid/app/backup/WallpaperBackupHelper;->mDesiredMinHeight:D

    #@37
    iget v9, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    #@39
    int-to-double v9, v9

    #@3a
    div-double v1, v7, v9

    #@3c
    .line 141
    .local v1, heightRatio:D
    const-wide/16 v7, 0x0

    #@3e
    cmpl-double v7, v5, v7

    #@40
    if-lez v7, :cond_65

    #@42
    const-wide v7, 0x3ff547ae147ae148L

    #@47
    cmpg-double v7, v5, v7

    #@49
    if-gez v7, :cond_65

    #@4b
    const-wide/16 v7, 0x0

    #@4d
    cmpl-double v7, v1, v7

    #@4f
    if-lez v7, :cond_65

    #@51
    const-wide v7, 0x3ff547ae147ae148L

    #@56
    cmpg-double v7, v1, v7

    #@58
    if-gez v7, :cond_65

    #@5a
    .line 145
    new-instance v7, Ljava/io/File;

    #@5c
    sget-object v8, Landroid/app/backup/WallpaperBackupHelper;->WALLPAPER_IMAGE:Ljava/lang/String;

    #@5e
    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@61
    invoke-virtual {v0, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@64
    .line 161
    .end local v0           #f:Ljava/io/File;
    .end local v1           #heightRatio:D
    .end local v4           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v5           #widthRatio:D
    :cond_64
    :goto_64
    return-void

    #@65
    .line 152
    .restart local v0       #f:Ljava/io/File;
    .restart local v1       #heightRatio:D
    .restart local v4       #options:Landroid/graphics/BitmapFactory$Options;
    .restart local v5       #widthRatio:D
    :cond_65
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@68
    goto :goto_64

    #@69
    .line 155
    .end local v0           #f:Ljava/io/File;
    .end local v1           #heightRatio:D
    .end local v4           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v5           #widthRatio:D
    :cond_69
    const-string v7, "/data/system/wallpaper_info.xml"

    #@6b
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v7

    #@6f
    if-eqz v7, :cond_64

    #@71
    .line 157
    new-instance v0, Ljava/io/File;

    #@73
    sget-object v7, Landroid/app/backup/WallpaperBackupHelper;->WALLPAPER_INFO:Ljava/lang/String;

    #@75
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@78
    .line 158
    .restart local v0       #f:Ljava/io/File;
    invoke-virtual {p0, v0, p1}, Landroid/app/backup/WallpaperBackupHelper;->writeFile(Ljava/io/File;Landroid/app/backup/BackupDataInputStream;)Z

    #@7b
    goto :goto_64
.end method

.method public bridge synthetic writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/backup/FileBackupHelperBase;->writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V

    #@3
    return-void
.end method
