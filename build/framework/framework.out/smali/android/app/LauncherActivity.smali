.class public abstract Landroid/app/LauncherActivity;
.super Landroid/app/ListActivity;
.source "LauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/LauncherActivity$IconResizer;,
        Landroid/app/LauncherActivity$ActivityAdapter;,
        Landroid/app/LauncherActivity$ListItem;
    }
.end annotation


# instance fields
.field mIconResizer:Landroid/app/LauncherActivity$IconResizer;

.field mIntent:Landroid/content/Intent;

.field mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    #@3
    .line 248
    return-void
.end method

.method private updateAlertTitle()V
    .registers 3

    #@0
    .prologue
    .line 364
    const v1, 0x1020267

    #@3
    invoke-virtual {p0, v1}, Landroid/app/LauncherActivity;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/TextView;

    #@9
    .line 365
    .local v0, alertTitle:Landroid/widget/TextView;
    if-eqz v0, :cond_12

    #@b
    .line 366
    invoke-virtual {p0}, Landroid/app/LauncherActivity;->getTitle()Ljava/lang/CharSequence;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@12
    .line 368
    :cond_12
    return-void
.end method

.method private updateButtonText()V
    .registers 3

    #@0
    .prologue
    .line 371
    const v1, 0x1020019

    #@3
    invoke-virtual {p0, v1}, Landroid/app/LauncherActivity;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/Button;

    #@9
    .line 372
    .local v0, cancelButton:Landroid/widget/Button;
    if-eqz v0, :cond_13

    #@b
    .line 373
    new-instance v1, Landroid/app/LauncherActivity$1;

    #@d
    invoke-direct {v1, p0}, Landroid/app/LauncherActivity$1;-><init>(Landroid/app/LauncherActivity;)V

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@13
    .line 379
    :cond_13
    return-void
.end method


# virtual methods
.method protected getTargetIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 432
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    return-object v0
.end method

.method protected intentForPosition(I)Landroid/content/Intent;
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    check-cast v0, Landroid/app/LauncherActivity$ActivityAdapter;

    #@4
    .line 414
    .local v0, adapter:Landroid/app/LauncherActivity$ActivityAdapter;
    invoke-virtual {v0, p1}, Landroid/app/LauncherActivity$ActivityAdapter;->intentForPosition(I)Landroid/content/Intent;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method protected itemForPosition(I)Landroid/app/LauncherActivity$ListItem;
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 423
    iget-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    check-cast v0, Landroid/app/LauncherActivity$ActivityAdapter;

    #@4
    .line 424
    .local v0, adapter:Landroid/app/LauncherActivity$ActivityAdapter;
    invoke-virtual {v0, p1}, Landroid/app/LauncherActivity$ActivityAdapter;->itemForPosition(I)Landroid/app/LauncherActivity$ListItem;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public makeListItems()Ljava/util/List;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/LauncherActivity$ListItem;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 455
    iget-object v5, p0, Landroid/app/LauncherActivity;->mIntent:Landroid/content/Intent;

    #@2
    invoke-virtual {p0, v5}, Landroid/app/LauncherActivity;->onQueryPackageManager(Landroid/content/Intent;)Ljava/util/List;

    #@5
    move-result-object v1

    #@6
    .line 456
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {p0, v1}, Landroid/app/LauncherActivity;->onSortResultList(Ljava/util/List;)V

    #@9
    .line 458
    new-instance v4, Ljava/util/ArrayList;

    #@b
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@e
    move-result v5

    #@f
    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    #@12
    .line 459
    .local v4, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/LauncherActivity$ListItem;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@15
    move-result v2

    #@16
    .line 460
    .local v2, listSize:I
    const/4 v0, 0x0

    #@17
    .local v0, i:I
    :goto_17
    if-ge v0, v2, :cond_2d

    #@19
    .line 461
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@1f
    .line 462
    .local v3, resolveInfo:Landroid/content/pm/ResolveInfo;
    new-instance v5, Landroid/app/LauncherActivity$ListItem;

    #@21
    iget-object v6, p0, Landroid/app/LauncherActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    #@23
    const/4 v7, 0x0

    #@24
    invoke-direct {v5, v6, v3, v7}, Landroid/app/LauncherActivity$ListItem;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Landroid/app/LauncherActivity$IconResizer;)V

    #@27
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 460
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_17

    #@2d
    .line 465
    .end local v3           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_2d
    return-object v4
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "icicle"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 340
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 342
    invoke-virtual {p0}, Landroid/app/LauncherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/app/LauncherActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    #@a
    .line 344
    const/4 v0, 0x5

    #@b
    invoke-virtual {p0, v0}, Landroid/app/LauncherActivity;->requestWindowFeature(I)Z

    #@e
    .line 345
    invoke-virtual {p0, v2}, Landroid/app/LauncherActivity;->setProgressBarIndeterminateVisibility(Z)V

    #@11
    .line 346
    invoke-virtual {p0}, Landroid/app/LauncherActivity;->onSetContentView()V

    #@14
    .line 348
    new-instance v0, Landroid/app/LauncherActivity$IconResizer;

    #@16
    invoke-direct {v0, p0}, Landroid/app/LauncherActivity$IconResizer;-><init>(Landroid/app/LauncherActivity;)V

    #@19
    iput-object v0, p0, Landroid/app/LauncherActivity;->mIconResizer:Landroid/app/LauncherActivity$IconResizer;

    #@1b
    .line 350
    new-instance v0, Landroid/content/Intent;

    #@1d
    invoke-virtual {p0}, Landroid/app/LauncherActivity;->getTargetIntent()Landroid/content/Intent;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@24
    iput-object v0, p0, Landroid/app/LauncherActivity;->mIntent:Landroid/content/Intent;

    #@26
    .line 351
    iget-object v0, p0, Landroid/app/LauncherActivity;->mIntent:Landroid/content/Intent;

    #@28
    const/4 v1, 0x0

    #@29
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@2c
    .line 352
    new-instance v0, Landroid/app/LauncherActivity$ActivityAdapter;

    #@2e
    iget-object v1, p0, Landroid/app/LauncherActivity;->mIconResizer:Landroid/app/LauncherActivity$IconResizer;

    #@30
    invoke-direct {v0, p0, v1}, Landroid/app/LauncherActivity$ActivityAdapter;-><init>(Landroid/app/LauncherActivity;Landroid/app/LauncherActivity$IconResizer;)V

    #@33
    iput-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@35
    .line 354
    iget-object v0, p0, Landroid/app/ListActivity;->mAdapter:Landroid/widget/ListAdapter;

    #@37
    invoke-virtual {p0, v0}, Landroid/app/LauncherActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    #@3a
    .line 355
    invoke-virtual {p0}, Landroid/app/LauncherActivity;->getListView()Landroid/widget/ListView;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    #@41
    .line 357
    invoke-direct {p0}, Landroid/app/LauncherActivity;->updateAlertTitle()V

    #@44
    .line 358
    invoke-direct {p0}, Landroid/app/LauncherActivity;->updateButtonText()V

    #@47
    .line 360
    const/4 v0, 0x0

    #@48
    invoke-virtual {p0, v0}, Landroid/app/LauncherActivity;->setProgressBarIndeterminateVisibility(Z)V

    #@4b
    .line 361
    return-void
.end method

.method protected onEvaluateShowIcons()Z
    .registers 2

    #@0
    .prologue
    .line 474
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 7
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 403
    invoke-virtual {p0, p3}, Landroid/app/LauncherActivity;->intentForPosition(I)Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 404
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/app/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    #@7
    .line 405
    return-void
.end method

.method protected onQueryPackageManager(Landroid/content/Intent;)Ljava/util/List;
    .registers 4
    .parameter "queryIntent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 440
    iget-object v0, p0, Landroid/app/LauncherActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method protected onSetContentView()V
    .registers 2

    #@0
    .prologue
    .line 398
    const v0, 0x1090021

    #@3
    invoke-virtual {p0, v0}, Landroid/app/LauncherActivity;->setContentView(I)V

    #@6
    .line 399
    return-void
.end method

.method protected onSortResultList(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 447
    .local p1, results:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v0, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    #@2
    iget-object v1, p0, Landroid/app/LauncherActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    #@4
    invoke-direct {v0, v1}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    #@7
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@a
    .line 448
    return-void
.end method

.method public setTitle(I)V
    .registers 2
    .parameter "titleId"

    #@0
    .prologue
    .line 389
    invoke-super {p0, p1}, Landroid/app/ListActivity;->setTitle(I)V

    #@3
    .line 390
    invoke-direct {p0}, Landroid/app/LauncherActivity;->updateAlertTitle()V

    #@6
    .line 391
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 383
    invoke-super {p0, p1}, Landroid/app/ListActivity;->setTitle(Ljava/lang/CharSequence;)V

    #@3
    .line 384
    invoke-direct {p0}, Landroid/app/LauncherActivity;->updateAlertTitle()V

    #@6
    .line 385
    return-void
.end method
