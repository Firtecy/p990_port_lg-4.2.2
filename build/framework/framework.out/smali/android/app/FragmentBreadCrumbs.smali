.class public Landroid/app/FragmentBreadCrumbs;
.super Landroid/view/ViewGroup;
.source "FragmentBreadCrumbs.java"

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_GRAVITY:I = 0x800013


# instance fields
.field mActivity:Landroid/app/Activity;

.field mContainer:Landroid/widget/LinearLayout;

.field private mGravity:I

.field mInflater:Landroid/view/LayoutInflater;

.field mMaxVisible:I

.field private mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mParentClickListener:Landroid/view/View$OnClickListener;

.field mParentEntry:Landroid/app/BackStackRecord;

.field mTopEntry:Landroid/app/BackStackRecord;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/app/FragmentBreadCrumbs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 84
    const v0, 0x1030089

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/app/FragmentBreadCrumbs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 46
    const/4 v1, -0x1

    #@5
    iput v1, p0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    #@7
    .line 338
    new-instance v1, Landroid/app/FragmentBreadCrumbs$1;

    #@9
    invoke-direct {v1, p0}, Landroid/app/FragmentBreadCrumbs$1;-><init>(Landroid/app/FragmentBreadCrumbs;)V

    #@c
    iput-object v1, p0, Landroid/app/FragmentBreadCrumbs;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@e
    .line 90
    sget-object v1, Lcom/android/internal/R$styleable;->FragmentBreadCrumbs:[I

    #@10
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@13
    move-result-object v0

    #@14
    .line 93
    .local v0, a:Landroid/content/res/TypedArray;
    const v1, 0x800013

    #@17
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@1a
    move-result v1

    #@1b
    iput v1, p0, Landroid/app/FragmentBreadCrumbs;->mGravity:I

    #@1d
    .line 96
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@20
    .line 97
    return-void
.end method

.method static synthetic access$000(Landroid/app/FragmentBreadCrumbs;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mParentClickListener:Landroid/view/View$OnClickListener;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/app/FragmentBreadCrumbs;)Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    #@2
    return-object v0
.end method

.method private createBackStackEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/BackStackRecord;
    .registers 5
    .parameter "title"
    .parameter "shortTitle"

    #@0
    .prologue
    .line 155
    if-nez p1, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 161
    :goto_3
    return-object v0

    #@4
    .line 157
    :cond_4
    new-instance v0, Landroid/app/BackStackRecord;

    #@6
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    #@8
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/app/FragmentManagerImpl;

    #@e
    invoke-direct {v0, v1}, Landroid/app/BackStackRecord;-><init>(Landroid/app/FragmentManagerImpl;)V

    #@11
    .line 159
    .local v0, entry:Landroid/app/BackStackRecord;
    invoke-virtual {v0, p1}, Landroid/app/BackStackRecord;->setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    #@14
    .line 160
    invoke-virtual {v0, p2}, Landroid/app/BackStackRecord;->setBreadCrumbShortTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    #@17
    goto :goto_3
.end method

.method private getPreEntry(I)Landroid/app/FragmentManager$BackStackEntry;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 276
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 277
    if-nez p1, :cond_9

    #@6
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    #@8
    .line 279
    :goto_8
    return-object v0

    #@9
    .line 277
    :cond_9
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    #@b
    goto :goto_8

    #@c
    .line 279
    :cond_c
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    #@e
    goto :goto_8
.end method

.method private getPreEntryCount()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 265
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    #@4
    if-eqz v0, :cond_d

    #@6
    move v0, v1

    #@7
    :goto_7
    iget-object v3, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    #@9
    if-eqz v3, :cond_f

    #@b
    :goto_b
    add-int/2addr v0, v1

    #@c
    return v0

    #@d
    :cond_d
    move v0, v2

    #@e
    goto :goto_7

    #@f
    :cond_f
    move v1, v2

    #@10
    goto :goto_b
.end method


# virtual methods
.method public onBackStackChanged()V
    .registers 1

    #@0
    .prologue
    .line 257
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    #@3
    .line 258
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 17
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 178
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getChildCount()I

    #@3
    move-result v2

    #@4
    .line 179
    .local v2, childCount:I
    if-nez v2, :cond_7

    #@6
    .line 220
    :goto_6
    return-void

    #@7
    .line 183
    :cond_7
    const/4 v8, 0x0

    #@8
    invoke-virtual {p0, v8}, Landroid/app/FragmentBreadCrumbs;->getChildAt(I)Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 185
    .local v0, child:Landroid/view/View;
    iget v5, p0, Landroid/view/View;->mPaddingTop:I

    #@e
    .line 186
    .local v5, childTop:I
    iget v8, p0, Landroid/view/View;->mPaddingTop:I

    #@10
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@13
    move-result v9

    #@14
    add-int/2addr v8, v9

    #@15
    iget v9, p0, Landroid/view/View;->mPaddingBottom:I

    #@17
    sub-int v1, v8, v9

    #@19
    .line 191
    .local v1, childBottom:I
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getLayoutDirection()I

    #@1c
    move-result v7

    #@1d
    .line 192
    .local v7, layoutDirection:I
    iget v8, p0, Landroid/app/FragmentBreadCrumbs;->mGravity:I

    #@1f
    const v9, 0x800007

    #@22
    and-int v6, v8, v9

    #@24
    .line 193
    .local v6, horizontalGravity:I
    invoke-static {v6, v7}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@27
    move-result v8

    #@28
    sparse-switch v8, :sswitch_data_78

    #@2b
    .line 206
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@2d
    .line 207
    .local v3, childLeft:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@30
    move-result v8

    #@31
    add-int v4, v3, v8

    #@33
    .line 211
    .local v4, childRight:I
    :goto_33
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@35
    if-ge v3, v8, :cond_39

    #@37
    .line 212
    iget v3, p0, Landroid/view/View;->mPaddingLeft:I

    #@39
    .line 215
    :cond_39
    iget v8, p0, Landroid/view/View;->mRight:I

    #@3b
    iget v9, p0, Landroid/view/View;->mLeft:I

    #@3d
    sub-int/2addr v8, v9

    #@3e
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@40
    sub-int/2addr v8, v9

    #@41
    if-le v4, v8, :cond_4c

    #@43
    .line 216
    iget v8, p0, Landroid/view/View;->mRight:I

    #@45
    iget v9, p0, Landroid/view/View;->mLeft:I

    #@47
    sub-int/2addr v8, v9

    #@48
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@4a
    sub-int v4, v8, v9

    #@4c
    .line 219
    :cond_4c
    invoke-virtual {v0, v3, v5, v4, v1}, Landroid/view/View;->layout(IIII)V

    #@4f
    goto :goto_6

    #@50
    .line 195
    .end local v3           #childLeft:I
    .end local v4           #childRight:I
    :sswitch_50
    iget v8, p0, Landroid/view/View;->mRight:I

    #@52
    iget v9, p0, Landroid/view/View;->mLeft:I

    #@54
    sub-int/2addr v8, v9

    #@55
    iget v9, p0, Landroid/view/View;->mPaddingRight:I

    #@57
    sub-int v4, v8, v9

    #@59
    .line 196
    .restart local v4       #childRight:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@5c
    move-result v8

    #@5d
    sub-int v3, v4, v8

    #@5f
    .line 197
    .restart local v3       #childLeft:I
    goto :goto_33

    #@60
    .line 200
    .end local v3           #childLeft:I
    .end local v4           #childRight:I
    :sswitch_60
    iget v8, p0, Landroid/view/View;->mPaddingLeft:I

    #@62
    iget v9, p0, Landroid/view/View;->mRight:I

    #@64
    iget v10, p0, Landroid/view/View;->mLeft:I

    #@66
    sub-int/2addr v9, v10

    #@67
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@6a
    move-result v10

    #@6b
    sub-int/2addr v9, v10

    #@6c
    div-int/lit8 v9, v9, 0x2

    #@6e
    add-int v3, v8, v9

    #@70
    .line 201
    .restart local v3       #childLeft:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@73
    move-result v8

    #@74
    add-int v4, v3, v8

    #@76
    .line 202
    .restart local v4       #childRight:I
    goto :goto_33

    #@77
    .line 193
    nop

    #@78
    :sswitch_data_78
    .sparse-switch
        0x1 -> :sswitch_60
        0x5 -> :sswitch_50
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 224
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getChildCount()I

    #@3
    move-result v1

    #@4
    .line 226
    .local v1, count:I
    const/4 v3, 0x0

    #@5
    .line 227
    .local v3, maxHeight:I
    const/4 v4, 0x0

    #@6
    .line 228
    .local v4, maxWidth:I
    const/4 v5, 0x0

    #@7
    .line 231
    .local v5, measuredChildState:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v1, :cond_34

    #@a
    .line 232
    invoke-virtual {p0, v2}, Landroid/app/FragmentBreadCrumbs;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 233
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@11
    move-result v6

    #@12
    const/16 v7, 0x8

    #@14
    if-eq v6, v7, :cond_31

    #@16
    .line 234
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/FragmentBreadCrumbs;->measureChild(Landroid/view/View;II)V

    #@19
    .line 235
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    #@1c
    move-result v6

    #@1d
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    #@20
    move-result v4

    #@21
    .line 236
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    #@24
    move-result v6

    #@25
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@28
    move-result v3

    #@29
    .line 237
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredState()I

    #@2c
    move-result v6

    #@2d
    invoke-static {v5, v6}, Landroid/app/FragmentBreadCrumbs;->combineMeasuredStates(II)I

    #@30
    move-result v5

    #@31
    .line 231
    :cond_31
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_8

    #@34
    .line 243
    .end local v0           #child:Landroid/view/View;
    :cond_34
    iget v6, p0, Landroid/view/View;->mPaddingLeft:I

    #@36
    iget v7, p0, Landroid/view/View;->mPaddingRight:I

    #@38
    add-int/2addr v6, v7

    #@39
    add-int/2addr v4, v6

    #@3a
    .line 244
    iget v6, p0, Landroid/view/View;->mPaddingTop:I

    #@3c
    iget v7, p0, Landroid/view/View;->mPaddingBottom:I

    #@3e
    add-int/2addr v6, v7

    #@3f
    add-int/2addr v3, v6

    #@40
    .line 247
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getSuggestedMinimumHeight()I

    #@43
    move-result v6

    #@44
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@47
    move-result v3

    #@48
    .line 248
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getSuggestedMinimumWidth()I

    #@4b
    move-result v6

    #@4c
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    #@4f
    move-result v4

    #@50
    .line 250
    invoke-static {v4, p1, v5}, Landroid/app/FragmentBreadCrumbs;->resolveSizeAndState(III)I

    #@53
    move-result v6

    #@54
    shl-int/lit8 v7, v5, 0x10

    #@56
    invoke-static {v3, p2, v7}, Landroid/app/FragmentBreadCrumbs;->resolveSizeAndState(III)I

    #@59
    move-result v7

    #@5a
    invoke-virtual {p0, v6, v7}, Landroid/app/FragmentBreadCrumbs;->setMeasuredDimension(II)V

    #@5d
    .line 253
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .registers 5
    .parameter "a"

    #@0
    .prologue
    .line 104
    iput-object p1, p0, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    #@2
    .line 105
    const-string/jumbo v0, "layout_inflater"

    #@5
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/view/LayoutInflater;

    #@b
    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mInflater:Landroid/view/LayoutInflater;

    #@d
    .line 106
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mInflater:Landroid/view/LayoutInflater;

    #@f
    const v1, 0x1090043

    #@12
    const/4 v2, 0x0

    #@13
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/widget/LinearLayout;

    #@19
    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@1b
    .line 109
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@1d
    invoke-virtual {p0, v0}, Landroid/app/FragmentBreadCrumbs;->addView(Landroid/view/View;)V

    #@20
    .line 110
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    #@27
    .line 111
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    #@2a
    .line 112
    new-instance v0, Landroid/animation/LayoutTransition;

    #@2c
    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    #@2f
    invoke-virtual {p0, v0}, Landroid/app/FragmentBreadCrumbs;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    #@32
    .line 113
    return-void
.end method

.method public setMaxVisible(I)V
    .registers 4
    .parameter "visibleCrumbs"

    #@0
    .prologue
    .line 120
    const/4 v0, 0x1

    #@1
    if-ge p1, v0, :cond_c

    #@3
    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v1, "visibleCrumbs must be greater than zero"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 123
    :cond_c
    iput p1, p0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    #@e
    .line 124
    return-void
.end method

.method public setOnBreadCrumbClickListener(Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 151
    iput-object p1, p0, Landroid/app/FragmentBreadCrumbs;->mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    #@2
    .line 152
    return-void
.end method

.method public setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .registers 5
    .parameter "title"
    .parameter "shortTitle"
    .parameter "listener"

    #@0
    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Landroid/app/FragmentBreadCrumbs;->createBackStackEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/BackStackRecord;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    #@6
    .line 140
    iput-object p3, p0, Landroid/app/FragmentBreadCrumbs;->mParentClickListener:Landroid/view/View$OnClickListener;

    #@8
    .line 141
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    #@b
    .line 142
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "title"
    .parameter "shortTitle"

    #@0
    .prologue
    .line 170
    invoke-direct {p0, p1, p2}, Landroid/app/FragmentBreadCrumbs;->createBackStackEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/BackStackRecord;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    #@6
    .line 171
    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    #@9
    .line 172
    return-void
.end method

.method updateCrumbs()V
    .registers 22

    #@0
    .prologue
    .line 284
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    #@4
    move-object/from16 v18, v0

    #@6
    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@9
    move-result-object v6

    #@a
    .line 285
    .local v6, fm:Landroid/app/FragmentManager;
    invoke-virtual {v6}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    #@d
    move-result v11

    #@e
    .line 286
    .local v11, numEntries:I
    invoke-direct/range {p0 .. p0}, Landroid/app/FragmentBreadCrumbs;->getPreEntryCount()I

    #@11
    move-result v12

    #@12
    .line 287
    .local v12, numPreEntries:I
    move-object/from16 v0, p0

    #@14
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@16
    move-object/from16 v18, v0

    #@18
    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getChildCount()I

    #@1b
    move-result v13

    #@1c
    .line 288
    .local v13, numViews:I
    const/4 v7, 0x0

    #@1d
    .local v7, i:I
    :goto_1d
    add-int v18, v11, v12

    #@1f
    move/from16 v0, v18

    #@21
    if-ge v7, v0, :cond_b4

    #@23
    .line 289
    if-ge v7, v12, :cond_50

    #@25
    move-object/from16 v0, p0

    #@27
    invoke-direct {v0, v7}, Landroid/app/FragmentBreadCrumbs;->getPreEntry(I)Landroid/app/FragmentManager$BackStackEntry;

    #@2a
    move-result-object v4

    #@2b
    .line 292
    .local v4, bse:Landroid/app/FragmentManager$BackStackEntry;
    :goto_2b
    if-ge v7, v13, :cond_5a

    #@2d
    .line 293
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@31
    move-object/from16 v18, v0

    #@33
    move-object/from16 v0, v18

    #@35
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@38
    move-result-object v16

    #@39
    .line 294
    .local v16, v:Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@3c
    move-result-object v14

    #@3d
    .line 295
    .local v14, tag:Ljava/lang/Object;
    if-eq v14, v4, :cond_5a

    #@3f
    .line 296
    move v9, v7

    #@40
    .local v9, j:I
    :goto_40
    if-ge v9, v13, :cond_59

    #@42
    .line 297
    move-object/from16 v0, p0

    #@44
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@46
    move-object/from16 v18, v0

    #@48
    move-object/from16 v0, v18

    #@4a
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    #@4d
    .line 296
    add-int/lit8 v9, v9, 0x1

    #@4f
    goto :goto_40

    #@50
    .line 289
    .end local v4           #bse:Landroid/app/FragmentManager$BackStackEntry;
    .end local v9           #j:I
    .end local v14           #tag:Ljava/lang/Object;
    .end local v16           #v:Landroid/view/View;
    :cond_50
    sub-int v18, v7, v12

    #@52
    move/from16 v0, v18

    #@54
    invoke-virtual {v6, v0}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    #@57
    move-result-object v4

    #@58
    goto :goto_2b

    #@59
    .line 299
    .restart local v4       #bse:Landroid/app/FragmentManager$BackStackEntry;
    .restart local v9       #j:I
    .restart local v14       #tag:Ljava/lang/Object;
    .restart local v16       #v:Landroid/view/View;
    :cond_59
    move v13, v7

    #@5a
    .line 302
    .end local v9           #j:I
    .end local v14           #tag:Ljava/lang/Object;
    .end local v16           #v:Landroid/view/View;
    :cond_5a
    if-lt v7, v13, :cond_b0

    #@5c
    .line 303
    move-object/from16 v0, p0

    #@5e
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mInflater:Landroid/view/LayoutInflater;

    #@60
    move-object/from16 v18, v0

    #@62
    const v19, 0x1090042

    #@65
    const/16 v20, 0x0

    #@67
    move-object/from16 v0, v18

    #@69
    move/from16 v1, v19

    #@6b
    move-object/from16 v2, p0

    #@6d
    move/from16 v3, v20

    #@6f
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@72
    move-result-object v8

    #@73
    .line 306
    .local v8, item:Landroid/view/View;
    const v18, 0x1020016

    #@76
    move/from16 v0, v18

    #@78
    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@7b
    move-result-object v15

    #@7c
    check-cast v15, Landroid/widget/TextView;

    #@7e
    .line 307
    .local v15, text:Landroid/widget/TextView;
    invoke-interface {v4}, Landroid/app/FragmentManager$BackStackEntry;->getBreadCrumbTitle()Ljava/lang/CharSequence;

    #@81
    move-result-object v18

    #@82
    move-object/from16 v0, v18

    #@84
    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@87
    .line 308
    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    #@8a
    .line 309
    if-nez v7, :cond_9a

    #@8c
    .line 310
    const v18, 0x102024b

    #@8f
    move/from16 v0, v18

    #@91
    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@94
    move-result-object v18

    #@95
    const/16 v19, 0x8

    #@97
    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    #@9a
    .line 312
    :cond_9a
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@9e
    move-object/from16 v18, v0

    #@a0
    move-object/from16 v0, v18

    #@a2
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    #@a5
    .line 313
    move-object/from16 v0, p0

    #@a7
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mOnClickListener:Landroid/view/View$OnClickListener;

    #@a9
    move-object/from16 v18, v0

    #@ab
    move-object/from16 v0, v18

    #@ad
    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@b0
    .line 288
    .end local v8           #item:Landroid/view/View;
    .end local v15           #text:Landroid/widget/TextView;
    :cond_b0
    add-int/lit8 v7, v7, 0x1

    #@b2
    goto/16 :goto_1d

    #@b4
    .line 316
    .end local v4           #bse:Landroid/app/FragmentManager$BackStackEntry;
    :cond_b4
    add-int v17, v11, v12

    #@b6
    .line 317
    .local v17, viewI:I
    move-object/from16 v0, p0

    #@b8
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@ba
    move-object/from16 v18, v0

    #@bc
    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getChildCount()I

    #@bf
    move-result v13

    #@c0
    .line 318
    :goto_c0
    move/from16 v0, v17

    #@c2
    if-le v13, v0, :cond_d2

    #@c4
    .line 319
    move-object/from16 v0, p0

    #@c6
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@c8
    move-object/from16 v18, v0

    #@ca
    add-int/lit8 v19, v13, -0x1

    #@cc
    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    #@cf
    .line 320
    add-int/lit8 v13, v13, -0x1

    #@d1
    goto :goto_c0

    #@d2
    .line 323
    :cond_d2
    const/4 v7, 0x0

    #@d3
    :goto_d3
    if-ge v7, v13, :cond_13e

    #@d5
    .line 324
    move-object/from16 v0, p0

    #@d7
    iget-object v0, v0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    #@d9
    move-object/from16 v18, v0

    #@db
    move-object/from16 v0, v18

    #@dd
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@e0
    move-result-object v5

    #@e1
    .line 326
    .local v5, child:Landroid/view/View;
    const v18, 0x1020016

    #@e4
    move/from16 v0, v18

    #@e6
    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@e9
    move-result-object v19

    #@ea
    add-int/lit8 v18, v13, -0x1

    #@ec
    move/from16 v0, v18

    #@ee
    if-ge v7, v0, :cond_135

    #@f0
    const/16 v18, 0x1

    #@f2
    :goto_f2
    move-object/from16 v0, v19

    #@f4
    move/from16 v1, v18

    #@f6
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    #@f9
    .line 327
    move-object/from16 v0, p0

    #@fb
    iget v0, v0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    #@fd
    move/from16 v18, v0

    #@ff
    if-lez v18, :cond_132

    #@101
    .line 329
    move-object/from16 v0, p0

    #@103
    iget v0, v0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    #@105
    move/from16 v18, v0

    #@107
    sub-int v18, v13, v18

    #@109
    move/from16 v0, v18

    #@10b
    if-ge v7, v0, :cond_138

    #@10d
    const/16 v18, 0x8

    #@10f
    :goto_10f
    move/from16 v0, v18

    #@111
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    #@114
    .line 330
    const v18, 0x102024b

    #@117
    move/from16 v0, v18

    #@119
    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@11c
    move-result-object v10

    #@11d
    .line 332
    .local v10, leftIcon:Landroid/view/View;
    move-object/from16 v0, p0

    #@11f
    iget v0, v0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    #@121
    move/from16 v18, v0

    #@123
    sub-int v18, v13, v18

    #@125
    move/from16 v0, v18

    #@127
    if-le v7, v0, :cond_13b

    #@129
    if-eqz v7, :cond_13b

    #@12b
    const/16 v18, 0x0

    #@12d
    :goto_12d
    move/from16 v0, v18

    #@12f
    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    #@132
    .line 323
    .end local v10           #leftIcon:Landroid/view/View;
    :cond_132
    add-int/lit8 v7, v7, 0x1

    #@134
    goto :goto_d3

    #@135
    .line 326
    :cond_135
    const/16 v18, 0x0

    #@137
    goto :goto_f2

    #@138
    .line 329
    :cond_138
    const/16 v18, 0x0

    #@13a
    goto :goto_10f

    #@13b
    .line 332
    .restart local v10       #leftIcon:Landroid/view/View;
    :cond_13b
    const/16 v18, 0x8

    #@13d
    goto :goto_12d

    #@13e
    .line 336
    .end local v5           #child:Landroid/view/View;
    .end local v10           #leftIcon:Landroid/view/View;
    :cond_13e
    return-void
.end method
