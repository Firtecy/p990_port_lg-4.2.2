.class final Landroid/app/Activity$ManagedCursor;
.super Ljava/lang/Object;
.source "Activity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Activity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ManagedCursor"
.end annotation


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private mReleased:Z

.field private mUpdated:Z


# direct methods
.method constructor <init>(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 741
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 742
    iput-object p1, p0, Landroid/app/Activity$ManagedCursor;->mCursor:Landroid/database/Cursor;

    #@6
    .line 743
    iput-boolean v0, p0, Landroid/app/Activity$ManagedCursor;->mReleased:Z

    #@8
    .line 744
    iput-boolean v0, p0, Landroid/app/Activity$ManagedCursor;->mUpdated:Z

    #@a
    .line 745
    return-void
.end method

.method static synthetic access$100(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 740
    iget-object v0, p0, Landroid/app/Activity$ManagedCursor;->mCursor:Landroid/database/Cursor;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/app/Activity$ManagedCursor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 740
    iget-boolean v0, p0, Landroid/app/Activity$ManagedCursor;->mReleased:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Landroid/app/Activity$ManagedCursor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 740
    iput-boolean p1, p0, Landroid/app/Activity$ManagedCursor;->mReleased:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Landroid/app/Activity$ManagedCursor;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 740
    iget-boolean v0, p0, Landroid/app/Activity$ManagedCursor;->mUpdated:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/app/Activity$ManagedCursor;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 740
    iput-boolean p1, p0, Landroid/app/Activity$ManagedCursor;->mUpdated:Z

    #@2
    return p1
.end method
