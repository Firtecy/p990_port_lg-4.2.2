.class public abstract Landroid/app/IBackupAgent$Stub;
.super Landroid/os/Binder;
.source "IBackupAgent.java"

# interfaces
.implements Landroid/app/IBackupAgent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IBackupAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/IBackupAgent$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.IBackupAgent"

.field static final TRANSACTION_doBackup:I = 0x1

.field static final TRANSACTION_doFullBackup:I = 0x3

.field static final TRANSACTION_doRestore:I = 0x2

.field static final TRANSACTION_doRestoreFile:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 22
    const-string v0, "android.app.IBackupAgent"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/IBackupAgent$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/IBackupAgent;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 30
    if-nez p0, :cond_4

    #@2
    .line 31
    const/4 v0, 0x0

    #@3
    .line 37
    :goto_3
    return-object v0

    #@4
    .line 33
    :cond_4
    const-string v1, "android.app.IBackupAgent"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 34
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/IBackupAgent;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 35
    check-cast v0, Landroid/app/IBackupAgent;

    #@12
    goto :goto_3

    #@13
    .line 37
    :cond_13
    new-instance v0, Landroid/app/IBackupAgent$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/IBackupAgent$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 25
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 45
    sparse-switch p1, :sswitch_data_11a

    #@3
    .line 156
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 49
    :sswitch_8
    const-string v1, "android.app.IBackupAgent"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 50
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 54
    :sswitch_11
    const-string v1, "android.app.IBackupAgent"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 56
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_5b

    #@1e
    .line 57
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    move-object/from16 v0, p2

    #@22
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/os/ParcelFileDescriptor;

    #@28
    .line 63
    .local v2, _arg0:Landroid/os/ParcelFileDescriptor;
    :goto_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_5d

    #@2e
    .line 64
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@30
    move-object/from16 v0, p2

    #@32
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@35
    move-result-object v3

    #@36
    check-cast v3, Landroid/os/ParcelFileDescriptor;

    #@38
    .line 70
    .local v3, _arg1:Landroid/os/ParcelFileDescriptor;
    :goto_38
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_5f

    #@3e
    .line 71
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    move-object/from16 v0, p2

    #@42
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@45
    move-result-object v4

    #@46
    check-cast v4, Landroid/os/ParcelFileDescriptor;

    #@48
    .line 77
    .local v4, _arg2:Landroid/os/ParcelFileDescriptor;
    :goto_48
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v5

    #@4c
    .line 79
    .local v5, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4f
    move-result-object v1

    #@50
    invoke-static {v1}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@53
    move-result-object v6

    #@54
    .local v6, _arg4:Landroid/app/backup/IBackupManager;
    move-object/from16 v1, p0

    #@56
    .line 80
    invoke-virtual/range {v1 .. v6}, Landroid/app/IBackupAgent$Stub;->doBackup(Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V

    #@59
    .line 81
    const/4 v1, 0x1

    #@5a
    goto :goto_7

    #@5b
    .line 60
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_arg1:Landroid/os/ParcelFileDescriptor;
    .end local v4           #_arg2:Landroid/os/ParcelFileDescriptor;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/app/backup/IBackupManager;
    :cond_5b
    const/4 v2, 0x0

    #@5c
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_28

    #@5d
    .line 67
    :cond_5d
    const/4 v3, 0x0

    #@5e
    .restart local v3       #_arg1:Landroid/os/ParcelFileDescriptor;
    goto :goto_38

    #@5f
    .line 74
    :cond_5f
    const/4 v4, 0x0

    #@60
    .restart local v4       #_arg2:Landroid/os/ParcelFileDescriptor;
    goto :goto_48

    #@61
    .line 85
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_arg1:Landroid/os/ParcelFileDescriptor;
    .end local v4           #_arg2:Landroid/os/ParcelFileDescriptor;
    :sswitch_61
    const-string v1, "android.app.IBackupAgent"

    #@63
    move-object/from16 v0, p2

    #@65
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68
    .line 87
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v1

    #@6c
    if-eqz v1, :cond_a0

    #@6e
    .line 88
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@70
    move-object/from16 v0, p2

    #@72
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@75
    move-result-object v2

    #@76
    check-cast v2, Landroid/os/ParcelFileDescriptor;

    #@78
    .line 94
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    :goto_78
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7b
    move-result v3

    #@7c
    .line 96
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v1

    #@80
    if-eqz v1, :cond_a2

    #@82
    .line 97
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@84
    move-object/from16 v0, p2

    #@86
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@89
    move-result-object v4

    #@8a
    check-cast v4, Landroid/os/ParcelFileDescriptor;

    #@8c
    .line 103
    .restart local v4       #_arg2:Landroid/os/ParcelFileDescriptor;
    :goto_8c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8f
    move-result v5

    #@90
    .line 105
    .restart local v5       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@93
    move-result-object v1

    #@94
    invoke-static {v1}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@97
    move-result-object v6

    #@98
    .restart local v6       #_arg4:Landroid/app/backup/IBackupManager;
    move-object/from16 v1, p0

    #@9a
    .line 106
    invoke-virtual/range {v1 .. v6}, Landroid/app/IBackupAgent$Stub;->doRestore(Landroid/os/ParcelFileDescriptor;ILandroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V

    #@9d
    .line 107
    const/4 v1, 0x1

    #@9e
    goto/16 :goto_7

    #@a0
    .line 91
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/os/ParcelFileDescriptor;
    .end local v5           #_arg3:I
    .end local v6           #_arg4:Landroid/app/backup/IBackupManager;
    :cond_a0
    const/4 v2, 0x0

    #@a1
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_78

    #@a2
    .line 100
    .restart local v3       #_arg1:I
    :cond_a2
    const/4 v4, 0x0

    #@a3
    .restart local v4       #_arg2:Landroid/os/ParcelFileDescriptor;
    goto :goto_8c

    #@a4
    .line 111
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/os/ParcelFileDescriptor;
    :sswitch_a4
    const-string v1, "android.app.IBackupAgent"

    #@a6
    move-object/from16 v0, p2

    #@a8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ab
    .line 113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v1

    #@af
    if-eqz v1, :cond_cf

    #@b1
    .line 114
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b3
    move-object/from16 v0, p2

    #@b5
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b8
    move-result-object v2

    #@b9
    check-cast v2, Landroid/os/ParcelFileDescriptor;

    #@bb
    .line 120
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    :goto_bb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@be
    move-result v3

    #@bf
    .line 122
    .restart local v3       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c2
    move-result-object v1

    #@c3
    invoke-static {v1}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@c6
    move-result-object v4

    #@c7
    .line 123
    .local v4, _arg2:Landroid/app/backup/IBackupManager;
    move-object/from16 v0, p0

    #@c9
    invoke-virtual {v0, v2, v3, v4}, Landroid/app/IBackupAgent$Stub;->doFullBackup(Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V

    #@cc
    .line 124
    const/4 v1, 0x1

    #@cd
    goto/16 :goto_7

    #@cf
    .line 117
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Landroid/app/backup/IBackupManager;
    :cond_cf
    const/4 v2, 0x0

    #@d0
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_bb

    #@d1
    .line 128
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    :sswitch_d1
    const-string v1, "android.app.IBackupAgent"

    #@d3
    move-object/from16 v0, p2

    #@d5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d8
    .line 130
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@db
    move-result v1

    #@dc
    if-eqz v1, :cond_118

    #@de
    .line 131
    sget-object v1, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e0
    move-object/from16 v0, p2

    #@e2
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e5
    move-result-object v2

    #@e6
    check-cast v2, Landroid/os/ParcelFileDescriptor;

    #@e8
    .line 137
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    :goto_e8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@eb
    move-result-wide v9

    #@ec
    .line 139
    .local v9, _arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ef
    move-result v4

    #@f0
    .line 141
    .local v4, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f3
    move-result-object v5

    #@f4
    .line 143
    .local v5, _arg3:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f7
    move-result-object v6

    #@f8
    .line 145
    .local v6, _arg4:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@fb
    move-result-wide v14

    #@fc
    .line 147
    .local v14, _arg5:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@ff
    move-result-wide v16

    #@100
    .line 149
    .local v16, _arg6:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@103
    move-result v18

    #@104
    .line 151
    .local v18, _arg7:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@107
    move-result-object v1

    #@108
    invoke-static {v1}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    #@10b
    move-result-object v19

    #@10c
    .local v19, _arg8:Landroid/app/backup/IBackupManager;
    move-object/from16 v7, p0

    #@10e
    move-object v8, v2

    #@10f
    move v11, v4

    #@110
    move-object v12, v5

    #@111
    move-object v13, v6

    #@112
    .line 152
    invoke-virtual/range {v7 .. v19}, Landroid/app/IBackupAgent$Stub;->doRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJILandroid/app/backup/IBackupManager;)V

    #@115
    .line 153
    const/4 v1, 0x1

    #@116
    goto/16 :goto_7

    #@118
    .line 134
    .end local v2           #_arg0:Landroid/os/ParcelFileDescriptor;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Ljava/lang/String;
    .end local v6           #_arg4:Ljava/lang/String;
    .end local v9           #_arg1:J
    .end local v14           #_arg5:J
    .end local v16           #_arg6:J
    .end local v18           #_arg7:I
    .end local v19           #_arg8:Landroid/app/backup/IBackupManager;
    :cond_118
    const/4 v2, 0x0

    #@119
    .restart local v2       #_arg0:Landroid/os/ParcelFileDescriptor;
    goto :goto_e8

    #@11a
    .line 45
    :sswitch_data_11a
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_61
        0x3 -> :sswitch_a4
        0x4 -> :sswitch_d1
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
