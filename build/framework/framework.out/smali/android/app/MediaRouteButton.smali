.class public Landroid/app/MediaRouteButton;
.super Landroid/view/View;
.source "MediaRouteButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/MediaRouteButton$MediaRouteCallback;
    }
.end annotation


# static fields
.field private static final ACTIVATED_STATE_SET:[I = null

.field private static final CHECKED_STATE_SET:[I = null

.field private static final TAG:Ljava/lang/String; = "MediaRouteButton"


# instance fields
.field private mAttachedToWindow:Z

.field private mCheatSheetEnabled:Z

.field private mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

.field private mExtendedSettingsClickListener:Landroid/view/View$OnClickListener;

.field private mIsConnecting:Z

.field private mMinHeight:I

.field private mMinWidth:I

.field private mRemoteActive:Z

.field private mRemoteIndicator:Landroid/graphics/drawable/Drawable;

.field private mRouteTypes:I

.field private mRouter:Landroid/media/MediaRouter;

.field private final mRouterCallback:Landroid/app/MediaRouteButton$MediaRouteCallback;

.field private mToggleMode:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 61
    new-array v0, v3, [I

    #@4
    const v1, 0x10100a0

    #@7
    aput v1, v0, v2

    #@9
    sput-object v0, Landroid/app/MediaRouteButton;->CHECKED_STATE_SET:[I

    #@b
    .line 65
    new-array v0, v3, [I

    #@d
    const v1, 0x10102fe

    #@10
    aput v1, v0, v2

    #@12
    sput-object v0, Landroid/app/MediaRouteButton;->ACTIVATED_STATE_SET:[I

    #@14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 70
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/app/MediaRouteButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 74
    const v0, 0x10103ad

    #@3
    invoke-direct {p0, p1, p2, v0}, Landroid/app/MediaRouteButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyleAttr"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 44
    new-instance v2, Landroid/app/MediaRouteButton$MediaRouteCallback;

    #@7
    const/4 v3, 0x0

    #@8
    invoke-direct {v2, p0, v3}, Landroid/app/MediaRouteButton$MediaRouteCallback;-><init>(Landroid/app/MediaRouteButton;Landroid/app/MediaRouteButton$1;)V

    #@b
    iput-object v2, p0, Landroid/app/MediaRouteButton;->mRouterCallback:Landroid/app/MediaRouteButton$MediaRouteCallback;

    #@d
    .line 80
    const-string/jumbo v2, "media_router"

    #@10
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/media/MediaRouter;

    #@16
    iput-object v2, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@18
    .line 82
    sget-object v2, Lcom/android/internal/R$styleable;->MediaRouteButton:[I

    #@1a
    invoke-virtual {p1, p2, v2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1d
    move-result-object v0

    #@1e
    .line 84
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x3

    #@1f
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@22
    move-result-object v2

    #@23
    invoke-direct {p0, v2}, Landroid/app/MediaRouteButton;->setRemoteIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V

    #@26
    .line 86
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@29
    move-result v2

    #@2a
    iput v2, p0, Landroid/app/MediaRouteButton;->mMinWidth:I

    #@2c
    .line 88
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@2f
    move-result v2

    #@30
    iput v2, p0, Landroid/app/MediaRouteButton;->mMinHeight:I

    #@32
    .line 90
    const/4 v2, 0x2

    #@33
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    #@36
    move-result v1

    #@37
    .line 93
    .local v1, routeTypes:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3a
    .line 95
    invoke-virtual {p0, v5}, Landroid/app/MediaRouteButton;->setClickable(Z)V

    #@3d
    .line 96
    invoke-virtual {p0, v5}, Landroid/app/MediaRouteButton;->setLongClickable(Z)V

    #@40
    .line 98
    invoke-virtual {p0, v1}, Landroid/app/MediaRouteButton;->setRouteTypes(I)V

    #@43
    .line 99
    return-void
.end method

.method static synthetic access$102(Landroid/app/MediaRouteButton;Lcom/android/internal/app/MediaRouteChooserDialogFragment;)Lcom/android/internal/app/MediaRouteChooserDialogFragment;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 40
    iput-object p1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2
    return-object p1
.end method

.method private getActivity()Landroid/app/Activity;
    .registers 4

    #@0
    .prologue
    .line 430
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    .line 431
    .local v0, context:Landroid/content/Context;
    :goto_4
    instance-of v1, v0, Landroid/content/ContextWrapper;

    #@6
    if-eqz v1, :cond_13

    #@8
    instance-of v1, v0, Landroid/app/Activity;

    #@a
    if-nez v1, :cond_13

    #@c
    .line 432
    check-cast v0, Landroid/content/ContextWrapper;

    #@e
    .end local v0           #context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    #@11
    move-result-object v0

    #@12
    .restart local v0       #context:Landroid/content/Context;
    goto :goto_4

    #@13
    .line 434
    :cond_13
    instance-of v1, v0, Landroid/app/Activity;

    #@15
    if-nez v1, :cond_1f

    #@17
    .line 435
    new-instance v1, Ljava/lang/IllegalStateException;

    #@19
    const-string v2, "The MediaRouteButton\'s Context is not an Activity."

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 438
    :cond_1f
    check-cast v0, Landroid/app/Activity;

    #@21
    .end local v0           #context:Landroid/content/Context;
    return-object v0
.end method

.method private setRemoteIndicatorDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "d"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 102
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@3
    if-eqz v0, :cond_10

    #@5
    .line 103
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@b
    .line 104
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {p0, v0}, Landroid/app/MediaRouteButton;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@10
    .line 106
    :cond_10
    iput-object p1, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@12
    .line 107
    if-eqz p1, :cond_28

    #@14
    .line 108
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@17
    .line 109
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getDrawableState()[I

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@1e
    .line 110
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getVisibility()I

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_2c

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@28
    .line 113
    :cond_28
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->refreshDrawableState()V

    #@2b
    .line 114
    return-void

    #@2c
    :cond_2c
    move v0, v1

    #@2d
    .line 110
    goto :goto_25
.end method

.method private updateRouteInfo()V
    .registers 1

    #@0
    .prologue
    .line 209
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->updateRemoteIndicator()V

    #@3
    .line 210
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->updateRouteCount()V

    #@6
    .line 211
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 283
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    #@3
    .line 285
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v1, :cond_13

    #@7
    .line 286
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getDrawableState()[I

    #@a
    move-result-object v0

    #@b
    .line 287
    .local v0, myDrawableState:[I
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@10
    .line 288
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->invalidate()V

    #@13
    .line 290
    .end local v0           #myDrawableState:[I
    :cond_13
    return-void
.end method

.method public getRouteTypes()I
    .registers 2

    #@0
    .prologue
    .line 214
    iget v0, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@2
    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 299
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@3
    .line 300
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 301
    :cond_c
    return-void
.end method

.method public onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 313
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@3
    .line 314
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/app/MediaRouteButton;->mAttachedToWindow:Z

    #@6
    .line 315
    iget v0, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@8
    if-eqz v0, :cond_16

    #@a
    .line 316
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@c
    iget v1, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@e
    iget-object v2, p0, Landroid/app/MediaRouteButton;->mRouterCallback:Landroid/app/MediaRouteButton$MediaRouteCallback;

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    #@13
    .line 317
    invoke-direct {p0}, Landroid/app/MediaRouteButton;->updateRouteInfo()V

    #@16
    .line 319
    :cond_16
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .registers 4
    .parameter "extraSpace"

    #@0
    .prologue
    .line 267
    add-int/lit8 v1, p1, 0x1

    #@2
    invoke-super {p0, v1}, Landroid/view/View;->onCreateDrawableState(I)[I

    #@5
    move-result-object v0

    #@6
    .line 273
    .local v0, drawableState:[I
    iget-boolean v1, p0, Landroid/app/MediaRouteButton;->mIsConnecting:Z

    #@8
    if-eqz v1, :cond_10

    #@a
    .line 274
    sget-object v1, Landroid/app/MediaRouteButton;->CHECKED_STATE_SET:[I

    #@c
    invoke-static {v0, v1}, Landroid/app/MediaRouteButton;->mergeDrawableStates([I[I)[I

    #@f
    .line 278
    :cond_f
    :goto_f
    return-object v0

    #@10
    .line 275
    :cond_10
    iget-boolean v1, p0, Landroid/app/MediaRouteButton;->mRemoteActive:Z

    #@12
    if-eqz v1, :cond_f

    #@14
    .line 276
    sget-object v1, Landroid/app/MediaRouteButton;->ACTIVATED_STATE_SET:[I

    #@16
    invoke-static {v0, v1}, Landroid/app/MediaRouteButton;->mergeDrawableStates([I[I)[I

    #@19
    goto :goto_f
.end method

.method public onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 323
    iget v0, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 324
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@6
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mRouterCallback:Landroid/app/MediaRouteButton$MediaRouteCallback;

    #@8
    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    #@b
    .line 326
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/app/MediaRouteButton;->mAttachedToWindow:Z

    #@e
    .line 327
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@11
    .line 328
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    .line 375
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 377
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@5
    if-nez v8, :cond_8

    #@7
    .line 391
    :goto_7
    return-void

    #@8
    .line 379
    :cond_8
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingLeft()I

    #@b
    move-result v5

    #@c
    .line 380
    .local v5, left:I
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getWidth()I

    #@f
    move-result v8

    #@10
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingRight()I

    #@13
    move-result v9

    #@14
    sub-int v6, v8, v9

    #@16
    .line 381
    .local v6, right:I
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingTop()I

    #@19
    move-result v7

    #@1a
    .line 382
    .local v7, top:I
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getHeight()I

    #@1d
    move-result v8

    #@1e
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingBottom()I

    #@21
    move-result v9

    #@22
    sub-int v0, v8, v9

    #@24
    .line 384
    .local v0, bottom:I
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@26
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@29
    move-result v4

    #@2a
    .line 385
    .local v4, drawWidth:I
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@2c
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2f
    move-result v1

    #@30
    .line 386
    .local v1, drawHeight:I
    sub-int v8, v6, v5

    #@32
    sub-int/2addr v8, v4

    #@33
    div-int/lit8 v8, v8, 0x2

    #@35
    add-int v2, v5, v8

    #@37
    .line 387
    .local v2, drawLeft:I
    sub-int v8, v0, v7

    #@39
    sub-int/2addr v8, v1

    #@3a
    div-int/lit8 v8, v8, 0x2

    #@3c
    add-int v3, v7, v8

    #@3e
    .line 389
    .local v3, drawTop:I
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@40
    add-int v9, v2, v4

    #@42
    add-int v10, v3, v1

    #@44
    invoke-virtual {v8, v2, v3, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@47
    .line 390
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@49
    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@4c
    goto :goto_7
.end method

.method protected onMeasure(II)V
    .registers 14
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 332
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@4
    move-result v7

    #@5
    .line 333
    .local v7, widthSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@8
    move-result v2

    #@9
    .line 334
    .local v2, heightSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@c
    move-result v6

    #@d
    .line 335
    .local v6, widthMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@10
    move-result v1

    #@11
    .line 337
    .local v1, heightMode:I
    iget v10, p0, Landroid/app/MediaRouteButton;->mMinWidth:I

    #@13
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@15
    if-eqz v8, :cond_51

    #@17
    iget-object v8, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@19
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1c
    move-result v8

    #@1d
    :goto_1d
    invoke-static {v10, v8}, Ljava/lang/Math;->max(II)I

    #@20
    move-result v4

    #@21
    .line 339
    .local v4, minWidth:I
    iget v8, p0, Landroid/app/MediaRouteButton;->mMinHeight:I

    #@23
    iget-object v10, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@25
    if-eqz v10, :cond_2d

    #@27
    iget-object v9, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@29
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@2c
    move-result v9

    #@2d
    :cond_2d
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@30
    move-result v3

    #@31
    .line 343
    .local v3, minHeight:I
    sparse-switch v6, :sswitch_data_76

    #@34
    .line 352
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingLeft()I

    #@37
    move-result v8

    #@38
    add-int/2addr v8, v4

    #@39
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingRight()I

    #@3c
    move-result v9

    #@3d
    add-int v5, v8, v9

    #@3f
    .line 357
    .local v5, width:I
    :goto_3f
    sparse-switch v1, :sswitch_data_80

    #@42
    .line 366
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingTop()I

    #@45
    move-result v8

    #@46
    add-int/2addr v8, v3

    #@47
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingBottom()I

    #@4a
    move-result v9

    #@4b
    add-int v0, v8, v9

    #@4d
    .line 370
    .local v0, height:I
    :goto_4d
    invoke-virtual {p0, v5, v0}, Landroid/app/MediaRouteButton;->setMeasuredDimension(II)V

    #@50
    .line 371
    return-void

    #@51
    .end local v0           #height:I
    .end local v3           #minHeight:I
    .end local v4           #minWidth:I
    .end local v5           #width:I
    :cond_51
    move v8, v9

    #@52
    .line 337
    goto :goto_1d

    #@53
    .line 345
    .restart local v3       #minHeight:I
    .restart local v4       #minWidth:I
    :sswitch_53
    move v5, v7

    #@54
    .line 346
    .restart local v5       #width:I
    goto :goto_3f

    #@55
    .line 348
    .end local v5           #width:I
    :sswitch_55
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingLeft()I

    #@58
    move-result v8

    #@59
    add-int/2addr v8, v4

    #@5a
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingRight()I

    #@5d
    move-result v9

    #@5e
    add-int/2addr v8, v9

    #@5f
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    #@62
    move-result v5

    #@63
    .line 349
    .restart local v5       #width:I
    goto :goto_3f

    #@64
    .line 359
    :sswitch_64
    move v0, v2

    #@65
    .line 360
    .restart local v0       #height:I
    goto :goto_4d

    #@66
    .line 362
    .end local v0           #height:I
    :sswitch_66
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingTop()I

    #@69
    move-result v8

    #@6a
    add-int/2addr v8, v3

    #@6b
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getPaddingBottom()I

    #@6e
    move-result v9

    #@6f
    add-int/2addr v8, v9

    #@70
    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    #@73
    move-result v0

    #@74
    .line 363
    .restart local v0       #height:I
    goto :goto_4d

    #@75
    .line 343
    nop

    #@76
    :sswitch_data_76
    .sparse-switch
        -0x80000000 -> :sswitch_55
        0x40000000 -> :sswitch_53
    .end sparse-switch

    #@80
    .line 357
    :sswitch_data_80
    .sparse-switch
        -0x80000000 -> :sswitch_66
        0x40000000 -> :sswitch_64
    .end sparse-switch
.end method

.method public performClick()Z
    .registers 8

    #@0
    .prologue
    .line 119
    invoke-super {p0}, Landroid/view/View;->performClick()Z

    #@3
    move-result v1

    #@4
    .line 120
    .local v1, handled:Z
    if-nez v1, :cond_a

    #@6
    .line 121
    const/4 v4, 0x0

    #@7
    invoke-virtual {p0, v4}, Landroid/app/MediaRouteButton;->playSoundEffect(I)V

    #@a
    .line 124
    :cond_a
    iget-boolean v4, p0, Landroid/app/MediaRouteButton;->mToggleMode:Z

    #@c
    if-eqz v4, :cond_4a

    #@e
    .line 125
    iget-boolean v4, p0, Landroid/app/MediaRouteButton;->mRemoteActive:Z

    #@10
    if-eqz v4, :cond_20

    #@12
    .line 126
    iget-object v4, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@14
    iget v5, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@16
    iget-object v6, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@18
    invoke-virtual {v6}, Landroid/media/MediaRouter;->getSystemAudioRoute()Landroid/media/MediaRouter$RouteInfo;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v4, v5, v6}, Landroid/media/MediaRouter;->selectRouteInt(ILandroid/media/MediaRouter$RouteInfo;)V

    #@1f
    .line 141
    :cond_1f
    :goto_1f
    return v1

    #@20
    .line 128
    :cond_20
    iget-object v4, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@22
    invoke-virtual {v4}, Landroid/media/MediaRouter;->getRouteCount()I

    #@25
    move-result v0

    #@26
    .line 129
    .local v0, N:I
    const/4 v2, 0x0

    #@27
    .local v2, i:I
    :goto_27
    if-ge v2, v0, :cond_1f

    #@29
    .line 130
    iget-object v4, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@2b
    invoke-virtual {v4, v2}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    #@2e
    move-result-object v3

    #@2f
    .line 131
    .local v3, route:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v3}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@32
    move-result v4

    #@33
    iget v5, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@35
    and-int/2addr v4, v5

    #@36
    if-eqz v4, :cond_47

    #@38
    iget-object v4, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@3a
    invoke-virtual {v4}, Landroid/media/MediaRouter;->getSystemAudioRoute()Landroid/media/MediaRouter$RouteInfo;

    #@3d
    move-result-object v4

    #@3e
    if-eq v3, v4, :cond_47

    #@40
    .line 133
    iget-object v4, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@42
    iget v5, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@44
    invoke-virtual {v4, v5, v3}, Landroid/media/MediaRouter;->selectRouteInt(ILandroid/media/MediaRouter$RouteInfo;)V

    #@47
    .line 129
    :cond_47
    add-int/lit8 v2, v2, 0x1

    #@49
    goto :goto_27

    #@4a
    .line 138
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #route:Landroid/media/MediaRouter$RouteInfo;
    :cond_4a
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->showDialog()V

    #@4d
    goto :goto_1f
.end method

.method public performLongClick()Z
    .registers 15

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 150
    invoke-super {p0}, Landroid/view/View;->performLongClick()Z

    #@5
    move-result v11

    #@6
    if-eqz v11, :cond_9

    #@8
    .line 187
    :goto_8
    return v9

    #@9
    .line 154
    :cond_9
    iget-boolean v11, p0, Landroid/app/MediaRouteButton;->mCheatSheetEnabled:Z

    #@b
    if-nez v11, :cond_f

    #@d
    move v9, v10

    #@e
    .line 155
    goto :goto_8

    #@f
    .line 158
    :cond_f
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getContentDescription()Ljava/lang/CharSequence;

    #@12
    move-result-object v1

    #@13
    .line 159
    .local v1, contentDesc:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16
    move-result v11

    #@17
    if-eqz v11, :cond_1b

    #@19
    move v9, v10

    #@1a
    .line 161
    goto :goto_8

    #@1b
    .line 164
    :cond_1b
    const/4 v11, 0x2

    #@1c
    new-array v6, v11, [I

    #@1e
    .line 165
    .local v6, screenPos:[I
    new-instance v3, Landroid/graphics/Rect;

    #@20
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@23
    .line 166
    .local v3, displayFrame:Landroid/graphics/Rect;
    invoke-virtual {p0, v6}, Landroid/app/MediaRouteButton;->getLocationOnScreen([I)V

    #@26
    .line 167
    invoke-virtual {p0, v3}, Landroid/app/MediaRouteButton;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    #@29
    .line 169
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getContext()Landroid/content/Context;

    #@2c
    move-result-object v2

    #@2d
    .line 170
    .local v2, context:Landroid/content/Context;
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getWidth()I

    #@30
    move-result v8

    #@31
    .line 171
    .local v8, width:I
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getHeight()I

    #@34
    move-result v4

    #@35
    .line 172
    .local v4, height:I
    aget v11, v6, v9

    #@37
    div-int/lit8 v12, v4, 0x2

    #@39
    add-int v5, v11, v12

    #@3b
    .line 173
    .local v5, midy:I
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3e
    move-result-object v11

    #@3f
    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@42
    move-result-object v11

    #@43
    iget v7, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    #@45
    .line 175
    .local v7, screenWidth:I
    invoke-static {v2, v1, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@48
    move-result-object v0

    #@49
    .line 176
    .local v0, cheatSheet:Landroid/widget/Toast;
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    #@4c
    move-result v11

    #@4d
    if-ge v5, v11, :cond_63

    #@4f
    .line 178
    const v11, 0x800035

    #@52
    aget v12, v6, v10

    #@54
    sub-int v12, v7, v12

    #@56
    div-int/lit8 v13, v8, 0x2

    #@58
    sub-int/2addr v12, v13

    #@59
    invoke-virtual {v0, v11, v12, v4}, Landroid/widget/Toast;->setGravity(III)V

    #@5c
    .line 184
    :goto_5c
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@5f
    .line 185
    invoke-virtual {p0, v10}, Landroid/app/MediaRouteButton;->performHapticFeedback(I)Z

    #@62
    goto :goto_8

    #@63
    .line 182
    :cond_63
    const/16 v11, 0x51

    #@65
    invoke-virtual {v0, v11, v10, v4}, Landroid/widget/Toast;->setGravity(III)V

    #@68
    goto :goto_5c
.end method

.method setCheatSheetEnabled(Z)V
    .registers 2
    .parameter "enable"

    #@0
    .prologue
    .line 145
    iput-boolean p1, p0, Landroid/app/MediaRouteButton;->mCheatSheetEnabled:Z

    #@2
    .line 146
    return-void
.end method

.method public setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 394
    iput-object p1, p0, Landroid/app/MediaRouteButton;->mExtendedSettingsClickListener:Landroid/view/View$OnClickListener;

    #@2
    .line 395
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 396
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    #@b
    .line 398
    :cond_b
    return-void
.end method

.method public setRouteTypes(I)V
    .registers 4
    .parameter "types"

    #@0
    .prologue
    .line 191
    iget v0, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@2
    if-ne p1, v0, :cond_5

    #@4
    .line 206
    :cond_4
    :goto_4
    return-void

    #@5
    .line 196
    :cond_5
    iget-boolean v0, p0, Landroid/app/MediaRouteButton;->mAttachedToWindow:Z

    #@7
    if-eqz v0, :cond_14

    #@9
    iget v0, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@b
    if-eqz v0, :cond_14

    #@d
    .line 197
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@f
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mRouterCallback:Landroid/app/MediaRouteButton$MediaRouteCallback;

    #@11
    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    #@14
    .line 200
    :cond_14
    iput p1, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@16
    .line 202
    iget-boolean v0, p0, Landroid/app/MediaRouteButton;->mAttachedToWindow:Z

    #@18
    if-eqz v0, :cond_4

    #@1a
    .line 203
    invoke-direct {p0}, Landroid/app/MediaRouteButton;->updateRouteInfo()V

    #@1d
    .line 204
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@1f
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mRouterCallback:Landroid/app/MediaRouteButton$MediaRouteCallback;

    #@21
    invoke-virtual {v0, p1, v1}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    #@24
    goto :goto_4
.end method

.method public setVisibility(I)V
    .registers 5
    .parameter "visibility"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 305
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    #@4
    .line 306
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 307
    iget-object v2, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->getVisibility()I

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_15

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@14
    .line 309
    :cond_14
    return-void

    #@15
    :cond_15
    move v0, v1

    #@16
    .line 307
    goto :goto_11
.end method

.method public showDialog()V
    .registers 4

    #@0
    .prologue
    .line 405
    invoke-direct {p0}, Landroid/app/MediaRouteButton;->getActivity()Landroid/app/Activity;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@7
    move-result-object v0

    #@8
    .line 406
    .local v0, fm:Landroid/app/FragmentManager;
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@a
    if-nez v1, :cond_16

    #@c
    .line 408
    const-string v1, "android:MediaRouteChooserDialogFragment"

    #@e
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@14
    iput-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@16
    .line 411
    :cond_16
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@18
    if-eqz v1, :cond_23

    #@1a
    .line 412
    const-string v1, "MediaRouteButton"

    #@1c
    const-string/jumbo v2, "showDialog(): Already showing!"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 426
    :goto_22
    return-void

    #@23
    .line 416
    :cond_23
    new-instance v1, Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@25
    invoke-direct {v1}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;-><init>()V

    #@28
    iput-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2a
    .line 417
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@2c
    iget-object v2, p0, Landroid/app/MediaRouteButton;->mExtendedSettingsClickListener:Landroid/view/View$OnClickListener;

    #@2e
    invoke-virtual {v1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    #@31
    .line 418
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@33
    new-instance v2, Landroid/app/MediaRouteButton$1;

    #@35
    invoke-direct {v2, p0}, Landroid/app/MediaRouteButton$1;-><init>(Landroid/app/MediaRouteButton;)V

    #@38
    invoke-virtual {v1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setLauncherListener(Lcom/android/internal/app/MediaRouteChooserDialogFragment$LauncherListener;)V

    #@3b
    .line 424
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@3d
    iget v2, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@3f
    invoke-virtual {v1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setRouteTypes(I)V

    #@42
    .line 425
    iget-object v1, p0, Landroid/app/MediaRouteButton;->mDialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@44
    const-string v2, "android:MediaRouteChooserDialogFragment"

    #@46
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    #@49
    goto :goto_22
.end method

.method updateRemoteIndicator()V
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 218
    iget-object v5, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@4
    iget v6, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@6
    invoke-virtual {v5, v6}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    #@9
    move-result-object v3

    #@a
    .line 219
    .local v3, selected:Landroid/media/MediaRouter$RouteInfo;
    iget-object v5, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@c
    invoke-virtual {v5}, Landroid/media/MediaRouter;->getSystemAudioRoute()Landroid/media/MediaRouter$RouteInfo;

    #@f
    move-result-object v5

    #@10
    if-eq v3, v5, :cond_31

    #@12
    move v1, v0

    #@13
    .line 220
    .local v1, isRemote:Z
    :goto_13
    if-eqz v3, :cond_33

    #@15
    invoke-virtual {v3}, Landroid/media/MediaRouter$RouteInfo;->getStatusCode()I

    #@18
    move-result v5

    #@19
    const/4 v6, 0x2

    #@1a
    if-ne v5, v6, :cond_33

    #@1c
    .line 223
    .local v0, isConnecting:Z
    :goto_1c
    const/4 v2, 0x0

    #@1d
    .line 224
    .local v2, needsRefresh:Z
    iget-boolean v4, p0, Landroid/app/MediaRouteButton;->mRemoteActive:Z

    #@1f
    if-eq v4, v1, :cond_24

    #@21
    .line 225
    iput-boolean v1, p0, Landroid/app/MediaRouteButton;->mRemoteActive:Z

    #@23
    .line 226
    const/4 v2, 0x1

    #@24
    .line 228
    :cond_24
    iget-boolean v4, p0, Landroid/app/MediaRouteButton;->mIsConnecting:Z

    #@26
    if-eq v4, v0, :cond_2b

    #@28
    .line 229
    iput-boolean v0, p0, Landroid/app/MediaRouteButton;->mIsConnecting:Z

    #@2a
    .line 230
    const/4 v2, 0x1

    #@2b
    .line 233
    :cond_2b
    if-eqz v2, :cond_30

    #@2d
    .line 234
    invoke-virtual {p0}, Landroid/app/MediaRouteButton;->refreshDrawableState()V

    #@30
    .line 236
    :cond_30
    return-void

    #@31
    .end local v0           #isConnecting:Z
    .end local v1           #isRemote:Z
    .end local v2           #needsRefresh:Z
    :cond_31
    move v1, v4

    #@32
    .line 219
    goto :goto_13

    #@33
    .restart local v1       #isRemote:Z
    :cond_33
    move v0, v4

    #@34
    .line 220
    goto :goto_1c
.end method

.method updateRouteCount()V
    .registers 10

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 239
    iget-object v6, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@4
    invoke-virtual {v6}, Landroid/media/MediaRouter;->getRouteCount()I

    #@7
    move-result v0

    #@8
    .line 240
    .local v0, N:I
    const/4 v1, 0x0

    #@9
    .line 241
    .local v1, count:I
    const/4 v2, 0x0

    #@a
    .line 242
    .local v2, hasVideoRoutes:Z
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_32

    #@d
    .line 243
    iget-object v6, p0, Landroid/app/MediaRouteButton;->mRouter:Landroid/media/MediaRouter;

    #@f
    invoke-virtual {v6, v3}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    #@12
    move-result-object v4

    #@13
    .line 244
    .local v4, route:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    #@16
    move-result v5

    #@17
    .line 245
    .local v5, routeTypes:I
    iget v6, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@19
    and-int/2addr v6, v5

    #@1a
    if-eqz v6, :cond_2c

    #@1c
    .line 246
    instance-of v6, v4, Landroid/media/MediaRouter$RouteGroup;

    #@1e
    if-eqz v6, :cond_2f

    #@20
    .line 247
    check-cast v4, Landroid/media/MediaRouter$RouteGroup;

    #@22
    .end local v4           #route:Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v4}, Landroid/media/MediaRouter$RouteGroup;->getRouteCount()I

    #@25
    move-result v6

    #@26
    add-int/2addr v1, v6

    #@27
    .line 251
    :goto_27
    and-int/lit8 v6, v5, 0x2

    #@29
    if-eqz v6, :cond_2c

    #@2b
    .line 252
    const/4 v2, 0x1

    #@2c
    .line 242
    :cond_2c
    add-int/lit8 v3, v3, 0x1

    #@2e
    goto :goto_b

    #@2f
    .line 249
    .restart local v4       #route:Landroid/media/MediaRouter$RouteInfo;
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_27

    #@32
    .line 257
    .end local v4           #route:Landroid/media/MediaRouter$RouteInfo;
    .end local v5           #routeTypes:I
    :cond_32
    if-eqz v1, :cond_46

    #@34
    move v6, v7

    #@35
    :goto_35
    invoke-virtual {p0, v6}, Landroid/app/MediaRouteButton;->setEnabled(Z)V

    #@38
    .line 261
    const/4 v6, 0x2

    #@39
    if-ne v1, v6, :cond_48

    #@3b
    iget v6, p0, Landroid/app/MediaRouteButton;->mRouteTypes:I

    #@3d
    and-int/lit8 v6, v6, 0x1

    #@3f
    if-eqz v6, :cond_48

    #@41
    if-nez v2, :cond_48

    #@43
    :goto_43
    iput-boolean v7, p0, Landroid/app/MediaRouteButton;->mToggleMode:Z

    #@45
    .line 263
    return-void

    #@46
    :cond_46
    move v6, v8

    #@47
    .line 257
    goto :goto_35

    #@48
    :cond_48
    move v7, v8

    #@49
    .line 261
    goto :goto_43
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_a

    #@6
    iget-object v0, p0, Landroid/app/MediaRouteButton;->mRemoteIndicator:Landroid/graphics/drawable/Drawable;

    #@8
    if-ne p1, v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method
