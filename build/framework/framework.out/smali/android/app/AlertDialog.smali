.class public Landroid/app/AlertDialog;
.super Landroid/app/Dialog;
.source "AlertDialog.java"

# interfaces
.implements Landroid/content/DialogInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/AlertDialog$Builder;
    }
.end annotation


# static fields
.field public static final THEME_DEVICE_DEFAULT_DARK:I = 0x4

.field public static final THEME_DEVICE_DEFAULT_LIGHT:I = 0x5

.field public static final THEME_HOLO_DARK:I = 0x2

.field public static final THEME_HOLO_LIGHT:I = 0x3

.field public static final THEME_TRADITIONAL:I = 0x1


# instance fields
.field private mAlert:Lcom/android/internal/app/AlertController;

.field private mResAlert:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 100
    const/4 v0, 0x0

    #@1
    invoke-static {p1, v0}, Landroid/app/AlertDialog;->resolveDialogTheme(Landroid/content/Context;I)I

    #@4
    move-result v0

    #@5
    const/4 v1, 0x1

    #@6
    invoke-direct {p0, p1, v0, v1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;IZ)V

    #@9
    .line 101
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "theme"

    #@0
    .prologue
    .line 112
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;IZ)V

    #@4
    .line 113
    return-void
.end method

.method constructor <init>(Landroid/content/Context;IZ)V
    .registers 7
    .parameter "context"
    .parameter "theme"
    .parameter "createThemeContextWrapper"

    #@0
    .prologue
    .line 116
    invoke-static {p1, p2}, Landroid/app/AlertDialog;->resolveDialogTheme(Landroid/content/Context;I)I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, p1, v0, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;IZ)V

    #@7
    .line 67
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/app/AlertDialog;->mResAlert:I

    #@a
    .line 118
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@c
    invoke-virtual {v0}, Landroid/view/Window;->alwaysReadCloseOnTouchAttr()V

    #@f
    .line 119
    new-instance v0, Lcom/android/internal/app/AlertController;

    #@11
    invoke-virtual {p0}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {p0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v0, v1, p0, v2}, Lcom/android/internal/app/AlertController;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V

    #@1c
    iput-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@1e
    .line 120
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .registers 6
    .parameter "context"
    .parameter "cancelable"
    .parameter "cancelListener"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 123
    invoke-static {p1, v1}, Landroid/app/AlertDialog;->resolveDialogTheme(Landroid/content/Context;I)I

    #@4
    move-result v0

    #@5
    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@8
    .line 67
    iput v1, p0, Landroid/app/AlertDialog;->mResAlert:I

    #@a
    .line 124
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@c
    invoke-virtual {v0}, Landroid/view/Window;->alwaysReadCloseOnTouchAttr()V

    #@f
    .line 125
    invoke-virtual {p0, p2}, Landroid/app/AlertDialog;->setCancelable(Z)V

    #@12
    .line 126
    invoke-virtual {p0, p3}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@15
    .line 127
    new-instance v0, Lcom/android/internal/app/AlertController;

    #@17
    invoke-virtual {p0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, p1, p0, v1}, Lcom/android/internal/app/AlertController;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V

    #@1e
    iput-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@20
    .line 128
    return-void
.end method

.method static synthetic access$000(Landroid/app/AlertDialog;)Lcom/android/internal/app/AlertController;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    return-object v0
.end method

.method static resolveDialogTheme(Landroid/content/Context;I)I
    .registers 6
    .parameter "context"
    .parameter "resid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 138
    if-ne p1, v3, :cond_7

    #@3
    .line 139
    const p1, 0x10302f2

    #@6
    .line 154
    .end local p1
    :cond_6
    :goto_6
    return p1

    #@7
    .line 140
    .restart local p1
    :cond_7
    const/4 v1, 0x2

    #@8
    if-ne p1, v1, :cond_e

    #@a
    .line 141
    const p1, 0x1030302

    #@d
    goto :goto_6

    #@e
    .line 142
    :cond_e
    const/4 v1, 0x3

    #@f
    if-ne p1, v1, :cond_15

    #@11
    .line 143
    const p1, 0x1030306

    #@14
    goto :goto_6

    #@15
    .line 144
    :cond_15
    const/4 v1, 0x4

    #@16
    if-ne p1, v1, :cond_1c

    #@18
    .line 145
    const p1, 0x103030e

    #@1b
    goto :goto_6

    #@1c
    .line 146
    :cond_1c
    const/4 v1, 0x5

    #@1d
    if-ne p1, v1, :cond_23

    #@1f
    .line 147
    const p1, 0x103030f

    #@22
    goto :goto_6

    #@23
    .line 148
    :cond_23
    const/high16 v1, 0x100

    #@25
    if-ge p1, v1, :cond_6

    #@27
    .line 151
    new-instance v0, Landroid/util/TypedValue;

    #@29
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@2c
    .line 152
    .local v0, outValue:Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@2f
    move-result-object v1

    #@30
    const v2, 0x1010309

    #@33
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@36
    .line 154
    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    #@38
    goto :goto_6
.end method


# virtual methods
.method public getButton(I)Landroid/widget/Button;
    .registers 3
    .parameter "whichButton"

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getListView()Landroid/widget/ListView;
    .registers 2

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/app/AlertController;->getListView()Landroid/widget/ListView;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 345
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 347
    iget v0, p0, Landroid/app/AlertDialog;->mResAlert:I

    #@5
    packed-switch v0, :pswitch_data_16

    #@8
    .line 353
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@a
    invoke-virtual {v0}, Lcom/android/internal/app/AlertController;->installContent()V

    #@d
    .line 357
    :goto_d
    return-void

    #@e
    .line 349
    :pswitch_e
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@10
    iget v1, p0, Landroid/app/AlertDialog;->mResAlert:I

    #@12
    invoke-virtual {v0, v1}, Lcom/android/internal/app/AlertController;->installContent(I)V

    #@15
    goto :goto_d

    #@16
    .line 347
    :pswitch_data_16
    .packed-switch 0x2030002
        :pswitch_e
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 361
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/app/AlertController;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    .line 362
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 367
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/app/AlertController;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    .line 368
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 6
    .parameter "whichButton"
    .parameter "text"
    .parameter "listener"

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/android/internal/app/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    #@6
    .line 246
    return-void
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
    .registers 6
    .parameter "whichButton"
    .parameter "text"
    .parameter "msg"

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/android/internal/app/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    #@6
    .line 232
    return-void
.end method

.method public setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 4
    .parameter "text"
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 286
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    #@4
    .line 287
    return-void
.end method

.method public setButton(Ljava/lang/CharSequence;Landroid/os/Message;)V
    .registers 4
    .parameter "text"
    .parameter "msg"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 254
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@4
    .line 255
    return-void
.end method

.method public setButton2(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 4
    .parameter "text"
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 299
    const/4 v0, -0x2

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    #@4
    .line 300
    return-void
.end method

.method public setButton2(Ljava/lang/CharSequence;Landroid/os/Message;)V
    .registers 4
    .parameter "text"
    .parameter "msg"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 263
    const/4 v0, -0x2

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@4
    .line 264
    return-void
.end method

.method public setButton3(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 4
    .parameter "text"
    .parameter "listener"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 312
    const/4 v0, -0x3

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    #@4
    .line 313
    return-void
.end method

.method public setButton3(Ljava/lang/CharSequence;Landroid/os/Message;)V
    .registers 4
    .parameter "text"
    .parameter "msg"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 272
    const/4 v0, -0x3

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    #@4
    .line 273
    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .registers 3
    .parameter "customTitleView"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setCustomTitle(Landroid/view/View;)V

    #@5
    .line 192
    return-void
.end method

.method public setIcon(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 321
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setIcon(I)V

    #@5
    .line 322
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "icon"

    #@0
    .prologue
    .line 325
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@5
    .line 326
    return-void
.end method

.method public setIconAttribute(I)V
    .registers 5
    .parameter "attrId"

    #@0
    .prologue
    .line 334
    new-instance v0, Landroid/util/TypedValue;

    #@2
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@5
    .line 335
    .local v0, out:Landroid/util/TypedValue;
    iget-object v1, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@a
    move-result-object v1

    #@b
    const/4 v2, 0x1

    #@c
    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@f
    .line 336
    iget-object v1, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@11
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    #@13
    invoke-virtual {v1, v2}, Lcom/android/internal/app/AlertController;->setIcon(I)V

    #@16
    .line 337
    return-void
.end method

.method public setInverseBackgroundForced(Z)V
    .registers 3
    .parameter "forceInverseBackground"

    #@0
    .prologue
    .line 340
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setInverseBackgroundForced(Z)V

    #@5
    .line 341
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "message"

    #@0
    .prologue
    .line 195
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setMessage(Ljava/lang/CharSequence;)V

    #@5
    .line 196
    return-void
.end method

.method public setResAlert(I)V
    .registers 2
    .parameter "ResAlert"

    #@0
    .prologue
    .line 133
    iput p1, p0, Landroid/app/AlertDialog;->mResAlert:I

    #@2
    .line 134
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    #@3
    .line 184
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@5
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setTitle(Ljava/lang/CharSequence;)V

    #@8
    .line 185
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AlertController;->setView(Landroid/view/View;)V

    #@5
    .line 203
    return-void
.end method

.method public setView(Landroid/view/View;IIII)V
    .registers 12
    .parameter "view"
    .parameter "viewSpacingLeft"
    .parameter "viewSpacingTop"
    .parameter "viewSpacingRight"
    .parameter "viewSpacingBottom"

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Landroid/app/AlertDialog;->mAlert:Lcom/android/internal/app/AlertController;

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/app/AlertController;->setView(Landroid/view/View;IIII)V

    #@a
    .line 218
    return-void
.end method
