.class public Landroid/app/Activity;
.super Landroid/view/ContextThemeWrapper;
.source "Activity.java"

# interfaces
.implements Landroid/view/LayoutInflater$Factory2;
.implements Landroid/view/Window$Callback;
.implements Landroid/view/KeyEvent$Callback;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Activity$ManagedCursor;,
        Landroid/app/Activity$NonConfigurationInstances;,
        Landroid/app/Activity$ManagedDialog;
    }
.end annotation


# static fields
.field private static final DEBUG_LIFECYCLE:Z = false

.field public static final DEFAULT_KEYS_DIALER:I = 0x1

.field public static final DEFAULT_KEYS_DISABLE:I = 0x0

.field public static final DEFAULT_KEYS_SEARCH_GLOBAL:I = 0x4

.field public static final DEFAULT_KEYS_SEARCH_LOCAL:I = 0x3

.field public static final DEFAULT_KEYS_SHORTCUT:I = 0x2

.field protected static final FOCUSED_STATE_SET:[I = null

.field static final FRAGMENTS_TAG:Ljava/lang/String; = "android:fragments"

.field public static final RESULT_CANCELED:I = 0x0

.field public static final RESULT_FIRST_USER:I = 0x1

.field public static final RESULT_OK:I = -0x1

.field private static final SAVED_DIALOGS_TAG:Ljava/lang/String; = "android:savedDialogs"

.field private static final SAVED_DIALOG_ARGS_KEY_PREFIX:Ljava/lang/String; = "android:dialog_args_"

.field private static final SAVED_DIALOG_IDS_KEY:Ljava/lang/String; = "android:savedDialogIds"

.field private static final SAVED_DIALOG_KEY_PREFIX:Ljava/lang/String; = "android:dialog_"

.field private static final TAG:Ljava/lang/String; = "Activity"

.field private static final WINDOW_HIERARCHY_TAG:Ljava/lang/String; = "android:viewHierarchyState"

.field private static final sLowProfilePackageNames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mActionBar:Lcom/android/internal/app/ActionBarImpl;

.field mActivityInfo:Landroid/content/pm/ActivityInfo;

.field mAllLoaderManagers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/app/LoaderManagerImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mApplication:Landroid/app/Application;

.field mCalled:Z

.field mChangingConfigurations:Z

.field mCheckedForLoaderManager:Z

.field private mComponent:Landroid/content/ComponentName;

.field mConfigChangeFlags:I

.field final mContainer:Landroid/app/FragmentContainer;

.field mCurrentConfig:Landroid/content/res/Configuration;

.field mDecor:Landroid/view/View;

.field private mDefaultKeyMode:I

.field private mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

.field private mDestroyed:Z

.field mEmbeddedID:Ljava/lang/String;

.field private mEnableDefaultActionBarUp:Z

.field mFinished:Z

.field final mFragments:Landroid/app/FragmentManagerImpl;

.field final mHandler:Landroid/os/Handler;

.field private mIdent:I

.field private final mInstanceTracker:Ljava/lang/Object;

.field private mInstrumentation:Landroid/app/Instrumentation;

.field mIntent:Landroid/content/Intent;

.field mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

.field mLoaderManager:Landroid/app/LoaderManagerImpl;

.field mLoadersStarted:Z

.field mMainThread:Landroid/app/ActivityThread;

.field private final mManagedCursors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Activity$ManagedCursor;",
            ">;"
        }
    .end annotation
.end field

.field private mManagedDialogs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/app/Activity$ManagedDialog;",
            ">;"
        }
    .end annotation
.end field

.field private mMenuInflater:Landroid/view/MenuInflater;

.field mParent:Landroid/app/Activity;

.field mResultCode:I

.field mResultData:Landroid/content/Intent;

.field mResumed:Z

.field private mSearchManager:Landroid/app/SearchManager;

.field mStartedActivity:Z

.field private mStopped:Z

.field mTemporaryPause:Z

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleColor:I

.field private mTitleReady:Z

.field private mToken:Landroid/os/IBinder;

.field private mUiThread:Ljava/lang/Thread;

.field mVisibleFromClient:Z

.field mVisibleFromServer:Z

.field private mWindow:Landroid/view/Window;

.field mWindowAdded:Z

.field private mWindowManager:Landroid/view/WindowManager;

.field private viewAgent:Lcom/lge/app/atsagent/IViewAgent;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 765
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const v2, 0x101009c

    #@7
    aput v2, v0, v1

    #@9
    sput-object v0, Landroid/app/Activity;->FOCUSED_STATE_SET:[I

    #@b
    .line 5430
    new-instance v0, Ljava/util/HashSet;

    #@d
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@10
    sput-object v0, Landroid/app/Activity;->sLowProfilePackageNames:Ljava/util/Set;

    #@12
    .line 5433
    sget-object v0, Landroid/app/Activity;->sLowProfilePackageNames:Ljava/util/Set;

    #@14
    const-string v1, "com.nttdocomo.android.mediaplayer"

    #@16
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@19
    .line 5434
    sget-object v0, Landroid/app/Activity;->sLowProfilePackageNames:Ljava/util/Set;

    #@1b
    const-string/jumbo v1, "jp.co.mmbi.app"

    #@1e
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@21
    .line 5435
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 652
    invoke-direct {p0}, Landroid/view/ContextThemeWrapper;-><init>()V

    #@5
    .line 700
    iput-boolean v1, p0, Landroid/app/Activity;->mTemporaryPause:Z

    #@7
    .line 702
    iput-boolean v1, p0, Landroid/app/Activity;->mChangingConfigurations:Z

    #@9
    .line 719
    iput-object v2, p0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@b
    .line 720
    iput-boolean v1, p0, Landroid/app/Activity;->mWindowAdded:Z

    #@d
    .line 721
    iput-boolean v1, p0, Landroid/app/Activity;->mVisibleFromServer:Z

    #@f
    .line 722
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Landroid/app/Activity;->mVisibleFromClient:Z

    #@12
    .line 723
    iput-object v2, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@14
    .line 727
    iput v1, p0, Landroid/app/Activity;->mTitleColor:I

    #@16
    .line 729
    new-instance v0, Landroid/app/FragmentManagerImpl;

    #@18
    invoke-direct {v0}, Landroid/app/FragmentManagerImpl;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@1d
    .line 730
    new-instance v0, Landroid/app/Activity$1;

    #@1f
    invoke-direct {v0, p0}, Landroid/app/Activity$1;-><init>(Landroid/app/Activity;)V

    #@22
    iput-object v0, p0, Landroid/app/Activity;->mContainer:Landroid/app/FragmentContainer;

    #@24
    .line 751
    new-instance v0, Ljava/util/ArrayList;

    #@26
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@29
    iput-object v0, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@2b
    .line 755
    iput v1, p0, Landroid/app/Activity;->mResultCode:I

    #@2d
    .line 756
    iput-object v2, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@2f
    .line 758
    iput-boolean v1, p0, Landroid/app/Activity;->mTitleReady:Z

    #@31
    .line 760
    iput v1, p0, Landroid/app/Activity;->mDefaultKeyMode:I

    #@33
    .line 761
    iput-object v2, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@35
    .line 763
    iput-object v2, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@37
    .line 767
    invoke-static {p0}, Landroid/os/StrictMode;->trackActivity(Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    move-result-object v0

    #@3b
    iput-object v0, p0, Landroid/app/Activity;->mInstanceTracker:Ljava/lang/Object;

    #@3d
    .line 771
    new-instance v0, Landroid/os/Handler;

    #@3f
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@42
    iput-object v0, p0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@44
    return-void
.end method

.method private createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter "dialogId"
    .parameter "state"
    .parameter "args"

    #@0
    .prologue
    .line 1005
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@3
    move-result v1

    #@4
    invoke-virtual {p0, v1, p3}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    #@7
    move-result-object v0

    #@8
    .line 1006
    .local v0, dialog:Landroid/app/Dialog;
    if-nez v0, :cond_c

    #@a
    .line 1007
    const/4 v0, 0x0

    #@b
    .line 1010
    .end local v0           #dialog:Landroid/app/Dialog;
    :goto_b
    return-object v0

    #@c
    .line 1009
    .restart local v0       #dialog:Landroid/app/Dialog;
    :cond_c
    invoke-virtual {v0, p2}, Landroid/app/Dialog;->dispatchOnCreate(Landroid/os/Bundle;)V

    #@f
    goto :goto_b
.end method

.method private dumpViewHierarchy(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V
    .registers 9
    .parameter "prefix"
    .parameter "writer"
    .parameter "view"

    #@0
    .prologue
    .line 4889
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    .line 4890
    if-nez p3, :cond_c

    #@5
    .line 4891
    const-string/jumbo v3, "null"

    #@8
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b
    .line 4907
    :cond_b
    return-void

    #@c
    .line 4894
    :cond_c
    invoke-virtual {p3}, Landroid/view/View;->toString()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13
    .line 4895
    instance-of v3, p3, Landroid/view/ViewGroup;

    #@15
    if-eqz v3, :cond_b

    #@17
    move-object v1, p3

    #@18
    .line 4898
    check-cast v1, Landroid/view/ViewGroup;

    #@1a
    .line 4899
    .local v1, grp:Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    #@1d
    move-result v0

    #@1e
    .line 4900
    .local v0, N:I
    if-lez v0, :cond_b

    #@20
    .line 4903
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, "  "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object p1

    #@33
    .line 4904
    const/4 v2, 0x0

    #@34
    .local v2, i:I
    :goto_34
    if-ge v2, v0, :cond_b

    #@36
    .line 4905
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@39
    move-result-object v3

    #@3a
    invoke-direct {p0, p1, p2, v3}, Landroid/app/Activity;->dumpViewHierarchy(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    #@3d
    .line 4904
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_34
.end method

.method private ensureSearchManager()V
    .registers 3

    #@0
    .prologue
    .line 4549
    iget-object v0, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 4554
    :goto_4
    return-void

    #@5
    .line 4553
    :cond_5
    new-instance v0, Landroid/app/SearchManager;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-direct {v0, p0, v1}, Landroid/app/SearchManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@b
    iput-object v0, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@d
    goto :goto_4
.end method

.method private hideFloatingAppsIfNeeded()V
    .registers 4

    #@0
    .prologue
    .line 5471
    sget-object v1, Landroid/app/Activity;->sLowProfilePackageNames:Ljava/util/Set;

    #@2
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_2e

    #@c
    .line 5472
    const-string v1, "Activity"

    #@e
    const-string/jumbo v2, "send broadcast to make floating apps to enter into the low profile mode"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 5473
    new-instance v0, Landroid/content/Intent;

    #@16
    const-string v1, "com.lge.intent.action.FLOATING_WINDOW_ENTER_LOWPROFILE"

    #@18
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1b
    .line 5474
    .local v0, i:Landroid/content/Intent;
    const-string v1, "hide"

    #@1d
    const/4 v2, 0x1

    #@1e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@21
    .line 5475
    const-string/jumbo v1, "package"

    #@24
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2b
    .line 5476
    invoke-virtual {p0, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    #@2e
    .line 5478
    .end local v0           #i:Landroid/content/Intent;
    :cond_2e
    return-void
.end method

.method private initActionBar()V
    .registers 4

    #@0
    .prologue
    .line 1940
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    .line 1944
    .local v0, window:Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@7
    .line 1946
    invoke-virtual {p0}, Landroid/app/Activity;->isChild()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_19

    #@d
    const/16 v1, 0x8

    #@f
    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_19

    #@15
    iget-object v1, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@17
    if-eqz v1, :cond_1a

    #@19
    .line 1952
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 1950
    :cond_1a
    new-instance v1, Lcom/android/internal/app/ActionBarImpl;

    #@1c
    invoke-direct {v1, p0}, Lcom/android/internal/app/ActionBarImpl;-><init>(Landroid/app/Activity;)V

    #@1f
    iput-object v1, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@21
    .line 1951
    iget-object v1, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@23
    iget-boolean v2, p0, Landroid/app/Activity;->mEnableDefaultActionBarUp:Z

    #@25
    invoke-virtual {v1, v2}, Lcom/android/internal/app/ActionBarImpl;->setDefaultDisplayHomeAsUpEnabled(Z)V

    #@28
    goto :goto_19
.end method

.method private isExternal()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5488
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@8
    move-result-object v1

    #@9
    .line 5489
    .local v1, lp:Landroid/view/WindowManager$LayoutParams;
    const-string v3, "Activity"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "[DSDR][Activity.java]isExternal() : extUsage == "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 5490
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@25
    and-int/lit8 v3, v3, 0x1

    #@27
    if-nez v3, :cond_2a

    #@29
    .line 5501
    :cond_29
    :goto_29
    return v2

    #@2a
    .line 5496
    :cond_2a
    iget-object v3, p0, Landroid/app/Activity;->mWindowManager:Landroid/view/WindowManager;

    #@2c
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Landroid/view/Display;->getVisibleStatus()I

    #@33
    move-result v0

    #@34
    .line 5497
    .local v0, dsdrStatus:I
    const-string v3, "Activity"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "[DSDR][Activity.java]isExternal() : dsdrStatus == "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 5498
    const/4 v3, 0x2

    #@4d
    if-lt v0, v3, :cond_29

    #@4f
    .line 5501
    const/4 v2, 0x1

    #@50
    goto :goto_29
.end method

.method private missingDialog(I)Ljava/lang/IllegalArgumentException;
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 3206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "no dialog with id "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, " was ever "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string/jumbo v2, "shown via Activity#showDialog"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@26
    return-object v0
.end method

.method private restoreManagedDialogs(Landroid/os/Bundle;)V
    .registers 12
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 978
    const-string v7, "android:savedDialogs"

    #@2
    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@5
    move-result-object v0

    #@6
    .line 979
    .local v0, b:Landroid/os/Bundle;
    if-nez v0, :cond_9

    #@8
    .line 1002
    :cond_8
    return-void

    #@9
    .line 983
    :cond_9
    const-string v7, "android:savedDialogIds"

    #@b
    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@e
    move-result-object v4

    #@f
    .line 984
    .local v4, ids:[I
    array-length v6, v4

    #@10
    .line 985
    .local v6, numDialogs:I
    new-instance v7, Landroid/util/SparseArray;

    #@12
    invoke-direct {v7, v6}, Landroid/util/SparseArray;-><init>(I)V

    #@15
    iput-object v7, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@17
    .line 986
    const/4 v3, 0x0

    #@18
    .local v3, i:I
    :goto_18
    if-ge v3, v6, :cond_8

    #@1a
    .line 987
    aget v7, v4, v3

    #@1c
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v1

    #@20
    .line 988
    .local v1, dialogId:Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@23
    move-result v7

    #@24
    invoke-static {v7}, Landroid/app/Activity;->savedDialogKeyFor(I)Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@2b
    move-result-object v2

    #@2c
    .line 989
    .local v2, dialogState:Landroid/os/Bundle;
    if-eqz v2, :cond_67

    #@2e
    .line 992
    new-instance v5, Landroid/app/Activity$ManagedDialog;

    #@30
    const/4 v7, 0x0

    #@31
    invoke-direct {v5, v7}, Landroid/app/Activity$ManagedDialog;-><init>(Landroid/app/Activity$1;)V

    #@34
    .line 993
    .local v5, md:Landroid/app/Activity$ManagedDialog;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@37
    move-result v7

    #@38
    invoke-static {v7}, Landroid/app/Activity;->savedDialogArgsKeyFor(I)Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@3f
    move-result-object v7

    #@40
    iput-object v7, v5, Landroid/app/Activity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    #@42
    .line 994
    iget-object v7, v5, Landroid/app/Activity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    #@44
    invoke-direct {p0, v1, v2, v7}, Landroid/app/Activity;->createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;

    #@47
    move-result-object v7

    #@48
    iput-object v7, v5, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@4a
    .line 995
    iget-object v7, v5, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@4c
    if-eqz v7, :cond_67

    #@4e
    .line 996
    iget-object v7, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@50
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@53
    move-result v8

    #@54
    invoke-virtual {v7, v8, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@57
    .line 997
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@5a
    move-result v7

    #@5b
    iget-object v8, v5, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@5d
    iget-object v9, v5, Landroid/app/Activity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    #@5f
    invoke-virtual {p0, v7, v8, v9}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    #@62
    .line 998
    iget-object v7, v5, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@64
    invoke-virtual {v7, v2}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@67
    .line 986
    .end local v5           #md:Landroid/app/Activity$ManagedDialog;
    :cond_67
    add-int/lit8 v3, v3, 0x1

    #@69
    goto :goto_18
.end method

.method private saveManagedDialogs(Landroid/os/Bundle;)V
    .registers 10
    .parameter "outState"

    #@0
    .prologue
    .line 1264
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@2
    if-nez v6, :cond_5

    #@4
    .line 1290
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1268
    :cond_5
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@7
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@a
    move-result v5

    #@b
    .line 1269
    .local v5, numDialogs:I
    if-eqz v5, :cond_4

    #@d
    .line 1273
    new-instance v0, Landroid/os/Bundle;

    #@f
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@12
    .line 1275
    .local v0, dialogState:Landroid/os/Bundle;
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@17
    move-result v6

    #@18
    new-array v2, v6, [I

    #@1a
    .line 1278
    .local v2, ids:[I
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    if-ge v1, v5, :cond_4a

    #@1d
    .line 1279
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@1f
    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@22
    move-result v3

    #@23
    .line 1280
    .local v3, key:I
    aput v3, v2, v1

    #@25
    .line 1281
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@27
    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@2a
    move-result-object v4

    #@2b
    check-cast v4, Landroid/app/Activity$ManagedDialog;

    #@2d
    .line 1282
    .local v4, md:Landroid/app/Activity$ManagedDialog;
    invoke-static {v3}, Landroid/app/Activity;->savedDialogKeyFor(I)Ljava/lang/String;

    #@30
    move-result-object v6

    #@31
    iget-object v7, v4, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@33
    invoke-virtual {v7}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    #@36
    move-result-object v7

    #@37
    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@3a
    .line 1283
    iget-object v6, v4, Landroid/app/Activity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    #@3c
    if-eqz v6, :cond_47

    #@3e
    .line 1284
    invoke-static {v3}, Landroid/app/Activity;->savedDialogArgsKeyFor(I)Ljava/lang/String;

    #@41
    move-result-object v6

    #@42
    iget-object v7, v4, Landroid/app/Activity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    #@44
    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@47
    .line 1278
    :cond_47
    add-int/lit8 v1, v1, 0x1

    #@49
    goto :goto_1b

    #@4a
    .line 1288
    .end local v3           #key:I
    .end local v4           #md:Landroid/app/Activity$ManagedDialog;
    :cond_4a
    const-string v6, "android:savedDialogIds"

    #@4c
    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    #@4f
    .line 1289
    const-string v6, "android:savedDialogs"

    #@51
    invoke-virtual {p1, v6, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@54
    goto :goto_4
.end method

.method private static savedDialogArgsKeyFor(I)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1018
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "android:dialog_args_"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method private static savedDialogKeyFor(I)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 1014
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "android:dialog_"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method private startIntentSenderForResultInner(Landroid/content/IntentSender;ILandroid/content/Intent;IILandroid/app/Activity;Landroid/os/Bundle;)V
    .registers 21
    .parameter "intent"
    .parameter "requestCode"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "activity"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 3590
    const/4 v5, 0x0

    #@1
    .line 3591
    .local v5, resolvedType:Ljava/lang/String;
    if-eqz p3, :cond_13

    #@3
    .line 3592
    const/4 v1, 0x0

    #@4
    :try_start_4
    move-object/from16 v0, p3

    #@6
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAllowFds(Z)V

    #@9
    .line 3593
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v1

    #@d
    move-object/from16 v0, p3

    #@f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    .line 3595
    :cond_13
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@16
    move-result-object v1

    #@17
    iget-object v2, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@19
    invoke-virtual {v2}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@1c
    move-result-object v2

    #@1d
    iget-object v6, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@1f
    move-object/from16 v0, p6

    #@21
    iget-object v7, v0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@23
    move-object v3, p1

    #@24
    move-object/from16 v4, p3

    #@26
    move v8, p2

    #@27
    move/from16 v9, p4

    #@29
    move/from16 v10, p5

    #@2b
    move-object/from16 v11, p7

    #@2d
    invoke-interface/range {v1 .. v11}, Landroid/app/IActivityManager;->startActivityIntentSender(Landroid/app/IApplicationThread;Landroid/content/IntentSender;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I

    #@30
    move-result v12

    #@31
    .line 3599
    .local v12, result:I
    const/4 v1, -0x6

    #@32
    if-ne v12, v1, :cond_41

    #@34
    .line 3600
    new-instance v1, Landroid/content/IntentSender$SendIntentException;

    #@36
    invoke-direct {v1}, Landroid/content/IntentSender$SendIntentException;-><init>()V

    #@39
    throw v1
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_3a} :catch_3a

    #@3a
    .line 3603
    .end local v12           #result:I
    :catch_3a
    move-exception v1

    #@3b
    .line 3605
    :goto_3b
    if-ltz p2, :cond_40

    #@3d
    .line 3613
    const/4 v1, 0x1

    #@3e
    iput-boolean v1, p0, Landroid/app/Activity;->mStartedActivity:Z

    #@40
    .line 3615
    :cond_40
    return-void

    #@41
    .line 3602
    .restart local v12       #result:I
    :cond_41
    const/4 v1, 0x0

    #@42
    :try_start_42
    invoke-static {v12, v1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V
    :try_end_45
    .catch Landroid/os/RemoteException; {:try_start_42 .. :try_end_45} :catch_3a

    #@45
    goto :goto_3b
.end method

.method private unhideFloatingAppsIfNeeded(ZLandroid/app/Activity;)V
    .registers 13
    .parameter "forceIfLastActivity"
    .parameter "activity"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 5439
    sget-object v6, Landroid/app/Activity;->sLowProfilePackageNames:Ljava/util/Set;

    #@4
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@7
    move-result-object v7

    #@8
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v6

    #@c
    if-eqz v6, :cond_71

    #@e
    .line 5442
    :try_start_e
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@11
    move-result-object v6

    #@12
    const/4 v7, 0x1

    #@13
    const/4 v8, 0x0

    #@14
    const/4 v9, 0x0

    #@15
    invoke-interface {v6, v7, v8, v9}, Landroid/app/IActivityManager;->getTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;

    #@18
    move-result-object v3

    #@19
    .line 5444
    .local v3, tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v3, :cond_71

    #@1b
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@1e
    move-result v6

    #@1f
    if-ne v6, v1, :cond_71

    #@21
    .line 5445
    const/4 v6, 0x0

    #@22
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v4

    #@26
    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    #@28
    .line 5447
    .local v4, ti:Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    #@2a
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v6

    #@36
    if-nez v6, :cond_72

    #@38
    move v2, v1

    #@39
    .line 5448
    .local v2, isObscuredByAnotherTask:Z
    :goto_39
    iget v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    #@3b
    if-ne v6, v1, :cond_74

    #@3d
    .line 5455
    .local v1, isLastActivity:Z
    :goto_3d
    if-nez v2, :cond_55

    #@3f
    if-eqz p1, :cond_71

    #@41
    if-eqz v1, :cond_71

    #@43
    iget-object v5, p2, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@45
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    iget-object v6, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    #@4b
    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v5

    #@53
    if-eqz v5, :cond_71

    #@55
    .line 5456
    :cond_55
    const-string v5, "Activity"

    #@57
    const-string/jumbo v6, "send broadcast to make floating apps to exit from the low profile mode"

    #@5a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 5457
    new-instance v0, Landroid/content/Intent;

    #@5f
    const-string v5, "com.lge.intent.action.FLOATING_WINDOW_EXIT_LOWPROFILE"

    #@61
    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@64
    .line 5458
    .local v0, i:Landroid/content/Intent;
    const-string/jumbo v5, "package"

    #@67
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6e
    .line 5459
    invoke-virtual {p0, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_71} :catch_76

    #@71
    .line 5467
    .end local v0           #i:Landroid/content/Intent;
    .end local v1           #isLastActivity:Z
    .end local v2           #isObscuredByAnotherTask:Z
    .end local v3           #tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v4           #ti:Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_71
    :goto_71
    return-void

    #@72
    .restart local v3       #tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .restart local v4       #ti:Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_72
    move v2, v5

    #@73
    .line 5447
    goto :goto_39

    #@74
    .restart local v2       #isObscuredByAnotherTask:Z
    :cond_74
    move v1, v5

    #@75
    .line 5448
    goto :goto_3d

    #@76
    .line 5463
    .end local v2           #isObscuredByAnotherTask:Z
    .end local v3           #tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v4           #ti:Landroid/app/ActivityManager$RunningTaskInfo;
    :catch_76
    move-exception v5

    #@77
    goto :goto_71
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 2012
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@7
    .line 2013
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@a
    .line 2014
    return-void
.end method

.method final attach(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/app/Instrumentation;Landroid/os/IBinder;ILandroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Landroid/app/Activity$NonConfigurationInstances;Landroid/content/res/Configuration;)V
    .registers 20
    .parameter "context"
    .parameter "aThread"
    .parameter "instr"
    .parameter "token"
    .parameter "ident"
    .parameter "application"
    .parameter "intent"
    .parameter "info"
    .parameter "title"
    .parameter "parent"
    .parameter "id"
    .parameter "lastNonConfigurationInstances"
    .parameter "config"

    #@0
    .prologue
    .line 5155
    invoke-virtual {p0, p1}, Landroid/app/Activity;->attachBaseContext(Landroid/content/Context;)V

    #@3
    .line 5157
    iget-object v1, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@5
    iget-object v2, p0, Landroid/app/Activity;->mContainer:Landroid/app/FragmentContainer;

    #@7
    const/4 v3, 0x0

    #@8
    invoke-virtual {v1, p0, v2, v3}, Landroid/app/FragmentManagerImpl;->attachActivity(Landroid/app/Activity;Landroid/app/FragmentContainer;Landroid/app/Fragment;)V

    #@b
    .line 5159
    invoke-static {p0}, Lcom/android/internal/policy/PolicyManager;->makeNewWindow(Landroid/content/Context;)Landroid/view/Window;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@11
    .line 5160
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@13
    invoke-virtual {v1, p0}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    #@16
    .line 5161
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@18
    invoke-virtual {v1}, Landroid/view/Window;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p0}, Landroid/view/LayoutInflater;->setPrivateFactory(Landroid/view/LayoutInflater$Factory2;)V

    #@1f
    .line 5162
    iget v1, p8, Landroid/content/pm/ActivityInfo;->softInputMode:I

    #@21
    if-eqz v1, :cond_2a

    #@23
    .line 5163
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@25
    iget v2, p8, Landroid/content/pm/ActivityInfo;->softInputMode:I

    #@27
    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    #@2a
    .line 5165
    :cond_2a
    iget v1, p8, Landroid/content/pm/ActivityInfo;->uiOptions:I

    #@2c
    if-eqz v1, :cond_35

    #@2e
    .line 5166
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@30
    iget v2, p8, Landroid/content/pm/ActivityInfo;->uiOptions:I

    #@32
    invoke-virtual {v1, v2}, Landroid/view/Window;->setUiOptions(I)V

    #@35
    .line 5168
    :cond_35
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@38
    move-result-object v1

    #@39
    iput-object v1, p0, Landroid/app/Activity;->mUiThread:Ljava/lang/Thread;

    #@3b
    .line 5170
    iput-object p2, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@3d
    .line 5171
    iput-object p3, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@3f
    .line 5172
    iput-object p4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@41
    .line 5173
    iput p5, p0, Landroid/app/Activity;->mIdent:I

    #@43
    .line 5174
    iput-object p6, p0, Landroid/app/Activity;->mApplication:Landroid/app/Application;

    #@45
    .line 5175
    iput-object p7, p0, Landroid/app/Activity;->mIntent:Landroid/content/Intent;

    #@47
    .line 5176
    invoke-virtual {p7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@4a
    move-result-object v1

    #@4b
    iput-object v1, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@4d
    .line 5177
    iput-object p8, p0, Landroid/app/Activity;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    #@4f
    .line 5178
    iput-object p9, p0, Landroid/app/Activity;->mTitle:Ljava/lang/CharSequence;

    #@51
    .line 5179
    move-object/from16 v0, p10

    #@53
    iput-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@55
    .line 5180
    move-object/from16 v0, p11

    #@57
    iput-object v0, p0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@59
    .line 5181
    move-object/from16 v0, p12

    #@5b
    iput-object v0, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@5d
    .line 5183
    iget-object v3, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@5f
    const-string/jumbo v1, "window"

    #@62
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@65
    move-result-object v1

    #@66
    check-cast v1, Landroid/view/WindowManager;

    #@68
    iget-object v4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6a
    iget-object v2, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@6c
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@6f
    move-result-object v5

    #@70
    iget v2, p8, Landroid/content/pm/ActivityInfo;->flags:I

    #@72
    and-int/lit16 v2, v2, 0x200

    #@74
    if-eqz v2, :cond_96

    #@76
    const/4 v2, 0x1

    #@77
    :goto_77
    invoke-virtual {v3, v1, v4, v5, v2}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;Z)V

    #@7a
    .line 5187
    iget-object v1, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@7c
    if-eqz v1, :cond_89

    #@7e
    .line 5188
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@80
    iget-object v2, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@82
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@85
    move-result-object v2

    #@86
    invoke-virtual {v1, v2}, Landroid/view/Window;->setContainer(Landroid/view/Window;)V

    #@89
    .line 5190
    :cond_89
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@8b
    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    #@8e
    move-result-object v1

    #@8f
    iput-object v1, p0, Landroid/app/Activity;->mWindowManager:Landroid/view/WindowManager;

    #@91
    .line 5191
    move-object/from16 v0, p13

    #@93
    iput-object v0, p0, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@95
    .line 5192
    return-void

    #@96
    .line 5183
    :cond_96
    const/4 v2, 0x0

    #@97
    goto :goto_77
.end method

.method final attach(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/app/Instrumentation;Landroid/os/IBinder;Landroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Landroid/app/Activity$NonConfigurationInstances;Landroid/content/res/Configuration;)V
    .registers 27
    .parameter "context"
    .parameter "aThread"
    .parameter "instr"
    .parameter "token"
    .parameter "application"
    .parameter "intent"
    .parameter "info"
    .parameter "title"
    .parameter "parent"
    .parameter "id"
    .parameter "lastNonConfigurationInstances"
    .parameter "config"

    #@0
    .prologue
    .line 5145
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object/from16 v2, p2

    #@5
    move-object/from16 v3, p3

    #@7
    move-object/from16 v4, p4

    #@9
    move-object/from16 v6, p5

    #@b
    move-object/from16 v7, p6

    #@d
    move-object/from16 v8, p7

    #@f
    move-object/from16 v9, p8

    #@11
    move-object/from16 v10, p9

    #@13
    move-object/from16 v11, p10

    #@15
    move-object/from16 v12, p11

    #@17
    move-object/from16 v13, p12

    #@19
    invoke-virtual/range {v0 .. v13}, Landroid/app/Activity;->attach(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/app/Instrumentation;Landroid/os/IBinder;ILandroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Landroid/app/Activity$NonConfigurationInstances;Landroid/content/res/Configuration;)V

    #@1c
    .line 5147
    return-void
.end method

.method public closeContextMenu()V
    .registers 3

    #@0
    .prologue
    .line 2982
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x6

    #@3
    invoke-virtual {v0, v1}, Landroid/view/Window;->closePanel(I)V

    #@6
    .line 2983
    return-void
.end method

.method public closeOptionsMenu()V
    .registers 3

    #@0
    .prologue
    .line 2923
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/view/Window;->closePanel(I)V

    #@6
    .line 2924
    return-void
.end method

.method public createPendingResult(ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    .registers 16
    .parameter "requestCode"
    .parameter "data"
    .parameter "flags"

    #@0
    .prologue
    .line 4391
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 4393
    .local v2, packageName:Ljava/lang/String;
    const/4 v0, 0x0

    #@5
    :try_start_5
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@8
    .line 4394
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@b
    move-result-object v0

    #@c
    const/4 v1, 0x3

    #@d
    iget-object v3, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@f
    if-nez v3, :cond_2f

    #@11
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@13
    :goto_13
    iget-object v4, p0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@15
    const/4 v5, 0x1

    #@16
    new-array v6, v5, [Landroid/content/Intent;

    #@18
    const/4 v5, 0x0

    #@19
    aput-object p2, v6, v5

    #@1b
    const/4 v7, 0x0

    #@1c
    const/4 v9, 0x0

    #@1d
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@20
    move-result v10

    #@21
    move v5, p1

    #@22
    move v8, p3

    #@23
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@26
    move-result-object v11

    #@27
    .line 4400
    .local v11, target:Landroid/content/IIntentSender;
    if-eqz v11, :cond_34

    #@29
    new-instance v0, Landroid/app/PendingIntent;

    #@2b
    invoke-direct {v0, v11}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V

    #@2e
    .line 4404
    .end local v11           #target:Landroid/content/IIntentSender;
    :goto_2e
    return-object v0

    #@2f
    .line 4394
    :cond_2f
    iget-object v3, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@31
    iget-object v3, v3, Landroid/app/Activity;->mToken:Landroid/os/IBinder;
    :try_end_33
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_33} :catch_36

    #@33
    goto :goto_13

    #@34
    .line 4400
    .restart local v11       #target:Landroid/content/IIntentSender;
    :cond_34
    const/4 v0, 0x0

    #@35
    goto :goto_2e

    #@36
    .line 4401
    .end local v11           #target:Landroid/content/IIntentSender;
    :catch_36
    move-exception v0

    #@37
    .line 4404
    const/4 v0, 0x0

    #@38
    goto :goto_2e
.end method

.method public final dismissDialog(I)V
    .registers 4
    .parameter "id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3190
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@2
    if-nez v1, :cond_9

    #@4
    .line 3191
    invoke-direct {p0, p1}, Landroid/app/Activity;->missingDialog(I)Ljava/lang/IllegalArgumentException;

    #@7
    move-result-object v1

    #@8
    throw v1

    #@9
    .line 3194
    :cond_9
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@b
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/app/Activity$ManagedDialog;

    #@11
    .line 3195
    .local v0, md:Landroid/app/Activity$ManagedDialog;
    if-nez v0, :cond_18

    #@13
    .line 3196
    invoke-direct {p0, p1}, Landroid/app/Activity;->missingDialog(I)Ljava/lang/IllegalArgumentException;

    #@16
    move-result-object v1

    #@17
    throw v1

    #@18
    .line 3198
    :cond_18
    iget-object v1, v0, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@1a
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    #@1d
    .line 3199
    return-void
.end method

.method dispatchActivityResult(Ljava/lang/String;IILandroid/content/Intent;)V
    .registers 7
    .parameter "who"
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 5419
    iget-object v1, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@2
    invoke-virtual {v1}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@5
    .line 5420
    if-nez p1, :cond_b

    #@7
    .line 5421
    invoke-virtual {p0, p2, p3, p4}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    #@a
    .line 5428
    :cond_a
    :goto_a
    return-void

    #@b
    .line 5423
    :cond_b
    iget-object v1, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@d
    invoke-virtual {v1, p1}, Landroid/app/FragmentManagerImpl;->findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;

    #@10
    move-result-object v0

    #@11
    .line 5424
    .local v0, frag:Landroid/app/Fragment;
    if-eqz v0, :cond_a

    #@13
    .line 5425
    invoke-virtual {v0, p2, p3, p4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    #@16
    goto :goto_a
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 2532
    invoke-virtual {p0}, Landroid/app/Activity;->onUserInteraction()V

    #@3
    .line 2533
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_f

    #@d
    .line 2534
    const/4 v0, 0x1

    #@e
    .line 2536
    :goto_e
    return v0

    #@f
    :cond_f
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v0

    #@13
    goto :goto_e
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 2455
    invoke-virtual {p0}, Landroid/app/Activity;->onUserInteraction()V

    #@3
    .line 2456
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@6
    move-result-object v1

    #@7
    .line 2457
    .local v1, win:Landroid/view/Window;
    invoke-virtual {v1, p1}, Landroid/view/Window;->superDispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_f

    #@d
    .line 2458
    const/4 v2, 0x1

    #@e
    .line 2462
    :goto_e
    return v2

    #@f
    .line 2460
    :cond_f
    iget-object v0, p0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@11
    .line 2461
    .local v0, decor:Landroid/view/View;
    if-nez v0, :cond_17

    #@13
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@16
    move-result-object v0

    #@17
    .line 2462
    :cond_17
    if-eqz v0, :cond_22

    #@19
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@1c
    move-result-object v2

    #@1d
    :goto_1d
    invoke-virtual {p1, p0, v2, p0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    #@20
    move-result v2

    #@21
    goto :goto_e

    #@22
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_1d
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2476
    invoke-virtual {p0}, Landroid/app/Activity;->onUserInteraction()V

    #@3
    .line 2477
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_f

    #@d
    .line 2478
    const/4 v0, 0x1

    #@e
    .line 2480
    :goto_e
    return v0

    #@f
    :cond_f
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@12
    move-result v0

    #@13
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    #@16
    move-result v0

    #@17
    goto :goto_e
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v5, -0x1

    #@2
    .line 2540
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9
    move-result-object v4

    #@a
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@d
    .line 2541
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {p1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@14
    .line 2543
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@1b
    move-result-object v1

    #@1c
    .line 2544
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1e
    if-ne v4, v5, :cond_3a

    #@20
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@22
    if-ne v4, v5, :cond_3a

    #@24
    move v0, v3

    #@25
    .line 2546
    .local v0, isFullScreen:Z
    :goto_25
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFullScreen(Z)V

    #@28
    .line 2548
    invoke-virtual {p0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    #@2b
    move-result-object v2

    #@2c
    .line 2549
    .local v2, title:Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2f
    move-result v4

    #@30
    if-nez v4, :cond_39

    #@32
    .line 2550
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@35
    move-result-object v4

    #@36
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@39
    .line 2553
    :cond_39
    return v3

    #@3a
    .line 2544
    .end local v0           #isFullScreen:Z
    .end local v2           #title:Ljava/lang/CharSequence;
    :cond_3a
    const/4 v0, 0x0

    #@3b
    goto :goto_25
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 2494
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 2495
    invoke-virtual {p0}, Landroid/app/Activity;->onUserInteraction()V

    #@9
    .line 2497
    :cond_9
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_15

    #@13
    .line 2498
    const/4 v0, 0x1

    #@14
    .line 2500
    :goto_14
    return v0

    #@15
    :cond_15
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@18
    move-result v0

    #@19
    goto :goto_14
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 2514
    invoke-virtual {p0}, Landroid/app/Activity;->onUserInteraction()V

    #@3
    .line 2515
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_f

    #@d
    .line 2516
    const/4 v0, 0x1

    #@e
    .line 2518
    :goto_e
    return v0

    #@f
    :cond_f
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    #@12
    move-result v0

    #@13
    goto :goto_e
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 4859
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dumpInner(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@3
    .line 4860
    return-void
.end method

.method dumpInner(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 4863
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v1, "Local Activity "

    #@5
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    .line 4864
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@b
    move-result v1

    #@c
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    .line 4865
    const-string v1, " State:"

    #@15
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18
    .line 4866
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "  "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    .line 4867
    .local v0, innerPrefix:Ljava/lang/String;
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    const-string/jumbo v1, "mResumed="

    #@31
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@34
    .line 4868
    iget-boolean v1, p0, Landroid/app/Activity;->mResumed:Z

    #@36
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@39
    const-string v1, " mStopped="

    #@3b
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e
    .line 4869
    iget-boolean v1, p0, Landroid/app/Activity;->mStopped:Z

    #@40
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    #@43
    const-string v1, " mFinished="

    #@45
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48
    .line 4870
    iget-boolean v1, p0, Landroid/app/Activity;->mFinished:Z

    #@4a
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@4d
    .line 4871
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@50
    const-string/jumbo v1, "mLoadersStarted="

    #@53
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    .line 4872
    iget-boolean v1, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@58
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@5b
    .line 4873
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5e
    const-string/jumbo v1, "mChangingConfigurations="

    #@61
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@64
    .line 4874
    iget-boolean v1, p0, Landroid/app/Activity;->mChangingConfigurations:Z

    #@66
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    #@69
    .line 4875
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6c
    const-string/jumbo v1, "mCurrentConfig="

    #@6f
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72
    .line 4876
    iget-object v1, p0, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@74
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@77
    .line 4877
    iget-object v1, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@79
    if-eqz v1, :cond_ad

    #@7b
    .line 4878
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7e
    const-string v1, "Loader Manager "

    #@80
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@83
    .line 4879
    iget-object v1, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@85
    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@88
    move-result v1

    #@89
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@8c
    move-result-object v1

    #@8d
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@90
    .line 4880
    const-string v1, ":"

    #@92
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@95
    .line 4881
    iget-object v1, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@97
    new-instance v2, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    const-string v3, "  "

    #@a2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v2

    #@a6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v2

    #@aa
    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/app/LoaderManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@ad
    .line 4883
    :cond_ad
    iget-object v1, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@af
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/app/FragmentManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@b2
    .line 4884
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b5
    const-string v1, "View Hierarchy:"

    #@b7
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ba
    .line 4885
    new-instance v1, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v1

    #@c3
    const-string v2, "  "

    #@c5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v1

    #@c9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v1

    #@cd
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@d0
    move-result-object v2

    #@d1
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@d4
    move-result-object v2

    #@d5
    invoke-direct {p0, v1, p3, v2}, Landroid/app/Activity;->dumpViewHierarchy(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    #@d8
    .line 4886
    return-void
.end method

.method public findViewById(I)Landroid/view/View;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 1922
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public finish()V
    .registers 5

    #@0
    .prologue
    .line 4231
    iget-object v2, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v2, :cond_23

    #@4
    .line 4234
    monitor-enter p0

    #@5
    .line 4235
    :try_start_5
    iget v0, p0, Landroid/app/Activity;->mResultCode:I

    #@7
    .line 4236
    .local v0, resultCode:I
    iget-object v1, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@9
    .line 4237
    .local v1, resultData:Landroid/content/Intent;
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_20

    #@a
    .line 4240
    if-eqz v1, :cond_10

    #@c
    .line 4241
    const/4 v2, 0x0

    #@d
    :try_start_d
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@10
    .line 4243
    :cond_10
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@16
    invoke-interface {v2, v3, v0, v1}, Landroid/app/IActivityManager;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;)Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_1f

    #@1c
    .line 4245
    const/4 v2, 0x1

    #@1d
    iput-boolean v2, p0, Landroid/app/Activity;->mFinished:Z
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_1f} :catch_29

    #@1f
    .line 4253
    .end local v0           #resultCode:I
    .end local v1           #resultData:Landroid/content/Intent;
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 4237
    :catchall_20
    move-exception v2

    #@21
    :try_start_21
    monitor-exit p0
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v2

    #@23
    .line 4251
    :cond_23
    iget-object v2, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@25
    invoke-virtual {v2, p0}, Landroid/app/Activity;->finishFromChild(Landroid/app/Activity;)V

    #@28
    goto :goto_1f

    #@29
    .line 4247
    .restart local v0       #resultCode:I
    .restart local v1       #resultData:Landroid/content/Intent;
    :catch_29
    move-exception v2

    #@2a
    goto :goto_1f
.end method

.method public finishActivity(I)V
    .registers 5
    .parameter "requestCode"

    #@0
    .prologue
    .line 4308
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 4310
    :try_start_4
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@a
    iget-object v2, p0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@c
    invoke-interface {v0, v1, v2, p1}, Landroid/app/IActivityManager;->finishSubActivity(Landroid/os/IBinder;Ljava/lang/String;I)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_16

    #@f
    .line 4318
    :goto_f
    return-void

    #@10
    .line 4316
    :cond_10
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@12
    invoke-virtual {v0, p0, p1}, Landroid/app/Activity;->finishActivityFromChild(Landroid/app/Activity;I)V

    #@15
    goto :goto_f

    #@16
    .line 4312
    :catch_16
    move-exception v0

    #@17
    goto :goto_f
.end method

.method public finishActivityFromChild(Landroid/app/Activity;I)V
    .registers 6
    .parameter "child"
    .parameter "requestCode"

    #@0
    .prologue
    .line 4330
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    iget-object v2, p1, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@8
    invoke-interface {v0, v1, v2, p2}, Landroid/app/IActivityManager;->finishSubActivity(Landroid/os/IBinder;Ljava/lang/String;I)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 4335
    :goto_b
    return-void

    #@c
    .line 4332
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public finishAffinity()V
    .registers 3

    #@0
    .prologue
    .line 4270
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 4271
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Can not be called from an embedded activity"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 4273
    :cond_c
    iget v0, p0, Landroid/app/Activity;->mResultCode:I

    #@e
    if-nez v0, :cond_14

    #@10
    iget-object v0, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@12
    if-eqz v0, :cond_1c

    #@14
    .line 4274
    :cond_14
    new-instance v0, Ljava/lang/IllegalStateException;

    #@16
    const-string v1, "Can not be called to deliver a result"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 4277
    :cond_1c
    :try_start_1c
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1f
    move-result-object v0

    #@20
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@22
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->finishActivityAffinity(Landroid/os/IBinder;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2b

    #@28
    .line 4278
    const/4 v0, 0x1

    #@29
    iput-boolean v0, p0, Landroid/app/Activity;->mFinished:Z
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_2b} :catch_2c

    #@2b
    .line 4283
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 4280
    :catch_2c
    move-exception v0

    #@2d
    goto :goto_2b
.end method

.method public finishFromChild(Landroid/app/Activity;)V
    .registers 2
    .parameter "child"

    #@0
    .prologue
    .line 4295
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    #@3
    .line 4296
    return-void
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .registers 2

    #@0
    .prologue
    .line 1931
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@3
    .line 1932
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@5
    return-object v0
.end method

.method public final getActivityToken()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 5196
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0}, Landroid/app/Activity;->getActivityToken()Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    iget-object v0, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@d
    goto :goto_a
.end method

.method public final getApplication()Landroid/app/Application;
    .registers 2

    #@0
    .prologue
    .line 794
    iget-object v0, p0, Landroid/app/Activity;->mApplication:Landroid/app/Application;

    #@2
    return-object v0
.end method

.method public getCallingActivity()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 4138
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getCallingActivity(Landroid/os/IBinder;)Landroid/content/ComponentName;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 4140
    :goto_a
    return-object v1

    #@b
    .line 4139
    :catch_b
    move-exception v0

    #@c
    .line 4140
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public getCallingPackage()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 4116
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getCallingPackage(Landroid/os/IBinder;)Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 4118
    :goto_a
    return-object v1

    #@b
    .line 4117
    :catch_b
    move-exception v0

    #@c
    .line 4118
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public getChangingConfigurations()I
    .registers 2

    #@0
    .prologue
    .line 1569
    iget v0, p0, Landroid/app/Activity;->mConfigChangeFlags:I

    #@2
    return v0
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 4528
    iget-object v0, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@2
    return-object v0
.end method

.method public getCurrentFocus()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 862
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@6
    invoke-virtual {v0}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getFragmentManager()Landroid/app/FragmentManager;
    .registers 2

    #@0
    .prologue
    .line 1739
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@2
    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 775
    iget-object v0, p0, Landroid/app/Activity;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method getLastNonConfigurationChildInstances()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1673
    iget-object v0, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@6
    iget-object v0, v0, Landroid/app/Activity$NonConfigurationInstances;->children:Ljava/util/HashMap;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getLastNonConfigurationInstance()Ljava/lang/Object;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1595
    iget-object v0, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@6
    iget-object v0, v0, Landroid/app/Activity$NonConfigurationInstances;->activity:Ljava/lang/Object;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getLayoutInflater()Landroid/view/LayoutInflater;
    .registers 2

    #@0
    .prologue
    .line 3380
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/view/Window;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getLoaderManager()Landroid/app/LoaderManager;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 828
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 829
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@7
    .line 833
    :goto_7
    return-object v0

    #@8
    .line 831
    :cond_8
    iput-boolean v2, p0, Landroid/app/Activity;->mCheckedForLoaderManager:Z

    #@a
    .line 832
    const/4 v0, 0x0

    #@b
    iget-boolean v1, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@d
    invoke-virtual {p0, v0, v1, v2}, Landroid/app/Activity;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@13
    .line 833
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@15
    goto :goto_7
.end method

.method getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;
    .registers 6
    .parameter "who"
    .parameter "started"
    .parameter "create"

    #@0
    .prologue
    .line 837
    iget-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@2
    if-nez v1, :cond_b

    #@4
    .line 838
    new-instance v1, Ljava/util/HashMap;

    #@6
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@b
    .line 840
    :cond_b
    iget-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@d
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/app/LoaderManagerImpl;

    #@13
    .line 841
    .local v0, lm:Landroid/app/LoaderManagerImpl;
    if-nez v0, :cond_22

    #@15
    .line 842
    if-eqz p3, :cond_21

    #@17
    .line 843
    new-instance v0, Landroid/app/LoaderManagerImpl;

    #@19
    .end local v0           #lm:Landroid/app/LoaderManagerImpl;
    invoke-direct {v0, p1, p0, p2}, Landroid/app/LoaderManagerImpl;-><init>(Ljava/lang/String;Landroid/app/Activity;Z)V

    #@1c
    .line 844
    .restart local v0       #lm:Landroid/app/LoaderManagerImpl;
    iget-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 849
    :cond_21
    :goto_21
    return-object v0

    #@22
    .line 847
    :cond_22
    invoke-virtual {v0, p0}, Landroid/app/LoaderManagerImpl;->updateActivity(Landroid/app/Activity;)V

    #@25
    goto :goto_21
.end method

.method public getLocalClassName()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 4511
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 4512
    .local v2, pkg:Ljava/lang/String;
    iget-object v3, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@6
    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 4513
    .local v0, cls:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@d
    move-result v1

    #@e
    .line 4514
    .local v1, packageLen:I
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_22

    #@14
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@17
    move-result v3

    #@18
    if-le v3, v1, :cond_22

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v3

    #@1e
    const/16 v4, 0x2e

    #@20
    if-eq v3, v4, :cond_23

    #@22
    .line 4518
    .end local v0           #cls:Ljava/lang/String;
    :cond_22
    :goto_22
    return-object v0

    #@23
    .restart local v0       #cls:Ljava/lang/String;
    :cond_23
    add-int/lit8 v3, v1, 0x1

    #@25
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    goto :goto_22
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .registers 3

    #@0
    .prologue
    .line 3388
    iget-object v0, p0, Landroid/app/Activity;->mMenuInflater:Landroid/view/MenuInflater;

    #@2
    if-nez v0, :cond_18

    #@4
    .line 3389
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@7
    .line 3390
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@9
    if-eqz v0, :cond_1b

    #@b
    .line 3391
    new-instance v0, Landroid/view/MenuInflater;

    #@d
    iget-object v1, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@f
    invoke-virtual {v1}, Lcom/android/internal/app/ActionBarImpl;->getThemedContext()Landroid/content/Context;

    #@12
    move-result-object v1

    #@13
    invoke-direct {v0, v1, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    #@16
    iput-object v0, p0, Landroid/app/Activity;->mMenuInflater:Landroid/view/MenuInflater;

    #@18
    .line 3396
    :cond_18
    :goto_18
    iget-object v0, p0, Landroid/app/Activity;->mMenuInflater:Landroid/view/MenuInflater;

    #@1a
    return-object v0

    #@1b
    .line 3393
    :cond_1b
    new-instance v0, Landroid/view/MenuInflater;

    #@1d
    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    #@20
    iput-object v0, p0, Landroid/app/Activity;->mMenuInflater:Landroid/view/MenuInflater;

    #@22
    goto :goto_18
.end method

.method public final getParent()Landroid/app/Activity;
    .registers 2

    #@0
    .prologue
    .line 804
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    return-object v0
.end method

.method public getParentActivityIntent()Landroid/content/Intent;
    .registers 10

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 5114
    iget-object v6, p0, Landroid/app/Activity;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    #@3
    iget-object v4, v6, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@5
    .line 5115
    .local v4, parentName:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_c

    #@b
    .line 5131
    :goto_b
    return-object v3

    #@c
    .line 5120
    :cond_c
    new-instance v5, Landroid/content/ComponentName;

    #@e
    invoke-direct {v5, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@11
    .line 5122
    .local v5, target:Landroid/content/ComponentName;
    :try_start_11
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@14
    move-result-object v6

    #@15
    const/4 v7, 0x0

    #@16
    invoke-virtual {v6, v5, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@19
    move-result-object v2

    #@1a
    .line 5123
    .local v2, parentInfo:Landroid/content/pm/ActivityInfo;
    iget-object v1, v2, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@1c
    .line 5124
    .local v1, parentActivity:Ljava/lang/String;
    if-nez v1, :cond_23

    #@1e
    invoke-static {v5}, Landroid/content/Intent;->makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@21
    move-result-object v3

    #@22
    .line 5127
    .local v3, parentIntent:Landroid/content/Intent;
    :goto_22
    goto :goto_b

    #@23
    .line 5124
    .end local v3           #parentIntent:Landroid/content/Intent;
    :cond_23
    new-instance v6, Landroid/content/Intent;

    #@25
    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    #@28
    invoke-virtual {v6, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_2b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11 .. :try_end_2b} :catch_2d

    #@2b
    move-result-object v3

    #@2c
    goto :goto_22

    #@2d
    .line 5128
    .end local v1           #parentActivity:Ljava/lang/String;
    .end local v2           #parentInfo:Landroid/content/pm/ActivityInfo;
    :catch_2d
    move-exception v0

    #@2e
    .line 5129
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "Activity"

    #@30
    new-instance v7, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v8, "getParentActivityIntent: bad parentActivityName \'"

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    const-string v8, "\' in manifest"

    #@41
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v7

    #@45
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_b
.end method

.method public getPreferences(I)Landroid/content/SharedPreferences;
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 4545
    invoke-virtual {p0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getRequestedOrientation()I
    .registers 3

    #@0
    .prologue
    .line 4440
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_f

    #@4
    .line 4442
    :try_start_4
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@a
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->getRequestedOrientation(Landroid/os/IBinder;)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_16

    #@d
    move-result v0

    #@e
    .line 4450
    :goto_e
    return v0

    #@f
    .line 4448
    :cond_f
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@11
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    #@14
    move-result v0

    #@15
    goto :goto_e

    #@16
    .line 4444
    :catch_16
    move-exception v0

    #@17
    .line 4450
    const/4 v0, -0x1

    #@18
    goto :goto_e
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 4558
    invoke-virtual {p0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 4559
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "System services not available to Activities before onCreate()"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 4563
    :cond_e
    const-string/jumbo v0, "window"

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1a

    #@17
    .line 4564
    iget-object v0, p0, Landroid/app/Activity;->mWindowManager:Landroid/view/WindowManager;

    #@19
    .line 4569
    :goto_19
    return-object v0

    #@1a
    .line 4565
    :cond_1a
    const-string/jumbo v0, "search"

    #@1d
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_29

    #@23
    .line 4566
    invoke-direct {p0}, Landroid/app/Activity;->ensureSearchManager()V

    #@26
    .line 4567
    iget-object v0, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@28
    goto :goto_19

    #@29
    .line 4569
    :cond_29
    invoke-super {p0, p1}, Landroid/view/ContextThemeWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    goto :goto_19
.end method

.method public getTaskId()I
    .registers 5

    #@0
    .prologue
    .line 4461
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    const/4 v3, 0x0

    #@7
    invoke-interface {v1, v2, v3}, Landroid/app/IActivityManager;->getTaskForActivity(Landroid/os/IBinder;Z)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 4464
    :goto_b
    return v1

    #@c
    .line 4463
    :catch_c
    move-exception v0

    #@d
    .line 4464
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@e
    goto :goto_b
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 4603
    iget-object v0, p0, Landroid/app/Activity;->mTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public final getTitleColor()I
    .registers 2

    #@0
    .prologue
    .line 4607
    iget v0, p0, Landroid/app/Activity;->mTitleColor:I

    #@2
    return v0
.end method

.method public final getVolumeControlStream()I
    .registers 2

    #@0
    .prologue
    .line 4726
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/view/Window;->getVolumeControlStream()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getWindow()Landroid/view/Window;
    .registers 2

    #@0
    .prologue
    .line 821
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    return-object v0
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .registers 2

    #@0
    .prologue
    .line 809
    iget-object v0, p0, Landroid/app/Activity;->mWindowManager:Landroid/view/WindowManager;

    #@2
    return-object v0
.end method

.method public hasWindowFocus()Z
    .registers 4

    #@0
    .prologue
    .line 2435
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    .line 2436
    .local v1, w:Landroid/view/Window;
    if-eqz v1, :cond_11

    #@6
    .line 2437
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    .line 2438
    .local v0, d:Landroid/view/View;
    if-eqz v0, :cond_11

    #@c
    .line 2439
    invoke-virtual {v0}, Landroid/view/View;->hasWindowFocus()Z

    #@f
    move-result v2

    #@10
    .line 2442
    .end local v0           #d:Landroid/view/View;
    :goto_10
    return v2

    #@11
    :cond_11
    const/4 v2, 0x0

    #@12
    goto :goto_10
.end method

.method invalidateFragment(Ljava/lang/String;)V
    .registers 4
    .parameter "who"

    #@0
    .prologue
    .line 1744
    iget-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@2
    if-eqz v1, :cond_1a

    #@4
    .line 1745
    iget-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/app/LoaderManagerImpl;

    #@c
    .line 1746
    .local v0, lm:Landroid/app/LoaderManagerImpl;
    if-eqz v0, :cond_1a

    #@e
    iget-boolean v1, v0, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    #@10
    if-nez v1, :cond_1a

    #@12
    .line 1747
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    #@15
    .line 1748
    iget-object v1, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@17
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 1751
    .end local v0           #lm:Landroid/app/LoaderManagerImpl;
    :cond_1a
    return-void
.end method

.method public invalidateOptionsMenu()V
    .registers 3

    #@0
    .prologue
    .line 2705
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/view/Window;->invalidatePanelMenu(I)V

    #@6
    .line 2706
    return-void
.end method

.method public isChangingConfigurations()Z
    .registers 2

    #@0
    .prologue
    .line 4206
    iget-boolean v0, p0, Landroid/app/Activity;->mChangingConfigurations:Z

    #@2
    return v0
.end method

.method public final isChild()Z
    .registers 2

    #@0
    .prologue
    .line 799
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isDestroyed()Z
    .registers 2

    #@0
    .prologue
    .line 4193
    iget-boolean v0, p0, Landroid/app/Activity;->mDestroyed:Z

    #@2
    return v0
.end method

.method public isFinishing()Z
    .registers 2

    #@0
    .prologue
    .line 4185
    iget-boolean v0, p0, Landroid/app/Activity;->mFinished:Z

    #@2
    return v0
.end method

.method public isImmersive()Z
    .registers 4

    #@0
    .prologue
    .line 4922
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->isImmersive(Landroid/os/IBinder;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 4924
    :goto_a
    return v1

    #@b
    .line 4923
    :catch_b
    move-exception v0

    #@c
    .line 4924
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public final isResumed()Z
    .registers 2

    #@0
    .prologue
    .line 5411
    iget-boolean v0, p0, Landroid/app/Activity;->mResumed:Z

    #@2
    return v0
.end method

.method public isTaskRoot()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 4476
    :try_start_2
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5
    move-result-object v3

    #@6
    iget-object v4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@8
    const/4 v5, 0x1

    #@9
    invoke-interface {v3, v4, v5}, Landroid/app/IActivityManager;->getTaskForActivity(Landroid/os/IBinder;Z)I
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_c} :catch_12

    #@c
    move-result v3

    #@d
    if-ltz v3, :cond_10

    #@f
    .line 4479
    :goto_f
    return v1

    #@10
    :cond_10
    move v1, v2

    #@11
    .line 4476
    goto :goto_f

    #@12
    .line 4478
    :catch_12
    move-exception v0

    #@13
    .local v0, e:Landroid/os/RemoteException;
    move v1, v2

    #@14
    .line 4479
    goto :goto_f
.end method

.method makeVisible()V
    .registers 4

    #@0
    .prologue
    .line 4165
    iget-boolean v1, p0, Landroid/app/Activity;->mWindowAdded:Z

    #@2
    if-nez v1, :cond_18

    #@4
    .line 4166
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    #@7
    move-result-object v0

    #@8
    .line 4167
    .local v0, wm:Landroid/view/ViewManager;
    iget-object v1, p0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@a
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@11
    move-result-object v2

    #@12
    invoke-interface {v0, v1, v2}, Landroid/view/ViewManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@15
    .line 4168
    const/4 v1, 0x1

    #@16
    iput-boolean v1, p0, Landroid/app/Activity;->mWindowAdded:Z

    #@18
    .line 4170
    .end local v0           #wm:Landroid/view/ViewManager;
    :cond_18
    iget-object v1, p0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@1a
    const/4 v2, 0x0

    #@1b
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@1e
    .line 4171
    return-void
.end method

.method public final managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "sortOrder"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1794
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    const/4 v4, 0x0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move-object v3, p3

    #@8
    move-object v5, p4

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v6

    #@d
    .line 1795
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_12

    #@f
    .line 1796
    invoke-virtual {p0, v6}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    #@12
    .line 1798
    :cond_12
    return-object v6
.end method

.method public final managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 13
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1834
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    move-object v3, p3

    #@7
    move-object v4, p4

    #@8
    move-object v5, p5

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@c
    move-result-object v6

    #@d
    .line 1835
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_12

    #@f
    .line 1836
    invoke-virtual {p0, v6}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    #@12
    .line 1838
    :cond_12
    return-object v6
.end method

.method public moveTaskToBack(Z)Z
    .registers 4
    .parameter "nonRoot"

    #@0
    .prologue
    .line 4496
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    invoke-interface {v0, v1, p1}, Landroid/app/IActivityManager;->moveActivityTaskToBack(Landroid/os/IBinder;Z)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v0

    #@a
    .line 4501
    :goto_a
    return v0

    #@b
    .line 4498
    :catch_b
    move-exception v0

    #@c
    .line 4501
    const/4 v0, 0x0

    #@d
    goto :goto_a
.end method

.method public navigateUpTo(Landroid/content/Intent;)Z
    .registers 10
    .parameter "upIntent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 5056
    iget-object v6, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@3
    if-nez v6, :cond_3a

    #@5
    .line 5057
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@8
    move-result-object v0

    #@9
    .line 5058
    .local v0, destInfo:Landroid/content/ComponentName;
    if-nez v0, :cond_1f

    #@b
    .line 5059
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {p1, v6}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    #@12
    move-result-object v0

    #@13
    .line 5060
    if-nez v0, :cond_16

    #@15
    .line 5082
    .end local v0           #destInfo:Landroid/content/ComponentName;
    :goto_15
    return v5

    #@16
    .line 5063
    .restart local v0       #destInfo:Landroid/content/ComponentName;
    :cond_16
    new-instance v4, Landroid/content/Intent;

    #@18
    invoke-direct {v4, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@1b
    .line 5064
    .end local p1
    .local v4, upIntent:Landroid/content/Intent;
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@1e
    move-object p1, v4

    #@1f
    .line 5068
    .end local v4           #upIntent:Landroid/content/Intent;
    .restart local p1
    :cond_1f
    monitor-enter p0

    #@20
    .line 5069
    :try_start_20
    iget v2, p0, Landroid/app/Activity;->mResultCode:I

    #@22
    .line 5070
    .local v2, resultCode:I
    iget-object v3, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@24
    .line 5071
    .local v3, resultData:Landroid/content/Intent;
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_20 .. :try_end_25} :catchall_35

    #@25
    .line 5072
    if-eqz v3, :cond_2a

    #@27
    .line 5073
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAllowFds(Z)V

    #@2a
    .line 5076
    :cond_2a
    :try_start_2a
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@2d
    move-result-object v6

    #@2e
    iget-object v7, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@30
    invoke-interface {v6, v7, p1, v2, v3}, Landroid/app/IActivityManager;->navigateUpTo(Landroid/os/IBinder;Landroid/content/Intent;ILandroid/content/Intent;)Z
    :try_end_33
    .catch Landroid/os/RemoteException; {:try_start_2a .. :try_end_33} :catch_38

    #@33
    move-result v5

    #@34
    goto :goto_15

    #@35
    .line 5071
    .end local v2           #resultCode:I
    .end local v3           #resultData:Landroid/content/Intent;
    :catchall_35
    move-exception v5

    #@36
    :try_start_36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v5

    #@38
    .line 5078
    .restart local v2       #resultCode:I
    .restart local v3       #resultData:Landroid/content/Intent;
    :catch_38
    move-exception v1

    #@39
    .line 5079
    .local v1, e:Landroid/os/RemoteException;
    goto :goto_15

    #@3a
    .line 5082
    .end local v0           #destInfo:Landroid/content/ComponentName;
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v2           #resultCode:I
    .end local v3           #resultData:Landroid/content/Intent;
    :cond_3a
    iget-object v5, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@3c
    invoke-virtual {v5, p0, p1}, Landroid/app/Activity;->navigateUpToFromChild(Landroid/app/Activity;Landroid/content/Intent;)Z

    #@3f
    move-result v5

    #@40
    goto :goto_15
.end method

.method public navigateUpToFromChild(Landroid/app/Activity;Landroid/content/Intent;)Z
    .registers 4
    .parameter "child"
    .parameter "upIntent"

    #@0
    .prologue
    .line 5099
    invoke-virtual {p0, p2}, Landroid/app/Activity;->navigateUpTo(Landroid/content/Intent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 4998
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 4989
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 4360
    return-void
.end method

.method protected onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .registers 5
    .parameter "theme"
    .parameter "resid"
    .parameter "first"

    #@0
    .prologue
    .line 3402
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_8

    #@4
    .line 3403
    invoke-super {p0, p1, p2, p3}, Landroid/view/ContextThemeWrapper;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    #@7
    .line 3412
    :goto_7
    return-void

    #@8
    .line 3406
    :cond_8
    :try_start_8
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@a
    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {p1, v0}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_11} :catch_16

    #@11
    .line 3410
    :goto_11
    const/4 v0, 0x0

    #@12
    invoke-virtual {p1, p2, v0}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    #@15
    goto :goto_7

    #@16
    .line 3407
    :catch_16
    move-exception v0

    #@17
    goto :goto_11
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .registers 2
    .parameter "fragment"

    #@0
    .prologue
    .line 1759
    return-void
.end method

.method public onAttachedToWindow()V
    .registers 1

    #@0
    .prologue
    .line 2414
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    #@0
    .prologue
    .line 2254
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@2
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->popBackStackImmediate()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_b

    #@8
    .line 2255
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    #@b
    .line 2257
    :cond_b
    return-void
.end method

.method protected onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "childActivity"
    .parameter "title"

    #@0
    .prologue
    .line 4623
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 1538
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@3
    .line 1540
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@5
    invoke-virtual {v0, p1}, Landroid/app/FragmentManagerImpl;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    #@8
    .line 1542
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 1544
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@e
    invoke-virtual {v0, p1}, Landroid/view/Window;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@11
    .line 1547
    :cond_11
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 1550
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@17
    invoke-virtual {v0, p1}, Lcom/android/internal/app/ActionBarImpl;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@1a
    .line 1552
    :cond_1a
    return-void
.end method

.method public onContentChanged()V
    .registers 1

    #@0
    .prologue
    .line 2374
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 3003
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 3004
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    #@9
    move-result v0

    #@a
    .line 3006
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 3017
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 3018
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onContextMenuClosed(Landroid/view/Menu;)V

    #@9
    .line 3020
    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 893
    const-string/jumbo v2, "user"

    #@4
    sget-object v3, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_3a

    #@c
    .line 894
    const-string v2, "debug.strictmode"

    #@e
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 897
    .local v1, prop_debug_strictmode:Ljava/lang/String;
    const-string v2, "1"

    #@14
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_3a

    #@1a
    .line 898
    new-instance v2, Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@1c
    invoke-direct {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    #@1f
    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDropBox()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    #@36
    move-result-object v2

    #@37
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@3a
    .line 910
    .end local v1           #prop_debug_strictmode:Ljava/lang/String;
    :cond_3a
    iget-object v2, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@3c
    if-eqz v2, :cond_44

    #@3e
    .line 911
    iget-object v2, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@40
    iget-object v2, v2, Landroid/app/Activity$NonConfigurationInstances;->loaders:Ljava/util/HashMap;

    #@42
    iput-object v2, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@44
    .line 913
    :cond_44
    iget-object v2, p0, Landroid/app/Activity;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    #@46
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@48
    if-eqz v2, :cond_50

    #@4a
    .line 914
    iget-object v2, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@4c
    if-nez v2, :cond_74

    #@4e
    .line 915
    iput-boolean v4, p0, Landroid/app/Activity;->mEnableDefaultActionBarUp:Z

    #@50
    .line 920
    :cond_50
    :goto_50
    if-eqz p1, :cond_65

    #@52
    .line 921
    const-string v2, "android:fragments"

    #@54
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@57
    move-result-object v0

    #@58
    .line 922
    .local v0, p:Landroid/os/Parcelable;
    iget-object v3, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@5a
    iget-object v2, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@5c
    if-eqz v2, :cond_7a

    #@5e
    iget-object v2, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@60
    iget-object v2, v2, Landroid/app/Activity$NonConfigurationInstances;->fragments:Ljava/util/ArrayList;

    #@62
    :goto_62
    invoke-virtual {v3, v0, v2}, Landroid/app/FragmentManagerImpl;->restoreAllState(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    #@65
    .line 925
    .end local v0           #p:Landroid/os/Parcelable;
    :cond_65
    iget-object v2, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@67
    invoke-virtual {v2}, Landroid/app/FragmentManagerImpl;->dispatchCreate()V

    #@6a
    .line 926
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2, p0, p1}, Landroid/app/Application;->dispatchActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@71
    .line 927
    iput-boolean v4, p0, Landroid/app/Activity;->mCalled:Z

    #@73
    .line 928
    return-void

    #@74
    .line 917
    :cond_74
    iget-object v2, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@76
    invoke-virtual {v2, v4}, Lcom/android/internal/app/ActionBarImpl;->setDefaultDisplayHomeAsUpEnabled(Z)V

    #@79
    goto :goto_50

    #@7a
    .line 922
    .restart local v0       #p:Landroid/os/Parcelable;
    :cond_7a
    const/4 v2, 0x0

    #@7b
    goto :goto_62
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 4
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    #@0
    .prologue
    .line 2940
    return-void
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1411
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter "id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3027
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4
    .parameter "id"
    .parameter "args"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3065
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public onCreateNavigateUpTaskStack(Landroid/app/TaskStackBuilder;)V
    .registers 2
    .parameter "builder"

    #@0
    .prologue
    .line 2879
    invoke-virtual {p1, p0}, Landroid/app/TaskStackBuilder;->addParentStack(Landroid/app/Activity;)Landroid/app/TaskStackBuilder;

    #@3
    .line 2880
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 2737
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 2738
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    #@9
    move-result v0

    #@a
    .line 2740
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    goto :goto_a
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .registers 6
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 2576
    if-nez p1, :cond_12

    #@2
    .line 2577
    invoke-virtual {p0, p2}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    #@5
    move-result v0

    #@6
    .line 2578
    .local v0, show:Z
    iget-object v1, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@8
    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    #@f
    move-result v1

    #@10
    or-int/2addr v0, v1

    #@11
    .line 2581
    .end local v0           #show:Z
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .registers 3
    .parameter "featureId"

    #@0
    .prologue
    .line 2564
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z
    .registers 4
    .parameter "outBitmap"
    .parameter "canvas"

    #@0
    .prologue
    .line 1390
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 15
    .parameter "parent"
    .parameter "name"
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v7, -0x1

    #@4
    .line 4770
    const-string v6, "fragment"

    #@6
    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v6

    #@a
    if-nez v6, :cond_11

    #@c
    .line 4771
    invoke-virtual {p0, p2, p3, p4}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    #@f
    move-result-object v6

    #@10
    .line 4845
    :goto_10
    return-object v6

    #@11
    .line 4774
    :cond_11
    const-string v6, "class"

    #@13
    invoke-interface {p4, v3, v6}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    .line 4775
    .local v2, fname:Ljava/lang/String;
    sget-object v6, Lcom/android/internal/R$styleable;->Fragment:[I

    #@19
    invoke-virtual {p3, p4, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@1c
    move-result-object v0

    #@1d
    .line 4777
    .local v0, a:Landroid/content/res/TypedArray;
    if-nez v2, :cond_23

    #@1f
    .line 4778
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 4780
    :cond_23
    invoke-virtual {v0, v9, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@26
    move-result v4

    #@27
    .line 4781
    .local v4, id:I
    const/4 v6, 0x2

    #@28
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    .line 4782
    .local v5, tag:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2f
    .line 4784
    if-eqz p1, :cond_35

    #@31
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@34
    move-result v1

    #@35
    .line 4785
    .local v1, containerId:I
    :cond_35
    if-ne v1, v7, :cond_5c

    #@37
    if-ne v4, v7, :cond_5c

    #@39
    if-nez v5, :cond_5c

    #@3b
    .line 4786
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@3d
    new-instance v7, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@45
    move-result-object v8

    #@46
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v7

    #@4a
    const-string v8, ": Must specify unique android:id, android:tag, or have a parent with an id for "

    #@4c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v6

    #@5c
    .line 4793
    :cond_5c
    if-eq v4, v7, :cond_64

    #@5e
    iget-object v6, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@60
    invoke-virtual {v6, v4}, Landroid/app/FragmentManagerImpl;->findFragmentById(I)Landroid/app/Fragment;

    #@63
    move-result-object v3

    #@64
    .line 4794
    .local v3, fragment:Landroid/app/Fragment;
    :cond_64
    if-nez v3, :cond_6e

    #@66
    if-eqz v5, :cond_6e

    #@68
    .line 4795
    iget-object v6, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@6a
    invoke-virtual {v6, v5}, Landroid/app/FragmentManagerImpl;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    #@6d
    move-result-object v3

    #@6e
    .line 4797
    :cond_6e
    if-nez v3, :cond_78

    #@70
    if-eq v1, v7, :cond_78

    #@72
    .line 4798
    iget-object v6, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@74
    invoke-virtual {v6, v1}, Landroid/app/FragmentManagerImpl;->findFragmentById(I)Landroid/app/Fragment;

    #@77
    move-result-object v3

    #@78
    .line 4801
    :cond_78
    sget-boolean v6, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@7a
    if-eqz v6, :cond_ad

    #@7c
    const-string v6, "Activity"

    #@7e
    new-instance v7, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string/jumbo v8, "onCreateView: id=0x"

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v7

    #@8a
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@8d
    move-result-object v8

    #@8e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v7

    #@92
    const-string v8, " fname="

    #@94
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v7

    #@98
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    const-string v8, " existing="

    #@9e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v7

    #@aa
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 4804
    :cond_ad
    if-nez v3, :cond_f3

    #@af
    .line 4805
    invoke-static {p0, v2}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Fragment;

    #@b2
    move-result-object v3

    #@b3
    .line 4806
    iput-boolean v9, v3, Landroid/app/Fragment;->mFromLayout:Z

    #@b5
    .line 4807
    if-eqz v4, :cond_f1

    #@b7
    move v6, v4

    #@b8
    :goto_b8
    iput v6, v3, Landroid/app/Fragment;->mFragmentId:I

    #@ba
    .line 4808
    iput v1, v3, Landroid/app/Fragment;->mContainerId:I

    #@bc
    .line 4809
    iput-object v5, v3, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@be
    .line 4810
    iput-boolean v9, v3, Landroid/app/Fragment;->mInLayout:Z

    #@c0
    .line 4811
    iget-object v6, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@c2
    iput-object v6, v3, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@c4
    .line 4812
    iget-object v6, v3, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@c6
    invoke-virtual {v3, p0, p4, v6}, Landroid/app/Fragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    #@c9
    .line 4813
    iget-object v6, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@cb
    invoke-virtual {v6, v3, v9}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    #@ce
    .line 4835
    :goto_ce
    iget-object v6, v3, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@d0
    if-nez v6, :cond_14f

    #@d2
    .line 4836
    new-instance v6, Ljava/lang/IllegalStateException;

    #@d4
    new-instance v7, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v8, "Fragment "

    #@db
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v7

    #@df
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v7

    #@e3
    const-string v8, " did not create a view."

    #@e5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v7

    #@e9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v7

    #@ed
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f0
    throw v6

    #@f1
    :cond_f1
    move v6, v1

    #@f2
    .line 4807
    goto :goto_b8

    #@f3
    .line 4815
    :cond_f3
    iget-boolean v6, v3, Landroid/app/Fragment;->mInLayout:Z

    #@f5
    if-eqz v6, :cond_13e

    #@f7
    .line 4818
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@f9
    new-instance v7, Ljava/lang/StringBuilder;

    #@fb
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@fe
    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    #@101
    move-result-object v8

    #@102
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v7

    #@106
    const-string v8, ": Duplicate id 0x"

    #@108
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v7

    #@10c
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10f
    move-result-object v8

    #@110
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v7

    #@114
    const-string v8, ", tag "

    #@116
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v7

    #@11a
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v7

    #@11e
    const-string v8, ", or parent id 0x"

    #@120
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v7

    #@124
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@127
    move-result-object v8

    #@128
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v7

    #@12c
    const-string v8, " with another fragment for "

    #@12e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v7

    #@132
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v7

    #@136
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v7

    #@13a
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13d
    throw v6

    #@13e
    .line 4825
    :cond_13e
    iput-boolean v9, v3, Landroid/app/Fragment;->mInLayout:Z

    #@140
    .line 4829
    iget-boolean v6, v3, Landroid/app/Fragment;->mRetaining:Z

    #@142
    if-nez v6, :cond_149

    #@144
    .line 4830
    iget-object v6, v3, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@146
    invoke-virtual {v3, p0, p4, v6}, Landroid/app/Fragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    #@149
    .line 4832
    :cond_149
    iget-object v6, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@14b
    invoke-virtual {v6, v3}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;)V

    #@14e
    goto :goto_ce

    #@14f
    .line 4839
    :cond_14f
    if-eqz v4, :cond_156

    #@151
    .line 4840
    iget-object v6, v3, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@153
    invoke-virtual {v6, v4}, Landroid/view/View;->setId(I)V

    #@156
    .line 4842
    :cond_156
    iget-object v6, v3, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@158
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@15b
    move-result-object v6

    #@15c
    if-nez v6, :cond_163

    #@15e
    .line 4843
    iget-object v6, v3, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@160
    invoke-virtual {v6, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    #@163
    .line 4845
    :cond_163
    iget-object v6, v3, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@165
    goto/16 :goto_10
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 5
    .parameter "name"
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 4756
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onDestroy()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    .line 1473
    :try_start_1
    iget-object v6, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@3
    if-eqz v6, :cond_a

    #@5
    .line 1474
    iget-object v6, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@7
    invoke-interface {v6}, Lcom/lge/app/atsagent/IViewAgent;->onHide()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_a} :catch_34

    #@a
    .line 1483
    :cond_a
    :goto_a
    invoke-direct {p0, v11, p0}, Landroid/app/Activity;->unhideFloatingAppsIfNeeded(ZLandroid/app/Activity;)V

    #@d
    .line 1486
    iput-boolean v11, p0, Landroid/app/Activity;->mCalled:Z

    #@f
    .line 1489
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@11
    if-eqz v6, :cond_50

    #@13
    .line 1490
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@15
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    #@18
    move-result v5

    #@19
    .line 1491
    .local v5, numDialogs:I
    const/4 v2, 0x0

    #@1a
    .local v2, i:I
    :goto_1a
    if-ge v2, v5, :cond_4d

    #@1c
    .line 1492
    iget-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@1e
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@21
    move-result-object v3

    #@22
    check-cast v3, Landroid/app/Activity$ManagedDialog;

    #@24
    .line 1493
    .local v3, md:Landroid/app/Activity$ManagedDialog;
    iget-object v6, v3, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@26
    invoke-virtual {v6}, Landroid/app/Dialog;->isShowing()Z

    #@29
    move-result v6

    #@2a
    if-eqz v6, :cond_31

    #@2c
    .line 1494
    iget-object v6, v3, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@2e
    invoke-virtual {v6}, Landroid/app/Dialog;->dismiss()V

    #@31
    .line 1491
    :cond_31
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_1a

    #@34
    .line 1476
    .end local v2           #i:I
    .end local v3           #md:Landroid/app/Activity$ManagedDialog;
    .end local v5           #numDialogs:I
    :catch_34
    move-exception v1

    #@35
    .line 1477
    .local v1, ex:Ljava/lang/Exception;
    const-string v6, "ViewAgent"

    #@37
    const-string v7, "Activity onDestroy exception: %s"

    #@39
    new-array v8, v11, [Ljava/lang/Object;

    #@3b
    const/4 v9, 0x0

    #@3c
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@3f
    move-result-object v10

    #@40
    aput-object v10, v8, v9

    #@42
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 1479
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@4c
    goto :goto_a

    #@4d
    .line 1497
    .end local v1           #ex:Ljava/lang/Exception;
    .restart local v2       #i:I
    .restart local v5       #numDialogs:I
    :cond_4d
    const/4 v6, 0x0

    #@4e
    iput-object v6, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@50
    .line 1501
    .end local v2           #i:I
    .end local v5           #numDialogs:I
    :cond_50
    iget-object v7, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@52
    monitor-enter v7

    #@53
    .line 1502
    :try_start_53
    iget-object v6, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@55
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@58
    move-result v4

    #@59
    .line 1503
    .local v4, numCursors:I
    const/4 v2, 0x0

    #@5a
    .restart local v2       #i:I
    :goto_5a
    if-ge v2, v4, :cond_70

    #@5c
    .line 1504
    iget-object v6, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@5e
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@61
    move-result-object v0

    #@62
    check-cast v0, Landroid/app/Activity$ManagedCursor;

    #@64
    .line 1505
    .local v0, c:Landroid/app/Activity$ManagedCursor;
    if-eqz v0, :cond_6d

    #@66
    .line 1506
    invoke-static {v0}, Landroid/app/Activity$ManagedCursor;->access$100(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;

    #@69
    move-result-object v6

    #@6a
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6d
    .line 1503
    :cond_6d
    add-int/lit8 v2, v2, 0x1

    #@6f
    goto :goto_5a

    #@70
    .line 1509
    .end local v0           #c:Landroid/app/Activity$ManagedCursor;
    :cond_70
    iget-object v6, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@75
    .line 1510
    monitor-exit v7
    :try_end_76
    .catchall {:try_start_53 .. :try_end_76} :catchall_87

    #@76
    .line 1513
    iget-object v6, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@78
    if-eqz v6, :cond_7f

    #@7a
    .line 1514
    iget-object v6, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@7c
    invoke-virtual {v6}, Landroid/app/SearchManager;->stopSearch()V

    #@7f
    .line 1517
    :cond_7f
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@82
    move-result-object v6

    #@83
    invoke-virtual {v6, p0}, Landroid/app/Application;->dispatchActivityDestroyed(Landroid/app/Activity;)V

    #@86
    .line 1518
    return-void

    #@87
    .line 1510
    .end local v2           #i:I
    .end local v4           #numCursors:I
    :catchall_87
    move-exception v6

    #@88
    :try_start_88
    monitor-exit v7
    :try_end_89
    .catchall {:try_start_88 .. :try_end_89} :catchall_87

    #@89
    throw v6
.end method

.method public onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 2424
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2336
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 12
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 2141
    const/4 v6, 0x4

    #@5
    if-ne p1, v6, :cond_1f

    #@7
    .line 2142
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@a
    move-result-object v6

    #@b
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@d
    const/4 v7, 0x5

    #@e
    if-lt v6, v7, :cond_15

    #@10
    .line 2144
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    #@13
    :goto_13
    move v1, v5

    #@14
    .line 2197
    :cond_14
    :goto_14
    return v1

    #@15
    .line 2147
    :cond_15
    iget-boolean v6, p0, Landroid/app/Activity;->mResumed:Z

    #@17
    if-nez v6, :cond_1b

    #@19
    move v1, v4

    #@1a
    goto :goto_14

    #@1b
    .line 2148
    :cond_1b
    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V

    #@1e
    goto :goto_13

    #@1f
    .line 2153
    :cond_1f
    iget v6, p0, Landroid/app/Activity;->mDefaultKeyMode:I

    #@21
    if-nez v6, :cond_25

    #@23
    move v1, v4

    #@24
    .line 2154
    goto :goto_14

    #@25
    .line 2155
    :cond_25
    iget v6, p0, Landroid/app/Activity;->mDefaultKeyMode:I

    #@27
    if-ne v6, v7, :cond_37

    #@29
    .line 2156
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v6, v4, p1, p2, v7}, Landroid/view/Window;->performPanelShortcut(IILandroid/view/KeyEvent;I)Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_35

    #@33
    move v1, v5

    #@34
    .line 2158
    goto :goto_14

    #@35
    :cond_35
    move v1, v4

    #@36
    .line 2160
    goto :goto_14

    #@37
    .line 2163
    :cond_37
    const/4 v0, 0x0

    #@38
    .line 2165
    .local v0, clearSpannable:Z
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@3b
    move-result v6

    #@3c
    if-nez v6, :cond_44

    #@3e
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    #@41
    move-result v6

    #@42
    if-eqz v6, :cond_58

    #@44
    .line 2166
    :cond_44
    const/4 v0, 0x1

    #@45
    .line 2167
    const/4 v1, 0x0

    #@46
    .line 2192
    .local v1, handled:Z
    :cond_46
    :goto_46
    if-eqz v0, :cond_14

    #@48
    .line 2193
    iget-object v5, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@4a
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->clear()V

    #@4d
    .line 2194
    iget-object v5, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@4f
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    #@52
    .line 2195
    iget-object v5, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@54
    invoke-static {v5, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@57
    goto :goto_14

    #@58
    .line 2169
    .end local v1           #handled:Z
    :cond_58
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    #@5b
    move-result-object v6

    #@5c
    iget-object v7, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@5e
    invoke-virtual {v6, v8, v7, p1, p2}, Landroid/text/method/TextKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    #@61
    move-result v1

    #@62
    .line 2171
    .restart local v1       #handled:Z
    if-eqz v1, :cond_46

    #@64
    iget-object v6, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@66
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    #@69
    move-result v6

    #@6a
    if-lez v6, :cond_46

    #@6c
    .line 2174
    iget-object v6, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@6e
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    .line 2175
    .local v3, str:Ljava/lang/String;
    const/4 v0, 0x1

    #@73
    .line 2177
    iget v6, p0, Landroid/app/Activity;->mDefaultKeyMode:I

    #@75
    packed-switch v6, :pswitch_data_aa

    #@78
    :pswitch_78
    goto :goto_46

    #@79
    .line 2179
    :pswitch_79
    new-instance v2, Landroid/content/Intent;

    #@7b
    const-string v5, "android.intent.action.DIAL"

    #@7d
    new-instance v6, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string/jumbo v7, "tel:"

    #@85
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v6

    #@8d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v6

    #@91
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@94
    move-result-object v6

    #@95
    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@98
    .line 2180
    .local v2, intent:Landroid/content/Intent;
    const/high16 v5, 0x1000

    #@9a
    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@9d
    .line 2181
    invoke-virtual {p0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    #@a0
    goto :goto_46

    #@a1
    .line 2184
    .end local v2           #intent:Landroid/content/Intent;
    :pswitch_a1
    invoke-virtual {p0, v3, v4, v8, v4}, Landroid/app/Activity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    #@a4
    goto :goto_46

    #@a5
    .line 2187
    :pswitch_a5
    invoke-virtual {p0, v3, v4, v8, v5}, Landroid/app/Activity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    #@a8
    goto :goto_46

    #@a9
    .line 2177
    nop

    #@aa
    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_79
        :pswitch_78
        :pswitch_a1
        :pswitch_a5
    .end packed-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2207
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    .line 2245
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 2270
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2226
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@4
    move-result-object v1

    #@5
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@7
    const/4 v2, 0x5

    #@8
    if-lt v1, v2, :cond_1d

    #@a
    .line 2228
    const/4 v1, 0x4

    #@b
    if-ne p1, v1, :cond_1d

    #@d
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_1d

    #@13
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1d

    #@19
    .line 2231
    iget-boolean v1, p0, Landroid/app/Activity;->mResumed:Z

    #@1b
    if-nez v1, :cond_1e

    #@1d
    .line 2236
    :cond_1d
    :goto_1d
    return v0

    #@1e
    .line 2232
    :cond_1e
    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V

    #@21
    .line 2233
    const/4 v0, 0x1

    #@22
    goto :goto_1d
.end method

.method public onLowMemory()V
    .registers 2

    #@0
    .prologue
    .line 1724
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@3
    .line 1725
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@5
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchLowMemory()V

    #@8
    .line 1726
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .registers 8
    .parameter "featureId"
    .parameter "item"

    #@0
    .prologue
    const v4, 0xc350

    #@3
    const/4 v3, 0x2

    #@4
    const/4 v1, 0x0

    #@5
    const/4 v0, 0x1

    #@6
    .line 2632
    sparse-switch p1, :sswitch_data_aa

    #@9
    move v0, v1

    #@a
    .line 2668
    :cond_a
    :goto_a
    return v0

    #@b
    .line 2637
    :sswitch_b
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitleCondensed()Ljava/lang/CharSequence;

    #@e
    move-result-object v2

    #@f
    if-eqz v2, :cond_54

    #@11
    .line 2638
    new-array v2, v3, [Ljava/lang/Object;

    #@13
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v3

    #@17
    aput-object v3, v2, v1

    #@19
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitleCondensed()Ljava/lang/CharSequence;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    aput-object v3, v2, v0

    #@23
    invoke-static {v4, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@26
    .line 2641
    :goto_26
    invoke-virtual {p0, p2}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    #@29
    move-result v2

    #@2a
    if-nez v2, :cond_a

    #@2c
    .line 2644
    iget-object v2, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@2e
    invoke-virtual {v2, p2}, Landroid/app/FragmentManagerImpl;->dispatchOptionsItemSelected(Landroid/view/MenuItem;)Z

    #@31
    move-result v2

    #@32
    if-nez v2, :cond_a

    #@34
    .line 2647
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    #@37
    move-result v0

    #@38
    const v2, 0x102002c

    #@3b
    if-ne v0, v2, :cond_6d

    #@3d
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@3f
    if-eqz v0, :cond_6d

    #@41
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@43
    invoke-virtual {v0}, Lcom/android/internal/app/ActionBarImpl;->getDisplayOptions()I

    #@46
    move-result v0

    #@47
    and-int/lit8 v0, v0, 0x4

    #@49
    if-eqz v0, :cond_6d

    #@4b
    .line 2649
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@4d
    if-nez v0, :cond_66

    #@4f
    .line 2650
    invoke-virtual {p0}, Landroid/app/Activity;->onNavigateUp()Z

    #@52
    move-result v0

    #@53
    goto :goto_a

    #@54
    .line 2640
    :cond_54
    new-array v2, v3, [Ljava/lang/Object;

    #@56
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v3

    #@5a
    aput-object v3, v2, v1

    #@5c
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitleCondensed()Ljava/lang/CharSequence;

    #@5f
    move-result-object v3

    #@60
    aput-object v3, v2, v0

    #@62
    invoke-static {v4, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@65
    goto :goto_26

    #@66
    .line 2652
    :cond_66
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@68
    invoke-virtual {v0, p0}, Landroid/app/Activity;->onNavigateUpFromChild(Landroid/app/Activity;)Z

    #@6b
    move-result v0

    #@6c
    goto :goto_a

    #@6d
    :cond_6d
    move v0, v1

    #@6e
    .line 2655
    goto :goto_a

    #@6f
    .line 2658
    :sswitch_6f
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitleCondensed()Ljava/lang/CharSequence;

    #@72
    move-result-object v2

    #@73
    if-eqz v2, :cond_98

    #@75
    .line 2659
    new-array v2, v3, [Ljava/lang/Object;

    #@77
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a
    move-result-object v3

    #@7b
    aput-object v3, v2, v1

    #@7d
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitleCondensed()Ljava/lang/CharSequence;

    #@80
    move-result-object v1

    #@81
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@84
    move-result-object v1

    #@85
    aput-object v1, v2, v0

    #@87
    invoke-static {v4, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@8a
    .line 2662
    :goto_8a
    invoke-virtual {p0, p2}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    #@8d
    move-result v1

    #@8e
    if-nez v1, :cond_a

    #@90
    .line 2665
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@92
    invoke-virtual {v0, p2}, Landroid/app/FragmentManagerImpl;->dispatchContextItemSelected(Landroid/view/MenuItem;)Z

    #@95
    move-result v0

    #@96
    goto/16 :goto_a

    #@98
    .line 2661
    :cond_98
    new-array v2, v3, [Ljava/lang/Object;

    #@9a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9d
    move-result-object v3

    #@9e
    aput-object v3, v2, v1

    #@a0
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitleCondensed()Ljava/lang/CharSequence;

    #@a3
    move-result-object v1

    #@a4
    aput-object v1, v2, v0

    #@a6
    invoke-static {v4, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@a9
    goto :goto_8a

    #@aa
    .line 2632
    :sswitch_data_aa
    .sparse-switch
        0x0 -> :sswitch_b
        0x6 -> :sswitch_6f
    .end sparse-switch
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .registers 6
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2611
    const/16 v0, 0x8

    #@3
    if-ne p1, v0, :cond_11

    #@5
    .line 2612
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@8
    .line 2613
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 2614
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@e
    invoke-virtual {v0, v2}, Lcom/android/internal/app/ActionBarImpl;->dispatchMenuVisibilityChanged(Z)V

    #@11
    .line 2619
    :cond_11
    :goto_11
    return v2

    #@12
    .line 2616
    :cond_12
    const-string v0, "Activity"

    #@14
    const-string v1, "Tried to open action bar menu with no action bar"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_11
.end method

.method public onNavigateUp()Z
    .registers 5

    #@0
    .prologue
    .line 2819
    invoke-virtual {p0}, Landroid/app/Activity;->getParentActivityIntent()Landroid/content/Intent;

    #@3
    move-result-object v1

    #@4
    .line 2820
    .local v1, upIntent:Landroid/content/Intent;
    if-eqz v1, :cond_40

    #@6
    .line 2821
    iget-object v2, p0, Landroid/app/Activity;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    #@8
    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@a
    if-nez v2, :cond_11

    #@c
    .line 2825
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    #@f
    .line 2844
    :goto_f
    const/4 v2, 0x1

    #@10
    .line 2846
    :goto_10
    return v2

    #@11
    .line 2826
    :cond_11
    invoke-virtual {p0, v1}, Landroid/app/Activity;->shouldUpRecreateTask(Landroid/content/Intent;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_3c

    #@17
    .line 2827
    invoke-static {p0}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    #@1a
    move-result-object v0

    #@1b
    .line 2828
    .local v0, b:Landroid/app/TaskStackBuilder;
    invoke-virtual {p0, v0}, Landroid/app/Activity;->onCreateNavigateUpTaskStack(Landroid/app/TaskStackBuilder;)V

    #@1e
    .line 2829
    invoke-virtual {p0, v0}, Landroid/app/Activity;->onPrepareNavigateUpTaskStack(Landroid/app/TaskStackBuilder;)V

    #@21
    .line 2830
    invoke-virtual {v0}, Landroid/app/TaskStackBuilder;->startActivities()V

    #@24
    .line 2834
    iget v2, p0, Landroid/app/Activity;->mResultCode:I

    #@26
    if-nez v2, :cond_2c

    #@28
    iget-object v2, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@2a
    if-eqz v2, :cond_38

    #@2c
    .line 2836
    :cond_2c
    const-string v2, "Activity"

    #@2e
    const-string/jumbo v3, "onNavigateUp only finishing topmost activity to return a result"

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 2837
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    #@37
    goto :goto_f

    #@38
    .line 2839
    :cond_38
    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    #@3b
    goto :goto_f

    #@3c
    .line 2842
    .end local v0           #b:Landroid/app/TaskStackBuilder;
    :cond_3c
    invoke-virtual {p0, v1}, Landroid/app/Activity;->navigateUpTo(Landroid/content/Intent;)Z

    #@3f
    goto :goto_f

    #@40
    .line 2846
    :cond_40
    const/4 v2, 0x0

    #@41
    goto :goto_10
.end method

.method public onNavigateUpFromChild(Landroid/app/Activity;)Z
    .registers 3
    .parameter "child"

    #@0
    .prologue
    .line 2856
    invoke-virtual {p0}, Landroid/app/Activity;->onNavigateUp()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 1188
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 2787
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 2788
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    #@9
    move-result v0

    #@a
    .line 2790
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 2905
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2906
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    #@9
    .line 2908
    :cond_9
    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 5
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 2682
    sparse-switch p1, :sswitch_data_1c

    #@3
    .line 2697
    :goto_3
    return-void

    #@4
    .line 2684
    :sswitch_4
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0, p2}, Landroid/app/FragmentManagerImpl;->dispatchOptionsMenuClosed(Landroid/view/Menu;)V

    #@9
    .line 2685
    invoke-virtual {p0, p2}, Landroid/app/Activity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    #@c
    goto :goto_3

    #@d
    .line 2689
    :sswitch_d
    invoke-virtual {p0, p2}, Landroid/app/Activity;->onContextMenuClosed(Landroid/view/Menu;)V

    #@10
    goto :goto_3

    #@11
    .line 2693
    :sswitch_11
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@14
    .line 2694
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@16
    const/4 v1, 0x0

    #@17
    invoke-virtual {v0, v1}, Lcom/android/internal/app/ActionBarImpl;->dispatchMenuVisibilityChanged(Z)V

    #@1a
    goto :goto_3

    #@1b
    .line 2682
    nop

    #@1c
    :sswitch_data_1c
    .sparse-switch
        0x0 -> :sswitch_4
        0x6 -> :sswitch_d
        0x8 -> :sswitch_11
    .end sparse-switch
.end method

.method protected onPause()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1334
    :try_start_2
    iget-object v1, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@4
    if-eqz v1, :cond_b

    #@6
    .line 1335
    iget-object v1, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@8
    invoke-interface {v1}, Lcom/lge/app/atsagent/IViewAgent;->onHide()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_b} :catch_18

    #@b
    .line 1344
    :cond_b
    :goto_b
    invoke-direct {p0, v5, p0}, Landroid/app/Activity;->unhideFloatingAppsIfNeeded(ZLandroid/app/Activity;)V

    #@e
    .line 1347
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Landroid/app/Application;->dispatchActivityPaused(Landroid/app/Activity;)V

    #@15
    .line 1348
    iput-boolean v6, p0, Landroid/app/Activity;->mCalled:Z

    #@17
    .line 1349
    return-void

    #@18
    .line 1337
    :catch_18
    move-exception v0

    #@19
    .line 1338
    .local v0, ex:Ljava/lang/Exception;
    const-string v1, "ViewAgent"

    #@1b
    const-string v2, "Activity pause exception: %s"

    #@1d
    new-array v3, v6, [Ljava/lang/Object;

    #@1f
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    aput-object v4, v3, v5

    #@25
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1340
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@2f
    goto :goto_b
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1037
    invoke-virtual {p0}, Landroid/app/Activity;->isChild()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_14

    #@7
    .line 1038
    iput-boolean v2, p0, Landroid/app/Activity;->mTitleReady:Z

    #@9
    .line 1039
    invoke-virtual {p0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0}, Landroid/app/Activity;->getTitleColor()I

    #@10
    move-result v1

    #@11
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    #@14
    .line 1041
    :cond_14
    iput-boolean v2, p0, Landroid/app/Activity;->mCalled:Z

    #@16
    .line 1042
    return-void
.end method

.method protected onPostResume()V
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1145
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@4
    move-result-object v1

    #@5
    .line 1146
    .local v1, win:Landroid/view/Window;
    if-eqz v1, :cond_a

    #@7
    invoke-virtual {v1}, Landroid/view/Window;->makeActive()V

    #@a
    .line 1147
    :cond_a
    iget-object v2, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@c
    if-eqz v2, :cond_13

    #@e
    iget-object v2, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@10
    invoke-virtual {v2, v3}, Lcom/android/internal/app/ActionBarImpl;->setShowHideAnimationEnabled(Z)V

    #@13
    .line 1148
    :cond_13
    iput-boolean v3, p0, Landroid/app/Activity;->mCalled:Z

    #@15
    .line 1152
    :try_start_15
    sget-boolean v2, Lcom/lge/app/atsagent/AtsViewAgent;->IS_ETA2_ACTIVATED:Z

    #@17
    if-eqz v2, :cond_50

    #@19
    iget-object v2, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@1b
    if-nez v2, :cond_50

    #@1d
    .line 1153
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@24
    move-result-object v2

    #@25
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, "::"

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-class v4, Landroid/app/Activity;

    #@3e
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v2, v3}, Lcom/lge/app/atsagent/AtsViewAgent;->createViewAgent(Landroid/view/View;Ljava/lang/String;)Lcom/lge/app/atsagent/IViewAgent;

    #@4d
    move-result-object v2

    #@4e
    iput-object v2, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@50
    .line 1156
    :cond_50
    iget-object v2, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@52
    if-eqz v2, :cond_59

    #@54
    .line 1157
    iget-object v2, p0, Landroid/app/Activity;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@56
    invoke-interface {v2}, Lcom/lge/app/atsagent/IViewAgent;->onShow()V
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_59} :catch_5a

    #@59
    .line 1164
    :cond_59
    :goto_59
    return-void

    #@5a
    .line 1159
    :catch_5a
    move-exception v0

    #@5b
    .line 1160
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "ViewAgent"

    #@5d
    new-instance v3, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v4, "ViewAgent.onShow Exception: "

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 1161
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@7a
    goto :goto_59
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 3
    .parameter "id"
    .parameter "dialog"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3074
    invoke-virtual {p2, p0}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    #@3
    .line 3075
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 4
    .parameter "id"
    .parameter "dialog"
    .parameter "args"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3103
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    #@3
    .line 3104
    return-void
.end method

.method public onPrepareNavigateUpTaskStack(Landroid/app/TaskStackBuilder;)V
    .registers 2
    .parameter "builder"

    #@0
    .prologue
    .line 2895
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 2762
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 2763
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@6
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@9
    move-result v0

    #@a
    .line 2765
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    goto :goto_a
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .registers 7
    .parameter "featureId"
    .parameter "view"
    .parameter "menu"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2594
    if-nez p1, :cond_18

    #@3
    if-eqz p3, :cond_18

    #@5
    .line 2595
    invoke-virtual {p0, p3}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@8
    move-result v0

    #@9
    .line 2596
    .local v0, goforit:Z
    iget-object v2, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@b
    invoke-virtual {v2, p3}, Landroid/app/FragmentManagerImpl;->dispatchPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@e
    move-result v2

    #@f
    or-int/2addr v0, v2

    #@10
    .line 2598
    if-eqz v0, :cond_19

    #@12
    invoke-interface {p3}, Landroid/view/Menu;->hasVisibleItems()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_19

    #@18
    .line 2602
    .end local v0           #goforit:Z
    :cond_18
    :goto_18
    return v1

    #@19
    .line 2598
    .restart local v0       #goforit:Z
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_18
.end method

.method protected onRestart()V
    .registers 2

    #@0
    .prologue
    .line 1098
    invoke-direct {p0}, Landroid/app/Activity;->hideFloatingAppsIfNeeded()V

    #@3
    .line 1100
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@6
    .line 1101
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 964
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    if-eqz v1, :cond_11

    #@4
    .line 965
    const-string v1, "android:viewHierarchyState"

    #@6
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 966
    .local v0, windowState:Landroid/os/Bundle;
    if-eqz v0, :cond_11

    #@c
    .line 967
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@e
    invoke-virtual {v1, v0}, Landroid/view/Window;->restoreHierarchyState(Landroid/os/Bundle;)V

    #@11
    .line 970
    .end local v0           #windowState:Landroid/os/Bundle;
    :cond_11
    return-void
.end method

.method protected onResume()V
    .registers 2

    #@0
    .prologue
    .line 1126
    invoke-direct {p0}, Landroid/app/Activity;->hideFloatingAppsIfNeeded()V

    #@3
    .line 1128
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p0}, Landroid/app/Application;->dispatchActivityResumed(Landroid/app/Activity;)V

    #@a
    .line 1129
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@d
    .line 1130
    return-void
.end method

.method onRetainNonConfigurationChildInstances()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1685
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 1652
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    #@0
    .prologue
    .line 1250
    const-string v1, "android:viewHierarchyState"

    #@2
    iget-object v2, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@4
    invoke-virtual {v2}, Landroid/view/Window;->saveHierarchyState()Landroid/os/Bundle;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@b
    .line 1251
    iget-object v1, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@d
    invoke-virtual {v1}, Landroid/app/FragmentManagerImpl;->saveAllState()Landroid/os/Parcelable;

    #@10
    move-result-object v0

    #@11
    .line 1252
    .local v0, p:Landroid/os/Parcelable;
    if-eqz v0, :cond_18

    #@13
    .line 1253
    const-string v1, "android:fragments"

    #@15
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@18
    .line 1255
    :cond_18
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p0, p1}, Landroid/app/Application;->dispatchActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@1f
    .line 1256
    return-void
.end method

.method public onSearchRequested()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 3261
    invoke-virtual {p0, v1, v0, v1, v0}, Landroid/app/Activity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    #@5
    .line 3262
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method protected onStart()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1060
    invoke-direct {p0}, Landroid/app/Activity;->hideFloatingAppsIfNeeded()V

    #@4
    .line 1062
    iput-boolean v3, p0, Landroid/app/Activity;->mCalled:Z

    #@6
    .line 1064
    iget-boolean v0, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@8
    if-nez v0, :cond_17

    #@a
    .line 1065
    iput-boolean v3, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@c
    .line 1066
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@e
    if-eqz v0, :cond_1f

    #@10
    .line 1067
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@12
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doStart()V

    #@15
    .line 1071
    :cond_15
    :goto_15
    iput-boolean v3, p0, Landroid/app/Activity;->mCheckedForLoaderManager:Z

    #@17
    .line 1074
    :cond_17
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0, p0}, Landroid/app/Application;->dispatchActivityStarted(Landroid/app/Activity;)V

    #@1e
    .line 1075
    return-void

    #@1f
    .line 1068
    :cond_1f
    iget-boolean v0, p0, Landroid/app/Activity;->mCheckedForLoaderManager:Z

    #@21
    if-nez v0, :cond_15

    #@23
    .line 1069
    const/4 v0, 0x0

    #@24
    iget-boolean v1, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@26
    const/4 v2, 0x0

    #@27
    invoke-virtual {p0, v0, v1, v2}, Landroid/app/Activity;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@2d
    goto :goto_15
.end method

.method protected onStop()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1435
    invoke-direct {p0, v1, p0}, Landroid/app/Activity;->unhideFloatingAppsIfNeeded(ZLandroid/app/Activity;)V

    #@4
    .line 1437
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@6
    if-eqz v0, :cond_d

    #@8
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setShowHideAnimationEnabled(Z)V

    #@d
    .line 1438
    :cond_d
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p0}, Landroid/app/Application;->dispatchActivityStopped(Landroid/app/Activity;)V

    #@14
    .line 1439
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@17
    .line 1440
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .registers 5
    .parameter "title"
    .parameter "color"

    #@0
    .prologue
    .line 4611
    iget-boolean v1, p0, Landroid/app/Activity;->mTitleReady:Z

    #@2
    if-eqz v1, :cond_12

    #@4
    .line 4612
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@7
    move-result-object v0

    #@8
    .line 4613
    .local v0, win:Landroid/view/Window;
    if-eqz v0, :cond_12

    #@a
    .line 4614
    invoke-virtual {v0, p1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@d
    .line 4615
    if-eqz p2, :cond_12

    #@f
    .line 4616
    invoke-virtual {v0, p2}, Landroid/view/Window;->setTitleColor(I)V

    #@12
    .line 4620
    .end local v0           #win:Landroid/view/Window;
    :cond_12
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2284
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p0, p1}, Landroid/view/Window;->shouldCloseOnTouch(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 2285
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    #@b
    .line 2286
    const/4 v0, 0x1

    #@c
    .line 2289
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 2307
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onTrimMemory(I)V
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 1730
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@3
    .line 1731
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@5
    invoke-virtual {v0, p1}, Landroid/app/FragmentManagerImpl;->dispatchTrimMemory(I)V

    #@8
    .line 1732
    return-void
.end method

.method public onUserInteraction()V
    .registers 1

    #@0
    .prologue
    .line 2359
    return-void
.end method

.method protected onUserLeaveHint()V
    .registers 1

    #@0
    .prologue
    .line 1367
    return-void
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 2365
    iget-object v1, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v1, :cond_15

    #@4
    .line 2366
    iget-object v0, p0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@6
    .line 2367
    .local v0, decor:Landroid/view/View;
    if-eqz v0, :cond_15

    #@8
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@b
    move-result-object v1

    #@c
    if-eqz v1, :cond_15

    #@e
    .line 2368
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v1, v0, p1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@15
    .line 2371
    .end local v0           #decor:Landroid/view/View;
    :cond_15
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter "hasFocus"

    #@0
    .prologue
    .line 2404
    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 4975
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@3
    .line 4976
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 4977
    iget-object v0, p0, Landroid/app/Activity;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/internal/app/ActionBarImpl;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@c
    move-result-object v0

    #@d
    .line 4979
    :goto_d
    return-object v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public openContextMenu(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 2975
    invoke-virtual {p1}, Landroid/view/View;->showContextMenu()Z

    #@3
    .line 2976
    return-void
.end method

.method public openOptionsMenu()V
    .registers 4

    #@0
    .prologue
    .line 2915
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->openPanel(ILandroid/view/KeyEvent;)V

    #@7
    .line 2916
    return-void
.end method

.method public overridePendingTransition(II)V
    .registers 6
    .parameter "enterAnim"
    .parameter "exitAnim"

    #@0
    .prologue
    .line 4046
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v0, v1, v2, p1, p2}, Landroid/app/IActivityManager;->overridePendingTransition(Landroid/os/IBinder;Ljava/lang/String;II)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 4050
    :goto_d
    return-void

    #@e
    .line 4048
    :catch_e
    move-exception v0

    #@f
    goto :goto_d
.end method

.method final performCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "icicle"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 5200
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 5201
    iget-object v1, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@6
    invoke-virtual {v1}, Landroid/view/Window;->getWindowStyle()Landroid/content/res/TypedArray;

    #@9
    move-result-object v1

    #@a
    const/16 v2, 0xa

    #@c
    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_13

    #@12
    const/4 v0, 0x1

    #@13
    :cond_13
    iput-boolean v0, p0, Landroid/app/Activity;->mVisibleFromClient:Z

    #@15
    .line 5203
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@17
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchActivityCreated()V

    #@1a
    .line 5204
    return-void
.end method

.method final performDestroy()V
    .registers 2

    #@0
    .prologue
    .line 5392
    invoke-direct {p0}, Landroid/app/Activity;->isExternal()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 5393
    const/4 v0, 0x0

    #@7
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setVisible(Z)V

    #@a
    .line 5398
    :cond_a
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/app/Activity;->mDestroyed:Z

    #@d
    .line 5399
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@f
    invoke-virtual {v0}, Landroid/view/Window;->destroy()V

    #@12
    .line 5400
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@14
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchDestroy()V

    #@17
    .line 5401
    invoke-virtual {p0}, Landroid/app/Activity;->onDestroy()V

    #@1a
    .line 5402
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1c
    if-eqz v0, :cond_23

    #@1e
    .line 5403
    iget-object v0, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@20
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    #@23
    .line 5405
    :cond_23
    return-void
.end method

.method final performPause()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5304
    invoke-direct {p0}, Landroid/app/Activity;->isExternal()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 5305
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@a
    .line 5306
    iput-boolean v2, p0, Landroid/app/Activity;->mResumed:Z

    #@c
    .line 5323
    :goto_c
    return-void

    #@d
    .line 5312
    :cond_d
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@f
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchPause()V

    #@12
    .line 5313
    iput-boolean v2, p0, Landroid/app/Activity;->mCalled:Z

    #@14
    .line 5314
    invoke-virtual {p0}, Landroid/app/Activity;->onPause()V

    #@17
    .line 5315
    iput-boolean v2, p0, Landroid/app/Activity;->mResumed:Z

    #@19
    .line 5316
    iget-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@1b
    if-nez v0, :cond_4c

    #@1d
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@20
    move-result-object v0

    #@21
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@23
    const/16 v1, 0x9

    #@25
    if-lt v0, v1, :cond_4c

    #@27
    .line 5318
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@29
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "Activity "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    iget-object v2, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@36
    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    const-string v2, " did not call through to super.onPause()"

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v0

    #@4c
    .line 5322
    :cond_4c
    iput-boolean v2, p0, Landroid/app/Activity;->mResumed:Z

    #@4e
    goto :goto_c
.end method

.method final performRestart()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 5231
    iget-object v3, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@3
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@6
    .line 5233
    iget-boolean v3, p0, Landroid/app/Activity;->mStopped:Z

    #@8
    if-eqz v3, :cond_b1

    #@a
    .line 5234
    iput-boolean v6, p0, Landroid/app/Activity;->mStopped:Z

    #@c
    .line 5235
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@e
    if-eqz v3, :cond_1d

    #@10
    iget-object v3, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@12
    if-nez v3, :cond_1d

    #@14
    .line 5236
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@17
    move-result-object v3

    #@18
    iget-object v4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@1a
    invoke-virtual {v3, v4, v6}, Landroid/view/WindowManagerGlobal;->setStoppedState(Landroid/os/IBinder;Z)V

    #@1d
    .line 5239
    :cond_1d
    iget-object v4, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@1f
    monitor-enter v4

    #@20
    .line 5240
    :try_start_20
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@25
    move-result v0

    #@26
    .line 5241
    .local v0, N:I
    const/4 v1, 0x0

    #@27
    .local v1, i:I
    :goto_27
    if-ge v1, v0, :cond_7d

    #@29
    .line 5242
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v2

    #@2f
    check-cast v2, Landroid/app/Activity$ManagedCursor;

    #@31
    .line 5243
    .local v2, mc:Landroid/app/Activity$ManagedCursor;
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$200(Landroid/app/Activity$ManagedCursor;)Z

    #@34
    move-result v3

    #@35
    if-nez v3, :cond_3d

    #@37
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$300(Landroid/app/Activity$ManagedCursor;)Z

    #@3a
    move-result v3

    #@3b
    if-eqz v3, :cond_7a

    #@3d
    .line 5244
    :cond_3d
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$100(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;

    #@40
    move-result-object v3

    #@41
    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    #@44
    move-result v3

    #@45
    if-nez v3, :cond_72

    #@47
    .line 5245
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@4a
    move-result-object v3

    #@4b
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@4d
    const/16 v5, 0xe

    #@4f
    if-lt v3, v5, :cond_72

    #@51
    .line 5247
    new-instance v3, Ljava/lang/IllegalStateException;

    #@53
    new-instance v5, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string/jumbo v6, "trying to requery an already closed cursor  "

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$100(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;

    #@62
    move-result-object v6

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-direct {v3, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@6e
    throw v3

    #@6f
    .line 5256
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #mc:Landroid/app/Activity$ManagedCursor;
    :catchall_6f
    move-exception v3

    #@70
    monitor-exit v4
    :try_end_71
    .catchall {:try_start_20 .. :try_end_71} :catchall_6f

    #@71
    throw v3

    #@72
    .line 5252
    .restart local v0       #N:I
    .restart local v1       #i:I
    .restart local v2       #mc:Landroid/app/Activity$ManagedCursor;
    :cond_72
    const/4 v3, 0x0

    #@73
    :try_start_73
    invoke-static {v2, v3}, Landroid/app/Activity$ManagedCursor;->access$202(Landroid/app/Activity$ManagedCursor;Z)Z

    #@76
    .line 5253
    const/4 v3, 0x0

    #@77
    invoke-static {v2, v3}, Landroid/app/Activity$ManagedCursor;->access$302(Landroid/app/Activity$ManagedCursor;Z)Z

    #@7a
    .line 5241
    :cond_7a
    add-int/lit8 v1, v1, 0x1

    #@7c
    goto :goto_27

    #@7d
    .line 5256
    .end local v2           #mc:Landroid/app/Activity$ManagedCursor;
    :cond_7d
    monitor-exit v4
    :try_end_7e
    .catchall {:try_start_73 .. :try_end_7e} :catchall_6f

    #@7e
    .line 5258
    iput-boolean v6, p0, Landroid/app/Activity;->mCalled:Z

    #@80
    .line 5259
    iget-object v3, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@82
    invoke-virtual {v3, p0}, Landroid/app/Instrumentation;->callActivityOnRestart(Landroid/app/Activity;)V

    #@85
    .line 5260
    iget-boolean v3, p0, Landroid/app/Activity;->mCalled:Z

    #@87
    if-nez v3, :cond_ae

    #@89
    .line 5261
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@8b
    new-instance v4, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v5, "Activity "

    #@92
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v4

    #@96
    iget-object v5, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@98
    invoke-virtual {v5}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@9b
    move-result-object v5

    #@9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    const-string v5, " did not call through to super.onRestart()"

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    invoke-direct {v3, v4}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@ad
    throw v3

    #@ae
    .line 5265
    :cond_ae
    invoke-virtual {p0}, Landroid/app/Activity;->performStart()V

    #@b1
    .line 5267
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_b1
    return-void
.end method

.method final performRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 939
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 940
    invoke-direct {p0, p1}, Landroid/app/Activity;->restoreManagedDialogs(Landroid/os/Bundle;)V

    #@6
    .line 941
    return-void
.end method

.method final performResume()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5270
    invoke-virtual {p0}, Landroid/app/Activity;->performRestart()V

    #@4
    .line 5272
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@9
    .line 5274
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Landroid/app/Activity;->mLastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@c
    .line 5276
    iput-boolean v1, p0, Landroid/app/Activity;->mCalled:Z

    #@e
    .line 5278
    iget-object v0, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@10
    invoke-virtual {v0, p0}, Landroid/app/Instrumentation;->callActivityOnResume(Landroid/app/Activity;)V

    #@13
    .line 5279
    iget-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@15
    if-nez v0, :cond_3c

    #@17
    .line 5280
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v2, "Activity "

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget-object v2, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@26
    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, " did not call through to super.onResume()"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v0

    #@3c
    .line 5286
    :cond_3c
    iput-boolean v1, p0, Landroid/app/Activity;->mCalled:Z

    #@3e
    .line 5288
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@40
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchResume()V

    #@43
    .line 5289
    iget-object v0, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@45
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@48
    .line 5291
    invoke-virtual {p0}, Landroid/app/Activity;->onPostResume()V

    #@4b
    .line 5292
    iget-boolean v0, p0, Landroid/app/Activity;->mCalled:Z

    #@4d
    if-nez v0, :cond_74

    #@4f
    .line 5293
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@51
    new-instance v1, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v2, "Activity "

    #@58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v1

    #@5c
    iget-object v2, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@5e
    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    const-string v2, " did not call through to super.onPostResume()"

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@73
    throw v0

    #@74
    .line 5297
    :cond_74
    return-void
.end method

.method final performSaveInstanceState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "outState"

    #@0
    .prologue
    .line 1199
    invoke-virtual {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 1200
    invoke-direct {p0, p1}, Landroid/app/Activity;->saveManagedDialogs(Landroid/os/Bundle;)V

    #@6
    .line 1202
    return-void
.end method

.method final performStart()V
    .registers 7

    #@0
    .prologue
    .line 5207
    iget-object v3, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@2
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@5
    .line 5208
    const/4 v3, 0x0

    #@6
    iput-boolean v3, p0, Landroid/app/Activity;->mCalled:Z

    #@8
    .line 5209
    iget-object v3, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@a
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@d
    .line 5210
    iget-object v3, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@f
    invoke-virtual {v3, p0}, Landroid/app/Instrumentation;->callActivityOnStart(Landroid/app/Activity;)V

    #@12
    .line 5211
    iget-boolean v3, p0, Landroid/app/Activity;->mCalled:Z

    #@14
    if-nez v3, :cond_3b

    #@16
    .line 5212
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "Activity "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    iget-object v5, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@25
    invoke-virtual {v5}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    const-string v5, " did not call through to super.onStart()"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-direct {v3, v4}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v3

    #@3b
    .line 5216
    :cond_3b
    iget-object v3, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@3d
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl;->dispatchStart()V

    #@40
    .line 5217
    iget-object v3, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@42
    if-eqz v3, :cond_66

    #@44
    .line 5218
    iget-object v3, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@46
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@49
    move-result v3

    #@4a
    new-array v2, v3, [Landroid/app/LoaderManagerImpl;

    #@4c
    .line 5219
    .local v2, loaders:[Landroid/app/LoaderManagerImpl;
    iget-object v3, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@4e
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@51
    move-result-object v3

    #@52
    invoke-interface {v3, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@55
    .line 5220
    if-eqz v2, :cond_66

    #@57
    .line 5221
    const/4 v0, 0x0

    #@58
    .local v0, i:I
    :goto_58
    array-length v3, v2

    #@59
    if-ge v0, v3, :cond_66

    #@5b
    .line 5222
    aget-object v1, v2, v0

    #@5d
    .line 5223
    .local v1, lm:Landroid/app/LoaderManagerImpl;
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl;->finishRetain()V

    #@60
    .line 5224
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl;->doReportStart()V

    #@63
    .line 5221
    add-int/lit8 v0, v0, 0x1

    #@65
    goto :goto_58

    #@66
    .line 5228
    .end local v0           #i:I
    .end local v1           #lm:Landroid/app/LoaderManagerImpl;
    .end local v2           #loaders:[Landroid/app/LoaderManagerImpl;
    :cond_66
    return-void
.end method

.method final performStop()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 5335
    invoke-direct {p0}, Landroid/app/Activity;->isExternal()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_b

    #@8
    .line 5336
    iput-boolean v5, p0, Landroid/app/Activity;->mCalled:Z

    #@a
    .line 5386
    :goto_a
    return-void

    #@b
    .line 5342
    :cond_b
    iget-boolean v3, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@d
    if-eqz v3, :cond_1e

    #@f
    .line 5343
    iput-boolean v6, p0, Landroid/app/Activity;->mLoadersStarted:Z

    #@11
    .line 5344
    iget-object v3, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@13
    if-eqz v3, :cond_1e

    #@15
    .line 5345
    iget-boolean v3, p0, Landroid/app/Activity;->mChangingConfigurations:Z

    #@17
    if-nez v3, :cond_71

    #@19
    .line 5346
    iget-object v3, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1b
    invoke-virtual {v3}, Landroid/app/LoaderManagerImpl;->doStop()V

    #@1e
    .line 5353
    :cond_1e
    :goto_1e
    iget-boolean v3, p0, Landroid/app/Activity;->mStopped:Z

    #@20
    if-nez v3, :cond_a2

    #@22
    .line 5354
    iget-object v3, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@24
    if-eqz v3, :cond_2b

    #@26
    .line 5355
    iget-object v3, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@28
    invoke-virtual {v3}, Landroid/view/Window;->closeAllPanels()V

    #@2b
    .line 5358
    :cond_2b
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@2d
    if-eqz v3, :cond_3c

    #@2f
    iget-object v3, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@31
    if-nez v3, :cond_3c

    #@33
    .line 5359
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@36
    move-result-object v3

    #@37
    iget-object v4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@39
    invoke-virtual {v3, v4, v5}, Landroid/view/WindowManagerGlobal;->setStoppedState(Landroid/os/IBinder;Z)V

    #@3c
    .line 5362
    :cond_3c
    iget-object v3, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@3e
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl;->dispatchStop()V

    #@41
    .line 5364
    iput-boolean v6, p0, Landroid/app/Activity;->mCalled:Z

    #@43
    .line 5365
    iget-object v3, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@45
    invoke-virtual {v3, p0}, Landroid/app/Instrumentation;->callActivityOnStop(Landroid/app/Activity;)V

    #@48
    .line 5366
    iget-boolean v3, p0, Landroid/app/Activity;->mCalled:Z

    #@4a
    if-nez v3, :cond_77

    #@4c
    .line 5367
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "Activity "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    iget-object v5, p0, Landroid/app/Activity;->mComponent:Landroid/content/ComponentName;

    #@5b
    invoke-virtual {v5}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    const-string v5, " did not call through to super.onStop()"

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v4

    #@6d
    invoke-direct {v3, v4}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@70
    throw v3

    #@71
    .line 5348
    :cond_71
    iget-object v3, p0, Landroid/app/Activity;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@73
    invoke-virtual {v3}, Landroid/app/LoaderManagerImpl;->doRetain()V

    #@76
    goto :goto_1e

    #@77
    .line 5372
    :cond_77
    iget-object v4, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@79
    monitor-enter v4

    #@7a
    .line 5373
    :try_start_7a
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@7c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7f
    move-result v0

    #@80
    .line 5374
    .local v0, N:I
    const/4 v1, 0x0

    #@81
    .local v1, i:I
    :goto_81
    if-ge v1, v0, :cond_9f

    #@83
    .line 5375
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@88
    move-result-object v2

    #@89
    check-cast v2, Landroid/app/Activity$ManagedCursor;

    #@8b
    .line 5376
    .local v2, mc:Landroid/app/Activity$ManagedCursor;
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$200(Landroid/app/Activity$ManagedCursor;)Z

    #@8e
    move-result v3

    #@8f
    if-nez v3, :cond_9c

    #@91
    .line 5377
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$100(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;

    #@94
    move-result-object v3

    #@95
    invoke-interface {v3}, Landroid/database/Cursor;->deactivate()V

    #@98
    .line 5378
    const/4 v3, 0x1

    #@99
    invoke-static {v2, v3}, Landroid/app/Activity$ManagedCursor;->access$202(Landroid/app/Activity$ManagedCursor;Z)Z

    #@9c
    .line 5374
    :cond_9c
    add-int/lit8 v1, v1, 0x1

    #@9e
    goto :goto_81

    #@9f
    .line 5381
    .end local v2           #mc:Landroid/app/Activity$ManagedCursor;
    :cond_9f
    monitor-exit v4
    :try_end_a0
    .catchall {:try_start_7a .. :try_end_a0} :catchall_a6

    #@a0
    .line 5383
    iput-boolean v5, p0, Landroid/app/Activity;->mStopped:Z

    #@a2
    .line 5385
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_a2
    iput-boolean v6, p0, Landroid/app/Activity;->mResumed:Z

    #@a4
    goto/16 :goto_a

    #@a6
    .line 5381
    :catchall_a6
    move-exception v3

    #@a7
    :try_start_a7
    monitor-exit v4
    :try_end_a8
    .catchall {:try_start_a7 .. :try_end_a8} :catchall_a6

    #@a8
    throw v3
.end method

.method final performUserLeaving()V
    .registers 1

    #@0
    .prologue
    .line 5326
    invoke-virtual {p0}, Landroid/app/Activity;->onUserInteraction()V

    #@3
    .line 5327
    invoke-virtual {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    #@6
    .line 5328
    return-void
.end method

.method public recreate()V
    .registers 9

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 4216
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 4217
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Can only be called on top-level activity"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 4219
    :cond_e
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@11
    move-result-object v0

    #@12
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@14
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getLooper()Landroid/os/Looper;

    #@17
    move-result-object v1

    #@18
    if-eq v0, v1, :cond_22

    #@1a
    .line 4220
    new-instance v0, Ljava/lang/IllegalStateException;

    #@1c
    const-string v1, "Must be called from main thread"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 4222
    :cond_22
    iget-object v0, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@24
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@26
    move-object v3, v2

    #@27
    move v5, v4

    #@28
    move-object v6, v2

    #@29
    move v7, v4

    #@2a
    invoke-virtual/range {v0 .. v7}, Landroid/app/ActivityThread;->requestRelaunchActivity(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;Z)V

    #@2d
    .line 4223
    return-void
.end method

.method public registerForContextMenu(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 2953
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@3
    .line 2954
    return-void
.end method

.method public final removeDialog(I)V
    .registers 4
    .parameter "id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3234
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@2
    if-eqz v1, :cond_18

    #@4
    .line 3235
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@6
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/app/Activity$ManagedDialog;

    #@c
    .line 3236
    .local v0, md:Landroid/app/Activity$ManagedDialog;
    if-eqz v0, :cond_18

    #@e
    .line 3237
    iget-object v1, v0, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@10
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    #@13
    .line 3238
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@15
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    #@18
    .line 3241
    .end local v0           #md:Landroid/app/Activity$ManagedDialog;
    :cond_18
    return-void
.end method

.method public final requestWindowFeature(I)Z
    .registers 3
    .parameter "featureId"

    #@0
    .prologue
    .line 3340
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->requestFeature(I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method retainNonConfigurationInstances()Landroid/app/Activity$NonConfigurationInstances;
    .registers 11

    #@0
    .prologue
    .line 1689
    invoke-virtual {p0}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 1690
    .local v0, activity:Ljava/lang/Object;
    invoke-virtual {p0}, Landroid/app/Activity;->onRetainNonConfigurationChildInstances()Ljava/util/HashMap;

    #@7
    move-result-object v1

    #@8
    .line 1691
    .local v1, children:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v8, p0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@a
    invoke-virtual {v8}, Landroid/app/FragmentManagerImpl;->retainNonConfig()Ljava/util/ArrayList;

    #@d
    move-result-object v2

    #@e
    .line 1692
    .local v2, fragments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    const/4 v7, 0x0

    #@f
    .line 1693
    .local v7, retainLoaders:Z
    iget-object v8, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@11
    if-eqz v8, :cond_3f

    #@13
    .line 1696
    iget-object v8, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@15
    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    #@18
    move-result v8

    #@19
    new-array v5, v8, [Landroid/app/LoaderManagerImpl;

    #@1b
    .line 1697
    .local v5, loaders:[Landroid/app/LoaderManagerImpl;
    iget-object v8, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@20
    move-result-object v8

    #@21
    invoke-interface {v8, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@24
    .line 1698
    if-eqz v5, :cond_3f

    #@26
    .line 1699
    const/4 v3, 0x0

    #@27
    .local v3, i:I
    :goto_27
    array-length v8, v5

    #@28
    if-ge v3, v8, :cond_3f

    #@2a
    .line 1700
    aget-object v4, v5, v3

    #@2c
    .line 1701
    .local v4, lm:Landroid/app/LoaderManagerImpl;
    iget-boolean v8, v4, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    #@2e
    if-eqz v8, :cond_34

    #@30
    .line 1702
    const/4 v7, 0x1

    #@31
    .line 1699
    :goto_31
    add-int/lit8 v3, v3, 0x1

    #@33
    goto :goto_27

    #@34
    .line 1704
    :cond_34
    invoke-virtual {v4}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    #@37
    .line 1705
    iget-object v8, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@39
    iget-object v9, v4, Landroid/app/LoaderManagerImpl;->mWho:Ljava/lang/String;

    #@3b
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    goto :goto_31

    #@3f
    .line 1710
    .end local v3           #i:I
    .end local v4           #lm:Landroid/app/LoaderManagerImpl;
    .end local v5           #loaders:[Landroid/app/LoaderManagerImpl;
    :cond_3f
    if-nez v0, :cond_49

    #@41
    if-nez v1, :cond_49

    #@43
    if-nez v2, :cond_49

    #@45
    if-nez v7, :cond_49

    #@47
    .line 1711
    const/4 v6, 0x0

    #@48
    .line 1719
    :goto_48
    return-object v6

    #@49
    .line 1714
    :cond_49
    new-instance v6, Landroid/app/Activity$NonConfigurationInstances;

    #@4b
    invoke-direct {v6}, Landroid/app/Activity$NonConfigurationInstances;-><init>()V

    #@4e
    .line 1715
    .local v6, nci:Landroid/app/Activity$NonConfigurationInstances;
    iput-object v0, v6, Landroid/app/Activity$NonConfigurationInstances;->activity:Ljava/lang/Object;

    #@50
    .line 1716
    iput-object v1, v6, Landroid/app/Activity$NonConfigurationInstances;->children:Ljava/util/HashMap;

    #@52
    .line 1717
    iput-object v2, v6, Landroid/app/Activity$NonConfigurationInstances;->fragments:Ljava/util/ArrayList;

    #@54
    .line 1718
    iget-object v8, p0, Landroid/app/Activity;->mAllLoaderManagers:Ljava/util/HashMap;

    #@56
    iput-object v8, v6, Landroid/app/Activity$NonConfigurationInstances;->loaders:Ljava/util/HashMap;

    #@58
    goto :goto_48
.end method

.method public final runOnUiThread(Ljava/lang/Runnable;)V
    .registers 4
    .parameter "action"

    #@0
    .prologue
    .line 4737
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/Activity;->mUiThread:Ljava/lang/Thread;

    #@6
    if-eq v0, v1, :cond_e

    #@8
    .line 4738
    iget-object v0, p0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@d
    .line 4742
    :goto_d
    return-void

    #@e
    .line 4740
    :cond_e
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    #@11
    goto :goto_d
.end method

.method public setContentView(I)V
    .registers 3
    .parameter "layoutResID"

    #@0
    .prologue
    .line 1964
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(I)V

    #@7
    .line 1965
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@a
    .line 1966
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 1984
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    #@7
    .line 1985
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@a
    .line 1986
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 2000
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@7
    .line 2001
    invoke-direct {p0}, Landroid/app/Activity;->initActionBar()V

    #@a
    .line 2002
    return-void
.end method

.method public final setDefaultKeyMode(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 2094
    iput p1, p0, Landroid/app/Activity;->mDefaultKeyMode:I

    #@2
    .line 2098
    packed-switch p1, :pswitch_data_1e

    #@5
    .line 2110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@a
    throw v0

    #@b
    .line 2101
    :pswitch_b
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@e
    .line 2112
    :goto_e
    return-void

    #@f
    .line 2106
    :pswitch_f
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@11
    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    #@14
    iput-object v0, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@16
    .line 2107
    iget-object v0, p0, Landroid/app/Activity;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    #@18
    const/4 v1, 0x0

    #@19
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@1c
    goto :goto_e

    #@1d
    .line 2098
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_b
        :pswitch_f
        :pswitch_b
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method public final setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "featureId"
    .parameter "drawable"

    #@0
    .prologue
    .line 3364
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@7
    .line 3365
    return-void
.end method

.method public final setFeatureDrawableAlpha(II)V
    .registers 4
    .parameter "featureId"
    .parameter "alpha"

    #@0
    .prologue
    .line 3372
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawableAlpha(II)V

    #@7
    .line 3373
    return-void
.end method

.method public final setFeatureDrawableResource(II)V
    .registers 4
    .parameter "featureId"
    .parameter "resId"

    #@0
    .prologue
    .line 3348
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    #@7
    .line 3349
    return-void
.end method

.method public final setFeatureDrawableUri(ILandroid/net/Uri;)V
    .registers 4
    .parameter "featureId"
    .parameter "uri"

    #@0
    .prologue
    .line 3356
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawableUri(ILandroid/net/Uri;)V

    #@7
    .line 3357
    return-void
.end method

.method public setFinishOnTouchOutside(Z)V
    .registers 3
    .parameter "finish"

    #@0
    .prologue
    .line 2021
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    #@5
    .line 2022
    return-void
.end method

.method public setImmersive(Z)V
    .registers 4
    .parameter "i"

    #@0
    .prologue
    .line 4945
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@6
    invoke-interface {v0, v1, p1}, Landroid/app/IActivityManager;->setImmersive(Landroid/os/IBinder;Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 4949
    :goto_9
    return-void

    #@a
    .line 4946
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .parameter "newIntent"

    #@0
    .prologue
    .line 789
    iput-object p1, p0, Landroid/app/Activity;->mIntent:Landroid/content/Intent;

    #@2
    .line 790
    return-void
.end method

.method final setParent(Landroid/app/Activity;)V
    .registers 2
    .parameter "parent"

    #@0
    .prologue
    .line 5138
    iput-object p1, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    .line 5139
    return-void
.end method

.method public setPersistent(Z)V
    .registers 2
    .parameter "isPersistent"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1913
    return-void
.end method

.method public final setProgress(I)V
    .registers 5
    .parameter "progress"

    #@0
    .prologue
    .line 4676
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x2

    #@5
    add-int/lit8 v2, p1, 0x0

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    #@a
    .line 4677
    return-void
.end method

.method public final setProgressBarIndeterminate(Z)V
    .registers 5
    .parameter "indeterminate"

    #@0
    .prologue
    .line 4661
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x2

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, -0x3

    #@8
    :goto_8
    invoke-virtual {v1, v2, v0}, Landroid/view/Window;->setFeatureInt(II)V

    #@b
    .line 4663
    return-void

    #@c
    .line 4661
    :cond_c
    const/4 v0, -0x4

    #@d
    goto :goto_8
.end method

.method public final setProgressBarIndeterminateVisibility(Z)V
    .registers 5
    .parameter "visible"

    #@0
    .prologue
    .line 4647
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x5

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, -0x1

    #@8
    :goto_8
    invoke-virtual {v1, v2, v0}, Landroid/view/Window;->setFeatureInt(II)V

    #@b
    .line 4649
    return-void

    #@c
    .line 4647
    :cond_c
    const/4 v0, -0x2

    #@d
    goto :goto_8
.end method

.method public final setProgressBarVisibility(Z)V
    .registers 5
    .parameter "visible"

    #@0
    .prologue
    .line 4634
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x2

    #@5
    if-eqz p1, :cond_c

    #@7
    const/4 v0, -0x1

    #@8
    :goto_8
    invoke-virtual {v1, v2, v0}, Landroid/view/Window;->setFeatureInt(II)V

    #@b
    .line 4636
    return-void

    #@c
    .line 4634
    :cond_c
    const/4 v0, -0x2

    #@d
    goto :goto_8
.end method

.method public setRequestedOrientation(I)V
    .registers 4
    .parameter "requestedOrientation"

    #@0
    .prologue
    .line 4418
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 4420
    :try_start_4
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@a
    invoke-interface {v0, v1, p1}, Landroid/app/IActivityManager;->setRequestedOrientation(Landroid/os/IBinder;I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_14

    #@d
    .line 4428
    :goto_d
    return-void

    #@e
    .line 4426
    :cond_e
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@10
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    #@13
    goto :goto_d

    #@14
    .line 4422
    :catch_14
    move-exception v0

    #@15
    goto :goto_d
.end method

.method public final setResult(I)V
    .registers 3
    .parameter "resultCode"

    #@0
    .prologue
    .line 4065
    monitor-enter p0

    #@1
    .line 4066
    :try_start_1
    iput p1, p0, Landroid/app/Activity;->mResultCode:I

    #@3
    .line 4067
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@6
    .line 4068
    monitor-exit p0

    #@7
    .line 4069
    return-void

    #@8
    .line 4068
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public final setResult(ILandroid/content/Intent;)V
    .registers 4
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 4094
    monitor-enter p0

    #@1
    .line 4095
    :try_start_1
    iput p1, p0, Landroid/app/Activity;->mResultCode:I

    #@3
    .line 4096
    iput-object p2, p0, Landroid/app/Activity;->mResultData:Landroid/content/Intent;

    #@5
    .line 4097
    monitor-exit p0

    #@6
    .line 4098
    return-void

    #@7
    .line 4097
    :catchall_7
    move-exception v0

    #@8
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public final setSecondaryProgress(I)V
    .registers 5
    .parameter "secondaryProgress"

    #@0
    .prologue
    .line 4693
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x2

    #@5
    add-int/lit16 v2, p1, 0x4e20

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    #@a
    .line 4695
    return-void
.end method

.method public setTitle(I)V
    .registers 3
    .parameter "titleId"

    #@0
    .prologue
    .line 4594
    invoke-virtual {p0, p1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    #@7
    .line 4595
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 4579
    iput-object p1, p0, Landroid/app/Activity;->mTitle:Ljava/lang/CharSequence;

    #@2
    .line 4580
    iget v0, p0, Landroid/app/Activity;->mTitleColor:I

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    #@7
    .line 4582
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 4583
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@d
    invoke-virtual {v0, p0, p1}, Landroid/app/Activity;->onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    #@10
    .line 4585
    :cond_10
    return-void
.end method

.method public setTitleColor(I)V
    .registers 3
    .parameter "textColor"

    #@0
    .prologue
    .line 4598
    iput p1, p0, Landroid/app/Activity;->mTitleColor:I

    #@2
    .line 4599
    iget-object v0, p0, Landroid/app/Activity;->mTitle:Ljava/lang/CharSequence;

    #@4
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    #@7
    .line 4600
    return-void
.end method

.method public setVisible(Z)V
    .registers 4
    .parameter "visible"

    #@0
    .prologue
    .line 4155
    iget-boolean v0, p0, Landroid/app/Activity;->mVisibleFromClient:Z

    #@2
    if-eq v0, p1, :cond_f

    #@4
    .line 4156
    iput-boolean p1, p0, Landroid/app/Activity;->mVisibleFromClient:Z

    #@6
    .line 4157
    iget-boolean v0, p0, Landroid/app/Activity;->mVisibleFromServer:Z

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 4158
    if-eqz p1, :cond_10

    #@c
    invoke-virtual {p0}, Landroid/app/Activity;->makeVisible()V

    #@f
    .line 4162
    :cond_f
    :goto_f
    return-void

    #@10
    .line 4159
    :cond_10
    iget-object v0, p0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@12
    const/4 v1, 0x4

    #@13
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@16
    goto :goto_f
.end method

.method public final setVolumeControlStream(I)V
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 4714
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setVolumeControlStream(I)V

    #@7
    .line 4715
    return-void
.end method

.method public shouldUpRecreateTask(Landroid/content/Intent;)Z
    .registers 10
    .parameter "targetIntent"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 5015
    :try_start_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@4
    move-result-object v3

    #@5
    .line 5016
    .local v3, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@8
    move-result-object v0

    #@9
    .line 5017
    .local v0, cn:Landroid/content/ComponentName;
    if-nez v0, :cond_f

    #@b
    .line 5018
    invoke-virtual {p1, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    #@e
    move-result-object v0

    #@f
    .line 5020
    :cond_f
    const/4 v5, 0x0

    #@10
    invoke-virtual {v3, v0, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@13
    move-result-object v2

    #@14
    .line 5021
    .local v2, info:Landroid/content/pm/ActivityInfo;
    iget-object v5, v2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@16
    if-nez v5, :cond_19

    #@18
    .line 5029
    .end local v0           #cn:Landroid/content/ComponentName;
    .end local v2           #info:Landroid/content/pm/ActivityInfo;
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :cond_18
    :goto_18
    return v4

    #@19
    .line 5024
    .restart local v0       #cn:Landroid/content/ComponentName;
    .restart local v2       #info:Landroid/content/pm/ActivityInfo;
    .restart local v3       #pm:Landroid/content/pm/PackageManager;
    :cond_19
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1c
    move-result-object v5

    #@1d
    iget-object v6, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@1f
    iget-object v7, v2, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;

    #@21
    invoke-interface {v5, v6, v7}, Landroid/app/IActivityManager;->targetTaskAffinityMatchesActivity(Landroid/os/IBinder;Ljava/lang/String;)Z
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_24} :catch_29
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_24} :catch_2b

    #@24
    move-result v5

    #@25
    if-nez v5, :cond_18

    #@27
    const/4 v4, 0x1

    #@28
    goto :goto_18

    #@29
    .line 5026
    .end local v0           #cn:Landroid/content/ComponentName;
    .end local v2           #info:Landroid/content/pm/ActivityInfo;
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :catch_29
    move-exception v1

    #@2a
    .line 5027
    .local v1, e:Landroid/os/RemoteException;
    goto :goto_18

    #@2b
    .line 5028
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_2b
    move-exception v1

    #@2c
    .line 5029
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_18
.end method

.method public final showDialog(I)V
    .registers 3
    .parameter "id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 3117
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    #@4
    .line 3118
    return-void
.end method

.method public final showDialog(ILandroid/os/Bundle;)Z
    .registers 6
    .parameter "id"
    .parameter "args"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3152
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@3
    if-nez v1, :cond_c

    #@5
    .line 3153
    new-instance v1, Landroid/util/SparseArray;

    #@7
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    #@a
    iput-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@c
    .line 3155
    :cond_c
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/app/Activity$ManagedDialog;

    #@14
    .line 3156
    .local v0, md:Landroid/app/Activity$ManagedDialog;
    if-nez v0, :cond_30

    #@16
    .line 3157
    new-instance v0, Landroid/app/Activity$ManagedDialog;

    #@18
    .end local v0           #md:Landroid/app/Activity$ManagedDialog;
    invoke-direct {v0, v2}, Landroid/app/Activity$ManagedDialog;-><init>(Landroid/app/Activity$1;)V

    #@1b
    .line 3158
    .restart local v0       #md:Landroid/app/Activity$ManagedDialog;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {p0, v1, v2, p2}, Landroid/app/Activity;->createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;

    #@22
    move-result-object v1

    #@23
    iput-object v1, v0, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@25
    .line 3159
    iget-object v1, v0, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@27
    if-nez v1, :cond_2b

    #@29
    .line 3160
    const/4 v1, 0x0

    #@2a
    .line 3168
    :goto_2a
    return v1

    #@2b
    .line 3162
    :cond_2b
    iget-object v1, p0, Landroid/app/Activity;->mManagedDialogs:Landroid/util/SparseArray;

    #@2d
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@30
    .line 3165
    :cond_30
    iput-object p2, v0, Landroid/app/Activity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    #@32
    .line 3166
    iget-object v1, v0, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@34
    invoke-virtual {p0, p1, v1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    #@37
    .line 3167
    iget-object v1, v0, Landroid/app/Activity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    #@39
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    #@3c
    .line 3168
    const/4 v1, 0x1

    #@3d
    goto :goto_2a
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 4960
    iget-object v0, p0, Landroid/app/Activity;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/view/View;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public startActivities([Landroid/content/Intent;)V
    .registers 3
    .parameter "intents"

    #@0
    .prologue
    .line 3679
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V

    #@4
    .line 3680
    return-void
.end method

.method public startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 10
    .parameter "intents"
    .parameter "options"

    #@0
    .prologue
    .line 3706
    iget-object v0, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@4
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@a
    move-object v1, p0

    #@b
    move-object v4, p0

    #@c
    move-object v5, p1

    #@d
    move-object v6, p2

    #@e
    invoke-virtual/range {v0 .. v6}, Landroid/app/Instrumentation;->execStartActivities(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;[Landroid/content/Intent;Landroid/os/Bundle;)V

    #@11
    .line 3708
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 3630
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    #@4
    .line 3631
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 4
    .parameter "intent"
    .parameter "options"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 3657
    if-eqz p2, :cond_7

    #@3
    .line 3658
    invoke-virtual {p0, p1, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@6
    .line 3664
    :goto_6
    return-void

    #@7
    .line 3662
    :cond_7
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    #@a
    goto :goto_6
.end method

.method public startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    .registers 14
    .parameter "intent"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    const/4 v6, -0x1

    #@1
    .line 3507
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@3
    if-eqz v0, :cond_d

    #@5
    .line 3508
    new-instance v0, Ljava/lang/RuntimeException;

    #@7
    const-string v1, "Called be called from a child"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 3510
    :cond_d
    iget-object v0, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@f
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@11
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@14
    move-result-object v2

    #@15
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@17
    move-object v1, p0

    #@18
    move-object v4, p0

    #@19
    move-object v5, p1

    #@1a
    move-object v7, p2

    #@1b
    move-object v8, p3

    #@1c
    invoke-virtual/range {v0 .. v8}, Landroid/app/Instrumentation;->execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/Instrumentation$ActivityResult;

    #@1f
    move-result-object v9

    #@20
    .line 3514
    .local v9, ar:Landroid/app/Instrumentation$ActivityResult;
    if-eqz v9, :cond_33

    #@22
    .line 3515
    iget-object v3, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@24
    iget-object v4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@26
    iget-object v5, p0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@28
    invoke-virtual {v9}, Landroid/app/Instrumentation$ActivityResult;->getResultCode()I

    #@2b
    move-result v7

    #@2c
    invoke-virtual {v9}, Landroid/app/Instrumentation$ActivityResult;->getResultData()Landroid/content/Intent;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual/range {v3 .. v8}, Landroid/app/ActivityThread;->sendActivityResult(Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/Intent;)V

    #@33
    .line 3519
    :cond_33
    return-void
.end method

.method public startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 4
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 3500
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0, p2}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@4
    .line 3501
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .registers 4
    .parameter "intent"
    .parameter "requestCode"

    #@0
    .prologue
    .line 3427
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@4
    .line 3428
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .registers 13
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 3465
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_31

    #@4
    .line 3466
    iget-object v0, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@6
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@8
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@b
    move-result-object v2

    #@c
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@e
    move-object v1, p0

    #@f
    move-object v4, p0

    #@10
    move-object v5, p1

    #@11
    move v6, p2

    #@12
    move-object v7, p3

    #@13
    invoke-virtual/range {v0 .. v7}, Landroid/app/Instrumentation;->execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;

    #@16
    move-result-object v8

    #@17
    .line 3470
    .local v8, ar:Landroid/app/Instrumentation$ActivityResult;
    if-eqz v8, :cond_2b

    #@19
    .line 3471
    iget-object v0, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@1b
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@1d
    iget-object v2, p0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@1f
    invoke-virtual {v8}, Landroid/app/Instrumentation$ActivityResult;->getResultCode()I

    #@22
    move-result v4

    #@23
    invoke-virtual {v8}, Landroid/app/Instrumentation$ActivityResult;->getResultData()Landroid/content/Intent;

    #@26
    move-result-object v5

    #@27
    move v3, p2

    #@28
    invoke-virtual/range {v0 .. v5}, Landroid/app/ActivityThread;->sendActivityResult(Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/Intent;)V

    #@2b
    .line 3475
    :cond_2b
    if-ltz p2, :cond_30

    #@2d
    .line 3483
    const/4 v0, 0x1

    #@2e
    iput-boolean v0, p0, Landroid/app/Activity;->mStartedActivity:Z

    #@30
    .line 3494
    .end local v8           #ar:Landroid/app/Instrumentation$ActivityResult;
    :cond_30
    :goto_30
    return-void

    #@31
    .line 3486
    :cond_31
    if-eqz p3, :cond_39

    #@33
    .line 3487
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@35
    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/app/Activity;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@38
    goto :goto_30

    #@39
    .line 3491
    :cond_39
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@3b
    invoke-virtual {v0, p0, p1, p2}, Landroid/app/Activity;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V

    #@3e
    goto :goto_30
.end method

.method public startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;I)V
    .registers 5
    .parameter "child"
    .parameter "intent"
    .parameter "requestCode"

    #@0
    .prologue
    .line 3915
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/app/Activity;->startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@4
    .line 3916
    return-void
.end method

.method public startActivityFromChild(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .registers 14
    .parameter "child"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 3939
    iget-object v0, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@4
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@a
    move-object v1, p0

    #@b
    move-object v4, p1

    #@c
    move-object v5, p2

    #@d
    move v6, p3

    #@e
    move-object v7, p4

    #@f
    invoke-virtual/range {v0 .. v7}, Landroid/app/Instrumentation;->execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;

    #@12
    move-result-object v8

    #@13
    .line 3943
    .local v8, ar:Landroid/app/Instrumentation$ActivityResult;
    if-eqz v8, :cond_27

    #@15
    .line 3944
    iget-object v0, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@17
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@19
    iget-object v2, p1, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@1b
    invoke-virtual {v8}, Landroid/app/Instrumentation$ActivityResult;->getResultCode()I

    #@1e
    move-result v4

    #@1f
    invoke-virtual {v8}, Landroid/app/Instrumentation$ActivityResult;->getResultData()Landroid/content/Intent;

    #@22
    move-result-object v5

    #@23
    move v3, p3

    #@24
    invoke-virtual/range {v0 .. v5}, Landroid/app/ActivityThread;->sendActivityResult(Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/Intent;)V

    #@27
    .line 3948
    :cond_27
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .registers 5
    .parameter "fragment"
    .parameter "intent"
    .parameter "requestCode"

    #@0
    .prologue
    .line 3965
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/app/Activity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@4
    .line 3966
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .registers 14
    .parameter "fragment"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 3990
    iget-object v0, p0, Landroid/app/Activity;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@4
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@a
    move-object v1, p0

    #@b
    move-object v4, p1

    #@c
    move-object v5, p2

    #@d
    move v6, p3

    #@e
    move-object v7, p4

    #@f
    invoke-virtual/range {v0 .. v7}, Landroid/app/Instrumentation;->execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;

    #@12
    move-result-object v8

    #@13
    .line 3994
    .local v8, ar:Landroid/app/Instrumentation$ActivityResult;
    if-eqz v8, :cond_27

    #@15
    .line 3995
    iget-object v0, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@17
    iget-object v1, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@19
    iget-object v2, p1, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@1b
    invoke-virtual {v8}, Landroid/app/Instrumentation$ActivityResult;->getResultCode()I

    #@1e
    move-result v4

    #@1f
    invoke-virtual {v8}, Landroid/app/Instrumentation$ActivityResult;->getResultData()Landroid/content/Intent;

    #@22
    move-result-object v5

    #@23
    move v3, p3

    #@24
    invoke-virtual/range {v0 .. v5}, Landroid/app/ActivityThread;->sendActivityResult(Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/Intent;)V

    #@27
    .line 3999
    :cond_27
    return-void
.end method

.method public startActivityIfNeeded(Landroid/content/Intent;I)Z
    .registers 4
    .parameter "intent"
    .parameter "requestCode"

    #@0
    .prologue
    .line 3780
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/Activity;->startActivityIfNeeded(Landroid/content/Intent;ILandroid/os/Bundle;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public startActivityIfNeeded(Landroid/content/Intent;ILandroid/os/Bundle;)Z
    .registers 16
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 3814
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_38

    #@4
    .line 3815
    const/4 v11, 0x1

    #@5
    .line 3817
    .local v11, result:I
    const/4 v0, 0x0

    #@6
    :try_start_6
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@9
    .line 3818
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Landroid/app/Activity;->mMainThread:Landroid/app/ActivityThread;

    #@f
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {p1, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    iget-object v4, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@1d
    iget-object v5, p0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@1f
    const/4 v7, 0x1

    #@20
    const/4 v8, 0x0

    #@21
    const/4 v9, 0x0

    #@22
    move-object v2, p1

    #@23
    move v6, p2

    #@24
    move-object v10, p3

    #@25
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->startActivity(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)I
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_28} :catch_41

    #@28
    move-result v11

    #@29
    .line 3828
    :goto_29
    invoke-static {v11, p1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V

    #@2c
    .line 3830
    if-ltz p2, :cond_31

    #@2e
    .line 3838
    const/4 v0, 0x1

    #@2f
    iput-boolean v0, p0, Landroid/app/Activity;->mStartedActivity:Z

    #@31
    .line 3840
    :cond_31
    const/4 v0, 0x1

    #@32
    if-eq v11, v0, :cond_36

    #@34
    const/4 v0, 0x1

    #@35
    :goto_35
    return v0

    #@36
    :cond_36
    const/4 v0, 0x0

    #@37
    goto :goto_35

    #@38
    .line 3843
    .end local v11           #result:I
    :cond_38
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@3a
    const-string/jumbo v1, "startActivityIfNeeded can only be called from a top-level activity"

    #@3d
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@40
    throw v0

    #@41
    .line 3824
    .restart local v11       #result:I
    :catch_41
    move-exception v0

    #@42
    goto :goto_29
.end method

.method public startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    .registers 13
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 3726
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V

    #@a
    .line 3728
    return-void
.end method

.method public startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V
    .registers 15
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 3753
    if-eqz p6, :cond_e

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v3, p2

    #@6
    move v4, p3

    #@7
    move v5, p4

    #@8
    move v6, p5

    #@9
    move-object v7, p6

    #@a
    .line 3754
    invoke-virtual/range {v0 .. v7}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V

    #@d
    .line 3762
    :goto_d
    return-void

    #@e
    :cond_e
    move-object v0, p0

    #@f
    move-object v1, p1

    #@10
    move-object v3, p2

    #@11
    move v4, p3

    #@12
    move v5, p4

    #@13
    move v6, p5

    #@14
    .line 3759
    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    #@17
    goto :goto_d
.end method

.method public startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    .registers 15
    .parameter "intent"
    .parameter "requestCode"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 3539
    const/4 v7, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    invoke-virtual/range {v0 .. v7}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V

    #@b
    .line 3541
    return-void
.end method

.method public startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V
    .registers 17
    .parameter "intent"
    .parameter "requestCode"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 3571
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_11

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move v2, p2

    #@7
    move-object v3, p3

    #@8
    move v4, p4

    #@9
    move v5, p5

    #@a
    move-object v6, p0

    #@b
    move-object/from16 v7, p7

    #@d
    .line 3572
    invoke-direct/range {v0 .. v7}, Landroid/app/Activity;->startIntentSenderForResultInner(Landroid/content/IntentSender;ILandroid/content/Intent;IILandroid/app/Activity;Landroid/os/Bundle;)V

    #@10
    .line 3583
    :goto_10
    return-void

    #@11
    .line 3574
    :cond_11
    if-eqz p7, :cond_22

    #@13
    .line 3575
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@15
    move-object v1, p0

    #@16
    move-object v2, p1

    #@17
    move v3, p2

    #@18
    move-object v4, p3

    #@19
    move v5, p4

    #@1a
    move v6, p5

    #@1b
    move v7, p6

    #@1c
    move-object/from16 v8, p7

    #@1e
    invoke-virtual/range {v0 .. v8}, Landroid/app/Activity;->startIntentSenderFromChild(Landroid/app/Activity;Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V

    #@21
    goto :goto_10

    #@22
    .line 3580
    :cond_22
    iget-object v0, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@24
    move-object v1, p0

    #@25
    move-object v2, p1

    #@26
    move v3, p2

    #@27
    move-object v4, p3

    #@28
    move v5, p4

    #@29
    move v6, p5

    #@2a
    move v7, p6

    #@2b
    invoke-virtual/range {v0 .. v7}, Landroid/app/Activity;->startIntentSenderFromChild(Landroid/app/Activity;Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    #@2e
    goto :goto_10
.end method

.method public startIntentSenderFromChild(Landroid/app/Activity;Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    .registers 17
    .parameter "child"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 4009
    const/4 v8, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    move/from16 v7, p7

    #@a
    invoke-virtual/range {v0 .. v8}, Landroid/app/Activity;->startIntentSenderFromChild(Landroid/app/Activity;Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V

    #@d
    .line 4011
    return-void
.end method

.method public startIntentSenderFromChild(Landroid/app/Activity;Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V
    .registers 17
    .parameter "child"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 4023
    move-object v0, p0

    #@1
    move-object v1, p2

    #@2
    move v2, p3

    #@3
    move-object v3, p4

    #@4
    move v4, p5

    #@5
    move v5, p6

    #@6
    move-object v6, p1

    #@7
    move-object/from16 v7, p8

    #@9
    invoke-direct/range {v0 .. v7}, Landroid/app/Activity;->startIntentSenderForResultInner(Landroid/content/IntentSender;ILandroid/content/Intent;IILandroid/app/Activity;Landroid/os/Bundle;)V

    #@c
    .line 4025
    return-void
.end method

.method public startManagingCursor(Landroid/database/Cursor;)V
    .registers 5
    .parameter "c"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1870
    iget-object v1, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 1871
    :try_start_3
    iget-object v0, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@5
    new-instance v2, Landroid/app/Activity$ManagedCursor;

    #@7
    invoke-direct {v2, p1}, Landroid/app/Activity$ManagedCursor;-><init>(Landroid/database/Cursor;)V

    #@a
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 1872
    monitor-exit v1

    #@e
    .line 1873
    return-void

    #@f
    .line 1872
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public startNextMatchingActivity(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 3862
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->startNextMatchingActivity(Landroid/content/Intent;Landroid/os/Bundle;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public startNextMatchingActivity(Landroid/content/Intent;Landroid/os/Bundle;)Z
    .registers 6
    .parameter "intent"
    .parameter "options"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3885
    iget-object v1, p0, Landroid/app/Activity;->mParent:Landroid/app/Activity;

    #@3
    if-nez v1, :cond_14

    #@5
    .line 3887
    const/4 v1, 0x0

    #@6
    :try_start_6
    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAllowFds(Z)V

    #@9
    .line 3888
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Landroid/app/Activity;->mToken:Landroid/os/IBinder;

    #@f
    invoke-interface {v1, v2, p1, p2}, Landroid/app/IActivityManager;->startNextMatchingActivity(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_12} :catch_1d

    #@12
    move-result v0

    #@13
    .line 3893
    :goto_13
    return v0

    #@14
    .line 3896
    :cond_14
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@16
    const-string/jumbo v1, "startNextMatchingActivity can only be called from a top-level activity"

    #@19
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v0

    #@1d
    .line 3890
    :catch_1d
    move-exception v1

    #@1e
    goto :goto_13
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 11
    .parameter "initialQuery"
    .parameter "selectInitialQuery"
    .parameter "appSearchData"
    .parameter "globalSearch"

    #@0
    .prologue
    .line 3297
    invoke-direct {p0}, Landroid/app/Activity;->ensureSearchManager()V

    #@3
    .line 3298
    iget-object v0, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@5
    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@8
    move-result-object v3

    #@9
    move-object v1, p1

    #@a
    move v2, p2

    #@b
    move-object v4, p3

    #@c
    move v5, p4

    #@d
    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    #@10
    .line 3300
    return-void
.end method

.method public stopManagingCursor(Landroid/database/Cursor;)V
    .registers 7
    .parameter "c"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1894
    iget-object v4, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 1895
    :try_start_3
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 1896
    .local v0, N:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_1f

    #@c
    .line 1897
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Landroid/app/Activity$ManagedCursor;

    #@14
    .line 1898
    .local v2, mc:Landroid/app/Activity$ManagedCursor;
    invoke-static {v2}, Landroid/app/Activity$ManagedCursor;->access$100(Landroid/app/Activity$ManagedCursor;)Landroid/database/Cursor;

    #@17
    move-result-object v3

    #@18
    if-ne v3, p1, :cond_21

    #@1a
    .line 1899
    iget-object v3, p0, Landroid/app/Activity;->mManagedCursors:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1f
    .line 1903
    .end local v2           #mc:Landroid/app/Activity$ManagedCursor;
    :cond_1f
    monitor-exit v4

    #@20
    .line 1904
    return-void

    #@21
    .line 1896
    .restart local v2       #mc:Landroid/app/Activity$ManagedCursor;
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_a

    #@24
    .line 1903
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #mc:Landroid/app/Activity$ManagedCursor;
    :catchall_24
    move-exception v3

    #@25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v3
.end method

.method public takeKeyEvents(Z)V
    .registers 3
    .parameter "get"

    #@0
    .prologue
    .line 3325
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->takeKeyEvents(Z)V

    #@7
    .line 3326
    return-void
.end method

.method public triggerSearch(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5
    .parameter "query"
    .parameter "appSearchData"

    #@0
    .prologue
    .line 3313
    invoke-direct {p0}, Landroid/app/Activity;->ensureSearchManager()V

    #@3
    .line 3314
    iget-object v0, p0, Landroid/app/Activity;->mSearchManager:Landroid/app/SearchManager;

    #@5
    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, p1, v1, p2}, Landroid/app/SearchManager;->triggerSearch(Ljava/lang/String;Landroid/content/ComponentName;Landroid/os/Bundle;)V

    #@c
    .line 3315
    return-void
.end method

.method public unregisterForContextMenu(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 2964
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@4
    .line 2965
    return-void
.end method
