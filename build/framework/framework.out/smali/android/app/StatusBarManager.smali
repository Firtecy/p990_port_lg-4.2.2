.class public Landroid/app/StatusBarManager;
.super Ljava/lang/Object;
.source "StatusBarManager.java"


# static fields
.field public static final DISABLE_BACK:I = 0x400000

.field public static final DISABLE_CLOCK:I = 0x800000

.field public static final DISABLE_EXPAND:I = 0x10000

.field public static final DISABLE_HOME:I = 0x200000

.field public static final DISABLE_MASK:I = 0x7ff0000

.field public static final DISABLE_NAVIGATION:I = 0x1200000
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DISABLE_NONE:I = 0x0

.field public static final DISABLE_NOTIFICATION:I = 0x4000000

.field public static final DISABLE_NOTIFICATION_ALERTS:I = 0x40000

.field public static final DISABLE_NOTIFICATION_ICONS:I = 0x20000

.field public static final DISABLE_NOTIFICATION_TICKER:I = 0x80000

.field public static final DISABLE_RECENT:I = 0x1000000

.field public static final DISABLE_SEARCH:I = 0x2000000

.field public static final DISABLE_SYSTEM_INFO:I = 0x100000

.field public static final NAVIGATION_HINT_BACK_ALT:I = 0x8

.field public static final NAVIGATION_HINT_BACK_NOP:I = 0x1

.field public static final NAVIGATION_HINT_HOME_NOP:I = 0x2

.field public static final NAVIGATION_HINT_RECENT_NOP:I = 0x4

.field public static final SYSTEMBAR_TYPE_NONE:I = 0x1

.field public static final SYSTEMBAR_TYPE_TRANSPARENT_ALL:I = 0x8

.field public static final SYSTEMBAR_TYPE_TRANSPARENT_NAVIGATIONBAR:I = 0x4

.field public static final SYSTEMBAR_TYPE_TRANSPARENT_STATUSBAR:I = 0x2


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Lcom/android/internal/statusbar/IStatusBarService;

.field private mToken:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    new-instance v0, Landroid/os/Binder;

    #@5
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@8
    iput-object v0, p0, Landroid/app/StatusBarManager;->mToken:Landroid/os/IBinder;

    #@a
    .line 83
    iput-object p1, p0, Landroid/app/StatusBarManager;->mContext:Landroid/content/Context;

    #@c
    .line 84
    return-void
.end method

.method private declared-synchronized getService()Lcom/android/internal/statusbar/IStatusBarService;
    .registers 3

    #@0
    .prologue
    .line 87
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/app/StatusBarManager;->mService:Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    if-nez v0, :cond_1e

    #@5
    .line 88
    const-string/jumbo v0, "statusbar"

    #@8
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@b
    move-result-object v0

    #@c
    invoke-static {v0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Landroid/app/StatusBarManager;->mService:Lcom/android/internal/statusbar/IStatusBarService;

    #@12
    .line 90
    iget-object v0, p0, Landroid/app/StatusBarManager;->mService:Lcom/android/internal/statusbar/IStatusBarService;

    #@14
    if-nez v0, :cond_1e

    #@16
    .line 91
    const-string v0, "StatusBarManager"

    #@18
    const-string/jumbo v1, "warning: no STATUS_BAR_SERVICE"

    #@1b
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 94
    :cond_1e
    iget-object v0, p0, Landroid/app/StatusBarManager;->mService:Lcom/android/internal/statusbar/IStatusBarService;
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_22

    #@20
    monitor-exit p0

    #@21
    return-object v0

    #@22
    .line 87
    :catchall_22
    move-exception v0

    #@23
    monitor-exit p0

    #@24
    throw v0
.end method


# virtual methods
.method public collapsePanels()V
    .registers 4

    #@0
    .prologue
    .line 159
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 160
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 161
    invoke-interface {v1}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 167
    :cond_9
    return-void

    #@a
    .line 163
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 165
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public disable(I)V
    .registers 9
    .parameter "what"

    #@0
    .prologue
    .line 103
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v3

    #@4
    .line 104
    .local v3, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v3, :cond_a3

    #@6
    .line 105
    iget-object v4, p0, Landroid/app/StatusBarManager;->mContext:Landroid/content/Context;

    #@8
    if-eqz v4, :cond_87

    #@a
    .line 106
    iget-object v4, p0, Landroid/app/StatusBarManager;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    .line 107
    .local v2, packageName:Ljava/lang/String;
    const-string v4, "StatusBarManager"

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "StatusBarManager.disable(0x"

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@20
    move-result-object v6

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    const-string v6, ") called from "

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 109
    const/high16 v4, 0x200

    #@38
    and-int/2addr v4, p1

    #@39
    if-eqz v4, :cond_53

    #@3b
    .line 110
    const-string v4, "StatusBarManager"

    #@3d
    new-instance v5, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v6, "DISABLE_SEARCH found, GlobalAccess(SearchPanel) will be disabled by "

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v5

    #@50
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 113
    :cond_53
    const-string v4, "android"

    #@55
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v4

    #@59
    if-eqz v4, :cond_87

    #@5b
    .line 114
    new-instance v1, Ljava/lang/RuntimeException;

    #@5d
    new-instance v4, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v5, "StatusBarManager.disable(0x"

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    const-string v5, ") Call Stack"

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v4

    #@7a
    invoke-direct {v1, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7d
    .line 115
    .local v1, here:Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@80
    .line 116
    const-string v4, "StatusBarManager"

    #@82
    const-string v5, "==============================="

    #@84
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@87
    .line 121
    .end local v1           #here:Ljava/lang/RuntimeException;
    .end local v2           #packageName:Ljava/lang/String;
    :cond_87
    if-nez p1, :cond_a4

    #@89
    .line 122
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@8c
    move-result-object v4

    #@8d
    if-eqz v4, :cond_a4

    #@8f
    .line 123
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@92
    move-result-object v4

    #@93
    const/4 v5, 0x0

    #@94
    const-string v6, "LGMDMStatusBarAdapter"

    #@96
    invoke-interface {v4, v5, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@99
    move-result v4

    #@9a
    if-eqz v4, :cond_a4

    #@9c
    .line 125
    const-string v4, "MDM"

    #@9e
    const-string v5, "LGMDM : Disable Expand StatusBar!!"

    #@a0
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 137
    :cond_a3
    :goto_a3
    return-void

    #@a4
    .line 131
    :cond_a4
    iget-object v4, p0, Landroid/app/StatusBarManager;->mToken:Landroid/os/IBinder;

    #@a6
    iget-object v5, p0, Landroid/app/StatusBarManager;->mContext:Landroid/content/Context;

    #@a8
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@ab
    move-result-object v5

    #@ac
    invoke-interface {v3, p1, v4, v5}, Lcom/android/internal/statusbar/IStatusBarService;->disable(ILandroid/os/IBinder;Ljava/lang/String;)V
    :try_end_af
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_af} :catch_b0

    #@af
    goto :goto_a3

    #@b0
    .line 133
    .end local v3           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_b0
    move-exception v0

    #@b1
    .line 135
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@b3
    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@b6
    throw v4
.end method

.method public disableTouch(Z)V
    .registers 5
    .parameter "disable"

    #@0
    .prologue
    .line 223
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 224
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 225
    invoke-interface {v1, p1}, Lcom/android/internal/statusbar/IStatusBarService;->disableTouch(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 231
    :cond_9
    return-void

    #@a
    .line 227
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 229
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public expandNotificationsPanel()V
    .registers 4

    #@0
    .prologue
    .line 144
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 145
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 146
    invoke-interface {v1}, Lcom/android/internal/statusbar/IStatusBarService;->expandNotificationsPanel()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 152
    :cond_9
    return-void

    #@a
    .line 148
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 150
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public expandSettingsPanel()V
    .registers 4

    #@0
    .prologue
    .line 174
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 175
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 176
    invoke-interface {v1}, Lcom/android/internal/statusbar/IStatusBarService;->expandSettingsPanel()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 182
    :cond_9
    return-void

    #@a
    .line 178
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 180
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public removeIcon(Ljava/lang/String;)V
    .registers 5
    .parameter "slot"

    #@0
    .prologue
    .line 199
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 200
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 201
    invoke-interface {v1, p1}, Lcom/android/internal/statusbar/IStatusBarService;->removeIcon(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 207
    :cond_9
    return-void

    #@a
    .line 203
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 205
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public setIcon(Ljava/lang/String;IILjava/lang/String;)V
    .registers 12
    .parameter "slot"
    .parameter "iconId"
    .parameter "iconLevel"
    .parameter "contentDescription"

    #@0
    .prologue
    .line 186
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v0

    #@4
    .line 187
    .local v0, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v0, :cond_13

    #@6
    .line 188
    iget-object v1, p0, Landroid/app/StatusBarManager;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    move-object v1, p1

    #@d
    move v3, p2

    #@e
    move v4, p3

    #@f
    move-object v5, p4

    #@10
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/statusbar/IStatusBarService;->setIcon(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_13} :catch_14

    #@13
    .line 195
    :cond_13
    return-void

    #@14
    .line 191
    .end local v0           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_14
    move-exception v6

    #@15
    .line 193
    .local v6, ex:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@17
    invoke-direct {v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@1a
    throw v1
.end method

.method public setIconVisibility(Ljava/lang/String;Z)V
    .registers 6
    .parameter "slot"
    .parameter "visible"

    #@0
    .prologue
    .line 211
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 212
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 213
    invoke-interface {v1, p1, p2}, Lcom/android/internal/statusbar/IStatusBarService;->setIconVisibility(Ljava/lang/String;Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 219
    :cond_9
    return-void

    #@a
    .line 215
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 217
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public setNavigationBackground(Ljava/lang/String;IIII)V
    .registers 13
    .parameter "pkg"
    .parameter "portResId"
    .parameter "landResId"
    .parameter "alpha"
    .parameter "reserved"

    #@0
    .prologue
    .line 247
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v0

    #@4
    .line 248
    .local v0, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v0, :cond_e

    #@6
    move-object v1, p1

    #@7
    move v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    move v5, p5

    #@b
    .line 249
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/statusbar/IStatusBarService;->setNavigationBackground(Ljava/lang/String;IIII)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_f

    #@e
    .line 255
    :cond_e
    return-void

    #@f
    .line 251
    .end local v0           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_f
    move-exception v6

    #@10
    .line 253
    .local v6, ex:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@12
    invoke-direct {v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@15
    throw v1
.end method

.method public setNavigationRotation(III)V
    .registers 7
    .parameter "rotation"
    .parameter "direction"
    .parameter "duration"

    #@0
    .prologue
    .line 258
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 259
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 260
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/statusbar/IStatusBarService;->setNavigationRotation(III)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 266
    :cond_9
    return-void

    #@a
    .line 262
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 264
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method

.method public setSystemBarType(I)V
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 236
    :try_start_0
    invoke-direct {p0}, Landroid/app/StatusBarManager;->getService()Lcom/android/internal/statusbar/IStatusBarService;

    #@3
    move-result-object v1

    #@4
    .line 237
    .local v1, svc:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v1, :cond_9

    #@6
    .line 238
    invoke-interface {v1, p1}, Lcom/android/internal/statusbar/IStatusBarService;->setSystemBarType(I)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 244
    :cond_9
    return-void

    #@a
    .line 240
    .end local v1           #svc:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_a
    move-exception v0

    #@b
    .line 242
    .local v0, ex:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@d
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@10
    throw v2
.end method
