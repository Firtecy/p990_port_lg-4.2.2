.class Landroid/app/IProcessObserver$Stub$Proxy;
.super Ljava/lang/Object;
.source "IProcessObserver.java"

# interfaces
.implements Landroid/app/IProcessObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IProcessObserver$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    iput-object p1, p0, Landroid/app/IProcessObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 89
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Landroid/app/IProcessObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 96
    const-string v0, "android.app.IProcessObserver"

    #@2
    return-object v0
.end method

.method public onForegroundActivitiesChanged(IIZ)V
    .registers 9
    .parameter "pid"
    .parameter "uid"
    .parameter "foregroundActivities"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 100
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 102
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.app.IProcessObserver"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 103
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 104
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 105
    if-eqz p3, :cond_21

    #@12
    :goto_12
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 106
    iget-object v1, p0, Landroid/app/IProcessObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/4 v2, 0x1

    #@18
    const/4 v3, 0x0

    #@19
    const/4 v4, 0x1

    #@1a
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_23

    #@1d
    .line 109
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 111
    return-void

    #@21
    .line 105
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_12

    #@23
    .line 109
    :catchall_23
    move-exception v1

    #@24
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    throw v1
.end method

.method public onImportanceChanged(III)V
    .registers 9
    .parameter "pid"
    .parameter "uid"
    .parameter "importance"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 114
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 116
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.IProcessObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 117
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 118
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 119
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 120
    iget-object v1, p0, Landroid/app/IProcessObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@14
    const/4 v2, 0x2

    #@15
    const/4 v3, 0x0

    #@16
    const/4 v4, 0x1

    #@17
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1a
    .catchall {:try_start_4 .. :try_end_1a} :catchall_1e

    #@1a
    .line 123
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 125
    return-void

    #@1e
    .line 123
    :catchall_1e
    move-exception v1

    #@1f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    throw v1
.end method

.method public onProcessDied(II)V
    .registers 8
    .parameter "pid"
    .parameter "uid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 128
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 130
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.IProcessObserver"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 131
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 132
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 133
    iget-object v1, p0, Landroid/app/IProcessObserver$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x3

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_1b

    #@17
    .line 136
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 138
    return-void

    #@1b
    .line 136
    :catchall_1b
    move-exception v1

    #@1c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    throw v1
.end method
