.class Landroid/app/ITransientNotification$Stub$Proxy;
.super Ljava/lang/Object;
.source "ITransientNotification.java"

# interfaces
.implements Landroid/app/ITransientNotification;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ITransientNotification$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    iput-object p1, p0, Landroid/app/ITransientNotification$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 67
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/app/ITransientNotification$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 74
    const-string v0, "android.app.ITransientNotification"

    #@2
    return-object v0
.end method

.method public hide()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 91
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.ITransientNotification"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 92
    iget-object v1, p0, Landroid/app/ITransientNotification$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x2

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 95
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 97
    return-void

    #@15
    .line 95
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public show()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 78
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 80
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "android.app.ITransientNotification"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 81
    iget-object v1, p0, Landroid/app/ITransientNotification$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x1

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 84
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 86
    return-void

    #@15
    .line 84
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method
