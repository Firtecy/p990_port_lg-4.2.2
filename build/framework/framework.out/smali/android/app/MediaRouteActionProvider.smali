.class public Landroid/app/MediaRouteActionProvider;
.super Landroid/view/ActionProvider;
.source "MediaRouteActionProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/MediaRouteActionProvider$RouterCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaRouteActionProvider"


# instance fields
.field private mCallback:Landroid/app/MediaRouteActionProvider$RouterCallback;

.field private mContext:Landroid/content/Context;

.field private mExtendedSettingsListener:Landroid/view/View$OnClickListener;

.field private mMenuItem:Landroid/view/MenuItem;

.field private mRouteTypes:I

.field private mRouter:Landroid/media/MediaRouter;

.field private mView:Landroid/app/MediaRouteButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/view/ActionProvider;-><init>(Landroid/content/Context;)V

    #@3
    .line 46
    iput-object p1, p0, Landroid/app/MediaRouteActionProvider;->mContext:Landroid/content/Context;

    #@5
    .line 47
    const-string/jumbo v0, "media_router"

    #@8
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/media/MediaRouter;

    #@e
    iput-object v0, p0, Landroid/app/MediaRouteActionProvider;->mRouter:Landroid/media/MediaRouter;

    #@10
    .line 48
    new-instance v0, Landroid/app/MediaRouteActionProvider$RouterCallback;

    #@12
    invoke-direct {v0, p0}, Landroid/app/MediaRouteActionProvider$RouterCallback;-><init>(Landroid/app/MediaRouteActionProvider;)V

    #@15
    iput-object v0, p0, Landroid/app/MediaRouteActionProvider;->mCallback:Landroid/app/MediaRouteActionProvider$RouterCallback;

    #@17
    .line 53
    const/4 v0, 0x1

    #@18
    invoke-virtual {p0, v0}, Landroid/app/MediaRouteActionProvider;->setRouteTypes(I)V

    #@1b
    .line 54
    return-void
.end method

.method private getActivity()Landroid/app/Activity;
    .registers 4

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mContext:Landroid/content/Context;

    #@2
    .line 114
    .local v0, context:Landroid/content/Context;
    :goto_2
    instance-of v1, v0, Landroid/content/ContextWrapper;

    #@4
    if-eqz v1, :cond_11

    #@6
    instance-of v1, v0, Landroid/app/Activity;

    #@8
    if-nez v1, :cond_11

    #@a
    .line 115
    check-cast v0, Landroid/content/ContextWrapper;

    #@c
    .end local v0           #context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    #@f
    move-result-object v0

    #@10
    .restart local v0       #context:Landroid/content/Context;
    goto :goto_2

    #@11
    .line 117
    :cond_11
    instance-of v1, v0, Landroid/app/Activity;

    #@13
    if-nez v1, :cond_1d

    #@15
    .line 118
    new-instance v1, Ljava/lang/IllegalStateException;

    #@17
    const-string v2, "The MediaRouteActionProvider\'s Context is not an Activity."

    #@19
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 122
    :cond_1d
    check-cast v0, Landroid/app/Activity;

    #@1f
    .end local v0           #context:Landroid/content/Context;
    return-object v0
.end method


# virtual methods
.method public isVisible()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 139
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider;->mRouter:Landroid/media/MediaRouter;

    #@3
    invoke-virtual {v1}, Landroid/media/MediaRouter;->getRouteCount()I

    #@6
    move-result v1

    #@7
    if-le v1, v0, :cond_a

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public onCreateActionView()Landroid/view/View;
    .registers 3

    #@0
    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Use onCreateActionView(MenuItem) instead."

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;
    .registers 6
    .parameter "item"

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mMenuItem:Landroid/view/MenuItem;

    #@2
    if-nez v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 78
    :cond_8
    const-string v0, "MediaRouteActionProvider"

    #@a
    const-string/jumbo v1, "onCreateActionView: this ActionProvider is already associated with a menu item. Don\'t reuse MediaRouteActionProvider instances! Abandoning the old one..."

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 82
    :cond_10
    iput-object p1, p0, Landroid/app/MediaRouteActionProvider;->mMenuItem:Landroid/view/MenuItem;

    #@12
    .line 83
    new-instance v0, Landroid/app/MediaRouteButton;

    #@14
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider;->mContext:Landroid/content/Context;

    #@16
    invoke-direct {v0, v1}, Landroid/app/MediaRouteButton;-><init>(Landroid/content/Context;)V

    #@19
    iput-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@1b
    .line 84
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@1d
    const/4 v1, 0x1

    #@1e
    invoke-virtual {v0, v1}, Landroid/app/MediaRouteButton;->setCheatSheetEnabled(Z)V

    #@21
    .line 85
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@23
    iget v1, p0, Landroid/app/MediaRouteActionProvider;->mRouteTypes:I

    #@25
    invoke-virtual {v0, v1}, Landroid/app/MediaRouteButton;->setRouteTypes(I)V

    #@28
    .line 86
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@2a
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    #@2c
    invoke-virtual {v0, v1}, Landroid/app/MediaRouteButton;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    #@2f
    .line 87
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@31
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@33
    const/4 v2, -0x2

    #@34
    const/4 v3, -0x1

    #@35
    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@38
    invoke-virtual {v0, v1}, Landroid/app/MediaRouteButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@3b
    .line 89
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@3d
    return-object v0
.end method

.method public onPerformDefaultAction()Z
    .registers 5

    #@0
    .prologue
    .line 94
    invoke-direct {p0}, Landroid/app/MediaRouteActionProvider;->getActivity()Landroid/app/Activity;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@7
    move-result-object v1

    #@8
    .line 96
    .local v1, fm:Landroid/app/FragmentManager;
    const-string v2, "android:MediaRouteChooserDialogFragment"

    #@a
    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@10
    .line 99
    .local v0, dialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;
    if-eqz v0, :cond_1c

    #@12
    .line 100
    const-string v2, "MediaRouteActionProvider"

    #@14
    const-string/jumbo v3, "onPerformDefaultAction(): Chooser dialog already showing!"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 101
    const/4 v2, 0x0

    #@1b
    .line 108
    :goto_1b
    return v2

    #@1c
    .line 104
    :cond_1c
    new-instance v0, Lcom/android/internal/app/MediaRouteChooserDialogFragment;

    #@1e
    .end local v0           #dialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;
    invoke-direct {v0}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;-><init>()V

    #@21
    .line 105
    .restart local v0       #dialogFragment:Lcom/android/internal/app/MediaRouteChooserDialogFragment;
    iget-object v2, p0, Landroid/app/MediaRouteActionProvider;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    #@23
    invoke-virtual {v0, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    #@26
    .line 106
    iget v2, p0, Landroid/app/MediaRouteActionProvider;->mRouteTypes:I

    #@28
    invoke-virtual {v0, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->setRouteTypes(I)V

    #@2b
    .line 107
    const-string v2, "android:MediaRouteChooserDialogFragment"

    #@2d
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/app/MediaRouteChooserDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    #@30
    .line 108
    const/4 v2, 0x1

    #@31
    goto :goto_1b
.end method

.method public overridesItemVisibility()Z
    .registers 2

    #@0
    .prologue
    .line 134
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 126
    iput-object p1, p0, Landroid/app/MediaRouteActionProvider;->mExtendedSettingsListener:Landroid/view/View$OnClickListener;

    #@2
    .line 127
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 128
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@8
    invoke-virtual {v0, p1}, Landroid/app/MediaRouteButton;->setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V

    #@b
    .line 130
    :cond_b
    return-void
.end method

.method public setRouteTypes(I)V
    .registers 4
    .parameter "types"

    #@0
    .prologue
    .line 57
    iget v0, p0, Landroid/app/MediaRouteActionProvider;->mRouteTypes:I

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 68
    :cond_4
    :goto_4
    return-void

    #@5
    .line 58
    :cond_5
    iget v0, p0, Landroid/app/MediaRouteActionProvider;->mRouteTypes:I

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 59
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mRouter:Landroid/media/MediaRouter;

    #@b
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider;->mCallback:Landroid/app/MediaRouteActionProvider$RouterCallback;

    #@d
    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    #@10
    .line 61
    :cond_10
    iput p1, p0, Landroid/app/MediaRouteActionProvider;->mRouteTypes:I

    #@12
    .line 62
    if-eqz p1, :cond_1b

    #@14
    .line 63
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mRouter:Landroid/media/MediaRouter;

    #@16
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider;->mCallback:Landroid/app/MediaRouteActionProvider$RouterCallback;

    #@18
    invoke-virtual {v0, p1, v1}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    #@1b
    .line 65
    :cond_1b
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@1d
    if-eqz v0, :cond_4

    #@1f
    .line 66
    iget-object v0, p0, Landroid/app/MediaRouteActionProvider;->mView:Landroid/app/MediaRouteButton;

    #@21
    iget v1, p0, Landroid/app/MediaRouteActionProvider;->mRouteTypes:I

    #@23
    invoke-virtual {v0, v1}, Landroid/app/MediaRouteButton;->setRouteTypes(I)V

    #@26
    goto :goto_4
.end method
