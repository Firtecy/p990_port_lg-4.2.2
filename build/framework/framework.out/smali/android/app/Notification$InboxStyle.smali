.class public Landroid/app/Notification$InboxStyle;
.super Landroid/app/Notification$Style;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InboxStyle"
.end annotation


# instance fields
.field private mTexts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 1944
    invoke-direct {p0}, Landroid/app/Notification$Style;-><init>()V

    #@3
    .line 1942
    new-instance v0, Ljava/util/ArrayList;

    #@5
    const/4 v1, 0x5

    #@6
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    iput-object v0, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@b
    .line 1945
    return-void
.end method

.method public constructor <init>(Landroid/app/Notification$Builder;)V
    .registers 4
    .parameter "builder"

    #@0
    .prologue
    .line 1947
    invoke-direct {p0}, Landroid/app/Notification$Style;-><init>()V

    #@3
    .line 1942
    new-instance v0, Ljava/util/ArrayList;

    #@5
    const/4 v1, 0x5

    #@6
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    iput-object v0, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@b
    .line 1948
    invoke-virtual {p0, p1}, Landroid/app/Notification$InboxStyle;->setBuilder(Landroid/app/Notification$Builder;)V

    #@e
    .line 1949
    return-void
.end method

.method private makeBigContentView()Landroid/widget/RemoteViews;
    .registers 14

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/16 v10, 0x8

    #@3
    .line 1978
    iget-object v8, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@5
    const/4 v11, 0x0

    #@6
    invoke-static {v8, v11}, Landroid/app/Notification$Builder;->access$702(Landroid/app/Notification$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@9
    .line 1979
    const v8, 0x1090096

    #@c
    invoke-virtual {p0, v8}, Landroid/app/Notification$InboxStyle;->getStandardView(I)Landroid/widget/RemoteViews;

    #@f
    move-result-object v1

    #@10
    .line 1981
    .local v1, contentView:Landroid/widget/RemoteViews;
    const v8, 0x1020015

    #@13
    invoke-virtual {v1, v8, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@16
    .line 1983
    const/4 v8, 0x7

    #@17
    new-array v6, v8, [I

    #@19
    fill-array-data v6, :array_78

    #@1c
    .line 1987
    .local v6, rowIds:[I
    move-object v0, v6

    #@1d
    .local v0, arr$:[I
    array-length v4, v0

    #@1e
    .local v4, len$:I
    const/4 v3, 0x0

    #@1f
    .local v3, i$:I
    :goto_1f
    if-ge v3, v4, :cond_29

    #@21
    aget v5, v0, v3

    #@23
    .line 1988
    .local v5, rowId:I
    invoke-virtual {v1, v5, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@26
    .line 1987
    add-int/lit8 v3, v3, 0x1

    #@28
    goto :goto_1f

    #@29
    .line 1992
    .end local v5           #rowId:I
    :cond_29
    const/4 v2, 0x0

    #@2a
    .line 1993
    .local v2, i:I
    :goto_2a
    iget-object v8, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@2f
    move-result v8

    #@30
    if-ge v2, v8, :cond_54

    #@32
    array-length v8, v6

    #@33
    if-ge v2, v8, :cond_54

    #@35
    .line 1994
    iget-object v8, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v7

    #@3b
    check-cast v7, Ljava/lang/CharSequence;

    #@3d
    .line 1995
    .local v7, str:Ljava/lang/CharSequence;
    if-eqz v7, :cond_51

    #@3f
    const-string v8, ""

    #@41
    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v8

    #@45
    if-nez v8, :cond_51

    #@47
    .line 1996
    aget v8, v6, v2

    #@49
    invoke-virtual {v1, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@4c
    .line 1997
    aget v8, v6, v2

    #@4e
    invoke-virtual {v1, v8, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@51
    .line 1999
    :cond_51
    add-int/lit8 v2, v2, 0x1

    #@53
    .line 2000
    goto :goto_2a

    #@54
    .line 2002
    .end local v7           #str:Ljava/lang/CharSequence;
    :cond_54
    const v11, 0x1020347

    #@57
    iget-object v8, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@5c
    move-result v8

    #@5d
    if-lez v8, :cond_73

    #@5f
    move v8, v9

    #@60
    :goto_60
    invoke-virtual {v1, v11, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@63
    .line 2005
    const v8, 0x1020346

    #@66
    iget-object v11, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@68
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@6b
    move-result v11

    #@6c
    array-length v12, v6

    #@6d
    if-le v11, v12, :cond_75

    #@6f
    :goto_6f
    invoke-virtual {v1, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@72
    .line 2008
    return-object v1

    #@73
    :cond_73
    move v8, v10

    #@74
    .line 2002
    goto :goto_60

    #@75
    :cond_75
    move v9, v10

    #@76
    .line 2005
    goto :goto_6f

    #@77
    .line 1983
    nop

    #@78
    :array_78
    .array-data 0x4
        0x3ft 0x3t 0x2t 0x1t
        0x40t 0x3t 0x2t 0x1t
        0x41t 0x3t 0x2t 0x1t
        0x42t 0x3t 0x2t 0x1t
        0x43t 0x3t 0x2t 0x1t
        0x44t 0x3t 0x2t 0x1t
        0x45t 0x3t 0x2t 0x1t
    .end array-data
.end method


# virtual methods
.method public addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
    .registers 3
    .parameter "cs"

    #@0
    .prologue
    .line 1972
    iget-object v0, p0, Landroid/app/Notification$InboxStyle;->mTexts:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 1973
    return-object p0
.end method

.method public build()Landroid/app/Notification;
    .registers 3

    #@0
    .prologue
    .line 2013
    invoke-virtual {p0}, Landroid/app/Notification$InboxStyle;->checkBuilder()V

    #@3
    .line 2014
    iget-object v1, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@5
    invoke-static {v1}, Landroid/app/Notification$Builder;->access$500(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    #@8
    move-result-object v0

    #@9
    .line 2015
    .local v0, wip:Landroid/app/Notification;
    invoke-direct {p0}, Landroid/app/Notification$InboxStyle;->makeBigContentView()Landroid/widget/RemoteViews;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@f
    .line 2016
    return-object v0
.end method

.method public setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 1956
    invoke-virtual {p0, p1}, Landroid/app/Notification$InboxStyle;->internalSetBigContentTitle(Ljava/lang/CharSequence;)V

    #@3
    .line 1957
    return-object p0
.end method

.method public setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
    .registers 2
    .parameter "cs"

    #@0
    .prologue
    .line 1964
    invoke-virtual {p0, p1}, Landroid/app/Notification$InboxStyle;->internalSetSummaryText(Ljava/lang/CharSequence;)V

    #@3
    .line 1965
    return-object p0
.end method
