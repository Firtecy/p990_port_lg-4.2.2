.class public Landroid/app/Application;
.super Landroid/content/ContextWrapper;
.source "Application.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Application$ActivityLifecycleCallbacks;
    }
.end annotation


# instance fields
.field private mActivityLifecycleCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Application$ActivityLifecycleCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mAlpha:I

.field private mAppNaviBgInfoLoaded:Z

.field private mComponentCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ComponentCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mDisabled:Z

.field private mLandResId:I

.field public mLoadedApk:Landroid/app/LoadedApk;

.field private mPortResId:I

.field private mReserved:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 80
    const/4 v0, 0x0

    #@2
    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@5
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@c
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@13
    .line 61
    iput v1, p0, Landroid/app/Application;->mPortResId:I

    #@15
    .line 62
    iput v1, p0, Landroid/app/Application;->mLandResId:I

    #@17
    .line 63
    iput v1, p0, Landroid/app/Application;->mAlpha:I

    #@19
    .line 64
    iput v1, p0, Landroid/app/Application;->mReserved:I

    #@1b
    .line 65
    iput-boolean v1, p0, Landroid/app/Application;->mDisabled:Z

    #@1d
    .line 66
    iput-boolean v1, p0, Landroid/app/Application;->mAppNaviBgInfoLoaded:Z

    #@1f
    .line 81
    return-void
.end method

.method private checkNaviStyle(Landroid/app/Activity;)V
    .registers 15
    .parameter "activity"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 260
    const/4 v2, 0x0

    #@2
    .line 261
    .local v2, portResId:I
    const/4 v3, 0x0

    #@3
    .line 262
    .local v3, landResId:I
    const/4 v4, 0x0

    #@4
    .line 263
    .local v4, alpha:I
    const/4 v5, 0x0

    #@5
    .line 264
    .local v5, reserved:I
    const/4 v8, 0x0

    #@6
    .line 266
    .local v8, disabled:Z
    invoke-virtual {p0}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@d
    move-result-object v11

    #@e
    .line 267
    .local v11, pm:Landroid/content/pm/PackageManager;
    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@11
    move-result-object v7

    #@12
    .line 268
    .local v7, cn:Landroid/content/ComponentName;
    const/4 v6, 0x0

    #@13
    .line 271
    .local v6, activityInfo:Landroid/content/pm/ActivityInfo;
    const/16 v0, 0x81

    #@15
    :try_start_15
    invoke-virtual {v11, v7, v0}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_18
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_15 .. :try_end_18} :catch_115

    #@18
    move-result-object v6

    #@19
    .line 275
    :goto_19
    if-eqz v6, :cond_b8

    #@1b
    .line 276
    iget-object v10, v6, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@1d
    .line 277
    .local v10, metaData:Landroid/os/Bundle;
    if-eqz v10, :cond_4c

    #@1f
    .line 278
    const-string/jumbo v0, "navi_bar_bg_disabled"

    #@22
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@25
    move-result v8

    #@26
    .line 279
    if-eqz v8, :cond_30

    #@28
    .line 280
    const-string v0, "NavigationBar"

    #@2a
    const-string v1, "Navi BG API disabled for this Activitiy"

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 330
    .end local v10           #metaData:Landroid/os/Bundle;
    :goto_2f
    return-void

    #@30
    .line 284
    .restart local v10       #metaData:Landroid/os/Bundle;
    :cond_30
    const-string/jumbo v0, "navi_bar_bg_port_res_id"

    #@33
    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@36
    move-result v2

    #@37
    .line 285
    const-string/jumbo v0, "navi_bar_bg_land_res_id"

    #@3a
    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@3d
    move-result v3

    #@3e
    .line 286
    const-string/jumbo v0, "navi_bar_bg_alpha"

    #@41
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@44
    move-result v4

    #@45
    .line 287
    const-string/jumbo v0, "navi_bar_bg_reserved"

    #@48
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@4b
    move-result v5

    #@4c
    .line 291
    :cond_4c
    if-eqz v10, :cond_52

    #@4e
    if-ne v2, v1, :cond_b8

    #@50
    if-ne v3, v1, :cond_b8

    #@52
    .line 292
    :cond_52
    iget-boolean v0, p0, Landroid/app/Application;->mAppNaviBgInfoLoaded:Z

    #@54
    if-nez v0, :cond_9b

    #@56
    .line 293
    const/4 v10, 0x0

    #@57
    .line 295
    :try_start_57
    invoke-virtual {p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    const/16 v12, 0x80

    #@61
    invoke-virtual {v0, v1, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@64
    move-result-object v0

    #@65
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@67
    iget-object v10, v0, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@69
    .line 296
    if-eqz v10, :cond_98

    #@6b
    .line 297
    const-string/jumbo v0, "navi_bar_bg_disabled"

    #@6e
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@71
    move-result v0

    #@72
    iput-boolean v0, p0, Landroid/app/Application;->mDisabled:Z

    #@74
    .line 298
    const-string/jumbo v0, "navi_bar_bg_port_res_id"

    #@77
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@7a
    move-result v0

    #@7b
    iput v0, p0, Landroid/app/Application;->mPortResId:I

    #@7d
    .line 299
    const-string/jumbo v0, "navi_bar_bg_land_res_id"

    #@80
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@83
    move-result v0

    #@84
    iput v0, p0, Landroid/app/Application;->mLandResId:I

    #@86
    .line 300
    const-string/jumbo v0, "navi_bar_bg_alpha"

    #@89
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@8c
    move-result v0

    #@8d
    iput v0, p0, Landroid/app/Application;->mAlpha:I

    #@8f
    .line 301
    const-string/jumbo v0, "navi_bar_bg_reserved"

    #@92
    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@95
    move-result v0

    #@96
    iput v0, p0, Landroid/app/Application;->mReserved:I

    #@98
    .line 304
    :cond_98
    const/4 v0, 0x1

    #@99
    iput-boolean v0, p0, Landroid/app/Application;->mAppNaviBgInfoLoaded:Z
    :try_end_9b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_57 .. :try_end_9b} :catch_a7

    #@9b
    .line 310
    :cond_9b
    :goto_9b
    iget-boolean v0, p0, Landroid/app/Application;->mDisabled:Z

    #@9d
    if-eqz v0, :cond_b0

    #@9f
    .line 311
    const-string v0, "NavigationBar"

    #@a1
    const-string v1, "Navi BG API disabled for this Application"

    #@a3
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    goto :goto_2f

    #@a7
    .line 305
    :catch_a7
    move-exception v9

    #@a8
    .line 306
    .local v9, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "NavigationBar"

    #@aa
    const-string v1, "Failed to get ApplicatiomInfo meta data : NameNotFoundException"

    #@ac
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    goto :goto_9b

    #@b0
    .line 315
    .end local v9           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_b0
    iget v2, p0, Landroid/app/Application;->mPortResId:I

    #@b2
    .line 316
    iget v3, p0, Landroid/app/Application;->mLandResId:I

    #@b4
    .line 317
    iget v4, p0, Landroid/app/Application;->mAlpha:I

    #@b6
    .line 318
    iget v5, p0, Landroid/app/Application;->mReserved:I

    #@b8
    .line 324
    .end local v10           #metaData:Landroid/os/Bundle;
    :cond_b8
    :try_start_b8
    const-string/jumbo v0, "statusbar"

    #@bb
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@be
    move-result-object v0

    #@bf
    check-cast v0, Landroid/app/StatusBarManager;

    #@c1
    invoke-virtual {p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    #@c4
    move-result-object v1

    #@c5
    invoke-virtual/range {v0 .. v5}, Landroid/app/StatusBarManager;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@c8
    .line 325
    const-string v0, "NavigationBar"

    #@ca
    new-instance v1, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string/jumbo v12, "meta-data ==> set Navi Bg (package:"

    #@d2
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    #@d9
    move-result-object v12

    #@da
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v1

    #@de
    const-string v12, ", portResource ID:"

    #@e0
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v1

    #@e4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v1

    #@e8
    const-string v12, ", landResource ID:"

    #@ea
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v1

    #@f2
    const-string v12, ", alpha:"

    #@f4
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v1

    #@f8
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v1

    #@fc
    const-string v12, ")"

    #@fe
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v1

    #@102
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v1

    #@106
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_109
    .catch Ljava/lang/Exception; {:try_start_b8 .. :try_end_109} :catch_10b

    #@109
    goto/16 :goto_2f

    #@10b
    .line 326
    :catch_10b
    move-exception v9

    #@10c
    .line 327
    .local v9, e:Ljava/lang/Exception;
    const-string v0, "NavigationBar"

    #@10e
    const-string v1, "Not found SystemUI API. (setNavigationBackground())"

    #@110
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@113
    goto/16 :goto_2f

    #@115
    .line 272
    .end local v9           #e:Ljava/lang/Exception;
    :catch_115
    move-exception v0

    #@116
    goto/16 :goto_19
.end method

.method private collectActivityLifecycleCallbacks()[Ljava/lang/Object;
    .registers 4

    #@0
    .prologue
    .line 248
    const/4 v0, 0x0

    #@1
    .line 249
    .local v0, callbacks:[Ljava/lang/Object;
    iget-object v2, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@3
    monitor-enter v2

    #@4
    .line 250
    :try_start_4
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v1

    #@a
    if-lez v1, :cond_12

    #@c
    .line 251
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    .line 253
    :cond_12
    monitor-exit v2

    #@13
    .line 254
    return-object v0

    #@14
    .line 253
    :catchall_14
    move-exception v1

    #@15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_4 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method private collectComponentCallbacks()[Ljava/lang/Object;
    .registers 4

    #@0
    .prologue
    .line 238
    const/4 v0, 0x0

    #@1
    .line 239
    .local v0, callbacks:[Ljava/lang/Object;
    iget-object v2, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@3
    monitor-enter v2

    #@4
    .line 240
    :try_start_4
    iget-object v1, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v1

    #@a
    if-lez v1, :cond_12

    #@c
    .line 241
    iget-object v1, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    .line 243
    :cond_12
    monitor-exit v2

    #@13
    .line 244
    return-object v0

    #@14
    .line 243
    :catchall_14
    move-exception v1

    #@15
    monitor-exit v2
    :try_end_16
    .catchall {:try_start_4 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method


# virtual methods
.method final attach(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 164
    invoke-virtual {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    #@3
    .line 165
    invoke-static {p1}, Landroid/app/ContextImpl;->getImpl(Landroid/content/Context;)Landroid/app/ContextImpl;

    #@6
    move-result-object v0

    #@7
    iget-object v0, v0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@9
    iput-object v0, p0, Landroid/app/Application;->mLoadedApk:Landroid/app/LoadedApk;

    #@b
    .line 166
    return-void
.end method

.method dispatchActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 6
    .parameter "activity"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 169
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 170
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 171
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 172
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@e
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@11
    .line 171
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 176
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method dispatchActivityDestroyed(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    #@0
    .prologue
    .line 229
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 230
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 231
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 232
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@e
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityDestroyed(Landroid/app/Activity;)V

    #@11
    .line 231
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 235
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method dispatchActivityPaused(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    #@0
    .prologue
    .line 201
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 202
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 203
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 204
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@e
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPaused(Landroid/app/Activity;)V

    #@11
    .line 203
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 207
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method dispatchActivityResumed(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    #@0
    .prologue
    .line 189
    invoke-direct {p0, p1}, Landroid/app/Application;->checkNaviStyle(Landroid/app/Activity;)V

    #@3
    .line 192
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    .line 193
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_17

    #@9
    .line 194
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    array-length v2, v0

    #@b
    if-ge v1, v2, :cond_17

    #@d
    .line 195
    aget-object v2, v0, v1

    #@f
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@11
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityResumed(Landroid/app/Activity;)V

    #@14
    .line 194
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_a

    #@17
    .line 198
    .end local v1           #i:I
    :cond_17
    return-void
.end method

.method dispatchActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 6
    .parameter "activity"
    .parameter "outState"

    #@0
    .prologue
    .line 219
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 220
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 221
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 222
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@e
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@11
    .line 221
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 226
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method dispatchActivityStarted(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    #@0
    .prologue
    .line 179
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 180
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 181
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 182
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@e
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStarted(Landroid/app/Activity;)V

    #@11
    .line 181
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 185
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method dispatchActivityStopped(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    #@0
    .prologue
    .line 210
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 211
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 212
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 213
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    #@e
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStopped(Landroid/app/Activity;)V

    #@11
    .line 212
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 216
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "newConfig"

    #@0
    .prologue
    .line 105
    invoke-direct {p0}, Landroid/app/Application;->collectComponentCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 106
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 107
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 108
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/content/ComponentCallbacks;

    #@e
    invoke-interface {v2, p1}, Landroid/content/ComponentCallbacks;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@11
    .line 107
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 111
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method public onCreate()V
    .registers 1

    #@0
    .prologue
    .line 93
    return-void
.end method

.method public onLowMemory()V
    .registers 4

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/app/Application;->collectComponentCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 115
    .local v0, callbacks:[Ljava/lang/Object;
    if-eqz v0, :cond_14

    #@6
    .line 116
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    array-length v2, v0

    #@8
    if-ge v1, v2, :cond_14

    #@a
    .line 117
    aget-object v2, v0, v1

    #@c
    check-cast v2, Landroid/content/ComponentCallbacks;

    #@e
    invoke-interface {v2}, Landroid/content/ComponentCallbacks;->onLowMemory()V

    #@11
    .line 116
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 120
    .end local v1           #i:I
    :cond_14
    return-void
.end method

.method public onTerminate()V
    .registers 1

    #@0
    .prologue
    .line 102
    return-void
.end method

.method public onTrimMemory(I)V
    .registers 6
    .parameter "level"

    #@0
    .prologue
    .line 123
    invoke-direct {p0}, Landroid/app/Application;->collectComponentCallbacks()[Ljava/lang/Object;

    #@3
    move-result-object v1

    #@4
    .line 124
    .local v1, callbacks:[Ljava/lang/Object;
    if-eqz v1, :cond_18

    #@6
    .line 125
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    array-length v3, v1

    #@8
    if-ge v2, v3, :cond_18

    #@a
    .line 126
    aget-object v0, v1, v2

    #@c
    .line 127
    .local v0, c:Ljava/lang/Object;
    instance-of v3, v0, Landroid/content/ComponentCallbacks2;

    #@e
    if-eqz v3, :cond_15

    #@10
    .line 128
    check-cast v0, Landroid/content/ComponentCallbacks2;

    #@12
    .end local v0           #c:Ljava/lang/Object;
    invoke-interface {v0, p1}, Landroid/content/ComponentCallbacks2;->onTrimMemory(I)V

    #@15
    .line 125
    :cond_15
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_7

    #@18
    .line 132
    .end local v2           #i:I
    :cond_18
    return-void
.end method

.method public registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 147
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 148
    :try_start_3
    iget-object v0, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8
    .line 149
    monitor-exit v1

    #@9
    .line 150
    return-void

    #@a
    .line 149
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 135
    iget-object v1, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 136
    :try_start_3
    iget-object v0, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8
    .line 137
    monitor-exit v1

    #@9
    .line 138
    return-void

    #@a
    .line 137
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 153
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 154
    :try_start_3
    iget-object v0, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 155
    monitor-exit v1

    #@9
    .line 156
    return-void

    #@a
    .line 155
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 141
    iget-object v1, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 142
    :try_start_3
    iget-object v0, p0, Landroid/app/Application;->mComponentCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 143
    monitor-exit v1

    #@9
    .line 144
    return-void

    #@a
    .line 143
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method
