.class public Landroid/app/TaskStackBuilder;
.super Ljava/lang/Object;
.source "TaskStackBuilder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TaskStackBuilder"


# instance fields
.field private final mIntents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final mSourceContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "a"

    #@0
    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@a
    .line 67
    iput-object p1, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@c
    .line 68
    return-void
.end method

.method public static create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 78
    new-instance v0, Landroid/app/TaskStackBuilder;

    #@2
    invoke-direct {v0, p0}, Landroid/app/TaskStackBuilder;-><init>(Landroid/content/Context;)V

    #@5
    return-object v0
.end method


# virtual methods
.method public addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;
    .registers 3
    .parameter "nextIntent"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 90
    return-object p0
.end method

.method public addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;
    .registers 4
    .parameter "nextIntent"

    #@0
    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@3
    move-result-object v0

    #@4
    .line 107
    .local v0, target:Landroid/content/ComponentName;
    if-nez v0, :cond_10

    #@6
    .line 108
    iget-object v1, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p1, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    #@f
    move-result-object v0

    #@10
    .line 110
    :cond_10
    if-eqz v0, :cond_15

    #@12
    .line 111
    invoke-virtual {p0, v0}, Landroid/app/TaskStackBuilder;->addParentStack(Landroid/content/ComponentName;)Landroid/app/TaskStackBuilder;

    #@15
    .line 113
    :cond_15
    invoke-virtual {p0, p1}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    #@18
    .line 114
    return-object p0
.end method

.method public addParentStack(Landroid/app/Activity;)Landroid/app/TaskStackBuilder;
    .registers 5
    .parameter "sourceActivity"

    #@0
    .prologue
    .line 128
    invoke-virtual {p1}, Landroid/app/Activity;->getParentActivityIntent()Landroid/content/Intent;

    #@3
    move-result-object v0

    #@4
    .line 129
    .local v0, parent:Landroid/content/Intent;
    if-eqz v0, :cond_1c

    #@6
    .line 132
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@9
    move-result-object v1

    #@a
    .line 133
    .local v1, target:Landroid/content/ComponentName;
    if-nez v1, :cond_16

    #@c
    .line 134
    iget-object v2, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@e
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    #@15
    move-result-object v1

    #@16
    .line 136
    :cond_16
    invoke-virtual {p0, v1}, Landroid/app/TaskStackBuilder;->addParentStack(Landroid/content/ComponentName;)Landroid/app/TaskStackBuilder;

    #@19
    .line 137
    invoke-virtual {p0, v0}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    #@1c
    .line 139
    .end local v1           #target:Landroid/content/ComponentName;
    :cond_1c
    return-object p0
.end method

.method public addParentStack(Landroid/content/ComponentName;)Landroid/app/TaskStackBuilder;
    .registers 11
    .parameter "sourceActivityName"

    #@0
    .prologue
    .line 164
    iget-object v7, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 165
    .local v2, insertAt:I
    iget-object v7, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@8
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b
    move-result-object v5

    #@c
    .line 167
    .local v5, pm:Landroid/content/pm/PackageManager;
    const/4 v7, 0x0

    #@d
    :try_start_d
    invoke-virtual {v5, p1, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@10
    move-result-object v1

    #@11
    .line 168
    .local v1, info:Landroid/content/pm/ActivityInfo;
    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@13
    .line 169
    .local v4, parentActivity:Ljava/lang/String;
    :goto_13
    if-eqz v4, :cond_49

    #@15
    .line 170
    new-instance v6, Landroid/content/ComponentName;

    #@17
    iget-object v7, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@19
    invoke-direct {v6, v7, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 171
    .local v6, target:Landroid/content/ComponentName;
    const/4 v7, 0x0

    #@1d
    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@20
    move-result-object v1

    #@21
    .line 172
    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->parentActivityName:Ljava/lang/String;

    #@23
    .line 173
    if-nez v4, :cond_3f

    #@25
    if-nez v2, :cond_3f

    #@27
    invoke-static {v6}, Landroid/content/Intent;->makeMainActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@2a
    move-result-object v3

    #@2b
    .line 176
    .local v3, parent:Landroid/content/Intent;
    :goto_2b
    iget-object v7, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v7, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_30
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_d .. :try_end_30} :catch_31

    #@30
    goto :goto_13

    #@31
    .line 178
    .end local v1           #info:Landroid/content/pm/ActivityInfo;
    .end local v3           #parent:Landroid/content/Intent;
    .end local v4           #parentActivity:Ljava/lang/String;
    .end local v6           #target:Landroid/content/ComponentName;
    :catch_31
    move-exception v0

    #@32
    .line 179
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "TaskStackBuilder"

    #@34
    const-string v8, "Bad ComponentName while traversing activity parent metadata"

    #@36
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 180
    new-instance v7, Ljava/lang/IllegalArgumentException;

    #@3b
    invoke-direct {v7, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    #@3e
    throw v7

    #@3f
    .line 173
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1       #info:Landroid/content/pm/ActivityInfo;
    .restart local v4       #parentActivity:Ljava/lang/String;
    .restart local v6       #target:Landroid/content/ComponentName;
    :cond_3f
    :try_start_3f
    new-instance v7, Landroid/content/Intent;

    #@41
    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    #@44
    invoke-virtual {v7, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_47
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3f .. :try_end_47} :catch_31

    #@47
    move-result-object v3

    #@48
    goto :goto_2b

    #@49
    .line 182
    .end local v6           #target:Landroid/content/ComponentName;
    :cond_49
    return-object p0
.end method

.method public addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/app/TaskStackBuilder;"
        }
    .end annotation

    #@0
    .prologue
    .line 151
    .local p1, sourceActivityClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/ComponentName;

    #@2
    iget-object v1, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@7
    invoke-virtual {p0, v0}, Landroid/app/TaskStackBuilder;->addParentStack(Landroid/content/ComponentName;)Landroid/app/TaskStackBuilder;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public editIntentAt(I)Landroid/content/Intent;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/Intent;

    #@8
    return-object v0
.end method

.method public getIntentCount()I
    .registers 2

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIntents()[Landroid/content/Intent;
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 298
    iget-object v2, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    new-array v1, v2, [Landroid/content/Intent;

    #@9
    .line 299
    .local v1, intents:[Landroid/content/Intent;
    array-length v2, v1

    #@a
    if-nez v2, :cond_d

    #@c
    .line 307
    :cond_c
    return-object v1

    #@d
    .line 301
    :cond_d
    new-instance v3, Landroid/content/Intent;

    #@f
    iget-object v2, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/content/Intent;

    #@17
    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@1a
    const v2, 0x1000c000

    #@1d
    invoke-virtual {v3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@20
    move-result-object v2

    #@21
    aput-object v2, v1, v4

    #@23
    .line 304
    const/4 v0, 0x1

    #@24
    .local v0, i:I
    :goto_24
    array-length v2, v1

    #@25
    if-ge v0, v2, :cond_c

    #@27
    .line 305
    new-instance v3, Landroid/content/Intent;

    #@29
    iget-object v2, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v2

    #@2f
    check-cast v2, Landroid/content/Intent;

    #@31
    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@34
    aput-object v3, v1, v0

    #@36
    .line 304
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_24
.end method

.method public getPendingIntent(II)Landroid/app/PendingIntent;
    .registers 4
    .parameter "requestCode"
    .parameter "flags"

    #@0
    .prologue
    .line 248
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/TaskStackBuilder;->getPendingIntent(IILandroid/os/Bundle;)Landroid/app/PendingIntent;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getPendingIntent(IILandroid/os/Bundle;)Landroid/app/PendingIntent;
    .registers 6
    .parameter "requestCode"
    .parameter "flags"
    .parameter "options"

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 268
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    const-string v1, "No intents added to TaskStackBuilder; cannot getPendingIntent"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 272
    :cond_10
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@12
    invoke-virtual {p0}, Landroid/app/TaskStackBuilder;->getIntents()[Landroid/content/Intent;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, p1, v1, p2, p3}, Landroid/app/PendingIntent;->getActivities(Landroid/content/Context;I[Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/PendingIntent;

    #@19
    move-result-object v0

    #@1a
    return-object v0
.end method

.method public getPendingIntent(IILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;
    .registers 11
    .parameter "requestCode"
    .parameter "flags"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 282
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    const-string v1, "No intents added to TaskStackBuilder; cannot getPendingIntent"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 286
    :cond_10
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@12
    invoke-virtual {p0}, Landroid/app/TaskStackBuilder;->getIntents()[Landroid/content/Intent;

    #@15
    move-result-object v2

    #@16
    move v1, p1

    #@17
    move v3, p2

    #@18
    move-object v4, p3

    #@19
    move-object v5, p4

    #@1a
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivitiesAsUser(Landroid/content/Context;I[Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@1d
    move-result-object v0

    #@1e
    return-object v0
.end method

.method public startActivities()V
    .registers 2

    #@0
    .prologue
    .line 208
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/app/TaskStackBuilder;->startActivities(Landroid/os/Bundle;)V

    #@4
    .line 209
    return-void
.end method

.method public startActivities(Landroid/os/Bundle;)V
    .registers 4
    .parameter "options"

    #@0
    .prologue
    .line 232
    new-instance v0, Landroid/os/UserHandle;

    #@2
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@5
    move-result v1

    #@6
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@9
    invoke-virtual {p0, p1, v0}, Landroid/app/TaskStackBuilder;->startActivities(Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@c
    .line 233
    return-void
.end method

.method public startActivities(Landroid/os/Bundle;Landroid/os/UserHandle;)V
    .registers 5
    .parameter "options"
    .parameter "userHandle"

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mIntents:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 217
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 221
    :cond_10
    iget-object v0, p0, Landroid/app/TaskStackBuilder;->mSourceContext:Landroid/content/Context;

    #@12
    invoke-virtual {p0}, Landroid/app/TaskStackBuilder;->getIntents()[Landroid/content/Intent;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Context;->startActivitiesAsUser([Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@19
    .line 222
    return-void
.end method
