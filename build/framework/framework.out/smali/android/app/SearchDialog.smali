.class public Landroid/app/SearchDialog;
.super Landroid/app/Dialog;
.source "SearchDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/SearchDialog$SearchBar;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final IME_OPTION_NO_MICROPHONE:Ljava/lang/String; = "nm"

.field private static final INSTANCE_KEY_APPDATA:Ljava/lang/String; = "data"

.field private static final INSTANCE_KEY_COMPONENT:Ljava/lang/String; = "comp"

.field private static final INSTANCE_KEY_USER_QUERY:Ljava/lang/String; = "uQry"

.field private static final LOG_TAG:Ljava/lang/String; = "SearchDialog"

.field private static final SEARCH_PLATE_LEFT_PADDING_NON_GLOBAL:I = 0x7


# instance fields
.field private mActivityContext:Landroid/content/Context;

.field private mAppIcon:Landroid/widget/ImageView;

.field private mAppSearchData:Landroid/os/Bundle;

.field private mBadgeLabel:Landroid/widget/TextView;

.field private mCloseSearch:Landroid/view/View;

.field private mConfChangeListener:Landroid/content/BroadcastReceiver;

.field private mLaunchComponent:Landroid/content/ComponentName;

.field private final mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

.field private final mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private final mOnSuggestionSelectionListener:Landroid/widget/SearchView$OnSuggestionListener;

.field private mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

.field private mSearchAutoCompleteImeOptions:I

.field private mSearchPlate:Landroid/view/View;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSearchable:Landroid/app/SearchableInfo;

.field private mUserQuery:Ljava/lang/String;

.field private final mVoiceAppSearchIntent:Landroid/content/Intent;

.field private final mVoiceWebSearchIntent:Landroid/content/Intent;

.field private mWorkingSpinner:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/SearchManager;)V
    .registers 7
    .parameter "context"
    .parameter "searchManager"

    #@0
    .prologue
    const/high16 v3, 0x1000

    #@2
    .line 125
    invoke-static {p1}, Landroid/app/SearchDialog;->resolveDialogTheme(Landroid/content/Context;)I

    #@5
    move-result v0

    #@6
    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@9
    .line 103
    new-instance v0, Landroid/app/SearchDialog$1;

    #@b
    invoke-direct {v0, p0}, Landroid/app/SearchDialog$1;-><init>(Landroid/app/SearchDialog;)V

    #@e
    iput-object v0, p0, Landroid/app/SearchDialog;->mConfChangeListener:Landroid/content/BroadcastReceiver;

    #@10
    .line 674
    new-instance v0, Landroid/app/SearchDialog$3;

    #@12
    invoke-direct {v0, p0}, Landroid/app/SearchDialog$3;-><init>(Landroid/app/SearchDialog;)V

    #@15
    iput-object v0, p0, Landroid/app/SearchDialog;->mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

    #@17
    .line 681
    new-instance v0, Landroid/app/SearchDialog$4;

    #@19
    invoke-direct {v0, p0}, Landroid/app/SearchDialog$4;-><init>(Landroid/app/SearchDialog;)V

    #@1c
    iput-object v0, p0, Landroid/app/SearchDialog;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@1e
    .line 694
    new-instance v0, Landroid/app/SearchDialog$5;

    #@20
    invoke-direct {v0, p0}, Landroid/app/SearchDialog$5;-><init>(Landroid/app/SearchDialog;)V

    #@23
    iput-object v0, p0, Landroid/app/SearchDialog;->mOnSuggestionSelectionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@25
    .line 128
    new-instance v0, Landroid/content/Intent;

    #@27
    const-string v1, "android.speech.action.WEB_SEARCH"

    #@29
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2c
    iput-object v0, p0, Landroid/app/SearchDialog;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@2e
    .line 129
    iget-object v0, p0, Landroid/app/SearchDialog;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@30
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@33
    .line 130
    iget-object v0, p0, Landroid/app/SearchDialog;->mVoiceWebSearchIntent:Landroid/content/Intent;

    #@35
    const-string v1, "android.speech.extra.LANGUAGE_MODEL"

    #@37
    const-string/jumbo v2, "web_search"

    #@3a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d
    .line 133
    new-instance v0, Landroid/content/Intent;

    #@3f
    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    #@41
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@44
    iput-object v0, p0, Landroid/app/SearchDialog;->mVoiceAppSearchIntent:Landroid/content/Intent;

    #@46
    .line 134
    iget-object v0, p0, Landroid/app/SearchDialog;->mVoiceAppSearchIntent:Landroid/content/Intent;

    #@48
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@4b
    .line 135
    return-void
.end method

.method static synthetic access$000(Landroid/app/SearchDialog;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/SearchDialog;->onClosePressed()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private createContentView()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 165
    const v1, 0x10900c1

    #@4
    invoke-virtual {p0, v1}, Landroid/app/SearchDialog;->setContentView(I)V

    #@7
    .line 168
    const v1, 0x1020372

    #@a
    invoke-virtual {p0, v1}, Landroid/app/SearchDialog;->findViewById(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/app/SearchDialog$SearchBar;

    #@10
    .line 169
    .local v0, searchBar:Landroid/app/SearchDialog$SearchBar;
    invoke-virtual {v0, p0}, Landroid/app/SearchDialog$SearchBar;->setSearchDialog(Landroid/app/SearchDialog;)V

    #@13
    .line 170
    const v1, 0x1020374

    #@16
    invoke-virtual {p0, v1}, Landroid/app/SearchDialog;->findViewById(I)Landroid/view/View;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/widget/SearchView;

    #@1c
    iput-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@1e
    .line 171
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@20
    invoke-virtual {v1, v4}, Landroid/widget/SearchView;->setIconified(Z)V

    #@23
    .line 172
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@25
    iget-object v2, p0, Landroid/app/SearchDialog;->mOnCloseListener:Landroid/widget/SearchView$OnCloseListener;

    #@27
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    #@2a
    .line 173
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@2c
    iget-object v2, p0, Landroid/app/SearchDialog;->mOnQueryChangeListener:Landroid/widget/SearchView$OnQueryTextListener;

    #@2e
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    #@31
    .line 174
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@33
    iget-object v2, p0, Landroid/app/SearchDialog;->mOnSuggestionSelectionListener:Landroid/widget/SearchView$OnSuggestionListener;

    #@35
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    #@38
    .line 175
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@3a
    invoke-virtual {v1}, Landroid/widget/SearchView;->onActionViewExpanded()V

    #@3d
    .line 177
    const v1, 0x1020027

    #@40
    invoke-virtual {p0, v1}, Landroid/app/SearchDialog;->findViewById(I)Landroid/view/View;

    #@43
    move-result-object v1

    #@44
    iput-object v1, p0, Landroid/app/SearchDialog;->mCloseSearch:Landroid/view/View;

    #@46
    .line 178
    iget-object v1, p0, Landroid/app/SearchDialog;->mCloseSearch:Landroid/view/View;

    #@48
    iget-object v2, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@4a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4d
    move-result-object v2

    #@4e
    const v3, 0x104050b

    #@51
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    #@58
    .line 179
    iget-object v1, p0, Landroid/app/SearchDialog;->mCloseSearch:Landroid/view/View;

    #@5a
    new-instance v2, Landroid/app/SearchDialog$2;

    #@5c
    invoke-direct {v2, p0}, Landroid/app/SearchDialog$2;-><init>(Landroid/app/SearchDialog;)V

    #@5f
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@62
    .line 187
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@64
    const v2, 0x1020376

    #@67
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@6a
    move-result-object v1

    #@6b
    check-cast v1, Landroid/widget/TextView;

    #@6d
    iput-object v1, p0, Landroid/app/SearchDialog;->mBadgeLabel:Landroid/widget/TextView;

    #@6f
    .line 188
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@71
    const v2, 0x102037b

    #@74
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@77
    move-result-object v1

    #@78
    check-cast v1, Landroid/widget/AutoCompleteTextView;

    #@7a
    iput-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@7c
    .line 190
    const v1, 0x1020373

    #@7f
    invoke-virtual {p0, v1}, Landroid/app/SearchDialog;->findViewById(I)Landroid/view/View;

    #@82
    move-result-object v1

    #@83
    check-cast v1, Landroid/widget/ImageView;

    #@85
    iput-object v1, p0, Landroid/app/SearchDialog;->mAppIcon:Landroid/widget/ImageView;

    #@87
    .line 191
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@89
    const v2, 0x102037a

    #@8c
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    #@8f
    move-result-object v1

    #@90
    iput-object v1, p0, Landroid/app/SearchDialog;->mSearchPlate:Landroid/view/View;

    #@92
    .line 192
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@95
    move-result-object v1

    #@96
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@99
    move-result-object v1

    #@9a
    const v2, 0x10804f3

    #@9d
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@a0
    move-result-object v1

    #@a1
    iput-object v1, p0, Landroid/app/SearchDialog;->mWorkingSpinner:Landroid/graphics/drawable/Drawable;

    #@a3
    .line 197
    invoke-virtual {p0, v4}, Landroid/app/SearchDialog;->setWorking(Z)V

    #@a6
    .line 200
    iget-object v1, p0, Landroid/app/SearchDialog;->mBadgeLabel:Landroid/widget/TextView;

    #@a8
    const/16 v2, 0x8

    #@aa
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    #@ad
    .line 203
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@af
    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getImeOptions()I

    #@b2
    move-result v1

    #@b3
    iput v1, p0, Landroid/app/SearchDialog;->mSearchAutoCompleteImeOptions:I

    #@b5
    .line 204
    return-void
.end method

.method private createIntent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;
    .registers 10
    .parameter "action"
    .parameter "data"
    .parameter "extraData"
    .parameter "query"
    .parameter "actionKey"
    .parameter "actionMsg"

    #@0
    .prologue
    .line 591
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5
    .line 592
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@a
    .line 596
    if-eqz p2, :cond_f

    #@c
    .line 597
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@f
    .line 599
    :cond_f
    const-string/jumbo v1, "user_query"

    #@12
    iget-object v2, p0, Landroid/app/SearchDialog;->mUserQuery:Ljava/lang/String;

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@17
    .line 600
    if-eqz p4, :cond_1f

    #@19
    .line 601
    const-string/jumbo v1, "query"

    #@1c
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 603
    :cond_1f
    if-eqz p3, :cond_26

    #@21
    .line 604
    const-string v1, "intent_extra_data_key"

    #@23
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@26
    .line 606
    :cond_26
    iget-object v1, p0, Landroid/app/SearchDialog;->mAppSearchData:Landroid/os/Bundle;

    #@28
    if-eqz v1, :cond_31

    #@2a
    .line 607
    const-string v1, "app_data"

    #@2c
    iget-object v2, p0, Landroid/app/SearchDialog;->mAppSearchData:Landroid/os/Bundle;

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@31
    .line 609
    :cond_31
    if-eqz p5, :cond_3d

    #@33
    .line 610
    const-string v1, "action_key"

    #@35
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@38
    .line 611
    const-string v1, "action_msg"

    #@3a
    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d
    .line 613
    :cond_3d
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@3f
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@46
    .line 614
    return-object v0
.end method

.method private doShow(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;)Z
    .registers 6
    .parameter "initialQuery"
    .parameter "selectInitialQuery"
    .parameter "componentName"
    .parameter "appSearchData"

    #@0
    .prologue
    .line 231
    invoke-direct {p0, p3, p4}, Landroid/app/SearchDialog;->show(Landroid/content/ComponentName;Landroid/os/Bundle;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 232
    const/4 v0, 0x0

    #@7
    .line 241
    :goto_7
    return v0

    #@8
    .line 236
    :cond_8
    invoke-direct {p0, p1}, Landroid/app/SearchDialog;->setUserQuery(Ljava/lang/String;)V

    #@b
    .line 237
    if-eqz p2, :cond_12

    #@d
    .line 238
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@f
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->selectAll()V

    #@12
    .line 241
    :cond_12
    const/4 v0, 0x1

    #@13
    goto :goto_7
.end method

.method private isEmpty(Landroid/widget/AutoCompleteTextView;)Z
    .registers 3
    .parameter "actv"

    #@0
    .prologue
    .line 647
    invoke-virtual {p1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method static isLandscapeMode(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 381
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    #@a
    const/4 v1, 0x2

    #@b
    if-ne v0, v1, :cond_f

    #@d
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method private isOutOfBounds(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "v"
    .parameter "event"

    #@0
    .prologue
    .line 498
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v3

    #@4
    float-to-int v1, v3

    #@5
    .line 499
    .local v1, x:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    #@8
    move-result v3

    #@9
    float-to-int v2, v3

    #@a
    .line 500
    .local v2, y:I
    iget-object v3, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@c
    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    #@13
    move-result v0

    #@14
    .line 501
    .local v0, slop:I
    neg-int v3, v0

    #@15
    if-lt v1, v3, :cond_28

    #@17
    neg-int v3, v0

    #@18
    if-lt v2, v3, :cond_28

    #@1a
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    #@1d
    move-result v3

    #@1e
    add-int/2addr v3, v0

    #@1f
    if-gt v1, v3, :cond_28

    #@21
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    #@24
    move-result v3

    #@25
    add-int/2addr v3, v0

    #@26
    if-le v2, v3, :cond_2a

    #@28
    :cond_28
    const/4 v3, 0x1

    #@29
    :goto_29
    return v3

    #@2a
    :cond_2a
    const/4 v3, 0x0

    #@2b
    goto :goto_29
.end method

.method private launchIntent(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 548
    if-nez p1, :cond_3

    #@2
    .line 564
    :goto_2
    return-void

    #@3
    .line 551
    :cond_3
    const-string v1, "SearchDialog"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string/jumbo v3, "launching "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 555
    :try_start_1c
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@23
    .line 560
    invoke-virtual {p0}, Landroid/app/SearchDialog;->dismiss()V
    :try_end_26
    .catch Ljava/lang/RuntimeException; {:try_start_1c .. :try_end_26} :catch_27

    #@26
    goto :goto_2

    #@27
    .line 561
    :catch_27
    move-exception v0

    #@28
    .line 562
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v1, "SearchDialog"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Failed launch activity: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    goto :goto_2
.end method

.method private onClosePressed()Z
    .registers 2

    #@0
    .prologue
    .line 666
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@2
    invoke-direct {p0, v0}, Landroid/app/SearchDialog;->isEmpty(Landroid/widget/AutoCompleteTextView;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 667
    invoke-virtual {p0}, Landroid/app/SearchDialog;->dismiss()V

    #@b
    .line 668
    const/4 v0, 0x1

    #@c
    .line 671
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method static resolveDialogTheme(Landroid/content/Context;)I
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 113
    new-instance v0, Landroid/util/TypedValue;

    #@2
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@5
    .line 114
    .local v0, outValue:Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@8
    move-result-object v1

    #@9
    const v2, 0x10103f4

    #@c
    const/4 v3, 0x1

    #@d
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@10
    .line 116
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    #@12
    return v1
.end method

.method private setUserQuery(Ljava/lang/String;)V
    .registers 4
    .parameter "query"

    #@0
    .prologue
    .line 711
    if-nez p1, :cond_4

    #@2
    .line 712
    const-string p1, ""

    #@4
    .line 714
    :cond_4
    iput-object p1, p0, Landroid/app/SearchDialog;->mUserQuery:Ljava/lang/String;

    #@6
    .line 715
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@8
    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    #@b
    .line 716
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@10
    move-result v1

    #@11
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setSelection(I)V

    #@14
    .line 717
    return-void
.end method

.method private show(Landroid/content/ComponentName;Landroid/os/Bundle;)Z
    .registers 6
    .parameter "componentName"
    .parameter "appSearchData"

    #@0
    .prologue
    .line 256
    iget-object v1, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@2
    const-string/jumbo v2, "search"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/app/SearchManager;

    #@b
    .line 259
    .local v0, searchManager:Landroid/app/SearchManager;
    invoke-virtual {v0, p1}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@11
    .line 261
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@13
    if-nez v1, :cond_17

    #@15
    .line 262
    const/4 v1, 0x0

    #@16
    .line 281
    :goto_16
    return v1

    #@17
    .line 265
    :cond_17
    iput-object p1, p0, Landroid/app/SearchDialog;->mLaunchComponent:Landroid/content/ComponentName;

    #@19
    .line 266
    iput-object p2, p0, Landroid/app/SearchDialog;->mAppSearchData:Landroid/os/Bundle;

    #@1b
    .line 267
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@1d
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Landroid/app/SearchableInfo;->getActivityContext(Landroid/content/Context;)Landroid/content/Context;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Landroid/app/SearchDialog;->mActivityContext:Landroid/content/Context;

    #@27
    .line 270
    invoke-virtual {p0}, Landroid/app/SearchDialog;->isShowing()Z

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_41

    #@2d
    .line 273
    invoke-direct {p0}, Landroid/app/SearchDialog;->createContentView()V

    #@30
    .line 274
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@32
    iget-object v2, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@34
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    #@37
    .line 275
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchView:Landroid/widget/SearchView;

    #@39
    iget-object v2, p0, Landroid/app/SearchDialog;->mAppSearchData:Landroid/os/Bundle;

    #@3b
    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setAppSearchData(Landroid/os/Bundle;)V

    #@3e
    .line 277
    invoke-virtual {p0}, Landroid/app/SearchDialog;->show()V

    #@41
    .line 279
    :cond_41
    invoke-direct {p0}, Landroid/app/SearchDialog;->updateUI()V

    #@44
    .line 281
    const/4 v1, 0x1

    #@45
    goto :goto_16
.end method

.method private updateSearchAppIcon()V
    .registers 10

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 435
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@4
    move-result-object v4

    #@5
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@8
    move-result-object v3

    #@9
    .line 438
    .local v3, pm:Landroid/content/pm/PackageManager;
    :try_start_9
    iget-object v4, p0, Landroid/app/SearchDialog;->mLaunchComponent:Landroid/content/ComponentName;

    #@b
    const/4 v5, 0x0

    #@c
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@f
    move-result-object v2

    #@10
    .line 439
    .local v2, info:Landroid/content/pm/ActivityInfo;
    iget-object v4, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@12
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    :try_end_15
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_15} :catch_39

    #@15
    move-result-object v1

    #@16
    .line 446
    .end local v2           #info:Landroid/content/pm/ActivityInfo;
    .local v1, icon:Landroid/graphics/drawable/Drawable;
    :goto_16
    iget-object v4, p0, Landroid/app/SearchDialog;->mAppIcon:Landroid/widget/ImageView;

    #@18
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@1b
    .line 447
    iget-object v4, p0, Landroid/app/SearchDialog;->mAppIcon:Landroid/widget/ImageView;

    #@1d
    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    #@20
    .line 448
    iget-object v4, p0, Landroid/app/SearchDialog;->mSearchPlate:Landroid/view/View;

    #@22
    const/4 v5, 0x7

    #@23
    iget-object v6, p0, Landroid/app/SearchDialog;->mSearchPlate:Landroid/view/View;

    #@25
    invoke-virtual {v6}, Landroid/view/View;->getPaddingTop()I

    #@28
    move-result v6

    #@29
    iget-object v7, p0, Landroid/app/SearchDialog;->mSearchPlate:Landroid/view/View;

    #@2b
    invoke-virtual {v7}, Landroid/view/View;->getPaddingRight()I

    #@2e
    move-result v7

    #@2f
    iget-object v8, p0, Landroid/app/SearchDialog;->mSearchPlate:Landroid/view/View;

    #@31
    invoke-virtual {v8}, Landroid/view/View;->getPaddingBottom()I

    #@34
    move-result v8

    #@35
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    #@38
    .line 449
    return-void

    #@39
    .line 442
    .end local v1           #icon:Landroid/graphics/drawable/Drawable;
    :catch_39
    move-exception v0

    #@3a
    .line 443
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    #@3d
    move-result-object v1

    #@3e
    .line 444
    .restart local v1       #icon:Landroid/graphics/drawable/Drawable;
    const-string v4, "SearchDialog"

    #@40
    new-instance v5, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    iget-object v6, p0, Landroid/app/SearchDialog;->mLaunchComponent:Landroid/content/ComponentName;

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    const-string v6, " not found, using generic app icon"

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_16
.end method

.method private updateSearchAutoComplete()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 430
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setDropDownDismissedOnCompletion(Z)V

    #@6
    .line 431
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setForceIgnoreOutsideTouch(Z)V

    #@b
    .line 432
    return-void
.end method

.method private updateSearchBadge()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 456
    const/16 v2, 0x8

    #@3
    .line 457
    .local v2, visibility:I
    const/4 v0, 0x0

    #@4
    .line 458
    .local v0, icon:Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    #@5
    .line 461
    .local v1, text:Ljava/lang/CharSequence;
    iget-object v3, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@7
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->useBadgeIcon()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_2e

    #@d
    .line 462
    iget-object v3, p0, Landroid/app/SearchDialog;->mActivityContext:Landroid/content/Context;

    #@f
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v3

    #@13
    iget-object v4, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@15
    invoke-virtual {v4}, Landroid/app/SearchableInfo;->getIconId()I

    #@18
    move-result v4

    #@19
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1c
    move-result-object v0

    #@1d
    .line 463
    const/4 v2, 0x0

    #@1e
    .line 471
    :cond_1e
    :goto_1e
    iget-object v3, p0, Landroid/app/SearchDialog;->mBadgeLabel:Landroid/widget/TextView;

    #@20
    invoke-virtual {v3, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@23
    .line 472
    iget-object v3, p0, Landroid/app/SearchDialog;->mBadgeLabel:Landroid/widget/TextView;

    #@25
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@28
    .line 473
    iget-object v3, p0, Landroid/app/SearchDialog;->mBadgeLabel:Landroid/widget/TextView;

    #@2a
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    #@2d
    .line 474
    return-void

    #@2e
    .line 465
    :cond_2e
    iget-object v3, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@30
    invoke-virtual {v3}, Landroid/app/SearchableInfo;->useBadgeLabel()Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_1e

    #@36
    .line 466
    iget-object v3, p0, Landroid/app/SearchDialog;->mActivityContext:Landroid/content/Context;

    #@38
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3b
    move-result-object v3

    #@3c
    iget-object v4, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@3e
    invoke-virtual {v4}, Landroid/app/SearchableInfo;->getLabelId()I

    #@41
    move-result v4

    #@42
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    .line 467
    const/4 v2, 0x0

    #@4b
    goto :goto_1e
.end method

.method private updateUI()V
    .registers 4

    #@0
    .prologue
    .line 389
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    if-eqz v1, :cond_51

    #@4
    .line 390
    iget-object v1, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@a
    .line 391
    invoke-direct {p0}, Landroid/app/SearchDialog;->updateSearchAutoComplete()V

    #@d
    .line 392
    invoke-direct {p0}, Landroid/app/SearchDialog;->updateSearchAppIcon()V

    #@10
    .line 393
    invoke-direct {p0}, Landroid/app/SearchDialog;->updateSearchBadge()V

    #@13
    .line 400
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@15
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getInputType()I

    #@18
    move-result v0

    #@19
    .line 403
    .local v0, inputType:I
    and-int/lit8 v1, v0, 0xf

    #@1b
    const/4 v2, 0x1

    #@1c
    if-ne v1, v2, :cond_2d

    #@1e
    .line 406
    const v1, -0x10001

    #@21
    and-int/2addr v0, v1

    #@22
    .line 407
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@24
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    if-eqz v1, :cond_2d

    #@2a
    .line 408
    const/high16 v1, 0x1

    #@2c
    or-int/2addr v0, v1

    #@2d
    .line 411
    :cond_2d
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@2f
    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setInputType(I)V

    #@32
    .line 412
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@34
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getImeOptions()I

    #@37
    move-result v1

    #@38
    iput v1, p0, Landroid/app/SearchDialog;->mSearchAutoCompleteImeOptions:I

    #@3a
    .line 413
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@3c
    iget v2, p0, Landroid/app/SearchDialog;->mSearchAutoCompleteImeOptions:I

    #@3e
    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setImeOptions(I)V

    #@41
    .line 417
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@43
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getVoiceSearchEnabled()Z

    #@46
    move-result v1

    #@47
    if-eqz v1, :cond_52

    #@49
    .line 418
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@4b
    const-string/jumbo v2, "nm"

    #@4e
    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setPrivateImeOptions(Ljava/lang/String;)V

    #@51
    .line 423
    .end local v0           #inputType:I
    :cond_51
    :goto_51
    return-void

    #@52
    .line 420
    .restart local v0       #inputType:I
    :cond_52
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@54
    const/4 v2, 0x0

    #@55
    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setPrivateImeOptions(Ljava/lang/String;)V

    #@58
    goto :goto_51
.end method


# virtual methods
.method public hide()V
    .registers 4

    #@0
    .prologue
    .line 508
    invoke-virtual {p0}, Landroid/app/SearchDialog;->isShowing()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 520
    :goto_6
    return-void

    #@7
    .line 512
    :cond_7
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@a
    move-result-object v1

    #@b
    const-string v2, "input_method"

    #@d
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@13
    .line 514
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_25

    #@15
    .line 515
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getWindow()Landroid/view/Window;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@20
    move-result-object v1

    #@21
    const/4 v2, 0x0

    #@22
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@25
    .line 519
    :cond_25
    invoke-super {p0}, Landroid/app/Dialog;->hide()V

    #@28
    goto :goto_6
.end method

.method public launchQuerySearch()V
    .registers 3

    #@0
    .prologue
    .line 526
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/app/SearchDialog;->launchQuerySearch(ILjava/lang/String;)V

    #@5
    .line 527
    return-void
.end method

.method protected launchQuerySearch(ILjava/lang/String;)V
    .registers 11
    .parameter "actionKey"
    .parameter "actionMsg"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 538
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@3
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@a
    move-result-object v4

    #@b
    .line 539
    .local v4, query:Ljava/lang/String;
    const-string v1, "android.intent.action.SEARCH"

    #@d
    .local v1, action:Ljava/lang/String;
    move-object v0, p0

    #@e
    move-object v3, v2

    #@f
    move v5, p1

    #@10
    move-object v6, p2

    #@11
    .line 540
    invoke-direct/range {v0 .. v6}, Landroid/app/SearchDialog;->createIntent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    #@14
    move-result-object v7

    #@15
    .line 541
    .local v7, intent:Landroid/content/Intent;
    invoke-direct {p0, v7}, Landroid/app/SearchDialog;->launchIntent(Landroid/content/Intent;)V

    #@18
    .line 542
    return-void
.end method

.method public onBackPressed()V
    .registers 4

    #@0
    .prologue
    .line 654
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    const-string v2, "input_method"

    #@6
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    #@c
    .line 656
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_28

    #@e
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_28

    #@14
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getWindow()Landroid/view/Window;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@1f
    move-result-object v1

    #@20
    const/4 v2, 0x0

    #@21
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_28

    #@27
    .line 662
    :goto_27
    return-void

    #@28
    .line 661
    :cond_28
    invoke-virtual {p0}, Landroid/app/SearchDialog;->cancel()V

    #@2b
    goto :goto_27
.end method

.method public onConfigurationChanged()V
    .registers 3

    #@0
    .prologue
    .line 370
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@2
    if-eqz v0, :cond_20

    #@4
    invoke-virtual {p0}, Landroid/app/SearchDialog;->isShowing()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_20

    #@a
    .line 372
    invoke-direct {p0}, Landroid/app/SearchDialog;->updateSearchAppIcon()V

    #@d
    .line 373
    invoke-direct {p0}, Landroid/app/SearchDialog;->updateSearchBadge()V

    #@10
    .line 374
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@13
    move-result-object v0

    #@14
    invoke-static {v0}, Landroid/app/SearchDialog;->isLandscapeMode(Landroid/content/Context;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_20

    #@1a
    .line 375
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@1c
    const/4 v1, 0x1

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->ensureImeVisible(Z)V

    #@20
    .line 378
    :cond_20
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 143
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 145
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getWindow()Landroid/view/Window;

    #@7
    move-result-object v1

    #@8
    .line 146
    .local v1, theWindow:Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@b
    move-result-object v0

    #@c
    .line 147
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@e
    .line 151
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@10
    .line 152
    const/16 v2, 0x37

    #@12
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@14
    .line 153
    const/16 v2, 0x10

    #@16
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@18
    .line 154
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@1b
    .line 157
    const/4 v2, 0x1

    #@1c
    invoke-virtual {p0, v2}, Landroid/app/SearchDialog;->setCanceledOnTouchOutside(Z)V

    #@1f
    .line 158
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 353
    if-nez p1, :cond_3

    #@2
    .line 364
    :cond_2
    :goto_2
    return-void

    #@3
    .line 355
    :cond_3
    const-string v3, "comp"

    #@5
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Landroid/content/ComponentName;

    #@b
    .line 356
    .local v1, launchComponent:Landroid/content/ComponentName;
    const-string v3, "data"

    #@d
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@10
    move-result-object v0

    #@11
    .line 357
    .local v0, appSearchData:Landroid/os/Bundle;
    const-string/jumbo v3, "uQry"

    #@14
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 360
    .local v2, userQuery:Ljava/lang/String;
    const/4 v3, 0x0

    #@19
    invoke-direct {p0, v2, v3, v1, v0}, Landroid/app/SearchDialog;->doShow(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_2

    #@1f
    goto :goto_2
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 333
    invoke-virtual {p0}, Landroid/app/SearchDialog;->isShowing()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 342
    :goto_7
    return-object v0

    #@8
    .line 335
    :cond_8
    new-instance v0, Landroid/os/Bundle;

    #@a
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@d
    .line 338
    .local v0, bundle:Landroid/os/Bundle;
    const-string v1, "comp"

    #@f
    iget-object v2, p0, Landroid/app/SearchDialog;->mLaunchComponent:Landroid/content/ComponentName;

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@14
    .line 339
    const-string v1, "data"

    #@16
    iget-object v2, p0, Landroid/app/SearchDialog;->mAppSearchData:Landroid/os/Bundle;

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@1b
    .line 340
    const-string/jumbo v1, "uQry"

    #@1e
    iget-object v2, p0, Landroid/app/SearchDialog;->mUserQuery:Ljava/lang/String;

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_7
.end method

.method public onStart()V
    .registers 4

    #@0
    .prologue
    .line 286
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    #@3
    .line 289
    new-instance v0, Landroid/content/IntentFilter;

    #@5
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@8
    .line 290
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    #@a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d
    .line 291
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Landroid/app/SearchDialog;->mConfChangeListener:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    .line 292
    return-void
.end method

.method public onStop()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 302
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    #@4
    .line 304
    invoke-virtual {p0}, Landroid/app/SearchDialog;->getContext()Landroid/content/Context;

    #@7
    move-result-object v0

    #@8
    iget-object v1, p0, Landroid/app/SearchDialog;->mConfChangeListener:Landroid/content/BroadcastReceiver;

    #@a
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@d
    .line 307
    iput-object v2, p0, Landroid/app/SearchDialog;->mLaunchComponent:Landroid/content/ComponentName;

    #@f
    .line 308
    iput-object v2, p0, Landroid/app/SearchDialog;->mAppSearchData:Landroid/os/Bundle;

    #@11
    .line 309
    iput-object v2, p0, Landroid/app/SearchDialog;->mSearchable:Landroid/app/SearchableInfo;

    #@13
    .line 310
    iput-object v2, p0, Landroid/app/SearchDialog;->mUserQuery:Ljava/lang/String;

    #@15
    .line 311
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 488
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isPopupShowing()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_15

    #@8
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchPlate:Landroid/view/View;

    #@a
    invoke-direct {p0, v0, p1}, Landroid/app/SearchDialog;->isOutOfBounds(Landroid/view/View;Landroid/view/MotionEvent;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 490
    invoke-virtual {p0}, Landroid/app/SearchDialog;->cancel()V

    #@13
    .line 491
    const/4 v0, 0x1

    #@14
    .line 494
    :goto_14
    return v0

    #@15
    :cond_15
    invoke-super {p0, p1}, Landroid/app/Dialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@18
    move-result v0

    #@19
    goto :goto_14
.end method

.method public setListSelection(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 570
    iget-object v0, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setListSelection(I)V

    #@5
    .line 571
    return-void
.end method

.method public setWorking(Z)V
    .registers 5
    .parameter "working"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 320
    iget-object v2, p0, Landroid/app/SearchDialog;->mWorkingSpinner:Landroid/graphics/drawable/Drawable;

    #@3
    if-eqz p1, :cond_15

    #@5
    const/16 v0, 0xff

    #@7
    :goto_7
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@a
    .line 321
    iget-object v0, p0, Landroid/app/SearchDialog;->mWorkingSpinner:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@f
    .line 322
    iget-object v0, p0, Landroid/app/SearchDialog;->mWorkingSpinner:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    #@14
    .line 323
    return-void

    #@15
    :cond_15
    move v0, v1

    #@16
    .line 320
    goto :goto_7
.end method

.method public show(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;)Z
    .registers 7
    .parameter "initialQuery"
    .parameter "selectInitialQuery"
    .parameter "componentName"
    .parameter "appSearchData"

    #@0
    .prologue
    .line 213
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/app/SearchDialog;->doShow(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;)Z

    #@3
    move-result v0

    #@4
    .line 214
    .local v0, success:Z
    if-eqz v0, :cond_b

    #@6
    .line 217
    iget-object v1, p0, Landroid/app/SearchDialog;->mSearchAutoComplete:Landroid/widget/AutoCompleteTextView;

    #@8
    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->showDropDownAfterLayout()V

    #@b
    .line 219
    :cond_b
    return v0
.end method
