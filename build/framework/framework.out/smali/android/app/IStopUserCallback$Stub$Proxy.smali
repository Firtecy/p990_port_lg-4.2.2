.class Landroid/app/IStopUserCallback$Stub$Proxy;
.super Ljava/lang/Object;
.source "IStopUserCallback.java"

# interfaces
.implements Landroid/app/IStopUserCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IStopUserCallback$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    iput-object p1, p0, Landroid/app/IStopUserCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 76
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/app/IStopUserCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 83
    const-string v0, "android.app.IStopUserCallback"

    #@2
    return-object v0
.end method

.method public userStopAborted(I)V
    .registers 7
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 102
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 103
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 105
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.app.IStopUserCallback"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 106
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 107
    iget-object v2, p0, Landroid/app/IStopUserCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x2

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 108
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 111
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 112
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 114
    return-void

    #@21
    .line 111
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 112
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public userStopped(I)V
    .registers 7
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 87
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 88
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 90
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.app.IStopUserCallback"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 91
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 92
    iget-object v2, p0, Landroid/app/IStopUserCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x1

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 93
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 96
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 97
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 99
    return-void

    #@21
    .line 96
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 97
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method
