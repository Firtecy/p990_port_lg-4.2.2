.class Landroid/app/ActivityThread$H;
.super Landroid/os/Handler;
.source "ActivityThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field public static final ACTIVITY_CONFIGURATION_CHANGED:I = 0x7d

.field public static final BIND_APPLICATION:I = 0x6e

.field public static final BIND_SERVICE:I = 0x79

.field public static final CLEAN_UP_CONTEXT:I = 0x77

.field public static final CONFIGURATION_CHANGED:I = 0x76

.field public static final CREATE_BACKUP_AGENT:I = 0x80

.field public static final CREATE_SERVICE:I = 0x72

.field public static final DESTROY_ACTIVITY:I = 0x6d

.field public static final DESTROY_BACKUP_AGENT:I = 0x81

.field public static final DISPATCH_PACKAGE_BROADCAST:I = 0x85

.field public static final DUMP_ACTIVITY:I = 0x88

.field public static final DUMP_HEAP:I = 0x87

.field public static final DUMP_PROVIDER:I = 0x8d

.field public static final DUMP_SERVICE:I = 0x7b

.field public static final ENABLE_JIT:I = 0x84

.field public static final EXIT_APPLICATION:I = 0x6f

.field public static final GC_WHEN_IDLE:I = 0x78

.field public static final HIDE_WINDOW:I = 0x6a

.field public static final LAUNCH_ACTIVITY:I = 0x64

.field public static final LOW_MEMORY:I = 0x7c

.field public static final NEW_INTENT:I = 0x70

.field public static final PAUSE_ACTIVITY:I = 0x65

.field public static final PAUSE_ACTIVITY_FINISHING:I = 0x66

.field public static final PROFILER_CONTROL:I = 0x7f

.field public static final RECEIVER:I = 0x71

.field public static final RELAUNCH_ACTIVITY:I = 0x7e

.field public static final REMOVE_PROVIDER:I = 0x83

.field public static final REQUEST_THUMBNAIL:I = 0x75

.field public static final RESUME_ACTIVITY:I = 0x6b

.field public static final SCHEDULE_CRASH:I = 0x86

.field public static final SEND_RESULT:I = 0x6c

.field public static final SERVICE_ARGS:I = 0x73

.field public static final SET_CORE_SETTINGS:I = 0x8a

.field public static final SHOW_WINDOW:I = 0x69

.field public static final SLEEPING:I = 0x89

.field public static final STOP_ACTIVITY_HIDE:I = 0x68

.field public static final STOP_ACTIVITY_SHOW:I = 0x67

.field public static final STOP_SERVICE:I = 0x74

.field public static final SUICIDE:I = 0x82

.field public static final TRIM_MEMORY:I = 0x8c

.field public static final UNBIND_SERVICE:I = 0x7a

.field public static final UNSTABLE_PROVIDER_DIED:I = 0x8e

.field public static final UPDATE_PACKAGE_COMPATIBILITY_INFO:I = 0x8b


# instance fields
.field final synthetic this$0:Landroid/app/ActivityThread;


# direct methods
.method private constructor <init>(Landroid/app/ActivityThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1137
    iput-object p1, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/ActivityThread;Landroid/app/ActivityThread$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1137
    invoke-direct {p0, p1}, Landroid/app/ActivityThread$H;-><init>(Landroid/app/ActivityThread;)V

    #@3
    return-void
.end method

.method private maybeSnapshot()V
    .registers 9

    #@0
    .prologue
    .line 1443
    iget-object v5, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@2
    iget-object v5, v5, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@4
    if-eqz v5, :cond_24

    #@6
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->isEnabled()Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_24

    #@c
    .line 1446
    iget-object v5, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@e
    iget-object v5, v5, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@10
    iget-object v5, v5, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@12
    iget-object v3, v5, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@14
    .line 1447
    .local v3, packageName:Ljava/lang/String;
    const/4 v2, 0x0

    #@15
    .line 1449
    .local v2, packageInfo:Landroid/content/pm/PackageInfo;
    :try_start_15
    iget-object v5, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@17
    invoke-virtual {v5}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    #@1a
    move-result-object v0

    #@1b
    .line 1450
    .local v0, context:Landroid/content/Context;
    if-nez v0, :cond_25

    #@1d
    .line 1451
    const-string v5, "ActivityThread"

    #@1f
    const-string v6, "cannot get a valid context"

    #@21
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1466
    .end local v0           #context:Landroid/content/Context;
    .end local v2           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v3           #packageName:Ljava/lang/String;
    :cond_24
    :goto_24
    return-void

    #@25
    .line 1454
    .restart local v0       #context:Landroid/content/Context;
    .restart local v2       #packageInfo:Landroid/content/pm/PackageInfo;
    .restart local v3       #packageName:Ljava/lang/String;
    :cond_25
    invoke-virtual {v0}, Landroid/app/ContextImpl;->getPackageManager()Landroid/content/pm/PackageManager;

    #@28
    move-result-object v4

    #@29
    .line 1455
    .local v4, pm:Landroid/content/pm/PackageManager;
    if-nez v4, :cond_56

    #@2b
    .line 1456
    const-string v5, "ActivityThread"

    #@2d
    const-string v6, "cannot get a valid PackageManager"

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_15 .. :try_end_32} :catch_33

    #@32
    goto :goto_24

    #@33
    .line 1461
    .end local v0           #context:Landroid/content/Context;
    .end local v4           #pm:Landroid/content/pm/PackageManager;
    :catch_33
    move-exception v1

    #@34
    .line 1462
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "ActivityThread"

    #@36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v7, "cannot get package info for "

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4c
    .line 1464
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_4c
    iget-object v5, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@4e
    iget-object v5, v5, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@50
    iget-object v5, v5, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@52
    invoke-static {v5, v2}, Lcom/android/internal/os/SamplingProfilerIntegration;->writeSnapshot(Ljava/lang/String;Landroid/content/pm/PackageInfo;)V

    #@55
    goto :goto_24

    #@56
    .line 1459
    .restart local v0       #context:Landroid/content/Context;
    .restart local v4       #pm:Landroid/content/pm/PackageManager;
    :cond_56
    const/4 v5, 0x1

    #@57
    :try_start_57
    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_5a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_57 .. :try_end_5a} :catch_33

    #@5a
    move-result-object v2

    #@5b
    goto :goto_4c
.end method


# virtual methods
.method codeToString(I)Ljava/lang/String;
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 1229
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const-wide/16 v8, 0x40

    #@5
    .line 1233
    iget v3, p1, Landroid/os/Message;->what:I

    #@7
    packed-switch v3, :pswitch_data_330

    #@a
    .line 1440
    :goto_a
    return-void

    #@b
    .line 1235
    :pswitch_b
    const-string v3, "activityStart"

    #@d
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@10
    .line 1236
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@14
    .line 1238
    .local v2, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@16
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@18
    iget-object v4, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1a
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@1c
    invoke-virtual {v3, v4, v5}, Landroid/app/ActivityThread;->getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;

    #@1f
    move-result-object v3

    #@20
    iput-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@22
    .line 1240
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@24
    invoke-static {v3, v2, v6}, Landroid/app/ActivityThread;->access$600(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)V

    #@27
    .line 1241
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@2a
    goto :goto_a

    #@2b
    .line 1244
    .end local v2           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :pswitch_2b
    const-string v3, "activityRestart"

    #@2d
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@30
    .line 1245
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@32
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@34
    .line 1246
    .restart local v2       #r:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@36
    invoke-static {v3, v2}, Landroid/app/ActivityThread;->access$700(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ActivityClientRecord;)V

    #@39
    .line 1247
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@3c
    goto :goto_a

    #@3d
    .line 1250
    .end local v2           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :pswitch_3d
    const-string v3, "activityPause"

    #@3f
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@42
    .line 1251
    iget-object v6, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@44
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@46
    check-cast v3, Landroid/os/IBinder;

    #@48
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@4a
    if-eqz v7, :cond_58

    #@4c
    :goto_4c
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@4e
    invoke-static {v6, v3, v5, v4, v7}, Landroid/app/ActivityThread;->access$800(Landroid/app/ActivityThread;Landroid/os/IBinder;ZZI)V

    #@51
    .line 1252
    invoke-direct {p0}, Landroid/app/ActivityThread$H;->maybeSnapshot()V

    #@54
    .line 1253
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@57
    goto :goto_a

    #@58
    :cond_58
    move v4, v5

    #@59
    .line 1251
    goto :goto_4c

    #@5a
    .line 1256
    :pswitch_5a
    const-string v3, "activityPause"

    #@5c
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@5f
    .line 1257
    iget-object v6, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@61
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@63
    check-cast v3, Landroid/os/IBinder;

    #@65
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@67
    if-eqz v7, :cond_6a

    #@69
    move v5, v4

    #@6a
    :cond_6a
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@6c
    invoke-static {v6, v3, v4, v5, v7}, Landroid/app/ActivityThread;->access$800(Landroid/app/ActivityThread;Landroid/os/IBinder;ZZI)V

    #@6f
    .line 1258
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@72
    goto :goto_a

    #@73
    .line 1261
    :pswitch_73
    const-string v3, "activityStop"

    #@75
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@78
    .line 1262
    iget-object v5, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@7a
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7c
    check-cast v3, Landroid/os/IBinder;

    #@7e
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@80
    invoke-static {v5, v3, v4, v6}, Landroid/app/ActivityThread;->access$900(Landroid/app/ActivityThread;Landroid/os/IBinder;ZI)V

    #@83
    .line 1263
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@86
    goto :goto_a

    #@87
    .line 1266
    :pswitch_87
    const-string v3, "activityStop"

    #@89
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@8c
    .line 1267
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@8e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@90
    check-cast v3, Landroid/os/IBinder;

    #@92
    iget v6, p1, Landroid/os/Message;->arg2:I

    #@94
    invoke-static {v4, v3, v5, v6}, Landroid/app/ActivityThread;->access$900(Landroid/app/ActivityThread;Landroid/os/IBinder;ZI)V

    #@97
    .line 1268
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@9a
    goto/16 :goto_a

    #@9c
    .line 1271
    :pswitch_9c
    const-string v3, "activityShowWindow"

    #@9e
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@a1
    .line 1272
    iget-object v5, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@a3
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a5
    check-cast v3, Landroid/os/IBinder;

    #@a7
    invoke-static {v5, v3, v4}, Landroid/app/ActivityThread;->access$1000(Landroid/app/ActivityThread;Landroid/os/IBinder;Z)V

    #@aa
    .line 1273
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@ad
    goto/16 :goto_a

    #@af
    .line 1276
    :pswitch_af
    const-string v3, "activityHideWindow"

    #@b1
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@b4
    .line 1277
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@b6
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b8
    check-cast v3, Landroid/os/IBinder;

    #@ba
    invoke-static {v4, v3, v5}, Landroid/app/ActivityThread;->access$1000(Landroid/app/ActivityThread;Landroid/os/IBinder;Z)V

    #@bd
    .line 1278
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@c0
    goto/16 :goto_a

    #@c2
    .line 1281
    :pswitch_c2
    const-string v3, "activityResume"

    #@c4
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@c7
    .line 1282
    iget-object v6, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@c9
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@cb
    check-cast v3, Landroid/os/IBinder;

    #@cd
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@cf
    if-eqz v7, :cond_d2

    #@d1
    move v5, v4

    #@d2
    :cond_d2
    invoke-virtual {v6, v3, v4, v5, v4}, Landroid/app/ActivityThread;->handleResumeActivity(Landroid/os/IBinder;ZZZ)V

    #@d5
    .line 1284
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@d8
    goto/16 :goto_a

    #@da
    .line 1287
    :pswitch_da
    const-string v3, "activityDeliverResult"

    #@dc
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@df
    .line 1288
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@e1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e3
    check-cast v3, Landroid/app/ActivityThread$ResultData;

    #@e5
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1100(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ResultData;)V

    #@e8
    .line 1289
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@eb
    goto/16 :goto_a

    #@ed
    .line 1292
    :pswitch_ed
    const-string v3, "activityDestroy"

    #@ef
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@f2
    .line 1293
    iget-object v6, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@f4
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f6
    check-cast v3, Landroid/os/IBinder;

    #@f8
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@fa
    if-eqz v7, :cond_106

    #@fc
    :goto_fc
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@fe
    invoke-static {v6, v3, v4, v7, v5}, Landroid/app/ActivityThread;->access$1200(Landroid/app/ActivityThread;Landroid/os/IBinder;ZIZ)V

    #@101
    .line 1295
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@104
    goto/16 :goto_a

    #@106
    :cond_106
    move v4, v5

    #@107
    .line 1293
    goto :goto_fc

    #@108
    .line 1298
    :pswitch_108
    const-string v3, "bindApplication"

    #@10a
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@10d
    .line 1299
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10f
    check-cast v1, Landroid/app/ActivityThread$AppBindData;

    #@111
    .line 1300
    .local v1, data:Landroid/app/ActivityThread$AppBindData;
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@113
    invoke-static {v3, v1}, Landroid/app/ActivityThread;->access$1300(Landroid/app/ActivityThread;Landroid/app/ActivityThread$AppBindData;)V

    #@116
    .line 1301
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@119
    goto/16 :goto_a

    #@11b
    .line 1304
    .end local v1           #data:Landroid/app/ActivityThread$AppBindData;
    :pswitch_11b
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@11d
    iget-object v3, v3, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@11f
    if-eqz v3, :cond_128

    #@121
    .line 1305
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@123
    iget-object v3, v3, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@125
    invoke-virtual {v3}, Landroid/app/Application;->onTerminate()V

    #@128
    .line 1307
    :cond_128
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@12b
    move-result-object v3

    #@12c
    invoke-virtual {v3}, Landroid/os/Looper;->quit()V

    #@12f
    goto/16 :goto_a

    #@131
    .line 1310
    :pswitch_131
    const-string v3, "activityNewIntent"

    #@133
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@136
    .line 1311
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@138
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13a
    check-cast v3, Landroid/app/ActivityThread$NewIntentData;

    #@13c
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1400(Landroid/app/ActivityThread;Landroid/app/ActivityThread$NewIntentData;)V

    #@13f
    .line 1312
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@142
    goto/16 :goto_a

    #@144
    .line 1315
    :pswitch_144
    const-string v3, "broadcastReceiveComp"

    #@146
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@149
    .line 1316
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@14b
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14d
    check-cast v3, Landroid/app/ActivityThread$ReceiverData;

    #@14f
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1500(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ReceiverData;)V

    #@152
    .line 1317
    invoke-direct {p0}, Landroid/app/ActivityThread$H;->maybeSnapshot()V

    #@155
    .line 1318
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@158
    goto/16 :goto_a

    #@15a
    .line 1321
    :pswitch_15a
    const-string/jumbo v3, "serviceCreate"

    #@15d
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@160
    .line 1322
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@162
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@164
    check-cast v3, Landroid/app/ActivityThread$CreateServiceData;

    #@166
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1600(Landroid/app/ActivityThread;Landroid/app/ActivityThread$CreateServiceData;)V

    #@169
    .line 1323
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@16c
    goto/16 :goto_a

    #@16e
    .line 1326
    :pswitch_16e
    const-string/jumbo v3, "serviceBind"

    #@171
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@174
    .line 1327
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@176
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@178
    check-cast v3, Landroid/app/ActivityThread$BindServiceData;

    #@17a
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1700(Landroid/app/ActivityThread;Landroid/app/ActivityThread$BindServiceData;)V

    #@17d
    .line 1328
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@180
    goto/16 :goto_a

    #@182
    .line 1331
    :pswitch_182
    const-string/jumbo v3, "serviceUnbind"

    #@185
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@188
    .line 1332
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@18a
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18c
    check-cast v3, Landroid/app/ActivityThread$BindServiceData;

    #@18e
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1800(Landroid/app/ActivityThread;Landroid/app/ActivityThread$BindServiceData;)V

    #@191
    .line 1333
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@194
    goto/16 :goto_a

    #@196
    .line 1336
    :pswitch_196
    const-string/jumbo v3, "serviceStart"

    #@199
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@19c
    .line 1337
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@19e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a0
    check-cast v3, Landroid/app/ActivityThread$ServiceArgsData;

    #@1a2
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$1900(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ServiceArgsData;)V

    #@1a5
    .line 1338
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@1a8
    goto/16 :goto_a

    #@1aa
    .line 1341
    :pswitch_1aa
    const-string/jumbo v3, "serviceStop"

    #@1ad
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@1b0
    .line 1342
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@1b2
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1b4
    check-cast v3, Landroid/os/IBinder;

    #@1b6
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2000(Landroid/app/ActivityThread;Landroid/os/IBinder;)V

    #@1b9
    .line 1343
    invoke-direct {p0}, Landroid/app/ActivityThread$H;->maybeSnapshot()V

    #@1bc
    .line 1344
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@1bf
    goto/16 :goto_a

    #@1c1
    .line 1347
    :pswitch_1c1
    const-string/jumbo v3, "requestThumbnail"

    #@1c4
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@1c7
    .line 1348
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@1c9
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1cb
    check-cast v3, Landroid/os/IBinder;

    #@1cd
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2100(Landroid/app/ActivityThread;Landroid/os/IBinder;)V

    #@1d0
    .line 1349
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@1d3
    goto/16 :goto_a

    #@1d5
    .line 1352
    :pswitch_1d5
    const-string v3, "configChanged"

    #@1d7
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@1da
    .line 1353
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@1dc
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1de
    check-cast v3, Landroid/content/res/Configuration;

    #@1e0
    iget v3, v3, Landroid/content/res/Configuration;->densityDpi:I

    #@1e2
    iput v3, v4, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@1e4
    .line 1354
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@1e6
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e8
    check-cast v3, Landroid/content/res/Configuration;

    #@1ea
    invoke-virtual {v4, v3, v6}, Landroid/app/ActivityThread;->handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@1ed
    .line 1355
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@1f0
    goto/16 :goto_a

    #@1f2
    .line 1358
    :pswitch_1f2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f4
    check-cast v0, Landroid/app/ActivityThread$ContextCleanupInfo;

    #@1f6
    .line 1359
    .local v0, cci:Landroid/app/ActivityThread$ContextCleanupInfo;
    iget-object v3, v0, Landroid/app/ActivityThread$ContextCleanupInfo;->context:Landroid/app/ContextImpl;

    #@1f8
    iget-object v4, v0, Landroid/app/ActivityThread$ContextCleanupInfo;->who:Ljava/lang/String;

    #@1fa
    iget-object v5, v0, Landroid/app/ActivityThread$ContextCleanupInfo;->what:Ljava/lang/String;

    #@1fc
    invoke-virtual {v3, v4, v5}, Landroid/app/ContextImpl;->performFinalCleanup(Ljava/lang/String;Ljava/lang/String;)V

    #@1ff
    goto/16 :goto_a

    #@201
    .line 1362
    .end local v0           #cci:Landroid/app/ActivityThread$ContextCleanupInfo;
    :pswitch_201
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@203
    invoke-virtual {v3}, Landroid/app/ActivityThread;->scheduleGcIdler()V

    #@206
    goto/16 :goto_a

    #@208
    .line 1365
    :pswitch_208
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@20a
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20c
    check-cast v3, Landroid/app/ActivityThread$DumpComponentInfo;

    #@20e
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2200(Landroid/app/ActivityThread;Landroid/app/ActivityThread$DumpComponentInfo;)V

    #@211
    goto/16 :goto_a

    #@213
    .line 1368
    :pswitch_213
    const-string/jumbo v3, "lowMemory"

    #@216
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@219
    .line 1369
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@21b
    invoke-virtual {v3}, Landroid/app/ActivityThread;->handleLowMemory()V

    #@21e
    .line 1370
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@221
    goto/16 :goto_a

    #@223
    .line 1373
    :pswitch_223
    const-string v3, "activityConfigChanged"

    #@225
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@228
    .line 1374
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@22a
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22c
    check-cast v3, Landroid/os/IBinder;

    #@22e
    invoke-virtual {v4, v3}, Landroid/app/ActivityThread;->handleActivityConfigurationChanged(Landroid/os/IBinder;)V

    #@231
    .line 1375
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@234
    goto/16 :goto_a

    #@236
    .line 1378
    :pswitch_236
    iget-object v6, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@238
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@23a
    if-eqz v3, :cond_247

    #@23c
    :goto_23c
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23e
    check-cast v3, Landroid/app/ActivityThread$ProfilerControlData;

    #@240
    iget v5, p1, Landroid/os/Message;->arg2:I

    #@242
    invoke-virtual {v6, v4, v3, v5}, Landroid/app/ActivityThread;->handleProfilerControl(ZLandroid/app/ActivityThread$ProfilerControlData;I)V

    #@245
    goto/16 :goto_a

    #@247
    :cond_247
    move v4, v5

    #@248
    goto :goto_23c

    #@249
    .line 1381
    :pswitch_249
    const-string v3, "backupCreateAgent"

    #@24b
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@24e
    .line 1382
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@250
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@252
    check-cast v3, Landroid/app/ActivityThread$CreateBackupAgentData;

    #@254
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2300(Landroid/app/ActivityThread;Landroid/app/ActivityThread$CreateBackupAgentData;)V

    #@257
    .line 1383
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@25a
    goto/16 :goto_a

    #@25c
    .line 1386
    :pswitch_25c
    const-string v3, "backupDestroyAgent"

    #@25e
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@261
    .line 1387
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@263
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@265
    check-cast v3, Landroid/app/ActivityThread$CreateBackupAgentData;

    #@267
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2400(Landroid/app/ActivityThread;Landroid/app/ActivityThread$CreateBackupAgentData;)V

    #@26a
    .line 1388
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@26d
    goto/16 :goto_a

    #@26f
    .line 1391
    :pswitch_26f
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@272
    move-result v3

    #@273
    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    #@276
    goto/16 :goto_a

    #@278
    .line 1394
    :pswitch_278
    const-string/jumbo v3, "providerRemove"

    #@27b
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@27e
    .line 1395
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@280
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@282
    check-cast v3, Landroid/app/ActivityThread$ProviderRefCount;

    #@284
    invoke-virtual {v4, v3}, Landroid/app/ActivityThread;->completeRemoveProvider(Landroid/app/ActivityThread$ProviderRefCount;)V

    #@287
    .line 1396
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@28a
    goto/16 :goto_a

    #@28c
    .line 1399
    :pswitch_28c
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@28e
    invoke-virtual {v3}, Landroid/app/ActivityThread;->ensureJitEnabled()V

    #@291
    goto/16 :goto_a

    #@293
    .line 1402
    :pswitch_293
    const-string v3, "broadcastPackage"

    #@295
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@298
    .line 1403
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@29a
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@29c
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29e
    check-cast v3, [Ljava/lang/String;

    #@2a0
    check-cast v3, [Ljava/lang/String;

    #@2a2
    invoke-virtual {v4, v5, v3}, Landroid/app/ActivityThread;->handleDispatchPackageBroadcast(I[Ljava/lang/String;)V

    #@2a5
    .line 1404
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@2a8
    goto/16 :goto_a

    #@2aa
    .line 1407
    :pswitch_2aa
    new-instance v4, Landroid/app/RemoteServiceException;

    #@2ac
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2ae
    check-cast v3, Ljava/lang/String;

    #@2b0
    invoke-direct {v4, v3}, Landroid/app/RemoteServiceException;-><init>(Ljava/lang/String;)V

    #@2b3
    throw v4

    #@2b4
    .line 1409
    :pswitch_2b4
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@2b6
    if-eqz v3, :cond_2c1

    #@2b8
    :goto_2b8
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2ba
    check-cast v3, Landroid/app/ActivityThread$DumpHeapData;

    #@2bc
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->handleDumpHeap(ZLandroid/app/ActivityThread$DumpHeapData;)V

    #@2bf
    goto/16 :goto_a

    #@2c1
    :cond_2c1
    move v4, v5

    #@2c2
    goto :goto_2b8

    #@2c3
    .line 1412
    :pswitch_2c3
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@2c5
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c7
    check-cast v3, Landroid/app/ActivityThread$DumpComponentInfo;

    #@2c9
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2500(Landroid/app/ActivityThread;Landroid/app/ActivityThread$DumpComponentInfo;)V

    #@2cc
    goto/16 :goto_a

    #@2ce
    .line 1415
    :pswitch_2ce
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@2d0
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d2
    check-cast v3, Landroid/app/ActivityThread$DumpComponentInfo;

    #@2d4
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2600(Landroid/app/ActivityThread;Landroid/app/ActivityThread$DumpComponentInfo;)V

    #@2d7
    goto/16 :goto_a

    #@2d9
    .line 1418
    :pswitch_2d9
    const-string/jumbo v3, "sleeping"

    #@2dc
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@2df
    .line 1419
    iget-object v6, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@2e1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e3
    check-cast v3, Landroid/os/IBinder;

    #@2e5
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@2e7
    if-eqz v7, :cond_2f1

    #@2e9
    :goto_2e9
    invoke-static {v6, v3, v4}, Landroid/app/ActivityThread;->access$2700(Landroid/app/ActivityThread;Landroid/os/IBinder;Z)V

    #@2ec
    .line 1420
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@2ef
    goto/16 :goto_a

    #@2f1
    :cond_2f1
    move v4, v5

    #@2f2
    .line 1419
    goto :goto_2e9

    #@2f3
    .line 1423
    :pswitch_2f3
    const-string/jumbo v3, "setCoreSettings"

    #@2f6
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@2f9
    .line 1424
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@2fb
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2fd
    check-cast v3, Landroid/os/Bundle;

    #@2ff
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2800(Landroid/app/ActivityThread;Landroid/os/Bundle;)V

    #@302
    .line 1425
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@305
    goto/16 :goto_a

    #@307
    .line 1428
    :pswitch_307
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@309
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@30b
    check-cast v3, Landroid/app/ActivityThread$UpdateCompatibilityData;

    #@30d
    invoke-static {v4, v3}, Landroid/app/ActivityThread;->access$2900(Landroid/app/ActivityThread;Landroid/app/ActivityThread$UpdateCompatibilityData;)V

    #@310
    goto/16 :goto_a

    #@312
    .line 1431
    :pswitch_312
    const-string/jumbo v3, "trimMemory"

    #@315
    invoke-static {v8, v9, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    #@318
    .line 1432
    iget-object v3, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@31a
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@31c
    invoke-virtual {v3, v4}, Landroid/app/ActivityThread;->handleTrimMemory(I)V

    #@31f
    .line 1433
    invoke-static {v8, v9}, Landroid/os/Trace;->traceEnd(J)V

    #@322
    goto/16 :goto_a

    #@324
    .line 1436
    :pswitch_324
    iget-object v4, p0, Landroid/app/ActivityThread$H;->this$0:Landroid/app/ActivityThread;

    #@326
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@328
    check-cast v3, Landroid/os/IBinder;

    #@32a
    invoke-virtual {v4, v3, v5}, Landroid/app/ActivityThread;->handleUnstableProviderDied(Landroid/os/IBinder;Z)V

    #@32d
    goto/16 :goto_a

    #@32f
    .line 1233
    nop

    #@330
    :pswitch_data_330
    .packed-switch 0x64
        :pswitch_b
        :pswitch_3d
        :pswitch_5a
        :pswitch_73
        :pswitch_87
        :pswitch_9c
        :pswitch_af
        :pswitch_c2
        :pswitch_da
        :pswitch_ed
        :pswitch_108
        :pswitch_11b
        :pswitch_131
        :pswitch_144
        :pswitch_15a
        :pswitch_196
        :pswitch_1aa
        :pswitch_1c1
        :pswitch_1d5
        :pswitch_1f2
        :pswitch_201
        :pswitch_16e
        :pswitch_182
        :pswitch_208
        :pswitch_213
        :pswitch_223
        :pswitch_2b
        :pswitch_236
        :pswitch_249
        :pswitch_25c
        :pswitch_26f
        :pswitch_278
        :pswitch_28c
        :pswitch_293
        :pswitch_2aa
        :pswitch_2b4
        :pswitch_2c3
        :pswitch_2d9
        :pswitch_2f3
        :pswitch_307
        :pswitch_312
        :pswitch_2ce
        :pswitch_324
    .end packed-switch
.end method
