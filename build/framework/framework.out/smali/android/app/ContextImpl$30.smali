.class final Landroid/app/ContextImpl$30;
.super Landroid/app/ContextImpl$ServiceFetcher;
.source "ContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ContextImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 488
    invoke-direct {p0}, Landroid/app/ContextImpl$ServiceFetcher;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createService(Landroid/app/ContextImpl;)Ljava/lang/Object;
    .registers 5
    .parameter "ctx"

    #@0
    .prologue
    .line 491
    :try_start_0
    new-instance v1, Landroid/os/storage/StorageManager;

    #@2
    iget-object v2, p1, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@4
    invoke-virtual {v2}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@b
    move-result-object v2

    #@c
    invoke-direct {v1, v2}, Landroid/os/storage/StorageManager;-><init>(Landroid/os/Looper;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_f} :catch_10

    #@f
    .line 494
    :goto_f
    return-object v1

    #@10
    .line 492
    :catch_10
    move-exception v0

    #@11
    .line 493
    .local v0, rex:Landroid/os/RemoteException;
    const-string v1, "ContextImpl"

    #@13
    const-string v2, "Failed to create StorageManager"

    #@15
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 494
    const/4 v1, 0x0

    #@19
    goto :goto_f
.end method
